package com.android.internal.telecom;

import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.telecom.CallAudioState;
import android.telecom.ConnectionRequest;
import android.telecom.Logging.Session;
import android.telecom.PhoneAccountHandle;
import java.util.ArrayList;
import java.util.List;

public interface IConnectionService extends IInterface {
  void abort(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void addConferenceParticipants(String paramString, List<Uri> paramList, Session.Info paramInfo) throws RemoteException;
  
  void addConnectionServiceAdapter(IConnectionServiceAdapter paramIConnectionServiceAdapter, Session.Info paramInfo) throws RemoteException;
  
  void answer(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void answerVideo(String paramString, int paramInt, Session.Info paramInfo) throws RemoteException;
  
  void conference(String paramString1, String paramString2, Session.Info paramInfo) throws RemoteException;
  
  void connectionServiceFocusGained(Session.Info paramInfo) throws RemoteException;
  
  void connectionServiceFocusLost(Session.Info paramInfo) throws RemoteException;
  
  void consultativeTransfer(String paramString1, String paramString2, Session.Info paramInfo) throws RemoteException;
  
  void createConference(PhoneAccountHandle paramPhoneAccountHandle, String paramString, ConnectionRequest paramConnectionRequest, boolean paramBoolean1, boolean paramBoolean2, Session.Info paramInfo) throws RemoteException;
  
  void createConferenceComplete(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void createConferenceFailed(PhoneAccountHandle paramPhoneAccountHandle, String paramString, ConnectionRequest paramConnectionRequest, boolean paramBoolean, Session.Info paramInfo) throws RemoteException;
  
  void createConnection(PhoneAccountHandle paramPhoneAccountHandle, String paramString, ConnectionRequest paramConnectionRequest, boolean paramBoolean1, boolean paramBoolean2, Session.Info paramInfo) throws RemoteException;
  
  void createConnectionComplete(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void createConnectionFailed(PhoneAccountHandle paramPhoneAccountHandle, String paramString, ConnectionRequest paramConnectionRequest, boolean paramBoolean, Session.Info paramInfo) throws RemoteException;
  
  void deflect(String paramString, Uri paramUri, Session.Info paramInfo) throws RemoteException;
  
  void disconnect(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void handoverComplete(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void handoverFailed(String paramString, ConnectionRequest paramConnectionRequest, int paramInt, Session.Info paramInfo) throws RemoteException;
  
  void hold(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void mergeConference(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void onCallAudioStateChanged(String paramString, CallAudioState paramCallAudioState, Session.Info paramInfo) throws RemoteException;
  
  void onExtrasChanged(String paramString, Bundle paramBundle, Session.Info paramInfo) throws RemoteException;
  
  void onPostDialContinue(String paramString, boolean paramBoolean, Session.Info paramInfo) throws RemoteException;
  
  void playDtmfTone(String paramString, char paramChar, Session.Info paramInfo) throws RemoteException;
  
  void pullExternalCall(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void reject(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void rejectWithMessage(String paramString1, String paramString2, Session.Info paramInfo) throws RemoteException;
  
  void rejectWithReason(String paramString, int paramInt, Session.Info paramInfo) throws RemoteException;
  
  void removeConnectionServiceAdapter(IConnectionServiceAdapter paramIConnectionServiceAdapter, Session.Info paramInfo) throws RemoteException;
  
  void respondToRttUpgradeRequest(String paramString, ParcelFileDescriptor paramParcelFileDescriptor1, ParcelFileDescriptor paramParcelFileDescriptor2, Session.Info paramInfo) throws RemoteException;
  
  void sendCallEvent(String paramString1, String paramString2, Bundle paramBundle, Session.Info paramInfo) throws RemoteException;
  
  void silence(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void splitFromConference(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void startRtt(String paramString, ParcelFileDescriptor paramParcelFileDescriptor1, ParcelFileDescriptor paramParcelFileDescriptor2, Session.Info paramInfo) throws RemoteException;
  
  void stopDtmfTone(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void stopRtt(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void swapConference(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void transfer(String paramString, Uri paramUri, boolean paramBoolean, Session.Info paramInfo) throws RemoteException;
  
  void unhold(String paramString, Session.Info paramInfo) throws RemoteException;
  
  class Default implements IConnectionService {
    public void addConnectionServiceAdapter(IConnectionServiceAdapter param1IConnectionServiceAdapter, Session.Info param1Info) throws RemoteException {}
    
    public void removeConnectionServiceAdapter(IConnectionServiceAdapter param1IConnectionServiceAdapter, Session.Info param1Info) throws RemoteException {}
    
    public void createConnection(PhoneAccountHandle param1PhoneAccountHandle, String param1String, ConnectionRequest param1ConnectionRequest, boolean param1Boolean1, boolean param1Boolean2, Session.Info param1Info) throws RemoteException {}
    
    public void createConnectionComplete(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void createConnectionFailed(PhoneAccountHandle param1PhoneAccountHandle, String param1String, ConnectionRequest param1ConnectionRequest, boolean param1Boolean, Session.Info param1Info) throws RemoteException {}
    
    public void createConference(PhoneAccountHandle param1PhoneAccountHandle, String param1String, ConnectionRequest param1ConnectionRequest, boolean param1Boolean1, boolean param1Boolean2, Session.Info param1Info) throws RemoteException {}
    
    public void createConferenceComplete(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void createConferenceFailed(PhoneAccountHandle param1PhoneAccountHandle, String param1String, ConnectionRequest param1ConnectionRequest, boolean param1Boolean, Session.Info param1Info) throws RemoteException {}
    
    public void abort(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void answerVideo(String param1String, int param1Int, Session.Info param1Info) throws RemoteException {}
    
    public void answer(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void deflect(String param1String, Uri param1Uri, Session.Info param1Info) throws RemoteException {}
    
    public void reject(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void rejectWithReason(String param1String, int param1Int, Session.Info param1Info) throws RemoteException {}
    
    public void rejectWithMessage(String param1String1, String param1String2, Session.Info param1Info) throws RemoteException {}
    
    public void transfer(String param1String, Uri param1Uri, boolean param1Boolean, Session.Info param1Info) throws RemoteException {}
    
    public void consultativeTransfer(String param1String1, String param1String2, Session.Info param1Info) throws RemoteException {}
    
    public void disconnect(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void silence(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void hold(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void unhold(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void onCallAudioStateChanged(String param1String, CallAudioState param1CallAudioState, Session.Info param1Info) throws RemoteException {}
    
    public void playDtmfTone(String param1String, char param1Char, Session.Info param1Info) throws RemoteException {}
    
    public void stopDtmfTone(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void conference(String param1String1, String param1String2, Session.Info param1Info) throws RemoteException {}
    
    public void splitFromConference(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void mergeConference(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void swapConference(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void addConferenceParticipants(String param1String, List<Uri> param1List, Session.Info param1Info) throws RemoteException {}
    
    public void onPostDialContinue(String param1String, boolean param1Boolean, Session.Info param1Info) throws RemoteException {}
    
    public void pullExternalCall(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void sendCallEvent(String param1String1, String param1String2, Bundle param1Bundle, Session.Info param1Info) throws RemoteException {}
    
    public void onExtrasChanged(String param1String, Bundle param1Bundle, Session.Info param1Info) throws RemoteException {}
    
    public void startRtt(String param1String, ParcelFileDescriptor param1ParcelFileDescriptor1, ParcelFileDescriptor param1ParcelFileDescriptor2, Session.Info param1Info) throws RemoteException {}
    
    public void stopRtt(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void respondToRttUpgradeRequest(String param1String, ParcelFileDescriptor param1ParcelFileDescriptor1, ParcelFileDescriptor param1ParcelFileDescriptor2, Session.Info param1Info) throws RemoteException {}
    
    public void connectionServiceFocusLost(Session.Info param1Info) throws RemoteException {}
    
    public void connectionServiceFocusGained(Session.Info param1Info) throws RemoteException {}
    
    public void handoverFailed(String param1String, ConnectionRequest param1ConnectionRequest, int param1Int, Session.Info param1Info) throws RemoteException {}
    
    public void handoverComplete(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IConnectionService {
    private static final String DESCRIPTOR = "com.android.internal.telecom.IConnectionService";
    
    static final int TRANSACTION_abort = 9;
    
    static final int TRANSACTION_addConferenceParticipants = 29;
    
    static final int TRANSACTION_addConnectionServiceAdapter = 1;
    
    static final int TRANSACTION_answer = 11;
    
    static final int TRANSACTION_answerVideo = 10;
    
    static final int TRANSACTION_conference = 25;
    
    static final int TRANSACTION_connectionServiceFocusGained = 38;
    
    static final int TRANSACTION_connectionServiceFocusLost = 37;
    
    static final int TRANSACTION_consultativeTransfer = 17;
    
    static final int TRANSACTION_createConference = 6;
    
    static final int TRANSACTION_createConferenceComplete = 7;
    
    static final int TRANSACTION_createConferenceFailed = 8;
    
    static final int TRANSACTION_createConnection = 3;
    
    static final int TRANSACTION_createConnectionComplete = 4;
    
    static final int TRANSACTION_createConnectionFailed = 5;
    
    static final int TRANSACTION_deflect = 12;
    
    static final int TRANSACTION_disconnect = 18;
    
    static final int TRANSACTION_handoverComplete = 40;
    
    static final int TRANSACTION_handoverFailed = 39;
    
    static final int TRANSACTION_hold = 20;
    
    static final int TRANSACTION_mergeConference = 27;
    
    static final int TRANSACTION_onCallAudioStateChanged = 22;
    
    static final int TRANSACTION_onExtrasChanged = 33;
    
    static final int TRANSACTION_onPostDialContinue = 30;
    
    static final int TRANSACTION_playDtmfTone = 23;
    
    static final int TRANSACTION_pullExternalCall = 31;
    
    static final int TRANSACTION_reject = 13;
    
    static final int TRANSACTION_rejectWithMessage = 15;
    
    static final int TRANSACTION_rejectWithReason = 14;
    
    static final int TRANSACTION_removeConnectionServiceAdapter = 2;
    
    static final int TRANSACTION_respondToRttUpgradeRequest = 36;
    
    static final int TRANSACTION_sendCallEvent = 32;
    
    static final int TRANSACTION_silence = 19;
    
    static final int TRANSACTION_splitFromConference = 26;
    
    static final int TRANSACTION_startRtt = 34;
    
    static final int TRANSACTION_stopDtmfTone = 24;
    
    static final int TRANSACTION_stopRtt = 35;
    
    static final int TRANSACTION_swapConference = 28;
    
    static final int TRANSACTION_transfer = 16;
    
    static final int TRANSACTION_unhold = 21;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telecom.IConnectionService");
    }
    
    public static IConnectionService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telecom.IConnectionService");
      if (iInterface != null && iInterface instanceof IConnectionService)
        return (IConnectionService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 40:
          return "handoverComplete";
        case 39:
          return "handoverFailed";
        case 38:
          return "connectionServiceFocusGained";
        case 37:
          return "connectionServiceFocusLost";
        case 36:
          return "respondToRttUpgradeRequest";
        case 35:
          return "stopRtt";
        case 34:
          return "startRtt";
        case 33:
          return "onExtrasChanged";
        case 32:
          return "sendCallEvent";
        case 31:
          return "pullExternalCall";
        case 30:
          return "onPostDialContinue";
        case 29:
          return "addConferenceParticipants";
        case 28:
          return "swapConference";
        case 27:
          return "mergeConference";
        case 26:
          return "splitFromConference";
        case 25:
          return "conference";
        case 24:
          return "stopDtmfTone";
        case 23:
          return "playDtmfTone";
        case 22:
          return "onCallAudioStateChanged";
        case 21:
          return "unhold";
        case 20:
          return "hold";
        case 19:
          return "silence";
        case 18:
          return "disconnect";
        case 17:
          return "consultativeTransfer";
        case 16:
          return "transfer";
        case 15:
          return "rejectWithMessage";
        case 14:
          return "rejectWithReason";
        case 13:
          return "reject";
        case 12:
          return "deflect";
        case 11:
          return "answer";
        case 10:
          return "answerVideo";
        case 9:
          return "abort";
        case 8:
          return "createConferenceFailed";
        case 7:
          return "createConferenceComplete";
        case 6:
          return "createConference";
        case 5:
          return "createConnectionFailed";
        case 4:
          return "createConnectionComplete";
        case 3:
          return "createConnection";
        case 2:
          return "removeConnectionServiceAdapter";
        case 1:
          break;
      } 
      return "addConnectionServiceAdapter";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IConnectionServiceAdapter iConnectionServiceAdapter;
      if (param1Int1 != 1598968902) {
        String str1, str3;
        ArrayList<Uri> arrayList;
        String str2, str4;
        char c;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 40:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            handoverComplete(str1, (Session.Info)param1Parcel1);
            return true;
          case 39:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str3 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              ConnectionRequest connectionRequest = (ConnectionRequest)ConnectionRequest.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str1 = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            handoverFailed(str3, (ConnectionRequest)str1, param1Int1, (Session.Info)param1Parcel1);
            return true;
          case 38:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            connectionServiceFocusGained((Session.Info)param1Parcel1);
            return true;
          case 37:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            connectionServiceFocusLost((Session.Info)param1Parcel1);
            return true;
          case 36:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str4 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str1 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str3 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            respondToRttUpgradeRequest(str4, (ParcelFileDescriptor)str1, (ParcelFileDescriptor)str3, (Session.Info)param1Parcel1);
            return true;
          case 35:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            stopRtt(str1, (Session.Info)param1Parcel1);
            return true;
          case 34:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str4 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str1 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str3 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            startRtt(str4, (ParcelFileDescriptor)str1, (ParcelFileDescriptor)str3, (Session.Info)param1Parcel1);
            return true;
          case 33:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str3 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str1 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onExtrasChanged(str3, (Bundle)str1, (Session.Info)param1Parcel1);
            return true;
          case 32:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str4 = param1Parcel1.readString();
            str3 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str1 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            sendCallEvent(str4, str3, (Bundle)str1, (Session.Info)param1Parcel1);
            return true;
          case 31:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            pullExternalCall(str1, (Session.Info)param1Parcel1);
            return true;
          case 30:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            bool1 = bool2;
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onPostDialContinue(str1, bool1, (Session.Info)param1Parcel1);
            return true;
          case 29:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            arrayList = param1Parcel1.createTypedArrayList(Uri.CREATOR);
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            addConferenceParticipants(str1, arrayList, (Session.Info)param1Parcel1);
            return true;
          case 28:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            swapConference(str1, (Session.Info)param1Parcel1);
            return true;
          case 27:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            mergeConference(str1, (Session.Info)param1Parcel1);
            return true;
          case 26:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            splitFromConference(str1, (Session.Info)param1Parcel1);
            return true;
          case 25:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str2 = param1Parcel1.readString();
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            conference(str2, str1, (Session.Info)param1Parcel1);
            return true;
          case 24:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            stopDtmfTone(str1, (Session.Info)param1Parcel1);
            return true;
          case 23:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            c = (char)param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            playDtmfTone(str1, c, (Session.Info)param1Parcel1);
            return true;
          case 22:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str2 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              CallAudioState callAudioState = (CallAudioState)CallAudioState.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str1 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onCallAudioStateChanged(str2, (CallAudioState)str1, (Session.Info)param1Parcel1);
            return true;
          case 21:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            unhold(str1, (Session.Info)param1Parcel1);
            return true;
          case 20:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            hold(str1, (Session.Info)param1Parcel1);
            return true;
          case 19:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            silence(str1, (Session.Info)param1Parcel1);
            return true;
          case 18:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            disconnect(str1, (Session.Info)param1Parcel1);
            return true;
          case 17:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str2 = param1Parcel1.readString();
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            consultativeTransfer(str2, str1, (Session.Info)param1Parcel1);
            return true;
          case 16:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str2 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str1 = null;
            } 
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            transfer(str2, (Uri)str1, bool1, (Session.Info)param1Parcel1);
            return true;
          case 15:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str2 = param1Parcel1.readString();
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            rejectWithMessage(str2, str1, (Session.Info)param1Parcel1);
            return true;
          case 14:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            rejectWithReason(str1, param1Int1, (Session.Info)param1Parcel1);
            return true;
          case 13:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            reject(str1, (Session.Info)param1Parcel1);
            return true;
          case 12:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str2 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str1 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            deflect(str2, (Uri)str1, (Session.Info)param1Parcel1);
            return true;
          case 11:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            answer(str1, (Session.Info)param1Parcel1);
            return true;
          case 10:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            answerVideo(str1, param1Int1, (Session.Info)param1Parcel1);
            return true;
          case 9:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            abort(str1, (Session.Info)param1Parcel1);
            return true;
          case 8:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            if (param1Parcel1.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str1 = null;
            } 
            str4 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              ConnectionRequest connectionRequest = (ConnectionRequest)ConnectionRequest.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            createConferenceFailed((PhoneAccountHandle)str1, str4, (ConnectionRequest)str2, bool1, (Session.Info)param1Parcel1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            createConferenceComplete(str1, (Session.Info)param1Parcel1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            if (param1Parcel1.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str1 = null;
            } 
            str4 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              ConnectionRequest connectionRequest = (ConnectionRequest)ConnectionRequest.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            if (param1Parcel1.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            createConference((PhoneAccountHandle)str1, str4, (ConnectionRequest)str2, bool1, bool2, (Session.Info)param1Parcel1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            if (param1Parcel1.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str1 = null;
            } 
            str4 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              ConnectionRequest connectionRequest = (ConnectionRequest)ConnectionRequest.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            createConnectionFailed((PhoneAccountHandle)str1, str4, (ConnectionRequest)str2, bool1, (Session.Info)param1Parcel1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            createConnectionComplete(str1, (Session.Info)param1Parcel1);
            return true;
          case 3:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            if (param1Parcel1.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str1 = null;
            } 
            str4 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              ConnectionRequest connectionRequest = (ConnectionRequest)ConnectionRequest.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            if (param1Parcel1.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            createConnection((PhoneAccountHandle)str1, str4, (ConnectionRequest)str2, bool1, bool2, (Session.Info)param1Parcel1);
            return true;
          case 2:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
            iConnectionServiceAdapter = IConnectionServiceAdapter.Stub.asInterface(param1Parcel1.readStrongBinder());
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            removeConnectionServiceAdapter(iConnectionServiceAdapter, (Session.Info)param1Parcel1);
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionService");
        iConnectionServiceAdapter = IConnectionServiceAdapter.Stub.asInterface(param1Parcel1.readStrongBinder());
        if (param1Parcel1.readInt() != 0) {
          Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        addConnectionServiceAdapter(iConnectionServiceAdapter, (Session.Info)param1Parcel1);
        return true;
      } 
      iConnectionServiceAdapter.writeString("com.android.internal.telecom.IConnectionService");
      return true;
    }
    
    private static class Proxy implements IConnectionService {
      public static IConnectionService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telecom.IConnectionService";
      }
      
      public void addConnectionServiceAdapter(IConnectionServiceAdapter param2IConnectionServiceAdapter, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          if (param2IConnectionServiceAdapter != null) {
            iBinder = param2IConnectionServiceAdapter.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().addConnectionServiceAdapter(param2IConnectionServiceAdapter, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeConnectionServiceAdapter(IConnectionServiceAdapter param2IConnectionServiceAdapter, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          if (param2IConnectionServiceAdapter != null) {
            iBinder = param2IConnectionServiceAdapter.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().removeConnectionServiceAdapter(param2IConnectionServiceAdapter, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void createConnection(PhoneAccountHandle param2PhoneAccountHandle, String param2String, ConnectionRequest param2ConnectionRequest, boolean param2Boolean1, boolean param2Boolean2, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          if (param2PhoneAccountHandle != null) {
            parcel.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          try {
            boolean bool;
            parcel.writeString(param2String);
            if (param2ConnectionRequest != null) {
              parcel.writeInt(1);
              param2ConnectionRequest.writeToParcel(parcel, 0);
            } else {
              parcel.writeInt(0);
            } 
            if (param2Boolean1) {
              bool = true;
            } else {
              bool = false;
            } 
            parcel.writeInt(bool);
            if (param2Boolean2) {
              bool = true;
            } else {
              bool = false;
            } 
            parcel.writeInt(bool);
            if (param2Info != null) {
              parcel.writeInt(1);
              param2Info.writeToParcel(parcel, 0);
            } else {
              parcel.writeInt(0);
            } 
            try {
              boolean bool1 = this.mRemote.transact(3, parcel, null, 1);
              if (!bool1 && IConnectionService.Stub.getDefaultImpl() != null) {
                IConnectionService.Stub.getDefaultImpl().createConnection(param2PhoneAccountHandle, param2String, param2ConnectionRequest, param2Boolean1, param2Boolean2, param2Info);
                parcel.recycle();
                return;
              } 
              parcel.recycle();
              return;
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2PhoneAccountHandle;
      }
      
      public void createConnectionComplete(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().createConnectionComplete(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void createConnectionFailed(PhoneAccountHandle param2PhoneAccountHandle, String param2String, ConnectionRequest param2ConnectionRequest, boolean param2Boolean, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          if (param2PhoneAccountHandle != null) {
            parcel.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeString(param2String);
          if (param2ConnectionRequest != null) {
            parcel.writeInt(1);
            param2ConnectionRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool1 = this.mRemote.transact(5, parcel, null, 1);
          if (!bool1 && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().createConnectionFailed(param2PhoneAccountHandle, param2String, param2ConnectionRequest, param2Boolean, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void createConference(PhoneAccountHandle param2PhoneAccountHandle, String param2String, ConnectionRequest param2ConnectionRequest, boolean param2Boolean1, boolean param2Boolean2, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          if (param2PhoneAccountHandle != null) {
            parcel.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          try {
            boolean bool;
            parcel.writeString(param2String);
            if (param2ConnectionRequest != null) {
              parcel.writeInt(1);
              param2ConnectionRequest.writeToParcel(parcel, 0);
            } else {
              parcel.writeInt(0);
            } 
            if (param2Boolean1) {
              bool = true;
            } else {
              bool = false;
            } 
            parcel.writeInt(bool);
            if (param2Boolean2) {
              bool = true;
            } else {
              bool = false;
            } 
            parcel.writeInt(bool);
            if (param2Info != null) {
              parcel.writeInt(1);
              param2Info.writeToParcel(parcel, 0);
            } else {
              parcel.writeInt(0);
            } 
            try {
              boolean bool1 = this.mRemote.transact(6, parcel, null, 1);
              if (!bool1 && IConnectionService.Stub.getDefaultImpl() != null) {
                IConnectionService.Stub.getDefaultImpl().createConference(param2PhoneAccountHandle, param2String, param2ConnectionRequest, param2Boolean1, param2Boolean2, param2Info);
                parcel.recycle();
                return;
              } 
              parcel.recycle();
              return;
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2PhoneAccountHandle;
      }
      
      public void createConferenceComplete(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().createConferenceComplete(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void createConferenceFailed(PhoneAccountHandle param2PhoneAccountHandle, String param2String, ConnectionRequest param2ConnectionRequest, boolean param2Boolean, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          if (param2PhoneAccountHandle != null) {
            parcel.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeString(param2String);
          if (param2ConnectionRequest != null) {
            parcel.writeInt(1);
            param2ConnectionRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool1 = this.mRemote.transact(8, parcel, null, 1);
          if (!bool1 && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().createConferenceFailed(param2PhoneAccountHandle, param2String, param2ConnectionRequest, param2Boolean, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void abort(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().abort(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void answerVideo(String param2String, int param2Int, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().answerVideo(param2String, param2Int, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void answer(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().answer(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void deflect(String param2String, Uri param2Uri, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(12, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().deflect(param2String, param2Uri, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void reject(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(13, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().reject(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void rejectWithReason(String param2String, int param2Int, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(14, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().rejectWithReason(param2String, param2Int, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void rejectWithMessage(String param2String1, String param2String2, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(15, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().rejectWithMessage(param2String1, param2String2, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void transfer(String param2String, Uri param2Uri, boolean param2Boolean, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool1 = this.mRemote.transact(16, parcel, null, 1);
          if (!bool1 && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().transfer(param2String, param2Uri, param2Boolean, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void consultativeTransfer(String param2String1, String param2String2, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(17, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().consultativeTransfer(param2String1, param2String2, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void disconnect(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(18, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().disconnect(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void silence(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(19, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().silence(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void hold(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(20, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().hold(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void unhold(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(21, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().unhold(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCallAudioStateChanged(String param2String, CallAudioState param2CallAudioState, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2CallAudioState != null) {
            parcel.writeInt(1);
            param2CallAudioState.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(22, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().onCallAudioStateChanged(param2String, param2CallAudioState, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void playDtmfTone(String param2String, char param2Char, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          parcel.writeInt(param2Char);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(23, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().playDtmfTone(param2String, param2Char, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void stopDtmfTone(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(24, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().stopDtmfTone(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void conference(String param2String1, String param2String2, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(25, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().conference(param2String1, param2String2, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void splitFromConference(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(26, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().splitFromConference(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void mergeConference(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(27, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().mergeConference(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void swapConference(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(28, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().swapConference(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void addConferenceParticipants(String param2String, List<Uri> param2List, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          parcel.writeTypedList(param2List);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(29, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().addConferenceParticipants(param2String, param2List, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPostDialContinue(String param2String, boolean param2Boolean, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool1 = this.mRemote.transact(30, parcel, null, 1);
          if (!bool1 && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().onPostDialContinue(param2String, param2Boolean, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void pullExternalCall(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(31, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().pullExternalCall(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendCallEvent(String param2String1, String param2String2, Bundle param2Bundle, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(32, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().sendCallEvent(param2String1, param2String2, param2Bundle, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onExtrasChanged(String param2String, Bundle param2Bundle, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(33, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().onExtrasChanged(param2String, param2Bundle, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void startRtt(String param2String, ParcelFileDescriptor param2ParcelFileDescriptor1, ParcelFileDescriptor param2ParcelFileDescriptor2, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2ParcelFileDescriptor1 != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor1.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ParcelFileDescriptor2 != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor2.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(34, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().startRtt(param2String, param2ParcelFileDescriptor1, param2ParcelFileDescriptor2, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void stopRtt(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(35, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().stopRtt(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void respondToRttUpgradeRequest(String param2String, ParcelFileDescriptor param2ParcelFileDescriptor1, ParcelFileDescriptor param2ParcelFileDescriptor2, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2ParcelFileDescriptor1 != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor1.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ParcelFileDescriptor2 != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor2.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(36, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().respondToRttUpgradeRequest(param2String, param2ParcelFileDescriptor1, param2ParcelFileDescriptor2, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void connectionServiceFocusLost(Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(37, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().connectionServiceFocusLost(param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void connectionServiceFocusGained(Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(38, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().connectionServiceFocusGained(param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void handoverFailed(String param2String, ConnectionRequest param2ConnectionRequest, int param2Int, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2ConnectionRequest != null) {
            parcel.writeInt(1);
            param2ConnectionRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(39, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().handoverFailed(param2String, param2ConnectionRequest, param2Int, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void handoverComplete(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionService");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(40, parcel, null, 1);
          if (!bool && IConnectionService.Stub.getDefaultImpl() != null) {
            IConnectionService.Stub.getDefaultImpl().handoverComplete(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IConnectionService param1IConnectionService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IConnectionService != null) {
          Proxy.sDefaultImpl = param1IConnectionService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IConnectionService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
