package com.android.internal.telecom;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telecom.CallAudioState;
import android.telecom.ParcelableCall;

public interface IInCallService extends IInterface {
  void addCall(ParcelableCall paramParcelableCall) throws RemoteException;
  
  void bringToForeground(boolean paramBoolean) throws RemoteException;
  
  void onCallAudioStateChanged(CallAudioState paramCallAudioState) throws RemoteException;
  
  void onCanAddCallChanged(boolean paramBoolean) throws RemoteException;
  
  void onConnectionEvent(String paramString1, String paramString2, Bundle paramBundle) throws RemoteException;
  
  void onHandoverComplete(String paramString) throws RemoteException;
  
  void onHandoverFailed(String paramString, int paramInt) throws RemoteException;
  
  void onRttInitiationFailure(String paramString, int paramInt) throws RemoteException;
  
  void onRttUpgradeRequest(String paramString, int paramInt) throws RemoteException;
  
  void setInCallAdapter(IInCallAdapter paramIInCallAdapter) throws RemoteException;
  
  void setPostDial(String paramString1, String paramString2) throws RemoteException;
  
  void setPostDialWait(String paramString1, String paramString2) throws RemoteException;
  
  void silenceRinger() throws RemoteException;
  
  void updateCall(ParcelableCall paramParcelableCall) throws RemoteException;
  
  class Default implements IInCallService {
    public void setInCallAdapter(IInCallAdapter param1IInCallAdapter) throws RemoteException {}
    
    public void addCall(ParcelableCall param1ParcelableCall) throws RemoteException {}
    
    public void updateCall(ParcelableCall param1ParcelableCall) throws RemoteException {}
    
    public void setPostDial(String param1String1, String param1String2) throws RemoteException {}
    
    public void setPostDialWait(String param1String1, String param1String2) throws RemoteException {}
    
    public void onCallAudioStateChanged(CallAudioState param1CallAudioState) throws RemoteException {}
    
    public void bringToForeground(boolean param1Boolean) throws RemoteException {}
    
    public void onCanAddCallChanged(boolean param1Boolean) throws RemoteException {}
    
    public void silenceRinger() throws RemoteException {}
    
    public void onConnectionEvent(String param1String1, String param1String2, Bundle param1Bundle) throws RemoteException {}
    
    public void onRttUpgradeRequest(String param1String, int param1Int) throws RemoteException {}
    
    public void onRttInitiationFailure(String param1String, int param1Int) throws RemoteException {}
    
    public void onHandoverFailed(String param1String, int param1Int) throws RemoteException {}
    
    public void onHandoverComplete(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInCallService {
    private static final String DESCRIPTOR = "com.android.internal.telecom.IInCallService";
    
    static final int TRANSACTION_addCall = 2;
    
    static final int TRANSACTION_bringToForeground = 7;
    
    static final int TRANSACTION_onCallAudioStateChanged = 6;
    
    static final int TRANSACTION_onCanAddCallChanged = 8;
    
    static final int TRANSACTION_onConnectionEvent = 10;
    
    static final int TRANSACTION_onHandoverComplete = 14;
    
    static final int TRANSACTION_onHandoverFailed = 13;
    
    static final int TRANSACTION_onRttInitiationFailure = 12;
    
    static final int TRANSACTION_onRttUpgradeRequest = 11;
    
    static final int TRANSACTION_setInCallAdapter = 1;
    
    static final int TRANSACTION_setPostDial = 4;
    
    static final int TRANSACTION_setPostDialWait = 5;
    
    static final int TRANSACTION_silenceRinger = 9;
    
    static final int TRANSACTION_updateCall = 3;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telecom.IInCallService");
    }
    
    public static IInCallService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telecom.IInCallService");
      if (iInterface != null && iInterface instanceof IInCallService)
        return (IInCallService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 14:
          return "onHandoverComplete";
        case 13:
          return "onHandoverFailed";
        case 12:
          return "onRttInitiationFailure";
        case 11:
          return "onRttUpgradeRequest";
        case 10:
          return "onConnectionEvent";
        case 9:
          return "silenceRinger";
        case 8:
          return "onCanAddCallChanged";
        case 7:
          return "bringToForeground";
        case 6:
          return "onCallAudioStateChanged";
        case 5:
          return "setPostDialWait";
        case 4:
          return "setPostDial";
        case 3:
          return "updateCall";
        case 2:
          return "addCall";
        case 1:
          break;
      } 
      return "setInCallAdapter";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        String str1, str2;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 14:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IInCallService");
            str1 = param1Parcel1.readString();
            onHandoverComplete(str1);
            return true;
          case 13:
            str1.enforceInterface("com.android.internal.telecom.IInCallService");
            str = str1.readString();
            param1Int1 = str1.readInt();
            onHandoverFailed(str, param1Int1);
            return true;
          case 12:
            str1.enforceInterface("com.android.internal.telecom.IInCallService");
            str = str1.readString();
            param1Int1 = str1.readInt();
            onRttInitiationFailure(str, param1Int1);
            return true;
          case 11:
            str1.enforceInterface("com.android.internal.telecom.IInCallService");
            str = str1.readString();
            param1Int1 = str1.readInt();
            onRttUpgradeRequest(str, param1Int1);
            return true;
          case 10:
            str1.enforceInterface("com.android.internal.telecom.IInCallService");
            str2 = str1.readString();
            str = str1.readString();
            if (str1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            onConnectionEvent(str2, str, (Bundle)str1);
            return true;
          case 9:
            str1.enforceInterface("com.android.internal.telecom.IInCallService");
            silenceRinger();
            return true;
          case 8:
            str1.enforceInterface("com.android.internal.telecom.IInCallService");
            bool1 = bool2;
            if (str1.readInt() != 0)
              bool1 = true; 
            onCanAddCallChanged(bool1);
            return true;
          case 7:
            str1.enforceInterface("com.android.internal.telecom.IInCallService");
            if (str1.readInt() != 0)
              bool1 = true; 
            bringToForeground(bool1);
            return true;
          case 6:
            str1.enforceInterface("com.android.internal.telecom.IInCallService");
            if (str1.readInt() != 0) {
              CallAudioState callAudioState = (CallAudioState)CallAudioState.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            onCallAudioStateChanged((CallAudioState)str1);
            return true;
          case 5:
            str1.enforceInterface("com.android.internal.telecom.IInCallService");
            str = str1.readString();
            str1 = str1.readString();
            setPostDialWait(str, str1);
            return true;
          case 4:
            str1.enforceInterface("com.android.internal.telecom.IInCallService");
            str = str1.readString();
            str1 = str1.readString();
            setPostDial(str, str1);
            return true;
          case 3:
            str1.enforceInterface("com.android.internal.telecom.IInCallService");
            if (str1.readInt() != 0) {
              ParcelableCall parcelableCall = (ParcelableCall)ParcelableCall.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            updateCall((ParcelableCall)str1);
            return true;
          case 2:
            str1.enforceInterface("com.android.internal.telecom.IInCallService");
            if (str1.readInt() != 0) {
              ParcelableCall parcelableCall = (ParcelableCall)ParcelableCall.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            addCall((ParcelableCall)str1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("com.android.internal.telecom.IInCallService");
        IInCallAdapter iInCallAdapter = IInCallAdapter.Stub.asInterface(str1.readStrongBinder());
        setInCallAdapter(iInCallAdapter);
        return true;
      } 
      str.writeString("com.android.internal.telecom.IInCallService");
      return true;
    }
    
    private static class Proxy implements IInCallService {
      public static IInCallService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telecom.IInCallService";
      }
      
      public void setInCallAdapter(IInCallAdapter param2IInCallAdapter) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallService");
          if (param2IInCallAdapter != null) {
            iBinder = param2IInCallAdapter.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IInCallService.Stub.getDefaultImpl() != null) {
            IInCallService.Stub.getDefaultImpl().setInCallAdapter(param2IInCallAdapter);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void addCall(ParcelableCall param2ParcelableCall) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallService");
          if (param2ParcelableCall != null) {
            parcel.writeInt(1);
            param2ParcelableCall.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IInCallService.Stub.getDefaultImpl() != null) {
            IInCallService.Stub.getDefaultImpl().addCall(param2ParcelableCall);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void updateCall(ParcelableCall param2ParcelableCall) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallService");
          if (param2ParcelableCall != null) {
            parcel.writeInt(1);
            param2ParcelableCall.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IInCallService.Stub.getDefaultImpl() != null) {
            IInCallService.Stub.getDefaultImpl().updateCall(param2ParcelableCall);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setPostDial(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallService");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IInCallService.Stub.getDefaultImpl() != null) {
            IInCallService.Stub.getDefaultImpl().setPostDial(param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setPostDialWait(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallService");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IInCallService.Stub.getDefaultImpl() != null) {
            IInCallService.Stub.getDefaultImpl().setPostDialWait(param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCallAudioStateChanged(CallAudioState param2CallAudioState) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallService");
          if (param2CallAudioState != null) {
            parcel.writeInt(1);
            param2CallAudioState.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IInCallService.Stub.getDefaultImpl() != null) {
            IInCallService.Stub.getDefaultImpl().onCallAudioStateChanged(param2CallAudioState);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void bringToForeground(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(7, parcel, null, 1);
          if (!bool1 && IInCallService.Stub.getDefaultImpl() != null) {
            IInCallService.Stub.getDefaultImpl().bringToForeground(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCanAddCallChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(8, parcel, null, 1);
          if (!bool1 && IInCallService.Stub.getDefaultImpl() != null) {
            IInCallService.Stub.getDefaultImpl().onCanAddCallChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void silenceRinger() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallService");
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IInCallService.Stub.getDefaultImpl() != null) {
            IInCallService.Stub.getDefaultImpl().silenceRinger();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onConnectionEvent(String param2String1, String param2String2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallService");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IInCallService.Stub.getDefaultImpl() != null) {
            IInCallService.Stub.getDefaultImpl().onConnectionEvent(param2String1, param2String2, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRttUpgradeRequest(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallService");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IInCallService.Stub.getDefaultImpl() != null) {
            IInCallService.Stub.getDefaultImpl().onRttUpgradeRequest(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRttInitiationFailure(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallService");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(12, parcel, null, 1);
          if (!bool && IInCallService.Stub.getDefaultImpl() != null) {
            IInCallService.Stub.getDefaultImpl().onRttInitiationFailure(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onHandoverFailed(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallService");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel, null, 1);
          if (!bool && IInCallService.Stub.getDefaultImpl() != null) {
            IInCallService.Stub.getDefaultImpl().onHandoverFailed(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onHandoverComplete(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallService");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(14, parcel, null, 1);
          if (!bool && IInCallService.Stub.getDefaultImpl() != null) {
            IInCallService.Stub.getDefaultImpl().onHandoverComplete(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInCallService param1IInCallService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInCallService != null) {
          Proxy.sDefaultImpl = param1IInCallService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInCallService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
