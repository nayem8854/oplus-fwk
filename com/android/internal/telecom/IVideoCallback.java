package com.android.internal.telecom;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telecom.VideoProfile;

public interface IVideoCallback extends IInterface {
  void changeCallDataUsage(long paramLong) throws RemoteException;
  
  void changeCameraCapabilities(VideoProfile.CameraCapabilities paramCameraCapabilities) throws RemoteException;
  
  void changePeerDimensions(int paramInt1, int paramInt2) throws RemoteException;
  
  void changeVideoQuality(int paramInt) throws RemoteException;
  
  void handleCallSessionEvent(int paramInt) throws RemoteException;
  
  void receiveSessionModifyRequest(VideoProfile paramVideoProfile) throws RemoteException;
  
  void receiveSessionModifyResponse(int paramInt, VideoProfile paramVideoProfile1, VideoProfile paramVideoProfile2) throws RemoteException;
  
  class Default implements IVideoCallback {
    public void receiveSessionModifyRequest(VideoProfile param1VideoProfile) throws RemoteException {}
    
    public void receiveSessionModifyResponse(int param1Int, VideoProfile param1VideoProfile1, VideoProfile param1VideoProfile2) throws RemoteException {}
    
    public void handleCallSessionEvent(int param1Int) throws RemoteException {}
    
    public void changePeerDimensions(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void changeCallDataUsage(long param1Long) throws RemoteException {}
    
    public void changeCameraCapabilities(VideoProfile.CameraCapabilities param1CameraCapabilities) throws RemoteException {}
    
    public void changeVideoQuality(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVideoCallback {
    private static final String DESCRIPTOR = "com.android.internal.telecom.IVideoCallback";
    
    static final int TRANSACTION_changeCallDataUsage = 5;
    
    static final int TRANSACTION_changeCameraCapabilities = 6;
    
    static final int TRANSACTION_changePeerDimensions = 4;
    
    static final int TRANSACTION_changeVideoQuality = 7;
    
    static final int TRANSACTION_handleCallSessionEvent = 3;
    
    static final int TRANSACTION_receiveSessionModifyRequest = 1;
    
    static final int TRANSACTION_receiveSessionModifyResponse = 2;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telecom.IVideoCallback");
    }
    
    public static IVideoCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telecom.IVideoCallback");
      if (iInterface != null && iInterface instanceof IVideoCallback)
        return (IVideoCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "changeVideoQuality";
        case 6:
          return "changeCameraCapabilities";
        case 5:
          return "changeCallDataUsage";
        case 4:
          return "changePeerDimensions";
        case 3:
          return "handleCallSessionEvent";
        case 2:
          return "receiveSessionModifyResponse";
        case 1:
          break;
      } 
      return "receiveSessionModifyRequest";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        long l;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IVideoCallback");
            param1Int1 = param1Parcel1.readInt();
            changeVideoQuality(param1Int1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IVideoCallback");
            if (param1Parcel1.readInt() != 0) {
              VideoProfile.CameraCapabilities cameraCapabilities = (VideoProfile.CameraCapabilities)VideoProfile.CameraCapabilities.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            changeCameraCapabilities((VideoProfile.CameraCapabilities)param1Parcel1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IVideoCallback");
            l = param1Parcel1.readLong();
            changeCallDataUsage(l);
            return true;
          case 4:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IVideoCallback");
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            changePeerDimensions(param1Int2, param1Int1);
            return true;
          case 3:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IVideoCallback");
            param1Int1 = param1Parcel1.readInt();
            handleCallSessionEvent(param1Int1);
            return true;
          case 2:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IVideoCallback");
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              VideoProfile videoProfile = (VideoProfile)VideoProfile.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              VideoProfile videoProfile = (VideoProfile)VideoProfile.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            receiveSessionModifyResponse(param1Int1, (VideoProfile)param1Parcel2, (VideoProfile)param1Parcel1);
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("com.android.internal.telecom.IVideoCallback");
        if (param1Parcel1.readInt() != 0) {
          VideoProfile videoProfile = (VideoProfile)VideoProfile.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        receiveSessionModifyRequest((VideoProfile)param1Parcel1);
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.telecom.IVideoCallback");
      return true;
    }
    
    private static class Proxy implements IVideoCallback {
      public static IVideoCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telecom.IVideoCallback";
      }
      
      public void receiveSessionModifyRequest(VideoProfile param2VideoProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IVideoCallback");
          if (param2VideoProfile != null) {
            parcel.writeInt(1);
            param2VideoProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IVideoCallback.Stub.getDefaultImpl() != null) {
            IVideoCallback.Stub.getDefaultImpl().receiveSessionModifyRequest(param2VideoProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void receiveSessionModifyResponse(int param2Int, VideoProfile param2VideoProfile1, VideoProfile param2VideoProfile2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IVideoCallback");
          parcel.writeInt(param2Int);
          if (param2VideoProfile1 != null) {
            parcel.writeInt(1);
            param2VideoProfile1.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2VideoProfile2 != null) {
            parcel.writeInt(1);
            param2VideoProfile2.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IVideoCallback.Stub.getDefaultImpl() != null) {
            IVideoCallback.Stub.getDefaultImpl().receiveSessionModifyResponse(param2Int, param2VideoProfile1, param2VideoProfile2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void handleCallSessionEvent(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IVideoCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IVideoCallback.Stub.getDefaultImpl() != null) {
            IVideoCallback.Stub.getDefaultImpl().handleCallSessionEvent(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void changePeerDimensions(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IVideoCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IVideoCallback.Stub.getDefaultImpl() != null) {
            IVideoCallback.Stub.getDefaultImpl().changePeerDimensions(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void changeCallDataUsage(long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IVideoCallback");
          parcel.writeLong(param2Long);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IVideoCallback.Stub.getDefaultImpl() != null) {
            IVideoCallback.Stub.getDefaultImpl().changeCallDataUsage(param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void changeCameraCapabilities(VideoProfile.CameraCapabilities param2CameraCapabilities) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IVideoCallback");
          if (param2CameraCapabilities != null) {
            parcel.writeInt(1);
            param2CameraCapabilities.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IVideoCallback.Stub.getDefaultImpl() != null) {
            IVideoCallback.Stub.getDefaultImpl().changeCameraCapabilities(param2CameraCapabilities);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void changeVideoQuality(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IVideoCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IVideoCallback.Stub.getDefaultImpl() != null) {
            IVideoCallback.Stub.getDefaultImpl().changeVideoQuality(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVideoCallback param1IVideoCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVideoCallback != null) {
          Proxy.sDefaultImpl = param1IVideoCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVideoCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
