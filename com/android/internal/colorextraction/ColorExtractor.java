package com.android.internal.colorextraction;

import android.app.WallpaperColors;
import android.app.WallpaperManager;
import android.content.Context;
import android.os.AsyncTask;
import android.util.SparseArray;
import com.android.internal.colorextraction.types.ExtractionType;
import com.android.internal.colorextraction.types.Tonal;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class ColorExtractor implements WallpaperManager.OnColorsChangedListener {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "ColorExtractor";
  
  public static final int TYPE_DARK = 1;
  
  public static final int TYPE_EXTRA_DARK = 2;
  
  public static final int TYPE_NORMAL = 0;
  
  private static final int[] sGradientTypes = new int[] { 0, 1, 2 };
  
  private final Context mContext;
  
  private final ExtractionType mExtractionType;
  
  protected final SparseArray<GradientColors[]> mGradientColors;
  
  protected WallpaperColors mLockColors;
  
  private final ArrayList<WeakReference<OnColorsChangedListener>> mOnColorsChangedListeners;
  
  protected WallpaperColors mSystemColors;
  
  public ColorExtractor(Context paramContext) {
    this(paramContext, tonal, true, wallpaperManager);
  }
  
  public ColorExtractor(Context paramContext, ExtractionType paramExtractionType, boolean paramBoolean, WallpaperManager paramWallpaperManager) {
    this.mContext = paramContext;
    this.mExtractionType = paramExtractionType;
    this.mGradientColors = (SparseArray)new SparseArray<>();
    for (byte b = 0; b < 2; ) {
      (new int[2])[0] = 2;
      (new int[2])[1] = 1;
      null = (new int[2])[b];
      GradientColors[] arrayOfGradientColors = new GradientColors[sGradientTypes.length];
      this.mGradientColors.append(null, arrayOfGradientColors);
      for (int i : sGradientTypes)
        arrayOfGradientColors[i] = new GradientColors(); 
      b++;
    } 
    this.mOnColorsChangedListeners = new ArrayList<>();
    if (paramWallpaperManager.isWallpaperSupported()) {
      paramWallpaperManager.addOnColorsChangedListener(this, null);
      initExtractColors(paramWallpaperManager, paramBoolean);
    } 
  }
  
  private void initExtractColors(WallpaperManager paramWallpaperManager, boolean paramBoolean) {
    if (paramBoolean) {
      this.mSystemColors = paramWallpaperManager.getWallpaperColors(1);
      this.mLockColors = paramWallpaperManager.getWallpaperColors(2);
      extractWallpaperColors();
    } else {
      (new LoadWallpaperColors()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Object[])new WallpaperManager[] { paramWallpaperManager });
    } 
  }
  
  class OnColorsChangedListener {
    public abstract void onColorsChanged(ColorExtractor param1ColorExtractor, int param1Int);
  }
  
  private class LoadWallpaperColors extends AsyncTask<WallpaperManager, Void, Void> {
    private WallpaperColors mLockColors;
    
    private WallpaperColors mSystemColors;
    
    final ColorExtractor this$0;
    
    private LoadWallpaperColors() {}
    
    protected Void doInBackground(WallpaperManager... param1VarArgs) {
      this.mSystemColors = param1VarArgs[0].getWallpaperColors(1);
      this.mLockColors = param1VarArgs[0].getWallpaperColors(2);
      return null;
    }
    
    protected void onPostExecute(Void param1Void) {
      ColorExtractor.this.mSystemColors = this.mSystemColors;
      ColorExtractor.this.mLockColors = this.mLockColors;
      ColorExtractor.this.extractWallpaperColors();
      ColorExtractor.this.triggerColorsChanged(3);
    }
  }
  
  protected void extractWallpaperColors() {
    GradientColors[] arrayOfGradientColors1 = this.mGradientColors.get(1);
    GradientColors[] arrayOfGradientColors2 = this.mGradientColors.get(2);
    extractInto(this.mSystemColors, arrayOfGradientColors1[0], arrayOfGradientColors1[1], arrayOfGradientColors1[2]);
    extractInto(this.mLockColors, arrayOfGradientColors2[0], arrayOfGradientColors2[1], arrayOfGradientColors2[2]);
  }
  
  public GradientColors getColors(int paramInt) {
    return getColors(paramInt, 1);
  }
  
  public GradientColors getColors(int paramInt1, int paramInt2) {
    if (paramInt2 == 0 || paramInt2 == 1 || paramInt2 == 2) {
      if (paramInt1 == 2 || paramInt1 == 1)
        return ((GradientColors[])this.mGradientColors.get(paramInt1))[paramInt2]; 
      throw new IllegalArgumentException("which should be FLAG_SYSTEM or FLAG_NORMAL");
    } 
    throw new IllegalArgumentException("type should be TYPE_NORMAL, TYPE_DARK or TYPE_EXTRA_DARK");
  }
  
  public WallpaperColors getWallpaperColors(int paramInt) {
    if (paramInt == 2)
      return this.mLockColors; 
    if (paramInt == 1)
      return this.mSystemColors; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid value for which: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void onColorsChanged(WallpaperColors paramWallpaperColors, int paramInt) {
    boolean bool = false;
    if ((paramInt & 0x2) != 0) {
      this.mLockColors = paramWallpaperColors;
      GradientColors[] arrayOfGradientColors = this.mGradientColors.get(2);
      extractInto(paramWallpaperColors, arrayOfGradientColors[0], arrayOfGradientColors[1], arrayOfGradientColors[2]);
      bool = true;
    } 
    if ((paramInt & 0x1) != 0) {
      this.mSystemColors = paramWallpaperColors;
      GradientColors[] arrayOfGradientColors = this.mGradientColors.get(1);
      extractInto(paramWallpaperColors, arrayOfGradientColors[0], arrayOfGradientColors[1], arrayOfGradientColors[2]);
      bool = true;
    } 
    if (bool)
      triggerColorsChanged(paramInt); 
  }
  
  protected void triggerColorsChanged(int paramInt) {
    ArrayList<WeakReference<OnColorsChangedListener>> arrayList = new ArrayList<>(this.mOnColorsChangedListeners);
    int i = arrayList.size();
    for (byte b = 0; b < i; b++) {
      WeakReference<OnColorsChangedListener> weakReference = arrayList.get(b);
      OnColorsChangedListener onColorsChangedListener = weakReference.get();
      if (onColorsChangedListener == null) {
        this.mOnColorsChangedListeners.remove(weakReference);
      } else {
        onColorsChangedListener.onColorsChanged(this, paramInt);
      } 
    } 
  }
  
  private void extractInto(WallpaperColors paramWallpaperColors, GradientColors paramGradientColors1, GradientColors paramGradientColors2, GradientColors paramGradientColors3) {
    this.mExtractionType.extractInto(paramWallpaperColors, paramGradientColors1, paramGradientColors2, paramGradientColors3);
  }
  
  public void destroy() {
    WallpaperManager wallpaperManager = (WallpaperManager)this.mContext.getSystemService(WallpaperManager.class);
    if (wallpaperManager != null)
      wallpaperManager.removeOnColorsChangedListener(this); 
  }
  
  public void addOnColorsChangedListener(OnColorsChangedListener paramOnColorsChangedListener) {
    this.mOnColorsChangedListeners.add(new WeakReference<>(paramOnColorsChangedListener));
  }
  
  public void removeOnColorsChangedListener(OnColorsChangedListener paramOnColorsChangedListener) {
    ArrayList<WeakReference<OnColorsChangedListener>> arrayList = new ArrayList<>(this.mOnColorsChangedListeners);
    int i = arrayList.size();
    for (byte b = 0; b < i; b++) {
      WeakReference<OnColorsChangedListener> weakReference = arrayList.get(b);
      if (weakReference.get() == paramOnColorsChangedListener) {
        this.mOnColorsChangedListeners.remove(weakReference);
        break;
      } 
    } 
  }
  
  class GradientColors {
    private int[] mColorPalette;
    
    private int mMainColor;
    
    private int mSecondaryColor;
    
    private boolean mSupportsDarkText;
    
    public void setMainColor(int param1Int) {
      this.mMainColor = param1Int;
    }
    
    public void setSecondaryColor(int param1Int) {
      this.mSecondaryColor = param1Int;
    }
    
    public void setColorPalette(int[] param1ArrayOfint) {
      this.mColorPalette = param1ArrayOfint;
    }
    
    public void setSupportsDarkText(boolean param1Boolean) {
      this.mSupportsDarkText = param1Boolean;
    }
    
    public void set(GradientColors param1GradientColors) {
      this.mMainColor = param1GradientColors.mMainColor;
      this.mSecondaryColor = param1GradientColors.mSecondaryColor;
      this.mColorPalette = param1GradientColors.mColorPalette;
      this.mSupportsDarkText = param1GradientColors.mSupportsDarkText;
    }
    
    public int getMainColor() {
      return this.mMainColor;
    }
    
    public int getSecondaryColor() {
      return this.mSecondaryColor;
    }
    
    public int[] getColorPalette() {
      return this.mColorPalette;
    }
    
    public boolean supportsDarkText() {
      return this.mSupportsDarkText;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool1 = false;
      if (param1Object == null || param1Object.getClass() != getClass())
        return false; 
      param1Object = param1Object;
      boolean bool2 = bool1;
      if (((GradientColors)param1Object).mMainColor == this.mMainColor) {
        bool2 = bool1;
        if (((GradientColors)param1Object).mSecondaryColor == this.mSecondaryColor) {
          bool2 = bool1;
          if (((GradientColors)param1Object).mSupportsDarkText == this.mSupportsDarkText)
            bool2 = true; 
        } 
      } 
      return bool2;
    }
    
    public int hashCode() {
      int i = this.mMainColor;
      int j = this.mSecondaryColor;
      boolean bool = this.mSupportsDarkText;
      return (i * 31 + j) * 31 + (bool ^ true);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("GradientColors(");
      stringBuilder.append(Integer.toHexString(this.mMainColor));
      stringBuilder.append(", ");
      int i = this.mSecondaryColor;
      stringBuilder.append(Integer.toHexString(i));
      stringBuilder.append(")");
      return stringBuilder.toString();
    }
  }
}
