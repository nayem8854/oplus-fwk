package com.android.internal.colorextraction.types;

import android.app.WallpaperColors;
import android.content.Context;
import android.content.res.XmlResourceParser;
import android.graphics.Color;
import android.util.Log;
import android.util.MathUtils;
import android.util.Range;
import com.android.internal.colorextraction.ColorExtractor;
import com.android.internal.graphics.ColorUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class Tonal implements ExtractionType {
  private static final boolean DEBUG = true;
  
  private static final float FIT_WEIGHT_H = 1.0F;
  
  private static final float FIT_WEIGHT_L = 10.0F;
  
  private static final float FIT_WEIGHT_S = 1.0F;
  
  public static final int MAIN_COLOR_DARK = -14671580;
  
  public static final int MAIN_COLOR_LIGHT = -2433824;
  
  public static final int MAIN_COLOR_REGULAR = -16777216;
  
  private static final String TAG = "Tonal";
  
  private final Context mContext;
  
  private final TonalPalette mGreyPalette;
  
  private float[] mTmpHSL = new float[3];
  
  private final ArrayList<TonalPalette> mTonalPalettes;
  
  public Tonal(Context paramContext) {
    ConfigParser configParser = new ConfigParser(paramContext);
    ArrayList<TonalPalette> arrayList = configParser.getTonalPalettes();
    this.mContext = paramContext;
    this.mGreyPalette = arrayList.get(0);
    this.mTonalPalettes.remove(0);
  }
  
  public void extractInto(WallpaperColors paramWallpaperColors, ColorExtractor.GradientColors paramGradientColors1, ColorExtractor.GradientColors paramGradientColors2, ColorExtractor.GradientColors paramGradientColors3) {
    boolean bool = runTonalExtraction(paramWallpaperColors, paramGradientColors1, paramGradientColors2, paramGradientColors3);
    if (!bool)
      applyFallback(paramWallpaperColors, paramGradientColors1, paramGradientColors2, paramGradientColors3); 
  }
  
  private boolean runTonalExtraction(WallpaperColors paramWallpaperColors, ColorExtractor.GradientColors paramGradientColors1, ColorExtractor.GradientColors paramGradientColors2, ColorExtractor.GradientColors paramGradientColors3) {
    boolean bool;
    if (paramWallpaperColors == null)
      return false; 
    List<Color> list = paramWallpaperColors.getMainColors();
    int i = list.size();
    int j = paramWallpaperColors.getColorHints();
    if ((j & 0x1) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (i == 0)
      return false; 
    Color color = list.get(0);
    i = color.toArgb();
    float[] arrayOfFloat3 = new float[3];
    ColorUtils.RGBToHSL(Color.red(i), Color.green(i), Color.blue(i), arrayOfFloat3);
    arrayOfFloat3[0] = arrayOfFloat3[0] / 360.0F;
    TonalPalette tonalPalette = findTonalPalette(arrayOfFloat3[0], arrayOfFloat3[1]);
    if (tonalPalette == null) {
      Log.w("Tonal", "Could not find a tonal palette!");
      return false;
    } 
    j = bestFit(tonalPalette, arrayOfFloat3[0], arrayOfFloat3[1], arrayOfFloat3[2]);
    if (j == -1) {
      Log.w("Tonal", "Could not find best fit!");
      return false;
    } 
    float[] arrayOfFloat2 = fit(tonalPalette.h, arrayOfFloat3[0], j, Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY);
    float[] arrayOfFloat1 = fit(tonalPalette.s, arrayOfFloat3[1], j, 0.0F, 1.0F);
    float[] arrayOfFloat4 = fit(tonalPalette.l, arrayOfFloat3[2], j, 0.0F, 1.0F);
    int[] arrayOfInt = getColorPalette(arrayOfFloat2, arrayOfFloat1, arrayOfFloat4);
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("Tonal Palette - index: ");
    stringBuilder2.append(j);
    stringBuilder2.append(". Main color: ");
    stringBuilder2.append(Integer.toHexString(getColorInt(j, arrayOfFloat2, arrayOfFloat1, arrayOfFloat4)));
    stringBuilder2.append("\nColors: ");
    stringBuilder2 = new StringBuilder(stringBuilder2.toString());
    for (i = 0; i < arrayOfFloat2.length; i++) {
      stringBuilder2.append(Integer.toHexString(getColorInt(i, arrayOfFloat2, arrayOfFloat1, arrayOfFloat4)));
      if (i < arrayOfFloat2.length - 1)
        stringBuilder2.append(", "); 
    } 
    Log.d("Tonal", stringBuilder2.toString());
    i = getColorInt(j, arrayOfFloat2, arrayOfFloat1, arrayOfFloat4);
    ColorUtils.colorToHSL(i, this.mTmpHSL);
    float arrayOfFloat5[] = this.mTmpHSL, f1 = arrayOfFloat5[2];
    ColorUtils.colorToHSL(-2433824, arrayOfFloat5);
    arrayOfFloat5 = this.mTmpHSL;
    float f2 = arrayOfFloat5[2];
    if (f1 > f2)
      return false; 
    ColorUtils.colorToHSL(-14671580, arrayOfFloat5);
    f2 = this.mTmpHSL[2];
    if (f1 < f2)
      return false; 
    paramGradientColors1.setMainColor(i);
    paramGradientColors1.setSecondaryColor(i);
    paramGradientColors1.setColorPalette(arrayOfInt);
    if (bool) {
      i = arrayOfFloat2.length - 1;
    } else if (j < 2) {
      i = 0;
    } else {
      i = Math.min(j, 3);
    } 
    i = getColorInt(i, arrayOfFloat2, arrayOfFloat1, arrayOfFloat4);
    paramGradientColors2.setMainColor(i);
    paramGradientColors2.setSecondaryColor(i);
    paramGradientColors2.setColorPalette(arrayOfInt);
    if (bool) {
      i = arrayOfFloat2.length - 1;
    } else if (j < 2) {
      i = 0;
    } else {
      i = 2;
    } 
    i = getColorInt(i, arrayOfFloat2, arrayOfFloat1, arrayOfFloat4);
    paramGradientColors3.setMainColor(i);
    paramGradientColors3.setSecondaryColor(i);
    paramGradientColors3.setColorPalette(arrayOfInt);
    paramGradientColors1.setSupportsDarkText(bool);
    paramGradientColors2.setSupportsDarkText(bool);
    paramGradientColors3.setSupportsDarkText(bool);
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("Gradients: \n\tNormal ");
    stringBuilder1.append(paramGradientColors1);
    stringBuilder1.append("\n\tDark ");
    stringBuilder1.append(paramGradientColors2);
    stringBuilder1.append("\n\tExtra dark: ");
    stringBuilder1.append(paramGradientColors3);
    Log.d("Tonal", stringBuilder1.toString());
    return true;
  }
  
  private void applyFallback(WallpaperColors paramWallpaperColors, ColorExtractor.GradientColors paramGradientColors1, ColorExtractor.GradientColors paramGradientColors2, ColorExtractor.GradientColors paramGradientColors3) {
    applyFallback(paramWallpaperColors, paramGradientColors1);
    applyFallback(paramWallpaperColors, paramGradientColors2);
    applyFallback(paramWallpaperColors, paramGradientColors3);
  }
  
  public void applyFallback(WallpaperColors paramWallpaperColors, ColorExtractor.GradientColors paramGradientColors) {
    boolean bool1;
    int i;
    boolean bool2;
    if (paramWallpaperColors != null && (
      paramWallpaperColors.getColorHints() & 0x1) != 0) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    if (paramWallpaperColors != null && (
      paramWallpaperColors.getColorHints() & 0x2) != 0) {
      i = 1;
    } else {
      i = 0;
    } 
    if (((this.mContext.getResources().getConfiguration()).uiMode & 0x30) == 32) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (bool1) {
      i = -2433824;
    } else if (i != 0 || bool2) {
      i = -14671580;
    } else {
      i = -16777216;
    } 
    float[] arrayOfFloat = new float[3];
    ColorUtils.colorToHSL(i, arrayOfFloat);
    paramGradientColors.setMainColor(i);
    paramGradientColors.setSecondaryColor(i);
    paramGradientColors.setSupportsDarkText(bool1);
    paramGradientColors.setColorPalette(getColorPalette(findTonalPalette(arrayOfFloat[0], arrayOfFloat[1])));
  }
  
  private int getColorInt(int paramInt, float[] paramArrayOffloat1, float[] paramArrayOffloat2, float[] paramArrayOffloat3) {
    this.mTmpHSL[0] = fract(paramArrayOffloat1[paramInt]) * 360.0F;
    paramArrayOffloat1 = this.mTmpHSL;
    paramArrayOffloat1[1] = paramArrayOffloat2[paramInt];
    paramArrayOffloat1[2] = paramArrayOffloat3[paramInt];
    return ColorUtils.HSLToColor(paramArrayOffloat1);
  }
  
  private int[] getColorPalette(float[] paramArrayOffloat1, float[] paramArrayOffloat2, float[] paramArrayOffloat3) {
    int[] arrayOfInt = new int[paramArrayOffloat1.length];
    for (byte b = 0; b < arrayOfInt.length; b++)
      arrayOfInt[b] = getColorInt(b, paramArrayOffloat1, paramArrayOffloat2, paramArrayOffloat3); 
    return arrayOfInt;
  }
  
  private int[] getColorPalette(TonalPalette paramTonalPalette) {
    return getColorPalette(paramTonalPalette.h, paramTonalPalette.s, paramTonalPalette.l);
  }
  
  private static float[] fit(float[] paramArrayOffloat, float paramFloat1, int paramInt, float paramFloat2, float paramFloat3) {
    float[] arrayOfFloat = new float[paramArrayOffloat.length];
    float f = paramArrayOffloat[paramInt];
    for (paramInt = 0; paramInt < paramArrayOffloat.length; paramInt++)
      arrayOfFloat[paramInt] = MathUtils.constrain(paramArrayOffloat[paramInt] + paramFloat1 - f, paramFloat2, paramFloat3); 
    return arrayOfFloat;
  }
  
  private static int bestFit(TonalPalette paramTonalPalette, float paramFloat1, float paramFloat2, float paramFloat3) {
    byte b = -1;
    float f = Float.POSITIVE_INFINITY;
    for (byte b1 = 0; b1 < paramTonalPalette.h.length; b1++, f = f1) {
      float f1 = paramTonalPalette.h[b1];
      f1 = Math.abs(paramFloat1 - f1);
      float f2 = paramTonalPalette.s[b1];
      float f3 = Math.abs(paramFloat2 - f2);
      f2 = paramTonalPalette.l[b1];
      f2 = f1 * 1.0F + f3 * 1.0F + Math.abs(paramFloat3 - f2) * 10.0F;
      f1 = f;
      if (f2 < f) {
        f1 = f2;
        b = b1;
      } 
    } 
    return b;
  }
  
  private TonalPalette findTonalPalette(float paramFloat1, float paramFloat2) {
    Object object2;
    if (paramFloat2 < 0.05F)
      return this.mGreyPalette; 
    Object object1 = null;
    float f = Float.POSITIVE_INFINITY;
    int i = this.mTonalPalettes.size();
    byte b = 0;
    while (true) {
      object2 = object1;
      if (b < i) {
        object2 = this.mTonalPalettes.get(b);
        if (paramFloat1 >= ((TonalPalette)object2).minHue && paramFloat1 <= ((TonalPalette)object2).maxHue)
          break; 
        if (((TonalPalette)object2).maxHue > 1.0F && paramFloat1 >= 0.0F && paramFloat1 <= fract(((TonalPalette)object2).maxHue))
          break; 
        if (((TonalPalette)object2).minHue < 0.0F && paramFloat1 >= fract(((TonalPalette)object2).minHue) && paramFloat1 <= 1.0F)
          break; 
        if (paramFloat1 <= ((TonalPalette)object2).minHue && ((TonalPalette)object2).minHue - paramFloat1 < f) {
          Object object3 = object2;
          paramFloat2 = ((TonalPalette)object2).minHue - paramFloat1;
          continue;
        } 
        if (paramFloat1 >= ((TonalPalette)object2).maxHue && paramFloat1 - ((TonalPalette)object2).maxHue < f) {
          Object object3 = object2;
          paramFloat2 = paramFloat1 - ((TonalPalette)object2).maxHue;
          continue;
        } 
        if (((TonalPalette)object2).maxHue > 1.0F && paramFloat1 >= fract(((TonalPalette)object2).maxHue)) {
          paramFloat2 = ((TonalPalette)object2).maxHue;
          if (paramFloat1 - fract(paramFloat2) < f) {
            Object object3 = object2;
            paramFloat2 = paramFloat1 - fract(((TonalPalette)object2).maxHue);
            continue;
          } 
        } 
        Object object = object1;
        paramFloat2 = f;
        if (((TonalPalette)object2).minHue < 0.0F) {
          object = object1;
          paramFloat2 = f;
          if (paramFloat1 <= fract(((TonalPalette)object2).minHue)) {
            float f1 = ((TonalPalette)object2).minHue;
            object = object1;
            paramFloat2 = f;
            if (fract(f1) - paramFloat1 < f) {
              object = object2;
              paramFloat2 = fract(((TonalPalette)object2).minHue) - paramFloat1;
            } 
          } 
        } 
        continue;
      } 
      break;
      b++;
      object1 = SYNTHETIC_LOCAL_VARIABLE_8;
      f = paramFloat2;
    } 
    return (TonalPalette)object2;
  }
  
  private static float fract(float paramFloat) {
    return paramFloat - (float)Math.floor(paramFloat);
  }
  
  class TonalPalette {
    public final float[] h;
    
    public final float[] l;
    
    public final float maxHue;
    
    public final float minHue;
    
    public final float[] s;
    
    TonalPalette(Tonal this$0, float[] param1ArrayOffloat1, float[] param1ArrayOffloat2) {
      if (this$0.length == param1ArrayOffloat1.length && param1ArrayOffloat1.length == param1ArrayOffloat2.length) {
        this.h = (float[])this$0;
        this.s = param1ArrayOffloat1;
        this.l = param1ArrayOffloat2;
        float f1 = Float.POSITIVE_INFINITY;
        float f2 = Float.NEGATIVE_INFINITY;
        int i;
        byte b;
        for (i = this$0.length, b = 0; b < i; ) {
          Tonal tonal = this$0[b];
          f1 = Math.min(tonal, f1);
          f2 = Math.max(tonal, f2);
          b++;
        } 
        this.minHue = f1;
        this.maxHue = f2;
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("All arrays should have the same size. h: ");
      stringBuilder.append(Arrays.toString((float[])this$0));
      stringBuilder.append(" s: ");
      stringBuilder.append(Arrays.toString(param1ArrayOffloat1));
      stringBuilder.append(" l: ");
      stringBuilder.append(Arrays.toString(param1ArrayOffloat2));
      throw new IllegalArgumentException(stringBuilder.toString());
    }
  }
  
  class ColorRange {
    private Range<Float> mHue;
    
    private Range<Float> mLightness;
    
    private Range<Float> mSaturation;
    
    public ColorRange(Tonal this$0, Range<Float> param1Range1, Range<Float> param1Range2) {
      this.mHue = (Range<Float>)this$0;
      this.mSaturation = param1Range1;
      this.mLightness = param1Range2;
    }
    
    public boolean containsColor(float param1Float1, float param1Float2, float param1Float3) {
      if (!this.mHue.contains(Float.valueOf(param1Float1)))
        return false; 
      if (!this.mSaturation.contains(Float.valueOf(param1Float2)))
        return false; 
      if (!this.mLightness.contains(Float.valueOf(param1Float3)))
        return false; 
      return true;
    }
    
    public float[] getCenter() {
      Range<Float> range = this.mHue;
      float f1 = ((Float)range.getLower()).floatValue(), f2 = (((Float)this.mHue.getUpper()).floatValue() - ((Float)this.mHue.getLower()).floatValue()) / 2.0F;
      range = this.mSaturation;
      float f3 = ((Float)range.getLower()).floatValue(), f4 = (((Float)this.mSaturation.getUpper()).floatValue() - ((Float)this.mSaturation.getLower()).floatValue()) / 2.0F;
      range = this.mLightness;
      float f5 = ((Float)range.getLower()).floatValue(), f6 = (((Float)this.mLightness.getUpper()).floatValue() - ((Float)this.mLightness.getLower()).floatValue()) / 2.0F;
      return new float[] { f1 + f2, f3 + f4, f5 + f6 };
    }
    
    public String toString() {
      return String.format("H: %s, S: %s, L %s", new Object[] { this.mHue, this.mSaturation, this.mLightness });
    }
  }
  
  class ConfigParser {
    private final ArrayList<Tonal.TonalPalette> mTonalPalettes;
    
    public ConfigParser(Tonal this$0) {
      this.mTonalPalettes = new ArrayList<>();
      try {
        XmlResourceParser xmlResourceParser = this$0.getResources().getXml(18284549);
        int i = xmlResourceParser.getEventType();
        while (i != 1) {
          if (i != 0 && i != 3)
            if (i == 2) {
              String str = xmlResourceParser.getName();
              if (str.equals("palettes"))
                parsePalettes((XmlPullParser)xmlResourceParser); 
            } else {
              XmlPullParserException xmlPullParserException = new XmlPullParserException();
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Invalid XML event ");
              stringBuilder.append(i);
              stringBuilder.append(" - ");
              stringBuilder.append(xmlResourceParser.getName());
              this(stringBuilder.toString(), (XmlPullParser)xmlResourceParser, null);
              throw xmlPullParserException;
            }  
          i = xmlResourceParser.next();
        } 
        return;
      } catch (XmlPullParserException|IOException xmlPullParserException) {
        throw new RuntimeException(xmlPullParserException);
      } 
    }
    
    public ArrayList<Tonal.TonalPalette> getTonalPalettes() {
      return this.mTonalPalettes;
    }
    
    private Tonal.ColorRange readRange(XmlPullParser param1XmlPullParser) throws XmlPullParserException, IOException {
      param1XmlPullParser.require(2, null, "range");
      float[] arrayOfFloat1 = readFloatArray(param1XmlPullParser.getAttributeValue(null, "h"));
      float[] arrayOfFloat2 = readFloatArray(param1XmlPullParser.getAttributeValue(null, "s"));
      float[] arrayOfFloat3 = readFloatArray(param1XmlPullParser.getAttributeValue(null, "l"));
      if (arrayOfFloat1 != null && arrayOfFloat2 != null && arrayOfFloat3 != null) {
        Range<Float> range1 = new Range<>(Float.valueOf(arrayOfFloat1[0]), Float.valueOf(arrayOfFloat1[1])), range2 = new Range<>(Float.valueOf(arrayOfFloat2[0]), Float.valueOf(arrayOfFloat2[1]));
        float f = arrayOfFloat3[0];
        return 
          new Tonal.ColorRange(range1, range2, new Range<>(Float.valueOf(f), Float.valueOf(arrayOfFloat3[1])));
      } 
      throw new XmlPullParserException("Incomplete range tag.", param1XmlPullParser, null);
    }
    
    private void parsePalettes(XmlPullParser param1XmlPullParser) throws XmlPullParserException, IOException {
      param1XmlPullParser.require(2, null, "palettes");
      while (param1XmlPullParser.next() != 3) {
        if (param1XmlPullParser.getEventType() != 2)
          continue; 
        String str = param1XmlPullParser.getName();
        if (str.equals("palette")) {
          this.mTonalPalettes.add(readPalette(param1XmlPullParser));
          param1XmlPullParser.next();
          continue;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid tag: ");
        stringBuilder.append(str);
        throw new XmlPullParserException(stringBuilder.toString());
      } 
    }
    
    private Tonal.TonalPalette readPalette(XmlPullParser param1XmlPullParser) throws XmlPullParserException, IOException {
      param1XmlPullParser.require(2, null, "palette");
      float[] arrayOfFloat1 = readFloatArray(param1XmlPullParser.getAttributeValue(null, "h"));
      float[] arrayOfFloat2 = readFloatArray(param1XmlPullParser.getAttributeValue(null, "s"));
      float[] arrayOfFloat3 = readFloatArray(param1XmlPullParser.getAttributeValue(null, "l"));
      if (arrayOfFloat1 != null && arrayOfFloat2 != null && arrayOfFloat3 != null)
        return new Tonal.TonalPalette(arrayOfFloat1, arrayOfFloat2, arrayOfFloat3); 
      throw new XmlPullParserException("Incomplete range tag.", param1XmlPullParser, null);
    }
    
    private float[] readFloatArray(String param1String) throws IOException, XmlPullParserException {
      String[] arrayOfString = param1String.replaceAll(" ", "").replaceAll("\n", "").split(",");
      float[] arrayOfFloat = new float[arrayOfString.length];
      for (byte b = 0; b < arrayOfString.length; b++)
        arrayOfFloat[b] = Float.parseFloat(arrayOfString[b]); 
      return arrayOfFloat;
    }
  }
}
