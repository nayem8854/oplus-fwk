package com.android.internal.colorextraction.types;

import android.app.WallpaperColors;
import com.android.internal.colorextraction.ColorExtractor;

public interface ExtractionType {
  void extractInto(WallpaperColors paramWallpaperColors, ColorExtractor.GradientColors paramGradientColors1, ColorExtractor.GradientColors paramGradientColors2, ColorExtractor.GradientColors paramGradientColors3);
}
