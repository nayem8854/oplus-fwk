package com.android.internal.colorextraction.drawable;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Xfermode;
import android.graphics.drawable.Drawable;
import android.view.animation.DecelerateInterpolator;
import com.android.internal.graphics.ColorUtils;

public class ScrimDrawable extends Drawable {
  private static final long COLOR_ANIMATION_DURATION = 2000L;
  
  private static final String TAG = "ScrimDrawable";
  
  private int mAlpha = 255;
  
  private ValueAnimator mColorAnimation;
  
  private int mMainColor;
  
  private int mMainColorTo;
  
  private final Paint mPaint;
  
  public ScrimDrawable() {
    Paint paint = new Paint();
    paint.setStyle(Paint.Style.FILL);
  }
  
  public void setColor(int paramInt, boolean paramBoolean) {
    if (paramInt == this.mMainColorTo)
      return; 
    ValueAnimator valueAnimator = this.mColorAnimation;
    if (valueAnimator != null && valueAnimator.isRunning())
      this.mColorAnimation.cancel(); 
    this.mMainColorTo = paramInt;
    if (paramBoolean) {
      int i = this.mMainColor;
      valueAnimator = ValueAnimator.ofFloat(new float[] { 0.0F, 1.0F });
      valueAnimator.setDuration(2000L);
      valueAnimator.addUpdateListener(new _$$Lambda$ScrimDrawable$UWtyAZ9Ss5P5TukFNvAyvh0pNf0(this, i, paramInt));
      valueAnimator.addListener((Animator.AnimatorListener)new AnimatorListenerAdapter() {
            final ScrimDrawable this$0;
            
            public void onAnimationEnd(Animator param1Animator, boolean param1Boolean) {
              if (ScrimDrawable.this.mColorAnimation == param1Animator)
                ScrimDrawable.access$002(ScrimDrawable.this, (ValueAnimator)null); 
            }
          });
      valueAnimator.setInterpolator(new DecelerateInterpolator());
      valueAnimator.start();
      this.mColorAnimation = valueAnimator;
    } else {
      this.mMainColor = paramInt;
      invalidateSelf();
    } 
  }
  
  public void setAlpha(int paramInt) {
    if (paramInt != this.mAlpha) {
      this.mAlpha = paramInt;
      invalidateSelf();
    } 
  }
  
  public int getAlpha() {
    return this.mAlpha;
  }
  
  public void setXfermode(Xfermode paramXfermode) {
    this.mPaint.setXfermode(paramXfermode);
    invalidateSelf();
  }
  
  public void setColorFilter(ColorFilter paramColorFilter) {
    this.mPaint.setColorFilter(paramColorFilter);
  }
  
  public ColorFilter getColorFilter() {
    return this.mPaint.getColorFilter();
  }
  
  public int getOpacity() {
    return -3;
  }
  
  public void draw(Canvas paramCanvas) {
    this.mPaint.setColor(this.mMainColor);
    this.mPaint.setAlpha(this.mAlpha);
    paramCanvas.drawRect(getBounds(), this.mPaint);
  }
  
  public int getMainColor() {
    return this.mMainColor;
  }
}
