package com.android.internal.os;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.BatteryStats;
import android.util.SparseArray;
import java.util.List;

public class SensorPowerCalculator extends PowerCalculator {
  private final double mGpsPower;
  
  private final List<Sensor> mSensors;
  
  public SensorPowerCalculator(PowerProfile paramPowerProfile, SensorManager paramSensorManager, BatteryStats paramBatteryStats, long paramLong, int paramInt) {
    this.mSensors = paramSensorManager.getSensorList(-1);
    this.mGpsPower = getAverageGpsPower(paramPowerProfile, paramBatteryStats, paramLong, paramInt);
  }
  
  public void calculateApp(BatterySipper paramBatterySipper, BatteryStats.Uid paramUid, long paramLong1, long paramLong2, int paramInt) {
    SparseArray<BatteryStats.Uid.Sensor> sparseArray = paramUid.getSensorStats();
    int i = sparseArray.size();
    for (byte b = 0; b < i; b++) {
      BatteryStats.Uid.Sensor sensor = sparseArray.valueAt(b);
      int j = sparseArray.keyAt(b);
      BatteryStats.Timer timer = sensor.getSensorTime();
      paramLong2 = timer.getTotalTimeLocked(paramLong1, paramInt) / 1000L;
      if (j != -10000) {
        int k = this.mSensors.size();
        for (byte b1 = 0; b1 < k; b1++) {
          Sensor sensor1 = this.mSensors.get(b1);
          if (sensor1.getHandle() == j) {
            paramBatterySipper.sensorPowerMah += ((float)paramLong2 * sensor1.getPower() / 3600000.0F);
            break;
          } 
        } 
      } else {
        paramBatterySipper.gpsTimeMs = paramLong2;
        paramBatterySipper.gpsPowerMah = paramBatterySipper.gpsTimeMs * this.mGpsPower / 3600000.0D;
      } 
    } 
  }
  
  private double getAverageGpsPower(PowerProfile paramPowerProfile, BatteryStats paramBatteryStats, long paramLong, int paramInt) {
    double d1 = paramPowerProfile.getAveragePowerOrDefault("gps.on", -1.0D);
    if (d1 != -1.0D)
      return d1; 
    d1 = 0.0D;
    long l = 0L;
    double d2 = 0.0D;
    for (byte b = 0; b < 2; b++) {
      long l1 = paramBatteryStats.getGpsSignalQualityTime(b, paramLong, paramInt);
      l += l1;
      d2 += paramPowerProfile.getAveragePower("gps.signalqualitybased", b) * l1;
    } 
    if (l != 0L)
      d1 = d2 / l; 
    return d1;
  }
}
