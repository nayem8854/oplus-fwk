package com.android.internal.os;

import android.app.ApplicationLoaders;
import android.app.LoadedApk;
import android.content.pm.ApplicationInfo;
import android.net.LocalSocket;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebViewFactory;
import android.webkit.WebViewFactoryProvider;
import android.webkit.WebViewLibraryLoader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

class WebViewZygoteInit {
  public static final String TAG = "WebViewZygoteInit";
  
  class WebViewZygoteServer extends ZygoteServer {
    private WebViewZygoteServer() {}
    
    protected ZygoteConnection createNewConnection(LocalSocket param1LocalSocket, String param1String) throws IOException {
      return new WebViewZygoteInit.WebViewZygoteConnection(param1LocalSocket, param1String);
    }
  }
  
  class WebViewZygoteConnection extends ZygoteConnection {
    WebViewZygoteConnection(WebViewZygoteInit this$0, String param1String) throws IOException {
      super((LocalSocket)this$0, param1String);
    }
    
    protected void preload() {}
    
    protected boolean isPreloadComplete() {
      return true;
    }
    
    protected boolean canPreloadApp() {
      return true;
    }
    
    protected void handlePreloadApp(ApplicationInfo param1ApplicationInfo) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Beginning application preload for ");
      stringBuilder.append(param1ApplicationInfo.packageName);
      Log.i("WebViewZygoteInit", stringBuilder.toString());
      LoadedApk loadedApk = new LoadedApk(null, param1ApplicationInfo, null, null, false, true, false);
      ClassLoader classLoader = loadedApk.getClassLoader();
      doPreload(classLoader, WebViewFactory.getWebViewLibrary(param1ApplicationInfo));
      Zygote.allowAppFilesAcrossFork(param1ApplicationInfo);
      Log.i("WebViewZygoteInit", "Application preload done");
    }
    
    protected void handlePreloadPackage(String param1String1, String param1String2, String param1String3, String param1String4) {
      Log.i("WebViewZygoteInit", "Beginning package preload");
      ClassLoader classLoader = ApplicationLoaders.getDefault().createAndCacheWebViewClassLoader(param1String1, param1String2, param1String4);
      for (String param1String1 : TextUtils.split(param1String1, File.pathSeparator))
        Zygote.nativeAllowFileAcrossFork(param1String1); 
      doPreload(classLoader, param1String3);
      Log.i("WebViewZygoteInit", "Package preload done");
    }
    
    private void doPreload(ClassLoader param1ClassLoader, String param1String) {
      WebViewLibraryLoader.loadNativeLibrary(param1ClassLoader, param1String);
      boolean bool1 = false, bool2 = false;
      boolean bool = true;
      boolean bool3 = bool1;
      try {
        Class<WebViewFactoryProvider> clazz = WebViewFactory.getWebViewProviderClass(param1ClassLoader);
        bool3 = bool1;
        Method method = clazz.getMethod("preloadInZygote", new Class[0]);
        bool3 = bool1;
        method.setAccessible(true);
        bool3 = bool1;
        if (method.getReturnType() != boolean.class) {
          bool3 = bool1;
          Log.e("WebViewZygoteInit", "Unexpected return type: preloadInZygote must return boolean");
          bool3 = bool2;
        } else {
          bool3 = bool1;
          method = clazz.getMethod("preloadInZygote", new Class[0]);
          bool3 = bool1;
          Boolean bool4 = (Boolean)method.invoke(null, new Object[0]);
          bool3 = bool1;
          bool1 = bool4.booleanValue();
          bool3 = bool1;
          if (!bool1) {
            bool3 = bool1;
            Log.e("WebViewZygoteInit", "preloadInZygote returned false");
            bool3 = bool1;
          } 
        } 
      } catch (ReflectiveOperationException reflectiveOperationException) {
        Log.e("WebViewZygoteInit", "Exception while preloading package", reflectiveOperationException);
      } 
      try {
        DataOutputStream dataOutputStream = getSocketOutputStream();
        if (!bool3)
          bool = false; 
        dataOutputStream.writeInt(bool);
        return;
      } catch (IOException iOException) {
        throw new IllegalStateException("Error writing to command socket", iOException);
      } 
    }
  }
  
  public static void main(String[] paramArrayOfString) {
    Log.i("WebViewZygoteInit", "Starting WebViewZygoteInit");
    WebViewZygoteServer webViewZygoteServer = new WebViewZygoteServer();
    ChildZygoteInit.runZygoteServer(webViewZygoteServer, paramArrayOfString);
  }
}
