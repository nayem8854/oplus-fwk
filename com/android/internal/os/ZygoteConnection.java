package com.android.internal.os;

import android.content.pm.ApplicationInfo;
import android.net.Credentials;
import android.net.LocalSocket;
import android.os.Parcel;
import android.os.Process;
import android.os.Trace;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.util.Log;
import dalvik.system.VMRuntime;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import libcore.io.IoUtils;

class ZygoteConnection {
  private static final String TAG = "Zygote";
  
  private final String abiList;
  
  private boolean isEof;
  
  private final LocalSocket mSocket;
  
  private final DataOutputStream mSocketOutStream;
  
  private final BufferedReader mSocketReader;
  
  private final Credentials peer;
  
  ZygoteConnection(LocalSocket paramLocalSocket, String paramString) throws IOException {
    this.mSocket = paramLocalSocket;
    this.abiList = paramString;
    this.mSocketOutStream = new DataOutputStream(paramLocalSocket.getOutputStream());
    this.mSocketReader = new BufferedReader(new InputStreamReader(paramLocalSocket.getInputStream()), 256);
    this.mSocket.setSoTimeout(1000);
    try {
      this.peer = this.mSocket.getPeerCredentials();
      this.isEof = false;
      return;
    } catch (IOException iOException) {
      Log.e("Zygote", "Cannot read peer credentials", iOException);
      throw iOException;
    } 
  }
  
  FileDescriptor getFileDescriptor() {
    return this.mSocket.getFileDescriptor();
  }
  
  Runnable processOneCommand(ZygoteServer paramZygoteServer) {
    try {
      Parcel parcel;
      String[] arrayOfString = Zygote.readArgumentList(this.mSocketReader);
      if (arrayOfString == null) {
        this.isEof = true;
        return null;
      } 
      byte[] arrayOfByte = null;
      FileDescriptor[] arrayOfFileDescriptor = null;
      ZygoteArguments zygoteArguments = new ZygoteArguments(arrayOfString);
      if (zygoteArguments.mBootCompleted) {
        handleBootCompleted();
        return null;
      } 
      if (zygoteArguments.mAbiListQuery) {
        handleAbiListQuery();
        return null;
      } 
      if (zygoteArguments.mPidQuery) {
        handlePidQuery();
        return null;
      } 
      if (zygoteArguments.mUsapPoolStatusSpecified)
        return handleUsapPoolStatusChange(paramZygoteServer, zygoteArguments.mUsapPoolEnabled); 
      if (zygoteArguments.mPreloadDefault) {
        handlePreload();
        return null;
      } 
      if (zygoteArguments.mPreloadPackage != null) {
        handlePreloadPackage(zygoteArguments.mPreloadPackage, zygoteArguments.mPreloadPackageLibs, zygoteArguments.mPreloadPackageLibFileName, zygoteArguments.mPreloadPackageCacheKey);
        return null;
      } 
      if (canPreloadApp() && zygoteArguments.mPreloadApp != null) {
        arrayOfByte = Base64.getDecoder().decode(zygoteArguments.mPreloadApp);
        parcel = Parcel.obtain();
        parcel.unmarshall(arrayOfByte, 0, arrayOfByte.length);
        parcel.setDataPosition(0);
        ApplicationInfo applicationInfo = (ApplicationInfo)ApplicationInfo.CREATOR.createFromParcel(parcel);
        parcel.recycle();
        if (applicationInfo != null) {
          handlePreloadApp(applicationInfo);
          return null;
        } 
        throw new IllegalArgumentException("Failed to deserialize --preload-app");
      } 
      if (zygoteArguments.mApiBlacklistExemptions != null)
        return handleApiBlacklistExemptions((ZygoteServer)parcel, zygoteArguments.mApiBlacklistExemptions); 
      if (zygoteArguments.mHiddenApiAccessLogSampleRate != -1 || zygoteArguments.mHiddenApiAccessStatslogSampleRate != -1)
        return handleHiddenApiAccessLogSampleRate((ZygoteServer)parcel, zygoteArguments.mHiddenApiAccessLogSampleRate, zygoteArguments.mHiddenApiAccessStatslogSampleRate); 
      if (zygoteArguments.mPermittedCapabilities == 0L && zygoteArguments.mEffectiveCapabilities == 0L) {
        int[][] arrayOfInt;
        FileDescriptor fileDescriptor2, fileDescriptor3;
        int[] arrayOfInt1;
        Zygote.applyUidSecurityPolicy(zygoteArguments, this.peer);
        Zygote.applyInvokeWithSecurityPolicy(zygoteArguments, this.peer);
        Zygote.applyDebuggerSystemProperty(zygoteArguments);
        Zygote.applyInvokeWithSystemProperty(zygoteArguments);
        arrayOfString = null;
        if (zygoteArguments.mRLimits != null)
          arrayOfInt = zygoteArguments.mRLimits.<int[]>toArray(Zygote.INT_ARRAY_2D); 
        if (zygoteArguments.mInvokeWith != null) {
          try {
            arrayOfFileDescriptor = Os.pipe2(OsConstants.O_CLOEXEC);
            fileDescriptor2 = arrayOfFileDescriptor[1];
            fileDescriptor3 = arrayOfFileDescriptor[0];
            Os.fcntlInt(fileDescriptor2, OsConstants.F_SETFD, 0);
            int j = fileDescriptor2.getInt$(), k = fileDescriptor3.getInt$();
            arrayOfInt1 = new int[] { j, k };
          } catch (ErrnoException errnoException) {
            throw new IllegalStateException("Unable to set up pipe for invoke-with", errnoException);
          } 
        } else {
          arrayOfInt1 = null;
        } 
        int[] arrayOfInt2 = new int[2];
        arrayOfInt2[0] = -1;
        arrayOfInt2[1] = -1;
        FileDescriptor fileDescriptor5 = this.mSocket.getFileDescriptor();
        if (fileDescriptor5 != null)
          arrayOfInt2[0] = fileDescriptor5.getInt$(); 
        fileDescriptor5 = errnoException.getZygoteSocketFileDescriptor();
        if (fileDescriptor5 != null)
          arrayOfInt2[1] = fileDescriptor5.getInt$(); 
        int i = Zygote.forkAndSpecialize(zygoteArguments.mUid, zygoteArguments.mGid, zygoteArguments.mGids, zygoteArguments.mRuntimeFlags, arrayOfInt, zygoteArguments.mMountExternal, zygoteArguments.mSeInfo, zygoteArguments.mNiceName, arrayOfInt2, arrayOfInt1, zygoteArguments.mStartChildZygote, zygoteArguments.mInstructionSet, zygoteArguments.mAppDataDir, zygoteArguments.mIsTopApp, zygoteArguments.mPkgDataInfoList, zygoteArguments.mWhitelistedDataInfoList, zygoteArguments.mBindMountAppDataDirs, zygoteArguments.mBindMountAppStorageDirs);
        if (i == 0) {
          FileDescriptor fileDescriptor6 = fileDescriptor2, fileDescriptor7 = fileDescriptor3;
          try {
            errnoException.setForkChild();
            fileDescriptor6 = fileDescriptor2;
            fileDescriptor7 = fileDescriptor3;
            errnoException.closeServerSocket();
            fileDescriptor6 = fileDescriptor2;
            fileDescriptor7 = fileDescriptor3;
            IoUtils.closeQuietly(fileDescriptor3);
            fileDescriptor7 = null;
            fileDescriptor6 = fileDescriptor2;
            return handleChildProc(zygoteArguments, fileDescriptor2, zygoteArguments.mStartChildZygote);
          } finally {
            IoUtils.closeQuietly(fileDescriptor6);
            IoUtils.closeQuietly(fileDescriptor7);
          } 
        } 
        FileDescriptor fileDescriptor1 = fileDescriptor2, fileDescriptor4 = fileDescriptor3;
        IoUtils.closeQuietly(fileDescriptor2);
        fileDescriptor1 = null;
        fileDescriptor4 = fileDescriptor3;
        handleParentProc(i, fileDescriptor3);
        IoUtils.closeQuietly(null);
        IoUtils.closeQuietly(fileDescriptor3);
        return null;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Client may not specify capabilities: permitted=0x");
      long l = zygoteArguments.mPermittedCapabilities;
      stringBuilder.append(Long.toHexString(l));
      stringBuilder.append(", effective=0x");
      l = zygoteArguments.mEffectiveCapabilities;
      stringBuilder.append(Long.toHexString(l));
      throw new ZygoteSecurityException(stringBuilder.toString());
    } catch (IOException iOException) {
      throw new IllegalStateException("IOException on command socket", iOException);
    } 
  }
  
  private void handleAbiListQuery() {
    try {
      byte[] arrayOfByte = this.abiList.getBytes(StandardCharsets.US_ASCII);
      this.mSocketOutStream.writeInt(arrayOfByte.length);
      this.mSocketOutStream.write(arrayOfByte);
      return;
    } catch (IOException iOException) {
      throw new IllegalStateException("Error writing to command socket", iOException);
    } 
  }
  
  private void handlePidQuery() {
    try {
      int i = Process.myPid();
      byte[] arrayOfByte = String.valueOf(i).getBytes(StandardCharsets.US_ASCII);
      this.mSocketOutStream.writeInt(arrayOfByte.length);
      this.mSocketOutStream.write(arrayOfByte);
      return;
    } catch (IOException iOException) {
      throw new IllegalStateException("Error writing to command socket", iOException);
    } 
  }
  
  private void handleBootCompleted() {
    try {
      this.mSocketOutStream.writeInt(0);
      VMRuntime.bootCompleted();
      return;
    } catch (IOException iOException) {
      throw new IllegalStateException("Error writing to command socket", iOException);
    } 
  }
  
  private void handlePreload() {
    try {
      if (isPreloadComplete()) {
        this.mSocketOutStream.writeInt(1);
      } else {
        preload();
        this.mSocketOutStream.writeInt(0);
      } 
      return;
    } catch (IOException iOException) {
      throw new IllegalStateException("Error writing to command socket", iOException);
    } 
  }
  
  private Runnable stateChangeWithUsapPoolReset(ZygoteServer paramZygoteServer, Runnable paramRunnable) {
    try {
      boolean bool = paramZygoteServer.isUsapPoolEnabled();
      if (bool) {
        Log.i("Zygote", "Emptying USAP Pool due to state change.");
        Zygote.emptyUsapPool();
      } 
      paramRunnable.run();
      if (paramZygoteServer.isUsapPoolEnabled()) {
        LocalSocket localSocket = this.mSocket;
        int i = localSocket.getFileDescriptor().getInt$();
        Runnable runnable = paramZygoteServer.fillUsapPool(new int[] { i }, false);
        if (runnable != null) {
          paramZygoteServer.setForkChild();
          return runnable;
        } 
        Log.i("Zygote", "Finished refilling USAP Pool after state change.");
      } 
      this.mSocketOutStream.writeInt(0);
      return null;
    } catch (IOException iOException) {
      throw new IllegalStateException("Error writing to command socket", iOException);
    } 
  }
  
  private Runnable handleApiBlacklistExemptions(ZygoteServer paramZygoteServer, String[] paramArrayOfString) {
    return stateChangeWithUsapPoolReset(paramZygoteServer, new _$$Lambda$ZygoteConnection$xjqM7qW7vAjTqh2tR5XRF5Vn5mk(paramArrayOfString));
  }
  
  private Runnable handleUsapPoolStatusChange(ZygoteServer paramZygoteServer, boolean paramBoolean) {
    try {
      Runnable runnable = paramZygoteServer.setUsapPoolStatus(paramBoolean, this.mSocket);
      if (runnable == null) {
        this.mSocketOutStream.writeInt(0);
      } else {
        paramZygoteServer.setForkChild();
      } 
      return runnable;
    } catch (IOException iOException) {
      throw new IllegalStateException("Error writing to command socket", iOException);
    } 
  }
  
  private Runnable handleHiddenApiAccessLogSampleRate(ZygoteServer paramZygoteServer, int paramInt1, int paramInt2) {
    return stateChangeWithUsapPoolReset(paramZygoteServer, new _$$Lambda$ZygoteConnection$KxVsZ_s4KsanePOHCU5JcuypPik(paramInt1, paramInt2));
  }
  
  protected void preload() {
    ZygoteInit.lazyPreload();
  }
  
  protected boolean isPreloadComplete() {
    return ZygoteInit.isPreloadComplete();
  }
  
  protected DataOutputStream getSocketOutputStream() {
    return this.mSocketOutStream;
  }
  
  protected void handlePreloadPackage(String paramString1, String paramString2, String paramString3, String paramString4) {
    throw new RuntimeException("Zygote does not support package preloading");
  }
  
  protected boolean canPreloadApp() {
    return false;
  }
  
  protected void handlePreloadApp(ApplicationInfo paramApplicationInfo) {
    throw new RuntimeException("Zygote does not support app preloading");
  }
  
  void closeSocket() {
    try {
      this.mSocket.close();
    } catch (IOException iOException) {
      Log.e("Zygote", "Exception while closing command socket in parent", iOException);
    } 
  }
  
  boolean isClosedByPeer() {
    return this.isEof;
  }
  
  private Runnable handleChildProc(ZygoteArguments paramZygoteArguments, FileDescriptor paramFileDescriptor, boolean paramBoolean) {
    closeSocket();
    Zygote.setAppProcessName(paramZygoteArguments, "Zygote");
    Trace.traceEnd(64L);
    if (paramZygoteArguments.mInvokeWith == null) {
      if (!paramBoolean)
        return ZygoteInit.zygoteInit(paramZygoteArguments.mTargetSdkVersion, paramZygoteArguments.mDisabledCompatChanges, paramZygoteArguments.mRemainingArgs, null); 
      return ZygoteInit.childZygoteInit(paramZygoteArguments.mTargetSdkVersion, paramZygoteArguments.mRemainingArgs, null);
    } 
    String str1 = paramZygoteArguments.mInvokeWith, str2 = paramZygoteArguments.mNiceName;
    int i = paramZygoteArguments.mTargetSdkVersion;
    String str3 = VMRuntime.getCurrentInstructionSet(), arrayOfString[] = paramZygoteArguments.mRemainingArgs;
    WrapperInit.execApplication(str1, str2, i, str3, paramFileDescriptor, arrayOfString);
    throw new IllegalStateException("WrapperInit.execApplication unexpectedly returned");
  }
  
  private void handleParentProc(int paramInt, FileDescriptor paramFileDescriptor) {
    // Byte code:
    //   0: iload_1
    //   1: istore_3
    //   2: iload_3
    //   3: ifle -> 11
    //   6: aload_0
    //   7: iload_1
    //   8: invokespecial setChildPgid : (I)V
    //   11: iconst_0
    //   12: istore #4
    //   14: iconst_0
    //   15: istore #5
    //   17: aload_2
    //   18: ifnull -> 571
    //   21: iload_3
    //   22: ifle -> 571
    //   25: iconst_m1
    //   26: istore_1
    //   27: iconst_4
    //   28: istore #6
    //   30: iload #4
    //   32: istore #7
    //   34: iload_1
    //   35: istore #8
    //   37: iconst_1
    //   38: anewarray android/system/StructPollfd
    //   41: astore #9
    //   43: iload #4
    //   45: istore #7
    //   47: iload_1
    //   48: istore #8
    //   50: new android/system/StructPollfd
    //   53: astore #10
    //   55: iload #4
    //   57: istore #7
    //   59: iload_1
    //   60: istore #8
    //   62: aload #10
    //   64: invokespecial <init> : ()V
    //   67: aload #9
    //   69: iconst_0
    //   70: aload #10
    //   72: aastore
    //   73: iload #4
    //   75: istore #7
    //   77: iload_1
    //   78: istore #8
    //   80: iconst_4
    //   81: newarray byte
    //   83: astore #10
    //   85: sipush #30000
    //   88: istore #11
    //   90: iconst_0
    //   91: istore #12
    //   93: iload #4
    //   95: istore #7
    //   97: iload_1
    //   98: istore #8
    //   100: invokestatic nanoTime : ()J
    //   103: lstore #13
    //   105: iload #5
    //   107: istore #7
    //   109: iload_1
    //   110: istore #8
    //   112: iload #12
    //   114: aload #10
    //   116: arraylength
    //   117: if_icmpge -> 343
    //   120: iload #11
    //   122: ifle -> 343
    //   125: iload #5
    //   127: istore #7
    //   129: iload_1
    //   130: istore #8
    //   132: aload #9
    //   134: iconst_0
    //   135: aaload
    //   136: aload_2
    //   137: putfield fd : Ljava/io/FileDescriptor;
    //   140: iload #5
    //   142: istore #7
    //   144: iload_1
    //   145: istore #8
    //   147: aload #9
    //   149: iconst_0
    //   150: aaload
    //   151: getstatic android/system/OsConstants.POLLIN : I
    //   154: i2s
    //   155: putfield events : S
    //   158: iload #5
    //   160: istore #7
    //   162: iload_1
    //   163: istore #8
    //   165: aload #9
    //   167: iconst_0
    //   168: aaload
    //   169: iconst_0
    //   170: putfield revents : S
    //   173: iload #5
    //   175: istore #7
    //   177: iload_1
    //   178: istore #8
    //   180: aload #9
    //   182: iconst_0
    //   183: aaload
    //   184: aconst_null
    //   185: putfield userData : Ljava/lang/Object;
    //   188: iload #5
    //   190: istore #7
    //   192: iload_1
    //   193: istore #8
    //   195: aload #9
    //   197: iload #11
    //   199: invokestatic poll : ([Landroid/system/StructPollfd;I)I
    //   202: istore #15
    //   204: iload #5
    //   206: istore #7
    //   208: iload_1
    //   209: istore #8
    //   211: invokestatic nanoTime : ()J
    //   214: lstore #16
    //   216: iload #5
    //   218: istore #7
    //   220: iload_1
    //   221: istore #8
    //   223: getstatic java/util/concurrent/TimeUnit.MILLISECONDS : Ljava/util/concurrent/TimeUnit;
    //   226: astore #18
    //   228: getstatic java/util/concurrent/TimeUnit.NANOSECONDS : Ljava/util/concurrent/TimeUnit;
    //   231: astore #19
    //   233: aload #18
    //   235: lload #16
    //   237: lload #13
    //   239: lsub
    //   240: aload #19
    //   242: invokevirtual convert : (JLjava/util/concurrent/TimeUnit;)J
    //   245: l2i
    //   246: istore #8
    //   248: sipush #30000
    //   251: iload #8
    //   253: isub
    //   254: istore #11
    //   256: iload #15
    //   258: ifle -> 314
    //   261: aload #9
    //   263: iconst_0
    //   264: aaload
    //   265: getfield revents : S
    //   268: getstatic android/system/OsConstants.POLLIN : I
    //   271: iand
    //   272: ifeq -> 343
    //   275: aload_2
    //   276: aload #10
    //   278: iload #12
    //   280: iconst_1
    //   281: invokestatic read : (Ljava/io/FileDescriptor;[BII)I
    //   284: istore #8
    //   286: iload #8
    //   288: iflt -> 301
    //   291: iload #12
    //   293: iload #8
    //   295: iadd
    //   296: istore #8
    //   298: goto -> 336
    //   301: new java/lang/RuntimeException
    //   304: astore_2
    //   305: aload_2
    //   306: ldc_w 'Some error'
    //   309: invokespecial <init> : (Ljava/lang/String;)V
    //   312: aload_2
    //   313: athrow
    //   314: iload #12
    //   316: istore #8
    //   318: iload #15
    //   320: ifne -> 336
    //   323: ldc 'Zygote'
    //   325: ldc_w 'Timed out waiting for child.'
    //   328: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   331: pop
    //   332: iload #12
    //   334: istore #8
    //   336: iload #8
    //   338: istore #12
    //   340: goto -> 105
    //   343: iload #5
    //   345: istore #7
    //   347: iload #12
    //   349: aload #10
    //   351: arraylength
    //   352: if_icmpne -> 389
    //   355: new java/io/DataInputStream
    //   358: astore #9
    //   360: new java/io/ByteArrayInputStream
    //   363: astore_2
    //   364: aload_2
    //   365: aload #10
    //   367: invokespecial <init> : ([B)V
    //   370: aload #9
    //   372: aload_2
    //   373: invokespecial <init> : (Ljava/io/InputStream;)V
    //   376: aload #9
    //   378: invokevirtual readInt : ()I
    //   381: istore #8
    //   383: iload #8
    //   385: istore_1
    //   386: goto -> 389
    //   389: iload_1
    //   390: iconst_m1
    //   391: if_icmpne -> 414
    //   394: ldc 'Zygote'
    //   396: ldc_w 'Error reading pid from wrapped process, child may have died'
    //   399: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   402: pop
    //   403: goto -> 414
    //   406: astore_2
    //   407: iload #7
    //   409: istore #5
    //   411: goto -> 429
    //   414: goto -> 443
    //   417: astore_2
    //   418: goto -> 429
    //   421: astore_2
    //   422: iload #7
    //   424: istore #5
    //   426: iload #8
    //   428: istore_1
    //   429: ldc 'Zygote'
    //   431: ldc_w 'Error reading pid from wrapped process, child may have died'
    //   434: aload_2
    //   435: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   438: pop
    //   439: iload #5
    //   441: istore #7
    //   443: iload #7
    //   445: istore #5
    //   447: iload_1
    //   448: ifle -> 574
    //   451: iload_1
    //   452: istore #8
    //   454: iload #8
    //   456: ifle -> 475
    //   459: iload #8
    //   461: iload_3
    //   462: if_icmpeq -> 475
    //   465: iload #8
    //   467: invokestatic getParentPid : (I)I
    //   470: istore #8
    //   472: goto -> 454
    //   475: iload #8
    //   477: ifle -> 518
    //   480: new java/lang/StringBuilder
    //   483: dup
    //   484: invokespecial <init> : ()V
    //   487: astore_2
    //   488: aload_2
    //   489: ldc_w 'Wrapped process has pid '
    //   492: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   495: pop
    //   496: aload_2
    //   497: iload_1
    //   498: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   501: pop
    //   502: ldc 'Zygote'
    //   504: aload_2
    //   505: invokevirtual toString : ()Ljava/lang/String;
    //   508: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   511: pop
    //   512: iconst_1
    //   513: istore #5
    //   515: goto -> 576
    //   518: new java/lang/StringBuilder
    //   521: dup
    //   522: invokespecial <init> : ()V
    //   525: astore_2
    //   526: aload_2
    //   527: ldc_w 'Wrapped process reported a pid that is not a child of the process that we forked: childPid='
    //   530: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   533: pop
    //   534: aload_2
    //   535: iload_3
    //   536: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   539: pop
    //   540: aload_2
    //   541: ldc_w ' innerPid='
    //   544: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   547: pop
    //   548: aload_2
    //   549: iload_1
    //   550: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   553: pop
    //   554: ldc 'Zygote'
    //   556: aload_2
    //   557: invokevirtual toString : ()Ljava/lang/String;
    //   560: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   563: pop
    //   564: iload #7
    //   566: istore #5
    //   568: goto -> 574
    //   571: iconst_0
    //   572: istore #5
    //   574: iload_3
    //   575: istore_1
    //   576: aload_0
    //   577: getfield mSocketOutStream : Ljava/io/DataOutputStream;
    //   580: iload_1
    //   581: invokevirtual writeInt : (I)V
    //   584: aload_0
    //   585: getfield mSocketOutStream : Ljava/io/DataOutputStream;
    //   588: iload #5
    //   590: invokevirtual writeBoolean : (Z)V
    //   593: return
    //   594: astore_2
    //   595: new java/lang/IllegalStateException
    //   598: dup
    //   599: ldc 'Error writing to command socket'
    //   601: aload_2
    //   602: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   605: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #522	-> 0
    //   #523	-> 6
    //   #526	-> 11
    //   #527	-> 17
    //   #528	-> 25
    //   #532	-> 27
    //   #534	-> 30
    //   #538	-> 73
    //   #540	-> 85
    //   #541	-> 90
    //   #542	-> 93
    //   #544	-> 105
    //   #545	-> 125
    //   #546	-> 140
    //   #547	-> 158
    //   #548	-> 173
    //   #550	-> 188
    //   #551	-> 204
    //   #552	-> 216
    //   #553	-> 233
    //   #556	-> 248
    //   #558	-> 256
    //   #559	-> 261
    //   #561	-> 275
    //   #562	-> 286
    //   #565	-> 291
    //   #566	-> 298
    //   #563	-> 301
    //   #570	-> 314
    //   #571	-> 323
    //   #573	-> 336
    //   #544	-> 343
    //   #575	-> 343
    //   #576	-> 355
    //   #577	-> 376
    //   #575	-> 389
    //   #580	-> 389
    //   #581	-> 394
    //   #583	-> 406
    //   #585	-> 414
    //   #583	-> 417
    //   #584	-> 429
    //   #589	-> 443
    //   #590	-> 451
    //   #591	-> 454
    //   #592	-> 465
    //   #594	-> 475
    //   #595	-> 480
    //   #596	-> 512
    //   #597	-> 512
    //   #599	-> 518
    //   #527	-> 571
    //   #607	-> 574
    //   #608	-> 584
    //   #611	-> 593
    //   #612	-> 593
    //   #609	-> 594
    //   #610	-> 595
    // Exception table:
    //   from	to	target	type
    //   37	43	421	java/lang/Exception
    //   50	55	421	java/lang/Exception
    //   62	67	421	java/lang/Exception
    //   80	85	421	java/lang/Exception
    //   100	105	421	java/lang/Exception
    //   112	120	421	java/lang/Exception
    //   132	140	421	java/lang/Exception
    //   147	158	421	java/lang/Exception
    //   165	173	421	java/lang/Exception
    //   180	188	421	java/lang/Exception
    //   195	204	421	java/lang/Exception
    //   211	216	421	java/lang/Exception
    //   223	228	421	java/lang/Exception
    //   228	233	417	java/lang/Exception
    //   233	248	417	java/lang/Exception
    //   261	275	417	java/lang/Exception
    //   275	286	417	java/lang/Exception
    //   301	314	417	java/lang/Exception
    //   323	332	417	java/lang/Exception
    //   347	355	417	java/lang/Exception
    //   355	376	417	java/lang/Exception
    //   376	383	417	java/lang/Exception
    //   394	403	406	java/lang/Exception
    //   576	584	594	java/io/IOException
    //   584	593	594	java/io/IOException
  }
  
  private void setChildPgid(int paramInt) {
    try {
      Os.setpgid(paramInt, Os.getpgid(this.peer.getPid()));
    } catch (ErrnoException errnoException) {
      Log.i("Zygote", "Zygote: setpgid failed. This is normal if peer is not in our session");
    } 
  }
}
