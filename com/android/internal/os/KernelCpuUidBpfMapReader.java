package com.android.internal.os;

import android.os.SystemClock;
import android.util.Slog;
import android.util.SparseArray;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public abstract class KernelCpuUidBpfMapReader {
  private static final KernelCpuUidBpfMapReader FREQ_TIME_READER = new KernelCpuUidFreqTimeBpfMapReader();
  
  static {
    ACTIVE_TIME_READER = new KernelCpuUidActiveTimeBpfMapReader();
    CLUSTER_TIME_READER = new KernelCpuUidClusterTimeBpfMapReader();
  }
  
  static KernelCpuUidBpfMapReader getFreqTimeReaderInstance() {
    return FREQ_TIME_READER;
  }
  
  static KernelCpuUidBpfMapReader getActiveTimeReaderInstance() {
    return ACTIVE_TIME_READER;
  }
  
  static KernelCpuUidBpfMapReader getClusterTimeReaderInstance() {
    return CLUSTER_TIME_READER;
  }
  
  final String mTag = getClass().getSimpleName();
  
  private int mErrors = 0;
  
  private boolean mTracking = false;
  
  protected SparseArray<long[]> mData = (SparseArray)new SparseArray<>();
  
  private long mLastReadTime = 0L;
  
  private static final KernelCpuUidBpfMapReader ACTIVE_TIME_READER;
  
  private static final KernelCpuUidBpfMapReader CLUSTER_TIME_READER;
  
  private static final int ERROR_THRESHOLD = 5;
  
  private static final long FRESHNESS_MS = 500L;
  
  protected final ReentrantReadWriteLock mLock;
  
  protected final ReentrantReadWriteLock.ReadLock mReadLock;
  
  protected final ReentrantReadWriteLock.WriteLock mWriteLock;
  
  public KernelCpuUidBpfMapReader() {
    ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock();
    this.mReadLock = reentrantReadWriteLock.readLock();
    this.mWriteLock = this.mLock.writeLock();
  }
  
  public void removeUidsInRange(int paramInt1, int paramInt2) {
    if (this.mErrors > 5)
      return; 
    this.mWriteLock.lock();
    paramInt1 = this.mData.indexOfKey(paramInt1);
    paramInt2 = this.mData.indexOfKey(paramInt2);
    try {
      this.mData.removeAtRange(paramInt1, paramInt2 - paramInt1 + 1);
    } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
      Slog.w(this.mTag, "removeUidsInRange ArrayIndexOutOfBoundsException", arrayIndexOutOfBoundsException);
    } 
    this.mWriteLock.unlock();
  }
  
  public BpfMapIterator open() {
    return open(false);
  }
  
  public BpfMapIterator open(boolean paramBoolean) {
    if (this.mErrors > 5)
      return null; 
    if (!this.mTracking && !startTrackingBpfTimes()) {
      Slog.w(this.mTag, "Failed to start tracking");
      this.mErrors++;
      return null;
    } 
    if (paramBoolean) {
      this.mWriteLock.lock();
    } else {
      this.mReadLock.lock();
      if (dataValid())
        return new BpfMapIterator(); 
      this.mReadLock.unlock();
      this.mWriteLock.lock();
      if (dataValid()) {
        this.mReadLock.lock();
        this.mWriteLock.unlock();
        return new BpfMapIterator();
      } 
    } 
    if (readBpfData()) {
      this.mLastReadTime = SystemClock.elapsedRealtime();
      this.mReadLock.lock();
      this.mWriteLock.unlock();
      return new BpfMapIterator();
    } 
    this.mWriteLock.unlock();
    this.mErrors++;
    Slog.w(this.mTag, "Failed to read bpf times");
    return null;
  }
  
  private boolean dataValid() {
    boolean bool;
    if (this.mData.size() > 0 && SystemClock.elapsedRealtime() - this.mLastReadTime < 500L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public abstract long[] getDataDimensions();
  
  protected abstract boolean readBpfData();
  
  public native boolean startTrackingBpfTimes();
  
  public class BpfMapIterator implements AutoCloseable {
    private int mPos;
    
    final KernelCpuUidBpfMapReader this$0;
    
    public boolean getNextUid(long[] param1ArrayOflong) {
      if (this.mPos >= KernelCpuUidBpfMapReader.this.mData.size())
        return false; 
      param1ArrayOflong[0] = KernelCpuUidBpfMapReader.this.mData.keyAt(this.mPos);
      System.arraycopy(KernelCpuUidBpfMapReader.this.mData.valueAt(this.mPos), 0, param1ArrayOflong, 1, ((long[])KernelCpuUidBpfMapReader.this.mData.valueAt(this.mPos)).length);
      this.mPos++;
      return true;
    }
    
    public void close() {
      KernelCpuUidBpfMapReader.this.mReadLock.unlock();
    }
  }
  
  class KernelCpuUidFreqTimeBpfMapReader extends KernelCpuUidBpfMapReader {
    private final native boolean removeUidRange(int param1Int1, int param1Int2);
    
    public final native long[] getDataDimensions();
    
    protected final native boolean readBpfData();
    
    public void removeUidsInRange(int param1Int1, int param1Int2) {
      this.mWriteLock.lock();
      super.removeUidsInRange(param1Int1, param1Int2);
      removeUidRange(param1Int1, param1Int2);
      this.mWriteLock.unlock();
    }
  }
  
  class KernelCpuUidActiveTimeBpfMapReader extends KernelCpuUidBpfMapReader {
    public final native long[] getDataDimensions();
    
    protected final native boolean readBpfData();
  }
  
  class KernelCpuUidClusterTimeBpfMapReader extends KernelCpuUidBpfMapReader {
    public final native long[] getDataDimensions();
    
    protected final native boolean readBpfData();
  }
}
