package com.android.internal.os;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;

public class AppFuseMount implements Parcelable {
  public AppFuseMount(int paramInt, ParcelFileDescriptor paramParcelFileDescriptor) {
    Preconditions.checkNotNull(paramParcelFileDescriptor);
    this.mountPointId = paramInt;
    this.fd = paramParcelFileDescriptor;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mountPointId);
    paramParcel.writeParcelable((Parcelable)this.fd, paramInt);
  }
  
  public static final Parcelable.Creator<AppFuseMount> CREATOR = new Parcelable.Creator<AppFuseMount>() {
      public AppFuseMount createFromParcel(Parcel param1Parcel) {
        return new AppFuseMount(param1Parcel.readInt(), (ParcelFileDescriptor)param1Parcel.readParcelable(null));
      }
      
      public AppFuseMount[] newArray(int param1Int) {
        return new AppFuseMount[param1Int];
      }
    };
  
  public final ParcelFileDescriptor fd;
  
  public final int mountPointId;
}
