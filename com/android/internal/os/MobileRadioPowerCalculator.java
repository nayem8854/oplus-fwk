package com.android.internal.os;

import android.os.BatteryStats;
import android.telephony.CellSignalStrength;

public class MobileRadioPowerCalculator extends PowerCalculator {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "MobileRadioPowerController";
  
  private final double[] mPowerBins = new double[CellSignalStrength.getNumSignalStrengthLevels()];
  
  private final double mPowerRadioOn;
  
  private final double mPowerScan;
  
  private BatteryStats mStats;
  
  private long mTotalAppMobileActiveMs = 0L;
  
  private double getMobilePowerPerPacket(long paramLong, int paramInt) {
    double d2, d1 = this.mPowerRadioOn / 3600.0D;
    long l1 = this.mStats.getNetworkActivityPackets(0, paramInt);
    long l2 = this.mStats.getNetworkActivityPackets(1, paramInt);
    l1 += l2;
    BatteryStats batteryStats = this.mStats;
    paramLong = batteryStats.getMobileRadioActiveTime(paramLong, paramInt) / 1000L;
    if (l1 != 0L && paramLong != 0L) {
      d2 = l1 / paramLong;
    } else {
      d2 = 12.20703125D;
    } 
    return d1 / d2 / 3600.0D;
  }
  
  public MobileRadioPowerCalculator(PowerProfile paramPowerProfile, BatteryStats paramBatteryStats) {
    double d = paramPowerProfile.getAveragePowerOrDefault("radio.active", -1.0D);
    if (d != -1.0D) {
      this.mPowerRadioOn = d;
    } else {
      double[] arrayOfDouble;
      d = 0.0D + paramPowerProfile.getAveragePower("modem.controller.rx");
      byte b = 0;
      while (true) {
        arrayOfDouble = this.mPowerBins;
        if (b < arrayOfDouble.length) {
          d += paramPowerProfile.getAveragePower("modem.controller.tx", b);
          b++;
          continue;
        } 
        break;
      } 
      this.mPowerRadioOn = d / (arrayOfDouble.length + 1);
    } 
    d = paramPowerProfile.getAveragePowerOrDefault("radio.on", -1.0D);
    if (d != -1.0D) {
      byte b = 0;
      while (true) {
        double[] arrayOfDouble = this.mPowerBins;
        if (b < arrayOfDouble.length) {
          arrayOfDouble[b] = paramPowerProfile.getAveragePower("radio.on", b);
          b++;
          continue;
        } 
        break;
      } 
    } else {
      d = paramPowerProfile.getAveragePower("modem.controller.idle");
      this.mPowerBins[0] = 25.0D * d / 180.0D;
      byte b = 1;
      while (true) {
        double[] arrayOfDouble = this.mPowerBins;
        if (b < arrayOfDouble.length) {
          arrayOfDouble[b] = Math.max(1.0D, d / 256.0D);
          b++;
          continue;
        } 
        break;
      } 
    } 
    this.mPowerScan = paramPowerProfile.getAveragePowerOrDefault("radio.scanning", 0.0D);
    this.mStats = paramBatteryStats;
  }
  
  public void calculateApp(BatterySipper paramBatterySipper, BatteryStats.Uid paramUid, long paramLong1, long paramLong2, int paramInt) {
    paramBatterySipper.mobileRxPackets = paramUid.getNetworkActivityPackets(0, paramInt);
    paramBatterySipper.mobileTxPackets = paramUid.getNetworkActivityPackets(1, paramInt);
    paramBatterySipper.mobileActive = paramUid.getMobileRadioActiveTime(paramInt) / 1000L;
    paramBatterySipper.mobileActiveCount = paramUid.getMobileRadioActiveCount(paramInt);
    paramBatterySipper.mobileRxBytes = paramUid.getNetworkActivityBytes(0, paramInt);
    paramBatterySipper.mobileTxBytes = paramUid.getNetworkActivityBytes(1, paramInt);
    if (paramBatterySipper.mobileActive > 0L) {
      this.mTotalAppMobileActiveMs += paramBatterySipper.mobileActive;
      paramBatterySipper.mobileRadioPowerMah = paramBatterySipper.mobileActive * this.mPowerRadioOn / 3600000.0D;
    } else {
      double d = (paramBatterySipper.mobileRxPackets + paramBatterySipper.mobileTxPackets);
      paramBatterySipper.mobileRadioPowerMah = d * getMobilePowerPerPacket(paramLong1, paramInt);
    } 
  }
  
  public void calculateRemaining(BatterySipper paramBatterySipper, BatteryStats paramBatteryStats, long paramLong1, long paramLong2, int paramInt) {
    double d1 = 0.0D;
    long l1 = 0L;
    paramLong2 = 0L;
    for (byte b = 0; b < this.mPowerBins.length; b++) {
      long l = paramBatteryStats.getPhoneSignalStrengthTime(b, paramLong1, paramInt) / 1000L;
      double d = l * this.mPowerBins[b] / 3600000.0D;
      d1 += d;
      l1 += l;
      if (b == 0)
        paramLong2 = l; 
    } 
    long l2 = paramBatteryStats.getPhoneSignalScanningTime(paramLong1, paramInt) / 1000L;
    double d2 = l2 * this.mPowerScan / 3600000.0D;
    d1 += d2;
    paramLong1 = this.mStats.getMobileRadioActiveTime(paramLong1, paramInt) / 1000L;
    paramLong1 -= this.mTotalAppMobileActiveMs;
    if (paramLong1 > 0L)
      d1 += this.mPowerRadioOn * paramLong1 / 3600000.0D; 
    if (d1 != 0.0D) {
      if (l1 != 0L)
        paramBatterySipper.noCoveragePercent = paramLong2 * 100.0D / l1; 
      paramBatterySipper.mobileActive = paramLong1;
      paramBatterySipper.mobileActiveCount = paramBatteryStats.getMobileRadioActiveUnknownCount(paramInt);
      paramBatterySipper.mobileRadioPowerMah = d1;
    } 
  }
  
  public void reset() {
    this.mTotalAppMobileActiveMs = 0L;
  }
  
  public void reset(BatteryStats paramBatteryStats) {
    reset();
    this.mStats = paramBatteryStats;
  }
}
