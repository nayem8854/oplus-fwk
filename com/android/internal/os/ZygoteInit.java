package com.android.internal.os;

import android.app.ApplicationLoaders;
import android.app.IOplusCommonInjector;
import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.pm.SharedLibraryInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Environment;
import android.os.IInstalld;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.Trace;
import android.os.UserHandle;
import android.os.ZygoteProcess;
import android.security.keystore.AndroidKeyStoreProvider;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.system.StructCapUserData;
import android.system.StructCapUserHeader;
import android.text.Hyphenator;
import android.util.Log;
import android.util.TimingsTraceLog;
import android.webkit.WebViewFactory;
import android.widget.TextView;
import com.android.internal.util.Preconditions;
import dalvik.system.VMRuntime;
import dalvik.system.ZygoteHooks;
import java.io.File;
import java.io.FileWriter;
import java.security.Provider;
import java.security.Security;

public class ZygoteInit {
  private static final String ABI_LIST_ARG = "--abi-list=";
  
  private static final int LOG_BOOT_PROGRESS_PRELOAD_END = 3030;
  
  private static final int LOG_BOOT_PROGRESS_PRELOAD_START = 3020;
  
  private static final String PRELOADED_CLASSES = "/system/etc/preloaded-classes";
  
  public static final boolean PRELOAD_RESOURCES = true;
  
  private static final String PROPERTY_DISABLE_GRAPHICS_DRIVER_PRELOADING = "ro.zygote.disable_gl_preload";
  
  private static final int ROOT_GID = 0;
  
  private static final int ROOT_UID = 0;
  
  private static final String SOCKET_NAME_ARG = "--socket-name=";
  
  private static final String TAG = "Zygote";
  
  private static final int UNPRIVILEGED_GID = 9999;
  
  private static final int UNPRIVILEGED_UID = 9999;
  
  private static Resources mResources;
  
  private static boolean sPreloadComplete;
  
  static void preload(TimingsTraceLog paramTimingsTraceLog) {
    Log.d("Zygote", "begin preload");
    paramTimingsTraceLog.traceBegin("BeginPreload");
    beginPreload();
    paramTimingsTraceLog.traceEnd();
    paramTimingsTraceLog.traceBegin("PreloadClasses");
    preloadClasses();
    paramTimingsTraceLog.traceEnd();
    paramTimingsTraceLog.traceBegin("CacheNonBootClasspathClassLoaders");
    cacheNonBootClasspathClassLoaders();
    paramTimingsTraceLog.traceEnd();
    paramTimingsTraceLog.traceBegin("PreloadResources");
    preloadResources();
    paramTimingsTraceLog.traceEnd();
    Trace.traceBegin(16384L, "PreloadAppProcessHALs");
    nativePreloadAppProcessHALs();
    Trace.traceEnd(16384L);
    Trace.traceBegin(16384L, "PreloadGraphicsDriver");
    maybePreloadGraphicsDriver();
    Trace.traceEnd(16384L);
    preloadSharedLibraries();
    preloadTextResources();
    WebViewFactory.prepareWebViewInZygote();
    endPreload();
    warmUpJcaProviders();
    Log.d("Zygote", "end preload");
    sPreloadComplete = true;
  }
  
  public static void lazyPreload() {
    Preconditions.checkState(sPreloadComplete ^ true);
    Log.i("Zygote", "Lazily preloading resources.");
    preload(new TimingsTraceLog("ZygoteInitTiming_lazy", 16384L));
  }
  
  private static void beginPreload() {
    Log.i("Zygote", "Calling ZygoteHooks.beginPreload()");
    ZygoteHooks.onBeginPreload();
  }
  
  private static void endPreload() {
    ZygoteHooks.onEndPreload();
    Log.i("Zygote", "Called ZygoteHooks.endPreload()");
  }
  
  private static void preloadSharedLibraries() {
    Log.i("Zygote", "Preloading shared libraries...");
    System.loadLibrary("android");
    System.loadLibrary("compiler_rt");
    System.loadLibrary("jnigraphics");
    try {
      System.loadLibrary("sfplugin_ccodec");
    } catch (Error|RuntimeException error) {}
    try {
      System.loadLibrary("qti_performance");
    } catch (UnsatisfiedLinkError unsatisfiedLinkError) {
      Log.e("Zygote", "Couldn't load qti_performance");
    } 
  }
  
  private static void maybePreloadGraphicsDriver() {
    if (!SystemProperties.getBoolean("ro.zygote.disable_gl_preload", false))
      nativePreloadGraphicsDriver(); 
  }
  
  private static void preloadTextResources() {
    Hyphenator.init();
    TextView.preloadFontCache();
  }
  
  private static void warmUpJcaProviders() {
    long l = SystemClock.uptimeMillis();
    Trace.traceBegin(16384L, "Starting installation of AndroidKeyStoreProvider");
    AndroidKeyStoreProvider.install();
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("Installed AndroidKeyStoreProvider in ");
    stringBuilder2.append(SystemClock.uptimeMillis() - l);
    stringBuilder2.append("ms.");
    String str2 = stringBuilder2.toString();
    Log.i("Zygote", str2);
    Trace.traceEnd(16384L);
    l = SystemClock.uptimeMillis();
    Trace.traceBegin(16384L, "Starting warm up of JCA providers");
    for (Provider provider : Security.getProviders())
      provider.warmUpServiceProvision(); 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("Warmed up JCA providers in ");
    stringBuilder1.append(SystemClock.uptimeMillis() - l);
    stringBuilder1.append("ms.");
    String str1 = stringBuilder1.toString();
    Log.i("Zygote", str1);
    Trace.traceEnd(16384L);
  }
  
  private static void preloadClasses() {
    // Byte code:
    //   0: ldc_w 'Failed to restore root'
    //   3: astore_0
    //   4: ldc_w 'ResetJitCounters'
    //   7: astore_1
    //   8: invokestatic getRuntime : ()Ldalvik/system/VMRuntime;
    //   11: astore_2
    //   12: new java/io/FileInputStream
    //   15: dup
    //   16: ldc '/system/etc/preloaded-classes'
    //   18: invokespecial <init> : (Ljava/lang/String;)V
    //   21: astore_3
    //   22: ldc 'Zygote'
    //   24: ldc_w 'Preloading classes...'
    //   27: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   30: pop
    //   31: invokestatic uptimeMillis : ()J
    //   34: lstore #4
    //   36: invokestatic getuid : ()I
    //   39: istore #6
    //   41: invokestatic getgid : ()I
    //   44: istore #7
    //   46: iconst_0
    //   47: istore #8
    //   49: iload #8
    //   51: istore #9
    //   53: iload #6
    //   55: ifne -> 100
    //   58: iload #8
    //   60: istore #9
    //   62: iload #7
    //   64: ifne -> 100
    //   67: iconst_0
    //   68: sipush #9999
    //   71: invokestatic setregid : (II)V
    //   74: iconst_0
    //   75: sipush #9999
    //   78: invokestatic setreuid : (II)V
    //   81: iconst_1
    //   82: istore #9
    //   84: goto -> 100
    //   87: astore_1
    //   88: new java/lang/RuntimeException
    //   91: dup
    //   92: ldc_w 'Failed to drop root'
    //   95: aload_1
    //   96: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   99: athrow
    //   100: aload_0
    //   101: astore #10
    //   103: aload_1
    //   104: astore #11
    //   106: aload_0
    //   107: astore #12
    //   109: aload_1
    //   110: astore #13
    //   112: new java/io/BufferedReader
    //   115: astore #14
    //   117: aload_0
    //   118: astore #10
    //   120: aload_1
    //   121: astore #11
    //   123: aload_0
    //   124: astore #12
    //   126: aload_1
    //   127: astore #13
    //   129: new java/io/InputStreamReader
    //   132: astore #15
    //   134: aload_0
    //   135: astore #10
    //   137: aload_1
    //   138: astore #11
    //   140: aload_0
    //   141: astore #12
    //   143: aload_1
    //   144: astore #13
    //   146: aload #15
    //   148: aload_3
    //   149: invokespecial <init> : (Ljava/io/InputStream;)V
    //   152: aload_0
    //   153: astore #10
    //   155: aload_1
    //   156: astore #11
    //   158: aload_0
    //   159: astore #12
    //   161: aload_1
    //   162: astore #13
    //   164: aload #14
    //   166: aload #15
    //   168: sipush #256
    //   171: invokespecial <init> : (Ljava/io/Reader;I)V
    //   174: iconst_0
    //   175: istore #8
    //   177: aload_0
    //   178: astore #10
    //   180: aload_1
    //   181: astore #11
    //   183: aload_0
    //   184: astore #12
    //   186: aload_1
    //   187: astore #13
    //   189: aload #14
    //   191: invokevirtual readLine : ()Ljava/lang/String;
    //   194: astore #15
    //   196: aload #15
    //   198: ifnull -> 501
    //   201: aload_0
    //   202: astore #10
    //   204: aload_1
    //   205: astore #11
    //   207: aload_0
    //   208: astore #12
    //   210: aload_1
    //   211: astore #13
    //   213: aload #15
    //   215: invokevirtual trim : ()Ljava/lang/String;
    //   218: astore #15
    //   220: aload_0
    //   221: astore #10
    //   223: aload_1
    //   224: astore #11
    //   226: aload_0
    //   227: astore #12
    //   229: aload_1
    //   230: astore #13
    //   232: aload #15
    //   234: ldc_w '#'
    //   237: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   240: ifne -> 498
    //   243: aload_0
    //   244: astore #10
    //   246: aload_1
    //   247: astore #11
    //   249: aload_0
    //   250: astore #12
    //   252: aload_1
    //   253: astore #13
    //   255: aload #15
    //   257: ldc_w ''
    //   260: invokevirtual equals : (Ljava/lang/Object;)Z
    //   263: istore #16
    //   265: iload #16
    //   267: ifeq -> 273
    //   270: goto -> 498
    //   273: ldc2_w 16384
    //   276: aload #15
    //   278: invokestatic traceBegin : (JLjava/lang/String;)V
    //   281: aload #15
    //   283: iconst_1
    //   284: aconst_null
    //   285: invokestatic forName : (Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;
    //   288: pop
    //   289: iinc #8, 1
    //   292: goto -> 489
    //   295: astore #11
    //   297: new java/lang/StringBuilder
    //   300: astore #13
    //   302: aload #13
    //   304: invokespecial <init> : ()V
    //   307: aload #13
    //   309: ldc_w 'Error preloading '
    //   312: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   315: pop
    //   316: aload #13
    //   318: aload #15
    //   320: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   323: pop
    //   324: aload #13
    //   326: ldc_w '.'
    //   329: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   332: pop
    //   333: ldc 'Zygote'
    //   335: aload #13
    //   337: invokevirtual toString : ()Ljava/lang/String;
    //   340: aload #11
    //   342: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   345: pop
    //   346: aload #11
    //   348: instanceof java/lang/Error
    //   351: ifne -> 383
    //   354: aload #11
    //   356: instanceof java/lang/RuntimeException
    //   359: ifeq -> 368
    //   362: aload #11
    //   364: checkcast java/lang/RuntimeException
    //   367: athrow
    //   368: new java/lang/RuntimeException
    //   371: astore #13
    //   373: aload #13
    //   375: aload #11
    //   377: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   380: aload #13
    //   382: athrow
    //   383: aload #11
    //   385: checkcast java/lang/Error
    //   388: athrow
    //   389: astore #13
    //   391: new java/lang/StringBuilder
    //   394: astore #11
    //   396: aload #11
    //   398: invokespecial <init> : ()V
    //   401: aload #11
    //   403: ldc_w 'Problem preloading '
    //   406: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   409: pop
    //   410: aload #11
    //   412: aload #15
    //   414: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   417: pop
    //   418: aload #11
    //   420: ldc_w ': '
    //   423: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   426: pop
    //   427: aload #11
    //   429: aload #13
    //   431: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   434: pop
    //   435: ldc 'Zygote'
    //   437: aload #11
    //   439: invokevirtual toString : ()Ljava/lang/String;
    //   442: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   445: pop
    //   446: goto -> 489
    //   449: astore #11
    //   451: new java/lang/StringBuilder
    //   454: astore #11
    //   456: aload #11
    //   458: invokespecial <init> : ()V
    //   461: aload #11
    //   463: ldc_w 'Class not found for preloading: '
    //   466: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   469: pop
    //   470: aload #11
    //   472: aload #15
    //   474: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   477: pop
    //   478: ldc 'Zygote'
    //   480: aload #11
    //   482: invokevirtual toString : ()Ljava/lang/String;
    //   485: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   488: pop
    //   489: ldc2_w 16384
    //   492: invokestatic traceEnd : (J)V
    //   495: goto -> 177
    //   498: goto -> 177
    //   501: new java/lang/StringBuilder
    //   504: astore #11
    //   506: aload #11
    //   508: invokespecial <init> : ()V
    //   511: aload #11
    //   513: ldc_w '...preloaded '
    //   516: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   519: pop
    //   520: aload #11
    //   522: iload #8
    //   524: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   527: pop
    //   528: aload #11
    //   530: ldc_w ' classes in '
    //   533: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   536: pop
    //   537: aload #11
    //   539: invokestatic uptimeMillis : ()J
    //   542: lload #4
    //   544: lsub
    //   545: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   548: pop
    //   549: aload #11
    //   551: ldc_w 'ms.'
    //   554: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   557: pop
    //   558: aload #11
    //   560: invokevirtual toString : ()Ljava/lang/String;
    //   563: astore #11
    //   565: ldc 'Zygote'
    //   567: aload #11
    //   569: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   572: pop
    //   573: aload_3
    //   574: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   577: ldc2_w 16384
    //   580: ldc_w 'PreloadDexCaches'
    //   583: invokestatic traceBegin : (JLjava/lang/String;)V
    //   586: aload_2
    //   587: invokevirtual preloadDexCaches : ()V
    //   590: ldc2_w 16384
    //   593: invokestatic traceEnd : (J)V
    //   596: ldc_w 'persist.device_config.runtime_native_boot.profilebootclasspath'
    //   599: ldc_w ''
    //   602: invokestatic get : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   605: astore #11
    //   607: aload #11
    //   609: invokevirtual length : ()I
    //   612: ifne -> 629
    //   615: ldc_w 'dalvik.vm.profilebootclasspath'
    //   618: ldc_w ''
    //   621: invokestatic get : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   624: astore #11
    //   626: goto -> 629
    //   629: ldc_w 'true'
    //   632: aload #11
    //   634: invokevirtual equals : (Ljava/lang/Object;)Z
    //   637: ifeq -> 656
    //   640: ldc2_w 16384
    //   643: aload_1
    //   644: invokestatic traceBegin : (JLjava/lang/String;)V
    //   647: invokestatic resetJitCounters : ()V
    //   650: ldc2_w 16384
    //   653: invokestatic traceEnd : (J)V
    //   656: iload #9
    //   658: ifeq -> 844
    //   661: iconst_0
    //   662: iconst_0
    //   663: invokestatic setreuid : (II)V
    //   666: iconst_0
    //   667: iconst_0
    //   668: invokestatic setregid : (II)V
    //   671: goto -> 830
    //   674: astore_1
    //   675: new java/lang/RuntimeException
    //   678: dup
    //   679: aload_0
    //   680: aload_1
    //   681: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   684: athrow
    //   685: astore #11
    //   687: aload_1
    //   688: astore #13
    //   690: aload #11
    //   692: astore_1
    //   693: aload #13
    //   695: astore #11
    //   697: goto -> 856
    //   700: astore #11
    //   702: aload_1
    //   703: astore #13
    //   705: aload_0
    //   706: astore_1
    //   707: goto -> 724
    //   710: astore_1
    //   711: aload #10
    //   713: astore_0
    //   714: goto -> 856
    //   717: astore_0
    //   718: aload #12
    //   720: astore_1
    //   721: aload_0
    //   722: astore #11
    //   724: ldc 'Zygote'
    //   726: ldc_w 'Error reading /system/etc/preloaded-classes.'
    //   729: aload #11
    //   731: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   734: pop
    //   735: aload_3
    //   736: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   739: ldc2_w 16384
    //   742: ldc_w 'PreloadDexCaches'
    //   745: invokestatic traceBegin : (JLjava/lang/String;)V
    //   748: aload_2
    //   749: invokevirtual preloadDexCaches : ()V
    //   752: ldc2_w 16384
    //   755: invokestatic traceEnd : (J)V
    //   758: ldc_w 'persist.device_config.runtime_native_boot.profilebootclasspath'
    //   761: ldc_w ''
    //   764: invokestatic get : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   767: astore_0
    //   768: aload_0
    //   769: invokevirtual length : ()I
    //   772: ifne -> 788
    //   775: ldc_w 'dalvik.vm.profilebootclasspath'
    //   778: ldc_w ''
    //   781: invokestatic get : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   784: astore_0
    //   785: goto -> 788
    //   788: ldc_w 'true'
    //   791: aload_0
    //   792: invokevirtual equals : (Ljava/lang/Object;)Z
    //   795: ifeq -> 815
    //   798: ldc2_w 16384
    //   801: aload #13
    //   803: invokestatic traceBegin : (JLjava/lang/String;)V
    //   806: invokestatic resetJitCounters : ()V
    //   809: ldc2_w 16384
    //   812: invokestatic traceEnd : (J)V
    //   815: iload #9
    //   817: ifeq -> 844
    //   820: iconst_0
    //   821: iconst_0
    //   822: invokestatic setreuid : (II)V
    //   825: iconst_0
    //   826: iconst_0
    //   827: invokestatic setregid : (II)V
    //   830: goto -> 844
    //   833: astore_0
    //   834: new java/lang/RuntimeException
    //   837: dup
    //   838: aload_1
    //   839: aload_0
    //   840: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   843: athrow
    //   844: return
    //   845: astore #12
    //   847: aload_1
    //   848: astore_0
    //   849: aload #13
    //   851: astore #11
    //   853: aload #12
    //   855: astore_1
    //   856: aload_3
    //   857: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   860: ldc2_w 16384
    //   863: ldc_w 'PreloadDexCaches'
    //   866: invokestatic traceBegin : (JLjava/lang/String;)V
    //   869: aload_2
    //   870: invokevirtual preloadDexCaches : ()V
    //   873: ldc2_w 16384
    //   876: invokestatic traceEnd : (J)V
    //   879: ldc_w 'persist.device_config.runtime_native_boot.profilebootclasspath'
    //   882: ldc_w ''
    //   885: invokestatic get : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   888: astore #12
    //   890: aload #12
    //   892: astore #13
    //   894: aload #12
    //   896: invokevirtual length : ()I
    //   899: ifne -> 913
    //   902: ldc_w 'dalvik.vm.profilebootclasspath'
    //   905: ldc_w ''
    //   908: invokestatic get : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   911: astore #13
    //   913: ldc_w 'true'
    //   916: aload #13
    //   918: invokevirtual equals : (Ljava/lang/Object;)Z
    //   921: ifeq -> 941
    //   924: ldc2_w 16384
    //   927: aload #11
    //   929: invokestatic traceBegin : (JLjava/lang/String;)V
    //   932: invokestatic resetJitCounters : ()V
    //   935: ldc2_w 16384
    //   938: invokestatic traceEnd : (J)V
    //   941: iload #9
    //   943: ifeq -> 970
    //   946: iconst_0
    //   947: iconst_0
    //   948: invokestatic setreuid : (II)V
    //   951: iconst_0
    //   952: iconst_0
    //   953: invokestatic setregid : (II)V
    //   956: goto -> 970
    //   959: astore_1
    //   960: new java/lang/RuntimeException
    //   963: dup
    //   964: aload_0
    //   965: aload_1
    //   966: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   969: athrow
    //   970: aload_1
    //   971: athrow
    //   972: astore_1
    //   973: ldc 'Zygote'
    //   975: ldc_w 'Couldn't find /system/etc/preloaded-classes.'
    //   978: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   981: pop
    //   982: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #267	-> 0
    //   #271	-> 12
    //   #275	-> 22
    //   #277	-> 22
    //   #278	-> 31
    //   #281	-> 36
    //   #282	-> 41
    //   #287	-> 46
    //   #288	-> 46
    //   #290	-> 67
    //   #291	-> 74
    //   #294	-> 81
    //   #296	-> 81
    //   #292	-> 87
    //   #293	-> 88
    //   #300	-> 100
    //   #303	-> 174
    //   #305	-> 177
    //   #307	-> 201
    //   #308	-> 220
    //   #309	-> 270
    //   #312	-> 273
    //   #319	-> 281
    //   #320	-> 289
    //   #334	-> 292
    //   #325	-> 295
    //   #326	-> 297
    //   #327	-> 346
    //   #330	-> 354
    //   #331	-> 362
    //   #333	-> 368
    //   #328	-> 383
    //   #323	-> 389
    //   #324	-> 391
    //   #334	-> 446
    //   #321	-> 449
    //   #322	-> 451
    //   #334	-> 489
    //   #335	-> 489
    //   #308	-> 498
    //   #305	-> 498
    //   #338	-> 501
    //   #339	-> 537
    //   #338	-> 565
    //   #343	-> 573
    //   #346	-> 577
    //   #347	-> 586
    //   #348	-> 590
    //   #354	-> 596
    //   #357	-> 607
    //   #358	-> 615
    //   #357	-> 629
    //   #360	-> 629
    //   #361	-> 640
    //   #362	-> 647
    //   #363	-> 650
    //   #367	-> 656
    //   #369	-> 661
    //   #370	-> 666
    //   #371	-> 674
    //   #372	-> 675
    //   #343	-> 685
    //   #340	-> 700
    //   #343	-> 710
    //   #340	-> 717
    //   #341	-> 724
    //   #343	-> 735
    //   #346	-> 739
    //   #347	-> 748
    //   #348	-> 752
    //   #354	-> 758
    //   #357	-> 768
    //   #358	-> 775
    //   #357	-> 788
    //   #360	-> 788
    //   #361	-> 798
    //   #362	-> 806
    //   #363	-> 809
    //   #367	-> 815
    //   #369	-> 820
    //   #370	-> 825
    //   #373	-> 830
    //   #371	-> 833
    //   #372	-> 834
    //   #375	-> 844
    //   #376	-> 844
    //   #343	-> 845
    //   #346	-> 860
    //   #347	-> 869
    //   #348	-> 873
    //   #354	-> 879
    //   #357	-> 890
    //   #358	-> 902
    //   #360	-> 913
    //   #361	-> 924
    //   #362	-> 932
    //   #363	-> 935
    //   #367	-> 941
    //   #369	-> 946
    //   #370	-> 951
    //   #373	-> 956
    //   #371	-> 959
    //   #372	-> 960
    //   #375	-> 970
    //   #272	-> 972
    //   #273	-> 973
    //   #274	-> 982
    // Exception table:
    //   from	to	target	type
    //   12	22	972	java/io/FileNotFoundException
    //   67	74	87	android/system/ErrnoException
    //   74	81	87	android/system/ErrnoException
    //   112	117	717	java/io/IOException
    //   112	117	710	finally
    //   129	134	717	java/io/IOException
    //   129	134	710	finally
    //   146	152	717	java/io/IOException
    //   146	152	710	finally
    //   164	174	717	java/io/IOException
    //   164	174	710	finally
    //   189	196	717	java/io/IOException
    //   189	196	710	finally
    //   213	220	717	java/io/IOException
    //   213	220	710	finally
    //   232	243	717	java/io/IOException
    //   232	243	710	finally
    //   255	265	717	java/io/IOException
    //   255	265	710	finally
    //   273	281	700	java/io/IOException
    //   273	281	685	finally
    //   281	289	449	java/lang/ClassNotFoundException
    //   281	289	389	java/lang/UnsatisfiedLinkError
    //   281	289	295	finally
    //   297	346	700	java/io/IOException
    //   297	346	685	finally
    //   346	354	700	java/io/IOException
    //   346	354	685	finally
    //   354	362	700	java/io/IOException
    //   354	362	685	finally
    //   362	368	700	java/io/IOException
    //   362	368	685	finally
    //   368	383	700	java/io/IOException
    //   368	383	685	finally
    //   383	389	700	java/io/IOException
    //   383	389	685	finally
    //   391	446	700	java/io/IOException
    //   391	446	685	finally
    //   451	489	700	java/io/IOException
    //   451	489	685	finally
    //   489	495	700	java/io/IOException
    //   489	495	685	finally
    //   501	537	700	java/io/IOException
    //   501	537	685	finally
    //   537	565	700	java/io/IOException
    //   537	565	685	finally
    //   565	573	700	java/io/IOException
    //   565	573	685	finally
    //   661	666	674	android/system/ErrnoException
    //   666	671	674	android/system/ErrnoException
    //   724	735	845	finally
    //   820	825	833	android/system/ErrnoException
    //   825	830	833	android/system/ErrnoException
    //   946	951	959	android/system/ErrnoException
    //   951	956	959	android/system/ErrnoException
  }
  
  private static void cacheNonBootClasspathClassLoaders() {
    SharedLibraryInfo sharedLibraryInfo1 = new SharedLibraryInfo("/system/framework/android.hidl.base-V1.0-java.jar", null, null, null, 0L, 0, null, null, null);
    SharedLibraryInfo sharedLibraryInfo2 = new SharedLibraryInfo("/system/framework/android.hidl.manager-V1.0-java.jar", null, null, null, 0L, 0, null, null, null);
    sharedLibraryInfo2.addDependency(sharedLibraryInfo1);
    SharedLibraryInfo sharedLibraryInfo3 = new SharedLibraryInfo("/system/framework/android.test.base.jar", null, null, null, 0L, 0, null, null, null);
    ApplicationLoaders.getDefault().createAndCacheNonBootclasspathSystemClassLoaders(new SharedLibraryInfo[] { sharedLibraryInfo1, sharedLibraryInfo2, sharedLibraryInfo3 });
  }
  
  private static void preloadResources() {
    VMRuntime.getRuntime();
    try {
      Resources resources = Resources.getSystem();
      resources.startPreloading();
      Log.i("Zygote", "Preloading resources...");
      ((IOplusCommonInjector)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusCommonInjector.DEFAULT, new Object[0])).hookPreloadResources(mResources, "Zygote");
      long l = SystemClock.uptimeMillis();
      TypedArray typedArray2 = mResources.obtainTypedArray(17236115);
      int i = preloadDrawables(typedArray2);
      typedArray2.recycle();
      StringBuilder stringBuilder2 = new StringBuilder();
      this();
      stringBuilder2.append("...preloaded ");
      stringBuilder2.append(i);
      stringBuilder2.append(" resources in ");
      stringBuilder2.append(SystemClock.uptimeMillis() - l);
      stringBuilder2.append("ms.");
      String str2 = stringBuilder2.toString();
      Log.i("Zygote", str2);
      l = SystemClock.uptimeMillis();
      TypedArray typedArray1 = mResources.obtainTypedArray(17236114);
      i = preloadColorStateLists(typedArray1);
      typedArray1.recycle();
      StringBuilder stringBuilder1 = new StringBuilder();
      this();
      stringBuilder1.append("...preloaded ");
      stringBuilder1.append(i);
      stringBuilder1.append(" resources in ");
      stringBuilder1.append(SystemClock.uptimeMillis() - l);
      stringBuilder1.append("ms.");
      String str1 = stringBuilder1.toString();
      Log.i("Zygote", str1);
      if (mResources.getBoolean(17891466)) {
        l = SystemClock.uptimeMillis();
        TypedArray typedArray = mResources.obtainTypedArray(17236116);
        i = preloadDrawables(typedArray);
        typedArray.recycle();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("...preloaded ");
        stringBuilder.append(i);
        stringBuilder.append(" resource in ");
        stringBuilder.append(SystemClock.uptimeMillis() - l);
        stringBuilder.append("ms.");
        String str = stringBuilder.toString();
        Log.i("Zygote", str);
      } 
      mResources.finishPreloading();
    } catch (RuntimeException runtimeException) {
      Log.w("Zygote", "Failure preloading resources", runtimeException);
    } 
  }
  
  private static int preloadColorStateLists(TypedArray paramTypedArray) {
    int i = paramTypedArray.length();
    for (byte b = 0; b < i; ) {
      int j = paramTypedArray.getResourceId(b, 0);
      if (j == 0 || 
        mResources.getColorStateList(j, null) != null) {
        b++;
        continue;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to find preloaded color resource #0x");
      stringBuilder.append(Integer.toHexString(j));
      stringBuilder.append(" (");
      stringBuilder.append(paramTypedArray.getString(b));
      stringBuilder.append(")");
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    return i;
  }
  
  private static int preloadDrawables(TypedArray paramTypedArray) {
    int i = paramTypedArray.length();
    for (byte b = 0; b < i; ) {
      int j = paramTypedArray.getResourceId(b, 0);
      if (j == 0 || 
        mResources.getDrawable(j, null) != null) {
        b++;
        continue;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to find preloaded drawable resource #0x");
      stringBuilder.append(Integer.toHexString(j));
      stringBuilder.append(" (");
      stringBuilder.append(paramTypedArray.getString(b));
      stringBuilder.append(")");
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    return i;
  }
  
  private static void gcAndFinalize() {
    ZygoteHooks.gcAndFinalize();
  }
  
  private static boolean shouldProfileSystemServer() {
    boolean bool = SystemProperties.getBoolean("dalvik.vm.profilesystemserver", false);
    return SystemProperties.getBoolean("persist.device_config.runtime_native_boot.profilesystemserver", bool);
  }
  
  private static Runnable handleSystemServerProcess(ZygoteArguments paramZygoteArguments) {
    String str1;
    Log.d("Zygote", "increase system server priority to -15");
    Process.setThreadPriority(-15);
    Os.umask(OsConstants.S_IRWXG | OsConstants.S_IRWXO);
    if (paramZygoteArguments.mNiceName != null)
      Process.setArgV0(paramZygoteArguments.mNiceName); 
    String str2 = Os.getenv("SYSTEMSERVERCLASSPATH");
    if (str2 != null) {
      performSystemServerDexOpt(str2);
      if (shouldProfileSystemServer() && (Build.IS_USERDEBUG || Build.IS_ENG))
        try {
          Log.d("Zygote", "Preparing system server profile");
          prepareSystemServerProfile(str2);
        } catch (Exception exception) {
          Log.wtf("Zygote", "Failed to set up system server profile", exception);
        }  
    } 
    if (paramZygoteArguments.mInvokeWith != null) {
      String[] arrayOfString2 = paramZygoteArguments.mRemainingArgs;
      String[] arrayOfString1 = arrayOfString2;
      if (str2 != null) {
        arrayOfString1 = new String[arrayOfString2.length + 2];
        arrayOfString1[0] = "-cp";
        arrayOfString1[1] = str2;
        System.arraycopy(arrayOfString2, 0, arrayOfString1, 2, arrayOfString2.length);
      } 
      str2 = paramZygoteArguments.mInvokeWith;
      String str = paramZygoteArguments.mNiceName;
      int i = paramZygoteArguments.mTargetSdkVersion;
      str1 = VMRuntime.getCurrentInstructionSet();
      WrapperInit.execApplication(str2, str, i, str1, null, arrayOfString1);
      throw new IllegalStateException("Unexpected return from WrapperInit.execApplication");
    } 
    ClassLoader classLoader = null;
    if (str2 != null) {
      classLoader = createPathClassLoader(str2, ((ZygoteArguments)str1).mTargetSdkVersion);
      Thread.currentThread().setContextClassLoader(classLoader);
    } 
    return zygoteInit(((ZygoteArguments)str1).mTargetSdkVersion, ((ZygoteArguments)str1).mDisabledCompatChanges, ((ZygoteArguments)str1).mRemainingArgs, classLoader);
  }
  
  private static void prepareSystemServerProfile(String paramString) throws RemoteException {
    if (paramString.isEmpty())
      return; 
    String[] arrayOfString = paramString.split(":");
    IInstalld iInstalld = IInstalld.Stub.asInterface(ServiceManager.getService("installd"));
    int i = UserHandle.getAppId(1000);
    String str2 = arrayOfString[0];
    iInstalld.prepareAppProfile("android", 0, i, "primary.prof", str2, null);
    File file = Environment.getDataProfilesDePackageDirectory(0, "android");
    String str1 = (new File(file, "primary.prof")).getAbsolutePath();
    VMRuntime.registerAppInfo(str1, arrayOfString);
  }
  
  public static void setApiBlacklistExemptions(String[] paramArrayOfString) {
    VMRuntime.getRuntime().setHiddenApiExemptions(paramArrayOfString);
  }
  
  public static void setHiddenApiAccessLogSampleRate(int paramInt) {
    VMRuntime.getRuntime().setHiddenApiAccessLogSamplingRate(paramInt);
  }
  
  public static void setHiddenApiUsageLogger(VMRuntime.HiddenApiUsageLogger paramHiddenApiUsageLogger) {
    VMRuntime.getRuntime();
    VMRuntime.setHiddenApiUsageLogger(paramHiddenApiUsageLogger);
  }
  
  static ClassLoader createPathClassLoader(String paramString, int paramInt) {
    String str = System.getProperty("java.library.path");
    ClassLoader classLoader = ClassLoader.getSystemClassLoader().getParent();
    return ClassLoaderFactory.createClassLoader(paramString, str, str, classLoader, paramInt, true, null);
  }
  
  private static void performSystemServerDexOpt(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: ldc ':'
    //   3: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   6: astore_1
    //   7: ldc_w 'installd'
    //   10: invokestatic getService : (Ljava/lang/String;)Landroid/os/IBinder;
    //   13: invokestatic asInterface : (Landroid/os/IBinder;)Landroid/os/IInstalld;
    //   16: astore_2
    //   17: invokestatic getRuntime : ()Ldalvik/system/VMRuntime;
    //   20: invokevirtual vmInstructionSet : ()Ljava/lang/String;
    //   23: astore_3
    //   24: aload_1
    //   25: arraylength
    //   26: istore #4
    //   28: ldc_w ''
    //   31: astore_0
    //   32: iconst_0
    //   33: istore #5
    //   35: iload #5
    //   37: iload #4
    //   39: if_icmpge -> 274
    //   42: aload_1
    //   43: iload #5
    //   45: aaload
    //   46: astore #6
    //   48: ldc_w 'dalvik.vm.systemservercompilerfilter'
    //   51: ldc_w 'verify'
    //   54: invokestatic get : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   57: astore #7
    //   59: aload_0
    //   60: invokestatic getSystemServerClassLoaderContext : (Ljava/lang/String;)Ljava/lang/String;
    //   63: astore #8
    //   65: aload #6
    //   67: aload_3
    //   68: aload #7
    //   70: aload #8
    //   72: iconst_0
    //   73: iconst_0
    //   74: invokestatic getDexOptNeeded : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)I
    //   77: istore #9
    //   79: goto -> 126
    //   82: astore #10
    //   84: new java/lang/StringBuilder
    //   87: dup
    //   88: invokespecial <init> : ()V
    //   91: astore #11
    //   93: aload #11
    //   95: ldc_w 'Error checking classpath element for system server: '
    //   98: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   101: pop
    //   102: aload #11
    //   104: aload #6
    //   106: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   109: pop
    //   110: ldc 'Zygote'
    //   112: aload #11
    //   114: invokevirtual toString : ()Ljava/lang/String;
    //   117: aload #10
    //   119: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   122: pop
    //   123: iconst_0
    //   124: istore #9
    //   126: iload #9
    //   128: ifeq -> 219
    //   131: getstatic android/os/storage/StorageManager.UUID_PRIVATE_INTERNAL : Ljava/lang/String;
    //   134: astore #10
    //   136: aload_2
    //   137: aload #6
    //   139: sipush #1000
    //   142: ldc_w '*'
    //   145: aload_3
    //   146: iload #9
    //   148: aconst_null
    //   149: iconst_0
    //   150: aload #7
    //   152: aload #10
    //   154: aload #8
    //   156: aconst_null
    //   157: iconst_0
    //   158: iconst_0
    //   159: aconst_null
    //   160: aconst_null
    //   161: ldc_w 'server-dexopt'
    //   164: invokeinterface dexopt : (Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   169: goto -> 219
    //   172: astore #7
    //   174: goto -> 177
    //   177: new java/lang/StringBuilder
    //   180: dup
    //   181: invokespecial <init> : ()V
    //   184: astore #8
    //   186: aload #8
    //   188: ldc_w 'Failed compiling classpath element for system server: '
    //   191: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   194: pop
    //   195: aload #8
    //   197: aload #6
    //   199: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   202: pop
    //   203: ldc 'Zygote'
    //   205: aload #8
    //   207: invokevirtual toString : ()Ljava/lang/String;
    //   210: aload #7
    //   212: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   215: pop
    //   216: goto -> 219
    //   219: aload_0
    //   220: aload #6
    //   222: invokestatic encodeSystemServerClassPath : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   225: astore_0
    //   226: goto -> 268
    //   229: astore #7
    //   231: new java/lang/StringBuilder
    //   234: dup
    //   235: invokespecial <init> : ()V
    //   238: astore #7
    //   240: aload #7
    //   242: ldc_w 'Missing classpath element for system server: '
    //   245: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   248: pop
    //   249: aload #7
    //   251: aload #6
    //   253: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   256: pop
    //   257: ldc 'Zygote'
    //   259: aload #7
    //   261: invokevirtual toString : ()Ljava/lang/String;
    //   264: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   267: pop
    //   268: iinc #5, 1
    //   271: goto -> 35
    //   274: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #655	-> 0
    //   #656	-> 7
    //   #657	-> 7
    //   #658	-> 17
    //   #660	-> 24
    //   #661	-> 24
    //   #664	-> 48
    //   #667	-> 59
    //   #668	-> 59
    //   #671	-> 65
    //   #685	-> 79
    //   #678	-> 82
    //   #682	-> 84
    //   #684	-> 123
    //   #687	-> 126
    //   #688	-> 131
    //   #689	-> 131
    //   #690	-> 131
    //   #691	-> 131
    //   #692	-> 131
    //   #693	-> 136
    //   #694	-> 136
    //   #696	-> 136
    //   #705	-> 169
    //   #701	-> 172
    //   #703	-> 177
    //   #687	-> 219
    //   #708	-> 219
    //   #674	-> 229
    //   #676	-> 231
    //   #677	-> 268
    //   #661	-> 268
    //   #711	-> 274
    // Exception table:
    //   from	to	target	type
    //   65	79	229	java/io/FileNotFoundException
    //   65	79	82	java/io/IOException
    //   136	169	172	android/os/RemoteException
    //   136	169	172	android/os/ServiceSpecificException
  }
  
  private static String getSystemServerClassLoaderContext(String paramString) {
    if (paramString == null) {
      paramString = "PCL[]";
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("PCL[");
      stringBuilder.append(paramString);
      stringBuilder.append("]");
      paramString = stringBuilder.toString();
    } 
    return paramString;
  }
  
  private static String encodeSystemServerClassPath(String paramString1, String paramString2) {
    if (paramString1 != null && !paramString1.isEmpty()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(":");
      stringBuilder.append(paramString2);
      paramString2 = stringBuilder.toString();
    } 
    return paramString2;
  }
  
  private static Runnable forkSystemServer(String paramString1, String paramString2, ZygoteServer paramZygoteServer) {
    long l = posixCapabilitiesAsBits(new int[] { 
          OsConstants.CAP_IPC_LOCK, OsConstants.CAP_KILL, OsConstants.CAP_NET_ADMIN, OsConstants.CAP_NET_BIND_SERVICE, OsConstants.CAP_NET_BROADCAST, OsConstants.CAP_NET_RAW, OsConstants.CAP_SYS_MODULE, OsConstants.CAP_SYS_NICE, OsConstants.CAP_SYS_PTRACE, OsConstants.CAP_SYS_TIME, 
          OsConstants.CAP_SYS_TTY_CONFIG, OsConstants.CAP_WAKE_ALARM, OsConstants.CAP_BLOCK_SUSPEND });
    StructCapUserHeader structCapUserHeader = new StructCapUserHeader(OsConstants._LINUX_CAPABILITY_VERSION_3, 0);
    try {
      StructCapUserData[] arrayOfStructCapUserData = Os.capget(structCapUserHeader);
      long l1 = (arrayOfStructCapUserData[0]).effective;
      l1 = ((arrayOfStructCapUserData[1]).effective << 32L | l1) & l;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("--capabilities=");
      stringBuilder.append(l1);
      stringBuilder.append(",");
      stringBuilder.append(l1);
      String str = stringBuilder.toString();
      try {
        ZygoteArguments zygoteArguments = new ZygoteArguments();
        this(new String[] { "--setuid=1000", "--setgid=1000", "--setgroups=1001,1002,1003,1004,1005,1006,1007,1008,1009,1010,1018,1021,1023,1024,1032,1065,3001,3002,3003,3006,3007,3009,3010,3011", str, "--nice-name=system_server", "--runtime-args", "--target-sdk-version=10000", "com.android.server.SystemServer" });
        Zygote.applyDebuggerSystemProperty(zygoteArguments);
        Zygote.applyInvokeWithSystemProperty(zygoteArguments);
        if (Zygote.nativeSupportsTaggedPointers())
          zygoteArguments.mRuntimeFlags |= 0x80000; 
        zygoteArguments.mRuntimeFlags |= 0x200000;
        if (shouldProfileSystemServer())
          zygoteArguments.mRuntimeFlags |= 0x4000; 
        int i = Zygote.forkSystemServer(zygoteArguments.mUid, zygoteArguments.mGid, zygoteArguments.mGids, zygoteArguments.mRuntimeFlags, null, zygoteArguments.mPermittedCapabilities, zygoteArguments.mEffectiveCapabilities);
        if (i == 0) {
          if (hasSecondZygote(paramString1))
            waitForSecondaryZygote(paramString2); 
          paramZygoteServer.closeServerSocket();
          return handleSystemServerProcess(zygoteArguments);
        } 
        return null;
      } catch (IllegalArgumentException illegalArgumentException) {
        throw new RuntimeException(illegalArgumentException);
      } 
    } catch (ErrnoException errnoException) {
      throw new RuntimeException("Failed to capget()", errnoException);
    } 
  }
  
  private static long posixCapabilitiesAsBits(int... paramVarArgs) {
    long l = 0L;
    int i;
    byte b;
    for (i = paramVarArgs.length, b = 0; b < i; ) {
      int j = paramVarArgs[b];
      if (j >= 0 && j <= OsConstants.CAP_LAST_CAP) {
        l |= 1L << j;
        b++;
      } 
      throw new IllegalArgumentException(String.valueOf(j));
    } 
    return l;
  }
  
  public static void main(String[] paramArrayOfString) {
    String str1 = null, str2 = null;
    FileWriter fileWriter = null;
    ZygoteHooks.startZygoteNoThreadCreation();
    try {
      String str;
      Os.setpgid(0, 0);
      if (!Process.is64Bit()) {
        Log.d("Zygote", "Writing 1 to /proc/oplus_healthinfo/vm_search_two_way_enabled");
        try {
          fileWriter = new FileWriter();
          this("/proc/oplus_healthinfo/vm_search_two_way_enabled");
          fileWriter.write("1");
          fileWriter.close();
          Log.d("Zygote", "Done writing /proc/oplus_healthinfo/vm_search_two_way_enabled");
        } catch (Exception exception) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Exception when writing /proc/oplus_healthinfo/vm_search_two_way_enabled, ");
          stringBuilder.append(exception);
          Log.d("Zygote", stringBuilder.toString());
        } 
      } 
      try {
        ZygoteServer zygoteServer;
        long l = SystemClock.elapsedRealtime();
        String str3 = SystemProperties.get("sys.boot_completed");
        boolean bool1 = "1".equals(str3);
        boolean bool2 = Process.is64Bit();
        if (bool2) {
          str3 = "Zygote64Timing";
        } else {
          str3 = "Zygote32Timing";
        } 
        TimingsTraceLog timingsTraceLog = new TimingsTraceLog();
        this(str3, 16384L);
        timingsTraceLog.traceBegin("ZygoteInit");
        RuntimeInit.preForkInit();
        boolean bool3 = false;
        String str4 = "zygote";
        str2 = null;
        boolean bool4 = false;
        byte b = 1;
        String str5 = str3;
        str3 = str1;
      } finally {
        fileWriter = null;
      } 
      try {
        Log.e("Zygote", "System zygote died with exception", (Throwable)fileWriter);
        throw fileWriter;
      } finally {
        if (str != null)
          str.closeServerSocket(); 
      } 
    } catch (ErrnoException errnoException) {
      throw new RuntimeException("Failed to setpgid(0,0)", errnoException);
    } 
  }
  
  private static boolean hasSecondZygote(String paramString) {
    return SystemProperties.get("ro.product.cpu.abilist").equals(paramString) ^ true;
  }
  
  private static void waitForSecondaryZygote(String paramString) {
    String str = "zygote";
    if ("zygote".equals(paramString)) {
      paramString = "zygote_secondary";
    } else {
      paramString = str;
    } 
    ZygoteProcess.waitForConnectionToZygote(paramString);
  }
  
  static boolean isPreloadComplete() {
    return sPreloadComplete;
  }
  
  public static final Runnable zygoteInit(int paramInt, long[] paramArrayOflong, String[] paramArrayOfString, ClassLoader paramClassLoader) {
    Trace.traceBegin(64L, "ZygoteInit");
    RuntimeInit.redirectLogStreams();
    RuntimeInit.commonInit();
    nativeZygoteInit();
    return RuntimeInit.applicationInit(paramInt, paramArrayOflong, paramArrayOfString, paramClassLoader);
  }
  
  static final Runnable childZygoteInit(int paramInt, String[] paramArrayOfString, ClassLoader paramClassLoader) {
    RuntimeInit.Arguments arguments = new RuntimeInit.Arguments(paramArrayOfString);
    return RuntimeInit.findStaticMain(arguments.startClass, arguments.startArgs, paramClassLoader);
  }
  
  private static native void nativePreloadAppProcessHALs();
  
  static native void nativePreloadGraphicsDriver();
  
  private static final native void nativeZygoteInit();
}
