package com.android.internal.os;

import android.os.OplusBaseEnvironment;
import android.os.SystemProperties;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public abstract class OplusBasePowerProfile {
  static final String TAG = "PowerProfile";
  
  FileInputStream fis;
  
  String mProjectPowerProfile;
  
  String mStrProjectVersion = SystemProperties.get("ro.product.prjversion");
  
  XmlPullParser parser;
  
  public OplusBasePowerProfile() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(OplusBaseEnvironment.getOplusProductDirectory().getAbsolutePath());
    stringBuilder.append("/etc/power_profile/power_profile.xml");
    this.mProjectPowerProfile = stringBuilder.toString();
    this.parser = null;
    this.fis = null;
  }
  
  void getOppoPowerProfileXmlParser() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("target project version: ");
    stringBuilder.append(this.mStrProjectVersion);
    stringBuilder.append("  power profile : ");
    stringBuilder.append(this.mProjectPowerProfile);
    Log.i("PowerProfile", stringBuilder.toString());
    if (this.mProjectPowerProfile != null && (new File(this.mProjectPowerProfile)).canRead()) {
      try {
        XmlPullParserFactory xmlPullParserFactory = XmlPullParserFactory.newInstance();
        this.parser = xmlPullParserFactory.newPullParser();
        FileInputStream fileInputStream = new FileInputStream();
        this(this.mProjectPowerProfile);
        this.fis = fileInputStream;
        this.parser.setInput(fileInputStream, "UTF-8");
      } catch (Exception exception) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("access power profile exception caught : ");
        stringBuilder1.append(exception.getMessage());
        Log.d("PowerProfile", stringBuilder1.toString());
        FileInputStream fileInputStream = this.fis;
        if (fileInputStream != null)
          try {
            fileInputStream.close();
            this.fis = null;
          } catch (IOException iOException) {
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("access power profile exception caught : ");
            stringBuilder2.append(iOException.getMessage());
            Log.d("PowerProfile", stringBuilder2.toString());
          }  
        this.parser = null;
      } 
    } else {
      this.parser = null;
    } 
  }
}
