package com.android.internal.os;

import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.EventLog;
import android.util.SparseIntArray;
import com.android.internal.util.Preconditions;
import dalvik.system.VMRuntime;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class BinderInternal {
  private static final String TAG = "BinderInternal";
  
  static final BinderProxyLimitListenerDelegate sBinderProxyLimitListenerDelegate;
  
  static WeakReference<GcWatcher> sGcWatcher = new WeakReference<>(new GcWatcher());
  
  static ArrayList<Runnable> sGcWatchers = new ArrayList<>();
  
  static long sLastGcTime;
  
  static Runnable[] sTmpWatchers = new Runnable[1];
  
  static {
    sBinderProxyLimitListenerDelegate = new BinderProxyLimitListenerDelegate();
  }
  
  static final class GcWatcher {
    protected void finalize() throws Throwable {
      BinderInternal.handleGc();
      BinderInternal.sLastGcTime = SystemClock.uptimeMillis();
      synchronized (BinderInternal.sGcWatchers) {
        BinderInternal.sTmpWatchers = BinderInternal.sGcWatchers.<Runnable>toArray(BinderInternal.sTmpWatchers);
        for (byte b = 0; b < BinderInternal.sTmpWatchers.length; b++) {
          if (BinderInternal.sTmpWatchers[b] != null)
            BinderInternal.sTmpWatchers[b].run(); 
        } 
        BinderInternal.sGcWatcher = new WeakReference<>(new GcWatcher());
        return;
      } 
    }
  }
  
  public static void addGcWatcher(Runnable paramRunnable) {
    synchronized (sGcWatchers) {
      sGcWatchers.add(paramRunnable);
      return;
    } 
  }
  
  public static class CallSession {
    public Class<? extends Binder> binderClass;
    
    long cpuTimeStarted;
    
    boolean exceptionThrown;
    
    long timeStarted;
    
    public int transactionCode;
  }
  
  public static long getLastGcTime() {
    return sLastGcTime;
  }
  
  public static void forceGc(String paramString) {
    EventLog.writeEvent(2741, paramString);
    VMRuntime.getRuntime().requestConcurrentGC();
  }
  
  static void forceBinderGc() {
    forceGc("Binder");
  }
  
  public static void binderProxyLimitCallbackFromNative(int paramInt) {
    sBinderProxyLimitListenerDelegate.notifyClient(paramInt);
  }
  
  public static void setBinderProxyCountCallback(BinderProxyLimitListener paramBinderProxyLimitListener, Handler paramHandler) {
    Preconditions.checkNotNull(paramHandler, "Must provide NonNull Handler to setBinderProxyCountCallback when setting BinderProxyLimitListener");
    sBinderProxyLimitListenerDelegate.setListener(paramBinderProxyLimitListener, paramHandler);
  }
  
  public static void clearBinderProxyCountCallback() {
    sBinderProxyLimitListenerDelegate.setListener(null, null);
  }
  
  public static final native void disableBackgroundScheduling(boolean paramBoolean);
  
  public static final native IBinder getContextObject();
  
  static final native void handleGc();
  
  public static final native void joinThreadPool();
  
  public static final native int nGetBinderProxyCount(int paramInt);
  
  public static final native SparseIntArray nGetBinderProxyPerUidCounts();
  
  public static final native void nSetBinderProxyCountEnabled(boolean paramBoolean);
  
  public static final native void nSetBinderProxyCountWatermarks(int paramInt1, int paramInt2);
  
  public static final native void setMaxThreads(int paramInt);
  
  private static class BinderProxyLimitListenerDelegate {
    private BinderInternal.BinderProxyLimitListener mBinderProxyLimitListener;
    
    private Handler mHandler;
    
    private BinderProxyLimitListenerDelegate() {}
    
    void setListener(BinderInternal.BinderProxyLimitListener param1BinderProxyLimitListener, Handler param1Handler) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: aload_1
      //   4: putfield mBinderProxyLimitListener : Lcom/android/internal/os/BinderInternal$BinderProxyLimitListener;
      //   7: aload_0
      //   8: aload_2
      //   9: putfield mHandler : Landroid/os/Handler;
      //   12: aload_0
      //   13: monitorexit
      //   14: return
      //   15: astore_1
      //   16: aload_0
      //   17: monitorexit
      //   18: aload_1
      //   19: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #257	-> 0
      //   #258	-> 2
      //   #259	-> 7
      //   #260	-> 12
      //   #261	-> 14
      //   #260	-> 15
      // Exception table:
      //   from	to	target	type
      //   2	7	15	finally
      //   7	12	15	finally
      //   12	14	15	finally
      //   16	18	15	finally
    }
    
    void notifyClient(int param1Int) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mBinderProxyLimitListener : Lcom/android/internal/os/BinderInternal$BinderProxyLimitListener;
      //   6: ifnull -> 30
      //   9: aload_0
      //   10: getfield mHandler : Landroid/os/Handler;
      //   13: astore_2
      //   14: new com/android/internal/os/BinderInternal$BinderProxyLimitListenerDelegate$1
      //   17: astore_3
      //   18: aload_3
      //   19: aload_0
      //   20: iload_1
      //   21: invokespecial <init> : (Lcom/android/internal/os/BinderInternal$BinderProxyLimitListenerDelegate;I)V
      //   24: aload_2
      //   25: aload_3
      //   26: invokevirtual post : (Ljava/lang/Runnable;)Z
      //   29: pop
      //   30: aload_0
      //   31: monitorexit
      //   32: return
      //   33: astore_3
      //   34: aload_0
      //   35: monitorexit
      //   36: aload_3
      //   37: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #264	-> 0
      //   #265	-> 2
      //   #266	-> 9
      //   #273	-> 30
      //   #274	-> 32
      //   #273	-> 33
      // Exception table:
      //   from	to	target	type
      //   2	9	33	finally
      //   9	30	33	finally
      //   30	32	33	finally
      //   34	36	33	finally
    }
  }
  
  class null implements Runnable {
    final BinderInternal.BinderProxyLimitListenerDelegate this$0;
    
    final int val$uid;
    
    public void run() {
      this.this$0.mBinderProxyLimitListener.onLimitReached(uid);
    }
  }
  
  public static interface BinderProxyLimitListener {
    void onLimitReached(int param1Int);
  }
  
  public static interface Observer {
    void callEnded(BinderInternal.CallSession param1CallSession, int param1Int1, int param1Int2, int param1Int3);
    
    BinderInternal.CallSession callStarted(Binder param1Binder, int param1Int1, int param1Int2);
    
    void callThrewException(BinderInternal.CallSession param1CallSession, Exception param1Exception);
  }
  
  @FunctionalInterface
  public static interface WorkSourceProvider {
    int resolveWorkSourceUid(int param1Int);
  }
}
