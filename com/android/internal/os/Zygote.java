package com.android.internal.os;

import android.content.pm.ApplicationInfo;
import android.net.Credentials;
import android.net.LocalServerSocket;
import android.net.LocalSocket;
import android.net.NetworkUtils;
import android.os.FactoryTest;
import android.os.Process;
import android.os.SystemProperties;
import android.os.Trace;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.util.Log;
import dalvik.annotation.optimization.FastNative;
import dalvik.system.ZygoteHooks;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import libcore.io.IoUtils;

public final class Zygote {
  private static final String ANDROID_SOCKET_PREFIX = "ANDROID_SOCKET_";
  
  public static final int API_ENFORCEMENT_POLICY_MASK = 12288;
  
  public static final int API_ENFORCEMENT_POLICY_SHIFT = Integer.numberOfTrailingZeros(12288);
  
  public static final String BIND_MOUNT_APP_DATA_DIRS = "--bind-mount-data-dirs";
  
  public static final String BIND_MOUNT_APP_STORAGE_DIRS = "--bind-mount-storage-dirs";
  
  public static final String CHILD_ZYGOTE_ABI_LIST_ARG = "--abi-list=";
  
  public static final String CHILD_ZYGOTE_SOCKET_NAME_ARG = "--zygote-socket=";
  
  public static final String CHILD_ZYGOTE_UID_RANGE_END = "--uid-range-end=";
  
  public static final String CHILD_ZYGOTE_UID_RANGE_START = "--uid-range-start=";
  
  public static final int DEBUG_ALWAYS_JIT = 64;
  
  public static final int DEBUG_ENABLE_ASSERT = 4;
  
  public static final int DEBUG_ENABLE_CHECKJNI = 2;
  
  public static final int DEBUG_ENABLE_JDWP = 1;
  
  public static final int DEBUG_ENABLE_JNI_LOGGING = 16;
  
  public static final int DEBUG_ENABLE_SAFEMODE = 8;
  
  public static final int DEBUG_GENERATE_DEBUG_INFO = 32;
  
  public static final int DEBUG_GENERATE_MINI_DEBUG_INFO = 2048;
  
  public static final int DEBUG_IGNORE_APP_SIGNAL_HANDLER = 131072;
  
  public static final int DEBUG_JAVA_DEBUGGABLE = 256;
  
  public static final int DEBUG_NATIVE_DEBUGGABLE = 128;
  
  public static final int DISABLE_TEST_API_ENFORCEMENT_POLICY = 262144;
  
  public static final int DISABLE_VERIFIER = 512;
  
  public static final int GWP_ASAN_LEVEL_ALWAYS = 4194304;
  
  public static final int GWP_ASAN_LEVEL_LOTTERY = 2097152;
  
  public static final int GWP_ASAN_LEVEL_MASK = 6291456;
  
  public static final int GWP_ASAN_LEVEL_NEVER = 0;
  
  static final int[][] INT_ARRAY_2D = new int[0][0];
  
  public static final int MEMORY_TAG_LEVEL_ASYNC = 1048576;
  
  public static final int MEMORY_TAG_LEVEL_MASK = 1572864;
  
  public static final int MEMORY_TAG_LEVEL_SYNC = 1572864;
  
  public static final int MEMORY_TAG_LEVEL_TBI = 524288;
  
  public static final int MOUNT_EXTERNAL_ANDROID_WRITABLE = 8;
  
  public static final int MOUNT_EXTERNAL_DEFAULT = 1;
  
  public static final int MOUNT_EXTERNAL_FULL = 6;
  
  public static final int MOUNT_EXTERNAL_INSTALLER = 5;
  
  public static final int MOUNT_EXTERNAL_LEGACY = 4;
  
  public static final int MOUNT_EXTERNAL_NONE = 0;
  
  public static final int MOUNT_EXTERNAL_OPLUS_ANDROID_WRITABLE = 9;
  
  public static final int MOUNT_EXTERNAL_PASS_THROUGH = 7;
  
  public static final int MOUNT_EXTERNAL_READ = 2;
  
  public static final int MOUNT_EXTERNAL_WRITE = 3;
  
  public static final int ONLY_USE_SYSTEM_OAT_FILES = 1024;
  
  public static final String PKG_DATA_INFO_MAP = "--pkg-data-info-map";
  
  public static final String PRIMARY_SOCKET_NAME = "zygote";
  
  private static final int PRIORITY_MAX = -20;
  
  public static final int PROFILE_FROM_SHELL = 32768;
  
  public static final int PROFILE_SYSTEM_SERVER = 16384;
  
  public static final long PROPERTY_CHECK_INTERVAL = 60000L;
  
  public static final int RLIMIT_BRK_S2W_MASK = -2147483648;
  
  public static final int RLIMIT_STACK_MASK = 1073741824;
  
  public static final String SECONDARY_SOCKET_NAME = "zygote_secondary";
  
  public static final int SOCKET_BUFFER_SIZE = 256;
  
  public static final String START_AS_TOP_APP_ARG = "--is-top-app";
  
  private static final String USAP_ERROR_PREFIX = "Invalid command to USAP: ";
  
  static final int USAP_MANAGEMENT_MESSAGE_BYTES = 8;
  
  public static final String USAP_POOL_PRIMARY_SOCKET_NAME = "usap_pool_primary";
  
  public static final String USAP_POOL_SECONDARY_SOCKET_NAME = "usap_pool_secondary";
  
  public static final int USE_APP_IMAGE_STARTUP_CACHE = 65536;
  
  public static final String WHITELISTED_DATA_INFO_MAP = "--whitelisted-data-info-map";
  
  private static boolean containsInetGid(int[] paramArrayOfint) {
    for (byte b = 0; b < paramArrayOfint.length; b++) {
      if (paramArrayOfint[b] == 3003)
        return true; 
    } 
    return false;
  }
  
  static int forkAndSpecialize(int paramInt1, int paramInt2, int[] paramArrayOfint1, int paramInt3, int[][] paramArrayOfint, int paramInt4, String paramString1, String paramString2, int[] paramArrayOfint2, int[] paramArrayOfint3, boolean paramBoolean1, String paramString3, String paramString4, boolean paramBoolean2, String[] paramArrayOfString1, String[] paramArrayOfString2, boolean paramBoolean3, boolean paramBoolean4) {
    ZygoteHooks.preFork();
    paramInt1 = nativeForkAndSpecialize(paramInt1, paramInt2, paramArrayOfint1, paramInt3, paramArrayOfint, paramInt4, paramString1, paramString2, paramArrayOfint2, paramArrayOfint3, paramBoolean1, paramString3, paramString4, paramBoolean2, paramArrayOfString1, paramArrayOfString2, paramBoolean3, paramBoolean4);
    if (paramInt1 == 0) {
      Trace.traceBegin(64L, "PostFork");
      if (paramArrayOfint1 != null && paramArrayOfint1.length > 0)
        NetworkUtils.setAllowNetworkingForProcess(containsInetGid(paramArrayOfint1)); 
    } 
    Thread.currentThread().setPriority(5);
    ZygoteHooks.postForkCommon();
    return paramInt1;
  }
  
  private static void specializeAppProcess(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3, int[][] paramArrayOfint1, int paramInt4, String paramString1, String paramString2, boolean paramBoolean1, String paramString3, String paramString4, boolean paramBoolean2, String[] paramArrayOfString1, String[] paramArrayOfString2, boolean paramBoolean3, boolean paramBoolean4) {
    nativeSpecializeAppProcess(paramInt1, paramInt2, paramArrayOfint, paramInt3, paramArrayOfint1, paramInt4, paramString1, paramString2, paramBoolean1, paramString3, paramString4, paramBoolean2, paramArrayOfString1, paramArrayOfString2, paramBoolean3, paramBoolean4);
    Trace.traceBegin(64L, "PostFork");
    Thread.currentThread().setPriority(5);
    ZygoteHooks.postForkCommon();
  }
  
  static int forkSystemServer(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3, int[][] paramArrayOfint1, long paramLong1, long paramLong2) {
    ZygoteHooks.preFork();
    paramInt1 = nativeForkSystemServer(paramInt1, paramInt2, paramArrayOfint, paramInt3, paramArrayOfint1, paramLong1, paramLong2);
    Thread.currentThread().setPriority(5);
    ZygoteHooks.postForkCommon();
    return paramInt1;
  }
  
  static void allowAppFilesAcrossFork(ApplicationInfo paramApplicationInfo) {
    for (String str : paramApplicationInfo.getAllApkPaths())
      nativeAllowFileAcrossFork(str); 
  }
  
  static void initNativeState(boolean paramBoolean) {
    nativeInitNativeState(paramBoolean);
  }
  
  public static String getConfigurationProperty(String paramString1, String paramString2) {
    paramString1 = String.join(".", new CharSequence[] { "persist.device_config", "runtime_native", paramString1 });
    return SystemProperties.get(paramString1, paramString2);
  }
  
  static void emptyUsapPool() {
    nativeEmptyUsapPool();
  }
  
  public static boolean getConfigurationPropertyBoolean(String paramString, Boolean paramBoolean) {
    paramString = String.join(".", new CharSequence[] { "persist.device_config", "runtime_native", paramString });
    boolean bool = paramBoolean.booleanValue();
    return SystemProperties.getBoolean(paramString, bool);
  }
  
  static int getUsapPoolCount() {
    return nativeGetUsapPoolCount();
  }
  
  static FileDescriptor getUsapPoolEventFD() {
    FileDescriptor fileDescriptor = new FileDescriptor();
    fileDescriptor.setInt$(nativeGetUsapPoolEventFD());
    return fileDescriptor;
  }
  
  static Runnable forkUsap(LocalServerSocket paramLocalServerSocket, int[] paramArrayOfint, boolean paramBoolean) {
    try {
      FileDescriptor[] arrayOfFileDescriptor = Os.pipe2(OsConstants.O_CLOEXEC);
      FileDescriptor fileDescriptor = arrayOfFileDescriptor[0];
      int i = nativeForkUsap(fileDescriptor.getInt$(), arrayOfFileDescriptor[1].getInt$(), paramArrayOfint, paramBoolean);
      if (i == 0) {
        IoUtils.closeQuietly(arrayOfFileDescriptor[0]);
        return usapMain(paramLocalServerSocket, arrayOfFileDescriptor[1]);
      } 
      IoUtils.closeQuietly(arrayOfFileDescriptor[1]);
      return null;
    } catch (ErrnoException errnoException) {
      throw new IllegalStateException("Unable to create USAP pipe.", errnoException);
    } 
  }
  
  private static Runnable usapMain(LocalServerSocket paramLocalServerSocket, FileDescriptor paramFileDescriptor) {
    int i = Process.myPid();
    if (Process.is64Bit()) {
      str = "usap64";
    } else {
      str = "usap32";
    } 
    Process.setArgV0(str);
    boostUsapPriority();
    String str = null;
    while (true) {
      Exception exception2;
      try {
        LocalSocket localSocket1 = paramLocalServerSocket.accept(), localSocket2 = localSocket1;
        try {
          blockSigTerm();
          BufferedReader bufferedReader = new BufferedReader();
          InputStreamReader inputStreamReader = new InputStreamReader();
          this(localSocket2.getInputStream());
          this(inputStreamReader);
          DataOutputStream dataOutputStream = new DataOutputStream();
          this(localSocket2.getOutputStream());
          try {
            Credentials credentials = localSocket2.getPeerCredentials();
            try {
              String str1, arrayOfString[] = readArgumentList(bufferedReader);
              if (arrayOfString != null) {
                ZygoteArguments zygoteArguments = new ZygoteArguments();
                this(arrayOfString);
                validateUsapCommand(zygoteArguments);
                try {
                  Exception exception;
                  String[] arrayOfString1;
                  int j = zygoteArguments.mRuntimeFlags;
                  if ((j & 0x40000000) != 0)
                    try {
                      if (!Process.is64Bit())
                        nativeReserveRlimitStack(); 
                    } finally {} 
                  j = zygoteArguments.mRuntimeFlags;
                  if ((j & Integer.MIN_VALUE) != 0)
                    nativeBrkSearchTwoWay(); 
                  setAppProcessName(zygoteArguments, "USAP");
                  applyUidSecurityPolicy(zygoteArguments, credentials);
                  applyDebuggerSystemProperty(zygoteArguments);
                  ArrayList<int[]> arrayList = zygoteArguments.mRLimits;
                  if (arrayList != null) {
                    int[][] arrayOfInt = zygoteArguments.mRLimits.<int[]>toArray(INT_ARRAY_2D);
                  } else {
                    arrayList = null;
                  } 
                  try {
                    dataOutputStream.writeInt(i);
                    IoUtils.closeQuietly((AutoCloseable)localSocket2);
                    try {
                      String str2;
                      Os.close(paramLocalServerSocket.getFileDescriptor());
                      try {
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        this(8);
                        DataOutputStream dataOutputStream1 = new DataOutputStream();
                        this(byteArrayOutputStream);
                        dataOutputStream1.writeLong(i);
                        dataOutputStream1.flush();
                        Os.write(paramFileDescriptor, byteArrayOutputStream.toByteArray(), 0, byteArrayOutputStream.size());
                        IoUtils.closeQuietly(paramFileDescriptor);
                        int k = zygoteArguments.mUid, m = zygoteArguments.mGid, arrayOfInt[] = zygoteArguments.mGids;
                        j = zygoteArguments.mRuntimeFlags;
                        i = zygoteArguments.mMountExternal;
                        str2 = zygoteArguments.mSeInfo;
                        String str3 = zygoteArguments.mNiceName;
                        boolean bool = zygoteArguments.mStartChildZygote;
                        String str4 = zygoteArguments.mInstructionSet;
                        str1 = zygoteArguments.mAppDataDir;
                        try {
                          boolean bool1 = zygoteArguments.mIsTopApp;
                          try {
                            arrayOfString1 = zygoteArguments.mPkgDataInfoList;
                            try {
                              String[] arrayOfString2 = zygoteArguments.mWhitelistedDataInfoList;
                              try {
                                specializeAppProcess(k, m, arrayOfInt, j, (int[][])arrayList, i, str2, str3, bool, str4, str1, bool1, arrayOfString1, arrayOfString2, zygoteArguments.mBindMountAppDataDirs, zygoteArguments.mBindMountAppStorageDirs);
                                Trace.traceEnd(64L);
                                Runnable runnable = ZygoteInit.zygoteInit(zygoteArguments.mTargetSdkVersion, zygoteArguments.mDisabledCompatChanges, zygoteArguments.mRemainingArgs, null);
                                unblockSigTerm();
                                return runnable;
                              } finally {}
                            } finally {}
                          } finally {}
                        } finally {}
                      } catch (Exception null) {
                        try {
                          j = str2.getInt$();
                          String str3 = exception.getMessage();
                          str3 = String.format("Failed to write PID (%d) to pipe (%d): %s", new Object[] { Integer.valueOf(i), Integer.valueOf(j), str3 });
                          Log.e("USAP", str3);
                          exception1 = new RuntimeException();
                          this(exception);
                          throw exception1;
                        } finally {}
                        IoUtils.closeQuietly((FileDescriptor)str2);
                        throw exception;
                      } finally {}
                    } catch (ErrnoException errnoException) {
                      Log.e("USAP", "Failed to close USAP pool socket");
                      exception = new RuntimeException();
                      this((Throwable)errnoException);
                      throw exception;
                    } 
                  } catch (IOException iOException) {
                    try {
                      StringBuilder stringBuilder = new StringBuilder();
                      this();
                      stringBuilder.append("Failed to write response to session socket: ");
                      stringBuilder.append(iOException.getMessage());
                      String str2 = stringBuilder.toString();
                      Log.e("USAP", str2);
                      exception1 = new RuntimeException();
                      this(iOException);
                      throw exception1;
                    } finally {}
                    IoUtils.closeQuietly((AutoCloseable)arrayOfString1);
                    try {
                      Os.close(exception.getFileDescriptor());
                      throw iOException;
                    } catch (ErrnoException errnoException) {}
                    throw errnoException;
                  } finally {}
                } finally {}
                unblockSigTerm();
                throw paramLocalServerSocket;
              } 
              try {
                Log.e("USAP", "Truncated command received.");
                IoUtils.closeQuietly((AutoCloseable)str1);
                unblockSigTerm();
                String str2 = str1;
                continue;
              } catch (Exception null) {}
            } catch (Exception null) {}
          } catch (Exception null) {}
        } catch (Exception exception1) {}
      } catch (Exception exception) {
        exception2 = exception1;
        exception1 = exception;
      } 
      Log.e("USAP", exception1.getMessage());
      IoUtils.closeQuietly((AutoCloseable)exception2);
      unblockSigTerm();
      exception1 = exception2;
    } 
  }
  
  private static void blockSigTerm() {
    nativeBlockSigTerm();
  }
  
  private static void unblockSigTerm() {
    nativeUnblockSigTerm();
  }
  
  private static void boostUsapPriority() {
    nativeBoostUsapPriority();
  }
  
  static void setAppProcessName(ZygoteArguments paramZygoteArguments, String paramString) {
    if (paramZygoteArguments.mNiceName != null) {
      Process.setArgV0(paramZygoteArguments.mNiceName);
    } else if (paramZygoteArguments.mPackageName != null) {
      Process.setArgV0(paramZygoteArguments.mPackageName);
    } else {
      Log.w(paramString, "Unable to set package name.");
    } 
  }
  
  private static void validateUsapCommand(ZygoteArguments paramZygoteArguments) {
    if (!paramZygoteArguments.mAbiListQuery) {
      if (!paramZygoteArguments.mPidQuery) {
        if (!paramZygoteArguments.mPreloadDefault) {
          if (paramZygoteArguments.mPreloadPackage == null) {
            if (paramZygoteArguments.mPreloadApp == null) {
              if (!paramZygoteArguments.mStartChildZygote) {
                if (paramZygoteArguments.mApiBlacklistExemptions == null) {
                  if (paramZygoteArguments.mHiddenApiAccessLogSampleRate == -1) {
                    if (paramZygoteArguments.mHiddenApiAccessStatslogSampleRate == -1) {
                      if (paramZygoteArguments.mInvokeWith == null) {
                        if (paramZygoteArguments.mPermittedCapabilities == 0L && paramZygoteArguments.mEffectiveCapabilities == 0L)
                          return; 
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append("Client may not specify capabilities: permitted=0x");
                        long l = paramZygoteArguments.mPermittedCapabilities;
                        stringBuilder.append(Long.toHexString(l));
                        stringBuilder.append(", effective=0x");
                        l = paramZygoteArguments.mEffectiveCapabilities;
                        stringBuilder.append(Long.toHexString(l));
                        throw new ZygoteSecurityException(stringBuilder.toString());
                      } 
                      throw new IllegalArgumentException("Invalid command to USAP: --invoke-with");
                    } 
                    throw new IllegalArgumentException("Invalid command to USAP: --hidden-api-statslog-sampling-rate=");
                  } 
                  throw new IllegalArgumentException("Invalid command to USAP: --hidden-api-log-sampling-rate=");
                } 
                throw new IllegalArgumentException("Invalid command to USAP: --set-api-blacklist-exemptions");
              } 
              throw new IllegalArgumentException("Invalid command to USAP: --start-child-zygote");
            } 
            throw new IllegalArgumentException("Invalid command to USAP: --preload-app");
          } 
          throw new IllegalArgumentException("Invalid command to USAP: --preload-package");
        } 
        throw new IllegalArgumentException("Invalid command to USAP: --preload-default");
      } 
      throw new IllegalArgumentException("Invalid command to USAP: --get-pid");
    } 
    throw new IllegalArgumentException("Invalid command to USAP: --query-abi-list");
  }
  
  static int[] getUsapPipeFDs() {
    return nativeGetUsapPipeFDs();
  }
  
  static boolean removeUsapTableEntry(int paramInt) {
    return nativeRemoveUsapTableEntry(paramInt);
  }
  
  static void applyUidSecurityPolicy(ZygoteArguments paramZygoteArguments, Credentials paramCredentials) throws ZygoteSecurityException {
    if (paramCredentials.getUid() == 1000) {
      boolean bool;
      if (FactoryTest.getMode() == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      if (bool && paramZygoteArguments.mUidSpecified && paramZygoteArguments.mUid < 1000)
        throw new ZygoteSecurityException("System UID may not launch process with UID < 1000"); 
    } 
    if (!paramZygoteArguments.mUidSpecified) {
      paramZygoteArguments.mUid = paramCredentials.getUid();
      paramZygoteArguments.mUidSpecified = true;
    } 
    if (!paramZygoteArguments.mGidSpecified) {
      paramZygoteArguments.mGid = paramCredentials.getGid();
      paramZygoteArguments.mGidSpecified = true;
    } 
  }
  
  static void applyDebuggerSystemProperty(ZygoteArguments paramZygoteArguments) {
    if (RoSystemProperties.DEBUGGABLE)
      paramZygoteArguments.mRuntimeFlags |= 0x1; 
  }
  
  static void applyInvokeWithSecurityPolicy(ZygoteArguments paramZygoteArguments, Credentials paramCredentials) throws ZygoteSecurityException {
    int i = paramCredentials.getUid();
    if (paramZygoteArguments.mInvokeWith == null || i == 0 || (paramZygoteArguments.mRuntimeFlags & 0x1) != 0)
      return; 
    throw new ZygoteSecurityException("Peer is permitted to specify an explicit invoke-with wrapper command only for debuggable applications.");
  }
  
  public static String getWrapProperty(String paramString) {
    if (paramString == null || paramString.isEmpty())
      return null; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("wrap.");
    stringBuilder.append(paramString);
    paramString = SystemProperties.get(stringBuilder.toString());
    if (paramString != null && !paramString.isEmpty())
      return paramString; 
    return null;
  }
  
  static void applyInvokeWithSystemProperty(ZygoteArguments paramZygoteArguments) {
    if (paramZygoteArguments.mInvokeWith == null)
      paramZygoteArguments.mInvokeWith = getWrapProperty(paramZygoteArguments.mNiceName); 
  }
  
  static String[] readArgumentList(BufferedReader paramBufferedReader) throws IOException {
    try {
      String str = paramBufferedReader.readLine();
      if (str == null)
        return null; 
      int i = Integer.parseInt(str);
      if (i <= 1024) {
        String[] arrayOfString = new String[i];
        for (byte b = 0; b < i; ) {
          arrayOfString[b] = paramBufferedReader.readLine();
          if (arrayOfString[b] != null) {
            b++;
            continue;
          } 
          throw new IOException("Truncated request");
        } 
        return arrayOfString;
      } 
      throw new IOException("Max arg count exceeded");
    } catch (NumberFormatException numberFormatException) {
      Log.e("Zygote", "Invalid Zygote wire format: non-int at argc");
      throw new IOException("Invalid wire format");
    } 
  }
  
  static LocalServerSocket createManagedSocketFromInitSocket(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ANDROID_SOCKET_");
    stringBuilder.append(paramString);
    paramString = stringBuilder.toString();
    try {
      String str = System.getenv(paramString);
      int i = Integer.parseInt(str);
      try {
        FileDescriptor fileDescriptor = new FileDescriptor();
        this();
        fileDescriptor.setInt$(i);
        return new LocalServerSocket(fileDescriptor);
      } catch (IOException iOException) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Error building socket from file descriptor: ");
        stringBuilder1.append(i);
        throw new RuntimeException(stringBuilder1.toString(), iOException);
      } 
    } catch (RuntimeException runtimeException) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Socket unset or invalid: ");
      stringBuilder1.append((String)iOException);
      throw new RuntimeException(stringBuilder1.toString(), runtimeException);
    } 
  }
  
  private static void callPostForkSystemServerHooks(int paramInt) {
    ZygoteHooks.postForkSystemServer(paramInt);
  }
  
  private static void callPostForkChildHooks(int paramInt, boolean paramBoolean1, boolean paramBoolean2, String paramString) {
    ZygoteHooks.postForkChild(paramInt, paramBoolean1, paramBoolean2, paramString);
  }
  
  static void execShell(String paramString) {
    String[] arrayOfString = new String[3];
    arrayOfString[0] = "/system/bin/sh";
    arrayOfString[1] = "-c";
    arrayOfString[2] = paramString;
    try {
      Os.execv(arrayOfString[0], arrayOfString);
      return;
    } catch (ErrnoException errnoException) {
      throw new RuntimeException(errnoException);
    } 
  }
  
  static void appendQuotedShellArgs(StringBuilder paramStringBuilder, String[] paramArrayOfString) {
    int i;
    byte b;
    for (i = paramArrayOfString.length, b = 0; b < i; ) {
      String str = paramArrayOfString[b];
      paramStringBuilder.append(" '");
      paramStringBuilder.append(str.replace("'", "'\\''"));
      paramStringBuilder.append("'");
      b++;
    } 
  }
  
  protected static native void nativeAllowFileAcrossFork(String paramString);
  
  private static native void nativeBlockSigTerm();
  
  private static native void nativeBoostUsapPriority();
  
  private static native void nativeBrkSearchTwoWay();
  
  private static native void nativeEmptyUsapPool();
  
  private static native int nativeForkAndSpecialize(int paramInt1, int paramInt2, int[] paramArrayOfint1, int paramInt3, int[][] paramArrayOfint, int paramInt4, String paramString1, String paramString2, int[] paramArrayOfint2, int[] paramArrayOfint3, boolean paramBoolean1, String paramString3, String paramString4, boolean paramBoolean2, String[] paramArrayOfString1, String[] paramArrayOfString2, boolean paramBoolean3, boolean paramBoolean4);
  
  private static native int nativeForkSystemServer(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3, int[][] paramArrayOfint1, long paramLong1, long paramLong2);
  
  private static native int nativeForkUsap(int paramInt1, int paramInt2, int[] paramArrayOfint, boolean paramBoolean);
  
  private static native int[] nativeGetUsapPipeFDs();
  
  private static native int nativeGetUsapPoolCount();
  
  private static native int nativeGetUsapPoolEventFD();
  
  protected static native void nativeInitNativeState(boolean paramBoolean);
  
  protected static native void nativeInstallSeccompUidGidFilter(int paramInt1, int paramInt2);
  
  @FastNative
  public static native int nativeParseSigChld(byte[] paramArrayOfbyte, int paramInt, int[] paramArrayOfint);
  
  static native void nativePreApplicationInit();
  
  private static native boolean nativeRemoveUsapTableEntry(int paramInt);
  
  private static native void nativeReserveRlimitStack();
  
  private static native void nativeSpecializeAppProcess(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3, int[][] paramArrayOfint1, int paramInt4, String paramString1, String paramString2, boolean paramBoolean1, String paramString3, String paramString4, boolean paramBoolean2, String[] paramArrayOfString1, String[] paramArrayOfString2, boolean paramBoolean3, boolean paramBoolean4);
  
  public static native boolean nativeSupportsTaggedPointers();
  
  private static native void nativeUnblockSigTerm();
}
