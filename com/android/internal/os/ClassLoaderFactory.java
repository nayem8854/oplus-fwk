package com.android.internal.os;

import android.os.Trace;
import dalvik.system.DelegateLastClassLoader;
import dalvik.system.DexClassLoader;
import dalvik.system.PathClassLoader;
import java.util.List;

public class ClassLoaderFactory {
  private static final String DELEGATE_LAST_CLASS_LOADER_NAME;
  
  private static final String DEX_CLASS_LOADER_NAME;
  
  private static final String PATH_CLASS_LOADER_NAME = PathClassLoader.class.getName();
  
  static {
    DEX_CLASS_LOADER_NAME = DexClassLoader.class.getName();
    DELEGATE_LAST_CLASS_LOADER_NAME = DelegateLastClassLoader.class.getName();
  }
  
  public static String getPathClassLoaderName() {
    return PATH_CLASS_LOADER_NAME;
  }
  
  public static boolean isValidClassLoaderName(String paramString) {
    boolean bool;
    if (paramString != null && (isPathClassLoaderName(paramString) || isDelegateLastClassLoaderName(paramString))) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isPathClassLoaderName(String paramString) {
    if (paramString != null && !PATH_CLASS_LOADER_NAME.equals(paramString)) {
      String str = DEX_CLASS_LOADER_NAME;
      return 
        str.equals(paramString);
    } 
    return true;
  }
  
  public static boolean isDelegateLastClassLoaderName(String paramString) {
    return DELEGATE_LAST_CLASS_LOADER_NAME.equals(paramString);
  }
  
  public static ClassLoader createClassLoader(String paramString1, String paramString2, ClassLoader paramClassLoader, String paramString3, List<ClassLoader> paramList) {
    ClassLoader[] arrayOfClassLoader;
    if (paramList == null) {
      paramList = null;
    } else {
      arrayOfClassLoader = paramList.<ClassLoader>toArray(new ClassLoader[paramList.size()]);
    } 
    if (isPathClassLoaderName(paramString3))
      return (ClassLoader)new PathClassLoader(paramString1, paramString2, paramClassLoader, arrayOfClassLoader); 
    if (isDelegateLastClassLoaderName(paramString3))
      return (ClassLoader)new DelegateLastClassLoader(paramString1, paramString2, paramClassLoader, arrayOfClassLoader); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid classLoaderName: ");
    stringBuilder.append(paramString3);
    throw new AssertionError(stringBuilder.toString());
  }
  
  public static ClassLoader createClassLoader(String paramString1, String paramString2, String paramString3, ClassLoader paramClassLoader, int paramInt, boolean paramBoolean, String paramString4) {
    return createClassLoader(paramString1, paramString2, paramString3, paramClassLoader, paramInt, paramBoolean, paramString4, null);
  }
  
  public static ClassLoader createClassLoader(String paramString1, String paramString2, String paramString3, ClassLoader paramClassLoader, int paramInt, boolean paramBoolean, String paramString4, List<ClassLoader> paramList) {
    paramClassLoader = createClassLoader(paramString1, paramString2, paramClassLoader, paramString4, paramList);
    Trace.traceBegin(64L, "createClassloaderNamespace");
    paramString2 = createClassloaderNamespace(paramClassLoader, paramInt, paramString2, paramString3, paramBoolean, paramString1);
    Trace.traceEnd(64L);
    if (paramString2 == null)
      return paramClassLoader; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unable to create namespace for the classloader ");
    stringBuilder.append(paramClassLoader);
    stringBuilder.append(": ");
    stringBuilder.append(paramString2);
    throw new UnsatisfiedLinkError(stringBuilder.toString());
  }
  
  private static native String createClassloaderNamespace(ClassLoader paramClassLoader, int paramInt, String paramString1, String paramString2, boolean paramBoolean, String paramString3);
}
