package com.android.internal.os;

import com.oplus.reflect.MethodParams;
import com.oplus.reflect.RefClass;
import com.oplus.reflect.RefMethod;
import com.oplus.reflect.RefObject;

public class OplusMirrorBatteryStatsImpl {
  public static Class<?> TYPE = RefClass.load(OplusMirrorBatteryStatsImpl.class, BatteryStatsImpl.Uid.Proc.class);
  
  public static RefMethod<Boolean> getHasReportCpuException;
  
  public static RefObject<String> mName;
  
  @MethodParams({boolean.class})
  public static RefMethod<Void> setHasReportCpuException;
}
