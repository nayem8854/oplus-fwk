package com.android.internal.os;

import android.os.BatteryStats;
import android.util.LongSparseArray;

public class MemoryPowerCalculator extends PowerCalculator {
  private static final boolean DEBUG = false;
  
  public static final String TAG = "MemoryPowerCalculator";
  
  private final double[] powerAverages;
  
  public MemoryPowerCalculator(PowerProfile paramPowerProfile) {
    int i = paramPowerProfile.getNumElements("memory.bandwidths");
    this.powerAverages = new double[i];
    for (byte b = 0; b < i; b++) {
      this.powerAverages[b] = paramPowerProfile.getAveragePower("memory.bandwidths", b);
      double d = this.powerAverages[b];
    } 
  }
  
  public void calculateApp(BatterySipper paramBatterySipper, BatteryStats.Uid paramUid, long paramLong1, long paramLong2, int paramInt) {}
  
  public void calculateRemaining(BatterySipper paramBatterySipper, BatteryStats paramBatteryStats, long paramLong1, long paramLong2, int paramInt) {
    double d = 0.0D;
    paramLong2 = 0L;
    LongSparseArray<BatteryStats.Timer> longSparseArray = paramBatteryStats.getKernelMemoryStats();
    for (byte b = 0; b < longSparseArray.size(); ) {
      double[] arrayOfDouble = this.powerAverages;
      if (b < arrayOfDouble.length) {
        double d1 = arrayOfDouble[(int)longSparseArray.keyAt(b)];
        long l = ((BatteryStats.Timer)longSparseArray.valueAt(b)).getTotalTimeLocked(paramLong1, paramInt);
        d1 = l * d1 / 60000.0D;
        d += d1 / 60.0D;
        paramLong2 += l;
        b++;
      } 
    } 
    paramBatterySipper.usagePowerMah = d;
    paramBatterySipper.usageTimeMs = paramLong2;
  }
}
