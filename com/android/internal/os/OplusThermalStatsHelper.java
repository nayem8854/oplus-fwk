package com.android.internal.os;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.OplusBaseBatteryStats;
import android.os.OplusManager;
import android.os.OplusThermalManager;
import android.os.OplusThermalState;
import android.os.Parcel;
import android.os.PowerManager;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.ArrayMap;
import android.util.Slog;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONObject;

class OplusThermalStatsHelper {
  private static final int ALARM_TEMP_UPLOAD = 56;
  
  private static final String ALARM_WAKEUP = "oppo.android.internal.thermalupload.ALARM_WAKEUP";
  
  private static final int AUDIOON_CHECK = 52;
  
  public static final int BATTERY_PLUGGED_NONE = 0;
  
  private static final int CAMERAON_CHECK = 51;
  
  private static final String CHARGE_MAP = "id_charge_map";
  
  private static final int COMMON_WRITE = 63;
  
  private static final int CONNECTTYPE_CHECK = 58;
  
  private static final int CPU_IDLE_CHECK_ENVI_COUNT = 3;
  
  private static final int CPU_IDLE_LESS_LOADING = 100;
  
  public OplusThermalStatsHelper(OplusBaseBatteryStatsImpl paramOplusBaseBatteryStatsImpl, Handler paramHandler, File paramFile1, File paramFile2) {
    this.mBatteryStatsReady = false;
    this.mThermalBuilder = new StringBuilder();
    this.mLock = new Object();
    this.mThermalHistoryBuffer = Parcel.obtain();
    this.mThermalHistoryLastWritten = new OplusBaseBatteryStats.ThermalItem();
    this.mThermalHistoryLastLastWritten = new OplusBaseBatteryStats.ThermalItem();
    this.mThermalHistoryLastRead = new OplusBaseBatteryStats.ThermalItem();
    this.mThermalHistoryLastLastRead = new OplusBaseBatteryStats.ThermalItem();
    this.mStartAnalizyHeat = false;
    this.mHoldHeat = false;
    this.mReceiver = null;
    this.mThermalTempMap = new ArrayMap<>();
    this.mThermalHourMap = new ArrayMap<>();
    this.mTempMonitorAppMap = new ArrayMap<>();
    this.mMonitorAppMap = new ArrayMap<>();
    this.mBatTempMap = new ArrayMap<>();
    this.mTempChargeUploadMap = new ArrayMap<>();
    this.mChargeUploadMap = new ArrayMap<>();
    this.mNetConnectType = -1;
    this.mSystemDir = null;
    this.mMonitorAppLimitTime = 2400000;
    this.mThermalMonitorApp = new ArrayList<>();
    this.mThermalHistoryCur = new OplusBaseBatteryStats.ThermalItem();
    this.mSimpleTopProcessesNeedUpload = "invalid";
    this.mHaveCaptured = false;
    this.mCaptureCpuFeqInterVal = 120000L;
    this.mSimpleTopProInterVal = 120000L;
    this.mThermalHistoryBufferLastPos = -1;
    this.mMoreHeatThreshold = 500;
    this.mHeatThreshold = 450;
    this.mLessHeatThreshold = 420;
    this.mPreHeatThreshold = 400;
    this.mHeatIncRatioThreshold = 10;
    this.mHeatHoldTimeThreshold = 1800000;
    this.mThermalBatteryTemp = true;
    this.mHeatHoldUploadTime = 90000;
    this.mHeatRecInterv = 2;
    this.mCpuLoadRecThreshold = 200;
    this.mCpuLoadRecInterv = 50;
    this.mTopCpuRecThreshold = 50;
    this.mTopCpuRecInterv = 20;
    this.mLastPhoneTemp = -1023;
    this.mLastPhoneTemp1 = -1023;
    this.mLastPhoneTemp2 = -1023;
    this.mLastPhoneTemp3 = -1023;
    this.mHeatIncRatioStartTime = 0L;
    this.mHoldHeatElapsedRealtime = 0L;
    this.mHoldHeatTime = -1;
    this.mCpuIdleCheckCount = 0;
    this.mGlobalBatTemp = 0;
    this.mGlobalPlugType = 0;
    this.mGlobalBatteryRealtimeCapacity = 0;
    this.mGlobalBatteryVoltage = 0;
    this.mGlobalFastCharge = false;
    this.mBatteryFcc = 0;
    this.mGlobalChargeId = 0;
    this.mGlobalFast2Normal = 0;
    this.mLastFast2Normal = 0;
    this.mGlobalScreenBrightnessMode = false;
    this.mGlobalFastCharger = false;
    this.mGlobalMaxPhoneTemp = -1023;
    this.mGlobalMaxBatTemp = -1023;
    this.cpuFreqReader = new OppoCpuFreqReader();
    this.mHeatReasonDetails = new HeatReasonDetails();
    this.mStats = paramOplusBaseBatteryStatsImpl;
    this.mThermalRecFile = paramFile1;
    this.mSystemDir = paramFile2;
    this.mHandler = new WorkHandler(paramHandler.getLooper());
  }
  
  public void setBatteryStatsReady(boolean paramBoolean) {
    this.mBatteryStatsReady = paramBoolean;
  }
  
  public boolean getBatteryStatsReadyStatus() {
    return this.mBatteryStatsReady;
  }
  
  public void onSystemReady(Context paramContext) {
    Slog.d("OppoThermalStats", "onSystemReady.....");
    this.mContext = paramContext;
  }
  
  public void setScreenBrightness(int paramInt) {
    this.mScreenBrightness = paramInt;
  }
  
  public int getScreenBrightness() {
    return this.mScreenBrightness;
  }
  
  public void setConnectyType(int paramInt) {
    this.mNetConnectType = paramInt;
  }
  
  public int getConnectyType() {
    return this.mNetConnectType;
  }
  
  static boolean DEBUG_THERMAL_TEMP = false;
  
  private static final int DELAY_CHECK = 4000;
  
  private static final String EXTRA_VOLUME_STREAM_TYPE = "android.media.EXTRA_VOLUME_STREAM_TYPE";
  
  private static final String EXTRA_VOLUME_STREAM_VALUE = "android.media.EXTRA_VOLUME_STREAM_VALUE";
  
  private static final int FLASHLIGHTON_CHECK = 55;
  
  private static final int GPSON_CHECK = 54;
  
  private static final String HEAT_LOG_ID = "040201";
  
  private static final int HEAT_REASON_ANALIZY = 59;
  
  private static final int INIT_THERMAL_PAR = 62;
  
  private static final int INVALID_DATA = -1023;
  
  private static final int MAX_HEAT_ANALIZY_SIZE = 400;
  
  private static final int MAX_HISTORY_BUFFER = 131072;
  
  static final int MSG_REPORT_UPDATE_CPU = 5;
  
  private static final int RESET_ALARM = 60;
  
  private static final int SYNC_TO_THERMAL_FILE = 64;
  
  public static final String TAG = "OppoThermalStats";
  
  public static final int THERMAL_EVENT_AUDIO = 3;
  
  public static final int THERMAL_EVENT_BASE = 1;
  
  public static final int THERMAL_EVENT_CAMERA = 2;
  
  public static final int THERMAL_EVENT_FLASH_LIGHT = 6;
  
  public static final int THERMAL_EVENT_GPS = 5;
  
  public static final int THERMAL_EVENT_VIDEO = 4;
  
  private static final String THERMAL_HEAT_EVENT = "id_thermal_heat";
  
  private static final String THERMAL_INFO_DCS = "/data/oppo/psw/dcs/";
  
  private static final String THERMAL_MONITOR_APP = "id_thermal_monitor_app";
  
  private static final String THERMAL_TAG = "20139";
  
  private static final String THERMAL_TEMP_EVENT = "id_thermal_temp";
  
  private static final int UPDATE_BRIGHTNESS = 57;
  
  private static final int UPDATE_VOLUME = 61;
  
  private static final int VIDEOON_CHECK = 53;
  
  private static final String VOLUME_CHANGED_ACTION = "android.media.VOLUME_CHANGED_ACTION";
  
  private static final String WAKELOCK_KEY = "thermalUpload";
  
  private OppoCpuFreqReader cpuFreqReader;
  
  private boolean isThermalFreatureOn;
  
  private AlarmManager mAlarmManager;
  
  private Map<String, Long> mBatTempMap;
  
  int mBatteryFcc;
  
  private boolean mBatteryStatsReady;
  
  private long mCaptureCpuFeqElapsRealtime;
  
  private long mCaptureCpuFeqInterVal;
  
  private Map<String, String> mChargeUploadMap;
  
  private Context mContext;
  
  private String mCpuFreqValues;
  
  private String mCpuFreqValuesNeedUpload;
  
  int mCpuIdleCheckCount;
  
  int mCpuLoadRecInterv;
  
  int mCpuLoadRecThreshold;
  
  int mGlobalBatTemp;
  
  int mGlobalBatteryCurrent;
  
  int mGlobalBatteryRealtimeCapacity;
  
  int mGlobalBatteryVoltage;
  
  int mGlobalChargeId;
  
  int mGlobalFast2Normal;
  
  boolean mGlobalFastCharge;
  
  boolean mGlobalFastCharger;
  
  int mGlobalMaxBatTemp;
  
  int mGlobalMaxPhoneTemp;
  
  int mGlobalPlugType;
  
  boolean mGlobalScreenBrightnessMode;
  
  int mGlobalVolumeLevel;
  
  private Handler mHandler;
  
  private boolean mHaveCaptured;
  
  int mHeatHoldTimeThreshold;
  
  int mHeatHoldUploadTime;
  
  long mHeatIncRatioStartTime;
  
  int mHeatIncRatioThreshold;
  
  private HeatReasonDetails mHeatReasonDetails;
  
  int mHeatRecInterv;
  
  int mHeatThreshold;
  
  private boolean mHoldHeat;
  
  long mHoldHeatElapsedRealtime;
  
  int mHoldHeatTime;
  
  private boolean mIteratingThermalHistory;
  
  int mLastFast2Normal;
  
  int mLastPhoneTemp;
  
  int mLastPhoneTemp1;
  
  int mLastPhoneTemp2;
  
  int mLastPhoneTemp3;
  
  int mLessHeatThreshold;
  
  private final Object mLock;
  
  private int mMonitorAppLimitTime;
  
  private Map<String, String> mMonitorAppMap;
  
  int mMoreHeatThreshold;
  
  private int mNetConnectType;
  
  int mNumThermalHistoryItems;
  
  private PackageManager mPackageManger;
  
  private PowerManager mPowerManager;
  
  int mPreHeatThreshold;
  
  private ThermalReceiver mReceiver;
  
  boolean mRecordThermalHistory;
  
  private int mScreenBrightness;
  
  private long mSimpleTopProInterVal;
  
  private String mSimpleTopProcesses;
  
  private String mSimpleTopProcessesNeedUpload;
  
  private boolean mStartAnalizyHeat;
  
  private final OplusBaseBatteryStatsImpl mStats;
  
  private File mSystemDir;
  
  private Map<String, Long> mTempChargeUploadMap;
  
  private Map<String, Long> mTempMonitorAppMap;
  
  boolean mThermalBatteryTemp;
  
  private final StringBuilder mThermalBuilder;
  
  boolean mThermalCaptureLog;
  
  int mThermalCaptureLogThreshold;
  
  boolean mThermalFeatureOn;
  
  private OplusBaseBatteryStats.ThermalItem mThermalHistory;
  
  private final Parcel mThermalHistoryBuffer;
  
  int mThermalHistoryBufferLastPos;
  
  final OplusBaseBatteryStats.ThermalItem mThermalHistoryCur;
  
  private final OplusBaseBatteryStats.ThermalItem mThermalHistoryLastLastRead;
  
  private final OplusBaseBatteryStats.ThermalItem mThermalHistoryLastLastWritten;
  
  private final OplusBaseBatteryStats.ThermalItem mThermalHistoryLastRead;
  
  private final OplusBaseBatteryStats.ThermalItem mThermalHistoryLastWritten;
  
  private Map<String, Integer> mThermalHourMap;
  
  ArrayList<String> mThermalMonitorApp;
  
  private final File mThermalRecFile;
  
  private Map<String, Long> mThermalTempMap;
  
  boolean mThermalUploadDcs;
  
  boolean mThermalUploadErrLog;
  
  boolean mThermalUploadLog;
  
  int mTopCpuRecInterv;
  
  int mTopCpuRecThreshold;
  
  private PowerManager.WakeLock mWakeLock;
  
  private PendingIntent mWakeupIntent;
  
  private class HeatReasonDetails {
    public int mAnalizyPosition = 0;
    
    public int mPhoneTemp = 0;
    
    public int mBatTemp = 0;
    
    private boolean mPlug = false;
    
    private int mTotalTime = -1;
    
    private int mAudioTime = 0;
    
    private int mCameraTime = 0;
    
    private int mVideoTime = 0;
    
    private int mGpsTime = 0;
    
    private int mFlashlightTime = 0;
    
    private int mPhoneOnTime = 0;
    
    private int mPhoneSignal = 0;
    
    private int mPhoneSignalCount = 0;
    
    private int mWifiTime = 0;
    
    private int mWifiStrenth = 0;
    
    private int mWifiStrenthCount = 0;
    
    private int mNet2GTime = 0;
    
    private int mNet3GTime = 0;
    
    private int mNet4GTime = 0;
    
    private int mCpuLoadingCount = 0;
    
    private int mCpuLoading = 0;
    
    private int mBackLight = 0;
    
    private int mBackLightCount = 0;
    
    private int mJobTime = 0;
    
    private int mMaxJobTime = 0;
    
    private String mMaxJobProc = "null";
    
    private int mSyncTime = 0;
    
    private int mMaxSyncTime = 0;
    
    private String mMaxSyncProc = "null";
    
    private int mMaxForeProcTime = 0;
    
    private String mMaxForeProc = "null";
    
    private int mMaxTopCpuRatio = 0;
    
    private String mMaxTopCpuProc = "null";
    
    private int mLongTopCpuTime = 0;
    
    private String mLongTopCpuProc = "null";
    
    private int mHeartReason = -1;
    
    private int mTemp0 = -1023;
    
    private int mTemp1 = -1023;
    
    private int mHeatRatio = -1023;
    
    private int mEnviTemp = -1023;
    
    private int mBatRm0 = -1;
    
    private int mBatRm1 = -1;
    
    private int mPlugNoneTime = 0;
    
    private int mPlugUsbTime = 0;
    
    private int mPlugAcTime = 0;
    
    private int mPlugWireTime = 0;
    
    private String mVersionName = "";
    
    private OplusBaseBatteryStats.ThermalItem[] mAnalizyHeatArray = new OplusBaseBatteryStats.ThermalItem[400];
    
    private HashMap<Integer, Integer> mPlugTimeMap = new HashMap<>();
    
    private HashMap<String, Integer> mJobProcTimeMap = new HashMap<>();
    
    private HashMap<String, Integer> mSyncProcTimeMap = new HashMap<>();
    
    private HashMap<String, Integer> mForeProcTimeMap = new HashMap<>();
    
    private HashMap<String, Integer> mTopCpuProcTimeMap = new HashMap<>();
    
    private HashMap<String, Integer> mTopCpuProcRatioMap = new HashMap<>();
    
    private OplusBaseBatteryStats.ThermalItem mLastAnalizyItem = new OplusBaseBatteryStats.ThermalItem();
    
    Map<String, String> mUpLoadMap = new HashMap<>();
    
    private boolean mIsUploadHeat = false;
    
    final OplusThermalStatsHelper this$0;
    
    public HeatReasonDetails() {
      if (this.mAnalizyHeatArray == null)
        this.mAnalizyHeatArray = new OplusBaseBatteryStats.ThermalItem[400]; 
      for (byte b = 0; b < this.mAnalizyHeatArray.length; b++) {
        OplusBaseBatteryStats.ThermalItem thermalItem = new OplusBaseBatteryStats.ThermalItem();
        this.mAnalizyHeatArray[b] = thermalItem;
      } 
    }
    
    public void addToHeatItem(OplusBaseBatteryStats.ThermalItem param1ThermalItem) {
      // Byte code:
      //   0: getstatic com/android/internal/os/OplusThermalStatsHelper.DEBUG_THERMAL_TEMP : Z
      //   3: ifeq -> 59
      //   6: new java/lang/StringBuilder
      //   9: dup
      //   10: invokespecial <init> : ()V
      //   13: astore_2
      //   14: aload_2
      //   15: ldc_w 'addToHeatItem:'
      //   18: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   21: pop
      //   22: aload_2
      //   23: aload_1
      //   24: getfield cmd : B
      //   27: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   30: pop
      //   31: aload_2
      //   32: ldc_w ' mAnalizyPosition='
      //   35: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   38: pop
      //   39: aload_2
      //   40: aload_0
      //   41: getfield mAnalizyPosition : I
      //   44: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   47: pop
      //   48: ldc_w 'OppoThermalStats'
      //   51: aload_2
      //   52: invokevirtual toString : ()Ljava/lang/String;
      //   55: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   58: pop
      //   59: aload_0
      //   60: getfield mAnalizyPosition : I
      //   63: istore_3
      //   64: iload_3
      //   65: sipush #400
      //   68: irem
      //   69: istore #4
      //   71: iload_3
      //   72: sipush #399
      //   75: if_icmpgt -> 90
      //   78: iload_3
      //   79: iflt -> 90
      //   82: iload #4
      //   84: istore_3
      //   85: iload #4
      //   87: ifge -> 148
      //   90: new java/lang/StringBuilder
      //   93: dup
      //   94: invokespecial <init> : ()V
      //   97: astore_2
      //   98: aload_2
      //   99: ldc_w 'AnalizyPosition reach max limit, mAnalizyPosition='
      //   102: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   105: pop
      //   106: aload_2
      //   107: aload_0
      //   108: getfield mAnalizyPosition : I
      //   111: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   114: pop
      //   115: aload_2
      //   116: ldc_w ' res='
      //   119: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   122: pop
      //   123: aload_2
      //   124: iload #4
      //   126: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   129: pop
      //   130: ldc_w 'OppoThermalStats'
      //   133: aload_2
      //   134: invokevirtual toString : ()Ljava/lang/String;
      //   137: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   140: pop
      //   141: aload_0
      //   142: iconst_0
      //   143: putfield mAnalizyPosition : I
      //   146: iconst_0
      //   147: istore_3
      //   148: aload_0
      //   149: getfield mAnalizyHeatArray : [Landroid/os/OplusBaseBatteryStats$ThermalItem;
      //   152: iload_3
      //   153: aaload
      //   154: aload_1
      //   155: invokevirtual setTo : (Landroid/os/OplusBaseBatteryStats$ThermalItem;)V
      //   158: aload_0
      //   159: aload_0
      //   160: getfield mAnalizyPosition : I
      //   163: iconst_1
      //   164: iadd
      //   165: putfield mAnalizyPosition : I
      //   168: goto -> 176
      //   171: astore_1
      //   172: aload_1
      //   173: invokevirtual printStackTrace : ()V
      //   176: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #303	-> 0
      //   #304	-> 6
      //   #306	-> 59
      //   #307	-> 71
      //   #308	-> 90
      //   #309	-> 141
      //   #312	-> 148
      //   #313	-> 158
      //   #316	-> 168
      //   #314	-> 171
      //   #315	-> 172
      //   #317	-> 176
      // Exception table:
      //   from	to	target	type
      //   148	158	171	java/lang/Exception
      //   158	168	171	java/lang/Exception
    }
    
    public void clear() {
      this.mPlug = false;
      this.mTotalTime = 0;
      this.mAudioTime = 0;
      this.mCameraTime = 0;
      this.mVideoTime = 0;
      this.mGpsTime = 0;
      this.mFlashlightTime = 0;
      this.mPhoneSignal = 0;
      this.mPhoneSignalCount = 0;
      this.mWifiTime = 0;
      this.mWifiStrenth = 0;
      this.mWifiStrenthCount = 0;
      this.mNet2GTime = 0;
      this.mNet3GTime = 0;
      this.mNet4GTime = 0;
      this.mCpuLoadingCount = 0;
      this.mCpuLoading = 0;
      this.mBackLight = 0;
      this.mBackLightCount = 0;
      this.mJobTime = 0;
      this.mMaxJobTime = 0;
      this.mMaxJobProc = "null";
      this.mSyncTime = 0;
      this.mMaxSyncTime = 0;
      this.mMaxSyncProc = "null";
      this.mMaxForeProcTime = 0;
      this.mMaxForeProc = "null";
      this.mMaxTopCpuRatio = 0;
      this.mMaxTopCpuProc = "null";
      this.mLongTopCpuTime = 0;
      this.mLongTopCpuProc = "null";
      this.mHeartReason = -1;
      this.mTemp0 = -1023;
      this.mTemp1 = -1023;
      this.mHeatRatio = -127;
      this.mEnviTemp = -1023;
      this.mBatRm0 = -1;
      this.mBatRm1 = -1;
      this.mPlugNoneTime = 0;
      this.mPlugUsbTime = 0;
      this.mPlugAcTime = 0;
      this.mPlugWireTime = 0;
      this.mVersionName = "";
      this.mPlugTimeMap.clear();
      this.mJobProcTimeMap.clear();
      this.mSyncProcTimeMap.clear();
      this.mForeProcTimeMap.clear();
      this.mTopCpuProcTimeMap.clear();
      this.mTopCpuProcRatioMap.clear();
      this.mLastAnalizyItem.clear();
      this.mUpLoadMap.clear();
      this.mIsUploadHeat = false;
    }
    
    public boolean hasCode() {
      Map<String, String> map = this.mUpLoadMap;
      if (map != null && map.size() > 0)
        return true; 
      return false;
    }
    
    private void addTotalTime(int param1Int) {
      this.mTotalTime += param1Int;
    }
    
    private void addAudioTime(int param1Int) {
      this.mAudioTime += param1Int;
    }
    
    private int getAudioTimeThousandths() {
      int i = this.mTotalTime;
      if (i == 0) {
        i = 0;
      } else {
        i = this.mAudioTime * 1000 / i;
      } 
      return i;
    }
    
    private void addCameraTime(int param1Int) {
      this.mCameraTime += param1Int;
    }
    
    private int getCameraTimeThousandths() {
      int i = this.mTotalTime;
      if (i == 0) {
        i = 0;
      } else {
        i = this.mCameraTime * 1000 / i;
      } 
      return i;
    }
    
    private void addVideoTime(int param1Int) {
      this.mVideoTime += param1Int;
    }
    
    private int getVideoTimeThousandths() {
      int i = this.mTotalTime;
      if (i == 0) {
        i = 0;
      } else {
        i = this.mVideoTime * 1000 / i;
      } 
      return i;
    }
    
    private void addGpsTime(int param1Int) {
      this.mGpsTime += param1Int;
    }
    
    private int getGpsTimeThousandths() {
      int i = this.mTotalTime;
      if (i == 0) {
        i = 0;
      } else {
        i = this.mGpsTime * 1000 / i;
      } 
      return i;
    }
    
    private void addFlashlightTime(int param1Int) {
      this.mFlashlightTime += param1Int;
    }
    
    private int getFlashlightTimeThousandths() {
      int i = this.mTotalTime;
      if (i == 0) {
        i = 0;
      } else {
        i = this.mFlashlightTime * 1000 / i;
      } 
      return i;
    }
    
    private void addPhoneOnTime(int param1Int) {
      this.mPhoneOnTime += param1Int;
    }
    
    private int getPhoneOnTimeThousandths() {
      int i = this.mTotalTime;
      if (i == 0) {
        i = 0;
      } else {
        i = this.mPhoneOnTime * 1000 / i;
      } 
      return i;
    }
    
    private void addPhoneSignal(int param1Int) {
      this.mPhoneSignal += param1Int;
      this.mPhoneSignalCount++;
    }
    
    private int getAvgPhoneSignal() {
      int i = this.mPhoneSignalCount;
      if (i == 0) {
        i = 0;
      } else {
        i = this.mPhoneSignal / i;
      } 
      return i;
    }
    
    private void addWifiTime(int param1Int) {
      this.mWifiTime += param1Int;
    }
    
    private int getWifiTimeThousandths() {
      int i = this.mTotalTime;
      if (i == 0) {
        i = 0;
      } else {
        i = this.mWifiTime * 1000 / i;
      } 
      return i;
    }
    
    private void addWifiStrenth(int param1Int) {
      this.mWifiStrenth += param1Int;
      this.mWifiStrenthCount++;
    }
    
    private int getAvgWifiStrenth() {
      int i = this.mWifiStrenthCount;
      if (i == 0) {
        i = 0;
      } else {
        i = this.mWifiStrenth / i;
      } 
      return i;
    }
    
    private void addNet2GTime(int param1Int) {
      this.mNet2GTime += param1Int;
    }
    
    private int getNet2GTimeThousandths() {
      int i = this.mTotalTime;
      if (i == 0) {
        i = 0;
      } else {
        i = this.mNet2GTime * 1000 / i;
      } 
      return i;
    }
    
    private void addNet3GTime(int param1Int) {
      this.mNet3GTime += param1Int;
    }
    
    private int getNet3GTimeThousandths() {
      int i = this.mTotalTime;
      if (i == 0) {
        i = 0;
      } else {
        i = this.mNet3GTime * 1000 / i;
      } 
      return i;
    }
    
    private void addNet4GTime(int param1Int) {
      this.mNet4GTime += param1Int;
    }
    
    private int getNet4GTimeThousandths() {
      int i = this.mTotalTime;
      if (i == 0) {
        i = 0;
      } else {
        i = this.mNet4GTime * 1000 / i;
      } 
      return i;
    }
    
    private void addCpuLoading(int param1Int) {
      this.mCpuLoadingCount++;
      this.mCpuLoading += param1Int;
    }
    
    private int getAvgCpuLoading() {
      int i = this.mCpuLoadingCount;
      if (i == 0) {
        i = 0;
      } else {
        i = this.mCpuLoading / i;
      } 
      return i;
    }
    
    private void addBackLight(int param1Int) {
      this.mBackLight += param1Int;
      this.mBackLightCount++;
    }
    
    private int getAvgBackLight() {
      int i = this.mBackLightCount;
      if (i == 0) {
        i = 0;
      } else {
        i = this.mBackLight / i;
      } 
      return i;
    }
    
    private void setEnviTmep(int param1Int) {
      this.mEnviTemp = param1Int;
    }
    
    private int getEnviTmep() {
      return this.mEnviTemp;
    }
    
    private void setBatRm0(int param1Int) {
      this.mBatRm0 = param1Int;
    }
    
    private void setBatRm1(int param1Int) {
      this.mBatRm1 = param1Int;
    }
    
    private int getCurrent() {
      int i = this.mBatRm0, j = 9999;
      if (i != -1) {
        int k = this.mBatRm1;
        if (k != -1 && i > k && getPlugNoneTimeThousandths() > 950) {
          k = this.mTotalTime;
          if (k != 0)
            j = (this.mBatRm0 - this.mBatRm1) * 3600000 / k; 
          return j;
        } 
      } 
      return 9999;
    }
    
    private void setTemp0(int param1Int) {
      this.mTemp0 = param1Int;
    }
    
    private void setTemp1(int param1Int) {
      this.mTemp1 = param1Int;
    }
    
    private int getHeatRatio() {
      int i = this.mTemp1, j = 9999;
      if (i != -1023) {
        int k = this.mTemp0;
        if (k != -1023 && i > k) {
          int m = this.mTotalTime;
          if (m != 0)
            j = (i - k) * 60000 / m; 
          return j;
        } 
      } 
      return 9999;
    }
    
    private void putPlugTypeAndTime(int param1Int1, int param1Int2) {
      int i = param1Int1;
      if (param1Int1 != 1) {
        i = param1Int1;
        if (param1Int1 != 2) {
          i = param1Int1;
          if (param1Int1 != 4)
            i = 0; 
        } 
      } 
      if (this.mPlugTimeMap.containsKey(Integer.valueOf(i))) {
        param1Int1 = ((Integer)this.mPlugTimeMap.get(Integer.valueOf(i))).intValue();
        this.mPlugTimeMap.put(Integer.valueOf(i), Integer.valueOf(param1Int2 + param1Int1));
      } else {
        this.mPlugTimeMap.put(Integer.valueOf(i), Integer.valueOf(param1Int2));
      } 
    }
    
    private int getPlugNoneTimeThousandths() {
      int i = 0;
      HashMap<Integer, Integer> hashMap = this.mPlugTimeMap;
      boolean bool = false;
      Integer integer = Integer.valueOf(0);
      if (hashMap.containsKey(integer))
        i = ((Integer)this.mPlugTimeMap.get(integer)).intValue(); 
      int j = this.mTotalTime;
      if (j == 0) {
        i = bool;
      } else {
        i = i * 1000 / j;
      } 
      return i;
    }
    
    private int getPlugUsbTimeThousandths() {
      int i = 0;
      HashMap<Integer, Integer> hashMap = this.mPlugTimeMap;
      Integer integer = Integer.valueOf(2);
      if (hashMap.containsKey(integer))
        i = ((Integer)this.mPlugTimeMap.get(integer)).intValue(); 
      int j = this.mTotalTime;
      if (j == 0) {
        i = 0;
      } else {
        i = i * 1000 / j;
      } 
      return i;
    }
    
    private int getPlugAcTimeThousandths() {
      int i = 0;
      HashMap<Integer, Integer> hashMap = this.mPlugTimeMap;
      Integer integer = Integer.valueOf(1);
      if (hashMap.containsKey(integer))
        i = ((Integer)this.mPlugTimeMap.get(integer)).intValue(); 
      int j = this.mTotalTime;
      if (j == 0) {
        i = 0;
      } else {
        i = i * 1000 / j;
      } 
      return i;
    }
    
    private int getPlugWireTimeThousandths() {
      int i = 0;
      HashMap<Integer, Integer> hashMap = this.mPlugTimeMap;
      Integer integer = Integer.valueOf(4);
      if (hashMap.containsKey(integer))
        i = ((Integer)this.mPlugTimeMap.get(integer)).intValue(); 
      int j = this.mTotalTime;
      if (j == 0) {
        i = 0;
      } else {
        i = i * 1000 / j;
      } 
      return i;
    }
    
    private void putJobProcAndTime(String param1String, int param1Int) {
      if (this.mJobProcTimeMap.containsKey(param1String)) {
        int i = ((Integer)this.mJobProcTimeMap.get(param1String)).intValue();
        this.mJobProcTimeMap.put(param1String, Integer.valueOf(param1Int + i));
      } else {
        this.mJobProcTimeMap.put(param1String, Integer.valueOf(param1Int));
      } 
      this.mJobTime += param1Int;
      try {
        for (Map.Entry<String, Integer> entry : this.mJobProcTimeMap.entrySet()) {
          String str = (String)entry.getKey();
          param1Int = ((Integer)entry.getValue()).intValue();
          if (this.mMaxJobTime < param1Int) {
            this.mMaxJobTime = param1Int;
            this.mMaxJobProc = str;
          } 
        } 
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
    }
    
    private int getJobTimeThousandths() {
      int i = this.mTotalTime;
      if (i == 0) {
        i = 0;
      } else {
        i = this.mJobTime * 1000 / i;
      } 
      return i;
    }
    
    private int getMaxJobTimeThousandths() {
      int i = this.mTotalTime;
      if (i == 0) {
        i = 0;
      } else {
        i = this.mMaxJobTime * 1000 / i;
      } 
      return i;
    }
    
    private String getMaxJobProc() {
      return this.mMaxJobProc;
    }
    
    private void putSyncProcAndTime(String param1String, int param1Int) {
      if (this.mSyncProcTimeMap.containsKey(param1String)) {
        int i = ((Integer)this.mSyncProcTimeMap.get(param1String)).intValue();
        this.mSyncProcTimeMap.put(param1String, Integer.valueOf(param1Int + i));
      } else {
        this.mSyncProcTimeMap.put(param1String, Integer.valueOf(param1Int));
      } 
      this.mSyncTime += param1Int;
      try {
        for (Map.Entry<String, Integer> entry : this.mSyncProcTimeMap.entrySet()) {
          param1String = (String)entry.getKey();
          param1Int = ((Integer)entry.getValue()).intValue();
          if (this.mMaxSyncTime < param1Int) {
            this.mMaxSyncTime = param1Int;
            this.mMaxSyncProc = param1String;
          } 
        } 
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
    }
    
    private int getSyncTimeThousandths() {
      int i = this.mTotalTime;
      if (i == 0) {
        i = 0;
      } else {
        i = this.mSyncTime * 1000 / i;
      } 
      return i;
    }
    
    private int getMaxSyncTimeThousandths() {
      int i = this.mTotalTime;
      if (i == 0) {
        i = 0;
      } else {
        i = this.mMaxSyncTime * 1000 / i;
      } 
      return i;
    }
    
    private String getMaxSyncProc() {
      return this.mMaxSyncProc;
    }
    
    private void putForeProcAndTime(String param1String1, String param1String2, int param1Int) {
      if (this.mForeProcTimeMap.containsKey(param1String2)) {
        int i = ((Integer)this.mForeProcTimeMap.get(param1String2)).intValue();
        this.mForeProcTimeMap.put(param1String2, Integer.valueOf(param1Int + i));
      } else {
        this.mForeProcTimeMap.put(param1String2, Integer.valueOf(param1Int));
      } 
      try {
        for (Map.Entry<String, Integer> entry : this.mForeProcTimeMap.entrySet()) {
          String str = (String)entry.getKey();
          param1Int = ((Integer)entry.getValue()).intValue();
          if (this.mMaxForeProcTime < param1Int) {
            this.mMaxForeProcTime = param1Int;
            this.mMaxForeProc = str;
          } 
        } 
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
      this.mVersionName = param1String1;
    }
    
    private int getMaxForeTimeThousandths() {
      int i = this.mTotalTime;
      if (i == 0) {
        i = 0;
      } else {
        i = this.mMaxForeProcTime * 1000 / i;
      } 
      return i;
    }
    
    private String getMaxForeProc() {
      return this.mMaxForeProc;
    }
    
    private void putTopCpuProcAndTime(String param1String, int param1Int) {
      if (this.mTopCpuProcTimeMap.containsKey(param1String)) {
        int i = ((Integer)this.mTopCpuProcTimeMap.get(param1String)).intValue();
        this.mTopCpuProcTimeMap.put(param1String, Integer.valueOf(param1Int + i));
      } else {
        this.mTopCpuProcTimeMap.put(param1String, Integer.valueOf(param1Int));
      } 
    }
    
    private int getTopCpuTimeThousandths() {
      int i = 0;
      try {
        if (!this.mMaxTopCpuProc.equals("null") && this.mTopCpuProcTimeMap.containsKey(this.mMaxTopCpuProc) && this.mTopCpuProcTimeMap.get(this.mMaxTopCpuProc) != null) {
          if (this.mTotalTime != 0)
            i = ((Integer)this.mTopCpuProcTimeMap.get(this.mMaxTopCpuProc)).intValue() * 1000 / this.mTotalTime; 
          return i;
        } 
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
      return 0;
    }
    
    private void putTopCpuProcAndRatio(String param1String, int param1Int) {
      if (this.mTopCpuProcRatioMap.containsKey(param1String)) {
        int i = ((Integer)this.mTopCpuProcRatioMap.get(param1String)).intValue();
        this.mTopCpuProcRatioMap.put(param1String, Integer.valueOf(param1Int + (i & 0xFFFF) | (0xFFFF & i >> 16) + 1 << 16));
      } else {
        this.mTopCpuProcRatioMap.put(param1String, Integer.valueOf(0x10000 | param1Int));
      } 
    }
    
    private int getMaxTopCpuRatio() {
      try {
        if (!this.mMaxTopCpuProc.equals("null") && this.mTopCpuProcRatioMap.containsKey(this.mMaxTopCpuProc) && this.mTopCpuProcRatioMap.get(this.mMaxTopCpuProc) != null) {
          int i = ((Integer)this.mTopCpuProcRatioMap.get(this.mMaxTopCpuProc)).intValue();
          this.mMaxTopCpuRatio = (i & 0xFFFF) / (0xFFFF & i >> 16);
        } 
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
      return this.mMaxTopCpuRatio;
    }
    
    private String getMaxTopCpuProc() {
      int i = -1;
      try {
        for (Map.Entry<String, Integer> entry : this.mTopCpuProcRatioMap.entrySet()) {
          String str = (String)entry.getKey();
          int j = ((Integer)entry.getValue()).intValue();
          int k = (j & 0xFFFF) / this.mCpuLoadingCount;
          j = i;
          if (i < k) {
            j = k;
            this.mMaxTopCpuProc = str;
          } 
          i = j;
        } 
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
      return this.mMaxTopCpuProc;
    }
    
    public boolean analizyHeatRecItem(long param1Long, int param1Int) {
      if (param1Int < 0 || param1Int < this.mAnalizyPosition - 400)
        return false; 
      try {
        OplusBaseBatteryStats.ThermalItem thermalItem = this.mAnalizyHeatArray[param1Int % 400];
        if (thermalItem == null)
          return false; 
        if (thermalItem.elapsedRealtime < param1Long || thermalItem.phoneTemp < OplusThermalStatsHelper.this.mHeatThreshold - 70)
          return false; 
        if (this.mLastAnalizyItem.elapsedRealtime > 0L) {
          int i = (int)(this.mLastAnalizyItem.elapsedRealtime - thermalItem.elapsedRealtime);
          addTotalTime(i);
          if (thermalItem.audioOn)
            addAudioTime(i); 
          if (thermalItem.cameraOn)
            addCameraTime(i); 
          if (thermalItem.videoOn)
            addVideoTime(i); 
          if (thermalItem.gpsOn)
            addGpsTime(i); 
          if (thermalItem.flashlightOn)
            addFlashlightTime(i); 
          if (thermalItem.phoneOnff)
            addPhoneOnTime(i); 
          if (thermalItem.connectNetType == 1) {
            addWifiTime(i);
          } else if (thermalItem.connectNetType == 2) {
            addNet2GTime(i);
          } else if (thermalItem.connectNetType == 3) {
            addNet3GTime(i);
          } else if (thermalItem.connectNetType == 4) {
            addNet4GTime(i);
          } 
          putPlugTypeAndTime(thermalItem.chargePlug, i);
          if (thermalItem.jobSchedule != null && !thermalItem.jobSchedule.equals("null"))
            putJobProcAndTime(thermalItem.jobSchedule, i); 
          if (thermalItem.netSync != null && !thermalItem.netSync.equals("null"))
            putSyncProcAndTime(thermalItem.netSync, i); 
          if (thermalItem.foreProc != null && !thermalItem.foreProc.equals("null"))
            putForeProcAndTime(thermalItem.versionName, thermalItem.foreProc, i); 
          if (thermalItem.topProc != null && !thermalItem.topProc.equals("null"))
            putTopCpuProcAndTime(thermalItem.topProc, i); 
        } 
        if (thermalItem.connectNetType == 1)
          addWifiStrenth(thermalItem.wifiSignal); 
        addPhoneSignal(thermalItem.phoneSignal);
        addCpuLoading(thermalItem.cpuLoading);
        addBackLight(thermalItem.backlight);
        if (getEnviTmep() == -1023)
          setEnviTmep(thermalItem.enviTemp); 
        if (param1Int == this.mAnalizyPosition - 1) {
          if (thermalItem.phoneTemp != -1023) {
            setBatRm1(thermalItem.batRm);
            setTemp1(thermalItem.phoneTemp);
          } 
        } else if (thermalItem.phoneTemp != -1023) {
          setBatRm0(thermalItem.batRm);
          setTemp0(thermalItem.phoneTemp);
        } 
        if (thermalItem.topProc != null)
          putTopCpuProcAndRatio(thermalItem.topProc, thermalItem.topCpu); 
        this.mLastAnalizyItem.setTo(thermalItem);
        return true;
      } catch (Exception exception) {
        Slog.e("OppoThermalStats", "analizyHeatRecItem pos error");
        return false;
      } 
    }
    
    private int getHeatReson() {
      int i = getPlugNoneTimeThousandths();
      boolean bool = false;
      if (i < 250) {
        i = 1;
      } else {
        i = 0;
      } 
      int j = getAudioTimeThousandths(), k = getJobTimeThousandths(), m = getVideoTimeThousandths();
      int n = getCameraTimeThousandths(), i1 = getSyncTimeThousandths();
      if (j + k + m + n + i1 + getPhoneOnTimeThousandths() + getFlashlightTimeThousandths() >= 750)
        bool = true; 
      if (getMaxTopCpuProc().equals(getMaxForeProc()) && getMaxForeTimeThousandths() >= 600 && getHeatRatio() >= OplusThermalStatsHelper.this.mHeatIncRatioThreshold) {
        if (i == 0)
          return 3; 
        return 4;
      } 
      if (!getMaxTopCpuProc().equals(getMaxForeProc()) && getMaxForeTimeThousandths() >= 600 && getHeatRatio() >= OplusThermalStatsHelper.this.mHeatIncRatioThreshold) {
        if (i == 0)
          return 1; 
        return 2;
      } 
      if (bool) {
        if (i == 0)
          return 5; 
        return 6;
      } 
      if (getHeatRatio() <= 500 && getAvgCpuLoading() <= 150) {
        if (i != 0 && getCurrent() < -200)
          return 7; 
        if (i == 0 && getCurrent() < 500)
          return 8; 
        return 9999;
      } 
      return 9999;
    }
    
    public void putHeatMaxTemp(int param1Int1, int param1Int2) {
      if (hasCode()) {
        if (this.mUpLoadMap.containsKey("maxPhoneTemp")) {
          try {
            int i = Integer.parseInt(this.mUpLoadMap.get("maxPhoneTemp"));
            if (i < param1Int1)
              this.mUpLoadMap.put("maxPhoneTemp", Integer.toString(param1Int1)); 
          } catch (Exception exception) {
            exception.printStackTrace();
          } 
        } else {
          this.mUpLoadMap.put("maxPhoneTemp", Integer.toString(param1Int1));
        } 
        if (this.mUpLoadMap.containsKey("maxBatTemp")) {
          try {
            param1Int1 = Integer.parseInt(this.mUpLoadMap.get("maxBatTemp"));
            if (param1Int1 < param1Int2)
              this.mUpLoadMap.put("maxBatTemp", Integer.toString(param1Int2)); 
          } catch (Exception exception) {
            exception.printStackTrace();
          } 
        } else {
          this.mUpLoadMap.put("maxBatTemp", Integer.toString(param1Int2));
        } 
      } 
    }
    
    public void getHeatReasonDetails() {
      this.mUpLoadMap.put("heatTime", DateFormat.format("yyyy-MM-dd-HH-mm-ss", System.currentTimeMillis()).toString());
      this.mUpLoadMap.put("heatReason", Integer.toString(getHeatReson()));
      this.mUpLoadMap.put("totalTime", Integer.toString(this.mTotalTime / 1000));
      this.mUpLoadMap.put("audioTimeRatio", Float.toString(getAudioTimeThousandths() / 10.0F));
      this.mUpLoadMap.put("cameraTimeRatio", Float.toString(getCameraTimeThousandths() / 10.0F));
      this.mUpLoadMap.put("gpsTimeRatio", Float.toString(getCameraTimeThousandths() / 10.0F));
      this.mUpLoadMap.put("flashlightTimeRatio", Float.toString(getFlashlightTimeThousandths() / 10.0F));
      this.mUpLoadMap.put("phoneOnTimeRatio", Float.toString(getPhoneOnTimeThousandths() / 10.0F));
      this.mUpLoadMap.put("phoneSignal", Integer.toString(getAvgPhoneSignal()));
      this.mUpLoadMap.put("wifiTimeRatio", Float.toString(getWifiTimeThousandths() / 10.0F));
      this.mUpLoadMap.put("wifiSignal", Integer.toString(getAvgWifiStrenth()));
      this.mUpLoadMap.put("2GTimeRatio", Float.toString(getNet2GTimeThousandths() / 10.0F));
      this.mUpLoadMap.put("3GTimeRatio", Float.toString(getNet3GTimeThousandths() / 10.0F));
      this.mUpLoadMap.put("4GTimeRatio", Float.toString(getNet4GTimeThousandths() / 10.0F));
      this.mUpLoadMap.put("cpuLoading", Float.toString(getAvgCpuLoading() / 10.0F));
      this.mUpLoadMap.put("backlight", Integer.toString(getAvgBackLight()));
      this.mUpLoadMap.put("jobProc", getMaxJobProc());
      this.mUpLoadMap.put("jobTimeRatio", Float.toString(getJobTimeThousandths() / 10.0F));
      this.mUpLoadMap.put("syncProcc", getMaxSyncProc());
      this.mUpLoadMap.put("syncTimeRatio", Float.toString(getSyncTimeThousandths() / 10.0F));
      this.mUpLoadMap.put("foreProc", "null");
      this.mUpLoadMap.put("foreProcTimeRatio", Float.toString(getMaxForeTimeThousandths() / 10.0F));
      this.mUpLoadMap.put("topcpuproc", getMaxTopCpuProc());
      this.mUpLoadMap.put("topcpuTimeRatio", Float.toString(getTopCpuTimeThousandths() / 10.0F));
      this.mUpLoadMap.put("topcpuRatio", Float.toString(getMaxTopCpuRatio() / 10.0F));
      this.mUpLoadMap.put("heatRatio", Float.toString(getHeatRatio() / 10.0F));
      this.mUpLoadMap.put("current", Integer.toString(getCurrent()));
      this.mUpLoadMap.put("enviTemp", Integer.toString(getEnviTmep()));
      this.mUpLoadMap.put("temp", Integer.toString(this.mPhoneTemp));
      this.mUpLoadMap.put("batTemp", Integer.toString(this.mBatTemp));
      this.mUpLoadMap.put("maxPhoneTemp", Integer.toString(this.mPhoneTemp));
      this.mUpLoadMap.put("maxBatTemp", Integer.toString(this.mBatTemp));
      this.mUpLoadMap.put("heatThreshold", Integer.toString(OplusThermalStatsHelper.this.mHeatThreshold));
      this.mUpLoadMap.put("foreProcVersion", this.mVersionName);
      try {
        for (Map.Entry<String, String> entry : this.mUpLoadMap.entrySet()) {
          if ("syncProcc".equals(entry.getKey()))
            continue; 
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("getHeatReasonDetails ");
          stringBuilder.append((String)entry.getKey());
          stringBuilder.append(":");
          stringBuilder.append((String)entry.getValue());
          Slog.i("OppoThermalStats", stringBuilder.toString());
        } 
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
    }
    
    public void uploadHeatEvent() {
      if (hasCode()) {
        try {
          for (Map.Entry<String, String> entry : this.mUpLoadMap.entrySet()) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("uploadHeatEvent ");
            stringBuilder.append((String)entry.getKey());
            stringBuilder.append(":");
            stringBuilder.append((String)entry.getValue());
            Slog.i("OppoThermalStats", stringBuilder.toString());
          } 
        } catch (Exception exception) {
          exception.printStackTrace();
        } 
        Thread thread = new Thread(new Runnable() {
              final OplusThermalStatsHelper.HeatReasonDetails this$1;
              
              public void run() {
                // Byte code:
                //   0: aload_0
                //   1: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   4: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   7: invokestatic access$000 : (Lcom/android/internal/os/OplusThermalStatsHelper;)Ljava/lang/Object;
                //   10: astore_1
                //   11: aload_1
                //   12: monitorenter
                //   13: aload_0
                //   14: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   17: invokestatic access$100 : (Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;)Z
                //   20: ifeq -> 26
                //   23: aload_1
                //   24: monitorexit
                //   25: return
                //   26: aload_0
                //   27: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   30: getfield mUpLoadMap : Ljava/util/Map;
                //   33: invokeinterface isEmpty : ()Z
                //   38: ifeq -> 44
                //   41: aload_1
                //   42: monitorexit
                //   43: return
                //   44: aload_0
                //   45: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   48: getfield mUpLoadMap : Ljava/util/Map;
                //   51: ldc 'heatReason'
                //   53: invokeinterface containsKey : (Ljava/lang/Object;)Z
                //   58: ifne -> 64
                //   61: aload_1
                //   62: monitorexit
                //   63: return
                //   64: aload_0
                //   65: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   68: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   71: invokestatic access$200 : (Lcom/android/internal/os/OplusThermalStatsHelper;)Landroid/content/Context;
                //   74: ifnonnull -> 88
                //   77: ldc 'OppoThermalStats'
                //   79: ldc 'upload heat event failed for context uninit!'
                //   81: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
                //   84: pop
                //   85: aload_1
                //   86: monitorexit
                //   87: return
                //   88: aload_0
                //   89: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   92: getfield mUpLoadMap : Ljava/util/Map;
                //   95: ldc 'simpleTopPro'
                //   97: aload_0
                //   98: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   101: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   104: invokestatic access$300 : (Lcom/android/internal/os/OplusThermalStatsHelper;)Ljava/lang/String;
                //   107: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
                //   112: pop
                //   113: aload_0
                //   114: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   117: getfield mUpLoadMap : Ljava/util/Map;
                //   120: ldc 'cpuFreq'
                //   122: aload_0
                //   123: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   126: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   129: invokestatic access$400 : (Lcom/android/internal/os/OplusThermalStatsHelper;)Ljava/lang/String;
                //   132: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
                //   137: pop
                //   138: aload_0
                //   139: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   142: getfield mUpLoadMap : Ljava/util/Map;
                //   145: astore_2
                //   146: new java/lang/StringBuilder
                //   149: astore_3
                //   150: aload_3
                //   151: invokespecial <init> : ()V
                //   154: aload_3
                //   155: ldc ''
                //   157: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   160: pop
                //   161: aload_3
                //   162: aload_0
                //   163: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   166: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   169: getfield mBatteryFcc : I
                //   172: invokevirtual append : (I)Ljava/lang/StringBuilder;
                //   175: pop
                //   176: aload_2
                //   177: ldc 'fcc'
                //   179: aload_3
                //   180: invokevirtual toString : ()Ljava/lang/String;
                //   183: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
                //   188: pop
                //   189: aload_0
                //   190: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   193: getfield mUpLoadMap : Ljava/util/Map;
                //   196: astore_2
                //   197: new java/lang/StringBuilder
                //   200: astore_3
                //   201: aload_3
                //   202: invokespecial <init> : ()V
                //   205: aload_3
                //   206: ldc ''
                //   208: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   211: pop
                //   212: aload_3
                //   213: aload_0
                //   214: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   217: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   220: getfield mGlobalBatteryRealtimeCapacity : I
                //   223: invokevirtual append : (I)Ljava/lang/StringBuilder;
                //   226: pop
                //   227: aload_2
                //   228: ldc 'batteryRm'
                //   230: aload_3
                //   231: invokevirtual toString : ()Ljava/lang/String;
                //   234: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
                //   239: pop
                //   240: aload_0
                //   241: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   244: getfield mUpLoadMap : Ljava/util/Map;
                //   247: astore_3
                //   248: new java/lang/StringBuilder
                //   251: astore_2
                //   252: aload_2
                //   253: invokespecial <init> : ()V
                //   256: aload_2
                //   257: ldc ''
                //   259: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   262: pop
                //   263: aload_2
                //   264: aload_0
                //   265: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   268: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   271: getfield mGlobalPlugType : I
                //   274: invokevirtual append : (I)Ljava/lang/StringBuilder;
                //   277: pop
                //   278: aload_3
                //   279: ldc 'plugType'
                //   281: aload_2
                //   282: invokevirtual toString : ()Ljava/lang/String;
                //   285: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
                //   290: pop
                //   291: aload_0
                //   292: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   295: getfield mUpLoadMap : Ljava/util/Map;
                //   298: astore_3
                //   299: new java/lang/StringBuilder
                //   302: astore_2
                //   303: aload_2
                //   304: invokespecial <init> : ()V
                //   307: aload_2
                //   308: ldc ''
                //   310: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   313: pop
                //   314: aload_2
                //   315: aload_0
                //   316: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   319: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   322: getfield mGlobalFastCharger : Z
                //   325: invokevirtual append : (Z)Ljava/lang/StringBuilder;
                //   328: pop
                //   329: aload_3
                //   330: ldc 'fastCharge'
                //   332: aload_2
                //   333: invokevirtual toString : ()Ljava/lang/String;
                //   336: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
                //   341: pop
                //   342: aload_0
                //   343: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   346: getfield mUpLoadMap : Ljava/util/Map;
                //   349: astore_2
                //   350: new java/lang/StringBuilder
                //   353: astore_3
                //   354: aload_3
                //   355: invokespecial <init> : ()V
                //   358: aload_3
                //   359: ldc ''
                //   361: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   364: pop
                //   365: aload_3
                //   366: aload_0
                //   367: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   370: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   373: getfield mGlobalBatteryCurrent : I
                //   376: invokevirtual append : (I)Ljava/lang/StringBuilder;
                //   379: pop
                //   380: aload_2
                //   381: ldc 'batteryCurrent'
                //   383: aload_3
                //   384: invokevirtual toString : ()Ljava/lang/String;
                //   387: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
                //   392: pop
                //   393: aload_0
                //   394: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   397: getfield mUpLoadMap : Ljava/util/Map;
                //   400: astore_2
                //   401: new java/lang/StringBuilder
                //   404: astore_3
                //   405: aload_3
                //   406: invokespecial <init> : ()V
                //   409: aload_3
                //   410: ldc ''
                //   412: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   415: pop
                //   416: aload_3
                //   417: aload_0
                //   418: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   421: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   424: getfield mGlobalBatteryVoltage : I
                //   427: invokevirtual append : (I)Ljava/lang/StringBuilder;
                //   430: pop
                //   431: aload_2
                //   432: ldc 'batteryVoltage'
                //   434: aload_3
                //   435: invokevirtual toString : ()Ljava/lang/String;
                //   438: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
                //   443: pop
                //   444: aload_0
                //   445: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   448: getfield mUpLoadMap : Ljava/util/Map;
                //   451: astore_2
                //   452: new java/lang/StringBuilder
                //   455: astore_3
                //   456: aload_3
                //   457: invokespecial <init> : ()V
                //   460: aload_3
                //   461: ldc ''
                //   463: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   466: pop
                //   467: aload_3
                //   468: aload_0
                //   469: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   472: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   475: getfield mGlobalVolumeLevel : I
                //   478: invokevirtual append : (I)Ljava/lang/StringBuilder;
                //   481: pop
                //   482: aload_2
                //   483: ldc 'volumeLevel'
                //   485: aload_3
                //   486: invokevirtual toString : ()Ljava/lang/String;
                //   489: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
                //   494: pop
                //   495: aload_0
                //   496: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   499: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   502: invokestatic access$200 : (Lcom/android/internal/os/OplusThermalStatsHelper;)Landroid/content/Context;
                //   505: ldc '20139'
                //   507: ldc 'id_thermal_heat'
                //   509: aload_0
                //   510: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   513: getfield mUpLoadMap : Ljava/util/Map;
                //   516: iconst_0
                //   517: invokestatic onCommon : (Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Z)V
                //   520: iconst_0
                //   521: istore #4
                //   523: iconst_0
                //   524: istore #5
                //   526: invokestatic getOnCommon : ()Z
                //   529: istore #6
                //   531: iload #6
                //   533: ifeq -> 590
                //   536: iload #4
                //   538: bipush #50
                //   540: if_icmpge -> 590
                //   543: ldc2_w 100
                //   546: invokestatic sleep : (J)V
                //   549: iinc #4, 1
                //   552: goto -> 526
                //   555: astore_2
                //   556: new java/lang/StringBuilder
                //   559: astore_3
                //   560: aload_3
                //   561: invokespecial <init> : ()V
                //   564: aload_3
                //   565: ldc 'sleep 100 ms is Interrupted because of '
                //   567: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   570: pop
                //   571: aload_3
                //   572: aload_2
                //   573: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
                //   576: pop
                //   577: ldc 'OppoThermalStats'
                //   579: aload_3
                //   580: invokevirtual toString : ()Ljava/lang/String;
                //   583: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
                //   586: pop
                //   587: goto -> 526
                //   590: aload_0
                //   591: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   594: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   597: invokevirtual writeThermalRecFile : ()V
                //   600: new android/content/Intent
                //   603: astore_2
                //   604: aload_2
                //   605: ldc 'oppo.intent.action.ACTION_THERMAL_SCENE'
                //   607: invokespecial <init> : (Ljava/lang/String;)V
                //   610: aload_0
                //   611: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   614: getfield mUpLoadMap : Ljava/util/Map;
                //   617: ldc 'heatReason'
                //   619: invokeinterface containsKey : (Ljava/lang/Object;)Z
                //   624: ifeq -> 654
                //   627: aload_2
                //   628: ldc 'reason'
                //   630: aload_0
                //   631: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   634: getfield mUpLoadMap : Ljava/util/Map;
                //   637: ldc 'heatReason'
                //   639: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
                //   644: checkcast java/lang/String
                //   647: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
                //   650: pop
                //   651: goto -> 663
                //   654: aload_2
                //   655: ldc 'reason'
                //   657: ldc '9999'
                //   659: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
                //   662: pop
                //   663: aload_0
                //   664: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   667: getfield mUpLoadMap : Ljava/util/Map;
                //   670: ldc 'current'
                //   672: invokeinterface containsKey : (Ljava/lang/Object;)Z
                //   677: ifeq -> 707
                //   680: aload_2
                //   681: ldc 'current'
                //   683: aload_0
                //   684: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   687: getfield mUpLoadMap : Ljava/util/Map;
                //   690: ldc 'current'
                //   692: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
                //   697: checkcast java/lang/String
                //   700: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
                //   703: pop
                //   704: goto -> 716
                //   707: aload_2
                //   708: ldc 'current'
                //   710: ldc '9999'
                //   712: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
                //   715: pop
                //   716: aload_0
                //   717: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   720: getfield mUpLoadMap : Ljava/util/Map;
                //   723: ldc 'maxPhoneTemp'
                //   725: invokeinterface containsKey : (Ljava/lang/Object;)Z
                //   730: ifeq -> 760
                //   733: aload_2
                //   734: ldc 'temp'
                //   736: aload_0
                //   737: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   740: getfield mUpLoadMap : Ljava/util/Map;
                //   743: ldc 'maxPhoneTemp'
                //   745: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
                //   750: checkcast java/lang/String
                //   753: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
                //   756: pop
                //   757: goto -> 769
                //   760: aload_2
                //   761: ldc 'temp'
                //   763: ldc '9999'
                //   765: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
                //   768: pop
                //   769: aload_0
                //   770: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   773: getfield mUpLoadMap : Ljava/util/Map;
                //   776: ldc 'maxBatTemp'
                //   778: invokeinterface containsKey : (Ljava/lang/Object;)Z
                //   783: ifeq -> 813
                //   786: aload_2
                //   787: ldc 'batTemp'
                //   789: aload_0
                //   790: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   793: getfield mUpLoadMap : Ljava/util/Map;
                //   796: ldc 'maxBatTemp'
                //   798: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
                //   803: checkcast java/lang/String
                //   806: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
                //   809: pop
                //   810: goto -> 822
                //   813: aload_2
                //   814: ldc 'batTemp'
                //   816: ldc '9999'
                //   818: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
                //   821: pop
                //   822: aload_0
                //   823: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   826: getfield mUpLoadMap : Ljava/util/Map;
                //   829: ldc 'cpuLoading'
                //   831: invokeinterface containsKey : (Ljava/lang/Object;)Z
                //   836: ifeq -> 866
                //   839: aload_2
                //   840: ldc 'cpuloading'
                //   842: aload_0
                //   843: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   846: getfield mUpLoadMap : Ljava/util/Map;
                //   849: ldc 'cpuloading'
                //   851: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
                //   856: checkcast java/lang/String
                //   859: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
                //   862: pop
                //   863: goto -> 875
                //   866: aload_2
                //   867: ldc 'cpuloading'
                //   869: ldc '9999'
                //   871: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
                //   874: pop
                //   875: aload_2
                //   876: ldc 'package'
                //   878: aload_0
                //   879: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   882: invokestatic access$500 : (Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;)Ljava/lang/String;
                //   885: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
                //   888: pop
                //   889: aload_0
                //   890: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   893: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   896: getfield mThermalUploadLog : Z
                //   899: ifne -> 945
                //   902: iload #5
                //   904: istore #6
                //   906: aload_0
                //   907: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   910: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   913: getfield mThermalUploadErrLog : Z
                //   916: ifeq -> 948
                //   919: aload_0
                //   920: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   923: invokestatic access$600 : (Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;)I
                //   926: iconst_1
                //   927: if_icmpeq -> 945
                //   930: iload #5
                //   932: istore #6
                //   934: aload_0
                //   935: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   938: invokestatic access$600 : (Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;)I
                //   941: iconst_2
                //   942: if_icmpne -> 948
                //   945: iconst_1
                //   946: istore #6
                //   948: aload_2
                //   949: ldc 'uploadLog'
                //   951: iload #6
                //   953: invokevirtual putExtra : (Ljava/lang/String;Z)Landroid/content/Intent;
                //   956: pop
                //   957: aload_2
                //   958: ldc 'com.oppo.oppopowermonitor'
                //   960: invokevirtual setPackage : (Ljava/lang/String;)Landroid/content/Intent;
                //   963: pop
                //   964: aload_0
                //   965: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   968: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   971: getfield mThermalCaptureLog : Z
                //   974: ifeq -> 1042
                //   977: aload_0
                //   978: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   981: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   984: getfield mGlobalMaxPhoneTemp : I
                //   987: aload_0
                //   988: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   991: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   994: getfield mThermalCaptureLogThreshold : I
                //   997: if_icmplt -> 1042
                //   1000: aload_0
                //   1001: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   1004: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   1007: invokestatic access$200 : (Lcom/android/internal/os/OplusThermalStatsHelper;)Landroid/content/Context;
                //   1010: ifnull -> 1030
                //   1013: aload_0
                //   1014: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   1017: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   1020: invokestatic access$200 : (Lcom/android/internal/os/OplusThermalStatsHelper;)Landroid/content/Context;
                //   1023: aload_2
                //   1024: getstatic android/os/UserHandle.CURRENT : Landroid/os/UserHandle;
                //   1027: invokevirtual sendBroadcastAsUser : (Landroid/content/Intent;Landroid/os/UserHandle;)V
                //   1030: aload_0
                //   1031: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   1034: iconst_1
                //   1035: invokestatic access$102 : (Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;Z)Z
                //   1038: pop
                //   1039: goto -> 1091
                //   1042: new java/lang/StringBuilder
                //   1045: astore_2
                //   1046: aload_2
                //   1047: invokespecial <init> : ()V
                //   1050: aload_2
                //   1051: ldc_w 'CaptureLog='
                //   1054: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   1057: pop
                //   1058: aload_2
                //   1059: aload_0
                //   1060: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   1063: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   1066: getfield mThermalCaptureLog : Z
                //   1069: invokevirtual append : (Z)Ljava/lang/StringBuilder;
                //   1072: pop
                //   1073: aload_2
                //   1074: ldc_w ' ,skip capture log'
                //   1077: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   1080: pop
                //   1081: ldc 'OppoThermalStats'
                //   1083: aload_2
                //   1084: invokevirtual toString : ()Ljava/lang/String;
                //   1087: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
                //   1090: pop
                //   1091: aload_0
                //   1092: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   1095: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
                //   1098: aload_0
                //   1099: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
                //   1102: getfield mUpLoadMap : Ljava/util/Map;
                //   1105: invokestatic access$700 : (Lcom/android/internal/os/OplusThermalStatsHelper;Ljava/util/Map;)Ljava/util/Map;
                //   1108: astore_2
                //   1109: aload_2
                //   1110: invokeinterface entrySet : ()Ljava/util/Set;
                //   1115: invokeinterface iterator : ()Ljava/util/Iterator;
                //   1120: astore #7
                //   1122: aload #7
                //   1124: invokeinterface hasNext : ()Z
                //   1129: ifeq -> 1215
                //   1132: aload #7
                //   1134: invokeinterface next : ()Ljava/lang/Object;
                //   1139: checkcast java/util/Map$Entry
                //   1142: astore_3
                //   1143: new java/lang/StringBuilder
                //   1146: astore #8
                //   1148: aload #8
                //   1150: invokespecial <init> : ()V
                //   1153: aload #8
                //   1155: ldc_w 'uploadStampHeat '
                //   1158: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   1161: pop
                //   1162: aload #8
                //   1164: aload_3
                //   1165: invokeinterface getKey : ()Ljava/lang/Object;
                //   1170: checkcast java/lang/String
                //   1173: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   1176: pop
                //   1177: aload #8
                //   1179: ldc_w ':'
                //   1182: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   1185: pop
                //   1186: aload #8
                //   1188: aload_3
                //   1189: invokeinterface getValue : ()Ljava/lang/Object;
                //   1194: checkcast java/lang/String
                //   1197: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   1200: pop
                //   1201: ldc 'OppoThermalStats'
                //   1203: aload #8
                //   1205: invokevirtual toString : ()Ljava/lang/String;
                //   1208: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
                //   1211: pop
                //   1212: goto -> 1122
                //   1215: goto -> 1223
                //   1218: astore_3
                //   1219: aload_3
                //   1220: invokevirtual printStackTrace : ()V
                //   1223: aload_2
                //   1224: invokeinterface size : ()I
                //   1229: ifle -> 1239
                //   1232: ldc_w '040201'
                //   1235: aload_2
                //   1236: invokestatic onStamp : (Ljava/lang/String;Ljava/util/Map;)V
                //   1239: aload_1
                //   1240: monitorexit
                //   1241: return
                //   1242: astore_2
                //   1243: aload_1
                //   1244: monitorexit
                //   1245: aload_2
                //   1246: athrow
                // Line number table:
                //   Java source line number -> byte code offset
                //   #915	-> 0
                //   #917	-> 13
                //   #918	-> 23
                //   #920	-> 26
                //   #921	-> 41
                //   #923	-> 44
                //   #924	-> 61
                //   #926	-> 64
                //   #927	-> 77
                //   #928	-> 85
                //   #930	-> 88
                //   #931	-> 113
                //   #932	-> 138
                //   #933	-> 189
                //   #934	-> 240
                //   #935	-> 291
                //   #936	-> 342
                //   #937	-> 393
                //   #938	-> 444
                //   #939	-> 495
                //   #940	-> 520
                //   #941	-> 523
                //   #942	-> 526
                //   #944	-> 543
                //   #945	-> 549
                //   #948	-> 552
                //   #946	-> 555
                //   #947	-> 556
                //   #948	-> 587
                //   #950	-> 590
                //   #951	-> 600
                //   #952	-> 610
                //   #953	-> 627
                //   #955	-> 654
                //   #957	-> 663
                //   #958	-> 680
                //   #960	-> 707
                //   #962	-> 716
                //   #963	-> 733
                //   #965	-> 760
                //   #967	-> 769
                //   #968	-> 786
                //   #970	-> 813
                //   #972	-> 822
                //   #973	-> 839
                //   #975	-> 866
                //   #978	-> 875
                //   #979	-> 889
                //   #980	-> 945
                //   #982	-> 948
                //   #983	-> 957
                //   #984	-> 964
                //   #985	-> 1000
                //   #986	-> 1030
                //   #988	-> 1042
                //   #992	-> 1091
                //   #995	-> 1109
                //   #996	-> 1143
                //   #997	-> 1212
                //   #1000	-> 1215
                //   #998	-> 1218
                //   #999	-> 1219
                //   #1002	-> 1223
                //   #1003	-> 1232
                //   #1005	-> 1239
                //   #1006	-> 1241
                //   #1005	-> 1242
                // Exception table:
                //   from	to	target	type
                //   13	23	1242	finally
                //   23	25	1242	finally
                //   26	41	1242	finally
                //   41	43	1242	finally
                //   44	61	1242	finally
                //   61	63	1242	finally
                //   64	77	1242	finally
                //   77	85	1242	finally
                //   85	87	1242	finally
                //   88	113	1242	finally
                //   113	138	1242	finally
                //   138	189	1242	finally
                //   189	240	1242	finally
                //   240	291	1242	finally
                //   291	342	1242	finally
                //   342	393	1242	finally
                //   393	444	1242	finally
                //   444	495	1242	finally
                //   495	520	1242	finally
                //   526	531	1242	finally
                //   543	549	555	java/lang/InterruptedException
                //   543	549	1242	finally
                //   556	587	1242	finally
                //   590	600	1242	finally
                //   600	610	1242	finally
                //   610	627	1242	finally
                //   627	651	1242	finally
                //   654	663	1242	finally
                //   663	680	1242	finally
                //   680	704	1242	finally
                //   707	716	1242	finally
                //   716	733	1242	finally
                //   733	757	1242	finally
                //   760	769	1242	finally
                //   769	786	1242	finally
                //   786	810	1242	finally
                //   813	822	1242	finally
                //   822	839	1242	finally
                //   839	863	1242	finally
                //   866	875	1242	finally
                //   875	889	1242	finally
                //   889	902	1242	finally
                //   906	930	1242	finally
                //   934	945	1242	finally
                //   948	957	1242	finally
                //   957	964	1242	finally
                //   964	1000	1242	finally
                //   1000	1030	1242	finally
                //   1030	1039	1242	finally
                //   1042	1091	1242	finally
                //   1091	1109	1242	finally
                //   1109	1122	1218	java/lang/Exception
                //   1109	1122	1242	finally
                //   1122	1143	1218	java/lang/Exception
                //   1122	1143	1242	finally
                //   1143	1212	1218	java/lang/Exception
                //   1143	1212	1242	finally
                //   1219	1223	1242	finally
                //   1223	1232	1242	finally
                //   1232	1239	1242	finally
                //   1239	1241	1242	finally
                //   1243	1245	1242	finally
              }
            });
        thread.start();
      } 
    }
    
    public void dumpThemalHeatDetailLocked(PrintWriter param1PrintWriter) {
      if (this.mUpLoadMap.size() > 0) {
        try {
          for (Map.Entry<String, String> entry : this.mUpLoadMap.entrySet()) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("uploadHeatReasonDetails ");
            stringBuilder.append((String)entry.getKey());
            stringBuilder.append(":");
            stringBuilder.append((String)entry.getValue());
            param1PrintWriter.println(stringBuilder.toString());
          } 
        } catch (Exception exception) {
          exception.printStackTrace();
        } 
      } else {
        exception.print("no heat record");
      } 
    }
  }
  
  class null implements Runnable {
    final OplusThermalStatsHelper.HeatReasonDetails this$1;
    
    public void run() {
      // Byte code:
      //   0: aload_0
      //   1: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   4: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   7: invokestatic access$000 : (Lcom/android/internal/os/OplusThermalStatsHelper;)Ljava/lang/Object;
      //   10: astore_1
      //   11: aload_1
      //   12: monitorenter
      //   13: aload_0
      //   14: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   17: invokestatic access$100 : (Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;)Z
      //   20: ifeq -> 26
      //   23: aload_1
      //   24: monitorexit
      //   25: return
      //   26: aload_0
      //   27: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   30: getfield mUpLoadMap : Ljava/util/Map;
      //   33: invokeinterface isEmpty : ()Z
      //   38: ifeq -> 44
      //   41: aload_1
      //   42: monitorexit
      //   43: return
      //   44: aload_0
      //   45: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   48: getfield mUpLoadMap : Ljava/util/Map;
      //   51: ldc 'heatReason'
      //   53: invokeinterface containsKey : (Ljava/lang/Object;)Z
      //   58: ifne -> 64
      //   61: aload_1
      //   62: monitorexit
      //   63: return
      //   64: aload_0
      //   65: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   68: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   71: invokestatic access$200 : (Lcom/android/internal/os/OplusThermalStatsHelper;)Landroid/content/Context;
      //   74: ifnonnull -> 88
      //   77: ldc 'OppoThermalStats'
      //   79: ldc 'upload heat event failed for context uninit!'
      //   81: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   84: pop
      //   85: aload_1
      //   86: monitorexit
      //   87: return
      //   88: aload_0
      //   89: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   92: getfield mUpLoadMap : Ljava/util/Map;
      //   95: ldc 'simpleTopPro'
      //   97: aload_0
      //   98: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   101: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   104: invokestatic access$300 : (Lcom/android/internal/os/OplusThermalStatsHelper;)Ljava/lang/String;
      //   107: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   112: pop
      //   113: aload_0
      //   114: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   117: getfield mUpLoadMap : Ljava/util/Map;
      //   120: ldc 'cpuFreq'
      //   122: aload_0
      //   123: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   126: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   129: invokestatic access$400 : (Lcom/android/internal/os/OplusThermalStatsHelper;)Ljava/lang/String;
      //   132: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   137: pop
      //   138: aload_0
      //   139: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   142: getfield mUpLoadMap : Ljava/util/Map;
      //   145: astore_2
      //   146: new java/lang/StringBuilder
      //   149: astore_3
      //   150: aload_3
      //   151: invokespecial <init> : ()V
      //   154: aload_3
      //   155: ldc ''
      //   157: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   160: pop
      //   161: aload_3
      //   162: aload_0
      //   163: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   166: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   169: getfield mBatteryFcc : I
      //   172: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   175: pop
      //   176: aload_2
      //   177: ldc 'fcc'
      //   179: aload_3
      //   180: invokevirtual toString : ()Ljava/lang/String;
      //   183: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   188: pop
      //   189: aload_0
      //   190: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   193: getfield mUpLoadMap : Ljava/util/Map;
      //   196: astore_2
      //   197: new java/lang/StringBuilder
      //   200: astore_3
      //   201: aload_3
      //   202: invokespecial <init> : ()V
      //   205: aload_3
      //   206: ldc ''
      //   208: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   211: pop
      //   212: aload_3
      //   213: aload_0
      //   214: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   217: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   220: getfield mGlobalBatteryRealtimeCapacity : I
      //   223: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   226: pop
      //   227: aload_2
      //   228: ldc 'batteryRm'
      //   230: aload_3
      //   231: invokevirtual toString : ()Ljava/lang/String;
      //   234: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   239: pop
      //   240: aload_0
      //   241: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   244: getfield mUpLoadMap : Ljava/util/Map;
      //   247: astore_3
      //   248: new java/lang/StringBuilder
      //   251: astore_2
      //   252: aload_2
      //   253: invokespecial <init> : ()V
      //   256: aload_2
      //   257: ldc ''
      //   259: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   262: pop
      //   263: aload_2
      //   264: aload_0
      //   265: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   268: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   271: getfield mGlobalPlugType : I
      //   274: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   277: pop
      //   278: aload_3
      //   279: ldc 'plugType'
      //   281: aload_2
      //   282: invokevirtual toString : ()Ljava/lang/String;
      //   285: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   290: pop
      //   291: aload_0
      //   292: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   295: getfield mUpLoadMap : Ljava/util/Map;
      //   298: astore_3
      //   299: new java/lang/StringBuilder
      //   302: astore_2
      //   303: aload_2
      //   304: invokespecial <init> : ()V
      //   307: aload_2
      //   308: ldc ''
      //   310: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   313: pop
      //   314: aload_2
      //   315: aload_0
      //   316: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   319: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   322: getfield mGlobalFastCharger : Z
      //   325: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   328: pop
      //   329: aload_3
      //   330: ldc 'fastCharge'
      //   332: aload_2
      //   333: invokevirtual toString : ()Ljava/lang/String;
      //   336: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   341: pop
      //   342: aload_0
      //   343: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   346: getfield mUpLoadMap : Ljava/util/Map;
      //   349: astore_2
      //   350: new java/lang/StringBuilder
      //   353: astore_3
      //   354: aload_3
      //   355: invokespecial <init> : ()V
      //   358: aload_3
      //   359: ldc ''
      //   361: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   364: pop
      //   365: aload_3
      //   366: aload_0
      //   367: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   370: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   373: getfield mGlobalBatteryCurrent : I
      //   376: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   379: pop
      //   380: aload_2
      //   381: ldc 'batteryCurrent'
      //   383: aload_3
      //   384: invokevirtual toString : ()Ljava/lang/String;
      //   387: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   392: pop
      //   393: aload_0
      //   394: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   397: getfield mUpLoadMap : Ljava/util/Map;
      //   400: astore_2
      //   401: new java/lang/StringBuilder
      //   404: astore_3
      //   405: aload_3
      //   406: invokespecial <init> : ()V
      //   409: aload_3
      //   410: ldc ''
      //   412: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   415: pop
      //   416: aload_3
      //   417: aload_0
      //   418: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   421: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   424: getfield mGlobalBatteryVoltage : I
      //   427: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   430: pop
      //   431: aload_2
      //   432: ldc 'batteryVoltage'
      //   434: aload_3
      //   435: invokevirtual toString : ()Ljava/lang/String;
      //   438: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   443: pop
      //   444: aload_0
      //   445: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   448: getfield mUpLoadMap : Ljava/util/Map;
      //   451: astore_2
      //   452: new java/lang/StringBuilder
      //   455: astore_3
      //   456: aload_3
      //   457: invokespecial <init> : ()V
      //   460: aload_3
      //   461: ldc ''
      //   463: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   466: pop
      //   467: aload_3
      //   468: aload_0
      //   469: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   472: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   475: getfield mGlobalVolumeLevel : I
      //   478: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   481: pop
      //   482: aload_2
      //   483: ldc 'volumeLevel'
      //   485: aload_3
      //   486: invokevirtual toString : ()Ljava/lang/String;
      //   489: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   494: pop
      //   495: aload_0
      //   496: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   499: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   502: invokestatic access$200 : (Lcom/android/internal/os/OplusThermalStatsHelper;)Landroid/content/Context;
      //   505: ldc '20139'
      //   507: ldc 'id_thermal_heat'
      //   509: aload_0
      //   510: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   513: getfield mUpLoadMap : Ljava/util/Map;
      //   516: iconst_0
      //   517: invokestatic onCommon : (Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Z)V
      //   520: iconst_0
      //   521: istore #4
      //   523: iconst_0
      //   524: istore #5
      //   526: invokestatic getOnCommon : ()Z
      //   529: istore #6
      //   531: iload #6
      //   533: ifeq -> 590
      //   536: iload #4
      //   538: bipush #50
      //   540: if_icmpge -> 590
      //   543: ldc2_w 100
      //   546: invokestatic sleep : (J)V
      //   549: iinc #4, 1
      //   552: goto -> 526
      //   555: astore_2
      //   556: new java/lang/StringBuilder
      //   559: astore_3
      //   560: aload_3
      //   561: invokespecial <init> : ()V
      //   564: aload_3
      //   565: ldc 'sleep 100 ms is Interrupted because of '
      //   567: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   570: pop
      //   571: aload_3
      //   572: aload_2
      //   573: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   576: pop
      //   577: ldc 'OppoThermalStats'
      //   579: aload_3
      //   580: invokevirtual toString : ()Ljava/lang/String;
      //   583: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   586: pop
      //   587: goto -> 526
      //   590: aload_0
      //   591: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   594: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   597: invokevirtual writeThermalRecFile : ()V
      //   600: new android/content/Intent
      //   603: astore_2
      //   604: aload_2
      //   605: ldc 'oppo.intent.action.ACTION_THERMAL_SCENE'
      //   607: invokespecial <init> : (Ljava/lang/String;)V
      //   610: aload_0
      //   611: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   614: getfield mUpLoadMap : Ljava/util/Map;
      //   617: ldc 'heatReason'
      //   619: invokeinterface containsKey : (Ljava/lang/Object;)Z
      //   624: ifeq -> 654
      //   627: aload_2
      //   628: ldc 'reason'
      //   630: aload_0
      //   631: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   634: getfield mUpLoadMap : Ljava/util/Map;
      //   637: ldc 'heatReason'
      //   639: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   644: checkcast java/lang/String
      //   647: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
      //   650: pop
      //   651: goto -> 663
      //   654: aload_2
      //   655: ldc 'reason'
      //   657: ldc '9999'
      //   659: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
      //   662: pop
      //   663: aload_0
      //   664: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   667: getfield mUpLoadMap : Ljava/util/Map;
      //   670: ldc 'current'
      //   672: invokeinterface containsKey : (Ljava/lang/Object;)Z
      //   677: ifeq -> 707
      //   680: aload_2
      //   681: ldc 'current'
      //   683: aload_0
      //   684: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   687: getfield mUpLoadMap : Ljava/util/Map;
      //   690: ldc 'current'
      //   692: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   697: checkcast java/lang/String
      //   700: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
      //   703: pop
      //   704: goto -> 716
      //   707: aload_2
      //   708: ldc 'current'
      //   710: ldc '9999'
      //   712: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
      //   715: pop
      //   716: aload_0
      //   717: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   720: getfield mUpLoadMap : Ljava/util/Map;
      //   723: ldc 'maxPhoneTemp'
      //   725: invokeinterface containsKey : (Ljava/lang/Object;)Z
      //   730: ifeq -> 760
      //   733: aload_2
      //   734: ldc 'temp'
      //   736: aload_0
      //   737: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   740: getfield mUpLoadMap : Ljava/util/Map;
      //   743: ldc 'maxPhoneTemp'
      //   745: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   750: checkcast java/lang/String
      //   753: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
      //   756: pop
      //   757: goto -> 769
      //   760: aload_2
      //   761: ldc 'temp'
      //   763: ldc '9999'
      //   765: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
      //   768: pop
      //   769: aload_0
      //   770: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   773: getfield mUpLoadMap : Ljava/util/Map;
      //   776: ldc 'maxBatTemp'
      //   778: invokeinterface containsKey : (Ljava/lang/Object;)Z
      //   783: ifeq -> 813
      //   786: aload_2
      //   787: ldc 'batTemp'
      //   789: aload_0
      //   790: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   793: getfield mUpLoadMap : Ljava/util/Map;
      //   796: ldc 'maxBatTemp'
      //   798: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   803: checkcast java/lang/String
      //   806: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
      //   809: pop
      //   810: goto -> 822
      //   813: aload_2
      //   814: ldc 'batTemp'
      //   816: ldc '9999'
      //   818: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
      //   821: pop
      //   822: aload_0
      //   823: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   826: getfield mUpLoadMap : Ljava/util/Map;
      //   829: ldc 'cpuLoading'
      //   831: invokeinterface containsKey : (Ljava/lang/Object;)Z
      //   836: ifeq -> 866
      //   839: aload_2
      //   840: ldc 'cpuloading'
      //   842: aload_0
      //   843: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   846: getfield mUpLoadMap : Ljava/util/Map;
      //   849: ldc 'cpuloading'
      //   851: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   856: checkcast java/lang/String
      //   859: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
      //   862: pop
      //   863: goto -> 875
      //   866: aload_2
      //   867: ldc 'cpuloading'
      //   869: ldc '9999'
      //   871: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
      //   874: pop
      //   875: aload_2
      //   876: ldc 'package'
      //   878: aload_0
      //   879: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   882: invokestatic access$500 : (Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;)Ljava/lang/String;
      //   885: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
      //   888: pop
      //   889: aload_0
      //   890: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   893: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   896: getfield mThermalUploadLog : Z
      //   899: ifne -> 945
      //   902: iload #5
      //   904: istore #6
      //   906: aload_0
      //   907: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   910: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   913: getfield mThermalUploadErrLog : Z
      //   916: ifeq -> 948
      //   919: aload_0
      //   920: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   923: invokestatic access$600 : (Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;)I
      //   926: iconst_1
      //   927: if_icmpeq -> 945
      //   930: iload #5
      //   932: istore #6
      //   934: aload_0
      //   935: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   938: invokestatic access$600 : (Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;)I
      //   941: iconst_2
      //   942: if_icmpne -> 948
      //   945: iconst_1
      //   946: istore #6
      //   948: aload_2
      //   949: ldc 'uploadLog'
      //   951: iload #6
      //   953: invokevirtual putExtra : (Ljava/lang/String;Z)Landroid/content/Intent;
      //   956: pop
      //   957: aload_2
      //   958: ldc 'com.oppo.oppopowermonitor'
      //   960: invokevirtual setPackage : (Ljava/lang/String;)Landroid/content/Intent;
      //   963: pop
      //   964: aload_0
      //   965: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   968: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   971: getfield mThermalCaptureLog : Z
      //   974: ifeq -> 1042
      //   977: aload_0
      //   978: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   981: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   984: getfield mGlobalMaxPhoneTemp : I
      //   987: aload_0
      //   988: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   991: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   994: getfield mThermalCaptureLogThreshold : I
      //   997: if_icmplt -> 1042
      //   1000: aload_0
      //   1001: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   1004: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   1007: invokestatic access$200 : (Lcom/android/internal/os/OplusThermalStatsHelper;)Landroid/content/Context;
      //   1010: ifnull -> 1030
      //   1013: aload_0
      //   1014: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   1017: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   1020: invokestatic access$200 : (Lcom/android/internal/os/OplusThermalStatsHelper;)Landroid/content/Context;
      //   1023: aload_2
      //   1024: getstatic android/os/UserHandle.CURRENT : Landroid/os/UserHandle;
      //   1027: invokevirtual sendBroadcastAsUser : (Landroid/content/Intent;Landroid/os/UserHandle;)V
      //   1030: aload_0
      //   1031: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   1034: iconst_1
      //   1035: invokestatic access$102 : (Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;Z)Z
      //   1038: pop
      //   1039: goto -> 1091
      //   1042: new java/lang/StringBuilder
      //   1045: astore_2
      //   1046: aload_2
      //   1047: invokespecial <init> : ()V
      //   1050: aload_2
      //   1051: ldc_w 'CaptureLog='
      //   1054: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1057: pop
      //   1058: aload_2
      //   1059: aload_0
      //   1060: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   1063: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   1066: getfield mThermalCaptureLog : Z
      //   1069: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   1072: pop
      //   1073: aload_2
      //   1074: ldc_w ' ,skip capture log'
      //   1077: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1080: pop
      //   1081: ldc 'OppoThermalStats'
      //   1083: aload_2
      //   1084: invokevirtual toString : ()Ljava/lang/String;
      //   1087: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   1090: pop
      //   1091: aload_0
      //   1092: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   1095: getfield this$0 : Lcom/android/internal/os/OplusThermalStatsHelper;
      //   1098: aload_0
      //   1099: getfield this$1 : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
      //   1102: getfield mUpLoadMap : Ljava/util/Map;
      //   1105: invokestatic access$700 : (Lcom/android/internal/os/OplusThermalStatsHelper;Ljava/util/Map;)Ljava/util/Map;
      //   1108: astore_2
      //   1109: aload_2
      //   1110: invokeinterface entrySet : ()Ljava/util/Set;
      //   1115: invokeinterface iterator : ()Ljava/util/Iterator;
      //   1120: astore #7
      //   1122: aload #7
      //   1124: invokeinterface hasNext : ()Z
      //   1129: ifeq -> 1215
      //   1132: aload #7
      //   1134: invokeinterface next : ()Ljava/lang/Object;
      //   1139: checkcast java/util/Map$Entry
      //   1142: astore_3
      //   1143: new java/lang/StringBuilder
      //   1146: astore #8
      //   1148: aload #8
      //   1150: invokespecial <init> : ()V
      //   1153: aload #8
      //   1155: ldc_w 'uploadStampHeat '
      //   1158: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1161: pop
      //   1162: aload #8
      //   1164: aload_3
      //   1165: invokeinterface getKey : ()Ljava/lang/Object;
      //   1170: checkcast java/lang/String
      //   1173: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1176: pop
      //   1177: aload #8
      //   1179: ldc_w ':'
      //   1182: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1185: pop
      //   1186: aload #8
      //   1188: aload_3
      //   1189: invokeinterface getValue : ()Ljava/lang/Object;
      //   1194: checkcast java/lang/String
      //   1197: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1200: pop
      //   1201: ldc 'OppoThermalStats'
      //   1203: aload #8
      //   1205: invokevirtual toString : ()Ljava/lang/String;
      //   1208: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
      //   1211: pop
      //   1212: goto -> 1122
      //   1215: goto -> 1223
      //   1218: astore_3
      //   1219: aload_3
      //   1220: invokevirtual printStackTrace : ()V
      //   1223: aload_2
      //   1224: invokeinterface size : ()I
      //   1229: ifle -> 1239
      //   1232: ldc_w '040201'
      //   1235: aload_2
      //   1236: invokestatic onStamp : (Ljava/lang/String;Ljava/util/Map;)V
      //   1239: aload_1
      //   1240: monitorexit
      //   1241: return
      //   1242: astore_2
      //   1243: aload_1
      //   1244: monitorexit
      //   1245: aload_2
      //   1246: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #915	-> 0
      //   #917	-> 13
      //   #918	-> 23
      //   #920	-> 26
      //   #921	-> 41
      //   #923	-> 44
      //   #924	-> 61
      //   #926	-> 64
      //   #927	-> 77
      //   #928	-> 85
      //   #930	-> 88
      //   #931	-> 113
      //   #932	-> 138
      //   #933	-> 189
      //   #934	-> 240
      //   #935	-> 291
      //   #936	-> 342
      //   #937	-> 393
      //   #938	-> 444
      //   #939	-> 495
      //   #940	-> 520
      //   #941	-> 523
      //   #942	-> 526
      //   #944	-> 543
      //   #945	-> 549
      //   #948	-> 552
      //   #946	-> 555
      //   #947	-> 556
      //   #948	-> 587
      //   #950	-> 590
      //   #951	-> 600
      //   #952	-> 610
      //   #953	-> 627
      //   #955	-> 654
      //   #957	-> 663
      //   #958	-> 680
      //   #960	-> 707
      //   #962	-> 716
      //   #963	-> 733
      //   #965	-> 760
      //   #967	-> 769
      //   #968	-> 786
      //   #970	-> 813
      //   #972	-> 822
      //   #973	-> 839
      //   #975	-> 866
      //   #978	-> 875
      //   #979	-> 889
      //   #980	-> 945
      //   #982	-> 948
      //   #983	-> 957
      //   #984	-> 964
      //   #985	-> 1000
      //   #986	-> 1030
      //   #988	-> 1042
      //   #992	-> 1091
      //   #995	-> 1109
      //   #996	-> 1143
      //   #997	-> 1212
      //   #1000	-> 1215
      //   #998	-> 1218
      //   #999	-> 1219
      //   #1002	-> 1223
      //   #1003	-> 1232
      //   #1005	-> 1239
      //   #1006	-> 1241
      //   #1005	-> 1242
      // Exception table:
      //   from	to	target	type
      //   13	23	1242	finally
      //   23	25	1242	finally
      //   26	41	1242	finally
      //   41	43	1242	finally
      //   44	61	1242	finally
      //   61	63	1242	finally
      //   64	77	1242	finally
      //   77	85	1242	finally
      //   85	87	1242	finally
      //   88	113	1242	finally
      //   113	138	1242	finally
      //   138	189	1242	finally
      //   189	240	1242	finally
      //   240	291	1242	finally
      //   291	342	1242	finally
      //   342	393	1242	finally
      //   393	444	1242	finally
      //   444	495	1242	finally
      //   495	520	1242	finally
      //   526	531	1242	finally
      //   543	549	555	java/lang/InterruptedException
      //   543	549	1242	finally
      //   556	587	1242	finally
      //   590	600	1242	finally
      //   600	610	1242	finally
      //   610	627	1242	finally
      //   627	651	1242	finally
      //   654	663	1242	finally
      //   663	680	1242	finally
      //   680	704	1242	finally
      //   707	716	1242	finally
      //   716	733	1242	finally
      //   733	757	1242	finally
      //   760	769	1242	finally
      //   769	786	1242	finally
      //   786	810	1242	finally
      //   813	822	1242	finally
      //   822	839	1242	finally
      //   839	863	1242	finally
      //   866	875	1242	finally
      //   875	889	1242	finally
      //   889	902	1242	finally
      //   906	930	1242	finally
      //   934	945	1242	finally
      //   948	957	1242	finally
      //   957	964	1242	finally
      //   964	1000	1242	finally
      //   1000	1030	1242	finally
      //   1030	1039	1242	finally
      //   1042	1091	1242	finally
      //   1091	1109	1242	finally
      //   1109	1122	1218	java/lang/Exception
      //   1109	1122	1242	finally
      //   1122	1143	1218	java/lang/Exception
      //   1122	1143	1242	finally
      //   1143	1212	1218	java/lang/Exception
      //   1143	1212	1242	finally
      //   1219	1223	1242	finally
      //   1223	1232	1242	finally
      //   1232	1239	1242	finally
      //   1239	1241	1242	finally
      //   1243	1245	1242	finally
    }
  }
  
  public void setThermalState(OplusThermalState paramOplusThermalState) {
    if (DEBUG_THERMAL_TEMP)
      Slog.d("OppoThermalStats", "setThermalState"); 
    if (paramOplusThermalState == null)
      return; 
    this.mGlobalPlugType = paramOplusThermalState.getPlugType();
    this.mBatteryFcc = paramOplusThermalState.getFcc();
    this.mGlobalChargeId = paramOplusThermalState.getChargeId();
    this.mGlobalFast2Normal = paramOplusThermalState.getFast2Normal();
    this.mGlobalFastCharger = paramOplusThermalState.getIsFastCharge();
    this.mGlobalBatteryRealtimeCapacity = paramOplusThermalState.getBatteryRm();
    int i = paramOplusThermalState.getThermalHeat(0);
    int j = paramOplusThermalState.getThermalHeat(1);
    int k = paramOplusThermalState.getThermalHeat(2);
    int m = paramOplusThermalState.getThermalHeat(3);
    int n = paramOplusThermalState.getBatteryTemperature();
    int i1 = paramOplusThermalState.getBatteryLevel();
    setThermalInfo(this.mGlobalPlugType, n, i1, this.mGlobalBatteryRealtimeCapacity, i, j, k, m);
  }
  
  public void setThermalInfoInternal(Context paramContext, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10, int paramInt11, int paramInt12, int paramInt13, int paramInt14, int paramInt15, int paramInt16, boolean paramBoolean, int paramInt17) {
    if (DEBUG_THERMAL_TEMP) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setThermalInfo batteryVoltage:");
      stringBuilder.append(paramInt6);
      stringBuilder.append(" batteryCurrent:");
      stringBuilder.append(paramInt17);
      stringBuilder.append(" batteryTemp:");
      stringBuilder.append(paramInt5);
      stringBuilder.append(" plugType:");
      stringBuilder.append(paramInt3);
      stringBuilder.append(" fastCharge:");
      stringBuilder.append(paramBoolean);
      Slog.d("OppoThermalStats", stringBuilder.toString());
    } 
    this.mGlobalBatTemp = paramInt5;
    this.mBatteryFcc = paramInt9;
    this.mGlobalChargeId = paramInt16;
    this.mGlobalFast2Normal = paramInt15;
    this.mGlobalFastCharger = paramBoolean;
    this.mGlobalPlugType = paramInt3;
    this.mGlobalBatteryRealtimeCapacity = paramInt10;
    this.mGlobalBatteryVoltage = paramInt6;
    this.mGlobalBatteryCurrent = paramInt17;
    setThermalInfo(paramInt3, paramInt5, paramInt4, paramInt10, paramInt11, paramInt12, paramInt13, paramInt14);
  }
  
  private void setThermalInfo(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8) {
    // Byte code:
    //   0: getstatic com/android/internal/os/OplusThermalStatsHelper.DEBUG_THERMAL_TEMP : Z
    //   3: ifeq -> 132
    //   6: new java/lang/StringBuilder
    //   9: dup
    //   10: invokespecial <init> : ()V
    //   13: astore #9
    //   15: aload #9
    //   17: ldc_w 'setThermalInfo plug:'
    //   20: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   23: pop
    //   24: aload #9
    //   26: iload_1
    //   27: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   30: pop
    //   31: aload #9
    //   33: ldc_w '  phoneTemp:'
    //   36: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   39: pop
    //   40: aload #9
    //   42: iload #5
    //   44: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   47: pop
    //   48: aload #9
    //   50: ldc_w '  batTemp:'
    //   53: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   56: pop
    //   57: aload #9
    //   59: iload_2
    //   60: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   63: pop
    //   64: aload #9
    //   66: ldc_w '  level:'
    //   69: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   72: pop
    //   73: aload #9
    //   75: iload_3
    //   76: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   79: pop
    //   80: aload #9
    //   82: ldc_w '  mThermalFeatureOn='
    //   85: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   88: pop
    //   89: aload #9
    //   91: aload_0
    //   92: getfield mThermalFeatureOn : Z
    //   95: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   98: pop
    //   99: aload #9
    //   101: ldc_w '  mHeatThreshold:'
    //   104: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   107: pop
    //   108: aload #9
    //   110: aload_0
    //   111: getfield mHeatThreshold : I
    //   114: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   117: pop
    //   118: ldc 'OppoThermalStats'
    //   120: aload #9
    //   122: invokevirtual toString : ()Ljava/lang/String;
    //   125: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   128: pop
    //   129: goto -> 132
    //   132: aload_0
    //   133: getfield mThermalFeatureOn : Z
    //   136: ifne -> 140
    //   139: return
    //   140: invokestatic currentTimeMillis : ()J
    //   143: lstore #10
    //   145: invokestatic elapsedRealtime : ()J
    //   148: lstore #12
    //   150: aload_0
    //   151: lload #10
    //   153: aload_0
    //   154: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   157: getfield currentTime : J
    //   160: invokespecial checkCurrentTimeChanged : (JJ)Z
    //   163: ifeq -> 203
    //   166: aload_0
    //   167: getfield mHandler : Landroid/os/Handler;
    //   170: bipush #60
    //   172: invokevirtual hasMessages : (I)Z
    //   175: ifeq -> 187
    //   178: aload_0
    //   179: getfield mHandler : Landroid/os/Handler;
    //   182: bipush #60
    //   184: invokevirtual removeMessages : (I)V
    //   187: aload_0
    //   188: getfield mHandler : Landroid/os/Handler;
    //   191: bipush #60
    //   193: ldc2_w 4000
    //   196: invokevirtual sendEmptyMessageDelayed : (IJ)Z
    //   199: pop
    //   200: goto -> 203
    //   203: lload #10
    //   205: aload_0
    //   206: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   209: getfield currentTime : J
    //   212: lsub
    //   213: invokestatic abs : (J)J
    //   216: ldc2_w 900000
    //   219: lcmp
    //   220: ifle -> 234
    //   223: aload_0
    //   224: bipush #25
    //   226: aload_0
    //   227: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   230: iconst_1
    //   231: invokevirtual addThermalHistoryBufferLocked : (BLandroid/os/OplusBaseBatteryStats$ThermalItem;Z)V
    //   234: aload_0
    //   235: getfield mThermalBatteryTemp : Z
    //   238: ifeq -> 247
    //   241: iload_2
    //   242: istore #5
    //   244: goto -> 247
    //   247: aload_0
    //   248: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   251: iload_1
    //   252: putfield chargePlug : I
    //   255: aload_0
    //   256: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   259: iload_2
    //   260: putfield batTemp : I
    //   263: aload_0
    //   264: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   267: iload_3
    //   268: putfield batPercent : I
    //   271: aload_0
    //   272: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   275: iload #4
    //   277: putfield batRm : I
    //   280: aload_0
    //   281: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   284: iload #5
    //   286: putfield phoneTemp : I
    //   289: aload_0
    //   290: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   293: iload #6
    //   295: putfield phoneTemp1 : I
    //   298: aload_0
    //   299: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   302: iload #7
    //   304: putfield phoneTemp2 : I
    //   307: aload_0
    //   308: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   311: iload #8
    //   313: putfield phoneTemp3 : I
    //   316: aload_0
    //   317: getfield mThermalHistoryLastWritten : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   320: getfield phoneTemp : I
    //   323: sipush #-1023
    //   326: if_icmpeq -> 447
    //   329: aload_0
    //   330: getfield mThermalHistoryLastWritten : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   333: getfield chargePlug : I
    //   336: iload_1
    //   337: if_icmpne -> 447
    //   340: aload_0
    //   341: getfield mThermalHistoryLastWritten : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   344: getfield phoneTemp : I
    //   347: istore_1
    //   348: iload_1
    //   349: iload #5
    //   351: isub
    //   352: invokestatic abs : (I)I
    //   355: aload_0
    //   356: getfield mHeatRecInterv : I
    //   359: if_icmpgt -> 447
    //   362: aload_0
    //   363: getfield mThermalHistoryLastWritten : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   366: getfield phoneTemp1 : I
    //   369: istore_1
    //   370: iload_1
    //   371: iload #6
    //   373: isub
    //   374: invokestatic abs : (I)I
    //   377: aload_0
    //   378: getfield mHeatRecInterv : I
    //   381: if_icmpgt -> 447
    //   384: aload_0
    //   385: getfield mThermalHistoryLastWritten : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   388: getfield phoneTemp2 : I
    //   391: istore_1
    //   392: iload_1
    //   393: iload #7
    //   395: isub
    //   396: invokestatic abs : (I)I
    //   399: aload_0
    //   400: getfield mHeatRecInterv : I
    //   403: if_icmpgt -> 447
    //   406: aload_0
    //   407: getfield mThermalHistoryLastWritten : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   410: getfield phoneTemp3 : I
    //   413: istore_1
    //   414: iload_1
    //   415: iload #8
    //   417: isub
    //   418: invokestatic abs : (I)I
    //   421: aload_0
    //   422: getfield mHeatRecInterv : I
    //   425: if_icmpgt -> 447
    //   428: aload_0
    //   429: getfield mThermalHistoryLastWritten : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   432: getfield batPercent : I
    //   435: iload_3
    //   436: if_icmpeq -> 442
    //   439: goto -> 447
    //   442: iconst_0
    //   443: istore_1
    //   444: goto -> 449
    //   447: iconst_1
    //   448: istore_1
    //   449: lload #12
    //   451: aload_0
    //   452: getfield mHeatIncRatioStartTime : J
    //   455: lsub
    //   456: ldc2_w 60000
    //   459: lcmp
    //   460: iflt -> 469
    //   463: iconst_1
    //   464: istore #14
    //   466: goto -> 472
    //   469: iconst_0
    //   470: istore #14
    //   472: getstatic com/android/internal/os/OplusThermalStatsHelper.DEBUG_THERMAL_TEMP : Z
    //   475: ifeq -> 537
    //   478: new java/lang/StringBuilder
    //   481: dup
    //   482: invokespecial <init> : ()V
    //   485: astore #9
    //   487: aload #9
    //   489: ldc_w 'addThermalRatio:'
    //   492: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   495: pop
    //   496: aload #9
    //   498: iload #14
    //   500: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   503: pop
    //   504: aload #9
    //   506: ldc_w '  mHeatIncRatioStartTime:'
    //   509: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   512: pop
    //   513: aload #9
    //   515: aload_0
    //   516: getfield mHeatIncRatioStartTime : J
    //   519: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   522: pop
    //   523: ldc 'OppoThermalStats'
    //   525: aload #9
    //   527: invokevirtual toString : ()Ljava/lang/String;
    //   530: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   533: pop
    //   534: goto -> 537
    //   537: iload #14
    //   539: ifeq -> 891
    //   542: aload_0
    //   543: getfield mLastPhoneTemp : I
    //   546: istore_3
    //   547: iload_3
    //   548: sipush #-1023
    //   551: if_icmpeq -> 594
    //   554: aload_0
    //   555: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   558: iload #5
    //   560: iload_3
    //   561: isub
    //   562: ldc_w 60000
    //   565: imul
    //   566: i2l
    //   567: lload #12
    //   569: aload_0
    //   570: getfield mHeatIncRatioStartTime : J
    //   573: lsub
    //   574: ldiv
    //   575: l2i
    //   576: i2b
    //   577: putfield thermalRatio : B
    //   580: aload_0
    //   581: bipush #20
    //   583: aload_0
    //   584: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   587: iconst_1
    //   588: invokevirtual addThermalHistoryBufferLocked : (BLandroid/os/OplusBaseBatteryStats$ThermalItem;Z)V
    //   591: goto -> 594
    //   594: aload_0
    //   595: iload #5
    //   597: putfield mLastPhoneTemp : I
    //   600: aload_0
    //   601: getfield mLastPhoneTemp1 : I
    //   604: istore_3
    //   605: iload_3
    //   606: sipush #-1023
    //   609: if_icmpeq -> 652
    //   612: aload_0
    //   613: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   616: iload #6
    //   618: iload_3
    //   619: isub
    //   620: ldc_w 60000
    //   623: imul
    //   624: i2l
    //   625: lload #12
    //   627: aload_0
    //   628: getfield mHeatIncRatioStartTime : J
    //   631: lsub
    //   632: ldiv
    //   633: l2i
    //   634: i2b
    //   635: putfield thermalRatio1 : B
    //   638: aload_0
    //   639: bipush #21
    //   641: aload_0
    //   642: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   645: iconst_1
    //   646: invokevirtual addThermalHistoryBufferLocked : (BLandroid/os/OplusBaseBatteryStats$ThermalItem;Z)V
    //   649: goto -> 652
    //   652: iload_1
    //   653: istore_3
    //   654: aload_0
    //   655: iload #6
    //   657: putfield mLastPhoneTemp1 : I
    //   660: aload_0
    //   661: getfield mLastPhoneTemp2 : I
    //   664: istore_3
    //   665: iload_3
    //   666: sipush #-1023
    //   669: if_icmpeq -> 709
    //   672: aload_0
    //   673: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   676: iload #7
    //   678: iload_3
    //   679: isub
    //   680: ldc_w 60000
    //   683: imul
    //   684: i2l
    //   685: lload #12
    //   687: aload_0
    //   688: getfield mHeatIncRatioStartTime : J
    //   691: lsub
    //   692: ldiv
    //   693: l2i
    //   694: i2b
    //   695: putfield thermalRatio2 : B
    //   698: aload_0
    //   699: bipush #22
    //   701: aload_0
    //   702: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   705: iconst_1
    //   706: invokevirtual addThermalHistoryBufferLocked : (BLandroid/os/OplusBaseBatteryStats$ThermalItem;Z)V
    //   709: aload_0
    //   710: iload #7
    //   712: putfield mLastPhoneTemp2 : I
    //   715: aload_0
    //   716: getfield mLastPhoneTemp3 : I
    //   719: istore_3
    //   720: iload_3
    //   721: sipush #-1023
    //   724: if_icmpeq -> 764
    //   727: aload_0
    //   728: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   731: iload #8
    //   733: iload_3
    //   734: isub
    //   735: ldc_w 60000
    //   738: imul
    //   739: i2l
    //   740: lload #12
    //   742: aload_0
    //   743: getfield mHeatIncRatioStartTime : J
    //   746: lsub
    //   747: ldiv
    //   748: l2i
    //   749: i2b
    //   750: putfield thermalRatio3 : B
    //   753: aload_0
    //   754: bipush #23
    //   756: aload_0
    //   757: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   760: iconst_1
    //   761: invokevirtual addThermalHistoryBufferLocked : (BLandroid/os/OplusBaseBatteryStats$ThermalItem;Z)V
    //   764: aload_0
    //   765: iload #8
    //   767: putfield mLastPhoneTemp3 : I
    //   770: aload_0
    //   771: lload #12
    //   773: putfield mHeatIncRatioStartTime : J
    //   776: aload_0
    //   777: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   780: getfield thermalRatio : B
    //   783: iconst_4
    //   784: if_icmpge -> 811
    //   787: aload_0
    //   788: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   791: getfield phoneTemp : I
    //   794: aload_0
    //   795: getfield mPreHeatThreshold : I
    //   798: if_icmple -> 804
    //   801: goto -> 811
    //   804: ldc 'OppoThermalStats'
    //   806: astore #9
    //   808: goto -> 897
    //   811: new java/lang/StringBuilder
    //   814: dup
    //   815: invokespecial <init> : ()V
    //   818: astore #9
    //   820: aload #9
    //   822: ldc_w 'REPORT_UPDATE_CPU  ->  phoneTemp:'
    //   825: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   828: pop
    //   829: aload #9
    //   831: aload_0
    //   832: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   835: getfield phoneTemp : I
    //   838: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   841: pop
    //   842: aload #9
    //   844: ldc_w '  preHeatThreshold:'
    //   847: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   850: pop
    //   851: aload #9
    //   853: aload_0
    //   854: getfield mPreHeatThreshold : I
    //   857: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   860: pop
    //   861: aload #9
    //   863: invokevirtual toString : ()Ljava/lang/String;
    //   866: astore #9
    //   868: ldc 'OppoThermalStats'
    //   870: astore #15
    //   872: aload #15
    //   874: aload #9
    //   876: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   879: pop
    //   880: aload_0
    //   881: getfield mStats : Lcom/android/internal/os/OplusBaseBatteryStatsImpl;
    //   884: lconst_0
    //   885: invokevirtual schedulerUpdateCpu : (J)V
    //   888: goto -> 897
    //   891: ldc 'OppoThermalStats'
    //   893: astore #9
    //   895: iload_1
    //   896: istore_3
    //   897: iload_1
    //   898: ifeq -> 911
    //   901: aload_0
    //   902: iconst_1
    //   903: aload_0
    //   904: getfield mThermalHistoryCur : Landroid/os/OplusBaseBatteryStats$ThermalItem;
    //   907: iconst_1
    //   908: invokevirtual addThermalHistoryBufferLocked : (BLandroid/os/OplusBaseBatteryStats$ThermalItem;Z)V
    //   911: iload #5
    //   913: aload_0
    //   914: getfield mHeatThreshold : I
    //   917: if_icmplt -> 1275
    //   920: lload #12
    //   922: ldc2_w 300000
    //   925: lcmp
    //   926: ifle -> 1275
    //   929: getstatic com/android/internal/os/OplusThermalStatsHelper.DEBUG_THERMAL_TEMP : Z
    //   932: ifeq -> 944
    //   935: ldc 'OppoThermalStats'
    //   937: ldc_w 'thermal monitoring ...'
    //   940: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   943: pop
    //   944: aload_0
    //   945: getfield mHoldHeatTime : I
    //   948: istore_1
    //   949: iload_1
    //   950: ifge -> 961
    //   953: aload_0
    //   954: iconst_0
    //   955: putfield mHoldHeatTime : I
    //   958: goto -> 999
    //   961: aload_0
    //   962: getfield mHoldHeatElapsedRealtime : J
    //   965: lstore #10
    //   967: lload #12
    //   969: lload #10
    //   971: lsub
    //   972: ldc2_w 1800000
    //   975: lcmp
    //   976: ifge -> 999
    //   979: lload #10
    //   981: lconst_0
    //   982: lcmp
    //   983: ifle -> 999
    //   986: aload_0
    //   987: iload_1
    //   988: i2l
    //   989: lload #12
    //   991: lload #10
    //   993: lsub
    //   994: ladd
    //   995: l2i
    //   996: putfield mHoldHeatTime : I
    //   999: aload_0
    //   1000: lload #12
    //   1002: putfield mHoldHeatElapsedRealtime : J
    //   1005: getstatic com/android/internal/os/OplusThermalStatsHelper.DEBUG_THERMAL_TEMP : Z
    //   1008: ifeq -> 1088
    //   1011: new java/lang/StringBuilder
    //   1014: dup
    //   1015: invokespecial <init> : ()V
    //   1018: astore #9
    //   1020: aload #9
    //   1022: ldc_w 'mHoldHeatTime = '
    //   1025: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1028: pop
    //   1029: aload #9
    //   1031: aload_0
    //   1032: getfield mHoldHeatTime : I
    //   1035: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1038: pop
    //   1039: aload #9
    //   1041: ldc_w '  mStartAnalizyHeat='
    //   1044: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1047: pop
    //   1048: aload #9
    //   1050: aload_0
    //   1051: getfield mStartAnalizyHeat : Z
    //   1054: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   1057: pop
    //   1058: aload #9
    //   1060: ldc_w '  mHeatHoldUploadTime:'
    //   1063: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1066: pop
    //   1067: aload #9
    //   1069: aload_0
    //   1070: getfield mHeatHoldUploadTime : I
    //   1073: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1076: pop
    //   1077: ldc 'OppoThermalStats'
    //   1079: aload #9
    //   1081: invokevirtual toString : ()Ljava/lang/String;
    //   1084: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   1087: pop
    //   1088: aload_0
    //   1089: getfield mStartAnalizyHeat : Z
    //   1092: ifne -> 1215
    //   1095: aload_0
    //   1096: getfield mHoldHeat : Z
    //   1099: ifne -> 1215
    //   1102: aload_0
    //   1103: getfield mHoldHeatTime : I
    //   1106: aload_0
    //   1107: getfield mHeatHoldUploadTime : I
    //   1110: if_icmple -> 1215
    //   1113: aload_0
    //   1114: iconst_1
    //   1115: putfield mHoldHeat : Z
    //   1118: aload_0
    //   1119: iconst_1
    //   1120: putfield mStartAnalizyHeat : Z
    //   1123: aload_0
    //   1124: iload #5
    //   1126: putfield mGlobalMaxPhoneTemp : I
    //   1129: aload_0
    //   1130: iload_2
    //   1131: putfield mGlobalMaxBatTemp : I
    //   1134: aload_0
    //   1135: getfield mHeatReasonDetails : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
    //   1138: invokevirtual clear : ()V
    //   1141: new android/os/Message
    //   1144: dup
    //   1145: invokespecial <init> : ()V
    //   1148: astore #9
    //   1150: aload #9
    //   1152: bipush #59
    //   1154: putfield what : I
    //   1157: aload #9
    //   1159: lload #12
    //   1161: ldc2_w 720000
    //   1164: lsub
    //   1165: invokestatic valueOf : (J)Ljava/lang/Long;
    //   1168: putfield obj : Ljava/lang/Object;
    //   1171: aload #9
    //   1173: aload_0
    //   1174: getfield mHeatReasonDetails : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
    //   1177: getfield mAnalizyPosition : I
    //   1180: iconst_1
    //   1181: isub
    //   1182: putfield arg1 : I
    //   1185: aload_0
    //   1186: getfield mHeatReasonDetails : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
    //   1189: iload #5
    //   1191: putfield mPhoneTemp : I
    //   1194: aload_0
    //   1195: getfield mHeatReasonDetails : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
    //   1198: iload_2
    //   1199: putfield mBatTemp : I
    //   1202: aload_0
    //   1203: getfield mHandler : Landroid/os/Handler;
    //   1206: aload #9
    //   1208: invokevirtual sendMessage : (Landroid/os/Message;)Z
    //   1211: pop
    //   1212: goto -> 1215
    //   1215: aload_0
    //   1216: getfield mGlobalMaxPhoneTemp : I
    //   1219: iload #5
    //   1221: if_icmpge -> 1230
    //   1224: aload_0
    //   1225: iload #5
    //   1227: putfield mGlobalMaxPhoneTemp : I
    //   1230: aload_0
    //   1231: getfield mGlobalMaxBatTemp : I
    //   1234: iload_2
    //   1235: if_icmpge -> 1243
    //   1238: aload_0
    //   1239: iload_2
    //   1240: putfield mGlobalMaxBatTemp : I
    //   1243: aload_0
    //   1244: getfield mHoldHeat : Z
    //   1247: ifeq -> 1467
    //   1250: aload_0
    //   1251: getfield mStartAnalizyHeat : Z
    //   1254: ifne -> 1467
    //   1257: aload_0
    //   1258: getfield mHeatReasonDetails : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
    //   1261: aload_0
    //   1262: getfield mGlobalMaxPhoneTemp : I
    //   1265: aload_0
    //   1266: getfield mGlobalMaxBatTemp : I
    //   1269: invokevirtual putHeatMaxTemp : (II)V
    //   1272: goto -> 1467
    //   1275: lload #12
    //   1277: aload_0
    //   1278: getfield mHoldHeatElapsedRealtime : J
    //   1281: lsub
    //   1282: aload_0
    //   1283: getfield mHeatHoldTimeThreshold : I
    //   1286: i2l
    //   1287: lcmp
    //   1288: ifle -> 1376
    //   1291: getstatic com/android/internal/os/OplusThermalStatsHelper.DEBUG_THERMAL_TEMP : Z
    //   1294: ifeq -> 1353
    //   1297: new java/lang/StringBuilder
    //   1300: dup
    //   1301: invokespecial <init> : ()V
    //   1304: astore #9
    //   1306: aload #9
    //   1308: ldc_w 'phoneTemp monitor exit, phone temp:'
    //   1311: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1314: pop
    //   1315: aload #9
    //   1317: iload #5
    //   1319: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1322: pop
    //   1323: aload #9
    //   1325: ldc_w ' mHeatHoldTimeThreshold:'
    //   1328: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1331: pop
    //   1332: aload #9
    //   1334: aload_0
    //   1335: getfield mHeatHoldTimeThreshold : I
    //   1338: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1341: pop
    //   1342: ldc 'OppoThermalStats'
    //   1344: aload #9
    //   1346: invokevirtual toString : ()Ljava/lang/String;
    //   1349: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   1352: pop
    //   1353: aload_0
    //   1354: iconst_0
    //   1355: putfield mHoldHeat : Z
    //   1358: aload_0
    //   1359: iconst_m1
    //   1360: putfield mHoldHeatTime : I
    //   1363: aload_0
    //   1364: lconst_0
    //   1365: putfield mHoldHeatElapsedRealtime : J
    //   1368: aload_0
    //   1369: iconst_0
    //   1370: putfield mHaveCaptured : Z
    //   1373: goto -> 1467
    //   1376: getstatic com/android/internal/os/OplusThermalStatsHelper.DEBUG_THERMAL_TEMP : Z
    //   1379: ifeq -> 1419
    //   1382: new java/lang/StringBuilder
    //   1385: dup
    //   1386: invokespecial <init> : ()V
    //   1389: astore #9
    //   1391: aload #9
    //   1393: ldc_w 'phoneTemp is decreasing, phoneTemp:'
    //   1396: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1399: pop
    //   1400: aload #9
    //   1402: iload #5
    //   1404: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1407: pop
    //   1408: ldc 'OppoThermalStats'
    //   1410: aload #9
    //   1412: invokevirtual toString : ()Ljava/lang/String;
    //   1415: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   1418: pop
    //   1419: aload_0
    //   1420: getfield mHoldHeat : Z
    //   1423: ifeq -> 1462
    //   1426: aload_0
    //   1427: getfield mStartAnalizyHeat : Z
    //   1430: ifne -> 1462
    //   1433: aload_0
    //   1434: getfield mHoldHeatTime : I
    //   1437: ifle -> 1462
    //   1440: getstatic com/android/internal/os/OplusThermalStatsHelper.DEBUG_THERMAL_TEMP : Z
    //   1443: ifeq -> 1455
    //   1446: ldc 'OppoThermalStats'
    //   1448: ldc_w 'uploadHeatEvent now'
    //   1451: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   1454: pop
    //   1455: aload_0
    //   1456: getfield mHeatReasonDetails : Lcom/android/internal/os/OplusThermalStatsHelper$HeatReasonDetails;
    //   1459: invokevirtual uploadHeatEvent : ()V
    //   1462: aload_0
    //   1463: iconst_m1
    //   1464: putfield mHoldHeatTime : I
    //   1467: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1077	-> 0
    //   #1078	-> 6
    //   #1077	-> 132
    //   #1080	-> 132
    //   #1081	-> 139
    //   #1083	-> 140
    //   #1084	-> 145
    //   #1085	-> 150
    //   #1086	-> 150
    //   #1087	-> 150
    //   #1088	-> 166
    //   #1089	-> 178
    //   #1091	-> 187
    //   #1087	-> 203
    //   #1093	-> 203
    //   #1094	-> 223
    //   #1096	-> 234
    //   #1097	-> 241
    //   #1096	-> 247
    //   #1099	-> 247
    //   #1100	-> 255
    //   #1101	-> 263
    //   #1102	-> 271
    //   #1103	-> 280
    //   #1104	-> 289
    //   #1105	-> 298
    //   #1106	-> 307
    //   #1107	-> 316
    //   #1109	-> 348
    //   #1110	-> 370
    //   #1111	-> 392
    //   #1112	-> 414
    //   #1114	-> 449
    //   #1115	-> 472
    //   #1116	-> 478
    //   #1115	-> 537
    //   #1118	-> 537
    //   #1119	-> 542
    //   #1120	-> 554
    //   #1121	-> 580
    //   #1119	-> 594
    //   #1123	-> 594
    //   #1124	-> 600
    //   #1125	-> 612
    //   #1126	-> 638
    //   #1124	-> 652
    //   #1128	-> 652
    //   #1129	-> 660
    //   #1130	-> 672
    //   #1131	-> 698
    //   #1133	-> 709
    //   #1134	-> 715
    //   #1135	-> 727
    //   #1136	-> 753
    //   #1138	-> 764
    //   #1139	-> 770
    //   #1140	-> 776
    //   #1142	-> 811
    //   #1144	-> 880
    //   #1118	-> 891
    //   #1147	-> 897
    //   #1148	-> 901
    //   #1151	-> 911
    //   #1153	-> 929
    //   #1154	-> 935
    //   #1156	-> 944
    //   #1157	-> 953
    //   #1159	-> 961
    //   #1160	-> 986
    //   #1163	-> 999
    //   #1164	-> 1005
    //   #1165	-> 1011
    //   #1167	-> 1088
    //   #1168	-> 1113
    //   #1169	-> 1118
    //   #1170	-> 1123
    //   #1171	-> 1129
    //   #1172	-> 1134
    //   #1173	-> 1134
    //   #1174	-> 1141
    //   #1175	-> 1150
    //   #1176	-> 1157
    //   #1177	-> 1171
    //   #1178	-> 1185
    //   #1179	-> 1194
    //   #1180	-> 1202
    //   #1167	-> 1215
    //   #1182	-> 1215
    //   #1183	-> 1224
    //   #1185	-> 1230
    //   #1186	-> 1238
    //   #1188	-> 1243
    //   #1189	-> 1257
    //   #1151	-> 1275
    //   #1191	-> 1275
    //   #1192	-> 1291
    //   #1193	-> 1297
    //   #1195	-> 1353
    //   #1196	-> 1358
    //   #1197	-> 1363
    //   #1198	-> 1368
    //   #1200	-> 1376
    //   #1201	-> 1382
    //   #1203	-> 1419
    //   #1204	-> 1440
    //   #1205	-> 1446
    //   #1207	-> 1455
    //   #1209	-> 1462
    //   #1211	-> 1467
  }
  
  private boolean checkCurrentTimeChanged(long paramLong1, long paramLong2) {
    if (paramLong2 - paramLong1 > 7200000L && paramLong2 > 1471228928L)
      return true; 
    return false;
  }
  
  public void setThermalConfig() {
    if (OplusThermalManager.mThermalFeatureOn != this.mThermalFeatureOn)
      if (OplusThermalManager.mThermalFeatureOn) {
        this.mThermalHistoryCur.clear();
        this.mThermalHistoryCur.cmd = 0;
        addThermalHistoryBufferLocked((byte)0, this.mThermalHistoryCur, true);
        this.mThermalFeatureOn = OplusThermalManager.mThermalFeatureOn;
        this.mThermalUploadDcs = OplusThermalManager.mThermalUploadDcs;
        this.mThermalUploadLog = OplusThermalManager.mThermalUploadLog;
        this.mThermalCaptureLog = OplusThermalManager.mThermalCaptureLog;
        this.mRecordThermalHistory = OplusThermalManager.mRecordThermalHistory;
        this.mThermalCaptureLogThreshold = OplusThermalManager.mThermalCaptureLogThreshold;
        this.mThermalUploadErrLog = OplusThermalManager.mThermalUploadErrLog;
        this.mMonitorAppLimitTime = OplusThermalManager.mMonitorAppLimitTime;
        this.mHeatHoldTimeThreshold = OplusThermalManager.mHeatHoldTimeThreshold;
        this.mThermalBatteryTemp = OplusThermalManager.mThermalBatteryTemp;
        this.mThermalMonitorApp.clear();
        this.mThermalMonitorApp.addAll(OplusThermalManager.mMonitorAppList);
        if (this.mHandler.hasMessages(60))
          this.mHandler.removeMessages(60); 
        this.mHandler.sendEmptyMessageDelayed(60, 4000L);
        if (this.mHandler.hasMessages(62))
          this.mHandler.removeMessages(62); 
        this.mHandler.sendEmptyMessageDelayed(62, 15000L);
      } else {
        this.mThermalFeatureOn = OplusThermalManager.mThermalFeatureOn;
        this.mThermalUploadDcs = false;
        this.mThermalUploadLog = false;
        this.mThermalCaptureLog = false;
        this.mRecordThermalHistory = false;
        this.mThermalUploadErrLog = false;
        this.mThermalMonitorApp.clear();
        clearThermalStatsBuffer();
        cancleUploadAlarm();
      }  
    this.mMoreHeatThreshold = OplusThermalManager.mMoreHeatThreshold;
    this.mHeatThreshold = OplusThermalManager.mHeatThreshold;
    this.mLessHeatThreshold = OplusThermalManager.mLessHeatThreshold;
    this.mPreHeatThreshold = OplusThermalManager.mPreHeatThreshold;
    this.mHeatIncRatioThreshold = OplusThermalManager.mHeatIncRatioThreshold;
    this.mHeatHoldTimeThreshold = OplusThermalManager.mHeatHoldTimeThreshold;
    this.mHeatHoldUploadTime = OplusThermalManager.mHeatHoldUploadTime;
    this.mHeatRecInterv = OplusThermalManager.mHeatRecInterv;
    this.mCpuLoadRecThreshold = OplusThermalManager.mCpuLoadRecThreshold;
    this.mCpuLoadRecInterv = OplusThermalManager.mCpuLoadRecInterv;
    this.mTopCpuRecThreshold = OplusThermalManager.mTopCpuRecThreshold;
    this.mTopCpuRecInterv = OplusThermalManager.mTopCpuRecInterv;
    this.mCaptureCpuFeqInterVal = OplusThermalManager.mHeatTopProInterval * 1000L;
    this.mSimpleTopProInterVal = OplusThermalManager.mHeatTopProInterval * 1000L;
  }
  
  public void resetThermalHistory() {
    this.mThermalHistoryCur.cmd = 19;
    addThermalHistoryBufferLocked((byte)19, this.mThermalHistoryCur, true);
  }
  
  public void setThermalHeatThreshold(PrintWriter paramPrintWriter, int paramInt) {
    this.mHeatThreshold = paramInt;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Battery set heatthreshold mHeatThreshold = ");
    stringBuilder.append(Integer.toString(paramInt));
    paramPrintWriter.println(stringBuilder.toString());
  }
  
  public void toggleThermalDebugSwith(PrintWriter paramPrintWriter, int paramInt) {
    if (paramInt == 1) {
      DEBUG_THERMAL_TEMP = true;
    } else {
      DEBUG_THERMAL_TEMP = false;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Battery set debug switch = ");
    stringBuilder.append(DEBUG_THERMAL_TEMP);
    paramPrintWriter.println(stringBuilder.toString());
  }
  
  public void setHeatBetweenTime(PrintWriter paramPrintWriter, int paramInt) {
    this.mHeatHoldTimeThreshold = paramInt;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Battery set heatBetweenTime = ");
    stringBuilder.append(Integer.toString(this.mHeatHoldTimeThreshold));
    paramPrintWriter.println(stringBuilder.toString());
  }
  
  public void setMonitorAppLimitTime(PrintWriter paramPrintWriter, int paramInt) {
    this.mMonitorAppLimitTime = paramInt;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Battery set setMonitorAppLimitTime = ");
    stringBuilder.append(Integer.toString(paramInt));
    paramPrintWriter.println(stringBuilder.toString());
  }
  
  public void getMonitorAppLocked(PrintWriter paramPrintWriter) {
    for (String str : this.mThermalMonitorApp) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getMonitorApp:");
      stringBuilder.append(str);
      paramPrintWriter.println(stringBuilder.toString());
    } 
  }
  
  public void printChargeMapLocked(PrintWriter paramPrintWriter) {
    try {
      for (Map.Entry<String, String> entry : this.mChargeUploadMap.entrySet()) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("mChargeUploadMap ");
        stringBuilder.append((String)entry.getKey());
        stringBuilder.append(":");
        stringBuilder.append((String)entry.getValue());
        paramPrintWriter.println(stringBuilder.toString());
      } 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public void printThermalHeatThreshold(PrintWriter paramPrintWriter) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Battery get heatthreshold mHeatThreshold = ");
    stringBuilder.append(Integer.toString(this.mHeatThreshold));
    paramPrintWriter.println(stringBuilder.toString());
  }
  
  public void printThermalUploadTemp(PrintWriter paramPrintWriter) {
    Map<String, String> map = getUploadThermalTemp();
    if (map.size() > 0) {
      try {
        for (Map.Entry<String, String> entry : map.entrySet()) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("uploadThermalTemp ");
          stringBuilder.append((String)entry.getKey());
          stringBuilder.append(":");
          stringBuilder.append((String)entry.getValue());
          paramPrintWriter.println(stringBuilder.toString());
        } 
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
    } else {
      exception.println("no upload message");
    } 
  }
  
  public void setThermalCpuLoading(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, String paramString1, String paramString2) {
    if (DEBUG_THERMAL_TEMP) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setThermalCpuLoading: mThermalFeatureOn");
      stringBuilder.append(this.mThermalFeatureOn);
      stringBuilder.append(" load1:");
      stringBuilder.append(paramInt1);
      stringBuilder.append(" load5:");
      stringBuilder.append(paramInt2);
      stringBuilder.append(" load15:");
      stringBuilder.append(paramInt3);
      stringBuilder.append(" cpuLoading:");
      stringBuilder.append(paramInt4);
      stringBuilder.append(" maxCpu:");
      stringBuilder.append(paramInt5);
      stringBuilder.append(" cpuProc:");
      stringBuilder.append(paramString1);
      stringBuilder.append("  phoneTemp:");
      stringBuilder.append(this.mThermalHistoryCur.phoneTemp);
      stringBuilder.append("  simpleTopProc:");
      stringBuilder.append(paramString2);
      Slog.d("OppoThermalStats", stringBuilder.toString());
    } 
    paramInt1 = this.mThermalHistoryCur.phoneTemp;
    if (this.mThermalHistoryCur.phoneTemp >= this.mPreHeatThreshold - 20 && 
      DEBUG_THERMAL_TEMP) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("SimpleTopProcesses: ");
      stringBuilder.append(paramString2);
      Slog.d("OppoThermalStats", stringBuilder.toString());
    } 
    this.mSimpleTopProcesses = paramString2;
    if (paramInt1 >= this.mPreHeatThreshold && !this.mHaveCaptured) {
      this.mSimpleTopProcessesNeedUpload = paramString2;
      if (DEBUG_THERMAL_TEMP) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("mSimpleTopProcessesNeedUpload: ");
        stringBuilder.append(this.mSimpleTopProcessesNeedUpload);
        Slog.d("OppoThermalStats", stringBuilder.toString());
      } 
      if (this.cpuFreqReader != null && SystemClock.elapsedRealtime() - this.mCaptureCpuFeqElapsRealtime > this.mCaptureCpuFeqInterVal) {
        this.mCaptureCpuFeqElapsRealtime = SystemClock.elapsedRealtime();
        this.mCpuFreqValues = paramString2 = this.cpuFreqReader.getSimpleCpuFreqInfor();
        if (paramString2 != null) {
          SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
          String str2 = simpleDateFormat.format(new Date(System.currentTimeMillis()));
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(this.mCpuFreqValues);
          stringBuilder.append("[");
          stringBuilder.append(str2);
          stringBuilder.append("]");
          String str1 = stringBuilder.toString();
          this.mCpuFreqValuesNeedUpload = str1;
          if (DEBUG_THERMAL_TEMP) {
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("cpuFreqs: ");
            stringBuilder1.append(this.mCpuFreqValues);
            Slog.d("OppoThermalStats", stringBuilder1.toString());
          } 
        } 
      } 
      this.mHaveCaptured = true;
    } else if (paramInt1 < this.mPreHeatThreshold) {
      if (DEBUG_THERMAL_TEMP) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("reset mHaveCaptured:");
        stringBuilder.append(this.mHaveCaptured);
        Slog.d("OppoThermalStats", stringBuilder.toString());
      } 
      this.mHaveCaptured = false;
    } 
    if (!this.mThermalFeatureOn)
      return; 
    this.mThermalHistoryCur.cpuLoading = paramInt4;
    calculateEnviTemp(paramInt2, paramInt3);
    if (paramInt5 <= this.mTopCpuRecThreshold && Math.abs(paramInt5 - this.mThermalHistoryCur.topCpu) <= this.mTopCpuRecInterv) {
      if (this.mThermalHistoryCur.topProc != null && paramString1 != null) {
        paramString2 = this.mThermalHistoryCur.topProc;
        if (!paramString2.equals(paramString1)) {
          this.mThermalHistoryCur.topCpu = paramInt5;
          this.mThermalHistoryCur.topProc = paramString1;
          addThermalHistoryBufferLocked((byte)18, this.mThermalHistoryCur, true);
          return;
        } 
      } 
      return;
    } 
    this.mThermalHistoryCur.topCpu = paramInt5;
    this.mThermalHistoryCur.topProc = paramString1;
    addThermalHistoryBufferLocked((byte)18, this.mThermalHistoryCur, true);
  }
  
  private void calculateEnviTemp(int paramInt1, int paramInt2) {
    if (this.mThermalHistoryCur.chargePlug > 0 || this.mThermalHistoryCur.flashlightOn)
      this.mCpuIdleCheckCount = 0; 
    long l = SystemClock.elapsedRealtime();
    if (l < 900000L)
      this.mCpuIdleCheckCount = 0; 
    if (paramInt1 <= 100 && paramInt2 <= 100) {
      this.mCpuIdleCheckCount++;
    } else {
      this.mCpuIdleCheckCount = 0;
    } 
    if (this.mCpuIdleCheckCount >= 3) {
      this.mCpuIdleCheckCount = 0;
      SystemClock.uptimeMillis();
      this.mThermalHistoryCur.enviTemp = this.mGlobalBatTemp;
      addThermalHistoryBufferLocked((byte)24, this.mThermalHistoryCur, true);
    } 
  }
  
  public void addThermalHistoryBufferLocked(byte paramByte, int paramInt, OplusBaseBatteryStats.ThermalItem paramThermalItem, boolean paramBoolean) {
    synchronized (this.mLock) {
      if (this.mIteratingThermalHistory)
        return; 
      paramThermalItem.cmd = paramByte;
      paramThermalItem.upTime = SystemClock.uptimeMillis();
      paramThermalItem.elapsedRealtime = SystemClock.elapsedRealtime();
      paramThermalItem.backlight = paramInt;
      if (paramByte == 0 || paramByte == 19 || paramByte == 25) {
        paramThermalItem.currentTime = System.currentTimeMillis();
        paramThermalItem.baseElapsedRealtime = paramThermalItem.elapsedRealtime;
      } 
      this.mThermalHistoryBufferLastPos = this.mThermalHistoryBuffer.dataPosition();
      this.mThermalHistoryLastLastWritten.setTo(this.mThermalHistoryLastWritten);
      this.mThermalHistoryLastWritten.setTo(paramThermalItem);
      writeThermalHistoryDelta(paramByte, this.mThermalHistoryBuffer, this.mThermalHistoryLastWritten, this.mThermalHistoryLastLastWritten, paramBoolean);
      return;
    } 
  }
  
  public void addThermalHistoryBufferLocked(byte paramByte, OplusBaseBatteryStats.ThermalItem paramThermalItem, boolean paramBoolean) {
    boolean bool;
    if (this.mStats.isScreenOn()) {
      bool = this.mScreenBrightness;
    } else {
      bool = false;
    } 
    synchronized (this.mLock) {
      if (this.mIteratingThermalHistory && paramThermalItem.cmd != 19)
        return; 
      paramThermalItem.cmd = paramByte;
      paramThermalItem.backlight = bool;
      paramThermalItem.upTime = SystemClock.uptimeMillis();
      paramThermalItem.elapsedRealtime = SystemClock.elapsedRealtime();
      if (paramByte == 0 || paramByte == 19 || paramByte == 25) {
        paramThermalItem.currentTime = System.currentTimeMillis();
        paramThermalItem.baseElapsedRealtime = paramThermalItem.elapsedRealtime;
      } 
      this.mThermalHistoryBufferLastPos = this.mThermalHistoryBuffer.dataPosition();
      this.mThermalHistoryLastLastWritten.setTo(this.mThermalHistoryLastWritten);
      this.mThermalHistoryLastWritten.setTo(paramThermalItem);
      if (this.mThermalUploadDcs)
        collectThermalTempMap(this.mThermalHistoryLastWritten, this.mThermalHistoryLastLastWritten); 
      if (!this.mStartAnalizyHeat && paramBoolean)
        this.mHeatReasonDetails.addToHeatItem(this.mThermalHistoryLastWritten); 
      collectMoinitAppMap(this.mThermalHistoryLastWritten, this.mThermalHistoryLastLastWritten);
      collectChargeMap(this.mThermalHistoryLastWritten, this.mThermalHistoryLastLastWritten);
      writeThermalHistoryDelta(paramByte, this.mThermalHistoryBuffer, this.mThermalHistoryLastWritten, this.mThermalHistoryLastLastWritten, paramBoolean);
      return;
    } 
  }
  
  public void writeThermalHistoryDelta(byte paramByte, Parcel paramParcel, OplusBaseBatteryStats.ThermalItem paramThermalItem1, OplusBaseBatteryStats.ThermalItem paramThermalItem2, boolean paramBoolean) {
    int i;
    if (DEBUG_THERMAL_TEMP && 
      this.mThermalHistoryBuffer != null && this.mThermalBuilder != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("mThermalBuilder size=");
      stringBuilder.append(this.mThermalBuilder.length());
      stringBuilder.append(" mThermalHistoryBuffer size=");
      stringBuilder.append(this.mThermalHistoryBuffer.dataSize());
      stringBuilder.append("  mRecordThermalHistory=");
      stringBuilder.append(this.mRecordThermalHistory);
      Slog.d("OppoThermalStats", stringBuilder.toString());
    } 
    if (!this.mRecordThermalHistory || this.mThermalBuilder == null)
      return; 
    if (paramThermalItem2 == null || paramByte == 0) {
      i = paramThermalItem1.backlight;
      int m = paramThermalItem1.cpuLoading;
      i = 0xFF000000 & paramThermalItem1.cmd << 24 | 0xFFC000 & m << 14 | i & 0x3FFF;
      paramParcel.writeInt(i);
      addThermalDetalToStringBuilder(i, paramBoolean, false);
      long l5 = paramThermalItem1.elapsedRealtime, l4 = (paramThermalItem1.cameraOn << 4), l6 = (paramThermalItem1.audioOn << 3);
      l6 = l5 << 5L | l4 | l6 | (paramThermalItem1.videoOn << 2) | (paramThermalItem1.gpsOn << 1) | paramThermalItem1.flashlightOn;
      paramParcel.writeLong(l6);
      addThermalDetalToStringBuilder(l6, paramBoolean, false);
      paramParcel.writeLong(paramThermalItem1.upTime << 8L | (paramThermalItem1.volume & 0xFF));
      l6 = paramThermalItem1.upTime;
      l5 = (paramThermalItem1.volume & 0x7F);
      if (paramThermalItem1.isAutoBrightness) {
        i = 128;
      } else {
        i = 0;
      } 
      addThermalDetalToStringBuilder(l6 << 8L | l5 | i, paramBoolean, false);
      paramParcel.writeLong(paramThermalItem1.currentTime);
      addThermalDetalToStringBuilder(paramThermalItem1.currentTime, paramBoolean, false);
      paramParcel.writeLong(paramThermalItem1.baseElapsedRealtime);
      addThermalDetalToStringBuilder(paramThermalItem1.baseElapsedRealtime, paramBoolean, true);
      return;
    } 
    if (paramThermalItem1.phoneTemp == -1023)
      return; 
    int j = paramThermalItem1.backlight, k = paramThermalItem1.cpuLoading;
    k = 0xFF000000 & paramThermalItem1.cmd << 24 | 0xFFC000 & k << 14 | j & 0x3FFF;
    paramParcel.writeInt(k);
    addThermalDetalToStringBuilder(k, paramBoolean, false);
    long l1 = paramThermalItem1.elapsedRealtime, l2 = (paramThermalItem1.cameraOn << 4), l3 = (paramThermalItem1.audioOn << 3);
    l3 = l1 << 5L | l2 | l3 | (paramThermalItem1.videoOn << 2) | (paramThermalItem1.gpsOn << 1) | paramThermalItem1.flashlightOn;
    paramParcel.writeLong(l3);
    addThermalDetalToStringBuilder(l3, paramBoolean, false);
    l2 = paramThermalItem1.upTime;
    l3 = (paramThermalItem1.volume & 0x7F);
    if (paramThermalItem1.isAutoBrightness) {
      k = 128;
    } else {
      k = 0;
    } 
    paramParcel.writeLong(l3 | l2 << 8L | k);
    l3 = paramThermalItem1.upTime;
    l2 = (paramThermalItem1.volume & 0x7F);
    if (paramThermalItem1.isAutoBrightness) {
      k = 128;
    } else {
      k = 0;
    } 
    addThermalDetalToStringBuilder(l3 << 8L | l2 | k, paramBoolean, false);
    if (i != 0)
      if (i == 1) {
        i = buildThermalBatteryInfo(paramThermalItem1);
        paramParcel.writeInt(i);
        addThermalDetalToStringBuilder(i, paramBoolean, false);
        l3 = buildThermalTempInfo(paramThermalItem1);
        paramParcel.writeLong(l3);
        addThermalDetalToStringBuilder(l3, paramBoolean, true);
      } else if (i == 3) {
        addThermalDetalToStringBuilder("", paramBoolean, true);
      } else if (i == 4) {
        i = paramThermalItem1.wifiStats;
        i = 0xFFFF & paramThermalItem1.wifiSignal | i << 16 & 0xFFFF0000;
        paramParcel.writeInt(i);
        addThermalDetalToStringBuilder(i, paramBoolean, true);
      } else if (i == 5) {
        paramParcel.writeBoolean(paramThermalItem1.phoneOnff);
        addThermalDetalToStringBuilder(paramThermalItem1.phoneOnff, paramBoolean, true);
      } else if (i == 6) {
        paramParcel.writeByte(paramThermalItem1.phoneState);
        addThermalDetalToStringBuilder(paramThermalItem1.phoneState, paramBoolean, true);
      } else if (i == 7) {
        paramParcel.writeByte(paramThermalItem1.phoneSignal);
        addThermalDetalToStringBuilder(paramThermalItem1.phoneSignal, paramBoolean, true);
      } else if (i == 8) {
        paramParcel.writeBoolean(paramThermalItem1.dataNetStatus);
        addThermalDetalToStringBuilder(paramThermalItem1.dataNetStatus, paramBoolean, true);
      } else if (i == 9) {
        paramParcel.writeByte(paramThermalItem1.connectNetType);
        addThermalDetalToStringBuilder(paramThermalItem1.connectNetType, paramBoolean, true);
      } else if (i == 10) {
        paramParcel.writeBoolean(paramThermalItem1.cameraOn);
        addThermalDetalToStringBuilder(paramThermalItem1.cameraOn, paramBoolean, true);
      } else if (i == 11) {
        paramParcel.writeBoolean(paramThermalItem1.audioOn);
        addThermalDetalToStringBuilder(paramThermalItem1.audioOn, paramBoolean, true);
      } else if (i == 12) {
        paramParcel.writeBoolean(paramThermalItem1.videoOn);
        addThermalDetalToStringBuilder(paramThermalItem1.videoOn, paramBoolean, true);
      } else if (i == 13) {
        paramParcel.writeBoolean(paramThermalItem1.gpsOn);
        addThermalDetalToStringBuilder(paramThermalItem1.gpsOn, paramBoolean, true);
      } else if (i == 14) {
        paramParcel.writeBoolean(paramThermalItem1.flashlightOn);
        addThermalDetalToStringBuilder(paramThermalItem1.flashlightOn, paramBoolean, true);
      } else if (i == 15) {
        paramParcel.writeString(paramThermalItem1.jobSchedule);
        addThermalDetalToStringBuilder(paramThermalItem1.jobSchedule, paramBoolean, true);
      } else if (i == 16) {
        paramParcel.writeString(paramThermalItem1.netSync);
        addThermalDetalToStringBuilder(paramThermalItem1.netSync, paramBoolean, true);
      } else if (i == 17) {
        paramParcel.writeString(paramThermalItem1.foreProc);
        addThermalDetalToStringBuilder(paramThermalItem1.foreProc, paramBoolean, false);
        paramParcel.writeString(paramThermalItem1.versionName);
        addThermalDetalToStringBuilder(paramThermalItem1.versionName, paramBoolean, true);
      } else if (i == 18) {
        paramParcel.writeString(paramThermalItem1.topProc);
        addThermalDetalToStringBuilder(paramThermalItem1.topProc, paramBoolean, false);
        paramParcel.writeInt(paramThermalItem1.topCpu);
        addThermalDetalToStringBuilder(paramThermalItem1.topCpu, paramBoolean, true);
      } else if (i == 19) {
        paramParcel.writeLong(paramThermalItem1.currentTime);
        addThermalDetalToStringBuilder(paramThermalItem1.currentTime, paramBoolean, false);
        paramParcel.writeLong(paramThermalItem1.baseElapsedRealtime);
        addThermalDetalToStringBuilder(paramThermalItem1.baseElapsedRealtime, paramBoolean, false);
        i = buildThermalBatteryInfo(paramThermalItem1);
        paramParcel.writeInt(i);
        addThermalDetalToStringBuilder(i, paramBoolean, false);
        l3 = buildThermalTempInfo(paramThermalItem1);
        paramParcel.writeLong(l3);
        addThermalDetalToStringBuilder(l3, paramBoolean, false);
        paramParcel.writeByte(paramThermalItem1.thermalRatio);
        addThermalDetalToStringBuilder(paramThermalItem1.thermalRatio, paramBoolean, false);
        paramParcel.writeByte(paramThermalItem1.thermalRatio1);
        addThermalDetalToStringBuilder(paramThermalItem1.thermalRatio1, paramBoolean, false);
        paramParcel.writeByte(paramThermalItem1.thermalRatio2);
        addThermalDetalToStringBuilder(paramThermalItem1.thermalRatio2, paramBoolean, false);
        paramParcel.writeByte(paramThermalItem1.thermalRatio3);
        addThermalDetalToStringBuilder(paramThermalItem1.thermalRatio3, paramBoolean, false);
        paramParcel.writeInt(paramThermalItem1.enviTemp);
        addThermalDetalToStringBuilder(paramThermalItem1.enviTemp, paramBoolean, false);
        i = paramThermalItem1.wifiStats;
        i = 0xFFFF & paramThermalItem1.wifiSignal | 0xFFFF0000 & i << 16;
        paramParcel.writeInt(i);
        addThermalDetalToStringBuilder(i, paramBoolean, false);
        paramParcel.writeBoolean(paramThermalItem1.phoneOnff);
        addThermalDetalToStringBuilder(paramThermalItem1.phoneOnff, paramBoolean, false);
        paramParcel.writeByte(paramThermalItem1.phoneState);
        addThermalDetalToStringBuilder(paramThermalItem1.phoneState, paramBoolean, false);
        paramParcel.writeByte(paramThermalItem1.phoneSignal);
        addThermalDetalToStringBuilder(paramThermalItem1.phoneSignal, paramBoolean, false);
        paramParcel.writeBoolean(paramThermalItem1.dataNetStatus);
        addThermalDetalToStringBuilder(paramThermalItem1.dataNetStatus, paramBoolean, false);
        paramParcel.writeByte(paramThermalItem1.connectNetType);
        addThermalDetalToStringBuilder(paramThermalItem1.connectNetType, paramBoolean, false);
        paramParcel.writeString(paramThermalItem1.jobSchedule);
        addThermalDetalToStringBuilder(paramThermalItem1.jobSchedule, paramBoolean, false);
        paramParcel.writeString(paramThermalItem1.netSync);
        addThermalDetalToStringBuilder(paramThermalItem1.netSync, paramBoolean, false);
        paramParcel.writeString(paramThermalItem1.foreProc);
        addThermalDetalToStringBuilder(paramThermalItem1.foreProc, paramBoolean, false);
        paramParcel.writeString(paramThermalItem1.versionName);
        addThermalDetalToStringBuilder(paramThermalItem1.versionName, paramBoolean, false);
        paramParcel.writeString(paramThermalItem1.topProc);
        addThermalDetalToStringBuilder(paramThermalItem1.topProc, paramBoolean, false);
        paramParcel.writeInt(paramThermalItem1.topCpu);
        addThermalDetalToStringBuilder(paramThermalItem1.topCpu, paramBoolean, true);
      } else if (i == 20) {
        paramParcel.writeByte(paramThermalItem1.thermalRatio);
        addThermalDetalToStringBuilder(paramThermalItem1.thermalRatio, paramBoolean, true);
      } else if (i == 21) {
        paramParcel.writeByte(paramThermalItem1.thermalRatio1);
        addThermalDetalToStringBuilder(paramThermalItem1.thermalRatio1, paramBoolean, true);
      } else if (i == 22) {
        paramParcel.writeByte(paramThermalItem1.thermalRatio2);
        addThermalDetalToStringBuilder(paramThermalItem1.thermalRatio2, paramBoolean, true);
      } else if (i == 23) {
        paramParcel.writeByte(paramThermalItem1.thermalRatio3);
        addThermalDetalToStringBuilder(paramThermalItem1.thermalRatio3, paramBoolean, true);
      } else if (i == 24) {
        paramParcel.writeInt(paramThermalItem1.enviTemp);
        addThermalDetalToStringBuilder(paramThermalItem1.enviTemp, paramBoolean, true);
      } else if (i == 25) {
        paramParcel.writeLong(paramThermalItem1.currentTime);
        addThermalDetalToStringBuilder(paramThermalItem1.currentTime, paramBoolean, false);
        paramParcel.writeLong(paramThermalItem1.baseElapsedRealtime);
        addThermalDetalToStringBuilder(paramThermalItem1.baseElapsedRealtime, paramBoolean, true);
      } else if (i == 26) {
        addThermalDetalToStringBuilder("", paramBoolean, true);
      }  
    if (this.mThermalHistoryBuffer != null) {
      StringBuilder stringBuilder = this.mThermalBuilder;
      if (stringBuilder != null && (
        stringBuilder.length() >= 65536 || this.mThermalHistoryBuffer.dataSize() >= 131072))
        if (!this.mHandler.hasMessages(64))
          this.mHandler.sendEmptyMessageDelayed(64, 0L);  
    } 
  }
  
  public void addThermalDetalToStringBuilder(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    if (paramBoolean1) {
      this.mThermalBuilder.append(paramInt);
      if (paramBoolean2) {
        this.mThermalBuilder.append(System.getProperty("line.separator"));
      } else {
        this.mThermalBuilder.append(" ");
      } 
    } 
  }
  
  public void addThermalDetalToStringBuilder(long paramLong, boolean paramBoolean1, boolean paramBoolean2) {
    if (paramBoolean1) {
      this.mThermalBuilder.append(paramLong);
      if (paramBoolean2) {
        this.mThermalBuilder.append(System.getProperty("line.separator"));
      } else {
        this.mThermalBuilder.append(" ");
      } 
    } 
  }
  
  public void addThermalDetalToStringBuilder(byte paramByte, boolean paramBoolean1, boolean paramBoolean2) {
    if (paramBoolean1) {
      this.mThermalBuilder.append(paramByte);
      if (paramBoolean2) {
        this.mThermalBuilder.append(System.getProperty("line.separator"));
      } else {
        this.mThermalBuilder.append(" ");
      } 
    } 
  }
  
  public void addThermalDetalToStringBuilder(String paramString, boolean paramBoolean1, boolean paramBoolean2) {
    if (paramBoolean1) {
      this.mThermalBuilder.append(paramString);
      if (paramBoolean2) {
        this.mThermalBuilder.append(System.getProperty("line.separator"));
      } else {
        this.mThermalBuilder.append(" ");
      } 
    } 
  }
  
  public void addThermalDetalToStringBuilder(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    if (paramBoolean2) {
      this.mThermalBuilder.append(paramBoolean1);
      if (paramBoolean3) {
        this.mThermalBuilder.append(System.getProperty("line.separator"));
      } else {
        this.mThermalBuilder.append(" ");
      } 
    } 
  }
  
  private void collectMoinitAppMap(OplusBaseBatteryStats.ThermalItem paramThermalItem1, OplusBaseBatteryStats.ThermalItem paramThermalItem2) {
    if (this.mThermalMonitorApp.contains(paramThermalItem2.foreProc)) {
      long l2, l1 = paramThermalItem1.elapsedRealtime - paramThermalItem2.elapsedRealtime;
      if (l1 < 0L) {
        l2 = 0L;
      } else {
        l2 = l1;
        if (l1 > 10800000L)
          l2 = 0L; 
      } 
      long l3 = 0L;
      Map<String, Long> map2 = this.mTempMonitorAppMap;
      StringBuilder stringBuilder3 = new StringBuilder();
      stringBuilder3.append(paramThermalItem2.foreProc);
      stringBuilder3.append("--Time");
      l1 = l3;
      if (map2.containsKey(stringBuilder3.toString()))
        try {
          map2 = this.mTempMonitorAppMap;
          stringBuilder3 = new StringBuilder();
          this();
          stringBuilder3.append(paramThermalItem2.foreProc);
          stringBuilder3.append("--Time");
          l1 = ((Long)map2.get(stringBuilder3.toString())).longValue();
        } catch (Exception exception) {
          exception.printStackTrace();
          l1 = l3;
        }  
      map2 = this.mTempMonitorAppMap;
      stringBuilder3 = new StringBuilder();
      stringBuilder3.append(paramThermalItem2.foreProc);
      stringBuilder3.append("--Time");
      map2.put(stringBuilder3.toString(), Long.valueOf(l2 + l1));
      if (paramThermalItem2.flashlightOn) {
        l3 = 0L;
        map2 = this.mTempMonitorAppMap;
        stringBuilder3 = new StringBuilder();
        stringBuilder3.append(paramThermalItem2.foreProc);
        stringBuilder3.append("--FlashOn");
        l1 = l3;
        if (map2.containsKey(stringBuilder3.toString()))
          try {
            map2 = this.mTempMonitorAppMap;
            stringBuilder3 = new StringBuilder();
            this();
            stringBuilder3.append(paramThermalItem2.foreProc);
            stringBuilder3.append("--FlashOn");
            l1 = ((Long)map2.get(stringBuilder3.toString())).longValue();
          } catch (Exception exception) {
            exception.printStackTrace();
            l1 = l3;
          }  
        Map<String, Long> map = this.mTempMonitorAppMap;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramThermalItem2.foreProc);
        stringBuilder.append("--FlashOn");
        map.put(stringBuilder.toString(), Long.valueOf(l2 + l1));
      } 
      if (paramThermalItem2.chargePlug != 0) {
        l3 = 0L;
        map2 = this.mTempMonitorAppMap;
        stringBuilder3 = new StringBuilder();
        stringBuilder3.append(paramThermalItem2.foreProc);
        stringBuilder3.append("--Charge");
        l1 = l3;
        if (map2.containsKey(stringBuilder3.toString()))
          try {
            map2 = this.mTempMonitorAppMap;
            stringBuilder3 = new StringBuilder();
            this();
            stringBuilder3.append(paramThermalItem2.foreProc);
            stringBuilder3.append("--Charge");
            l1 = ((Long)map2.get(stringBuilder3.toString())).longValue();
          } catch (Exception exception) {
            exception.printStackTrace();
            l1 = l3;
          }  
        Map<String, Long> map = this.mTempMonitorAppMap;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramThermalItem2.foreProc);
        stringBuilder.append("--Charge");
        map.put(stringBuilder.toString(), Long.valueOf(l2 + l1));
      } 
      map2 = this.mTempMonitorAppMap;
      stringBuilder3 = new StringBuilder();
      stringBuilder3.append(paramThermalItem2.foreProc);
      stringBuilder3.append("--StartBatRm");
      if (!map2.containsKey(stringBuilder3.toString())) {
        Map<String, Long> map = this.mTempMonitorAppMap;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramThermalItem2.foreProc);
        stringBuilder.append("--StartBatRm");
        map.put(stringBuilder.toString(), Long.valueOf(paramThermalItem1.batRm));
      } 
      map2 = this.mTempMonitorAppMap;
      stringBuilder3 = new StringBuilder();
      stringBuilder3.append(paramThermalItem2.foreProc);
      stringBuilder3.append("--EndBatRm");
      map2.put(stringBuilder3.toString(), Long.valueOf(paramThermalItem1.batRm));
      map2 = this.mTempMonitorAppMap;
      stringBuilder3 = new StringBuilder();
      stringBuilder3.append(paramThermalItem2.foreProc);
      stringBuilder3.append("--MaxPhoneTemp");
      if (map2.containsKey(stringBuilder3.toString())) {
        l3 = 0L;
        try {
          Map<String, Long> map = this.mTempMonitorAppMap;
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append(paramThermalItem2.foreProc);
          stringBuilder.append("--MaxPhoneTemp");
          l1 = ((Long)map.get(stringBuilder.toString())).longValue();
        } catch (Exception exception) {
          exception.printStackTrace();
          l1 = l3;
        } 
        if (paramThermalItem1.phoneTemp > l1) {
          Map<String, Long> map = this.mTempMonitorAppMap;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(paramThermalItem2.foreProc);
          stringBuilder.append("--MaxPhoneTemp");
          map.put(stringBuilder.toString(), Long.valueOf(paramThermalItem1.phoneTemp));
        } 
      } else {
        Map<String, Long> map = this.mTempMonitorAppMap;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramThermalItem2.foreProc);
        stringBuilder.append("--MaxPhoneTemp");
        map.put(stringBuilder.toString(), Long.valueOf(paramThermalItem1.phoneTemp));
      } 
      map2 = this.mTempMonitorAppMap;
      stringBuilder3 = new StringBuilder();
      stringBuilder3.append(paramThermalItem2.foreProc);
      stringBuilder3.append("--MaxBatTemp");
      if (map2.containsKey(stringBuilder3.toString())) {
        l1 = 0L;
        try {
          Map<String, Long> map = this.mTempMonitorAppMap;
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append(paramThermalItem2.foreProc);
          stringBuilder.append("--MaxBatTemp");
          l1 = l3 = ((Long)map.get(stringBuilder.toString())).longValue();
        } catch (Exception exception) {
          exception.printStackTrace();
        } 
        if (paramThermalItem1.batTemp > l1) {
          Map<String, Long> map = this.mTempMonitorAppMap;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(paramThermalItem2.foreProc);
          stringBuilder.append("--MaxBatTemp");
          map.put(stringBuilder.toString(), Long.valueOf(paramThermalItem1.batTemp));
        } 
      } else {
        Map<String, Long> map = this.mTempMonitorAppMap;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramThermalItem2.foreProc);
        stringBuilder.append("--MaxBatTemp");
        map.put(stringBuilder.toString(), Long.valueOf(paramThermalItem1.batTemp));
      } 
      Map<String, Long> map3 = this.mTempMonitorAppMap;
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramThermalItem2.foreProc);
      stringBuilder1.append("--AppCpu");
      if (map3.containsKey(stringBuilder1.toString())) {
        l1 = 0L;
        try {
          map3 = this.mTempMonitorAppMap;
          stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append(paramThermalItem2.foreProc);
          stringBuilder1.append("--AppCpu");
          l1 = l3 = ((Long)map3.get(stringBuilder1.toString())).longValue();
        } catch (Exception exception) {
          exception.printStackTrace();
        } 
        map3 = this.mTempMonitorAppMap;
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append(paramThermalItem2.foreProc);
        stringBuilder1.append("--AppCpu");
        map3.put(stringBuilder1.toString(), Long.valueOf(paramThermalItem2.topCpu * l2 + l1));
      } else {
        Map<String, Long> map = this.mTempMonitorAppMap;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramThermalItem2.foreProc);
        stringBuilder.append("--AppCpu");
        map.put(stringBuilder.toString(), Long.valueOf(paramThermalItem2.topCpu * l2));
      } 
      map3 = this.mTempMonitorAppMap;
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramThermalItem2.foreProc);
      stringBuilder1.append("--Cpu");
      if (map3.containsKey(stringBuilder1.toString())) {
        l3 = 0L;
        try {
          map3 = this.mTempMonitorAppMap;
          stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append(paramThermalItem2.foreProc);
          stringBuilder1.append("--Cpu");
          l1 = ((Long)map3.get(stringBuilder1.toString())).longValue();
        } catch (Exception exception) {
          exception.printStackTrace();
          l1 = l3;
        } 
        Map<String, Long> map = this.mTempMonitorAppMap;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramThermalItem2.foreProc);
        stringBuilder.append("--Cpu");
        map.put(stringBuilder.toString(), Long.valueOf(paramThermalItem2.cpuLoading * l2 + l1));
      } else {
        Map<String, Long> map = this.mTempMonitorAppMap;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramThermalItem2.foreProc);
        stringBuilder.append("--Cpu");
        map.put(stringBuilder.toString(), Long.valueOf(paramThermalItem2.cpuLoading * l2));
      } 
      map3 = this.mTempMonitorAppMap;
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramThermalItem2.foreProc);
      stringBuilder1.append("--Backlight");
      if (map3.containsKey(stringBuilder1.toString())) {
        l3 = 0L;
        try {
          map3 = this.mTempMonitorAppMap;
          stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append(paramThermalItem2.foreProc);
          stringBuilder1.append("--Backlight");
          l1 = ((Long)map3.get(stringBuilder1.toString())).longValue();
        } catch (Exception exception) {
          exception.printStackTrace();
          l1 = l3;
        } 
        Map<String, Long> map = this.mTempMonitorAppMap;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramThermalItem2.foreProc);
        stringBuilder.append("--Backlight");
        map.put(stringBuilder.toString(), Long.valueOf(paramThermalItem2.backlight * l2 + l1));
      } else {
        Map<String, Long> map = this.mTempMonitorAppMap;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramThermalItem2.foreProc);
        stringBuilder.append("--Backlight");
        map.put(stringBuilder.toString(), Long.valueOf(paramThermalItem2.backlight * l2));
      } 
      Map<String, Long> map1 = this.mTempMonitorAppMap;
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append(paramThermalItem2.foreProc);
      stringBuilder2.append("--Volume");
      if (map1.containsKey(stringBuilder2.toString())) {
        l3 = 0L;
        try {
          map1 = this.mTempMonitorAppMap;
          stringBuilder2 = new StringBuilder();
          this();
          stringBuilder2.append(paramThermalItem2.foreProc);
          stringBuilder2.append("--Volume");
          l1 = ((Long)map1.get(stringBuilder2.toString())).longValue();
        } catch (Exception exception) {
          exception.printStackTrace();
          l1 = l3;
        } 
        map1 = this.mTempMonitorAppMap;
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append(paramThermalItem2.foreProc);
        stringBuilder2.append("--Volume");
        map1.put(stringBuilder2.toString(), Long.valueOf(paramThermalItem2.volume * l2 + l1));
      } else {
        map1 = this.mTempMonitorAppMap;
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append(paramThermalItem2.foreProc);
        stringBuilder2.append("--Volume");
        map1.put(stringBuilder2.toString(), Long.valueOf(paramThermalItem2.volume * l2));
      } 
      if (paramThermalItem2.connectNetType == 0) {
        l3 = 0L;
        map1 = this.mTempMonitorAppMap;
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append(paramThermalItem2.foreProc);
        stringBuilder2.append("--NetMobile");
        l1 = l3;
        if (map1.containsKey(stringBuilder2.toString()))
          try {
            Map<String, Long> map4 = this.mTempMonitorAppMap;
            StringBuilder stringBuilder4 = new StringBuilder();
            this();
            stringBuilder4.append(paramThermalItem2.foreProc);
            stringBuilder4.append("--NetMobile");
            l1 = ((Long)map4.get(stringBuilder4.toString())).longValue();
          } catch (Exception exception) {
            exception.printStackTrace();
            l1 = l3;
          }  
        Map<String, Long> map = this.mTempMonitorAppMap;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramThermalItem2.foreProc);
        stringBuilder.append("--NetMobile");
        map.put(stringBuilder.toString(), Long.valueOf(l1 + l2));
      } 
      if (paramThermalItem2.connectNetType == 1) {
        l3 = 0L;
        map1 = this.mTempMonitorAppMap;
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append(paramThermalItem2.foreProc);
        stringBuilder2.append("--NetWifi");
        l1 = l3;
        if (map1.containsKey(stringBuilder2.toString()))
          try {
            Map<String, Long> map4 = this.mTempMonitorAppMap;
            StringBuilder stringBuilder4 = new StringBuilder();
            this();
            stringBuilder4.append(paramThermalItem2.foreProc);
            stringBuilder4.append("--NetWifi");
            l1 = ((Long)map4.get(stringBuilder4.toString())).longValue();
          } catch (Exception exception) {
            exception.printStackTrace();
            l1 = l3;
          }  
        Map<String, Long> map = this.mTempMonitorAppMap;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramThermalItem2.foreProc);
        stringBuilder.append("--NetWifi");
        map.put(stringBuilder.toString(), Long.valueOf(l1 + l2));
      } 
      if (paramThermalItem2.connectNetType == -1) {
        l3 = 0L;
        Map<String, Long> map5 = this.mTempMonitorAppMap;
        StringBuilder stringBuilder4 = new StringBuilder();
        stringBuilder4.append(paramThermalItem2.foreProc);
        stringBuilder4.append("--NetNone");
        l1 = l3;
        if (map5.containsKey(stringBuilder4.toString()))
          try {
            Map<String, Long> map = this.mTempMonitorAppMap;
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append(paramThermalItem2.foreProc);
            stringBuilder.append("--NetNone");
            l1 = ((Long)map.get(stringBuilder.toString())).longValue();
          } catch (Exception exception) {
            exception.printStackTrace();
            l1 = l3;
          }  
        Map<String, Long> map4 = this.mTempMonitorAppMap;
        StringBuilder stringBuilder5 = new StringBuilder();
        stringBuilder5.append(paramThermalItem2.foreProc);
        stringBuilder5.append("--NetNone");
        map4.put(stringBuilder5.toString(), Long.valueOf(l1 + l2));
      } 
      if (paramThermalItem2.isAutoBrightness == true) {
        l3 = 0L;
        Map<String, Long> map5 = this.mTempMonitorAppMap;
        StringBuilder stringBuilder4 = new StringBuilder();
        stringBuilder4.append(paramThermalItem2.foreProc);
        stringBuilder4.append("--AutoBrightness");
        l1 = l3;
        if (map5.containsKey(stringBuilder4.toString()))
          try {
            Map<String, Long> map = this.mTempMonitorAppMap;
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append(paramThermalItem2.foreProc);
            stringBuilder.append("--AutoBrightness");
            l1 = ((Long)map.get(stringBuilder.toString())).longValue();
          } catch (Exception exception) {
            exception.printStackTrace();
            l1 = l3;
          }  
        Map<String, Long> map4 = this.mTempMonitorAppMap;
        StringBuilder stringBuilder5 = new StringBuilder();
        stringBuilder5.append(paramThermalItem2.foreProc);
        stringBuilder5.append("--AutoBrightness");
        map4.put(stringBuilder5.toString(), Long.valueOf(l1 + l2));
      } 
      if (!paramThermalItem1.foreProc.equals(paramThermalItem2.foreProc)) {
        map1 = this.mTempMonitorAppMap;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramThermalItem2.foreProc);
        stringBuilder.append("--Time");
        if (map1.containsKey(stringBuilder.toString())) {
          Map<String, Long> map = this.mTempMonitorAppMap;
          StringBuilder stringBuilder4 = new StringBuilder();
          stringBuilder4.append(paramThermalItem2.foreProc);
          stringBuilder4.append("--Time");
          l1 = ((Long)map.get(stringBuilder4.toString())).longValue();
          boolean bool = true;
          if (l1 >= this.mMonitorAppLimitTime) {
            map = this.mTempMonitorAppMap;
            stringBuilder4 = new StringBuilder();
            stringBuilder4.append(paramThermalItem2.foreProc);
            stringBuilder4.append("--FlashOn");
            boolean bool1 = bool;
            if (map.containsKey(stringBuilder4.toString())) {
              map = this.mTempMonitorAppMap;
              stringBuilder4 = new StringBuilder();
              stringBuilder4.append(paramThermalItem2.foreProc);
              stringBuilder4.append("--FlashOn");
              l2 = ((Long)map.get(stringBuilder4.toString())).longValue();
              bool1 = bool;
              if (l2 * 100L / l1 > 20L)
                bool1 = false; 
            } 
            Map<String, Long> map4 = this.mTempMonitorAppMap;
            StringBuilder stringBuilder5 = new StringBuilder();
            stringBuilder5.append(paramThermalItem2.foreProc);
            stringBuilder5.append("--Cpu");
            bool = bool1;
            if (map4.containsKey(stringBuilder5.toString())) {
              Map<String, Long> map5 = this.mTempMonitorAppMap;
              StringBuilder stringBuilder6 = new StringBuilder();
              stringBuilder6.append(paramThermalItem2.foreProc);
              stringBuilder6.append("--AppCpu");
              bool = bool1;
              if (map5.containsKey(stringBuilder6.toString())) {
                Map<String, Long> map6 = this.mTempMonitorAppMap;
                StringBuilder stringBuilder7 = new StringBuilder();
                stringBuilder7.append(paramThermalItem2.foreProc);
                stringBuilder7.append("--Cpu");
                l2 = ((Long)map6.get(stringBuilder7.toString())).longValue();
                map6 = this.mTempMonitorAppMap;
                stringBuilder7 = new StringBuilder();
                stringBuilder7.append(paramThermalItem2.foreProc);
                stringBuilder7.append("--AppCpu");
                l1 = ((Long)map6.get(stringBuilder7.toString())).longValue();
                if (l2 > 0L) {
                  if (100L * l1 / l2 < 10L)
                    bool1 = false; 
                } else {
                  bool1 = false;
                } 
                bool = bool1;
                if (bool1) {
                  this.mMonitorAppMap = getMonitorAppMap(paramThermalItem2.foreProc, paramThermalItem2.versionName);
                  startUploadMonitorApp();
                  bool = bool1;
                } 
              } 
            } 
            try {
              for (Map.Entry<String, Long> entry : this.mTempMonitorAppMap.entrySet()) {
                stringBuilder5 = new StringBuilder();
                this();
                stringBuilder5.append("mTempMonitorAppMap ");
                stringBuilder5.append((String)entry.getKey());
                stringBuilder5.append(":");
                stringBuilder5.append(entry.getValue());
                Slog.i("OppoThermalStats", stringBuilder5.toString());
              } 
            } catch (Exception exception) {
              exception.printStackTrace();
            } 
          } 
        } 
        this.mTempMonitorAppMap.clear();
      } 
    } 
  }
  
  private void collectChargeMap(OplusBaseBatteryStats.ThermalItem paramThermalItem1, OplusBaseBatteryStats.ThermalItem paramThermalItem2) {
    Calendar calendar;
    long l2, l1 = paramThermalItem1.elapsedRealtime - paramThermalItem2.elapsedRealtime;
    if (l1 < 0L) {
      l2 = 0L;
    } else {
      l2 = l1;
      if (l1 > 10800000L)
        l2 = 0L; 
    } 
    if (paramThermalItem1.chargePlug != paramThermalItem2.chargePlug) {
      this.mLastFast2Normal = 0;
      if (paramThermalItem2.chargePlug == 0) {
        this.mTempChargeUploadMap.clear();
        calendar = Calendar.getInstance();
        int i = calendar.get(11);
        this.mTempChargeUploadMap.put("startTemp", Long.valueOf(paramThermalItem1.batTemp));
        this.mTempChargeUploadMap.put("startBatrm", Long.valueOf(paramThermalItem1.batRm));
        this.mTempChargeUploadMap.put("startLevel", Long.valueOf(paramThermalItem1.batPercent));
        this.mTempChargeUploadMap.put("startTime", Long.valueOf(paramThermalItem1.elapsedRealtime - paramThermalItem1.baseElapsedRealtime + paramThermalItem1.currentTime));
        this.mTempChargeUploadMap.put("startHour", Long.valueOf(i));
        this.mTempChargeUploadMap.put("minTemp", Long.valueOf(paramThermalItem1.batTemp));
        this.mTempChargeUploadMap.put("maxTemp", Long.valueOf(paramThermalItem1.batTemp));
        this.mTempChargeUploadMap.put("chargeTime", Long.valueOf(paramThermalItem1.elapsedRealtime));
      } else {
        if (this.mTempChargeUploadMap.containsKey("chargeTime") && this.mTempChargeUploadMap.containsKey("startTime")) {
          l1 = paramThermalItem1.elapsedRealtime - ((Long)this.mTempChargeUploadMap.get("chargeTime")).longValue();
          if (((OplusBaseBatteryStats.ThermalItem)calendar).backlight > 0)
            if (!this.mTempChargeUploadMap.containsKey("screenOnTime")) {
              this.mTempChargeUploadMap.put("screenOnTime", Long.valueOf(l2));
            } else {
              long l = ((Long)this.mTempChargeUploadMap.get("screenOnTime")).longValue();
              this.mTempChargeUploadMap.put("screenOnTime", Long.valueOf(l2 + l));
            }  
          if (l1 >= 120000L) {
            this.mTempChargeUploadMap.put("chargeTime", Long.valueOf(l1));
            this.mTempChargeUploadMap.put("endTemp", Long.valueOf(paramThermalItem1.batTemp));
            this.mTempChargeUploadMap.put("endBatrm", Long.valueOf(paramThermalItem1.batRm));
            this.mTempChargeUploadMap.put("endLevel", Long.valueOf(paramThermalItem1.batPercent));
            this.mTempChargeUploadMap.put("endTime", Long.valueOf(paramThermalItem1.elapsedRealtime - paramThermalItem1.baseElapsedRealtime + paramThermalItem1.currentTime));
            this.mTempChargeUploadMap.put("chargePlug", Long.valueOf(((OplusBaseBatteryStats.ThermalItem)calendar).chargePlug));
            if (this.mTempChargeUploadMap.containsKey("minTemp")) {
              l2 = ((Long)this.mTempChargeUploadMap.get("minTemp")).longValue();
              if (paramThermalItem1.batTemp < l2)
                this.mTempChargeUploadMap.put("minTemp", Long.valueOf(paramThermalItem1.batTemp)); 
            } 
            if (this.mTempChargeUploadMap.containsKey("maxTemp")) {
              l2 = ((Long)this.mTempChargeUploadMap.get("maxTemp")).longValue();
              if (paramThermalItem1.batTemp > l2)
                this.mTempChargeUploadMap.put("maxTemp", Long.valueOf(paramThermalItem1.batTemp)); 
            } 
            this.mChargeUploadMap = getUploadChargeMap();
            startUploadChargeMap();
          } 
        } 
        this.mTempChargeUploadMap.clear();
      } 
    } else if (((OplusBaseBatteryStats.ThermalItem)calendar).chargePlug != 0) {
      if (!this.mTempChargeUploadMap.containsKey("startTime")) {
        this.mTempChargeUploadMap.clear();
        Calendar calendar1 = Calendar.getInstance();
        int i = calendar1.get(11);
        this.mTempChargeUploadMap.put("startTemp", Long.valueOf(paramThermalItem1.batTemp));
        this.mTempChargeUploadMap.put("startBatrm", Long.valueOf(paramThermalItem1.batRm));
        this.mTempChargeUploadMap.put("startLevel", Long.valueOf(paramThermalItem1.batPercent));
        this.mTempChargeUploadMap.put("startTime", Long.valueOf(paramThermalItem1.elapsedRealtime - paramThermalItem1.baseElapsedRealtime + paramThermalItem1.currentTime));
        this.mTempChargeUploadMap.put("startHour", Long.valueOf(i));
        this.mTempChargeUploadMap.put("minTemp", Long.valueOf(paramThermalItem1.batTemp));
        this.mTempChargeUploadMap.put("maxTemp", Long.valueOf(paramThermalItem1.batTemp));
        this.mTempChargeUploadMap.put("chargeTime", Long.valueOf(paramThermalItem1.elapsedRealtime));
      } 
      if (((OplusBaseBatteryStats.ThermalItem)calendar).backlight > 0)
        if (!this.mTempChargeUploadMap.containsKey("screenOnTime")) {
          this.mTempChargeUploadMap.put("screenOnTime", Long.valueOf(l2));
        } else {
          l1 = ((Long)this.mTempChargeUploadMap.get("screenOnTime")).longValue();
          this.mTempChargeUploadMap.put("screenOnTime", Long.valueOf(l2 + l1));
        }  
      if (this.mTempChargeUploadMap.containsKey("minTemp")) {
        l2 = ((Long)this.mTempChargeUploadMap.get("minTemp")).longValue();
        if (paramThermalItem1.batTemp < l2)
          this.mTempChargeUploadMap.put("minTemp", Long.valueOf(paramThermalItem1.batTemp)); 
      } else if (this.mTempChargeUploadMap.containsKey("maxTemp")) {
        l2 = ((Long)this.mTempChargeUploadMap.get("maxTemp")).longValue();
        if (paramThermalItem1.batTemp > l2)
          this.mTempChargeUploadMap.put("maxTemp", Long.valueOf(paramThermalItem1.batTemp)); 
      } 
      if (this.mLastFast2Normal != this.mGlobalFast2Normal) {
        this.mTempChargeUploadMap.put("f2nTemp", Long.valueOf(paramThermalItem1.batTemp));
        this.mTempChargeUploadMap.put("f2nLevel", Long.valueOf(paramThermalItem1.batPercent));
        this.mTempChargeUploadMap.put("f2nbatRm", Long.valueOf(paramThermalItem1.batRm));
        this.mTempChargeUploadMap.put("f2nTime", Long.valueOf(paramThermalItem1.currentTime));
      } 
      this.mLastFast2Normal = this.mGlobalFast2Normal;
    } 
  }
  
  private void collectThermalTempMap(OplusBaseBatteryStats.ThermalItem paramThermalItem1, OplusBaseBatteryStats.ThermalItem paramThermalItem2) {
    long l2, l1 = paramThermalItem1.elapsedRealtime - paramThermalItem2.elapsedRealtime;
    if (l1 < 0L) {
      l2 = 0L;
    } else {
      l2 = l1;
      if (l1 > 10800000L)
        l2 = 0L; 
    } 
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH");
    long l3 = paramThermalItem1.elapsedRealtime, l4 = paramThermalItem1.baseElapsedRealtime, l5 = paramThermalItem1.currentTime;
    l1 = paramThermalItem2.elapsedRealtime;
    long l6 = paramThermalItem2.baseElapsedRealtime, l7 = paramThermalItem2.currentTime;
    try {
      Date date1 = new Date();
      this(l3 - l4 + l5);
      Integer integer = Integer.valueOf(Integer.parseInt(simpleDateFormat.format(date1)));
      Date date2 = new Date();
      this(l1 - l6 + l7);
      int i = Integer.parseInt(simpleDateFormat.format(date2));
      int j = paramThermalItem1.phoneTemp, k = 360;
      if (j > 360)
        k = paramThermalItem1.phoneTemp; 
      j = integer.intValue();
      if (j != i && integer.intValue() != -1 && i != -1) {
        Map<String, Integer> map = this.mThermalHourMap;
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("hour");
        stringBuilder.append(Integer.toString(integer.intValue()));
        if (!map.containsKey(stringBuilder.toString())) {
          Map<String, Integer> map1 = this.mThermalHourMap;
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("hour");
          stringBuilder1.append(Integer.toString(integer.intValue()));
          map1.put(stringBuilder1.toString(), Integer.valueOf(k));
        } 
      } 
      if (integer.intValue() == i && integer.intValue() != -1 && i != -1) {
        Map<String, Integer> map = this.mThermalHourMap;
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("hour");
        stringBuilder.append(Integer.toString(integer.intValue()));
        if (!map.containsKey(stringBuilder.toString())) {
          Map<String, Integer> map1 = this.mThermalHourMap;
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("hour");
          stringBuilder1.append(Integer.toString(integer.intValue()));
          map1.put(stringBuilder1.toString(), Integer.valueOf(k));
        } else {
          map = this.mThermalHourMap;
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("hour");
          stringBuilder.append(Integer.toString(integer.intValue()));
          i = ((Integer)map.get(stringBuilder.toString())).intValue();
          if (i < k) {
            map = this.mThermalHourMap;
            stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("hour");
            stringBuilder.append(Integer.toString(integer.intValue()));
            map.put(stringBuilder.toString(), Integer.valueOf(k));
          } 
        } 
      } 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    if (paramThermalItem2.phoneTemp != -1023 && paramThermalItem2.elapsedRealtime > 10000L) {
      String str;
      if (paramThermalItem2.phoneTemp / 10 <= 36) {
        str = "36";
      } else if (paramThermalItem2.phoneTemp / 10 >= 60) {
        str = "60";
      } else {
        str = Integer.toString(paramThermalItem2.phoneTemp / 10);
      } 
      if (this.mThermalTempMap.containsKey(str)) {
        l1 = ((Long)this.mThermalTempMap.get(str)).longValue();
        this.mThermalTempMap.remove(str);
        this.mThermalTempMap.put(str, Long.valueOf(l2 + l1));
      } else {
        this.mThermalTempMap.put(str, Long.valueOf(l2));
      } 
    } 
    if (paramThermalItem2.batTemp != -1023 && paramThermalItem2.elapsedRealtime > 10000L) {
      String str;
      if (paramThermalItem2.batTemp / 10 <= 36) {
        str = "36";
      } else if (paramThermalItem2.batTemp / 10 >= 60) {
        str = "60";
      } else {
        str = Integer.toString(paramThermalItem2.batTemp / 10);
      } 
      if (this.mBatTempMap.containsKey(str)) {
        l1 = ((Long)this.mBatTempMap.get(str)).longValue();
        this.mBatTempMap.remove(str);
        this.mBatTempMap.put(str, Long.valueOf(l2 + l1));
      } else {
        this.mBatTempMap.put(str, Long.valueOf(l2));
      } 
    } 
  }
  
  private int buildThermalBatteryInfo(OplusBaseBatteryStats.ThermalItem paramThermalItem) {
    return paramThermalItem.batPercent << 25 & 0xFE000000 | paramThermalItem.chargePlug << 15 & 0x1FF8000 | paramThermalItem.batRm << 1 & 0x7FFE;
  }
  
  private long buildThermalTempInfo(OplusBaseBatteryStats.ThermalItem paramThermalItem) {
    return (paramThermalItem.phoneTemp3 & 0xFFFL) << 48L | (paramThermalItem.phoneTemp2 & 0xFFFL) << 36L | (paramThermalItem.phoneTemp1 & 0xFFFL) << 24L | (0xFFFL & paramThermalItem.phoneTemp) << 12L | paramThermalItem.batTemp;
  }
  
  private void readThermalBatteryInfo(int paramInt, OplusBaseBatteryStats.ThermalItem paramThermalItem) {
    paramThermalItem.batPercent = (0xFE000000 & paramInt) >>> 25;
    paramThermalItem.chargePlug = (0x1FF8000 & paramInt) >>> 15;
    paramThermalItem.batRm = (paramInt & 0x7FFE) >>> 1;
  }
  
  private void readThermalTempInfo(long paramLong, OplusBaseBatteryStats.ThermalItem paramThermalItem) {
    paramThermalItem.phoneTemp3 = symbolInt((int)(paramLong >> 48L & 0xFFFL));
    paramThermalItem.phoneTemp2 = symbolInt((int)(paramLong >> 36L & 0xFFFL));
    paramThermalItem.phoneTemp1 = symbolInt((int)(paramLong >> 24L & 0xFFFL));
    paramThermalItem.phoneTemp = symbolInt((int)(paramLong >> 12L & 0xFFFL));
    paramThermalItem.batTemp = symbolInt((int)(paramLong & 0xFFFL));
  }
  
  public void clearThermalStatsBuffer() {
    synchronized (this.mLock) {
      this.mThermalHistoryBuffer.setDataPosition(0);
      this.mThermalHistoryBuffer.setDataSize(0);
      this.mThermalHistoryBuffer.setDataCapacity(this.mStats.getHistoryBufferSize());
      this.mThermalTempMap.clear();
      this.mBatTempMap.clear();
      this.mThermalHourMap.clear();
      if (this.mThermalHistoryLastLastWritten != null)
        this.mThermalHistoryLastLastWritten.clear(); 
      return;
    } 
  }
  
  public void clearHistoryBuffer() {
    synchronized (this.mLock) {
      this.mThermalHistoryBuffer.setDataPosition(0);
      this.mThermalHistoryBuffer.setDataSize(0);
      this.mThermalHistoryBuffer.setDataCapacity(this.mStats.getHistoryBufferSize());
      this.mThermalHistoryBufferLastPos = -1;
      if (this.mThermalHistoryLastLastWritten != null)
        this.mThermalHistoryLastLastWritten.clear(); 
      return;
    } 
  }
  
  public void addThermalScreenBrightnessEvent(long paramLong1, long paramLong2, int paramInt1, int paramInt2) {
    if (!this.mThermalFeatureOn)
      return; 
    if (paramInt2 <= 0) {
      addThermalScreenBrightness(paramLong1, paramLong2, paramInt1);
    } else {
      addThermalScreenBrightnessDelayed(paramLong1, paramLong2, paramInt1, paramInt2);
    } 
  }
  
  private void addThermalScreenBrightnessDelayed(long paramLong1, long paramLong2, int paramInt1, int paramInt2) {
    this.mHandler.removeMessages(57);
    Message message = new Message();
    message.what = 57;
    message.arg1 = paramInt1;
    this.mHandler.sendMessageDelayed(message, paramInt2);
  }
  
  private void addThermalScreenBrightness(long paramLong1, long paramLong2, int paramInt) {
    if (this.mHandler.hasMessages(57))
      this.mHandler.removeMessages(57); 
    addThermalHistoryBufferLocked((byte)3, paramInt, this.mThermalHistoryCur, true);
  }
  
  public void addThermalWifiStatus(long paramLong1, long paramLong2, int paramInt) {
    if (!this.mThermalFeatureOn)
      return; 
    this.mThermalHistoryCur.wifiStats = paramInt;
    if (paramInt == 0 || paramInt == 3) {
      this.mThermalHistoryCur.wifiSignal = 0;
    } else {
      this.mThermalHistoryCur.wifiSignal = this.mStats.getWifiSignalStrengthBin();
    } 
    addThermalHistoryBufferLocked((byte)4, this.mThermalHistoryCur, true);
  }
  
  public void addThermalWifiRssi(long paramLong1, long paramLong2, int paramInt) {
    if (!this.mThermalFeatureOn)
      return; 
    if (this.mThermalHistoryCur.wifiSignal != paramInt) {
      this.mThermalHistoryCur.wifiSignal = paramInt;
      addThermalHistoryBufferLocked((byte)4, this.mThermalHistoryCur, true);
    } 
  }
  
  public void addThermalPhoneOnOff(long paramLong1, long paramLong2, boolean paramBoolean) {
    if (!this.mThermalFeatureOn)
      return; 
    this.mThermalHistoryCur.phoneOnff = paramBoolean;
    addThermalHistoryBufferLocked((byte)5, this.mThermalHistoryCur, true);
  }
  
  public void addThermalPhoneState(long paramLong1, long paramLong2, byte paramByte) {
    if (!this.mThermalFeatureOn)
      return; 
    if (paramByte != this.mThermalHistoryCur.phoneState) {
      this.mThermalHistoryCur.phoneState = paramByte;
      addThermalHistoryBufferLocked((byte)6, this.mThermalHistoryCur, true);
    } 
  }
  
  public void addThermalPhoneSignal(long paramLong1, long paramLong2, byte paramByte) {
    if (!this.mThermalFeatureOn)
      return; 
    if (paramByte >= 0 && paramByte <= 4) {
      this.mThermalHistoryCur.phoneSignal = paramByte;
      addThermalHistoryBufferLocked((byte)7, this.mThermalHistoryCur, true);
    } 
  }
  
  public void addThermalNetState(long paramLong1, long paramLong2, boolean paramBoolean) {
    if (!this.mThermalFeatureOn)
      return; 
    this.mThermalHistoryCur.dataNetStatus = paramBoolean;
    addThermalHistoryBufferLocked((byte)8, this.mThermalHistoryCur, true);
  }
  
  public void addThermalConnectType(long paramLong1, long paramLong2, byte paramByte) {
    if (!this.mThermalFeatureOn)
      return; 
    this.mHandler.removeMessages(58);
    Message message = new Message();
    message.what = 58;
    message.arg1 = paramByte;
    this.mHandler.sendMessageDelayed(message, 4000L);
  }
  
  public void addThermalCameraOnff(long paramLong1, long paramLong2, boolean paramBoolean) {
    if (!this.mThermalFeatureOn)
      return; 
    this.mHandler.removeMessages(51);
    Message message = new Message();
    message.what = 51;
    message.arg1 = paramBoolean;
    this.mHandler.sendMessageDelayed(message, 4000L);
  }
  
  public void addThermalAudioOnff(long paramLong1, long paramLong2, boolean paramBoolean) {
    if (!this.mThermalFeatureOn)
      return; 
    this.mHandler.removeMessages(52);
    Message message = new Message();
    message.what = 52;
    message.arg1 = paramBoolean;
    this.mHandler.sendMessageDelayed(message, 4000L);
  }
  
  public void addThermalVideoOnff(long paramLong1, long paramLong2, boolean paramBoolean) {
    if (!this.mThermalFeatureOn)
      return; 
    this.mHandler.removeMessages(53);
    Message message = new Message();
    message.what = 53;
    message.arg1 = paramBoolean;
    this.mHandler.sendMessageDelayed(message, 4000L);
  }
  
  public void addThermalGpsOnff(long paramLong1, long paramLong2, boolean paramBoolean) {
    if (!this.mThermalFeatureOn)
      return; 
    this.mHandler.removeMessages(54);
    Message message = new Message();
    message.what = 54;
    message.arg1 = paramBoolean;
    this.mHandler.sendMessageDelayed(message, 4000L);
  }
  
  public void addThermalFlashLightOnff(long paramLong1, long paramLong2, boolean paramBoolean) {
    if (!this.mThermalFeatureOn)
      return; 
    this.mHandler.removeMessages(55);
    Message message = new Message();
    message.what = 55;
    message.arg1 = paramBoolean;
    this.mHandler.sendMessageDelayed(message, 4000L);
  }
  
  private int eventTypeToMessageID(int paramInt) {
    if (paramInt != 2) {
      if (paramInt != 3) {
        if (paramInt != 4) {
          if (paramInt != 5) {
            if (paramInt != 6)
              return -1; 
            return 55;
          } 
          return 54;
        } 
        return 53;
      } 
      return 52;
    } 
    return 51;
  }
  
  public void addThermalOnOffEvent(int paramInt, long paramLong1, long paramLong2, boolean paramBoolean) {
    paramInt = eventTypeToMessageID(paramInt);
    if (-1 == paramInt) {
      Slog.e("OppoThermalStats", "addThermalOnOffEvent, unsupport event type!");
      return;
    } 
    if (!this.mThermalFeatureOn)
      return; 
    this.mHandler.removeMessages(paramInt);
    Message message = new Message();
    message.what = paramInt;
    message.arg1 = paramBoolean;
    this.mHandler.sendMessageDelayed(message, 4000L);
  }
  
  public void addThermalJobProc(long paramLong1, long paramLong2, String paramString) {
    if (!this.mThermalFeatureOn)
      return; 
    if ((this.mThermalHistoryCur.jobSchedule == null && paramString != null && !paramString.equals("null")) || (this.mThermalHistoryCur.jobSchedule != null && !this.mThermalHistoryCur.jobSchedule.equals(paramString))) {
      this.mThermalHistoryCur.jobSchedule = paramString;
      addThermalHistoryBufferLocked((byte)15, this.mThermalHistoryCur, true);
    } 
  }
  
  public void addThermalnetSyncProc(long paramLong1, long paramLong2, String paramString) {
    if (!this.mThermalFeatureOn)
      return; 
    if ((this.mThermalHistoryCur.netSync == null && paramString != null && !paramString.equals("null")) || (this.mThermalHistoryCur.netSync != null && !this.mThermalHistoryCur.netSync.equals(paramString))) {
      this.mThermalHistoryCur.netSync = paramString;
      addThermalHistoryBufferLocked((byte)16, this.mThermalHistoryCur, true);
    } 
  }
  
  public void addThermalForeProc(long paramLong1, long paramLong2, String paramString, int paramInt) {
    if (!this.mThermalFeatureOn)
      return; 
    if ((this.mThermalHistoryCur.foreProc == null && paramString != null && !paramString.equals("null")) || (this.mThermalHistoryCur.foreProc != null && !this.mThermalHistoryCur.foreProc.equals(paramString))) {
      String str1 = "0000";
      String[] arrayOfString = paramString.split(":");
      String str2 = str1;
      if (arrayOfString[0] != null)
        try {
          PackageInfo packageInfo = this.mPackageManger.getPackageInfoAsUser(arrayOfString[0], 0, -2);
          str2 = str1;
          if (packageInfo != null)
            str2 = packageInfo.versionName; 
        } catch (Exception exception) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Error getting package info: ");
          stringBuilder.append(arrayOfString[0]);
          Slog.e("OppoThermalStats", stringBuilder.toString());
          str2 = str1;
        }  
      this.mThermalHistoryCur.foreProc = paramString;
      this.mThermalHistoryCur.versionName = str2;
      addThermalHistoryBufferLocked((byte)17, this.mThermalHistoryCur, true);
    } 
  }
  
  public OplusBaseBatteryStats.ThermalItem getThermalHistoryFromFile(BufferedReader paramBufferedReader, PrintWriter paramPrintWriter, OplusBaseBatteryStats.ThermalHistoryPrinter paramThermalHistoryPrinter) throws IOException {
    OplusBaseBatteryStats.ThermalItem thermalItem = new OplusBaseBatteryStats.ThermalItem();
    new OplusBaseBatteryStats.ThermalItem();
    while (true) {
      String str = paramBufferedReader.readLine();
      if (str != null) {
        String[] arrayOfString = str.split(" ");
        try {
          boolean bool;
          int i = Integer.parseInt(arrayOfString[0]);
          thermalItem.backlight = i & 0x3FFF;
          thermalItem.cpuLoading = i >> 14 & 0x3FF;
          thermalItem.cmd = (byte)(i >> 24 & 0xFF);
          byte b = thermalItem.cmd;
          long l = Long.parseLong(arrayOfString[1]);
          thermalItem.elapsedRealtime = l >> 5L;
          if ((0x1FL & l) >> 4L == 1L) {
            bool = true;
          } else {
            bool = false;
          } 
          thermalItem.cameraOn = bool;
          if ((0xFL & l) >> 3L == 1L) {
            bool = true;
          } else {
            bool = false;
          } 
          thermalItem.audioOn = bool;
          if ((0x7L & l) >> 2L == 1L) {
            bool = true;
          } else {
            bool = false;
          } 
          thermalItem.videoOn = bool;
          if ((0x3L & l) >> 1L == 1L) {
            bool = true;
          } else {
            bool = false;
          } 
          thermalItem.gpsOn = bool;
          if ((l & 0x1L) == 1L) {
            bool = true;
          } else {
            bool = false;
          } 
          thermalItem.flashlightOn = bool;
          l = Long.parseLong(arrayOfString[2]);
          thermalItem.upTime = l >> 8L;
          thermalItem.volume = (int)(l & 0x7FL);
          if ((l & 0x80L) == 128L) {
            bool = true;
          } else {
            bool = false;
          } 
          thermalItem.isAutoBrightness = bool;
          if (b == 0) {
            thermalItem.currentTime = Long.parseLong(arrayOfString[3]);
            thermalItem.baseElapsedRealtime = Long.parseLong(arrayOfString[4]);
          } else if (b == 1) {
            i = Integer.parseInt(arrayOfString[3]);
            readThermalBatteryInfo(i, thermalItem);
            l = Long.parseLong(arrayOfString[4]);
            readThermalTempInfo(l, thermalItem);
          } else if (b == 2) {
            l = Long.parseLong(arrayOfString[3]);
            readThermalTempInfo(l, thermalItem);
          } else if (b == 4) {
            i = Integer.parseInt(arrayOfString[3]);
            thermalItem.wifiStats = symbolInt(i >> 16 & 0xFFFF);
            thermalItem.wifiSignal = symbolInt(0xFFFF & i);
          } else if (b == 5) {
            thermalItem.phoneOnff = Boolean.parseBoolean(arrayOfString[3]);
          } else if (b == 6) {
            thermalItem.phoneState = Byte.parseByte(arrayOfString[3]);
          } else if (b == 7) {
            thermalItem.phoneSignal = Byte.parseByte(arrayOfString[3]);
          } else if (b == 8) {
            thermalItem.dataNetStatus = Boolean.parseBoolean(arrayOfString[3]);
          } else if (b == 9) {
            thermalItem.connectNetType = Byte.parseByte(arrayOfString[3]);
          } else if (b == 10) {
            thermalItem.cameraOn = Boolean.parseBoolean(arrayOfString[3]);
          } else if (b == 11) {
            thermalItem.audioOn = Boolean.parseBoolean(arrayOfString[3]);
          } else if (b == 12) {
            thermalItem.videoOn = Boolean.parseBoolean(arrayOfString[3]);
          } else if (b == 13) {
            thermalItem.gpsOn = Boolean.parseBoolean(arrayOfString[3]);
          } else if (b == 14) {
            thermalItem.flashlightOn = Boolean.parseBoolean(arrayOfString[3]);
          } else if (b == 15) {
            thermalItem.jobSchedule = arrayOfString[3];
          } else if (b == 16) {
            thermalItem.netSync = arrayOfString[3];
          } else if (b == 17) {
            thermalItem.foreProc = arrayOfString[3];
            thermalItem.versionName = arrayOfString[4];
          } else if (b == 18) {
            thermalItem.topProc = arrayOfString[3];
            thermalItem.topCpu = Integer.parseInt(arrayOfString[4]);
          } else if (b == 19) {
            thermalItem.currentTime = Long.parseLong(arrayOfString[3]);
            thermalItem.baseElapsedRealtime = Long.parseLong(arrayOfString[4]);
            i = Integer.parseInt(arrayOfString[5]);
            readThermalBatteryInfo(i, thermalItem);
            l = Long.parseLong(arrayOfString[6]);
            readThermalTempInfo(l, thermalItem);
            thermalItem.thermalRatio = Byte.parseByte(arrayOfString[7]);
            thermalItem.thermalRatio1 = Byte.parseByte(arrayOfString[8]);
            thermalItem.thermalRatio2 = Byte.parseByte(arrayOfString[9]);
            thermalItem.thermalRatio3 = Byte.parseByte(arrayOfString[10]);
            thermalItem.enviTemp = Integer.parseInt(arrayOfString[11]);
            i = Integer.parseInt(arrayOfString[12]);
            thermalItem.wifiStats = symbolInt(i >> 16 & 0xFFFF);
            thermalItem.wifiSignal = symbolInt(i & 0xFFFF);
            thermalItem.phoneOnff = Boolean.parseBoolean(arrayOfString[13]);
            thermalItem.phoneState = Byte.parseByte(arrayOfString[14]);
            thermalItem.phoneSignal = Byte.parseByte(arrayOfString[15]);
            thermalItem.dataNetStatus = Boolean.parseBoolean(arrayOfString[16]);
            thermalItem.connectNetType = Byte.parseByte(arrayOfString[17]);
            thermalItem.jobSchedule = arrayOfString[18];
            thermalItem.netSync = arrayOfString[19];
            thermalItem.foreProc = arrayOfString[20];
            thermalItem.versionName = arrayOfString[21];
            thermalItem.topProc = arrayOfString[22];
            thermalItem.topCpu = Integer.parseInt(arrayOfString[23]);
          } else if (b == 20) {
            thermalItem.thermalRatio = Byte.parseByte(arrayOfString[3]);
          } else if (b == 21) {
            thermalItem.thermalRatio1 = Byte.parseByte(arrayOfString[3]);
          } else if (b == 22) {
            thermalItem.thermalRatio2 = Byte.parseByte(arrayOfString[3]);
          } else if (b == 23) {
            thermalItem.thermalRatio3 = Byte.parseByte(arrayOfString[3]);
          } else if (b == 24) {
            thermalItem.enviTemp = Integer.parseInt(arrayOfString[3]);
          } else if (b == 25) {
            thermalItem.currentTime = Long.parseLong(arrayOfString[3]);
            thermalItem.elapsedRealtime = Long.parseLong(arrayOfString[4]);
          } 
          if (paramPrintWriter != null && paramThermalHistoryPrinter != null) {
            paramThermalHistoryPrinter.printNextItem(paramPrintWriter, thermalItem);
            continue;
          } 
          this.mThermalHistoryLastLastRead.setTo(this.mThermalHistoryLastRead);
          this.mThermalHistoryLastRead.setTo(thermalItem);
          writeThermalHistoryDelta(b, this.mThermalHistoryBuffer, this.mThermalHistoryLastRead, this.mThermalHistoryLastLastRead, false);
        } catch (Exception exception) {
          exception.printStackTrace();
        } 
        continue;
      } 
      break;
    } 
    return thermalItem;
  }
  
  public void readThermalHistoryDelta(Parcel paramParcel, OplusBaseBatteryStats.ThermalItem paramThermalItem, long paramLong) {
    int i = paramParcel.readInt();
    paramThermalItem.backlight = i & 0x3FFF;
    paramThermalItem.cpuLoading = i >> 14 & 0x3FF;
    paramThermalItem.cmd = (byte)(i >> 24 & 0xFF);
    i = paramThermalItem.cmd;
    paramLong = paramParcel.readLong();
    paramThermalItem.elapsedRealtime = paramLong >> 5L;
    boolean bool1 = false;
    if ((0x1FL & paramLong) >> 4L == 1L) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    paramThermalItem.cameraOn = bool2;
    if ((0xFL & paramLong) >> 3L == 1L) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    paramThermalItem.audioOn = bool2;
    if ((0x7L & paramLong) >> 2L == 1L) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    paramThermalItem.videoOn = bool2;
    if ((0x3L & paramLong) >> 1L == 1L) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    paramThermalItem.gpsOn = bool2;
    if ((paramLong & 0x1L) == 1L) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    paramThermalItem.flashlightOn = bool2;
    paramLong = paramParcel.readLong();
    paramThermalItem.upTime = paramLong >> 8L;
    paramThermalItem.volume = (int)(0x7FL & paramLong);
    boolean bool2 = bool1;
    if ((paramLong & 0x80L) == 128L)
      bool2 = true; 
    paramThermalItem.isAutoBrightness = bool2;
    if (i == 0) {
      paramThermalItem.currentTime = paramParcel.readLong();
      paramThermalItem.baseElapsedRealtime = paramParcel.readLong();
    } else if (i == 1) {
      i = paramParcel.readInt();
      readThermalBatteryInfo(i, paramThermalItem);
      paramLong = paramParcel.readLong();
      readThermalTempInfo(paramLong, paramThermalItem);
    } else if (i == 2) {
      paramLong = paramParcel.readLong();
      readThermalTempInfo(paramLong, paramThermalItem);
    } else if (i == 4) {
      i = paramParcel.readInt();
      paramThermalItem.wifiStats = symbolInt(i >> 16 & 0xFFFF);
      paramThermalItem.wifiSignal = symbolInt(0xFFFF & i);
    } else if (i == 5) {
      paramThermalItem.phoneOnff = paramParcel.readBoolean();
    } else if (i == 6) {
      paramThermalItem.phoneState = paramParcel.readByte();
    } else if (i == 7) {
      paramThermalItem.phoneSignal = paramParcel.readByte();
    } else if (i == 8) {
      paramThermalItem.dataNetStatus = paramParcel.readBoolean();
    } else if (i == 9) {
      paramThermalItem.connectNetType = paramParcel.readByte();
    } else if (i == 10) {
      paramThermalItem.cameraOn = paramParcel.readBoolean();
    } else if (i == 11) {
      paramThermalItem.audioOn = paramParcel.readBoolean();
    } else if (i == 12) {
      paramThermalItem.videoOn = paramParcel.readBoolean();
    } else if (i == 13) {
      paramThermalItem.gpsOn = paramParcel.readBoolean();
    } else if (i == 14) {
      paramThermalItem.flashlightOn = paramParcel.readBoolean();
    } else if (i == 15) {
      paramThermalItem.jobSchedule = paramParcel.readString();
    } else if (i == 16) {
      paramThermalItem.netSync = paramParcel.readString();
    } else if (i == 17) {
      paramThermalItem.foreProc = paramParcel.readString();
      paramThermalItem.versionName = paramParcel.readString();
    } else if (i == 18) {
      paramThermalItem.topProc = paramParcel.readString();
      paramThermalItem.topCpu = paramParcel.readInt();
    } else if (i == 19) {
      paramThermalItem.currentTime = paramParcel.readLong();
      paramThermalItem.baseElapsedRealtime = paramParcel.readLong();
      i = paramParcel.readInt();
      readThermalBatteryInfo(i, paramThermalItem);
      paramLong = paramParcel.readLong();
      readThermalTempInfo(paramLong, paramThermalItem);
      paramThermalItem.thermalRatio = paramParcel.readByte();
      paramThermalItem.thermalRatio1 = paramParcel.readByte();
      paramThermalItem.thermalRatio2 = paramParcel.readByte();
      paramThermalItem.thermalRatio3 = paramParcel.readByte();
      paramThermalItem.enviTemp = paramParcel.readInt();
      i = paramParcel.readInt();
      paramThermalItem.wifiStats = symbolInt(i >> 16 & 0xFFFF);
      paramThermalItem.wifiSignal = symbolInt(0xFFFF & i);
      paramThermalItem.phoneOnff = paramParcel.readBoolean();
      paramThermalItem.phoneState = paramParcel.readByte();
      paramThermalItem.phoneSignal = paramParcel.readByte();
      paramThermalItem.dataNetStatus = paramParcel.readBoolean();
      paramThermalItem.connectNetType = paramParcel.readByte();
      paramThermalItem.jobSchedule = paramParcel.readString();
      paramThermalItem.netSync = paramParcel.readString();
      paramThermalItem.foreProc = paramParcel.readString();
      paramThermalItem.versionName = paramParcel.readString();
      paramThermalItem.topProc = paramParcel.readString();
      paramThermalItem.topCpu = paramParcel.readInt();
    } else if (i == 20) {
      paramThermalItem.thermalRatio = paramParcel.readByte();
    } else if (i == 21) {
      paramThermalItem.thermalRatio1 = paramParcel.readByte();
    } else if (i == 22) {
      paramThermalItem.thermalRatio2 = paramParcel.readByte();
    } else if (i == 23) {
      paramThermalItem.thermalRatio3 = paramParcel.readByte();
    } else if (i == 24) {
      paramThermalItem.enviTemp = paramParcel.readInt();
    } else if (i == 25) {
      paramThermalItem.currentTime = paramParcel.readLong();
      paramThermalItem.baseElapsedRealtime = paramParcel.readLong();
    } 
  }
  
  public void startUploadTemp() {
    BackgroundThread.getHandler().post(new Runnable() {
          final OplusThermalStatsHelper this$0;
          
          public void run() {
            OplusThermalStatsHelper.this.startIteratingThermalHistoryLocked();
            OplusThermalStatsHelper.this.uploadThermalTemp();
            OplusThermalStatsHelper.this.clearThermalStatsBuffer();
            if (OplusThermalStatsHelper.this.mSystemDir != null && OplusThermalStatsHelper.this.mThermalRecFile != null && 
              OplusThermalStatsHelper.this.mThermalRecFile.exists()) {
              SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy-MM-dd-HH-mm", Locale.US);
              StringBuilder stringBuilder1 = new StringBuilder();
              stringBuilder1.append(OplusThermalStatsHelper.this.mSystemDir.toString());
              stringBuilder1.append("/thermal/dcs");
              File file2 = new File(stringBuilder1.toString());
              if (!file2.isDirectory() || !file2.exists())
                file2.mkdirs(); 
              StringBuilder stringBuilder3 = new StringBuilder();
              stringBuilder3.append(OplusThermalStatsHelper.this.mSystemDir.toString());
              stringBuilder3.append("/thermal/dcs/thermalstats.bin");
              File file3 = new File(stringBuilder3.toString());
              if (file3.exists())
                file3.delete(); 
              StringBuilder stringBuilder2 = new StringBuilder();
              stringBuilder2.append("thermalstats_");
              stringBuilder2.append(simpleDateFormat.format(Long.valueOf(System.currentTimeMillis())));
              stringBuilder2.append(".bin");
              File file1 = new File(file2, stringBuilder2.toString());
              OplusThermalStatsHelper.this.mThermalRecFile.renameTo(file1);
              File[] arrayOfFile = file2.listFiles();
              if (arrayOfFile.length > 14) {
                byte b = 0;
                file2 = arrayOfFile[0];
                for (int i = arrayOfFile.length; b < i; ) {
                  File file = arrayOfFile[b];
                  file1 = file2;
                  if (file.lastModified() < file2.lastModified())
                    file1 = file; 
                  b++;
                  file2 = file1;
                } 
                file2.delete();
              } 
            } 
            OplusThermalStatsHelper.this.mThermalHistoryCur.cmd = 19;
            OplusThermalStatsHelper oplusThermalStatsHelper = OplusThermalStatsHelper.this;
            oplusThermalStatsHelper.addThermalHistoryBufferLocked((byte)19, oplusThermalStatsHelper.mThermalHistoryCur, true);
            OplusThermalStatsHelper.this.finishIteratingThermalHistoryLocked();
            if (OplusThermalStatsHelper.this.mHandler.hasMessages(60))
              OplusThermalStatsHelper.this.mHandler.removeMessages(60); 
            OplusThermalStatsHelper.this.mHandler.sendEmptyMessageDelayed(60, 4000L);
            Intent intent = new Intent("oppo.intent.action.ACTION_OPPO_UPLOADFCC");
            intent.putExtra("fcc", OplusThermalStatsHelper.this.mBatteryFcc);
            if (OplusThermalStatsHelper.this.mContext != null)
              OplusThermalStatsHelper.this.mContext.sendBroadcastAsUser(intent, UserHandle.CURRENT); 
          }
        });
  }
  
  public void startUploadMonitorApp() {
    BackgroundThread.getHandler().post(new Runnable() {
          final OplusThermalStatsHelper this$0;
          
          public void run() {
            OplusThermalStatsHelper.this.uploadMonitorApp();
          }
        });
  }
  
  public void startUploadChargeMap() {
    BackgroundThread.getHandler().post(new Runnable() {
          final OplusThermalStatsHelper this$0;
          
          public void run() {
            OplusThermalStatsHelper.this.uploadChargeMap();
          }
        });
  }
  
  private boolean copyFile(File paramFile1, File paramFile2) {
    try {
      FileInputStream fileInputStream = new FileInputStream();
      this(paramFile1);
      BufferedInputStream bufferedInputStream = new BufferedInputStream();
      this(fileInputStream);
      if (!paramFile2.exists())
        paramFile2.createNewFile(); 
      FileOutputStream fileOutputStream = new FileOutputStream();
      this(paramFile2);
      BufferedOutputStream bufferedOutputStream = new BufferedOutputStream();
      this(fileOutputStream);
      byte[] arrayOfByte = new byte[4096];
      while (bufferedInputStream.read(arrayOfByte) != -1)
        bufferedOutputStream.write(arrayOfByte); 
      bufferedOutputStream.flush();
      fileInputStream.close();
      bufferedInputStream.close();
      fileOutputStream.close();
      bufferedOutputStream.close();
      return true;
    } catch (IOException iOException) {
      iOException.printStackTrace();
      return false;
    } 
  }
  
  public void writeThermalRecFile() {
    synchronized (this.mLock) {
      if (this.mThermalFeatureOn) {
        boolean bool = this.mIteratingThermalHistory;
        if (!bool) {
          try {
            FileOutputStream fileOutputStream = new FileOutputStream();
            this(this.mThermalRecFile, true);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream();
            this(fileOutputStream);
            bufferedOutputStream.write(this.mThermalBuilder.toString().getBytes());
            bufferedOutputStream.flush();
            fileOutputStream.close();
            bufferedOutputStream.close();
            this.mThermalBuilder.delete(0, this.mThermalBuilder.length());
          } catch (IOException iOException) {
            Slog.w("BatteryStats", "Error writing thermal record file battery statistics", iOException);
          } 
          return;
        } 
      } 
      return;
    } 
  }
  
  private void initUploadAlarm() {
    BackgroundThread.getHandler().post(new Runnable() {
          final OplusThermalStatsHelper this$0;
          
          public void run() {
            if (OplusThermalStatsHelper.this.mContext == null || !OplusThermalStatsHelper.this.mThermalFeatureOn)
              return; 
            if (OplusThermalStatsHelper.this.mReceiver == null) {
              OplusThermalStatsHelper oplusThermalStatsHelper = OplusThermalStatsHelper.this;
              OplusThermalStatsHelper.access$1102(oplusThermalStatsHelper, new OplusThermalStatsHelper.ThermalReceiver());
              IntentFilter intentFilter = new IntentFilter();
              intentFilter.addAction("oppo.android.internal.thermalupload.ALARM_WAKEUP");
              intentFilter.addAction("android.media.VOLUME_CHANGED_ACTION");
              OplusThermalStatsHelper.this.mContext.registerReceiverAsUser(OplusThermalStatsHelper.this.mReceiver, UserHandle.CURRENT, intentFilter, null, null);
            } 
            if (OplusThermalStatsHelper.this.mAlarmManager != null && OplusThermalStatsHelper.this.mWakeupIntent != null) {
              OplusThermalStatsHelper.this.mAlarmManager.cancel(OplusThermalStatsHelper.this.mWakeupIntent);
            } else {
              OplusThermalStatsHelper oplusThermalStatsHelper = OplusThermalStatsHelper.this;
              OplusThermalStatsHelper.access$1302(oplusThermalStatsHelper, (AlarmManager)oplusThermalStatsHelper.mContext.getSystemService("alarm"));
              oplusThermalStatsHelper = OplusThermalStatsHelper.this;
              OplusThermalStatsHelper.access$1402(oplusThermalStatsHelper, PendingIntent.getBroadcast(oplusThermalStatsHelper.mContext, 0, new Intent("oppo.android.internal.thermalupload.ALARM_WAKEUP"), 0));
            } 
            if (OplusThermalStatsHelper.this.mPowerManager != null && OplusThermalStatsHelper.this.mWakeLock != null) {
              if (OplusThermalStatsHelper.this.mWakeLock.isHeld())
                OplusThermalStatsHelper.this.mWakeLock.release(); 
            } else {
              OplusThermalStatsHelper oplusThermalStatsHelper = OplusThermalStatsHelper.this;
              OplusThermalStatsHelper.access$1502(oplusThermalStatsHelper, (PowerManager)oplusThermalStatsHelper.mContext.getSystemService("power"));
              oplusThermalStatsHelper = OplusThermalStatsHelper.this;
              OplusThermalStatsHelper.access$1602(oplusThermalStatsHelper, oplusThermalStatsHelper.mPowerManager.newWakeLock(1, "thermalUpload"));
            } 
            if (OplusThermalStatsHelper.this.mPackageManger == null) {
              OplusThermalStatsHelper oplusThermalStatsHelper = OplusThermalStatsHelper.this;
              OplusThermalStatsHelper.access$1702(oplusThermalStatsHelper, oplusThermalStatsHelper.mContext.getPackageManager());
            } 
            Slog.i("OppoThermalStats", " initUploadAlarm ");
            Calendar calendar = Calendar.getInstance();
            int i = calendar.get(6);
            int j = calendar.get(1);
            if (i >= 365) {
              calendar.set(1, j + 1);
              calendar.set(6, 1);
            } else {
              calendar.set(6, i + 1);
            } 
            calendar.set(11, 0);
            new Random();
            calendar.set(12, 10);
            calendar.set(13, 0);
            OplusThermalStatsHelper.this.mAlarmManager.setExact(0, calendar.getTimeInMillis(), OplusThermalStatsHelper.this.mWakeupIntent);
          }
        });
  }
  
  private void cancleUploadAlarm() {
    BackgroundThread.getHandler().post(new Runnable() {
          final OplusThermalStatsHelper this$0;
          
          public void run() {
            if (OplusThermalStatsHelper.this.mAlarmManager != null && OplusThermalStatsHelper.this.mWakeupIntent != null)
              OplusThermalStatsHelper.this.mAlarmManager.cancel(OplusThermalStatsHelper.this.mWakeupIntent); 
            if (OplusThermalStatsHelper.this.mPowerManager != null && OplusThermalStatsHelper.this.mWakeLock != null && 
              OplusThermalStatsHelper.this.mWakeLock.isHeld())
              OplusThermalStatsHelper.this.mWakeLock.release(); 
            if (OplusThermalStatsHelper.this.mReceiver != null && OplusThermalStatsHelper.this.mContext != null)
              try {
                OplusThermalStatsHelper.this.mContext.unregisterReceiver(OplusThermalStatsHelper.this.mReceiver);
              } catch (Exception exception) {
                exception.printStackTrace();
              }  
          }
        });
  }
  
  public void clearThermalAllHistory() {
    BackgroundThread.getHandler().post(new Runnable() {
          final OplusThermalStatsHelper this$0;
          
          public void run() {
            OplusThermalStatsHelper.this.writeThermalRecFile();
            OplusThermalStatsHelper.this.startIteratingThermalHistoryLocked();
            OplusThermalStatsHelper.this.mThermalRecFile.delete();
            OplusThermalStatsHelper.this.clearThermalStatsBuffer();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(OplusThermalStatsHelper.this.mSystemDir.toString());
            stringBuilder.append("/thermal/dcs");
            file = new File(stringBuilder.toString());
            if (file.isDirectory() && file.exists())
              for (File file : file.listFiles())
                file.delete();  
            OplusThermalStatsHelper.this.finishIteratingThermalHistoryLocked();
            OplusThermalStatsHelper.this.resetThermalHistory();
          }
        });
  }
  
  public void backupThermalStatsFile() {
    BackgroundThread.getHandler().post(new Runnable() {
          final OplusThermalStatsHelper this$0;
          
          public void run() {
            OplusThermalStatsHelper.this.writeThermalRecFile();
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append(OplusThermalStatsHelper.this.mSystemDir.toString());
            stringBuilder2.append("/thermal/dcs");
            File file = new File(stringBuilder2.toString());
            if (!file.isDirectory() || !file.exists())
              file.mkdirs(); 
            OplusThermalStatsHelper oplusThermalStatsHelper = OplusThermalStatsHelper.this;
            file = oplusThermalStatsHelper.mThermalRecFile;
            StringBuilder stringBuilder3 = new StringBuilder();
            stringBuilder3.append(OplusThermalStatsHelper.this.mSystemDir);
            stringBuilder3.append("/thermal/dcs/");
            stringBuilder3.append(OplusThermalStatsHelper.this.mThermalRecFile.getName());
            oplusThermalStatsHelper.copyFile(file, new File(stringBuilder3.toString()));
            Intent intent = new Intent("oppo.intent.action.ACTION_OPPO_SAVE_THERMAL_HISTORY");
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append(OplusThermalStatsHelper.this.mSystemDir);
            stringBuilder1.append("/thermal/dcs/");
            intent.putExtra("save_path", stringBuilder1.toString());
            intent.putExtra("save_to_path", "thermalrec");
            intent.setPackage("com.oppo.oppopowermonitor");
            if (OplusThermalStatsHelper.this.mContext != null)
              OplusThermalStatsHelper.this.mContext.sendBroadcastAsUser(intent, UserHandle.CURRENT); 
          }
        });
  }
  
  public void backupThermalLogFile() {
    BackgroundThread.getHandler().post(new Runnable() {
          final OplusThermalStatsHelper this$0;
          
          public void run() {
            Intent intent = new Intent("oppo.intent.action.ACTION_OPPO_SAVE_THERMAL_HISTORY");
            intent.putExtra("save_path", "data/oppo/psw/thermal_backup/");
            intent.putExtra("save_to_path", "thermallog");
            intent.setPackage("com.oppo.oppopowermonitor");
            if (OplusThermalStatsHelper.this.mContext != null)
              OplusThermalStatsHelper.this.mContext.sendBroadcastAsUser(intent, UserHandle.CURRENT); 
          }
        });
  }
  
  public void dumpThemalHeatDetailLocked(PrintWriter paramPrintWriter) {
    this.mHeatReasonDetails.dumpThemalHeatDetailLocked(paramPrintWriter);
  }
  
  public void uploadThermalTemp() {
    if (this.mThermalUploadDcs) {
      Map<String, String> map = getUploadThermalTemp();
      try {
        for (Map.Entry<String, String> entry : map.entrySet()) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("uploadThermalTemp ");
          stringBuilder.append((String)entry.getKey());
          stringBuilder.append(":");
          stringBuilder.append((String)entry.getValue());
          Slog.i("OppoThermalStats", stringBuilder.toString());
        } 
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
      if (map.size() > 0) {
        Context context = this.mContext;
        if (context != null)
          ThermalStatistics.onCommon(context, "20139", "id_thermal_temp", map, false); 
      } 
      map = getStampThermalTemp();
      try {
        for (Map.Entry<String, String> entry : map.entrySet()) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("uploadStampTemp ");
          stringBuilder.append((String)entry.getKey());
          stringBuilder.append(":");
          stringBuilder.append((String)entry.getValue());
          Slog.i("OppoThermalStats", stringBuilder.toString());
        } 
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
      if (map.size() > 0)
        OplusManager.onStamp("040201", map); 
    } 
  }
  
  public void uploadMonitorApp() {
    synchronized (this.mLock) {
      if (this.mMonitorAppMap == null)
        return; 
      try {
        for (Map.Entry<String, String> entry : this.mMonitorAppMap.entrySet()) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("uploadMonitorApp ");
          stringBuilder.append((String)entry.getKey());
          stringBuilder.append(":");
          stringBuilder.append((String)entry.getValue());
          Slog.i("OppoThermalStats", stringBuilder.toString());
        } 
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
      if (this.mMonitorAppMap.size() > 0 && 
        this.mContext != null)
        ThermalStatistics.onCommon(this.mContext, "20139", "id_thermal_monitor_app", this.mMonitorAppMap, false); 
      return;
    } 
  }
  
  public void uploadChargeMap() {
    Map<String, String> map = this.mChargeUploadMap;
    if (map == null)
      return; 
    for (Map.Entry<String, String> entry : map.entrySet()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("mChargeUploadMap ");
      stringBuilder.append((String)entry.getKey());
      stringBuilder.append(":");
      stringBuilder.append((String)entry.getValue());
      Slog.i("OppoThermalStats", stringBuilder.toString());
    } 
    if (this.mChargeUploadMap.size() > 0) {
      Context context = this.mContext;
      if (context != null)
        ThermalStatistics.onCommon(context, "20139", "id_charge_map", this.mChargeUploadMap, false); 
    } 
  }
  
  public void getPhoneTemp(PrintWriter paramPrintWriter) {
    paramPrintWriter.println(this.mThermalHistoryCur.phoneTemp);
  }
  
  private Map<String, String> getUploadThermalTemp() {
    synchronized (this.mLock) {
      HashMap<Object, Object> hashMap = new HashMap<>();
      this();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      int i = 0, j = 0;
      int k = 36;
      byte b = 36;
      try {
        Iterator<Map.Entry> iterator = this.mThermalTempMap.entrySet().iterator();
        while (true) {
          i = j;
          if (iterator.hasNext()) {
            i = j;
            Map.Entry entry = iterator.next();
            i = j;
            int m = (int)(((Long)entry.getValue()).longValue() / 60000L);
            boolean bool = false;
            j += m;
            int n = k;
            if (m > 0) {
              n = k;
              if (m < 2000) {
                i = j;
                StringBuilder stringBuilder1 = new StringBuilder();
                i = j;
                this();
                i = j;
                stringBuilder1.append("phonetemp");
                i = j;
                stringBuilder1.append((String)entry.getKey());
                i = j;
                hashMap.put(stringBuilder1.toString(), Integer.toString(m));
                try {
                  i = Integer.parseInt((String)entry.getKey());
                } catch (Exception exception) {
                  i = j;
                  exception.printStackTrace();
                  i = bool;
                } 
                n = k;
                if (k < i)
                  n = i; 
              } 
            } 
            k = n;
            continue;
          } 
          break;
        } 
        i = j;
        hashMap.put("maxPhoneTemp", Integer.toString(k));
      } catch (Exception exception) {
        exception.printStackTrace();
        j = i;
      } 
      i = j;
      try {
        Iterator<Map.Entry> iterator = this.mBatTempMap.entrySet().iterator();
        k = b;
        while (true) {
          i = j;
          if (iterator.hasNext()) {
            i = j;
            Map.Entry entry = iterator.next();
            i = j;
            int m = (int)(((Long)entry.getValue()).longValue() / 60000L);
            b = 0;
            j += m;
            int n = k;
            if (m > 0) {
              n = k;
              if (m < 2000) {
                i = j;
                StringBuilder stringBuilder1 = new StringBuilder();
                i = j;
                this();
                i = j;
                stringBuilder1.append("battemp");
                i = j;
                stringBuilder1.append((String)entry.getKey());
                i = j;
                hashMap.put(stringBuilder1.toString(), Integer.toString(m));
                try {
                  i = Integer.parseInt((String)entry.getKey());
                } catch (Exception exception) {
                  i = j;
                  exception.printStackTrace();
                  i = b;
                } 
                n = k;
                if (k < i)
                  n = i; 
              } 
            } 
            k = n;
            continue;
          } 
          break;
        } 
        i = j;
        hashMap.put("maxBatTemp", Integer.toString(k));
        i = j;
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
      try {
        for (Map.Entry<String, Integer> entry : this.mThermalHourMap.entrySet()) {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append((String)entry.getKey());
          stringBuilder1.append(":");
          stringBuilder1.append(Integer.toString(((Integer)entry.getValue()).intValue()));
          stringBuilder1.append(";");
          stringBuilder.append(stringBuilder1.toString());
        } 
        hashMap.put("hourtempMap", stringBuilder.toString());
        Slog.d("Upload hourtemp", "put hourtemp in upLoadMap");
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
      if (i > 4000) {
        hashMap.clear();
        return (Map)hashMap;
      } 
      hashMap.put("holdtimeThreshold", Integer.toString(this.mHeatHoldTimeThreshold));
      hashMap.put("moreHeatThreshold", Integer.toString(this.mMoreHeatThreshold / 10));
      hashMap.put("heatThreshold", Integer.toString(this.mHeatThreshold / 10));
      hashMap.put("lessHeatThreshold", Integer.toString(this.mLessHeatThreshold / 10));
      return (Map)hashMap;
    } 
  }
  
  private Map<String, String> getUploadChargeMap() {
    synchronized (this.mLock) {
      HashMap<Object, Object> hashMap = new HashMap<>();
      this();
      try {
        for (Map.Entry<String, Long> entry : this.mTempChargeUploadMap.entrySet()) {
          if (((String)entry.getKey()).equals("chargeTime")) {
            long l = ((Long)entry.getValue()).longValue();
            hashMap.put("chargeTime", Long.toString(l / 60000L));
            continue;
          } 
          if (((String)entry.getKey()).equals("startTime")) {
            long l = ((Long)entry.getValue()).longValue();
            hashMap.put("startTime", DateFormat.format("yyyy-MM-dd-HH-mm-ss", l).toString());
            continue;
          } 
          if (((String)entry.getKey()).equals("endTime")) {
            long l = ((Long)entry.getValue()).longValue();
            hashMap.put("endTime", DateFormat.format("yyyy-MM-dd-HH-mm-ss", l).toString());
            continue;
          } 
          if (((String)entry.getKey()).equals("chargePlug")) {
            int i = (int)(((Long)entry.getValue()).longValue() & 0xFFFFL);
            if (i != 1) {
              if (i != 2) {
                if (i != 4) {
                  hashMap.put("chargePlug", "none");
                  continue;
                } 
                hashMap.put("chargePlug", "wireless");
                continue;
              } 
              hashMap.put("chargePlug", "usb");
              continue;
            } 
            if (this.mGlobalFastCharger) {
              hashMap.put("chargePlug", "ac_fast");
              continue;
            } 
            hashMap.put("chargePlug", "ac_normal");
            continue;
          } 
          if (((String)entry.getKey()).equals("screenOnTime")) {
            long l = ((Long)entry.getValue()).longValue();
            hashMap.put("screenOnTime", Long.toString(l / 60000L));
            continue;
          } 
          if (((String)entry.getKey()).equals("f2nTime")) {
            long l = ((Long)entry.getValue()).longValue();
            hashMap.put("f2nTime", Long.toString(l / 60000L));
            continue;
          } 
          hashMap.put(entry.getKey(), Long.toString(((Long)entry.getValue()).longValue()));
        } 
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
      hashMap.put("chargeId", Integer.toString(this.mGlobalChargeId));
      return (Map)hashMap;
    } 
  }
  
  private Map<String, String> getStampThermalTemp() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLock : Ljava/lang/Object;
    //   4: astore_1
    //   5: aload_1
    //   6: monitorenter
    //   7: new java/util/HashMap
    //   10: astore_2
    //   11: aload_2
    //   12: invokespecial <init> : ()V
    //   15: iconst_0
    //   16: istore_3
    //   17: iconst_0
    //   18: istore #4
    //   20: bipush #36
    //   22: istore #5
    //   24: bipush #36
    //   26: istore #6
    //   28: new java/lang/StringBuilder
    //   31: astore #7
    //   33: aload #7
    //   35: invokespecial <init> : ()V
    //   38: new java/lang/StringBuilder
    //   41: astore #8
    //   43: aload #8
    //   45: invokespecial <init> : ()V
    //   48: new java/lang/StringBuilder
    //   51: astore #9
    //   53: aload #9
    //   55: invokespecial <init> : ()V
    //   58: aload_2
    //   59: ldc_w 'key'
    //   62: ldc_w 'TempMap'
    //   65: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   70: pop
    //   71: iload #5
    //   73: istore #10
    //   75: aload #8
    //   77: astore #11
    //   79: aload #9
    //   81: astore #12
    //   83: aload_0
    //   84: getfield mThermalTempMap : Ljava/util/Map;
    //   87: invokeinterface entrySet : ()Ljava/util/Set;
    //   92: invokeinterface iterator : ()Ljava/util/Iterator;
    //   97: astore #13
    //   99: iload #4
    //   101: istore_3
    //   102: iload #5
    //   104: istore #10
    //   106: aload #8
    //   108: astore #11
    //   110: aload #9
    //   112: astore #12
    //   114: aload #13
    //   116: invokeinterface hasNext : ()Z
    //   121: ifeq -> 393
    //   124: iload #4
    //   126: istore_3
    //   127: iload #5
    //   129: istore #10
    //   131: aload #8
    //   133: astore #11
    //   135: aload #9
    //   137: astore #12
    //   139: aload #13
    //   141: invokeinterface next : ()Ljava/lang/Object;
    //   146: checkcast java/util/Map$Entry
    //   149: astore #14
    //   151: iload #4
    //   153: istore_3
    //   154: iload #5
    //   156: istore #10
    //   158: aload #8
    //   160: astore #11
    //   162: aload #9
    //   164: astore #12
    //   166: aload #14
    //   168: invokeinterface getValue : ()Ljava/lang/Object;
    //   173: ifnonnull -> 179
    //   176: goto -> 99
    //   179: iload #4
    //   181: istore_3
    //   182: iload #5
    //   184: istore #10
    //   186: aload #8
    //   188: astore #11
    //   190: aload #9
    //   192: astore #12
    //   194: aload #14
    //   196: invokeinterface getValue : ()Ljava/lang/Object;
    //   201: checkcast java/lang/Long
    //   204: invokevirtual longValue : ()J
    //   207: lstore #15
    //   209: iload #4
    //   211: istore_3
    //   212: lload #15
    //   214: ldc2_w 60000
    //   217: ldiv
    //   218: lstore #15
    //   220: lload #15
    //   222: l2i
    //   223: istore #17
    //   225: iconst_0
    //   226: istore #18
    //   228: iload #4
    //   230: iload #17
    //   232: iadd
    //   233: istore #10
    //   235: iload #5
    //   237: istore_3
    //   238: iload #17
    //   240: ifle -> 374
    //   243: iload #5
    //   245: istore_3
    //   246: iload #17
    //   248: sipush #2000
    //   251: if_icmpge -> 374
    //   254: new java/lang/StringBuilder
    //   257: astore #12
    //   259: aload #12
    //   261: invokespecial <init> : ()V
    //   264: aload #12
    //   266: ldc_w 'phonetemp'
    //   269: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   272: pop
    //   273: aload #12
    //   275: aload #14
    //   277: invokeinterface getKey : ()Ljava/lang/Object;
    //   282: checkcast java/lang/String
    //   285: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   288: pop
    //   289: aload #12
    //   291: ldc_w ':'
    //   294: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   297: pop
    //   298: aload #12
    //   300: iload #17
    //   302: invokestatic toString : (I)Ljava/lang/String;
    //   305: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   308: pop
    //   309: aload #12
    //   311: ldc_w ';'
    //   314: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   317: pop
    //   318: aload #7
    //   320: aload #12
    //   322: invokevirtual toString : ()Ljava/lang/String;
    //   325: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   328: pop
    //   329: aload #14
    //   331: invokeinterface getKey : ()Ljava/lang/Object;
    //   336: checkcast java/lang/String
    //   339: invokestatic parseInt : (Ljava/lang/String;)I
    //   342: istore #4
    //   344: goto -> 361
    //   347: astore #12
    //   349: iload #10
    //   351: istore_3
    //   352: aload #12
    //   354: invokevirtual printStackTrace : ()V
    //   357: iload #18
    //   359: istore #4
    //   361: iload #5
    //   363: istore_3
    //   364: iload #5
    //   366: iload #4
    //   368: if_icmpge -> 374
    //   371: iload #4
    //   373: istore_3
    //   374: iload #10
    //   376: istore #4
    //   378: iload_3
    //   379: istore #5
    //   381: goto -> 99
    //   384: astore #11
    //   386: aload #8
    //   388: astore #12
    //   390: goto -> 429
    //   393: aload #8
    //   395: astore #12
    //   397: aload #9
    //   399: astore #8
    //   401: iload #4
    //   403: istore_3
    //   404: aload #12
    //   406: astore #9
    //   408: goto -> 442
    //   411: astore #8
    //   413: aload #12
    //   415: astore #9
    //   417: aload #11
    //   419: astore #12
    //   421: iload #10
    //   423: istore #5
    //   425: aload #8
    //   427: astore #11
    //   429: aload #11
    //   431: invokevirtual printStackTrace : ()V
    //   434: aload #9
    //   436: astore #8
    //   438: aload #12
    //   440: astore #9
    //   442: aload_2
    //   443: ldc_w 'phoneTempMap'
    //   446: aload #7
    //   448: invokevirtual toString : ()Ljava/lang/String;
    //   451: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   456: pop
    //   457: aload_2
    //   458: ldc_w 'maxPhoneTemp'
    //   461: iload #5
    //   463: invokestatic toString : (I)Ljava/lang/String;
    //   466: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   471: pop
    //   472: aload #8
    //   474: astore #12
    //   476: aload_0
    //   477: getfield mThermalHourMap : Ljava/util/Map;
    //   480: invokeinterface entrySet : ()Ljava/util/Set;
    //   485: invokeinterface iterator : ()Ljava/util/Iterator;
    //   490: astore #11
    //   492: aload #8
    //   494: astore #12
    //   496: aload #11
    //   498: invokeinterface hasNext : ()Z
    //   503: ifeq -> 634
    //   506: aload #8
    //   508: astore #12
    //   510: aload #11
    //   512: invokeinterface next : ()Ljava/lang/Object;
    //   517: checkcast java/util/Map$Entry
    //   520: astore #7
    //   522: aload #8
    //   524: astore #12
    //   526: new java/lang/StringBuilder
    //   529: astore #13
    //   531: aload #8
    //   533: astore #12
    //   535: aload #13
    //   537: invokespecial <init> : ()V
    //   540: aload #8
    //   542: astore #12
    //   544: aload #13
    //   546: aload #7
    //   548: invokeinterface getKey : ()Ljava/lang/Object;
    //   553: checkcast java/lang/String
    //   556: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   559: pop
    //   560: aload #8
    //   562: astore #12
    //   564: aload #13
    //   566: ldc_w ':'
    //   569: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   572: pop
    //   573: aload #8
    //   575: astore #12
    //   577: aload #13
    //   579: aload #7
    //   581: invokeinterface getValue : ()Ljava/lang/Object;
    //   586: checkcast java/lang/Integer
    //   589: invokevirtual intValue : ()I
    //   592: invokestatic toString : (I)Ljava/lang/String;
    //   595: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   598: pop
    //   599: aload #8
    //   601: astore #12
    //   603: aload #13
    //   605: ldc_w ';'
    //   608: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   611: pop
    //   612: aload #8
    //   614: astore #12
    //   616: aload #13
    //   618: invokevirtual toString : ()Ljava/lang/String;
    //   621: astore #12
    //   623: aload #8
    //   625: aload #12
    //   627: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   630: pop
    //   631: goto -> 492
    //   634: aload_2
    //   635: ldc_w 'hourtempMap'
    //   638: aload #8
    //   640: invokevirtual toString : ()Ljava/lang/String;
    //   643: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   648: pop
    //   649: goto -> 664
    //   652: astore #8
    //   654: goto -> 659
    //   657: astore #8
    //   659: aload #8
    //   661: invokevirtual printStackTrace : ()V
    //   664: iload_3
    //   665: istore #4
    //   667: iload #6
    //   669: istore #10
    //   671: aload #9
    //   673: astore #8
    //   675: aload_0
    //   676: getfield mBatTempMap : Ljava/util/Map;
    //   679: invokeinterface entrySet : ()Ljava/util/Set;
    //   684: invokeinterface iterator : ()Ljava/util/Iterator;
    //   689: astore #12
    //   691: iload #6
    //   693: istore #5
    //   695: iload_3
    //   696: istore #4
    //   698: iload #5
    //   700: istore #10
    //   702: aload #9
    //   704: astore #8
    //   706: aload #12
    //   708: invokeinterface hasNext : ()Z
    //   713: ifeq -> 1032
    //   716: iload_3
    //   717: istore #4
    //   719: iload #5
    //   721: istore #10
    //   723: aload #9
    //   725: astore #8
    //   727: aload #12
    //   729: invokeinterface next : ()Ljava/lang/Object;
    //   734: checkcast java/util/Map$Entry
    //   737: astore #11
    //   739: iload_3
    //   740: istore #4
    //   742: iload #5
    //   744: istore #10
    //   746: aload #9
    //   748: astore #8
    //   750: aload #11
    //   752: invokeinterface getValue : ()Ljava/lang/Object;
    //   757: checkcast java/lang/Long
    //   760: invokevirtual longValue : ()J
    //   763: ldc2_w 60000
    //   766: ldiv
    //   767: l2i
    //   768: istore #18
    //   770: iconst_0
    //   771: istore #6
    //   773: iload_3
    //   774: iload #18
    //   776: iadd
    //   777: istore_3
    //   778: iload #18
    //   780: ifle -> 1021
    //   783: iload #18
    //   785: sipush #2000
    //   788: if_icmpge -> 1021
    //   791: iload_3
    //   792: istore #4
    //   794: iload #5
    //   796: istore #10
    //   798: aload #9
    //   800: astore #8
    //   802: new java/lang/StringBuilder
    //   805: astore #7
    //   807: iload_3
    //   808: istore #4
    //   810: iload #5
    //   812: istore #10
    //   814: aload #9
    //   816: astore #8
    //   818: aload #7
    //   820: invokespecial <init> : ()V
    //   823: iload_3
    //   824: istore #4
    //   826: iload #5
    //   828: istore #10
    //   830: aload #9
    //   832: astore #8
    //   834: aload #7
    //   836: ldc_w 'battemp'
    //   839: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   842: pop
    //   843: iload_3
    //   844: istore #4
    //   846: iload #5
    //   848: istore #10
    //   850: aload #9
    //   852: astore #8
    //   854: aload #7
    //   856: aload #11
    //   858: invokeinterface getKey : ()Ljava/lang/Object;
    //   863: checkcast java/lang/String
    //   866: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   869: pop
    //   870: iload_3
    //   871: istore #4
    //   873: iload #5
    //   875: istore #10
    //   877: aload #9
    //   879: astore #8
    //   881: aload #7
    //   883: ldc_w ':'
    //   886: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   889: pop
    //   890: iload_3
    //   891: istore #4
    //   893: iload #5
    //   895: istore #10
    //   897: aload #9
    //   899: astore #8
    //   901: aload #7
    //   903: iload #18
    //   905: invokestatic toString : (I)Ljava/lang/String;
    //   908: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   911: pop
    //   912: iload_3
    //   913: istore #4
    //   915: iload #5
    //   917: istore #10
    //   919: aload #9
    //   921: astore #8
    //   923: aload #7
    //   925: ldc_w ';'
    //   928: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   931: pop
    //   932: iload_3
    //   933: istore #4
    //   935: iload #5
    //   937: istore #10
    //   939: aload #9
    //   941: astore #8
    //   943: aload #7
    //   945: invokevirtual toString : ()Ljava/lang/String;
    //   948: astore #7
    //   950: aload #9
    //   952: astore #8
    //   954: aload #8
    //   956: aload #7
    //   958: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   961: pop
    //   962: aload #11
    //   964: invokeinterface getKey : ()Ljava/lang/Object;
    //   969: checkcast java/lang/String
    //   972: invokestatic parseInt : (Ljava/lang/String;)I
    //   975: istore #4
    //   977: goto -> 991
    //   980: astore #11
    //   982: aload #11
    //   984: invokevirtual printStackTrace : ()V
    //   987: iload #6
    //   989: istore #4
    //   991: iload #5
    //   993: istore #10
    //   995: iload #5
    //   997: iload #4
    //   999: if_icmpge -> 1025
    //   1002: iload #4
    //   1004: istore #10
    //   1006: goto -> 1025
    //   1009: astore #12
    //   1011: iload_3
    //   1012: istore #4
    //   1014: aload #8
    //   1016: astore #9
    //   1018: goto -> 1048
    //   1021: iload #5
    //   1023: istore #10
    //   1025: iload #10
    //   1027: istore #5
    //   1029: goto -> 695
    //   1032: iload_3
    //   1033: istore #4
    //   1035: goto -> 1053
    //   1038: astore #12
    //   1040: aload #8
    //   1042: astore #9
    //   1044: iload #10
    //   1046: istore #5
    //   1048: aload #12
    //   1050: invokevirtual printStackTrace : ()V
    //   1053: aload_2
    //   1054: ldc_w 'batTempMap'
    //   1057: aload #9
    //   1059: invokevirtual toString : ()Ljava/lang/String;
    //   1062: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1067: pop
    //   1068: aload_2
    //   1069: ldc_w 'maxBatTemp'
    //   1072: iload #5
    //   1074: invokestatic toString : (I)Ljava/lang/String;
    //   1077: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1082: pop
    //   1083: aload_2
    //   1084: ldc_w 'tempTime'
    //   1087: ldc_w 'yyyy-MM-dd-HH-mm-ss'
    //   1090: invokestatic currentTimeMillis : ()J
    //   1093: invokestatic format : (Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;
    //   1096: invokeinterface toString : ()Ljava/lang/String;
    //   1101: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1106: pop
    //   1107: iload #4
    //   1109: sipush #4000
    //   1112: if_icmple -> 1125
    //   1115: aload_2
    //   1116: invokeinterface clear : ()V
    //   1121: aload_1
    //   1122: monitorexit
    //   1123: aload_2
    //   1124: areturn
    //   1125: aload_1
    //   1126: monitorexit
    //   1127: aload_2
    //   1128: areturn
    //   1129: astore #9
    //   1131: aload_1
    //   1132: monitorexit
    //   1133: aload #9
    //   1135: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3151	-> 0
    //   #3152	-> 7
    //   #3153	-> 15
    //   #3154	-> 20
    //   #3155	-> 24
    //   #3156	-> 28
    //   #3157	-> 38
    //   #3160	-> 48
    //   #3162	-> 58
    //   #3164	-> 71
    //   #3167	-> 151
    //   #3168	-> 176
    //   #3171	-> 179
    //   #3172	-> 225
    //   #3173	-> 228
    //   #3174	-> 235
    //   #3177	-> 254
    //   #3178	-> 329
    //   #3182	-> 344
    //   #3179	-> 347
    //   #3181	-> 349
    //   #3183	-> 361
    //   #3184	-> 371
    //   #3187	-> 374
    //   #3188	-> 384
    //   #3190	-> 393
    //   #3188	-> 411
    //   #3189	-> 429
    //   #3191	-> 442
    //   #3192	-> 457
    //   #3196	-> 472
    //   #3197	-> 522
    //   #3198	-> 631
    //   #3199	-> 634
    //   #3202	-> 649
    //   #3200	-> 652
    //   #3201	-> 659
    //   #3205	-> 664
    //   #3206	-> 739
    //   #3207	-> 770
    //   #3208	-> 773
    //   #3209	-> 778
    //   #3211	-> 791
    //   #3213	-> 962
    //   #3217	-> 977
    //   #3214	-> 980
    //   #3216	-> 982
    //   #3218	-> 991
    //   #3219	-> 1002
    //   #3223	-> 1009
    //   #3209	-> 1021
    //   #3222	-> 1025
    //   #3225	-> 1032
    //   #3223	-> 1038
    //   #3224	-> 1048
    //   #3226	-> 1053
    //   #3227	-> 1068
    //   #3228	-> 1083
    //   #3229	-> 1107
    //   #3230	-> 1115
    //   #3231	-> 1121
    //   #3233	-> 1125
    //   #3234	-> 1129
    // Exception table:
    //   from	to	target	type
    //   7	15	1129	finally
    //   28	38	1129	finally
    //   38	48	1129	finally
    //   48	58	1129	finally
    //   58	71	1129	finally
    //   83	99	411	java/lang/Exception
    //   83	99	1129	finally
    //   114	124	411	java/lang/Exception
    //   114	124	1129	finally
    //   139	151	411	java/lang/Exception
    //   139	151	1129	finally
    //   166	176	411	java/lang/Exception
    //   166	176	1129	finally
    //   194	209	411	java/lang/Exception
    //   194	209	1129	finally
    //   212	220	384	java/lang/Exception
    //   212	220	1129	finally
    //   254	329	347	java/lang/Exception
    //   254	329	1129	finally
    //   329	344	347	java/lang/Exception
    //   329	344	1129	finally
    //   352	357	384	java/lang/Exception
    //   352	357	1129	finally
    //   429	434	1129	finally
    //   442	457	1129	finally
    //   457	472	1129	finally
    //   476	492	657	java/lang/Exception
    //   476	492	1129	finally
    //   496	506	657	java/lang/Exception
    //   496	506	1129	finally
    //   510	522	657	java/lang/Exception
    //   510	522	1129	finally
    //   526	531	657	java/lang/Exception
    //   526	531	1129	finally
    //   535	540	657	java/lang/Exception
    //   535	540	1129	finally
    //   544	560	657	java/lang/Exception
    //   544	560	1129	finally
    //   564	573	657	java/lang/Exception
    //   564	573	1129	finally
    //   577	599	657	java/lang/Exception
    //   577	599	1129	finally
    //   603	612	657	java/lang/Exception
    //   603	612	1129	finally
    //   616	623	657	java/lang/Exception
    //   616	623	1129	finally
    //   623	631	652	java/lang/Exception
    //   623	631	1129	finally
    //   634	649	652	java/lang/Exception
    //   634	649	1129	finally
    //   659	664	1129	finally
    //   675	691	1038	java/lang/Exception
    //   675	691	1129	finally
    //   706	716	1038	java/lang/Exception
    //   706	716	1129	finally
    //   727	739	1038	java/lang/Exception
    //   727	739	1129	finally
    //   750	770	1038	java/lang/Exception
    //   750	770	1129	finally
    //   802	807	1038	java/lang/Exception
    //   802	807	1129	finally
    //   818	823	1038	java/lang/Exception
    //   818	823	1129	finally
    //   834	843	1038	java/lang/Exception
    //   834	843	1129	finally
    //   854	870	1038	java/lang/Exception
    //   854	870	1129	finally
    //   881	890	1038	java/lang/Exception
    //   881	890	1129	finally
    //   901	912	1038	java/lang/Exception
    //   901	912	1129	finally
    //   923	932	1038	java/lang/Exception
    //   923	932	1129	finally
    //   943	950	1038	java/lang/Exception
    //   943	950	1129	finally
    //   954	962	1009	java/lang/Exception
    //   954	962	1129	finally
    //   962	977	980	java/lang/Exception
    //   962	977	1129	finally
    //   982	987	1009	java/lang/Exception
    //   982	987	1129	finally
    //   1048	1053	1129	finally
    //   1053	1068	1129	finally
    //   1068	1083	1129	finally
    //   1083	1107	1129	finally
    //   1115	1121	1129	finally
    //   1121	1123	1129	finally
    //   1125	1127	1129	finally
    //   1131	1133	1129	finally
  }
  
  private Map<String, String> getStampThermalHeat(Map<String, String> paramMap) {
    HashMap<Object, Object> hashMap = new HashMap<>();
    StringBuilder stringBuilder = new StringBuilder();
    hashMap.put("key", "HeatMap");
    try {
      for (Map.Entry<String, String> entry : paramMap.entrySet()) {
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append((String)entry.getKey());
        stringBuilder1.append(":");
        stringBuilder1.append((String)entry.getValue());
        stringBuilder1.append(";");
        stringBuilder.append(stringBuilder1.toString());
      } 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    hashMap.put("phoneHeatMap", stringBuilder.toString());
    hashMap.put("heatTime", DateFormat.format("yyyy-MM-dd-HH-mm-ss", System.currentTimeMillis()).toString());
    return (Map)hashMap;
  }
  
  private Map<String, String> getMonitorAppMap(String paramString1, String paramString2) {
    HashMap<Object, Object> hashMap = new HashMap<>();
    long l1 = 0L;
    long l2 = 0L;
    long l3 = 0L;
    long l4 = 0L;
    long l5 = 0L;
    long l6 = 0L;
    long l7 = 0L;
    Map<String, Long> map3 = this.mTempMonitorAppMap;
    StringBuilder stringBuilder6 = new StringBuilder();
    stringBuilder6.append(paramString1);
    long l8 = 0L;
    stringBuilder6.append("--Time");
    if (map3.containsKey(stringBuilder6.toString())) {
      map3 = this.mTempMonitorAppMap;
      stringBuilder6 = new StringBuilder();
      stringBuilder6.append(paramString1);
      stringBuilder6.append("--Time");
      l9 = ((Long)map3.get(stringBuilder6.toString())).longValue();
    } else {
      l9 = 0L;
    } 
    map3 = this.mTempMonitorAppMap;
    long l10 = 0L;
    stringBuilder6 = new StringBuilder();
    stringBuilder6.append(paramString1);
    stringBuilder6.append("--Charge");
    if (map3.containsKey(stringBuilder6.toString())) {
      Map<String, Long> map = this.mTempMonitorAppMap;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append("--Charge");
      l10 = ((Long)map.get(stringBuilder.toString())).longValue();
    } 
    Map<String, Long> map5 = this.mTempMonitorAppMap;
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append(paramString1);
    long l11 = 0L;
    stringBuilder3.append("--Volume");
    if (map5.containsKey(stringBuilder3.toString())) {
      Map<String, Long> map = this.mTempMonitorAppMap;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append("--Volume");
      l8 = ((Long)map.get(stringBuilder.toString())).longValue();
    } 
    Map<String, Long> map2 = this.mTempMonitorAppMap;
    StringBuilder stringBuilder5 = new StringBuilder();
    stringBuilder5.append(paramString1);
    stringBuilder5.append("--Backlight");
    if (map2.containsKey(stringBuilder5.toString())) {
      map2 = this.mTempMonitorAppMap;
      stringBuilder5 = new StringBuilder();
      stringBuilder5.append(paramString1);
      stringBuilder5.append("--Backlight");
      l1 = ((Long)map2.get(stringBuilder5.toString())).longValue();
    } 
    map2 = this.mTempMonitorAppMap;
    stringBuilder5 = new StringBuilder();
    stringBuilder5.append(paramString1);
    stringBuilder5.append("--StartBatRm");
    if (map2.containsKey(stringBuilder5.toString())) {
      Map<String, Long> map = this.mTempMonitorAppMap;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append("--StartBatRm");
      l2 = ((Long)map.get(stringBuilder.toString())).longValue();
    } 
    map2 = this.mTempMonitorAppMap;
    stringBuilder5 = new StringBuilder();
    stringBuilder5.append(paramString1);
    stringBuilder5.append("--EndBatRm");
    if (map2.containsKey(stringBuilder5.toString())) {
      Map<String, Long> map = this.mTempMonitorAppMap;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append("--EndBatRm");
      l11 = ((Long)map.get(stringBuilder.toString())).longValue();
    } 
    map2 = this.mTempMonitorAppMap;
    stringBuilder5 = new StringBuilder();
    stringBuilder5.append(paramString1);
    stringBuilder5.append("--MaxPhoneTemp");
    if (map2.containsKey(stringBuilder5.toString())) {
      Map<String, Long> map = this.mTempMonitorAppMap;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append("--MaxPhoneTemp");
      l3 = ((Long)map.get(stringBuilder.toString())).longValue();
    } 
    Map<String, Long> map4 = this.mTempMonitorAppMap;
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(paramString1);
    stringBuilder2.append("--MaxBatTemp");
    if (map4.containsKey(stringBuilder2.toString())) {
      map4 = this.mTempMonitorAppMap;
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append(paramString1);
      stringBuilder2.append("--MaxBatTemp");
      l4 = ((Long)map4.get(stringBuilder2.toString())).longValue();
    } 
    map4 = this.mTempMonitorAppMap;
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(paramString1);
    stringBuilder2.append("--NetWifi");
    if (map4.containsKey(stringBuilder2.toString())) {
      map4 = this.mTempMonitorAppMap;
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append(paramString1);
      stringBuilder2.append("--NetWifi");
      l6 = ((Long)map4.get(stringBuilder2.toString())).longValue();
    } 
    map4 = this.mTempMonitorAppMap;
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(paramString1);
    stringBuilder2.append("--NetMobile");
    if (map4.containsKey(stringBuilder2.toString())) {
      Map<String, Long> map = this.mTempMonitorAppMap;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append("--NetMobile");
      l5 = ((Long)map.get(stringBuilder.toString())).longValue();
    } 
    Map<String, Long> map1 = this.mTempMonitorAppMap;
    StringBuilder stringBuilder4 = new StringBuilder();
    stringBuilder4.append(paramString1);
    stringBuilder4.append("--AutoBrightness");
    if (map1.containsKey(stringBuilder4.toString())) {
      Map<String, Long> map = this.mTempMonitorAppMap;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append("--AutoBrightness");
      l7 = ((Long)map.get(stringBuilder.toString())).longValue();
    } 
    if (l9 <= 0L)
      return null; 
    if (l10 * 100L / l9 > 80L) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append("--Current");
      hashMap.put(stringBuilder.toString(), "9999");
      stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append("--Charge");
      hashMap.put(stringBuilder.toString(), "True");
    } else if (l10 * 100L / l9 < 20L) {
      l10 = (l2 - l11) * 3600000L / l9;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append("--Current");
      hashMap.put(stringBuilder.toString(), Long.toString(l10));
      stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append("--Charge");
      hashMap.put(stringBuilder.toString(), "False");
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append("--Current");
      hashMap.put(stringBuilder.toString(), "9999");
      stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append("--Charge");
      hashMap.put(stringBuilder.toString(), "Unknown");
    } 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(paramString1);
    stringBuilder1.append("--Time");
    hashMap.put(stringBuilder1.toString(), Long.toString(l9 / 60000L));
    if (l5 * 100L / l9 > 80L) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString1);
      stringBuilder1.append("--NetType");
      hashMap.put(stringBuilder1.toString(), "Mobile");
    } else if (l6 * 100L / l9 > 80L) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString1);
      stringBuilder1.append("--NetType");
      hashMap.put(stringBuilder1.toString(), "Wifi");
    } else if (0L * 100L / l9 > 80L) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString1);
      stringBuilder1.append("--NetType");
      hashMap.put(stringBuilder1.toString(), "None");
    } else {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString1);
      stringBuilder1.append("--NetType");
      hashMap.put(stringBuilder1.toString(), "Unkown");
    } 
    if (l7 * 100L / l9 > 80L) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString1);
      stringBuilder1.append("--AutoBright");
      hashMap.put(stringBuilder1.toString(), "True");
    } else if (100L * l7 / l9 < 20L) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString1);
      stringBuilder1.append("--AutoBright");
      hashMap.put(stringBuilder1.toString(), "False");
    } else {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString1);
      stringBuilder1.append("--AutoBright");
      hashMap.put(stringBuilder1.toString(), "Unkown");
    } 
    l10 = l8 / l9;
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append(paramString1);
    stringBuilder1.append("--Volume");
    hashMap.put(stringBuilder1.toString(), Long.toString(l10));
    long l9 = l1 / l9;
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append(paramString1);
    stringBuilder1.append("--BackLight");
    hashMap.put(stringBuilder1.toString(), Long.toString(l9));
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append(paramString1);
    stringBuilder1.append("--MaxPhoneTemp");
    hashMap.put(stringBuilder1.toString(), Long.toString(l3));
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append(paramString1);
    stringBuilder1.append("--MaxBatTemp");
    hashMap.put(stringBuilder1.toString(), Long.toString(l4));
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append(paramString1);
    stringBuilder1.append("--VerisonName");
    hashMap.put(stringBuilder1.toString(), paramString2);
    return (Map)hashMap;
  }
  
  class ThermalReceiver extends BroadcastReceiver {
    final OplusThermalStatsHelper this$0;
    
    private ThermalReceiver() {}
    
    public void onReceive(Context param1Context, Intent param1Intent) {
      Message message;
      String str = param1Intent.getAction();
      if (str.equals("oppo.android.internal.thermalupload.ALARM_WAKEUP")) {
        if (OplusThermalStatsHelper.this.mRecordThermalHistory && OplusThermalStatsHelper.this.getThermalHistoryUsedSize() < 2048) {
          if (OplusThermalStatsHelper.this.mHandler.hasMessages(60))
            OplusThermalStatsHelper.this.mHandler.removeMessages(60); 
          OplusThermalStatsHelper.this.mHandler.sendEmptyMessageDelayed(60, 4000L);
          return;
        } 
        message = new Message();
        message.what = 56;
        OplusThermalStatsHelper.this.mHandler.sendMessageDelayed(message, 1000L);
      } else if ("android.media.VOLUME_CHANGED_ACTION".equals(message)) {
        int i = param1Intent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_TYPE", -1);
        int j = param1Intent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_VALUE", 0);
        if (i == 3) {
          if (OplusThermalStatsHelper.this.mHandler.hasMessages(61))
            OplusThermalStatsHelper.this.mHandler.removeMessages(61); 
          message = new Message();
          message.what = 61;
          message.arg1 = j;
          OplusThermalStatsHelper.this.mGlobalVolumeLevel = j;
          OplusThermalStatsHelper.this.mHandler.sendMessageDelayed(message, 2000L);
        } 
      } 
    }
  }
  
  private void addhistorySizeValue(StringBuilder paramStringBuilder, long paramLong) {
    float f1 = (float)paramLong;
    String str = "";
    float f2 = f1;
    if (f1 >= 10240.0F) {
      str = "KB";
      f2 = f1 / 1024.0F;
    } 
    f1 = f2;
    if (f2 >= 10240.0F) {
      str = "MB";
      f1 = f2 / 1024.0F;
    } 
    float f3 = f1;
    if (f1 >= 10240.0F) {
      str = "GB";
      f3 = f1 / 1024.0F;
    } 
    f2 = f3;
    if (f3 >= 10240.0F) {
      str = "TB";
      f2 = f3 / 1024.0F;
    } 
    f1 = f2;
    if (f2 >= 10240.0F) {
      str = "PB";
      f1 = f2 / 1024.0F;
    } 
    paramStringBuilder.append((int)f1);
    paramStringBuilder.append(str);
  }
  
  private int symbolInt(int paramInt) {
    int i;
    if ((paramInt & 0x8000) == 32768) {
      i = paramInt - 65535 - 1;
    } else {
      i = paramInt;
      if ((paramInt & 0x800) == 2048)
        i = paramInt - 4095 - 1; 
    } 
    return i;
  }
  
  public static byte getNetWorkClass(int paramInt) {
    switch (paramInt) {
      default:
        return 0;
      case 13:
        return 4;
      case 3:
      case 5:
      case 6:
      case 8:
      case 9:
      case 10:
      case 12:
      case 14:
      case 15:
        return 3;
      case 1:
      case 2:
      case 4:
      case 7:
      case 11:
        break;
    } 
    return 2;
  }
  
  public void notePhoneDataConnectionStateLocked(long paramLong1, long paramLong2, int paramInt) {
    if (getConnectyType() == 0) {
      byte b = getNetWorkClass(paramInt);
      if (b != this.mThermalHistoryCur.connectNetType)
        addThermalConnectType(paramLong1, paramLong2, b); 
    } 
  }
  
  public void noteScreenBrightnessModeChangedLock(boolean paramBoolean) {
    this.mGlobalScreenBrightnessMode = paramBoolean;
    if (!this.mThermalFeatureOn)
      return; 
    if (paramBoolean != this.mThermalHistoryCur.isAutoBrightness) {
      this.mThermalHistoryCur.isAutoBrightness = paramBoolean;
      if (this.mHandler.hasMessages(63))
        this.mHandler.removeMessages(63); 
      this.mHandler.sendEmptyMessageDelayed(63, 1000L);
    } 
  }
  
  public boolean startIteratingThermalHistoryLocked() {
    if (this.mThermalHistoryBuffer.dataSize() <= 0)
      return false; 
    this.mThermalHistoryBuffer.setDataPosition(0);
    this.mIteratingThermalHistory = true;
    return true;
  }
  
  public void finishIteratingThermalHistoryLocked() {
    Parcel parcel = this.mThermalHistoryBuffer;
    parcel.setDataPosition(parcel.dataSize());
    this.mIteratingThermalHistory = false;
  }
  
  public int getThermalHistoryUsedSize() {
    return this.mThermalHistoryBuffer.dataSize();
  }
  
  public boolean getNextThermalHistoryLocked(OplusBaseBatteryStats.ThermalItem paramThermalItem, long paramLong) {
    int i = this.mThermalHistoryBuffer.dataPosition();
    if (i == 0)
      paramThermalItem.clear(); 
    if (i >= this.mThermalHistoryBuffer.dataSize()) {
      i = 1;
    } else {
      i = 0;
    } 
    if (i != 0)
      return false; 
    readThermalHistoryDelta(this.mThermalHistoryBuffer, paramThermalItem, paramLong);
    return true;
  }
  
  public static class ThermalStatistics {
    private static final String APP_ID = "appId";
    
    private static final String APP_NAME = "appName";
    
    private static final String APP_PACKAGE = "appPackage";
    
    private static final String APP_VERSION = "appVersion";
    
    private static final int COMMON = 1006;
    
    private static final int COMMON_LIST = 1010;
    
    private static final String DATA_TYPE = "dataType";
    
    private static final String EVENT_ID = "eventID";
    
    private static final String LOG_MAP = "logMap";
    
    private static final String LOG_TAG = "logTag";
    
    private static final String MAP_LIST = "mapList";
    
    private static final String SSOID = "ssoid";
    
    private static final String TAG = "ThermalStatistics--";
    
    private static final String UPLOAD_NOW = "uploadNow";
    
    private static int appId = 20139;
    
    private static boolean mIsOnCommon = false;
    
    private static ExecutorService sSingleThreadExecutor = Executors.newSingleThreadExecutor();
    
    static {
    
    }
    
    public static void onCommon(final Context context, final String logTag, final String eventId, final Map<String, String> cloneMap, final boolean uploadNow) {
      mIsOnCommon = true;
      if (context == null) {
        Slog.w("common_test", "context is null!");
        mIsOnCommon = false;
        return;
      } 
      if (SystemProperties.getBoolean("persist.sys.assert.panic", false)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onCommon begin: logTag=");
        stringBuilder.append(logTag);
        stringBuilder.append(", eventId=");
        stringBuilder.append(eventId);
        stringBuilder.append(", logMap=");
        stringBuilder.append(cloneMap);
        stringBuilder.append(", uploadNow=");
        stringBuilder.append(uploadNow);
        Slog.d("common_test", stringBuilder.toString());
      } 
      if (TextUtils.isEmpty(logTag)) {
        mIsOnCommon = false;
        return;
      } 
      if (cloneMap != null) {
        cloneMap = new HashMap<>(cloneMap);
      } else {
        cloneMap = new HashMap<>();
      } 
      Slog.i("common_test", "context is startservice");
      Runnable runnable = new Runnable() {
          final Map val$cloneMap;
          
          final Context val$context;
          
          final String val$eventId;
          
          final String val$logTag;
          
          final boolean val$uploadNow;
          
          public void run() {
            try {
              Intent intent = new Intent();
              this();
              intent.setClassName("com.nearme.statistics.rom", "com.nearme.statistics.rom.service.ReceiverService");
              intent.putExtra("appPackage", "system");
              intent.putExtra("appName", "system");
              intent.putExtra("appVersion", "system");
              intent.putExtra("ssoid", "system");
              intent.putExtra("appId", OplusThermalStatsHelper.ThermalStatistics.appId);
              intent.putExtra("eventID", eventId);
              intent.putExtra("uploadNow", uploadNow);
              intent.putExtra("logTag", logTag);
              intent.putExtra("logMap", OplusThermalStatsHelper.ThermalStatistics.getCommonObject(cloneMap).toString());
              intent.putExtra("dataType", 1006);
              if (context != null)
                context.startServiceAsUser(intent, UserHandle.CURRENT); 
            } catch (Exception exception) {
              Slog.e("ThermalStatistics--", "start service failed");
            } 
            cloneMap.clear();
            OplusThermalStatsHelper.ThermalStatistics.access$2102(false);
          }
        };
      sSingleThreadExecutor.execute(runnable);
    }
    
    public static boolean getOnCommon() {
      return mIsOnCommon;
    }
    
    private static JSONObject getCommonObject(Map<String, String> param1Map) {
      JSONObject jSONObject = new JSONObject();
      if (param1Map != null && !param1Map.isEmpty())
        try {
          for (String str : param1Map.keySet())
            jSONObject.put(str, param1Map.get(str)); 
        } catch (Exception exception) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Exception: ");
          stringBuilder.append(exception);
          Slog.w("ThermalStatistics--", stringBuilder.toString());
        }  
      return jSONObject;
    }
  }
  
  class null implements Runnable {
    final Map val$cloneMap;
    
    final Context val$context;
    
    final String val$eventId;
    
    final String val$logTag;
    
    final boolean val$uploadNow;
    
    public void run() {
      try {
        Intent intent = new Intent();
        this();
        intent.setClassName("com.nearme.statistics.rom", "com.nearme.statistics.rom.service.ReceiverService");
        intent.putExtra("appPackage", "system");
        intent.putExtra("appName", "system");
        intent.putExtra("appVersion", "system");
        intent.putExtra("ssoid", "system");
        intent.putExtra("appId", OplusThermalStatsHelper.ThermalStatistics.appId);
        intent.putExtra("eventID", eventId);
        intent.putExtra("uploadNow", uploadNow);
        intent.putExtra("logTag", logTag);
        intent.putExtra("logMap", OplusThermalStatsHelper.ThermalStatistics.getCommonObject(cloneMap).toString());
        intent.putExtra("dataType", 1006);
        if (context != null)
          context.startServiceAsUser(intent, UserHandle.CURRENT); 
      } catch (Exception exception) {
        Slog.e("ThermalStatistics--", "start service failed");
      } 
      cloneMap.clear();
      OplusThermalStatsHelper.ThermalStatistics.access$2102(false);
    }
  }
  
  public void startAnalyzeBatteryStats() {
    BackgroundThread.getHandler().post(new Runnable() {
          final OplusThermalStatsHelper this$0;
          
          public void run() {
            Intent intent = new Intent("oppo.intent.action.PARSE_BATTERYSTATS_START");
            intent.setPackage("com.oppo.oppopowermonitor");
            if (OplusThermalStatsHelper.this.mContext != null)
              OplusThermalStatsHelper.this.mContext.sendBroadcastAsUser(intent, UserHandle.CURRENT); 
          }
        });
  }
  
  class WorkHandler extends Handler {
    final OplusThermalStatsHelper this$0;
    
    public WorkHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      OplusThermalStatsHelper oplusThermalStatsHelper2;
      AudioManager audioManager;
      OplusThermalStatsHelper oplusThermalStatsHelper1;
      int i = param1Message.what;
      boolean bool1 = false;
      boolean bool2 = false, bool3 = false, bool4 = false;
      boolean bool5 = false;
      switch (i) {
        default:
          return;
        case 64:
          Slog.d("OppoThermalStats", "SYNC_TO_THERMAL_FILE");
          OplusThermalStatsHelper.this.writeThermalRecFile();
          OplusThermalStatsHelper.this.clearHistoryBuffer();
        case 63:
          oplusThermalStatsHelper2 = OplusThermalStatsHelper.this;
          oplusThermalStatsHelper2.addThermalHistoryBufferLocked((byte)26, oplusThermalStatsHelper2.mThermalHistoryCur, true);
        case 62:
          if (OplusThermalStatsHelper.this.mContext != null) {
            audioManager = (AudioManager)OplusThermalStatsHelper.this.mContext.getSystemService("audio");
            if (audioManager != null) {
              OplusThermalStatsHelper.this.mThermalHistoryCur.volume = audioManager.getStreamVolume(3);
              OplusThermalStatsHelper.this.mThermalHistoryCur.isAutoBrightness = OplusThermalStatsHelper.this.mGlobalScreenBrightnessMode;
            } else {
              Slog.w("OppoThermalStats", "INIT_THERMAL_PAR: failed to get audioManager!!");
            } 
          } 
        case 61:
          i = ((Message)audioManager).arg1;
          if (i != OplusThermalStatsHelper.this.mThermalHistoryCur.volume) {
            OplusThermalStatsHelper.this.mThermalHistoryCur.volume = i;
            if (OplusThermalStatsHelper.this.mHandler.hasMessages(63))
              OplusThermalStatsHelper.this.mHandler.removeMessages(63); 
            OplusThermalStatsHelper.this.mHandler.sendEmptyMessageDelayed(63, 1000L);
          } 
        case 60:
          OplusThermalStatsHelper.this.initUploadAlarm();
        case 59:
          try {
            if (OplusThermalStatsHelper.this.mWakeLock != null && !OplusThermalStatsHelper.this.mWakeLock.isHeld())
              OplusThermalStatsHelper.this.mWakeLock.acquire(60000L); 
            long l = ((Long)((Message)audioManager).obj).longValue();
            i = ((Message)audioManager).arg1;
            if (OplusThermalStatsHelper.this.mHeatReasonDetails.analizyHeatRecItem(l, i)) {
              Message message = new Message();
              this();
              message.what = 59;
              message.obj = Long.valueOf(l);
              message.arg1 = i - 1;
              OplusThermalStatsHelper.this.mHandler.sendMessageDelayed(message, 1L);
            } else {
              OplusThermalStatsHelper.this.mHeatReasonDetails.getHeatReasonDetails();
              OplusThermalStatsHelper.this.mHeatReasonDetails.mAnalizyPosition = 0;
              OplusThermalStatsHelper.access$2302(OplusThermalStatsHelper.this, false);
            } 
          } catch (Exception exception) {
            exception.printStackTrace();
          } 
        case 58:
          i = ((Message)exception).arg1;
          if (i != OplusThermalStatsHelper.this.mThermalHistoryCur.connectNetType) {
            OplusThermalStatsHelper.this.mThermalHistoryCur.connectNetType = (byte)i;
            oplusThermalStatsHelper1 = OplusThermalStatsHelper.this;
            oplusThermalStatsHelper1.addThermalHistoryBufferLocked((byte)9, oplusThermalStatsHelper1.mThermalHistoryCur, true);
          } 
        case 57:
          i = ((Message)oplusThermalStatsHelper1).arg1;
          if (i != OplusThermalStatsHelper.this.mThermalHistoryCur.backlight) {
            oplusThermalStatsHelper1 = OplusThermalStatsHelper.this;
            oplusThermalStatsHelper1.addThermalHistoryBufferLocked((byte)3, i, oplusThermalStatsHelper1.mThermalHistoryCur, true);
          } 
        case 56:
          if (!OplusThermalStatsHelper.this.mWakeLock.isHeld())
            OplusThermalStatsHelper.this.mWakeLock.acquire(25000L); 
          OplusThermalStatsHelper.this.writeThermalRecFile();
          OplusThermalStatsHelper.this.startUploadTemp();
          OplusThermalStatsHelper.this.startAnalyzeBatteryStats();
        case 55:
          bool1 = bool5;
          if (((Message)oplusThermalStatsHelper1).arg1 == 1)
            bool1 = true; 
          if (bool1 != OplusThermalStatsHelper.this.mThermalHistoryCur.flashlightOn) {
            OplusThermalStatsHelper.this.mThermalHistoryCur.flashlightOn = bool1;
            if (OplusThermalStatsHelper.this.mHandler.hasMessages(63))
              OplusThermalStatsHelper.this.mHandler.removeMessages(63); 
            OplusThermalStatsHelper.this.mHandler.sendEmptyMessageDelayed(63, 1000L);
          } 
        case 54:
          if (((Message)oplusThermalStatsHelper1).arg1 == 1)
            bool1 = true; 
          if (bool1 != OplusThermalStatsHelper.this.mThermalHistoryCur.gpsOn) {
            OplusThermalStatsHelper.this.mThermalHistoryCur.gpsOn = bool1;
            if (OplusThermalStatsHelper.this.mHandler.hasMessages(63))
              OplusThermalStatsHelper.this.mHandler.removeMessages(63); 
            OplusThermalStatsHelper.this.mHandler.sendEmptyMessageDelayed(63, 1000L);
          } 
        case 53:
          bool1 = bool2;
          if (((Message)oplusThermalStatsHelper1).arg1 == 1)
            bool1 = true; 
          if (bool1 != OplusThermalStatsHelper.this.mThermalHistoryCur.videoOn) {
            OplusThermalStatsHelper.this.mThermalHistoryCur.videoOn = bool1;
            if (OplusThermalStatsHelper.this.mHandler.hasMessages(63))
              OplusThermalStatsHelper.this.mHandler.removeMessages(63); 
            OplusThermalStatsHelper.this.mHandler.sendEmptyMessageDelayed(63, 1000L);
          } 
        case 52:
          bool1 = bool3;
          if (((Message)oplusThermalStatsHelper1).arg1 == 1)
            bool1 = true; 
          if (bool1 != OplusThermalStatsHelper.this.mThermalHistoryCur.audioOn) {
            OplusThermalStatsHelper.this.mThermalHistoryCur.audioOn = bool1;
            if (OplusThermalStatsHelper.this.mHandler.hasMessages(63))
              OplusThermalStatsHelper.this.mHandler.removeMessages(63); 
            OplusThermalStatsHelper.this.mHandler.sendEmptyMessageDelayed(63, 1000L);
          } 
        case 51:
          break;
      } 
      bool1 = bool4;
      if (((Message)oplusThermalStatsHelper1).arg1 == 1)
        bool1 = true; 
      if (bool1 != OplusThermalStatsHelper.this.mThermalHistoryCur.cameraOn) {
        OplusThermalStatsHelper.this.mThermalHistoryCur.cameraOn = bool1;
        if (OplusThermalStatsHelper.this.mHandler.hasMessages(63))
          OplusThermalStatsHelper.this.mHandler.removeMessages(63); 
        OplusThermalStatsHelper.this.mHandler.sendEmptyMessageDelayed(63, 1000L);
      } 
    }
  }
}
