package com.android.internal.os;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.util.SparseArray;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ThreadLocalRandom;

public class LooperStats implements Looper.Observer {
  private final SparseArray<Entry> mEntries = new SparseArray<>(512);
  
  private final Object mLock = new Object();
  
  private final Entry mOverflowEntry = new Entry("OVERFLOW");
  
  private final Entry mHashCollisionEntry = new Entry("HASH_COLLISION");
  
  private final ConcurrentLinkedQueue<DispatchSession> mSessionPool = new ConcurrentLinkedQueue<>();
  
  private long mStartCurrentTime = System.currentTimeMillis();
  
  private long mStartElapsedTime = SystemClock.elapsedRealtime();
  
  private boolean mAddDebugEntries = false;
  
  private boolean mTrackScreenInteractive = false;
  
  public static final String DEBUG_ENTRY_PREFIX = "__DEBUG_";
  
  private static final boolean DISABLED_SCREEN_STATE_TRACKING_VALUE = false;
  
  private static final int SESSION_POOL_SIZE = 50;
  
  private CachedDeviceState.TimeInStateStopwatch mBatteryStopwatch;
  
  private CachedDeviceState.Readonly mDeviceState;
  
  private final int mEntriesSizeCap;
  
  private int mSamplingInterval;
  
  public LooperStats(int paramInt1, int paramInt2) {
    this.mSamplingInterval = paramInt1;
    this.mEntriesSizeCap = paramInt2;
  }
  
  public void setDeviceState(CachedDeviceState.Readonly paramReadonly) {
    CachedDeviceState.TimeInStateStopwatch timeInStateStopwatch = this.mBatteryStopwatch;
    if (timeInStateStopwatch != null)
      timeInStateStopwatch.close(); 
    this.mDeviceState = paramReadonly;
    this.mBatteryStopwatch = paramReadonly.createTimeOnBatteryStopwatch();
  }
  
  public void setAddDebugEntries(boolean paramBoolean) {
    this.mAddDebugEntries = paramBoolean;
  }
  
  public Object messageDispatchStarting() {
    if (deviceStateAllowsCollection() && shouldCollectDetailedData()) {
      DispatchSession dispatchSession = this.mSessionPool.poll();
      if (dispatchSession == null)
        dispatchSession = new DispatchSession(); 
      dispatchSession.startTimeMicro = getElapsedRealtimeMicro();
      dispatchSession.cpuStartMicro = getThreadTimeMicro();
      dispatchSession.systemUptimeMillis = getSystemUptimeMillis();
      return dispatchSession;
    } 
    return DispatchSession.NOT_SAMPLED;
  }
  
  public void messageDispatched(Object paramObject, Message paramMessage) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial deviceStateAllowsCollection : ()Z
    //   4: ifne -> 8
    //   7: return
    //   8: aload_1
    //   9: checkcast com/android/internal/os/LooperStats$DispatchSession
    //   12: astore_3
    //   13: aload_3
    //   14: getstatic com/android/internal/os/LooperStats$DispatchSession.NOT_SAMPLED : Lcom/android/internal/os/LooperStats$DispatchSession;
    //   17: if_acmpeq -> 26
    //   20: iconst_1
    //   21: istore #4
    //   23: goto -> 29
    //   26: iconst_0
    //   27: istore #4
    //   29: aload_0
    //   30: aload_2
    //   31: iload #4
    //   33: invokespecial findEntry : (Landroid/os/Message;Z)Lcom/android/internal/os/LooperStats$Entry;
    //   36: astore_1
    //   37: aload_1
    //   38: ifnull -> 208
    //   41: aload_1
    //   42: monitorenter
    //   43: aload_1
    //   44: aload_1
    //   45: getfield messageCount : J
    //   48: lconst_1
    //   49: ladd
    //   50: putfield messageCount : J
    //   53: aload_3
    //   54: getstatic com/android/internal/os/LooperStats$DispatchSession.NOT_SAMPLED : Lcom/android/internal/os/LooperStats$DispatchSession;
    //   57: if_acmpeq -> 198
    //   60: aload_1
    //   61: aload_1
    //   62: getfield recordedMessageCount : J
    //   65: lconst_1
    //   66: ladd
    //   67: putfield recordedMessageCount : J
    //   70: aload_0
    //   71: invokevirtual getElapsedRealtimeMicro : ()J
    //   74: aload_3
    //   75: getfield startTimeMicro : J
    //   78: lsub
    //   79: lstore #5
    //   81: aload_0
    //   82: invokevirtual getThreadTimeMicro : ()J
    //   85: aload_3
    //   86: getfield cpuStartMicro : J
    //   89: lsub
    //   90: lstore #7
    //   92: aload_1
    //   93: aload_1
    //   94: getfield totalLatencyMicro : J
    //   97: lload #5
    //   99: ladd
    //   100: putfield totalLatencyMicro : J
    //   103: aload_1
    //   104: aload_1
    //   105: getfield maxLatencyMicro : J
    //   108: lload #5
    //   110: invokestatic max : (JJ)J
    //   113: putfield maxLatencyMicro : J
    //   116: aload_1
    //   117: aload_1
    //   118: getfield cpuUsageMicro : J
    //   121: lload #7
    //   123: ladd
    //   124: putfield cpuUsageMicro : J
    //   127: aload_1
    //   128: aload_1
    //   129: getfield maxCpuUsageMicro : J
    //   132: lload #7
    //   134: invokestatic max : (JJ)J
    //   137: putfield maxCpuUsageMicro : J
    //   140: aload_2
    //   141: invokevirtual getWhen : ()J
    //   144: lconst_0
    //   145: lcmp
    //   146: ifle -> 198
    //   149: lconst_0
    //   150: aload_3
    //   151: getfield systemUptimeMillis : J
    //   154: aload_2
    //   155: invokevirtual getWhen : ()J
    //   158: lsub
    //   159: invokestatic max : (JJ)J
    //   162: lstore #7
    //   164: aload_1
    //   165: aload_1
    //   166: getfield delayMillis : J
    //   169: lload #7
    //   171: ladd
    //   172: putfield delayMillis : J
    //   175: aload_1
    //   176: aload_1
    //   177: getfield maxDelayMillis : J
    //   180: lload #7
    //   182: invokestatic max : (JJ)J
    //   185: putfield maxDelayMillis : J
    //   188: aload_1
    //   189: aload_1
    //   190: getfield recordedDelayMessageCount : J
    //   193: lconst_1
    //   194: ladd
    //   195: putfield recordedDelayMessageCount : J
    //   198: aload_1
    //   199: monitorexit
    //   200: goto -> 208
    //   203: astore_2
    //   204: aload_1
    //   205: monitorexit
    //   206: aload_2
    //   207: athrow
    //   208: aload_0
    //   209: aload_3
    //   210: invokespecial recycleSession : (Lcom/android/internal/os/LooperStats$DispatchSession;)V
    //   213: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #94	-> 0
    //   #95	-> 7
    //   #98	-> 8
    //   #99	-> 13
    //   #100	-> 37
    //   #101	-> 41
    //   #102	-> 43
    //   #103	-> 53
    //   #104	-> 60
    //   #105	-> 70
    //   #106	-> 81
    //   #107	-> 92
    //   #108	-> 103
    //   #109	-> 116
    //   #110	-> 127
    //   #111	-> 140
    //   #112	-> 149
    //   #113	-> 164
    //   #114	-> 175
    //   #115	-> 188
    //   #118	-> 198
    //   #121	-> 208
    //   #122	-> 213
    // Exception table:
    //   from	to	target	type
    //   43	53	203	finally
    //   53	60	203	finally
    //   60	70	203	finally
    //   70	81	203	finally
    //   81	92	203	finally
    //   92	103	203	finally
    //   103	116	203	finally
    //   116	127	203	finally
    //   127	140	203	finally
    //   140	149	203	finally
    //   149	164	203	finally
    //   164	175	203	finally
    //   175	188	203	finally
    //   188	198	203	finally
    //   198	200	203	finally
    //   204	206	203	finally
  }
  
  public void dispatchingThrewException(Object paramObject, Message paramMessage, Exception paramException) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial deviceStateAllowsCollection : ()Z
    //   4: ifne -> 8
    //   7: return
    //   8: aload_1
    //   9: checkcast com/android/internal/os/LooperStats$DispatchSession
    //   12: astore_1
    //   13: aload_1
    //   14: getstatic com/android/internal/os/LooperStats$DispatchSession.NOT_SAMPLED : Lcom/android/internal/os/LooperStats$DispatchSession;
    //   17: if_acmpeq -> 26
    //   20: iconst_1
    //   21: istore #4
    //   23: goto -> 29
    //   26: iconst_0
    //   27: istore #4
    //   29: aload_0
    //   30: aload_2
    //   31: iload #4
    //   33: invokespecial findEntry : (Landroid/os/Message;Z)Lcom/android/internal/os/LooperStats$Entry;
    //   36: astore_2
    //   37: aload_2
    //   38: ifnull -> 63
    //   41: aload_2
    //   42: monitorenter
    //   43: aload_2
    //   44: aload_2
    //   45: getfield exceptionCount : J
    //   48: lconst_1
    //   49: ladd
    //   50: putfield exceptionCount : J
    //   53: aload_2
    //   54: monitorexit
    //   55: goto -> 63
    //   58: astore_1
    //   59: aload_2
    //   60: monitorexit
    //   61: aload_1
    //   62: athrow
    //   63: aload_0
    //   64: aload_1
    //   65: invokespecial recycleSession : (Lcom/android/internal/os/LooperStats$DispatchSession;)V
    //   68: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #126	-> 0
    //   #127	-> 7
    //   #130	-> 8
    //   #131	-> 13
    //   #132	-> 37
    //   #133	-> 41
    //   #134	-> 43
    //   #135	-> 53
    //   #138	-> 63
    //   #139	-> 68
    // Exception table:
    //   from	to	target	type
    //   43	53	58	finally
    //   53	55	58	finally
    //   59	61	58	finally
  }
  
  private boolean deviceStateAllowsCollection() {
    boolean bool;
    CachedDeviceState.Readonly readonly = this.mDeviceState;
    if (readonly != null && !readonly.isCharging()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public List<ExportedEntry> getEntries() {
    synchronized (this.mLock) {
      int i = this.mEntries.size();
      ArrayList<ExportedEntry> arrayList = new ArrayList();
      this(i);
      for (byte b = 0; b < i;) {
        synchronized ((Entry)this.mEntries.valueAt(b)) {
          ExportedEntry exportedEntry = new ExportedEntry();
          this(null);
          arrayList.add(exportedEntry);
          b++;
        } 
      } 
      maybeAddSpecialEntry(arrayList, this.mOverflowEntry);
      maybeAddSpecialEntry(arrayList, this.mHashCollisionEntry);
      if (this.mAddDebugEntries && this.mBatteryStopwatch != null) {
        arrayList.add(createDebugEntry("start_time_millis", this.mStartElapsedTime));
        arrayList.add(createDebugEntry("end_time_millis", SystemClock.elapsedRealtime()));
        null = this.mBatteryStopwatch;
        null = createDebugEntry("battery_time_millis", null.getMillis());
        arrayList.add(null);
        arrayList.add(createDebugEntry("sampling_interval", this.mSamplingInterval));
      } 
      return arrayList;
    } 
  }
  
  private ExportedEntry createDebugEntry(String paramString, long paramLong) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("__DEBUG_");
    stringBuilder.append(paramString);
    Entry entry = new Entry(stringBuilder.toString());
    entry.messageCount = 1L;
    entry.recordedMessageCount = 1L;
    entry.totalLatencyMicro = paramLong;
    return new ExportedEntry(entry);
  }
  
  public long getStartTimeMillis() {
    return this.mStartCurrentTime;
  }
  
  public long getStartElapsedTimeMillis() {
    return this.mStartElapsedTime;
  }
  
  public long getBatteryTimeMillis() {
    long l;
    CachedDeviceState.TimeInStateStopwatch timeInStateStopwatch = this.mBatteryStopwatch;
    if (timeInStateStopwatch != null) {
      l = timeInStateStopwatch.getMillis();
    } else {
      l = 0L;
    } 
    return l;
  }
  
  private void maybeAddSpecialEntry(List<ExportedEntry> paramList, Entry paramEntry) {
    // Byte code:
    //   0: aload_2
    //   1: monitorenter
    //   2: aload_2
    //   3: getfield messageCount : J
    //   6: lconst_0
    //   7: lcmp
    //   8: ifgt -> 20
    //   11: aload_2
    //   12: getfield exceptionCount : J
    //   15: lconst_0
    //   16: lcmp
    //   17: ifle -> 37
    //   20: new com/android/internal/os/LooperStats$ExportedEntry
    //   23: astore_3
    //   24: aload_3
    //   25: aload_2
    //   26: invokespecial <init> : (Lcom/android/internal/os/LooperStats$Entry;)V
    //   29: aload_1
    //   30: aload_3
    //   31: invokeinterface add : (Ljava/lang/Object;)Z
    //   36: pop
    //   37: aload_2
    //   38: monitorexit
    //   39: return
    //   40: astore_1
    //   41: aload_2
    //   42: monitorexit
    //   43: aload_1
    //   44: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #195	-> 0
    //   #196	-> 2
    //   #197	-> 20
    //   #199	-> 37
    //   #200	-> 39
    //   #199	-> 40
    // Exception table:
    //   from	to	target	type
    //   2	20	40	finally
    //   20	37	40	finally
    //   37	39	40	finally
    //   41	43	40	finally
  }
  
  public void reset() {
    synchronized (this.mLock) {
      this.mEntries.clear();
      synchronized (this.mHashCollisionEntry) {
        this.mHashCollisionEntry.reset();
        synchronized (this.mOverflowEntry) {
          this.mOverflowEntry.reset();
          this.mStartCurrentTime = System.currentTimeMillis();
          this.mStartElapsedTime = SystemClock.elapsedRealtime();
          null = this.mBatteryStopwatch;
          if (null != null)
            null.reset(); 
          return;
        } 
      } 
    } 
  }
  
  public void setSamplingInterval(int paramInt) {
    this.mSamplingInterval = paramInt;
  }
  
  public void setTrackScreenInteractive(boolean paramBoolean) {
    this.mTrackScreenInteractive = paramBoolean;
  }
  
  private Entry findEntry(Message paramMessage, boolean paramBoolean) {
    boolean bool;
    if (this.mTrackScreenInteractive) {
      bool = this.mDeviceState.isScreenInteractive();
    } else {
      bool = false;
    } 
    int i = Entry.idFor(paramMessage, bool);
    synchronized (this.mLock) {
      Entry entry1, entry2 = this.mEntries.get(i);
      Entry entry3 = entry2;
      if (entry2 == null) {
        if (!paramBoolean)
          return null; 
        if (this.mEntries.size() >= this.mEntriesSizeCap) {
          entry1 = this.mOverflowEntry;
          return entry1;
        } 
        entry3 = new Entry();
        this((Message)entry1, bool);
        this.mEntries.put(i, entry3);
      } 
      if (entry3.workSourceUid == ((Message)entry1).workSourceUid) {
        Handler handler = entry3.handler;
        if (handler.getClass() == entry1.getTarget().getClass()) {
          handler = entry3.handler;
          if (handler.getLooper().getThread() == entry1.getTarget().getLooper().getThread() && entry3.isInteractive == bool)
            return entry3; 
        } 
      } 
      return this.mHashCollisionEntry;
    } 
  }
  
  private void recycleSession(DispatchSession paramDispatchSession) {
    if (paramDispatchSession != DispatchSession.NOT_SAMPLED && this.mSessionPool.size() < 50)
      this.mSessionPool.add(paramDispatchSession); 
  }
  
  protected long getThreadTimeMicro() {
    return SystemClock.currentThreadTimeMicro();
  }
  
  protected long getElapsedRealtimeMicro() {
    return SystemClock.elapsedRealtimeNanos() / 1000L;
  }
  
  protected long getSystemUptimeMillis() {
    return SystemClock.uptimeMillis();
  }
  
  protected boolean shouldCollectDetailedData() {
    boolean bool;
    if (ThreadLocalRandom.current().nextInt() % this.mSamplingInterval == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  class DispatchSession {
    private DispatchSession() {}
    
    static final DispatchSession NOT_SAMPLED = new DispatchSession();
    
    public long cpuStartMicro;
    
    public long startTimeMicro;
    
    public long systemUptimeMillis;
  }
  
  class Entry {
    public long cpuUsageMicro;
    
    public long delayMillis;
    
    public long exceptionCount;
    
    public final Handler handler;
    
    public final boolean isInteractive;
    
    public long maxCpuUsageMicro;
    
    public long maxDelayMillis;
    
    public long maxLatencyMicro;
    
    public long messageCount;
    
    public final String messageName;
    
    public long recordedDelayMessageCount;
    
    public long recordedMessageCount;
    
    public long totalLatencyMicro;
    
    public final int workSourceUid;
    
    Entry(LooperStats this$0, boolean param1Boolean) {
      this.workSourceUid = ((Message)this$0).workSourceUid;
      Handler handler = this$0.getTarget();
      this.messageName = handler.getMessageName((Message)this$0);
      this.isInteractive = param1Boolean;
    }
    
    Entry(LooperStats this$0) {
      this.workSourceUid = -1;
      this.messageName = (String)this$0;
      this.handler = null;
      this.isInteractive = false;
    }
    
    void reset() {
      this.messageCount = 0L;
      this.recordedMessageCount = 0L;
      this.exceptionCount = 0L;
      this.totalLatencyMicro = 0L;
      this.maxLatencyMicro = 0L;
      this.cpuUsageMicro = 0L;
      this.maxCpuUsageMicro = 0L;
      this.delayMillis = 0L;
      this.maxDelayMillis = 0L;
      this.recordedDelayMessageCount = 0L;
    }
    
    static int idFor(Message param1Message, boolean param1Boolean) {
      int i = param1Message.workSourceUid;
      int j = param1Message.getTarget().getLooper().getThread().hashCode();
      int k = param1Message.getTarget().getClass().hashCode();
      if (param1Boolean) {
        m = 1231;
      } else {
        m = 1237;
      } 
      int m = (((7 * 31 + i) * 31 + j) * 31 + k) * 31 + m;
      if (param1Message.getCallback() != null)
        return m * 31 + param1Message.getCallback().getClass().hashCode(); 
      return m * 31 + param1Message.what;
    }
  }
  
  class ExportedEntry {
    public final long cpuUsageMicros;
    
    public final long delayMillis;
    
    public final long exceptionCount;
    
    public final String handlerClassName;
    
    public final boolean isInteractive;
    
    public final long maxCpuUsageMicros;
    
    public final long maxDelayMillis;
    
    public final long maxLatencyMicros;
    
    public final long messageCount;
    
    public final String messageName;
    
    public final long recordedDelayMessageCount;
    
    public final long recordedMessageCount;
    
    public final String threadName;
    
    public final long totalLatencyMicros;
    
    public final int workSourceUid;
    
    ExportedEntry(LooperStats this$0) {
      this.workSourceUid = ((LooperStats.Entry)this$0).workSourceUid;
      if (((LooperStats.Entry)this$0).handler != null) {
        this.handlerClassName = ((LooperStats.Entry)this$0).handler.getClass().getName();
        this.threadName = ((LooperStats.Entry)this$0).handler.getLooper().getThread().getName();
      } else {
        this.handlerClassName = "";
        this.threadName = "";
      } 
      this.isInteractive = ((LooperStats.Entry)this$0).isInteractive;
      this.messageName = ((LooperStats.Entry)this$0).messageName;
      this.messageCount = ((LooperStats.Entry)this$0).messageCount;
      this.recordedMessageCount = ((LooperStats.Entry)this$0).recordedMessageCount;
      this.exceptionCount = ((LooperStats.Entry)this$0).exceptionCount;
      this.totalLatencyMicros = ((LooperStats.Entry)this$0).totalLatencyMicro;
      this.maxLatencyMicros = ((LooperStats.Entry)this$0).maxLatencyMicro;
      this.cpuUsageMicros = ((LooperStats.Entry)this$0).cpuUsageMicro;
      this.maxCpuUsageMicros = ((LooperStats.Entry)this$0).maxCpuUsageMicro;
      this.delayMillis = ((LooperStats.Entry)this$0).delayMillis;
      this.maxDelayMillis = ((LooperStats.Entry)this$0).maxDelayMillis;
      this.recordedDelayMessageCount = ((LooperStats.Entry)this$0).recordedDelayMessageCount;
    }
  }
}
