package com.android.internal.os;

import android.util.Slog;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

class OppoCpuFreqReader {
  private String[] onLinePath;
  
  private String[] maxPath;
  
  private long[] isolateStatus;
  
  private String[] isolatePath;
  
  private boolean initialized = false;
  
  private String[] curPath;
  
  private int cpusPresent;
  
  private long[] cpufreqMax;
  
  private long[] cpuOnLineStatus;
  
  private long[] cpuCurrentFreq;
  
  private boolean DEBUG = false;
  
  private static final String TAG = "OppoCpuFreqReader";
  
  private void init() {
    try {
      FileReader fileReader = new FileReader();
      this("/sys/devices/system/cpu/present");
      try {
        BufferedReader bufferedReader = new BufferedReader();
        this(fileReader);
        Scanner scanner2 = new Scanner();
        this(bufferedReader);
        Scanner scanner1 = scanner2.useDelimiter("[-\n]");
        scanner1.nextInt();
        this.cpusPresent = scanner1.nextInt() + 1;
        scanner1.close();
        fileReader.close();
      } catch (Exception exception) {
        Slog.e("OppoCpuFreqReader", "Cannot do CPU stats due to /sys/devices/system/cpu/present parsing problem");
        fileReader.close();
      } finally {
        Exception exception;
      } 
    } catch (FileNotFoundException fileNotFoundException) {
      Slog.e("OppoCpuFreqReader", "Cannot do CPU stats since /sys/devices/system/cpu/present is missing");
    } catch (IOException iOException) {
      Slog.e("OppoCpuFreqReader", "Error closing file");
    } 
    int i = this.cpusPresent;
    this.cpufreqMax = new long[i];
    this.cpuCurrentFreq = new long[i];
    this.cpuOnLineStatus = new long[i];
    this.isolateStatus = new long[i];
    this.maxPath = new String[i];
    this.curPath = new String[i];
    this.onLinePath = new String[i];
    this.isolatePath = new String[i];
    for (i = 0; i < this.cpusPresent; i++) {
      this.cpufreqMax[i] = 0L;
      this.cpuCurrentFreq[i] = 0L;
      this.cpuOnLineStatus[i] = 0L;
      this.isolateStatus[i] = 0L;
      String[] arrayOfString3 = this.maxPath;
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("/sys/devices/system/cpu/cpu");
      stringBuilder2.append(i);
      stringBuilder2.append("/cpufreq/cpuinfo_max_freq");
      arrayOfString3[i] = stringBuilder2.toString();
      String[] arrayOfString1 = this.curPath;
      StringBuilder stringBuilder3 = new StringBuilder();
      stringBuilder3.append("/sys/devices/system/cpu/cpu");
      stringBuilder3.append(i);
      stringBuilder3.append("/cpufreq/scaling_cur_freq");
      arrayOfString1[i] = stringBuilder3.toString();
      arrayOfString1 = this.onLinePath;
      stringBuilder3 = new StringBuilder();
      stringBuilder3.append("/sys/devices/system/cpu/cpu");
      stringBuilder3.append(i);
      stringBuilder3.append("/online");
      arrayOfString1[i] = stringBuilder3.toString();
      String[] arrayOfString2 = this.isolatePath;
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("/sys/devices/system/cpu/cpu");
      stringBuilder1.append(i);
      stringBuilder1.append("/isolate");
      arrayOfString2[i] = stringBuilder1.toString();
    } 
    this.initialized = true;
  }
  
  private void readCpuFreqValues() {
    if (!this.initialized)
      init(); 
    for (byte b = 0; b < this.cpusPresent; b++) {
      long l1 = readFreqFromFile(this.maxPath[b]);
      this.cpufreqMax[b] = l1;
      long l2 = readFreqFromFile(this.curPath[b]);
      this.cpuCurrentFreq[b] = l2;
      long l3 = readFreqFromFile(this.onLinePath[b]);
      this.cpuOnLineStatus[b] = l3;
      long l4 = readFreqFromFile(this.isolatePath[b]);
      this.isolateStatus[b] = l4;
      if (this.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("cpufreqMax:");
        stringBuilder.append(l1);
        Slog.d("OppoCpuFreqReader", stringBuilder.toString());
        stringBuilder = new StringBuilder();
        stringBuilder.append("cpufreqCur:");
        stringBuilder.append(l2);
        Slog.d("OppoCpuFreqReader", stringBuilder.toString());
        stringBuilder = new StringBuilder();
        stringBuilder.append("cpuOnLine:");
        stringBuilder.append(l3);
        Slog.d("OppoCpuFreqReader", stringBuilder.toString());
        stringBuilder = new StringBuilder();
        stringBuilder.append("isolateTemp:");
        stringBuilder.append(l4);
        Slog.d("OppoCpuFreqReader", stringBuilder.toString());
      } 
    } 
  }
  
  private long readFreqFromFile(String paramString) {
    long l1 = 0L;
    long l2 = l1, l3 = l1;
    try {
      FileReader fileReader = new FileReader();
      l2 = l1;
      l3 = l1;
      this(paramString);
      long l4 = l1, l5 = l1;
      try {
        BufferedReader bufferedReader = new BufferedReader();
        l4 = l1;
        l5 = l1;
        this(fileReader);
        l4 = l1;
        l5 = l1;
        Scanner scanner = new Scanner();
        l4 = l1;
        l5 = l1;
        this(bufferedReader);
        l4 = l1;
        l5 = l1;
        l1 = scanner.nextLong();
        l4 = l1;
        l5 = l1;
      } catch (Exception exception) {
      
      } finally {
        l2 = l4;
        l3 = l4;
        fileReader.close();
        l2 = l4;
        l3 = l4;
      } 
      l3 = l5;
    } catch (FileNotFoundException fileNotFoundException) {
    
    } catch (IOException iOException) {
      Slog.e("OppoCpuFreqReader", "Error closing file");
      l3 = l2;
    } 
    return l3;
  }
  
  public long[] getCpuMaxFreq() {
    readCpuFreqValues();
    return this.cpufreqMax;
  }
  
  public long[] getCpuCurrentFreq() {
    readCpuFreqValues();
    return this.cpuCurrentFreq;
  }
  
  public long[] getCpuOnLineStatus() {
    readCpuFreqValues();
    return this.cpuOnLineStatus;
  }
  
  public long[] getIsolateStatus() {
    readCpuFreqValues();
    return this.isolateStatus;
  }
  
  public String getSimpleCpuFreqInfor() {
    readCpuFreqValues();
    StringBuilder stringBuilder = new StringBuilder();
    if (this.cpuOnLineStatus != null && this.cpufreqMax != null && this.cpuCurrentFreq != null) {
      stringBuilder.append("[");
      int i = this.cpuOnLineStatus.length;
      for (byte b = 0; b < i; b++) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("");
        stringBuilder1.append(this.cpuOnLineStatus[b]);
        stringBuilder1.append("/");
        stringBuilder1.append(this.isolateStatus[b]);
        stringBuilder1.append("/");
        stringBuilder1.append(this.cpufreqMax[b]);
        stringBuilder1.append("/");
        stringBuilder1.append(this.cpuCurrentFreq[b]);
        stringBuilder.append(stringBuilder1.toString());
        if (b != i - 1)
          stringBuilder.append(", "); 
      } 
      stringBuilder.append("]");
    } 
    return stringBuilder.toString();
  }
}
