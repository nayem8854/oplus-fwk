package com.android.internal.os;

import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Slog;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class StoragedUidIoStatsReader {
  private static final String TAG = StoragedUidIoStatsReader.class.getSimpleName();
  
  private static String sUidIoFile = "/proc/uid_io/stats";
  
  public StoragedUidIoStatsReader() {}
  
  public StoragedUidIoStatsReader(String paramString) {
    sUidIoFile = paramString;
  }
  
  public void readAbsolute(Callback paramCallback) {
    int i = StrictMode.allowThreadDiskReadsMask();
    File file = new File(sUidIoFile);
    try {
      BufferedReader bufferedReader = Files.newBufferedReader(file.toPath());
      try {
        while (true) {
          String str = bufferedReader.readLine();
          if (str != null) {
            String str1, arrayOfString[] = TextUtils.split(str, " ");
            if (arrayOfString.length != 11) {
              str1 = TAG;
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Malformed entry in ");
              stringBuilder.append(sUidIoFile);
              stringBuilder.append(": ");
              stringBuilder.append(str);
              Slog.e(str1, stringBuilder.toString());
              continue;
            } 
            str = str1[0];
            try {
              int j = Integer.parseInt(str1[0], 10);
              long l1 = Long.parseLong(str1[1], 10);
              long l2 = Long.parseLong(str1[2], 10);
              long l3 = Long.parseLong(str1[3], 10);
              long l4 = Long.parseLong(str1[4], 10);
              long l5 = Long.parseLong(str1[5], 10);
              long l6 = Long.parseLong(str1[6], 10);
              long l7 = Long.parseLong(str1[7], 10);
              long l8 = Long.parseLong(str1[8], 10);
              long l9 = Long.parseLong(str1[9], 10);
              long l10 = Long.parseLong(str1[10], 10);
              paramCallback.onUidStorageStats(j, l1, l2, l3, l4, l5, l6, l7, l8, l9, l10);
            } catch (NumberFormatException numberFormatException) {
              str1 = TAG;
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Could not parse entry in ");
              stringBuilder.append(sUidIoFile);
              stringBuilder.append(": ");
              stringBuilder.append(numberFormatException.getMessage());
              Slog.e(str1, stringBuilder.toString());
            } 
            continue;
          } 
          break;
        } 
        if (bufferedReader != null)
          bufferedReader.close(); 
      } finally {
        if (bufferedReader != null)
          try {
            bufferedReader.close();
          } finally {
            bufferedReader = null;
          }  
      } 
    } catch (IOException iOException) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Failed to read ");
      stringBuilder.append(sUidIoFile);
      stringBuilder.append(": ");
      stringBuilder.append(iOException.getMessage());
      Slog.e(str, stringBuilder.toString());
      StrictMode.setThreadPolicyMask(i);
    } finally {}
  }
  
  public static interface Callback {
    void onUidStorageStats(int param1Int, long param1Long1, long param1Long2, long param1Long3, long param1Long4, long param1Long5, long param1Long6, long param1Long7, long param1Long8, long param1Long9, long param1Long10);
  }
}
