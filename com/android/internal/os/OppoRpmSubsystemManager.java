package com.android.internal.os;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.util.Slog;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

class OppoRpmSubsystemManager {
  public static final class RpmSubsysDescription {
    public final int mask;
    
    public final String name;
    
    public RpmSubsysDescription(int param1Int, String param1String) {
      this.mask = param1Int;
      this.name = param1String;
    }
  }
  
  public static final RpmSubsysDescription[] RPM_SUBSYS_STATE_DESCRIPTIONS = new RpmSubsysDescription[] { new RpmSubsysDescription(1, "APSS"), new RpmSubsysDescription(2, "MPSS"), new RpmSubsysDescription(4, "ADSP"), new RpmSubsysDescription(8, "CDSP"), new RpmSubsysDescription(16, "TZ"), new RpmSubsysDescription(32, "SLPI"), new RpmSubsysDescription(64, "SLPI_ISLAND"), new RpmSubsysDescription(128, "vmin"), new RpmSubsysDescription(256, "vlow") };
  
  static {
    File file = new File("/proc/rpmh_modem/sleepinfo");
    OPPO_MODEM_OUT = file.exists();
  }
  
  int mApssStats = 0;
  
  long mApssStatsTime = 0L;
  
  int mMpssStats = 0;
  
  long mMpssStatsTime = 0L;
  
  int mAdspStats = 0;
  
  long mAdspStatsTime = 0L;
  
  int mCdspStats = 0;
  
  long mCdspStatsTime = 0L;
  
  int mTzStats = 0;
  
  long mTzStatsTime = 0L;
  
  int mVlowStats = 0;
  
  long mVlowStatsTime = 0L;
  
  int mVminStats = 0;
  
  long mVminStatsTime = 0L;
  
  int mSlpiStats = 0;
  
  long mSlpiStatsTime = 0L;
  
  int mSlplandStats = 0;
  
  long mSlplandStatsTime = 0L;
  
  int mLastApssStats = 0;
  
  long mLastApssStatsTime = 0L;
  
  int mLastMpssStats = 0;
  
  long mLastMpssStatsTime = 0L;
  
  int mLastAdspStats = 0;
  
  long mLastAdspStatsTime = 0L;
  
  int mLastCdspStats = 0;
  
  long mLastCdspStatsTime = 0L;
  
  int mLastTzStats = 0;
  
  long mLastTzStatsTime = 0L;
  
  int mLastVlowStats = 0;
  
  long mLastVlowStatsTime = 0L;
  
  int mLastVminStats = 0;
  
  long mLastVminStatsTime = 0L;
  
  int mLastSlpiStats = 0;
  
  long mLastSlpiStatsTime = 0L;
  
  int mLastSlplandStats = 0;
  
  long mLastSlplandStatsTime = 0L;
  
  long mCurrentElapseRealTime = 0L;
  
  long mLastUpdatedElapseRealTime = 0L;
  
  long mApssStatsTimeScreenDelta = 0L;
  
  long mMpssStatsTimeScreenDelta = 0L;
  
  long mAdspStatsTimeScreenDelta = 0L;
  
  long mCdspStatsTimeScreenDelta = 0L;
  
  long mTzStatsTimeScreenDelta = 0L;
  
  long mVlowStatsTimeScreenDelta = 0L;
  
  long mVminStatsTimeScreenDelta = 0L;
  
  long mSlpiStatsTimeScreenDelta = 0L;
  
  long mSlplandStatsTimeScreenDelta = 0L;
  
  int mVlowStatsScreenDelta = 0;
  
  int mVminStatsScreenDelta = 0;
  
  int mApssStatsScreenDelta = 0;
  
  int mMpssStatsScreenDelta = 0;
  
  int mAdspStatsScreenDelta = 0;
  
  int mCdspStatsScreenDelta = 0;
  
  int mTzStatsScreenDelta = 0;
  
  int mSlpiStatsScreenDelta = 0;
  
  int mSlplandStatsScreenDelta = 0;
  
  double mApssSuspendRatio = 0.0D;
  
  double mMpssSuspendRatio = 0.0D;
  
  double mAdspSuspendRatio = 0.0D;
  
  double mCdspSuspendRatio = 0.0D;
  
  double mVminSuspendRatio = 0.0D;
  
  double mVlowSuspendRatio = 0.0D;
  
  double mSlpiSuspendRatio = 0.0D;
  
  double mSlplandSuspendRatio = 0.0D;
  
  private boolean DEBUG = false;
  
  private double mHundredPercentage = 100.0D;
  
  private int mNumThree = 3;
  
  private int mNumSixteen = 16;
  
  private long mMpssClockCount = 19200L;
  
  private int mDelayUs = 200;
  
  private String mLastStepRpmSuspendRatioSummary = "[]";
  
  private static final int AND_SDK_R = 30;
  
  static final String DEFAULT_OPPO_RPM_MASTER_STATS = "/sys/power/rpmh_stats/oplus_rpmh_master_stats";
  
  static final String DEFAULT_OPPO_RPM_STATS = "/sys/power/system_sleep/oplus_rpmh_stats";
  
  public static final int FLAG_OPPO_RPMH_ADSP_AVAIL = 4;
  
  public static final int FLAG_OPPO_RPMH_APSS_AVAIL = 1;
  
  public static final int FLAG_OPPO_RPMH_CDSP_AVAIL = 8;
  
  public static final int FLAG_OPPO_RPMH_MPSS_AVAIL = 2;
  
  public static final int FLAG_OPPO_RPMH_SLPI_AVAIL = 32;
  
  public static final int FLAG_OPPO_RPMH_SLPI_ISLAND_AVAIL = 64;
  
  public static final int FLAG_OPPO_RPMH_TZ_AVAIL = 16;
  
  public static final int FLAG_OPPO_RPMH_VLOW_AVAIL = 256;
  
  public static final int FLAG_OPPO_RPMH_VMIN_AVAIL = 128;
  
  static final String MODEM_SLEEP_RESET = "OPPO_MARK_RESTART";
  
  static final String MPSS_STASTICS_PATH = "/proc/rpmh_modem/sleepinfo";
  
  static final boolean OPPO_MODEM_OUT;
  
  private static final String QCOM_NAME = "qcom";
  
  public static final String TAG = "OppoRpmSubsystemManager";
  
  static final File mpssFile;
  
  private Context mContext;
  
  private String mOppoRpmMasterStatsPath;
  
  private String mOppoRpmStatsPath;
  
  private int mOppoRpmhAvailableSummary;
  
  public OppoRpmSubsystemManager(Context paramContext, Handler paramHandler) {
    this.mContext = paramContext;
    initConfig();
  }
  
  private void initConfig() {
    Slog.d("OppoRpmSubsystemManager", "init...");
    String str = SystemProperties.get("ro.hardware");
    int i = Build.VERSION.SDK_INT;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("hardware=");
    stringBuilder.append(str);
    stringBuilder.append(" sdkVersion=");
    stringBuilder.append(i);
    Slog.d("OppoRpmSubsystemManager", stringBuilder.toString());
    if ("qcom".equals(str)) {
      if (i >= 30) {
        this.mOppoRpmStatsPath = "/sys/power/system_sleep/oplus_rpmh_stats";
        this.mOppoRpmMasterStatsPath = "/sys/power/rpmh_stats/oplus_rpmh_master_stats";
      } else {
        this.mOppoRpmStatsPath = "/sys/power/system_sleep/oplus_rpmh_stats";
        this.mOppoRpmMasterStatsPath = "/sys/power/rpmh_stats/oplus_rpmh_master_stats";
      } 
    } else {
      String str1 = SystemProperties.get("ro.vendor.mediatek.platform");
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("mtk_platform=");
      stringBuilder1.append(str1);
      Slog.d("OppoRpmSubsystemManager", stringBuilder1.toString());
      if (i >= 30) {
        this.mOppoRpmStatsPath = "/sys/kernel/mtk_lpm/cpuidle/spm/oppo_rpmh_stats";
        this.mOppoRpmMasterStatsPath = "/sys/kernel/mtk_lpm/cpuidle/spm/oppo_rpmh_master_stats";
        if ("MT6885".equals(str1)) {
          this.mOppoRpmStatsPath = "/proc/mtk_lpm/spm/oplus_rpmh_stats";
          this.mOppoRpmMasterStatsPath = "/proc/mtk_lpm/spm/oplus_rpmh_master_stats";
        } 
      } else if ("MT6779".equals(str1)) {
        this.mOppoRpmStatsPath = "/sys/kernel/debug/cpuidle/spm/oppo_rpmh_stats";
        this.mOppoRpmMasterStatsPath = "/sys/kernel/debug/cpuidle/spm/oppo_rpmh_master_stats";
      } else if ("MT6885".equals(str1)) {
        this.mOppoRpmStatsPath = "/d/spm/oppo_rpmh_stats";
        this.mOppoRpmMasterStatsPath = "/d/spm/oppo_rpmh_master_stats";
      } else if ("MT6853".equals(str1)) {
        this.mOppoRpmStatsPath = "/d/spm/oppo_rpmh_stats";
        this.mOppoRpmMasterStatsPath = "/d/spm/oppo_rpmh_master_stats";
      } else if ("MT6873".equals(str1)) {
        this.mOppoRpmStatsPath = "/d/spm/oppo_rpmh_stats";
        this.mOppoRpmMasterStatsPath = "/d/spm/oppo_rpmh_master_stats";
      } else {
        this.mOppoRpmStatsPath = "/d/oppo_rpm_stats";
        this.mOppoRpmMasterStatsPath = "/d/oppo_rpm_master_stats";
      } 
    } 
    if (!(new File(this.mOppoRpmStatsPath)).exists())
      this.mOppoRpmStatsPath = "/sys/power/system_sleep/oplus_rpmh_stats"; 
    if (!(new File(this.mOppoRpmMasterStatsPath)).exists())
      this.mOppoRpmMasterStatsPath = "/sys/power/rpmh_stats/oplus_rpmh_master_stats"; 
    stringBuilder = new StringBuilder();
    stringBuilder.append("OppoRpmStatsPath=");
    stringBuilder.append(this.mOppoRpmStatsPath);
    Slog.d("OppoRpmSubsystemManager", stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("OppoRpmMasterStatsPath=");
    stringBuilder.append(this.mOppoRpmMasterStatsPath);
    Slog.d("OppoRpmSubsystemManager", stringBuilder.toString());
  }
  
  public String getOppoRpmStatsFilePath() {
    return this.mOppoRpmStatsPath;
  }
  
  public String getOppoRpmMasterStatsFilePath() {
    return this.mOppoRpmMasterStatsPath;
  }
  
  public int getRpmhAvailableSummary() {
    return this.mOppoRpmhAvailableSummary;
  }
  
  public String getRpmSubsysDescription(int paramInt) {
    int i = RPM_SUBSYS_STATE_DESCRIPTIONS.length;
    if (i <= 0)
      return null; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{ ");
    for (byte b = 0; b < i; b++) {
      if (((RPM_SUBSYS_STATE_DESCRIPTIONS[b]).mask & paramInt) != 0) {
        stringBuilder.append((RPM_SUBSYS_STATE_DESCRIPTIONS[b]).name);
        stringBuilder.append(":1 ");
      } else {
        stringBuilder.append((RPM_SUBSYS_STATE_DESCRIPTIONS[b]).name);
        stringBuilder.append(":0 ");
      } 
    } 
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  private void updateRpmhAvailableSummary(String paramString) {
    int i = RPM_SUBSYS_STATE_DESCRIPTIONS.length;
    if (i <= 0)
      return; 
    for (byte b = 0; b < i; b++) {
      if ((RPM_SUBSYS_STATE_DESCRIPTIONS[b]).name.equals(paramString))
        this.mOppoRpmhAvailableSummary |= (RPM_SUBSYS_STATE_DESCRIPTIONS[b]).mask; 
    } 
    if (this.DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" mOppoRpmhAvailableSummary=");
      stringBuilder.append(this.mOppoRpmhAvailableSummary);
      Slog.d("OppoRpmSubsystemManager", stringBuilder.toString());
      Slog.d("OppoRpmSubsystemManager", getRpmSubsysDescription(this.mOppoRpmhAvailableSummary));
    } 
  }
  
  private void measureOppoRpmMasterStatsDelta() {
    Slog.d("OppoRpmSubsystemManager", "measureOppoRpmMasterStatsDelta... ");
    long l = this.mCurrentElapseRealTime - this.mLastUpdatedElapseRealTime;
    if (l <= 0L)
      return; 
    this.mApssStatsScreenDelta = this.mApssStats - this.mLastApssStats;
    this.mMpssStatsScreenDelta = this.mMpssStats - this.mLastMpssStats;
    this.mAdspStatsScreenDelta = this.mAdspStats - this.mLastAdspStats;
    this.mCdspStatsScreenDelta = this.mCdspStats - this.mLastCdspStats;
    this.mVlowStatsScreenDelta = this.mVlowStats - this.mLastVlowStats;
    this.mVminStatsScreenDelta = this.mVminStats - this.mLastVminStats;
    this.mTzStatsScreenDelta = this.mTzStats - this.mLastTzStats;
    this.mSlpiStatsScreenDelta = this.mSlpiStats - this.mLastSlpiStats;
    this.mSlplandStatsScreenDelta = this.mSlplandStats - this.mLastSlplandStats;
    if (this.DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("[");
      stringBuilder.append(this.mApssStatsScreenDelta);
      stringBuilder.append(",");
      stringBuilder.append(this.mMpssStatsScreenDelta);
      stringBuilder.append(",");
      stringBuilder.append(this.mAdspStatsScreenDelta);
      stringBuilder.append(",");
      stringBuilder.append(this.mCdspStatsScreenDelta);
      stringBuilder.append(",");
      stringBuilder.append(this.mVlowStatsScreenDelta);
      stringBuilder.append(",");
      stringBuilder.append(this.mVminStatsScreenDelta);
      stringBuilder.append(",");
      stringBuilder.append(this.mTzStatsScreenDelta);
      stringBuilder.append(",");
      stringBuilder.append(this.mSlpiStatsScreenDelta);
      stringBuilder.append(",");
      stringBuilder.append(this.mSlplandStatsScreenDelta);
      stringBuilder.append("]");
      Slog.d("OppoRpmSubsystemManager", stringBuilder.toString());
    } 
    this.mApssStatsTimeScreenDelta = this.mApssStatsTime - this.mLastApssStatsTime;
    this.mMpssStatsTimeScreenDelta = this.mMpssStatsTime - this.mLastMpssStatsTime;
    this.mAdspStatsTimeScreenDelta = this.mAdspStatsTime - this.mLastAdspStatsTime;
    this.mCdspStatsTimeScreenDelta = this.mCdspStatsTime - this.mLastCdspStatsTime;
    this.mTzStatsTimeScreenDelta = this.mTzStatsTime - this.mLastTzStatsTime;
    this.mSlpiStatsTimeScreenDelta = this.mSlpiStatsTime - this.mLastSlpiStatsTime;
    this.mSlplandStatsTimeScreenDelta = this.mSlplandStatsTime - this.mLastSlplandStatsTime;
    this.mVlowStatsTimeScreenDelta = this.mVlowStatsTime - this.mLastVlowStatsTime;
    this.mVminStatsTimeScreenDelta = this.mVminStatsTime - this.mLastVminStatsTime;
    if (this.DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("[");
      stringBuilder.append(this.mApssStatsTimeScreenDelta);
      stringBuilder.append(",");
      stringBuilder.append(this.mMpssStatsTimeScreenDelta);
      stringBuilder.append(",");
      stringBuilder.append(this.mAdspStatsTimeScreenDelta);
      stringBuilder.append(",");
      stringBuilder.append(this.mCdspStatsTimeScreenDelta);
      stringBuilder.append(",");
      stringBuilder.append(this.mVlowStatsTimeScreenDelta);
      stringBuilder.append(",");
      stringBuilder.append(this.mVminStatsTimeScreenDelta);
      stringBuilder.append(",");
      stringBuilder.append(this.mTzStatsTimeScreenDelta);
      stringBuilder.append(",");
      stringBuilder.append(this.mSlpiStatsTimeScreenDelta);
      stringBuilder.append(",");
      stringBuilder.append(this.mSlplandStatsTimeScreenDelta);
      stringBuilder.append("]");
      Slog.d("OppoRpmSubsystemManager", stringBuilder.toString());
    } 
    double d1 = this.mApssStatsTimeScreenDelta / l, d2 = this.mHundredPercentage;
    this.mApssSuspendRatio = d1 * d2;
    this.mMpssSuspendRatio = this.mMpssStatsTimeScreenDelta / l * d2;
    this.mAdspSuspendRatio = this.mAdspStatsTimeScreenDelta / l * d2;
    this.mVminSuspendRatio = this.mVminStatsTimeScreenDelta / l * d2;
    this.mVlowSuspendRatio = this.mVlowStatsTimeScreenDelta / l * d2;
    this.mCdspSuspendRatio = this.mCdspStatsTimeScreenDelta / l * d2;
    this.mSlpiSuspendRatio = this.mSlpiStatsTimeScreenDelta / l * d2;
    this.mSlplandSuspendRatio = this.mSlplandStatsTimeScreenDelta / l * d2;
    if (this.DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ApssSuspendRatio:");
      stringBuilder.append(this.mApssSuspendRatio);
      stringBuilder.append("  MpssSuspendRatio:");
      stringBuilder.append(this.mMpssSuspendRatio);
      stringBuilder.append("  AdspSuspendRatio:");
      stringBuilder.append(this.mAdspSuspendRatio);
      stringBuilder.append("  CdspSuspendRatio:");
      stringBuilder.append(this.mCdspSuspendRatio);
      stringBuilder.append("  VminSuspendRatio:");
      stringBuilder.append(this.mVminSuspendRatio);
      stringBuilder.append("  VlowSuspendRatio:");
      stringBuilder.append(this.mVlowSuspendRatio);
      stringBuilder.append("  SlpiSuspendRatio:");
      stringBuilder.append(this.mSlpiSuspendRatio);
      stringBuilder.append("  SlplandSuspendRatio:");
      stringBuilder.append(this.mSlplandSuspendRatio);
      Slog.d("OppoRpmSubsystemManager", stringBuilder.toString());
    } 
    String str = "[]";
    try {
      String str1 = String.format("[apss:%.2f, mpss:%.2f, adsp:%.2f, cdsp:%.2f, vim:%.2f, vlow:%.2f, Slpi:%.2f, Slpland:%.2f]", new Object[] { Double.valueOf(this.mApssSuspendRatio), Double.valueOf(this.mMpssSuspendRatio), Double.valueOf(this.mAdspSuspendRatio), Double.valueOf(this.mCdspSuspendRatio), Double.valueOf(this.mVminSuspendRatio), Double.valueOf(this.mVlowSuspendRatio), Double.valueOf(this.mSlpiSuspendRatio), Double.valueOf(this.mSlplandSuspendRatio) });
    } catch (Exception exception) {
      Slog.e("OppoRpmSubsystemManager", "measureOppoRpmMasterStatsDelta excetion");
    } 
    this.mLastStepRpmSuspendRatioSummary = str;
  }
  
  private void trigger() {
    this.mLastUpdatedElapseRealTime = this.mCurrentElapseRealTime;
    this.mCurrentElapseRealTime = SystemClock.elapsedRealtime();
    getOppoRpmStatsScreen();
    getOppoRpmMasterStatsScreen();
    measureOppoRpmMasterStatsDelta();
  }
  
  private void getOppoRpmStatsScreen() {
    this.mLastVlowStats = this.mVlowStats;
    this.mLastVlowStatsTime = this.mVlowStatsTime;
    this.mLastVminStats = this.mVminStats;
    this.mLastVminStatsTime = this.mVminStatsTime;
    File file = new File(this.mOppoRpmStatsPath);
    BufferedReader bufferedReader1 = null, bufferedReader2 = null, bufferedReader3 = null, bufferedReader4 = null;
    if (this.DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getOppoRpmStatsScreen:");
      stringBuilder.append(this.mOppoRpmStatsPath);
      Slog.d("OppoRpmSubsystemManager", stringBuilder.toString());
    } 
    BufferedReader bufferedReader5 = bufferedReader4, bufferedReader6 = bufferedReader1, bufferedReader7 = bufferedReader2, bufferedReader8 = bufferedReader3;
    try {
      BufferedReader bufferedReader = new BufferedReader();
      bufferedReader5 = bufferedReader4;
      bufferedReader6 = bufferedReader1;
      bufferedReader7 = bufferedReader2;
      bufferedReader8 = bufferedReader3;
      FileReader fileReader = new FileReader();
      bufferedReader5 = bufferedReader4;
      bufferedReader6 = bufferedReader1;
      bufferedReader7 = bufferedReader2;
      bufferedReader8 = bufferedReader3;
      this(file);
      bufferedReader5 = bufferedReader4;
      bufferedReader6 = bufferedReader1;
      bufferedReader7 = bufferedReader2;
      bufferedReader8 = bufferedReader3;
      this(fileReader);
      bufferedReader1 = bufferedReader;
      while (true) {
        bufferedReader5 = bufferedReader1;
        bufferedReader6 = bufferedReader1;
        bufferedReader7 = bufferedReader1;
        bufferedReader8 = bufferedReader1;
        String str = bufferedReader1.readLine();
        if (str != null) {
          bufferedReader5 = bufferedReader1;
          bufferedReader6 = bufferedReader1;
          bufferedReader7 = bufferedReader1;
          bufferedReader8 = bufferedReader1;
          if (this.DEBUG) {
            bufferedReader5 = bufferedReader1;
            bufferedReader6 = bufferedReader1;
            bufferedReader7 = bufferedReader1;
            bufferedReader8 = bufferedReader1;
            Slog.d("OppoRpmSubsystemManager", str);
          } 
          bufferedReader5 = bufferedReader1;
          bufferedReader6 = bufferedReader1;
          bufferedReader7 = bufferedReader1;
          bufferedReader8 = bufferedReader1;
          if (str.length() != 0) {
            bufferedReader5 = bufferedReader1;
            bufferedReader6 = bufferedReader1;
            bufferedReader7 = bufferedReader1;
            bufferedReader8 = bufferedReader1;
            String[] arrayOfString = str.split(":");
            bufferedReader5 = bufferedReader1;
            bufferedReader6 = bufferedReader1;
            bufferedReader7 = bufferedReader1;
            bufferedReader8 = bufferedReader1;
            if (arrayOfString.length == this.mNumThree) {
              bufferedReader5 = bufferedReader1;
              bufferedReader6 = bufferedReader1;
              bufferedReader7 = bufferedReader1;
              bufferedReader8 = bufferedReader1;
              updateRpmhAvailableSummary(arrayOfString[0]);
              bufferedReader5 = bufferedReader1;
              bufferedReader6 = bufferedReader1;
              bufferedReader7 = bufferedReader1;
              bufferedReader8 = bufferedReader1;
              if (arrayOfString[0].equals("vlow")) {
                bufferedReader5 = bufferedReader1;
                bufferedReader6 = bufferedReader1;
                bufferedReader7 = bufferedReader1;
                bufferedReader8 = bufferedReader1;
                this.mVlowStats = Integer.parseInt(arrayOfString[1], this.mNumSixteen);
                bufferedReader5 = bufferedReader1;
                bufferedReader6 = bufferedReader1;
                bufferedReader7 = bufferedReader1;
                bufferedReader8 = bufferedReader1;
                this.mVlowStatsTime = Long.parseLong(arrayOfString[2], this.mNumSixteen);
                continue;
              } 
              bufferedReader5 = bufferedReader1;
              bufferedReader6 = bufferedReader1;
              bufferedReader7 = bufferedReader1;
              bufferedReader8 = bufferedReader1;
              if (arrayOfString[0].equals("vmin")) {
                bufferedReader5 = bufferedReader1;
                bufferedReader6 = bufferedReader1;
                bufferedReader7 = bufferedReader1;
                bufferedReader8 = bufferedReader1;
                this.mVminStats = Integer.parseInt(arrayOfString[1], this.mNumSixteen);
                bufferedReader5 = bufferedReader1;
                bufferedReader6 = bufferedReader1;
                bufferedReader7 = bufferedReader1;
                bufferedReader8 = bufferedReader1;
                this.mVminStatsTime = Long.parseLong(arrayOfString[2], this.mNumSixteen);
              } 
            } 
          } 
          continue;
        } 
        break;
      } 
      try {
        bufferedReader1.close();
      } catch (IOException iOException) {
        iOException.printStackTrace();
      } 
    } catch (FileNotFoundException fileNotFoundException) {
      bufferedReader5 = bufferedReader8;
      fileNotFoundException.printStackTrace();
      if (bufferedReader8 != null)
        bufferedReader8.close(); 
    } catch (IOException iOException) {
      bufferedReader5 = bufferedReader7;
      iOException.printStackTrace();
      if (bufferedReader7 != null)
        bufferedReader7.close(); 
    } catch (NumberFormatException numberFormatException) {
      bufferedReader5 = bufferedReader6;
      numberFormatException.printStackTrace();
      if (bufferedReader6 != null)
        bufferedReader6.close(); 
    } finally {}
  }
  
  private class GetMpssSleepinfo {
    public int mMpssStats = 0;
    
    public long mMpssStatsTime = 0L;
    
    final OppoRpmSubsystemManager this$0;
    
    public boolean startQmiService() {
      boolean bool;
      File file = new File("/proc/rpmh_modem/sleepinfo");
      if (!file.exists())
        return false; 
      try {
        OppoRpmSubsystemManager.this.writeStringToFile("/proc/rpmh_modem/sleepinfo", "OPPO_MARK_RESTART");
        SystemProperties.set("ctl.start", "qmi_master_stats_service");
        Slog.d("OppoRpmSubsystemManager", "qmi_master_stats_service start");
        bool = true;
      } catch (Exception exception) {
        exception.printStackTrace();
        bool = false;
      } 
      try {
        Thread.sleep(OppoRpmSubsystemManager.this.mDelayUs);
      } catch (InterruptedException interruptedException) {
        interruptedException.printStackTrace();
        bool = false;
      } 
      return bool;
    }
    
    public int getMpssStats() {
      File file = new File("/proc/rpmh_modem/sleepinfo");
      BufferedReader bufferedReader1 = null, bufferedReader2 = null, bufferedReader3 = null, bufferedReader4 = null;
      Slog.d("OppoRpmSubsystemManager", "getMpssStats:/proc/rpmh_modem/sleepinfo");
      BufferedReader bufferedReader5 = bufferedReader4, bufferedReader6 = bufferedReader1, bufferedReader7 = bufferedReader2, bufferedReader8 = bufferedReader3;
      try {
        BufferedReader bufferedReader = new BufferedReader();
        bufferedReader5 = bufferedReader4;
        bufferedReader6 = bufferedReader1;
        bufferedReader7 = bufferedReader2;
        bufferedReader8 = bufferedReader3;
        FileReader fileReader = new FileReader();
        bufferedReader5 = bufferedReader4;
        bufferedReader6 = bufferedReader1;
        bufferedReader7 = bufferedReader2;
        bufferedReader8 = bufferedReader3;
        this(file);
        bufferedReader5 = bufferedReader4;
        bufferedReader6 = bufferedReader1;
        bufferedReader7 = bufferedReader2;
        bufferedReader8 = bufferedReader3;
        this(fileReader);
        bufferedReader1 = bufferedReader;
        while (true) {
          bufferedReader5 = bufferedReader1;
          bufferedReader6 = bufferedReader1;
          bufferedReader7 = bufferedReader1;
          bufferedReader8 = bufferedReader1;
          String str = bufferedReader1.readLine();
          if (str != null) {
            bufferedReader5 = bufferedReader1;
            bufferedReader6 = bufferedReader1;
            bufferedReader7 = bufferedReader1;
            bufferedReader8 = bufferedReader1;
            if (str.length() != 0) {
              bufferedReader5 = bufferedReader1;
              bufferedReader6 = bufferedReader1;
              bufferedReader7 = bufferedReader1;
              bufferedReader8 = bufferedReader1;
              String[] arrayOfString = str.split(":");
              bufferedReader5 = bufferedReader1;
              bufferedReader6 = bufferedReader1;
              bufferedReader7 = bufferedReader1;
              bufferedReader8 = bufferedReader1;
              if (arrayOfString.length == 2) {
                bufferedReader5 = bufferedReader1;
                bufferedReader6 = bufferedReader1;
                bufferedReader7 = bufferedReader1;
                bufferedReader8 = bufferedReader1;
                if (arrayOfString[0].trim().contains("Count")) {
                  bufferedReader5 = bufferedReader1;
                  bufferedReader6 = bufferedReader1;
                  bufferedReader7 = bufferedReader1;
                  bufferedReader8 = bufferedReader1;
                  this.mMpssStats = Integer.parseInt(arrayOfString[1].trim().substring(2), OppoRpmSubsystemManager.this.mNumSixteen);
                } 
              } 
            } 
            continue;
          } 
          break;
        } 
        try {
          bufferedReader1.close();
        } catch (IOException iOException) {
          iOException.printStackTrace();
        } 
      } catch (FileNotFoundException fileNotFoundException) {
        bufferedReader5 = bufferedReader8;
        fileNotFoundException.printStackTrace();
        if (bufferedReader8 != null)
          bufferedReader8.close(); 
      } catch (IOException iOException) {
        bufferedReader5 = bufferedReader7;
        iOException.printStackTrace();
        if (bufferedReader7 != null)
          bufferedReader7.close(); 
      } catch (NumberFormatException numberFormatException) {
        bufferedReader5 = bufferedReader6;
        numberFormatException.printStackTrace();
        if (bufferedReader6 != null)
          bufferedReader6.close(); 
      } finally {}
      return this.mMpssStats;
    }
    
    public long getMpssSleepTime() {
      File file = new File("/proc/rpmh_modem/sleepinfo");
      BufferedReader bufferedReader1 = null, bufferedReader2 = null, bufferedReader3 = null, bufferedReader4 = null;
      BufferedReader bufferedReader5 = bufferedReader4, bufferedReader6 = bufferedReader1, bufferedReader7 = bufferedReader2, bufferedReader8 = bufferedReader3;
      try {
        BufferedReader bufferedReader = new BufferedReader();
        bufferedReader5 = bufferedReader4;
        bufferedReader6 = bufferedReader1;
        bufferedReader7 = bufferedReader2;
        bufferedReader8 = bufferedReader3;
        FileReader fileReader = new FileReader();
        bufferedReader5 = bufferedReader4;
        bufferedReader6 = bufferedReader1;
        bufferedReader7 = bufferedReader2;
        bufferedReader8 = bufferedReader3;
        this(file);
        bufferedReader5 = bufferedReader4;
        bufferedReader6 = bufferedReader1;
        bufferedReader7 = bufferedReader2;
        bufferedReader8 = bufferedReader3;
        this(fileReader);
        bufferedReader1 = bufferedReader;
        while (true) {
          bufferedReader5 = bufferedReader1;
          bufferedReader6 = bufferedReader1;
          bufferedReader7 = bufferedReader1;
          bufferedReader8 = bufferedReader1;
          String str = bufferedReader1.readLine();
          if (str != null) {
            bufferedReader5 = bufferedReader1;
            bufferedReader6 = bufferedReader1;
            bufferedReader7 = bufferedReader1;
            bufferedReader8 = bufferedReader1;
            if (str.length() != 0) {
              bufferedReader5 = bufferedReader1;
              bufferedReader6 = bufferedReader1;
              bufferedReader7 = bufferedReader1;
              bufferedReader8 = bufferedReader1;
              String[] arrayOfString = str.split(":");
              bufferedReader5 = bufferedReader1;
              bufferedReader6 = bufferedReader1;
              bufferedReader7 = bufferedReader1;
              bufferedReader8 = bufferedReader1;
              if (arrayOfString.length == 2) {
                bufferedReader5 = bufferedReader1;
                bufferedReader6 = bufferedReader1;
                bufferedReader7 = bufferedReader1;
                bufferedReader8 = bufferedReader1;
                if (arrayOfString[0].trim().contains("Duration")) {
                  bufferedReader5 = bufferedReader1;
                  bufferedReader6 = bufferedReader1;
                  bufferedReader7 = bufferedReader1;
                  bufferedReader8 = bufferedReader1;
                  this.mMpssStatsTime = Long.parseLong(arrayOfString[1].trim().substring(2), OppoRpmSubsystemManager.this.mNumSixteen) / OppoRpmSubsystemManager.this.mMpssClockCount;
                } 
              } 
            } 
            continue;
          } 
          break;
        } 
        try {
          bufferedReader1.close();
        } catch (IOException iOException) {
          iOException.printStackTrace();
        } 
      } catch (FileNotFoundException fileNotFoundException) {
        bufferedReader5 = bufferedReader8;
        fileNotFoundException.printStackTrace();
        if (bufferedReader8 != null)
          bufferedReader8.close(); 
      } catch (IOException iOException) {
        bufferedReader5 = bufferedReader7;
        iOException.printStackTrace();
        if (bufferedReader7 != null)
          bufferedReader7.close(); 
      } catch (NumberFormatException numberFormatException) {
        bufferedReader5 = bufferedReader6;
        numberFormatException.printStackTrace();
        if (bufferedReader6 != null)
          bufferedReader6.close(); 
      } finally {}
      return this.mMpssStatsTime;
    }
    
    private GetMpssSleepinfo() {}
  }
  
  private void getOppoRpmMasterStatsScreen() {
    this.mLastApssStats = this.mApssStats;
    this.mLastApssStatsTime = this.mApssStatsTime;
    this.mLastMpssStats = this.mMpssStats;
    this.mLastMpssStatsTime = this.mMpssStatsTime;
    this.mLastAdspStats = this.mAdspStats;
    this.mLastAdspStatsTime = this.mAdspStatsTime;
    this.mLastCdspStats = this.mCdspStats;
    this.mLastCdspStatsTime = this.mCdspStatsTime;
    this.mLastTzStats = this.mTzStats;
    this.mLastTzStatsTime = this.mTzStatsTime;
    this.mLastSlpiStats = this.mSlpiStats;
    this.mLastSlpiStatsTime = this.mSlpiStatsTime;
    this.mLastSlplandStats = this.mSlplandStats;
    this.mLastSlplandStatsTime = this.mSlplandStatsTime;
    File file = new File(this.mOppoRpmMasterStatsPath);
    BufferedReader bufferedReader1 = null, bufferedReader2 = null, bufferedReader3 = null;
    StringBuilder stringBuilder1 = null;
    boolean bool = true;
    GetMpssSleepinfo getMpssSleepinfo = new GetMpssSleepinfo();
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("getOppoRpmMasterStatsScreen:");
    stringBuilder2.append(this.mOppoRpmMasterStatsPath);
    Slog.d("OppoRpmSubsystemManager", stringBuilder2.toString());
    stringBuilder2 = stringBuilder1;
    BufferedReader bufferedReader4 = bufferedReader1, bufferedReader5 = bufferedReader2, bufferedReader6 = bufferedReader3;
    try {
      BufferedReader bufferedReader8 = new BufferedReader();
      stringBuilder2 = stringBuilder1;
      bufferedReader4 = bufferedReader1;
      bufferedReader5 = bufferedReader2;
      bufferedReader6 = bufferedReader3;
      FileReader fileReader = new FileReader();
      stringBuilder2 = stringBuilder1;
      bufferedReader4 = bufferedReader1;
      bufferedReader5 = bufferedReader2;
      bufferedReader6 = bufferedReader3;
      this(file);
      stringBuilder2 = stringBuilder1;
      bufferedReader4 = bufferedReader1;
      bufferedReader5 = bufferedReader2;
      bufferedReader6 = bufferedReader3;
      this(fileReader);
      BufferedReader bufferedReader7 = bufferedReader8;
      while (true) {
        BufferedReader bufferedReader = bufferedReader7;
        bufferedReader4 = bufferedReader7;
        bufferedReader5 = bufferedReader7;
        bufferedReader6 = bufferedReader7;
        String str = bufferedReader7.readLine();
        if (str != null) {
          bufferedReader = bufferedReader7;
          bufferedReader4 = bufferedReader7;
          bufferedReader5 = bufferedReader7;
          bufferedReader6 = bufferedReader7;
          Slog.d("OppoRpmSubsystemManager", str);
          boolean bool1 = bool;
          bufferedReader = bufferedReader7;
          bufferedReader4 = bufferedReader7;
          bufferedReader5 = bufferedReader7;
          bufferedReader6 = bufferedReader7;
          if (str.length() != 0) {
            bufferedReader = bufferedReader7;
            bufferedReader4 = bufferedReader7;
            bufferedReader5 = bufferedReader7;
            bufferedReader6 = bufferedReader7;
            String[] arrayOfString = str.split(":");
            bool1 = bool;
            bufferedReader = bufferedReader7;
            bufferedReader4 = bufferedReader7;
            bufferedReader5 = bufferedReader7;
            bufferedReader6 = bufferedReader7;
            if (arrayOfString.length == this.mNumThree) {
              bufferedReader = bufferedReader7;
              bufferedReader4 = bufferedReader7;
              bufferedReader5 = bufferedReader7;
              bufferedReader6 = bufferedReader7;
              updateRpmhAvailableSummary(arrayOfString[0]);
              bufferedReader = bufferedReader7;
              bufferedReader4 = bufferedReader7;
              bufferedReader5 = bufferedReader7;
              bufferedReader6 = bufferedReader7;
              if (arrayOfString[0].equals("APSS")) {
                bufferedReader = bufferedReader7;
                bufferedReader4 = bufferedReader7;
                bufferedReader5 = bufferedReader7;
                bufferedReader6 = bufferedReader7;
                this.mApssStats = Integer.parseInt(arrayOfString[1], this.mNumSixteen);
                bufferedReader = bufferedReader7;
                bufferedReader4 = bufferedReader7;
                bufferedReader5 = bufferedReader7;
                bufferedReader6 = bufferedReader7;
                this.mApssStatsTime = Long.parseLong(arrayOfString[2], this.mNumSixteen);
                bool1 = bool;
              } else {
                bufferedReader = bufferedReader7;
                bufferedReader4 = bufferedReader7;
                bufferedReader5 = bufferedReader7;
                bufferedReader6 = bufferedReader7;
                if (arrayOfString[0].equals("MPSS")) {
                  bufferedReader = bufferedReader7;
                  bufferedReader4 = bufferedReader7;
                  bufferedReader5 = bufferedReader7;
                  bufferedReader6 = bufferedReader7;
                  this.mMpssStats = Integer.parseInt(arrayOfString[1], this.mNumSixteen);
                  bufferedReader = bufferedReader7;
                  bufferedReader4 = bufferedReader7;
                  bufferedReader5 = bufferedReader7;
                  bufferedReader6 = bufferedReader7;
                  this.mMpssStatsTime = Long.parseLong(arrayOfString[2], this.mNumSixteen);
                  bool1 = false;
                } else {
                  bufferedReader = bufferedReader7;
                  bufferedReader4 = bufferedReader7;
                  bufferedReader5 = bufferedReader7;
                  bufferedReader6 = bufferedReader7;
                  if (arrayOfString[0].equals("ADSP")) {
                    bufferedReader = bufferedReader7;
                    bufferedReader4 = bufferedReader7;
                    bufferedReader5 = bufferedReader7;
                    bufferedReader6 = bufferedReader7;
                    this.mAdspStats = Integer.parseInt(arrayOfString[1], this.mNumSixteen);
                    bufferedReader = bufferedReader7;
                    bufferedReader4 = bufferedReader7;
                    bufferedReader5 = bufferedReader7;
                    bufferedReader6 = bufferedReader7;
                    this.mAdspStatsTime = Long.parseLong(arrayOfString[2], this.mNumSixteen);
                    bool1 = bool;
                  } else {
                    bufferedReader = bufferedReader7;
                    bufferedReader4 = bufferedReader7;
                    bufferedReader5 = bufferedReader7;
                    bufferedReader6 = bufferedReader7;
                    if (arrayOfString[0].equals("CDSP")) {
                      bufferedReader = bufferedReader7;
                      bufferedReader4 = bufferedReader7;
                      bufferedReader5 = bufferedReader7;
                      bufferedReader6 = bufferedReader7;
                      this.mCdspStats = Integer.parseInt(arrayOfString[1], this.mNumSixteen);
                      bufferedReader = bufferedReader7;
                      bufferedReader4 = bufferedReader7;
                      bufferedReader5 = bufferedReader7;
                      bufferedReader6 = bufferedReader7;
                      this.mCdspStatsTime = Long.parseLong(arrayOfString[2], this.mNumSixteen);
                      bool1 = bool;
                    } else {
                      bufferedReader = bufferedReader7;
                      bufferedReader4 = bufferedReader7;
                      bufferedReader5 = bufferedReader7;
                      bufferedReader6 = bufferedReader7;
                      if (arrayOfString[0].equals("TZ")) {
                        bufferedReader = bufferedReader7;
                        bufferedReader4 = bufferedReader7;
                        bufferedReader5 = bufferedReader7;
                        bufferedReader6 = bufferedReader7;
                        this.mTzStats = Integer.parseInt(arrayOfString[1], this.mNumSixteen);
                        bufferedReader = bufferedReader7;
                        bufferedReader4 = bufferedReader7;
                        bufferedReader5 = bufferedReader7;
                        bufferedReader6 = bufferedReader7;
                        this.mTzStatsTime = Long.parseLong(arrayOfString[2], this.mNumSixteen);
                        bool1 = bool;
                      } else {
                        bufferedReader = bufferedReader7;
                        bufferedReader4 = bufferedReader7;
                        bufferedReader5 = bufferedReader7;
                        bufferedReader6 = bufferedReader7;
                        if (arrayOfString[0].equals("SLPI")) {
                          bufferedReader = bufferedReader7;
                          bufferedReader4 = bufferedReader7;
                          bufferedReader5 = bufferedReader7;
                          bufferedReader6 = bufferedReader7;
                          this.mSlpiStats = Integer.parseInt(arrayOfString[1], this.mNumSixteen);
                          bufferedReader = bufferedReader7;
                          bufferedReader4 = bufferedReader7;
                          bufferedReader5 = bufferedReader7;
                          bufferedReader6 = bufferedReader7;
                          this.mSlpiStatsTime = Long.parseLong(arrayOfString[2], this.mNumSixteen);
                          bool1 = bool;
                        } else {
                          bool1 = bool;
                          bufferedReader = bufferedReader7;
                          bufferedReader4 = bufferedReader7;
                          bufferedReader5 = bufferedReader7;
                          bufferedReader6 = bufferedReader7;
                          if (arrayOfString[0].equals("SLPI_ISLAND")) {
                            bufferedReader = bufferedReader7;
                            bufferedReader4 = bufferedReader7;
                            bufferedReader5 = bufferedReader7;
                            bufferedReader6 = bufferedReader7;
                            this.mSlplandStats = Integer.parseInt(arrayOfString[1], this.mNumSixteen);
                            bufferedReader = bufferedReader7;
                            bufferedReader4 = bufferedReader7;
                            bufferedReader5 = bufferedReader7;
                            bufferedReader6 = bufferedReader7;
                            this.mSlplandStatsTime = Long.parseLong(arrayOfString[2], this.mNumSixteen);
                            bool1 = bool;
                          } 
                        } 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
          bool = bool1;
          continue;
        } 
        break;
      } 
      if (bool) {
        BufferedReader bufferedReader = bufferedReader7;
        bufferedReader4 = bufferedReader7;
        bufferedReader5 = bufferedReader7;
        bufferedReader6 = bufferedReader7;
        StringBuilder stringBuilder = new StringBuilder();
        bufferedReader = bufferedReader7;
        bufferedReader4 = bufferedReader7;
        bufferedReader5 = bufferedReader7;
        bufferedReader6 = bufferedReader7;
        this();
        bufferedReader = bufferedReader7;
        bufferedReader4 = bufferedReader7;
        bufferedReader5 = bufferedReader7;
        bufferedReader6 = bufferedReader7;
        stringBuilder.append("IsModemOut=");
        bufferedReader = bufferedReader7;
        bufferedReader4 = bufferedReader7;
        bufferedReader5 = bufferedReader7;
        bufferedReader6 = bufferedReader7;
        stringBuilder.append(bool);
        bufferedReader = bufferedReader7;
        bufferedReader4 = bufferedReader7;
        bufferedReader5 = bufferedReader7;
        bufferedReader6 = bufferedReader7;
        Slog.d("OppoRpmSubsystemManager", stringBuilder.toString());
        bufferedReader = bufferedReader7;
        bufferedReader4 = bufferedReader7;
        bufferedReader5 = bufferedReader7;
        bufferedReader6 = bufferedReader7;
        if (getMpssSleepinfo.startQmiService()) {
          bufferedReader = bufferedReader7;
          bufferedReader4 = bufferedReader7;
          bufferedReader5 = bufferedReader7;
          bufferedReader6 = bufferedReader7;
          this.mMpssStats = getMpssSleepinfo.getMpssStats();
          bufferedReader = bufferedReader7;
          bufferedReader4 = bufferedReader7;
          bufferedReader5 = bufferedReader7;
          bufferedReader6 = bufferedReader7;
          this.mMpssStatsTime = getMpssSleepinfo.getMpssSleepTime();
        } 
      } 
      try {
        bufferedReader7.close();
      } catch (IOException iOException) {
        iOException.printStackTrace();
      } 
    } catch (FileNotFoundException fileNotFoundException) {
      BufferedReader bufferedReader = bufferedReader6;
      fileNotFoundException.printStackTrace();
      if (bufferedReader6 != null)
        bufferedReader6.close(); 
    } catch (IOException iOException) {
      BufferedReader bufferedReader = bufferedReader5;
      iOException.printStackTrace();
      if (bufferedReader5 != null)
        bufferedReader5.close(); 
    } catch (NumberFormatException numberFormatException) {
      BufferedReader bufferedReader = bufferedReader4;
      numberFormatException.printStackTrace();
      if (bufferedReader4 != null)
        bufferedReader4.close(); 
    } finally {}
  }
  
  private boolean writeStringToFile(String paramString1, String paramString2) {
    boolean bool1 = false, bool2 = false, bool3 = false;
    FileWriter fileWriter1 = null, fileWriter2 = null;
    FileWriter fileWriter3 = fileWriter2, fileWriter4 = fileWriter1;
    try {
      File file = new File();
      fileWriter3 = fileWriter2;
      fileWriter4 = fileWriter1;
      this(paramString1);
      fileWriter3 = fileWriter2;
      fileWriter4 = fileWriter1;
      FileWriter fileWriter = new FileWriter();
      fileWriter3 = fileWriter2;
      fileWriter4 = fileWriter1;
      this(file);
      fileWriter3 = fileWriter;
      fileWriter4 = fileWriter;
      fileWriter.write(paramString2);
      fileWriter3 = fileWriter;
      fileWriter4 = fileWriter;
      fileWriter.close();
      bool3 = true;
      bool2 = true;
      bool1 = true;
      if (false) {
        bool2 = bool3;
        try {
          throw new NullPointerException();
        } catch (Exception exception) {
          Slog.e("OppoRpmSubsystemManager", "finally close writer failed.", exception);
        } 
      } 
    } catch (Exception exception) {
      fileWriter3 = fileWriter4;
      Slog.e("OppoRpmSubsystemManager", "writeStringToFile sorry write wrong");
      fileWriter3 = fileWriter4;
      exception.printStackTrace();
      if (fileWriter4 != null) {
        bool2 = bool1;
        fileWriter4.close();
        bool2 = bool3;
      } 
    } finally {}
    return bool2;
  }
  
  public void onBatteryDrained() {
    Slog.d("OppoRpmSubsystemManager", "battery drained... ");
    trigger();
  }
  
  public void onScreenStateChaned(boolean paramBoolean) {}
  
  public void onBootCompleted() {
    trigger();
  }
  
  public String getLastStepRpmSuspendRatioSummary() {
    return this.mLastStepRpmSuspendRatioSummary;
  }
}
