package com.android.internal.os;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.provider.Settings;
import android.telephony.ModemActivityInfo;
import android.text.format.DateFormat;
import android.util.Slog;
import core.java.com.android.internal.os.SmartEndcStatus;
import java.util.Arrays;

class OppoDevicePowerStats {
  private final Object mLock = new Object();
  
  private String mCurrentTopActivity = "unknow";
  
  private String mCurModemInfoSummary = "0";
  
  private String mLastModemInfoSummary = "0";
  
  private String mCurEndcInfoSummary = "0";
  
  private String mLastEndcInfoSummary = "0";
  
  private int[] mCurModemTxTimes = new int[5];
  
  private boolean mMeaturing = false;
  
  public static final boolean DEBUG = false;
  
  private static final double MILLISECONDS_IN_HOUR = 3600000.0D;
  
  private static final long MILLISECONDS_IN_YEAR = 31536000000L;
  
  static final int MSG_NOTIFY_MOBILE_ACTIVTY_UPDATED = 1;
  
  static final int MSG_NOTIFY_POWER_DRAIN_UPDATED = 2;
  
  static final int MSG_NOTIFY_TOP_ACTIVITY_UPDATED = 3;
  
  static final int MSG_NOTIFY_TRAFFIC_UPDATED = 4;
  
  private static final String NHS_MD_ACI_SAFE_PERMISSION = "com.oppo.nhs.permission.NHS_MD_ACI_SAFE_PERMISSION";
  
  private static final int ONE_THOUSAND = 1000;
  
  public static final String TAG = "OppoDevicePowerStats";
  
  private static final int TRAN_CODE = 1013;
  
  private static final int TRAN_FLAG = 0;
  
  private static final long UPDATE_INTERVAL = 60000L;
  
  private boolean mBTHeadsetConnected;
  
  private long mBluetoothPowerDrainMaMs;
  
  private Context mContext;
  
  private DevicePowerDetails mCurOppoDevicePowerDetails;
  
  private SmartEndcStatus mCurSmartEndcStatus;
  
  private long mCurStepBatteryUpTime;
  
  private int mCurrScreenBrightness;
  
  private DevicePowerDetails mDevicePowerDetailsDelta;
  
  private long mGpsPowerDrainMaMs;
  
  private DevicePowerDetails mLastLastOppoDevicePowerDetails;
  
  private DevicePowerDetails mLastOppoDevicePowerDetails;
  
  private long mLastStepBatteryUpTime;
  
  private long mMobilePowerDrainMaMs;
  
  private long mMobileRxTotalBytes;
  
  private long mMobileTxTotalBytes;
  
  private OppoRpmSubsystemManager mOppoRpmManager;
  
  private PowerDetailsConstants mPowerConstants;
  
  private PowerDetailsReceiver mPowerDetailsReceiver;
  
  private PowerProfile mPowerProfile;
  
  private int mScreenRefreshMode;
  
  private int mVolumeMusicSpeaker;
  
  private boolean mWifiApStateEnabled;
  
  private long mWifiPowerDrainMaMs;
  
  private long mWifiRxTotalBytes;
  
  private long mWifiTxTotalBytes;
  
  private boolean mWiredHeadsetConnected;
  
  public OppoDevicePowerStats(Context paramContext, Handler paramHandler) {
    this.mContext = paramContext;
    this.mPowerConstants = new PowerDetailsConstants(paramHandler);
    this.mOppoRpmManager = new OppoRpmSubsystemManager(this.mContext, paramHandler);
    this.mCurOppoDevicePowerDetails = new DevicePowerDetails();
    this.mLastOppoDevicePowerDetails = new DevicePowerDetails();
    this.mLastLastOppoDevicePowerDetails = new DevicePowerDetails();
    this.mDevicePowerDetailsDelta = new DevicePowerDetails();
    this.mCurSmartEndcStatus = new SmartEndcStatus();
  }
  
  public void onSystemServicesReady(Context paramContext) {
    this.mContext = paramContext;
    this.mPowerConstants.startObserving(paramContext, paramContext.getContentResolver());
    PowerDetailsReceiver powerDetailsReceiver = new PowerDetailsReceiver(paramContext);
    powerDetailsReceiver.register();
  }
  
  class WorkHandler extends Handler {
    final OppoDevicePowerStats this$0;
    
    public WorkHandler(Looper param1Looper) {
      super(param1Looper, null, true);
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
    }
  }
  
  public void clear() {
    this.mCurOppoDevicePowerDetails.clear();
    this.mLastOppoDevicePowerDetails.clear();
    this.mLastLastOppoDevicePowerDetails.clear();
    this.mDevicePowerDetailsDelta.clear();
  }
  
  public void dumpHistory() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("current:");
    stringBuilder.append(this.mCurOppoDevicePowerDetails.toString());
    Slog.d("OppoDevicePowerStats", stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("last0:");
    stringBuilder.append(this.mLastOppoDevicePowerDetails.toString());
    Slog.d("OppoDevicePowerStats", stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("last1:");
    stringBuilder.append(this.mLastLastOppoDevicePowerDetails.toString());
    Slog.d("OppoDevicePowerStats", stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("delta:");
    stringBuilder.append(this.mDevicePowerDetailsDelta.toString());
    Slog.d("OppoDevicePowerStats", stringBuilder.toString());
  }
  
  public void recordBluetoothPowerDrainMaMs(long paramLong) {
    synchronized (this.mLock) {
      DevicePowerDetails devicePowerDetails = this.mCurOppoDevicePowerDetails;
      devicePowerDetails.mBluetoothPowerDrainMaMs += paramLong;
      return;
    } 
  }
  
  public void recordMobilePowerDrainMaMs(long paramLong) {
    synchronized (this.mLock) {
      DevicePowerDetails devicePowerDetails = this.mCurOppoDevicePowerDetails;
      devicePowerDetails.mMobilePowerDrainMaMs += paramLong;
      return;
    } 
  }
  
  public void recordWifiPowerDrainMaMs(long paramLong) {
    synchronized (this.mLock) {
      DevicePowerDetails devicePowerDetails = this.mCurOppoDevicePowerDetails;
      devicePowerDetails.mWifiPowerDrainMaMs += paramLong;
      return;
    } 
  }
  
  public void recordGpsPowerDrainMaMs(long paramLong) {
    synchronized (this.mLock) {
      this.mCurOppoDevicePowerDetails.mGpsPowerDrainMaMs = paramLong;
      return;
    } 
  }
  
  public void recordBrightness(int paramInt) {
    synchronized (this.mLock) {
      this.mCurOppoDevicePowerDetails.mBrightness = paramInt;
      return;
    } 
  }
  
  public void recordResumeActivity(String paramString) {
    synchronized (this.mLock) {
      this.mCurOppoDevicePowerDetails.mActivtiyName = paramString;
      return;
    } 
  }
  
  public void recordNetworkActivityBytes(int paramInt, long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLock : Ljava/lang/Object;
    //   4: astore #4
    //   6: aload #4
    //   8: monitorenter
    //   9: iload_1
    //   10: ifeq -> 94
    //   13: iload_1
    //   14: iconst_1
    //   15: if_icmpeq -> 73
    //   18: iload_1
    //   19: iconst_2
    //   20: if_icmpeq -> 52
    //   23: iload_1
    //   24: iconst_3
    //   25: if_icmpeq -> 31
    //   28: goto -> 112
    //   31: aload_0
    //   32: getfield mCurOppoDevicePowerDetails : Lcom/android/internal/os/OppoDevicePowerStats$DevicePowerDetails;
    //   35: astore #5
    //   37: aload #5
    //   39: aload #5
    //   41: getfield mWifiTxTotalBytes : J
    //   44: lload_2
    //   45: ladd
    //   46: putfield mWifiTxTotalBytes : J
    //   49: goto -> 112
    //   52: aload_0
    //   53: getfield mCurOppoDevicePowerDetails : Lcom/android/internal/os/OppoDevicePowerStats$DevicePowerDetails;
    //   56: astore #5
    //   58: aload #5
    //   60: aload #5
    //   62: getfield mWifiRxTotalBytes : J
    //   65: lload_2
    //   66: ladd
    //   67: putfield mWifiRxTotalBytes : J
    //   70: goto -> 112
    //   73: aload_0
    //   74: getfield mCurOppoDevicePowerDetails : Lcom/android/internal/os/OppoDevicePowerStats$DevicePowerDetails;
    //   77: astore #5
    //   79: aload #5
    //   81: aload #5
    //   83: getfield mMobileTxTotalBytes : J
    //   86: lload_2
    //   87: ladd
    //   88: putfield mMobileTxTotalBytes : J
    //   91: goto -> 112
    //   94: aload_0
    //   95: getfield mCurOppoDevicePowerDetails : Lcom/android/internal/os/OppoDevicePowerStats$DevicePowerDetails;
    //   98: astore #5
    //   100: aload #5
    //   102: aload #5
    //   104: getfield mMobileRxTotalBytes : J
    //   107: lload_2
    //   108: ladd
    //   109: putfield mMobileRxTotalBytes : J
    //   112: aload #4
    //   114: monitorexit
    //   115: return
    //   116: astore #5
    //   118: aload #4
    //   120: monitorexit
    //   121: aload #5
    //   123: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #240	-> 0
    //   #244	-> 9
    //   #255	-> 31
    //   #256	-> 49
    //   #252	-> 52
    //   #253	-> 70
    //   #249	-> 73
    //   #250	-> 91
    //   #246	-> 94
    //   #247	-> 112
    //   #260	-> 112
    //   #261	-> 115
    //   #260	-> 116
    // Exception table:
    //   from	to	target	type
    //   31	49	116	finally
    //   52	70	116	finally
    //   73	91	116	finally
    //   94	112	116	finally
    //   112	115	116	finally
    //   118	121	116	finally
  }
  
  public void updateMobileRadioState(ModemActivityInfo paramModemActivityInfo) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLock : Ljava/lang/Object;
    //   4: astore_2
    //   5: aload_2
    //   6: monitorenter
    //   7: aload_1
    //   8: ifnonnull -> 14
    //   11: aload_2
    //   12: monitorexit
    //   13: return
    //   14: iconst_5
    //   15: newarray int
    //   17: astore_3
    //   18: iconst_0
    //   19: istore #4
    //   21: iload #4
    //   23: iconst_5
    //   24: if_icmpge -> 54
    //   27: aload_3
    //   28: iload #4
    //   30: aload_1
    //   31: invokevirtual getTransmitPowerInfo : ()Ljava/util/List;
    //   34: iload #4
    //   36: invokeinterface get : (I)Ljava/lang/Object;
    //   41: checkcast android/telephony/ModemActivityInfo$TransmitPower
    //   44: invokevirtual getTimeInMillis : ()I
    //   47: iastore
    //   48: iinc #4, 1
    //   51: goto -> 21
    //   54: aload_0
    //   55: getfield mCurOppoDevicePowerDetails : Lcom/android/internal/os/OppoDevicePowerStats$DevicePowerDetails;
    //   58: getfield mTxTimeMs : [I
    //   61: arraylength
    //   62: istore #5
    //   64: aload_3
    //   65: arraylength
    //   66: istore #4
    //   68: iload #5
    //   70: iload #4
    //   72: if_icmpne -> 112
    //   75: iconst_0
    //   76: istore #4
    //   78: iload #4
    //   80: iload #5
    //   82: if_icmpge -> 112
    //   85: aload_0
    //   86: getfield mCurOppoDevicePowerDetails : Lcom/android/internal/os/OppoDevicePowerStats$DevicePowerDetails;
    //   89: getfield mTxTimeMs : [I
    //   92: astore_1
    //   93: aload_1
    //   94: iload #4
    //   96: aload_1
    //   97: iload #4
    //   99: iaload
    //   100: aload_3
    //   101: iload #4
    //   103: iaload
    //   104: iadd
    //   105: iastore
    //   106: iinc #4, 1
    //   109: goto -> 78
    //   112: aload_2
    //   113: monitorexit
    //   114: return
    //   115: astore_1
    //   116: aload_2
    //   117: monitorexit
    //   118: aload_1
    //   119: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #266	-> 0
    //   #267	-> 7
    //   #268	-> 11
    //   #273	-> 14
    //   #274	-> 18
    //   #275	-> 27
    //   #274	-> 48
    //   #277	-> 54
    //   #278	-> 64
    //   #280	-> 68
    //   #281	-> 75
    //   #282	-> 85
    //   #281	-> 106
    //   #288	-> 112
    //   #289	-> 114
    //   #288	-> 115
    // Exception table:
    //   from	to	target	type
    //   11	13	115	finally
    //   14	18	115	finally
    //   27	48	115	finally
    //   54	64	115	finally
    //   64	68	115	finally
    //   85	93	115	finally
    //   112	114	115	finally
    //   116	118	115	finally
  }
  
  class PowerDetailsConstants extends ContentObserver {
    public static final String KEY_DISPLAY_REF_MODE = "coloros_screen_refresh_rate";
    
    public static final String KEY_VOL_MUSIC_SPK = "volume_music_speaker";
    
    public static final String TAG = "OppoDevicePowerStats";
    
    private Context mContext;
    
    private ContentResolver mResolver;
    
    final OppoDevicePowerStats this$0;
    
    public PowerDetailsConstants(Handler param1Handler) {
      super(param1Handler);
    }
    
    public void onChange(boolean param1Boolean, Uri param1Uri) {
      updateConstants();
    }
    
    public void startObserving(Context param1Context, ContentResolver param1ContentResolver) {
      this.mContext = param1Context;
      this.mResolver = param1ContentResolver;
      param1ContentResolver.registerContentObserver(Settings.Secure.getUriFor("coloros_screen_refresh_rate"), false, this);
      this.mResolver.registerContentObserver(Settings.System.getUriFor("volume_music_speaker"), false, this);
      updateConstants();
    }
    
    private void updateConstants() {
      Context context = this.mContext;
      if (context == null) {
        Slog.d("OppoDevicePowerStats", "Context is null");
        return;
      } 
      OppoDevicePowerStats.access$002(OppoDevicePowerStats.this, Settings.Secure.getInt(context.getContentResolver(), "coloros_screen_refresh_rate", 0));
      OppoDevicePowerStats.access$102(OppoDevicePowerStats.this, Settings.System.getInt(this.mContext.getContentResolver(), "volume_music_speaker", -1));
      OppoDevicePowerStats.this.mCurOppoDevicePowerDetails.mRefreshRateSetting = OppoDevicePowerStats.this.mScreenRefreshMode;
      OppoDevicePowerStats.this.mCurOppoDevicePowerDetails.mVolumeMusicSpeaker = OppoDevicePowerStats.this.mVolumeMusicSpeaker;
    }
  }
  
  class PowerDetailsReceiver extends BroadcastReceiver {
    public static final String ACTION_MDPWR_REPORT_TO_BATTERY_STATES = "oppo.intent.action.MDPWR_REPORT_TO_BATTERY_STATES";
    
    public static final String ACTION_POWERSTATS_FORECE_UPDATE = "oppo.intent.action.powerstats.FORECE_UPDATE";
    
    public static final String ACTION_SMART5G_KEY_INFO = "oplus.intent.action.SMART5G_KEYINFO";
    
    public static final String TAG = "OppoDevicePowerStats";
    
    private Context mContext;
    
    private boolean mRegistered;
    
    final OppoDevicePowerStats this$0;
    
    public PowerDetailsReceiver(Context param1Context) {
      this.mContext = param1Context;
    }
    
    public void onReceive(Context param1Context, Intent param1Intent) {
      StringBuilder stringBuilder1;
      String str = param1Intent.getAction();
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("on receive ");
      stringBuilder2.append(str);
      Slog.d("OppoDevicePowerStats", stringBuilder2.toString());
      if ("android.net.wifi.WIFI_AP_STATE_CHANGED".equals(str)) {
        int i = param1Intent.getIntExtra("wifi_state", 11);
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("wifi ap state = ");
        stringBuilder1.append(i);
        Slog.d("OppoDevicePowerStats", stringBuilder1.toString());
        WifiManager wifiManager = (WifiManager)param1Context.getSystemService("wifi");
        if (i == 13) {
          OppoDevicePowerStats.access$302(OppoDevicePowerStats.this, true);
        } else {
          OppoDevicePowerStats.access$302(OppoDevicePowerStats.this, false);
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("wifi ap enabled ");
        stringBuilder.append(OppoDevicePowerStats.this.mWifiApStateEnabled);
        Slog.d("OppoDevicePowerStats", stringBuilder.toString());
      } else if ("android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED".equals(str)) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        int i = bluetoothAdapter.getProfileConnectionState(1);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("BT headset connected state ");
        stringBuilder.append(i);
        Slog.d("OppoDevicePowerStats", stringBuilder.toString());
        if (2 == i) {
          OppoDevicePowerStats.access$402(OppoDevicePowerStats.this, true);
        } else {
          OppoDevicePowerStats.access$402(OppoDevicePowerStats.this, false);
        } 
      } else if ("android.intent.action.HEADSET_PLUG".equals(str)) {
        if (stringBuilder1.hasExtra("state")) {
          int i = stringBuilder1.getIntExtra("state", 1);
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("wired headset connected ");
          stringBuilder.append(i);
          Slog.d("OppoDevicePowerStats", stringBuilder.toString());
          OppoDevicePowerStats.access$502(OppoDevicePowerStats.this, true);
        } else {
          OppoDevicePowerStats.access$502(OppoDevicePowerStats.this, false);
        } 
      } else {
        String str1;
        if ("oppo.intent.action.MDPWR_REPORT_TO_BATTERY_STATES".equals(str)) {
          str1 = stringBuilder1.getStringExtra("ModemActivityInfo");
          if (str1 != null && !str1.equals(OppoDevicePowerStats.this.mCurModemInfoSummary)) {
            OppoDevicePowerStats oppoDevicePowerStats = OppoDevicePowerStats.this;
            OppoDevicePowerStats.access$702(oppoDevicePowerStats, oppoDevicePowerStats.mCurModemInfoSummary);
            OppoDevicePowerStats.access$602(OppoDevicePowerStats.this, str1);
          } 
        } else if ("oplus.intent.action.SMART5G_KEYINFO".equals(str)) {
          OppoDevicePowerStats.access$802(OppoDevicePowerStats.this, SmartEndcStatus.creatEndcStatusFormIntent((Intent)str1));
          String str2 = OppoDevicePowerStats.this.mCurSmartEndcStatus.toStringLite();
          if (str2 != null && !OppoDevicePowerStats.this.mCurEndcInfoSummary.equals(OppoDevicePowerStats.this.mLastEndcInfoSummary)) {
            OppoDevicePowerStats oppoDevicePowerStats = OppoDevicePowerStats.this;
            OppoDevicePowerStats.access$902(oppoDevicePowerStats, oppoDevicePowerStats.mCurEndcInfoSummary);
            OppoDevicePowerStats.access$1002(OppoDevicePowerStats.this, str2);
          } 
        } else {
          "oppo.intent.action.powerstats.FORECE_UPDATE".equals(str);
        } 
      } 
      if (OppoDevicePowerStats.this.mBTHeadsetConnected) {
        OppoDevicePowerStats.this.mCurOppoDevicePowerDetails.mHeadsetConnectedType = 2;
      } else if (OppoDevicePowerStats.this.mWiredHeadsetConnected) {
        OppoDevicePowerStats.this.mCurOppoDevicePowerDetails.mHeadsetConnectedType = 1;
      } else {
        OppoDevicePowerStats.this.mCurOppoDevicePowerDetails.mHeadsetConnectedType = 0;
      } 
      OppoDevicePowerStats.this.mCurOppoDevicePowerDetails.mVolumeMusicSpeaker = OppoDevicePowerStats.this.mVolumeMusicSpeaker;
      OppoDevicePowerStats.this.mCurOppoDevicePowerDetails.mWifiApEnabled = OppoDevicePowerStats.this.mWifiApStateEnabled;
      OppoDevicePowerStats.this.mCurOppoDevicePowerDetails.mModemActivityInfo = OppoDevicePowerStats.this.mCurModemInfoSummary;
      OppoDevicePowerStats.this.mCurOppoDevicePowerDetails.mEndcInfoSummary = OppoDevicePowerStats.this.mCurEndcInfoSummary;
    }
    
    public void register() {
      if (!this.mRegistered && this.mContext != null) {
        Slog.d("OppoDevicePowerStats", "registerReceiver");
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.wifi.WIFI_AP_STATE_CHANGED");
        intentFilter.addAction("android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED");
        intentFilter.addAction("android.intent.action.HEADSET_PLUG");
        intentFilter.addAction("oppo.intent.action.MDPWR_REPORT_TO_BATTERY_STATES");
        intentFilter.addAction("oppo.intent.action.powerstats.FORECE_UPDATE");
        intentFilter.addAction("oplus.intent.action.SMART5G_KEYINFO");
        this.mContext.registerReceiver(this, intentFilter, "com.oppo.nhs.permission.NHS_MD_ACI_SAFE_PERMISSION", null);
        this.mRegistered = true;
      } 
    }
    
    public void unregister() {
      if (this.mRegistered) {
        Context context = this.mContext;
        if (context != null) {
          context.unregisterReceiver(this);
          this.mRegistered = false;
        } 
      } 
    }
  }
  
  public String getDevicePowerStatsDeltaString() {
    synchronized (this.mLock) {
      if (this.mDevicePowerDetailsDelta != null) {
        String str = this.mDevicePowerDetailsDelta.toString();
        dumpHistory();
        return str;
      } 
      return "";
    } 
  }
  
  private SmartEndcStatus creatEndcStatusFormIntent(Intent paramIntent) {
    boolean bool = paramIntent.getBooleanExtra("Switch", false);
    long l1 = paramIntent.getLongExtra("EndcDura", 0L);
    long l2 = paramIntent.getLongExtra("NoEndcDura", 0L);
    long l3 = paramIntent.getLongExtra("EnEndcTime", 0L);
    long l4 = paramIntent.getLongExtra("DisEndcTime", 0L);
    int i = paramIntent.getIntExtra("LteSpeedCntL0", 0);
    int j = paramIntent.getIntExtra("LteSpeedCntL1", 0);
    int k = paramIntent.getIntExtra("LteSpeedCntL2", 0);
    int m = paramIntent.getIntExtra("LteSpeedCntL3", 0);
    int n = paramIntent.getIntExtra("LteSpeedCntL4", 0);
    int i1 = paramIntent.getIntExtra("EnEndcSpeedHighCnt", 0);
    int i2 = paramIntent.getIntExtra("EnEndcSwitchOffCnt", 0);
    int i3 = paramIntent.getIntExtra("EnEndcLtePoorCnt", 0);
    int i4 = paramIntent.getIntExtra("EnEndcLteJamCnt", 0);
    return new SmartEndcStatus(bool, l1, l2, l3, l4, i, j, k, m, n, i1, i2, i3, i4, paramIntent.getIntExtra("EnEndcProhibitCnt", 0));
  }
  
  private int getSurfaceFlingerRefreshCounts() {
    int i = 0;
    try {
      IBinder iBinder = ServiceManager.getService("SurfaceFlinger");
      if (iBinder != null) {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        parcel1.writeInterfaceToken("android.ui.ISurfaceComposer");
        iBinder.transact(1013, parcel1, parcel2, 0);
        i = parcel2.readInt();
        parcel1.recycle();
        parcel2.recycle();
      } 
    } catch (RemoteException remoteException) {
      Slog.e("OppoDevicePowerStats", "get RefreshCounts failed");
      i = 0;
    } 
    return i;
  }
  
  public void onBatteryStepDrained() {
    updateDevicePowerDetails(true);
  }
  
  public void onChagrgeStepChanged() {}
  
  private void updateDevicePowerDetails(boolean paramBoolean) {
    Slog.d("OppoDevicePowerStats", "updateDevicePowerDetails");
    synchronized (this.mLock) {
      if (this.mCurOppoDevicePowerDetails == null)
        return; 
      this.mCurOppoDevicePowerDetails.mCurrentTime = System.currentTimeMillis();
      this.mCurOppoDevicePowerDetails.mRefreshCounts = getSurfaceFlingerRefreshCounts();
      if (paramBoolean) {
        this.mOppoRpmManager.onBatteryDrained();
        this.mCurOppoDevicePowerDetails.mLastStepRpmStatsSummary = this.mOppoRpmManager.getLastStepRpmSuspendRatioSummary();
        this.mLastLastOppoDevicePowerDetails.setTo(this.mLastOppoDevicePowerDetails);
        this.mLastOppoDevicePowerDetails.setTo(this.mCurOppoDevicePowerDetails);
        computeDelta(this.mLastOppoDevicePowerDetails, this.mLastLastOppoDevicePowerDetails, this.mDevicePowerDetailsDelta);
      } 
      return;
    } 
  }
  
  private void computeDelta(DevicePowerDetails paramDevicePowerDetails1, DevicePowerDetails paramDevicePowerDetails2, DevicePowerDetails paramDevicePowerDetails3) {
    double d;
    this.mLastStepBatteryUpTime = this.mCurStepBatteryUpTime;
    this.mCurStepBatteryUpTime = SystemClock.uptimeMillis();
    paramDevicePowerDetails3.setTo(paramDevicePowerDetails1);
    int[] arrayOfInt2 = new int[5];
    int[] arrayOfInt3 = this.mLastOppoDevicePowerDetails.mTxTimeMs;
    int[] arrayOfInt4 = new int[5];
    long l1 = (this.mCurStepBatteryUpTime - this.mLastStepBatteryUpTime) / 1000L;
    Arrays.fill(arrayOfInt2, 0);
    Arrays.fill(arrayOfInt3, 0);
    Arrays.fill(arrayOfInt4, 0);
    long l2 = paramDevicePowerDetails1.mRefreshCounts;
    long l3 = paramDevicePowerDetails2.mRefreshCounts;
    if (l1 > 0L) {
      d = ((l2 - l3) / l1);
    } else {
      d = 0.0D;
    } 
    paramDevicePowerDetails3.mRefreshesPerSecond = d;
    int[] arrayOfInt1 = paramDevicePowerDetails1.mTxTimeMs;
    arrayOfInt3 = paramDevicePowerDetails2.mTxTimeMs;
    if (arrayOfInt1 != null && arrayOfInt3 != null) {
      int i = arrayOfInt1.length;
      int j = arrayOfInt3.length;
      if (i == j)
        for (j = 0; j < i; j++)
          arrayOfInt4[j] = arrayOfInt1[j] - arrayOfInt3[j];  
    } 
    paramDevicePowerDetails3.mDetaTxTimeMs = arrayOfInt4;
    paramDevicePowerDetails3.mMobileRxTotalBytes = paramDevicePowerDetails2.mMobileRxTotalBytes;
    paramDevicePowerDetails3.mMobileTxTotalBytes = paramDevicePowerDetails2.mMobileTxTotalBytes;
    paramDevicePowerDetails3.mWifiRxTotalBytes = paramDevicePowerDetails2.mWifiRxTotalBytes;
    paramDevicePowerDetails3.mWifiTxTotalBytes = paramDevicePowerDetails2.mWifiTxTotalBytes;
    paramDevicePowerDetails3.mMobilePowerDrainMaMs = paramDevicePowerDetails2.mMobilePowerDrainMaMs;
    paramDevicePowerDetails3.mWifiPowerDrainMaMs = paramDevicePowerDetails2.mWifiPowerDrainMaMs;
    paramDevicePowerDetails3.mGpsPowerDrainMaMs = paramDevicePowerDetails2.mGpsPowerDrainMaMs;
    paramDevicePowerDetails3.mBluetoothPowerDrainMaMs = paramDevicePowerDetails2.mBluetoothPowerDrainMaMs;
  }
  
  class DevicePowerDetails implements Parcelable {
    public String mActivtiyName;
    
    public long mBluetoothPowerDrainMaMs;
    
    public int mBrightness;
    
    public long mCurrentTime;
    
    public int[] mDetaTxTimeMs;
    
    public String mEndcInfoSummary;
    
    public long mGpsPowerDrainMaMs;
    
    public int mHeadsetConnectedType;
    
    public String mLastStepRpmStatsSummary;
    
    public long mMobilePowerDrainMaMs;
    
    public long mMobileRxTotalBytes;
    
    public long mMobileTxTotalBytes;
    
    public String mModemActivityInfo;
    
    public long mRefreshCounts;
    
    public int mRefreshRateSetting;
    
    public double mRefreshesPerSecond;
    
    public int[] mTxTimeMs;
    
    public int mVolumeMusicSpeaker;
    
    public boolean mWifiApEnabled;
    
    public long mWifiPowerDrainMaMs;
    
    public long mWifiRxTotalBytes;
    
    public long mWifiTxTotalBytes;
    
    public DevicePowerDetails() {
      clear();
    }
    
    public DevicePowerDetails(OppoDevicePowerStats this$0) {
      readFromParcel((Parcel)this$0);
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void clear() {
      this.mCurrentTime = 0L;
      this.mRefreshesPerSecond = 0.0D;
      this.mRefreshCounts = 0L;
      this.mRefreshRateSetting = 0;
      this.mBrightness = 0;
      int[] arrayOfInt = new int[5];
      this.mDetaTxTimeMs = new int[5];
      Arrays.fill(arrayOfInt, 0);
      Arrays.fill(this.mDetaTxTimeMs, 0);
      this.mMobileRxTotalBytes = 0L;
      this.mMobileTxTotalBytes = 0L;
      this.mWifiRxTotalBytes = 0L;
      this.mWifiTxTotalBytes = 0L;
      this.mMobilePowerDrainMaMs = 0L;
      this.mWifiPowerDrainMaMs = 0L;
      this.mGpsPowerDrainMaMs = 0L;
      this.mBluetoothPowerDrainMaMs = 0L;
      this.mVolumeMusicSpeaker = 0;
      this.mHeadsetConnectedType = 0;
      this.mModemActivityInfo = "0";
      this.mEndcInfoSummary = "0";
      this.mLastStepRpmStatsSummary = "0";
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      writeToParcel(param1Parcel);
    }
    
    public void writeToParcel(Parcel param1Parcel) {
      param1Parcel.writeLong(this.mCurrentTime);
      param1Parcel.writeLong(this.mRefreshCounts);
      param1Parcel.writeInt(this.mRefreshRateSetting);
      param1Parcel.writeInt(this.mBrightness);
      param1Parcel.writeString(this.mActivtiyName);
      param1Parcel.writeDouble(this.mRefreshesPerSecond);
      int i = this.mDetaTxTimeMs.length;
      for (byte b = 0; b < i; b++)
        param1Parcel.writeLong(this.mDetaTxTimeMs[b]); 
      param1Parcel.writeLong(this.mMobileRxTotalBytes);
      param1Parcel.writeLong(this.mMobileTxTotalBytes);
      param1Parcel.writeLong(this.mWifiRxTotalBytes);
      param1Parcel.writeLong(this.mWifiTxTotalBytes);
      param1Parcel.writeLong(this.mMobilePowerDrainMaMs);
      param1Parcel.writeLong(this.mWifiPowerDrainMaMs);
      param1Parcel.writeLong(this.mGpsPowerDrainMaMs);
      param1Parcel.writeLong(this.mBluetoothPowerDrainMaMs);
      param1Parcel.writeBoolean(this.mWifiApEnabled);
      param1Parcel.writeString(this.mLastStepRpmStatsSummary);
      param1Parcel.writeString(this.mModemActivityInfo);
      param1Parcel.writeString(this.mEndcInfoSummary);
    }
    
    public void readFromParcel(Parcel param1Parcel) {
      try {
        this.mCurrentTime = param1Parcel.readLong();
        this.mRefreshCounts = param1Parcel.readLong();
        this.mRefreshRateSetting = param1Parcel.readInt();
        this.mBrightness = param1Parcel.readInt();
        this.mActivtiyName = param1Parcel.readString();
        this.mRefreshesPerSecond = param1Parcel.readDouble();
        int i = this.mDetaTxTimeMs.length;
        for (byte b = 0; b < i; b++)
          this.mDetaTxTimeMs[b] = param1Parcel.readInt(); 
        this.mMobileRxTotalBytes = param1Parcel.readLong();
        this.mMobileTxTotalBytes = param1Parcel.readLong();
        this.mWifiRxTotalBytes = param1Parcel.readLong();
        this.mWifiTxTotalBytes = param1Parcel.readLong();
        this.mMobilePowerDrainMaMs = param1Parcel.readLong();
        this.mWifiPowerDrainMaMs = param1Parcel.readLong();
        this.mGpsPowerDrainMaMs = param1Parcel.readLong();
        this.mBluetoothPowerDrainMaMs = param1Parcel.readLong();
        this.mWifiApEnabled = param1Parcel.readBoolean();
        this.mLastStepRpmStatsSummary = param1Parcel.readString();
        this.mModemActivityInfo = param1Parcel.readString();
        this.mEndcInfoSummary = param1Parcel.readString();
      } catch (Exception exception) {
        Slog.e("read DevicePowerDetails", "Error reading fromParcel ", exception);
      } 
    }
    
    public void setTo(DevicePowerDetails param1DevicePowerDetails) {
      this.mCurrentTime = param1DevicePowerDetails.mCurrentTime;
      this.mRefreshCounts = param1DevicePowerDetails.mRefreshCounts;
      this.mRefreshRateSetting = param1DevicePowerDetails.mRefreshRateSetting;
      this.mBrightness = param1DevicePowerDetails.mBrightness;
      this.mActivtiyName = param1DevicePowerDetails.mActivtiyName;
      this.mRefreshesPerSecond = param1DevicePowerDetails.mRefreshesPerSecond;
      this.mDetaTxTimeMs = param1DevicePowerDetails.mDetaTxTimeMs;
      this.mMobileRxTotalBytes = param1DevicePowerDetails.mMobileRxTotalBytes;
      this.mMobileTxTotalBytes = param1DevicePowerDetails.mMobileTxTotalBytes;
      this.mWifiRxTotalBytes = param1DevicePowerDetails.mWifiRxTotalBytes;
      this.mWifiTxTotalBytes = param1DevicePowerDetails.mWifiTxTotalBytes;
      this.mMobilePowerDrainMaMs = param1DevicePowerDetails.mMobilePowerDrainMaMs;
      this.mWifiPowerDrainMaMs = param1DevicePowerDetails.mWifiPowerDrainMaMs;
      this.mGpsPowerDrainMaMs = param1DevicePowerDetails.mGpsPowerDrainMaMs;
      this.mBluetoothPowerDrainMaMs = param1DevicePowerDetails.mBluetoothPowerDrainMaMs;
      this.mWifiApEnabled = param1DevicePowerDetails.mWifiApEnabled;
      this.mLastStepRpmStatsSummary = param1DevicePowerDetails.mLastStepRpmStatsSummary;
      this.mModemActivityInfo = param1DevicePowerDetails.mModemActivityInfo;
      this.mEndcInfoSummary = param1DevicePowerDetails.mEndcInfoSummary;
    }
    
    public String toString() {
      StringBuilder stringBuilder1 = new StringBuilder();
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("PowerDetails: time=");
      stringBuilder2.append(DateFormat.format("yyyy-MM-dd-HH-mm-ss", this.mCurrentTime).toString());
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder1.append(", top=");
      stringBuilder1.append(this.mActivtiyName);
      stringBuilder1.append(", ref_counts=");
      stringBuilder1.append(this.mRefreshCounts);
      stringBuilder1.append(", ref_mode=");
      stringBuilder1.append(this.mRefreshRateSetting);
      stringBuilder1.append(", brightness=");
      stringBuilder1.append(this.mBrightness);
      stringBuilder1.append(", refs_per_sec=");
      stringBuilder1.append(this.mRefreshesPerSecond);
      stringBuilder1.append(", modem_txTimeMs=");
      stringBuilder1.append(Arrays.toString(this.mDetaTxTimeMs));
      stringBuilder1.append(", traffic=");
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append("[");
      stringBuilder2.append(this.mMobileRxTotalBytes);
      stringBuilder2.append(",");
      stringBuilder2.append(this.mMobileTxTotalBytes);
      stringBuilder2.append(",");
      stringBuilder2.append(this.mWifiRxTotalBytes);
      stringBuilder2.append(",");
      stringBuilder2.append(this.mWifiTxTotalBytes);
      stringBuilder2.append("]");
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder1.append(", ctr_drain=");
      stringBuilder1.append("[");
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append(BatteryStatsHelper.makemAh(this.mMobilePowerDrainMaMs / 3600000.0D));
      stringBuilder2.append(",");
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append(BatteryStatsHelper.makemAh(this.mWifiPowerDrainMaMs / 3600000.0D));
      stringBuilder2.append(",");
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append(BatteryStatsHelper.makemAh(this.mGpsPowerDrainMaMs / 3600000.0D));
      stringBuilder2.append(",");
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append(BatteryStatsHelper.makemAh(this.mBluetoothPowerDrainMaMs / 3600000.0D));
      stringBuilder2.append("]");
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder1.append(", rpmstat=");
      stringBuilder1.append(this.mLastStepRpmStatsSummary);
      stringBuilder1.append(", sap=");
      stringBuilder1.append(this.mWifiApEnabled);
      stringBuilder1.append(", audio=[");
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append("spk:");
      stringBuilder2.append(this.mVolumeMusicSpeaker);
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append(", headset:");
      stringBuilder2.append(this.mHeadsetConnectedType);
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder1.append("]");
      stringBuilder1.append("\n");
      stringBuilder1.append(", modem=[");
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append("activity:");
      stringBuilder2.append(this.mModemActivityInfo);
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder1.append("]");
      stringBuilder1.append("\n");
      stringBuilder1.append(", endc=");
      stringBuilder1.append(this.mEndcInfoSummary);
      return stringBuilder1.toString();
    }
    
    public String getJsonString() {
      return null;
    }
  }
}
