package com.android.internal.os;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharsetDecoder;
import java.util.Formatter;
import java.util.Locale;

public abstract class LoggingPrintStream extends PrintStream {
  private final StringBuilder builder = new StringBuilder();
  
  private CharBuffer decodedChars;
  
  private CharsetDecoder decoder;
  
  private ByteBuffer encodedBytes;
  
  private final Formatter formatter;
  
  protected LoggingPrintStream() {
    super(new OutputStream() {
          public void write(int param1Int) throws IOException {
            throw new AssertionError();
          }
        });
    this.formatter = new Formatter(this.builder, null);
  }
  
  public void flush() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iconst_1
    //   4: invokespecial flush : (Z)V
    //   7: aload_0
    //   8: monitorexit
    //   9: return
    //   10: astore_1
    //   11: aload_0
    //   12: monitorexit
    //   13: aload_1
    //   14: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #79	-> 2
    //   #80	-> 7
    //   #78	-> 10
    // Exception table:
    //   from	to	target	type
    //   2	7	10	finally
  }
  
  private void flush(boolean paramBoolean) {
    int i = this.builder.length();
    int j = 0;
    while (j < i) {
      StringBuilder stringBuilder = this.builder;
      int k = stringBuilder.indexOf("\n", j);
      if (k != -1) {
        log(this.builder.substring(j, k));
        j = k + 1;
      } 
    } 
    if (paramBoolean) {
      if (j < i)
        log(this.builder.substring(j)); 
      this.builder.setLength(0);
    } else {
      this.builder.delete(0, j);
    } 
  }
  
  public void write(int paramInt) {
    write(new byte[] { (byte)paramInt }, 0, 1);
  }
  
  public void write(byte[] paramArrayOfbyte) {
    write(paramArrayOfbyte, 0, paramArrayOfbyte.length);
  }
  
  public void write(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield decoder : Ljava/nio/charset/CharsetDecoder;
    //   6: ifnonnull -> 65
    //   9: aload_0
    //   10: bipush #80
    //   12: invokestatic allocate : (I)Ljava/nio/ByteBuffer;
    //   15: putfield encodedBytes : Ljava/nio/ByteBuffer;
    //   18: aload_0
    //   19: bipush #80
    //   21: invokestatic allocate : (I)Ljava/nio/CharBuffer;
    //   24: putfield decodedChars : Ljava/nio/CharBuffer;
    //   27: invokestatic defaultCharset : ()Ljava/nio/charset/Charset;
    //   30: invokevirtual newDecoder : ()Ljava/nio/charset/CharsetDecoder;
    //   33: astore #4
    //   35: getstatic java/nio/charset/CodingErrorAction.REPLACE : Ljava/nio/charset/CodingErrorAction;
    //   38: astore #5
    //   40: aload #4
    //   42: aload #5
    //   44: invokevirtual onMalformedInput : (Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;
    //   47: astore #5
    //   49: getstatic java/nio/charset/CodingErrorAction.REPLACE : Ljava/nio/charset/CodingErrorAction;
    //   52: astore #4
    //   54: aload_0
    //   55: aload #5
    //   57: aload #4
    //   59: invokevirtual onUnmappableCharacter : (Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;
    //   62: putfield decoder : Ljava/nio/charset/CharsetDecoder;
    //   65: iload_2
    //   66: iload_3
    //   67: iadd
    //   68: istore_3
    //   69: iload_2
    //   70: iload_3
    //   71: if_icmpge -> 179
    //   74: aload_0
    //   75: getfield encodedBytes : Ljava/nio/ByteBuffer;
    //   78: invokevirtual remaining : ()I
    //   81: iload_3
    //   82: iload_2
    //   83: isub
    //   84: invokestatic min : (II)I
    //   87: istore #6
    //   89: aload_0
    //   90: getfield encodedBytes : Ljava/nio/ByteBuffer;
    //   93: aload_1
    //   94: iload_2
    //   95: iload #6
    //   97: invokevirtual put : ([BII)Ljava/nio/ByteBuffer;
    //   100: pop
    //   101: iload_2
    //   102: iload #6
    //   104: iadd
    //   105: istore_2
    //   106: aload_0
    //   107: getfield encodedBytes : Ljava/nio/ByteBuffer;
    //   110: invokevirtual flip : ()Ljava/nio/Buffer;
    //   113: pop
    //   114: aload_0
    //   115: getfield decoder : Ljava/nio/charset/CharsetDecoder;
    //   118: aload_0
    //   119: getfield encodedBytes : Ljava/nio/ByteBuffer;
    //   122: aload_0
    //   123: getfield decodedChars : Ljava/nio/CharBuffer;
    //   126: iconst_0
    //   127: invokevirtual decode : (Ljava/nio/ByteBuffer;Ljava/nio/CharBuffer;Z)Ljava/nio/charset/CoderResult;
    //   130: astore #4
    //   132: aload_0
    //   133: getfield decodedChars : Ljava/nio/CharBuffer;
    //   136: invokevirtual flip : ()Ljava/nio/Buffer;
    //   139: pop
    //   140: aload_0
    //   141: getfield builder : Ljava/lang/StringBuilder;
    //   144: aload_0
    //   145: getfield decodedChars : Ljava/nio/CharBuffer;
    //   148: invokevirtual append : (Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;
    //   151: pop
    //   152: aload_0
    //   153: getfield decodedChars : Ljava/nio/CharBuffer;
    //   156: invokevirtual clear : ()Ljava/nio/Buffer;
    //   159: pop
    //   160: aload #4
    //   162: invokevirtual isOverflow : ()Z
    //   165: ifne -> 114
    //   168: aload_0
    //   169: getfield encodedBytes : Ljava/nio/ByteBuffer;
    //   172: invokevirtual compact : ()Ljava/nio/ByteBuffer;
    //   175: pop
    //   176: goto -> 69
    //   179: aload_0
    //   180: iconst_0
    //   181: invokespecial flush : (Z)V
    //   184: aload_0
    //   185: monitorexit
    //   186: return
    //   187: astore_1
    //   188: aload_0
    //   189: monitorexit
    //   190: aload_1
    //   191: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #124	-> 2
    //   #125	-> 9
    //   #126	-> 18
    //   #127	-> 27
    //   #128	-> 40
    //   #129	-> 54
    //   #132	-> 65
    //   #133	-> 69
    //   #136	-> 74
    //   #137	-> 89
    //   #138	-> 101
    //   #140	-> 106
    //   #144	-> 114
    //   #147	-> 132
    //   #148	-> 140
    //   #149	-> 152
    //   #150	-> 160
    //   #151	-> 168
    //   #152	-> 176
    //   #153	-> 179
    //   #154	-> 184
    //   #123	-> 187
    // Exception table:
    //   from	to	target	type
    //   2	9	187	finally
    //   9	18	187	finally
    //   18	27	187	finally
    //   27	40	187	finally
    //   40	54	187	finally
    //   54	65	187	finally
    //   74	89	187	finally
    //   89	101	187	finally
    //   106	114	187	finally
    //   114	132	187	finally
    //   132	140	187	finally
    //   140	152	187	finally
    //   152	160	187	finally
    //   160	168	187	finally
    //   168	176	187	finally
    //   179	184	187	finally
  }
  
  public boolean checkError() {
    return false;
  }
  
  protected void setError() {}
  
  public void close() {}
  
  public PrintStream format(String paramString, Object... paramVarArgs) {
    return format(Locale.getDefault(), paramString, paramVarArgs);
  }
  
  public PrintStream printf(String paramString, Object... paramVarArgs) {
    return format(paramString, paramVarArgs);
  }
  
  public PrintStream printf(Locale paramLocale, String paramString, Object... paramVarArgs) {
    return format(paramLocale, paramString, paramVarArgs);
  }
  
  public PrintStream format(Locale paramLocale, String paramString, Object... paramVarArgs) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_2
    //   3: ifnull -> 30
    //   6: aload_0
    //   7: getfield formatter : Ljava/util/Formatter;
    //   10: aload_1
    //   11: aload_2
    //   12: aload_3
    //   13: invokevirtual format : (Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;
    //   16: pop
    //   17: aload_0
    //   18: iconst_0
    //   19: invokespecial flush : (Z)V
    //   22: aload_0
    //   23: monitorexit
    //   24: aload_0
    //   25: areturn
    //   26: astore_1
    //   27: goto -> 42
    //   30: new java/lang/NullPointerException
    //   33: astore_1
    //   34: aload_1
    //   35: ldc 'format'
    //   37: invokespecial <init> : (Ljava/lang/String;)V
    //   40: aload_1
    //   41: athrow
    //   42: aload_0
    //   43: monitorexit
    //   44: aload_1
    //   45: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #190	-> 2
    //   #194	-> 6
    //   #195	-> 17
    //   #196	-> 22
    //   #189	-> 26
    //   #191	-> 30
    //   #189	-> 42
    // Exception table:
    //   from	to	target	type
    //   6	17	26	finally
    //   17	22	26	finally
    //   30	42	26	finally
  }
  
  public void print(char[] paramArrayOfchar) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield builder : Ljava/lang/StringBuilder;
    //   6: aload_1
    //   7: invokevirtual append : ([C)Ljava/lang/StringBuilder;
    //   10: pop
    //   11: aload_0
    //   12: iconst_0
    //   13: invokespecial flush : (Z)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_1
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_1
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #201	-> 2
    //   #202	-> 11
    //   #203	-> 16
    //   #200	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	11	19	finally
    //   11	16	19	finally
  }
  
  public void print(char paramChar) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield builder : Ljava/lang/StringBuilder;
    //   6: iload_1
    //   7: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   10: pop
    //   11: iload_1
    //   12: bipush #10
    //   14: if_icmpne -> 22
    //   17: aload_0
    //   18: iconst_0
    //   19: invokespecial flush : (Z)V
    //   22: aload_0
    //   23: monitorexit
    //   24: return
    //   25: astore_2
    //   26: aload_0
    //   27: monitorexit
    //   28: aload_2
    //   29: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #207	-> 2
    //   #208	-> 11
    //   #209	-> 17
    //   #211	-> 22
    //   #206	-> 25
    // Exception table:
    //   from	to	target	type
    //   2	11	25	finally
    //   17	22	25	finally
  }
  
  public void print(double paramDouble) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield builder : Ljava/lang/StringBuilder;
    //   6: dload_1
    //   7: invokevirtual append : (D)Ljava/lang/StringBuilder;
    //   10: pop
    //   11: aload_0
    //   12: monitorexit
    //   13: return
    //   14: astore_3
    //   15: aload_0
    //   16: monitorexit
    //   17: aload_3
    //   18: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #215	-> 2
    //   #216	-> 11
    //   #214	-> 14
    // Exception table:
    //   from	to	target	type
    //   2	11	14	finally
  }
  
  public void print(float paramFloat) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield builder : Ljava/lang/StringBuilder;
    //   6: fload_1
    //   7: invokevirtual append : (F)Ljava/lang/StringBuilder;
    //   10: pop
    //   11: aload_0
    //   12: monitorexit
    //   13: return
    //   14: astore_2
    //   15: aload_0
    //   16: monitorexit
    //   17: aload_2
    //   18: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #220	-> 2
    //   #221	-> 11
    //   #219	-> 14
    // Exception table:
    //   from	to	target	type
    //   2	11	14	finally
  }
  
  public void print(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield builder : Ljava/lang/StringBuilder;
    //   6: iload_1
    //   7: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   10: pop
    //   11: aload_0
    //   12: monitorexit
    //   13: return
    //   14: astore_2
    //   15: aload_0
    //   16: monitorexit
    //   17: aload_2
    //   18: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #225	-> 2
    //   #226	-> 11
    //   #224	-> 14
    // Exception table:
    //   from	to	target	type
    //   2	11	14	finally
  }
  
  public void print(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield builder : Ljava/lang/StringBuilder;
    //   6: lload_1
    //   7: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   10: pop
    //   11: aload_0
    //   12: monitorexit
    //   13: return
    //   14: astore_3
    //   15: aload_0
    //   16: monitorexit
    //   17: aload_3
    //   18: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #230	-> 2
    //   #231	-> 11
    //   #229	-> 14
    // Exception table:
    //   from	to	target	type
    //   2	11	14	finally
  }
  
  public void print(Object paramObject) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield builder : Ljava/lang/StringBuilder;
    //   6: aload_1
    //   7: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   10: pop
    //   11: aload_0
    //   12: iconst_0
    //   13: invokespecial flush : (Z)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_1
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_1
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #235	-> 2
    //   #236	-> 11
    //   #237	-> 16
    //   #234	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	11	19	finally
    //   11	16	19	finally
  }
  
  public void print(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield builder : Ljava/lang/StringBuilder;
    //   6: aload_1
    //   7: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10: pop
    //   11: aload_0
    //   12: iconst_0
    //   13: invokespecial flush : (Z)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_1
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_1
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #241	-> 2
    //   #242	-> 11
    //   #243	-> 16
    //   #240	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	11	19	finally
    //   11	16	19	finally
  }
  
  public void print(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield builder : Ljava/lang/StringBuilder;
    //   6: iload_1
    //   7: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   10: pop
    //   11: aload_0
    //   12: monitorexit
    //   13: return
    //   14: astore_2
    //   15: aload_0
    //   16: monitorexit
    //   17: aload_2
    //   18: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #247	-> 2
    //   #248	-> 11
    //   #246	-> 14
    // Exception table:
    //   from	to	target	type
    //   2	11	14	finally
  }
  
  public void println() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iconst_1
    //   4: invokespecial flush : (Z)V
    //   7: aload_0
    //   8: monitorexit
    //   9: return
    //   10: astore_1
    //   11: aload_0
    //   12: monitorexit
    //   13: aload_1
    //   14: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #252	-> 2
    //   #253	-> 7
    //   #251	-> 10
    // Exception table:
    //   from	to	target	type
    //   2	7	10	finally
  }
  
  public void println(char[] paramArrayOfchar) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield builder : Ljava/lang/StringBuilder;
    //   6: aload_1
    //   7: invokevirtual append : ([C)Ljava/lang/StringBuilder;
    //   10: pop
    //   11: aload_0
    //   12: iconst_1
    //   13: invokespecial flush : (Z)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_1
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_1
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #257	-> 2
    //   #258	-> 11
    //   #259	-> 16
    //   #256	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	11	19	finally
    //   11	16	19	finally
  }
  
  public void println(char paramChar) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield builder : Ljava/lang/StringBuilder;
    //   6: iload_1
    //   7: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   10: pop
    //   11: aload_0
    //   12: iconst_1
    //   13: invokespecial flush : (Z)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_2
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_2
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #263	-> 2
    //   #264	-> 11
    //   #265	-> 16
    //   #262	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	11	19	finally
    //   11	16	19	finally
  }
  
  public void println(double paramDouble) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield builder : Ljava/lang/StringBuilder;
    //   6: dload_1
    //   7: invokevirtual append : (D)Ljava/lang/StringBuilder;
    //   10: pop
    //   11: aload_0
    //   12: iconst_1
    //   13: invokespecial flush : (Z)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_3
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_3
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #269	-> 2
    //   #270	-> 11
    //   #271	-> 16
    //   #268	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	11	19	finally
    //   11	16	19	finally
  }
  
  public void println(float paramFloat) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield builder : Ljava/lang/StringBuilder;
    //   6: fload_1
    //   7: invokevirtual append : (F)Ljava/lang/StringBuilder;
    //   10: pop
    //   11: aload_0
    //   12: iconst_1
    //   13: invokespecial flush : (Z)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_2
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_2
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #275	-> 2
    //   #276	-> 11
    //   #277	-> 16
    //   #274	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	11	19	finally
    //   11	16	19	finally
  }
  
  public void println(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield builder : Ljava/lang/StringBuilder;
    //   6: iload_1
    //   7: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   10: pop
    //   11: aload_0
    //   12: iconst_1
    //   13: invokespecial flush : (Z)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_2
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_2
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #281	-> 2
    //   #282	-> 11
    //   #283	-> 16
    //   #280	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	11	19	finally
    //   11	16	19	finally
  }
  
  public void println(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield builder : Ljava/lang/StringBuilder;
    //   6: lload_1
    //   7: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   10: pop
    //   11: aload_0
    //   12: iconst_1
    //   13: invokespecial flush : (Z)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_3
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_3
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #287	-> 2
    //   #288	-> 11
    //   #289	-> 16
    //   #286	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	11	19	finally
    //   11	16	19	finally
  }
  
  public void println(Object paramObject) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield builder : Ljava/lang/StringBuilder;
    //   6: aload_1
    //   7: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   10: pop
    //   11: aload_0
    //   12: iconst_1
    //   13: invokespecial flush : (Z)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_1
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_1
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #293	-> 2
    //   #294	-> 11
    //   #295	-> 16
    //   #292	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	11	19	finally
    //   11	16	19	finally
  }
  
  public void println(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield builder : Ljava/lang/StringBuilder;
    //   6: invokevirtual length : ()I
    //   9: ifne -> 79
    //   12: aload_1
    //   13: ifnull -> 79
    //   16: aload_1
    //   17: invokevirtual length : ()I
    //   20: istore_2
    //   21: iconst_0
    //   22: istore_3
    //   23: iload_3
    //   24: iload_2
    //   25: if_icmpge -> 62
    //   28: aload_1
    //   29: bipush #10
    //   31: iload_3
    //   32: invokevirtual indexOf : (II)I
    //   35: istore #4
    //   37: iload #4
    //   39: iconst_m1
    //   40: if_icmpeq -> 62
    //   43: aload_0
    //   44: aload_1
    //   45: iload_3
    //   46: iload #4
    //   48: invokevirtual substring : (II)Ljava/lang/String;
    //   51: invokevirtual log : (Ljava/lang/String;)V
    //   54: iload #4
    //   56: iconst_1
    //   57: iadd
    //   58: istore_3
    //   59: goto -> 23
    //   62: iload_3
    //   63: iload_2
    //   64: if_icmpge -> 76
    //   67: aload_0
    //   68: aload_1
    //   69: iload_3
    //   70: invokevirtual substring : (I)Ljava/lang/String;
    //   73: invokevirtual log : (Ljava/lang/String;)V
    //   76: goto -> 93
    //   79: aload_0
    //   80: getfield builder : Ljava/lang/StringBuilder;
    //   83: aload_1
    //   84: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   87: pop
    //   88: aload_0
    //   89: iconst_1
    //   90: invokespecial flush : (Z)V
    //   93: aload_0
    //   94: monitorexit
    //   95: return
    //   96: astore_1
    //   97: aload_0
    //   98: monitorexit
    //   99: aload_1
    //   100: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #299	-> 2
    //   #301	-> 16
    //   #303	-> 21
    //   #307	-> 23
    //   #308	-> 28
    //   #309	-> 43
    //   #310	-> 54
    //   #313	-> 62
    //   #314	-> 67
    //   #316	-> 76
    //   #317	-> 79
    //   #318	-> 88
    //   #320	-> 93
    //   #298	-> 96
    // Exception table:
    //   from	to	target	type
    //   2	12	96	finally
    //   16	21	96	finally
    //   28	37	96	finally
    //   43	54	96	finally
    //   67	76	96	finally
    //   79	88	96	finally
    //   88	93	96	finally
  }
  
  public void println(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield builder : Ljava/lang/StringBuilder;
    //   6: iload_1
    //   7: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   10: pop
    //   11: aload_0
    //   12: iconst_1
    //   13: invokespecial flush : (Z)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_2
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_2
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #324	-> 2
    //   #325	-> 11
    //   #326	-> 16
    //   #323	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	11	19	finally
    //   11	16	19	finally
  }
  
  public PrintStream append(char paramChar) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iload_1
    //   4: invokevirtual print : (C)V
    //   7: aload_0
    //   8: monitorexit
    //   9: aload_0
    //   10: areturn
    //   11: astore_2
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_2
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #330	-> 2
    //   #331	-> 7
    //   #329	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	7	11	finally
  }
  
  public PrintStream append(CharSequence paramCharSequence) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield builder : Ljava/lang/StringBuilder;
    //   6: aload_1
    //   7: invokevirtual append : (Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;
    //   10: pop
    //   11: aload_0
    //   12: iconst_0
    //   13: invokespecial flush : (Z)V
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_0
    //   19: areturn
    //   20: astore_1
    //   21: aload_0
    //   22: monitorexit
    //   23: aload_1
    //   24: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #336	-> 2
    //   #337	-> 11
    //   #338	-> 16
    //   #335	-> 20
    // Exception table:
    //   from	to	target	type
    //   2	11	20	finally
    //   11	16	20	finally
  }
  
  public PrintStream append(CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield builder : Ljava/lang/StringBuilder;
    //   6: aload_1
    //   7: iload_2
    //   8: iload_3
    //   9: invokevirtual append : (Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;
    //   12: pop
    //   13: aload_0
    //   14: iconst_0
    //   15: invokespecial flush : (Z)V
    //   18: aload_0
    //   19: monitorexit
    //   20: aload_0
    //   21: areturn
    //   22: astore_1
    //   23: aload_0
    //   24: monitorexit
    //   25: aload_1
    //   26: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #344	-> 2
    //   #345	-> 13
    //   #346	-> 18
    //   #343	-> 22
    // Exception table:
    //   from	to	target	type
    //   2	13	22	finally
    //   13	18	22	finally
  }
  
  protected abstract void log(String paramString);
}
