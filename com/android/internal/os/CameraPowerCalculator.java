package com.android.internal.os;

import android.os.BatteryStats;

public class CameraPowerCalculator extends PowerCalculator {
  private final double mCameraPowerOnAvg;
  
  public CameraPowerCalculator(PowerProfile paramPowerProfile) {
    this.mCameraPowerOnAvg = paramPowerProfile.getAveragePower("camera.avg");
  }
  
  public void calculateApp(BatterySipper paramBatterySipper, BatteryStats.Uid paramUid, long paramLong1, long paramLong2, int paramInt) {
    BatteryStats.Timer timer = paramUid.getCameraTurnedOnTimer();
    if (timer != null) {
      paramLong1 = timer.getTotalTimeLocked(paramLong1, paramInt) / 1000L;
      paramBatterySipper.cameraTimeMs = paramLong1;
      paramBatterySipper.cameraPowerMah = paramLong1 * this.mCameraPowerOnAvg / 3600000.0D;
    } else {
      paramBatterySipper.cameraTimeMs = 0L;
      paramBatterySipper.cameraPowerMah = 0.0D;
    } 
  }
}
