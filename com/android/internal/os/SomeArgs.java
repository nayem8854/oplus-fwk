package com.android.internal.os;

public final class SomeArgs {
  private static final int MAX_POOL_SIZE = 10;
  
  static final int WAIT_FINISHED = 2;
  
  static final int WAIT_NONE = 0;
  
  static final int WAIT_WAITING = 1;
  
  private static SomeArgs sPool;
  
  private static Object sPoolLock = new Object();
  
  private static int sPoolSize;
  
  public Object arg1;
  
  public Object arg2;
  
  public Object arg3;
  
  public Object arg4;
  
  public Object arg5;
  
  public Object arg6;
  
  public Object arg7;
  
  public Object arg8;
  
  public Object arg9;
  
  public int argi1;
  
  public int argi2;
  
  public int argi3;
  
  public int argi4;
  
  public int argi5;
  
  public int argi6;
  
  private boolean mInPool;
  
  private SomeArgs mNext;
  
  int mWaitState = 0;
  
  public static SomeArgs obtain() {
    synchronized (sPoolLock) {
      if (sPoolSize > 0) {
        SomeArgs someArgs1 = sPool;
        sPool = sPool.mNext;
        someArgs1.mNext = null;
        someArgs1.mInPool = false;
        sPoolSize--;
        return someArgs1;
      } 
      SomeArgs someArgs = new SomeArgs();
      this();
      return someArgs;
    } 
  }
  
  public void complete() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mWaitState : I
    //   6: iconst_1
    //   7: if_icmpne -> 22
    //   10: aload_0
    //   11: iconst_2
    //   12: putfield mWaitState : I
    //   15: aload_0
    //   16: invokevirtual notifyAll : ()V
    //   19: aload_0
    //   20: monitorexit
    //   21: return
    //   22: new java/lang/IllegalStateException
    //   25: astore_1
    //   26: aload_1
    //   27: ldc 'Not waiting'
    //   29: invokespecial <init> : (Ljava/lang/String;)V
    //   32: aload_1
    //   33: athrow
    //   34: astore_1
    //   35: aload_0
    //   36: monitorexit
    //   37: aload_1
    //   38: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #87	-> 0
    //   #88	-> 2
    //   #91	-> 10
    //   #92	-> 15
    //   #93	-> 19
    //   #94	-> 21
    //   #89	-> 22
    //   #93	-> 34
    // Exception table:
    //   from	to	target	type
    //   2	10	34	finally
    //   10	15	34	finally
    //   15	19	34	finally
    //   19	21	34	finally
    //   22	34	34	finally
    //   35	37	34	finally
  }
  
  public void recycle() {
    if (!this.mInPool) {
      if (this.mWaitState != 0)
        return; 
      synchronized (sPoolLock) {
        clear();
        if (sPoolSize < 10) {
          this.mNext = sPool;
          this.mInPool = true;
          sPool = this;
          sPoolSize++;
        } 
        return;
      } 
    } 
    throw new IllegalStateException("Already recycled.");
  }
  
  private void clear() {
    this.arg1 = null;
    this.arg2 = null;
    this.arg3 = null;
    this.arg4 = null;
    this.arg5 = null;
    this.arg6 = null;
    this.arg7 = null;
    this.arg8 = null;
    this.arg9 = null;
    this.argi1 = 0;
    this.argi2 = 0;
    this.argi3 = 0;
    this.argi4 = 0;
    this.argi5 = 0;
    this.argi6 = 0;
  }
}
