package com.android.internal.os;

import android.common.ColorFrameworkFactory;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.BatteryStats;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.OplusBaseBatteryStats;
import android.os.OplusThermalState;
import android.os.Parcel;
import android.os.SystemClock;
import android.util.ArrayMap;
import android.util.AtomicFile;
import android.util.Log;
import android.util.Slog;
import android.util.SparseArray;
import com.oplus.deepthinker.IOplusDeepThinkerManager;
import com.oplus.eventhub.sdk.aidl.TriggerEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import oplus.util.OplusStatistics;

public abstract class OplusBaseBatteryStatsImpl extends BatteryStats {
  private final StringBuilder mFormatBuilder = new StringBuilder(32);
  
  private final Formatter mFormatter = new Formatter(this.mFormatBuilder);
  
  protected static boolean DEBUG_UID_SCREEN_BASIC = false;
  
  static {
    DEBUG_DETAIL = false;
    DEBUG_UID_SCREEN_DETAIL = false;
    ACTIVITY_MONITOR_MIN_TIME = 20;
    HIGH_POWER_THRESHOLD = 800L;
  }
  
  public ScreenoffBatteryStats mScreenoffBatteryStats = new ScreenoffBatteryStats();
  
  private Handler mLocalHandler = null;
  
  class StatisticsEntry {
    final String mPkgName;
    
    final long mTime;
    
    final int mUid;
    
    final OplusBaseBatteryStatsImpl this$0;
    
    StatisticsEntry(String param1String, int param1Int, long param1Long) {
      this.mPkgName = param1String;
      this.mUid = param1Int;
      this.mTime = param1Long;
    }
  }
  
  final Comparator<StatisticsEntry> StatisticsComparator = (Comparator<StatisticsEntry>)new Object(this);
  
  class WakeLockEntry extends StatisticsEntry {
    final int mCount;
    
    final String mTagName;
    
    final OplusBaseBatteryStatsImpl this$0;
    
    WakeLockEntry(String param1String1, String param1String2, int param1Int1, long param1Long, int param1Int2) {
      super(param1String2, param1Int1, param1Long);
      this.mTagName = param1String1;
      this.mCount = param1Int2;
    }
  }
  
  class SensorEntry extends StatisticsEntry {
    final int mHandle;
    
    final OplusBaseBatteryStatsImpl this$0;
    
    SensorEntry(int param1Int1, String param1String, int param1Int2, long param1Long) {
      super(param1String, param1Int2, param1Long);
      this.mHandle = param1Int1;
    }
  }
  
  class ScreenoffBatteryStats {
    private long mlastRcdTime = -1L;
    
    HashMap<String, Long> mPhoneSignalLevels = new HashMap<>();
    
    HashMap<String, Long> mWifiSignalLevels = new HashMap<>();
    
    HashMap<String, OplusBaseBatteryStatsImpl.WakeLockEntry> mAndroidWakelocks = new HashMap<>();
    
    SparseArray<SparseArray<Long>> mSensors = new SparseArray<>();
    
    SparseArray<Long> mBtTraffic = new SparseArray<>();
    
    SparseArray<Long> mMobileWakeup = new SparseArray<>();
    
    SparseArray<Long> mWifiWakeup = new SparseArray<>();
    
    long btRxTotalBytes;
    
    long btTxTotalBytes;
    
    long mScreenOffElapsedTime;
    
    final OplusBaseBatteryStatsImpl this$0;
    
    public void update() {
      long l1 = SystemClock.elapsedRealtime();
      this.mScreenOffElapsedTime = l1;
      long l2 = this.mlastRcdTime;
      if (-1L != l2 && l1 - l2 <= 60000L) {
        if (OplusBaseBatteryStatsImpl.DEBUG_DETAIL)
          Slog.d("OplusBaseBatteryStatsImpl", "ScreenoffBatteryStats: interval too short. ignore."); 
        return;
      } 
      if (OplusBaseBatteryStatsImpl.DEBUG_DETAIL)
        Slog.d("OplusBaseBatteryStatsImpl", "ScreenoffBatteryStats: start."); 
      this.mlastRcdTime = l1;
      SystemClock.uptimeMillis();
      l2 = l1 * 1000L;
      updateNetWorkTraffic(0);
      updatePhoneSignalLevels(l2, 0);
      updateWifiSignalLevels(l2, 0);
      this.mAndroidWakelocks.clear();
      this.mSensors.clear();
      this.mMobileWakeup.clear();
      this.mWifiWakeup.clear();
      SparseArray<BatteryStats.Uid> sparseArray = OplusBaseBatteryStatsImpl.this.getUidStats();
      int i = sparseArray.size();
      for (byte b = 0; b < i; b++) {
        BatteryStats.Uid uid = sparseArray.valueAt(b);
        int j = uid.getUid();
        updataAndroidWakelockPerUid(uid, j, l2, 0);
        updateSensorsGpsPerUid(uid, j, l2, 0);
        updataTrafficPerUid(uid, j, l2, 0);
        updateMobileWakeupPerUid(uid, j, l2, 0);
        updateWifiWakeupPerUid(uid, j, l2, 0);
      } 
      if (OplusBaseBatteryStatsImpl.DEBUG_DETAIL)
        Slog.d("OplusBaseBatteryStatsImpl", "ScreenoffBatteryStats: exit."); 
    }
    
    private void updateNetWorkTraffic(int param1Int) {
      this.btRxTotalBytes = OplusBaseBatteryStatsImpl.this.getNetworkActivityBytes(4, param1Int);
      this.btTxTotalBytes = OplusBaseBatteryStatsImpl.this.getNetworkActivityBytes(5, param1Int);
      if (OplusBaseBatteryStatsImpl.DEBUG_DETAIL) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ScreenoffBatteryStats. btRxTotalBytes=");
        stringBuilder.append(this.btRxTotalBytes);
        stringBuilder.append(", btTxTotalBytes=");
        stringBuilder.append(this.btTxTotalBytes);
        Slog.d("OplusBaseBatteryStatsImpl", stringBuilder.toString());
      } 
    }
    
    private void updatePhoneSignalLevels(long param1Long, int param1Int) {
      this.mPhoneSignalLevels.clear();
      for (byte b = 0; b < 5; b++) {
        long l = OplusBaseBatteryStatsImpl.this.getPhoneSignalStrengthTime(b, param1Long, param1Int) / 1000L;
        if (l > 0L) {
          if (OplusBaseBatteryStatsImpl.DEBUG_DETAIL) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("ScreenoffBatteryStats: PhoneSignalLevels. time=");
            stringBuilder.append(l);
            stringBuilder.append(", level=");
            Slog.d("OplusBaseBatteryStatsImpl", stringBuilder.toString());
          } 
          this.mPhoneSignalLevels.put("", Long.valueOf(l));
        } 
      } 
    }
    
    private void updateWifiSignalLevels(long param1Long, int param1Int) {
      this.mWifiSignalLevels.clear();
      for (byte b = 0; b < 5; b++) {
        long l = OplusBaseBatteryStatsImpl.this.getWifiSignalStrengthTime(b, param1Long, param1Int) / 1000L;
        if (l > 0L) {
          String str;
          if (true) {
            str = "";
          } else {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("level");
            stringBuilder.append(b);
            str = stringBuilder.toString();
          } 
          if (OplusBaseBatteryStatsImpl.DEBUG_DETAIL) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("ScreenoffBatteryStats: WifiSignalLevels. time=");
            stringBuilder.append(l);
            stringBuilder.append(", level=");
            stringBuilder.append(str);
            Slog.d("OplusBaseBatteryStatsImpl", stringBuilder.toString());
          } 
          this.mWifiSignalLevels.put(str, Long.valueOf(l));
        } 
      } 
    }
    
    private void updataAndroidWakelockPerUid(BatteryStats.Uid param1Uid, int param1Int1, long param1Long, int param1Int2) {
      ArrayMap arrayMap = param1Uid.getWakelockStats();
      for (int i = arrayMap.size() - 1; i >= 0; i--) {
        BatteryStats.Uid.Wakelock wakelock = (BatteryStats.Uid.Wakelock)arrayMap.valueAt(i);
        String str = (String)arrayMap.keyAt(i);
        BatteryStats.Timer timer = wakelock.getWakeTime(0);
        if (timer != null) {
          long l = (timer.getTotalTimeLocked(param1Long, param1Int2) + 500L) / 1000L;
          if (l > 30000L) {
            int j = timer.getCountLocked(param1Int2);
            HashMap<String, OplusBaseBatteryStatsImpl.WakeLockEntry> hashMap = this.mAndroidWakelocks;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(str);
            stringBuilder.append(param1Int1);
            hashMap.put(stringBuilder.toString(), new OplusBaseBatteryStatsImpl.WakeLockEntry(str, "android", param1Int1, l, j));
            if (OplusBaseBatteryStatsImpl.DEBUG_DETAIL) {
              stringBuilder = new StringBuilder();
              stringBuilder.append("ScreenoffBatteryStats: AndroidWakelocks=");
              stringBuilder.append(str);
              stringBuilder.append(", time=");
              stringBuilder.append(l);
              stringBuilder.append(", uid=");
              stringBuilder.append(param1Int1);
              Slog.d("OplusBaseBatteryStatsImpl", stringBuilder.toString());
            } 
          } 
        } 
      } 
    }
    
    private void updateSensorsGpsPerUid(BatteryStats.Uid param1Uid, int param1Int1, long param1Long, int param1Int2) {
      SparseArray<BatteryStats.Uid.Sensor> sparseArray = param1Uid.getSensorStats();
      int i = sparseArray.size();
      for (byte b = 0; b < i; b++) {
        BatteryStats.Uid.Sensor sensor = sparseArray.valueAt(b);
        sparseArray.keyAt(b);
        int j = sensor.getHandle();
        BatteryStats.Timer timer = sensor.getSensorTime();
        if (timer != null) {
          long l = (timer.getTotalTimeLocked(param1Long, param1Int2) + 500L) / 1000L;
          if (l > 30000L) {
            SparseArray<Long> sparseArray1 = this.mSensors.get(param1Int1);
            if (sparseArray1 == null) {
              sparseArray1 = new SparseArray();
              sparseArray1.put(j, Long.valueOf(l));
              this.mSensors.put(param1Int1, sparseArray1);
            } else {
              sparseArray1.put(j, Long.valueOf(l));
            } 
            if (OplusBaseBatteryStatsImpl.DEBUG_DETAIL) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("ScreenoffBatteryStats: sensors. handle=");
              stringBuilder.append(j);
              stringBuilder.append(", time=");
              stringBuilder.append(l);
              stringBuilder.append(", uid=");
              stringBuilder.append(param1Int1);
              Slog.d("OplusBaseBatteryStatsImpl", stringBuilder.toString());
            } 
          } 
        } 
      } 
    }
    
    private void updataTrafficPerUid(BatteryStats.Uid param1Uid, int param1Int1, long param1Long, int param1Int2) {
      long l = param1Uid.getNetworkActivityBytes(4, param1Int2);
      param1Long = param1Uid.getNetworkActivityBytes(5, param1Int2);
      param1Long = l + param1Long;
      if (param1Long > 1048576L) {
        this.mBtTraffic.put(param1Int1, Long.valueOf(param1Long));
        if (OplusBaseBatteryStatsImpl.DEBUG_DETAIL) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("updataTrafficPerUid: BtTraffic. uid=");
          stringBuilder.append(param1Int1);
          stringBuilder.append(", btTraffic=");
          OplusBaseBatteryStatsImpl oplusBaseBatteryStatsImpl = OplusBaseBatteryStatsImpl.this;
          stringBuilder.append(oplusBaseBatteryStatsImpl.formatBytesLocal(param1Long));
          String str = stringBuilder.toString();
          Slog.d("OplusBaseBatteryStatsImpl", str);
        } 
      } 
    }
    
    private void updateMobileWakeupPerUid(BatteryStats.Uid param1Uid, int param1Int1, long param1Long, int param1Int2) {
      param1Long = param1Uid.getMobileRadioApWakeupCount(param1Int2);
      if (param1Long > 0L) {
        this.mMobileWakeup.put(param1Int1, Long.valueOf(param1Long));
        if (OplusBaseBatteryStatsImpl.DEBUG_DETAIL) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("updateMobileWakeupPerUid: uid=");
          stringBuilder.append(param1Int1);
          stringBuilder.append(", mobileWakeup=");
          stringBuilder.append(param1Long);
          Slog.d("OplusBaseBatteryStatsImpl", stringBuilder.toString());
        } 
      } 
    }
    
    private void updateWifiWakeupPerUid(BatteryStats.Uid param1Uid, int param1Int1, long param1Long, int param1Int2) {
      param1Long = param1Uid.getWifiRadioApWakeupCount(param1Int2);
      if (param1Long > 0L) {
        this.mWifiWakeup.put(param1Int1, Long.valueOf(param1Long));
        if (OplusBaseBatteryStatsImpl.DEBUG_DETAIL) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("updateWifiWakeupPerUid: uid=");
          stringBuilder.append(param1Int1);
          stringBuilder.append(", wifiWakeup=");
          stringBuilder.append(param1Long);
          Slog.d("OplusBaseBatteryStatsImpl", stringBuilder.toString());
        } 
      } 
    }
  }
  
  private OplusThermalStatsHelper mOplusThermalStatsHelper = null;
  
  private File mThermalRecFile = null;
  
  protected static int ACTIVITY_MONITOR_MIN_TIME = 0;
  
  protected static final String BATTERY_REALTIME_CAPACITY = "/sys/class/power_supply/battery/batt_rm";
  
  private static final long BYTES_PER_GB = 1073741824L;
  
  private static final long BYTES_PER_KB = 1024L;
  
  private static final long BYTES_PER_MB = 1048576L;
  
  private static final boolean DEBUG = true;
  
  protected static boolean DEBUG_DETAIL = false;
  
  protected static boolean DEBUG_UID_SCREEN_DETAIL = false;
  
  static final long DELAY_SYSTEM_READY = 5000L;
  
  private static final String DUMP_HISTORY_INTENT_O = "oppo.intent.action.ACTION_OPPO_POWER_CHECKIN_SAVED";
  
  protected static long HIGH_POWER_THRESHOLD = 0L;
  
  private static final int MAX_NUM_UPLOAD = 3;
  
  protected static final long MILLISECONDS_IN_YEAR = 31536000000L;
  
  private static final int MIN_APP_NET_WAKEUP_CYCLE = 180000;
  
  private static final int MIN_RECORD_SENSOR_HELD_TIME = 30000;
  
  private static final long MIN_RECORD_TRAFFIC_BYTES = 1048576L;
  
  private static final int MIN_RECORD_WAKELOCK_HELD_TIME = 30000;
  
  private static final int MIN_SCREEN_OFF_DURATION = 1800000;
  
  private static final int MIN_SENSOR_HELD_TIME = 720000;
  
  private static final int MIN_SIGNAL_INTERVAL = 300000;
  
  private static final long MIN_TRAFFIC_BYTES = 12582912L;
  
  private static final int MIN_WAKELOCK_HELD_TIME = 720000;
  
  static final int MSG_CHECK_AVERAGE_CURRENT = 256;
  
  static final int MSG_SYSTEM_READY = 128;
  
  static final long ONE_HOUR_IN_MS = 3600000L;
  
  private static final String TAG = "OplusBaseBatteryStatsImpl";
  
  protected static final String UPLOAD_ACTIVITY_BATTERY_RECORD = "activity_battery_record";
  
  protected static final String UPLOAD_BATTERYSTATS_RESET = "batterystats_reset";
  
  protected static final String UPLOAD_LOGTAG = "20089";
  
  boolean DEBUG_SUPPER_APP;
  
  protected Context mContext;
  
  private IOplusDeepThinkerManager mDeepThinkerManager;
  
  private List<Sensor> mListAllSensor;
  
  BatteryStatsImpl.Uid mUidTopActivity;
  
  int pausedBatteryLevel;
  
  long pausedElapsedRealtime;
  
  int resumedBatteryLevel;
  
  int resumedBatteryRealtimeCapacity;
  
  String resumedClass;
  
  long resumedElapsedRealtime;
  
  String resumedPackage;
  
  int resumedUid;
  
  public void noteBaseScreenStateLocked(int paramInt1, int paramInt2, long paramLong, SparseArray<BatteryStatsImpl.Uid> paramSparseArray) {
    if (isScreenOn(paramInt1)) {
      if (DEBUG_UID_SCREEN_BASIC && this.mUidTopActivity != null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("noteScreenStateLocked: STATE_ON. UidTopActivity uid=");
        stringBuilder.append(this.mUidTopActivity.mUid);
        Slog.d("OplusBaseBatteryStatsImpl", stringBuilder.toString());
      } 
    } else if (isScreenOn(paramInt2)) {
      if (DEBUG_UID_SCREEN_BASIC && this.mUidTopActivity != null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("noteScreenStateLocked: STATE_OFF. UidTopActivity uid=");
        stringBuilder.append(this.mUidTopActivity.mUid);
        Slog.d("OplusBaseBatteryStatsImpl", stringBuilder.toString());
      } 
      screenoffStatsRcd();
    } 
  }
  
  public void dumpBaseScreenOffIdleLocked(Context paramContext, PrintWriter paramPrintWriter, List<Sensor> paramList) {
    StringBuilder stringBuilder1;
    OplusBaseBatteryStatsImpl oplusBaseBatteryStatsImpl = this;
    PrintWriter printWriter = paramPrintWriter;
    SystemClock.uptimeMillis();
    long l1 = SystemClock.elapsedRealtime() * 1000L;
    long l2 = SystemClock.elapsedRealtime() - oplusBaseBatteryStatsImpl.mScreenoffBatteryStats.mScreenOffElapsedTime;
    if (l2 < 1800000L) {
      if (DEBUG_DETAIL) {
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("dumpScreenOffIdleLocked: screenoffInterval=");
        stringBuilder1.append(l2);
        stringBuilder1.append("ms, ignore.");
        Slog.d("OplusBaseBatteryStatsImpl", stringBuilder1.toString());
      } 
      printWriter.println("ScreenOffIntervalShort");
      return;
    } 
    StringBuilder stringBuilder2 = new StringBuilder(128);
    printWriter.println("ScreenOffBatteryStats");
    dumpNetworkTraffic((PrintWriter)stringBuilder1, stringBuilder2, l1, 0);
    dumpPhoneSignalLevels((PrintWriter)stringBuilder1, stringBuilder2, l1, 0);
    dumpWifiSignalLevels((PrintWriter)stringBuilder1, stringBuilder2, l1, 0);
    SparseArray<BatteryStats.Uid> sparseArray = getUidStats();
    int i = sparseArray.size();
    ArrayList<WakeLockEntry> arrayList = new ArrayList();
    ArrayList<SensorEntry> arrayList1 = new ArrayList();
    ArrayList<SensorEntry> arrayList2 = new ArrayList();
    ArrayList<StatisticsEntry> arrayList3 = new ArrayList();
    ArrayList<StatisticsEntry> arrayList4 = new ArrayList();
    ArrayList<StatisticsEntry> arrayList5 = new ArrayList();
    for (byte b = 0; b < i; b++) {
      BatteryStats.Uid uid = sparseArray.valueAt(b);
      getAndroidWakelockPerUid(l1, 0, uid, arrayList);
      getSensorsGpsPerUid(l1, 0, uid, arrayList1, arrayList2);
      getTrafficPerUid(l1, 0, uid, arrayList3);
      getMobileWakeupPerUid(l1, 0, uid, arrayList4);
      getWifiWakeupPerUid(l1, 0, uid, arrayList5);
    } 
    oplusBaseBatteryStatsImpl.dumpAndroidWakelock(arrayList, stringBuilder2, printWriter);
    oplusBaseBatteryStatsImpl.dumpSensors(arrayList1, stringBuilder2, printWriter, paramList);
    oplusBaseBatteryStatsImpl.dumpGps(arrayList2, stringBuilder2, printWriter);
    oplusBaseBatteryStatsImpl.dumpTraffic(arrayList3, stringBuilder2, printWriter, "AppBtTraffic:  ");
    dumpAppNetWakeup(arrayList4, stringBuilder2, (PrintWriter)stringBuilder1, l2, "AppModemWakeupCycle:  ");
    dumpAppNetWakeup(arrayList5, stringBuilder2, (PrintWriter)stringBuilder1, l2, "AppWifiWakeupCycle:  ");
  }
  
  public boolean isScreenOn() {
    return isScreenOn(getScreenState());
  }
  
  private boolean isScreenOn(int paramInt) {
    return (paramInt == 2 || paramInt == 5 || paramInt == 6);
  }
  
  private void dumpNetworkTraffic(PrintWriter paramPrintWriter, StringBuilder paramStringBuilder, long paramLong, int paramInt) {
    paramLong = getNetworkActivityBytes(4, paramInt);
    long l = getNetworkActivityBytes(5, paramInt);
    paramLong = paramLong + l - this.mScreenoffBatteryStats.btRxTotalBytes + this.mScreenoffBatteryStats.btTxTotalBytes;
    if (DEBUG_DETAIL) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("dumpNetworkTraffic: btTraffic=");
      stringBuilder.append(paramLong);
      Slog.d("OplusBaseBatteryStatsImpl", stringBuilder.toString());
    } 
    if (paramLong > 12582912L) {
      paramStringBuilder.setLength(0);
      paramStringBuilder.append("BtTraffic:  ");
      paramStringBuilder.append(formatBytesLocal(paramLong));
      paramPrintWriter.println(paramStringBuilder.toString());
    } 
  }
  
  private void dumpPhoneSignalLevels(PrintWriter paramPrintWriter, StringBuilder paramStringBuilder, long paramLong, int paramInt) {
    paramStringBuilder.setLength(0);
    paramStringBuilder.append("PhoneSignalLevels:  ");
    boolean bool = false;
    HashMap<String, Long> hashMap = this.mScreenoffBatteryStats.mPhoneSignalLevels;
    for (byte b = 0; b < 5; b++) {
      long l = getPhoneSignalStrengthTime(b, paramLong, paramInt) / 1000L;
      if (l > 0L) {
        long l1 = 0L;
        long l2 = l1;
        if (hashMap != null) {
          l2 = l1;
          if (hashMap.get("") != null)
            l2 = ((Long)hashMap.get("")).longValue(); 
        } 
        l1 = l - l2;
        if (l1 > 0L) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("[");
          stringBuilder.append("");
          stringBuilder.append("] (");
          paramStringBuilder.append(stringBuilder.toString());
          formatTimeMs(paramStringBuilder, l1);
          paramStringBuilder.append("), ");
          boolean bool1 = bool;
          if (b <= 2) {
            bool1 = bool;
            if (l1 > 300000L)
              bool1 = true; 
          } 
          bool = bool1;
          if (DEBUG_DETAIL) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("dumpPhoneSignalLevels: levelStr=");
            stringBuilder.append("");
            stringBuilder.append(", timeBak=");
            stringBuilder.append(l2);
            stringBuilder.append(", time=");
            stringBuilder.append(l);
            stringBuilder.append(", timeDelta=");
            stringBuilder.append(l1);
            Slog.d("OplusBaseBatteryStatsImpl", stringBuilder.toString());
            bool = bool1;
          } 
        } 
      } 
    } 
    if (bool)
      paramPrintWriter.println(paramStringBuilder.toString()); 
  }
  
  private void dumpWifiSignalLevels(PrintWriter paramPrintWriter, StringBuilder paramStringBuilder, long paramLong, int paramInt) {
    paramStringBuilder.setLength(0);
    paramStringBuilder.append("WifiSignalLevels:  ");
    boolean bool1 = true;
    HashMap<String, Long> hashMap = this.mScreenoffBatteryStats.mWifiSignalLevels;
    boolean bool2 = false;
    for (byte b = 0; b < 5; b++) {
      String str;
      if (bool1) {
        str = "";
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("level");
        stringBuilder.append(b);
        str = stringBuilder.toString();
      } 
      long l = getWifiSignalStrengthTime(b, paramLong, paramInt) / 1000L;
      if (l > 0L) {
        long l1 = 0L;
        long l2 = l1;
        if (hashMap != null) {
          l2 = l1;
          if (hashMap.get(str) != null)
            l2 = ((Long)hashMap.get(str)).longValue(); 
        } 
        l1 = l - l2;
        if (l1 > 0L) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("[");
          stringBuilder.append(str);
          stringBuilder.append("] (");
          paramStringBuilder.append(stringBuilder.toString());
          formatTimeMs(paramStringBuilder, l1);
          paramStringBuilder.append("), ");
          boolean bool = bool2;
          if (b <= 2) {
            bool = bool2;
            if (l1 > 300000L)
              bool = true; 
          } 
          bool2 = bool;
          if (DEBUG_DETAIL) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("dumpWifiSignalLevels: levelStr=");
            stringBuilder.append(str);
            stringBuilder.append(", timeBak=");
            stringBuilder.append(l2);
            stringBuilder.append(", time=");
            stringBuilder.append(l);
            stringBuilder.append(", timeDelta=");
            stringBuilder.append(l1);
            Slog.d("OplusBaseBatteryStatsImpl", stringBuilder.toString());
            bool2 = bool;
          } 
        } 
      } 
    } 
    if (bool2)
      paramPrintWriter.println(paramStringBuilder.toString()); 
  }
  
  private void getAndroidWakelockPerUid(long paramLong, int paramInt, BatteryStats.Uid paramUid, ArrayList<WakeLockEntry> paramArrayList) {
    if (paramArrayList == null || paramUid == null)
      return; 
    HashMap<String, WakeLockEntry> hashMap = this.mScreenoffBatteryStats.mAndroidWakelocks;
    ArrayMap arrayMap = paramUid.getWakelockStats();
    int i = paramUid.getUid();
    for (int j = arrayMap.size() - 1; j >= 0; j--) {
      BatteryStats.Uid.Wakelock wakelock = (BatteryStats.Uid.Wakelock)arrayMap.valueAt(j);
      String str = (String)arrayMap.keyAt(j);
      BatteryStats.Timer timer = wakelock.getWakeTime(0);
      if (timer != null) {
        long l1 = (timer.getTotalTimeLocked(paramLong, paramInt) + 500L) / 1000L;
        long l2 = 0L;
        if (l1 > 0L) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(str);
          stringBuilder.append(i);
          WakeLockEntry wakeLockEntry = hashMap.get(stringBuilder.toString());
          if (wakeLockEntry != null)
            l2 = wakeLockEntry.mTime; 
          long l = l1 - l2;
          if (l > 720000L) {
            if (wakeLockEntry != null) {
              k = wakeLockEntry.mCount;
            } else {
              k = 0;
            } 
            int k = timer.getCountLocked(paramInt) - k;
            String str1 = getPackageName(paramUid, i);
            paramArrayList.add(new WakeLockEntry(str, str1, i, l, k));
            if (DEBUG_DETAIL) {
              StringBuilder stringBuilder1 = new StringBuilder();
              stringBuilder1.append("dumpAndroidWakelockPerUid: tag=");
              stringBuilder1.append(str);
              stringBuilder1.append(", timeBak=");
              stringBuilder1.append(l2);
              stringBuilder1.append(", totalTimeMillis=");
              stringBuilder1.append(l1);
              stringBuilder1.append(", heldTime=");
              stringBuilder1.append(l);
              stringBuilder1.append(", uid=");
              stringBuilder1.append(i);
              stringBuilder1.append(", count=");
              stringBuilder1.append(k);
              Slog.d("OplusBaseBatteryStatsImpl", stringBuilder1.toString());
            } 
          } 
        } 
      } 
    } 
  }
  
  private void getSensorsGpsPerUid(long paramLong, int paramInt, BatteryStats.Uid paramUid, ArrayList<SensorEntry> paramArrayList1, ArrayList<SensorEntry> paramArrayList2) {
    if (paramArrayList1 == null || paramArrayList2 == null || paramUid == null)
      return; 
    int i = paramUid.getUid();
    SparseArray<Long> sparseArray = this.mScreenoffBatteryStats.mSensors.get(i);
    SparseArray<BatteryStats.Uid.Sensor> sparseArray1 = paramUid.getSensorStats();
    int j = sparseArray1.size();
    for (byte b = 0; b < j; b++) {
      BatteryStats.Uid.Sensor sensor = sparseArray1.valueAt(b);
      int k = sensor.getHandle();
      BatteryStats.Timer timer = sensor.getSensorTime();
      if (timer != null) {
        long l = (timer.getTotalTimeLocked(paramLong, paramInt) + 500L) / 1000L;
        if (l > 0L) {
          long l1;
          if (sparseArray != null && 
            sparseArray.get(k) != null) {
            l1 = ((Long)sparseArray.get(k)).longValue();
          } else {
            l1 = 0L;
          } 
          long l2 = l - l1;
          if (l2 > 720000L) {
            String str = getPackageName(paramUid, i);
            if (-10000 == k) {
              paramArrayList2.add(new SensorEntry(k, str, i, l2));
            } else {
              paramArrayList1.add(new SensorEntry(k, str, i, l2));
            } 
            if (DEBUG_DETAIL) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("getSensorsGpsPerUid: handle=");
              stringBuilder.append(k);
              stringBuilder.append(", timeBak=");
              stringBuilder.append(l1);
              stringBuilder.append(", totalTime=");
              stringBuilder.append(l);
              stringBuilder.append(", heldTime=");
              stringBuilder.append(l2);
              stringBuilder.append(", uid=");
              stringBuilder.append(i);
              Slog.d("OplusBaseBatteryStatsImpl", stringBuilder.toString());
            } 
          } 
        } 
      } 
    } 
  }
  
  private void getTrafficPerUid(long paramLong, int paramInt, BatteryStats.Uid paramUid, ArrayList<StatisticsEntry> paramArrayList) {
    // Byte code:
    //   0: aload #4
    //   2: ifnull -> 142
    //   5: aload #5
    //   7: ifnonnull -> 13
    //   10: goto -> 142
    //   13: aload #4
    //   15: iconst_4
    //   16: iload_3
    //   17: invokevirtual getNetworkActivityBytes : (II)J
    //   20: lstore #6
    //   22: aload #4
    //   24: iconst_5
    //   25: iload_3
    //   26: invokevirtual getNetworkActivityBytes : (II)J
    //   29: lstore #8
    //   31: aload #4
    //   33: invokevirtual getUid : ()I
    //   36: istore_3
    //   37: aload_0
    //   38: getfield mScreenoffBatteryStats : Lcom/android/internal/os/OplusBaseBatteryStatsImpl$ScreenoffBatteryStats;
    //   41: getfield mBtTraffic : Landroid/util/SparseArray;
    //   44: ifnull -> 86
    //   47: aload_0
    //   48: getfield mScreenoffBatteryStats : Lcom/android/internal/os/OplusBaseBatteryStatsImpl$ScreenoffBatteryStats;
    //   51: getfield mBtTraffic : Landroid/util/SparseArray;
    //   54: astore #10
    //   56: aload #10
    //   58: iload_3
    //   59: invokevirtual get : (I)Ljava/lang/Object;
    //   62: ifnull -> 86
    //   65: aload_0
    //   66: getfield mScreenoffBatteryStats : Lcom/android/internal/os/OplusBaseBatteryStatsImpl$ScreenoffBatteryStats;
    //   69: getfield mBtTraffic : Landroid/util/SparseArray;
    //   72: iload_3
    //   73: invokevirtual get : (I)Ljava/lang/Object;
    //   76: checkcast java/lang/Long
    //   79: invokevirtual longValue : ()J
    //   82: lstore_1
    //   83: goto -> 88
    //   86: lconst_0
    //   87: lstore_1
    //   88: lload #6
    //   90: lload #8
    //   92: ladd
    //   93: lload_1
    //   94: lsub
    //   95: lstore_1
    //   96: lload_1
    //   97: ldc2_w 12582912
    //   100: lcmp
    //   101: iflt -> 141
    //   104: iconst_0
    //   105: ifne -> 120
    //   108: aload_0
    //   109: aload #4
    //   111: iload_3
    //   112: invokespecial getPackageName : (Landroid/os/BatteryStats$Uid;I)Ljava/lang/String;
    //   115: astore #4
    //   117: goto -> 123
    //   120: aconst_null
    //   121: astore #4
    //   123: aload #5
    //   125: new com/android/internal/os/OplusBaseBatteryStatsImpl$StatisticsEntry
    //   128: dup
    //   129: aload_0
    //   130: aload #4
    //   132: iload_3
    //   133: lload_1
    //   134: invokespecial <init> : (Lcom/android/internal/os/OplusBaseBatteryStatsImpl;Ljava/lang/String;IJ)V
    //   137: invokevirtual add : (Ljava/lang/Object;)Z
    //   140: pop
    //   141: return
    //   142: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #770	-> 0
    //   #774	-> 13
    //   #775	-> 22
    //   #776	-> 31
    //   #778	-> 37
    //   #781	-> 37
    //   #782	-> 37
    //   #783	-> 56
    //   #784	-> 65
    //   #787	-> 86
    //   #788	-> 96
    //   #789	-> 104
    //   #790	-> 108
    //   #789	-> 120
    //   #792	-> 123
    //   #794	-> 141
    //   #771	-> 142
  }
  
  private void getMobileWakeupPerUid(long paramLong, int paramInt, BatteryStats.Uid paramUid, ArrayList<StatisticsEntry> paramArrayList) {
    // Byte code:
    //   0: aload #4
    //   2: ifnull -> 214
    //   5: aload #5
    //   7: ifnonnull -> 13
    //   10: goto -> 214
    //   13: aload #4
    //   15: iload_3
    //   16: invokevirtual getMobileRadioApWakeupCount : (I)J
    //   19: lstore #6
    //   21: aload #4
    //   23: invokevirtual getUid : ()I
    //   26: istore_3
    //   27: aload_0
    //   28: getfield mScreenoffBatteryStats : Lcom/android/internal/os/OplusBaseBatteryStatsImpl$ScreenoffBatteryStats;
    //   31: getfield mMobileWakeup : Landroid/util/SparseArray;
    //   34: ifnull -> 76
    //   37: aload_0
    //   38: getfield mScreenoffBatteryStats : Lcom/android/internal/os/OplusBaseBatteryStatsImpl$ScreenoffBatteryStats;
    //   41: getfield mMobileWakeup : Landroid/util/SparseArray;
    //   44: astore #8
    //   46: aload #8
    //   48: iload_3
    //   49: invokevirtual get : (I)Ljava/lang/Object;
    //   52: ifnull -> 76
    //   55: aload_0
    //   56: getfield mScreenoffBatteryStats : Lcom/android/internal/os/OplusBaseBatteryStatsImpl$ScreenoffBatteryStats;
    //   59: getfield mMobileWakeup : Landroid/util/SparseArray;
    //   62: iload_3
    //   63: invokevirtual get : (I)Ljava/lang/Object;
    //   66: checkcast java/lang/Long
    //   69: invokevirtual longValue : ()J
    //   72: lstore_1
    //   73: goto -> 78
    //   76: lconst_0
    //   77: lstore_1
    //   78: lload #6
    //   80: lload_1
    //   81: lsub
    //   82: lstore #9
    //   84: lload #9
    //   86: lconst_0
    //   87: lcmp
    //   88: ifle -> 213
    //   91: aload #5
    //   93: new com/android/internal/os/OplusBaseBatteryStatsImpl$StatisticsEntry
    //   96: dup
    //   97: aload_0
    //   98: aload_0
    //   99: aload #4
    //   101: iload_3
    //   102: invokespecial getPackageName : (Landroid/os/BatteryStats$Uid;I)Ljava/lang/String;
    //   105: iload_3
    //   106: lload #9
    //   108: invokespecial <init> : (Lcom/android/internal/os/OplusBaseBatteryStatsImpl;Ljava/lang/String;IJ)V
    //   111: invokevirtual add : (Ljava/lang/Object;)Z
    //   114: pop
    //   115: getstatic com/android/internal/os/OplusBaseBatteryStatsImpl.DEBUG_DETAIL : Z
    //   118: ifeq -> 210
    //   121: new java/lang/StringBuilder
    //   124: dup
    //   125: invokespecial <init> : ()V
    //   128: astore #4
    //   130: aload #4
    //   132: ldc_w 'getMobileWakeupPerUid: uid='
    //   135: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   138: pop
    //   139: aload #4
    //   141: iload_3
    //   142: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   145: pop
    //   146: aload #4
    //   148: ldc_w ', mobileWakeUp='
    //   151: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   154: pop
    //   155: aload #4
    //   157: lload #9
    //   159: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   162: pop
    //   163: aload #4
    //   165: ldc_w ', mobileWakeupNow='
    //   168: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   171: pop
    //   172: aload #4
    //   174: lload #6
    //   176: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   179: pop
    //   180: aload #4
    //   182: ldc_w ', mobileWakeupBak='
    //   185: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   188: pop
    //   189: aload #4
    //   191: lload_1
    //   192: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   195: pop
    //   196: ldc 'OplusBaseBatteryStatsImpl'
    //   198: aload #4
    //   200: invokevirtual toString : ()Ljava/lang/String;
    //   203: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   206: pop
    //   207: goto -> 213
    //   210: goto -> 213
    //   213: return
    //   214: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #798	-> 0
    //   #803	-> 13
    //   #804	-> 21
    //   #807	-> 27
    //   #808	-> 27
    //   #809	-> 46
    //   #810	-> 55
    //   #814	-> 76
    //   #815	-> 84
    //   #816	-> 91
    //   #817	-> 115
    //   #818	-> 121
    //   #817	-> 210
    //   #815	-> 213
    //   #824	-> 213
    //   #798	-> 214
    //   #799	-> 214
  }
  
  private void getWifiWakeupPerUid(long paramLong, int paramInt, BatteryStats.Uid paramUid, ArrayList<StatisticsEntry> paramArrayList) {
    // Byte code:
    //   0: aload #4
    //   2: ifnull -> 214
    //   5: aload #5
    //   7: ifnonnull -> 13
    //   10: goto -> 214
    //   13: aload #4
    //   15: iload_3
    //   16: invokevirtual getWifiRadioApWakeupCount : (I)J
    //   19: lstore #6
    //   21: aload #4
    //   23: invokevirtual getUid : ()I
    //   26: istore_3
    //   27: aload_0
    //   28: getfield mScreenoffBatteryStats : Lcom/android/internal/os/OplusBaseBatteryStatsImpl$ScreenoffBatteryStats;
    //   31: getfield mWifiWakeup : Landroid/util/SparseArray;
    //   34: ifnull -> 76
    //   37: aload_0
    //   38: getfield mScreenoffBatteryStats : Lcom/android/internal/os/OplusBaseBatteryStatsImpl$ScreenoffBatteryStats;
    //   41: getfield mWifiWakeup : Landroid/util/SparseArray;
    //   44: astore #8
    //   46: aload #8
    //   48: iload_3
    //   49: invokevirtual get : (I)Ljava/lang/Object;
    //   52: ifnull -> 76
    //   55: aload_0
    //   56: getfield mScreenoffBatteryStats : Lcom/android/internal/os/OplusBaseBatteryStatsImpl$ScreenoffBatteryStats;
    //   59: getfield mWifiWakeup : Landroid/util/SparseArray;
    //   62: iload_3
    //   63: invokevirtual get : (I)Ljava/lang/Object;
    //   66: checkcast java/lang/Long
    //   69: invokevirtual longValue : ()J
    //   72: lstore_1
    //   73: goto -> 78
    //   76: lconst_0
    //   77: lstore_1
    //   78: lload #6
    //   80: lload_1
    //   81: lsub
    //   82: lstore #9
    //   84: lload #9
    //   86: lconst_0
    //   87: lcmp
    //   88: ifle -> 213
    //   91: aload #5
    //   93: new com/android/internal/os/OplusBaseBatteryStatsImpl$StatisticsEntry
    //   96: dup
    //   97: aload_0
    //   98: aload_0
    //   99: aload #4
    //   101: iload_3
    //   102: invokespecial getPackageName : (Landroid/os/BatteryStats$Uid;I)Ljava/lang/String;
    //   105: iload_3
    //   106: lload #9
    //   108: invokespecial <init> : (Lcom/android/internal/os/OplusBaseBatteryStatsImpl;Ljava/lang/String;IJ)V
    //   111: invokevirtual add : (Ljava/lang/Object;)Z
    //   114: pop
    //   115: getstatic com/android/internal/os/OplusBaseBatteryStatsImpl.DEBUG_DETAIL : Z
    //   118: ifeq -> 210
    //   121: new java/lang/StringBuilder
    //   124: dup
    //   125: invokespecial <init> : ()V
    //   128: astore #4
    //   130: aload #4
    //   132: ldc_w 'getWifiWakeupPerUid: uid='
    //   135: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   138: pop
    //   139: aload #4
    //   141: iload_3
    //   142: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   145: pop
    //   146: aload #4
    //   148: ldc_w ', wifiWakeUp='
    //   151: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   154: pop
    //   155: aload #4
    //   157: lload #9
    //   159: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   162: pop
    //   163: aload #4
    //   165: ldc_w ', wifiWakeupNow='
    //   168: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   171: pop
    //   172: aload #4
    //   174: lload #6
    //   176: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   179: pop
    //   180: aload #4
    //   182: ldc_w ', wifiWakeupBak='
    //   185: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   188: pop
    //   189: aload #4
    //   191: lload_1
    //   192: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   195: pop
    //   196: ldc 'OplusBaseBatteryStatsImpl'
    //   198: aload #4
    //   200: invokevirtual toString : ()Ljava/lang/String;
    //   203: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   206: pop
    //   207: goto -> 213
    //   210: goto -> 213
    //   213: return
    //   214: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #828	-> 0
    //   #833	-> 13
    //   #834	-> 21
    //   #837	-> 27
    //   #838	-> 27
    //   #839	-> 46
    //   #840	-> 55
    //   #844	-> 76
    //   #845	-> 84
    //   #846	-> 91
    //   #847	-> 115
    //   #848	-> 121
    //   #847	-> 210
    //   #845	-> 213
    //   #854	-> 213
    //   #828	-> 214
    //   #829	-> 214
  }
  
  private void dumpAndroidWakelock(ArrayList<WakeLockEntry> paramArrayList, StringBuilder paramStringBuilder, PrintWriter paramPrintWriter) {
    if (paramArrayList.size() > 0) {
      Collections.sort((List)paramArrayList, this.StatisticsComparator);
      paramStringBuilder.setLength(0);
      paramStringBuilder.append("AndroidWakeLocks:  ");
      for (byte b = 0; b < paramArrayList.size(); b++) {
        WakeLockEntry wakeLockEntry = paramArrayList.get(b);
        if (b >= 3)
          break; 
        paramStringBuilder.append("<");
        paramStringBuilder.append(wakeLockEntry.mUid);
        paramStringBuilder.append("> {");
        paramStringBuilder.append(wakeLockEntry.mPkgName);
        paramStringBuilder.append("} [");
        paramStringBuilder.append(wakeLockEntry.mTagName);
        paramStringBuilder.append("] (");
        formatTimeMs(paramStringBuilder, wakeLockEntry.mTime);
        paramStringBuilder.append(") #");
        paramStringBuilder.append(wakeLockEntry.mCount);
        paramStringBuilder.append("#, ");
      } 
      paramPrintWriter.println(paramStringBuilder.toString());
    } 
  }
  
  private void dumpSensors(ArrayList<SensorEntry> paramArrayList, StringBuilder paramStringBuilder, PrintWriter paramPrintWriter, List<Sensor> paramList) {
    if (paramArrayList.size() > 0) {
      Collections.sort((List)paramArrayList, this.StatisticsComparator);
      paramStringBuilder.setLength(0);
      paramStringBuilder.append("Sensors:  ");
      int i = 0;
      for (byte b = 0; b < paramArrayList.size(); b++, i = j) {
        int j;
        SensorEntry sensorEntry = paramArrayList.get(b);
        if (i >= 3)
          break; 
        Sensor sensor = getSensorForHandle(sensorEntry.mHandle, paramList);
        if (sensor == null) {
          j = i;
        } else {
          int k = sensor.getType();
          String str = sensor.getName();
          j = i;
          if (k != 19) {
            j = i;
            if (k != 18) {
              j = i;
              if (k != 17)
                if (str == null) {
                  j = i;
                } else {
                  paramStringBuilder.append("<");
                  paramStringBuilder.append(k);
                  paramStringBuilder.append("> {");
                  paramStringBuilder.append(sensorEntry.mPkgName);
                  paramStringBuilder.append("} [");
                  paramStringBuilder.append(str);
                  paramStringBuilder.append("] (");
                  formatTimeMs(paramStringBuilder, sensorEntry.mTime);
                  paramStringBuilder.append("), ");
                  j = i + 1;
                }  
            } 
          } 
        } 
      } 
      if (i > 0)
        paramPrintWriter.println(paramStringBuilder.toString()); 
    } 
  }
  
  private void dumpGps(ArrayList<SensorEntry> paramArrayList, StringBuilder paramStringBuilder, PrintWriter paramPrintWriter) {
    if (paramArrayList.size() > 0) {
      Collections.sort((List)paramArrayList, this.StatisticsComparator);
      paramStringBuilder.setLength(0);
      paramStringBuilder.append("Gps:  ");
      for (byte b = 0; b < paramArrayList.size(); b++) {
        SensorEntry sensorEntry = paramArrayList.get(b);
        if (b >= 3)
          break; 
        paramStringBuilder.append("<");
        paramStringBuilder.append(sensorEntry.mUid);
        paramStringBuilder.append("> {");
        paramStringBuilder.append(sensorEntry.mPkgName);
        paramStringBuilder.append("} (");
        formatTimeMs(paramStringBuilder, sensorEntry.mTime);
        paramStringBuilder.append("), ");
      } 
      paramPrintWriter.println(paramStringBuilder.toString());
    } 
  }
  
  private void dumpTraffic(ArrayList<StatisticsEntry> paramArrayList, StringBuilder paramStringBuilder, PrintWriter paramPrintWriter, String paramString) {
    if (paramArrayList.size() > 0) {
      Collections.sort(paramArrayList, this.StatisticsComparator);
      paramStringBuilder.setLength(0);
      paramStringBuilder.append(paramString);
      for (byte b = 0; b < paramArrayList.size(); b++) {
        StatisticsEntry statisticsEntry = paramArrayList.get(b);
        if (b >= 3)
          break; 
        paramStringBuilder.append("<");
        paramStringBuilder.append(statisticsEntry.mUid);
        paramStringBuilder.append("> {");
        paramStringBuilder.append(statisticsEntry.mPkgName);
        paramStringBuilder.append("} (");
        paramStringBuilder.append(formatBytesLocal(statisticsEntry.mTime));
        paramStringBuilder.append("), ");
      } 
      paramPrintWriter.println(paramStringBuilder.toString());
    } 
  }
  
  private void dumpAppNetWakeup(ArrayList<StatisticsEntry> paramArrayList, StringBuilder paramStringBuilder, PrintWriter paramPrintWriter, long paramLong, String paramString) {
    if (paramArrayList == null || 
      paramArrayList.isEmpty())
      return; 
    Collections.sort(paramArrayList, this.StatisticsComparator);
    boolean bool = false;
    paramStringBuilder.setLength(0);
    paramStringBuilder.append(paramString);
    for (byte b = 0; b < paramArrayList.size(); b++) {
      StatisticsEntry statisticsEntry = paramArrayList.get(b);
      long l = paramLong / statisticsEntry.mTime;
      if (b >= 3 || l > 180000L)
        break; 
      paramStringBuilder.append("<");
      paramStringBuilder.append(statisticsEntry.mUid);
      paramStringBuilder.append("> {");
      paramStringBuilder.append(statisticsEntry.mPkgName);
      paramStringBuilder.append("} (");
      formatTimeMs(paramStringBuilder, l);
      paramStringBuilder.append("), ");
      bool = true;
    } 
    if (bool)
      paramPrintWriter.println(paramStringBuilder.toString()); 
  }
  
  private String getPackageName(BatteryStats.Uid paramUid, int paramInt) {
    String str2 = null;
    if (paramInt == 1000) {
      str2 = "Android";
    } else {
      String str;
      ArrayMap arrayMap = paramUid.getPackageStats();
      int i = arrayMap.size() - 1;
      while (true) {
        str = str2;
        if (i >= 0) {
          str = (String)arrayMap.keyAt(i);
          if (!"android".equals(str))
            break; 
          i--;
          continue;
        } 
        break;
      } 
      str2 = str;
      if (str == null) {
        ArrayMap arrayMap1 = paramUid.getProcessStats();
        i = arrayMap1.size() - 1;
        while (true) {
          str2 = str;
          if (i >= 0) {
            str2 = (String)arrayMap1.keyAt(i);
            if (!"*wakelock*".equals(str2))
              break; 
            i--;
            continue;
          } 
          break;
        } 
      } 
    } 
    String str1 = str2;
    if (str2 == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("uid(");
      stringBuilder.append(paramInt);
      stringBuilder.append(")");
      str1 = stringBuilder.toString();
    } 
    return str1;
  }
  
  private Sensor getSensorForHandle(int paramInt, List<Sensor> paramList) {
    Sensor sensor2, sensor1 = null;
    if (paramList == null)
      return null; 
    byte b = 0;
    while (true) {
      sensor2 = sensor1;
      if (b < paramList.size()) {
        sensor2 = paramList.get(b);
        if (sensor2.getHandle() == paramInt)
          break; 
        b++;
        continue;
      } 
      break;
    } 
    return sensor2;
  }
  
  private void screenoffStatsRcd() {
    this.mScreenoffBatteryStats.update();
  }
  
  private String formatBytesLocal(long paramLong) {
    this.mFormatBuilder.setLength(0);
    if (paramLong < 1024L) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramLong);
      stringBuilder.append("B");
      return stringBuilder.toString();
    } 
    if (paramLong < 1048576L) {
      this.mFormatter.format("%.2fKB", new Object[] { Double.valueOf(paramLong / 1024.0D) });
      return this.mFormatBuilder.toString();
    } 
    if (paramLong < 1073741824L) {
      this.mFormatter.format("%.2fMB", new Object[] { Double.valueOf(paramLong / 1048576.0D) });
      return this.mFormatBuilder.toString();
    } 
    this.mFormatter.format("%.2fGB", new Object[] { Double.valueOf(paramLong / 1.073741824E9D) });
    return this.mFormatBuilder.toString();
  }
  
  protected boolean onBatteryStatsMessageHandle(Message paramMessage) {
    if (paramMessage == null)
      return false; 
    boolean bool = false;
    int i = paramMessage.what;
    if (i != 128) {
      if (i == 256) {
        long l1 = getClockElapsedRealtime() - this.resumedElapsedRealtime;
        i = this.resumedBatteryRealtimeCapacity - getBatteryRealtimeCapacity();
        long l2 = i * 3600000L / l1;
        if (this.DEBUG_SUPPER_APP) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("resumedTimeInMs=");
          stringBuilder.append(l1);
          stringBuilder.append("  deltaBC=");
          stringBuilder.append(i);
          stringBuilder.append("  averageCurrent=");
          stringBuilder.append(l2);
          Slog.d("OplusBaseBatteryStatsImpl", stringBuilder.toString());
        } 
        if (l2 > HIGH_POWER_THRESHOLD) {
          Intent intent = new Intent("oppo.intent.action.ACTION_HIGH_POWER_SCENE");
          intent.putExtra("appName", this.resumedPackage);
          intent.putExtra("averageCurrent", l2);
          intent.setPackage("com.oppo.oppopowermonitor");
          Thread thread = new Thread((Runnable)new Object(this, intent));
          thread.start();
        } 
        bool = true;
      } 
    } else {
      getAllSensorList();
      OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
      if (oplusThermalStatsHelper != null)
        oplusThermalStatsHelper.setBatteryStatsReady(true); 
      bool = true;
    } 
    return bool;
  }
  
  protected void addThermalForeProc(long paramLong1, long paramLong2, String paramString, int paramInt1, int paramInt2) {
    if (-1 == paramInt2 || paramInt2 == 32776 || paramInt2 == 32771) {
      paramInt2 = 1;
    } else {
      paramInt2 = 0;
    } 
    if (paramInt2 != 0) {
      OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
      if (oplusThermalStatsHelper != null)
        oplusThermalStatsHelper.addThermalForeProc(paramLong1, paramLong2, paramString, paramInt1); 
    } 
  }
  
  public void noteConnectivityChangedLocked(int paramInt, String paramString, long paramLong1, long paramLong2) {
    byte b2, b1 = -1;
    if (paramString.equals("DISCONNECTED")) {
      paramInt = -1;
      oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
      b2 = paramInt;
      if (oplusThermalStatsHelper != null) {
        oplusThermalStatsHelper.addThermalConnectType(paramLong1, paramLong2, (byte)0);
        b2 = paramInt;
      } 
    } else if (oplusThermalStatsHelper.equals("CONNECTED") && paramInt == 1) {
      paramInt = 1;
      oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
      b2 = paramInt;
      if (oplusThermalStatsHelper != null) {
        oplusThermalStatsHelper.addThermalConnectType(paramLong1, paramLong2, (byte)1);
        b2 = paramInt;
      } 
    } else {
      b2 = b1;
      if (oplusThermalStatsHelper.equals("CONNECTED")) {
        b2 = b1;
        if (paramInt == 0)
          b2 = 0; 
      } 
    } 
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper != null)
      oplusThermalStatsHelper.setConnectyType(b2); 
  }
  
  public OplusBaseBatteryStatsImpl() {
    this.DEBUG_SUPPER_APP = true;
    this.resumedUid = -1;
    this.resumedBatteryLevel = -1;
    this.resumedElapsedRealtime = -1L;
    this.resumedBatteryRealtimeCapacity = -1;
    this.resumedPackage = null;
    this.resumedClass = null;
    this.pausedBatteryLevel = -1;
    this.pausedElapsedRealtime = -1L;
    this.mThermalRecFile = null;
  }
  
  public OplusBaseBatteryStatsImpl(File paramFile, Handler paramHandler) {
    this.DEBUG_SUPPER_APP = true;
    this.resumedUid = -1;
    this.resumedBatteryLevel = -1;
    this.resumedElapsedRealtime = -1L;
    this.resumedBatteryRealtimeCapacity = -1;
    this.resumedPackage = null;
    this.resumedClass = null;
    this.pausedBatteryLevel = -1;
    this.pausedElapsedRealtime = -1L;
    if (paramFile == null) {
      this.mThermalRecFile = null;
      Slog.d("OplusBaseBatteryStatsImpl", "systemDir IS NULL");
    } else {
      File file = new File(paramFile, "thermalstats.bin");
      if (file != null && file.exists()) {
        Slog.d("OplusBaseBatteryStatsImpl", "thermalstats.bin creat success");
      } else {
        Slog.d("OplusBaseBatteryStatsImpl", "thermalstats.bin creat failed");
      } 
    } 
    this.mLocalHandler = paramHandler;
    this.mOplusThermalStatsHelper = new OplusThermalStatsHelper(this, paramHandler, this.mThermalRecFile, paramFile);
  }
  
  protected int getBatteryRealtimeCapacity() {
    int j, i = 0;
    File file = new File("/sys/class/power_supply/battery/batt_rm");
    BufferedReader bufferedReader1 = null, bufferedReader2 = null, bufferedReader3 = null, bufferedReader4 = null;
    BufferedReader bufferedReader5 = bufferedReader4, bufferedReader6 = bufferedReader1, bufferedReader7 = bufferedReader2, bufferedReader8 = bufferedReader3;
    try {
      BufferedReader bufferedReader = new BufferedReader();
      bufferedReader5 = bufferedReader4;
      bufferedReader6 = bufferedReader1;
      bufferedReader7 = bufferedReader2;
      bufferedReader8 = bufferedReader3;
      FileReader fileReader = new FileReader();
      bufferedReader5 = bufferedReader4;
      bufferedReader6 = bufferedReader1;
      bufferedReader7 = bufferedReader2;
      bufferedReader8 = bufferedReader3;
      this(file);
      bufferedReader5 = bufferedReader4;
      bufferedReader6 = bufferedReader1;
      bufferedReader7 = bufferedReader2;
      bufferedReader8 = bufferedReader3;
      this(fileReader);
      bufferedReader2 = bufferedReader;
      while (true) {
        bufferedReader5 = bufferedReader2;
        bufferedReader6 = bufferedReader2;
        bufferedReader7 = bufferedReader2;
        bufferedReader8 = bufferedReader2;
        String str = bufferedReader2.readLine();
        if (str != null) {
          int k = i;
          bufferedReader5 = bufferedReader2;
          bufferedReader6 = bufferedReader2;
          bufferedReader7 = bufferedReader2;
          bufferedReader8 = bufferedReader2;
          if (str.length() != 0) {
            bufferedReader5 = bufferedReader2;
            bufferedReader6 = bufferedReader2;
            bufferedReader7 = bufferedReader2;
            bufferedReader8 = bufferedReader2;
            k = Integer.parseInt(str.trim());
          } 
          i = k;
          continue;
        } 
        break;
      } 
      j = i;
      try {
        bufferedReader2.close();
        j = i;
      } catch (IOException iOException) {
        iOException.printStackTrace();
      } 
    } catch (FileNotFoundException fileNotFoundException) {
      bufferedReader5 = bufferedReader8;
      fileNotFoundException.printStackTrace();
      i = -1;
      j = i;
      if (bufferedReader8 != null) {
        j = i;
        bufferedReader8.close();
        j = i;
      } 
    } catch (IOException iOException) {
      bufferedReader5 = bufferedReader7;
      iOException.printStackTrace();
      i = -1;
      j = i;
      if (bufferedReader7 != null) {
        j = i;
        bufferedReader7.close();
        j = i;
      } 
    } catch (NumberFormatException numberFormatException) {
      bufferedReader5 = bufferedReader6;
      numberFormatException.printStackTrace();
      i = -1;
      j = i;
      if (bufferedReader6 != null) {
        j = i;
        bufferedReader6.close();
        j = i;
      } 
    } finally {}
    return j;
  }
  
  public void onSystemServicesReady(Context paramContext) {
    Slog.d("OplusBaseBatteryStatsImpl", "systemReady.......");
    this.mLocalHandler.sendEmptyMessageDelayed(128, 5000L);
    this.mContext = paramContext;
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper != null)
      oplusThermalStatsHelper.onSystemReady(paramContext); 
  }
  
  protected void collectCheckinFile() {
    if (getLowDischargeAmountSinceCharge() >= 20) {
      Parcel parcel = Parcel.obtain();
      onWriteSummaryToParcel(parcel, true);
      BackgroundThread.getHandler().post((Runnable)new Object(this, parcel));
    } 
  }
  
  private void getAllSensorList() {
    Context context = this.mContext;
    if (context == null)
      return; 
    SensorManager sensorManager = (SensorManager)context.getSystemService("sensor");
    if (sensorManager == null) {
      Slog.d("OplusBaseBatteryStatsImpl", "getAllSensorList: SensorManager is null.");
      return;
    } 
    this.mListAllSensor = sensorManager.getSensorList(-1);
    if (DEBUG_DETAIL)
      for (byte b = 0; b < this.mListAllSensor.size(); b++) {
        Sensor sensor = this.mListAllSensor.get(b);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("getAllSensorList: sensor handle(");
        stringBuilder.append(sensor.getHandle());
        stringBuilder.append("), type(");
        stringBuilder.append(sensor.getType());
        stringBuilder.append("), name(");
        stringBuilder.append(sensor.getName());
        stringBuilder.append(")");
        String str = stringBuilder.toString();
        Slog.d("OplusBaseBatteryStatsImpl", str);
      }  
  }
  
  public void oplusLogSwitch(boolean paramBoolean) {
    DEBUG_DETAIL = paramBoolean;
    DEBUG_UID_SCREEN_BASIC = paramBoolean;
    DEBUG_UID_SCREEN_DETAIL = paramBoolean;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("oplusLogSwitch: en=");
    stringBuilder.append(paramBoolean);
    Slog.d("OplusBaseBatteryStatsImpl", stringBuilder.toString());
  }
  
  public void dumpScreenOffIdleLocked(Context paramContext, PrintWriter paramPrintWriter) {
    dumpBaseScreenOffIdleLocked(paramContext, paramPrintWriter, this.mListAllSensor);
  }
  
  public boolean startIteratingThermalHistoryLocked() {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper != null)
      return oplusThermalStatsHelper.startIteratingThermalHistoryLocked(); 
    return false;
  }
  
  public void finishIteratingThermalHistoryLocked() {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper != null)
      oplusThermalStatsHelper.finishIteratingThermalHistoryLocked(); 
  }
  
  public int getThermalHistoryUsedSize() {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper != null)
      return oplusThermalStatsHelper.getThermalHistoryUsedSize(); 
    return -1;
  }
  
  public boolean getNextThermalHistoryLocked(OplusBaseBatteryStats.ThermalItem paramThermalItem, long paramLong) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper != null)
      return oplusThermalStatsHelper.getNextThermalHistoryLocked(paramThermalItem, paramLong); 
    return false;
  }
  
  public OplusBaseBatteryStats.ThermalItem getThermalHistoryFromFile(BufferedReader paramBufferedReader, PrintWriter paramPrintWriter, OplusBaseBatteryStats.ThermalHistoryPrinter paramThermalHistoryPrinter) throws IOException {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper != null)
      return oplusThermalStatsHelper.getThermalHistoryFromFile(paramBufferedReader, paramPrintWriter, paramThermalHistoryPrinter); 
    return null;
  }
  
  public void getThermalRawHistoryFromFile(BufferedReader paramBufferedReader, PrintWriter paramPrintWriter) throws IOException {
    while (true) {
      String str = paramBufferedReader.readLine();
      if (str != null) {
        if (paramPrintWriter != null) {
          paramPrintWriter.print(str);
          paramPrintWriter.println();
        } 
        continue;
      } 
      break;
    } 
  }
  
  public boolean getBatteryStatsReady() {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper != null)
      return oplusThermalStatsHelper.getBatteryStatsReadyStatus(); 
    return false;
  }
  
  public void noteScreenBrightnessModeChangedLock(boolean paramBoolean) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper != null)
      oplusThermalStatsHelper.noteScreenBrightnessModeChangedLock(paramBoolean); 
  }
  
  public void setThermalState(OplusThermalState paramOplusThermalState) {
    if (DEBUG_DETAIL)
      Slog.d("OplusBaseBatteryStatsImpl", "setThermalState"); 
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper != null)
      oplusThermalStatsHelper.setThermalState(paramOplusThermalState); 
  }
  
  public void schedulerUpdateCpu(long paramLong) {
    onSchedulerUpdateCpu(paramLong);
  }
  
  public void dumpThemalRawLocked(PrintWriter paramPrintWriter, long paramLong) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper != null)
      oplusThermalStatsHelper.writeThermalRecFile(); 
    dumpThemalRawLockedInner(paramPrintWriter, paramLong);
  }
  
  public void setThermalCpuLoading(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, String paramString1, String paramString2) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper != null)
      oplusThermalStatsHelper.setThermalCpuLoading(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramString1, paramString2); 
  }
  
  public OplusThermalStatsHelper getOplusThermalStatsHelper() {
    return this.mOplusThermalStatsHelper;
  }
  
  public void clearThermalAllHistory() {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper != null)
      oplusThermalStatsHelper.clearThermalAllHistory(); 
  }
  
  public void toggleThermalDebugSwith(PrintWriter paramPrintWriter, int paramInt) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper != null)
      oplusThermalStatsHelper.toggleThermalDebugSwith(paramPrintWriter, paramInt); 
  }
  
  public void updateCpuStatsNow(PrintWriter paramPrintWriter) {
    schedulerUpdateCpu(0L);
  }
  
  public void setThermalHeatThreshold(PrintWriter paramPrintWriter, int paramInt) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper != null)
      oplusThermalStatsHelper.setThermalHeatThreshold(paramPrintWriter, paramInt); 
  }
  
  public void setHeatBetweenTime(PrintWriter paramPrintWriter, int paramInt) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper != null)
      oplusThermalStatsHelper.setHeatBetweenTime(paramPrintWriter, paramInt); 
  }
  
  public void setMonitorAppLimitTime(PrintWriter paramPrintWriter, int paramInt) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper != null)
      oplusThermalStatsHelper.setMonitorAppLimitTime(paramPrintWriter, paramInt); 
  }
  
  public void getMonitorAppLocked(PrintWriter paramPrintWriter) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper != null)
      oplusThermalStatsHelper.getMonitorAppLocked(paramPrintWriter); 
  }
  
  public void backupThermalStatsFile() {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper != null)
      oplusThermalStatsHelper.backupThermalStatsFile(); 
  }
  
  public void dumpThemalHeatDetailLocked(PrintWriter paramPrintWriter) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper != null)
      oplusThermalStatsHelper.dumpThemalHeatDetailLocked(paramPrintWriter); 
  }
  
  public void getPhoneTemp(PrintWriter paramPrintWriter) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return; 
    oplusThermalStatsHelper.getPhoneTemp(paramPrintWriter);
  }
  
  public void printThermalUploadTemp(PrintWriter paramPrintWriter) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return; 
    oplusThermalStatsHelper.printThermalUploadTemp(paramPrintWriter);
  }
  
  public void printChargeMapLocked(PrintWriter paramPrintWriter) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return; 
    oplusThermalStatsHelper.printChargeMapLocked(paramPrintWriter);
  }
  
  public void printThermalHeatThreshold(PrintWriter paramPrintWriter) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return; 
    oplusThermalStatsHelper.printThermalHeatThreshold(paramPrintWriter);
  }
  
  public void backupThermalLogFile() {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return; 
    oplusThermalStatsHelper.backupThermalLogFile();
  }
  
  public void setThermalConfig() {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return; 
    oplusThermalStatsHelper.setThermalConfig();
  }
  
  public void addThermalnetSyncProc(long paramLong1, long paramLong2, String paramString) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return; 
    oplusThermalStatsHelper.addThermalnetSyncProc(paramLong1, paramLong2, paramString);
  }
  
  public void addThermalJobProc(long paramLong1, long paramLong2, String paramString) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return; 
    oplusThermalStatsHelper.addThermalJobProc(paramLong1, paramLong2, paramString);
  }
  
  private void reportToEventFountain(TriggerEvent paramTriggerEvent) {
    if (paramTriggerEvent == null)
      return; 
    IOplusDeepThinkerManager iOplusDeepThinkerManager = getDeepThinkerManager();
    iOplusDeepThinkerManager.run(new _$$Lambda$OplusBaseBatteryStatsImpl$MdWwPbgBUQtNGMeBhlw5sNcu8nk(iOplusDeepThinkerManager, paramTriggerEvent));
  }
  
  private TriggerEvent assembleTriggerEvent(int paramInt1, long paramLong1, long paramLong2, boolean paramBoolean, int paramInt2) {
    TriggerEvent triggerEvent;
    Bundle bundle = null;
    if (paramInt1 != 2) {
      if (paramInt1 == 4) {
        if (paramBoolean) {
          paramInt1 = 26;
        } else {
          paramInt1 = 27;
        } 
        bundle = new Bundle();
        bundle.putInt("uid", paramInt2);
        bundle.putLong("elapsed_real_time", paramLong1);
        triggerEvent = new TriggerEvent(paramInt1, 0, null, bundle);
      } 
    } else {
      if (paramBoolean) {
        paramInt1 = 18;
      } else {
        paramInt1 = 19;
      } 
      bundle = new Bundle();
      bundle.putInt("uid", paramInt2);
      bundle.putLong("elapsed_real_time", paramLong1);
      triggerEvent = new TriggerEvent(paramInt1, 0, null, bundle);
    } 
    return triggerEvent;
  }
  
  private IOplusDeepThinkerManager getDeepThinkerManager() {
    if (this.mDeepThinkerManager == null)
      this.mDeepThinkerManager = ColorFrameworkFactory.getInstance().getOplusDeepThinkerManager(this.mContext); 
    return this.mDeepThinkerManager;
  }
  
  public void addThermalOnOffEvent(int paramInt1, long paramLong1, long paramLong2, boolean paramBoolean, int paramInt2) {
    addThermalOnOffEvent(paramInt1, paramLong1, paramLong2, paramBoolean);
    reportToEventFountain(assembleTriggerEvent(paramInt1, paramLong1, paramLong2, paramBoolean, paramInt2));
  }
  
  public void addThermalOnOffEvent(int paramInt, long paramLong1, long paramLong2, boolean paramBoolean) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return; 
    oplusThermalStatsHelper.addThermalOnOffEvent(paramInt, paramLong1, paramLong2, paramBoolean);
  }
  
  public void addThermalScreenBrightnessEvent(long paramLong1, long paramLong2, int paramInt1, int paramInt2) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return; 
    oplusThermalStatsHelper.addThermalScreenBrightnessEvent(paramLong1, paramLong2, paramInt1, paramInt2);
  }
  
  public void setScreenBrightness(int paramInt) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return; 
    oplusThermalStatsHelper.setScreenBrightness(paramInt);
  }
  
  public void addThermalNetState(long paramLong1, long paramLong2, boolean paramBoolean) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return; 
    oplusThermalStatsHelper.addThermalNetState(paramLong1, paramLong2, paramBoolean);
  }
  
  public void addThermalPhoneOnOff(long paramLong1, long paramLong2, boolean paramBoolean) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return; 
    oplusThermalStatsHelper.addThermalPhoneOnOff(paramLong1, paramLong2, paramBoolean);
  }
  
  public void addThermalPhoneSignal(long paramLong1, long paramLong2, byte paramByte) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return; 
    oplusThermalStatsHelper.addThermalPhoneSignal(paramLong1, paramLong2, paramByte);
  }
  
  public void addThermalPhoneState(long paramLong1, long paramLong2, byte paramByte) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return; 
    oplusThermalStatsHelper.addThermalPhoneState(paramLong1, paramLong2, paramByte);
  }
  
  public void notePhoneDataConnectionStateLocked(long paramLong1, long paramLong2, int paramInt) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return; 
    oplusThermalStatsHelper.notePhoneDataConnectionStateLocked(paramLong1, paramLong2, paramInt);
  }
  
  public void addThermalWifiStatus(long paramLong1, long paramLong2, int paramInt) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return; 
    oplusThermalStatsHelper.addThermalWifiStatus(paramLong1, paramLong2, paramInt);
  }
  
  public void addThermalWifiRssi(long paramLong1, long paramLong2, int paramInt) {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return; 
    oplusThermalStatsHelper.addThermalWifiRssi(paramLong1, paramLong2, paramInt);
  }
  
  public void writeThermalRecFile() {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return; 
    oplusThermalStatsHelper.writeThermalRecFile();
  }
  
  public void clearThermalStatsBuffer() {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return; 
    oplusThermalStatsHelper.clearThermalStatsBuffer();
  }
  
  public int getScreenBrightness() {
    OplusThermalStatsHelper oplusThermalStatsHelper = this.mOplusThermalStatsHelper;
    if (oplusThermalStatsHelper == null)
      return 0; 
    return oplusThermalStatsHelper.getScreenBrightness();
  }
  
  public boolean readThermalRecFile() {
    boolean bool;
    File file1 = this.mThermalRecFile;
    if (file1 != null && file1.exists()) {
      bool = true;
    } else {
      bool = false;
    } 
    if (!bool)
      return false; 
    BufferedReader bufferedReader1 = null;
    File file2 = null;
    file1 = file2;
    BufferedReader bufferedReader2 = bufferedReader1;
    try {
      long l = System.currentTimeMillis();
      file1 = file2;
      bufferedReader2 = bufferedReader1;
      BufferedReader bufferedReader5 = new BufferedReader();
      file1 = file2;
      bufferedReader2 = bufferedReader1;
      FileReader fileReader = new FileReader();
      file1 = file2;
      bufferedReader2 = bufferedReader1;
      this(this.mThermalRecFile);
      file1 = file2;
      bufferedReader2 = bufferedReader1;
      this(fileReader);
      BufferedReader bufferedReader4 = bufferedReader5;
      BufferedReader bufferedReader3 = bufferedReader4;
      bufferedReader2 = bufferedReader4;
      getThermalHistoryFromFile(bufferedReader4, (PrintWriter)null, (OplusBaseBatteryStats.ThermalHistoryPrinter)null);
      bufferedReader3 = bufferedReader4;
      bufferedReader2 = bufferedReader4;
      bufferedReader4.close();
      bufferedReader4 = null;
      bufferedReader5 = null;
      bufferedReader3 = bufferedReader5;
      bufferedReader2 = bufferedReader4;
      StringBuilder stringBuilder = new StringBuilder();
      bufferedReader3 = bufferedReader5;
      bufferedReader2 = bufferedReader4;
      this();
      bufferedReader3 = bufferedReader5;
      bufferedReader2 = bufferedReader4;
      stringBuilder.append("readThermal history file lost time = ");
      bufferedReader3 = bufferedReader5;
      bufferedReader2 = bufferedReader4;
      stringBuilder.append(System.currentTimeMillis() - l);
      bufferedReader3 = bufferedReader5;
      bufferedReader2 = bufferedReader4;
      Slog.i("OplusBaseBatteryStatsImpl", stringBuilder.toString());
      if (false)
        try {
          throw new NullPointerException();
        } catch (Exception exception) {} 
    } catch (Exception exception) {
      BufferedReader bufferedReader = bufferedReader2;
      Slog.e("BatteryStats", "Error reading thermalFile statistics", exception);
      bufferedReader = bufferedReader2;
      clearThermalStatsBuffer();
      if (bufferedReader2 != null)
        bufferedReader2.close(); 
    } finally {}
    return true;
  }
  
  public void dumpThemalLocked(PrintWriter paramPrintWriter, long paramLong) {
    long l1 = getHistoryTotalSize();
    long l2 = getThermalHistoryUsedSize();
    if (startIteratingThermalHistoryLocked())
      try {
        paramPrintWriter.print("Thermal History (");
        paramPrintWriter.print(100L * l2 / l1);
        paramPrintWriter.print("% used, ");
        printSizeValueLocal(paramPrintWriter, l2);
        paramPrintWriter.print(" used of ");
        printSizeValueLocal(paramPrintWriter, l1);
        paramPrintWriter.println("):");
        dumpThermalHistoryLocked(paramPrintWriter, paramLong);
        paramPrintWriter.println();
      } finally {
        finishIteratingThermalHistoryLocked();
      }  
  }
  
  private void dumpThemalRawLockedInner(PrintWriter paramPrintWriter, long paramLong) {
    if (startIteratingThermalHistoryLocked()) {
      try {
        File file = new File();
        this("/data/system/thermalstats.bin");
        if (!file.exists()) {
          paramPrintWriter.println("no raw file");
          finishIteratingThermalHistoryLocked();
          finishIteratingThermalHistoryLocked();
          return;
        } 
        BufferedReader bufferedReader = new BufferedReader();
        FileReader fileReader = new FileReader();
        this(file);
        this(fileReader);
        try {
          getThermalRawHistoryFromFile(bufferedReader, paramPrintWriter);
          bufferedReader.close();
        } catch (IOException iOException) {
          iOException.printStackTrace();
        } 
      } catch (Exception exception) {
        Slog.e("BatteryStats", "Error reading thermalFile statistics", exception);
      } finally {}
      finishIteratingThermalHistoryLocked();
    } 
  }
  
  public void dumpThemalRecLocked(Context paramContext, PrintWriter paramPrintWriter, int paramInt1, int paramInt2, long paramLong) {
    if (startIteratingThermalHistoryLocked()) {
      try {
        File file = new File();
        this("/data/system/thermal/dcs");
        boolean bool = file.isDirectory();
        if (!bool || !file.exists()) {
          paramPrintWriter.println("no history file");
          finishIteratingThermalHistoryLocked();
          finishIteratingThermalHistoryLocked();
          return;
        } 
        File[] arrayOfFile = file.listFiles();
        if (arrayOfFile.length <= 0) {
          paramPrintWriter.println("no history file");
          finishIteratingThermalHistoryLocked();
          finishIteratingThermalHistoryLocked();
          return;
        } 
        for (paramInt2 = arrayOfFile.length, paramInt1 = 0; paramInt1 < paramInt2; ) {
          File file1 = arrayOfFile[paramInt1];
          OplusBaseBatteryStats.ThermalHistoryPrinter thermalHistoryPrinter = new OplusBaseBatteryStats.ThermalHistoryPrinter();
          this();
          BufferedReader bufferedReader = new BufferedReader();
          FileReader fileReader = new FileReader();
          this(file1);
          this(fileReader);
          try {
            getThermalHistoryFromFile(bufferedReader, paramPrintWriter, thermalHistoryPrinter);
            bufferedReader.close();
          } catch (IOException iOException) {
            iOException.printStackTrace();
          } 
          paramInt1++;
        } 
      } catch (Exception exception) {
        Slog.e("BatteryStats", "Error reading thermalFile statistics", exception);
      } finally {}
      finishIteratingThermalHistoryLocked();
    } 
  }
  
  private void dumpThermalHistoryLocked(PrintWriter paramPrintWriter, long paramLong) {
    OplusBaseBatteryStats.ThermalHistoryPrinter thermalHistoryPrinter = new OplusBaseBatteryStats.ThermalHistoryPrinter();
    OplusBaseBatteryStats.ThermalItem thermalItem = new OplusBaseBatteryStats.ThermalItem();
    while (getNextThermalHistoryLocked(thermalItem, paramLong)) {
      if (thermalItem.elapsedRealtime >= paramLong)
        thermalHistoryPrinter.printNextItem(paramPrintWriter, thermalItem); 
    } 
  }
  
  private void printSizeValueLocal(PrintWriter paramPrintWriter, long paramLong) {
    float f1 = (float)paramLong;
    String str = "";
    float f2 = f1;
    if (f1 >= 10240.0F) {
      str = "KB";
      f2 = f1 / 1024.0F;
    } 
    f1 = f2;
    if (f2 >= 10240.0F) {
      str = "MB";
      f1 = f2 / 1024.0F;
    } 
    f2 = f1;
    if (f1 >= 10240.0F) {
      str = "GB";
      f2 = f1 / 1024.0F;
    } 
    f1 = f2;
    if (f2 >= 10240.0F) {
      str = "TB";
      f1 = f2 / 1024.0F;
    } 
    f2 = f1;
    if (f1 >= 10240.0F) {
      str = "PB";
      f2 = f1 / 1024.0F;
    } 
    paramPrintWriter.print((int)f2);
    paramPrintWriter.print(str);
  }
  
  public void noteActivityResumedLocked(int paramInt, ComponentName paramComponentName, boolean paramBoolean, BatteryStats.HistoryItem paramHistoryItem, long paramLong, Handler paramHandler, String paramString) {
    if (paramBoolean && paramComponentName != null) {
      this.resumedUid = paramInt;
      this.resumedBatteryLevel = paramHistoryItem.batteryLevel;
      this.resumedElapsedRealtime = paramLong;
      this.resumedPackage = paramComponentName.getPackageName();
      this.resumedClass = paramComponentName.getClassName();
      this.resumedBatteryRealtimeCapacity = getBatteryRealtimeCapacity();
      if (this.DEBUG_SUPPER_APP) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("resumedUid:");
        stringBuilder.append(this.resumedUid);
        stringBuilder.append(" resumedBatteryLevel:");
        stringBuilder.append(this.resumedBatteryLevel);
        stringBuilder.append("  resumedPackage:");
        stringBuilder.append(this.resumedPackage);
        stringBuilder.append("  resumedBatteryRealtimeCapacity:");
        stringBuilder.append(this.resumedBatteryRealtimeCapacity);
        Slog.d("OplusBaseBatteryStatsImpl", stringBuilder.toString());
      } 
      if (paramHandler.hasMessages(256))
        paramHandler.removeMessages(256); 
      Message message = paramHandler.obtainMessage(256);
      paramHandler.sendMessageDelayed(message, (ACTIVITY_MONITOR_MIN_TIME * 60 * 1000));
    } 
    if (paramComponentName != null)
      paramComponentName.getClassName(); 
  }
  
  public void noteActivityPausedLocked(int paramInt, ComponentName paramComponentName, boolean paramBoolean, BatteryStats.HistoryItem paramHistoryItem, long paramLong, Handler paramHandler) {
    if (paramHandler.hasMessages(256))
      paramHandler.removeMessages(256); 
    if (paramBoolean && paramInt == this.resumedUid) {
      this.pausedBatteryLevel = paramHistoryItem.batteryLevel;
      this.pausedElapsedRealtime = paramLong;
      paramLong = (paramLong - this.resumedElapsedRealtime) / 60000L;
      if (this.DEBUG_SUPPER_APP) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("pausedBatteryLevel:");
        stringBuilder.append(this.pausedBatteryLevel);
        stringBuilder.append(" activityFocusedTimeInMin:");
        stringBuilder.append(paramLong);
        Slog.d("OplusBaseBatteryStatsImpl", stringBuilder.toString());
      } 
      if (paramLong > ACTIVITY_MONITOR_MIN_TIME) {
        HashMap<Object, Object> hashMap = new HashMap<>();
        hashMap.put("appname", this.resumedPackage);
        hashMap.put("activity", this.resumedClass);
        hashMap.put("duration", String.valueOf(paramLong));
        hashMap.put("startlevel", String.valueOf(this.resumedBatteryLevel));
        hashMap.put("endlevel", String.valueOf(this.pausedBatteryLevel));
        hashMap.put("deltaBC", String.valueOf(this.resumedBatteryRealtimeCapacity - getBatteryRealtimeCapacity()));
        Context context = this.mContext;
        if (context != null) {
          OplusStatistics.onCommon(context, "20089", "activity_battery_record", hashMap, false);
        } else {
          Log.i("OplusBaseBatteryStatsImpl", "noteRecordSensorForOppoLocked ,but context is null!");
        } 
      } 
    } 
  }
  
  protected abstract AtomicFile getBatteryCheckinFile();
  
  protected abstract long getClockElapsedRealtime();
  
  public abstract int getHistoryBufferSize();
  
  protected abstract int getScreenState();
  
  public abstract int getWifiSignalStrengthBin();
  
  protected abstract void onBatterySendBroadcast(Intent paramIntent);
  
  protected abstract void onNoteActivityResumed(int paramInt);
  
  protected abstract void onSchedulerUpdateCpu(long paramLong);
  
  protected abstract void onWriteSummaryToParcel(Parcel paramParcel, boolean paramBoolean);
  
  class Clocks {
    public abstract long elapsedRealtime();
    
    public abstract long uptimeMillis();
  }
}
