package com.android.internal.os;

import android.os.Binder;
import android.os.Process;
import android.os.SystemClock;
import android.text.format.DateFormat;
import android.util.ArrayMap;
import android.util.Pair;
import android.util.Slog;
import android.util.SparseArray;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.ToDoubleFunction;

public class BinderCallsStats implements BinderInternal.Observer {
  class OverflowBinder extends Binder {}
  
  private static final Class<? extends Binder> OVERFLOW_BINDER = (Class)OverflowBinder.class;
  
  private boolean mDetailedTracking = true;
  
  private int mPeriodicSamplingInterval = 1000;
  
  private int mMaxBinderCallStatsCount = 1500;
  
  private final SparseArray<UidEntry> mUidEntries = new SparseArray<>();
  
  private final ArrayMap<String, Integer> mExceptionCounts = new ArrayMap<>();
  
  private final Queue<BinderInternal.CallSession> mCallSessionsPool = new ConcurrentLinkedQueue<>();
  
  private final Object mLock = new Object();
  
  private long mStartCurrentTime = System.currentTimeMillis();
  
  private long mStartElapsedTime = SystemClock.elapsedRealtime();
  
  private long mCallStatsCount = 0L;
  
  private boolean mAddDebugEntries = false;
  
  private boolean mTrackDirectCallingUid = true;
  
  private boolean mTrackScreenInteractive = false;
  
  private static final int CALL_SESSIONS_POOL_SIZE = 100;
  
  private static final String DEBUG_ENTRY_PREFIX = "__DEBUG_";
  
  public static final boolean DEFAULT_TRACK_DIRECT_CALLING_UID = true;
  
  public static final boolean DEFAULT_TRACK_SCREEN_INTERACTIVE = false;
  
  public static final boolean DETAILED_TRACKING_DEFAULT = true;
  
  public static final boolean ENABLED_DEFAULT = true;
  
  private static final String EXCEPTION_COUNT_OVERFLOW_NAME = "overflow";
  
  public static final int MAX_BINDER_CALL_STATS_COUNT_DEFAULT = 1500;
  
  private static final int MAX_EXCEPTION_COUNT_SIZE = 50;
  
  private static final int OVERFLOW_DIRECT_CALLING_UID = -1;
  
  private static final boolean OVERFLOW_SCREEN_INTERACTIVE = false;
  
  private static final int OVERFLOW_TRANSACTION_CODE = -1;
  
  public static final int PERIODIC_SAMPLING_INTERVAL_DEFAULT = 1000;
  
  private static final String TAG = "BinderCallsStats";
  
  private CachedDeviceState.TimeInStateStopwatch mBatteryStopwatch;
  
  private CachedDeviceState.Readonly mDeviceState;
  
  private final Random mRandom;
  
  class Injector {
    public Random getRandomGenerator() {
      return new Random();
    }
  }
  
  public BinderCallsStats(Injector paramInjector) {
    this.mRandom = paramInjector.getRandomGenerator();
  }
  
  public void setDeviceState(CachedDeviceState.Readonly paramReadonly) {
    CachedDeviceState.TimeInStateStopwatch timeInStateStopwatch = this.mBatteryStopwatch;
    if (timeInStateStopwatch != null)
      timeInStateStopwatch.close(); 
    this.mDeviceState = paramReadonly;
    this.mBatteryStopwatch = paramReadonly.createTimeOnBatteryStopwatch();
  }
  
  public BinderInternal.CallSession callStarted(Binder paramBinder, int paramInt1, int paramInt2) {
    CachedDeviceState.Readonly readonly = this.mDeviceState;
    if (readonly == null || readonly.isCharging())
      return null; 
    BinderInternal.CallSession callSession = obtainCallSession();
    callSession.binderClass = (Class)paramBinder.getClass();
    callSession.transactionCode = paramInt1;
    callSession.exceptionThrown = false;
    callSession.cpuTimeStarted = -1L;
    callSession.timeStarted = -1L;
    if (shouldRecordDetailedData()) {
      callSession.cpuTimeStarted = getThreadTimeMicro();
      callSession.timeStarted = getElapsedRealtimeMicro();
    } 
    return callSession;
  }
  
  private BinderInternal.CallSession obtainCallSession() {
    BinderInternal.CallSession callSession = this.mCallSessionsPool.poll();
    if (callSession == null)
      callSession = new BinderInternal.CallSession(); 
    return callSession;
  }
  
  public void callEnded(BinderInternal.CallSession paramCallSession, int paramInt1, int paramInt2, int paramInt3) {
    if (paramCallSession == null)
      return; 
    processCallEnded(paramCallSession, paramInt1, paramInt2, paramInt3);
    if (this.mCallSessionsPool.size() < 100)
      this.mCallSessionsPool.add(paramCallSession); 
  }
  
  private void processCallEnded(BinderInternal.CallSession paramCallSession, int paramInt1, int paramInt2, int paramInt3) {
    // Byte code:
    //   0: aload_1
    //   1: getfield cpuTimeStarted : J
    //   4: lconst_0
    //   5: lcmp
    //   6: iflt -> 15
    //   9: iconst_1
    //   10: istore #5
    //   12: goto -> 18
    //   15: iconst_0
    //   16: istore #5
    //   18: iload #5
    //   20: ifeq -> 48
    //   23: aload_0
    //   24: invokevirtual getThreadTimeMicro : ()J
    //   27: aload_1
    //   28: getfield cpuTimeStarted : J
    //   31: lsub
    //   32: lstore #6
    //   34: aload_0
    //   35: invokevirtual getElapsedRealtimeMicro : ()J
    //   38: aload_1
    //   39: getfield timeStarted : J
    //   42: lsub
    //   43: lstore #8
    //   45: goto -> 54
    //   48: lconst_0
    //   49: lstore #6
    //   51: lconst_0
    //   52: lstore #8
    //   54: aload_0
    //   55: getfield mTrackScreenInteractive : Z
    //   58: ifeq -> 73
    //   61: aload_0
    //   62: getfield mDeviceState : Lcom/android/internal/os/CachedDeviceState$Readonly;
    //   65: invokevirtual isScreenInteractive : ()Z
    //   68: istore #10
    //   70: goto -> 76
    //   73: iconst_0
    //   74: istore #10
    //   76: aload_0
    //   77: getfield mTrackDirectCallingUid : Z
    //   80: ifeq -> 92
    //   83: aload_0
    //   84: invokevirtual getCallingUid : ()I
    //   87: istore #11
    //   89: goto -> 95
    //   92: iconst_m1
    //   93: istore #11
    //   95: aload_0
    //   96: getfield mLock : Ljava/lang/Object;
    //   99: astore #12
    //   101: aload #12
    //   103: monitorenter
    //   104: aload_0
    //   105: getfield mDeviceState : Lcom/android/internal/os/CachedDeviceState$Readonly;
    //   108: ifnull -> 542
    //   111: aload_0
    //   112: getfield mDeviceState : Lcom/android/internal/os/CachedDeviceState$Readonly;
    //   115: invokevirtual isCharging : ()Z
    //   118: ifeq -> 124
    //   121: goto -> 542
    //   124: aload_0
    //   125: iload #4
    //   127: invokespecial getUidEntry : (I)Lcom/android/internal/os/BinderCallsStats$UidEntry;
    //   130: astore #13
    //   132: aload #13
    //   134: getfield callCount : J
    //   137: lstore #14
    //   139: lconst_1
    //   140: lstore #16
    //   142: aload #13
    //   144: lload #14
    //   146: lconst_1
    //   147: ladd
    //   148: putfield callCount : J
    //   151: iload #5
    //   153: ifeq -> 490
    //   156: aload #13
    //   158: aload #13
    //   160: getfield cpuTimeMicros : J
    //   163: lload #6
    //   165: ladd
    //   166: putfield cpuTimeMicros : J
    //   169: aload #13
    //   171: aload #13
    //   173: getfield recordedCallCount : J
    //   176: lconst_1
    //   177: ladd
    //   178: putfield recordedCallCount : J
    //   181: aload_1
    //   182: getfield binderClass : Ljava/lang/Class;
    //   185: astore #18
    //   187: aload_1
    //   188: getfield transactionCode : I
    //   191: istore #5
    //   193: aload_0
    //   194: getfield mCallStatsCount : J
    //   197: lstore #14
    //   199: aload_0
    //   200: getfield mMaxBinderCallStatsCount : I
    //   203: istore #4
    //   205: lload #14
    //   207: iload #4
    //   209: i2l
    //   210: lcmp
    //   211: iflt -> 220
    //   214: iconst_1
    //   215: istore #19
    //   217: goto -> 223
    //   220: iconst_0
    //   221: istore #19
    //   223: aload #12
    //   225: astore #20
    //   227: aload #13
    //   229: iload #11
    //   231: aload #18
    //   233: iload #5
    //   235: iload #10
    //   237: iload #19
    //   239: invokevirtual getOrCreate : (ILjava/lang/Class;IZZ)Lcom/android/internal/os/BinderCallsStats$CallStat;
    //   242: astore #18
    //   244: aload #18
    //   246: getfield callCount : J
    //   249: lstore #14
    //   251: lload #14
    //   253: lconst_0
    //   254: lcmp
    //   255: ifne -> 264
    //   258: iconst_1
    //   259: istore #4
    //   261: goto -> 267
    //   264: iconst_0
    //   265: istore #4
    //   267: iload #4
    //   269: ifeq -> 293
    //   272: aload_0
    //   273: aload_0
    //   274: getfield mCallStatsCount : J
    //   277: lconst_1
    //   278: ladd
    //   279: putfield mCallStatsCount : J
    //   282: goto -> 293
    //   285: astore_1
    //   286: aload #20
    //   288: astore #12
    //   290: goto -> 551
    //   293: aload #18
    //   295: aload #18
    //   297: getfield callCount : J
    //   300: lconst_1
    //   301: ladd
    //   302: putfield callCount : J
    //   305: aload #18
    //   307: aload #18
    //   309: getfield recordedCallCount : J
    //   312: lconst_1
    //   313: ladd
    //   314: putfield recordedCallCount : J
    //   317: aload #18
    //   319: aload #18
    //   321: getfield cpuTimeMicros : J
    //   324: lload #6
    //   326: ladd
    //   327: putfield cpuTimeMicros : J
    //   330: aload #18
    //   332: aload #18
    //   334: getfield maxCpuTimeMicros : J
    //   337: lload #6
    //   339: invokestatic max : (JJ)J
    //   342: putfield maxCpuTimeMicros : J
    //   345: aload #18
    //   347: aload #18
    //   349: getfield latencyMicros : J
    //   352: lload #8
    //   354: ladd
    //   355: putfield latencyMicros : J
    //   358: aload #18
    //   360: getfield maxLatencyMicros : J
    //   363: lstore #6
    //   365: aload #18
    //   367: lload #6
    //   369: lload #8
    //   371: invokestatic max : (JJ)J
    //   374: putfield maxLatencyMicros : J
    //   377: aload_0
    //   378: getfield mDetailedTracking : Z
    //   381: ifeq -> 475
    //   384: aload #18
    //   386: getfield exceptionCount : J
    //   389: lstore #6
    //   391: aload_1
    //   392: getfield exceptionThrown : Z
    //   395: ifeq -> 405
    //   398: lload #16
    //   400: lstore #8
    //   402: goto -> 408
    //   405: lconst_0
    //   406: lstore #8
    //   408: aload #18
    //   410: lload #6
    //   412: lload #8
    //   414: ladd
    //   415: putfield exceptionCount : J
    //   418: aload #18
    //   420: getfield maxRequestSizeBytes : J
    //   423: lstore #6
    //   425: iload_2
    //   426: i2l
    //   427: lstore #8
    //   429: aload #18
    //   431: lload #6
    //   433: lload #8
    //   435: invokestatic max : (JJ)J
    //   438: putfield maxRequestSizeBytes : J
    //   441: aload #18
    //   443: getfield maxReplySizeBytes : J
    //   446: lstore #6
    //   448: iload_3
    //   449: i2l
    //   450: lstore #8
    //   452: aload #18
    //   454: lload #6
    //   456: lload #8
    //   458: invokestatic max : (JJ)J
    //   461: putfield maxReplySizeBytes : J
    //   464: goto -> 475
    //   467: astore_1
    //   468: aload #20
    //   470: astore #12
    //   472: goto -> 551
    //   475: goto -> 534
    //   478: astore_1
    //   479: aload #20
    //   481: astore #12
    //   483: goto -> 551
    //   486: astore_1
    //   487: goto -> 551
    //   490: aload #12
    //   492: astore #18
    //   494: aload #18
    //   496: astore #20
    //   498: aload #13
    //   500: iload #11
    //   502: aload_1
    //   503: getfield binderClass : Ljava/lang/Class;
    //   506: aload_1
    //   507: getfield transactionCode : I
    //   510: iload #10
    //   512: invokevirtual get : (ILjava/lang/Class;IZ)Lcom/android/internal/os/BinderCallsStats$CallStat;
    //   515: astore_1
    //   516: aload_1
    //   517: ifnull -> 534
    //   520: aload #18
    //   522: astore #20
    //   524: aload_1
    //   525: aload_1
    //   526: getfield callCount : J
    //   529: lconst_1
    //   530: ladd
    //   531: putfield callCount : J
    //   534: aload #12
    //   536: astore #20
    //   538: aload #12
    //   540: monitorexit
    //   541: return
    //   542: aload #12
    //   544: astore #20
    //   546: aload #12
    //   548: monitorexit
    //   549: return
    //   550: astore_1
    //   551: aload #12
    //   553: astore #20
    //   555: aload #12
    //   557: monitorexit
    //   558: aload_1
    //   559: athrow
    //   560: astore_1
    //   561: aload #20
    //   563: astore #12
    //   565: goto -> 551
    // Line number table:
    //   Java source line number -> byte code offset
    //   #157	-> 0
    //   #160	-> 18
    //   #161	-> 23
    //   #162	-> 34
    //   #164	-> 48
    //   #165	-> 51
    //   #167	-> 54
    //   #168	-> 61
    //   #169	-> 73
    //   #170	-> 76
    //   #171	-> 83
    //   #172	-> 92
    //   #174	-> 95
    //   #176	-> 104
    //   #180	-> 124
    //   #181	-> 132
    //   #183	-> 151
    //   #184	-> 156
    //   #185	-> 169
    //   #187	-> 181
    //   #191	-> 244
    //   #192	-> 267
    //   #193	-> 272
    //   #220	-> 285
    //   #196	-> 293
    //   #197	-> 305
    //   #198	-> 317
    //   #199	-> 330
    //   #200	-> 345
    //   #201	-> 358
    //   #202	-> 365
    //   #203	-> 377
    //   #204	-> 384
    //   #205	-> 418
    //   #206	-> 429
    //   #207	-> 441
    //   #208	-> 452
    //   #220	-> 467
    //   #203	-> 475
    //   #210	-> 475
    //   #220	-> 478
    //   #213	-> 490
    //   #216	-> 516
    //   #217	-> 520
    //   #220	-> 534
    //   #221	-> 541
    //   #176	-> 542
    //   #177	-> 542
    //   #220	-> 550
    // Exception table:
    //   from	to	target	type
    //   104	121	550	finally
    //   124	132	550	finally
    //   132	139	550	finally
    //   142	151	550	finally
    //   156	169	550	finally
    //   169	181	550	finally
    //   181	199	550	finally
    //   199	205	486	finally
    //   227	244	478	finally
    //   244	251	478	finally
    //   272	282	285	finally
    //   293	305	478	finally
    //   305	317	478	finally
    //   317	330	478	finally
    //   330	345	478	finally
    //   345	358	478	finally
    //   358	365	478	finally
    //   365	377	478	finally
    //   377	384	478	finally
    //   384	398	478	finally
    //   408	418	478	finally
    //   418	425	478	finally
    //   429	441	467	finally
    //   441	448	467	finally
    //   452	464	560	finally
    //   498	516	560	finally
    //   524	534	560	finally
    //   538	541	560	finally
    //   546	549	560	finally
    //   555	558	560	finally
  }
  
  private UidEntry getUidEntry(int paramInt) {
    UidEntry uidEntry1 = this.mUidEntries.get(paramInt);
    UidEntry uidEntry2 = uidEntry1;
    if (uidEntry1 == null) {
      uidEntry2 = new UidEntry(paramInt);
      this.mUidEntries.put(paramInt, uidEntry2);
    } 
    return uidEntry2;
  }
  
  public void callThrewException(BinderInternal.CallSession paramCallSession, Exception paramException) {
    if (paramCallSession == null)
      return; 
    int i = 1;
    paramCallSession.exceptionThrown = true;
    try {
      null = paramException.getClass().getName();
      synchronized (this.mLock) {
        if (this.mExceptionCounts.size() >= 50)
          null = "overflow"; 
        Integer integer = this.mExceptionCounts.get(null);
        ArrayMap<String, Integer> arrayMap = this.mExceptionCounts;
        if (integer != null)
          i = 1 + integer.intValue(); 
        arrayMap.put(null, Integer.valueOf(i));
      } 
    } catch (RuntimeException runtimeException) {
      Slog.wtf("BinderCallsStats", "Unexpected exception while updating mExceptionCounts");
    } 
  }
  
  private Method getDefaultTransactionNameMethod(Class<? extends Binder> paramClass) {
    try {
      return paramClass.getMethod("getDefaultTransactionName", new Class[] { int.class });
    } catch (NoSuchMethodException noSuchMethodException) {
      return null;
    } 
  }
  
  private String resolveTransactionCode(Method paramMethod, int paramInt) {
    if (paramMethod == null)
      return null; 
    try {
      return (String)paramMethod.invoke(null, new Object[] { Integer.valueOf(paramInt) });
    } catch (IllegalAccessException|java.lang.reflect.InvocationTargetException|ClassCastException illegalAccessException) {
      throw new RuntimeException(illegalAccessException);
    } 
  }
  
  public ArrayList<ExportedCallStat> getExportedCallStats() {
    if (!this.mDetailedTracking)
      return new ArrayList<>(); 
    ArrayList<ExportedCallStat> arrayList = new ArrayList();
    synchronized (this.mLock) {
      int i = this.mUidEntries.size();
      byte b;
      for (b = 0; b < i; b++) {
        UidEntry uidEntry = this.mUidEntries.valueAt(b);
        for (CallStat callStat : uidEntry.getCallStatsList()) {
          ExportedCallStat exportedCallStat = new ExportedCallStat();
          this();
          exportedCallStat.workSourceUid = uidEntry.workSourceUid;
          exportedCallStat.callingUid = callStat.callingUid;
          exportedCallStat.className = callStat.binderClass.getName();
          exportedCallStat.binderClass = callStat.binderClass;
          exportedCallStat.transactionCode = callStat.transactionCode;
          exportedCallStat.screenInteractive = callStat.screenInteractive;
          exportedCallStat.cpuTimeMicros = callStat.cpuTimeMicros;
          exportedCallStat.maxCpuTimeMicros = callStat.maxCpuTimeMicros;
          exportedCallStat.latencyMicros = callStat.latencyMicros;
          exportedCallStat.maxLatencyMicros = callStat.maxLatencyMicros;
          exportedCallStat.recordedCallCount = callStat.recordedCallCount;
          exportedCallStat.callCount = callStat.callCount;
          exportedCallStat.maxRequestSizeBytes = callStat.maxRequestSizeBytes;
          exportedCallStat.maxReplySizeBytes = callStat.maxReplySizeBytes;
          exportedCallStat.exceptionCount = callStat.exceptionCount;
          arrayList.add(exportedCallStat);
        } 
      } 
      Method method = null;
      null = null;
      arrayList.sort((Comparator<? super ExportedCallStat>)_$$Lambda$BinderCallsStats$sqXweH5BoxhmZvI188ctqYiACRk.INSTANCE);
      for (ExportedCallStat exportedCallStat : arrayList) {
        b = 0;
        if (false)
          throw new NullPointerException(); 
        if (true)
          method = getDefaultTransactionNameMethod(exportedCallStat.binderClass); 
        if (false)
          throw new NullPointerException(); 
        b = 1;
        if (true || b != 0) {
          null = resolveTransactionCode(method, exportedCallStat.transactionCode);
          if (null == null)
            null = String.valueOf(exportedCallStat.transactionCode); 
        } 
        Object object = null;
        exportedCallStat.methodName = (String)null;
        null = object;
      } 
      if (this.mAddDebugEntries && this.mBatteryStopwatch != null) {
        arrayList.add(createDebugEntry("start_time_millis", this.mStartElapsedTime));
        arrayList.add(createDebugEntry("end_time_millis", SystemClock.elapsedRealtime()));
        null = this.mBatteryStopwatch;
        null = createDebugEntry("battery_time_millis", null.getMillis());
        arrayList.add(null);
        arrayList.add(createDebugEntry("sampling_interval", this.mPeriodicSamplingInterval));
      } 
      return arrayList;
    } 
  }
  
  private ExportedCallStat createDebugEntry(String paramString, long paramLong) {
    int i = Process.myUid();
    ExportedCallStat exportedCallStat = new ExportedCallStat();
    exportedCallStat.className = "";
    exportedCallStat.workSourceUid = i;
    exportedCallStat.callingUid = i;
    exportedCallStat.recordedCallCount = 1L;
    exportedCallStat.callCount = 1L;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("__DEBUG_");
    stringBuilder.append(paramString);
    exportedCallStat.methodName = stringBuilder.toString();
    exportedCallStat.latencyMicros = paramLong;
    return exportedCallStat;
  }
  
  public ArrayMap<String, Integer> getExportedExceptionStats() {
    synchronized (this.mLock) {
      ArrayMap<Object, Object> arrayMap = new ArrayMap<>();
      this((ArrayMap)this.mExceptionCounts);
      return (ArrayMap)arrayMap;
    } 
  }
  
  public void dump(PrintWriter paramPrintWriter, AppIdToPackageMap paramAppIdToPackageMap, boolean paramBoolean) {
    synchronized (this.mLock) {
      dumpLocked(paramPrintWriter, paramAppIdToPackageMap, paramBoolean);
      return;
    } 
  }
  
  private void dumpLocked(PrintWriter paramPrintWriter, AppIdToPackageMap paramAppIdToPackageMap, boolean paramBoolean) {
    List<UidEntry> list;
    long l1 = 0L;
    long l2 = 0L;
    long l3 = 0L;
    paramPrintWriter.print("Start time: ");
    paramPrintWriter.println(DateFormat.format("yyyy-MM-dd HH:mm:ss", this.mStartCurrentTime));
    paramPrintWriter.print("On battery time (ms): ");
    CachedDeviceState.TimeInStateStopwatch timeInStateStopwatch = this.mBatteryStopwatch;
    if (timeInStateStopwatch != null) {
      l4 = timeInStateStopwatch.getMillis();
    } else {
      l4 = 0L;
    } 
    paramPrintWriter.println(l4);
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("Sampling interval period: ");
    stringBuilder1.append(this.mPeriodicSamplingInterval);
    paramPrintWriter.println(stringBuilder1.toString());
    ArrayList<UidEntry> arrayList1 = new ArrayList();
    int i = this.mUidEntries.size();
    long l4;
    int j;
    for (j = 0, l4 = l2; j < i; j++) {
      UidEntry uidEntry = this.mUidEntries.valueAt(j);
      arrayList1.add(uidEntry);
      l3 += uidEntry.cpuTimeMicros;
      l4 += uidEntry.recordedCallCount;
      l1 += uidEntry.callCount;
    } 
    arrayList1.sort(Comparator.<UidEntry>comparingDouble((ToDoubleFunction<? super UidEntry>)_$$Lambda$BinderCallsStats$iPOmTqbqUiHzgsAugINuZgf9tls.INSTANCE).reversed());
    String str2 = "";
    if (paramBoolean) {
      str1 = "";
    } else {
      str1 = "(top 90% by cpu time) ";
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    StringBuilder stringBuilder4 = new StringBuilder();
    stringBuilder4.append("Per-UID raw data ");
    stringBuilder4.append(str1);
    stringBuilder4.append("(package/uid, worksource, call_desc, screen_interactive, cpu_time_micros, max_cpu_time_micros, latency_time_micros, max_latency_time_micros, exception_count, max_request_size_bytes, max_reply_size_bytes, recorded_call_count, call_count):");
    paramPrintWriter.println(stringBuilder4.toString());
    ArrayList<ExportedCallStat> arrayList2 = getExportedCallStats();
    arrayList2.sort((Comparator<? super ExportedCallStat>)_$$Lambda$BinderCallsStats$233x_Qux4c_AiqShYaWwvFplEXs.INSTANCE);
    for (Iterator<ExportedCallStat> iterator = arrayList2.iterator(); iterator.hasNext(); ) {
      ExportedCallStat exportedCallStat = iterator.next();
      if (exportedCallStat.methodName.startsWith("__DEBUG_"))
        continue; 
      stringBuilder2.setLength(0);
      stringBuilder2.append("    ");
      i = exportedCallStat.callingUid;
      stringBuilder2.append(paramAppIdToPackageMap.mapUid(i));
      stringBuilder2.append(',');
      i = exportedCallStat.workSourceUid;
      stringBuilder2.append(paramAppIdToPackageMap.mapUid(i));
      stringBuilder2.append(',');
      stringBuilder2.append(exportedCallStat.className);
      stringBuilder2.append('#');
      stringBuilder2.append(exportedCallStat.methodName);
      stringBuilder2.append(',');
      stringBuilder2.append(exportedCallStat.screenInteractive);
      stringBuilder2.append(',');
      stringBuilder2.append(exportedCallStat.cpuTimeMicros);
      stringBuilder2.append(',');
      stringBuilder2.append(exportedCallStat.maxCpuTimeMicros);
      stringBuilder2.append(',');
      stringBuilder2.append(exportedCallStat.latencyMicros);
      stringBuilder2.append(',');
      stringBuilder2.append(exportedCallStat.maxLatencyMicros);
      stringBuilder2.append(',');
      if (this.mDetailedTracking) {
        l2 = exportedCallStat.exceptionCount;
      } else {
        l2 = 95L;
      } 
      stringBuilder2.append(l2);
      stringBuilder2.append(',');
      if (this.mDetailedTracking) {
        l2 = exportedCallStat.maxRequestSizeBytes;
      } else {
        l2 = 95L;
      } 
      stringBuilder2.append(l2);
      stringBuilder2.append(',');
      if (this.mDetailedTracking) {
        l2 = exportedCallStat.maxReplySizeBytes;
      } else {
        l2 = 95L;
      } 
      stringBuilder2.append(l2);
      stringBuilder2.append(',');
      stringBuilder2.append(exportedCallStat.recordedCallCount);
      stringBuilder2.append(',');
      stringBuilder2.append(exportedCallStat.callCount);
      paramPrintWriter.println(stringBuilder2);
    } 
    paramPrintWriter.println();
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append("Per-UID Summary ");
    stringBuilder3.append(str1);
    stringBuilder3.append("(cpu_time, % of total cpu_time, recorded_call_count, call_count, package/uid):");
    paramPrintWriter.println(stringBuilder3.toString());
    if (paramBoolean) {
      list = arrayList1;
    } else {
      list = getHighestValues(arrayList1, (ToDoubleFunction<UidEntry>)_$$Lambda$BinderCallsStats$xI0E0RpviGYsokEB7ojNx8LEbUc.INSTANCE, 0.9D);
    } 
    String str1, str3;
    StringBuilder stringBuilder5;
    Iterator<UidEntry> iterator1;
    for (iterator1 = list.iterator(), stringBuilder5 = stringBuilder2, str3 = str1, str1 = str2; iterator1.hasNext(); ) {
      UidEntry uidEntry = iterator1.next();
      str2 = paramAppIdToPackageMap.mapUid(uidEntry.workSourceUid);
      long l5 = uidEntry.cpuTimeMicros;
      double d1 = uidEntry.cpuTimeMicros * 100.0D / l3;
      long l6 = uidEntry.recordedCallCount;
      l2 = uidEntry.callCount;
      paramPrintWriter.println(String.format("  %10d %3.0f%% %8d %8d %s", new Object[] { Long.valueOf(l5), Double.valueOf(d1), Long.valueOf(l6), Long.valueOf(l2), str2 }));
    } 
    paramPrintWriter.println();
    double d = l3 / l4;
    paramPrintWriter.println(String.format("  Summary: total_cpu_time=%d, calls_count=%d, avg_call_cpu_time=%.0f", new Object[] { Long.valueOf(l3), Long.valueOf(l1), Double.valueOf(d) }));
    paramPrintWriter.println();
    paramPrintWriter.println("Exceptions thrown (exception_count, class_name):");
    ArrayList arrayList = new ArrayList();
    this.mExceptionCounts.entrySet().iterator().forEachRemaining(new _$$Lambda$BinderCallsStats$Vota0PqfoPWckjXH35wE48myGdk(arrayList));
    arrayList.sort((Comparator)_$$Lambda$BinderCallsStats$_YP_7pwoNn8TN0iTmo5Q1r2lQz0.INSTANCE);
    for (Pair pair : arrayList) {
      paramPrintWriter.println(String.format("  %6d %s", new Object[] { pair.second, pair.first }));
    } 
    if (this.mPeriodicSamplingInterval != 1) {
      paramPrintWriter.println(str1);
      paramPrintWriter.println("/!\\ Displayed data is sampled. See sampling interval at the top.");
    } 
  }
  
  protected long getThreadTimeMicro() {
    return SystemClock.currentThreadTimeMicro();
  }
  
  protected int getCallingUid() {
    return Binder.getCallingUid();
  }
  
  protected long getElapsedRealtimeMicro() {
    return SystemClock.elapsedRealtimeNanos() / 1000L;
  }
  
  protected boolean shouldRecordDetailedData() {
    boolean bool;
    if (this.mRandom.nextInt() % this.mPeriodicSamplingInterval == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setDetailedTracking(boolean paramBoolean) {
    synchronized (this.mLock) {
      if (paramBoolean != this.mDetailedTracking) {
        this.mDetailedTracking = paramBoolean;
        reset();
      } 
      return;
    } 
  }
  
  public void setTrackScreenInteractive(boolean paramBoolean) {
    synchronized (this.mLock) {
      if (paramBoolean != this.mTrackScreenInteractive) {
        this.mTrackScreenInteractive = paramBoolean;
        reset();
      } 
      return;
    } 
  }
  
  public void setTrackDirectCallerUid(boolean paramBoolean) {
    synchronized (this.mLock) {
      if (paramBoolean != this.mTrackDirectCallingUid) {
        this.mTrackDirectCallingUid = paramBoolean;
        reset();
      } 
      return;
    } 
  }
  
  public void setAddDebugEntries(boolean paramBoolean) {
    this.mAddDebugEntries = paramBoolean;
  }
  
  public void setMaxBinderCallStats(int paramInt) {
    if (paramInt <= 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Ignored invalid max value (value must be positive): ");
      stringBuilder.append(paramInt);
      Slog.w("BinderCallsStats", stringBuilder.toString());
      return;
    } 
    synchronized (this.mLock) {
      if (paramInt != this.mMaxBinderCallStatsCount) {
        this.mMaxBinderCallStatsCount = paramInt;
        reset();
      } 
      return;
    } 
  }
  
  public void setSamplingInterval(int paramInt) {
    if (paramInt <= 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Ignored invalid sampling interval (value must be positive): ");
      stringBuilder.append(paramInt);
      Slog.w("BinderCallsStats", stringBuilder.toString());
      return;
    } 
    synchronized (this.mLock) {
      if (paramInt != this.mPeriodicSamplingInterval) {
        this.mPeriodicSamplingInterval = paramInt;
        reset();
      } 
      return;
    } 
  }
  
  public void reset() {
    synchronized (this.mLock) {
      this.mCallStatsCount = 0L;
      this.mUidEntries.clear();
      this.mExceptionCounts.clear();
      this.mStartCurrentTime = System.currentTimeMillis();
      this.mStartElapsedTime = SystemClock.elapsedRealtime();
      if (this.mBatteryStopwatch != null)
        this.mBatteryStopwatch.reset(); 
      return;
    } 
  }
  
  class ExportedCallStat {
    Class<? extends Binder> binderClass;
    
    public long callCount;
    
    public int callingUid;
    
    public String className;
    
    public long cpuTimeMicros;
    
    public long exceptionCount;
    
    public long latencyMicros;
    
    public long maxCpuTimeMicros;
    
    public long maxLatencyMicros;
    
    public long maxReplySizeBytes;
    
    public long maxRequestSizeBytes;
    
    public String methodName;
    
    public long recordedCallCount;
    
    public boolean screenInteractive;
    
    int transactionCode;
    
    public int workSourceUid;
  }
  
  class CallStat {
    public final Class<? extends Binder> binderClass;
    
    public long callCount;
    
    public final int callingUid;
    
    public long cpuTimeMicros;
    
    public long exceptionCount;
    
    public long latencyMicros;
    
    public long maxCpuTimeMicros;
    
    public long maxLatencyMicros;
    
    public long maxReplySizeBytes;
    
    public long maxRequestSizeBytes;
    
    public long recordedCallCount;
    
    public final boolean screenInteractive;
    
    public final int transactionCode;
    
    CallStat(BinderCallsStats this$0, Class<? extends Binder> param1Class, int param1Int1, boolean param1Boolean) {
      this.callingUid = this$0;
      this.binderClass = param1Class;
      this.transactionCode = param1Int1;
      this.screenInteractive = param1Boolean;
    }
  }
  
  class CallStatKey {
    public Class<? extends Binder> binderClass;
    
    public int callingUid;
    
    private boolean screenInteractive;
    
    public int transactionCode;
    
    public boolean equals(Object<? extends Binder> param1Object) {
      null = true;
      if (this == param1Object)
        return true; 
      CallStatKey callStatKey = (CallStatKey)param1Object;
      if (this.callingUid == callStatKey.callingUid && this.transactionCode == callStatKey.transactionCode && this.screenInteractive == callStatKey.screenInteractive) {
        param1Object = (Object<? extends Binder>)this.binderClass;
        Class<? extends Binder> clazz = callStatKey.binderClass;
        if (param1Object.equals(clazz))
          return null; 
      } 
      return false;
    }
    
    public int hashCode() {
      char c;
      int i = this.binderClass.hashCode();
      int j = this.transactionCode;
      int k = this.callingUid;
      if (this.screenInteractive) {
        c = 'ӏ';
      } else {
        c = 'ӕ';
      } 
      return ((i * 31 + j) * 31 + k) * 31 + c;
    }
  }
  
  class UidEntry {
    public long callCount;
    
    public long cpuTimeMicros;
    
    private Map<BinderCallsStats.CallStatKey, BinderCallsStats.CallStat> mCallStats;
    
    private BinderCallsStats.CallStatKey mTempKey;
    
    public long recordedCallCount;
    
    public int workSourceUid;
    
    UidEntry(BinderCallsStats this$0) {
      this.mCallStats = new ArrayMap<>();
      this.mTempKey = new BinderCallsStats.CallStatKey();
      this.workSourceUid = this$0;
    }
    
    BinderCallsStats.CallStat get(int param1Int1, Class<? extends Binder> param1Class, int param1Int2, boolean param1Boolean) {
      this.mTempKey.callingUid = param1Int1;
      this.mTempKey.binderClass = param1Class;
      this.mTempKey.transactionCode = param1Int2;
      BinderCallsStats.CallStatKey.access$002(this.mTempKey, param1Boolean);
      return this.mCallStats.get(this.mTempKey);
    }
    
    BinderCallsStats.CallStat getOrCreate(int param1Int1, Class<? extends Binder> param1Class, int param1Int2, boolean param1Boolean1, boolean param1Boolean2) {
      BinderCallsStats.CallStat callStat1 = get(param1Int1, param1Class, param1Int2, param1Boolean1);
      BinderCallsStats.CallStat callStat2 = callStat1;
      if (callStat1 == null) {
        Class<? extends Binder> clazz;
        if (param1Boolean2) {
          BinderCallsStats.CallStat callStat = get(-1, BinderCallsStats.OVERFLOW_BINDER, -1, false);
          if (callStat != null)
            return callStat; 
          param1Int1 = -1;
          clazz = BinderCallsStats.OVERFLOW_BINDER;
          param1Int2 = -1;
          param1Boolean1 = false;
        } 
        callStat2 = new BinderCallsStats.CallStat(param1Int1, clazz, param1Int2, param1Boolean1);
        BinderCallsStats.CallStatKey callStatKey = new BinderCallsStats.CallStatKey();
        callStatKey.callingUid = param1Int1;
        callStatKey.binderClass = clazz;
        callStatKey.transactionCode = param1Int2;
        BinderCallsStats.CallStatKey.access$002(callStatKey, param1Boolean1);
        this.mCallStats.put(callStatKey, callStat2);
      } 
      return callStat2;
    }
    
    public Collection<BinderCallsStats.CallStat> getCallStatsList() {
      return this.mCallStats.values();
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("UidEntry{cpuTimeMicros=");
      stringBuilder.append(this.cpuTimeMicros);
      stringBuilder.append(", callCount=");
      stringBuilder.append(this.callCount);
      stringBuilder.append(", mCallStats=");
      stringBuilder.append(this.mCallStats);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      param1Object = param1Object;
      if (this.workSourceUid != ((UidEntry)param1Object).workSourceUid)
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      return this.workSourceUid;
    }
  }
  
  public SparseArray<UidEntry> getUidEntries() {
    return this.mUidEntries;
  }
  
  public ArrayMap<String, Integer> getExceptionCounts() {
    return this.mExceptionCounts;
  }
  
  public static <T> List<T> getHighestValues(List<T> paramList, ToDoubleFunction<T> paramToDoubleFunction, double paramDouble) {
    // Byte code:
    //   0: new java/util/ArrayList
    //   3: dup
    //   4: aload_0
    //   5: invokespecial <init> : (Ljava/util/Collection;)V
    //   8: astore #4
    //   10: aload #4
    //   12: aload_1
    //   13: invokestatic comparingDouble : (Ljava/util/function/ToDoubleFunction;)Ljava/util/Comparator;
    //   16: invokeinterface reversed : ()Ljava/util/Comparator;
    //   21: invokeinterface sort : (Ljava/util/Comparator;)V
    //   26: dconst_0
    //   27: dstore #5
    //   29: aload_0
    //   30: invokeinterface iterator : ()Ljava/util/Iterator;
    //   35: astore_0
    //   36: aload_0
    //   37: invokeinterface hasNext : ()Z
    //   42: ifeq -> 69
    //   45: aload_0
    //   46: invokeinterface next : ()Ljava/lang/Object;
    //   51: astore #7
    //   53: dload #5
    //   55: aload_1
    //   56: aload #7
    //   58: invokeinterface applyAsDouble : (Ljava/lang/Object;)D
    //   63: dadd
    //   64: dstore #5
    //   66: goto -> 36
    //   69: new java/util/ArrayList
    //   72: dup
    //   73: invokespecial <init> : ()V
    //   76: astore_0
    //   77: dconst_0
    //   78: dstore #8
    //   80: aload #4
    //   82: invokeinterface iterator : ()Ljava/util/Iterator;
    //   87: astore #7
    //   89: aload #7
    //   91: invokeinterface hasNext : ()Z
    //   96: ifeq -> 146
    //   99: aload #7
    //   101: invokeinterface next : ()Ljava/lang/Object;
    //   106: astore #4
    //   108: dload #8
    //   110: dload_2
    //   111: dload #5
    //   113: dmul
    //   114: dcmpl
    //   115: ifle -> 121
    //   118: goto -> 146
    //   121: aload_0
    //   122: aload #4
    //   124: invokeinterface add : (Ljava/lang/Object;)Z
    //   129: pop
    //   130: dload #8
    //   132: aload_1
    //   133: aload #4
    //   135: invokeinterface applyAsDouble : (Ljava/lang/Object;)D
    //   140: dadd
    //   141: dstore #8
    //   143: goto -> 89
    //   146: aload_0
    //   147: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #773	-> 0
    //   #774	-> 10
    //   #775	-> 26
    //   #776	-> 29
    //   #777	-> 53
    //   #778	-> 66
    //   #779	-> 69
    //   #780	-> 77
    //   #781	-> 80
    //   #782	-> 108
    //   #783	-> 118
    //   #785	-> 121
    //   #786	-> 130
    //   #787	-> 143
    //   #788	-> 146
  }
  
  private static int compareByCpuDesc(ExportedCallStat paramExportedCallStat1, ExportedCallStat paramExportedCallStat2) {
    return Long.compare(paramExportedCallStat2.cpuTimeMicros, paramExportedCallStat1.cpuTimeMicros);
  }
  
  private static int compareByBinderClassAndCode(ExportedCallStat paramExportedCallStat1, ExportedCallStat paramExportedCallStat2) {
    int i = paramExportedCallStat1.className.compareTo(paramExportedCallStat2.className);
    if (i == 0)
      i = Integer.compare(paramExportedCallStat1.transactionCode, paramExportedCallStat2.transactionCode); 
    return i;
  }
}
