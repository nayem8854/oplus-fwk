package com.android.internal.os;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.provider.Settings;
import android.util.KeyValueListParser;
import android.util.Range;
import android.util.Slog;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class KernelCpuThreadReaderSettingsObserver extends ContentObserver {
  private static final String COLLECTED_UIDS_DEFAULT = "0-0;1000-1000";
  
  private static final String COLLECTED_UIDS_SETTINGS_KEY = "collected_uids";
  
  private static final int MINIMUM_TOTAL_CPU_USAGE_MILLIS_DEFAULT = 10000;
  
  private static final String MINIMUM_TOTAL_CPU_USAGE_MILLIS_SETTINGS_KEY = "minimum_total_cpu_usage_millis";
  
  private static final int NUM_BUCKETS_DEFAULT = 8;
  
  private static final String NUM_BUCKETS_SETTINGS_KEY = "num_buckets";
  
  private static final String TAG = "KernelCpuThreadReaderSettingsObserver";
  
  private final Context mContext;
  
  private final KernelCpuThreadReader mKernelCpuThreadReader;
  
  private final KernelCpuThreadReaderDiff mKernelCpuThreadReaderDiff;
  
  public static KernelCpuThreadReaderDiff getSettingsModifiedReader(Context paramContext) {
    KernelCpuThreadReaderSettingsObserver kernelCpuThreadReaderSettingsObserver = new KernelCpuThreadReaderSettingsObserver(paramContext);
    Uri uri = Settings.Global.getUriFor("kernel_cpu_thread_reader");
    ContentResolver contentResolver = paramContext.getContentResolver();
    contentResolver.registerContentObserver(uri, false, kernelCpuThreadReaderSettingsObserver, 0);
    return kernelCpuThreadReaderSettingsObserver.mKernelCpuThreadReaderDiff;
  }
  
  private KernelCpuThreadReaderSettingsObserver(Context paramContext) {
    super(BackgroundThread.getHandler());
    KernelCpuThreadReaderDiff kernelCpuThreadReaderDiff;
    this.mContext = paramContext;
    UidPredicate uidPredicate = UidPredicate.fromString("0-0;1000-1000");
    KernelCpuThreadReader kernelCpuThreadReader = KernelCpuThreadReader.create(8, uidPredicate);
    if (kernelCpuThreadReader == null) {
      kernelCpuThreadReader = null;
    } else {
      kernelCpuThreadReaderDiff = new KernelCpuThreadReaderDiff(kernelCpuThreadReader, 10000);
    } 
    this.mKernelCpuThreadReaderDiff = kernelCpuThreadReaderDiff;
  }
  
  public void onChange(boolean paramBoolean, Collection<Uri> paramCollection, int paramInt1, int paramInt2) {
    updateReader();
  }
  
  private void updateReader() {
    if (this.mKernelCpuThreadReader == null)
      return; 
    KeyValueListParser keyValueListParser = new KeyValueListParser(',');
    try {
      Context context = this.mContext;
      ContentResolver contentResolver = context.getContentResolver();
      String str = Settings.Global.getString(contentResolver, "kernel_cpu_thread_reader");
      keyValueListParser.setString(str);
      try {
        str = keyValueListParser.getString("collected_uids", "0-0;1000-1000");
        UidPredicate uidPredicate = UidPredicate.fromString(str);
        KernelCpuThreadReader kernelCpuThreadReader = this.mKernelCpuThreadReader;
        int i = keyValueListParser.getInt("num_buckets", 8);
        kernelCpuThreadReader.setNumBuckets(i);
        this.mKernelCpuThreadReader.setUidPredicate(uidPredicate);
        KernelCpuThreadReaderDiff kernelCpuThreadReaderDiff = this.mKernelCpuThreadReaderDiff;
        i = keyValueListParser.getInt("minimum_total_cpu_usage_millis", 10000);
        kernelCpuThreadReaderDiff.setMinimumTotalCpuUsageMillis(i);
        return;
      } catch (NumberFormatException numberFormatException) {
        Slog.w("KernelCpuThreadReaderSettingsObserver", "Failed to get UID predicate", numberFormatException);
        return;
      } 
    } catch (IllegalArgumentException illegalArgumentException) {
      Slog.e("KernelCpuThreadReaderSettingsObserver", "Bad settings", illegalArgumentException);
      return;
    } 
  }
  
  class UidPredicate implements Predicate<Integer> {
    private static final Pattern UID_RANGE_PATTERN = Pattern.compile("([0-9]+)-([0-9]+)");
    
    private static final String UID_SPECIFIER_DELIMITER = ";";
    
    private final List<Range<Integer>> mAcceptedUidRanges;
    
    public static UidPredicate fromString(String param1String) throws NumberFormatException {
      ArrayList<Range<Integer>> arrayList = new ArrayList();
      String[] arrayOfString;
      int i;
      byte b;
      for (arrayOfString = param1String.split(";"), i = arrayOfString.length, b = 0; b < i; ) {
        Range<Integer> range;
        String str = arrayOfString[b];
        Matcher matcher = UID_RANGE_PATTERN.matcher(str);
        if (matcher.matches()) {
          int j = Integer.parseInt(matcher.group(1));
          int k = Integer.parseInt(matcher.group(2));
          range = Range.create(Integer.valueOf(j), Integer.valueOf(k));
          arrayList.add(range);
          b++;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to recognize as number range: ");
        stringBuilder.append((String)range);
        throw new NumberFormatException(stringBuilder.toString());
      } 
      return new UidPredicate(arrayList);
    }
    
    private UidPredicate(KernelCpuThreadReaderSettingsObserver this$0) {
      this.mAcceptedUidRanges = (List<Range<Integer>>)this$0;
    }
    
    public boolean test(Integer param1Integer) {
      for (byte b = 0; b < this.mAcceptedUidRanges.size(); b++) {
        if (((Range<Integer>)this.mAcceptedUidRanges.get(b)).contains(param1Integer))
          return true; 
      } 
      return false;
    }
  }
}
