package com.android.internal.os;

import android.os.BatteryStats;
import android.util.ArrayMap;

public class WakelockPowerCalculator extends PowerCalculator {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "WakelockPowerCalculator";
  
  private final double mPowerWakelock;
  
  private long mTotalAppWakelockTimeMs = 0L;
  
  public WakelockPowerCalculator(PowerProfile paramPowerProfile) {
    this.mPowerWakelock = paramPowerProfile.getAveragePower("cpu.idle");
  }
  
  public void calculateApp(BatterySipper paramBatterySipper, BatteryStats.Uid paramUid, long paramLong1, long paramLong2, int paramInt) {
    paramLong2 = 0L;
    ArrayMap arrayMap = paramUid.getWakelockStats();
    int i = arrayMap.size();
    for (byte b = 0; b < i; b++) {
      BatteryStats.Uid.Wakelock wakelock = (BatteryStats.Uid.Wakelock)arrayMap.valueAt(b);
      BatteryStats.Timer timer = wakelock.getWakeTime(0);
      if (timer != null)
        paramLong2 += timer.getTotalTimeLocked(paramLong1, paramInt); 
    } 
    paramBatterySipper.wakeLockTimeMs = paramLong2 / 1000L;
    this.mTotalAppWakelockTimeMs += paramBatterySipper.wakeLockTimeMs;
    paramBatterySipper.wakeLockPowerMah = paramBatterySipper.wakeLockTimeMs * this.mPowerWakelock / 3600000.0D;
  }
  
  public void calculateRemaining(BatterySipper paramBatterySipper, BatteryStats paramBatteryStats, long paramLong1, long paramLong2, int paramInt) {
    long l = paramBatteryStats.getBatteryUptime(paramLong2) / 1000L;
    paramLong2 = this.mTotalAppWakelockTimeMs;
    paramLong1 = l - paramLong2 + paramBatteryStats.getScreenOnTime(paramLong1, paramInt) / 1000L;
    if (paramLong1 > 0L) {
      double d = paramLong1 * this.mPowerWakelock / 3600000.0D;
      paramBatterySipper.wakeLockTimeMs += paramLong1;
      paramBatterySipper.wakeLockPowerMah += d;
    } 
  }
  
  public void reset() {
    this.mTotalAppWakelockTimeMs = 0L;
  }
}
