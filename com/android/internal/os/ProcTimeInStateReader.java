package com.android.internal.os;

import android.os.Process;
import com.android.internal.util.ArrayUtils;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ProcTimeInStateReader {
  private static final String TAG = "ProcTimeInStateReader";
  
  private static final List<Integer> TIME_IN_STATE_HEADER_LINE_FORMAT;
  
  private static final List<Integer> TIME_IN_STATE_LINE_FREQUENCY_FORMAT;
  
  static {
    Integer integer = Integer.valueOf(10);
    TIME_IN_STATE_LINE_FREQUENCY_FORMAT = Arrays.asList(new Integer[] { Integer.valueOf(8224), integer });
  }
  
  private static final List<Integer> TIME_IN_STATE_LINE_TIME_FORMAT = Arrays.asList(new Integer[] { Integer.valueOf(32), Integer.valueOf(8202) });
  
  private long[] mFrequenciesKhz;
  
  private int[] mTimeInStateTimeFormat;
  
  static {
    TIME_IN_STATE_HEADER_LINE_FORMAT = Collections.singletonList(integer);
  }
  
  public ProcTimeInStateReader(Path paramPath) throws IOException {
    initializeTimeInStateFormat(paramPath);
  }
  
  public long[] getUsageTimesMillis(Path paramPath) {
    long[] arrayOfLong = new long[this.mFrequenciesKhz.length];
    String str = paramPath.toString();
    int[] arrayOfInt = this.mTimeInStateTimeFormat;
    boolean bool = Process.readProcFile(str, arrayOfInt, null, arrayOfLong, null);
    if (!bool)
      return null; 
    for (byte b = 0; b < arrayOfLong.length; b++)
      arrayOfLong[b] = arrayOfLong[b] * 10L; 
    return arrayOfLong;
  }
  
  public long[] getFrequenciesKhz() {
    return this.mFrequenciesKhz;
  }
  
  private void initializeTimeInStateFormat(Path paramPath) throws IOException {
    byte[] arrayOfByte = Files.readAllBytes(paramPath);
    ArrayList<Integer> arrayList1 = new ArrayList();
    ArrayList<Integer> arrayList2 = new ArrayList();
    int i;
    byte b;
    for (i = 0, b = 0; i < arrayOfByte.length; i++) {
      if (!Character.isDigit(arrayOfByte[i])) {
        arrayList1.addAll(TIME_IN_STATE_HEADER_LINE_FORMAT);
        arrayList2.addAll(TIME_IN_STATE_HEADER_LINE_FORMAT);
      } else {
        arrayList1.addAll(TIME_IN_STATE_LINE_FREQUENCY_FORMAT);
        arrayList2.addAll(TIME_IN_STATE_LINE_TIME_FORMAT);
        b++;
      } 
      while (i < arrayOfByte.length && arrayOfByte[i] != 10)
        i++; 
    } 
    if (b != 0) {
      long[] arrayOfLong = new long[b];
      i = arrayOfByte.length;
      int[] arrayOfInt = ArrayUtils.convertToIntArray(arrayList1);
      boolean bool = Process.parseProcLine(arrayOfByte, 0, i, arrayOfInt, null, arrayOfLong, null);
      if (bool) {
        this.mTimeInStateTimeFormat = ArrayUtils.convertToIntArray(arrayList2);
        this.mFrequenciesKhz = arrayOfLong;
        return;
      } 
      throw new IOException("Failed to parse time_in_state file");
    } 
    throw new IOException("Empty time_in_state file");
  }
}
