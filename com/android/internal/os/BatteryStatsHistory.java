package com.android.internal.os;

import android.os.BatteryStats;
import android.os.Parcel;
import android.os.StatFs;
import android.os.SystemClock;
import android.util.ArraySet;
import android.util.AtomicFile;
import android.util.Slog;
import com.android.internal.util.ParseUtils;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class BatteryStatsHistory {
  private final List<Integer> mFileNumbers = new ArrayList<>();
  
  private List<Parcel> mHistoryParcels = null;
  
  private int mRecordCount = 0;
  
  private int mParcelIndex = 0;
  
  private static final boolean DEBUG = false;
  
  public static final String FILE_SUFFIX = ".bin";
  
  public static final String HISTORY_DIR = "battery-history";
  
  private static final int MIN_FREE_SPACE = 104857600;
  
  private static final String TAG = "BatteryStatsHistory";
  
  private AtomicFile mActiveFile;
  
  private int mCurrentFileIndex;
  
  private Parcel mCurrentParcel;
  
  private int mCurrentParcelEnd;
  
  private final Parcel mHistoryBuffer;
  
  private final File mHistoryDir;
  
  private final BatteryStatsImpl mStats;
  
  public BatteryStatsHistory(BatteryStatsImpl paramBatteryStatsImpl, File paramFile, Parcel paramParcel) {
    this.mStats = paramBatteryStatsImpl;
    this.mHistoryBuffer = paramParcel;
    File file = new File(paramFile, "battery-history");
    file.mkdirs();
    if (!this.mHistoryDir.exists()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("HistoryDir does not exist:");
      stringBuilder.append(this.mHistoryDir.getPath());
      Slog.wtf("BatteryStatsHistory", stringBuilder.toString());
    } 
    final ArraySet<? extends Integer> dedup = new ArraySet();
    this.mHistoryDir.listFiles(new FilenameFilter() {
          final BatteryStatsHistory this$0;
          
          final Set val$dedup;
          
          public boolean accept(File param1File, String param1String) {
            int i = param1String.lastIndexOf(".bin");
            if (i <= 0)
              return false; 
            Integer integer = Integer.valueOf(ParseUtils.parseInt(param1String.substring(0, i), -1));
            if (integer.intValue() != -1) {
              dedup.add(integer);
              return true;
            } 
            return false;
          }
        });
    if (!arraySet.isEmpty()) {
      this.mFileNumbers.addAll(arraySet);
      Collections.sort(this.mFileNumbers);
      List<Integer> list = this.mFileNumbers;
      setActiveFile(((Integer)list.get(list.size() - 1)).intValue());
    } else {
      this.mFileNumbers.add(Integer.valueOf(0));
      setActiveFile(0);
    } 
  }
  
  public BatteryStatsHistory(BatteryStatsImpl paramBatteryStatsImpl, Parcel paramParcel) {
    this.mStats = paramBatteryStatsImpl;
    this.mHistoryDir = null;
    this.mHistoryBuffer = paramParcel;
  }
  
  private void setActiveFile(int paramInt) {
    this.mActiveFile = getFile(paramInt);
  }
  
  private AtomicFile getFile(int paramInt) {
    File file = this.mHistoryDir;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramInt);
    stringBuilder.append(".bin");
    return new AtomicFile(new File(file, stringBuilder.toString()));
  }
  
  public void startNextFile() {
    if (this.mFileNumbers.isEmpty()) {
      Slog.wtf("BatteryStatsHistory", "mFileNumbers should never be empty");
      return;
    } 
    List<Integer> list = this.mFileNumbers;
    int i = ((Integer)list.get(list.size() - 1)).intValue() + 1;
    this.mFileNumbers.add(Integer.valueOf(i));
    setActiveFile(i);
    if (!hasFreeDiskSpace()) {
      i = ((Integer)this.mFileNumbers.remove(0)).intValue();
      getFile(i).delete();
    } 
    while (this.mFileNumbers.size() > this.mStats.mConstants.MAX_HISTORY_FILES) {
      i = ((Integer)this.mFileNumbers.get(0)).intValue();
      getFile(i).delete();
      this.mFileNumbers.remove(0);
    } 
  }
  
  public void resetAllFiles() {
    for (Integer integer : this.mFileNumbers)
      getFile(integer.intValue()).delete(); 
    this.mFileNumbers.clear();
    this.mFileNumbers.add(Integer.valueOf(0));
    setActiveFile(0);
  }
  
  public boolean startIteratingHistory() {
    this.mRecordCount = 0;
    this.mCurrentFileIndex = 0;
    this.mCurrentParcel = null;
    this.mCurrentParcelEnd = 0;
    this.mParcelIndex = 0;
    return true;
  }
  
  public void finishIteratingHistory() {
    Parcel parcel = this.mHistoryBuffer;
    parcel.setDataPosition(parcel.dataSize());
  }
  
  public Parcel getNextParcel(BatteryStats.HistoryItem paramHistoryItem) {
    if (this.mRecordCount == 0)
      paramHistoryItem.clear(); 
    this.mRecordCount++;
    Parcel parcel = this.mCurrentParcel;
    if (parcel != null) {
      if (parcel.dataPosition() < this.mCurrentParcelEnd)
        return this.mCurrentParcel; 
      Parcel parcel1 = this.mHistoryBuffer;
      parcel = this.mCurrentParcel;
      if (parcel1 == parcel)
        return null; 
      List<Parcel> list = this.mHistoryParcels;
      if (list == null || 
        !list.contains(parcel))
        this.mCurrentParcel.recycle(); 
    } 
    while (this.mCurrentFileIndex < this.mFileNumbers.size() - 1) {
      this.mCurrentParcel = null;
      this.mCurrentParcelEnd = 0;
      parcel = Parcel.obtain();
      List<Integer> list = this.mFileNumbers;
      int i = this.mCurrentFileIndex;
      this.mCurrentFileIndex = i + 1;
      AtomicFile atomicFile = getFile(((Integer)list.get(i)).intValue());
      if (readFileToParcel(parcel, atomicFile)) {
        int j = parcel.readInt();
        i = parcel.dataPosition();
        this.mCurrentParcelEnd = j = i + j;
        this.mCurrentParcel = parcel;
        if (i < j)
          return parcel; 
        continue;
      } 
      parcel.recycle();
    } 
    if (this.mHistoryParcels != null)
      while (this.mParcelIndex < this.mHistoryParcels.size()) {
        List<Parcel> list = this.mHistoryParcels;
        int i = this.mParcelIndex;
        this.mParcelIndex = i + 1;
        Parcel parcel1 = list.get(i);
        if (!skipHead(parcel1))
          continue; 
        int j = parcel1.readInt();
        i = parcel1.dataPosition();
        this.mCurrentParcelEnd = j = i + j;
        this.mCurrentParcel = parcel1;
        if (i < j)
          return parcel1; 
      }  
    if (this.mHistoryBuffer.dataSize() <= 0)
      return null; 
    this.mHistoryBuffer.setDataPosition(0);
    this.mCurrentParcel = parcel = this.mHistoryBuffer;
    this.mCurrentParcelEnd = parcel.dataSize();
    return this.mCurrentParcel;
  }
  
  public boolean readFileToParcel(Parcel paramParcel, AtomicFile paramAtomicFile) {
    try {
      SystemClock.uptimeMillis();
      byte[] arrayOfByte = paramAtomicFile.readFully();
      paramParcel.unmarshall(arrayOfByte, 0, arrayOfByte.length);
      paramParcel.setDataPosition(0);
      return skipHead(paramParcel);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error reading file ");
      stringBuilder.append(paramAtomicFile.getBaseFile().getPath());
      Slog.e("BatteryStatsHistory", stringBuilder.toString(), exception);
      return false;
    } 
  }
  
  private boolean skipHead(Parcel paramParcel) {
    paramParcel.setDataPosition(0);
    int i = paramParcel.readInt();
    if (i != 186)
      return false; 
    paramParcel.readLong();
    return true;
  }
  
  public void writeToParcel(Parcel paramParcel) {
    SystemClock.uptimeMillis();
    paramParcel.writeInt(this.mFileNumbers.size() - 1);
    for (byte b = 0; b < this.mFileNumbers.size() - 1; b++) {
      AtomicFile atomicFile = getFile(((Integer)this.mFileNumbers.get(b)).intValue());
      byte[] arrayOfByte = new byte[0];
      try {
        byte[] arrayOfByte1 = atomicFile.readFully();
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Error reading file ");
        stringBuilder.append(atomicFile.getBaseFile().getPath());
        Slog.e("BatteryStatsHistory", stringBuilder.toString(), exception);
      } 
      paramParcel.writeByteArray(arrayOfByte);
    } 
  }
  
  public void readFromParcel(Parcel paramParcel) {
    SystemClock.uptimeMillis();
    this.mHistoryParcels = new ArrayList<>();
    int i = paramParcel.readInt();
    for (byte b = 0; b < i; b++) {
      byte[] arrayOfByte = paramParcel.createByteArray();
      if (arrayOfByte.length != 0) {
        Parcel parcel = Parcel.obtain();
        parcel.unmarshall(arrayOfByte, 0, arrayOfByte.length);
        parcel.setDataPosition(0);
        this.mHistoryParcels.add(parcel);
      } 
    } 
  }
  
  private boolean hasFreeDiskSpace() {
    boolean bool;
    StatFs statFs = new StatFs(this.mHistoryDir.getAbsolutePath());
    if (statFs.getAvailableBytes() > 104857600L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public List<Integer> getFilesNumbers() {
    return this.mFileNumbers;
  }
  
  public AtomicFile getActiveFile() {
    return this.mActiveFile;
  }
  
  public int getHistoryUsedSize() {
    int i = 0;
    int j;
    for (j = 0; j < this.mFileNumbers.size() - 1; j++)
      i = (int)(i + getFile(((Integer)this.mFileNumbers.get(j)).intValue()).getBaseFile().length()); 
    j = i + this.mHistoryBuffer.dataSize();
    int k = j;
    if (this.mHistoryParcels != null) {
      i = 0;
      while (true) {
        k = j;
        if (i < this.mHistoryParcels.size()) {
          j += ((Parcel)this.mHistoryParcels.get(i)).dataSize();
          i++;
          continue;
        } 
        break;
      } 
    } 
    return k;
  }
}
