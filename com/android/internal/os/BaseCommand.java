package com.android.internal.os;

import android.os.BasicShellCommandHandler;
import java.io.PrintStream;

public abstract class BaseCommand {
  public static final String FATAL_ERROR_CODE = "Error type 1";
  
  public static final String NO_CLASS_ERROR_CODE = "Error type 3";
  
  public static final String NO_SYSTEM_ERROR_CODE = "Error type 2";
  
  protected final BasicShellCommandHandler mArgs = (BasicShellCommandHandler)new Object(this);
  
  private String[] mRawArgs;
  
  public void run(String[] paramArrayOfString) {
    if (paramArrayOfString.length < 1) {
      onShowUsage(System.out);
      return;
    } 
    this.mRawArgs = paramArrayOfString;
    this.mArgs.init(null, null, null, null, paramArrayOfString, 0);
    try {
      onRun();
    } catch (IllegalArgumentException illegalArgumentException) {
      onShowUsage(System.err);
      System.err.println();
      PrintStream printStream = System.err;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error: ");
      stringBuilder.append(illegalArgumentException.getMessage());
      printStream.println(stringBuilder.toString());
    } catch (Exception exception) {
      exception.printStackTrace(System.err);
      System.exit(1);
    } 
  }
  
  public void showUsage() {
    onShowUsage(System.err);
  }
  
  public void showError(String paramString) {
    onShowUsage(System.err);
    System.err.println();
    System.err.println(paramString);
  }
  
  public String nextOption() {
    return this.mArgs.getNextOption();
  }
  
  public String nextArg() {
    return this.mArgs.getNextArg();
  }
  
  public String peekNextArg() {
    return this.mArgs.peekNextArg();
  }
  
  public String nextArgRequired() {
    return this.mArgs.getNextArgRequired();
  }
  
  public String[] getRawArgs() {
    return this.mRawArgs;
  }
  
  public abstract void onRun() throws Exception;
  
  public abstract void onShowUsage(PrintStream paramPrintStream);
}
