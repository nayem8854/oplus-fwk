package com.android.internal.os;

import android.os.BatteryStats;

public class MediaPowerCalculator extends PowerCalculator {
  private static final int MS_IN_HR = 3600000;
  
  private final double mAudioAveragePowerMa;
  
  private final double mVideoAveragePowerMa;
  
  public MediaPowerCalculator(PowerProfile paramPowerProfile) {
    this.mAudioAveragePowerMa = paramPowerProfile.getAveragePower("audio");
    this.mVideoAveragePowerMa = paramPowerProfile.getAveragePower("video");
  }
  
  public void calculateApp(BatterySipper paramBatterySipper, BatteryStats.Uid paramUid, long paramLong1, long paramLong2, int paramInt) {
    BatteryStats.Timer timer2 = paramUid.getAudioTurnedOnTimer();
    if (timer2 == null) {
      paramBatterySipper.audioTimeMs = 0L;
      paramBatterySipper.audioPowerMah = 0.0D;
    } else {
      paramLong2 = timer2.getTotalTimeLocked(paramLong1, paramInt) / 1000L;
      paramBatterySipper.audioTimeMs = paramLong2;
      paramBatterySipper.audioPowerMah = paramLong2 * this.mAudioAveragePowerMa / 3600000.0D;
    } 
    BatteryStats.Timer timer1 = paramUid.getVideoTurnedOnTimer();
    if (timer1 == null) {
      paramBatterySipper.videoTimeMs = 0L;
      paramBatterySipper.videoPowerMah = 0.0D;
    } else {
      paramLong1 = timer1.getTotalTimeLocked(paramLong1, paramInt) / 1000L;
      paramBatterySipper.videoTimeMs = paramLong1;
      paramBatterySipper.videoPowerMah = paramLong1 * this.mVideoAveragePowerMa / 3600000.0D;
    } 
  }
}
