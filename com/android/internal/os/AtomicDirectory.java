package com.android.internal.os;

import android.os.FileUtils;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.util.ArrayMap;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

public final class AtomicDirectory {
  private static final String LOG_TAG = AtomicDirectory.class.getSimpleName();
  
  private final File mBackupDirectory;
  
  private final File mBaseDirectory;
  
  private final ArrayMap<File, FileOutputStream> mOpenFiles = new ArrayMap<>();
  
  public AtomicDirectory(File paramFile) {
    Preconditions.checkNotNull(paramFile, "baseDirectory cannot be null");
    this.mBaseDirectory = paramFile;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramFile.getPath());
    stringBuilder.append("_bak");
    this.mBackupDirectory = new File(stringBuilder.toString());
  }
  
  public File getBackupDirectory() {
    return this.mBackupDirectory;
  }
  
  public File startRead() throws IOException {
    restore();
    ensureBaseDirectory();
    return this.mBaseDirectory;
  }
  
  public void finishRead() {}
  
  public File startWrite() throws IOException {
    backup();
    ensureBaseDirectory();
    return this.mBaseDirectory;
  }
  
  public FileOutputStream openWrite(File paramFile) throws IOException {
    if (!paramFile.isDirectory() && paramFile.getParentFile().equals(this.mBaseDirectory)) {
      if (!this.mOpenFiles.containsKey(paramFile)) {
        FileOutputStream fileOutputStream = new FileOutputStream(paramFile);
        this.mOpenFiles.put(paramFile, fileOutputStream);
        return fileOutputStream;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Already open file ");
      stringBuilder1.append(paramFile.getAbsolutePath());
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Must be a file in ");
    stringBuilder.append(this.mBaseDirectory);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void closeWrite(FileOutputStream paramFileOutputStream) {
    int i = this.mOpenFiles.indexOfValue(paramFileOutputStream);
    if (i >= 0) {
      this.mOpenFiles.removeAt(i);
      FileUtils.sync(paramFileOutputStream);
      FileUtils.closeQuietly(paramFileOutputStream);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unknown file stream ");
    stringBuilder.append(paramFileOutputStream);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void failWrite(FileOutputStream paramFileOutputStream) {
    int i = this.mOpenFiles.indexOfValue(paramFileOutputStream);
    if (i >= 0) {
      this.mOpenFiles.removeAt(i);
      FileUtils.closeQuietly(paramFileOutputStream);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unknown file stream ");
    stringBuilder.append(paramFileOutputStream);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void finishWrite() {
    throwIfSomeFilesOpen();
    syncDirectory(this.mBaseDirectory);
    syncParentDirectory();
    deleteDirectory(this.mBackupDirectory);
    syncParentDirectory();
  }
  
  public void failWrite() {
    throwIfSomeFilesOpen();
    try {
      restore();
    } catch (IOException iOException) {
      Log.e(LOG_TAG, "Failed to restore in failWrite()", iOException);
    } 
  }
  
  public boolean exists() {
    return (this.mBaseDirectory.exists() || this.mBackupDirectory.exists());
  }
  
  public void delete() {
    boolean bool;
    int i = 0;
    if (this.mBaseDirectory.exists())
      i = false | deleteDirectory(this.mBaseDirectory); 
    int j = i;
    if (this.mBackupDirectory.exists())
      bool = i | deleteDirectory(this.mBackupDirectory); 
    if (bool)
      syncParentDirectory(); 
  }
  
  private void ensureBaseDirectory() throws IOException {
    if (this.mBaseDirectory.exists())
      return; 
    if (this.mBaseDirectory.mkdirs()) {
      FileUtils.setPermissions(this.mBaseDirectory.getPath(), 505, -1, -1);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Failed to create directory ");
    stringBuilder.append(this.mBaseDirectory);
    throw new IOException(stringBuilder.toString());
  }
  
  private void throwIfSomeFilesOpen() {
    if (this.mOpenFiles.isEmpty())
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unclosed files: ");
    ArrayMap<File, FileOutputStream> arrayMap = this.mOpenFiles;
    stringBuilder.append(Arrays.toString(arrayMap.keySet().toArray()));
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  private void backup() throws IOException {
    if (!this.mBaseDirectory.exists())
      return; 
    if (this.mBackupDirectory.exists())
      deleteDirectory(this.mBackupDirectory); 
    if (this.mBaseDirectory.renameTo(this.mBackupDirectory)) {
      syncParentDirectory();
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Failed to backup ");
    stringBuilder.append(this.mBaseDirectory);
    stringBuilder.append(" to ");
    stringBuilder.append(this.mBackupDirectory);
    throw new IOException(stringBuilder.toString());
  }
  
  private void restore() throws IOException {
    if (!this.mBackupDirectory.exists())
      return; 
    if (this.mBaseDirectory.exists())
      deleteDirectory(this.mBaseDirectory); 
    if (this.mBackupDirectory.renameTo(this.mBaseDirectory)) {
      syncParentDirectory();
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Failed to restore ");
    stringBuilder.append(this.mBackupDirectory);
    stringBuilder.append(" to ");
    stringBuilder.append(this.mBaseDirectory);
    throw new IOException(stringBuilder.toString());
  }
  
  private static boolean deleteDirectory(File paramFile) {
    return FileUtils.deleteContentsAndDir(paramFile);
  }
  
  private void syncParentDirectory() {
    syncDirectory(this.mBaseDirectory.getParentFile());
  }
  
  private static void syncDirectory(File paramFile) {
    String str = paramFile.getAbsolutePath();
    try {
      FileDescriptor fileDescriptor = Os.open(str, OsConstants.O_RDONLY, 0);
      try {
        Os.fsync(fileDescriptor);
        FileUtils.closeQuietly(fileDescriptor);
      } catch (ErrnoException errnoException) {
        String str1 = LOG_TAG;
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Failed to fsync ");
        stringBuilder.append(str);
        Log.e(str1, stringBuilder.toString(), (Throwable)errnoException);
        FileUtils.closeQuietly(fileDescriptor);
      } finally {}
      return;
    } catch (ErrnoException errnoException) {
      String str1 = LOG_TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to open ");
      stringBuilder.append(str);
      Log.e(str1, stringBuilder.toString(), (Throwable)errnoException);
      return;
    } 
  }
}
