package com.android.internal.os;

import android.os.BatteryStats;

public class BatterySipper implements Comparable<BatterySipper> {
  public double audioPowerMah;
  
  public long audioTimeMs;
  
  public double bluetoothPowerMah;
  
  public long bluetoothRunningTimeMs;
  
  public long btRxBytes;
  
  public long btTxBytes;
  
  public double cameraPowerMah;
  
  public long cameraTimeMs;
  
  public long cpuFgTimeMs;
  
  public double cpuPowerMah;
  
  public long cpuTimeMs;
  
  public DrainType drainType;
  
  public double flashlightPowerMah;
  
  public long flashlightTimeMs;
  
  public double gpsPowerMah;
  
  public long gpsTimeMs;
  
  public String[] mPackages;
  
  public long mobileActive;
  
  public int mobileActiveCount;
  
  public double mobileRadioPowerMah;
  
  public long mobileRxBytes;
  
  public long mobileRxPackets;
  
  public long mobileTxBytes;
  
  public long mobileTxPackets;
  
  public double mobilemspp;
  
  public double noCoveragePercent;
  
  public String packageWithHighestDrain;
  
  public double percent;
  
  public double proportionalSmearMah;
  
  public double screenPowerMah;
  
  public double sensorPowerMah;
  
  public boolean shouldHide;
  
  public double totalPowerMah;
  
  public double totalSmearedPowerMah;
  
  public BatteryStats.Uid uidObj;
  
  public double usagePowerMah;
  
  public long usageTimeMs;
  
  public int userId;
  
  public double videoPowerMah;
  
  public long videoTimeMs;
  
  public double wakeLockPowerMah;
  
  public long wakeLockTimeMs;
  
  public double wifiPowerMah;
  
  public long wifiRunningTimeMs;
  
  public long wifiRxBytes;
  
  public long wifiRxPackets;
  
  public long wifiTxBytes;
  
  public long wifiTxPackets;
  
  public enum DrainType {
    AMBIENT_DISPLAY, APP, BLUETOOTH, CAMERA, CELL, FLASHLIGHT, IDLE, MEMORY, OVERCOUNTED, PHONE, SCREEN, UNACCOUNTED, USER, WIFI;
    
    private static final DrainType[] $VALUES;
    
    static {
      DrainType drainType = new DrainType("WIFI", 13);
      $VALUES = new DrainType[] { 
          AMBIENT_DISPLAY, APP, BLUETOOTH, CAMERA, CELL, FLASHLIGHT, IDLE, MEMORY, OVERCOUNTED, PHONE, 
          SCREEN, UNACCOUNTED, USER, drainType };
    }
  }
  
  public BatterySipper(DrainType paramDrainType, BatteryStats.Uid paramUid, double paramDouble) {
    this.totalPowerMah = paramDouble;
    this.drainType = paramDrainType;
    this.uidObj = paramUid;
  }
  
  public void computeMobilemspp() {
    double d;
    long l = this.mobileRxPackets + this.mobileTxPackets;
    if (l > 0L) {
      d = this.mobileActive / l;
    } else {
      d = 0.0D;
    } 
    this.mobilemspp = d;
  }
  
  public int compareTo(BatterySipper paramBatterySipper) {
    DrainType drainType = this.drainType;
    if (drainType != paramBatterySipper.drainType) {
      if (drainType == DrainType.OVERCOUNTED)
        return 1; 
      if (paramBatterySipper.drainType == DrainType.OVERCOUNTED)
        return -1; 
    } 
    return Double.compare(paramBatterySipper.totalPowerMah, this.totalPowerMah);
  }
  
  public String[] getPackages() {
    return this.mPackages;
  }
  
  public int getUid() {
    BatteryStats.Uid uid = this.uidObj;
    if (uid == null)
      return 0; 
    return uid.getUid();
  }
  
  public void add(BatterySipper paramBatterySipper) {
    this.totalPowerMah += paramBatterySipper.totalPowerMah;
    this.usageTimeMs += paramBatterySipper.usageTimeMs;
    this.usagePowerMah += paramBatterySipper.usagePowerMah;
    this.audioTimeMs += paramBatterySipper.audioTimeMs;
    this.cpuTimeMs += paramBatterySipper.cpuTimeMs;
    this.gpsTimeMs += paramBatterySipper.gpsTimeMs;
    this.wifiRunningTimeMs += paramBatterySipper.wifiRunningTimeMs;
    this.cpuFgTimeMs += paramBatterySipper.cpuFgTimeMs;
    this.videoTimeMs += paramBatterySipper.videoTimeMs;
    this.wakeLockTimeMs += paramBatterySipper.wakeLockTimeMs;
    this.cameraTimeMs += paramBatterySipper.cameraTimeMs;
    this.flashlightTimeMs += paramBatterySipper.flashlightTimeMs;
    this.bluetoothRunningTimeMs += paramBatterySipper.bluetoothRunningTimeMs;
    this.mobileRxPackets += paramBatterySipper.mobileRxPackets;
    this.mobileTxPackets += paramBatterySipper.mobileTxPackets;
    this.mobileActive += paramBatterySipper.mobileActive;
    this.mobileActiveCount += paramBatterySipper.mobileActiveCount;
    this.wifiRxPackets += paramBatterySipper.wifiRxPackets;
    this.wifiTxPackets += paramBatterySipper.wifiTxPackets;
    this.mobileRxBytes += paramBatterySipper.mobileRxBytes;
    this.mobileTxBytes += paramBatterySipper.mobileTxBytes;
    this.wifiRxBytes += paramBatterySipper.wifiRxBytes;
    this.wifiTxBytes += paramBatterySipper.wifiTxBytes;
    this.btRxBytes += paramBatterySipper.btRxBytes;
    this.btTxBytes += paramBatterySipper.btTxBytes;
    this.audioPowerMah += paramBatterySipper.audioPowerMah;
    this.wifiPowerMah += paramBatterySipper.wifiPowerMah;
    this.gpsPowerMah += paramBatterySipper.gpsPowerMah;
    this.cpuPowerMah += paramBatterySipper.cpuPowerMah;
    this.sensorPowerMah += paramBatterySipper.sensorPowerMah;
    this.mobileRadioPowerMah += paramBatterySipper.mobileRadioPowerMah;
    this.wakeLockPowerMah += paramBatterySipper.wakeLockPowerMah;
    this.cameraPowerMah += paramBatterySipper.cameraPowerMah;
    this.flashlightPowerMah += paramBatterySipper.flashlightPowerMah;
    this.bluetoothPowerMah += paramBatterySipper.bluetoothPowerMah;
    this.screenPowerMah += paramBatterySipper.screenPowerMah;
    this.videoPowerMah += paramBatterySipper.videoPowerMah;
    this.proportionalSmearMah += paramBatterySipper.proportionalSmearMah;
    this.totalSmearedPowerMah += paramBatterySipper.totalSmearedPowerMah;
  }
  
  public double sumPower() {
    double d = this.usagePowerMah + this.wifiPowerMah + this.gpsPowerMah + this.cpuPowerMah + this.sensorPowerMah + this.mobileRadioPowerMah + this.wakeLockPowerMah + this.cameraPowerMah + this.flashlightPowerMah + this.bluetoothPowerMah + this.audioPowerMah + this.videoPowerMah;
    this.totalSmearedPowerMah = this.screenPowerMah + d + this.proportionalSmearMah;
    return d;
  }
}
