package com.android.internal.os;

import android.os.Process;
import android.util.Slog;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.Preconditions;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Predicate;

public class KernelCpuThreadReader {
  private static final String CPU_STATISTICS_FILENAME = "time_in_state";
  
  private static final boolean DEBUG = false;
  
  private static final Path DEFAULT_INITIAL_TIME_IN_STATE_PATH;
  
  private static final String DEFAULT_PROCESS_NAME = "unknown_process";
  
  private static final Path DEFAULT_PROC_PATH;
  
  private static final String DEFAULT_THREAD_NAME = "unknown_thread";
  
  private static final int ID_ERROR = -1;
  
  private static final String PROCESS_DIRECTORY_FILTER = "[0-9]*";
  
  private static final String PROCESS_NAME_FILENAME = "cmdline";
  
  private static final String TAG = "KernelCpuThreadReader";
  
  private static final String THREAD_NAME_FILENAME = "comm";
  
  private int[] mFrequenciesKhz;
  
  private FrequencyBucketCreator mFrequencyBucketCreator;
  
  private final Injector mInjector;
  
  private final Path mProcPath;
  
  private final ProcTimeInStateReader mProcTimeInStateReader;
  
  private Predicate<Integer> mUidPredicate;
  
  static {
    Path path = Paths.get("/proc", new String[0]);
    DEFAULT_INITIAL_TIME_IN_STATE_PATH = path.resolve("self/time_in_state");
  }
  
  public KernelCpuThreadReader(int paramInt, Predicate<Integer> paramPredicate, Path paramPath1, Path paramPath2, Injector paramInjector) throws IOException {
    this.mUidPredicate = paramPredicate;
    this.mProcPath = paramPath1;
    this.mProcTimeInStateReader = new ProcTimeInStateReader(paramPath2);
    this.mInjector = paramInjector;
    setNumBuckets(paramInt);
  }
  
  public static KernelCpuThreadReader create(int paramInt, Predicate<Integer> paramPredicate) {
    try {
      Path path1 = DEFAULT_PROC_PATH, path2 = DEFAULT_INITIAL_TIME_IN_STATE_PATH;
      Injector injector = new Injector();
      this();
      return new KernelCpuThreadReader(paramInt, paramPredicate, path1, path2, injector);
    } catch (IOException iOException) {
      Slog.e("KernelCpuThreadReader", "Failed to initialize KernelCpuThreadReader", iOException);
      return null;
    } 
  }
  
  public ArrayList<ProcessCpuUsage> getProcessCpuUsage() {
    ArrayList<ProcessCpuUsage> arrayList = new ArrayList();
    try {
      Path path = this.mProcPath;
      DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path, "[0-9]*");
      try {
        for (Path path1 : directoryStream) {
          int i = getProcessId(path1);
          int j = this.mInjector.getUidForPid(i);
          if (j == -1 || i == -1)
            continue; 
          if (!this.mUidPredicate.test(Integer.valueOf(j)))
            continue; 
          ProcessCpuUsage processCpuUsage = getProcessCpuUsage(path1, i, j);
          if (processCpuUsage != null)
            arrayList.add(processCpuUsage); 
        } 
        if (directoryStream != null)
          directoryStream.close(); 
        return arrayList;
      } finally {
        if (directoryStream != null)
          try {
            directoryStream.close();
          } finally {
            directoryStream = null;
          }  
      } 
    } catch (IOException iOException) {
      Slog.w("KernelCpuThreadReader", "Failed to iterate over process paths", iOException);
      return null;
    } 
  }
  
  public int[] getCpuFrequenciesKhz() {
    return this.mFrequenciesKhz;
  }
  
  void setNumBuckets(int paramInt) {
    if (paramInt < 1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Number of buckets must be at least 1, but was ");
      stringBuilder.append(paramInt);
      Slog.w("KernelCpuThreadReader", stringBuilder.toString());
      return;
    } 
    int[] arrayOfInt = this.mFrequenciesKhz;
    if (arrayOfInt != null && arrayOfInt.length == paramInt)
      return; 
    ProcTimeInStateReader procTimeInStateReader1 = this.mProcTimeInStateReader;
    FrequencyBucketCreator frequencyBucketCreator = new FrequencyBucketCreator(procTimeInStateReader1.getFrequenciesKhz(), paramInt);
    ProcTimeInStateReader procTimeInStateReader2 = this.mProcTimeInStateReader;
    long[] arrayOfLong = procTimeInStateReader2.getFrequenciesKhz();
    this.mFrequenciesKhz = frequencyBucketCreator.bucketFrequencies(arrayOfLong);
  }
  
  void setUidPredicate(Predicate<Integer> paramPredicate) {
    this.mUidPredicate = paramPredicate;
  }
  
  private ProcessCpuUsage getProcessCpuUsage(Path paramPath, int paramInt1, int paramInt2) {
    Path path = paramPath.resolve("task");
    ArrayList<ThreadCpuUsage> arrayList = new ArrayList();
    try {
      DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path);
      try {
        for (Path path1 : directoryStream) {
          ThreadCpuUsage threadCpuUsage = getThreadCpuUsage(path1);
          if (threadCpuUsage == null)
            continue; 
          arrayList.add(threadCpuUsage);
        } 
        if (directoryStream != null)
          directoryStream.close(); 
        return new ProcessCpuUsage(paramInt1, getProcessName(paramPath), paramInt2, arrayList);
      } finally {
        if (directoryStream != null)
          try {
            directoryStream.close();
          } finally {
            arrayList = null;
          }  
      } 
    } catch (IOException|java.nio.file.DirectoryIteratorException iOException) {
      return null;
    } 
  }
  
  private ThreadCpuUsage getThreadCpuUsage(Path paramPath) {
    try {
      String str = paramPath.getFileName().toString();
      int i = Integer.parseInt(str);
      str = getThreadName(paramPath);
      paramPath = paramPath.resolve("time_in_state");
      long[] arrayOfLong = this.mProcTimeInStateReader.getUsageTimesMillis(paramPath);
      if (arrayOfLong == null)
        return null; 
      int[] arrayOfInt = this.mFrequencyBucketCreator.bucketValues(arrayOfLong);
      return new ThreadCpuUsage(i, str, arrayOfInt);
    } catch (NumberFormatException numberFormatException) {
      Slog.w("KernelCpuThreadReader", "Failed to parse thread ID when iterating over /proc/*/task", numberFormatException);
      return null;
    } 
  }
  
  private String getProcessName(Path paramPath) {
    paramPath = paramPath.resolve("cmdline");
    String str = ProcStatsUtil.readSingleLineProcFile(paramPath.toString());
    if (str != null)
      return str; 
    return "unknown_process";
  }
  
  private String getThreadName(Path paramPath) {
    paramPath = paramPath.resolve("comm");
    String str = ProcStatsUtil.readNullSeparatedFile(paramPath.toString());
    if (str == null)
      return "unknown_thread"; 
    return str;
  }
  
  private int getProcessId(Path paramPath) {
    String str = paramPath.getFileName().toString();
    try {
      return Integer.parseInt(str);
    } catch (NumberFormatException numberFormatException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to parse ");
      stringBuilder.append(str);
      stringBuilder.append(" as process ID");
      Slog.w("KernelCpuThreadReader", stringBuilder.toString(), numberFormatException);
      return -1;
    } 
  }
  
  public static class FrequencyBucketCreator {
    private final int[] mBucketStartIndices;
    
    private final int mNumBuckets;
    
    private final int mNumFrequencies;
    
    public FrequencyBucketCreator(long[] param1ArrayOflong, int param1Int) {
      this.mNumFrequencies = param1ArrayOflong.length;
      int[] arrayOfInt = getClusterStartIndices(param1ArrayOflong);
      int i = this.mNumFrequencies;
      this.mBucketStartIndices = arrayOfInt = getBucketStartIndices(arrayOfInt, param1Int, i);
      this.mNumBuckets = arrayOfInt.length;
    }
    
    public int[] bucketValues(long[] param1ArrayOflong) {
      boolean bool;
      if (param1ArrayOflong.length == this.mNumFrequencies) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkArgument(bool);
      int[] arrayOfInt = new int[this.mNumBuckets];
      for (byte b = 0; b < this.mNumBuckets; b++) {
        int i = getLowerBound(b, this.mBucketStartIndices);
        int arrayOfInt1[] = this.mBucketStartIndices, j = param1ArrayOflong.length;
        j = getUpperBound(b, arrayOfInt1, j);
        for (; i < j; i++)
          arrayOfInt[b] = (int)(arrayOfInt[b] + param1ArrayOflong[i]); 
      } 
      return arrayOfInt;
    }
    
    public int[] bucketFrequencies(long[] param1ArrayOflong) {
      boolean bool;
      if (param1ArrayOflong.length == this.mNumFrequencies) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkArgument(bool);
      int[] arrayOfInt = new int[this.mNumBuckets];
      for (byte b = 0; b < arrayOfInt.length; b++)
        arrayOfInt[b] = (int)param1ArrayOflong[this.mBucketStartIndices[b]]; 
      return arrayOfInt;
    }
    
    private static int[] getClusterStartIndices(long[] param1ArrayOflong) {
      ArrayList<Integer> arrayList = new ArrayList();
      arrayList.add(Integer.valueOf(0));
      for (byte b = 0; b < param1ArrayOflong.length - 1; b++) {
        if (param1ArrayOflong[b] >= param1ArrayOflong[b + 1])
          arrayList.add(Integer.valueOf(b + 1)); 
      } 
      return ArrayUtils.convertToIntArray(arrayList);
    }
    
    private static int[] getBucketStartIndices(int[] param1ArrayOfint, int param1Int1, int param1Int2) {
      int i = param1ArrayOfint.length;
      if (i > param1Int1)
        return Arrays.copyOfRange(param1ArrayOfint, 0, param1Int1); 
      ArrayList<Integer> arrayList = new ArrayList();
      for (byte b = 0; b < i; b++) {
        int m, j = getLowerBound(b, param1ArrayOfint);
        int k = getUpperBound(b, param1ArrayOfint, param1Int2);
        if (b != i - 1) {
          m = param1Int1 / i;
        } else {
          m = param1Int1 / i;
          m = param1Int1 - (i - 1) * m;
        } 
        int n = (k - j) / m;
        int i1 = Math.max(1, n);
        for (n = 0; n < m; n++) {
          int i2 = n * i1 + j;
          if (i2 >= k)
            break; 
          arrayList.add(Integer.valueOf(i2));
        } 
      } 
      return ArrayUtils.convertToIntArray(arrayList);
    }
    
    private static int getLowerBound(int param1Int, int[] param1ArrayOfint) {
      return param1ArrayOfint[param1Int];
    }
    
    private static int getUpperBound(int param1Int1, int[] param1ArrayOfint, int param1Int2) {
      if (param1Int1 != param1ArrayOfint.length - 1)
        return param1ArrayOfint[param1Int1 + 1]; 
      return param1Int2;
    }
  }
  
  public static class ProcessCpuUsage {
    public final int processId;
    
    public final String processName;
    
    public ArrayList<KernelCpuThreadReader.ThreadCpuUsage> threadCpuUsages;
    
    public final int uid;
    
    public ProcessCpuUsage(int param1Int1, String param1String, int param1Int2, ArrayList<KernelCpuThreadReader.ThreadCpuUsage> param1ArrayList) {
      this.processId = param1Int1;
      this.processName = param1String;
      this.uid = param1Int2;
      this.threadCpuUsages = param1ArrayList;
    }
  }
  
  public static class ThreadCpuUsage {
    public final int threadId;
    
    public final String threadName;
    
    public int[] usageTimesMillis;
    
    public ThreadCpuUsage(int param1Int, String param1String, int[] param1ArrayOfint) {
      this.threadId = param1Int;
      this.threadName = param1String;
      this.usageTimesMillis = param1ArrayOfint;
    }
  }
  
  public static class Injector {
    public int getUidForPid(int param1Int) {
      return Process.getUidForPid(param1Int);
    }
  }
}
