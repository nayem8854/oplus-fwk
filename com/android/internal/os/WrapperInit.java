package com.android.internal.os;

import android.os.Process;
import android.util.Slog;
import android.util.TimingsTraceLog;
import dalvik.system.VMRuntime;
import java.io.DataOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import libcore.io.IoUtils;

public class WrapperInit {
  private static final String TAG = "AndroidRuntime";
  
  public static void main(String[] paramArrayOfString) {
    int i = Integer.parseInt(paramArrayOfString[0], 10);
    int j = Integer.parseInt(paramArrayOfString[1], 10);
    if (i != 0)
      try {
        FileDescriptor fileDescriptor = new FileDescriptor();
        this();
        fileDescriptor.setInt$(i);
        DataOutputStream dataOutputStream = new DataOutputStream();
        FileOutputStream fileOutputStream = new FileOutputStream();
        this(fileDescriptor);
        this(fileOutputStream);
        dataOutputStream.writeInt(Process.myPid());
        dataOutputStream.close();
        IoUtils.closeQuietly(fileDescriptor);
      } catch (IOException iOException) {
        Slog.d("AndroidRuntime", "Could not write pid of wrapped process to Zygote pipe.", iOException);
      }  
    ZygoteInit.preload(new TimingsTraceLog("WrapperInitTiming", 16384L));
    String[] arrayOfString = new String[paramArrayOfString.length - 2];
    System.arraycopy(paramArrayOfString, 2, arrayOfString, 0, arrayOfString.length);
    Runnable runnable = wrapperInit(j, arrayOfString);
    runnable.run();
  }
  
  public static void execApplication(String paramString1, String paramString2, int paramInt, String paramString3, FileDescriptor paramFileDescriptor, String[] paramArrayOfString) {
    boolean bool;
    StringBuilder stringBuilder = new StringBuilder(paramString1);
    if (VMRuntime.is64BitInstructionSet(paramString3)) {
      paramString1 = "/system/bin/app_process64";
    } else {
      paramString1 = "/system/bin/app_process32";
    } 
    stringBuilder.append(' ');
    stringBuilder.append(paramString1);
    stringBuilder.append(" -Xcompiler-option --generate-mini-debug-info");
    stringBuilder.append(" /system/bin --application");
    if (paramString2 != null) {
      stringBuilder.append(" '--nice-name=");
      stringBuilder.append(paramString2);
      stringBuilder.append("'");
    } 
    stringBuilder.append(" com.android.internal.os.WrapperInit ");
    if (paramFileDescriptor != null) {
      bool = paramFileDescriptor.getInt$();
    } else {
      bool = false;
    } 
    stringBuilder.append(bool);
    stringBuilder.append(' ');
    stringBuilder.append(paramInt);
    Zygote.appendQuotedShellArgs(stringBuilder, paramArrayOfString);
    preserveCapabilities();
    Zygote.execShell(stringBuilder.toString());
  }
  
  private static Runnable wrapperInit(int paramInt, String[] paramArrayOfString) {
    ClassLoader classLoader1 = null;
    ClassLoader classLoader2 = classLoader1;
    String[] arrayOfString = paramArrayOfString;
    if (paramArrayOfString != null) {
      classLoader2 = classLoader1;
      arrayOfString = paramArrayOfString;
      if (paramArrayOfString.length > 2) {
        classLoader2 = classLoader1;
        arrayOfString = paramArrayOfString;
        if (paramArrayOfString[0].equals("-cp")) {
          classLoader2 = ZygoteInit.createPathClassLoader(paramArrayOfString[1], paramInt);
          Thread.currentThread().setContextClassLoader(classLoader2);
          arrayOfString = new String[paramArrayOfString.length - 2];
          System.arraycopy(paramArrayOfString, 2, arrayOfString, 0, paramArrayOfString.length - 2);
        } 
      } 
    } 
    Zygote.nativePreApplicationInit();
    return RuntimeInit.applicationInit(paramInt, null, arrayOfString, classLoader2);
  }
  
  private static void preserveCapabilities() {
    // Byte code:
    //   0: new android/system/StructCapUserHeader
    //   3: dup
    //   4: getstatic android/system/OsConstants._LINUX_CAPABILITY_VERSION_3 : I
    //   7: iconst_0
    //   8: invokespecial <init> : (II)V
    //   11: astore_0
    //   12: aload_0
    //   13: invokestatic capget : (Landroid/system/StructCapUserHeader;)[Landroid/system/StructCapUserData;
    //   16: astore_1
    //   17: aload_1
    //   18: iconst_0
    //   19: aaload
    //   20: getfield permitted : I
    //   23: aload_1
    //   24: iconst_0
    //   25: aaload
    //   26: getfield inheritable : I
    //   29: if_icmpne -> 47
    //   32: aload_1
    //   33: iconst_1
    //   34: aaload
    //   35: getfield permitted : I
    //   38: aload_1
    //   39: iconst_1
    //   40: aaload
    //   41: getfield inheritable : I
    //   44: if_icmpeq -> 108
    //   47: aload_1
    //   48: iconst_0
    //   49: new android/system/StructCapUserData
    //   52: dup
    //   53: aload_1
    //   54: iconst_0
    //   55: aaload
    //   56: getfield effective : I
    //   59: aload_1
    //   60: iconst_0
    //   61: aaload
    //   62: getfield permitted : I
    //   65: aload_1
    //   66: iconst_0
    //   67: aaload
    //   68: getfield permitted : I
    //   71: invokespecial <init> : (III)V
    //   74: aastore
    //   75: aload_1
    //   76: iconst_1
    //   77: new android/system/StructCapUserData
    //   80: dup
    //   81: aload_1
    //   82: iconst_1
    //   83: aaload
    //   84: getfield effective : I
    //   87: aload_1
    //   88: iconst_1
    //   89: aaload
    //   90: getfield permitted : I
    //   93: aload_1
    //   94: iconst_1
    //   95: aaload
    //   96: getfield permitted : I
    //   99: invokespecial <init> : (III)V
    //   102: aastore
    //   103: aload_0
    //   104: aload_1
    //   105: invokestatic capset : (Landroid/system/StructCapUserHeader;[Landroid/system/StructCapUserData;)V
    //   108: iconst_0
    //   109: istore_2
    //   110: iload_2
    //   111: bipush #64
    //   113: if_icmpge -> 200
    //   116: iload_2
    //   117: invokestatic CAP_TO_INDEX : (I)I
    //   120: istore_3
    //   121: iload_2
    //   122: invokestatic CAP_TO_MASK : (I)I
    //   125: istore #4
    //   127: aload_1
    //   128: iload_3
    //   129: aaload
    //   130: getfield inheritable : I
    //   133: iload #4
    //   135: iand
    //   136: ifeq -> 194
    //   139: getstatic android/system/OsConstants.PR_CAP_AMBIENT : I
    //   142: getstatic android/system/OsConstants.PR_CAP_AMBIENT_RAISE : I
    //   145: i2l
    //   146: iload_2
    //   147: i2l
    //   148: lconst_0
    //   149: lconst_0
    //   150: invokestatic prctl : (IJJJJ)I
    //   153: pop
    //   154: goto -> 194
    //   157: astore_0
    //   158: new java/lang/StringBuilder
    //   161: dup
    //   162: invokespecial <init> : ()V
    //   165: astore #5
    //   167: aload #5
    //   169: ldc 'RuntimeInit: Failed to raise ambient capability '
    //   171: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   174: pop
    //   175: aload #5
    //   177: iload_2
    //   178: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   181: pop
    //   182: ldc 'AndroidRuntime'
    //   184: aload #5
    //   186: invokevirtual toString : ()Ljava/lang/String;
    //   189: aload_0
    //   190: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   193: pop
    //   194: iinc #2, 1
    //   197: goto -> 110
    //   200: return
    //   201: astore_1
    //   202: ldc 'AndroidRuntime'
    //   204: ldc 'RuntimeInit: Failed capset'
    //   206: aload_1
    //   207: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   210: pop
    //   211: return
    //   212: astore_1
    //   213: ldc 'AndroidRuntime'
    //   215: ldc 'RuntimeInit: Failed capget'
    //   217: aload_1
    //   218: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   221: pop
    //   222: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #189	-> 0
    //   #193	-> 12
    //   #197	-> 17
    //   #199	-> 17
    //   #201	-> 47
    //   #203	-> 75
    //   #206	-> 103
    //   #210	-> 108
    //   #213	-> 108
    //   #214	-> 116
    //   #215	-> 121
    //   #216	-> 127
    //   #218	-> 139
    //   #225	-> 154
    //   #220	-> 157
    //   #223	-> 158
    //   #213	-> 194
    //   #228	-> 200
    //   #207	-> 201
    //   #208	-> 202
    //   #209	-> 211
    //   #194	-> 212
    //   #195	-> 213
    //   #196	-> 222
    // Exception table:
    //   from	to	target	type
    //   12	17	212	android/system/ErrnoException
    //   103	108	201	android/system/ErrnoException
    //   139	154	157	android/system/ErrnoException
  }
}
