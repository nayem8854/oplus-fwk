package com.android.internal.os;

import android.os.BatteryStats;

public class WifiPowerEstimator extends PowerCalculator {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "WifiPowerEstimator";
  
  private long mTotalAppWifiRunningTimeMs = 0L;
  
  private final double mWifiPowerBatchScan;
  
  private final double mWifiPowerOn;
  
  private final double mWifiPowerPerPacket;
  
  private final double mWifiPowerScan;
  
  public WifiPowerEstimator(PowerProfile paramPowerProfile) {
    this.mWifiPowerPerPacket = getWifiPowerPerPacket(paramPowerProfile);
    this.mWifiPowerOn = paramPowerProfile.getAveragePower("wifi.on");
    this.mWifiPowerScan = paramPowerProfile.getAveragePower("wifi.scan");
    this.mWifiPowerBatchScan = paramPowerProfile.getAveragePower("wifi.batchedscan");
  }
  
  private static double getWifiPowerPerPacket(PowerProfile paramPowerProfile) {
    double d = paramPowerProfile.getAveragePower("wifi.active") / 3600.0D;
    return d / 61.03515625D;
  }
  
  public void calculateApp(BatterySipper paramBatterySipper, BatteryStats.Uid paramUid, long paramLong1, long paramLong2, int paramInt) {
    paramBatterySipper.wifiRxPackets = paramUid.getNetworkActivityPackets(2, paramInt);
    paramBatterySipper.wifiTxPackets = paramUid.getNetworkActivityPackets(3, paramInt);
    paramBatterySipper.wifiRxBytes = paramUid.getNetworkActivityBytes(2, paramInt);
    paramBatterySipper.wifiTxBytes = paramUid.getNetworkActivityBytes(3, paramInt);
    double d1 = (paramBatterySipper.wifiRxPackets + paramBatterySipper.wifiTxPackets), d2 = this.mWifiPowerPerPacket;
    paramBatterySipper.wifiRunningTimeMs = paramUid.getWifiRunningTime(paramLong1, paramInt) / 1000L;
    this.mTotalAppWifiRunningTimeMs += paramBatterySipper.wifiRunningTimeMs;
    double d3 = paramBatterySipper.wifiRunningTimeMs * this.mWifiPowerOn / 3600000.0D;
    paramLong2 = paramUid.getWifiScanTime(paramLong1, paramInt) / 1000L;
    double d4 = paramLong2 * this.mWifiPowerScan / 3600000.0D;
    double d5 = 0.0D;
    for (byte b = 0; b < 5; b++) {
      paramLong2 = paramUid.getWifiBatchedScanTime(b, paramLong1, paramInt) / 1000L;
      double d = paramLong2 * this.mWifiPowerBatchScan / 3600000.0D;
      d5 += d;
    } 
    paramBatterySipper.wifiPowerMah = d1 * d2 + d3 + d4 + d5;
  }
  
  public void calculateRemaining(BatterySipper paramBatterySipper, BatteryStats paramBatteryStats, long paramLong1, long paramLong2, int paramInt) {
    paramLong1 = paramBatteryStats.getGlobalWifiRunningTime(paramLong1, paramInt) / 1000L;
    double d = (paramLong1 - this.mTotalAppWifiRunningTimeMs) * this.mWifiPowerOn / 3600000.0D;
    paramBatterySipper.wifiRunningTimeMs = paramLong1;
    paramBatterySipper.wifiPowerMah = Math.max(0.0D, d);
  }
  
  public void reset() {
    this.mTotalAppWifiRunningTimeMs = 0L;
  }
}
