package com.android.internal.os;

import android.os.BatteryStats;
import android.util.ArrayMap;
import android.util.Log;

public class CpuPowerCalculator extends PowerCalculator {
  private static final boolean DEBUG = false;
  
  private static final long MICROSEC_IN_HR = 3600000000L;
  
  private static final String TAG = "CpuPowerCalculator";
  
  private final PowerProfile mProfile;
  
  public CpuPowerCalculator(PowerProfile paramPowerProfile) {
    this.mProfile = paramPowerProfile;
  }
  
  public void calculateApp(BatterySipper paramBatterySipper, BatteryStats.Uid paramUid, long paramLong1, long paramLong2, int paramInt) {
    paramBatterySipper.cpuTimeMs = (paramUid.getUserCpuTimeUs(paramInt) + paramUid.getSystemCpuTimeUs(paramInt)) / 1000L;
    int i = this.mProfile.getNumCpuClusters();
    double d1 = 0.0D;
    int j;
    for (j = 0; j < i; j++) {
      int m = this.mProfile.getNumSpeedStepsInCpuCluster(j);
      for (byte b1 = 0; b1 < m; b1++) {
        paramLong1 = paramUid.getTimeAtCpuSpeed(j, b1, paramInt);
        double d4 = paramLong1;
        PowerProfile powerProfile = this.mProfile;
        double d5 = powerProfile.getAveragePowerForCpuCore(j, b1);
        d1 += d4 * d5;
      } 
    } 
    double d2 = d1 + (paramUid.getCpuActiveTime() * 1000L) * this.mProfile.getAveragePower("cpu.active");
    long[] arrayOfLong2 = paramUid.getCpuClusterTimes();
    d1 = d2;
    if (arrayOfLong2 != null)
      if (arrayOfLong2.length == i) {
        for (j = 0, d1 = d2; j < i; j++) {
          double d = (arrayOfLong2[j] * 1000L);
          PowerProfile powerProfile = this.mProfile;
          d2 = powerProfile.getAveragePowerForCpuCluster(j);
          d1 += d * d2;
        } 
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UID ");
        stringBuilder.append(paramUid.getUid());
        stringBuilder.append(" CPU cluster # mismatch: Power Profile # ");
        stringBuilder.append(i);
        stringBuilder.append(" actual # ");
        stringBuilder.append(arrayOfLong2.length);
        Log.w("CpuPowerCalculator", stringBuilder.toString());
        d1 = d2;
      }  
    paramBatterySipper.cpuPowerMah = d1 / 3.6E9D;
    double d3 = 0.0D;
    paramBatterySipper.cpuFgTimeMs = 0L;
    ArrayMap arrayMap = paramUid.getProcessStats();
    int k = arrayMap.size();
    long[] arrayOfLong1;
    byte b;
    for (b = 0, d2 = d1, arrayOfLong1 = arrayOfLong2, j = i; b < k; b++, d3 = d1) {
      BatteryStats.Uid.Proc proc = (BatteryStats.Uid.Proc)arrayMap.valueAt(b);
      String str = (String)arrayMap.keyAt(b);
      paramBatterySipper.cpuFgTimeMs += proc.getForegroundTime(paramInt);
      paramLong2 = proc.getUserTime(paramInt);
      paramLong1 = proc.getSystemTime(paramInt);
      paramLong1 = paramLong2 + paramLong1 + proc.getForegroundTime(paramInt);
      if (paramBatterySipper.packageWithHighestDrain != null) {
        String str1 = paramBatterySipper.packageWithHighestDrain;
        if (!str1.startsWith("*")) {
          d1 = d3;
          if (d3 < paramLong1) {
            d1 = d3;
            if (!str.startsWith("*")) {
              d1 = paramLong1;
              paramBatterySipper.packageWithHighestDrain = str;
            } 
          } 
          continue;
        } 
      } 
      d1 = paramLong1;
      paramBatterySipper.packageWithHighestDrain = str;
      continue;
    } 
    if (paramBatterySipper.cpuFgTimeMs > paramBatterySipper.cpuTimeMs)
      paramBatterySipper.cpuTimeMs = paramBatterySipper.cpuFgTimeMs; 
  }
}
