package com.android.internal.os;

import java.util.HashMap;

public class KernelWakelockStats extends HashMap<String, KernelWakelockStats.Entry> {
  int kernelWakelockVersion;
  
  public static class Entry {
    public int mCount;
    
    public long mTotalTime;
    
    public int mVersion;
    
    Entry(int param1Int1, long param1Long, int param1Int2) {
      this.mCount = param1Int1;
      this.mTotalTime = param1Long;
      this.mVersion = param1Int2;
    }
  }
}
