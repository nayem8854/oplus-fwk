package com.android.internal.os;

import android.os.StrictMode;
import android.os.SystemClock;
import android.util.Slog;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.CharBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class KernelCpuProcStringReader {
  private static final KernelCpuProcStringReader ACTIVE_TIME_READER;
  
  private static final KernelCpuProcStringReader CLUSTER_TIME_READER;
  
  private static final int ERROR_THRESHOLD = 5;
  
  private static final KernelCpuProcStringReader FREQ_TIME_READER;
  
  private static final long FRESHNESS = 500L;
  
  private static final int MAX_BUFFER_SIZE = 1048576;
  
  private static final String PROC_UID_ACTIVE_TIME = "/proc/uid_concurrent_active_time";
  
  private static final String PROC_UID_CLUSTER_TIME = "/proc/uid_concurrent_policy_time";
  
  private static final String PROC_UID_FREQ_TIME = "/proc/uid_time_in_state";
  
  private static final String PROC_UID_USER_SYS_TIME = "/proc/uid_cputime/show_uid_stat";
  
  private static final String TAG = KernelCpuProcStringReader.class.getSimpleName();
  
  private static final KernelCpuProcStringReader USER_SYS_TIME_READER;
  
  private char[] mBuf;
  
  static {
    FREQ_TIME_READER = new KernelCpuProcStringReader("/proc/uid_time_in_state");
    ACTIVE_TIME_READER = new KernelCpuProcStringReader("/proc/uid_concurrent_active_time");
    CLUSTER_TIME_READER = new KernelCpuProcStringReader("/proc/uid_concurrent_policy_time");
    USER_SYS_TIME_READER = new KernelCpuProcStringReader("/proc/uid_cputime/show_uid_stat");
  }
  
  static KernelCpuProcStringReader getFreqTimeReaderInstance() {
    return FREQ_TIME_READER;
  }
  
  static KernelCpuProcStringReader getActiveTimeReaderInstance() {
    return ACTIVE_TIME_READER;
  }
  
  static KernelCpuProcStringReader getClusterTimeReaderInstance() {
    return CLUSTER_TIME_READER;
  }
  
  static KernelCpuProcStringReader getUserSysTimeReaderInstance() {
    return USER_SYS_TIME_READER;
  }
  
  private int mErrors = 0;
  
  private final Path mFile;
  
  private long mLastReadTime = 0L;
  
  private final ReentrantReadWriteLock mLock;
  
  private final ReentrantReadWriteLock.ReadLock mReadLock;
  
  private int mSize;
  
  private final ReentrantReadWriteLock.WriteLock mWriteLock;
  
  public KernelCpuProcStringReader(String paramString) {
    ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock();
    this.mReadLock = reentrantReadWriteLock.readLock();
    this.mWriteLock = this.mLock.writeLock();
    this.mFile = Paths.get(paramString, new String[0]);
  }
  
  public ProcFileIterator open() {
    return open(false);
  }
  
  public ProcFileIterator open(boolean paramBoolean) {
    if (this.mErrors >= 5)
      return null; 
    if (paramBoolean) {
      this.mWriteLock.lock();
    } else {
      this.mReadLock.lock();
      if (dataValid())
        return new ProcFileIterator(this.mSize); 
      this.mReadLock.unlock();
      this.mWriteLock.lock();
      if (dataValid()) {
        this.mReadLock.lock();
        this.mWriteLock.unlock();
        return new ProcFileIterator(this.mSize);
      } 
    } 
    int i = 0;
    this.mSize = 0;
    int j = StrictMode.allowThreadDiskReadsMask();
    try {
      BufferedReader bufferedReader = Files.newBufferedReader(this.mFile);
      int k = i;
      try {
        if (this.mBuf == null) {
          this.mBuf = new char[1024];
          k = i;
        } 
        while (true) {
          i = bufferedReader.read(this.mBuf, k, this.mBuf.length - k);
          if (i >= 0) {
            i = k + i;
            k = i;
            if (i == this.mBuf.length) {
              if (this.mBuf.length == 1048576) {
                this.mErrors++;
                String str = TAG;
                StringBuilder stringBuilder = new StringBuilder();
                this();
                stringBuilder.append("Proc file too large: ");
                stringBuilder.append(this.mFile);
                Slog.e(str, stringBuilder.toString());
                if (bufferedReader != null)
                  bufferedReader.close(); 
                StrictMode.setThreadPolicyMask(j);
                return null;
              } 
              this.mBuf = Arrays.copyOf(this.mBuf, Math.min(this.mBuf.length << 1, 1048576));
              k = i;
            } 
            continue;
          } 
          break;
        } 
        this.mSize = k;
        this.mLastReadTime = SystemClock.elapsedRealtime();
        this.mReadLock.lock();
        ProcFileIterator procFileIterator = new ProcFileIterator(k);
        if (bufferedReader != null)
          bufferedReader.close(); 
        StrictMode.setThreadPolicyMask(j);
        return procFileIterator;
      } finally {
        if (bufferedReader != null)
          try {
            bufferedReader.close();
          } finally {
            bufferedReader = null;
          }  
      } 
    } catch (FileNotFoundException|java.nio.file.NoSuchFileException fileNotFoundException) {
      this.mErrors++;
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("File not found. It's normal if not implemented: ");
      stringBuilder.append(this.mFile);
      Slog.w(str, stringBuilder.toString());
    } catch (IOException iOException) {
      this.mErrors++;
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Error reading ");
      stringBuilder.append(this.mFile);
      Slog.e(str, stringBuilder.toString(), iOException);
    } finally {
      Exception exception;
    } 
    StrictMode.setThreadPolicyMask(j);
    this.mWriteLock.unlock();
    return null;
  }
  
  private boolean dataValid() {
    boolean bool;
    if (this.mSize > 0 && SystemClock.elapsedRealtime() - this.mLastReadTime < 500L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public class ProcFileIterator implements AutoCloseable {
    private int mPos;
    
    private final int mSize;
    
    final KernelCpuProcStringReader this$0;
    
    public ProcFileIterator(int param1Int) {
      this.mSize = param1Int;
    }
    
    public boolean hasNextLine() {
      boolean bool;
      if (this.mPos < this.mSize) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public CharBuffer nextLine() {
      if (this.mPos >= this.mSize)
        return null; 
      int i = this.mPos;
      while (i < this.mSize && KernelCpuProcStringReader.this.mBuf[i] != '\n')
        i++; 
      int j = this.mPos;
      this.mPos = i + 1;
      return CharBuffer.wrap(KernelCpuProcStringReader.this.mBuf, j, i - j);
    }
    
    public int size() {
      return this.mSize;
    }
    
    public void close() {
      KernelCpuProcStringReader.this.mReadLock.unlock();
    }
  }
  
  public static int asLongs(CharBuffer paramCharBuffer, long[] paramArrayOflong) {
    if (paramCharBuffer == null)
      return -1; 
    int i = paramCharBuffer.position();
    byte b = 0;
    long l = -1L;
    while (paramCharBuffer.remaining() > 0 && b < paramArrayOflong.length) {
      char c = paramCharBuffer.get();
      if (!isNumber(c) && c != ' ' && c != ':') {
        paramCharBuffer.position(i);
        return -2;
      } 
      if (l < 0L) {
        if (isNumber(c))
          l = (c - 48); 
        continue;
      } 
      if (isNumber(c)) {
        long l1 = 10L * l + c - 48L;
        l = l1;
        if (l1 < 0L) {
          paramCharBuffer.position(i);
          return -3;
        } 
        continue;
      } 
      paramArrayOflong[b] = l;
      l = -1L;
      b++;
    } 
    int j = b;
    if (l >= 0L) {
      paramArrayOflong[b] = l;
      j = b + 1;
    } 
    paramCharBuffer.position(i);
    return j;
  }
  
  private static boolean isNumber(char paramChar) {
    boolean bool;
    if (paramChar >= '0' && paramChar <= '9') {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
