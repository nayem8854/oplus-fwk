package com.android.internal.os;

import android.os.StrictMode;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.LongSparseLongArray;
import android.util.Slog;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class KernelMemoryBandwidthStats {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "KernelMemoryBandwidthStats";
  
  private static final String mSysfsFile = "/sys/kernel/memory_state_time/show_stat";
  
  protected final LongSparseLongArray mBandwidthEntries = new LongSparseLongArray();
  
  private boolean mStatsDoNotExist = false;
  
  public void updateStats() {
    if (this.mStatsDoNotExist)
      return; 
    long l = SystemClock.uptimeMillis();
    StrictMode.ThreadPolicy threadPolicy = StrictMode.allowThreadDiskReads();
    try {
      BufferedReader bufferedReader = new BufferedReader();
      null = new FileReader();
      this("/sys/kernel/memory_state_time/show_stat");
      this(null);
      try {
        parseStats(bufferedReader);
      } finally {
        try {
          bufferedReader.close();
        } finally {
          bufferedReader = null;
        } 
      } 
    } catch (FileNotFoundException fileNotFoundException) {
      Slog.w("KernelMemoryBandwidthStats", "No kernel memory bandwidth stats available");
      this.mBandwidthEntries.clear();
      this.mStatsDoNotExist = true;
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Failed to read memory bandwidth: ");
      stringBuilder.append(iOException.getMessage());
      Slog.e("KernelMemoryBandwidthStats", stringBuilder.toString());
      this.mBandwidthEntries.clear();
    } finally {
      Exception exception;
    } 
    StrictMode.setThreadPolicy(threadPolicy);
    l = SystemClock.uptimeMillis() - l;
    if (l > 100L) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Reading memory bandwidth file took ");
      stringBuilder.append(l);
      stringBuilder.append("ms");
      Slog.w("KernelMemoryBandwidthStats", stringBuilder.toString());
    } 
  }
  
  public void parseStats(BufferedReader paramBufferedReader) throws IOException {
    TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(' ');
    this.mBandwidthEntries.clear();
    label17: while (true) {
      String str = paramBufferedReader.readLine();
      if (str != null) {
        simpleStringSplitter.setString(str);
        simpleStringSplitter.next();
        byte b = 0;
        while (true) {
          int i = this.mBandwidthEntries.indexOfKey(b);
          if (i >= 0) {
            LongSparseLongArray longSparseLongArray = this.mBandwidthEntries;
            long l1 = b, l2 = longSparseLongArray.valueAt(i);
            long l3 = Long.parseLong(simpleStringSplitter.next()) / 1000000L;
            longSparseLongArray.put(l1, l2 + l3);
          } else {
            this.mBandwidthEntries.put(b, Long.parseLong(simpleStringSplitter.next()) / 1000000L);
          } 
          b++;
          if (!simpleStringSplitter.hasNext())
            continue label17; 
        } 
      } 
      break;
    } 
  }
  
  public LongSparseLongArray getBandwidthEntries() {
    return this.mBandwidthEntries;
  }
}
