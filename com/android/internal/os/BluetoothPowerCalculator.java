package com.android.internal.os;

import android.os.BatteryStats;

public class BluetoothPowerCalculator extends PowerCalculator {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "BluetoothPowerCalculator";
  
  private double mAppTotalPowerMah = 0.0D;
  
  private long mAppTotalTimeMs = 0L;
  
  private final double mIdleMa;
  
  private final double mRxMa;
  
  private final double mTxMa;
  
  public BluetoothPowerCalculator(PowerProfile paramPowerProfile) {
    this.mIdleMa = paramPowerProfile.getAveragePower("bluetooth.controller.idle");
    this.mRxMa = paramPowerProfile.getAveragePower("bluetooth.controller.rx");
    this.mTxMa = paramPowerProfile.getAveragePower("bluetooth.controller.tx");
  }
  
  public void calculateApp(BatterySipper paramBatterySipper, BatteryStats.Uid paramUid, long paramLong1, long paramLong2, int paramInt) {
    BatteryStats.ControllerActivityCounter controllerActivityCounter = paramUid.getBluetoothControllerActivity();
    if (controllerActivityCounter == null)
      return; 
    paramLong1 = controllerActivityCounter.getIdleTimeCounter().getCountLocked(paramInt);
    long l1 = controllerActivityCounter.getRxTimeCounter().getCountLocked(paramInt);
    paramLong2 = controllerActivityCounter.getTxTimeCounters()[0].getCountLocked(paramInt);
    long l2 = paramLong1 + paramLong2 + l1;
    double d = controllerActivityCounter.getPowerCounter().getCountLocked(paramInt) / 3600000.0D;
    if (d == 0.0D)
      d = (paramLong1 * this.mIdleMa + l1 * this.mRxMa + paramLong2 * this.mTxMa) / 3600000.0D; 
    paramBatterySipper.bluetoothPowerMah = d;
    paramBatterySipper.bluetoothRunningTimeMs = l2;
    paramBatterySipper.btRxBytes = paramUid.getNetworkActivityBytes(4, paramInt);
    paramBatterySipper.btTxBytes = paramUid.getNetworkActivityBytes(5, paramInt);
    this.mAppTotalPowerMah += d;
    this.mAppTotalTimeMs += l2;
  }
  
  public void calculateRemaining(BatterySipper paramBatterySipper, BatteryStats paramBatteryStats, long paramLong1, long paramLong2, int paramInt) {
    BatteryStats.ControllerActivityCounter controllerActivityCounter = paramBatteryStats.getBluetoothControllerActivity();
    paramLong1 = controllerActivityCounter.getIdleTimeCounter().getCountLocked(paramInt);
    long l = controllerActivityCounter.getTxTimeCounters()[0].getCountLocked(paramInt);
    paramLong2 = controllerActivityCounter.getRxTimeCounter().getCountLocked(paramInt);
    double d = controllerActivityCounter.getPowerCounter().getCountLocked(paramInt) / 3600000.0D;
    if (d == 0.0D)
      d = (paramLong1 * this.mIdleMa + paramLong2 * this.mRxMa + l * this.mTxMa) / 3600000.0D; 
    d = Math.max(0.0D, d - this.mAppTotalPowerMah);
    paramBatterySipper.bluetoothPowerMah = d;
    paramBatterySipper.bluetoothRunningTimeMs = Math.max(0L, paramLong1 + l + paramLong2 - this.mAppTotalTimeMs);
  }
  
  public void reset() {
    this.mAppTotalPowerMah = 0.0D;
    this.mAppTotalTimeMs = 0L;
  }
}
