package com.android.internal.os;

import android.net.LocalServerSocket;
import android.net.LocalSocket;
import android.os.SystemClock;
import android.os.Trace;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.system.StructPollfd;
import android.util.Log;
import dalvik.system.ZygoteHooks;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.ToIntFunction;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class ZygoteServer {
  private boolean mUsapPoolEnabled = false;
  
  private int mUsapPoolSizeMax = 0;
  
  private int mUsapPoolSizeMin = 0;
  
  private int mUsapPoolRefillThreshold = 0;
  
  private int mUsapPoolRefillDelayMs = -1;
  
  static final boolean $assertionsDisabled = false;
  
  private static final int INVALID_TIMESTAMP = -1;
  
  public static final String TAG = "ZygoteServer";
  
  private static final String USAP_POOL_REFILL_DELAY_MS_DEFAULT = "3000";
  
  private static final String USAP_POOL_SIZE_MAX_DEFAULT = "10";
  
  private static final int USAP_POOL_SIZE_MAX_LIMIT = 100;
  
  private static final String USAP_POOL_SIZE_MIN_DEFAULT = "1";
  
  private static final int USAP_POOL_SIZE_MIN_LIMIT = 1;
  
  private boolean mCloseSocketFd;
  
  private boolean mIsFirstPropertyCheck;
  
  private boolean mIsForkChild;
  
  private long mLastPropCheckTimestamp;
  
  private final FileDescriptor mUsapPoolEventFD;
  
  private UsapPoolRefillAction mUsapPoolRefillAction;
  
  private long mUsapPoolRefillTriggerTimestamp;
  
  private final LocalServerSocket mUsapPoolSocket;
  
  private final boolean mUsapPoolSupported;
  
  private LocalServerSocket mZygoteSocket;
  
  private enum UsapPoolRefillAction {
    DELAYED, IMMEDIATE, NONE;
    
    private static final UsapPoolRefillAction[] $VALUES;
    
    static {
      UsapPoolRefillAction usapPoolRefillAction = new UsapPoolRefillAction("NONE", 2);
      $VALUES = new UsapPoolRefillAction[] { DELAYED, IMMEDIATE, usapPoolRefillAction };
    }
  }
  
  void setForkChild() {
    this.mIsForkChild = true;
  }
  
  public boolean isUsapPoolEnabled() {
    return this.mUsapPoolEnabled;
  }
  
  void registerServerSocketAtAbstractName(String paramString) {
    if (this.mZygoteSocket == null)
      try {
        LocalServerSocket localServerSocket = new LocalServerSocket();
        this(paramString);
        this.mZygoteSocket = localServerSocket;
        this.mCloseSocketFd = false;
      } catch (IOException iOException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Error binding to abstract socket '");
        stringBuilder.append(paramString);
        stringBuilder.append("'");
        throw new RuntimeException(stringBuilder.toString(), iOException);
      }  
  }
  
  private ZygoteConnection acceptCommandPeer(String paramString) {
    try {
      return createNewConnection(this.mZygoteSocket.accept(), paramString);
    } catch (IOException iOException) {
      throw new RuntimeException("IOException during accept()", iOException);
    } 
  }
  
  protected ZygoteConnection createNewConnection(LocalSocket paramLocalSocket, String paramString) throws IOException {
    return new ZygoteConnection(paramLocalSocket, paramString);
  }
  
  void closeServerSocket() {
    try {
      if (this.mZygoteSocket != null) {
        FileDescriptor fileDescriptor = this.mZygoteSocket.getFileDescriptor();
        this.mZygoteSocket.close();
        if (fileDescriptor != null && this.mCloseSocketFd)
          Os.close(fileDescriptor); 
      } 
    } catch (IOException iOException) {
      Log.e("ZygoteServer", "Zygote:  error closing sockets", iOException);
    } catch (ErrnoException errnoException) {
      Log.e("ZygoteServer", "Zygote:  error closing descriptor", (Throwable)errnoException);
    } 
    this.mZygoteSocket = null;
  }
  
  FileDescriptor getZygoteSocketFileDescriptor() {
    return this.mZygoteSocket.getFileDescriptor();
  }
  
  private void fetchUsapPoolPolicyProps() {
    if (this.mUsapPoolSupported) {
      String str = Zygote.getConfigurationProperty("usap_pool_size_max", "10");
      if (!str.isEmpty())
        this.mUsapPoolSizeMax = Integer.min(Integer.parseInt(str), 100); 
      str = Zygote.getConfigurationProperty("usap_pool_size_min", "1");
      if (!str.isEmpty()) {
        int j = Integer.parseInt(str);
        this.mUsapPoolSizeMin = Integer.max(j, 1);
      } 
      int i = this.mUsapPoolSizeMax / 2;
      str = Integer.toString(i);
      str = Zygote.getConfigurationProperty("usap_refill_threshold", str);
      if (!str.isEmpty()) {
        int j = Integer.parseInt(str);
        i = this.mUsapPoolSizeMax;
        this.mUsapPoolRefillThreshold = Integer.min(j, i);
      } 
      str = Zygote.getConfigurationProperty("usap_pool_refill_delay_ms", "3000");
      if (!str.isEmpty())
        this.mUsapPoolRefillDelayMs = Integer.parseInt(str); 
      if (this.mUsapPoolSizeMin >= this.mUsapPoolSizeMax) {
        Log.w("ZygoteServer", "The max size of the USAP pool must be greater than the minimum size.  Restoring default values.");
        this.mUsapPoolSizeMax = Integer.parseInt("10");
        this.mUsapPoolSizeMin = Integer.parseInt("1");
        this.mUsapPoolRefillThreshold = this.mUsapPoolSizeMax / 2;
      } 
    } 
  }
  
  ZygoteServer() {
    this.mIsFirstPropertyCheck = true;
    this.mLastPropCheckTimestamp = 0L;
    this.mUsapPoolEventFD = null;
    this.mZygoteSocket = null;
    this.mUsapPoolSocket = null;
    this.mUsapPoolSupported = false;
  }
  
  ZygoteServer(boolean paramBoolean) {
    this.mIsFirstPropertyCheck = true;
    this.mLastPropCheckTimestamp = 0L;
    this.mUsapPoolEventFD = Zygote.getUsapPoolEventFD();
    if (paramBoolean) {
      this.mZygoteSocket = Zygote.createManagedSocketFromInitSocket("zygote");
      this.mUsapPoolSocket = Zygote.createManagedSocketFromInitSocket("usap_pool_primary");
    } else {
      this.mZygoteSocket = Zygote.createManagedSocketFromInitSocket("zygote_secondary");
      this.mUsapPoolSocket = Zygote.createManagedSocketFromInitSocket("usap_pool_secondary");
    } 
    this.mUsapPoolSupported = true;
    fetchUsapPoolPolicyProps();
  }
  
  private void fetchUsapPoolPolicyPropsWithMinInterval() {
    long l = SystemClock.elapsedRealtime();
    if (this.mIsFirstPropertyCheck || l - this.mLastPropCheckTimestamp >= 60000L) {
      this.mIsFirstPropertyCheck = false;
      this.mLastPropCheckTimestamp = l;
      fetchUsapPoolPolicyProps();
    } 
  }
  
  private void fetchUsapPoolPolicyPropsIfUnfetched() {
    if (this.mIsFirstPropertyCheck) {
      this.mIsFirstPropertyCheck = false;
      fetchUsapPoolPolicyProps();
    } 
  }
  
  Runnable fillUsapPool(int[] paramArrayOfint, boolean paramBoolean) {
    Trace.traceBegin(64L, "Zygote:FillUsapPool");
    fetchUsapPoolPolicyPropsIfUnfetched();
    int i = Zygote.getUsapPoolCount();
    if (paramBoolean) {
      i = this.mUsapPoolSizeMin - i;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Priority USAP Pool refill. New USAPs: ");
      stringBuilder.append(i);
      Log.i("zygote", stringBuilder.toString());
    } else {
      i = this.mUsapPoolSizeMax - i;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Delayed USAP Pool refill. New USAPs: ");
      stringBuilder.append(i);
      Log.i("zygote", stringBuilder.toString());
    } 
    ZygoteHooks.preFork();
    while (--i >= 0) {
      LocalServerSocket localServerSocket = this.mUsapPoolSocket;
      Runnable runnable = Zygote.forkUsap(localServerSocket, paramArrayOfint, paramBoolean);
      if (runnable != null)
        return runnable; 
    } 
    ZygoteHooks.postForkCommon();
    resetUsapRefillState();
    Trace.traceEnd(64L);
    return null;
  }
  
  Runnable setUsapPoolStatus(boolean paramBoolean, LocalSocket paramLocalSocket) {
    String str;
    if (!this.mUsapPoolSupported) {
      Log.w("ZygoteServer", "Attempting to enable a USAP pool for a Zygote that doesn't support it.");
      return null;
    } 
    if (this.mUsapPoolEnabled == paramBoolean)
      return null; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("USAP Pool status change: ");
    if (paramBoolean) {
      str = "ENABLED";
    } else {
      str = "DISABLED";
    } 
    stringBuilder.append(str);
    Log.i("ZygoteServer", stringBuilder.toString());
    this.mUsapPoolEnabled = paramBoolean;
    if (paramBoolean)
      return fillUsapPool(new int[] { paramLocalSocket.getFileDescriptor().getInt$() }, false); 
    Zygote.emptyUsapPool();
    return null;
  }
  
  void resetUsapRefillState() {
    this.mUsapPoolRefillAction = UsapPoolRefillAction.NONE;
    this.mUsapPoolRefillTriggerTimestamp = -1L;
  }
  
  Runnable runSelectLoop(String paramString) {
    ArrayList<FileDescriptor> arrayList = new ArrayList();
    ArrayList<ZygoteConnection> arrayList1 = new ArrayList();
    arrayList.add(this.mZygoteSocket.getFileDescriptor());
    arrayList1.add(null);
    this.mUsapPoolRefillTriggerTimestamp = -1L;
    while (true) {
      ZygoteConnection zygoteConnection;
      StructPollfd[] arrayOfStructPollfd;
      int i, j;
      fetchUsapPoolPolicyPropsWithMinInterval();
      this.mUsapPoolRefillAction = UsapPoolRefillAction.NONE;
      if (this.mUsapPoolEnabled) {
        zygoteConnection = (ZygoteConnection)Zygote.getUsapPipeFDs();
        arrayOfStructPollfd = new StructPollfd[arrayList.size() + 1 + zygoteConnection.length];
      } else {
        arrayOfStructPollfd = new StructPollfd[arrayList.size()];
        zygoteConnection = null;
      } 
      byte b = 0;
      for (FileDescriptor fileDescriptor : arrayList) {
        arrayOfStructPollfd[b] = new StructPollfd();
        (arrayOfStructPollfd[b]).fd = fileDescriptor;
        (arrayOfStructPollfd[b]).events = (short)OsConstants.POLLIN;
        b++;
      } 
      if (this.mUsapPoolEnabled) {
        arrayOfStructPollfd[b] = new StructPollfd();
        (arrayOfStructPollfd[b]).fd = this.mUsapPoolEventFD;
        (arrayOfStructPollfd[b]).events = (short)OsConstants.POLLIN;
        i = b + 1;
        for (int k = zygoteConnection.length; j < k; ) {
          Object object = zygoteConnection[j];
          FileDescriptor fileDescriptor = new FileDescriptor();
          fileDescriptor.setInt$(object);
          arrayOfStructPollfd[i] = new StructPollfd();
          (arrayOfStructPollfd[i]).fd = fileDescriptor;
          (arrayOfStructPollfd[i]).events = (short)OsConstants.POLLIN;
          i++;
          j++;
        } 
        j = i;
      } else {
        j = b;
      } 
      if (this.mUsapPoolRefillTriggerTimestamp == -1L) {
        i = -1;
      } else {
        long l = System.currentTimeMillis() - this.mUsapPoolRefillTriggerTimestamp;
        i = this.mUsapPoolRefillDelayMs;
        if (l >= i) {
          i = -1;
        } else if (l <= 0L) {
          i = this.mUsapPoolRefillDelayMs;
        } else {
          i = (int)(i - l);
        } 
      } 
      try {
        i = Os.poll(arrayOfStructPollfd, i);
        if (i == 0) {
          this.mUsapPoolRefillTriggerTimestamp = -1L;
          this.mUsapPoolRefillAction = UsapPoolRefillAction.DELAYED;
        } else {
          int k = 0;
          i = j;
          j = k;
          while (true) {
            if (--i >= 0) {
              if (((arrayOfStructPollfd[i]).revents & OsConstants.POLLIN) == 0)
                continue; 
              if (i == 0) {
                zygoteConnection = acceptCommandPeer(paramString);
                arrayList1.add(zygoteConnection);
                arrayList.add(zygoteConnection.getFileDescriptor());
                continue;
              } 
              if (i < b) {
                try {
                  IllegalStateException illegalStateException;
                  ZygoteConnection zygoteConnection1 = arrayList1.get(i);
                  Runnable runnable = zygoteConnection1.processOneCommand(this);
                  boolean bool = this.mIsForkChild;
                  if (bool) {
                    if (runnable != null) {
                      this.mIsForkChild = false;
                      return runnable;
                    } 
                    illegalStateException = new IllegalStateException();
                    this("command == null");
                    throw illegalStateException;
                  } 
                  if (illegalStateException == null) {
                    if (zygoteConnection1.isClosedByPeer()) {
                      zygoteConnection1.closeSocket();
                      arrayList1.remove(i);
                      arrayList.remove(i);
                    } 
                  } else {
                    illegalStateException = new IllegalStateException();
                    this("command != null");
                    throw illegalStateException;
                  } 
                } catch (Exception exception) {
                
                } finally {}
              } else {
                try {
                  byte[] arrayOfByte = new byte[8];
                  FileDescriptor fileDescriptor = (arrayOfStructPollfd[i]).fd;
                  k = arrayOfByte.length;
                  k = Os.read(fileDescriptor, arrayOfByte, 0, k);
                  if (k == 8) {
                    DataInputStream dataInputStream = new DataInputStream();
                    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream();
                    this(arrayOfByte);
                    this(byteArrayInputStream);
                    long l = dataInputStream.readLong();
                    if (i > b)
                      Zygote.removeUsapTableEntry((int)l); 
                    j = 1;
                    continue;
                  } 
                  StringBuilder stringBuilder = new StringBuilder();
                  this();
                  stringBuilder.append("Incomplete read from USAP management FD of size ");
                  stringBuilder.append(k);
                  Log.e("ZygoteServer", stringBuilder.toString());
                } catch (Exception exception) {
                  if (i == b) {
                    StringBuilder stringBuilder1 = new StringBuilder();
                    stringBuilder1.append("Failed to read from USAP pool event FD: ");
                    stringBuilder1.append(exception.getMessage());
                    str = stringBuilder1.toString();
                    Log.e("ZygoteServer", str);
                    continue;
                  } 
                  StringBuilder stringBuilder = new StringBuilder();
                  stringBuilder.append("Failed to read from USAP reporting pipe: ");
                  stringBuilder.append(str.getMessage());
                  String str = stringBuilder.toString();
                  Log.e("ZygoteServer", str);
                } 
                continue;
              } 
            } else {
              break;
            } 
            this.mIsForkChild = false;
          } 
          if (j != 0) {
            i = Zygote.getUsapPoolCount();
            if (i < this.mUsapPoolSizeMin) {
              this.mUsapPoolRefillAction = UsapPoolRefillAction.IMMEDIATE;
            } else if (this.mUsapPoolSizeMax - i >= this.mUsapPoolRefillThreshold) {
              this.mUsapPoolRefillTriggerTimestamp = System.currentTimeMillis();
            } 
          } 
        } 
        if (this.mUsapPoolRefillAction != UsapPoolRefillAction.NONE) {
          i = arrayList.size();
          boolean bool = true;
          List<FileDescriptor> list = arrayList.subList(1, i);
          Stream<FileDescriptor> stream = list.stream();
          -$.Lambda.sHtqZgGVjxOf9IJdAdZO6gwD_Do sHtqZgGVjxOf9IJdAdZO6gwD_Do = _$$Lambda$sHtqZgGVjxOf9IJdAdZO6gwD_Do.INSTANCE;
          IntStream intStream = stream.mapToInt((ToIntFunction<? super FileDescriptor>)sHtqZgGVjxOf9IJdAdZO6gwD_Do);
          int[] arrayOfInt = intStream.toArray();
          if (this.mUsapPoolRefillAction != UsapPoolRefillAction.IMMEDIATE)
            bool = false; 
          Runnable runnable = fillUsapPool(arrayOfInt, bool);
          if (runnable != null)
            return runnable; 
          if (bool)
            this.mUsapPoolRefillTriggerTimestamp = System.currentTimeMillis(); 
        } 
      } catch (ErrnoException errnoException) {
        throw new RuntimeException("poll failed", errnoException);
      } 
    } 
  }
}
