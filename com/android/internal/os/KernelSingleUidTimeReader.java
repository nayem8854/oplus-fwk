package com.android.internal.os;

import android.util.SparseArray;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;

public class KernelSingleUidTimeReader {
  private static final String TAG = KernelSingleUidTimeReader.class.getName();
  
  private SparseArray<long[]> mLastUidCpuTimeMs = (SparseArray)new SparseArray<>();
  
  private boolean mSingleUidCpuTimesAvailable = true;
  
  private boolean mBpfTimesAvailable = true;
  
  private static final boolean DBG = false;
  
  private static final String PROC_FILE_DIR = "/proc/uid/";
  
  private static final String PROC_FILE_NAME = "/time_in_state";
  
  public static final int TOTAL_READ_ERROR_COUNT = 5;
  
  private static final String UID_TIMES_PROC_FILE = "/proc/uid_time_in_state";
  
  private final int mCpuFreqsCount;
  
  private boolean mCpuFreqsCountVerified;
  
  private final Injector mInjector;
  
  private int mReadErrorCounter;
  
  KernelSingleUidTimeReader(int paramInt) {
    this(paramInt, new Injector());
  }
  
  public KernelSingleUidTimeReader(int paramInt, Injector paramInjector) {
    this.mInjector = paramInjector;
    this.mCpuFreqsCount = paramInt;
    if (paramInt == 0)
      this.mSingleUidCpuTimesAvailable = false; 
  }
  
  public boolean singleUidCpuTimesAvailable() {
    return this.mSingleUidCpuTimesAvailable;
  }
  
  public long[] readDeltaMs(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mSingleUidCpuTimesAvailable : Z
    //   6: ifne -> 13
    //   9: aload_0
    //   10: monitorexit
    //   11: aconst_null
    //   12: areturn
    //   13: aload_0
    //   14: getfield mBpfTimesAvailable : Z
    //   17: ifeq -> 83
    //   20: aload_0
    //   21: getfield mInjector : Lcom/android/internal/os/KernelSingleUidTimeReader$Injector;
    //   24: iload_1
    //   25: invokevirtual readBpfData : (I)[J
    //   28: astore_2
    //   29: aload_2
    //   30: arraylength
    //   31: ifne -> 42
    //   34: aload_0
    //   35: iconst_0
    //   36: putfield mBpfTimesAvailable : Z
    //   39: goto -> 83
    //   42: aload_0
    //   43: getfield mCpuFreqsCountVerified : Z
    //   46: ifne -> 67
    //   49: aload_2
    //   50: arraylength
    //   51: aload_0
    //   52: getfield mCpuFreqsCount : I
    //   55: if_icmpeq -> 67
    //   58: aload_0
    //   59: iconst_0
    //   60: putfield mSingleUidCpuTimesAvailable : Z
    //   63: aload_0
    //   64: monitorexit
    //   65: aconst_null
    //   66: areturn
    //   67: aload_0
    //   68: iconst_1
    //   69: putfield mCpuFreqsCountVerified : Z
    //   72: aload_0
    //   73: iload_1
    //   74: aload_2
    //   75: invokevirtual computeDelta : (I[J)[J
    //   78: astore_2
    //   79: aload_0
    //   80: monitorexit
    //   81: aload_2
    //   82: areturn
    //   83: new java/lang/StringBuilder
    //   86: astore_2
    //   87: aload_2
    //   88: ldc '/proc/uid/'
    //   90: invokespecial <init> : (Ljava/lang/String;)V
    //   93: aload_2
    //   94: iload_1
    //   95: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   98: pop
    //   99: aload_2
    //   100: ldc '/time_in_state'
    //   102: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   105: pop
    //   106: aload_2
    //   107: invokevirtual toString : ()Ljava/lang/String;
    //   110: astore_3
    //   111: aload_0
    //   112: getfield mInjector : Lcom/android/internal/os/KernelSingleUidTimeReader$Injector;
    //   115: aload_3
    //   116: invokevirtual readData : (Ljava/lang/String;)[B
    //   119: astore_2
    //   120: aload_0
    //   121: getfield mCpuFreqsCountVerified : Z
    //   124: ifne -> 134
    //   127: aload_0
    //   128: aload_2
    //   129: arraylength
    //   130: aload_3
    //   131: invokespecial verifyCpuFreqsCount : (ILjava/lang/String;)V
    //   134: aload_2
    //   135: invokestatic wrap : ([B)Ljava/nio/ByteBuffer;
    //   138: astore_2
    //   139: aload_2
    //   140: invokestatic nativeOrder : ()Ljava/nio/ByteOrder;
    //   143: invokevirtual order : (Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
    //   146: pop
    //   147: aload_0
    //   148: aload_2
    //   149: invokespecial readCpuTimesFromByteBuffer : (Ljava/nio/ByteBuffer;)[J
    //   152: astore_2
    //   153: aload_0
    //   154: iload_1
    //   155: aload_2
    //   156: invokevirtual computeDelta : (I[J)[J
    //   159: astore_2
    //   160: aload_0
    //   161: monitorexit
    //   162: aload_2
    //   163: areturn
    //   164: astore_2
    //   165: aload_0
    //   166: getfield mReadErrorCounter : I
    //   169: iconst_1
    //   170: iadd
    //   171: istore_1
    //   172: aload_0
    //   173: iload_1
    //   174: putfield mReadErrorCounter : I
    //   177: iload_1
    //   178: iconst_5
    //   179: if_icmplt -> 187
    //   182: aload_0
    //   183: iconst_0
    //   184: putfield mSingleUidCpuTimesAvailable : Z
    //   187: aload_0
    //   188: monitorexit
    //   189: aconst_null
    //   190: areturn
    //   191: astore_2
    //   192: aload_0
    //   193: monitorexit
    //   194: aload_2
    //   195: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #86	-> 0
    //   #87	-> 2
    //   #88	-> 9
    //   #90	-> 13
    //   #91	-> 20
    //   #92	-> 29
    //   #93	-> 34
    //   #94	-> 42
    //   #95	-> 58
    //   #96	-> 63
    //   #98	-> 67
    //   #99	-> 72
    //   #103	-> 83
    //   #104	-> 93
    //   #105	-> 99
    //   #108	-> 111
    //   #109	-> 120
    //   #110	-> 127
    //   #112	-> 134
    //   #113	-> 139
    //   #114	-> 147
    //   #121	-> 153
    //   #123	-> 153
    //   #115	-> 164
    //   #116	-> 165
    //   #117	-> 182
    //   #120	-> 187
    //   #124	-> 191
    // Exception table:
    //   from	to	target	type
    //   2	9	191	finally
    //   9	11	191	finally
    //   13	20	191	finally
    //   20	29	191	finally
    //   29	34	191	finally
    //   34	39	191	finally
    //   42	58	191	finally
    //   58	63	191	finally
    //   63	65	191	finally
    //   67	72	191	finally
    //   72	81	191	finally
    //   83	93	191	finally
    //   93	99	191	finally
    //   99	111	191	finally
    //   111	120	164	java/lang/Exception
    //   111	120	191	finally
    //   120	127	164	java/lang/Exception
    //   120	127	191	finally
    //   127	134	164	java/lang/Exception
    //   127	134	191	finally
    //   134	139	164	java/lang/Exception
    //   134	139	191	finally
    //   139	147	164	java/lang/Exception
    //   139	147	191	finally
    //   147	153	164	java/lang/Exception
    //   147	153	191	finally
    //   153	162	191	finally
    //   165	177	191	finally
    //   182	187	191	finally
    //   187	189	191	finally
    //   192	194	191	finally
  }
  
  private void verifyCpuFreqsCount(int paramInt, String paramString) {
    paramInt /= 8;
    if (this.mCpuFreqsCount == paramInt) {
      this.mCpuFreqsCountVerified = true;
      return;
    } 
    this.mSingleUidCpuTimesAvailable = false;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Freq count didn't match,count from /proc/uid_time_in_state=");
    stringBuilder.append(this.mCpuFreqsCount);
    stringBuilder.append(", butcount from ");
    stringBuilder.append(paramString);
    stringBuilder.append("=");
    stringBuilder.append(paramInt);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  private long[] readCpuTimesFromByteBuffer(ByteBuffer paramByteBuffer) {
    long[] arrayOfLong = new long[this.mCpuFreqsCount];
    for (byte b = 0; b < this.mCpuFreqsCount; b++)
      arrayOfLong[b] = paramByteBuffer.getLong() * 10L; 
    return arrayOfLong;
  }
  
  public long[] computeDelta(int paramInt, long[] paramArrayOflong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mSingleUidCpuTimesAvailable : Z
    //   6: ifne -> 13
    //   9: aload_0
    //   10: monitorexit
    //   11: aconst_null
    //   12: areturn
    //   13: aload_0
    //   14: getfield mLastUidCpuTimeMs : Landroid/util/SparseArray;
    //   17: iload_1
    //   18: invokevirtual get : (I)Ljava/lang/Object;
    //   21: checkcast [J
    //   24: astore_3
    //   25: aload_0
    //   26: aload_3
    //   27: aload_2
    //   28: invokevirtual getDeltaLocked : ([J[J)[J
    //   31: astore_3
    //   32: aload_3
    //   33: ifnonnull -> 40
    //   36: aload_0
    //   37: monitorexit
    //   38: aconst_null
    //   39: areturn
    //   40: iconst_0
    //   41: istore #4
    //   43: aload_3
    //   44: arraylength
    //   45: iconst_1
    //   46: isub
    //   47: istore #5
    //   49: iload #4
    //   51: istore #6
    //   53: iload #5
    //   55: iflt -> 79
    //   58: aload_3
    //   59: iload #5
    //   61: laload
    //   62: lconst_0
    //   63: lcmp
    //   64: ifle -> 73
    //   67: iconst_1
    //   68: istore #6
    //   70: goto -> 79
    //   73: iinc #5, -1
    //   76: goto -> 49
    //   79: iload #6
    //   81: ifeq -> 97
    //   84: aload_0
    //   85: getfield mLastUidCpuTimeMs : Landroid/util/SparseArray;
    //   88: iload_1
    //   89: aload_2
    //   90: invokevirtual put : (ILjava/lang/Object;)V
    //   93: aload_0
    //   94: monitorexit
    //   95: aload_3
    //   96: areturn
    //   97: aload_0
    //   98: monitorexit
    //   99: aconst_null
    //   100: areturn
    //   101: astore_2
    //   102: aload_0
    //   103: monitorexit
    //   104: aload_2
    //   105: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #155	-> 0
    //   #156	-> 2
    //   #157	-> 9
    //   #160	-> 13
    //   #161	-> 25
    //   #162	-> 32
    //   #166	-> 36
    //   #169	-> 40
    //   #170	-> 43
    //   #171	-> 58
    //   #172	-> 67
    //   #173	-> 70
    //   #170	-> 73
    //   #176	-> 79
    //   #177	-> 84
    //   #178	-> 93
    //   #180	-> 97
    //   #182	-> 101
    // Exception table:
    //   from	to	target	type
    //   2	9	101	finally
    //   9	11	101	finally
    //   13	25	101	finally
    //   25	32	101	finally
    //   36	38	101	finally
    //   43	49	101	finally
    //   84	93	101	finally
    //   93	95	101	finally
    //   97	99	101	finally
    //   102	104	101	finally
  }
  
  public long[] getDeltaLocked(long[] paramArrayOflong1, long[] paramArrayOflong2) {
    int i;
    for (i = paramArrayOflong2.length - 1; i >= 0; i--) {
      if (paramArrayOflong2[i] < 0L)
        return null; 
    } 
    if (paramArrayOflong1 == null)
      return paramArrayOflong2; 
    long[] arrayOfLong = new long[paramArrayOflong2.length];
    for (i = paramArrayOflong2.length - 1; i >= 0; i--) {
      arrayOfLong[i] = paramArrayOflong2[i] - paramArrayOflong1[i];
      if (arrayOfLong[i] < 0L)
        return null; 
    } 
    return arrayOfLong;
  }
  
  public void setAllUidsCpuTimesMs(SparseArray<long[]> paramSparseArray) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mLastUidCpuTimeMs : Landroid/util/SparseArray;
    //   6: invokevirtual clear : ()V
    //   9: aload_1
    //   10: invokevirtual size : ()I
    //   13: iconst_1
    //   14: isub
    //   15: istore_2
    //   16: iload_2
    //   17: iflt -> 58
    //   20: aload_1
    //   21: iload_2
    //   22: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   25: checkcast [J
    //   28: astore_3
    //   29: aload_3
    //   30: ifnull -> 52
    //   33: aload_0
    //   34: getfield mLastUidCpuTimeMs : Landroid/util/SparseArray;
    //   37: aload_1
    //   38: iload_2
    //   39: invokevirtual keyAt : (I)I
    //   42: aload_3
    //   43: invokevirtual clone : ()Ljava/lang/Object;
    //   46: checkcast [J
    //   49: invokevirtual put : (ILjava/lang/Object;)V
    //   52: iinc #2, -1
    //   55: goto -> 16
    //   58: aload_0
    //   59: monitorexit
    //   60: return
    //   61: astore_1
    //   62: aload_0
    //   63: monitorexit
    //   64: aload_1
    //   65: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #214	-> 0
    //   #215	-> 2
    //   #216	-> 9
    //   #217	-> 20
    //   #218	-> 29
    //   #219	-> 33
    //   #216	-> 52
    //   #222	-> 58
    //   #223	-> 60
    //   #222	-> 61
    // Exception table:
    //   from	to	target	type
    //   2	9	61	finally
    //   9	16	61	finally
    //   20	29	61	finally
    //   33	52	61	finally
    //   58	60	61	finally
    //   62	64	61	finally
  }
  
  public void removeUid(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mLastUidCpuTimeMs : Landroid/util/SparseArray;
    //   6: iload_1
    //   7: invokevirtual delete : (I)V
    //   10: aload_0
    //   11: monitorexit
    //   12: return
    //   13: astore_2
    //   14: aload_0
    //   15: monitorexit
    //   16: aload_2
    //   17: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #226	-> 0
    //   #227	-> 2
    //   #228	-> 10
    //   #229	-> 12
    //   #228	-> 13
    // Exception table:
    //   from	to	target	type
    //   2	10	13	finally
    //   10	12	13	finally
    //   14	16	13	finally
  }
  
  public void removeUidsInRange(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: iload_2
    //   1: iload_1
    //   2: if_icmpge -> 6
    //   5: return
    //   6: aload_0
    //   7: monitorenter
    //   8: aload_0
    //   9: getfield mLastUidCpuTimeMs : Landroid/util/SparseArray;
    //   12: iload_1
    //   13: aconst_null
    //   14: invokevirtual put : (ILjava/lang/Object;)V
    //   17: aload_0
    //   18: getfield mLastUidCpuTimeMs : Landroid/util/SparseArray;
    //   21: iload_2
    //   22: aconst_null
    //   23: invokevirtual put : (ILjava/lang/Object;)V
    //   26: aload_0
    //   27: getfield mLastUidCpuTimeMs : Landroid/util/SparseArray;
    //   30: iload_1
    //   31: invokevirtual indexOfKey : (I)I
    //   34: istore_1
    //   35: aload_0
    //   36: getfield mLastUidCpuTimeMs : Landroid/util/SparseArray;
    //   39: iload_2
    //   40: invokevirtual indexOfKey : (I)I
    //   43: istore_2
    //   44: aload_0
    //   45: getfield mLastUidCpuTimeMs : Landroid/util/SparseArray;
    //   48: iload_1
    //   49: iload_2
    //   50: iload_1
    //   51: isub
    //   52: iconst_1
    //   53: iadd
    //   54: invokevirtual removeAtRange : (II)V
    //   57: aload_0
    //   58: monitorexit
    //   59: return
    //   60: astore_3
    //   61: aload_0
    //   62: monitorexit
    //   63: aload_3
    //   64: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #232	-> 0
    //   #233	-> 5
    //   #235	-> 6
    //   #236	-> 8
    //   #237	-> 17
    //   #238	-> 26
    //   #239	-> 35
    //   #240	-> 44
    //   #241	-> 57
    //   #242	-> 59
    //   #241	-> 60
    // Exception table:
    //   from	to	target	type
    //   8	17	60	finally
    //   17	26	60	finally
    //   26	35	60	finally
    //   35	44	60	finally
    //   44	57	60	finally
    //   57	59	60	finally
    //   61	63	60	finally
  }
  
  public static class Injector {
    public native long[] readBpfData(int param1Int);
    
    public byte[] readData(String param1String) throws IOException {
      return Files.readAllBytes(Paths.get(param1String, new String[0]));
    }
  }
  
  public SparseArray<long[]> getLastUidCpuTimeMs() {
    return this.mLastUidCpuTimeMs;
  }
  
  public void setSingleUidCpuTimesAvailable(boolean paramBoolean) {
    this.mSingleUidCpuTimesAvailable = paramBoolean;
  }
  
  private static final native boolean canReadBpfTimes();
}
