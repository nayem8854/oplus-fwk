package com.android.internal.os;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

@Deprecated
public class HandlerCaller {
  final Callback mCallback;
  
  final Handler mH;
  
  final Looper mMainLooper;
  
  public static interface Callback {
    void executeMessage(Message param1Message);
  }
  
  class MyHandler extends Handler {
    final HandlerCaller this$0;
    
    MyHandler(Looper param1Looper, boolean param1Boolean) {
      super(param1Looper, null, param1Boolean);
    }
    
    public void handleMessage(Message param1Message) {
      HandlerCaller.this.mCallback.executeMessage(param1Message);
    }
  }
  
  public HandlerCaller(Context paramContext, Looper paramLooper, Callback paramCallback, boolean paramBoolean) {
    Looper looper;
    if (paramLooper != null) {
      looper = paramLooper;
    } else {
      looper = looper.getMainLooper();
    } 
    this.mMainLooper = looper;
    this.mH = new MyHandler(this.mMainLooper, paramBoolean);
    this.mCallback = paramCallback;
  }
  
  public Handler getHandler() {
    return this.mH;
  }
  
  public void executeOrSendMessage(Message paramMessage) {
    if (Looper.myLooper() == this.mMainLooper) {
      this.mCallback.executeMessage(paramMessage);
      paramMessage.recycle();
      return;
    } 
    this.mH.sendMessage(paramMessage);
  }
  
  public void sendMessageDelayed(Message paramMessage, long paramLong) {
    this.mH.sendMessageDelayed(paramMessage, paramLong);
  }
  
  public boolean hasMessages(int paramInt) {
    return this.mH.hasMessages(paramInt);
  }
  
  public void removeMessages(int paramInt) {
    this.mH.removeMessages(paramInt);
  }
  
  public void removeMessages(int paramInt, Object paramObject) {
    this.mH.removeMessages(paramInt, paramObject);
  }
  
  public void sendMessage(Message paramMessage) {
    this.mH.sendMessage(paramMessage);
  }
  
  public SomeArgs sendMessageAndWait(Message paramMessage) {
    // Byte code:
    //   0: invokestatic myLooper : ()Landroid/os/Looper;
    //   3: aload_0
    //   4: getfield mH : Landroid/os/Handler;
    //   7: invokevirtual getLooper : ()Landroid/os/Looper;
    //   10: if_acmpeq -> 73
    //   13: aload_1
    //   14: getfield obj : Ljava/lang/Object;
    //   17: checkcast com/android/internal/os/SomeArgs
    //   20: astore_2
    //   21: aload_2
    //   22: iconst_1
    //   23: putfield mWaitState : I
    //   26: aload_0
    //   27: getfield mH : Landroid/os/Handler;
    //   30: aload_1
    //   31: invokevirtual sendMessage : (Landroid/os/Message;)Z
    //   34: pop
    //   35: aload_2
    //   36: monitorenter
    //   37: aload_2
    //   38: getfield mWaitState : I
    //   41: istore_3
    //   42: iload_3
    //   43: iconst_1
    //   44: if_icmpne -> 59
    //   47: aload_2
    //   48: invokevirtual wait : ()V
    //   51: goto -> 37
    //   54: astore_1
    //   55: aload_2
    //   56: monitorexit
    //   57: aconst_null
    //   58: areturn
    //   59: aload_2
    //   60: monitorexit
    //   61: aload_2
    //   62: iconst_0
    //   63: putfield mWaitState : I
    //   66: aload_2
    //   67: areturn
    //   68: astore_1
    //   69: aload_2
    //   70: monitorexit
    //   71: aload_1
    //   72: athrow
    //   73: new java/lang/IllegalStateException
    //   76: dup
    //   77: ldc 'Can't wait on same thread as looper'
    //   79: invokespecial <init> : (Ljava/lang/String;)V
    //   82: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #98	-> 0
    //   #101	-> 13
    //   #102	-> 21
    //   #103	-> 26
    //   #104	-> 35
    //   #105	-> 37
    //   #107	-> 47
    //   #110	-> 51
    //   #108	-> 54
    //   #109	-> 55
    //   #112	-> 59
    //   #113	-> 61
    //   #114	-> 66
    //   #112	-> 68
    //   #99	-> 73
    // Exception table:
    //   from	to	target	type
    //   37	42	68	finally
    //   47	51	54	java/lang/InterruptedException
    //   47	51	68	finally
    //   55	57	68	finally
    //   59	61	68	finally
    //   69	71	68	finally
  }
  
  public Message obtainMessage(int paramInt) {
    return this.mH.obtainMessage(paramInt);
  }
  
  public Message obtainMessageBO(int paramInt, boolean paramBoolean, Object paramObject) {
    return this.mH.obtainMessage(paramInt, paramBoolean, 0, paramObject);
  }
  
  public Message obtainMessageBOO(int paramInt, boolean paramBoolean, Object paramObject1, Object paramObject2) {
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.arg1 = paramObject1;
    someArgs.arg2 = paramObject2;
    return this.mH.obtainMessage(paramInt, paramBoolean, 0, someArgs);
  }
  
  public Message obtainMessageO(int paramInt, Object paramObject) {
    return this.mH.obtainMessage(paramInt, 0, 0, paramObject);
  }
  
  public Message obtainMessageI(int paramInt1, int paramInt2) {
    return this.mH.obtainMessage(paramInt1, paramInt2, 0);
  }
  
  public Message obtainMessageII(int paramInt1, int paramInt2, int paramInt3) {
    return this.mH.obtainMessage(paramInt1, paramInt2, paramInt3);
  }
  
  public Message obtainMessageIO(int paramInt1, int paramInt2, Object paramObject) {
    return this.mH.obtainMessage(paramInt1, paramInt2, 0, paramObject);
  }
  
  public Message obtainMessageIIO(int paramInt1, int paramInt2, int paramInt3, Object paramObject) {
    return this.mH.obtainMessage(paramInt1, paramInt2, paramInt3, paramObject);
  }
  
  public Message obtainMessageIIOO(int paramInt1, int paramInt2, int paramInt3, Object paramObject1, Object paramObject2) {
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.arg1 = paramObject1;
    someArgs.arg2 = paramObject2;
    return this.mH.obtainMessage(paramInt1, paramInt2, paramInt3, someArgs);
  }
  
  public Message obtainMessageIOO(int paramInt1, int paramInt2, Object paramObject1, Object paramObject2) {
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.arg1 = paramObject1;
    someArgs.arg2 = paramObject2;
    return this.mH.obtainMessage(paramInt1, paramInt2, 0, someArgs);
  }
  
  public Message obtainMessageIOOO(int paramInt1, int paramInt2, Object paramObject1, Object paramObject2, Object paramObject3) {
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.arg1 = paramObject1;
    someArgs.arg2 = paramObject2;
    someArgs.arg3 = paramObject3;
    return this.mH.obtainMessage(paramInt1, paramInt2, 0, someArgs);
  }
  
  public Message obtainMessageIIOOO(int paramInt1, int paramInt2, int paramInt3, Object paramObject1, Object paramObject2, Object paramObject3) {
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.arg1 = paramObject1;
    someArgs.arg2 = paramObject2;
    someArgs.arg3 = paramObject3;
    return this.mH.obtainMessage(paramInt1, paramInt2, paramInt3, someArgs);
  }
  
  public Message obtainMessageIIOOOO(int paramInt1, int paramInt2, int paramInt3, Object paramObject1, Object paramObject2, Object paramObject3, Object paramObject4) {
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.arg1 = paramObject1;
    someArgs.arg2 = paramObject2;
    someArgs.arg3 = paramObject3;
    someArgs.arg4 = paramObject4;
    return this.mH.obtainMessage(paramInt1, paramInt2, paramInt3, someArgs);
  }
  
  public Message obtainMessageOO(int paramInt, Object paramObject1, Object paramObject2) {
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.arg1 = paramObject1;
    someArgs.arg2 = paramObject2;
    return this.mH.obtainMessage(paramInt, 0, 0, someArgs);
  }
  
  public Message obtainMessageOOO(int paramInt, Object paramObject1, Object paramObject2, Object paramObject3) {
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.arg1 = paramObject1;
    someArgs.arg2 = paramObject2;
    someArgs.arg3 = paramObject3;
    return this.mH.obtainMessage(paramInt, 0, 0, someArgs);
  }
  
  public Message obtainMessageOOOO(int paramInt, Object paramObject1, Object paramObject2, Object paramObject3, Object paramObject4) {
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.arg1 = paramObject1;
    someArgs.arg2 = paramObject2;
    someArgs.arg3 = paramObject3;
    someArgs.arg4 = paramObject4;
    return this.mH.obtainMessage(paramInt, 0, 0, someArgs);
  }
  
  public Message obtainMessageOOOOO(int paramInt, Object paramObject1, Object paramObject2, Object paramObject3, Object paramObject4, Object paramObject5) {
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.arg1 = paramObject1;
    someArgs.arg2 = paramObject2;
    someArgs.arg3 = paramObject3;
    someArgs.arg4 = paramObject4;
    someArgs.arg5 = paramObject5;
    return this.mH.obtainMessage(paramInt, 0, 0, someArgs);
  }
  
  public Message obtainMessageOOOOII(int paramInt1, Object paramObject1, Object paramObject2, Object paramObject3, Object paramObject4, int paramInt2, int paramInt3) {
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.arg1 = paramObject1;
    someArgs.arg2 = paramObject2;
    someArgs.arg3 = paramObject3;
    someArgs.arg4 = paramObject4;
    someArgs.argi5 = paramInt2;
    someArgs.argi6 = paramInt3;
    return this.mH.obtainMessage(paramInt1, 0, 0, someArgs);
  }
  
  public Message obtainMessageIIII(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.argi1 = paramInt2;
    someArgs.argi2 = paramInt3;
    someArgs.argi3 = paramInt4;
    someArgs.argi4 = paramInt5;
    return this.mH.obtainMessage(paramInt1, 0, 0, someArgs);
  }
  
  public Message obtainMessageIIIIII(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7) {
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.argi1 = paramInt2;
    someArgs.argi2 = paramInt3;
    someArgs.argi3 = paramInt4;
    someArgs.argi4 = paramInt5;
    someArgs.argi5 = paramInt6;
    someArgs.argi6 = paramInt7;
    return this.mH.obtainMessage(paramInt1, 0, 0, someArgs);
  }
  
  public Message obtainMessageIIIIO(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, Object paramObject) {
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.arg1 = paramObject;
    someArgs.argi1 = paramInt2;
    someArgs.argi2 = paramInt3;
    someArgs.argi3 = paramInt4;
    someArgs.argi4 = paramInt5;
    return this.mH.obtainMessage(paramInt1, 0, 0, someArgs);
  }
}
