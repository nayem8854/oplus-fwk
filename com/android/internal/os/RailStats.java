package com.android.internal.os;

import android.util.ArrayMap;
import android.util.Slog;
import java.util.Map;

public final class RailStats {
  private Map<Long, RailInfoData> mRailInfoData = new ArrayMap<>();
  
  private long mCellularTotalEnergyUseduWs = 0L;
  
  private long mWifiTotalEnergyUseduWs = 0L;
  
  private boolean mRailStatsAvailability = true;
  
  private static final String CELLULAR_SUBSYSTEM = "cellular";
  
  private static final String TAG = "RailStats";
  
  private static final String WIFI_SUBSYSTEM = "wifi";
  
  public void updateRailData(long paramLong1, String paramString1, String paramString2, long paramLong2, long paramLong3) {
    // Byte code:
    //   0: aload #4
    //   2: ldc 'wifi'
    //   4: invokevirtual equals : (Ljava/lang/Object;)Z
    //   7: ifne -> 21
    //   10: aload #4
    //   12: ldc 'cellular'
    //   14: invokevirtual equals : (Ljava/lang/Object;)Z
    //   17: ifne -> 21
    //   20: return
    //   21: aload_0
    //   22: getfield mRailInfoData : Ljava/util/Map;
    //   25: lload_1
    //   26: invokestatic valueOf : (J)Ljava/lang/Long;
    //   29: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   34: checkcast com/android/internal/os/RailStats$RailInfoData
    //   37: astore #9
    //   39: aload #9
    //   41: ifnonnull -> 118
    //   44: aload_0
    //   45: getfield mRailInfoData : Ljava/util/Map;
    //   48: lload_1
    //   49: invokestatic valueOf : (J)Ljava/lang/Long;
    //   52: new com/android/internal/os/RailStats$RailInfoData
    //   55: dup
    //   56: lload_1
    //   57: aload_3
    //   58: aload #4
    //   60: lload #5
    //   62: lload #7
    //   64: aconst_null
    //   65: invokespecial <init> : (JLjava/lang/String;Ljava/lang/String;JJLcom/android/internal/os/RailStats$1;)V
    //   68: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   73: pop
    //   74: aload #4
    //   76: ldc 'wifi'
    //   78: invokevirtual equals : (Ljava/lang/Object;)Z
    //   81: ifeq -> 96
    //   84: aload_0
    //   85: aload_0
    //   86: getfield mWifiTotalEnergyUseduWs : J
    //   89: lload #7
    //   91: ladd
    //   92: putfield mWifiTotalEnergyUseduWs : J
    //   95: return
    //   96: aload #4
    //   98: ldc 'cellular'
    //   100: invokevirtual equals : (Ljava/lang/Object;)Z
    //   103: ifeq -> 117
    //   106: aload_0
    //   107: aload_0
    //   108: getfield mCellularTotalEnergyUseduWs : J
    //   111: lload #7
    //   113: ladd
    //   114: putfield mCellularTotalEnergyUseduWs : J
    //   117: return
    //   118: aload #9
    //   120: getfield timestampSinceBootMs : J
    //   123: lstore_1
    //   124: lload #7
    //   126: aload #9
    //   128: getfield energyUsedSinceBootuWs : J
    //   131: lsub
    //   132: lstore #10
    //   134: lload #5
    //   136: lload_1
    //   137: lsub
    //   138: lconst_0
    //   139: lcmp
    //   140: iflt -> 153
    //   143: lload #10
    //   145: lstore_1
    //   146: lload #10
    //   148: lconst_0
    //   149: lcmp
    //   150: ifge -> 159
    //   153: aload #9
    //   155: getfield energyUsedSinceBootuWs : J
    //   158: lstore_1
    //   159: aload #9
    //   161: lload #5
    //   163: putfield timestampSinceBootMs : J
    //   166: aload #9
    //   168: lload #7
    //   170: putfield energyUsedSinceBootuWs : J
    //   173: aload #4
    //   175: ldc 'wifi'
    //   177: invokevirtual equals : (Ljava/lang/Object;)Z
    //   180: ifeq -> 194
    //   183: aload_0
    //   184: aload_0
    //   185: getfield mWifiTotalEnergyUseduWs : J
    //   188: lload_1
    //   189: ladd
    //   190: putfield mWifiTotalEnergyUseduWs : J
    //   193: return
    //   194: aload #4
    //   196: ldc 'cellular'
    //   198: invokevirtual equals : (Ljava/lang/Object;)Z
    //   201: ifeq -> 214
    //   204: aload_0
    //   205: aload_0
    //   206: getfield mCellularTotalEnergyUseduWs : J
    //   209: lload_1
    //   210: ladd
    //   211: putfield mCellularTotalEnergyUseduWs : J
    //   214: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #46	-> 0
    //   #47	-> 20
    //   #49	-> 21
    //   #50	-> 39
    //   #51	-> 44
    //   #53	-> 74
    //   #54	-> 84
    //   #55	-> 95
    //   #57	-> 96
    //   #58	-> 106
    //   #60	-> 117
    //   #62	-> 118
    //   #63	-> 124
    //   #64	-> 134
    //   #65	-> 153
    //   #67	-> 159
    //   #68	-> 166
    //   #69	-> 173
    //   #70	-> 183
    //   #71	-> 193
    //   #73	-> 194
    //   #74	-> 204
    //   #76	-> 214
  }
  
  public void resetCellularTotalEnergyUsed() {
    this.mCellularTotalEnergyUseduWs = 0L;
  }
  
  public void resetWifiTotalEnergyUsed() {
    this.mWifiTotalEnergyUseduWs = 0L;
  }
  
  public long getCellularTotalEnergyUseduWs() {
    return this.mCellularTotalEnergyUseduWs;
  }
  
  public long getWifiTotalEnergyUseduWs() {
    return this.mWifiTotalEnergyUseduWs;
  }
  
  public void reset() {
    this.mCellularTotalEnergyUseduWs = 0L;
    this.mWifiTotalEnergyUseduWs = 0L;
  }
  
  public RailStats getRailStats() {
    return this;
  }
  
  public void setRailStatsAvailability(boolean paramBoolean) {
    this.mRailStatsAvailability = paramBoolean;
  }
  
  public boolean isRailStatsAvailable() {
    return this.mRailStatsAvailability;
  }
  
  public static class RailInfoData {
    private static final String TAG = "RailInfoData";
    
    public long energyUsedSinceBootuWs;
    
    public long index;
    
    public String railName;
    
    public String subSystemName;
    
    public long timestampSinceBootMs;
    
    private RailInfoData(long param1Long1, String param1String1, String param1String2, long param1Long2, long param1Long3) {
      this.index = param1Long1;
      this.railName = param1String1;
      this.subSystemName = param1String2;
      this.timestampSinceBootMs = param1Long2;
      this.energyUsedSinceBootuWs = param1Long3;
    }
    
    public void printData() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Index = ");
      stringBuilder.append(this.index);
      Slog.d("RailInfoData", stringBuilder.toString());
      stringBuilder = new StringBuilder();
      stringBuilder.append("RailName = ");
      stringBuilder.append(this.railName);
      Slog.d("RailInfoData", stringBuilder.toString());
      stringBuilder = new StringBuilder();
      stringBuilder.append("SubSystemName = ");
      stringBuilder.append(this.subSystemName);
      Slog.d("RailInfoData", stringBuilder.toString());
      stringBuilder = new StringBuilder();
      stringBuilder.append("TimestampSinceBootMs = ");
      stringBuilder.append(this.timestampSinceBootMs);
      Slog.d("RailInfoData", stringBuilder.toString());
      stringBuilder = new StringBuilder();
      stringBuilder.append("EnergyUsedSinceBootuWs = ");
      stringBuilder.append(this.energyUsedSinceBootuWs);
      Slog.d("RailInfoData", stringBuilder.toString());
    }
  }
}
