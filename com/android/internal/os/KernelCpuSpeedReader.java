package com.android.internal.os;

import android.os.StrictMode;
import android.system.Os;
import android.system.OsConstants;
import android.text.TextUtils;
import android.util.Slog;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class KernelCpuSpeedReader {
  private static final String TAG = "KernelCpuSpeedReader";
  
  private final long[] mDeltaSpeedTimesMs;
  
  private final long mJiffyMillis;
  
  private final long[] mLastSpeedTimesMs;
  
  private final int mNumSpeedSteps;
  
  private final String mProcFile;
  
  public KernelCpuSpeedReader(int paramInt1, int paramInt2) {
    this.mProcFile = String.format("/sys/devices/system/cpu/cpu%d/cpufreq/stats/time_in_state", new Object[] { Integer.valueOf(paramInt1) });
    this.mNumSpeedSteps = paramInt2;
    this.mLastSpeedTimesMs = new long[paramInt2];
    this.mDeltaSpeedTimesMs = new long[paramInt2];
    long l = Os.sysconf(OsConstants._SC_CLK_TCK);
    this.mJiffyMillis = 1000L / l;
  }
  
  public long[] readDelta() {
    StrictMode.ThreadPolicy threadPolicy = StrictMode.allowThreadDiskReads();
    try {
      BufferedReader bufferedReader = new BufferedReader();
      null = new FileReader();
      this(this.mProcFile);
      this(null);
      try {
        TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter();
        this(' ');
        byte b = 0;
        while (b < this.mLastSpeedTimesMs.length) {
          String str = bufferedReader.readLine();
          if (str != null) {
            try {
              simpleStringSplitter.setString(str);
              simpleStringSplitter.next();
              long l = Long.parseLong(simpleStringSplitter.next()) * this.mJiffyMillis;
              if (l < this.mLastSpeedTimesMs[b]) {
                this.mDeltaSpeedTimesMs[b] = l;
              } else {
                this.mDeltaSpeedTimesMs[b] = l - this.mLastSpeedTimesMs[b];
              } 
              this.mLastSpeedTimesMs[b] = l;
            } catch (NumberFormatException numberFormatException) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("speedIndex = ");
              stringBuilder.append(b);
              stringBuilder.append(", Failed to read cpu-freq: ");
              stringBuilder.append(numberFormatException.getMessage());
              Slog.e("KernelCpuSpeedReader", stringBuilder.toString());
              this.mDeltaSpeedTimesMs[b] = 0L;
              this.mLastSpeedTimesMs[b] = 0L;
              b++;
            } 
            b++;
          } 
        } 
      } finally {
        try {
          bufferedReader.close();
        } finally {
          bufferedReader = null;
        } 
      } 
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Failed to read cpu-freq: ");
      stringBuilder.append(iOException.getMessage());
      Slog.e("KernelCpuSpeedReader", stringBuilder.toString());
      Arrays.fill(this.mDeltaSpeedTimesMs, 0L);
    } finally {
      Exception exception;
    } 
    StrictMode.setThreadPolicy(threadPolicy);
    return this.mDeltaSpeedTimesMs;
  }
  
  public long[] readAbsolute() {
    StrictMode.ThreadPolicy threadPolicy = StrictMode.allowThreadDiskReads();
    long[] arrayOfLong = new long[this.mNumSpeedSteps];
    try {
      BufferedReader bufferedReader = new BufferedReader();
      null = new FileReader();
      this(this.mProcFile);
      this(null);
      try {
        TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter();
        this(' ');
        byte b = 0;
        while (b < this.mNumSpeedSteps) {
          String str = bufferedReader.readLine();
          if (str != null) {
            simpleStringSplitter.setString(str);
            simpleStringSplitter.next();
            long l1 = Long.parseLong(simpleStringSplitter.next()), l2 = this.mJiffyMillis;
            arrayOfLong[b] = l1 * l2;
            b++;
          } 
        } 
      } finally {
        try {
          bufferedReader.close();
        } finally {
          bufferedReader = null;
        } 
      } 
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Failed to read cpu-freq: ");
      stringBuilder.append(iOException.getMessage());
      Slog.e("KernelCpuSpeedReader", stringBuilder.toString());
      Arrays.fill(arrayOfLong, 0L);
    } finally {}
    StrictMode.setThreadPolicy(threadPolicy);
    return arrayOfLong;
  }
}
