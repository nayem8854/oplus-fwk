package com.android.internal.os;

import android.os.Binder;
import android.os.DropBoxManager;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IDropBoxManagerService extends IInterface {
  void add(DropBoxManager.Entry paramEntry) throws RemoteException;
  
  DropBoxManager.Entry getNextEntry(String paramString1, long paramLong, String paramString2) throws RemoteException;
  
  boolean isTagEnabled(String paramString) throws RemoteException;
  
  class Default implements IDropBoxManagerService {
    public void add(DropBoxManager.Entry param1Entry) throws RemoteException {}
    
    public boolean isTagEnabled(String param1String) throws RemoteException {
      return false;
    }
    
    public DropBoxManager.Entry getNextEntry(String param1String1, long param1Long, String param1String2) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDropBoxManagerService {
    private static final String DESCRIPTOR = "com.android.internal.os.IDropBoxManagerService";
    
    static final int TRANSACTION_add = 1;
    
    static final int TRANSACTION_getNextEntry = 3;
    
    static final int TRANSACTION_isTagEnabled = 2;
    
    public Stub() {
      attachInterface(this, "com.android.internal.os.IDropBoxManagerService");
    }
    
    public static IDropBoxManagerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.os.IDropBoxManagerService");
      if (iInterface != null && iInterface instanceof IDropBoxManagerService)
        return (IDropBoxManagerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "getNextEntry";
        } 
        return "isTagEnabled";
      } 
      return "add";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1) {
        DropBoxManager.Entry entry;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.android.internal.os.IDropBoxManagerService");
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.internal.os.IDropBoxManagerService");
          String str2 = param1Parcel1.readString();
          long l = param1Parcel1.readLong();
          String str1 = param1Parcel1.readString();
          entry = getNextEntry(str2, l, str1);
          param1Parcel2.writeNoException();
          if (entry != null) {
            param1Parcel2.writeInt(1);
            entry.writeToParcel(param1Parcel2, 1);
          } else {
            param1Parcel2.writeInt(0);
          } 
          return true;
        } 
        entry.enforceInterface("com.android.internal.os.IDropBoxManagerService");
        str = entry.readString();
        boolean bool = isTagEnabled(str);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool);
        return true;
      } 
      str.enforceInterface("com.android.internal.os.IDropBoxManagerService");
      if (str.readInt() != 0) {
        DropBoxManager.Entry entry = (DropBoxManager.Entry)DropBoxManager.Entry.CREATOR.createFromParcel((Parcel)str);
      } else {
        str = null;
      } 
      add((DropBoxManager.Entry)str);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IDropBoxManagerService {
      public static IDropBoxManagerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.os.IDropBoxManagerService";
      }
      
      public void add(DropBoxManager.Entry param2Entry) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.os.IDropBoxManagerService");
          if (param2Entry != null) {
            parcel1.writeInt(1);
            param2Entry.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IDropBoxManagerService.Stub.getDefaultImpl() != null) {
            IDropBoxManagerService.Stub.getDefaultImpl().add(param2Entry);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isTagEnabled(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.os.IDropBoxManagerService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IDropBoxManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IDropBoxManagerService.Stub.getDefaultImpl().isTagEnabled(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public DropBoxManager.Entry getNextEntry(String param2String1, long param2Long, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.os.IDropBoxManagerService");
          parcel1.writeString(param2String1);
          parcel1.writeLong(param2Long);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IDropBoxManagerService.Stub.getDefaultImpl() != null)
            return IDropBoxManagerService.Stub.getDefaultImpl().getNextEntry(param2String1, param2Long, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            DropBoxManager.Entry entry = (DropBoxManager.Entry)DropBoxManager.Entry.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (DropBoxManager.Entry)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDropBoxManagerService param1IDropBoxManagerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDropBoxManagerService != null) {
          Proxy.sDefaultImpl = param1IDropBoxManagerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDropBoxManagerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
