package com.android.internal.os;

import android.app.AppGlobals;
import android.content.pm.IPackageManager;
import android.content.pm.PackageInfo;
import android.content.pm.ParceledListSlice;
import android.os.RemoteException;
import android.os.UserHandle;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class AppIdToPackageMap {
  private final Map<Integer, String> mAppIdToPackageMap;
  
  public AppIdToPackageMap(Map<Integer, String> paramMap) {
    this.mAppIdToPackageMap = paramMap;
  }
  
  public static AppIdToPackageMap getSnapshot() {
    try {
      IPackageManager iPackageManager = AppGlobals.getPackageManager();
      ParceledListSlice parceledListSlice = iPackageManager.getInstalledPackages(794624, 0);
      List list = parceledListSlice.getList();
      HashMap<Object, Object> hashMap = new HashMap<>();
      for (PackageInfo packageInfo : list) {
        int i = packageInfo.applicationInfo.uid;
        if (packageInfo.sharedUserId != null && hashMap.containsKey(Integer.valueOf(i))) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("shared:");
          stringBuilder.append(packageInfo.sharedUserId);
          hashMap.put(Integer.valueOf(i), stringBuilder.toString());
          continue;
        } 
        hashMap.put(Integer.valueOf(i), packageInfo.packageName);
      } 
      return new AppIdToPackageMap((Map)hashMap);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String mapAppId(int paramInt) {
    String str = this.mAppIdToPackageMap.get(Integer.valueOf(paramInt));
    if (str == null)
      str = String.valueOf(paramInt); 
    return str;
  }
  
  public String mapUid(int paramInt) {
    int i = UserHandle.getAppId(paramInt);
    String str1 = this.mAppIdToPackageMap.get(Integer.valueOf(i));
    String str2 = UserHandle.formatUid(paramInt);
    if (str1 != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str1);
      stringBuilder.append('/');
      stringBuilder.append(str2);
      str2 = stringBuilder.toString();
    } 
    return str2;
  }
}
