package com.android.internal.os;

import android.os.BatteryStats;

public class WifiPowerCalculator extends PowerCalculator {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "WifiPowerCalculator";
  
  private final double mIdleCurrentMa;
  
  private final double mRxCurrentMa;
  
  private double mTotalAppPowerDrain = 0.0D;
  
  private long mTotalAppRunningTime = 0L;
  
  private final double mTxCurrentMa;
  
  public WifiPowerCalculator(PowerProfile paramPowerProfile) {
    this.mIdleCurrentMa = paramPowerProfile.getAveragePower("wifi.controller.idle");
    this.mTxCurrentMa = paramPowerProfile.getAveragePower("wifi.controller.tx");
    this.mRxCurrentMa = paramPowerProfile.getAveragePower("wifi.controller.rx");
  }
  
  public void calculateApp(BatterySipper paramBatterySipper, BatteryStats.Uid paramUid, long paramLong1, long paramLong2, int paramInt) {
    BatteryStats.ControllerActivityCounter controllerActivityCounter = paramUid.getWifiControllerActivity();
    if (controllerActivityCounter == null)
      return; 
    long l = controllerActivityCounter.getIdleTimeCounter().getCountLocked(paramInt);
    paramLong1 = controllerActivityCounter.getTxTimeCounters()[0].getCountLocked(paramInt);
    paramLong2 = controllerActivityCounter.getRxTimeCounter().getCountLocked(paramInt);
    paramBatterySipper.wifiRunningTimeMs = l + paramLong2 + paramLong1;
    this.mTotalAppRunningTime += paramBatterySipper.wifiRunningTimeMs;
    paramBatterySipper.wifiPowerMah = (l * this.mIdleCurrentMa + paramLong1 * this.mTxCurrentMa + paramLong2 * this.mRxCurrentMa) / 3600000.0D;
    this.mTotalAppPowerDrain += paramBatterySipper.wifiPowerMah;
    paramBatterySipper.wifiRxPackets = paramUid.getNetworkActivityPackets(2, paramInt);
    paramBatterySipper.wifiTxPackets = paramUid.getNetworkActivityPackets(3, paramInt);
    paramBatterySipper.wifiRxBytes = paramUid.getNetworkActivityBytes(2, paramInt);
    paramBatterySipper.wifiTxBytes = paramUid.getNetworkActivityBytes(3, paramInt);
  }
  
  public void calculateRemaining(BatterySipper paramBatterySipper, BatteryStats paramBatteryStats, long paramLong1, long paramLong2, int paramInt) {
    BatteryStats.ControllerActivityCounter controllerActivityCounter = paramBatteryStats.getWifiControllerActivity();
    paramLong1 = controllerActivityCounter.getIdleTimeCounter().getCountLocked(paramInt);
    long l = controllerActivityCounter.getTxTimeCounters()[0].getCountLocked(paramInt);
    paramLong2 = controllerActivityCounter.getRxTimeCounter().getCountLocked(paramInt);
    paramBatterySipper.wifiRunningTimeMs = Math.max(0L, paramLong1 + paramLong2 + l - this.mTotalAppRunningTime);
    double d = controllerActivityCounter.getPowerCounter().getCountLocked(paramInt) / 3600000.0D;
    if (d == 0.0D)
      d = (paramLong1 * this.mIdleCurrentMa + l * this.mTxCurrentMa + paramLong2 * this.mRxCurrentMa) / 3600000.0D; 
    paramBatterySipper.wifiPowerMah = Math.max(0.0D, d - this.mTotalAppPowerDrain);
  }
  
  public void reset() {
    this.mTotalAppPowerDrain = 0.0D;
    this.mTotalAppRunningTime = 0L;
  }
}
