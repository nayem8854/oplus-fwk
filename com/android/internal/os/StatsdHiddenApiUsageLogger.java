package com.android.internal.os;

import android.metrics.LogMaker;
import android.os.Process;
import com.android.internal.logging.MetricsLogger;
import com.android.internal.util.FrameworkStatsLog;
import dalvik.system.VMRuntime;

class StatsdHiddenApiUsageLogger implements VMRuntime.HiddenApiUsageLogger {
  private final MetricsLogger mMetricsLogger = new MetricsLogger();
  
  private static final StatsdHiddenApiUsageLogger sInstance = new StatsdHiddenApiUsageLogger();
  
  private int mHiddenApiAccessLogSampleRate = 0;
  
  private int mHiddenApiAccessStatslogSampleRate = 0;
  
  static void setHiddenApiAccessLogSampleRates(int paramInt1, int paramInt2) {
    StatsdHiddenApiUsageLogger statsdHiddenApiUsageLogger = sInstance;
    statsdHiddenApiUsageLogger.mHiddenApiAccessLogSampleRate = paramInt1;
    statsdHiddenApiUsageLogger.mHiddenApiAccessStatslogSampleRate = paramInt2;
  }
  
  static StatsdHiddenApiUsageLogger getInstance() {
    return sInstance;
  }
  
  public void hiddenApiUsed(int paramInt1, String paramString1, String paramString2, int paramInt2, boolean paramBoolean) {
    if (paramInt1 < this.mHiddenApiAccessLogSampleRate)
      logUsage(paramString1, paramString2, paramInt2, paramBoolean); 
    if (paramInt1 < this.mHiddenApiAccessStatslogSampleRate)
      newLogUsage(paramString2, paramInt2, paramBoolean); 
  }
  
  private void logUsage(String paramString1, String paramString2, int paramInt, boolean paramBoolean) {
    boolean bool = false;
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            paramInt = bool;
          } else {
            paramInt = 3;
          } 
        } else {
          paramInt = 2;
        } 
      } else {
        paramInt = 1;
      } 
    } else {
      paramInt = 0;
    } 
    LogMaker logMaker2 = new LogMaker(1391);
    LogMaker logMaker1 = logMaker2.setPackageName(paramString1);
    logMaker1 = logMaker1.addTaggedData(1394, paramString2);
    logMaker1 = logMaker1.addTaggedData(1392, Integer.valueOf(paramInt));
    if (paramBoolean)
      logMaker1.addTaggedData(1393, Integer.valueOf(1)); 
    this.mMetricsLogger.write(logMaker1);
  }
  
  private void newLogUsage(String paramString, int paramInt, boolean paramBoolean) {
    int i = 0;
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            paramInt = i;
          } else {
            paramInt = 3;
          } 
        } else {
          paramInt = 2;
        } 
      } else {
        paramInt = 1;
      } 
    } else {
      paramInt = 0;
    } 
    i = Process.myUid();
    FrameworkStatsLog.write(178, i, paramString, paramInt, paramBoolean);
  }
}
