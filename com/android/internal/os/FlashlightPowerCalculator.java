package com.android.internal.os;

import android.os.BatteryStats;

public class FlashlightPowerCalculator extends PowerCalculator {
  private final double mFlashlightPowerOnAvg;
  
  public FlashlightPowerCalculator(PowerProfile paramPowerProfile) {
    this.mFlashlightPowerOnAvg = paramPowerProfile.getAveragePower("camera.flashlight");
  }
  
  public void calculateApp(BatterySipper paramBatterySipper, BatteryStats.Uid paramUid, long paramLong1, long paramLong2, int paramInt) {
    BatteryStats.Timer timer = paramUid.getFlashlightTurnedOnTimer();
    if (timer != null) {
      paramLong1 = timer.getTotalTimeLocked(paramLong1, paramInt) / 1000L;
      paramBatterySipper.flashlightTimeMs = paramLong1;
      paramBatterySipper.flashlightPowerMah = paramLong1 * this.mFlashlightPowerOnAvg / 3600000.0D;
    } else {
      paramBatterySipper.flashlightTimeMs = 0L;
      paramBatterySipper.flashlightPowerMah = 0.0D;
    } 
  }
}
