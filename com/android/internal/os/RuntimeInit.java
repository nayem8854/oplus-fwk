package com.android.internal.os;

import android.app.ActivityManager;
import android.app.ActivityThread;
import android.app.ApplicationErrorReport;
import android.app.IActivityManager;
import android.ddm.DdmRegister;
import android.os.Build;
import android.os.Debug;
import android.os.IBinder;
import android.os.Process;
import android.os.SystemProperties;
import android.os.Trace;
import android.util.Log;
import android.util.Slog;
import com.android.internal.logging.AndroidConfig;
import com.android.server.NetworkManagementSocketTagger;
import dalvik.system.RuntimeHooks;
import dalvik.system.ThreadPrioritySetter;
import dalvik.system.VMRuntime;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.logging.LogManager;
import libcore.content.type.MimeMap;

public class RuntimeInit {
  static final boolean DEBUG = false;
  
  static final String TAG = "AndroidRuntime";
  
  private static boolean initialized;
  
  private static IBinder mApplicationObject;
  
  private static volatile boolean mCrashing = false;
  
  private static volatile ApplicationWtfHandler sDefaultApplicationWtfHandler;
  
  private static int Clog_e(String paramString1, String paramString2, Throwable paramThrowable) {
    return Log.printlns(4, 6, paramString1, paramString2, paramThrowable);
  }
  
  public static void logUncaught(String paramString1, String paramString2, int paramInt, Throwable paramThrowable) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("FATAL EXCEPTION: ");
    stringBuilder.append(paramString1);
    stringBuilder.append("\n");
    if (paramString2 != null) {
      stringBuilder.append("Process: ");
      stringBuilder.append(paramString2);
      stringBuilder.append(", ");
    } 
    stringBuilder.append("PID: ");
    stringBuilder.append(paramInt);
    Clog_e("AndroidRuntime", stringBuilder.toString(), paramThrowable);
  }
  
  private static class LoggingHandler implements Thread.UncaughtExceptionHandler {
    private LoggingHandler() {}
    
    public volatile boolean mTriggered = false;
    
    public void uncaughtException(Thread param1Thread, Throwable param1Throwable) {
      this.mTriggered = true;
      if (RuntimeInit.mCrashing)
        return; 
      if (RuntimeInit.mApplicationObject == null && 1000 == Process.myUid()) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("*** FATAL EXCEPTION IN SYSTEM PROCESS: ");
        stringBuilder.append(param1Thread.getName());
        RuntimeInit.Clog_e("AndroidRuntime", stringBuilder.toString(), param1Throwable);
      } else {
        RuntimeInit.logUncaught(param1Thread.getName(), ActivityThread.currentProcessName(), Process.myPid(), param1Throwable);
      } 
      if ("main".equals(param1Thread.getName()) && param1Thread == ActivityThread.currentActivityThread().getLooper().getThread())
        try {
          param1Thread.getUncaughtExceptionHandler().uncaughtException(param1Thread, param1Throwable);
        } finally {
          param1Throwable = null;
        }  
    }
  }
  
  class KillApplicationHandler implements Thread.UncaughtExceptionHandler {
    private final RuntimeInit.LoggingHandler mLoggingHandler;
    
    public KillApplicationHandler(RuntimeInit this$0) {
      Objects.requireNonNull(this$0);
      this.mLoggingHandler = (RuntimeInit.LoggingHandler)this$0;
    }
    
    public void uncaughtException(Thread param1Thread, Throwable param1Throwable) {
      try {
        ensureLogging(param1Thread, param1Throwable);
        boolean bool = RuntimeInit.mCrashing;
        if (bool) {
          Process.killProcess(Process.myPid());
          return;
        } 
        RuntimeInit.access$002(true);
        if (ActivityThread.currentActivityThread() != null)
          ActivityThread.currentActivityThread().stopProfiling(); 
        IActivityManager iActivityManager = ActivityManager.getService();
        IBinder iBinder = RuntimeInit.mApplicationObject;
        ApplicationErrorReport.ParcelableCrashInfo parcelableCrashInfo = new ApplicationErrorReport.ParcelableCrashInfo();
        this(param1Throwable);
      } finally {
        param1Thread = null;
      } 
      Process.killProcess(Process.myPid());
      System.exit(10);
    }
    
    private void ensureLogging(Thread param1Thread, Throwable param1Throwable) {
      if (!this.mLoggingHandler.mTriggered)
        try {
          this.mLoggingHandler.uncaughtException(param1Thread, param1Throwable);
        } finally {} 
    }
  }
  
  public static void preForkInit() {
    RuntimeHooks.setThreadPrioritySetter(new RuntimeThreadPrioritySetter());
    enableDdms();
    MimeMap.setDefaultSupplier((Supplier)_$$Lambda$6_ytl6NLMGWt_iQr4_PfakNWUKQ.INSTANCE);
  }
  
  private static class RuntimeThreadPrioritySetter implements ThreadPrioritySetter {
    private RuntimeThreadPrioritySetter() {}
    
    private static final int[] NICE_VALUES = new int[] { 19, 16, 13, 10, 0, -2, -4, -5, -6, -8 };
    
    public void setPriority(int param1Int1, int param1Int2) {
      int[] arrayOfInt = NICE_VALUES;
      if (arrayOfInt.length == 10) {
        if (param1Int2 >= 1 && param1Int2 <= 10) {
          Process.setThreadPriority(param1Int1, arrayOfInt[param1Int2 - 1]);
          return;
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Priority out of range: ");
        stringBuilder1.append(param1Int2);
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected NICE_VALUES.length=");
      stringBuilder.append(NICE_VALUES.length);
      throw new AssertionError(stringBuilder.toString());
    }
  }
  
  protected static final void commonInit() {
    LoggingHandler loggingHandler = new LoggingHandler();
    RuntimeHooks.setUncaughtExceptionPreHandler(loggingHandler);
    Thread.setDefaultUncaughtExceptionHandler(new KillApplicationHandler(loggingHandler));
    RuntimeHooks.setTimeZoneIdSupplier((Supplier)_$$Lambda$RuntimeInit$ep4ioD9YINkHI5Q1wZ0N_7VFAOg.INSTANCE);
    LogManager.getLogManager().reset();
    new AndroidConfig();
    String str = getDefaultUserAgent();
    System.setProperty("http.agent", str);
    NetworkManagementSocketTagger.install();
    str = SystemProperties.get("ro.kernel.android.tracing");
    if (str.equals("1")) {
      Slog.i("AndroidRuntime", "NOTE: emulator trace profiling enabled");
      Debug.enableEmulatorTraceOutput();
    } 
    initialized = true;
  }
  
  private static String getDefaultUserAgent() {
    StringBuilder stringBuilder = new StringBuilder(64);
    stringBuilder.append("Dalvik/");
    stringBuilder.append(System.getProperty("java.vm.version"));
    stringBuilder.append(" (Linux; U; Android ");
    String str = Build.VERSION.RELEASE_OR_CODENAME;
    if (str.length() <= 0)
      str = "1.0"; 
    stringBuilder.append(str);
    if ("REL".equals(Build.VERSION.CODENAME)) {
      str = Build.MODEL;
      if (str.length() > 0) {
        stringBuilder.append("; ");
        stringBuilder.append(str);
      } 
    } 
    str = Build.ID;
    if (str.length() > 0) {
      stringBuilder.append(" Build/");
      stringBuilder.append(str);
    } 
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
  
  protected static Runnable findStaticMain(String paramString, String[] paramArrayOfString, ClassLoader paramClassLoader) {
    try {
      Class<?> clazz = Class.forName(paramString, true, paramClassLoader);
      try {
        Method method = clazz.getMethod("main", new Class[] { String[].class });
        int i = method.getModifiers();
        if (Modifier.isStatic(i) && Modifier.isPublic(i))
          return new MethodAndArgsCaller(method, paramArrayOfString); 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Main method is not public and static on ");
        stringBuilder.append(paramString);
        throw new RuntimeException(stringBuilder.toString());
      } catch (NoSuchMethodException noSuchMethodException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Missing static main on ");
        stringBuilder.append(paramString);
        throw new RuntimeException(stringBuilder.toString(), noSuchMethodException);
      } catch (SecurityException securityException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Problem getting static main on ");
        stringBuilder.append(paramString);
        throw new RuntimeException(stringBuilder.toString(), securityException);
      } 
    } catch (ClassNotFoundException classNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Missing class when invoking static main ");
      stringBuilder.append(paramString);
      throw new RuntimeException(stringBuilder.toString(), classNotFoundException);
    } 
  }
  
  public static final void main(String[] paramArrayOfString) {
    preForkInit();
    if (paramArrayOfString.length == 2 && paramArrayOfString[1].equals("application"))
      redirectLogStreams(); 
    commonInit();
    nativeFinishInit();
  }
  
  protected static Runnable applicationInit(int paramInt, long[] paramArrayOflong, String[] paramArrayOfString, ClassLoader paramClassLoader) {
    nativeSetExitWithoutCleanup(true);
    VMRuntime.getRuntime().setTargetSdkVersion(paramInt);
    VMRuntime.getRuntime().setDisabledCompatChanges(paramArrayOflong);
    Arguments arguments = new Arguments(paramArrayOfString);
    Trace.traceEnd(64L);
    return findStaticMain(arguments.startClass, arguments.startArgs, paramClassLoader);
  }
  
  public static void redirectLogStreams() {
    System.out.close();
    System.setOut(new AndroidPrintStream(4, "System.out"));
    System.err.close();
    System.setErr(new AndroidPrintStream(5, "System.err"));
  }
  
  public static void wtf(String paramString, Throwable paramThrowable, boolean paramBoolean) {
    boolean bool = false;
    try {
      IActivityManager iActivityManager = ActivityManager.getService();
      if (iActivityManager != null) {
        IBinder iBinder = mApplicationObject;
        ApplicationErrorReport.ParcelableCrashInfo parcelableCrashInfo = new ApplicationErrorReport.ParcelableCrashInfo();
        this(paramThrowable);
        int i = Process.myPid();
        paramBoolean = iActivityManager.handleApplicationWtf(iBinder, paramString, paramBoolean, parcelableCrashInfo, i);
      } else {
        ApplicationWtfHandler applicationWtfHandler = sDefaultApplicationWtfHandler;
        if (applicationWtfHandler != null) {
          IBinder iBinder = mApplicationObject;
          ApplicationErrorReport.ParcelableCrashInfo parcelableCrashInfo = new ApplicationErrorReport.ParcelableCrashInfo();
          this(paramThrowable);
          int i = Process.myPid();
          paramBoolean = applicationWtfHandler.handleApplicationWtf(iBinder, paramString, paramBoolean, parcelableCrashInfo, i);
        } else {
          Slog.e("AndroidRuntime", "Original WTF:", paramThrowable);
          paramBoolean = bool;
        } 
      } 
    } finally {
      paramString = null;
    } 
  }
  
  public static void setDefaultApplicationWtfHandler(ApplicationWtfHandler paramApplicationWtfHandler) {
    sDefaultApplicationWtfHandler = paramApplicationWtfHandler;
  }
  
  public static final void setApplicationObject(IBinder paramIBinder) {
    mApplicationObject = paramIBinder;
  }
  
  public static final IBinder getApplicationObject() {
    return mApplicationObject;
  }
  
  private static void enableDdms() {
    DdmRegister.registerHandlers();
  }
  
  private static final native void nativeFinishInit();
  
  private static final native void nativeSetExitWithoutCleanup(boolean paramBoolean);
  
  class Arguments {
    String[] startArgs;
    
    String startClass;
    
    Arguments(RuntimeInit this$0) throws IllegalArgumentException {
      parseArgs((String[])this$0);
    }
    
    private void parseArgs(String[] param1ArrayOfString) throws IllegalArgumentException {
      int j, i = 0;
      while (true) {
        j = i;
        if (i < param1ArrayOfString.length) {
          String str = param1ArrayOfString[i];
          if (str.equals("--")) {
            j = i + 1;
            break;
          } 
          if (!str.startsWith("--")) {
            j = i;
            break;
          } 
          i++;
          continue;
        } 
        break;
      } 
      if (j != param1ArrayOfString.length) {
        i = j + 1;
        this.startClass = param1ArrayOfString[j];
        String[] arrayOfString = new String[param1ArrayOfString.length - i];
        System.arraycopy(param1ArrayOfString, i, arrayOfString, 0, arrayOfString.length);
        return;
      } 
      throw new IllegalArgumentException("Missing classname argument to RuntimeInit!");
    }
  }
  
  static class MethodAndArgsCaller implements Runnable {
    private final String[] mArgs;
    
    private final Method mMethod;
    
    public MethodAndArgsCaller(Method param1Method, String[] param1ArrayOfString) {
      this.mMethod = param1Method;
      this.mArgs = param1ArrayOfString;
    }
    
    public void run() {
      try {
        this.mMethod.invoke(null, new Object[] { this.mArgs });
        return;
      } catch (IllegalAccessException illegalAccessException) {
        throw new RuntimeException(illegalAccessException);
      } catch (InvocationTargetException invocationTargetException) {
        Throwable throwable = invocationTargetException.getCause();
        if (!(throwable instanceof RuntimeException)) {
          if (throwable instanceof Error)
            throw (Error)throwable; 
          throw new RuntimeException(invocationTargetException);
        } 
        throw (RuntimeException)throwable;
      } 
    }
  }
  
  class ApplicationWtfHandler {
    public abstract boolean handleApplicationWtf(IBinder param1IBinder, String param1String, boolean param1Boolean, ApplicationErrorReport.ParcelableCrashInfo param1ParcelableCrashInfo, int param1Int);
  }
}
