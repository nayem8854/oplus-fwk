package com.android.internal.os;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ByteTransferPipe extends TransferPipe {
  static final String TAG = "ByteTransferPipe";
  
  private ByteArrayOutputStream mOutputStream;
  
  public ByteTransferPipe() throws IOException {}
  
  public ByteTransferPipe(String paramString) throws IOException {
    super(paramString, "ByteTransferPipe");
  }
  
  protected OutputStream getNewOutputStream() {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    return byteArrayOutputStream;
  }
  
  public byte[] get() throws IOException {
    go(null);
    return this.mOutputStream.toByteArray();
  }
}
