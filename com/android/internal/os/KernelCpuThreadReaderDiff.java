package com.android.internal.os;

import android.util.ArrayMap;
import android.util.Slog;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class KernelCpuThreadReaderDiff {
  private static final int OTHER_THREADS_ID = -1;
  
  private static final String OTHER_THREADS_NAME = "__OTHER_THREADS";
  
  private static final String TAG = "KernelCpuThreadReaderDiff";
  
  private int mMinimumTotalCpuUsageMillis;
  
  private Map<ThreadKey, int[]> mPreviousCpuUsage;
  
  private final KernelCpuThreadReader mReader;
  
  public KernelCpuThreadReaderDiff(KernelCpuThreadReader paramKernelCpuThreadReader, int paramInt) {
    this.mReader = (KernelCpuThreadReader)Preconditions.checkNotNull(paramKernelCpuThreadReader);
    this.mMinimumTotalCpuUsageMillis = paramInt;
    this.mPreviousCpuUsage = null;
  }
  
  public ArrayList<KernelCpuThreadReader.ProcessCpuUsage> getProcessCpuUsageDiffed() {
    null = null;
    Map<ThreadKey, int[]> map = null;
    try {
      KernelCpuThreadReader kernelCpuThreadReader = this.mReader;
      map = null;
      ArrayList<KernelCpuThreadReader.ProcessCpuUsage> arrayList = kernelCpuThreadReader.getProcessCpuUsage();
      map = null;
      null = createCpuUsageMap(arrayList);
      map = null;
      Map<ThreadKey, int[]> map1 = this.mPreviousCpuUsage;
      if (map1 == null)
        return null; 
      byte b = 0;
      while (true) {
        map = null;
        if (b < arrayList.size()) {
          map = null;
          KernelCpuThreadReader.ProcessCpuUsage processCpuUsage = arrayList.get(b);
          map = null;
          changeToDiffs(this.mPreviousCpuUsage, processCpuUsage);
          map = null;
          applyThresholding(processCpuUsage);
          b++;
          continue;
        } 
        break;
      } 
      return arrayList;
    } finally {
      this.mPreviousCpuUsage = map;
    } 
  }
  
  public int[] getCpuFrequenciesKhz() {
    return this.mReader.getCpuFrequenciesKhz();
  }
  
  void setMinimumTotalCpuUsageMillis(int paramInt) {
    if (paramInt < 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Negative minimumTotalCpuUsageMillis: ");
      stringBuilder.append(paramInt);
      Slog.w("KernelCpuThreadReaderDiff", stringBuilder.toString());
      return;
    } 
    this.mMinimumTotalCpuUsageMillis = paramInt;
  }
  
  private static Map<ThreadKey, int[]> createCpuUsageMap(List<KernelCpuThreadReader.ProcessCpuUsage> paramList) {
    ArrayMap<Object, Object> arrayMap = new ArrayMap<>();
    for (byte b = 0; b < paramList.size(); b++) {
      KernelCpuThreadReader.ProcessCpuUsage processCpuUsage = paramList.get(b);
      for (byte b1 = 0; b1 < processCpuUsage.threadCpuUsages.size(); b1++) {
        ArrayList<KernelCpuThreadReader.ThreadCpuUsage> arrayList = processCpuUsage.threadCpuUsages;
        KernelCpuThreadReader.ThreadCpuUsage threadCpuUsage = arrayList.get(b1);
        arrayMap.put(new ThreadKey(processCpuUsage.processId, threadCpuUsage.threadId, processCpuUsage.processName, threadCpuUsage.threadName), threadCpuUsage.usageTimesMillis);
      } 
    } 
    return (Map)arrayMap;
  }
  
  private static void changeToDiffs(Map<ThreadKey, int[]> paramMap, KernelCpuThreadReader.ProcessCpuUsage paramProcessCpuUsage) {
    for (byte b = 0; b < paramProcessCpuUsage.threadCpuUsages.size(); b++) {
      ArrayList<KernelCpuThreadReader.ThreadCpuUsage> arrayList = paramProcessCpuUsage.threadCpuUsages;
      KernelCpuThreadReader.ThreadCpuUsage threadCpuUsage = arrayList.get(b);
      ThreadKey threadKey = new ThreadKey(paramProcessCpuUsage.processId, threadCpuUsage.threadId, paramProcessCpuUsage.processName, threadCpuUsage.threadName);
      int[] arrayOfInt2 = paramMap.get(threadKey);
      int[] arrayOfInt1 = arrayOfInt2;
      if (arrayOfInt2 == null)
        arrayOfInt1 = new int[threadCpuUsage.usageTimesMillis.length]; 
      arrayOfInt2 = threadCpuUsage.usageTimesMillis;
      threadCpuUsage.usageTimesMillis = cpuTimeDiff(arrayOfInt2, arrayOfInt1);
    } 
  }
  
  private void applyThresholding(KernelCpuThreadReader.ProcessCpuUsage paramProcessCpuUsage) {
    int[] arrayOfInt;
    ArrayList arrayList = null;
    ArrayList<KernelCpuThreadReader.ThreadCpuUsage> arrayList1 = new ArrayList();
    for (byte b = 0; b < paramProcessCpuUsage.threadCpuUsages.size(); b++) {
      ArrayList<KernelCpuThreadReader.ThreadCpuUsage> arrayList2 = paramProcessCpuUsage.threadCpuUsages;
      KernelCpuThreadReader.ThreadCpuUsage threadCpuUsage = arrayList2.get(b);
      if (this.mMinimumTotalCpuUsageMillis > totalCpuUsage(threadCpuUsage.usageTimesMillis)) {
        int[] arrayOfInt1;
        arrayList2 = arrayList;
        if (arrayList == null)
          arrayOfInt1 = new int[threadCpuUsage.usageTimesMillis.length]; 
        addToCpuUsage(arrayOfInt1, threadCpuUsage.usageTimesMillis);
        arrayOfInt = arrayOfInt1;
      } else {
        arrayList1.add(threadCpuUsage);
      } 
    } 
    if (arrayOfInt != null)
      arrayList1.add(new KernelCpuThreadReader.ThreadCpuUsage(-1, "__OTHER_THREADS", arrayOfInt)); 
    paramProcessCpuUsage.threadCpuUsages = arrayList1;
  }
  
  private static int totalCpuUsage(int[] paramArrayOfint) {
    int i = 0;
    for (byte b = 0; b < paramArrayOfint.length; b++)
      i += paramArrayOfint[b]; 
    return i;
  }
  
  private static void addToCpuUsage(int[] paramArrayOfint1, int[] paramArrayOfint2) {
    for (byte b = 0; b < paramArrayOfint1.length; b++)
      paramArrayOfint1[b] = paramArrayOfint1[b] + paramArrayOfint2[b]; 
  }
  
  private static int[] cpuTimeDiff(int[] paramArrayOfint1, int[] paramArrayOfint2) {
    int[] arrayOfInt = new int[paramArrayOfint1.length];
    for (byte b = 0; b < paramArrayOfint1.length; b++)
      arrayOfInt[b] = paramArrayOfint1[b] - paramArrayOfint2[b]; 
    return arrayOfInt;
  }
  
  private static class ThreadKey {
    private final int mProcessId;
    
    private final int mProcessNameHash;
    
    private final int mThreadId;
    
    private final int mThreadNameHash;
    
    ThreadKey(int param1Int1, int param1Int2, String param1String1, String param1String2) {
      this.mProcessId = param1Int1;
      this.mThreadId = param1Int2;
      this.mProcessNameHash = Objects.hash(new Object[] { param1String1 });
      this.mThreadNameHash = Objects.hash(new Object[] { param1String2 });
    }
    
    public int hashCode() {
      return Objects.hash(new Object[] { Integer.valueOf(this.mProcessId), Integer.valueOf(this.mThreadId), Integer.valueOf(this.mProcessNameHash), Integer.valueOf(this.mThreadNameHash) });
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof ThreadKey;
      boolean bool1 = false;
      if (!bool)
        return false; 
      param1Object = param1Object;
      bool = bool1;
      if (this.mProcessId == ((ThreadKey)param1Object).mProcessId) {
        bool = bool1;
        if (this.mThreadId == ((ThreadKey)param1Object).mThreadId) {
          bool = bool1;
          if (this.mProcessNameHash == ((ThreadKey)param1Object).mProcessNameHash) {
            bool = bool1;
            if (this.mThreadNameHash == ((ThreadKey)param1Object).mThreadNameHash)
              bool = true; 
          } 
        } 
      } 
      return bool;
    }
  }
}
