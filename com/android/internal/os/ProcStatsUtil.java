package com.android.internal.os;

import android.os.StrictMode;
import java.io.FileInputStream;
import java.io.IOException;

public final class ProcStatsUtil {
  private static final boolean DEBUG = false;
  
  private static final int READ_SIZE = 1024;
  
  private static final String TAG = "ProcStatsUtil";
  
  public static String readNullSeparatedFile(String paramString) {
    String str = readSingleLineProcFile(paramString);
    if (str == null)
      return null; 
    int i = str.indexOf("\000\000");
    paramString = str;
    if (i != -1)
      paramString = str.substring(0, i); 
    return paramString.replace("\000", " ");
  }
  
  public static String readSingleLineProcFile(String paramString) {
    return readTerminatedProcFile(paramString, (byte)10);
  }
  
  public static String readTerminatedProcFile(String paramString, byte paramByte) {
    StrictMode.ThreadPolicy threadPolicy = StrictMode.allowThreadDiskReads();
    try {
      FileInputStream fileInputStream = new FileInputStream();
      this(paramString);
      String str = null;
    } catch (IOException iOException) {
      return null;
    } finally {
      StrictMode.setThreadPolicy(threadPolicy);
    } 
  }
}
