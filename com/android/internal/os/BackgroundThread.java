package com.android.internal.os;

import android.os.Handler;
import android.os.HandlerExecutor;
import android.os.HandlerThread;
import android.os.Looper;
import java.util.concurrent.Executor;

public final class BackgroundThread extends HandlerThread {
  private static final long SLOW_DELIVERY_THRESHOLD_MS = 30000L;
  
  private static final long SLOW_DISPATCH_THRESHOLD_MS = 10000L;
  
  private static Handler sHandler;
  
  private static HandlerExecutor sHandlerExecutor;
  
  private static BackgroundThread sInstance;
  
  private BackgroundThread() {
    super("android.bg", 10);
  }
  
  private static void ensureThreadLocked() {
    if (sInstance == null) {
      BackgroundThread backgroundThread = new BackgroundThread();
      backgroundThread.start();
      Looper looper = sInstance.getLooper();
      looper.setTraceTag(524288L);
      looper.setSlowLogThresholdMs(10000L, 30000L);
      Handler handler = new Handler(sInstance.getLooper());
      sHandlerExecutor = new HandlerExecutor(handler);
    } 
  }
  
  public static BackgroundThread get() {
    // Byte code:
    //   0: ldc com/android/internal/os/BackgroundThread
    //   2: monitorenter
    //   3: invokestatic ensureThreadLocked : ()V
    //   6: getstatic com/android/internal/os/BackgroundThread.sInstance : Lcom/android/internal/os/BackgroundThread;
    //   9: astore_0
    //   10: ldc com/android/internal/os/BackgroundThread
    //   12: monitorexit
    //   13: aload_0
    //   14: areturn
    //   15: astore_0
    //   16: ldc com/android/internal/os/BackgroundThread
    //   18: monitorexit
    //   19: aload_0
    //   20: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #55	-> 0
    //   #56	-> 3
    //   #57	-> 6
    //   #58	-> 15
    // Exception table:
    //   from	to	target	type
    //   3	6	15	finally
    //   6	13	15	finally
    //   16	19	15	finally
  }
  
  public static Handler getHandler() {
    // Byte code:
    //   0: ldc com/android/internal/os/BackgroundThread
    //   2: monitorenter
    //   3: invokestatic ensureThreadLocked : ()V
    //   6: getstatic com/android/internal/os/BackgroundThread.sHandler : Landroid/os/Handler;
    //   9: astore_0
    //   10: ldc com/android/internal/os/BackgroundThread
    //   12: monitorexit
    //   13: aload_0
    //   14: areturn
    //   15: astore_0
    //   16: ldc com/android/internal/os/BackgroundThread
    //   18: monitorexit
    //   19: aload_0
    //   20: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #62	-> 0
    //   #63	-> 3
    //   #64	-> 6
    //   #65	-> 15
    // Exception table:
    //   from	to	target	type
    //   3	6	15	finally
    //   6	13	15	finally
    //   16	19	15	finally
  }
  
  public static Executor getExecutor() {
    // Byte code:
    //   0: ldc com/android/internal/os/BackgroundThread
    //   2: monitorenter
    //   3: invokestatic ensureThreadLocked : ()V
    //   6: getstatic com/android/internal/os/BackgroundThread.sHandlerExecutor : Landroid/os/HandlerExecutor;
    //   9: astore_0
    //   10: ldc com/android/internal/os/BackgroundThread
    //   12: monitorexit
    //   13: aload_0
    //   14: areturn
    //   15: astore_0
    //   16: ldc com/android/internal/os/BackgroundThread
    //   18: monitorexit
    //   19: aload_0
    //   20: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #69	-> 0
    //   #70	-> 3
    //   #71	-> 6
    //   #72	-> 15
    // Exception table:
    //   from	to	target	type
    //   3	6	15	finally
    //   6	13	15	finally
    //   16	19	15	finally
  }
}
