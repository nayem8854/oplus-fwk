package com.android.internal.os;

import android.os.OplusThermalManager;
import android.os.Process;
import android.os.StrictMode;
import android.os.SystemClock;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.util.ArrayMap;
import android.util.Slog;
import android.util.proto.ProtoOutputStream;
import com.android.internal.util.FastPrintWriter;
import java.io.File;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ProcessCpuTracker {
  private static final int[] PROCESS_STATS_FORMAT = new int[] { 
      32, 544, 32, 32, 32, 32, 32, 32, 32, 8224, 
      32, 8224, 32, 8224, 8224 };
  
  private final long[] mProcessStatsData = new long[4];
  
  private final long[] mSinglePidStatsData = new long[4];
  
  static {
    PROCESS_FULL_STATS_FORMAT = new int[] { 
        32, 4640, 32, 32, 32, 32, 32, 32, 32, 8224, 
        32, 8224, 32, 8224, 8224, 32, 32, 32, 32, 32, 
        32, 32, 8224 };
    SYSTEM_CPU_FORMAT = new int[] { 288, 8224, 8224, 8224, 8224, 8224, 8224, 8224 };
    LOAD_AVERAGE_FORMAT = new int[] { 16416, 16416, 16416 };
    sLoadComparator = new Comparator<Stats>() {
        public final int compare(ProcessCpuTracker.Stats param1Stats1, ProcessCpuTracker.Stats param1Stats2) {
          int i = param1Stats1.rel_utime + param1Stats1.rel_stime;
          int j = param1Stats2.rel_utime + param1Stats2.rel_stime;
          byte b = -1;
          if (i != j) {
            if (i <= j)
              b = 1; 
            return b;
          } 
          if (param1Stats1.added != param1Stats2.added) {
            if (!param1Stats1.added)
              b = 1; 
            return b;
          } 
          if (param1Stats1.removed != param1Stats2.removed) {
            if (!param1Stats1.added)
              b = 1; 
            return b;
          } 
          return 0;
        }
      };
  }
  
  private final String[] mProcessFullStatsStringData = new String[6];
  
  private final long[] mProcessFullStatsData = new long[6];
  
  private final long[] mSystemCpuData = new long[7];
  
  private final float[] mLoadAverageData = new float[3];
  
  private float mLoad1 = 0.0F;
  
  private float mLoad5 = 0.0F;
  
  private float mLoad15 = 0.0F;
  
  private final ArrayList<Stats> mProcStats = new ArrayList<>();
  
  private final ArrayList<Stats> mWorkingProcs = new ArrayList<>();
  
  private boolean mFirst = true;
  
  private int maxCpuThousandths = -1;
  
  private String maxCpuProName = "null";
  
  private static final boolean DEBUG = false;
  
  private static final int[] LOAD_AVERAGE_FORMAT;
  
  private static final int[] PROCESS_FULL_STATS_FORMAT;
  
  static final int PROCESS_FULL_STAT_MAJOR_FAULTS = 2;
  
  static final int PROCESS_FULL_STAT_MINOR_FAULTS = 1;
  
  static final int PROCESS_FULL_STAT_STIME = 4;
  
  static final int PROCESS_FULL_STAT_UTIME = 3;
  
  static final int PROCESS_FULL_STAT_VSIZE = 5;
  
  static final int PROCESS_STAT_MAJOR_FAULTS = 1;
  
  static final int PROCESS_STAT_MINOR_FAULTS = 0;
  
  static final int PROCESS_STAT_STIME = 3;
  
  static final int PROCESS_STAT_UTIME = 2;
  
  private static final int[] SYSTEM_CPU_FORMAT;
  
  private static final String TAG = "ProcessCpuTracker";
  
  private static final boolean localLOGV = false;
  
  private static final Comparator<Stats> sLoadComparator;
  
  private long mBaseIdleTime;
  
  private long mBaseIoWaitTime;
  
  private long mBaseIrqTime;
  
  private long mBaseSoftIrqTime;
  
  private long mBaseSystemTime;
  
  private long mBaseUserTime;
  
  private int[] mCurPids;
  
  private int[] mCurThreadPids;
  
  private long mCurrentSampleRealTime;
  
  private long mCurrentSampleTime;
  
  private long mCurrentSampleWallTime;
  
  private final boolean mIncludeThreads;
  
  private final long mJiffyMillis;
  
  private long mLastSampleRealTime;
  
  private long mLastSampleTime;
  
  private long mLastSampleWallTime;
  
  private long mLastTopSampleTime;
  
  private int mRelIdleTime;
  
  private int mRelIoWaitTime;
  
  private int mRelIrqTime;
  
  private int mRelSoftIrqTime;
  
  private boolean mRelStatsAreGood;
  
  private int mRelSystemTime;
  
  private int mRelUserTime;
  
  private String mSimpleTopProcessInfo;
  
  private ArrayMap<String, String> mTopThreeProcessesSnapShot;
  
  private boolean mWorkingProcsSorted;
  
  class Stats extends OppoBaseProcessCpuTracker.OppoBaseStats {
    public boolean active;
    
    public boolean added;
    
    public String baseName;
    
    public long base_majfaults;
    
    public long base_minfaults;
    
    public long base_stime;
    
    public long base_uptime;
    
    public long base_utime;
    
    public BatteryStatsImpl.Uid.Proc batteryStats;
    
    final String cmdlineFile;
    
    public int cpuThousandths;
    
    public boolean interesting;
    
    public String name;
    
    public int nameWidth;
    
    public final int pid;
    
    public int rel_majfaults;
    
    public int rel_minfaults;
    
    public int rel_stime;
    
    public long rel_uptime;
    
    public int rel_utime;
    
    public boolean removed;
    
    final String statFile;
    
    final ArrayList<Stats> threadStats;
    
    final String threadsDir;
    
    public final int uid;
    
    public long vsize;
    
    public boolean working;
    
    final ArrayList<Stats> workingThreads;
    
    Stats(ProcessCpuTracker this$0, int param1Int1, boolean param1Boolean) {
      this.pid = this$0;
      if (param1Int1 < 0) {
        File file = new File("/proc", Integer.toString(this.pid));
        this.uid = getUid(file.toString());
        this.statFile = (new File(file, "stat")).toString();
        this.cmdlineFile = (new File(file, "cmdline")).toString();
        this.threadsDir = (new File(file, "task")).toString();
        if (param1Boolean) {
          this.threadStats = new ArrayList<>();
          this.workingThreads = new ArrayList<>();
        } else {
          this.threadStats = null;
          this.workingThreads = null;
        } 
      } else {
        File file = new File("/proc", Integer.toString(param1Int1));
        file = new File(file, "task");
        int i = this.pid;
        file = new File(file, Integer.toString(i));
        this.uid = getUid(file.toString());
        this.statFile = (new File(file, "stat")).toString();
        this.cmdlineFile = null;
        this.threadsDir = null;
        this.threadStats = null;
        this.workingThreads = null;
      } 
    }
    
    private static int getUid(String param1String) {
      try {
        return (Os.stat(param1String)).st_uid;
      } catch (ErrnoException errnoException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to stat(");
        stringBuilder.append(param1String);
        stringBuilder.append("): ");
        stringBuilder.append(errnoException);
        Slog.w("ProcessCpuTracker", stringBuilder.toString());
        return -1;
      } 
    }
  }
  
  public static interface FilterStats {
    boolean needed(ProcessCpuTracker.Stats param1Stats);
  }
  
  public void onLoadChanged(float paramFloat1, float paramFloat2, float paramFloat3) {}
  
  public int onMeasureProcessName(String paramString) {
    return 0;
  }
  
  public int getLoad1() {
    return (int)(this.mLoad1 * 10.0F);
  }
  
  public int getLoad5() {
    return (int)(this.mLoad5 * 10.0F);
  }
  
  public int getLoad15() {
    return (int)(this.mLoad15 * 10.0F);
  }
  
  public ArrayMap<String, String> getSimpleTopProcessesSnapShot() {
    long l1 = OplusThermalManager.mHeatTopProInterval * 1000L;
    boolean bool = OplusThermalManager.mHeatTopProFeatureOn;
    long l2 = l1;
    if (l1 < 60000L)
      l2 = 180000L; 
    if (bool && 
      SystemClock.elapsedRealtime() - this.mLastTopSampleTime >= l2) {
      this.mWorkingProcsSorted = false;
      this.mLastTopSampleTime = SystemClock.elapsedRealtime();
      buildWorkingProcs();
    } 
    return this.mTopThreeProcessesSnapShot;
  }
  
  private void collectSimpleTopThreeProcessesInfo(ArrayList<Stats> paramArrayList) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    if (this.mTopThreeProcessesSnapShot != null && paramArrayList != null && 
      paramArrayList.size() >= OplusThermalManager.mHeatTopProCounts) {
      this.mTopThreeProcessesSnapShot.clear();
      int i = OplusThermalManager.mHeatTopProCounts;
      for (byte b = 0; b < i; b++) {
        Stats stats = paramArrayList.get(b);
        String str1 = calcuteRatio(stats);
        this.mTopThreeProcessesSnapShot.put(stats.name, str1);
      } 
      String str = simpleDateFormat.format(new Date(this.mLastSampleWallTime));
      this.mTopThreeProcessesSnapShot.put("lastSamepleWallTime", str);
    } 
  }
  
  public ProcessCpuTracker(boolean paramBoolean) {
    this.mTopThreeProcessesSnapShot = new ArrayMap<>();
    this.mSimpleTopProcessInfo = "";
    this.mIncludeThreads = paramBoolean;
    long l = Os.sysconf(OsConstants._SC_CLK_TCK);
    this.mJiffyMillis = 1000L / l;
  }
  
  private String getRatioString(long paramLong1, long paramLong2) {
    paramLong1 = 1000L * paramLong1 / paramLong2;
    paramLong2 = paramLong1 / 10L;
    String str1 = Long.toString(paramLong2);
    String str2 = str1;
    if (paramLong2 < 10L) {
      paramLong1 -= 10L * paramLong2;
      str2 = str1;
      if (paramLong1 != 0L) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str1);
        stringBuilder.append(".");
        stringBuilder.append(Long.toString(paramLong1));
        str2 = stringBuilder.toString();
      } 
    } 
    return str2;
  }
  
  private String calcuteRatio(Stats paramStats) {
    int i = (int)paramStats.rel_uptime;
    int j = i;
    if (i == 0)
      j = 1; 
    int k = paramStats.rel_utime;
    i = paramStats.rel_stime;
    return getRatioString((k + i), j);
  }
  
  public void init() {
    this.mFirst = true;
    update();
  }
  
  public void update() {
    long l1 = SystemClock.uptimeMillis();
    long l2 = SystemClock.elapsedRealtime();
    long l3 = System.currentTimeMillis();
    null = this.mSystemCpuData;
    if (Process.readProcFile("/proc/stat", SYSTEM_CPU_FORMAT, null, null, null)) {
      long l4 = null[0], l5 = null[1], l6 = this.mJiffyMillis;
      l5 = (l4 + l5) * l6;
      long l7 = null[2] * l6;
      long l8 = null[3] * l6;
      l4 = null[4] * l6;
      long l9 = null[5] * l6;
      l6 *= null[6];
      this.mRelUserTime = (int)(l5 - this.mBaseUserTime);
      this.mRelSystemTime = (int)(l7 - this.mBaseSystemTime);
      this.mRelIoWaitTime = (int)(l4 - this.mBaseIoWaitTime);
      this.mRelIrqTime = (int)(l9 - this.mBaseIrqTime);
      this.mRelSoftIrqTime = (int)(l6 - this.mBaseSoftIrqTime);
      this.mRelIdleTime = (int)(l8 - this.mBaseIdleTime);
      this.mRelStatsAreGood = true;
      this.mBaseUserTime = l5;
      this.mBaseSystemTime = l7;
      this.mBaseIoWaitTime = l4;
      this.mBaseIrqTime = l9;
      this.mBaseSoftIrqTime = l6;
      this.mBaseIdleTime = l8;
    } 
    this.mLastSampleTime = this.mCurrentSampleTime;
    this.mCurrentSampleTime = l1;
    this.mLastSampleRealTime = this.mCurrentSampleRealTime;
    this.mCurrentSampleRealTime = l2;
    this.mLastSampleWallTime = this.mCurrentSampleWallTime;
    this.mCurrentSampleWallTime = l3;
    StrictMode.ThreadPolicy threadPolicy = StrictMode.allowThreadDiskReads();
    try {
      this.mCurPids = collectStats("/proc", -1, this.mFirst, this.mCurPids, this.mProcStats);
      StrictMode.setThreadPolicy(threadPolicy);
      float[] arrayOfFloat = this.mLoadAverageData;
      if (Process.readProcFile("/proc/loadavg", LOAD_AVERAGE_FORMAT, null, null, arrayOfFloat)) {
        float f1 = arrayOfFloat[0];
        float f2 = arrayOfFloat[1];
        float f3 = arrayOfFloat[2];
        if (f1 != this.mLoad1 || f2 != this.mLoad5 || f3 != this.mLoad15) {
          this.mLoad1 = f1;
          this.mLoad5 = f2;
          this.mLoad15 = f3;
          onLoadChanged(f1, f2, f3);
        } 
      } 
      this.mWorkingProcsSorted = false;
      return;
    } finally {
      StrictMode.setThreadPolicy(threadPolicy);
    } 
  }
  
  private int[] collectStats(String paramString, int paramInt, boolean paramBoolean, int[] paramArrayOfint, ArrayList<Stats> paramArrayList) {
    int i = paramInt;
    paramArrayOfint = Process.getPids(paramString, paramArrayOfint);
    if (paramArrayOfint == null) {
      i = 0;
    } else {
      i = paramArrayOfint.length;
    } 
    try {
      int j = paramArrayList.size();
      int k = 0;
      int m = -1;
      paramString = "null";
      int n = 0, i1 = i;
      i = n;
      while (true) {
        int i2 = paramInt;
        if (i < i1) {
          Stats stats;
          int i3 = paramArrayOfint[i];
          if (i3 < 0)
            break; 
          if (k < j) {
            stats = paramArrayList.get(k);
          } else {
            stats = null;
          } 
          if (stats != null && stats.pid == i3) {
            stats.added = false;
            stats.working = false;
            n = k + 1;
            if (stats.interesting) {
              long l = SystemClock.uptimeMillis();
              long[] arrayOfLong = this.mProcessStatsData;
              if (Process.readProcFile(stats.statFile.toString(), PROCESS_STATS_FORMAT, null, arrayOfLong, null)) {
                long l1 = arrayOfLong[0];
                long l2 = arrayOfLong[1];
                long l3 = arrayOfLong[2], l4 = this.mJiffyMillis;
                l3 *= l4;
                l4 *= arrayOfLong[3];
                if (l3 == stats.base_utime && l4 == stats.base_stime) {
                  stats.rel_utime = 0;
                  stats.rel_stime = 0;
                  stats.rel_minfaults = 0;
                  stats.rel_majfaults = 0;
                  if (stats.active)
                    stats.active = false; 
                } else {
                  if (!stats.active)
                    stats.active = true; 
                  if (i2 < 0) {
                    getName(stats, stats.cmdlineFile);
                    if (stats.threadStats != null)
                      this.mCurThreadPids = collectStats(stats.threadsDir, i3, false, this.mCurThreadPids, stats.threadStats); 
                  } 
                  stats.rel_uptime = l - stats.base_uptime;
                  stats.base_uptime = l;
                  stats.rel_utime = (int)(l3 - stats.base_utime);
                  stats.rel_stime = (int)(l4 - stats.base_stime);
                  stats.base_utime = l3;
                  stats.base_stime = l4;
                  stats.rel_minfaults = (int)(l1 - stats.base_minfaults);
                  stats.rel_majfaults = (int)(l2 - stats.base_majfaults);
                  stats.base_minfaults = l1;
                  stats.base_majfaults = l2;
                  stats.working = true;
                  k = this.mRelUserTime + this.mRelSystemTime + this.mRelIrqTime + this.mRelIdleTime;
                  if (k <= 0) {
                    stats.cpuThousandths = 0;
                  } else {
                    stats.cpuThousandths = (stats.rel_utime + stats.rel_stime) * 1000 / k;
                  } 
                  if (m < stats.cpuThousandths) {
                    m = stats.cpuThousandths;
                    paramString = stats.name;
                  } 
                } 
              } 
            } 
          } else {
            if (stats == null || stats.pid > i3) {
              boolean bool = this.mIncludeThreads;
              stats = new Stats(i3, paramInt, bool);
              paramArrayList.add(k, stats);
              String[] arrayOfString = this.mProcessFullStatsStringData;
              long[] arrayOfLong = this.mProcessFullStatsData;
              stats.base_uptime = SystemClock.uptimeMillis();
              String str = stats.statFile.toString();
              if (Process.readProcFile(str, PROCESS_FULL_STATS_FORMAT, arrayOfString, arrayOfLong, null)) {
                stats.vsize = arrayOfLong[5];
                stats.interesting = true;
                stats.baseName = arrayOfString[0];
                stats.base_minfaults = arrayOfLong[1];
                stats.base_majfaults = arrayOfLong[2];
                stats.base_utime = arrayOfLong[3] * this.mJiffyMillis;
                stats.base_stime = arrayOfLong[4] * this.mJiffyMillis;
                n = this.mRelUserTime + this.mRelSystemTime + this.mRelIrqTime + this.mRelIdleTime;
                if (n <= 0) {
                  stats.cpuThousandths = 0;
                } else {
                  stats.cpuThousandths = (int)((stats.rel_utime + stats.base_stime) * 1000L / n);
                } 
                n = m;
                if (m < stats.cpuThousandths) {
                  n = stats.cpuThousandths;
                  paramString = stats.name;
                } 
                m = n;
              } else {
                stats.baseName = "<unknown>";
                stats.base_stime = 0L;
                stats.base_utime = 0L;
                stats.base_majfaults = 0L;
                stats.base_minfaults = 0L;
              } 
              if (paramInt < 0) {
                getName(stats, stats.cmdlineFile);
                if (stats.threadStats != null)
                  this.mCurThreadPids = collectStats(stats.threadsDir, i3, true, this.mCurThreadPids, stats.threadStats); 
              } else if (stats.interesting) {
                stats.name = stats.baseName;
                stats.nameWidth = onMeasureProcessName(stats.name);
              } 
              stats.rel_utime = 0;
              stats.rel_stime = 0;
              stats.rel_minfaults = 0;
              stats.rel_majfaults = 0;
              stats.added = true;
              if (!paramBoolean && stats.interesting)
                stats.working = true; 
              n = k + 1;
              j++;
            } 
            stats.rel_utime = 0;
            stats.rel_stime = 0;
            stats.rel_minfaults = 0;
            stats.rel_majfaults = 0;
            stats.removed = true;
            stats.working = true;
            paramArrayList.remove(k);
            j--;
            i--;
            n = k;
          } 
        } else {
          break;
        } 
        i++;
        k = paramInt;
        k = n;
      } 
      this.maxCpuThousandths = m;
      this.maxCpuProName = paramString;
      paramInt = j;
      while (k < paramInt) {
        Stats stats = paramArrayList.get(k);
        stats.rel_utime = 0;
        stats.rel_stime = 0;
        stats.rel_minfaults = 0;
        stats.rel_majfaults = 0;
        stats.removed = true;
        stats.working = true;
        paramArrayList.remove(k);
        paramInt--;
      } 
      return paramArrayOfint;
    } catch (NullPointerException nullPointerException) {
      Slog.i("ProcessCpuTracker", "collectStats(): allProcs is null!");
      return null;
    } 
  }
  
  public long getCpuTimeForPid(int paramInt) {
    synchronized (this.mSinglePidStatsData) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("/proc/");
      stringBuilder.append(paramInt);
      stringBuilder.append("/stat");
      String str = stringBuilder.toString();
      long[] arrayOfLong = this.mSinglePidStatsData;
      if (Process.readProcFile(str, PROCESS_STATS_FORMAT, null, arrayOfLong, null)) {
        long l1 = arrayOfLong[2], l2 = arrayOfLong[3];
        long l3 = this.mJiffyMillis;
        return l3 * (l1 + l2);
      } 
      return 0L;
    } 
  }
  
  public final int getLastUserTime() {
    return this.mRelUserTime;
  }
  
  public final int getLastSystemTime() {
    return this.mRelSystemTime;
  }
  
  public final int getLastIoWaitTime() {
    return this.mRelIoWaitTime;
  }
  
  public final int getLastIrqTime() {
    return this.mRelIrqTime;
  }
  
  public final int getLastSoftIrqTime() {
    return this.mRelSoftIrqTime;
  }
  
  public final int getLastIdleTime() {
    return this.mRelIdleTime;
  }
  
  public final int getMaxCpuThousandths() {
    return this.maxCpuThousandths;
  }
  
  public final String getMaxCpuProName() {
    return this.maxCpuProName;
  }
  
  public final boolean hasGoodLastStats() {
    return this.mRelStatsAreGood;
  }
  
  public final float getTotalCpuPercent() {
    int i = this.mRelUserTime, j = this.mRelSystemTime, k = this.mRelIrqTime, m = i + j + k + this.mRelIdleTime;
    if (m <= 0)
      return 0.0F; 
    return (i + j + k) * 100.0F / m;
  }
  
  final void buildWorkingProcs() {
    if (!this.mWorkingProcsSorted) {
      this.mWorkingProcs.clear();
      int i = this.mProcStats.size();
      for (byte b = 0; b < i; b++) {
        Stats stats = this.mProcStats.get(b);
        if (stats.working) {
          this.mWorkingProcs.add(stats);
          if (stats.threadStats != null && stats.threadStats.size() > 1) {
            stats.workingThreads.clear();
            int j = stats.threadStats.size();
            for (byte b1 = 0; b1 < j; b1++) {
              Stats stats1 = stats.threadStats.get(b1);
              if (stats1.working)
                stats.workingThreads.add(stats1); 
            } 
            Collections.sort(stats.workingThreads, sLoadComparator);
          } 
        } 
      } 
      Collections.sort(this.mWorkingProcs, sLoadComparator);
      collectSimpleTopThreeProcessesInfo(this.mWorkingProcs);
      this.mWorkingProcsSorted = true;
    } 
  }
  
  public final int countStats() {
    return this.mProcStats.size();
  }
  
  public final Stats getStats(int paramInt) {
    return this.mProcStats.get(paramInt);
  }
  
  public final List<Stats> getStats(FilterStats paramFilterStats) {
    ArrayList<Stats> arrayList = new ArrayList(this.mProcStats.size());
    int i = this.mProcStats.size();
    for (byte b = 0; b < i; b++) {
      Stats stats = this.mProcStats.get(b);
      if (paramFilterStats.needed(stats))
        arrayList.add(stats); 
    } 
    return arrayList;
  }
  
  public final int countWorkingStats() {
    buildWorkingProcs();
    return this.mWorkingProcs.size();
  }
  
  public final Stats getWorkingStats(int paramInt) {
    return this.mWorkingProcs.get(paramInt);
  }
  
  public final void dumpProto(FileDescriptor paramFileDescriptor) {
    long l1 = SystemClock.uptimeMillis();
    ProtoOutputStream protoOutputStream = new ProtoOutputStream(paramFileDescriptor);
    long l2 = protoOutputStream.start(1146756268033L);
    protoOutputStream.write(1108101562369L, this.mLoad1);
    protoOutputStream.write(1108101562370L, this.mLoad5);
    protoOutputStream.write(1108101562371L, this.mLoad15);
    protoOutputStream.end(l2);
    buildWorkingProcs();
    protoOutputStream.write(1112396529666L, l1);
    protoOutputStream.write(1112396529667L, this.mLastSampleTime);
    protoOutputStream.write(1112396529668L, this.mCurrentSampleTime);
    protoOutputStream.write(1112396529669L, this.mLastSampleRealTime);
    protoOutputStream.write(1112396529670L, this.mCurrentSampleRealTime);
    protoOutputStream.write(1112396529671L, this.mLastSampleWallTime);
    protoOutputStream.write(1112396529672L, this.mCurrentSampleWallTime);
    protoOutputStream.write(1120986464265L, this.mRelUserTime);
    protoOutputStream.write(1120986464266L, this.mRelSystemTime);
    protoOutputStream.write(1120986464267L, this.mRelIoWaitTime);
    protoOutputStream.write(1120986464268L, this.mRelIrqTime);
    protoOutputStream.write(1120986464269L, this.mRelSoftIrqTime);
    protoOutputStream.write(1120986464270L, this.mRelIdleTime);
    int i = this.mRelUserTime, j = this.mRelSystemTime, k = this.mRelIoWaitTime, m = this.mRelIrqTime, n = this.mRelSoftIrqTime, i1 = this.mRelIdleTime;
    protoOutputStream.write(1120986464271L, i + j + k + m + n + i1);
    for (Stats stats : this.mWorkingProcs) {
      dumpProcessCpuProto(protoOutputStream, stats, null);
      if (!stats.removed && stats.workingThreads != null)
        for (Stats stats1 : stats.workingThreads)
          dumpProcessCpuProto(protoOutputStream, stats1, stats);  
    } 
    protoOutputStream.flush();
  }
  
  private static void dumpProcessCpuProto(ProtoOutputStream paramProtoOutputStream, Stats paramStats1, Stats paramStats2) {
    long l = paramProtoOutputStream.start(2246267895824L);
    paramProtoOutputStream.write(1120986464257L, paramStats1.uid);
    paramProtoOutputStream.write(1120986464258L, paramStats1.pid);
    paramProtoOutputStream.write(1138166333443L, paramStats1.name);
    paramProtoOutputStream.write(1133871366148L, paramStats1.added);
    paramProtoOutputStream.write(1133871366149L, paramStats1.removed);
    paramProtoOutputStream.write(1120986464262L, paramStats1.rel_uptime);
    paramProtoOutputStream.write(1120986464263L, paramStats1.rel_utime);
    paramProtoOutputStream.write(1120986464264L, paramStats1.rel_stime);
    paramProtoOutputStream.write(1120986464265L, paramStats1.rel_minfaults);
    paramProtoOutputStream.write(1120986464266L, paramStats1.rel_majfaults);
    if (paramStats2 != null)
      paramProtoOutputStream.write(1120986464267L, paramStats2.pid); 
    paramProtoOutputStream.end(l);
  }
  
  public final String printCurrentLoad() {
    StringWriter stringWriter = new StringWriter();
    FastPrintWriter fastPrintWriter = new FastPrintWriter(stringWriter, false, 128);
    fastPrintWriter.print("Load: ");
    fastPrintWriter.print(this.mLoad1);
    fastPrintWriter.print(" / ");
    fastPrintWriter.print(this.mLoad5);
    fastPrintWriter.print(" / ");
    fastPrintWriter.println(this.mLoad15);
    fastPrintWriter.flush();
    return stringWriter.toString();
  }
  
  public final String printCurrentState(long paramLong) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    buildWorkingProcs();
    StringWriter stringWriter = new StringWriter();
    FastPrintWriter fastPrintWriter = new FastPrintWriter(stringWriter, false, 1024);
    fastPrintWriter.print("CPU usage from ");
    long l1 = this.mLastSampleTime;
    if (paramLong > l1) {
      fastPrintWriter.print(paramLong - l1);
      fastPrintWriter.print("ms to ");
      fastPrintWriter.print(paramLong - this.mCurrentSampleTime);
      fastPrintWriter.print("ms ago");
    } else {
      fastPrintWriter.print(l1 - paramLong);
      fastPrintWriter.print("ms to ");
      fastPrintWriter.print(this.mCurrentSampleTime - paramLong);
      fastPrintWriter.print("ms later");
    } 
    fastPrintWriter.print(" (");
    fastPrintWriter.print(simpleDateFormat.format(new Date(this.mLastSampleWallTime)));
    fastPrintWriter.print(" to ");
    fastPrintWriter.print(simpleDateFormat.format(new Date(this.mCurrentSampleWallTime)));
    fastPrintWriter.print(")");
    long l2 = this.mCurrentSampleTime;
    l1 = this.mLastSampleTime;
    long l3 = this.mCurrentSampleRealTime - this.mLastSampleRealTime;
    paramLong = 0L;
    if (l3 > 0L)
      paramLong = (l2 - l1) * 100L / l3; 
    if (paramLong != 100L) {
      fastPrintWriter.print(" with ");
      fastPrintWriter.print(paramLong);
      fastPrintWriter.print("% awake");
    } 
    fastPrintWriter.println(":");
    int i = this.mRelUserTime, j = this.mRelSystemTime, k = this.mRelIoWaitTime, m = this.mRelIrqTime, n = this.mRelSoftIrqTime, i1 = this.mRelIdleTime;
    int i2 = this.mWorkingProcs.size();
    for (byte b = 0; b < i2; b++) {
      String str;
      Stats stats = this.mWorkingProcs.get(b);
      if (stats.added) {
        str = " +";
      } else if (stats.removed) {
        str = " -";
      } else {
        str = "  ";
      } 
      printProcessCPU((PrintWriter)fastPrintWriter, str, stats.pid, stats.name, (int)stats.rel_uptime, stats.rel_utime, stats.rel_stime, 0, 0, 0, stats.rel_minfaults, stats.rel_majfaults);
      if (!stats.removed && stats.workingThreads != null) {
        int i3 = stats.workingThreads.size();
        for (byte b1 = 0; b1 < i3; b1++) {
          Stats stats1 = stats.workingThreads.get(b1);
          if (stats1.added) {
            str = "   +";
          } else if (stats1.removed) {
            str = "   -";
          } else {
            str = "    ";
          } 
          int i4 = stats1.pid;
          String str1 = stats1.name;
          int i5 = (int)stats.rel_uptime, i6 = stats1.rel_utime, i7 = stats1.rel_stime;
          printProcessCPU((PrintWriter)fastPrintWriter, str, i4, str1, i5, i6, i7, 0, 0, 0, 0, 0);
        } 
      } 
    } 
    printProcessCPU((PrintWriter)fastPrintWriter, "", -1, "TOTAL", i + j + k + m + n + i1, this.mRelUserTime, this.mRelSystemTime, this.mRelIoWaitTime, this.mRelIrqTime, this.mRelSoftIrqTime, 0, 0);
    fastPrintWriter.flush();
    return stringWriter.toString();
  }
  
  private void printRatio(PrintWriter paramPrintWriter, long paramLong1, long paramLong2) {
    paramLong1 = 1000L * paramLong1 / paramLong2;
    paramLong2 = paramLong1 / 10L;
    paramPrintWriter.print(paramLong2);
    if (paramLong2 < 10L) {
      paramLong1 -= 10L * paramLong2;
      if (paramLong1 != 0L) {
        paramPrintWriter.print('.');
        paramPrintWriter.print(paramLong1);
      } 
    } 
  }
  
  private void printProcessCPU(PrintWriter paramPrintWriter, String paramString1, int paramInt1, String paramString2, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9) {
    paramPrintWriter.print(paramString1);
    if (paramInt2 == 0)
      paramInt2 = 1; 
    printRatio(paramPrintWriter, (paramInt3 + paramInt4 + paramInt5 + paramInt6 + paramInt7), paramInt2);
    paramPrintWriter.print("% ");
    if (paramInt1 >= 0) {
      paramPrintWriter.print(paramInt1);
      paramPrintWriter.print("/");
    } 
    paramPrintWriter.print(paramString2);
    paramPrintWriter.print(": ");
    printRatio(paramPrintWriter, paramInt3, paramInt2);
    paramPrintWriter.print("% user + ");
    printRatio(paramPrintWriter, paramInt4, paramInt2);
    paramPrintWriter.print("% kernel");
    if (paramInt5 > 0) {
      paramPrintWriter.print(" + ");
      printRatio(paramPrintWriter, paramInt5, paramInt2);
      paramPrintWriter.print("% iowait");
    } 
    if (paramInt6 > 0) {
      paramPrintWriter.print(" + ");
      printRatio(paramPrintWriter, paramInt6, paramInt2);
      paramPrintWriter.print("% irq");
    } 
    if (paramInt7 > 0) {
      paramPrintWriter.print(" + ");
      printRatio(paramPrintWriter, paramInt7, paramInt2);
      paramPrintWriter.print("% softirq");
    } 
    if (paramInt8 > 0 || paramInt9 > 0) {
      paramPrintWriter.print(" / faults:");
      if (paramInt8 > 0) {
        paramPrintWriter.print(" ");
        paramPrintWriter.print(paramInt8);
        paramPrintWriter.print(" minor");
      } 
      if (paramInt9 > 0) {
        paramPrintWriter.print(" ");
        paramPrintWriter.print(paramInt9);
        paramPrintWriter.print(" major");
      } 
    } 
    paramPrintWriter.println();
  }
  
  private void getName(Stats paramStats, String paramString) {
    // Byte code:
    //   0: aload_1
    //   1: getfield name : Ljava/lang/String;
    //   4: astore_3
    //   5: aload_1
    //   6: getfield name : Ljava/lang/String;
    //   9: ifnull -> 75
    //   12: aload_1
    //   13: getfield name : Ljava/lang/String;
    //   16: ldc_w 'app_process'
    //   19: invokevirtual equals : (Ljava/lang/Object;)Z
    //   22: ifne -> 75
    //   25: aload_1
    //   26: getfield name : Ljava/lang/String;
    //   29: astore #4
    //   31: aload #4
    //   33: ldc_w '<pre-initialized>'
    //   36: invokevirtual equals : (Ljava/lang/Object;)Z
    //   39: ifne -> 75
    //   42: aload_1
    //   43: getfield name : Ljava/lang/String;
    //   46: astore #4
    //   48: aload #4
    //   50: ldc_w 'usap32'
    //   53: invokevirtual equals : (Ljava/lang/Object;)Z
    //   56: ifne -> 75
    //   59: aload_3
    //   60: astore #4
    //   62: aload_1
    //   63: getfield name : Ljava/lang/String;
    //   66: ldc_w 'usap64'
    //   69: invokevirtual equals : (Ljava/lang/Object;)Z
    //   72: ifeq -> 156
    //   75: aload_2
    //   76: iconst_0
    //   77: invokestatic readTerminatedProcFile : (Ljava/lang/String;B)Ljava/lang/String;
    //   80: astore #4
    //   82: aload_3
    //   83: astore_2
    //   84: aload #4
    //   86: ifnull -> 143
    //   89: aload_3
    //   90: astore_2
    //   91: aload #4
    //   93: invokevirtual length : ()I
    //   96: iconst_1
    //   97: if_icmple -> 143
    //   100: aload #4
    //   102: ldc_w '/'
    //   105: invokevirtual lastIndexOf : (Ljava/lang/String;)I
    //   108: istore #5
    //   110: aload #4
    //   112: astore_2
    //   113: iload #5
    //   115: ifle -> 143
    //   118: aload #4
    //   120: astore_2
    //   121: iload #5
    //   123: aload #4
    //   125: invokevirtual length : ()I
    //   128: iconst_1
    //   129: isub
    //   130: if_icmpge -> 143
    //   133: aload #4
    //   135: iload #5
    //   137: iconst_1
    //   138: iadd
    //   139: invokevirtual substring : (I)Ljava/lang/String;
    //   142: astore_2
    //   143: aload_2
    //   144: astore #4
    //   146: aload_2
    //   147: ifnonnull -> 156
    //   150: aload_1
    //   151: getfield baseName : Ljava/lang/String;
    //   154: astore #4
    //   156: aload_1
    //   157: getfield name : Ljava/lang/String;
    //   160: ifnull -> 175
    //   163: aload #4
    //   165: aload_1
    //   166: getfield name : Ljava/lang/String;
    //   169: invokevirtual equals : (Ljava/lang/Object;)Z
    //   172: ifne -> 193
    //   175: aload_1
    //   176: aload #4
    //   178: putfield name : Ljava/lang/String;
    //   181: aload_1
    //   182: aload_0
    //   183: aload_1
    //   184: getfield name : Ljava/lang/String;
    //   187: invokevirtual onMeasureProcessName : (Ljava/lang/String;)I
    //   190: putfield nameWidth : I
    //   193: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1113	-> 0
    //   #1114	-> 5
    //   #1115	-> 31
    //   #1118	-> 48
    //   #1120	-> 75
    //   #1121	-> 82
    //   #1122	-> 100
    //   #1123	-> 100
    //   #1124	-> 110
    //   #1125	-> 133
    //   #1128	-> 143
    //   #1129	-> 150
    //   #1132	-> 156
    //   #1133	-> 175
    //   #1134	-> 181
    //   #1136	-> 193
  }
}
