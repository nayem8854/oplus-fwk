package com.android.internal.os;

import com.oplus.reflect.RefClass;
import com.oplus.reflect.RefInt;

public class OplusMirrorZygote {
  public static RefInt MOUNT_EXTERNAL_OPLUS_ANDROID_WRITABLE;
  
  public static Class<?> TYPE = RefClass.load(OplusMirrorZygote.class, Zygote.class);
}
