package com.android.internal.os;

import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.system.suspend.ISuspendControlService;
import android.system.suspend.WakeLockInfo;
import android.util.Slog;
import java.util.Iterator;

public class KernelWakelockReader {
  private static int sKernelWakelockUpdateVersion = 0;
  
  static {
    PROC_WAKELOCKS_FORMAT = new int[] { 5129, 8201, 9, 9, 9, 8201 };
    WAKEUP_SOURCES_FORMAT = new int[] { 4105, 8457, 265, 265, 265, 265, 8457 };
  }
  
  private final String[] mProcWakelocksName = new String[3];
  
  private final long[] mProcWakelocksData = new long[3];
  
  private ISuspendControlService mSuspendControlService = null;
  
  private byte[] mKernelWakelockBuffer = new byte[32768];
  
  private static final int[] PROC_WAKELOCKS_FORMAT;
  
  private static final String TAG = "KernelWakelockReader";
  
  private static final int[] WAKEUP_SOURCES_FORMAT;
  
  private static final String sSysClassWakeupDir = "/sys/class/wakeup";
  
  private static final String sWakelockFile = "/proc/wakelocks";
  
  private static final String sWakeupSourceFile = "/d/wakeup_sources";
  
  public final KernelWakelockStats readKernelWakelockStats(KernelWakelockStats paramKernelWakelockStats) {
    // Byte code:
    //   0: new java/io/File
    //   3: dup
    //   4: ldc '/sys/class/wakeup'
    //   6: invokespecial <init> : (Ljava/lang/String;)V
    //   9: invokevirtual exists : ()Z
    //   12: istore_2
    //   13: iload_2
    //   14: ifeq -> 47
    //   17: aload_0
    //   18: aload_1
    //   19: invokevirtual updateVersion : (Lcom/android/internal/os/KernelWakelockStats;)Lcom/android/internal/os/KernelWakelockStats;
    //   22: pop
    //   23: aload_0
    //   24: aload_1
    //   25: invokespecial getWakelockStatsFromSystemSuspend : (Lcom/android/internal/os/KernelWakelockStats;)Lcom/android/internal/os/KernelWakelockStats;
    //   28: ifnonnull -> 41
    //   31: ldc 'KernelWakelockReader'
    //   33: ldc 'Failed to get wakelock stats from SystemSuspend'
    //   35: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   38: pop
    //   39: aconst_null
    //   40: areturn
    //   41: aload_0
    //   42: aload_1
    //   43: invokevirtual removeOldStats : (Lcom/android/internal/os/KernelWakelockStats;)Lcom/android/internal/os/KernelWakelockStats;
    //   46: areturn
    //   47: aload_0
    //   48: getfield mKernelWakelockBuffer : [B
    //   51: iconst_0
    //   52: invokestatic fill : ([BB)V
    //   55: iconst_0
    //   56: istore_3
    //   57: invokestatic uptimeMillis : ()J
    //   60: lstore #4
    //   62: invokestatic allowThreadDiskReadsMask : ()I
    //   65: istore #6
    //   67: new java/io/FileInputStream
    //   70: astore #7
    //   72: aload #7
    //   74: ldc '/proc/wakelocks'
    //   76: invokespecial <init> : (Ljava/lang/String;)V
    //   79: iconst_0
    //   80: istore_2
    //   81: goto -> 107
    //   84: astore_1
    //   85: goto -> 373
    //   88: astore_1
    //   89: goto -> 357
    //   92: astore #7
    //   94: new java/io/FileInputStream
    //   97: dup
    //   98: ldc '/d/wakeup_sources'
    //   100: invokespecial <init> : (Ljava/lang/String;)V
    //   103: astore #7
    //   105: iconst_1
    //   106: istore_2
    //   107: aload #7
    //   109: aload_0
    //   110: getfield mKernelWakelockBuffer : [B
    //   113: iload_3
    //   114: aload_0
    //   115: getfield mKernelWakelockBuffer : [B
    //   118: arraylength
    //   119: iload_3
    //   120: isub
    //   121: invokevirtual read : ([BII)I
    //   124: istore #8
    //   126: iload #8
    //   128: ifle -> 139
    //   131: iload_3
    //   132: iload #8
    //   134: iadd
    //   135: istore_3
    //   136: goto -> 107
    //   139: aload #7
    //   141: invokevirtual close : ()V
    //   144: iload #6
    //   146: invokestatic setThreadPolicyMask : (I)V
    //   149: invokestatic uptimeMillis : ()J
    //   152: lload #4
    //   154: lsub
    //   155: lstore #4
    //   157: lload #4
    //   159: ldc2_w 100
    //   162: lcmp
    //   163: ifle -> 210
    //   166: new java/lang/StringBuilder
    //   169: dup
    //   170: invokespecial <init> : ()V
    //   173: astore #7
    //   175: aload #7
    //   177: ldc 'Reading wakelock stats took '
    //   179: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   182: pop
    //   183: aload #7
    //   185: lload #4
    //   187: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   190: pop
    //   191: aload #7
    //   193: ldc 'ms'
    //   195: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   198: pop
    //   199: ldc 'KernelWakelockReader'
    //   201: aload #7
    //   203: invokevirtual toString : ()Ljava/lang/String;
    //   206: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   209: pop
    //   210: iload_3
    //   211: istore #8
    //   213: iload_3
    //   214: ifle -> 300
    //   217: iload_3
    //   218: aload_0
    //   219: getfield mKernelWakelockBuffer : [B
    //   222: arraylength
    //   223: if_icmplt -> 265
    //   226: new java/lang/StringBuilder
    //   229: dup
    //   230: invokespecial <init> : ()V
    //   233: astore #7
    //   235: aload #7
    //   237: ldc 'Kernel wake locks exceeded mKernelWakelockBuffer size '
    //   239: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   242: pop
    //   243: aload #7
    //   245: aload_0
    //   246: getfield mKernelWakelockBuffer : [B
    //   249: arraylength
    //   250: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   253: pop
    //   254: ldc 'KernelWakelockReader'
    //   256: aload #7
    //   258: invokevirtual toString : ()Ljava/lang/String;
    //   261: invokestatic wtf : (Ljava/lang/String;Ljava/lang/String;)I
    //   264: pop
    //   265: iconst_0
    //   266: istore #6
    //   268: iload_3
    //   269: istore #8
    //   271: iload #6
    //   273: iload_3
    //   274: if_icmpge -> 300
    //   277: aload_0
    //   278: getfield mKernelWakelockBuffer : [B
    //   281: iload #6
    //   283: baload
    //   284: ifne -> 294
    //   287: iload #6
    //   289: istore #8
    //   291: goto -> 300
    //   294: iinc #6, 1
    //   297: goto -> 268
    //   300: aload_0
    //   301: aload_1
    //   302: invokevirtual updateVersion : (Lcom/android/internal/os/KernelWakelockStats;)Lcom/android/internal/os/KernelWakelockStats;
    //   305: pop
    //   306: aload_0
    //   307: aload_1
    //   308: invokespecial getWakelockStatsFromSystemSuspend : (Lcom/android/internal/os/KernelWakelockStats;)Lcom/android/internal/os/KernelWakelockStats;
    //   311: ifnonnull -> 322
    //   314: ldc 'KernelWakelockReader'
    //   316: ldc 'Failed to get Native wakelock stats from SystemSuspend'
    //   318: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   321: pop
    //   322: aload_0
    //   323: aload_0
    //   324: getfield mKernelWakelockBuffer : [B
    //   327: iload #8
    //   329: iload_2
    //   330: aload_1
    //   331: invokevirtual parseProcWakelocks : ([BIZLcom/android/internal/os/KernelWakelockStats;)Lcom/android/internal/os/KernelWakelockStats;
    //   334: pop
    //   335: aload_0
    //   336: aload_1
    //   337: invokevirtual removeOldStats : (Lcom/android/internal/os/KernelWakelockStats;)Lcom/android/internal/os/KernelWakelockStats;
    //   340: areturn
    //   341: astore_1
    //   342: ldc 'KernelWakelockReader'
    //   344: ldc 'neither /proc/wakelocks nor /d/wakeup_sources exists'
    //   346: invokestatic wtf : (Ljava/lang/String;Ljava/lang/String;)I
    //   349: pop
    //   350: iload #6
    //   352: invokestatic setThreadPolicyMask : (I)V
    //   355: aconst_null
    //   356: areturn
    //   357: ldc 'KernelWakelockReader'
    //   359: ldc 'failed to read kernel wakelocks'
    //   361: aload_1
    //   362: invokestatic wtf : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   365: pop
    //   366: iload #6
    //   368: invokestatic setThreadPolicyMask : (I)V
    //   371: aconst_null
    //   372: areturn
    //   373: iload #6
    //   375: invokestatic setThreadPolicyMask : (I)V
    //   378: aload_1
    //   379: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #78	-> 0
    //   #80	-> 13
    //   #82	-> 17
    //   #83	-> 23
    //   #84	-> 31
    //   #85	-> 39
    //   #87	-> 41
    //   #89	-> 47
    //   #90	-> 55
    //   #92	-> 57
    //   #94	-> 62
    //   #98	-> 67
    //   #99	-> 79
    //   #109	-> 81
    //   #122	-> 84
    //   #118	-> 88
    //   #100	-> 92
    //   #102	-> 94
    //   #103	-> 105
    //   #108	-> 105
    //   #112	-> 107
    //   #114	-> 131
    //   #117	-> 139
    //   #122	-> 144
    //   #123	-> 149
    //   #125	-> 149
    //   #126	-> 157
    //   #127	-> 166
    //   #130	-> 210
    //   #131	-> 217
    //   #132	-> 226
    //   #136	-> 265
    //   #137	-> 277
    //   #138	-> 287
    //   #139	-> 287
    //   #136	-> 294
    //   #144	-> 300
    //   #146	-> 306
    //   #147	-> 314
    //   #150	-> 322
    //   #151	-> 335
    //   #104	-> 341
    //   #105	-> 342
    //   #107	-> 350
    //   #122	-> 350
    //   #107	-> 355
    //   #119	-> 357
    //   #120	-> 366
    //   #122	-> 366
    //   #120	-> 371
    //   #122	-> 373
    //   #123	-> 378
    // Exception table:
    //   from	to	target	type
    //   67	79	92	java/io/FileNotFoundException
    //   67	79	88	java/io/IOException
    //   67	79	84	finally
    //   94	105	341	java/io/FileNotFoundException
    //   94	105	88	java/io/IOException
    //   94	105	84	finally
    //   107	126	88	java/io/IOException
    //   107	126	84	finally
    //   139	144	88	java/io/IOException
    //   139	144	84	finally
    //   342	350	88	java/io/IOException
    //   342	350	84	finally
    //   357	366	84	finally
  }
  
  private KernelWakelockStats getWakelockStatsFromSystemSuspend(KernelWakelockStats paramKernelWakelockStats) {
    if (this.mSuspendControlService == null)
      try {
        IBinder iBinder = ServiceManager.getServiceOrThrow("suspend_control");
        this.mSuspendControlService = ISuspendControlService.Stub.asInterface(iBinder);
      } catch (android.os.ServiceManager.ServiceNotFoundException serviceNotFoundException) {
        Slog.wtf("KernelWakelockReader", "Required service suspend_control not available", (Throwable)serviceNotFoundException);
        return null;
      }  
    try {
      WakeLockInfo[] arrayOfWakeLockInfo = this.mSuspendControlService.getWakeLockStats();
      updateWakelockStats(arrayOfWakeLockInfo, (KernelWakelockStats)serviceNotFoundException);
      return (KernelWakelockStats)serviceNotFoundException;
    } catch (RemoteException remoteException) {
      Slog.wtf("KernelWakelockReader", "Failed to obtain wakelock stats from ISuspendControlService", (Throwable)remoteException);
      return null;
    } 
  }
  
  public KernelWakelockStats updateWakelockStats(WakeLockInfo[] paramArrayOfWakeLockInfo, KernelWakelockStats paramKernelWakelockStats) {
    int i;
    byte b;
    for (i = paramArrayOfWakeLockInfo.length, b = 0; b < i; ) {
      WakeLockInfo wakeLockInfo = paramArrayOfWakeLockInfo[b];
      if (!paramKernelWakelockStats.containsKey(wakeLockInfo.name)) {
        paramKernelWakelockStats.put(wakeLockInfo.name, new KernelWakelockStats.Entry((int)wakeLockInfo.activeCount, wakeLockInfo.totalTime * 1000L, sKernelWakelockUpdateVersion));
      } else {
        KernelWakelockStats.Entry entry = paramKernelWakelockStats.get(wakeLockInfo.name);
        entry.mCount = (int)wakeLockInfo.activeCount;
        entry.mTotalTime = wakeLockInfo.totalTime * 1000L;
        entry.mVersion = sKernelWakelockUpdateVersion;
      } 
      b++;
    } 
    return paramKernelWakelockStats;
  }
  
  public KernelWakelockStats parseProcWakelocks(byte[] paramArrayOfbyte, int paramInt, boolean paramBoolean, KernelWakelockStats paramKernelWakelockStats) {
    // Byte code:
    //   0: iconst_0
    //   1: istore #5
    //   3: iload #5
    //   5: iload_2
    //   6: if_icmpge -> 31
    //   9: aload_1
    //   10: iload #5
    //   12: baload
    //   13: bipush #10
    //   15: if_icmpeq -> 31
    //   18: aload_1
    //   19: iload #5
    //   21: baload
    //   22: ifeq -> 31
    //   25: iinc #5, 1
    //   28: goto -> 3
    //   31: iinc #5, 1
    //   34: iload #5
    //   36: istore #6
    //   38: aload_0
    //   39: monitorenter
    //   40: iload #6
    //   42: istore #7
    //   44: iload #6
    //   46: iload_2
    //   47: if_icmpge -> 436
    //   50: iload #5
    //   52: istore #6
    //   54: iload #6
    //   56: iload_2
    //   57: if_icmpge -> 82
    //   60: aload_1
    //   61: iload #6
    //   63: baload
    //   64: bipush #10
    //   66: if_icmpeq -> 82
    //   69: aload_1
    //   70: iload #6
    //   72: baload
    //   73: ifeq -> 82
    //   76: iinc #6, 1
    //   79: goto -> 54
    //   82: iload #6
    //   84: iload_2
    //   85: iconst_1
    //   86: isub
    //   87: if_icmple -> 97
    //   90: iload #6
    //   92: istore #7
    //   94: goto -> 436
    //   97: aload_0
    //   98: getfield mProcWakelocksName : [Ljava/lang/String;
    //   101: astore #8
    //   103: aload_0
    //   104: getfield mProcWakelocksData : [J
    //   107: astore #9
    //   109: iload #5
    //   111: istore #7
    //   113: iload #7
    //   115: iload #6
    //   117: if_icmpge -> 143
    //   120: aload_1
    //   121: iload #7
    //   123: baload
    //   124: sipush #128
    //   127: iand
    //   128: ifeq -> 137
    //   131: aload_1
    //   132: iload #7
    //   134: bipush #63
    //   136: bastore
    //   137: iinc #7, 1
    //   140: goto -> 113
    //   143: iload_3
    //   144: ifeq -> 155
    //   147: getstatic com/android/internal/os/KernelWakelockReader.WAKEUP_SOURCES_FORMAT : [I
    //   150: astore #10
    //   152: goto -> 160
    //   155: getstatic com/android/internal/os/KernelWakelockReader.PROC_WAKELOCKS_FORMAT : [I
    //   158: astore #10
    //   160: aload_1
    //   161: iload #5
    //   163: iload #6
    //   165: aload #10
    //   167: aload #8
    //   169: aload #9
    //   171: aconst_null
    //   172: invokestatic parseProcLine : ([BII[I[Ljava/lang/String;[J[F)Z
    //   175: istore #11
    //   177: aload #8
    //   179: iconst_0
    //   180: aaload
    //   181: invokevirtual trim : ()Ljava/lang/String;
    //   184: astore #10
    //   186: aload #9
    //   188: iconst_1
    //   189: laload
    //   190: l2i
    //   191: istore #7
    //   193: iload_3
    //   194: ifeq -> 210
    //   197: aload #9
    //   199: iconst_2
    //   200: laload
    //   201: ldc2_w 1000
    //   204: lmul
    //   205: lstore #12
    //   207: goto -> 224
    //   210: aload #9
    //   212: iconst_2
    //   213: laload
    //   214: ldc2_w 500
    //   217: ladd
    //   218: ldc2_w 1000
    //   221: ldiv
    //   222: lstore #12
    //   224: iload #11
    //   226: ifeq -> 354
    //   229: aload #10
    //   231: invokevirtual length : ()I
    //   234: ifle -> 354
    //   237: aload #4
    //   239: aload #10
    //   241: invokevirtual containsKey : (Ljava/lang/Object;)Z
    //   244: ifne -> 277
    //   247: new com/android/internal/os/KernelWakelockStats$Entry
    //   250: astore #9
    //   252: aload #9
    //   254: iload #7
    //   256: lload #12
    //   258: getstatic com/android/internal/os/KernelWakelockReader.sKernelWakelockUpdateVersion : I
    //   261: invokespecial <init> : (IJI)V
    //   264: aload #4
    //   266: aload #10
    //   268: aload #9
    //   270: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   273: pop
    //   274: goto -> 427
    //   277: aload #4
    //   279: aload #10
    //   281: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   284: checkcast com/android/internal/os/KernelWakelockStats$Entry
    //   287: astore #10
    //   289: aload #10
    //   291: getfield mVersion : I
    //   294: getstatic com/android/internal/os/KernelWakelockReader.sKernelWakelockUpdateVersion : I
    //   297: if_icmpne -> 329
    //   300: aload #10
    //   302: aload #10
    //   304: getfield mCount : I
    //   307: iload #7
    //   309: iadd
    //   310: putfield mCount : I
    //   313: aload #10
    //   315: aload #10
    //   317: getfield mTotalTime : J
    //   320: lload #12
    //   322: ladd
    //   323: putfield mTotalTime : J
    //   326: goto -> 351
    //   329: aload #10
    //   331: iload #7
    //   333: putfield mCount : I
    //   336: aload #10
    //   338: lload #12
    //   340: putfield mTotalTime : J
    //   343: aload #10
    //   345: getstatic com/android/internal/os/KernelWakelockReader.sKernelWakelockUpdateVersion : I
    //   348: putfield mVersion : I
    //   351: goto -> 427
    //   354: iload #11
    //   356: ifne -> 427
    //   359: new java/lang/StringBuilder
    //   362: astore #10
    //   364: aload #10
    //   366: invokespecial <init> : ()V
    //   369: aload #10
    //   371: ldc 'Failed to parse proc line: '
    //   373: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   376: pop
    //   377: new java/lang/String
    //   380: astore #9
    //   382: aload #9
    //   384: aload_1
    //   385: iload #5
    //   387: iload #6
    //   389: iload #5
    //   391: isub
    //   392: invokespecial <init> : ([BII)V
    //   395: aload #10
    //   397: aload #9
    //   399: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   402: pop
    //   403: ldc 'KernelWakelockReader'
    //   405: aload #10
    //   407: invokevirtual toString : ()Ljava/lang/String;
    //   410: invokestatic wtf : (Ljava/lang/String;Ljava/lang/String;)I
    //   413: pop
    //   414: goto -> 427
    //   417: astore #10
    //   419: ldc 'KernelWakelockReader'
    //   421: ldc 'Failed to parse proc line!'
    //   423: invokestatic wtf : (Ljava/lang/String;Ljava/lang/String;)I
    //   426: pop
    //   427: iload #6
    //   429: iconst_1
    //   430: iadd
    //   431: istore #5
    //   433: goto -> 40
    //   436: aload_0
    //   437: monitorexit
    //   438: aload #4
    //   440: areturn
    //   441: astore_1
    //   442: aload_0
    //   443: monitorexit
    //   444: aload_1
    //   445: athrow
    //   446: astore_1
    //   447: goto -> 442
    // Line number table:
    //   Java source line number -> byte code offset
    //   #220	-> 0
    //   #221	-> 31
    //   #223	-> 38
    //   #224	-> 40
    //   #225	-> 50
    //   #226	-> 54
    //   #227	-> 76
    //   #230	-> 82
    //   #231	-> 90
    //   #234	-> 97
    //   #235	-> 103
    //   #239	-> 109
    //   #240	-> 120
    //   #239	-> 137
    //   #242	-> 143
    //   #243	-> 143
    //   #244	-> 155
    //   #242	-> 160
    //   #247	-> 177
    //   #248	-> 186
    //   #250	-> 193
    //   #252	-> 197
    //   #255	-> 210
    //   #258	-> 224
    //   #259	-> 237
    //   #260	-> 247
    //   #263	-> 277
    //   #264	-> 289
    //   #265	-> 300
    //   #266	-> 313
    //   #268	-> 329
    //   #269	-> 336
    //   #270	-> 343
    //   #272	-> 351
    //   #273	-> 354
    //   #275	-> 359
    //   #279	-> 414
    //   #277	-> 417
    //   #278	-> 419
    //   #281	-> 427
    //   #282	-> 433
    //   #284	-> 436
    //   #285	-> 441
    // Exception table:
    //   from	to	target	type
    //   97	103	446	finally
    //   103	109	446	finally
    //   147	152	446	finally
    //   155	160	446	finally
    //   160	177	446	finally
    //   177	186	446	finally
    //   210	224	446	finally
    //   229	237	446	finally
    //   237	247	446	finally
    //   247	274	446	finally
    //   277	289	446	finally
    //   289	300	446	finally
    //   300	313	446	finally
    //   313	326	446	finally
    //   329	336	446	finally
    //   336	343	446	finally
    //   343	351	446	finally
    //   359	414	417	java/lang/Exception
    //   359	414	446	finally
    //   419	427	446	finally
    //   436	438	441	finally
    //   442	444	446	finally
  }
  
  public KernelWakelockStats updateVersion(KernelWakelockStats paramKernelWakelockStats) {
    int i = sKernelWakelockUpdateVersion + 1;
    paramKernelWakelockStats.kernelWakelockVersion = i;
    return paramKernelWakelockStats;
  }
  
  public KernelWakelockStats removeOldStats(KernelWakelockStats paramKernelWakelockStats) {
    Iterator<KernelWakelockStats.Entry> iterator = paramKernelWakelockStats.values().iterator();
    while (iterator.hasNext()) {
      if (((KernelWakelockStats.Entry)iterator.next()).mVersion != sKernelWakelockUpdateVersion)
        iterator.remove(); 
    } 
    return paramKernelWakelockStats;
  }
}
