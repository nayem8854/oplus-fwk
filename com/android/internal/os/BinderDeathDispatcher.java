package com.android.internal.os;

import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import android.util.ArrayMap;
import android.util.ArraySet;
import java.io.PrintWriter;

public class BinderDeathDispatcher<T extends IInterface> {
  private static final String TAG = "BinderDeathDispatcher";
  
  private final Object mLock = new Object();
  
  private final ArrayMap<IBinder, RecipientsInfo> mTargets = new ArrayMap<>();
  
  class RecipientsInfo implements IBinder.DeathRecipient {
    ArraySet<IBinder.DeathRecipient> mRecipients = new ArraySet<>();
    
    final IBinder mTarget;
    
    final BinderDeathDispatcher this$0;
    
    private RecipientsInfo(IBinder param1IBinder) {
      this.mTarget = param1IBinder;
    }
    
    public void binderDied() {
      synchronized (BinderDeathDispatcher.this.mLock) {
        ArraySet<IBinder.DeathRecipient> arraySet = this.mRecipients;
        this.mRecipients = null;
        BinderDeathDispatcher.access$100(BinderDeathDispatcher.this).remove(this.mTarget);
        if (arraySet == null)
          return; 
        int i = arraySet.size();
        for (byte b = 0; b < i; b++)
          ((IBinder.DeathRecipient)arraySet.valueAt(b)).binderDied(); 
        return;
      } 
    }
  }
  
  public int linkToDeath(T paramT, IBinder.DeathRecipient paramDeathRecipient) {
    IBinder iBinder = paramT.asBinder();
    synchronized (this.mLock) {
      RecipientsInfo recipientsInfo2 = this.mTargets.get(iBinder);
      RecipientsInfo recipientsInfo1 = recipientsInfo2;
      if (recipientsInfo2 == null) {
        recipientsInfo1 = new RecipientsInfo();
        this(this, iBinder);
        try {
          iBinder.linkToDeath(recipientsInfo1, 0);
          this.mTargets.put(iBinder, recipientsInfo1);
        } catch (RemoteException remoteException) {
          return -1;
        } 
      } 
      ((RecipientsInfo)remoteException).mRecipients.add(paramDeathRecipient);
      return ((RecipientsInfo)remoteException).mRecipients.size();
    } 
  }
  
  public void unlinkToDeath(T paramT, IBinder.DeathRecipient paramDeathRecipient) {
    IBinder iBinder = paramT.asBinder();
    synchronized (this.mLock) {
      RecipientsInfo recipientsInfo = this.mTargets.get(iBinder);
      if (recipientsInfo == null)
        return; 
      if (recipientsInfo.mRecipients.remove(paramDeathRecipient) && recipientsInfo.mRecipients.size() == 0) {
        recipientsInfo.mTarget.unlinkToDeath(recipientsInfo, 0);
        this.mTargets.remove(recipientsInfo.mTarget);
      } 
      return;
    } 
  }
  
  public void dump(PrintWriter paramPrintWriter, String paramString) {
    synchronized (this.mLock) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("# of watched binders: ");
      paramPrintWriter.println(this.mTargets.size());
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("# of death recipients: ");
      int i = 0;
      for (RecipientsInfo recipientsInfo : this.mTargets.values())
        i += recipientsInfo.mRecipients.size(); 
      paramPrintWriter.println(i);
      return;
    } 
  }
  
  public ArrayMap<IBinder, RecipientsInfo> getTargetsForTest() {
    return this.mTargets;
  }
}
