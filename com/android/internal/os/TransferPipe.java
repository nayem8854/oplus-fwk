package com.android.internal.os;

import android.os.IBinder;
import android.os.IInterface;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import libcore.io.IoUtils;

public class TransferPipe implements Runnable, Closeable {
  static final boolean DEBUG = false;
  
  static final long DEFAULT_TIMEOUT = 5000L;
  
  static final String TAG = "TransferPipe";
  
  String mBufferPrefix;
  
  boolean mComplete;
  
  long mEndTime;
  
  String mFailure;
  
  final ParcelFileDescriptor[] mFds;
  
  FileDescriptor mOutFd;
  
  final Thread mThread;
  
  public TransferPipe() throws IOException {
    this(null);
  }
  
  public TransferPipe(String paramString) throws IOException {
    this(paramString, "TransferPipe");
  }
  
  protected TransferPipe(String paramString1, String paramString2) throws IOException {
    this.mThread = new Thread(this, paramString2);
    this.mFds = ParcelFileDescriptor.createPipe();
    this.mBufferPrefix = paramString1;
  }
  
  ParcelFileDescriptor getReadFd() {
    return this.mFds[0];
  }
  
  public ParcelFileDescriptor getWriteFd() {
    return this.mFds[1];
  }
  
  public void setBufferPrefix(String paramString) {
    this.mBufferPrefix = paramString;
  }
  
  public static void dumpAsync(IBinder paramIBinder, FileDescriptor paramFileDescriptor, String[] paramArrayOfString) throws IOException, RemoteException {
    goDump(paramIBinder, paramFileDescriptor, paramArrayOfString);
  }
  
  public static byte[] dumpAsync(IBinder paramIBinder, String... paramVarArgs) throws IOException, RemoteException {
    ParcelFileDescriptor[] arrayOfParcelFileDescriptor = ParcelFileDescriptor.createPipe();
    try {
      dumpAsync(paramIBinder, arrayOfParcelFileDescriptor[1].getFileDescriptor(), paramVarArgs);
      arrayOfParcelFileDescriptor[1].close();
      arrayOfParcelFileDescriptor[1] = null;
      null = new byte[4096];
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    } finally {
      arrayOfParcelFileDescriptor[0].close();
      IoUtils.closeQuietly((AutoCloseable)arrayOfParcelFileDescriptor[1]);
    } 
  }
  
  static void go(Caller paramCaller, IInterface paramIInterface, FileDescriptor paramFileDescriptor, String paramString, String[] paramArrayOfString) throws IOException, RemoteException {
    go(paramCaller, paramIInterface, paramFileDescriptor, paramString, paramArrayOfString, 5000L);
  }
  
  static void go(Caller paramCaller, IInterface paramIInterface, FileDescriptor paramFileDescriptor, String paramString, String[] paramArrayOfString, long paramLong) throws IOException, RemoteException {
    if (paramIInterface.asBinder() instanceof android.os.Binder) {
      try {
        paramCaller.go(paramIInterface, paramFileDescriptor, paramString, paramArrayOfString);
      } catch (RemoteException null) {}
      return;
    } 
    TransferPipe transferPipe = new TransferPipe();
    try {
      null.go(paramIInterface, transferPipe.getWriteFd().getFileDescriptor(), paramString, paramArrayOfString);
      transferPipe.go(paramFileDescriptor, paramLong);
      return;
    } finally {
      try {
        transferPipe.close();
      } finally {
        paramIInterface = null;
      } 
    } 
  }
  
  static void goDump(IBinder paramIBinder, FileDescriptor paramFileDescriptor, String[] paramArrayOfString) throws IOException, RemoteException {
    goDump(paramIBinder, paramFileDescriptor, paramArrayOfString, 5000L);
  }
  
  static void goDump(IBinder paramIBinder, FileDescriptor paramFileDescriptor, String[] paramArrayOfString, long paramLong) throws IOException, RemoteException {
    if (paramIBinder instanceof android.os.Binder) {
      try {
        paramIBinder.dump(paramFileDescriptor, paramArrayOfString);
      } catch (RemoteException null) {}
      return;
    } 
    TransferPipe transferPipe = new TransferPipe();
    try {
      null.dumpAsync(transferPipe.getWriteFd().getFileDescriptor(), paramArrayOfString);
      transferPipe.go(paramFileDescriptor, paramLong);
      return;
    } finally {
      try {
        transferPipe.close();
      } finally {
        paramFileDescriptor = null;
      } 
    } 
  }
  
  public void go(FileDescriptor paramFileDescriptor) throws IOException {
    go(paramFileDescriptor, 5000L);
  }
  
  public void go(FileDescriptor paramFileDescriptor, long paramLong) throws IOException {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: putfield mOutFd : Ljava/io/FileDescriptor;
    //   7: aload_0
    //   8: invokestatic uptimeMillis : ()J
    //   11: lload_2
    //   12: ladd
    //   13: putfield mEndTime : J
    //   16: aload_0
    //   17: iconst_1
    //   18: invokevirtual closeFd : (I)V
    //   21: aload_0
    //   22: getfield mThread : Ljava/lang/Thread;
    //   25: invokevirtual start : ()V
    //   28: aload_0
    //   29: getfield mFailure : Ljava/lang/String;
    //   32: ifnonnull -> 94
    //   35: aload_0
    //   36: getfield mComplete : Z
    //   39: ifne -> 94
    //   42: aload_0
    //   43: getfield mEndTime : J
    //   46: lstore #4
    //   48: invokestatic uptimeMillis : ()J
    //   51: lstore_2
    //   52: lload #4
    //   54: lload_2
    //   55: lsub
    //   56: lstore_2
    //   57: lload_2
    //   58: lconst_0
    //   59: lcmp
    //   60: ifle -> 75
    //   63: aload_0
    //   64: lload_2
    //   65: invokevirtual wait : (J)V
    //   68: goto -> 72
    //   71: astore_1
    //   72: goto -> 28
    //   75: aload_0
    //   76: getfield mThread : Ljava/lang/Thread;
    //   79: invokevirtual interrupt : ()V
    //   82: new java/io/IOException
    //   85: astore_1
    //   86: aload_1
    //   87: ldc 'Timeout'
    //   89: invokespecial <init> : (Ljava/lang/String;)V
    //   92: aload_1
    //   93: athrow
    //   94: aload_0
    //   95: getfield mFailure : Ljava/lang/String;
    //   98: ifnonnull -> 108
    //   101: aload_0
    //   102: monitorexit
    //   103: aload_0
    //   104: invokevirtual kill : ()V
    //   107: return
    //   108: new java/io/IOException
    //   111: astore_1
    //   112: aload_1
    //   113: aload_0
    //   114: getfield mFailure : Ljava/lang/String;
    //   117: invokespecial <init> : (Ljava/lang/String;)V
    //   120: aload_1
    //   121: athrow
    //   122: astore_1
    //   123: aload_0
    //   124: monitorexit
    //   125: aload_1
    //   126: athrow
    //   127: astore_1
    //   128: aload_0
    //   129: invokevirtual kill : ()V
    //   132: aload_1
    //   133: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #183	-> 0
    //   #184	-> 2
    //   #185	-> 7
    //   #191	-> 16
    //   #193	-> 21
    //   #195	-> 28
    //   #196	-> 42
    //   #197	-> 57
    //   #204	-> 63
    //   #206	-> 68
    //   #205	-> 71
    //   #207	-> 72
    //   #199	-> 75
    //   #200	-> 82
    //   #210	-> 94
    //   #213	-> 101
    //   #215	-> 103
    //   #216	-> 107
    //   #217	-> 107
    //   #211	-> 108
    //   #213	-> 122
    //   #215	-> 127
    //   #216	-> 132
    // Exception table:
    //   from	to	target	type
    //   0	2	127	finally
    //   2	7	122	finally
    //   7	16	122	finally
    //   16	21	122	finally
    //   21	28	122	finally
    //   28	42	122	finally
    //   42	52	122	finally
    //   63	68	71	java/lang/InterruptedException
    //   63	68	122	finally
    //   75	82	122	finally
    //   82	94	122	finally
    //   94	101	122	finally
    //   101	103	122	finally
    //   108	122	122	finally
    //   123	125	122	finally
    //   125	127	127	finally
  }
  
  void closeFd(int paramInt) {
    ParcelFileDescriptor[] arrayOfParcelFileDescriptor = this.mFds;
    if (arrayOfParcelFileDescriptor[paramInt] != null) {
      try {
        arrayOfParcelFileDescriptor[paramInt].close();
      } catch (IOException iOException) {}
      this.mFds[paramInt] = null;
    } 
  }
  
  public void close() {
    kill();
  }
  
  public void kill() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iconst_0
    //   4: invokevirtual closeFd : (I)V
    //   7: aload_0
    //   8: iconst_1
    //   9: invokevirtual closeFd : (I)V
    //   12: aload_0
    //   13: monitorexit
    //   14: return
    //   15: astore_1
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_1
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #236	-> 0
    //   #237	-> 2
    //   #238	-> 7
    //   #239	-> 12
    //   #240	-> 14
    //   #239	-> 15
    // Exception table:
    //   from	to	target	type
    //   2	7	15	finally
    //   7	12	15	finally
    //   12	14	15	finally
    //   16	18	15	finally
  }
  
  protected OutputStream getNewOutputStream() {
    return new FileOutputStream(this.mOutFd);
  }
  
  public void run() {
    // Byte code:
    //   0: sipush #1024
    //   3: newarray byte
    //   5: astore_1
    //   6: aload_0
    //   7: monitorenter
    //   8: aload_0
    //   9: invokevirtual getReadFd : ()Landroid/os/ParcelFileDescriptor;
    //   12: astore_2
    //   13: aload_2
    //   14: ifnonnull -> 28
    //   17: ldc 'TransferPipe'
    //   19: ldc 'Pipe has been closed...'
    //   21: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   24: pop
    //   25: aload_0
    //   26: monitorexit
    //   27: return
    //   28: new java/io/FileInputStream
    //   31: astore_3
    //   32: aload_3
    //   33: aload_2
    //   34: invokevirtual getFileDescriptor : ()Ljava/io/FileDescriptor;
    //   37: invokespecial <init> : (Ljava/io/FileDescriptor;)V
    //   40: aload_0
    //   41: invokevirtual getNewOutputStream : ()Ljava/io/OutputStream;
    //   44: astore #4
    //   46: aload_0
    //   47: monitorexit
    //   48: aconst_null
    //   49: astore_2
    //   50: iconst_1
    //   51: istore #5
    //   53: aload_0
    //   54: getfield mBufferPrefix : Ljava/lang/String;
    //   57: astore #6
    //   59: iload #5
    //   61: istore #7
    //   63: aload #6
    //   65: ifnull -> 78
    //   68: aload #6
    //   70: invokevirtual getBytes : ()[B
    //   73: astore_2
    //   74: iload #5
    //   76: istore #7
    //   78: aload_3
    //   79: aload_1
    //   80: invokevirtual read : ([B)I
    //   83: istore #8
    //   85: iload #8
    //   87: ifle -> 282
    //   90: aload_2
    //   91: ifnonnull -> 106
    //   94: aload #4
    //   96: aload_1
    //   97: iconst_0
    //   98: iload #8
    //   100: invokevirtual write : ([BII)V
    //   103: goto -> 78
    //   106: iconst_0
    //   107: istore #9
    //   109: iconst_0
    //   110: istore #5
    //   112: iload #5
    //   114: iload #8
    //   116: if_icmpge -> 259
    //   119: iload #7
    //   121: istore #10
    //   123: iload #9
    //   125: istore #11
    //   127: iload #5
    //   129: istore #12
    //   131: aload_1
    //   132: iload #5
    //   134: baload
    //   135: bipush #10
    //   137: if_icmpeq -> 242
    //   140: iload #5
    //   142: iload #9
    //   144: if_icmple -> 160
    //   147: aload #4
    //   149: aload_1
    //   150: iload #9
    //   152: iload #5
    //   154: iload #9
    //   156: isub
    //   157: invokevirtual write : ([BII)V
    //   160: iload #5
    //   162: istore #9
    //   164: iload #7
    //   166: istore #10
    //   168: iload #5
    //   170: istore #11
    //   172: iload #7
    //   174: ifeq -> 190
    //   177: aload #4
    //   179: aload_2
    //   180: invokevirtual write : ([B)V
    //   183: iconst_0
    //   184: istore #10
    //   186: iload #5
    //   188: istore #11
    //   190: iload #11
    //   192: iconst_1
    //   193: iadd
    //   194: istore #5
    //   196: iload #5
    //   198: iload #8
    //   200: if_icmpge -> 216
    //   203: iload #5
    //   205: istore #11
    //   207: aload_1
    //   208: iload #5
    //   210: baload
    //   211: bipush #10
    //   213: if_icmpne -> 190
    //   216: iload #9
    //   218: istore #11
    //   220: iload #5
    //   222: istore #12
    //   224: iload #5
    //   226: iload #8
    //   228: if_icmpge -> 242
    //   231: iconst_1
    //   232: istore #10
    //   234: iload #5
    //   236: istore #12
    //   238: iload #9
    //   240: istore #11
    //   242: iload #12
    //   244: iconst_1
    //   245: iadd
    //   246: istore #5
    //   248: iload #10
    //   250: istore #7
    //   252: iload #11
    //   254: istore #9
    //   256: goto -> 112
    //   259: iload #8
    //   261: iload #9
    //   263: if_icmple -> 279
    //   266: aload #4
    //   268: aload_1
    //   269: iload #9
    //   271: iload #8
    //   273: iload #9
    //   275: isub
    //   276: invokevirtual write : ([BII)V
    //   279: goto -> 78
    //   282: aload_0
    //   283: getfield mThread : Ljava/lang/Thread;
    //   286: invokevirtual isInterrupted : ()Z
    //   289: pop
    //   290: aload_0
    //   291: monitorenter
    //   292: aload_0
    //   293: iconst_1
    //   294: putfield mComplete : Z
    //   297: aload_0
    //   298: invokevirtual notifyAll : ()V
    //   301: aload_0
    //   302: monitorexit
    //   303: return
    //   304: astore_2
    //   305: aload_0
    //   306: monitorexit
    //   307: aload_2
    //   308: athrow
    //   309: astore_2
    //   310: aload_0
    //   311: monitorenter
    //   312: aload_0
    //   313: aload_2
    //   314: invokevirtual toString : ()Ljava/lang/String;
    //   317: putfield mFailure : Ljava/lang/String;
    //   320: aload_0
    //   321: invokevirtual notifyAll : ()V
    //   324: aload_0
    //   325: monitorexit
    //   326: return
    //   327: astore_2
    //   328: aload_0
    //   329: monitorexit
    //   330: aload_2
    //   331: athrow
    //   332: astore_2
    //   333: aload_0
    //   334: monitorexit
    //   335: aload_2
    //   336: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #248	-> 0
    //   #252	-> 6
    //   #253	-> 8
    //   #254	-> 13
    //   #255	-> 17
    //   #256	-> 25
    //   #258	-> 28
    //   #259	-> 40
    //   #260	-> 46
    //   #263	-> 48
    //   #264	-> 50
    //   #265	-> 53
    //   #266	-> 68
    //   #271	-> 78
    //   #273	-> 90
    //   #274	-> 94
    //   #276	-> 106
    //   #277	-> 109
    //   #278	-> 119
    //   #279	-> 140
    //   #280	-> 147
    //   #282	-> 160
    //   #283	-> 164
    //   #284	-> 177
    //   #285	-> 183
    //   #288	-> 190
    //   #289	-> 196
    //   #290	-> 216
    //   #291	-> 231
    //   #277	-> 242
    //   #295	-> 259
    //   #296	-> 266
    //   #298	-> 279
    //   #301	-> 282
    //   #310	-> 290
    //   #312	-> 290
    //   #313	-> 292
    //   #314	-> 297
    //   #315	-> 301
    //   #316	-> 303
    //   #315	-> 304
    //   #304	-> 309
    //   #305	-> 310
    //   #306	-> 312
    //   #307	-> 320
    //   #308	-> 324
    //   #309	-> 327
    //   #260	-> 332
    // Exception table:
    //   from	to	target	type
    //   8	13	332	finally
    //   17	25	332	finally
    //   25	27	332	finally
    //   28	40	332	finally
    //   40	46	332	finally
    //   46	48	332	finally
    //   78	85	309	java/io/IOException
    //   94	103	309	java/io/IOException
    //   147	160	309	java/io/IOException
    //   177	183	309	java/io/IOException
    //   266	279	309	java/io/IOException
    //   282	290	309	java/io/IOException
    //   292	297	304	finally
    //   297	301	304	finally
    //   301	303	304	finally
    //   305	307	304	finally
    //   312	320	327	finally
    //   320	324	327	finally
    //   324	326	327	finally
    //   328	330	327	finally
    //   333	335	332	finally
  }
  
  static interface Caller {
    void go(IInterface param1IInterface, FileDescriptor param1FileDescriptor, String param1String, String[] param1ArrayOfString) throws RemoteException;
  }
}
