package com.android.internal.os;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

public interface IParcelFileDescriptorFactory extends IInterface {
  ParcelFileDescriptor open(String paramString, int paramInt) throws RemoteException;
  
  class Default implements IParcelFileDescriptorFactory {
    public ParcelFileDescriptor open(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IParcelFileDescriptorFactory {
    private static final String DESCRIPTOR = "com.android.internal.os.IParcelFileDescriptorFactory";
    
    static final int TRANSACTION_open = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.os.IParcelFileDescriptorFactory");
    }
    
    public static IParcelFileDescriptorFactory asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.os.IParcelFileDescriptorFactory");
      if (iInterface != null && iInterface instanceof IParcelFileDescriptorFactory)
        return (IParcelFileDescriptorFactory)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "open";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.os.IParcelFileDescriptorFactory");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.os.IParcelFileDescriptorFactory");
      String str = param1Parcel1.readString();
      param1Int1 = param1Parcel1.readInt();
      ParcelFileDescriptor parcelFileDescriptor = open(str, param1Int1);
      param1Parcel2.writeNoException();
      if (parcelFileDescriptor != null) {
        param1Parcel2.writeInt(1);
        parcelFileDescriptor.writeToParcel(param1Parcel2, 1);
      } else {
        param1Parcel2.writeInt(0);
      } 
      return true;
    }
    
    private static class Proxy implements IParcelFileDescriptorFactory {
      public static IParcelFileDescriptorFactory sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.os.IParcelFileDescriptorFactory";
      }
      
      public ParcelFileDescriptor open(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.os.IParcelFileDescriptorFactory");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IParcelFileDescriptorFactory.Stub.getDefaultImpl() != null)
            return IParcelFileDescriptorFactory.Stub.getDefaultImpl().open(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParcelFileDescriptor)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IParcelFileDescriptorFactory param1IParcelFileDescriptorFactory) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IParcelFileDescriptorFactory != null) {
          Proxy.sDefaultImpl = param1IParcelFileDescriptorFactory;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IParcelFileDescriptorFactory getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
