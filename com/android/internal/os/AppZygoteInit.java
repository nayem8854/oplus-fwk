package com.android.internal.os;

import android.app.LoadedApk;
import android.app.ZygotePreload;
import android.content.ComponentName;
import android.content.pm.ApplicationInfo;
import android.net.LocalSocket;
import android.util.Log;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;

class AppZygoteInit {
  public static final String TAG = "AppZygoteInit";
  
  private static ZygoteServer sServer;
  
  class AppZygoteServer extends ZygoteServer {
    private AppZygoteServer() {}
    
    protected ZygoteConnection createNewConnection(LocalSocket param1LocalSocket, String param1String) throws IOException {
      return new AppZygoteInit.AppZygoteConnection(param1LocalSocket, param1String);
    }
  }
  
  class AppZygoteConnection extends ZygoteConnection {
    AppZygoteConnection(AppZygoteInit this$0, String param1String) throws IOException {
      super((LocalSocket)this$0, param1String);
    }
    
    protected void preload() {}
    
    protected boolean isPreloadComplete() {
      return true;
    }
    
    protected boolean canPreloadApp() {
      return true;
    }
    
    protected void handlePreloadApp(ApplicationInfo param1ApplicationInfo) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Beginning application preload for ");
      stringBuilder.append(param1ApplicationInfo.packageName);
      Log.i("AppZygoteInit", stringBuilder.toString());
      LoadedApk loadedApk = new LoadedApk(null, param1ApplicationInfo, null, null, false, true, false);
      ClassLoader classLoader = loadedApk.getClassLoader();
      Zygote.allowAppFilesAcrossFork(param1ApplicationInfo);
      String str = param1ApplicationInfo.zygotePreloadName;
      boolean bool = true;
      if (str != null) {
        try {
          StringBuilder stringBuilder1;
          ComponentName componentName = ComponentName.createRelative(param1ApplicationInfo.packageName, param1ApplicationInfo.zygotePreloadName);
          Class<?> clazz = Class.forName(componentName.getClassName(), true, classLoader);
          if (!ZygotePreload.class.isAssignableFrom(clazz)) {
            stringBuilder1 = new StringBuilder();
            this();
            stringBuilder1.append(componentName.getClassName());
            stringBuilder1.append(" does not implement ");
            stringBuilder1.append(ZygotePreload.class.getName());
            String str1 = stringBuilder1.toString();
            Log.e("AppZygoteInit", str1);
          } else {
            Constructor<ZygotePreload> constructor = stringBuilder1.getConstructor(new Class[0]);
            ZygotePreload zygotePreload = constructor.newInstance(new Object[0]);
            zygotePreload.doPreload(param1ApplicationInfo);
          } 
        } catch (ReflectiveOperationException reflectiveOperationException) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("AppZygote application preload failed for ");
          stringBuilder1.append(param1ApplicationInfo.zygotePreloadName);
          Log.e("AppZygoteInit", stringBuilder1.toString(), reflectiveOperationException);
        } 
      } else {
        Log.i("AppZygoteInit", "No zygotePreloadName attribute specified.");
      } 
      try {
        DataOutputStream dataOutputStream = getSocketOutputStream();
        if (classLoader == null)
          bool = false; 
        dataOutputStream.writeInt(bool);
        Log.i("AppZygoteInit", "Application preload done");
        return;
      } catch (IOException iOException) {
        throw new IllegalStateException("Error writing to command socket", iOException);
      } 
    }
  }
  
  public static void main(String[] paramArrayOfString) {
    AppZygoteServer appZygoteServer = new AppZygoteServer();
    ChildZygoteInit.runZygoteServer(appZygoteServer, paramArrayOfString);
  }
}
