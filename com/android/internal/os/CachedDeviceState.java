package com.android.internal.os;

import android.os.SystemClock;
import java.util.ArrayList;

public class CachedDeviceState {
  private final Object mStopwatchesLock = new Object();
  
  private volatile boolean mScreenInteractive;
  
  private final ArrayList<TimeInStateStopwatch> mOnBatteryStopwatches = new ArrayList<>();
  
  private volatile boolean mCharging;
  
  public CachedDeviceState() {
    this.mCharging = true;
    this.mScreenInteractive = false;
  }
  
  public CachedDeviceState(boolean paramBoolean1, boolean paramBoolean2) {
    this.mCharging = paramBoolean1;
    this.mScreenInteractive = paramBoolean2;
  }
  
  public void setScreenInteractive(boolean paramBoolean) {
    this.mScreenInteractive = paramBoolean;
  }
  
  public void setCharging(boolean paramBoolean) {
    if (this.mCharging != paramBoolean) {
      this.mCharging = paramBoolean;
      updateStopwatches(paramBoolean ^ true);
    } 
  }
  
  private void updateStopwatches(boolean paramBoolean) {
    synchronized (this.mStopwatchesLock) {
      int i = this.mOnBatteryStopwatches.size();
      for (byte b = 0; b < i; b++) {
        if (paramBoolean) {
          ((TimeInStateStopwatch)this.mOnBatteryStopwatches.get(b)).start();
        } else {
          ((TimeInStateStopwatch)this.mOnBatteryStopwatches.get(b)).stop();
        } 
      } 
      return;
    } 
  }
  
  public Readonly getReadonlyClient() {
    return new Readonly();
  }
  
  public class Readonly {
    final CachedDeviceState this$0;
    
    public boolean isCharging() {
      return CachedDeviceState.this.mCharging;
    }
    
    public boolean isScreenInteractive() {
      return CachedDeviceState.this.mScreenInteractive;
    }
    
    public CachedDeviceState.TimeInStateStopwatch createTimeOnBatteryStopwatch() {
      synchronized (CachedDeviceState.this.mStopwatchesLock) {
        CachedDeviceState.TimeInStateStopwatch timeInStateStopwatch = new CachedDeviceState.TimeInStateStopwatch();
        this();
        CachedDeviceState.this.mOnBatteryStopwatches.add(timeInStateStopwatch);
        if (!CachedDeviceState.this.mCharging)
          timeInStateStopwatch.start(); 
        return timeInStateStopwatch;
      } 
    }
  }
  
  public class TimeInStateStopwatch implements AutoCloseable {
    private final Object mLock;
    
    private long mStartTimeMillis;
    
    private long mTotalTimeMillis;
    
    final CachedDeviceState this$0;
    
    public TimeInStateStopwatch() {
      this.mLock = new Object();
    }
    
    public long getMillis() {
      synchronized (this.mLock) {
        long l1 = this.mTotalTimeMillis, l2 = elapsedTime();
        return l1 + l2;
      } 
    }
    
    public void reset() {
      // Byte code:
      //   0: aload_0
      //   1: getfield mLock : Ljava/lang/Object;
      //   4: astore_1
      //   5: aload_1
      //   6: monitorenter
      //   7: lconst_0
      //   8: lstore_2
      //   9: aload_0
      //   10: lconst_0
      //   11: putfield mTotalTimeMillis : J
      //   14: aload_0
      //   15: invokevirtual isRunning : ()Z
      //   18: ifeq -> 25
      //   21: invokestatic elapsedRealtime : ()J
      //   24: lstore_2
      //   25: aload_0
      //   26: lload_2
      //   27: putfield mStartTimeMillis : J
      //   30: aload_1
      //   31: monitorexit
      //   32: return
      //   33: astore #4
      //   35: aload_1
      //   36: monitorexit
      //   37: aload #4
      //   39: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #121	-> 0
      //   #122	-> 7
      //   #123	-> 14
      //   #124	-> 30
      //   #125	-> 32
      //   #124	-> 33
      // Exception table:
      //   from	to	target	type
      //   9	14	33	finally
      //   14	25	33	finally
      //   25	30	33	finally
      //   30	32	33	finally
      //   35	37	33	finally
    }
    
    private void start() {
      synchronized (this.mLock) {
        if (!isRunning())
          this.mStartTimeMillis = SystemClock.elapsedRealtime(); 
        return;
      } 
    }
    
    private void stop() {
      synchronized (this.mLock) {
        if (isRunning()) {
          this.mTotalTimeMillis += elapsedTime();
          this.mStartTimeMillis = 0L;
        } 
        return;
      } 
    }
    
    private long elapsedTime() {
      long l;
      if (isRunning()) {
        l = SystemClock.elapsedRealtime() - this.mStartTimeMillis;
      } else {
        l = 0L;
      } 
      return l;
    }
    
    public boolean isRunning() {
      boolean bool;
      if (this.mStartTimeMillis > 0L) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void close() {
      synchronized (CachedDeviceState.this.mStopwatchesLock) {
        CachedDeviceState.this.mOnBatteryStopwatches.remove(this);
        return;
      } 
    }
  }
}
