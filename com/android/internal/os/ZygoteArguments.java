package com.android.internal.os;

import java.util.ArrayList;

class ZygoteArguments {
  int mUid = 0;
  
  int mGid = 0;
  
  int mMountExternal = 0;
  
  boolean mUsapPoolStatusSpecified = false;
  
  int mHiddenApiAccessLogSampleRate = -1;
  
  int mHiddenApiAccessStatslogSampleRate = -1;
  
  long[] mDisabledCompatChanges = null;
  
  boolean mAbiListQuery;
  
  String[] mApiBlacklistExemptions;
  
  String mAppDataDir;
  
  boolean mBindMountAppDataDirs;
  
  boolean mBindMountAppStorageDirs;
  
  boolean mBootCompleted;
  
  private boolean mCapabilitiesSpecified;
  
  long mEffectiveCapabilities;
  
  boolean mGidSpecified;
  
  int[] mGids;
  
  String mInstructionSet;
  
  String mInvokeWith;
  
  boolean mIsTopApp;
  
  String mNiceName;
  
  String mPackageName;
  
  long mPermittedCapabilities;
  
  boolean mPidQuery;
  
  String[] mPkgDataInfoList;
  
  String mPreloadApp;
  
  boolean mPreloadDefault;
  
  String mPreloadPackage;
  
  String mPreloadPackageCacheKey;
  
  String mPreloadPackageLibFileName;
  
  String mPreloadPackageLibs;
  
  ArrayList<int[]> mRLimits;
  
  String[] mRemainingArgs;
  
  int mRuntimeFlags;
  
  String mSeInfo;
  
  private boolean mSeInfoSpecified;
  
  boolean mStartChildZygote;
  
  int mTargetSdkVersion;
  
  private boolean mTargetSdkVersionSpecified;
  
  boolean mUidSpecified;
  
  boolean mUsapPoolEnabled;
  
  String[] mWhitelistedDataInfoList;
  
  ZygoteArguments(String[] paramArrayOfString) throws IllegalArgumentException {
    parseArgs(paramArrayOfString);
  }
  
  private void parseArgs(String[] paramArrayOfString) throws IllegalArgumentException {
    // Byte code:
    //   0: iconst_0
    //   1: istore_2
    //   2: iconst_0
    //   3: istore_3
    //   4: iconst_1
    //   5: istore #4
    //   7: aload_1
    //   8: arraylength
    //   9: istore #5
    //   11: iconst_0
    //   12: istore #6
    //   14: iload_2
    //   15: istore #7
    //   17: iload_2
    //   18: iload #5
    //   20: if_icmpge -> 1486
    //   23: aload_1
    //   24: iload_2
    //   25: aaload
    //   26: astore #8
    //   28: aload #8
    //   30: ldc '--'
    //   32: invokevirtual equals : (Ljava/lang/Object;)Z
    //   35: ifeq -> 46
    //   38: iload_2
    //   39: iconst_1
    //   40: iadd
    //   41: istore #7
    //   43: goto -> 1486
    //   46: aload #8
    //   48: ldc '--setuid='
    //   50: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   53: ifeq -> 93
    //   56: aload_0
    //   57: getfield mUidSpecified : Z
    //   60: ifne -> 83
    //   63: aload_0
    //   64: iconst_1
    //   65: putfield mUidSpecified : Z
    //   68: aload_0
    //   69: aload #8
    //   71: invokestatic getAssignmentValue : (Ljava/lang/String;)Ljava/lang/String;
    //   74: invokestatic parseInt : (Ljava/lang/String;)I
    //   77: putfield mUid : I
    //   80: goto -> 1480
    //   83: new java/lang/IllegalArgumentException
    //   86: dup
    //   87: ldc 'Duplicate arg specified'
    //   89: invokespecial <init> : (Ljava/lang/String;)V
    //   92: athrow
    //   93: aload #8
    //   95: ldc '--setgid='
    //   97: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   100: ifeq -> 140
    //   103: aload_0
    //   104: getfield mGidSpecified : Z
    //   107: ifne -> 130
    //   110: aload_0
    //   111: iconst_1
    //   112: putfield mGidSpecified : Z
    //   115: aload_0
    //   116: aload #8
    //   118: invokestatic getAssignmentValue : (Ljava/lang/String;)Ljava/lang/String;
    //   121: invokestatic parseInt : (Ljava/lang/String;)I
    //   124: putfield mGid : I
    //   127: goto -> 1480
    //   130: new java/lang/IllegalArgumentException
    //   133: dup
    //   134: ldc 'Duplicate arg specified'
    //   136: invokespecial <init> : (Ljava/lang/String;)V
    //   139: athrow
    //   140: aload #8
    //   142: ldc '--target-sdk-version='
    //   144: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   147: ifeq -> 187
    //   150: aload_0
    //   151: getfield mTargetSdkVersionSpecified : Z
    //   154: ifne -> 177
    //   157: aload_0
    //   158: iconst_1
    //   159: putfield mTargetSdkVersionSpecified : Z
    //   162: aload_0
    //   163: aload #8
    //   165: invokestatic getAssignmentValue : (Ljava/lang/String;)Ljava/lang/String;
    //   168: invokestatic parseInt : (Ljava/lang/String;)I
    //   171: putfield mTargetSdkVersion : I
    //   174: goto -> 1480
    //   177: new java/lang/IllegalArgumentException
    //   180: dup
    //   181: ldc 'Duplicate target-sdk-version specified'
    //   183: invokespecial <init> : (Ljava/lang/String;)V
    //   186: athrow
    //   187: aload #8
    //   189: ldc '--runtime-args'
    //   191: invokevirtual equals : (Ljava/lang/Object;)Z
    //   194: ifeq -> 202
    //   197: iconst_1
    //   198: istore_3
    //   199: goto -> 1480
    //   202: aload #8
    //   204: ldc '--runtime-flags='
    //   206: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   209: ifeq -> 227
    //   212: aload_0
    //   213: aload #8
    //   215: invokestatic getAssignmentValue : (Ljava/lang/String;)Ljava/lang/String;
    //   218: invokestatic parseInt : (Ljava/lang/String;)I
    //   221: putfield mRuntimeFlags : I
    //   224: goto -> 1480
    //   227: aload #8
    //   229: ldc '--seinfo='
    //   231: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   234: ifeq -> 271
    //   237: aload_0
    //   238: getfield mSeInfoSpecified : Z
    //   241: ifne -> 261
    //   244: aload_0
    //   245: iconst_1
    //   246: putfield mSeInfoSpecified : Z
    //   249: aload_0
    //   250: aload #8
    //   252: invokestatic getAssignmentValue : (Ljava/lang/String;)Ljava/lang/String;
    //   255: putfield mSeInfo : Ljava/lang/String;
    //   258: goto -> 1480
    //   261: new java/lang/IllegalArgumentException
    //   264: dup
    //   265: ldc 'Duplicate arg specified'
    //   267: invokespecial <init> : (Ljava/lang/String;)V
    //   270: athrow
    //   271: aload #8
    //   273: ldc '--capabilities='
    //   275: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   278: ifeq -> 385
    //   281: aload_0
    //   282: getfield mCapabilitiesSpecified : Z
    //   285: ifne -> 375
    //   288: aload_0
    //   289: iconst_1
    //   290: putfield mCapabilitiesSpecified : Z
    //   293: aload #8
    //   295: invokestatic getAssignmentValue : (Ljava/lang/String;)Ljava/lang/String;
    //   298: astore #8
    //   300: aload #8
    //   302: ldc ','
    //   304: iconst_2
    //   305: invokevirtual split : (Ljava/lang/String;I)[Ljava/lang/String;
    //   308: astore #8
    //   310: aload #8
    //   312: arraylength
    //   313: iconst_1
    //   314: if_icmpne -> 344
    //   317: aload #8
    //   319: iconst_0
    //   320: aaload
    //   321: invokestatic decode : (Ljava/lang/String;)Ljava/lang/Long;
    //   324: invokevirtual longValue : ()J
    //   327: lstore #9
    //   329: aload_0
    //   330: lload #9
    //   332: putfield mEffectiveCapabilities : J
    //   335: aload_0
    //   336: lload #9
    //   338: putfield mPermittedCapabilities : J
    //   341: goto -> 372
    //   344: aload_0
    //   345: aload #8
    //   347: iconst_0
    //   348: aaload
    //   349: invokestatic decode : (Ljava/lang/String;)Ljava/lang/Long;
    //   352: invokevirtual longValue : ()J
    //   355: putfield mPermittedCapabilities : J
    //   358: aload_0
    //   359: aload #8
    //   361: iconst_1
    //   362: aaload
    //   363: invokestatic decode : (Ljava/lang/String;)Ljava/lang/Long;
    //   366: invokevirtual longValue : ()J
    //   369: putfield mEffectiveCapabilities : J
    //   372: goto -> 1480
    //   375: new java/lang/IllegalArgumentException
    //   378: dup
    //   379: ldc 'Duplicate arg specified'
    //   381: invokespecial <init> : (Ljava/lang/String;)V
    //   384: athrow
    //   385: aload #8
    //   387: ldc '--rlimit='
    //   389: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   392: ifeq -> 487
    //   395: aload #8
    //   397: invokestatic getAssignmentList : (Ljava/lang/String;)[Ljava/lang/String;
    //   400: astore #11
    //   402: aload #11
    //   404: arraylength
    //   405: iconst_3
    //   406: if_icmpne -> 477
    //   409: aload #11
    //   411: arraylength
    //   412: newarray int
    //   414: astore #8
    //   416: iconst_0
    //   417: istore #7
    //   419: iload #7
    //   421: aload #11
    //   423: arraylength
    //   424: if_icmpge -> 446
    //   427: aload #8
    //   429: iload #7
    //   431: aload #11
    //   433: iload #7
    //   435: aaload
    //   436: invokestatic parseInt : (Ljava/lang/String;)I
    //   439: iastore
    //   440: iinc #7, 1
    //   443: goto -> 419
    //   446: aload_0
    //   447: getfield mRLimits : Ljava/util/ArrayList;
    //   450: ifnonnull -> 464
    //   453: aload_0
    //   454: new java/util/ArrayList
    //   457: dup
    //   458: invokespecial <init> : ()V
    //   461: putfield mRLimits : Ljava/util/ArrayList;
    //   464: aload_0
    //   465: getfield mRLimits : Ljava/util/ArrayList;
    //   468: aload #8
    //   470: invokevirtual add : (Ljava/lang/Object;)Z
    //   473: pop
    //   474: goto -> 1480
    //   477: new java/lang/IllegalArgumentException
    //   480: dup
    //   481: ldc '--rlimit= should have 3 comma-delimited ints'
    //   483: invokespecial <init> : (Ljava/lang/String;)V
    //   486: athrow
    //   487: aload #8
    //   489: ldc '--setgroups='
    //   491: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   494: ifeq -> 566
    //   497: aload_0
    //   498: getfield mGids : [I
    //   501: ifnonnull -> 556
    //   504: aload #8
    //   506: invokestatic getAssignmentList : (Ljava/lang/String;)[Ljava/lang/String;
    //   509: astore #8
    //   511: aload_0
    //   512: aload #8
    //   514: arraylength
    //   515: newarray int
    //   517: putfield mGids : [I
    //   520: aload #8
    //   522: arraylength
    //   523: iconst_1
    //   524: isub
    //   525: istore #7
    //   527: iload #7
    //   529: iflt -> 553
    //   532: aload_0
    //   533: getfield mGids : [I
    //   536: iload #7
    //   538: aload #8
    //   540: iload #7
    //   542: aaload
    //   543: invokestatic parseInt : (Ljava/lang/String;)I
    //   546: iastore
    //   547: iinc #7, -1
    //   550: goto -> 527
    //   553: goto -> 1480
    //   556: new java/lang/IllegalArgumentException
    //   559: dup
    //   560: ldc 'Duplicate arg specified'
    //   562: invokespecial <init> : (Ljava/lang/String;)V
    //   565: athrow
    //   566: aload #8
    //   568: ldc '--invoke-with'
    //   570: invokevirtual equals : (Ljava/lang/Object;)Z
    //   573: ifeq -> 617
    //   576: aload_0
    //   577: getfield mInvokeWith : Ljava/lang/String;
    //   580: ifnonnull -> 607
    //   583: iinc #2, 1
    //   586: aload_0
    //   587: aload_1
    //   588: iload_2
    //   589: aaload
    //   590: putfield mInvokeWith : Ljava/lang/String;
    //   593: goto -> 1480
    //   596: astore_1
    //   597: new java/lang/IllegalArgumentException
    //   600: dup
    //   601: ldc '--invoke-with requires argument'
    //   603: invokespecial <init> : (Ljava/lang/String;)V
    //   606: athrow
    //   607: new java/lang/IllegalArgumentException
    //   610: dup
    //   611: ldc 'Duplicate arg specified'
    //   613: invokespecial <init> : (Ljava/lang/String;)V
    //   616: athrow
    //   617: aload #8
    //   619: ldc '--nice-name='
    //   621: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   624: ifeq -> 656
    //   627: aload_0
    //   628: getfield mNiceName : Ljava/lang/String;
    //   631: ifnonnull -> 646
    //   634: aload_0
    //   635: aload #8
    //   637: invokestatic getAssignmentValue : (Ljava/lang/String;)Ljava/lang/String;
    //   640: putfield mNiceName : Ljava/lang/String;
    //   643: goto -> 1480
    //   646: new java/lang/IllegalArgumentException
    //   649: dup
    //   650: ldc 'Duplicate arg specified'
    //   652: invokespecial <init> : (Ljava/lang/String;)V
    //   655: athrow
    //   656: aload #8
    //   658: ldc '--mount-external-default'
    //   660: invokevirtual equals : (Ljava/lang/Object;)Z
    //   663: ifeq -> 674
    //   666: aload_0
    //   667: iconst_1
    //   668: putfield mMountExternal : I
    //   671: goto -> 1480
    //   674: aload #8
    //   676: ldc '--mount-external-read'
    //   678: invokevirtual equals : (Ljava/lang/Object;)Z
    //   681: ifeq -> 692
    //   684: aload_0
    //   685: iconst_2
    //   686: putfield mMountExternal : I
    //   689: goto -> 1480
    //   692: aload #8
    //   694: ldc '--mount-external-write'
    //   696: invokevirtual equals : (Ljava/lang/Object;)Z
    //   699: ifeq -> 710
    //   702: aload_0
    //   703: iconst_3
    //   704: putfield mMountExternal : I
    //   707: goto -> 1480
    //   710: aload #8
    //   712: ldc '--mount-external-full'
    //   714: invokevirtual equals : (Ljava/lang/Object;)Z
    //   717: ifeq -> 729
    //   720: aload_0
    //   721: bipush #6
    //   723: putfield mMountExternal : I
    //   726: goto -> 1480
    //   729: aload #8
    //   731: ldc '--mount-external-installer'
    //   733: invokevirtual equals : (Ljava/lang/Object;)Z
    //   736: ifeq -> 747
    //   739: aload_0
    //   740: iconst_5
    //   741: putfield mMountExternal : I
    //   744: goto -> 1480
    //   747: aload #8
    //   749: ldc '--mount-external-legacy'
    //   751: invokevirtual equals : (Ljava/lang/Object;)Z
    //   754: ifeq -> 765
    //   757: aload_0
    //   758: iconst_4
    //   759: putfield mMountExternal : I
    //   762: goto -> 1480
    //   765: aload #8
    //   767: ldc '--mount-external-pass-through'
    //   769: invokevirtual equals : (Ljava/lang/Object;)Z
    //   772: ifeq -> 784
    //   775: aload_0
    //   776: bipush #7
    //   778: putfield mMountExternal : I
    //   781: goto -> 1480
    //   784: aload #8
    //   786: ldc '--mount-external-android-writable'
    //   788: invokevirtual equals : (Ljava/lang/Object;)Z
    //   791: ifeq -> 803
    //   794: aload_0
    //   795: bipush #8
    //   797: putfield mMountExternal : I
    //   800: goto -> 1480
    //   803: aload #8
    //   805: ldc '--mount-external-oppo-android-writable'
    //   807: invokevirtual equals : (Ljava/lang/Object;)Z
    //   810: ifeq -> 822
    //   813: aload_0
    //   814: bipush #9
    //   816: putfield mMountExternal : I
    //   819: goto -> 1480
    //   822: aload #8
    //   824: ldc '--query-abi-list'
    //   826: invokevirtual equals : (Ljava/lang/Object;)Z
    //   829: ifeq -> 840
    //   832: aload_0
    //   833: iconst_1
    //   834: putfield mAbiListQuery : Z
    //   837: goto -> 1480
    //   840: aload #8
    //   842: ldc '--get-pid'
    //   844: invokevirtual equals : (Ljava/lang/Object;)Z
    //   847: ifeq -> 858
    //   850: aload_0
    //   851: iconst_1
    //   852: putfield mPidQuery : Z
    //   855: goto -> 1480
    //   858: aload #8
    //   860: ldc '--boot-completed'
    //   862: invokevirtual equals : (Ljava/lang/Object;)Z
    //   865: ifeq -> 876
    //   868: aload_0
    //   869: iconst_1
    //   870: putfield mBootCompleted : Z
    //   873: goto -> 1480
    //   876: aload #8
    //   878: ldc '--instruction-set='
    //   880: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   883: ifeq -> 898
    //   886: aload_0
    //   887: aload #8
    //   889: invokestatic getAssignmentValue : (Ljava/lang/String;)Ljava/lang/String;
    //   892: putfield mInstructionSet : Ljava/lang/String;
    //   895: goto -> 1480
    //   898: aload #8
    //   900: ldc '--app-data-dir='
    //   902: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   905: ifeq -> 920
    //   908: aload_0
    //   909: aload #8
    //   911: invokestatic getAssignmentValue : (Ljava/lang/String;)Ljava/lang/String;
    //   914: putfield mAppDataDir : Ljava/lang/String;
    //   917: goto -> 1480
    //   920: aload #8
    //   922: ldc '--preload-app'
    //   924: invokevirtual equals : (Ljava/lang/Object;)Z
    //   927: ifeq -> 943
    //   930: iinc #2, 1
    //   933: aload_0
    //   934: aload_1
    //   935: iload_2
    //   936: aaload
    //   937: putfield mPreloadApp : Ljava/lang/String;
    //   940: goto -> 1480
    //   943: aload #8
    //   945: ldc '--preload-package'
    //   947: invokevirtual equals : (Ljava/lang/Object;)Z
    //   950: ifeq -> 996
    //   953: iinc #2, 1
    //   956: aload_0
    //   957: aload_1
    //   958: iload_2
    //   959: aaload
    //   960: putfield mPreloadPackage : Ljava/lang/String;
    //   963: iinc #2, 1
    //   966: aload_0
    //   967: aload_1
    //   968: iload_2
    //   969: aaload
    //   970: putfield mPreloadPackageLibs : Ljava/lang/String;
    //   973: iinc #2, 1
    //   976: aload_0
    //   977: aload_1
    //   978: iload_2
    //   979: aaload
    //   980: putfield mPreloadPackageLibFileName : Ljava/lang/String;
    //   983: iinc #2, 1
    //   986: aload_0
    //   987: aload_1
    //   988: iload_2
    //   989: aaload
    //   990: putfield mPreloadPackageCacheKey : Ljava/lang/String;
    //   993: goto -> 1480
    //   996: aload #8
    //   998: ldc_w '--preload-default'
    //   1001: invokevirtual equals : (Ljava/lang/Object;)Z
    //   1004: ifeq -> 1018
    //   1007: aload_0
    //   1008: iconst_1
    //   1009: putfield mPreloadDefault : Z
    //   1012: iconst_0
    //   1013: istore #4
    //   1015: goto -> 1480
    //   1018: aload #8
    //   1020: ldc_w '--start-child-zygote'
    //   1023: invokevirtual equals : (Ljava/lang/Object;)Z
    //   1026: ifeq -> 1037
    //   1029: aload_0
    //   1030: iconst_1
    //   1031: putfield mStartChildZygote : Z
    //   1034: goto -> 1480
    //   1037: aload #8
    //   1039: ldc_w '--set-api-blacklist-exemptions'
    //   1042: invokevirtual equals : (Ljava/lang/Object;)Z
    //   1045: ifeq -> 1073
    //   1048: aload_0
    //   1049: aload_1
    //   1050: iload_2
    //   1051: iconst_1
    //   1052: iadd
    //   1053: aload_1
    //   1054: arraylength
    //   1055: invokestatic copyOfRange : ([Ljava/lang/Object;II)[Ljava/lang/Object;
    //   1058: checkcast [Ljava/lang/String;
    //   1061: putfield mApiBlacklistExemptions : [Ljava/lang/String;
    //   1064: aload_1
    //   1065: arraylength
    //   1066: istore_2
    //   1067: iconst_0
    //   1068: istore #4
    //   1070: goto -> 1480
    //   1073: aload #8
    //   1075: ldc_w '--hidden-api-log-sampling-rate='
    //   1078: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   1081: ifeq -> 1147
    //   1084: aload #8
    //   1086: invokestatic getAssignmentValue : (Ljava/lang/String;)Ljava/lang/String;
    //   1089: astore #8
    //   1091: aload_0
    //   1092: aload #8
    //   1094: invokestatic parseInt : (Ljava/lang/String;)I
    //   1097: putfield mHiddenApiAccessLogSampleRate : I
    //   1100: iconst_0
    //   1101: istore #4
    //   1103: goto -> 1480
    //   1106: astore_1
    //   1107: new java/lang/StringBuilder
    //   1110: dup
    //   1111: invokespecial <init> : ()V
    //   1114: astore #11
    //   1116: aload #11
    //   1118: ldc_w 'Invalid log sampling rate: '
    //   1121: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1124: pop
    //   1125: aload #11
    //   1127: aload #8
    //   1129: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1132: pop
    //   1133: new java/lang/IllegalArgumentException
    //   1136: dup
    //   1137: aload #11
    //   1139: invokevirtual toString : ()Ljava/lang/String;
    //   1142: aload_1
    //   1143: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   1146: athrow
    //   1147: aload #8
    //   1149: ldc_w '--hidden-api-statslog-sampling-rate='
    //   1152: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   1155: ifeq -> 1221
    //   1158: aload #8
    //   1160: invokestatic getAssignmentValue : (Ljava/lang/String;)Ljava/lang/String;
    //   1163: astore #8
    //   1165: aload_0
    //   1166: aload #8
    //   1168: invokestatic parseInt : (Ljava/lang/String;)I
    //   1171: putfield mHiddenApiAccessStatslogSampleRate : I
    //   1174: iconst_0
    //   1175: istore #4
    //   1177: goto -> 1480
    //   1180: astore_1
    //   1181: new java/lang/StringBuilder
    //   1184: dup
    //   1185: invokespecial <init> : ()V
    //   1188: astore #11
    //   1190: aload #11
    //   1192: ldc_w 'Invalid statslog sampling rate: '
    //   1195: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1198: pop
    //   1199: aload #11
    //   1201: aload #8
    //   1203: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1206: pop
    //   1207: new java/lang/IllegalArgumentException
    //   1210: dup
    //   1211: aload #11
    //   1213: invokevirtual toString : ()Ljava/lang/String;
    //   1216: aload_1
    //   1217: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   1220: athrow
    //   1221: aload #8
    //   1223: ldc_w '--package-name='
    //   1226: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   1229: ifeq -> 1261
    //   1232: aload_0
    //   1233: getfield mPackageName : Ljava/lang/String;
    //   1236: ifnonnull -> 1251
    //   1239: aload_0
    //   1240: aload #8
    //   1242: invokestatic getAssignmentValue : (Ljava/lang/String;)Ljava/lang/String;
    //   1245: putfield mPackageName : Ljava/lang/String;
    //   1248: goto -> 1480
    //   1251: new java/lang/IllegalArgumentException
    //   1254: dup
    //   1255: ldc 'Duplicate arg specified'
    //   1257: invokespecial <init> : (Ljava/lang/String;)V
    //   1260: athrow
    //   1261: aload #8
    //   1263: ldc_w '--usap-pool-enabled='
    //   1266: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   1269: ifeq -> 1295
    //   1272: aload_0
    //   1273: iconst_1
    //   1274: putfield mUsapPoolStatusSpecified : Z
    //   1277: aload_0
    //   1278: aload #8
    //   1280: invokestatic getAssignmentValue : (Ljava/lang/String;)Ljava/lang/String;
    //   1283: invokestatic parseBoolean : (Ljava/lang/String;)Z
    //   1286: putfield mUsapPoolEnabled : Z
    //   1289: iconst_0
    //   1290: istore #4
    //   1292: goto -> 1480
    //   1295: aload #8
    //   1297: ldc_w '--is-top-app'
    //   1300: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   1303: ifeq -> 1314
    //   1306: aload_0
    //   1307: iconst_1
    //   1308: putfield mIsTopApp : Z
    //   1311: goto -> 1480
    //   1314: aload #8
    //   1316: ldc_w '--disabled-compat-changes='
    //   1319: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   1322: ifeq -> 1396
    //   1325: aload_0
    //   1326: getfield mDisabledCompatChanges : [J
    //   1329: ifnonnull -> 1386
    //   1332: aload #8
    //   1334: invokestatic getAssignmentList : (Ljava/lang/String;)[Ljava/lang/String;
    //   1337: astore #8
    //   1339: aload #8
    //   1341: arraylength
    //   1342: istore #6
    //   1344: aload_0
    //   1345: iload #6
    //   1347: newarray long
    //   1349: putfield mDisabledCompatChanges : [J
    //   1352: iconst_0
    //   1353: istore #7
    //   1355: iload #7
    //   1357: iload #6
    //   1359: if_icmpge -> 1383
    //   1362: aload_0
    //   1363: getfield mDisabledCompatChanges : [J
    //   1366: iload #7
    //   1368: aload #8
    //   1370: iload #7
    //   1372: aaload
    //   1373: invokestatic parseLong : (Ljava/lang/String;)J
    //   1376: lastore
    //   1377: iinc #7, 1
    //   1380: goto -> 1355
    //   1383: goto -> 1480
    //   1386: new java/lang/IllegalArgumentException
    //   1389: dup
    //   1390: ldc 'Duplicate arg specified'
    //   1392: invokespecial <init> : (Ljava/lang/String;)V
    //   1395: athrow
    //   1396: aload #8
    //   1398: ldc_w '--pkg-data-info-map'
    //   1401: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   1404: ifeq -> 1419
    //   1407: aload_0
    //   1408: aload #8
    //   1410: invokestatic getAssignmentList : (Ljava/lang/String;)[Ljava/lang/String;
    //   1413: putfield mPkgDataInfoList : [Ljava/lang/String;
    //   1416: goto -> 1480
    //   1419: aload #8
    //   1421: ldc_w '--whitelisted-data-info-map'
    //   1424: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   1427: ifeq -> 1442
    //   1430: aload_0
    //   1431: aload #8
    //   1433: invokestatic getAssignmentList : (Ljava/lang/String;)[Ljava/lang/String;
    //   1436: putfield mWhitelistedDataInfoList : [Ljava/lang/String;
    //   1439: goto -> 1480
    //   1442: aload #8
    //   1444: ldc_w '--bind-mount-storage-dirs'
    //   1447: invokevirtual equals : (Ljava/lang/Object;)Z
    //   1450: ifeq -> 1461
    //   1453: aload_0
    //   1454: iconst_1
    //   1455: putfield mBindMountAppStorageDirs : Z
    //   1458: goto -> 1480
    //   1461: iload_2
    //   1462: istore #7
    //   1464: aload #8
    //   1466: ldc_w '--bind-mount-data-dirs'
    //   1469: invokevirtual equals : (Ljava/lang/Object;)Z
    //   1472: ifeq -> 1486
    //   1475: aload_0
    //   1476: iconst_1
    //   1477: putfield mBindMountAppDataDirs : Z
    //   1480: iinc #2, 1
    //   1483: goto -> 7
    //   1486: aload_0
    //   1487: getfield mBootCompleted : Z
    //   1490: ifeq -> 1515
    //   1493: aload_1
    //   1494: arraylength
    //   1495: iload #7
    //   1497: isub
    //   1498: ifgt -> 1504
    //   1501: goto -> 1679
    //   1504: new java/lang/IllegalArgumentException
    //   1507: dup
    //   1508: ldc_w 'Unexpected arguments after --boot-completed'
    //   1511: invokespecial <init> : (Ljava/lang/String;)V
    //   1514: athrow
    //   1515: aload_0
    //   1516: getfield mAbiListQuery : Z
    //   1519: ifne -> 1671
    //   1522: aload_0
    //   1523: getfield mPidQuery : Z
    //   1526: ifeq -> 1532
    //   1529: goto -> 1671
    //   1532: aload_0
    //   1533: getfield mPreloadPackage : Ljava/lang/String;
    //   1536: ifnull -> 1561
    //   1539: aload_1
    //   1540: arraylength
    //   1541: iload #7
    //   1543: isub
    //   1544: ifgt -> 1550
    //   1547: goto -> 1679
    //   1550: new java/lang/IllegalArgumentException
    //   1553: dup
    //   1554: ldc_w 'Unexpected arguments after --preload-package.'
    //   1557: invokespecial <init> : (Ljava/lang/String;)V
    //   1560: athrow
    //   1561: aload_0
    //   1562: getfield mPreloadApp : Ljava/lang/String;
    //   1565: ifnull -> 1590
    //   1568: aload_1
    //   1569: arraylength
    //   1570: iload #7
    //   1572: isub
    //   1573: ifgt -> 1579
    //   1576: goto -> 1679
    //   1579: new java/lang/IllegalArgumentException
    //   1582: dup
    //   1583: ldc_w 'Unexpected arguments after --preload-app.'
    //   1586: invokespecial <init> : (Ljava/lang/String;)V
    //   1589: athrow
    //   1590: iload #4
    //   1592: ifeq -> 1679
    //   1595: iload_3
    //   1596: ifeq -> 1630
    //   1599: aload_1
    //   1600: arraylength
    //   1601: iload #7
    //   1603: isub
    //   1604: anewarray java/lang/String
    //   1607: astore #8
    //   1609: aload_0
    //   1610: aload #8
    //   1612: putfield mRemainingArgs : [Ljava/lang/String;
    //   1615: aload_1
    //   1616: iload #7
    //   1618: aload #8
    //   1620: iconst_0
    //   1621: aload #8
    //   1623: arraylength
    //   1624: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   1627: goto -> 1679
    //   1630: new java/lang/StringBuilder
    //   1633: dup
    //   1634: invokespecial <init> : ()V
    //   1637: astore #8
    //   1639: aload #8
    //   1641: ldc_w 'Unexpected argument : '
    //   1644: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1647: pop
    //   1648: aload #8
    //   1650: aload_1
    //   1651: iload #7
    //   1653: aaload
    //   1654: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1657: pop
    //   1658: new java/lang/IllegalArgumentException
    //   1661: dup
    //   1662: aload #8
    //   1664: invokevirtual toString : ()Ljava/lang/String;
    //   1667: invokespecial <init> : (Ljava/lang/String;)V
    //   1670: athrow
    //   1671: aload_1
    //   1672: arraylength
    //   1673: iload #7
    //   1675: isub
    //   1676: ifgt -> 1758
    //   1679: aload_0
    //   1680: getfield mStartChildZygote : Z
    //   1683: ifeq -> 1757
    //   1686: iconst_0
    //   1687: istore_3
    //   1688: aload_0
    //   1689: getfield mRemainingArgs : [Ljava/lang/String;
    //   1692: astore #8
    //   1694: aload #8
    //   1696: arraylength
    //   1697: istore #7
    //   1699: iload #6
    //   1701: istore #4
    //   1703: iload_3
    //   1704: istore_2
    //   1705: iload #4
    //   1707: iload #7
    //   1709: if_icmpge -> 1739
    //   1712: aload #8
    //   1714: iload #4
    //   1716: aaload
    //   1717: astore_1
    //   1718: aload_1
    //   1719: ldc_w '--zygote-socket='
    //   1722: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   1725: ifeq -> 1733
    //   1728: iconst_1
    //   1729: istore_2
    //   1730: goto -> 1739
    //   1733: iinc #4, 1
    //   1736: goto -> 1703
    //   1739: iload_2
    //   1740: ifeq -> 1746
    //   1743: goto -> 1757
    //   1746: new java/lang/IllegalArgumentException
    //   1749: dup
    //   1750: ldc_w '--start-child-zygote specified without --zygote-socket='
    //   1753: invokespecial <init> : (Ljava/lang/String;)V
    //   1756: athrow
    //   1757: return
    //   1758: new java/lang/IllegalArgumentException
    //   1761: dup
    //   1762: ldc_w 'Unexpected arguments after --query-abi-list.'
    //   1765: invokespecial <init> : (Ljava/lang/String;)V
    //   1768: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #272	-> 0
    //   #274	-> 2
    //   #276	-> 4
    //   #277	-> 7
    //   #278	-> 23
    //   #280	-> 28
    //   #281	-> 38
    //   #282	-> 43
    //   #283	-> 46
    //   #284	-> 56
    //   #288	-> 63
    //   #289	-> 68
    //   #285	-> 83
    //   #290	-> 93
    //   #291	-> 103
    //   #295	-> 110
    //   #296	-> 115
    //   #292	-> 130
    //   #297	-> 140
    //   #298	-> 150
    //   #302	-> 157
    //   #303	-> 162
    //   #299	-> 177
    //   #304	-> 187
    //   #305	-> 197
    //   #306	-> 202
    //   #307	-> 212
    //   #308	-> 227
    //   #309	-> 237
    //   #313	-> 244
    //   #314	-> 249
    //   #310	-> 261
    //   #315	-> 271
    //   #316	-> 281
    //   #320	-> 288
    //   #321	-> 293
    //   #323	-> 300
    //   #325	-> 310
    //   #326	-> 317
    //   #327	-> 335
    //   #329	-> 344
    //   #330	-> 358
    //   #332	-> 372
    //   #317	-> 375
    //   #332	-> 385
    //   #334	-> 395
    //   #336	-> 402
    //   #340	-> 409
    //   #342	-> 416
    //   #343	-> 427
    //   #342	-> 440
    //   #346	-> 446
    //   #347	-> 453
    //   #350	-> 464
    //   #351	-> 474
    //   #337	-> 477
    //   #351	-> 487
    //   #352	-> 497
    //   #357	-> 504
    //   #359	-> 511
    //   #361	-> 520
    //   #362	-> 532
    //   #361	-> 547
    //   #364	-> 553
    //   #353	-> 556
    //   #364	-> 566
    //   #365	-> 576
    //   #370	-> 583
    //   #374	-> 593
    //   #371	-> 596
    //   #372	-> 597
    //   #366	-> 607
    //   #375	-> 617
    //   #376	-> 627
    //   #380	-> 634
    //   #377	-> 646
    //   #381	-> 656
    //   #382	-> 666
    //   #383	-> 674
    //   #384	-> 684
    //   #385	-> 692
    //   #386	-> 702
    //   #387	-> 710
    //   #388	-> 720
    //   #389	-> 729
    //   #390	-> 739
    //   #391	-> 747
    //   #392	-> 757
    //   #393	-> 765
    //   #394	-> 775
    //   #395	-> 784
    //   #396	-> 794
    //   #399	-> 803
    //   #400	-> 813
    //   #402	-> 822
    //   #403	-> 832
    //   #404	-> 840
    //   #405	-> 850
    //   #406	-> 858
    //   #407	-> 868
    //   #408	-> 876
    //   #409	-> 886
    //   #410	-> 898
    //   #411	-> 908
    //   #412	-> 920
    //   #413	-> 930
    //   #414	-> 943
    //   #415	-> 953
    //   #416	-> 963
    //   #417	-> 973
    //   #418	-> 983
    //   #419	-> 996
    //   #420	-> 1007
    //   #421	-> 1012
    //   #422	-> 1018
    //   #423	-> 1029
    //   #424	-> 1037
    //   #427	-> 1048
    //   #428	-> 1064
    //   #429	-> 1067
    //   #430	-> 1073
    //   #431	-> 1084
    //   #433	-> 1091
    //   #437	-> 1100
    //   #438	-> 1100
    //   #439	-> 1103
    //   #434	-> 1106
    //   #435	-> 1107
    //   #439	-> 1147
    //   #440	-> 1158
    //   #442	-> 1165
    //   #446	-> 1174
    //   #447	-> 1174
    //   #448	-> 1177
    //   #443	-> 1180
    //   #444	-> 1181
    //   #448	-> 1221
    //   #449	-> 1232
    //   #452	-> 1239
    //   #450	-> 1251
    //   #453	-> 1261
    //   #454	-> 1272
    //   #455	-> 1277
    //   #456	-> 1289
    //   #457	-> 1295
    //   #458	-> 1306
    //   #459	-> 1314
    //   #460	-> 1325
    //   #463	-> 1332
    //   #464	-> 1339
    //   #465	-> 1344
    //   #466	-> 1352
    //   #467	-> 1362
    //   #466	-> 1377
    //   #469	-> 1383
    //   #461	-> 1386
    //   #469	-> 1396
    //   #470	-> 1407
    //   #471	-> 1419
    //   #472	-> 1430
    //   #473	-> 1442
    //   #474	-> 1453
    //   #475	-> 1461
    //   #476	-> 1475
    //   #277	-> 1480
    //   #482	-> 1486
    //   #483	-> 1493
    //   #484	-> 1504
    //   #486	-> 1515
    //   #490	-> 1532
    //   #491	-> 1539
    //   #492	-> 1550
    //   #495	-> 1561
    //   #496	-> 1568
    //   #497	-> 1579
    //   #500	-> 1590
    //   #501	-> 1595
    //   #505	-> 1599
    //   #506	-> 1615
    //   #502	-> 1630
    //   #487	-> 1671
    //   #509	-> 1679
    //   #510	-> 1686
    //   #511	-> 1688
    //   #512	-> 1718
    //   #513	-> 1728
    //   #514	-> 1730
    //   #511	-> 1733
    //   #517	-> 1739
    //   #518	-> 1746
    //   #522	-> 1757
    //   #488	-> 1758
    // Exception table:
    //   from	to	target	type
    //   586	593	596	java/lang/IndexOutOfBoundsException
    //   1091	1100	1106	java/lang/NumberFormatException
    //   1165	1174	1180	java/lang/NumberFormatException
  }
  
  private static String getAssignmentValue(String paramString) {
    return paramString.substring(paramString.indexOf('=') + 1);
  }
  
  private static String[] getAssignmentList(String paramString) {
    return getAssignmentValue(paramString).split(",");
  }
}
