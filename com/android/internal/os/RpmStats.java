package com.android.internal.os;

import android.util.ArrayMap;
import java.util.Map;

public final class RpmStats {
  public Map<String, PowerStatePlatformSleepState> mPlatformLowPowerStats = new ArrayMap<>();
  
  public Map<String, PowerStateSubsystem> mSubsystemLowPowerStats = new ArrayMap<>();
  
  public PowerStatePlatformSleepState getAndUpdatePlatformState(String paramString, long paramLong, int paramInt) {
    PowerStatePlatformSleepState powerStatePlatformSleepState1 = this.mPlatformLowPowerStats.get(paramString);
    PowerStatePlatformSleepState powerStatePlatformSleepState2 = powerStatePlatformSleepState1;
    if (powerStatePlatformSleepState1 == null) {
      powerStatePlatformSleepState2 = new PowerStatePlatformSleepState();
      this.mPlatformLowPowerStats.put(paramString, powerStatePlatformSleepState2);
    } 
    powerStatePlatformSleepState2.mTimeMs = paramLong;
    powerStatePlatformSleepState2.mCount = paramInt;
    return powerStatePlatformSleepState2;
  }
  
  public PowerStateSubsystem getSubsystem(String paramString) {
    PowerStateSubsystem powerStateSubsystem1 = this.mSubsystemLowPowerStats.get(paramString);
    PowerStateSubsystem powerStateSubsystem2 = powerStateSubsystem1;
    if (powerStateSubsystem1 == null) {
      powerStateSubsystem2 = new PowerStateSubsystem();
      this.mSubsystemLowPowerStats.put(paramString, powerStateSubsystem2);
    } 
    return powerStateSubsystem2;
  }
  
  public static class PowerStateElement {
    public int mCount;
    
    public long mTimeMs;
    
    private PowerStateElement(long param1Long, int param1Int) {
      this.mTimeMs = param1Long;
      this.mCount = param1Int;
    }
  }
  
  public static class PowerStatePlatformSleepState {
    public int mCount;
    
    public long mTimeMs;
    
    public Map<String, RpmStats.PowerStateElement> mVoters = new ArrayMap<>();
    
    public void putVoter(String param1String, long param1Long, int param1Int) {
      RpmStats.PowerStateElement powerStateElement = this.mVoters.get(param1String);
      if (powerStateElement == null) {
        this.mVoters.put(param1String, new RpmStats.PowerStateElement(param1Long, param1Int));
      } else {
        powerStateElement.mTimeMs = param1Long;
        powerStateElement.mCount = param1Int;
      } 
    }
  }
  
  public static class PowerStateSubsystem {
    public Map<String, RpmStats.PowerStateElement> mStates;
    
    public PowerStateSubsystem() {
      this.mStates = new ArrayMap<>();
    }
    
    public void putState(String param1String, long param1Long, int param1Int) {
      RpmStats.PowerStateElement powerStateElement = this.mStates.get(param1String);
      if (powerStateElement == null) {
        this.mStates.put(param1String, new RpmStats.PowerStateElement(param1Long, param1Int));
      } else {
        powerStateElement.mTimeMs = param1Long;
        powerStateElement.mCount = param1Int;
      } 
    }
  }
}
