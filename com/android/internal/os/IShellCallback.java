package com.android.internal.os;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

public interface IShellCallback extends IInterface {
  ParcelFileDescriptor openFile(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  class Default implements IShellCallback {
    public ParcelFileDescriptor openFile(String param1String1, String param1String2, String param1String3) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IShellCallback {
    private static final String DESCRIPTOR = "com.android.internal.os.IShellCallback";
    
    static final int TRANSACTION_openFile = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.os.IShellCallback");
    }
    
    public static IShellCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.os.IShellCallback");
      if (iInterface != null && iInterface instanceof IShellCallback)
        return (IShellCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "openFile";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.os.IShellCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.os.IShellCallback");
      String str2 = param1Parcel1.readString();
      String str3 = param1Parcel1.readString();
      String str1 = param1Parcel1.readString();
      ParcelFileDescriptor parcelFileDescriptor = openFile(str2, str3, str1);
      param1Parcel2.writeNoException();
      if (parcelFileDescriptor != null) {
        param1Parcel2.writeInt(1);
        parcelFileDescriptor.writeToParcel(param1Parcel2, 1);
      } else {
        param1Parcel2.writeInt(0);
      } 
      return true;
    }
    
    private static class Proxy implements IShellCallback {
      public static IShellCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.os.IShellCallback";
      }
      
      public ParcelFileDescriptor openFile(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.os.IShellCallback");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IShellCallback.Stub.getDefaultImpl() != null)
            return IShellCallback.Stub.getDefaultImpl().openFile(param2String1, param2String2, param2String3); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (ParcelFileDescriptor)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IShellCallback param1IShellCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IShellCallback != null) {
          Proxy.sDefaultImpl = param1IShellCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IShellCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
