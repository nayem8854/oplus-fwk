package com.android.internal.os;

import android.content.Context;
import android.util.proto.ProtoOutputStream;
import java.util.HashMap;

public class PowerProfile extends OplusBasePowerProfile {
  private static final String ATTR_NAME = "name";
  
  private static final String CPU_CLUSTER_POWER_COUNT = "cpu.cluster_power.cluster";
  
  private static final String CPU_CORE_POWER_PREFIX = "cpu.core_power.cluster";
  
  private static final String CPU_CORE_SPEED_PREFIX = "cpu.core_speeds.cluster";
  
  private static final String CPU_PER_CLUSTER_CORE_COUNT = "cpu.clusters.cores";
  
  public static final String POWER_AMBIENT_DISPLAY = "ambient.on";
  
  public static final String POWER_AUDIO = "audio";
  
  public static final String POWER_BATTERY_CAPACITY = "battery.capacity";
  
  @Deprecated
  public static final String POWER_BLUETOOTH_ACTIVE = "bluetooth.active";
  
  @Deprecated
  public static final String POWER_BLUETOOTH_AT_CMD = "bluetooth.at";
  
  public static final String POWER_BLUETOOTH_CONTROLLER_IDLE = "bluetooth.controller.idle";
  
  public static final String POWER_BLUETOOTH_CONTROLLER_OPERATING_VOLTAGE = "bluetooth.controller.voltage";
  
  public static final String POWER_BLUETOOTH_CONTROLLER_RX = "bluetooth.controller.rx";
  
  public static final String POWER_BLUETOOTH_CONTROLLER_TX = "bluetooth.controller.tx";
  
  @Deprecated
  public static final String POWER_BLUETOOTH_ON = "bluetooth.on";
  
  public static final String POWER_CAMERA = "camera.avg";
  
  public static final String POWER_CPU_ACTIVE = "cpu.active";
  
  public static final String POWER_CPU_IDLE = "cpu.idle";
  
  public static final String POWER_CPU_SUSPEND = "cpu.suspend";
  
  public static final String POWER_FLASHLIGHT = "camera.flashlight";
  
  public static final String POWER_GPS_ON = "gps.on";
  
  public static final String POWER_GPS_OPERATING_VOLTAGE = "gps.voltage";
  
  public static final String POWER_GPS_SIGNAL_QUALITY_BASED = "gps.signalqualitybased";
  
  public static final String POWER_MEMORY = "memory.bandwidths";
  
  public static final String POWER_MODEM_CONTROLLER_IDLE = "modem.controller.idle";
  
  public static final String POWER_MODEM_CONTROLLER_OPERATING_VOLTAGE = "modem.controller.voltage";
  
  public static final String POWER_MODEM_CONTROLLER_RX = "modem.controller.rx";
  
  public static final String POWER_MODEM_CONTROLLER_SLEEP = "modem.controller.sleep";
  
  public static final String POWER_MODEM_CONTROLLER_TX = "modem.controller.tx";
  
  public static final String POWER_RADIO_ACTIVE = "radio.active";
  
  public static final String POWER_RADIO_ON = "radio.on";
  
  public static final String POWER_RADIO_SCANNING = "radio.scanning";
  
  public static final String POWER_SCREEN_FULL = "screen.full";
  
  public static final String POWER_SCREEN_ON = "screen.on";
  
  public static final String POWER_VIDEO = "video";
  
  public static final String POWER_WIFI_ACTIVE = "wifi.active";
  
  public static final String POWER_WIFI_BATCHED_SCAN = "wifi.batchedscan";
  
  public static final String POWER_WIFI_CONTROLLER_IDLE = "wifi.controller.idle";
  
  public static final String POWER_WIFI_CONTROLLER_OPERATING_VOLTAGE = "wifi.controller.voltage";
  
  public static final String POWER_WIFI_CONTROLLER_RX = "wifi.controller.rx";
  
  public static final String POWER_WIFI_CONTROLLER_TX = "wifi.controller.tx";
  
  public static final String POWER_WIFI_CONTROLLER_TX_LEVELS = "wifi.controller.tx_levels";
  
  public static final String POWER_WIFI_ON = "wifi.on";
  
  public static final String POWER_WIFI_SCAN = "wifi.scan";
  
  private static final String TAG_ARRAY = "array";
  
  private static final String TAG_ARRAYITEM = "value";
  
  private static final String TAG_DEVICE = "device";
  
  private static final String TAG_ITEM = "item";
  
  private static final Object sLock;
  
  static final HashMap<String, Double[]> sPowerArrayMap;
  
  static final HashMap<String, Double> sPowerItemMap = new HashMap<>();
  
  private CpuClusterKey[] mCpuClusters;
  
  static {
    sPowerArrayMap = (HashMap)new HashMap<>();
    sLock = new Object();
  }
  
  public PowerProfile(Context paramContext) {
    this(paramContext, false);
  }
  
  public PowerProfile(Context paramContext, boolean paramBoolean) {
    synchronized (sLock) {
      if (sPowerItemMap.size() == 0 && sPowerArrayMap.size() == 0)
        readPowerValuesFromXml(paramContext, paramBoolean); 
      initCpuClusters();
      return;
    } 
  }
  
  private void readPowerValuesFromXml(Context paramContext, boolean paramBoolean) {
    // Byte code:
    //   0: iload_2
    //   1: ifeq -> 10
    //   4: ldc 18284563
    //   6: istore_3
    //   7: goto -> 13
    //   10: ldc 18284562
    //   12: istore_3
    //   13: aload_0
    //   14: invokevirtual getOppoPowerProfileXmlParser : ()V
    //   17: aload_1
    //   18: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   21: astore #4
    //   23: aload_0
    //   24: getfield parser : Lorg/xmlpull/v1/XmlPullParser;
    //   27: ifnonnull -> 40
    //   30: aload_0
    //   31: aload #4
    //   33: iload_3
    //   34: invokevirtual getXml : (I)Landroid/content/res/XmlResourceParser;
    //   37: putfield parser : Lorg/xmlpull/v1/XmlPullParser;
    //   40: iconst_0
    //   41: istore #5
    //   43: new java/util/ArrayList
    //   46: dup
    //   47: invokespecial <init> : ()V
    //   50: astore #6
    //   52: aconst_null
    //   53: astore_1
    //   54: aload_0
    //   55: getfield parser : Lorg/xmlpull/v1/XmlPullParser;
    //   58: ldc 'device'
    //   60: invokestatic beginDocument : (Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V
    //   63: aload_0
    //   64: getfield parser : Lorg/xmlpull/v1/XmlPullParser;
    //   67: invokestatic nextElement : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   70: aload_0
    //   71: getfield parser : Lorg/xmlpull/v1/XmlPullParser;
    //   74: invokeinterface getName : ()Ljava/lang/String;
    //   79: astore #7
    //   81: aload #7
    //   83: ifnonnull -> 344
    //   86: iload #5
    //   88: ifeq -> 115
    //   91: getstatic com/android/internal/os/PowerProfile.sPowerArrayMap : Ljava/util/HashMap;
    //   94: aload_1
    //   95: aload #6
    //   97: aload #6
    //   99: invokevirtual size : ()I
    //   102: anewarray java/lang/Double
    //   105: invokevirtual toArray : ([Ljava/lang/Object;)[Ljava/lang/Object;
    //   108: checkcast [Ljava/lang/Double;
    //   111: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   114: pop
    //   115: aload_0
    //   116: getfield parser : Lorg/xmlpull/v1/XmlPullParser;
    //   119: instanceof android/content/res/XmlResourceParser
    //   122: ifeq -> 147
    //   125: ldc_w 'PowerProfile'
    //   128: ldc_w 'parse close here'
    //   131: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   134: pop
    //   135: aload_0
    //   136: getfield parser : Lorg/xmlpull/v1/XmlPullParser;
    //   139: checkcast android/content/res/XmlResourceParser
    //   142: invokeinterface close : ()V
    //   147: aload_0
    //   148: getfield fis : Ljava/io/FileInputStream;
    //   151: ifnull -> 208
    //   154: aload_0
    //   155: getfield fis : Ljava/io/FileInputStream;
    //   158: invokevirtual close : ()V
    //   161: aload_0
    //   162: aconst_null
    //   163: putfield fis : Ljava/io/FileInputStream;
    //   166: goto -> 208
    //   169: astore #8
    //   171: new java/lang/StringBuilder
    //   174: dup
    //   175: invokespecial <init> : ()V
    //   178: astore_1
    //   179: aload_1
    //   180: ldc_w 'access power profile exception caught : '
    //   183: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   186: pop
    //   187: aload_1
    //   188: aload #8
    //   190: invokevirtual getMessage : ()Ljava/lang/String;
    //   193: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   196: pop
    //   197: ldc_w 'PowerProfile'
    //   200: aload_1
    //   201: invokevirtual toString : ()Ljava/lang/String;
    //   204: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   207: pop
    //   208: iconst_4
    //   209: newarray int
    //   211: astore #8
    //   213: aload #8
    //   215: dup
    //   216: iconst_0
    //   217: ldc_w 17694746
    //   220: iastore
    //   221: dup
    //   222: iconst_1
    //   223: ldc_w 17694751
    //   226: iastore
    //   227: dup
    //   228: iconst_2
    //   229: ldc_w 17694752
    //   232: iastore
    //   233: dup
    //   234: iconst_3
    //   235: ldc_w 17694750
    //   238: iastore
    //   239: pop
    //   240: iconst_0
    //   241: istore_3
    //   242: iload_3
    //   243: aload #8
    //   245: arraylength
    //   246: if_icmpge -> 343
    //   249: iconst_4
    //   250: anewarray java/lang/String
    //   253: dup
    //   254: iconst_0
    //   255: ldc 'bluetooth.controller.idle'
    //   257: aastore
    //   258: dup
    //   259: iconst_1
    //   260: ldc 'bluetooth.controller.rx'
    //   262: aastore
    //   263: dup
    //   264: iconst_2
    //   265: ldc 'bluetooth.controller.tx'
    //   267: aastore
    //   268: dup
    //   269: iconst_3
    //   270: ldc 'bluetooth.controller.voltage'
    //   272: aastore
    //   273: iload_3
    //   274: aaload
    //   275: astore_1
    //   276: getstatic com/android/internal/os/PowerProfile.sPowerItemMap : Ljava/util/HashMap;
    //   279: aload_1
    //   280: invokevirtual containsKey : (Ljava/lang/Object;)Z
    //   283: ifeq -> 307
    //   286: getstatic com/android/internal/os/PowerProfile.sPowerItemMap : Ljava/util/HashMap;
    //   289: aload_1
    //   290: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   293: checkcast java/lang/Double
    //   296: invokevirtual doubleValue : ()D
    //   299: dconst_0
    //   300: dcmpl
    //   301: ifle -> 307
    //   304: goto -> 337
    //   307: aload #4
    //   309: aload #8
    //   311: iload_3
    //   312: iaload
    //   313: invokevirtual getInteger : (I)I
    //   316: istore #5
    //   318: iload #5
    //   320: ifle -> 337
    //   323: getstatic com/android/internal/os/PowerProfile.sPowerItemMap : Ljava/util/HashMap;
    //   326: aload_1
    //   327: iload #5
    //   329: i2d
    //   330: invokestatic valueOf : (D)Ljava/lang/Double;
    //   333: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   336: pop
    //   337: iinc #3, 1
    //   340: goto -> 242
    //   343: return
    //   344: iload #5
    //   346: istore_3
    //   347: iload #5
    //   349: ifeq -> 391
    //   352: iload #5
    //   354: istore_3
    //   355: aload #7
    //   357: ldc 'value'
    //   359: invokevirtual equals : (Ljava/lang/Object;)Z
    //   362: ifne -> 391
    //   365: getstatic com/android/internal/os/PowerProfile.sPowerArrayMap : Ljava/util/HashMap;
    //   368: aload_1
    //   369: aload #6
    //   371: aload #6
    //   373: invokevirtual size : ()I
    //   376: anewarray java/lang/Double
    //   379: invokevirtual toArray : ([Ljava/lang/Object;)[Ljava/lang/Object;
    //   382: checkcast [Ljava/lang/Double;
    //   385: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   388: pop
    //   389: iconst_0
    //   390: istore_3
    //   391: aload #7
    //   393: ldc 'array'
    //   395: invokevirtual equals : (Ljava/lang/Object;)Z
    //   398: istore_2
    //   399: iload_2
    //   400: ifeq -> 428
    //   403: iconst_1
    //   404: istore #5
    //   406: aload #6
    //   408: invokevirtual clear : ()V
    //   411: aload_0
    //   412: getfield parser : Lorg/xmlpull/v1/XmlPullParser;
    //   415: aconst_null
    //   416: ldc 'name'
    //   418: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   423: astore #9
    //   425: goto -> 590
    //   428: aload #7
    //   430: ldc 'item'
    //   432: invokevirtual equals : (Ljava/lang/Object;)Z
    //   435: ifne -> 454
    //   438: iload_3
    //   439: istore #5
    //   441: aload_1
    //   442: astore #9
    //   444: aload #7
    //   446: ldc 'value'
    //   448: invokevirtual equals : (Ljava/lang/Object;)Z
    //   451: ifeq -> 590
    //   454: iload_3
    //   455: ifne -> 475
    //   458: aload_0
    //   459: getfield parser : Lorg/xmlpull/v1/XmlPullParser;
    //   462: aconst_null
    //   463: ldc 'name'
    //   465: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   470: astore #8
    //   472: goto -> 478
    //   475: aconst_null
    //   476: astore #8
    //   478: iload_3
    //   479: istore #5
    //   481: aload_1
    //   482: astore #9
    //   484: aload_0
    //   485: getfield parser : Lorg/xmlpull/v1/XmlPullParser;
    //   488: invokeinterface next : ()I
    //   493: iconst_4
    //   494: if_icmpne -> 590
    //   497: aload_0
    //   498: getfield parser : Lorg/xmlpull/v1/XmlPullParser;
    //   501: invokeinterface getText : ()Ljava/lang/String;
    //   506: astore #9
    //   508: dconst_0
    //   509: dstore #10
    //   511: aload #9
    //   513: invokestatic valueOf : (Ljava/lang/String;)Ljava/lang/Double;
    //   516: invokevirtual doubleValue : ()D
    //   519: dstore #12
    //   521: goto -> 530
    //   524: astore #9
    //   526: dload #10
    //   528: dstore #12
    //   530: aload #7
    //   532: ldc 'item'
    //   534: invokevirtual equals : (Ljava/lang/Object;)Z
    //   537: ifeq -> 563
    //   540: getstatic com/android/internal/os/PowerProfile.sPowerItemMap : Ljava/util/HashMap;
    //   543: aload #8
    //   545: dload #12
    //   547: invokestatic valueOf : (D)Ljava/lang/Double;
    //   550: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   553: pop
    //   554: iload_3
    //   555: istore #5
    //   557: aload_1
    //   558: astore #9
    //   560: goto -> 590
    //   563: iload_3
    //   564: istore #5
    //   566: aload_1
    //   567: astore #9
    //   569: iload_3
    //   570: ifeq -> 590
    //   573: aload #6
    //   575: dload #12
    //   577: invokestatic valueOf : (D)Ljava/lang/Double;
    //   580: invokevirtual add : (Ljava/lang/Object;)Z
    //   583: pop
    //   584: aload_1
    //   585: astore #9
    //   587: iload_3
    //   588: istore #5
    //   590: aload #9
    //   592: astore_1
    //   593: goto -> 63
    //   596: astore #8
    //   598: goto -> 630
    //   601: astore #8
    //   603: new java/lang/RuntimeException
    //   606: astore_1
    //   607: aload_1
    //   608: aload #8
    //   610: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   613: aload_1
    //   614: athrow
    //   615: astore_1
    //   616: new java/lang/RuntimeException
    //   619: astore #8
    //   621: aload #8
    //   623: aload_1
    //   624: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   627: aload #8
    //   629: athrow
    //   630: aload_0
    //   631: getfield parser : Lorg/xmlpull/v1/XmlPullParser;
    //   634: instanceof android/content/res/XmlResourceParser
    //   637: ifeq -> 662
    //   640: ldc_w 'PowerProfile'
    //   643: ldc_w 'parse close here'
    //   646: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   649: pop
    //   650: aload_0
    //   651: getfield parser : Lorg/xmlpull/v1/XmlPullParser;
    //   654: checkcast android/content/res/XmlResourceParser
    //   657: invokeinterface close : ()V
    //   662: aload_0
    //   663: getfield fis : Ljava/io/FileInputStream;
    //   666: ifnull -> 725
    //   669: aload_0
    //   670: getfield fis : Ljava/io/FileInputStream;
    //   673: invokevirtual close : ()V
    //   676: aload_0
    //   677: aconst_null
    //   678: putfield fis : Ljava/io/FileInputStream;
    //   681: goto -> 725
    //   684: astore_1
    //   685: new java/lang/StringBuilder
    //   688: dup
    //   689: invokespecial <init> : ()V
    //   692: astore #9
    //   694: aload #9
    //   696: ldc_w 'access power profile exception caught : '
    //   699: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   702: pop
    //   703: aload #9
    //   705: aload_1
    //   706: invokevirtual getMessage : ()Ljava/lang/String;
    //   709: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   712: pop
    //   713: ldc_w 'PowerProfile'
    //   716: aload #9
    //   718: invokevirtual toString : ()Ljava/lang/String;
    //   721: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   724: pop
    //   725: aload #8
    //   727: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #270	-> 0
    //   #271	-> 10
    //   #274	-> 13
    //   #276	-> 17
    //   #283	-> 23
    //   #284	-> 30
    //   #287	-> 40
    //   #288	-> 43
    //   #289	-> 52
    //   #292	-> 54
    //   #295	-> 63
    //   #297	-> 70
    //   #298	-> 81
    //   #327	-> 86
    //   #328	-> 91
    //   #341	-> 115
    //   #342	-> 125
    //   #343	-> 135
    //   #345	-> 147
    //   #347	-> 154
    //   #348	-> 161
    //   #349	-> 169
    //   #350	-> 171
    //   #357	-> 208
    //   #364	-> 240
    //   #371	-> 240
    //   #372	-> 249
    //   #375	-> 276
    //   #376	-> 304
    //   #378	-> 307
    //   #379	-> 318
    //   #380	-> 323
    //   #371	-> 337
    //   #383	-> 343
    //   #300	-> 344
    //   #302	-> 365
    //   #303	-> 389
    //   #305	-> 391
    //   #306	-> 403
    //   #307	-> 406
    //   #308	-> 411
    //   #309	-> 428
    //   #310	-> 454
    //   #311	-> 454
    //   #312	-> 478
    //   #313	-> 497
    //   #314	-> 508
    //   #316	-> 511
    //   #318	-> 521
    //   #317	-> 524
    //   #319	-> 530
    //   #320	-> 540
    //   #321	-> 563
    //   #322	-> 573
    //   #326	-> 590
    //   #341	-> 596
    //   #332	-> 601
    //   #333	-> 603
    //   #330	-> 615
    //   #331	-> 616
    //   #341	-> 630
    //   #342	-> 640
    //   #343	-> 650
    //   #345	-> 662
    //   #347	-> 669
    //   #348	-> 676
    //   #351	-> 681
    //   #349	-> 684
    //   #350	-> 685
    //   #354	-> 725
    // Exception table:
    //   from	to	target	type
    //   54	63	615	org/xmlpull/v1/XmlPullParserException
    //   54	63	601	java/io/IOException
    //   54	63	596	finally
    //   63	70	615	org/xmlpull/v1/XmlPullParserException
    //   63	70	601	java/io/IOException
    //   63	70	596	finally
    //   70	81	615	org/xmlpull/v1/XmlPullParserException
    //   70	81	601	java/io/IOException
    //   70	81	596	finally
    //   91	115	615	org/xmlpull/v1/XmlPullParserException
    //   91	115	601	java/io/IOException
    //   91	115	596	finally
    //   154	161	169	java/io/IOException
    //   161	166	169	java/io/IOException
    //   355	365	615	org/xmlpull/v1/XmlPullParserException
    //   355	365	601	java/io/IOException
    //   355	365	596	finally
    //   365	389	615	org/xmlpull/v1/XmlPullParserException
    //   365	389	601	java/io/IOException
    //   365	389	596	finally
    //   391	399	615	org/xmlpull/v1/XmlPullParserException
    //   391	399	601	java/io/IOException
    //   391	399	596	finally
    //   406	411	615	org/xmlpull/v1/XmlPullParserException
    //   406	411	601	java/io/IOException
    //   406	411	596	finally
    //   411	425	615	org/xmlpull/v1/XmlPullParserException
    //   411	425	601	java/io/IOException
    //   411	425	596	finally
    //   428	438	615	org/xmlpull/v1/XmlPullParserException
    //   428	438	601	java/io/IOException
    //   428	438	596	finally
    //   444	454	615	org/xmlpull/v1/XmlPullParserException
    //   444	454	601	java/io/IOException
    //   444	454	596	finally
    //   458	472	615	org/xmlpull/v1/XmlPullParserException
    //   458	472	601	java/io/IOException
    //   458	472	596	finally
    //   484	497	615	org/xmlpull/v1/XmlPullParserException
    //   484	497	601	java/io/IOException
    //   484	497	596	finally
    //   497	508	615	org/xmlpull/v1/XmlPullParserException
    //   497	508	601	java/io/IOException
    //   497	508	596	finally
    //   511	521	524	java/lang/NumberFormatException
    //   511	521	615	org/xmlpull/v1/XmlPullParserException
    //   511	521	601	java/io/IOException
    //   511	521	596	finally
    //   530	540	615	org/xmlpull/v1/XmlPullParserException
    //   530	540	601	java/io/IOException
    //   530	540	596	finally
    //   540	554	615	org/xmlpull/v1/XmlPullParserException
    //   540	554	601	java/io/IOException
    //   540	554	596	finally
    //   573	584	615	org/xmlpull/v1/XmlPullParserException
    //   573	584	601	java/io/IOException
    //   573	584	596	finally
    //   603	615	596	finally
    //   616	630	596	finally
    //   669	676	684	java/io/IOException
    //   676	681	684	java/io/IOException
  }
  
  private void initCpuClusters() {
    if (sPowerArrayMap.containsKey("cpu.clusters.cores")) {
      Double[] arrayOfDouble = sPowerArrayMap.get("cpu.clusters.cores");
      this.mCpuClusters = new CpuClusterKey[arrayOfDouble.length];
      for (byte b = 0; b < arrayOfDouble.length; b++) {
        int i = (int)Math.round(arrayOfDouble[b].doubleValue());
        CpuClusterKey[] arrayOfCpuClusterKey = this.mCpuClusters;
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("cpu.core_speeds.cluster");
        stringBuilder1.append(b);
        String str1 = stringBuilder1.toString();
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("cpu.cluster_power.cluster");
        stringBuilder2.append(b);
        String str2 = stringBuilder2.toString();
        StringBuilder stringBuilder3 = new StringBuilder();
        stringBuilder3.append("cpu.core_power.cluster");
        stringBuilder3.append(b);
        arrayOfCpuClusterKey[b] = new CpuClusterKey(str2, stringBuilder3.toString(), i);
      } 
    } else {
      this.mCpuClusters = new CpuClusterKey[1];
      int i = 1;
      if (sPowerItemMap.containsKey("cpu.clusters.cores"))
        i = (int)Math.round(((Double)sPowerItemMap.get("cpu.clusters.cores")).doubleValue()); 
      this.mCpuClusters[0] = new CpuClusterKey("cpu.cluster_power.cluster0", "cpu.core_power.cluster0", i);
    } 
  }
  
  class CpuClusterKey {
    private final String clusterPowerKey;
    
    private final String corePowerKey;
    
    private final String freqKey;
    
    private final int numCpus;
    
    private CpuClusterKey(PowerProfile this$0, String param1String1, String param1String2, int param1Int) {
      this.freqKey = (String)this$0;
      this.clusterPowerKey = param1String1;
      this.corePowerKey = param1String2;
      this.numCpus = param1Int;
    }
  }
  
  public int getNumCpuClusters() {
    return this.mCpuClusters.length;
  }
  
  public int getNumCoresInCpuCluster(int paramInt) {
    if (paramInt >= 0) {
      CpuClusterKey[] arrayOfCpuClusterKey = this.mCpuClusters;
      if (paramInt < arrayOfCpuClusterKey.length)
        return (arrayOfCpuClusterKey[paramInt]).numCpus; 
    } 
    return 0;
  }
  
  public int getNumSpeedStepsInCpuCluster(int paramInt) {
    if (paramInt >= 0) {
      CpuClusterKey[] arrayOfCpuClusterKey = this.mCpuClusters;
      if (paramInt < arrayOfCpuClusterKey.length) {
        if (sPowerArrayMap.containsKey((arrayOfCpuClusterKey[paramInt]).freqKey))
          return ((Double[])sPowerArrayMap.get((this.mCpuClusters[paramInt]).freqKey)).length; 
        return 1;
      } 
    } 
    return 0;
  }
  
  public double getAveragePowerForCpuCluster(int paramInt) {
    if (paramInt >= 0) {
      CpuClusterKey[] arrayOfCpuClusterKey = this.mCpuClusters;
      if (paramInt < arrayOfCpuClusterKey.length)
        return getAveragePower((arrayOfCpuClusterKey[paramInt]).clusterPowerKey); 
    } 
    return 0.0D;
  }
  
  public double getAveragePowerForCpuCore(int paramInt1, int paramInt2) {
    if (paramInt1 >= 0) {
      CpuClusterKey[] arrayOfCpuClusterKey = this.mCpuClusters;
      if (paramInt1 < arrayOfCpuClusterKey.length)
        return getAveragePower((arrayOfCpuClusterKey[paramInt1]).corePowerKey, paramInt2); 
    } 
    return 0.0D;
  }
  
  public int getNumElements(String paramString) {
    if (sPowerItemMap.containsKey(paramString))
      return 1; 
    if (sPowerArrayMap.containsKey(paramString))
      return ((Double[])sPowerArrayMap.get(paramString)).length; 
    return 0;
  }
  
  public double getAveragePowerOrDefault(String paramString, double paramDouble) {
    if (sPowerItemMap.containsKey(paramString))
      return ((Double)sPowerItemMap.get(paramString)).doubleValue(); 
    if (sPowerArrayMap.containsKey(paramString))
      return ((Double[])sPowerArrayMap.get(paramString))[0].doubleValue(); 
    return paramDouble;
  }
  
  public double getAveragePower(String paramString) {
    return getAveragePowerOrDefault(paramString, 0.0D);
  }
  
  public double getAveragePower(String paramString, int paramInt) {
    if (sPowerItemMap.containsKey(paramString))
      return ((Double)sPowerItemMap.get(paramString)).doubleValue(); 
    if (sPowerArrayMap.containsKey(paramString)) {
      Double[] arrayOfDouble = sPowerArrayMap.get(paramString);
      if (arrayOfDouble.length > paramInt && paramInt >= 0)
        return arrayOfDouble[paramInt].doubleValue(); 
      if (paramInt < 0 || arrayOfDouble.length == 0)
        return 0.0D; 
      return arrayOfDouble[arrayOfDouble.length - 1].doubleValue();
    } 
    return 0.0D;
  }
  
  public double getBatteryCapacity() {
    return getAveragePower("battery.capacity");
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream) {
    writePowerConstantToProto(paramProtoOutputStream, "cpu.suspend", 1103806595073L);
    writePowerConstantToProto(paramProtoOutputStream, "cpu.idle", 1103806595074L);
    writePowerConstantToProto(paramProtoOutputStream, "cpu.active", 1103806595075L);
    for (byte b = 0; b < this.mCpuClusters.length; b++) {
      long l = paramProtoOutputStream.start(2246267895848L);
      paramProtoOutputStream.write(1120986464257L, b);
      HashMap<String, Double> hashMap = sPowerItemMap;
      CpuClusterKey cpuClusterKey = this.mCpuClusters[b];
      double d = ((Double)hashMap.get(cpuClusterKey.clusterPowerKey)).doubleValue();
      paramProtoOutputStream.write(1103806595074L, d);
      paramProtoOutputStream.write(1120986464259L, (this.mCpuClusters[b]).numCpus);
      Double[] arrayOfDouble;
      int i;
      boolean bool;
      byte b1;
      for (arrayOfDouble = sPowerArrayMap.get((this.mCpuClusters[b]).freqKey), i = arrayOfDouble.length, bool = false, b1 = 0; b1 < i; ) {
        Double double_ = arrayOfDouble[b1];
        paramProtoOutputStream.write(2211908157444L, double_.doubleValue());
        b1++;
      } 
      for (arrayOfDouble = sPowerArrayMap.get((this.mCpuClusters[b]).corePowerKey), i = arrayOfDouble.length, b1 = bool; b1 < i; ) {
        Double double_ = arrayOfDouble[b1];
        paramProtoOutputStream.write(2203318222853L, double_.doubleValue());
        b1++;
      } 
      paramProtoOutputStream.end(l);
    } 
    writePowerConstantToProto(paramProtoOutputStream, "wifi.scan", 1103806595076L);
    writePowerConstantToProto(paramProtoOutputStream, "wifi.on", 1103806595077L);
    writePowerConstantToProto(paramProtoOutputStream, "wifi.active", 1103806595078L);
    writePowerConstantToProto(paramProtoOutputStream, "wifi.controller.idle", 1103806595079L);
    writePowerConstantToProto(paramProtoOutputStream, "wifi.controller.rx", 1103806595080L);
    writePowerConstantToProto(paramProtoOutputStream, "wifi.controller.tx", 1103806595081L);
    writePowerConstantArrayToProto(paramProtoOutputStream, "wifi.controller.tx_levels", 2203318222858L);
    writePowerConstantToProto(paramProtoOutputStream, "wifi.controller.voltage", 1103806595083L);
    writePowerConstantToProto(paramProtoOutputStream, "bluetooth.controller.idle", 1103806595084L);
    writePowerConstantToProto(paramProtoOutputStream, "bluetooth.controller.rx", 1103806595085L);
    writePowerConstantToProto(paramProtoOutputStream, "bluetooth.controller.tx", 1103806595086L);
    writePowerConstantToProto(paramProtoOutputStream, "bluetooth.controller.voltage", 1103806595087L);
    writePowerConstantToProto(paramProtoOutputStream, "modem.controller.sleep", 1103806595088L);
    writePowerConstantToProto(paramProtoOutputStream, "modem.controller.idle", 1103806595089L);
    writePowerConstantToProto(paramProtoOutputStream, "modem.controller.rx", 1103806595090L);
    writePowerConstantArrayToProto(paramProtoOutputStream, "modem.controller.tx", 2203318222867L);
    writePowerConstantToProto(paramProtoOutputStream, "modem.controller.voltage", 1103806595092L);
    writePowerConstantToProto(paramProtoOutputStream, "gps.on", 1103806595093L);
    writePowerConstantArrayToProto(paramProtoOutputStream, "gps.signalqualitybased", 2203318222870L);
    writePowerConstantToProto(paramProtoOutputStream, "gps.voltage", 1103806595095L);
    writePowerConstantToProto(paramProtoOutputStream, "bluetooth.on", 1103806595096L);
    writePowerConstantToProto(paramProtoOutputStream, "bluetooth.active", 1103806595097L);
    writePowerConstantToProto(paramProtoOutputStream, "bluetooth.at", 1103806595098L);
    writePowerConstantToProto(paramProtoOutputStream, "ambient.on", 1103806595099L);
    writePowerConstantToProto(paramProtoOutputStream, "screen.on", 1103806595100L);
    writePowerConstantToProto(paramProtoOutputStream, "radio.on", 1103806595101L);
    writePowerConstantToProto(paramProtoOutputStream, "radio.scanning", 1103806595102L);
    writePowerConstantToProto(paramProtoOutputStream, "radio.active", 1103806595103L);
    writePowerConstantToProto(paramProtoOutputStream, "screen.full", 1103806595104L);
    writePowerConstantToProto(paramProtoOutputStream, "audio", 1103806595105L);
    writePowerConstantToProto(paramProtoOutputStream, "video", 1103806595106L);
    writePowerConstantToProto(paramProtoOutputStream, "camera.flashlight", 1103806595107L);
    writePowerConstantToProto(paramProtoOutputStream, "memory.bandwidths", 1103806595108L);
    writePowerConstantToProto(paramProtoOutputStream, "camera.avg", 1103806595109L);
    writePowerConstantToProto(paramProtoOutputStream, "wifi.batchedscan", 1103806595110L);
    writePowerConstantToProto(paramProtoOutputStream, "battery.capacity", 1103806595111L);
  }
  
  private void writePowerConstantToProto(ProtoOutputStream paramProtoOutputStream, String paramString, long paramLong) {
    if (sPowerItemMap.containsKey(paramString))
      paramProtoOutputStream.write(paramLong, ((Double)sPowerItemMap.get(paramString)).doubleValue()); 
  }
  
  private void writePowerConstantArrayToProto(ProtoOutputStream paramProtoOutputStream, String paramString, long paramLong) {
    if (sPowerArrayMap.containsKey(paramString))
      for (Double double_ : (Double[])sPowerArrayMap.get(paramString))
        paramProtoOutputStream.write(paramLong, double_.doubleValue());  
  }
}
