package com.android.internal.os.logging;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Pair;
import com.android.internal.logging.MetricsLogger;
import com.android.internal.util.FrameworkStatsLog;

public class MetricsLoggerWrapper {
  private static final int METRIC_VALUE_DISMISSED_BY_DRAG = 1;
  
  private static final int METRIC_VALUE_DISMISSED_BY_TAP = 0;
  
  public static void logPictureInPictureDismissByTap(Context paramContext, Pair<ComponentName, Integer> paramPair) {
    MetricsLogger.action(paramContext, 822, 0);
    ComponentName componentName2 = (ComponentName)paramPair.first;
    Integer integer = (Integer)paramPair.second;
    int i = getUid(paramContext, componentName2, integer.intValue());
    ComponentName componentName1 = (ComponentName)paramPair.first;
    String str = componentName1.flattenToString();
    FrameworkStatsLog.write(52, i, str, 4);
  }
  
  public static void logPictureInPictureDismissByDrag(Context paramContext, Pair<ComponentName, Integer> paramPair) {
    MetricsLogger.action(paramContext, 822, 1);
    ComponentName componentName2 = (ComponentName)paramPair.first;
    Integer integer = (Integer)paramPair.second;
    int i = getUid(paramContext, componentName2, integer.intValue());
    ComponentName componentName1 = (ComponentName)paramPair.first;
    String str = componentName1.flattenToString();
    FrameworkStatsLog.write(52, i, str, 4);
  }
  
  public static void logPictureInPictureMinimize(Context paramContext, boolean paramBoolean, Pair<ComponentName, Integer> paramPair) {
    MetricsLogger.action(paramContext, 821, paramBoolean);
    ComponentName componentName2 = (ComponentName)paramPair.first;
    Integer integer = (Integer)paramPair.second;
    int i = getUid(paramContext, componentName2, integer.intValue());
    ComponentName componentName1 = (ComponentName)paramPair.first;
    String str = componentName1.flattenToString();
    FrameworkStatsLog.write(52, i, str, 3);
  }
  
  private static int getUid(Context paramContext, ComponentName paramComponentName, int paramInt) {
    byte b = -1;
    if (paramComponentName == null)
      return -1; 
    try {
      PackageManager packageManager = paramContext.getPackageManager();
      String str = paramComponentName.getPackageName();
      paramInt = (packageManager.getApplicationInfoAsUser(str, 0, paramInt)).uid;
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      paramInt = b;
    } 
    return paramInt;
  }
  
  public static void logPictureInPictureMenuVisible(Context paramContext, boolean paramBoolean) {
    MetricsLogger.visibility(paramContext, 823, paramBoolean);
  }
  
  public static void logPictureInPictureEnter(Context paramContext, int paramInt, String paramString, boolean paramBoolean) {
    MetricsLogger.action(paramContext, 819, paramBoolean);
    FrameworkStatsLog.write(52, paramInt, paramString, 1);
  }
  
  public static void logPictureInPictureFullScreen(Context paramContext, int paramInt, String paramString) {
    MetricsLogger.action(paramContext, 820);
    FrameworkStatsLog.write(52, paramInt, paramString, 2);
  }
  
  public static void logAppOverlayEnter(int paramInt1, String paramString, boolean paramBoolean1, int paramInt2, boolean paramBoolean2) {
    if (paramBoolean1)
      if (paramInt2 != 2038) {
        FrameworkStatsLog.write(59, paramInt1, paramString, true, 1);
      } else if (!paramBoolean2) {
        FrameworkStatsLog.write(59, paramInt1, paramString, false, 1);
      }  
  }
  
  public static void logAppOverlayExit(int paramInt1, String paramString, boolean paramBoolean1, int paramInt2, boolean paramBoolean2) {
    if (paramBoolean1)
      if (paramInt2 != 2038) {
        FrameworkStatsLog.write(59, paramInt1, paramString, true, 2);
      } else if (!paramBoolean2) {
        FrameworkStatsLog.write(59, paramInt1, paramString, false, 2);
      }  
  }
}
