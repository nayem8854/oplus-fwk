package com.android.internal.os;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.os.BatteryStats;
import android.os.Bundle;
import android.os.IBinder;
import android.os.MemoryFile;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.UserHandle;
import android.util.ArrayMap;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseLongArray;
import com.android.internal.app.IBatteryStats;
import com.android.internal.util.ArrayUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class BatteryStatsHelper {
  private static final String TAG = BatteryStatsHelper.class.getSimpleName();
  
  private static ArrayMap<File, BatteryStats> sFileXfer = new ArrayMap<>();
  
  private final List<BatterySipper> mUsageList = new ArrayList<>();
  
  private final List<BatterySipper> mWifiSippers = new ArrayList<>();
  
  private final List<BatterySipper> mBluetoothSippers = new ArrayList<>();
  
  private final SparseArray<List<BatterySipper>> mUserSippers = new SparseArray<>();
  
  private final List<BatterySipper> mMobilemsppList = new ArrayList<>();
  
  private int mStatsType = 0;
  
  private long mStatsPeriod = 0L;
  
  private double mMaxPower = 1.0D;
  
  private double mMaxRealPower = 1.0D;
  
  boolean mHasWifiPowerReporting = false;
  
  boolean mHasBluetoothPowerReporting = false;
  
  static final boolean DEBUG = false;
  
  private static Intent sBatteryBroadcastXfer;
  
  private static BatteryStats sStatsXfer;
  
  private Intent mBatteryBroadcast;
  
  private IBatteryStats mBatteryInfo;
  
  long mBatteryRealtimeUs;
  
  long mBatteryTimeRemainingUs;
  
  long mBatteryUptimeUs;
  
  PowerCalculator mBluetoothPowerCalculator;
  
  PowerCalculator mCameraPowerCalculator;
  
  long mChargeTimeRemainingUs;
  
  private final boolean mCollectBatteryBroadcast;
  
  private double mComputedPower;
  
  private final Context mContext;
  
  PowerCalculator mCpuPowerCalculator;
  
  PowerCalculator mFlashlightPowerCalculator;
  
  private double mMaxDrainedPower;
  
  PowerCalculator mMediaPowerCalculator;
  
  PowerCalculator mMemoryPowerCalculator;
  
  private double mMinDrainedPower;
  
  MobileRadioPowerCalculator mMobileRadioPowerCalculator;
  
  private PackageManager mPackageManager;
  
  private PowerProfile mPowerProfile;
  
  long mRawRealtimeUs;
  
  long mRawUptimeUs;
  
  PowerCalculator mSensorPowerCalculator;
  
  private String[] mServicepackageArray;
  
  private BatteryStats mStats;
  
  private String[] mSystemPackageArray;
  
  private double mTotalPower;
  
  long mTypeBatteryRealtimeUs;
  
  long mTypeBatteryUptimeUs;
  
  PowerCalculator mWakelockPowerCalculator;
  
  private final boolean mWifiOnly;
  
  PowerCalculator mWifiPowerCalculator;
  
  public static boolean checkWifiOnly(Context paramContext) {
    ConnectivityManager connectivityManager = (ConnectivityManager)paramContext.getSystemService("connectivity");
    if (connectivityManager == null)
      return false; 
    return connectivityManager.isNetworkSupported(0) ^ true;
  }
  
  public static boolean checkHasWifiPowerReporting(BatteryStats paramBatteryStats, PowerProfile paramPowerProfile) {
    boolean bool;
    if (paramBatteryStats.hasWifiActivityReporting() && 
      paramPowerProfile.getAveragePower("wifi.controller.idle") != 0.0D && 
      paramPowerProfile.getAveragePower("wifi.controller.rx") != 0.0D && 
      paramPowerProfile.getAveragePower("wifi.controller.tx") != 0.0D) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean checkHasBluetoothPowerReporting(BatteryStats paramBatteryStats, PowerProfile paramPowerProfile) {
    boolean bool;
    if (paramBatteryStats.hasBluetoothActivityReporting() && 
      paramPowerProfile.getAveragePower("bluetooth.controller.idle") != 0.0D && 
      paramPowerProfile.getAveragePower("bluetooth.controller.rx") != 0.0D && 
      paramPowerProfile.getAveragePower("bluetooth.controller.tx") != 0.0D) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public BatteryStatsHelper(Context paramContext) {
    this(paramContext, true);
  }
  
  public BatteryStatsHelper(Context paramContext, boolean paramBoolean) {
    this(paramContext, paramBoolean, checkWifiOnly(paramContext));
  }
  
  public BatteryStatsHelper(Context paramContext, boolean paramBoolean1, boolean paramBoolean2) {
    this.mContext = paramContext;
    this.mCollectBatteryBroadcast = paramBoolean1;
    this.mWifiOnly = paramBoolean2;
    this.mPackageManager = paramContext.getPackageManager();
    Resources resources = paramContext.getResources();
    this.mSystemPackageArray = resources.getStringArray(17235993);
    this.mServicepackageArray = resources.getStringArray(17235992);
  }
  
  public void storeStatsHistoryInFile(String paramString) {
    synchronized (sFileXfer) {
      File file = makeFilePath(this.mContext, paramString);
      sFileXfer.put(file, getStats());
      FileOutputStream fileOutputStream1 = null;
      String str = null;
      paramString = str;
      FileOutputStream fileOutputStream2 = fileOutputStream1;
      try {
        FileOutputStream fileOutputStream5 = new FileOutputStream();
        paramString = str;
        fileOutputStream2 = fileOutputStream1;
        this(file);
        FileOutputStream fileOutputStream4 = fileOutputStream5;
        FileOutputStream fileOutputStream3 = fileOutputStream4;
        fileOutputStream2 = fileOutputStream4;
        Parcel parcel = Parcel.obtain();
        fileOutputStream3 = fileOutputStream4;
        fileOutputStream2 = fileOutputStream4;
        getStats().writeToParcelWithoutUids(parcel, 0);
        fileOutputStream3 = fileOutputStream4;
        fileOutputStream2 = fileOutputStream4;
        byte[] arrayOfByte = parcel.marshall();
        fileOutputStream3 = fileOutputStream4;
        fileOutputStream2 = fileOutputStream4;
        fileOutputStream4.write(arrayOfByte);
        try {
          fileOutputStream4.close();
        } catch (IOException iOException) {}
      } catch (IOException iOException) {
        FileOutputStream fileOutputStream = fileOutputStream2;
        Log.w(TAG, "Unable to write history to file", iOException);
        if (fileOutputStream2 != null)
          fileOutputStream2.close(); 
      } finally {}
      return;
    } 
  }
  
  public static BatteryStats statsFromFile(Context paramContext, String paramString) {
    synchronized (sFileXfer) {
      IBinder iBinder;
      FileInputStream fileInputStream;
      File file = makeFilePath(paramContext, paramString);
      BatteryStats batteryStats1 = sFileXfer.get(file);
      if (batteryStats1 != null)
        return batteryStats1; 
      String str = null;
      BatteryStats batteryStats2 = null;
      batteryStats1 = batteryStats2;
      paramString = str;
      try {
        FileInputStream fileInputStream2 = new FileInputStream();
        batteryStats1 = batteryStats2;
        paramString = str;
        this(file);
        FileInputStream fileInputStream1 = fileInputStream2;
        fileInputStream = fileInputStream2;
        byte[] arrayOfByte = readFully(fileInputStream2);
        fileInputStream1 = fileInputStream2;
        fileInputStream = fileInputStream2;
        Parcel parcel = Parcel.obtain();
        fileInputStream1 = fileInputStream2;
        fileInputStream = fileInputStream2;
        parcel.unmarshall(arrayOfByte, 0, arrayOfByte.length);
        fileInputStream1 = fileInputStream2;
        fileInputStream = fileInputStream2;
        parcel.setDataPosition(0);
        fileInputStream1 = fileInputStream2;
        fileInputStream = fileInputStream2;
        BatteryStats batteryStats = (BatteryStats)BatteryStatsImpl.CREATOR.createFromParcel(parcel);
        try {
          fileInputStream2.close();
        } catch (IOException iOException) {}
        return batteryStats;
      } catch (IOException iOException) {
        FileInputStream fileInputStream1 = fileInputStream;
        Log.w(TAG, "Unable to read history to file", iOException);
        if (fileInputStream != null)
          try {
            fileInputStream.close();
          } catch (IOException iOException1) {} 
        iBinder = ServiceManager.getService("batterystats");
        return getStats(IBatteryStats.Stub.asInterface(iBinder));
      } finally {}
      if (iBinder != null)
        try {
          iBinder.close();
        } catch (IOException iOException) {} 
      throw fileInputStream;
    } 
  }
  
  public static void dropFile(Context paramContext, String paramString) {
    makeFilePath(paramContext, paramString).delete();
  }
  
  private static File makeFilePath(Context paramContext, String paramString) {
    return new File(paramContext.getFilesDir(), paramString);
  }
  
  public void clearStats() {
    this.mStats = null;
  }
  
  public BatteryStats getStats() {
    if (this.mStats == null)
      load(); 
    return this.mStats;
  }
  
  public Intent getBatteryBroadcast() {
    if (this.mBatteryBroadcast == null && this.mCollectBatteryBroadcast)
      load(); 
    return this.mBatteryBroadcast;
  }
  
  public PowerProfile getPowerProfile() {
    return this.mPowerProfile;
  }
  
  public void create(BatteryStats paramBatteryStats) {
    this.mPowerProfile = new PowerProfile(this.mContext);
    this.mStats = paramBatteryStats;
  }
  
  public void create(Bundle paramBundle) {
    if (paramBundle != null) {
      this.mStats = sStatsXfer;
      this.mBatteryBroadcast = sBatteryBroadcastXfer;
    } 
    IBinder iBinder = ServiceManager.getService("batterystats");
    this.mBatteryInfo = IBatteryStats.Stub.asInterface(iBinder);
    this.mPowerProfile = new PowerProfile(this.mContext);
  }
  
  public void storeState() {
    sStatsXfer = this.mStats;
    sBatteryBroadcastXfer = this.mBatteryBroadcast;
  }
  
  public static String makemAh(double paramDouble) {
    String str;
    if (paramDouble == 0.0D)
      return "0"; 
    if (paramDouble < 1.0E-5D) {
      str = "%.8f";
    } else if (paramDouble < 1.0E-4D) {
      str = "%.7f";
    } else if (paramDouble < 0.001D) {
      str = "%.6f";
    } else if (paramDouble < 0.01D) {
      str = "%.5f";
    } else if (paramDouble < 0.1D) {
      str = "%.4f";
    } else if (paramDouble < 1.0D) {
      str = "%.3f";
    } else if (paramDouble < 10.0D) {
      str = "%.2f";
    } else if (paramDouble < 100.0D) {
      str = "%.1f";
    } else {
      str = "%.0f";
    } 
    return String.format(Locale.ENGLISH, str, new Object[] { Double.valueOf(paramDouble) });
  }
  
  public void refreshStats(int paramInt1, int paramInt2) {
    SparseArray<UserHandle> sparseArray = new SparseArray(1);
    sparseArray.put(paramInt2, new UserHandle(paramInt2));
    refreshStats(paramInt1, sparseArray);
  }
  
  public void refreshStats(int paramInt, List<UserHandle> paramList) {
    int i = paramList.size();
    SparseArray<UserHandle> sparseArray = new SparseArray(i);
    for (byte b = 0; b < i; b++) {
      UserHandle userHandle = paramList.get(b);
      sparseArray.put(userHandle.getIdentifier(), userHandle);
    } 
    refreshStats(paramInt, sparseArray);
  }
  
  public void refreshStats(int paramInt, SparseArray<UserHandle> paramSparseArray) {
    long l1 = SystemClock.elapsedRealtime();
    long l2 = SystemClock.uptimeMillis();
    refreshStats(paramInt, paramSparseArray, l1 * 1000L, l2 * 1000L);
  }
  
  public void refreshStats(int paramInt, SparseArray<UserHandle> paramSparseArray, long paramLong1, long paramLong2) {
    if (paramInt != 0) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("refreshStats called for statsType ");
      stringBuilder.append(paramInt);
      stringBuilder.append(" but only STATS_SINCE_CHARGED is supported. Using STATS_SINCE_CHARGED instead.");
      Log.w(str, stringBuilder.toString());
    } 
    getStats();
    this.mMaxPower = 0.0D;
    this.mMaxRealPower = 0.0D;
    this.mComputedPower = 0.0D;
    this.mTotalPower = 0.0D;
    this.mUsageList.clear();
    this.mWifiSippers.clear();
    this.mBluetoothSippers.clear();
    this.mUserSippers.clear();
    this.mMobilemsppList.clear();
    if (this.mStats == null)
      return; 
    if (this.mCpuPowerCalculator == null)
      this.mCpuPowerCalculator = new CpuPowerCalculator(this.mPowerProfile); 
    this.mCpuPowerCalculator.reset();
    if (this.mMemoryPowerCalculator == null)
      this.mMemoryPowerCalculator = new MemoryPowerCalculator(this.mPowerProfile); 
    this.mMemoryPowerCalculator.reset();
    if (this.mWakelockPowerCalculator == null)
      this.mWakelockPowerCalculator = new WakelockPowerCalculator(this.mPowerProfile); 
    this.mWakelockPowerCalculator.reset();
    if (this.mMobileRadioPowerCalculator == null)
      this.mMobileRadioPowerCalculator = new MobileRadioPowerCalculator(this.mPowerProfile, this.mStats); 
    this.mMobileRadioPowerCalculator.reset(this.mStats);
    boolean bool = checkHasWifiPowerReporting(this.mStats, this.mPowerProfile);
    if (this.mWifiPowerCalculator == null || bool != this.mHasWifiPowerReporting) {
      WifiPowerEstimator wifiPowerEstimator;
      if (bool) {
        WifiPowerCalculator wifiPowerCalculator = new WifiPowerCalculator(this.mPowerProfile);
      } else {
        wifiPowerEstimator = new WifiPowerEstimator(this.mPowerProfile);
      } 
      this.mWifiPowerCalculator = wifiPowerEstimator;
      this.mHasWifiPowerReporting = bool;
    } 
    this.mWifiPowerCalculator.reset();
    bool = checkHasBluetoothPowerReporting(this.mStats, this.mPowerProfile);
    if (this.mBluetoothPowerCalculator == null || bool != this.mHasBluetoothPowerReporting) {
      this.mBluetoothPowerCalculator = new BluetoothPowerCalculator(this.mPowerProfile);
      this.mHasBluetoothPowerReporting = bool;
    } 
    this.mBluetoothPowerCalculator.reset();
    PowerProfile powerProfile2 = this.mPowerProfile;
    Context context = this.mContext;
    SensorPowerCalculator sensorPowerCalculator = new SensorPowerCalculator(powerProfile2, (SensorManager)context.getSystemService("sensor"), this.mStats, paramLong1, paramInt);
    sensorPowerCalculator.reset();
    if (this.mCameraPowerCalculator == null)
      this.mCameraPowerCalculator = new CameraPowerCalculator(this.mPowerProfile); 
    this.mCameraPowerCalculator.reset();
    if (this.mFlashlightPowerCalculator == null)
      this.mFlashlightPowerCalculator = new FlashlightPowerCalculator(this.mPowerProfile); 
    this.mFlashlightPowerCalculator.reset();
    if (this.mMediaPowerCalculator == null)
      this.mMediaPowerCalculator = new MediaPowerCalculator(this.mPowerProfile); 
    this.mMediaPowerCalculator.reset();
    this.mStatsType = paramInt;
    this.mRawUptimeUs = paramLong2;
    this.mRawRealtimeUs = paramLong1;
    this.mBatteryUptimeUs = this.mStats.getBatteryUptime(paramLong2);
    this.mBatteryRealtimeUs = this.mStats.getBatteryRealtime(paramLong1);
    this.mTypeBatteryUptimeUs = this.mStats.computeBatteryUptime(paramLong2, this.mStatsType);
    this.mTypeBatteryRealtimeUs = this.mStats.computeBatteryRealtime(paramLong1, this.mStatsType);
    this.mBatteryTimeRemainingUs = this.mStats.computeBatteryTimeRemaining(paramLong1);
    this.mChargeTimeRemainingUs = this.mStats.computeChargeTimeRemaining(paramLong1);
    double d1 = this.mStats.getLowDischargeAmountSinceCharge();
    PowerProfile powerProfile1 = this.mPowerProfile;
    this.mMinDrainedPower = d1 * powerProfile1.getBatteryCapacity() / 100.0D;
    d1 = this.mStats.getHighDischargeAmountSinceCharge();
    powerProfile1 = this.mPowerProfile;
    this.mMaxDrainedPower = d1 * powerProfile1.getBatteryCapacity() / 100.0D;
    processAppUsage(paramSparseArray);
    for (paramInt = 0; paramInt < this.mUsageList.size(); paramInt++) {
      BatterySipper batterySipper = this.mUsageList.get(paramInt);
      batterySipper.computeMobilemspp();
      if (batterySipper.mobilemspp != 0.0D)
        this.mMobilemsppList.add(batterySipper); 
    } 
    for (paramInt = 0; paramInt < this.mUserSippers.size(); paramInt++) {
      List<BatterySipper> list = this.mUserSippers.valueAt(paramInt);
      for (byte b = 0; b < list.size(); b++) {
        BatterySipper batterySipper = list.get(b);
        batterySipper.computeMobilemspp();
        if (batterySipper.mobilemspp != 0.0D)
          this.mMobilemsppList.add(batterySipper); 
      } 
    } 
    Collections.sort(this.mMobilemsppList, new Comparator<BatterySipper>() {
          final BatteryStatsHelper this$0;
          
          public int compare(BatterySipper param1BatterySipper1, BatterySipper param1BatterySipper2) {
            return Double.compare(param1BatterySipper2.mobilemspp, param1BatterySipper1.mobilemspp);
          }
        });
    processMiscUsage();
    Collections.sort(this.mUsageList);
    if (!this.mUsageList.isEmpty()) {
      this.mMaxPower = d1 = ((BatterySipper)this.mUsageList.get(0)).totalPowerMah;
      this.mMaxRealPower = d1;
      int i = this.mUsageList.size();
      for (paramInt = 0; paramInt < i; paramInt++)
        this.mComputedPower += ((BatterySipper)this.mUsageList.get(paramInt)).totalPowerMah; 
    } 
    this.mTotalPower = this.mComputedPower;
    if (this.mStats.getLowDischargeAmountSinceCharge() > 1) {
      double d = this.mMinDrainedPower;
      d1 = this.mComputedPower;
      if (d > d1) {
        d1 = d - d1;
        this.mTotalPower = d;
        BatterySipper batterySipper = new BatterySipper(BatterySipper.DrainType.UNACCOUNTED, null, d1);
        int i = Collections.binarySearch((List)this.mUsageList, batterySipper);
        paramInt = i;
        if (i < 0)
          paramInt = -(i + 1); 
        this.mUsageList.add(paramInt, batterySipper);
        this.mMaxPower = Math.max(this.mMaxPower, d1);
      } else {
        d = this.mMaxDrainedPower;
        if (d < d1) {
          d1 -= d;
          BatterySipper batterySipper = new BatterySipper(BatterySipper.DrainType.OVERCOUNTED, null, d1);
          int i = Collections.binarySearch((List)this.mUsageList, batterySipper);
          paramInt = i;
          if (i < 0)
            paramInt = -(i + 1); 
          this.mUsageList.add(paramInt, batterySipper);
          this.mMaxPower = Math.max(this.mMaxPower, d1);
        } 
      } 
    } 
    d1 = removeHiddenBatterySippers(this.mUsageList);
    double d2 = getTotalPower() - d1;
    if (Math.abs(d2) > 0.001D)
      for (byte b = 0; b < paramInt; b++) {
        BatterySipper batterySipper = this.mUsageList.get(b);
        if (!batterySipper.shouldHide) {
          batterySipper.proportionalSmearMah = (batterySipper.totalPowerMah + batterySipper.screenPowerMah) / d2 * d1;
          batterySipper.sumPower();
        } 
      }  
  }
  
  private void processAppUsage(SparseArray<UserHandle> paramSparseArray) {
    // Byte code:
    //   0: aload_1
    //   1: iconst_m1
    //   2: invokevirtual get : (I)Ljava/lang/Object;
    //   5: ifnull -> 13
    //   8: iconst_1
    //   9: istore_2
    //   10: goto -> 15
    //   13: iconst_0
    //   14: istore_2
    //   15: aload_0
    //   16: aload_0
    //   17: getfield mTypeBatteryRealtimeUs : J
    //   20: putfield mStatsPeriod : J
    //   23: aconst_null
    //   24: astore_3
    //   25: aload_0
    //   26: getfield mStats : Landroid/os/BatteryStats;
    //   29: invokevirtual getUidStats : ()Landroid/util/SparseArray;
    //   32: astore #4
    //   34: aload #4
    //   36: invokevirtual size : ()I
    //   39: istore #5
    //   41: iconst_0
    //   42: istore #6
    //   44: iload #6
    //   46: iload #5
    //   48: if_icmpge -> 483
    //   51: aload #4
    //   53: iload #6
    //   55: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   58: checkcast android/os/BatteryStats$Uid
    //   61: astore #7
    //   63: new com/android/internal/os/BatterySipper
    //   66: dup
    //   67: getstatic com/android/internal/os/BatterySipper$DrainType.APP : Lcom/android/internal/os/BatterySipper$DrainType;
    //   70: aload #7
    //   72: dconst_0
    //   73: invokespecial <init> : (Lcom/android/internal/os/BatterySipper$DrainType;Landroid/os/BatteryStats$Uid;D)V
    //   76: astore #8
    //   78: aload_0
    //   79: getfield mCpuPowerCalculator : Lcom/android/internal/os/PowerCalculator;
    //   82: aload #8
    //   84: aload #7
    //   86: aload_0
    //   87: getfield mRawRealtimeUs : J
    //   90: aload_0
    //   91: getfield mRawUptimeUs : J
    //   94: aload_0
    //   95: getfield mStatsType : I
    //   98: invokevirtual calculateApp : (Lcom/android/internal/os/BatterySipper;Landroid/os/BatteryStats$Uid;JJI)V
    //   101: aload_0
    //   102: getfield mWakelockPowerCalculator : Lcom/android/internal/os/PowerCalculator;
    //   105: aload #8
    //   107: aload #7
    //   109: aload_0
    //   110: getfield mRawRealtimeUs : J
    //   113: aload_0
    //   114: getfield mRawUptimeUs : J
    //   117: aload_0
    //   118: getfield mStatsType : I
    //   121: invokevirtual calculateApp : (Lcom/android/internal/os/BatterySipper;Landroid/os/BatteryStats$Uid;JJI)V
    //   124: aload_0
    //   125: getfield mMobileRadioPowerCalculator : Lcom/android/internal/os/MobileRadioPowerCalculator;
    //   128: aload #8
    //   130: aload #7
    //   132: aload_0
    //   133: getfield mRawRealtimeUs : J
    //   136: aload_0
    //   137: getfield mRawUptimeUs : J
    //   140: aload_0
    //   141: getfield mStatsType : I
    //   144: invokevirtual calculateApp : (Lcom/android/internal/os/BatterySipper;Landroid/os/BatteryStats$Uid;JJI)V
    //   147: aload_0
    //   148: getfield mWifiPowerCalculator : Lcom/android/internal/os/PowerCalculator;
    //   151: aload #8
    //   153: aload #7
    //   155: aload_0
    //   156: getfield mRawRealtimeUs : J
    //   159: aload_0
    //   160: getfield mRawUptimeUs : J
    //   163: aload_0
    //   164: getfield mStatsType : I
    //   167: invokevirtual calculateApp : (Lcom/android/internal/os/BatterySipper;Landroid/os/BatteryStats$Uid;JJI)V
    //   170: aload_0
    //   171: getfield mBluetoothPowerCalculator : Lcom/android/internal/os/PowerCalculator;
    //   174: aload #8
    //   176: aload #7
    //   178: aload_0
    //   179: getfield mRawRealtimeUs : J
    //   182: aload_0
    //   183: getfield mRawUptimeUs : J
    //   186: aload_0
    //   187: getfield mStatsType : I
    //   190: invokevirtual calculateApp : (Lcom/android/internal/os/BatterySipper;Landroid/os/BatteryStats$Uid;JJI)V
    //   193: aload_0
    //   194: getfield mSensorPowerCalculator : Lcom/android/internal/os/PowerCalculator;
    //   197: aload #8
    //   199: aload #7
    //   201: aload_0
    //   202: getfield mRawRealtimeUs : J
    //   205: aload_0
    //   206: getfield mRawUptimeUs : J
    //   209: aload_0
    //   210: getfield mStatsType : I
    //   213: invokevirtual calculateApp : (Lcom/android/internal/os/BatterySipper;Landroid/os/BatteryStats$Uid;JJI)V
    //   216: aload_0
    //   217: getfield mCameraPowerCalculator : Lcom/android/internal/os/PowerCalculator;
    //   220: aload #8
    //   222: aload #7
    //   224: aload_0
    //   225: getfield mRawRealtimeUs : J
    //   228: aload_0
    //   229: getfield mRawUptimeUs : J
    //   232: aload_0
    //   233: getfield mStatsType : I
    //   236: invokevirtual calculateApp : (Lcom/android/internal/os/BatterySipper;Landroid/os/BatteryStats$Uid;JJI)V
    //   239: aload_0
    //   240: getfield mFlashlightPowerCalculator : Lcom/android/internal/os/PowerCalculator;
    //   243: aload #8
    //   245: aload #7
    //   247: aload_0
    //   248: getfield mRawRealtimeUs : J
    //   251: aload_0
    //   252: getfield mRawUptimeUs : J
    //   255: aload_0
    //   256: getfield mStatsType : I
    //   259: invokevirtual calculateApp : (Lcom/android/internal/os/BatterySipper;Landroid/os/BatteryStats$Uid;JJI)V
    //   262: aload_0
    //   263: getfield mMediaPowerCalculator : Lcom/android/internal/os/PowerCalculator;
    //   266: aload #8
    //   268: aload #7
    //   270: aload_0
    //   271: getfield mRawRealtimeUs : J
    //   274: aload_0
    //   275: getfield mRawUptimeUs : J
    //   278: aload_0
    //   279: getfield mStatsType : I
    //   282: invokevirtual calculateApp : (Lcom/android/internal/os/BatterySipper;Landroid/os/BatteryStats$Uid;JJI)V
    //   285: aload #8
    //   287: invokevirtual sumPower : ()D
    //   290: dstore #9
    //   292: dload #9
    //   294: dconst_0
    //   295: dcmpl
    //   296: ifne -> 310
    //   299: aload_3
    //   300: astore #11
    //   302: aload #7
    //   304: invokevirtual getUid : ()I
    //   307: ifne -> 474
    //   310: aload #8
    //   312: invokevirtual getUid : ()I
    //   315: istore #12
    //   317: iload #12
    //   319: invokestatic getUserId : (I)I
    //   322: istore #13
    //   324: iload #12
    //   326: sipush #1010
    //   329: if_icmpne -> 347
    //   332: aload_0
    //   333: getfield mWifiSippers : Ljava/util/List;
    //   336: aload #8
    //   338: invokeinterface add : (Ljava/lang/Object;)Z
    //   343: pop
    //   344: goto -> 462
    //   347: iload #12
    //   349: sipush #1002
    //   352: if_icmpne -> 370
    //   355: aload_0
    //   356: getfield mBluetoothSippers : Ljava/util/List;
    //   359: aload #8
    //   361: invokeinterface add : (Ljava/lang/Object;)Z
    //   366: pop
    //   367: goto -> 462
    //   370: iload_2
    //   371: ifne -> 450
    //   374: aload_1
    //   375: iload #13
    //   377: invokevirtual get : (I)Ljava/lang/Object;
    //   380: ifnonnull -> 450
    //   383: iload #12
    //   385: invokestatic getAppId : (I)I
    //   388: sipush #10000
    //   391: if_icmplt -> 450
    //   394: aload_0
    //   395: getfield mUserSippers : Landroid/util/SparseArray;
    //   398: iload #13
    //   400: invokevirtual get : (I)Ljava/lang/Object;
    //   403: checkcast java/util/List
    //   406: astore #7
    //   408: aload #7
    //   410: astore #11
    //   412: aload #7
    //   414: ifnonnull -> 437
    //   417: new java/util/ArrayList
    //   420: dup
    //   421: invokespecial <init> : ()V
    //   424: astore #11
    //   426: aload_0
    //   427: getfield mUserSippers : Landroid/util/SparseArray;
    //   430: iload #13
    //   432: aload #11
    //   434: invokevirtual put : (ILjava/lang/Object;)V
    //   437: aload #11
    //   439: aload #8
    //   441: invokeinterface add : (Ljava/lang/Object;)Z
    //   446: pop
    //   447: goto -> 462
    //   450: aload_0
    //   451: getfield mUsageList : Ljava/util/List;
    //   454: aload #8
    //   456: invokeinterface add : (Ljava/lang/Object;)Z
    //   461: pop
    //   462: aload_3
    //   463: astore #11
    //   465: iload #12
    //   467: ifne -> 474
    //   470: aload #8
    //   472: astore #11
    //   474: iinc #6, 1
    //   477: aload #11
    //   479: astore_3
    //   480: goto -> 44
    //   483: aload_3
    //   484: ifnull -> 516
    //   487: aload_0
    //   488: getfield mWakelockPowerCalculator : Lcom/android/internal/os/PowerCalculator;
    //   491: aload_3
    //   492: aload_0
    //   493: getfield mStats : Landroid/os/BatteryStats;
    //   496: aload_0
    //   497: getfield mRawRealtimeUs : J
    //   500: aload_0
    //   501: getfield mRawUptimeUs : J
    //   504: aload_0
    //   505: getfield mStatsType : I
    //   508: invokevirtual calculateRemaining : (Lcom/android/internal/os/BatterySipper;Landroid/os/BatteryStats;JJI)V
    //   511: aload_3
    //   512: invokevirtual sumPower : ()D
    //   515: pop2
    //   516: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #569	-> 0
    //   #570	-> 15
    //   #572	-> 23
    //   #573	-> 25
    //   #574	-> 34
    //   #576	-> 41
    //   #577	-> 51
    //   #578	-> 63
    //   #580	-> 78
    //   #582	-> 101
    //   #583	-> 124
    //   #585	-> 147
    //   #586	-> 170
    //   #588	-> 193
    //   #589	-> 216
    //   #590	-> 239
    //   #592	-> 262
    //   #594	-> 285
    //   #599	-> 292
    //   #603	-> 310
    //   #604	-> 317
    //   #605	-> 324
    //   #606	-> 332
    //   #607	-> 347
    //   #608	-> 355
    //   #609	-> 370
    //   #610	-> 383
    //   #612	-> 394
    //   #613	-> 408
    //   #614	-> 417
    //   #615	-> 426
    //   #617	-> 437
    //   #618	-> 447
    //   #619	-> 450
    //   #622	-> 462
    //   #623	-> 470
    //   #576	-> 474
    //   #628	-> 483
    //   #632	-> 487
    //   #634	-> 511
    //   #636	-> 516
  }
  
  private void addPhoneUsage() {
    long l = this.mStats.getPhoneOnTime(this.mRawRealtimeUs, this.mStatsType) / 1000L;
    double d = this.mPowerProfile.getAveragePower("radio.active") * l / 3600000.0D;
    if (d != 0.0D)
      addEntry(BatterySipper.DrainType.PHONE, l, d); 
  }
  
  private void addScreenUsage() {
    long l = this.mStats.getScreenOnTime(this.mRawRealtimeUs, this.mStatsType) / 1000L;
    double d1 = 0.0D + l * this.mPowerProfile.getAveragePower("screen.on");
    PowerProfile powerProfile = this.mPowerProfile;
    double d2 = powerProfile.getAveragePower("screen.full");
    for (byte b = 0; b < 5; b++) {
      double d3 = (b + 0.5F) * d2 / 5.0D;
      long l1 = this.mStats.getScreenBrightnessTime(b, this.mRawRealtimeUs, this.mStatsType) / 1000L;
      double d4 = l1;
      d1 += d4 * d3;
    } 
    d1 /= 3600000.0D;
    if (d1 != 0.0D)
      addEntry(BatterySipper.DrainType.SCREEN, l, d1); 
  }
  
  private void addAmbientDisplayUsage() {
    long l = this.mStats.getScreenDozeTime(this.mRawRealtimeUs, this.mStatsType) / 1000L;
    double d = this.mPowerProfile.getAveragePower("ambient.on") * l / 3600000.0D;
    if (d > 0.0D)
      addEntry(BatterySipper.DrainType.AMBIENT_DISPLAY, l, d); 
  }
  
  private void addRadioUsage() {
    BatterySipper batterySipper = new BatterySipper(BatterySipper.DrainType.CELL, null, 0.0D);
    this.mMobileRadioPowerCalculator.calculateRemaining(batterySipper, this.mStats, this.mRawRealtimeUs, this.mRawUptimeUs, this.mStatsType);
    batterySipper.sumPower();
    if (batterySipper.totalPowerMah > 0.0D)
      this.mUsageList.add(batterySipper); 
  }
  
  private void aggregateSippers(BatterySipper paramBatterySipper, List<BatterySipper> paramList, String paramString) {
    for (byte b = 0; b < paramList.size(); b++) {
      BatterySipper batterySipper = paramList.get(b);
      paramBatterySipper.add(batterySipper);
    } 
    paramBatterySipper.computeMobilemspp();
    paramBatterySipper.sumPower();
  }
  
  private void addIdleUsage() {
    double d1 = (this.mTypeBatteryRealtimeUs / 1000L);
    PowerProfile powerProfile = this.mPowerProfile;
    double d2 = powerProfile.getAveragePower("cpu.suspend");
    double d3 = (this.mTypeBatteryUptimeUs / 1000L);
    powerProfile = this.mPowerProfile;
    double d4 = powerProfile.getAveragePower("cpu.idle");
    d3 = (d1 * d2 + d3 * d4) / 3600000.0D;
    if (d3 != 0.0D)
      addEntry(BatterySipper.DrainType.IDLE, this.mTypeBatteryRealtimeUs / 1000L, d3); 
  }
  
  private void addWiFiUsage() {
    BatterySipper batterySipper = new BatterySipper(BatterySipper.DrainType.WIFI, null, 0.0D);
    this.mWifiPowerCalculator.calculateRemaining(batterySipper, this.mStats, this.mRawRealtimeUs, this.mRawUptimeUs, this.mStatsType);
    aggregateSippers(batterySipper, this.mWifiSippers, "WIFI");
    if (batterySipper.totalPowerMah > 0.0D)
      this.mUsageList.add(batterySipper); 
  }
  
  private void addBluetoothUsage() {
    BatterySipper batterySipper = new BatterySipper(BatterySipper.DrainType.BLUETOOTH, null, 0.0D);
    this.mBluetoothPowerCalculator.calculateRemaining(batterySipper, this.mStats, this.mRawRealtimeUs, this.mRawUptimeUs, this.mStatsType);
    aggregateSippers(batterySipper, this.mBluetoothSippers, "Bluetooth");
    if (batterySipper.totalPowerMah > 0.0D)
      this.mUsageList.add(batterySipper); 
  }
  
  private void addUserUsage() {
    for (byte b = 0; b < this.mUserSippers.size(); b++) {
      int i = this.mUserSippers.keyAt(b);
      BatterySipper batterySipper = new BatterySipper(BatterySipper.DrainType.USER, null, 0.0D);
      batterySipper.userId = i;
      aggregateSippers(batterySipper, this.mUserSippers.valueAt(b), "User");
      this.mUsageList.add(batterySipper);
    } 
  }
  
  private void addMemoryUsage() {
    BatterySipper batterySipper = new BatterySipper(BatterySipper.DrainType.MEMORY, null, 0.0D);
    this.mMemoryPowerCalculator.calculateRemaining(batterySipper, this.mStats, this.mRawRealtimeUs, this.mRawUptimeUs, this.mStatsType);
    batterySipper.sumPower();
    if (batterySipper.totalPowerMah > 0.0D)
      this.mUsageList.add(batterySipper); 
  }
  
  private void processMiscUsage() {
    addUserUsage();
    addPhoneUsage();
    addScreenUsage();
    addAmbientDisplayUsage();
    addWiFiUsage();
    addBluetoothUsage();
    addMemoryUsage();
    addIdleUsage();
    if (!this.mWifiOnly)
      addRadioUsage(); 
  }
  
  private BatterySipper addEntry(BatterySipper.DrainType paramDrainType, long paramLong, double paramDouble) {
    BatterySipper batterySipper = new BatterySipper(paramDrainType, null, 0.0D);
    batterySipper.usagePowerMah = paramDouble;
    batterySipper.usageTimeMs = paramLong;
    batterySipper.sumPower();
    this.mUsageList.add(batterySipper);
    return batterySipper;
  }
  
  public List<BatterySipper> getUsageList() {
    return this.mUsageList;
  }
  
  public List<BatterySipper> getMobilemsppList() {
    return this.mMobilemsppList;
  }
  
  public long getStatsPeriod() {
    return this.mStatsPeriod;
  }
  
  public int getStatsType() {
    return this.mStatsType;
  }
  
  public double getMaxPower() {
    return this.mMaxPower;
  }
  
  public double getMaxRealPower() {
    return this.mMaxRealPower;
  }
  
  public double getTotalPower() {
    return this.mTotalPower;
  }
  
  public double getComputedPower() {
    return this.mComputedPower;
  }
  
  public double getMinDrainedPower() {
    return this.mMinDrainedPower;
  }
  
  public double getMaxDrainedPower() {
    return this.mMaxDrainedPower;
  }
  
  public static byte[] readFully(FileInputStream paramFileInputStream) throws IOException {
    return readFully(paramFileInputStream, paramFileInputStream.available());
  }
  
  public static byte[] readFully(FileInputStream paramFileInputStream, int paramInt) throws IOException {
    int i = 0;
    byte[] arrayOfByte = new byte[paramInt];
    paramInt = i;
    while (true) {
      i = paramFileInputStream.read(arrayOfByte, paramInt, arrayOfByte.length - paramInt);
      if (i <= 0)
        return arrayOfByte; 
      paramInt += i;
      i = paramFileInputStream.available();
      byte[] arrayOfByte1 = arrayOfByte;
      if (i > arrayOfByte.length - paramInt) {
        arrayOfByte1 = new byte[paramInt + i];
        System.arraycopy(arrayOfByte, 0, arrayOfByte1, 0, paramInt);
      } 
      arrayOfByte = arrayOfByte1;
    } 
  }
  
  public double removeHiddenBatterySippers(List<BatterySipper> paramList) {
    double d = 0.0D;
    BatterySipper batterySipper = null;
    for (int i = paramList.size() - 1; i >= 0; i--, d = d1) {
      BatterySipper batterySipper1 = paramList.get(i);
      batterySipper1.shouldHide = shouldHideSipper(batterySipper1);
      double d1 = d;
      if (batterySipper1.shouldHide) {
        d1 = d;
        if (batterySipper1.drainType != BatterySipper.DrainType.OVERCOUNTED) {
          d1 = d;
          if (batterySipper1.drainType != BatterySipper.DrainType.SCREEN) {
            d1 = d;
            if (batterySipper1.drainType != BatterySipper.DrainType.AMBIENT_DISPLAY) {
              d1 = d;
              if (batterySipper1.drainType != BatterySipper.DrainType.UNACCOUNTED) {
                d1 = d;
                if (batterySipper1.drainType != BatterySipper.DrainType.BLUETOOTH) {
                  d1 = d;
                  if (batterySipper1.drainType != BatterySipper.DrainType.WIFI) {
                    d1 = d;
                    if (batterySipper1.drainType != BatterySipper.DrainType.IDLE)
                      d1 = d + batterySipper1.totalPowerMah; 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
      if (batterySipper1.drainType == BatterySipper.DrainType.SCREEN)
        batterySipper = batterySipper1; 
    } 
    smearScreenBatterySipper(paramList, batterySipper);
    return d;
  }
  
  public void smearScreenBatterySipper(List<BatterySipper> paramList, BatterySipper paramBatterySipper) {
    long l = 0L;
    SparseLongArray sparseLongArray = new SparseLongArray();
    byte b;
    int i;
    for (b = 0, i = paramList.size(); b < i; b++, l = l1) {
      BatteryStats.Uid uid = ((BatterySipper)paramList.get(b)).uidObj;
      long l1 = l;
      if (uid != null) {
        l1 = getProcessForegroundTimeMs(uid, 0);
        sparseLongArray.put(uid.getUid(), l1);
        l1 = l + l1;
      } 
    } 
    if (paramBatterySipper != null && l >= 600000L) {
      double d = paramBatterySipper.totalPowerMah;
      for (b = 0, i = paramList.size(); b < i; b++) {
        paramBatterySipper = paramList.get(b);
        paramBatterySipper.screenPowerMah = sparseLongArray.get(paramBatterySipper.getUid(), 0L) * d / l;
      } 
    } 
  }
  
  public boolean shouldHideSipper(BatterySipper paramBatterySipper) {
    BatterySipper.DrainType drainType = paramBatterySipper.drainType;
    return (drainType != BatterySipper.DrainType.IDLE && drainType != BatterySipper.DrainType.CELL && drainType != BatterySipper.DrainType.SCREEN && drainType != BatterySipper.DrainType.AMBIENT_DISPLAY && drainType != BatterySipper.DrainType.UNACCOUNTED && drainType != BatterySipper.DrainType.OVERCOUNTED) ? (




      
      (isTypeService(paramBatterySipper) || 
      isTypeSystem(paramBatterySipper))) : true;
  }
  
  public boolean isTypeService(BatterySipper paramBatterySipper) {
    String[] arrayOfString = this.mPackageManager.getPackagesForUid(paramBatterySipper.getUid());
    if (arrayOfString == null)
      return false; 
    int i;
    byte b;
    for (i = arrayOfString.length, b = 0; b < i; ) {
      String str = arrayOfString[b];
      if (ArrayUtils.contains((Object[])this.mServicepackageArray, str))
        return true; 
      b++;
    } 
    return false;
  }
  
  public boolean isTypeSystem(BatterySipper paramBatterySipper) {
    int i;
    if (paramBatterySipper.uidObj == null) {
      i = -1;
    } else {
      i = paramBatterySipper.getUid();
    } 
    paramBatterySipper.mPackages = this.mPackageManager.getPackagesForUid(i);
    if (i >= 0 && i < 10000)
      return true; 
    if (paramBatterySipper.mPackages != null)
      for (String str : paramBatterySipper.mPackages) {
        if (ArrayUtils.contains((Object[])this.mSystemPackageArray, str))
          return true; 
      }  
    return false;
  }
  
  public long convertUsToMs(long paramLong) {
    return paramLong / 1000L;
  }
  
  public long convertMsToUs(long paramLong) {
    return 1000L * paramLong;
  }
  
  public long getForegroundActivityTotalTimeUs(BatteryStats.Uid paramUid, long paramLong) {
    BatteryStats.Timer timer = paramUid.getForegroundActivityTimer();
    if (timer != null)
      return timer.getTotalTimeLocked(paramLong, 0); 
    return 0L;
  }
  
  public long getProcessForegroundTimeMs(BatteryStats.Uid paramUid, int paramInt) {
    long l1 = convertMsToUs(SystemClock.elapsedRealtime());
    int[] arrayOfInt = new int[1];
    byte b = 0;
    arrayOfInt[0] = 0;
    long l2 = 0L;
    for (int i = arrayOfInt.length; b < i; ) {
      int j = arrayOfInt[b];
      long l = paramUid.getProcessStateTime(j, l1, paramInt);
      l2 += l;
      b++;
    } 
    l2 = Math.min(l2, getForegroundActivityTotalTimeUs(paramUid, l1));
    return convertUsToMs(l2);
  }
  
  public void setPackageManager(PackageManager paramPackageManager) {
    this.mPackageManager = paramPackageManager;
  }
  
  public void setSystemPackageArray(String[] paramArrayOfString) {
    this.mSystemPackageArray = paramArrayOfString;
  }
  
  public void setServicePackageArray(String[] paramArrayOfString) {
    this.mServicepackageArray = paramArrayOfString;
  }
  
  private void load() {
    IBatteryStats iBatteryStats = this.mBatteryInfo;
    if (iBatteryStats == null)
      return; 
    this.mStats = getStats(iBatteryStats);
    if (this.mCollectBatteryBroadcast)
      this.mBatteryBroadcast = this.mContext.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED")); 
  }
  
  private static BatteryStatsImpl getStats(IBatteryStats paramIBatteryStats) {
    try {
      ParcelFileDescriptor parcelFileDescriptor = paramIBatteryStats.getStatisticsStream();
      if (parcelFileDescriptor != null)
        try {
          ParcelFileDescriptor.AutoCloseInputStream autoCloseInputStream = new ParcelFileDescriptor.AutoCloseInputStream();
          this(parcelFileDescriptor);
          try {
            byte[] arrayOfByte = readFully((FileInputStream)autoCloseInputStream, MemoryFile.getSize(parcelFileDescriptor.getFileDescriptor()));
            Parcel parcel = Parcel.obtain();
            parcel.unmarshall(arrayOfByte, 0, arrayOfByte.length);
            parcel.setDataPosition(0);
            Parcelable.Creator<BatteryStatsImpl> creator = BatteryStatsImpl.CREATOR;
            return (BatteryStatsImpl)creator.createFromParcel(parcel);
          } finally {
            try {
              autoCloseInputStream.close();
            } finally {
              autoCloseInputStream = null;
            } 
          } 
        } catch (IOException iOException) {
          Log.w(TAG, "Unable to read statistics stream", iOException);
        }  
    } catch (RemoteException remoteException) {
      Log.w(TAG, "RemoteException:", (Throwable)remoteException);
    } 
    return new BatteryStatsImpl();
  }
}
