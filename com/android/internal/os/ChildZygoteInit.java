package com.android.internal.os;

import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.util.Log;

public class ChildZygoteInit {
  private static final String TAG = "ChildZygoteInit";
  
  static String parseSocketNameFromArgs(String[] paramArrayOfString) {
    int i;
    byte b;
    for (i = paramArrayOfString.length, b = 0; b < i; ) {
      String str = paramArrayOfString[b];
      if (str.startsWith("--zygote-socket="))
        return str.substring("--zygote-socket=".length()); 
      b++;
    } 
    return null;
  }
  
  static String parseAbiListFromArgs(String[] paramArrayOfString) {
    int i;
    byte b;
    for (i = paramArrayOfString.length, b = 0; b < i; ) {
      String str = paramArrayOfString[b];
      if (str.startsWith("--abi-list="))
        return str.substring("--abi-list=".length()); 
      b++;
    } 
    return null;
  }
  
  static int parseIntFromArg(String[] paramArrayOfString, String paramString) {
    int i = -1;
    int j;
    byte b;
    for (j = paramArrayOfString.length, b = 0; b < j; ) {
      String str = paramArrayOfString[b];
      if (str.startsWith(paramString)) {
        str = str.substring(str.indexOf('=') + 1);
        try {
          i = Integer.parseInt(str);
        } catch (NumberFormatException numberFormatException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Invalid int argument: ");
          stringBuilder.append(str);
          throw new IllegalArgumentException(stringBuilder.toString(), numberFormatException);
        } 
      } 
      b++;
    } 
    return i;
  }
  
  static void runZygoteServer(ZygoteServer paramZygoteServer, String[] paramArrayOfString) {
    String str = parseSocketNameFromArgs(paramArrayOfString);
    if (str != null) {
      String str1 = parseAbiListFromArgs(paramArrayOfString);
      if (str1 != null)
        try {
          Os.prctl(OsConstants.PR_SET_NO_NEW_PRIVS, 1L, 0L, 0L, 0L);
          int i = parseIntFromArg(paramArrayOfString, "--uid-range-start=");
          int j = parseIntFromArg(paramArrayOfString, "--uid-range-end=");
          if (i != -1 && j != -1) {
            if (i <= j) {
              if (i >= 90000) {
                Zygote.nativeInstallSeccompUidGidFilter(i, j);
                try {
                  paramZygoteServer.registerServerSocketAtAbstractName(str);
                  StringBuilder stringBuilder = new StringBuilder();
                  this();
                  stringBuilder.append("ABSTRACT/");
                  stringBuilder.append(str);
                  Zygote.nativeAllowFileAcrossFork(stringBuilder.toString());
                  Runnable runnable = paramZygoteServer.runSelectLoop(str1);
                  paramZygoteServer.closeServerSocket();
                  if (runnable != null)
                    runnable.run(); 
                  return;
                } catch (RuntimeException runtimeException) {
                  Log.e("ChildZygoteInit", "Fatal exception:", runtimeException);
                  throw runtimeException;
                } finally {}
                paramZygoteServer.closeServerSocket();
                throw paramArrayOfString;
              } 
              throw new RuntimeException("Passed in UID range does not map to isolated processes.");
            } 
            throw new RuntimeException("Passed in UID range is invalid, min > max.");
          } 
          throw new RuntimeException("Couldn't parse UID range start/end");
        } catch (ErrnoException errnoException) {
          throw new RuntimeException("Failed to set PR_SET_NO_NEW_PRIVS", errnoException);
        }  
      throw new NullPointerException("No abiList specified");
    } 
    throw new NullPointerException("No socketName specified");
  }
}
