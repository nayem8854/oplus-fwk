package com.android.internal.os;

import android.os.Handler;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.ProxyFileDescriptorCallback;
import android.system.ErrnoException;
import android.system.OsConstants;
import android.util.Log;
import android.util.SparseArray;
import com.android.internal.util.Preconditions;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ThreadFactory;

public class FuseAppLoop implements Handler.Callback {
  private static final boolean DEBUG = Log.isLoggable("FuseAppLoop", 3);
  
  private static final ThreadFactory sDefaultThreadFactory = (ThreadFactory)new Object();
  
  private final Object mLock = new Object();
  
  private final SparseArray<CallbackEntry> mCallbackMap = new SparseArray<>();
  
  private final BytesMap mBytesMap = new BytesMap();
  
  private final LinkedList<Args> mArgsPool = new LinkedList<>();
  
  private int mNextInode = 2;
  
  private static final int ARGS_POOL_SIZE = 50;
  
  private static final int FUSE_FSYNC = 20;
  
  private static final int FUSE_GETATTR = 3;
  
  private static final int FUSE_LOOKUP = 1;
  
  private static final int FUSE_MAX_WRITE = 131072;
  
  private static final int FUSE_OK = 0;
  
  private static final int FUSE_OPEN = 14;
  
  private static final int FUSE_READ = 15;
  
  private static final int FUSE_RELEASE = 18;
  
  private static final int FUSE_WRITE = 16;
  
  private static final int MIN_INODE = 2;
  
  public static final int ROOT_INODE = 1;
  
  private static final String TAG = "FuseAppLoop";
  
  private long mInstance;
  
  private final int mMountPointId;
  
  private final Thread mThread;
  
  public FuseAppLoop(int paramInt, ParcelFileDescriptor paramParcelFileDescriptor, ThreadFactory paramThreadFactory) {
    this.mMountPointId = paramInt;
    ThreadFactory threadFactory = paramThreadFactory;
    if (paramThreadFactory == null)
      threadFactory = sDefaultThreadFactory; 
    this.mInstance = native_new(paramParcelFileDescriptor.detachFd());
    Thread thread = threadFactory.newThread(new _$$Lambda$FuseAppLoop$e9Yru2f_btesWlxIgerkPnHibpg(this));
    thread.start();
  }
  
  public int registerCallback(ProxyFileDescriptorCallback paramProxyFileDescriptorCallback, Handler paramHandler) throws FuseUnavailableMountException {
    synchronized (this.mLock) {
      Objects.requireNonNull(paramProxyFileDescriptorCallback);
      Objects.requireNonNull(paramHandler);
      SparseArray<CallbackEntry> sparseArray = this.mCallbackMap;
      int i = sparseArray.size();
      boolean bool1 = false;
      if (i < 2147483645) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      Preconditions.checkState(bool2, "Too many opened files.");
      boolean bool2 = bool1;
      if (Thread.currentThread().getId() != paramHandler.getLooper().getThread().getId())
        bool2 = true; 
      Preconditions.checkArgument(bool2, "Handler must be different from the current thread");
      if (this.mInstance != 0L) {
        do {
          i = this.mNextInode;
          int j = this.mNextInode + 1;
          if (j >= 0)
            continue; 
          this.mNextInode = 2;
        } while (this.mCallbackMap.get(i) != null);
        sparseArray = this.mCallbackMap;
        CallbackEntry callbackEntry = new CallbackEntry();
        Handler handler = new Handler();
        this(paramHandler.getLooper(), this);
        this(paramProxyFileDescriptorCallback, handler);
        sparseArray.put(i, callbackEntry);
        return i;
      } 
      FuseUnavailableMountException fuseUnavailableMountException = new FuseUnavailableMountException();
      this(this.mMountPointId);
      throw fuseUnavailableMountException;
    } 
  }
  
  public void unregisterCallback(int paramInt) {
    synchronized (this.mLock) {
      this.mCallbackMap.remove(paramInt);
      return;
    } 
  }
  
  public int getMountPointId() {
    return this.mMountPointId;
  }
  
  public boolean handleMessage(Message paramMessage) {
    // Byte code:
    //   0: aload_1
    //   1: getfield obj : Ljava/lang/Object;
    //   4: checkcast com/android/internal/os/FuseAppLoop$Args
    //   7: astore_2
    //   8: aload_2
    //   9: getfield entry : Lcom/android/internal/os/FuseAppLoop$CallbackEntry;
    //   12: astore_3
    //   13: aload_2
    //   14: getfield inode : J
    //   17: lstore #4
    //   19: aload_2
    //   20: getfield unique : J
    //   23: lstore #6
    //   25: aload_2
    //   26: getfield size : I
    //   29: istore #8
    //   31: aload_2
    //   32: getfield offset : J
    //   35: lstore #9
    //   37: aload_2
    //   38: getfield data : [B
    //   41: astore #11
    //   43: aload_1
    //   44: getfield what : I
    //   47: istore #12
    //   49: iload #12
    //   51: iconst_1
    //   52: if_icmpeq -> 774
    //   55: iload #12
    //   57: iconst_3
    //   58: if_icmpeq -> 657
    //   61: iload #12
    //   63: bipush #18
    //   65: if_icmpeq -> 551
    //   68: iload #12
    //   70: bipush #20
    //   72: if_icmpeq -> 493
    //   75: iload #12
    //   77: bipush #15
    //   79: if_icmpeq -> 391
    //   82: iload #12
    //   84: bipush #16
    //   86: if_icmpne -> 253
    //   89: aload_3
    //   90: getfield callback : Landroid/os/ProxyFileDescriptorCallback;
    //   93: lload #9
    //   95: iload #8
    //   97: aload #11
    //   99: invokevirtual onWrite : (JI[B)I
    //   102: istore #12
    //   104: aload_0
    //   105: getfield mLock : Ljava/lang/Object;
    //   108: astore #13
    //   110: aload #13
    //   112: monitorenter
    //   113: aload_0
    //   114: getfield mInstance : J
    //   117: lconst_0
    //   118: lcmp
    //   119: ifeq -> 154
    //   122: aload_0
    //   123: getfield mInstance : J
    //   126: lstore #14
    //   128: aload #13
    //   130: astore_3
    //   131: iconst_1
    //   132: istore #16
    //   134: lload #4
    //   136: lstore #9
    //   138: aload #11
    //   140: astore_1
    //   141: aload_0
    //   142: lload #14
    //   144: lload #6
    //   146: iload #12
    //   148: invokevirtual native_replyWrite : (JJI)V
    //   151: goto -> 154
    //   154: iconst_1
    //   155: istore #17
    //   157: aload #11
    //   159: astore_1
    //   160: aload #13
    //   162: astore_3
    //   163: iload #17
    //   165: istore #16
    //   167: lload #4
    //   169: lstore #9
    //   171: aload_0
    //   172: aload_2
    //   173: invokespecial recycleLocked : (Lcom/android/internal/os/FuseAppLoop$Args;)V
    //   176: aload #11
    //   178: astore_1
    //   179: aload #13
    //   181: astore_3
    //   182: iload #17
    //   184: istore #16
    //   186: lload #4
    //   188: lstore #9
    //   190: aload #13
    //   192: monitorexit
    //   193: goto -> 855
    //   196: astore_1
    //   197: iconst_1
    //   198: istore #17
    //   200: aload_1
    //   201: astore #18
    //   203: aload #11
    //   205: astore_1
    //   206: aload #13
    //   208: astore_3
    //   209: iload #17
    //   211: istore #16
    //   213: lload #4
    //   215: lstore #9
    //   217: aload #13
    //   219: monitorexit
    //   220: iload #17
    //   222: istore #16
    //   224: aload #18
    //   226: athrow
    //   227: astore #18
    //   229: aload_1
    //   230: astore #11
    //   232: aload_3
    //   233: astore #13
    //   235: iload #16
    //   237: istore #17
    //   239: lload #9
    //   241: lstore #4
    //   243: goto -> 203
    //   246: astore_1
    //   247: iconst_1
    //   248: istore #16
    //   250: goto -> 894
    //   253: iconst_1
    //   254: istore #17
    //   256: lload #4
    //   258: lstore #9
    //   260: aload #11
    //   262: astore_3
    //   263: aload_3
    //   264: astore #11
    //   266: iload #17
    //   268: istore #16
    //   270: lload #9
    //   272: lstore #4
    //   274: new java/lang/IllegalArgumentException
    //   277: astore #13
    //   279: aload_3
    //   280: astore #11
    //   282: iload #17
    //   284: istore #16
    //   286: lload #9
    //   288: lstore #4
    //   290: new java/lang/StringBuilder
    //   293: astore #18
    //   295: aload_3
    //   296: astore #11
    //   298: iload #17
    //   300: istore #16
    //   302: lload #9
    //   304: lstore #4
    //   306: aload #18
    //   308: invokespecial <init> : ()V
    //   311: aload_3
    //   312: astore #11
    //   314: iload #17
    //   316: istore #16
    //   318: lload #9
    //   320: lstore #4
    //   322: aload #18
    //   324: ldc_w 'Unknown FUSE command: '
    //   327: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   330: pop
    //   331: aload_3
    //   332: astore #11
    //   334: iload #17
    //   336: istore #16
    //   338: lload #9
    //   340: lstore #4
    //   342: aload #18
    //   344: aload_1
    //   345: getfield what : I
    //   348: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   351: pop
    //   352: aload_3
    //   353: astore #11
    //   355: iload #17
    //   357: istore #16
    //   359: lload #9
    //   361: lstore #4
    //   363: aload #13
    //   365: aload #18
    //   367: invokevirtual toString : ()Ljava/lang/String;
    //   370: invokespecial <init> : (Ljava/lang/String;)V
    //   373: aload_3
    //   374: astore #11
    //   376: iload #17
    //   378: istore #16
    //   380: lload #9
    //   382: lstore #4
    //   384: aload #13
    //   386: athrow
    //   387: astore_1
    //   388: goto -> 894
    //   391: iconst_1
    //   392: istore #16
    //   394: aload_3
    //   395: getfield callback : Landroid/os/ProxyFileDescriptorCallback;
    //   398: astore_1
    //   399: aload_1
    //   400: lload #9
    //   402: iload #8
    //   404: aload #11
    //   406: invokevirtual onRead : (JI[B)I
    //   409: istore #12
    //   411: aload_0
    //   412: getfield mLock : Ljava/lang/Object;
    //   415: astore_3
    //   416: aload_3
    //   417: monitorenter
    //   418: aload_0
    //   419: getfield mInstance : J
    //   422: lconst_0
    //   423: lcmp
    //   424: ifeq -> 450
    //   427: aload_0
    //   428: getfield mInstance : J
    //   431: lstore #4
    //   433: aload_3
    //   434: astore_1
    //   435: aload_0
    //   436: lload #4
    //   438: lload #6
    //   440: iload #12
    //   442: aload #11
    //   444: invokevirtual native_replyRead : (JJI[B)V
    //   447: goto -> 450
    //   450: aload_3
    //   451: astore_1
    //   452: aload_0
    //   453: aload_2
    //   454: invokespecial recycleLocked : (Lcom/android/internal/os/FuseAppLoop$Args;)V
    //   457: aload_3
    //   458: astore_1
    //   459: aload_3
    //   460: monitorexit
    //   461: goto -> 855
    //   464: astore_1
    //   465: aload_3
    //   466: astore #11
    //   468: aload_1
    //   469: astore_3
    //   470: aload #11
    //   472: astore_1
    //   473: aload #11
    //   475: monitorexit
    //   476: aload_3
    //   477: athrow
    //   478: astore_3
    //   479: aload_1
    //   480: astore #11
    //   482: goto -> 470
    //   485: astore_1
    //   486: goto -> 894
    //   489: astore_1
    //   490: goto -> 894
    //   493: aload_3
    //   494: getfield callback : Landroid/os/ProxyFileDescriptorCallback;
    //   497: invokevirtual onFsync : ()V
    //   500: aload_0
    //   501: getfield mLock : Ljava/lang/Object;
    //   504: astore_1
    //   505: aload_1
    //   506: monitorenter
    //   507: aload_0
    //   508: getfield mInstance : J
    //   511: lconst_0
    //   512: lcmp
    //   513: ifeq -> 527
    //   516: aload_0
    //   517: aload_0
    //   518: getfield mInstance : J
    //   521: lload #6
    //   523: iconst_0
    //   524: invokevirtual native_replySimple : (JJI)V
    //   527: aload_0
    //   528: aload_2
    //   529: invokespecial recycleLocked : (Lcom/android/internal/os/FuseAppLoop$Args;)V
    //   532: aload_1
    //   533: monitorexit
    //   534: goto -> 855
    //   537: astore #11
    //   539: aload_1
    //   540: monitorexit
    //   541: aload #11
    //   543: athrow
    //   544: iconst_1
    //   545: istore #16
    //   547: astore_1
    //   548: goto -> 894
    //   551: iconst_1
    //   552: istore #16
    //   554: aload_3
    //   555: getfield callback : Landroid/os/ProxyFileDescriptorCallback;
    //   558: invokevirtual onRelease : ()V
    //   561: aload_0
    //   562: getfield mLock : Ljava/lang/Object;
    //   565: astore #11
    //   567: aload #11
    //   569: monitorenter
    //   570: aload_0
    //   571: getfield mInstance : J
    //   574: lstore #9
    //   576: lload #9
    //   578: lconst_0
    //   579: lcmp
    //   580: ifeq -> 601
    //   583: aload_0
    //   584: aload_0
    //   585: getfield mInstance : J
    //   588: lload #6
    //   590: iconst_0
    //   591: invokevirtual native_replySimple : (JJI)V
    //   594: goto -> 601
    //   597: astore_1
    //   598: goto -> 640
    //   601: aload_0
    //   602: getfield mBytesMap : Lcom/android/internal/os/FuseAppLoop$BytesMap;
    //   605: astore_1
    //   606: lload #4
    //   608: lstore #9
    //   610: lload #9
    //   612: lstore #4
    //   614: aload_1
    //   615: lload #9
    //   617: invokevirtual stopUsing : (J)V
    //   620: lload #9
    //   622: lstore #4
    //   624: aload_0
    //   625: aload_2
    //   626: invokespecial recycleLocked : (Lcom/android/internal/os/FuseAppLoop$Args;)V
    //   629: lload #9
    //   631: lstore #4
    //   633: aload #11
    //   635: monitorexit
    //   636: goto -> 855
    //   639: astore_1
    //   640: aload #11
    //   642: monitorexit
    //   643: aload_1
    //   644: athrow
    //   645: astore_1
    //   646: goto -> 894
    //   649: astore_1
    //   650: goto -> 640
    //   653: astore_1
    //   654: goto -> 894
    //   657: iconst_1
    //   658: istore #16
    //   660: aload_3
    //   661: getfield callback : Landroid/os/ProxyFileDescriptorCallback;
    //   664: invokevirtual onGetSize : ()J
    //   667: lstore #14
    //   669: aload_0
    //   670: getfield mLock : Ljava/lang/Object;
    //   673: astore #11
    //   675: aload #11
    //   677: monitorenter
    //   678: aload_0
    //   679: getfield mInstance : J
    //   682: lconst_0
    //   683: lcmp
    //   684: ifeq -> 715
    //   687: aload_0
    //   688: getfield mInstance : J
    //   691: lstore #19
    //   693: aload #11
    //   695: astore_1
    //   696: lload #6
    //   698: lstore #9
    //   700: aload_0
    //   701: lload #19
    //   703: lload #6
    //   705: lload #4
    //   707: lload #14
    //   709: invokevirtual native_replyGetAttr : (JJJJ)V
    //   712: goto -> 715
    //   715: aload #11
    //   717: astore_1
    //   718: lload #6
    //   720: lstore #9
    //   722: aload_0
    //   723: aload_2
    //   724: invokespecial recycleLocked : (Lcom/android/internal/os/FuseAppLoop$Args;)V
    //   727: aload #11
    //   729: astore_1
    //   730: lload #6
    //   732: lstore #9
    //   734: aload #11
    //   736: monitorexit
    //   737: goto -> 855
    //   740: astore_1
    //   741: aload_1
    //   742: astore_3
    //   743: aload #11
    //   745: astore_1
    //   746: lload #6
    //   748: lstore #9
    //   750: aload #11
    //   752: monitorexit
    //   753: aload_3
    //   754: athrow
    //   755: astore_3
    //   756: aload_1
    //   757: astore #11
    //   759: lload #9
    //   761: lstore #6
    //   763: goto -> 743
    //   766: astore_1
    //   767: goto -> 894
    //   770: astore_1
    //   771: goto -> 894
    //   774: lload #6
    //   776: lstore #9
    //   778: lload #9
    //   780: lstore #6
    //   782: aload_3
    //   783: getfield callback : Landroid/os/ProxyFileDescriptorCallback;
    //   786: invokevirtual onGetSize : ()J
    //   789: lstore #14
    //   791: lload #9
    //   793: lstore #6
    //   795: aload_0
    //   796: getfield mLock : Ljava/lang/Object;
    //   799: astore #11
    //   801: lload #9
    //   803: lstore #6
    //   805: aload #11
    //   807: monitorenter
    //   808: aload_0
    //   809: getfield mInstance : J
    //   812: lconst_0
    //   813: lcmp
    //   814: ifeq -> 841
    //   817: aload_0
    //   818: getfield mInstance : J
    //   821: lstore #6
    //   823: aload #11
    //   825: astore_1
    //   826: aload_0
    //   827: lload #6
    //   829: lload #9
    //   831: lload #4
    //   833: lload #14
    //   835: invokevirtual native_replyLookup : (JJJJ)V
    //   838: goto -> 841
    //   841: aload #11
    //   843: astore_1
    //   844: aload_0
    //   845: aload_2
    //   846: invokespecial recycleLocked : (Lcom/android/internal/os/FuseAppLoop$Args;)V
    //   849: aload #11
    //   851: astore_1
    //   852: aload #11
    //   854: monitorexit
    //   855: iconst_1
    //   856: istore #16
    //   858: goto -> 935
    //   861: astore_1
    //   862: aload_1
    //   863: astore_3
    //   864: aload #11
    //   866: astore_1
    //   867: aload #11
    //   869: monitorexit
    //   870: lload #9
    //   872: lstore #6
    //   874: aload_3
    //   875: athrow
    //   876: astore_3
    //   877: aload_1
    //   878: astore #11
    //   880: goto -> 864
    //   883: iconst_1
    //   884: istore #16
    //   886: astore_1
    //   887: goto -> 894
    //   890: astore_1
    //   891: iconst_1
    //   892: istore #16
    //   894: aload_0
    //   895: getfield mLock : Ljava/lang/Object;
    //   898: astore #11
    //   900: aload #11
    //   902: monitorenter
    //   903: ldc 'FuseAppLoop'
    //   905: ldc_w ''
    //   908: aload_1
    //   909: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   912: pop
    //   913: aload_1
    //   914: invokestatic getError : (Ljava/lang/Exception;)I
    //   917: istore #12
    //   919: aload_0
    //   920: lload #6
    //   922: iload #12
    //   924: invokespecial replySimpleLocked : (JI)V
    //   927: aload_0
    //   928: aload_2
    //   929: invokespecial recycleLocked : (Lcom/android/internal/os/FuseAppLoop$Args;)V
    //   932: aload #11
    //   934: monitorexit
    //   935: iload #16
    //   937: ireturn
    //   938: astore_1
    //   939: aload #11
    //   941: monitorexit
    //   942: aload_1
    //   943: athrow
    //   944: astore_1
    //   945: goto -> 939
    // Line number table:
    //   Java source line number -> byte code offset
    //   #149	-> 0
    //   #150	-> 8
    //   #151	-> 13
    //   #152	-> 19
    //   #153	-> 25
    //   #154	-> 31
    //   #155	-> 37
    //   #158	-> 43
    //   #190	-> 89
    //   #191	-> 104
    //   #192	-> 113
    //   #193	-> 122
    //   #192	-> 154
    //   #195	-> 154
    //   #196	-> 176
    //   #197	-> 193
    //   #196	-> 196
    //   #220	-> 246
    //   #218	-> 253
    //   #220	-> 387
    //   #180	-> 391
    //   #182	-> 411
    //   #183	-> 418
    //   #184	-> 427
    //   #183	-> 450
    //   #186	-> 450
    //   #187	-> 457
    //   #188	-> 461
    //   #187	-> 464
    //   #220	-> 485
    //   #199	-> 493
    //   #200	-> 500
    //   #201	-> 507
    //   #202	-> 516
    //   #204	-> 527
    //   #205	-> 532
    //   #206	-> 534
    //   #205	-> 537
    //   #220	-> 544
    //   #208	-> 551
    //   #209	-> 561
    //   #210	-> 570
    //   #211	-> 583
    //   #215	-> 597
    //   #213	-> 601
    //   #214	-> 620
    //   #215	-> 629
    //   #216	-> 636
    //   #215	-> 639
    //   #220	-> 645
    //   #215	-> 649
    //   #220	-> 653
    //   #170	-> 657
    //   #171	-> 669
    //   #172	-> 678
    //   #173	-> 687
    //   #172	-> 715
    //   #175	-> 715
    //   #176	-> 727
    //   #177	-> 737
    //   #176	-> 740
    //   #220	-> 766
    //   #160	-> 774
    //   #161	-> 791
    //   #162	-> 808
    //   #163	-> 817
    //   #162	-> 841
    //   #165	-> 841
    //   #166	-> 849
    //   #167	-> 855
    //   #226	-> 855
    //   #166	-> 861
    //   #220	-> 883
    //   #221	-> 894
    //   #222	-> 903
    //   #223	-> 913
    //   #224	-> 927
    //   #225	-> 932
    //   #228	-> 935
    //   #225	-> 938
    // Exception table:
    //   from	to	target	type
    //   43	49	890	java/lang/Exception
    //   89	104	246	java/lang/Exception
    //   104	113	246	java/lang/Exception
    //   113	122	196	finally
    //   122	128	196	finally
    //   141	151	227	finally
    //   171	176	227	finally
    //   190	193	227	finally
    //   217	220	227	finally
    //   224	227	387	java/lang/Exception
    //   274	279	387	java/lang/Exception
    //   290	295	387	java/lang/Exception
    //   306	311	387	java/lang/Exception
    //   322	331	387	java/lang/Exception
    //   342	352	387	java/lang/Exception
    //   363	373	387	java/lang/Exception
    //   384	387	387	java/lang/Exception
    //   394	399	489	java/lang/Exception
    //   399	411	485	java/lang/Exception
    //   411	418	485	java/lang/Exception
    //   418	427	464	finally
    //   427	433	464	finally
    //   435	447	478	finally
    //   452	457	478	finally
    //   459	461	478	finally
    //   473	476	478	finally
    //   476	478	544	java/lang/Exception
    //   493	500	544	java/lang/Exception
    //   500	507	544	java/lang/Exception
    //   507	516	537	finally
    //   516	527	537	finally
    //   527	532	537	finally
    //   532	534	537	finally
    //   539	541	537	finally
    //   541	544	544	java/lang/Exception
    //   554	561	653	java/lang/Exception
    //   561	570	653	java/lang/Exception
    //   570	576	639	finally
    //   583	594	597	finally
    //   601	606	639	finally
    //   614	620	649	finally
    //   624	629	649	finally
    //   633	636	649	finally
    //   640	643	649	finally
    //   643	645	645	java/lang/Exception
    //   660	669	770	java/lang/Exception
    //   669	678	766	java/lang/Exception
    //   678	687	740	finally
    //   687	693	740	finally
    //   700	712	755	finally
    //   722	727	755	finally
    //   734	737	755	finally
    //   750	753	755	finally
    //   753	755	883	java/lang/Exception
    //   782	791	883	java/lang/Exception
    //   795	801	883	java/lang/Exception
    //   805	808	883	java/lang/Exception
    //   808	817	861	finally
    //   817	823	861	finally
    //   826	838	876	finally
    //   844	849	876	finally
    //   852	855	876	finally
    //   867	870	876	finally
    //   874	876	883	java/lang/Exception
    //   903	913	938	finally
    //   913	919	938	finally
    //   919	927	944	finally
    //   927	932	944	finally
    //   932	935	944	finally
    //   939	942	944	finally
  }
  
  private void onCommand(int paramInt1, long paramLong1, long paramLong2, long paramLong3, int paramInt2, byte[] paramArrayOfbyte) {
    Object object = this.mLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      Args args;
      if (this.mArgsPool.size() == 0) {
        args = new Args();
        this();
      } else {
        args = this.mArgsPool.pop();
      } 
      args.unique = paramLong1;
      args.inode = paramLong2;
      args.offset = paramLong3;
      args.size = paramInt2;
      args.data = paramArrayOfbyte;
      args.entry = getCallbackEntryOrThrowLocked(paramLong2);
      Handler handler1 = args.entry.handler, handler2 = args.entry.handler;
      Message message = Message.obtain(handler2, paramInt1, 0, 0, args);
      if (!handler1.sendMessage(message)) {
        ErrnoException errnoException = new ErrnoException();
        this("onCommand", OsConstants.EBADF);
        throw errnoException;
      } 
    } catch (Exception exception) {
      replySimpleLocked(paramLong1, getError(exception));
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
  }
  
  private byte[] onOpen(long paramLong1, long paramLong2) {
    Object object = this.mLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      CallbackEntry callbackEntry = getCallbackEntryOrThrowLocked(paramLong2);
      if (!callbackEntry.opened) {
        if (this.mInstance != 0L) {
          native_replyOpen(this.mInstance, paramLong1, paramLong2);
          callbackEntry.opened = true;
          byte[] arrayOfByte = this.mBytesMap.startUsing(paramLong2);
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          return arrayOfByte;
        } 
      } else {
        ErrnoException errnoException = new ErrnoException();
        this("onOpen", OsConstants.EMFILE);
        throw errnoException;
      } 
    } catch (ErrnoException errnoException) {
      replySimpleLocked(paramLong1, getError((Exception)errnoException));
    } finally {
      Exception exception;
    } 
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    return null;
  }
  
  private static int getError(Exception paramException) {
    if (paramException instanceof ErrnoException) {
      int i = ((ErrnoException)paramException).errno;
      if (i != OsConstants.ENOSYS)
        return -i; 
    } 
    return -OsConstants.EBADF;
  }
  
  private CallbackEntry getCallbackEntryOrThrowLocked(long paramLong) throws ErrnoException {
    CallbackEntry callbackEntry = this.mCallbackMap.get(checkInode(paramLong));
    if (callbackEntry != null)
      return callbackEntry; 
    throw new ErrnoException("getCallbackEntryOrThrowLocked", OsConstants.ENOENT);
  }
  
  private void recycleLocked(Args paramArgs) {
    if (this.mArgsPool.size() < 50)
      this.mArgsPool.add(paramArgs); 
  }
  
  private void replySimpleLocked(long paramLong, int paramInt) {
    long l = this.mInstance;
    if (l != 0L)
      native_replySimple(l, paramLong, paramInt); 
  }
  
  private static int checkInode(long paramLong) {
    Preconditions.checkArgumentInRange(paramLong, 2L, 2147483647L, "checkInode");
    return (int)paramLong;
  }
  
  native void native_delete(long paramLong);
  
  native long native_new(int paramInt);
  
  native void native_replyGetAttr(long paramLong1, long paramLong2, long paramLong3, long paramLong4);
  
  native void native_replyLookup(long paramLong1, long paramLong2, long paramLong3, long paramLong4);
  
  native void native_replyOpen(long paramLong1, long paramLong2, long paramLong3);
  
  native void native_replyRead(long paramLong1, long paramLong2, int paramInt, byte[] paramArrayOfbyte);
  
  native void native_replySimple(long paramLong1, long paramLong2, int paramInt);
  
  native void native_replyWrite(long paramLong1, long paramLong2, int paramInt);
  
  native void native_start(long paramLong);
  
  class UnmountedException extends Exception {}
  
  class CallbackEntry {
    final ProxyFileDescriptorCallback callback;
    
    final Handler handler;
    
    boolean opened;
    
    CallbackEntry(FuseAppLoop this$0, Handler param1Handler) {
      Objects.requireNonNull(this$0);
      this.callback = (ProxyFileDescriptorCallback)this$0;
      Objects.requireNonNull(param1Handler);
      this.handler = param1Handler;
    }
    
    long getThreadId() {
      return this.handler.getLooper().getThread().getId();
    }
  }
  
  class BytesMapEntry {
    private BytesMapEntry() {}
    
    int counter = 0;
    
    byte[] bytes = new byte[131072];
  }
  
  class BytesMap {
    final Map<Long, FuseAppLoop.BytesMapEntry> mEntries = new HashMap<>();
    
    byte[] startUsing(long param1Long) {
      FuseAppLoop.BytesMapEntry bytesMapEntry1 = this.mEntries.get(Long.valueOf(param1Long));
      FuseAppLoop.BytesMapEntry bytesMapEntry2 = bytesMapEntry1;
      if (bytesMapEntry1 == null) {
        bytesMapEntry2 = new FuseAppLoop.BytesMapEntry();
        this.mEntries.put(Long.valueOf(param1Long), bytesMapEntry2);
      } 
      bytesMapEntry2.counter++;
      return bytesMapEntry2.bytes;
    }
    
    void stopUsing(long param1Long) {
      FuseAppLoop.BytesMapEntry bytesMapEntry = this.mEntries.get(Long.valueOf(param1Long));
      Objects.requireNonNull(bytesMapEntry);
      bytesMapEntry.counter--;
      if (bytesMapEntry.counter <= 0)
        this.mEntries.remove(Long.valueOf(param1Long)); 
    }
    
    void clear() {
      this.mEntries.clear();
    }
    
    private BytesMap() {}
  }
  
  class Args {
    byte[] data;
    
    FuseAppLoop.CallbackEntry entry;
    
    long inode;
    
    long offset;
    
    int size;
    
    long unique;
    
    private Args() {}
  }
}
