package com.android.internal;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.MathUtils;
import java.util.LinkedList;
import java.util.Queue;

public class BrightnessSynchronizer {
  private static final Uri BRIGHTNESS_FLOAT_URI;
  
  private static final Uri BRIGHTNESS_URI = Settings.System.getUriFor("screen_brightness");
  
  public static final float EPSILON = 0.001F;
  
  private static final int MSG_UPDATE_FLOAT = 1;
  
  private static final int MSG_UPDATE_INT = 2;
  
  private static final String TAG = "BrightnessSynchronizer";
  
  private final Context mContext;
  
  private final Handler mHandler;
  
  private float mPreferredSettingValue;
  
  static {
    BRIGHTNESS_FLOAT_URI = Settings.System.getUriFor("screen_brightness_float");
  }
  
  private final Queue<Object> mWriteHistory = new LinkedList();
  
  public BrightnessSynchronizer(Context paramContext) {
    Object object = new Object(this);
    this.mContext = paramContext;
    BrightnessSyncObserver brightnessSyncObserver = new BrightnessSyncObserver((Handler)object);
    brightnessSyncObserver.startObserving();
  }
  
  public static float brightnessIntToFloat(Context paramContext, int paramInt) {
    PowerManager powerManager = (PowerManager)paramContext.getSystemService(PowerManager.class);
    float f1 = powerManager.getBrightnessConstraint(0);
    float f2 = powerManager.getBrightnessConstraint(1);
    int i = Math.round(brightnessFloatToIntRange(f1, 0.0F, 1.0F, 1.0F, 255.0F));
    int j = Math.round(brightnessFloatToIntRange(f2, 0.0F, 1.0F, 1.0F, 255.0F));
    return brightnessIntToFloat(paramInt, i, j, f1, f2);
  }
  
  public static float brightnessIntToFloat(int paramInt1, int paramInt2, int paramInt3, float paramFloat1, float paramFloat2) {
    if (paramInt1 == 0)
      return -1.0F; 
    if (paramInt1 == -1)
      return Float.NaN; 
    return MathUtils.constrainedMap(paramFloat1, paramFloat2, paramInt2, paramInt3, paramInt1);
  }
  
  public static int brightnessFloatToInt(Context paramContext, float paramFloat) {
    return Math.round(brightnessFloatToIntRange(paramContext, paramFloat));
  }
  
  public static float brightnessFloatToIntRange(Context paramContext, float paramFloat) {
    PowerManager powerManager = (PowerManager)paramContext.getSystemService(PowerManager.class);
    float f1 = powerManager.getBrightnessConstraint(0);
    float f2 = powerManager.getBrightnessConstraint(1);
    float f3 = brightnessFloatToIntRange(f1, 0.0F, 1.0F, 1.0F, 255.0F);
    float f4 = brightnessFloatToIntRange(f2, 0.0F, 1.0F, 1.0F, 255.0F);
    return brightnessFloatToIntRange(paramFloat, f1, f2, f3, f4);
  }
  
  private static float brightnessFloatToIntRange(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5) {
    if (floatEquals(paramFloat1, -1.0F))
      return 0.0F; 
    if (Float.isNaN(paramFloat1))
      return -1.0F; 
    return MathUtils.constrainedMap(paramFloat4, paramFloat5, paramFloat2, paramFloat3, paramFloat1);
  }
  
  private static float getScreenBrightnessFloat(Context paramContext) {
    return Settings.System.getFloatForUser(paramContext.getContentResolver(), "screen_brightness_float", Float.NaN, -2);
  }
  
  private static int getScreenBrightnessInt(Context paramContext) {
    return Settings.System.getIntForUser(paramContext.getContentResolver(), "screen_brightness", 0, -2);
  }
  
  private void updateBrightnessFloatFromInt(int paramInt) {
    Object object = this.mWriteHistory.peek();
    if (object != null && object.equals(Integer.valueOf(paramInt))) {
      this.mWriteHistory.poll();
    } else {
      if (brightnessFloatToInt(this.mContext, this.mPreferredSettingValue) == paramInt)
        return; 
      float f = brightnessIntToFloat(this.mContext, paramInt);
      this.mWriteHistory.offer(Float.valueOf(f));
      this.mPreferredSettingValue = f;
      Settings.System.putFloatForUser(this.mContext.getContentResolver(), "screen_brightness_float", f, -2);
    } 
  }
  
  private void updateBrightnessIntFromFloat(float paramFloat) {
    int i = brightnessFloatToInt(this.mContext, paramFloat);
    Object object = this.mWriteHistory.peek();
    if (object != null && object.equals(Float.valueOf(paramFloat))) {
      this.mWriteHistory.poll();
    } else {
      this.mWriteHistory.offer(Integer.valueOf(i));
      this.mPreferredSettingValue = paramFloat;
      Settings.System.putIntForUser(this.mContext.getContentResolver(), "screen_brightness", i, -2);
    } 
  }
  
  public static boolean floatEquals(float paramFloat1, float paramFloat2) {
    if (paramFloat1 == paramFloat2)
      return true; 
    if (Float.isNaN(paramFloat1) && Float.isNaN(paramFloat2))
      return true; 
    if (Math.abs(paramFloat1 - paramFloat2) < 0.001F)
      return true; 
    return false;
  }
  
  class BrightnessSyncObserver extends ContentObserver {
    final BrightnessSynchronizer this$0;
    
    BrightnessSyncObserver(Handler param1Handler) {
      super(param1Handler);
    }
    
    public void onChange(boolean param1Boolean) {
      onChange(param1Boolean, null);
    }
    
    public void onChange(boolean param1Boolean, Uri param1Uri) {
      if (param1Boolean)
        return; 
      if (BrightnessSynchronizer.BRIGHTNESS_URI.equals(param1Uri)) {
        int i = BrightnessSynchronizer.getScreenBrightnessInt(BrightnessSynchronizer.this.mContext);
        BrightnessSynchronizer.this.mHandler.obtainMessage(1, i, 0).sendToTarget();
      } else if (BrightnessSynchronizer.BRIGHTNESS_FLOAT_URI.equals(param1Uri)) {
        float f = BrightnessSynchronizer.getScreenBrightnessFloat(BrightnessSynchronizer.this.mContext);
        int i = Float.floatToIntBits(f);
        BrightnessSynchronizer.this.mHandler.obtainMessage(2, i, 0).sendToTarget();
      } 
    }
    
    public void startObserving() {
      ContentResolver contentResolver = BrightnessSynchronizer.this.mContext.getContentResolver();
      contentResolver.unregisterContentObserver(this);
      contentResolver.registerContentObserver(BrightnessSynchronizer.BRIGHTNESS_URI, false, this, -1);
    }
    
    public void stopObserving() {
      ContentResolver contentResolver = BrightnessSynchronizer.this.mContext.getContentResolver();
      contentResolver.unregisterContentObserver(this);
    }
  }
}
