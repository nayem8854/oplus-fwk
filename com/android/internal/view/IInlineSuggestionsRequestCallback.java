package com.android.internal.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.autofill.AutofillId;
import android.view.inputmethod.InlineSuggestionsRequest;

public interface IInlineSuggestionsRequestCallback extends IInterface {
  void onInlineSuggestionsRequest(InlineSuggestionsRequest paramInlineSuggestionsRequest, IInlineSuggestionsResponseCallback paramIInlineSuggestionsResponseCallback) throws RemoteException;
  
  void onInlineSuggestionsSessionInvalidated() throws RemoteException;
  
  void onInlineSuggestionsUnsupported() throws RemoteException;
  
  void onInputMethodFinishInput() throws RemoteException;
  
  void onInputMethodFinishInputView() throws RemoteException;
  
  void onInputMethodShowInputRequested(boolean paramBoolean) throws RemoteException;
  
  void onInputMethodStartInput(AutofillId paramAutofillId) throws RemoteException;
  
  void onInputMethodStartInputView() throws RemoteException;
  
  class Default implements IInlineSuggestionsRequestCallback {
    public void onInlineSuggestionsUnsupported() throws RemoteException {}
    
    public void onInlineSuggestionsRequest(InlineSuggestionsRequest param1InlineSuggestionsRequest, IInlineSuggestionsResponseCallback param1IInlineSuggestionsResponseCallback) throws RemoteException {}
    
    public void onInputMethodStartInput(AutofillId param1AutofillId) throws RemoteException {}
    
    public void onInputMethodShowInputRequested(boolean param1Boolean) throws RemoteException {}
    
    public void onInputMethodStartInputView() throws RemoteException {}
    
    public void onInputMethodFinishInputView() throws RemoteException {}
    
    public void onInputMethodFinishInput() throws RemoteException {}
    
    public void onInlineSuggestionsSessionInvalidated() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInlineSuggestionsRequestCallback {
    private static final String DESCRIPTOR = "com.android.internal.view.IInlineSuggestionsRequestCallback";
    
    static final int TRANSACTION_onInlineSuggestionsRequest = 2;
    
    static final int TRANSACTION_onInlineSuggestionsSessionInvalidated = 8;
    
    static final int TRANSACTION_onInlineSuggestionsUnsupported = 1;
    
    static final int TRANSACTION_onInputMethodFinishInput = 7;
    
    static final int TRANSACTION_onInputMethodFinishInputView = 6;
    
    static final int TRANSACTION_onInputMethodShowInputRequested = 4;
    
    static final int TRANSACTION_onInputMethodStartInput = 3;
    
    static final int TRANSACTION_onInputMethodStartInputView = 5;
    
    public Stub() {
      attachInterface(this, "com.android.internal.view.IInlineSuggestionsRequestCallback");
    }
    
    public static IInlineSuggestionsRequestCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.view.IInlineSuggestionsRequestCallback");
      if (iInterface != null && iInterface instanceof IInlineSuggestionsRequestCallback)
        return (IInlineSuggestionsRequestCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "onInlineSuggestionsSessionInvalidated";
        case 7:
          return "onInputMethodFinishInput";
        case 6:
          return "onInputMethodFinishInputView";
        case 5:
          return "onInputMethodStartInputView";
        case 4:
          return "onInputMethodShowInputRequested";
        case 3:
          return "onInputMethodStartInput";
        case 2:
          return "onInlineSuggestionsRequest";
        case 1:
          break;
      } 
      return "onInlineSuggestionsUnsupported";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        IInlineSuggestionsResponseCallback iInlineSuggestionsResponseCallback;
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("com.android.internal.view.IInlineSuggestionsRequestCallback");
            onInlineSuggestionsSessionInvalidated();
            return true;
          case 7:
            param1Parcel1.enforceInterface("com.android.internal.view.IInlineSuggestionsRequestCallback");
            onInputMethodFinishInput();
            return true;
          case 6:
            param1Parcel1.enforceInterface("com.android.internal.view.IInlineSuggestionsRequestCallback");
            onInputMethodFinishInputView();
            return true;
          case 5:
            param1Parcel1.enforceInterface("com.android.internal.view.IInlineSuggestionsRequestCallback");
            onInputMethodStartInputView();
            return true;
          case 4:
            param1Parcel1.enforceInterface("com.android.internal.view.IInlineSuggestionsRequestCallback");
            if (param1Parcel1.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            onInputMethodShowInputRequested(bool);
            return true;
          case 3:
            param1Parcel1.enforceInterface("com.android.internal.view.IInlineSuggestionsRequestCallback");
            if (param1Parcel1.readInt() != 0) {
              AutofillId autofillId = (AutofillId)AutofillId.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onInputMethodStartInput((AutofillId)param1Parcel1);
            return true;
          case 2:
            param1Parcel1.enforceInterface("com.android.internal.view.IInlineSuggestionsRequestCallback");
            if (param1Parcel1.readInt() != 0) {
              InlineSuggestionsRequest inlineSuggestionsRequest = (InlineSuggestionsRequest)InlineSuggestionsRequest.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            iInlineSuggestionsResponseCallback = IInlineSuggestionsResponseCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            onInlineSuggestionsRequest((InlineSuggestionsRequest)param1Parcel2, iInlineSuggestionsResponseCallback);
            return true;
          case 1:
            break;
        } 
        iInlineSuggestionsResponseCallback.enforceInterface("com.android.internal.view.IInlineSuggestionsRequestCallback");
        onInlineSuggestionsUnsupported();
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.view.IInlineSuggestionsRequestCallback");
      return true;
    }
    
    private static class Proxy implements IInlineSuggestionsRequestCallback {
      public static IInlineSuggestionsRequestCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.view.IInlineSuggestionsRequestCallback";
      }
      
      public void onInlineSuggestionsUnsupported() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInlineSuggestionsRequestCallback");
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IInlineSuggestionsRequestCallback.Stub.getDefaultImpl() != null) {
            IInlineSuggestionsRequestCallback.Stub.getDefaultImpl().onInlineSuggestionsUnsupported();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onInlineSuggestionsRequest(InlineSuggestionsRequest param2InlineSuggestionsRequest, IInlineSuggestionsResponseCallback param2IInlineSuggestionsResponseCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.view.IInlineSuggestionsRequestCallback");
          if (param2InlineSuggestionsRequest != null) {
            parcel.writeInt(1);
            param2InlineSuggestionsRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IInlineSuggestionsResponseCallback != null) {
            iBinder = param2IInlineSuggestionsResponseCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IInlineSuggestionsRequestCallback.Stub.getDefaultImpl() != null) {
            IInlineSuggestionsRequestCallback.Stub.getDefaultImpl().onInlineSuggestionsRequest(param2InlineSuggestionsRequest, param2IInlineSuggestionsResponseCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onInputMethodStartInput(AutofillId param2AutofillId) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInlineSuggestionsRequestCallback");
          if (param2AutofillId != null) {
            parcel.writeInt(1);
            param2AutofillId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IInlineSuggestionsRequestCallback.Stub.getDefaultImpl() != null) {
            IInlineSuggestionsRequestCallback.Stub.getDefaultImpl().onInputMethodStartInput(param2AutofillId);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onInputMethodShowInputRequested(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.view.IInlineSuggestionsRequestCallback");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(4, parcel, null, 1);
          if (!bool1 && IInlineSuggestionsRequestCallback.Stub.getDefaultImpl() != null) {
            IInlineSuggestionsRequestCallback.Stub.getDefaultImpl().onInputMethodShowInputRequested(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onInputMethodStartInputView() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInlineSuggestionsRequestCallback");
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IInlineSuggestionsRequestCallback.Stub.getDefaultImpl() != null) {
            IInlineSuggestionsRequestCallback.Stub.getDefaultImpl().onInputMethodStartInputView();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onInputMethodFinishInputView() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInlineSuggestionsRequestCallback");
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IInlineSuggestionsRequestCallback.Stub.getDefaultImpl() != null) {
            IInlineSuggestionsRequestCallback.Stub.getDefaultImpl().onInputMethodFinishInputView();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onInputMethodFinishInput() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInlineSuggestionsRequestCallback");
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IInlineSuggestionsRequestCallback.Stub.getDefaultImpl() != null) {
            IInlineSuggestionsRequestCallback.Stub.getDefaultImpl().onInputMethodFinishInput();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onInlineSuggestionsSessionInvalidated() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInlineSuggestionsRequestCallback");
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IInlineSuggestionsRequestCallback.Stub.getDefaultImpl() != null) {
            IInlineSuggestionsRequestCallback.Stub.getDefaultImpl().onInlineSuggestionsSessionInvalidated();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInlineSuggestionsRequestCallback param1IInlineSuggestionsRequestCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInlineSuggestionsRequestCallback != null) {
          Proxy.sDefaultImpl = param1IInlineSuggestionsRequestCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInlineSuggestionsRequestCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
