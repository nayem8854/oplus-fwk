package com.android.internal.view;

import android.annotation.NonNull;
import android.content.ComponentName;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.autofill.AutofillId;
import com.android.internal.util.AnnotationValidations;
import java.util.Objects;

public final class InlineSuggestionsRequestInfo implements Parcelable {
  public InlineSuggestionsRequestInfo(ComponentName paramComponentName, AutofillId paramAutofillId, Bundle paramBundle) {
    this.mComponentName = paramComponentName;
    AnnotationValidations.validate(NonNull.class, (NonNull)null, paramComponentName);
    this.mAutofillId = paramAutofillId;
    AnnotationValidations.validate(NonNull.class, (NonNull)null, paramAutofillId);
    this.mUiExtras = paramBundle;
    AnnotationValidations.validate(NonNull.class, (NonNull)null, paramBundle);
  }
  
  public ComponentName getComponentName() {
    return this.mComponentName;
  }
  
  public AutofillId getAutofillId() {
    return this.mAutofillId;
  }
  
  public Bundle getUiExtras() {
    return this.mUiExtras;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("InlineSuggestionsRequestInfo { componentName = ");
    stringBuilder.append(this.mComponentName);
    stringBuilder.append(", autofillId = ");
    stringBuilder.append(this.mAutofillId);
    stringBuilder.append(", uiExtras = ");
    stringBuilder.append(this.mUiExtras);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    ComponentName componentName1 = this.mComponentName, componentName2 = ((InlineSuggestionsRequestInfo)paramObject).mComponentName;
    if (Objects.equals(componentName1, componentName2)) {
      AutofillId autofillId2 = this.mAutofillId, autofillId1 = ((InlineSuggestionsRequestInfo)paramObject).mAutofillId;
      if (Objects.equals(autofillId2, autofillId1)) {
        Bundle bundle = this.mUiExtras;
        paramObject = ((InlineSuggestionsRequestInfo)paramObject).mUiExtras;
        if (Objects.equals(bundle, paramObject))
          return null; 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = Objects.hashCode(this.mComponentName);
    int j = Objects.hashCode(this.mAutofillId);
    int k = Objects.hashCode(this.mUiExtras);
    return ((1 * 31 + i) * 31 + j) * 31 + k;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeTypedObject((Parcelable)this.mComponentName, paramInt);
    paramParcel.writeTypedObject((Parcelable)this.mAutofillId, paramInt);
    paramParcel.writeBundle(this.mUiExtras);
  }
  
  public int describeContents() {
    return 0;
  }
  
  InlineSuggestionsRequestInfo(Parcel paramParcel) {
    ComponentName componentName = (ComponentName)paramParcel.readTypedObject(ComponentName.CREATOR);
    AutofillId autofillId = (AutofillId)paramParcel.readTypedObject(AutofillId.CREATOR);
    Bundle bundle = paramParcel.readBundle();
    this.mComponentName = componentName;
    AnnotationValidations.validate(NonNull.class, (NonNull)null, componentName);
    this.mAutofillId = autofillId;
    AnnotationValidations.validate(NonNull.class, (NonNull)null, autofillId);
    this.mUiExtras = bundle;
    AnnotationValidations.validate(NonNull.class, (NonNull)null, bundle);
  }
  
  public static final Parcelable.Creator<InlineSuggestionsRequestInfo> CREATOR = new Parcelable.Creator<InlineSuggestionsRequestInfo>() {
      public InlineSuggestionsRequestInfo[] newArray(int param1Int) {
        return new InlineSuggestionsRequestInfo[param1Int];
      }
      
      public InlineSuggestionsRequestInfo createFromParcel(Parcel param1Parcel) {
        return new InlineSuggestionsRequestInfo(param1Parcel);
      }
    };
  
  private final AutofillId mAutofillId;
  
  private final ComponentName mComponentName;
  
  private final Bundle mUiExtras;
  
  @Deprecated
  private void __metadata() {}
}
