package com.android.internal.view;

import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.input.InputManager;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.util.MergedConfiguration;
import android.view.DisplayCutout;
import android.view.DragEvent;
import android.view.IScrollCaptureController;
import android.view.IWindow;
import android.view.IWindowSession;
import android.view.InsetsSourceControl;
import android.view.InsetsState;
import com.android.internal.os.IResultReceiver;
import java.io.IOException;

public class BaseIWindow extends IWindow.Stub {
  public int mSeq;
  
  private IWindowSession mSession;
  
  public void setSession(IWindowSession paramIWindowSession) {
    this.mSession = paramIWindowSession;
  }
  
  public void resized(Rect paramRect1, Rect paramRect2, Rect paramRect3, Rect paramRect4, boolean paramBoolean1, MergedConfiguration paramMergedConfiguration, Rect paramRect5, boolean paramBoolean2, boolean paramBoolean3, int paramInt, DisplayCutout.ParcelableWrapper paramParcelableWrapper) {
    if (paramBoolean1)
      try {
        this.mSession.finishDrawing((IWindow)this, null);
      } catch (RemoteException remoteException) {} 
  }
  
  public void locationInParentDisplayChanged(Point paramPoint) {}
  
  public void insetsChanged(InsetsState paramInsetsState) {}
  
  public void insetsControlChanged(InsetsState paramInsetsState, InsetsSourceControl[] paramArrayOfInsetsSourceControl) {}
  
  public void showInsets(int paramInt, boolean paramBoolean) {}
  
  public void hideInsets(int paramInt, boolean paramBoolean) {}
  
  public void moved(int paramInt1, int paramInt2) {}
  
  public void dispatchAppVisibility(boolean paramBoolean) {}
  
  public void dispatchGetNewSurface() {}
  
  public void windowFocusChanged(boolean paramBoolean1, boolean paramBoolean2) {}
  
  public void executeCommand(String paramString1, String paramString2, ParcelFileDescriptor paramParcelFileDescriptor) {
    if (paramParcelFileDescriptor != null)
      try {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Unsupported command ");
        stringBuilder.append(paramString1);
        paramParcelFileDescriptor.closeWithError(stringBuilder.toString());
      } catch (IOException iOException) {} 
  }
  
  public void closeSystemDialogs(String paramString) {}
  
  public void dispatchWallpaperOffsets(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, boolean paramBoolean) {
    if (paramBoolean)
      try {
        this.mSession.wallpaperOffsetsComplete(asBinder());
      } catch (RemoteException remoteException) {} 
  }
  
  public void dispatchDragEvent(DragEvent paramDragEvent) {
    if (paramDragEvent.getAction() == 3)
      try {
        this.mSession.reportDropResult((IWindow)this, false);
      } catch (RemoteException remoteException) {} 
  }
  
  public void updatePointerIcon(float paramFloat1, float paramFloat2) {
    InputManager.getInstance().setPointerIconType(1);
  }
  
  public void dispatchSystemUiVisibilityChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mSeq = paramInt1;
  }
  
  public void dispatchWallpaperCommand(String paramString, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle, boolean paramBoolean) {
    if (paramBoolean)
      try {
        this.mSession.wallpaperCommandComplete(asBinder(), null);
      } catch (RemoteException remoteException) {} 
  }
  
  public void dispatchWindowShown() {}
  
  public void requestAppKeyboardShortcuts(IResultReceiver paramIResultReceiver, int paramInt) {}
  
  public void dispatchPointerCaptureChanged(boolean paramBoolean) {}
  
  public void requestScrollCapture(IScrollCaptureController paramIScrollCaptureController) {
    try {
      paramIScrollCaptureController.onClientUnavailable();
    } catch (RemoteException remoteException) {}
  }
}
