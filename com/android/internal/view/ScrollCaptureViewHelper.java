package com.android.internal.view;

import android.graphics.Rect;

interface ScrollCaptureViewHelper<V extends android.view.View> {
  public static final int DOWN = 1;
  
  public static final int UP = -1;
  
  default boolean onAcceptSession(V paramV) {
    boolean bool = true;
    if (paramV != null && paramV.isVisibleToUser()) {
      boolean bool1 = bool;
      if (!paramV.canScrollVertically(-1)) {
        if (paramV.canScrollVertically(1))
          return bool; 
      } else {
        return bool1;
      } 
    } 
    return false;
  }
  
  default Rect onComputeScrollBounds(V paramV) {
    int i = paramV.getPaddingLeft(), j = paramV.getPaddingTop();
    int k = paramV.getWidth(), m = paramV.getPaddingRight();
    return new Rect(i, j, k - m, paramV.getHeight() - paramV.getPaddingBottom());
  }
  
  void onPrepareForEnd(V paramV);
  
  void onPrepareForStart(V paramV, Rect paramRect);
  
  Rect onScrollRequested(V paramV, Rect paramRect1, Rect paramRect2);
}
