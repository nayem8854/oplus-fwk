package com.android.internal.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.PopupWindow;
import com.android.internal.view.menu.MenuBuilder;
import com.android.internal.widget.FloatingToolbar;
import java.util.Arrays;
import java.util.Objects;

public final class FloatingActionMode extends ActionMode {
  private final Rect mViewRectOnScreen;
  
  private final int[] mViewPositionOnScreen;
  
  private final Rect mScreenRect;
  
  private final int[] mRootViewPositionOnScreen;
  
  private final Rect mPreviousViewRectOnScreen;
  
  private final int[] mPreviousViewPositionOnScreen;
  
  private final Rect mPreviousContentRectOnScreen;
  
  private final View mOriginatingView;
  
  private final Runnable mMovingOff = (Runnable)new Object(this);
  
  private final MenuBuilder mMenu;
  
  private final Runnable mHideOff = (Runnable)new Object(this);
  
  private FloatingToolbarVisibilityHelper mFloatingToolbarVisibilityHelper;
  
  private FloatingToolbar mFloatingToolbar;
  
  private final Point mDisplaySize;
  
  private final Context mContext;
  
  private final Rect mContentRectOnScreen;
  
  private final Rect mContentRect;
  
  private final ActionMode.Callback2 mCallback;
  
  private final int mBottomAllowance;
  
  private static final int MOVING_HIDE_DELAY = 50;
  
  private static final int MAX_HIDE_DURATION = 3000;
  
  public FloatingActionMode(Context paramContext, ActionMode.Callback2 paramCallback2, View paramView, FloatingToolbar paramFloatingToolbar) {
    Objects.requireNonNull(paramContext);
    this.mContext = paramContext;
    Objects.requireNonNull(paramCallback2);
    this.mCallback = paramCallback2;
    this.mMenu = (new MenuBuilder(paramContext)).setDefaultShowAsAction(1);
    setType(1);
    this.mMenu.setCallback(new MenuBuilder.Callback() {
          final FloatingActionMode this$0;
          
          public void onMenuModeChange(MenuBuilder param1MenuBuilder) {}
          
          public boolean onMenuItemSelected(MenuBuilder param1MenuBuilder, MenuItem param1MenuItem) {
            return FloatingActionMode.this.mCallback.onActionItemClicked(FloatingActionMode.this, param1MenuItem);
          }
        });
    this.mContentRect = new Rect();
    this.mContentRectOnScreen = new Rect();
    this.mPreviousContentRectOnScreen = new Rect();
    this.mViewPositionOnScreen = new int[2];
    this.mPreviousViewPositionOnScreen = new int[2];
    this.mRootViewPositionOnScreen = new int[2];
    this.mViewRectOnScreen = new Rect();
    this.mPreviousViewRectOnScreen = new Rect();
    this.mScreenRect = new Rect();
    Objects.requireNonNull(paramView);
    View view = paramView;
    view.getLocationOnScreen(this.mViewPositionOnScreen);
    Resources resources = paramContext.getResources();
    this.mBottomAllowance = resources.getDimensionPixelSize(17105097);
    this.mDisplaySize = new Point();
    Objects.requireNonNull(paramFloatingToolbar);
    setFloatingToolbar(paramFloatingToolbar);
  }
  
  private void setFloatingToolbar(FloatingToolbar paramFloatingToolbar) {
    MenuBuilder menuBuilder = this.mMenu;
    paramFloatingToolbar = paramFloatingToolbar.setMenu(menuBuilder);
    _$$Lambda$FloatingActionMode$LU5MpPuKYDtwlFAuYhXYfzgLNLE _$$Lambda$FloatingActionMode$LU5MpPuKYDtwlFAuYhXYfzgLNLE = new _$$Lambda$FloatingActionMode$LU5MpPuKYDtwlFAuYhXYfzgLNLE(this);
    this.mFloatingToolbar = paramFloatingToolbar = paramFloatingToolbar.setOnMenuItemClickListener(_$$Lambda$FloatingActionMode$LU5MpPuKYDtwlFAuYhXYfzgLNLE);
    FloatingToolbarVisibilityHelper floatingToolbarVisibilityHelper = new FloatingToolbarVisibilityHelper(paramFloatingToolbar);
    floatingToolbarVisibilityHelper.activate();
  }
  
  public void setTitle(CharSequence paramCharSequence) {}
  
  public void setTitle(int paramInt) {}
  
  public void setSubtitle(CharSequence paramCharSequence) {}
  
  public void setSubtitle(int paramInt) {}
  
  public void setCustomView(View paramView) {}
  
  public void invalidate() {
    this.mCallback.onPrepareActionMode(this, this.mMenu);
    invalidateContentRect();
  }
  
  public void invalidateContentRect() {
    this.mCallback.onGetContentRect(this, this.mOriginatingView, this.mContentRect);
    repositionToolbar();
  }
  
  public void updateViewLocationInWindow() {
    this.mOriginatingView.getLocationOnScreen(this.mViewPositionOnScreen);
    this.mOriginatingView.getRootView().getLocationOnScreen(this.mRootViewPositionOnScreen);
    this.mOriginatingView.getGlobalVisibleRect(this.mViewRectOnScreen);
    Rect rect = this.mViewRectOnScreen;
    int[] arrayOfInt2 = this.mRootViewPositionOnScreen;
    rect.offset(arrayOfInt2[0], arrayOfInt2[1]);
    if (Arrays.equals(this.mViewPositionOnScreen, this.mPreviousViewPositionOnScreen)) {
      Rect rect1 = this.mViewRectOnScreen;
      rect = this.mPreviousViewRectOnScreen;
      if (!rect1.equals(rect)) {
        repositionToolbar();
        int[] arrayOfInt3 = this.mPreviousViewPositionOnScreen, arrayOfInt4 = this.mViewPositionOnScreen;
        arrayOfInt3[0] = arrayOfInt4[0];
        arrayOfInt3[1] = arrayOfInt4[1];
        this.mPreviousViewRectOnScreen.set(this.mViewRectOnScreen);
        return;
      } 
      return;
    } 
    repositionToolbar();
    int[] arrayOfInt1 = this.mPreviousViewPositionOnScreen;
    arrayOfInt2 = this.mViewPositionOnScreen;
    arrayOfInt1[0] = arrayOfInt2[0];
    arrayOfInt1[1] = arrayOfInt2[1];
    this.mPreviousViewRectOnScreen.set(this.mViewRectOnScreen);
  }
  
  private void repositionToolbar() {
    this.mContentRectOnScreen.set(this.mContentRect);
    ViewParent viewParent = this.mOriginatingView.getParent();
    if (viewParent instanceof ViewGroup) {
      ((ViewGroup)viewParent).getChildVisibleRect(this.mOriginatingView, this.mContentRectOnScreen, null, true);
      Rect rect = this.mContentRectOnScreen;
      int[] arrayOfInt = this.mRootViewPositionOnScreen;
      rect.offset(arrayOfInt[0], arrayOfInt[1]);
    } else {
      Rect rect = this.mContentRectOnScreen;
      int[] arrayOfInt = this.mViewPositionOnScreen;
      rect.offset(arrayOfInt[0], arrayOfInt[1]);
    } 
    if (isContentRectWithinBounds()) {
      this.mFloatingToolbarVisibilityHelper.setOutOfBounds(false);
      Rect rect = this.mContentRectOnScreen;
      int i = rect.left, j = this.mViewRectOnScreen.left;
      i = Math.max(i, j);
      j = this.mContentRectOnScreen.top;
      int k = this.mViewRectOnScreen.top;
      j = Math.max(j, k);
      int m = this.mContentRectOnScreen.right;
      k = this.mViewRectOnScreen.right;
      k = Math.min(m, k);
      m = this.mContentRectOnScreen.bottom;
      int n = this.mViewRectOnScreen.bottom, i1 = this.mBottomAllowance;
      m = Math.min(m, n + i1);
      rect.set(i, j, k, m);
      if (!this.mContentRectOnScreen.equals(this.mPreviousContentRectOnScreen)) {
        this.mOriginatingView.removeCallbacks(this.mMovingOff);
        this.mFloatingToolbarVisibilityHelper.setMoving(true);
        this.mOriginatingView.postDelayed(this.mMovingOff, 50L);
        this.mFloatingToolbar.setContentRect(this.mContentRectOnScreen);
        this.mFloatingToolbar.updateLayout();
      } 
    } else {
      this.mFloatingToolbarVisibilityHelper.setOutOfBounds(true);
      this.mContentRectOnScreen.setEmpty();
    } 
    this.mFloatingToolbarVisibilityHelper.updateToolbarVisibility();
    this.mPreviousContentRectOnScreen.set(this.mContentRectOnScreen);
  }
  
  private boolean isContentRectWithinBounds() {
    this.mContext.getDisplayNoVerify().getRealSize(this.mDisplaySize);
    Rect rect = this.mScreenRect;
    int i = this.mDisplaySize.x, j = this.mDisplaySize.y;
    boolean bool = false;
    rect.set(0, 0, i, j);
    if (intersectsClosed(this.mContentRectOnScreen, this.mScreenRect)) {
      Rect rect1 = this.mContentRectOnScreen;
      rect = this.mViewRectOnScreen;
      if (intersectsClosed(rect1, rect))
        bool = true; 
    } 
    return bool;
  }
  
  private static boolean intersectsClosed(Rect paramRect1, Rect paramRect2) {
    boolean bool;
    if (paramRect1.left <= paramRect2.right && paramRect2.left <= paramRect1.right && paramRect1.top <= paramRect2.bottom && paramRect2.top <= paramRect1.bottom) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void hide(long paramLong) {
    long l = paramLong;
    if (paramLong == -1L)
      l = ViewConfiguration.getDefaultActionModeHideDuration(); 
    paramLong = Math.min(3000L, l);
    this.mOriginatingView.removeCallbacks(this.mHideOff);
    if (paramLong <= 0L) {
      this.mHideOff.run();
    } else {
      this.mFloatingToolbarVisibilityHelper.setHideRequested(true);
      this.mFloatingToolbarVisibilityHelper.updateToolbarVisibility();
      this.mOriginatingView.postDelayed(this.mHideOff, paramLong);
    } 
  }
  
  public void setOutsideTouchable(boolean paramBoolean, PopupWindow.OnDismissListener paramOnDismissListener) {
    this.mFloatingToolbar.setOutsideTouchable(paramBoolean, paramOnDismissListener);
  }
  
  public void onWindowFocusChanged(boolean paramBoolean) {
    this.mFloatingToolbarVisibilityHelper.setWindowFocused(paramBoolean);
    this.mFloatingToolbarVisibilityHelper.updateToolbarVisibility();
  }
  
  public void finish() {
    reset();
    this.mCallback.onDestroyActionMode(this);
  }
  
  public Menu getMenu() {
    return this.mMenu;
  }
  
  public CharSequence getTitle() {
    return null;
  }
  
  public CharSequence getSubtitle() {
    return null;
  }
  
  public View getCustomView() {
    return null;
  }
  
  public MenuInflater getMenuInflater() {
    return new MenuInflater(this.mContext);
  }
  
  private void reset() {
    this.mFloatingToolbar.dismiss();
    this.mFloatingToolbarVisibilityHelper.deactivate();
    this.mOriginatingView.removeCallbacks(this.mMovingOff);
    this.mOriginatingView.removeCallbacks(this.mHideOff);
  }
  
  private boolean isViewStillActive() {
    if (this.mOriginatingView.getWindowVisibility() == 0) {
      View view = this.mOriginatingView;
      if (view.isShown())
        return true; 
    } 
    return false;
  }
  
  class FloatingToolbarVisibilityHelper {
    private static final long MIN_SHOW_DURATION_FOR_MOVE_HIDE = 500L;
    
    private boolean mActive;
    
    private boolean mHideRequested;
    
    private long mLastShowTime;
    
    private boolean mMoving;
    
    private boolean mOutOfBounds;
    
    private final FloatingToolbar mToolbar;
    
    private boolean mWindowFocused = true;
    
    public FloatingToolbarVisibilityHelper(FloatingActionMode this$0) {
      Objects.requireNonNull(this$0);
      this.mToolbar = (FloatingToolbar)this$0;
    }
    
    public void activate() {
      this.mHideRequested = false;
      this.mMoving = false;
      this.mOutOfBounds = false;
      this.mWindowFocused = true;
      this.mActive = true;
    }
    
    public void deactivate() {
      this.mActive = false;
      this.mToolbar.dismiss();
    }
    
    public void setHideRequested(boolean param1Boolean) {
      this.mHideRequested = param1Boolean;
    }
    
    public void setMoving(boolean param1Boolean) {
      boolean bool;
      if (System.currentTimeMillis() - this.mLastShowTime > 500L) {
        bool = true;
      } else {
        bool = false;
      } 
      if (!param1Boolean || bool)
        this.mMoving = param1Boolean; 
    }
    
    public void setOutOfBounds(boolean param1Boolean) {
      this.mOutOfBounds = param1Boolean;
    }
    
    public void setWindowFocused(boolean param1Boolean) {
      this.mWindowFocused = param1Boolean;
    }
    
    public void updateToolbarVisibility() {
      if (!this.mActive)
        return; 
      if (this.mHideRequested || this.mMoving || this.mOutOfBounds || !this.mWindowFocused) {
        this.mToolbar.hide();
        return;
      } 
      this.mToolbar.show();
      this.mLastShowTime = System.currentTimeMillis();
    }
  }
}
