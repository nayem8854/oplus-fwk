package com.android.internal.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.inputmethod.EditorInfo;

public interface IInputMethodClient extends IInterface {
  void applyImeVisibility(boolean paramBoolean) throws RemoteException;
  
  void onBindMethod(InputBindResult paramInputBindResult) throws RemoteException;
  
  void onUnbindMethod(int paramInt1, int paramInt2) throws RemoteException;
  
  void reportFullscreenMode(boolean paramBoolean) throws RemoteException;
  
  void reportPreRendered(EditorInfo paramEditorInfo) throws RemoteException;
  
  void scheduleStartInputIfNecessary(boolean paramBoolean) throws RemoteException;
  
  void setActive(boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void updateActivityViewToScreenMatrix(int paramInt, float[] paramArrayOffloat) throws RemoteException;
  
  class Default implements IInputMethodClient {
    public void onBindMethod(InputBindResult param1InputBindResult) throws RemoteException {}
    
    public void onUnbindMethod(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void setActive(boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public void scheduleStartInputIfNecessary(boolean param1Boolean) throws RemoteException {}
    
    public void reportFullscreenMode(boolean param1Boolean) throws RemoteException {}
    
    public void reportPreRendered(EditorInfo param1EditorInfo) throws RemoteException {}
    
    public void applyImeVisibility(boolean param1Boolean) throws RemoteException {}
    
    public void updateActivityViewToScreenMatrix(int param1Int, float[] param1ArrayOffloat) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInputMethodClient {
    private static final String DESCRIPTOR = "com.android.internal.view.IInputMethodClient";
    
    static final int TRANSACTION_applyImeVisibility = 7;
    
    static final int TRANSACTION_onBindMethod = 1;
    
    static final int TRANSACTION_onUnbindMethod = 2;
    
    static final int TRANSACTION_reportFullscreenMode = 5;
    
    static final int TRANSACTION_reportPreRendered = 6;
    
    static final int TRANSACTION_scheduleStartInputIfNecessary = 4;
    
    static final int TRANSACTION_setActive = 3;
    
    static final int TRANSACTION_updateActivityViewToScreenMatrix = 8;
    
    public Stub() {
      attachInterface(this, "com.android.internal.view.IInputMethodClient");
    }
    
    public static IInputMethodClient asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.view.IInputMethodClient");
      if (iInterface != null && iInterface instanceof IInputMethodClient)
        return (IInputMethodClient)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "updateActivityViewToScreenMatrix";
        case 7:
          return "applyImeVisibility";
        case 6:
          return "reportPreRendered";
        case 5:
          return "reportFullscreenMode";
        case 4:
          return "scheduleStartInputIfNecessary";
        case 3:
          return "setActive";
        case 2:
          return "onUnbindMethod";
        case 1:
          break;
      } 
      return "onBindMethod";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        float[] arrayOfFloat;
        boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("com.android.internal.view.IInputMethodClient");
            param1Int1 = param1Parcel1.readInt();
            arrayOfFloat = param1Parcel1.createFloatArray();
            updateActivityViewToScreenMatrix(param1Int1, arrayOfFloat);
            return true;
          case 7:
            arrayOfFloat.enforceInterface("com.android.internal.view.IInputMethodClient");
            bool1 = bool4;
            if (arrayOfFloat.readInt() != 0)
              bool1 = true; 
            applyImeVisibility(bool1);
            return true;
          case 6:
            arrayOfFloat.enforceInterface("com.android.internal.view.IInputMethodClient");
            if (arrayOfFloat.readInt() != 0) {
              EditorInfo editorInfo = (EditorInfo)EditorInfo.CREATOR.createFromParcel((Parcel)arrayOfFloat);
            } else {
              arrayOfFloat = null;
            } 
            reportPreRendered((EditorInfo)arrayOfFloat);
            return true;
          case 5:
            arrayOfFloat.enforceInterface("com.android.internal.view.IInputMethodClient");
            if (arrayOfFloat.readInt() != 0)
              bool1 = true; 
            reportFullscreenMode(bool1);
            return true;
          case 4:
            arrayOfFloat.enforceInterface("com.android.internal.view.IInputMethodClient");
            bool1 = bool2;
            if (arrayOfFloat.readInt() != 0)
              bool1 = true; 
            scheduleStartInputIfNecessary(bool1);
            return true;
          case 3:
            arrayOfFloat.enforceInterface("com.android.internal.view.IInputMethodClient");
            if (arrayOfFloat.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            if (arrayOfFloat.readInt() != 0)
              bool3 = true; 
            setActive(bool1, bool3);
            return true;
          case 2:
            arrayOfFloat.enforceInterface("com.android.internal.view.IInputMethodClient");
            param1Int1 = arrayOfFloat.readInt();
            param1Int2 = arrayOfFloat.readInt();
            onUnbindMethod(param1Int1, param1Int2);
            return true;
          case 1:
            break;
        } 
        arrayOfFloat.enforceInterface("com.android.internal.view.IInputMethodClient");
        if (arrayOfFloat.readInt() != 0) {
          InputBindResult inputBindResult = (InputBindResult)InputBindResult.CREATOR.createFromParcel((Parcel)arrayOfFloat);
        } else {
          arrayOfFloat = null;
        } 
        onBindMethod((InputBindResult)arrayOfFloat);
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.view.IInputMethodClient");
      return true;
    }
    
    private static class Proxy implements IInputMethodClient {
      public static IInputMethodClient sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.view.IInputMethodClient";
      }
      
      public void onBindMethod(InputBindResult param2InputBindResult) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethodClient");
          if (param2InputBindResult != null) {
            parcel.writeInt(1);
            param2InputBindResult.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IInputMethodClient.Stub.getDefaultImpl() != null) {
            IInputMethodClient.Stub.getDefaultImpl().onBindMethod(param2InputBindResult);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onUnbindMethod(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethodClient");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IInputMethodClient.Stub.getDefaultImpl() != null) {
            IInputMethodClient.Stub.getDefaultImpl().onUnbindMethod(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setActive(boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethodClient");
          boolean bool1 = false;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel.writeInt(bool2);
          boolean bool2 = bool1;
          if (param2Boolean2)
            bool2 = true; 
          parcel.writeInt(bool2);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IInputMethodClient.Stub.getDefaultImpl() != null) {
            IInputMethodClient.Stub.getDefaultImpl().setActive(param2Boolean1, param2Boolean2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void scheduleStartInputIfNecessary(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethodClient");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(4, parcel, null, 1);
          if (!bool1 && IInputMethodClient.Stub.getDefaultImpl() != null) {
            IInputMethodClient.Stub.getDefaultImpl().scheduleStartInputIfNecessary(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void reportFullscreenMode(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethodClient");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(5, parcel, null, 1);
          if (!bool1 && IInputMethodClient.Stub.getDefaultImpl() != null) {
            IInputMethodClient.Stub.getDefaultImpl().reportFullscreenMode(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void reportPreRendered(EditorInfo param2EditorInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethodClient");
          if (param2EditorInfo != null) {
            parcel.writeInt(1);
            param2EditorInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IInputMethodClient.Stub.getDefaultImpl() != null) {
            IInputMethodClient.Stub.getDefaultImpl().reportPreRendered(param2EditorInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void applyImeVisibility(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethodClient");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(7, parcel, null, 1);
          if (!bool1 && IInputMethodClient.Stub.getDefaultImpl() != null) {
            IInputMethodClient.Stub.getDefaultImpl().applyImeVisibility(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void updateActivityViewToScreenMatrix(int param2Int, float[] param2ArrayOffloat) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethodClient");
          parcel.writeInt(param2Int);
          parcel.writeFloatArray(param2ArrayOffloat);
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IInputMethodClient.Stub.getDefaultImpl() != null) {
            IInputMethodClient.Stub.getDefaultImpl().updateActivityViewToScreenMatrix(param2Int, param2ArrayOffloat);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInputMethodClient param1IInputMethodClient) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInputMethodClient != null) {
          Proxy.sDefaultImpl = param1IInputMethodClient;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInputMethodClient getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
