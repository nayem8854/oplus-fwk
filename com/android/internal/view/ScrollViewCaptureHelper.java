package com.android.internal.view;

import android.graphics.Point;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;

public class ScrollViewCaptureHelper implements ScrollCaptureViewHelper<ViewGroup> {
  private int mOverScrollMode;
  
  private boolean mScrollBarEnabled;
  
  private int mStartScrollY;
  
  public void onPrepareForStart(ViewGroup paramViewGroup, Rect paramRect) {
    this.mStartScrollY = paramViewGroup.getScrollY();
    int i = paramViewGroup.getOverScrollMode();
    if (i != 2)
      paramViewGroup.setOverScrollMode(2); 
    boolean bool = paramViewGroup.isVerticalScrollBarEnabled();
    if (bool)
      paramViewGroup.setVerticalScrollBarEnabled(false); 
  }
  
  public Rect onScrollRequested(ViewGroup paramViewGroup, Rect paramRect1, Rect paramRect2) {
    View view = paramViewGroup.getChildAt(0);
    if (view == null)
      return null; 
    int i = paramViewGroup.getScrollY(), j = this.mStartScrollY;
    paramRect2 = new Rect(paramRect2);
    paramRect2.offset(0, -(i - j));
    paramRect2.offset(paramRect1.left, paramRect1.top);
    Rect rect = new Rect(paramRect2);
    i = paramViewGroup.getScrollX();
    int k = view.getLeft();
    int m = paramViewGroup.getScrollY();
    j = view.getTop();
    rect.offset(i - k, m - j);
    view.requestRectangleOnScreen(new Rect(rect), true);
    k = paramViewGroup.getScrollY();
    int n = this.mStartScrollY;
    Point point = new Point();
    rect = new Rect(rect);
    if (!paramViewGroup.getChildVisibleRect(view, rect, point)) {
      rect.setEmpty();
      return rect;
    } 
    rect.offset(-point.x, -point.y);
    j = view.getLeft();
    int i1 = paramViewGroup.getScrollX();
    i = view.getTop();
    m = paramViewGroup.getScrollY();
    rect.offset(j - i1, i - m);
    rect.offset(-paramRect1.left, -paramRect1.top);
    rect.offset(0, k - n);
    return rect;
  }
  
  public void onPrepareForEnd(ViewGroup paramViewGroup) {
    paramViewGroup.scrollTo(0, this.mStartScrollY);
    int i = this.mOverScrollMode;
    if (i != 2)
      paramViewGroup.setOverScrollMode(i); 
    if (this.mScrollBarEnabled)
      paramViewGroup.setVerticalScrollBarEnabled(true); 
  }
}
