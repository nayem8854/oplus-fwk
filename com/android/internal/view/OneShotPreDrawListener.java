package com.android.internal.view;

import android.view.View;
import android.view.ViewTreeObserver;

public class OneShotPreDrawListener implements ViewTreeObserver.OnPreDrawListener, View.OnAttachStateChangeListener {
  private final boolean mReturnValue;
  
  private final Runnable mRunnable;
  
  private final View mView;
  
  private ViewTreeObserver mViewTreeObserver;
  
  private OneShotPreDrawListener(View paramView, boolean paramBoolean, Runnable paramRunnable) {
    this.mView = paramView;
    this.mViewTreeObserver = paramView.getViewTreeObserver();
    this.mRunnable = paramRunnable;
    this.mReturnValue = paramBoolean;
  }
  
  public static OneShotPreDrawListener add(View paramView, Runnable paramRunnable) {
    return add(paramView, true, paramRunnable);
  }
  
  public static OneShotPreDrawListener add(View paramView, boolean paramBoolean, Runnable paramRunnable) {
    OneShotPreDrawListener oneShotPreDrawListener = new OneShotPreDrawListener(paramView, paramBoolean, paramRunnable);
    paramView.getViewTreeObserver().addOnPreDrawListener(oneShotPreDrawListener);
    paramView.addOnAttachStateChangeListener(oneShotPreDrawListener);
    return oneShotPreDrawListener;
  }
  
  public boolean onPreDraw() {
    removeListener();
    this.mRunnable.run();
    return this.mReturnValue;
  }
  
  public void removeListener() {
    if (this.mViewTreeObserver.isAlive()) {
      this.mViewTreeObserver.removeOnPreDrawListener(this);
    } else {
      this.mView.getViewTreeObserver().removeOnPreDrawListener(this);
    } 
    this.mView.removeOnAttachStateChangeListener(this);
  }
  
  public void onViewAttachedToWindow(View paramView) {
    this.mViewTreeObserver = paramView.getViewTreeObserver();
  }
  
  public void onViewDetachedFromWindow(View paramView) {
    removeListener();
  }
}
