package com.android.internal.view;

import android.annotation.NonNull;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.InsetsFlags;
import android.view.ViewDebug;
import com.android.internal.util.AnnotationValidations;

public class AppearanceRegion implements Parcelable {
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mAppearance != ((AppearanceRegion)paramObject).mAppearance || !this.mBounds.equals(((AppearanceRegion)paramObject).mBounds))
      bool = false; 
    return bool;
  }
  
  public String toString() {
    int i = this.mAppearance;
    String str = ViewDebug.flagsToString(InsetsFlags.class, "appearance", i);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("AppearanceRegion{");
    stringBuilder.append(str);
    stringBuilder.append(" bounds=");
    stringBuilder.append(this.mBounds.toShortString());
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public AppearanceRegion(int paramInt, Rect paramRect) {
    this.mAppearance = paramInt;
    this.mBounds = paramRect;
    AnnotationValidations.validate(NonNull.class, (NonNull)null, paramRect);
  }
  
  public int getAppearance() {
    return this.mAppearance;
  }
  
  public Rect getBounds() {
    return this.mBounds;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mAppearance);
    paramParcel.writeTypedObject((Parcelable)this.mBounds, paramInt);
  }
  
  public int describeContents() {
    return 0;
  }
  
  protected AppearanceRegion(Parcel paramParcel) {
    int i = paramParcel.readInt();
    Rect rect = (Rect)paramParcel.readTypedObject(Rect.CREATOR);
    this.mAppearance = i;
    this.mBounds = rect;
    AnnotationValidations.validate(NonNull.class, (NonNull)null, rect);
  }
  
  public static final Parcelable.Creator<AppearanceRegion> CREATOR = new Parcelable.Creator<AppearanceRegion>() {
      public AppearanceRegion[] newArray(int param1Int) {
        return new AppearanceRegion[param1Int];
      }
      
      public AppearanceRegion createFromParcel(Parcel param1Parcel) {
        return new AppearanceRegion(param1Parcel);
      }
    };
  
  private int mAppearance;
  
  private Rect mBounds;
  
  @Deprecated
  private void __metadata() {}
}
