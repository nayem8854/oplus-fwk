package com.android.internal.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.autofill.AutofillId;
import android.view.inputmethod.InlineSuggestionsResponse;

public interface IInlineSuggestionsResponseCallback extends IInterface {
  void onInlineSuggestionsResponse(AutofillId paramAutofillId, InlineSuggestionsResponse paramInlineSuggestionsResponse) throws RemoteException;
  
  class Default implements IInlineSuggestionsResponseCallback {
    public void onInlineSuggestionsResponse(AutofillId param1AutofillId, InlineSuggestionsResponse param1InlineSuggestionsResponse) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInlineSuggestionsResponseCallback {
    private static final String DESCRIPTOR = "com.android.internal.view.IInlineSuggestionsResponseCallback";
    
    static final int TRANSACTION_onInlineSuggestionsResponse = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.view.IInlineSuggestionsResponseCallback");
    }
    
    public static IInlineSuggestionsResponseCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.view.IInlineSuggestionsResponseCallback");
      if (iInterface != null && iInterface instanceof IInlineSuggestionsResponseCallback)
        return (IInlineSuggestionsResponseCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onInlineSuggestionsResponse";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.view.IInlineSuggestionsResponseCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.view.IInlineSuggestionsResponseCallback");
      if (param1Parcel1.readInt() != 0) {
        AutofillId autofillId = (AutofillId)AutofillId.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        InlineSuggestionsResponse inlineSuggestionsResponse = (InlineSuggestionsResponse)InlineSuggestionsResponse.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onInlineSuggestionsResponse((AutofillId)param1Parcel2, (InlineSuggestionsResponse)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IInlineSuggestionsResponseCallback {
      public static IInlineSuggestionsResponseCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.view.IInlineSuggestionsResponseCallback";
      }
      
      public void onInlineSuggestionsResponse(AutofillId param2AutofillId, InlineSuggestionsResponse param2InlineSuggestionsResponse) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInlineSuggestionsResponseCallback");
          if (param2AutofillId != null) {
            parcel.writeInt(1);
            param2AutofillId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2InlineSuggestionsResponse != null) {
            parcel.writeInt(1);
            param2InlineSuggestionsResponse.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IInlineSuggestionsResponseCallback.Stub.getDefaultImpl() != null) {
            IInlineSuggestionsResponseCallback.Stub.getDefaultImpl().onInlineSuggestionsResponse(param2AutofillId, param2InlineSuggestionsResponse);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInlineSuggestionsResponseCallback param1IInlineSuggestionsResponseCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInlineSuggestionsResponseCallback != null) {
          Proxy.sDefaultImpl = param1IInlineSuggestionsResponseCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInlineSuggestionsResponseCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
