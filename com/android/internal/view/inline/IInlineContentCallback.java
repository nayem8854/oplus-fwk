package com.android.internal.view.inline;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.SurfaceControlViewHost;

public interface IInlineContentCallback extends IInterface {
  void onClick() throws RemoteException;
  
  void onContent(SurfaceControlViewHost.SurfacePackage paramSurfacePackage, int paramInt1, int paramInt2) throws RemoteException;
  
  void onLongClick() throws RemoteException;
  
  class Default implements IInlineContentCallback {
    public void onContent(SurfaceControlViewHost.SurfacePackage param1SurfacePackage, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onClick() throws RemoteException {}
    
    public void onLongClick() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInlineContentCallback {
    private static final String DESCRIPTOR = "com.android.internal.view.inline.IInlineContentCallback";
    
    static final int TRANSACTION_onClick = 2;
    
    static final int TRANSACTION_onContent = 1;
    
    static final int TRANSACTION_onLongClick = 3;
    
    public Stub() {
      attachInterface(this, "com.android.internal.view.inline.IInlineContentCallback");
    }
    
    public static IInlineContentCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.view.inline.IInlineContentCallback");
      if (iInterface != null && iInterface instanceof IInlineContentCallback)
        return (IInlineContentCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onLongClick";
        } 
        return "onClick";
      } 
      return "onContent";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.android.internal.view.inline.IInlineContentCallback");
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.internal.view.inline.IInlineContentCallback");
          onLongClick();
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.view.inline.IInlineContentCallback");
        onClick();
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.view.inline.IInlineContentCallback");
      if (param1Parcel1.readInt() != 0) {
        SurfaceControlViewHost.SurfacePackage surfacePackage = (SurfaceControlViewHost.SurfacePackage)SurfaceControlViewHost.SurfacePackage.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      param1Int2 = param1Parcel1.readInt();
      param1Int1 = param1Parcel1.readInt();
      onContent((SurfaceControlViewHost.SurfacePackage)param1Parcel2, param1Int2, param1Int1);
      return true;
    }
    
    private static class Proxy implements IInlineContentCallback {
      public static IInlineContentCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.view.inline.IInlineContentCallback";
      }
      
      public void onContent(SurfaceControlViewHost.SurfacePackage param2SurfacePackage, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.inline.IInlineContentCallback");
          if (param2SurfacePackage != null) {
            parcel.writeInt(1);
            param2SurfacePackage.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IInlineContentCallback.Stub.getDefaultImpl() != null) {
            IInlineContentCallback.Stub.getDefaultImpl().onContent(param2SurfacePackage, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onClick() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.inline.IInlineContentCallback");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IInlineContentCallback.Stub.getDefaultImpl() != null) {
            IInlineContentCallback.Stub.getDefaultImpl().onClick();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onLongClick() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.inline.IInlineContentCallback");
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IInlineContentCallback.Stub.getDefaultImpl() != null) {
            IInlineContentCallback.Stub.getDefaultImpl().onLongClick();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInlineContentCallback param1IInlineContentCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInlineContentCallback != null) {
          Proxy.sDefaultImpl = param1IInlineContentCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInlineContentCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
