package com.android.internal.view.inline;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IInlineContentProvider extends IInterface {
  void onSurfacePackageReleased() throws RemoteException;
  
  void provideContent(int paramInt1, int paramInt2, IInlineContentCallback paramIInlineContentCallback) throws RemoteException;
  
  void requestSurfacePackage() throws RemoteException;
  
  class Default implements IInlineContentProvider {
    public void provideContent(int param1Int1, int param1Int2, IInlineContentCallback param1IInlineContentCallback) throws RemoteException {}
    
    public void requestSurfacePackage() throws RemoteException {}
    
    public void onSurfacePackageReleased() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInlineContentProvider {
    private static final String DESCRIPTOR = "com.android.internal.view.inline.IInlineContentProvider";
    
    static final int TRANSACTION_onSurfacePackageReleased = 3;
    
    static final int TRANSACTION_provideContent = 1;
    
    static final int TRANSACTION_requestSurfacePackage = 2;
    
    public Stub() {
      attachInterface(this, "com.android.internal.view.inline.IInlineContentProvider");
    }
    
    public static IInlineContentProvider asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.view.inline.IInlineContentProvider");
      if (iInterface != null && iInterface instanceof IInlineContentProvider)
        return (IInlineContentProvider)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onSurfacePackageReleased";
        } 
        return "requestSurfacePackage";
      } 
      return "provideContent";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.android.internal.view.inline.IInlineContentProvider");
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.internal.view.inline.IInlineContentProvider");
          onSurfacePackageReleased();
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.view.inline.IInlineContentProvider");
        requestSurfacePackage();
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.view.inline.IInlineContentProvider");
      param1Int2 = param1Parcel1.readInt();
      param1Int1 = param1Parcel1.readInt();
      IInlineContentCallback iInlineContentCallback = IInlineContentCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
      provideContent(param1Int2, param1Int1, iInlineContentCallback);
      return true;
    }
    
    private static class Proxy implements IInlineContentProvider {
      public static IInlineContentProvider sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.view.inline.IInlineContentProvider";
      }
      
      public void provideContent(int param2Int1, int param2Int2, IInlineContentCallback param2IInlineContentCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.view.inline.IInlineContentProvider");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2IInlineContentCallback != null) {
            iBinder = param2IInlineContentCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IInlineContentProvider.Stub.getDefaultImpl() != null) {
            IInlineContentProvider.Stub.getDefaultImpl().provideContent(param2Int1, param2Int2, param2IInlineContentCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestSurfacePackage() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.inline.IInlineContentProvider");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IInlineContentProvider.Stub.getDefaultImpl() != null) {
            IInlineContentProvider.Stub.getDefaultImpl().requestSurfacePackage();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSurfacePackageReleased() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.inline.IInlineContentProvider");
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IInlineContentProvider.Stub.getDefaultImpl() != null) {
            IInlineContentProvider.Stub.getDefaultImpl().onSurfacePackageReleased();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInlineContentProvider param1IInlineContentProvider) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInlineContentProvider != null) {
          Proxy.sDefaultImpl = param1IInlineContentProvider;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInlineContentProvider getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
