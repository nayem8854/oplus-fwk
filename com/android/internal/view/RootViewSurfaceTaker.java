package com.android.internal.view;

import android.view.InputQueue;
import android.view.PendingInsetsController;
import android.view.SurfaceHolder;

public interface RootViewSurfaceTaker {
  void onRootViewScrollYChanged(int paramInt);
  
  PendingInsetsController providePendingInsetsController();
  
  void setSurfaceFormat(int paramInt);
  
  void setSurfaceKeepScreenOn(boolean paramBoolean);
  
  void setSurfaceType(int paramInt);
  
  InputQueue.Callback willYouTakeTheInputQueue();
  
  SurfaceHolder.Callback2 willYouTakeTheSurface();
}
