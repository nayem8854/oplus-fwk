package com.android.internal.view;

import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.RemoteException;
import android.os.UserHandle;
import android.provider.Settings;
import android.util.Log;
import android.view.IWindowManager;
import android.view.WindowManagerGlobal;

public final class RotationPolicy {
  private static final int CURRENT_ROTATION = -1;
  
  public static final int NATURAL_ROTATION = 0;
  
  private static final String TAG = "RotationPolicy";
  
  public static boolean isRotationSupported(Context paramContext) {
    boolean bool;
    PackageManager packageManager = paramContext.getPackageManager();
    if (packageManager.hasSystemFeature("android.hardware.sensor.accelerometer") && 
      packageManager.hasSystemFeature("android.hardware.screen.portrait") && 
      packageManager.hasSystemFeature("android.hardware.screen.landscape") && 
      paramContext.getResources().getBoolean(17891543)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static int getRotationLockOrientation(Context paramContext) {
    if (!areAllRotationsAllowed(paramContext)) {
      Point point = new Point();
      IWindowManager iWindowManager = WindowManagerGlobal.getWindowManagerService();
      try {
        int i = paramContext.getDisplayId();
        iWindowManager.getInitialDisplaySize(i, point);
        int j = point.x;
        i = point.y;
        if (j < i) {
          i = 1;
        } else {
          i = 2;
        } 
        return i;
      } catch (RemoteException remoteException) {
        Log.w("RotationPolicy", "Unable to get the display size");
      } 
    } 
    return 0;
  }
  
  public static boolean isRotationLockToggleVisible(Context paramContext) {
    boolean bool = isRotationSupported(paramContext);
    boolean bool1 = false;
    if (bool && 
      Settings.System.getIntForUser(paramContext.getContentResolver(), "hide_rotation_lock_toggle_for_accessibility", 0, -2) == 0)
      bool1 = true; 
    return bool1;
  }
  
  public static boolean isRotationLocked(Context paramContext) {
    ContentResolver contentResolver = paramContext.getContentResolver();
    boolean bool = false;
    if (Settings.System.getIntForUser(contentResolver, "accelerometer_rotation", 0, -2) == 0)
      bool = true; 
    return bool;
  }
  
  public static void setRotationLock(Context paramContext, boolean paramBoolean) {
    boolean bool;
    if (areAllRotationsAllowed(paramContext)) {
      bool = true;
    } else {
      bool = false;
    } 
    setRotationLockAtAngle(paramContext, paramBoolean, bool);
  }
  
  public static void setRotationLockAtAngle(Context paramContext, boolean paramBoolean, int paramInt) {
    Settings.System.putIntForUser(paramContext.getContentResolver(), "hide_rotation_lock_toggle_for_accessibility", 0, -2);
    setRotationLock(paramBoolean, paramInt);
  }
  
  public static void setRotationLockForAccessibility(Context paramContext, boolean paramBoolean) {
    ContentResolver contentResolver = paramContext.getContentResolver();
    Settings.System.putIntForUser(contentResolver, "hide_rotation_lock_toggle_for_accessibility", paramBoolean, -2);
    setRotationLock(paramBoolean, 0);
  }
  
  private static boolean areAllRotationsAllowed(Context paramContext) {
    return paramContext.getResources().getBoolean(17891341);
  }
  
  private static void setRotationLock(final boolean enabled, final int rotation) {
    AsyncTask.execute(new Runnable() {
          final boolean val$enabled;
          
          final int val$rotation;
          
          public void run() {
            try {
              IWindowManager iWindowManager = WindowManagerGlobal.getWindowManagerService();
              if (enabled) {
                iWindowManager.freezeRotation(rotation);
              } else {
                iWindowManager.thawRotation();
              } 
            } catch (RemoteException remoteException) {
              Log.w("RotationPolicy", "Unable to save auto-rotate setting");
            } 
          }
        });
  }
  
  public static void registerRotationPolicyListener(Context paramContext, RotationPolicyListener paramRotationPolicyListener) {
    registerRotationPolicyListener(paramContext, paramRotationPolicyListener, UserHandle.getCallingUserId());
  }
  
  public static void registerRotationPolicyListener(Context paramContext, RotationPolicyListener paramRotationPolicyListener, int paramInt) {
    paramContext.getContentResolver().registerContentObserver(Settings.System.getUriFor("accelerometer_rotation"), false, paramRotationPolicyListener.mObserver, paramInt);
    paramContext.getContentResolver().registerContentObserver(Settings.System.getUriFor("hide_rotation_lock_toggle_for_accessibility"), false, paramRotationPolicyListener.mObserver, paramInt);
  }
  
  public static void unregisterRotationPolicyListener(Context paramContext, RotationPolicyListener paramRotationPolicyListener) {
    paramContext.getContentResolver().unregisterContentObserver(paramRotationPolicyListener.mObserver);
  }
  
  public static abstract class RotationPolicyListener {
    final ContentObserver mObserver;
    
    public RotationPolicyListener() {
      this.mObserver = (ContentObserver)new Object(this, new Handler());
    }
    
    public abstract void onChange();
  }
}
