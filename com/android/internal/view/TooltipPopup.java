package com.android.internal.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.IBinder;
import android.util.Slog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.WindowManagerGlobal;
import android.widget.TextView;

public class TooltipPopup {
  private final WindowManager.LayoutParams mLayoutParams = new WindowManager.LayoutParams();
  
  private final Rect mTmpDisplayFrame = new Rect();
  
  private final int[] mTmpAnchorPos = new int[2];
  
  private final int[] mTmpAppPos = new int[2];
  
  private static final String TAG = "TooltipPopup";
  
  private final View mContentView;
  
  private final Context mContext;
  
  private final TextView mMessageView;
  
  public TooltipPopup(Context paramContext) {
    this.mContext = paramContext;
    View view = LayoutInflater.from(paramContext).inflate(17367341, null);
    this.mMessageView = (TextView)view.findViewById(16908299);
    WindowManager.LayoutParams layoutParams = this.mLayoutParams;
    Context context = this.mContext;
    String str = context.getString(17041414);
    layoutParams.setTitle(str);
    this.mLayoutParams.packageName = this.mContext.getOpPackageName();
    this.mLayoutParams.type = 1005;
    this.mLayoutParams.width = -2;
    this.mLayoutParams.height = -2;
    this.mLayoutParams.format = -3;
    this.mLayoutParams.windowAnimations = 16974600;
    this.mLayoutParams.flags = 24;
  }
  
  public void show(View paramView, int paramInt1, int paramInt2, boolean paramBoolean, CharSequence paramCharSequence) {
    if (isShowing())
      hide(); 
    this.mMessageView.setText(paramCharSequence);
    computePosition(paramView, paramInt1, paramInt2, paramBoolean, this.mLayoutParams);
    WindowManager windowManager = (WindowManager)this.mContext.getSystemService("window");
    windowManager.addView(this.mContentView, (ViewGroup.LayoutParams)this.mLayoutParams);
  }
  
  public void hide() {
    if (!isShowing())
      return; 
    WindowManager windowManager = (WindowManager)this.mContext.getSystemService("window");
    windowManager.removeView(this.mContentView);
  }
  
  public View getContentView() {
    return this.mContentView;
  }
  
  public boolean isShowing() {
    boolean bool;
    if (this.mContentView.getParent() != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void computePosition(View paramView, int paramInt1, int paramInt2, boolean paramBoolean, WindowManager.LayoutParams paramLayoutParams) {
    paramLayoutParams.token = paramView.getApplicationWindowToken();
    int i = this.mContext.getResources().getDimensionPixelOffset(17105541);
    if (paramView.getWidth() < i)
      paramInt1 = paramView.getWidth() / 2; 
    if (paramView.getHeight() >= i) {
      i = this.mContext.getResources().getDimensionPixelOffset(17105540);
      j = paramInt2 + i;
      i = paramInt2 - i;
      paramInt2 = j;
    } else {
      paramInt2 = paramView.getHeight();
      i = 0;
    } 
    paramLayoutParams.gravity = 49;
    Resources resources = this.mContext.getResources();
    if (paramBoolean) {
      j = 17105544;
    } else {
      j = 17105543;
    } 
    int j = resources.getDimensionPixelOffset(j);
    WindowManagerGlobal windowManagerGlobal = WindowManagerGlobal.getInstance();
    IBinder iBinder = paramView.getApplicationWindowToken();
    View view = windowManagerGlobal.getWindowView(iBinder);
    if (view == null) {
      Slog.e("TooltipPopup", "Cannot find app view");
      return;
    } 
    view.getWindowVisibleDisplayFrame(this.mTmpDisplayFrame);
    view.getLocationOnScreen(this.mTmpAppPos);
    paramView.getLocationOnScreen(this.mTmpAnchorPos);
    int arrayOfInt2[] = this.mTmpAnchorPos, k = arrayOfInt2[0], arrayOfInt1[] = this.mTmpAppPos;
    arrayOfInt2[0] = k - arrayOfInt1[0];
    arrayOfInt2[1] = arrayOfInt2[1] - arrayOfInt1[1];
    paramLayoutParams.x = arrayOfInt2[0] + paramInt1 - view.getWidth() / 2;
    paramInt1 = View.MeasureSpec.makeMeasureSpec(0, 0);
    this.mContentView.measure(paramInt1, paramInt1);
    paramInt1 = this.mContentView.getMeasuredHeight();
    arrayOfInt1 = this.mTmpAnchorPos;
    i = arrayOfInt1[1] + i - j - paramInt1;
    paramInt2 = arrayOfInt1[1] + paramInt2 + j;
    if (paramBoolean) {
      if (i >= 0) {
        paramLayoutParams.y = i;
      } else {
        paramLayoutParams.y = paramInt2;
      } 
    } else if (paramInt2 + paramInt1 <= this.mTmpDisplayFrame.height()) {
      paramLayoutParams.y = paramInt2;
    } else {
      paramLayoutParams.y = i;
    } 
  }
}
