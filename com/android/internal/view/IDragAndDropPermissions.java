package com.android.internal.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IDragAndDropPermissions extends IInterface {
  void release() throws RemoteException;
  
  void take(IBinder paramIBinder) throws RemoteException;
  
  void takeTransient(IBinder paramIBinder) throws RemoteException;
  
  class Default implements IDragAndDropPermissions {
    public void take(IBinder param1IBinder) throws RemoteException {}
    
    public void takeTransient(IBinder param1IBinder) throws RemoteException {}
    
    public void release() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDragAndDropPermissions {
    private static final String DESCRIPTOR = "com.android.internal.view.IDragAndDropPermissions";
    
    static final int TRANSACTION_release = 3;
    
    static final int TRANSACTION_take = 1;
    
    static final int TRANSACTION_takeTransient = 2;
    
    public Stub() {
      attachInterface(this, "com.android.internal.view.IDragAndDropPermissions");
    }
    
    public static IDragAndDropPermissions asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.view.IDragAndDropPermissions");
      if (iInterface != null && iInterface instanceof IDragAndDropPermissions)
        return (IDragAndDropPermissions)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "release";
        } 
        return "takeTransient";
      } 
      return "take";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.android.internal.view.IDragAndDropPermissions");
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.internal.view.IDragAndDropPermissions");
          release();
          param1Parcel2.writeNoException();
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.view.IDragAndDropPermissions");
        iBinder = param1Parcel1.readStrongBinder();
        takeTransient(iBinder);
        param1Parcel2.writeNoException();
        return true;
      } 
      iBinder.enforceInterface("com.android.internal.view.IDragAndDropPermissions");
      IBinder iBinder = iBinder.readStrongBinder();
      take(iBinder);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IDragAndDropPermissions {
      public static IDragAndDropPermissions sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.view.IDragAndDropPermissions";
      }
      
      public void take(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.view.IDragAndDropPermissions");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IDragAndDropPermissions.Stub.getDefaultImpl() != null) {
            IDragAndDropPermissions.Stub.getDefaultImpl().take(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void takeTransient(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.view.IDragAndDropPermissions");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IDragAndDropPermissions.Stub.getDefaultImpl() != null) {
            IDragAndDropPermissions.Stub.getDefaultImpl().takeTransient(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void release() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.view.IDragAndDropPermissions");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IDragAndDropPermissions.Stub.getDefaultImpl() != null) {
            IDragAndDropPermissions.Stub.getDefaultImpl().release();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDragAndDropPermissions param1IDragAndDropPermissions) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDragAndDropPermissions != null) {
          Proxy.sDefaultImpl = param1IDragAndDropPermissions;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDragAndDropPermissions getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
