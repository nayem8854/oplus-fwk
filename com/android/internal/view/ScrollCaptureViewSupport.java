package com.android.internal.view;

import android.graphics.HardwareRenderer;
import android.graphics.Matrix;
import android.graphics.RecordingCanvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.RenderNode;
import android.os.Handler;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.view.ScrollCaptureCallback;
import android.view.ScrollCaptureSession;
import android.view.Surface;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.function.Consumer;

public class ScrollCaptureViewSupport<V extends View> implements ScrollCaptureCallback {
  private boolean mEnded;
  
  private ViewRenderer mRenderer;
  
  private boolean mStarted;
  
  private Handler mUiHandler;
  
  private final ScrollCaptureViewHelper<V> mViewHelper;
  
  private final WeakReference<V> mWeakView;
  
  static <V extends View> ScrollCaptureCallback createCallback(V paramV, ScrollCaptureViewHelper<V> paramScrollCaptureViewHelper) {
    return new ScrollCaptureViewSupport<>(paramV, paramScrollCaptureViewHelper);
  }
  
  ScrollCaptureViewSupport(V paramV, ScrollCaptureViewHelper<V> paramScrollCaptureViewHelper) {
    this.mWeakView = new WeakReference<>(paramV);
    this.mRenderer = new ViewRenderer();
    this.mUiHandler = paramV.getHandler();
    this.mViewHelper = paramScrollCaptureViewHelper;
  }
  
  public final void onScrollCaptureSearch(Consumer<Rect> paramConsumer) {
    View view = (View)this.mWeakView.get();
    this.mStarted = false;
    this.mEnded = false;
    if (view != null && view.isVisibleToUser() && this.mViewHelper.onAcceptSession((V)view)) {
      paramConsumer.accept(this.mViewHelper.onComputeScrollBounds((V)view));
      return;
    } 
    paramConsumer.accept(null);
  }
  
  public final void onScrollCaptureStart(ScrollCaptureSession paramScrollCaptureSession, Runnable paramRunnable) {
    View view = (View)this.mWeakView.get();
    this.mEnded = false;
    this.mStarted = true;
    if (view != null && view.isVisibleToUser()) {
      this.mRenderer.setSurface(paramScrollCaptureSession.getSurface());
      this.mViewHelper.onPrepareForStart((V)view, paramScrollCaptureSession.getScrollBounds());
    } 
    paramRunnable.run();
  }
  
  public final void onScrollCaptureImageRequest(ScrollCaptureSession paramScrollCaptureSession, Rect paramRect) {
    View view = (View)this.mWeakView.get();
    if (view == null || !view.isVisibleToUser()) {
      paramScrollCaptureSession.notifyBufferSent(0L, null);
      return;
    } 
    paramRect = this.mViewHelper.onScrollRequested((V)view, paramScrollCaptureSession.getScrollBounds(), paramRect);
    this.mRenderer.renderFrame(view, paramRect, this.mUiHandler, new _$$Lambda$ScrollCaptureViewSupport$WhYdis6PgpbNdc_fEsMsJ4b7okA(paramScrollCaptureSession, paramRect));
  }
  
  public final void onScrollCaptureEnd(Runnable paramRunnable) {
    View view = (View)this.mWeakView.get();
    if (this.mStarted && !this.mEnded) {
      this.mViewHelper.onPrepareForEnd((V)view);
      this.mEnded = true;
      this.mRenderer.trimMemory();
      this.mRenderer.setSurface(null);
    } 
    paramRunnable.run();
  }
  
  class ViewRenderer {
    private final RectF mTempRectF = new RectF();
    
    private final Rect mSourceRect = new Rect();
    
    private final Rect mTempRect = new Rect();
    
    private final Matrix mTempMatrix = new Matrix();
    
    private final int[] mTempLocation = new int[2];
    
    private long mLastRenderedSourceDrawingId = -1L;
    
    private static final float AMBIENT_SHADOW_ALPHA = 0.039F;
    
    private static final float LIGHT_RADIUS_DP = 800.0F;
    
    private static final float LIGHT_Z_DP = 400.0F;
    
    private static final float SPOT_SHADOW_ALPHA = 0.039F;
    
    private static final String TAG = "ViewRenderer";
    
    private HardwareRenderer mRenderer;
    
    private RenderNode mRootRenderNode;
    
    ViewRenderer() {
      this.mRenderer = new HardwareRenderer();
      RenderNode renderNode = new RenderNode("ScrollCaptureRoot");
      this.mRenderer.setContentRoot(renderNode);
      this.mRenderer.setOpaque(false);
    }
    
    public void setSurface(Surface param1Surface) {
      this.mRenderer.setSurface(param1Surface);
    }
    
    private boolean updateForView(View param1View) {
      if (this.mLastRenderedSourceDrawingId == param1View.getUniqueDrawingId())
        return false; 
      this.mLastRenderedSourceDrawingId = param1View.getUniqueDrawingId();
      return true;
    }
    
    private void setupLighting(View param1View) {
      this.mLastRenderedSourceDrawingId = param1View.getUniqueDrawingId();
      DisplayMetrics displayMetrics = param1View.getResources().getDisplayMetrics();
      param1View.getLocationOnScreen(this.mTempLocation);
      float f1 = displayMetrics.widthPixels / 2.0F, f2 = this.mTempLocation[0];
      float f3 = (displayMetrics.heightPixels - this.mTempLocation[1]);
      int i = (int)(displayMetrics.density * 400.0F);
      int j = (int)(displayMetrics.density * 800.0F);
      this.mRenderer.setLightSourceGeometry(f1 - f2, f3, i, j);
      this.mRenderer.setLightSourceAlpha(0.039F, 0.039F);
    }
    
    public void renderFrame(View param1View, Rect param1Rect, Handler param1Handler, Runnable param1Runnable) {
      if (updateForView(param1View))
        setupLighting(param1View); 
      buildRootDisplayList(param1View, param1Rect);
      HardwareRenderer.FrameRenderRequest frameRenderRequest = this.mRenderer.createRenderRequest();
      frameRenderRequest.setVsyncTime(SystemClock.elapsedRealtimeNanos());
      Objects.requireNonNull(param1Handler);
      frameRenderRequest.setFrameCommitCallback(new _$$Lambda$LfzJt661qZfn2w_6SYHFbD3aMy0(param1Handler), param1Runnable);
      frameRenderRequest.setWaitForPresent(true);
      frameRenderRequest.syncAndDraw();
    }
    
    public void trimMemory() {
      this.mRenderer.clearContent();
    }
    
    public void destroy() {
      this.mRenderer.destroy();
    }
    
    private void transformToRoot(View param1View, Rect param1Rect1, Rect param1Rect2) {
      this.mTempMatrix.reset();
      param1View.transformMatrixToGlobal(this.mTempMatrix);
      this.mTempRectF.set(param1Rect1);
      this.mTempMatrix.mapRect(this.mTempRectF);
      this.mTempRectF.round(param1Rect2);
    }
    
    private void buildRootDisplayList(View param1View, Rect param1Rect) {
      View view = param1View.getRootView();
      transformToRoot(param1View, param1Rect, this.mTempRect);
      this.mRootRenderNode.setPosition(0, 0, this.mTempRect.width(), this.mTempRect.height());
      RenderNode renderNode = this.mRootRenderNode;
      int i = this.mTempRect.width();
      Rect rect = this.mTempRect;
      int j = rect.height();
      RecordingCanvas recordingCanvas = renderNode.beginRecording(i, j);
      recordingCanvas.translate(-this.mTempRect.left, -this.mTempRect.top);
      recordingCanvas.drawRenderNode(view.updateDisplayListIfDirty());
      this.mRootRenderNode.endRecording();
    }
  }
}
