package com.android.internal.view;

import android.view.SurfaceHolder;

public class SurfaceCallbackHelper {
  int mFinishDrawingCollected = 0;
  
  int mFinishDrawingExpected = 0;
  
  private Runnable mFinishDrawingRunnable = new Runnable() {
      final SurfaceCallbackHelper this$0;
      
      public void run() {
        synchronized (SurfaceCallbackHelper.this) {
          SurfaceCallbackHelper surfaceCallbackHelper = SurfaceCallbackHelper.this;
          surfaceCallbackHelper.mFinishDrawingCollected++;
          if (SurfaceCallbackHelper.this.mFinishDrawingCollected < SurfaceCallbackHelper.this.mFinishDrawingExpected)
            return; 
          SurfaceCallbackHelper.this.mRunnable.run();
          return;
        } 
      }
    };
  
  Runnable mRunnable;
  
  public SurfaceCallbackHelper(Runnable paramRunnable) {
    this.mRunnable = paramRunnable;
  }
  
  public void dispatchSurfaceRedrawNeededAsync(SurfaceHolder paramSurfaceHolder, SurfaceHolder.Callback[] paramArrayOfCallback) {
    // Byte code:
    //   0: aload_2
    //   1: ifnull -> 91
    //   4: aload_2
    //   5: arraylength
    //   6: ifne -> 12
    //   9: goto -> 91
    //   12: aload_0
    //   13: monitorenter
    //   14: aload_0
    //   15: aload_2
    //   16: arraylength
    //   17: putfield mFinishDrawingExpected : I
    //   20: iconst_0
    //   21: istore_3
    //   22: aload_0
    //   23: iconst_0
    //   24: putfield mFinishDrawingCollected : I
    //   27: aload_0
    //   28: monitorexit
    //   29: aload_2
    //   30: arraylength
    //   31: istore #4
    //   33: iload_3
    //   34: iload #4
    //   36: if_icmpge -> 85
    //   39: aload_2
    //   40: iload_3
    //   41: aaload
    //   42: astore #5
    //   44: aload #5
    //   46: instanceof android/view/SurfaceHolder$Callback2
    //   49: ifeq -> 70
    //   52: aload #5
    //   54: checkcast android/view/SurfaceHolder$Callback2
    //   57: aload_1
    //   58: aload_0
    //   59: getfield mFinishDrawingRunnable : Ljava/lang/Runnable;
    //   62: invokeinterface surfaceRedrawNeededAsync : (Landroid/view/SurfaceHolder;Ljava/lang/Runnable;)V
    //   67: goto -> 79
    //   70: aload_0
    //   71: getfield mFinishDrawingRunnable : Ljava/lang/Runnable;
    //   74: invokeinterface run : ()V
    //   79: iinc #3, 1
    //   82: goto -> 33
    //   85: return
    //   86: astore_1
    //   87: aload_0
    //   88: monitorexit
    //   89: aload_1
    //   90: athrow
    //   91: aload_0
    //   92: getfield mRunnable : Ljava/lang/Runnable;
    //   95: invokeinterface run : ()V
    //   100: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #47	-> 0
    //   #52	-> 12
    //   #53	-> 14
    //   #54	-> 20
    //   #55	-> 27
    //   #57	-> 29
    //   #58	-> 44
    //   #59	-> 52
    //   #62	-> 70
    //   #57	-> 79
    //   #65	-> 85
    //   #55	-> 86
    //   #48	-> 91
    //   #49	-> 100
    // Exception table:
    //   from	to	target	type
    //   14	20	86	finally
    //   22	27	86	finally
    //   27	29	86	finally
    //   87	89	86	finally
  }
}
