package com.android.internal.view;

import android.inputmethodservice.AbstractInputMethodService;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CorrectionInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionInspector;
import android.view.inputmethod.InputContentInfo;
import com.android.internal.inputmethod.CancellationGroup;
import com.android.internal.inputmethod.ICharSequenceResultCallback;
import com.android.internal.inputmethod.IExtractedTextResultCallback;
import com.android.internal.inputmethod.IIntResultCallback;
import com.android.internal.inputmethod.ResultCallbacks;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

public class InputConnectionWrapper implements InputConnection {
  private static final int MAX_WAIT_TIME_MILLIS = 2000;
  
  private static final String TAG = "InputConnectionWrapper";
  
  private final CancellationGroup mCancellationGroup;
  
  private final IInputContext mIInputContext;
  
  private final WeakReference<AbstractInputMethodService> mInputMethodService;
  
  private final int mMissingMethods;
  
  public InputConnectionWrapper(WeakReference<AbstractInputMethodService> paramWeakReference, IInputContext paramIInputContext, int paramInt, CancellationGroup paramCancellationGroup) {
    this.mInputMethodService = paramWeakReference;
    this.mIInputContext = paramIInputContext;
    this.mMissingMethods = paramInt;
    this.mCancellationGroup = paramCancellationGroup;
  }
  
  private static void logInternal(String paramString, boolean paramBoolean, Object paramObject) {
    if (paramBoolean) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append(" didn't respond in ");
      stringBuilder.append(2000);
      stringBuilder.append(" msec. Returning default: ");
      stringBuilder.append(paramObject);
      Log.w("InputConnectionWrapper", stringBuilder.toString());
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append(" was canceled before complete. Returning default: ");
      stringBuilder.append(paramObject);
      Log.w("InputConnectionWrapper", stringBuilder.toString());
    } 
  }
  
  private static int getResultOrZero(CancellationGroup.Completable.Int paramInt, String paramString) {
    boolean bool = paramInt.await(2000, TimeUnit.MILLISECONDS);
    if (paramInt.hasValue())
      return paramInt.getValue(); 
    logInternal(paramString, bool, Integer.valueOf(0));
    return 0;
  }
  
  private static <T> T getResultOrNull(CancellationGroup.Completable.Values<T> paramValues, String paramString) {
    boolean bool = paramValues.await(2000, TimeUnit.MILLISECONDS);
    if (paramValues.hasValue())
      return (T)paramValues.getValue(); 
    logInternal(paramString, bool, null);
    return null;
  }
  
  public CharSequence getTextAfterCursor(int paramInt1, int paramInt2) {
    if (this.mCancellationGroup.isCanceled())
      return null; 
    CancellationGroup cancellationGroup = this.mCancellationGroup;
    CancellationGroup.Completable.CharSequence charSequence = cancellationGroup.createCompletableCharSequence();
    try {
      this.mIInputContext.getTextAfterCursor(paramInt1, paramInt2, (ICharSequenceResultCallback)ResultCallbacks.of(charSequence));
      return getResultOrNull((CancellationGroup.Completable.Values<CharSequence>)charSequence, "getTextAfterCursor()");
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public CharSequence getTextBeforeCursor(int paramInt1, int paramInt2) {
    if (this.mCancellationGroup.isCanceled())
      return null; 
    CancellationGroup cancellationGroup = this.mCancellationGroup;
    CancellationGroup.Completable.CharSequence charSequence = cancellationGroup.createCompletableCharSequence();
    try {
      this.mIInputContext.getTextBeforeCursor(paramInt1, paramInt2, (ICharSequenceResultCallback)ResultCallbacks.of(charSequence));
      return getResultOrNull((CancellationGroup.Completable.Values<CharSequence>)charSequence, "getTextBeforeCursor()");
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public CharSequence getSelectedText(int paramInt) {
    if (this.mCancellationGroup.isCanceled())
      return null; 
    if (isMethodMissing(1))
      return null; 
    CancellationGroup cancellationGroup = this.mCancellationGroup;
    CancellationGroup.Completable.CharSequence charSequence = cancellationGroup.createCompletableCharSequence();
    try {
      this.mIInputContext.getSelectedText(paramInt, (ICharSequenceResultCallback)ResultCallbacks.of(charSequence));
      return getResultOrNull((CancellationGroup.Completable.Values<CharSequence>)charSequence, "getSelectedText()");
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public int getCursorCapsMode(int paramInt) {
    if (this.mCancellationGroup.isCanceled())
      return 0; 
    CancellationGroup cancellationGroup = this.mCancellationGroup;
    CancellationGroup.Completable.Int int_ = cancellationGroup.createCompletableInt();
    try {
      this.mIInputContext.getCursorCapsMode(paramInt, (IIntResultCallback)ResultCallbacks.of(int_));
      return getResultOrZero(int_, "getCursorCapsMode()");
    } catch (RemoteException remoteException) {
      return 0;
    } 
  }
  
  public ExtractedText getExtractedText(ExtractedTextRequest paramExtractedTextRequest, int paramInt) {
    if (this.mCancellationGroup.isCanceled())
      return null; 
    CancellationGroup cancellationGroup = this.mCancellationGroup;
    CancellationGroup.Completable.ExtractedText extractedText = cancellationGroup.createCompletableExtractedText();
    try {
      this.mIInputContext.getExtractedText(paramExtractedTextRequest, paramInt, (IExtractedTextResultCallback)ResultCallbacks.of(extractedText));
      return getResultOrNull((CancellationGroup.Completable.Values<ExtractedText>)extractedText, "getExtractedText()");
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public boolean commitText(CharSequence paramCharSequence, int paramInt) {
    try {
      this.mIInputContext.commitText(paramCharSequence, paramInt);
      notifyUserActionIfNecessary();
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  private void notifyUserActionIfNecessary() {
    AbstractInputMethodService abstractInputMethodService = this.mInputMethodService.get();
    if (abstractInputMethodService == null)
      return; 
    abstractInputMethodService.notifyUserActionIfNecessary();
  }
  
  public boolean commitCompletion(CompletionInfo paramCompletionInfo) {
    if (isMethodMissing(4))
      return false; 
    try {
      this.mIInputContext.commitCompletion(paramCompletionInfo);
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean commitCorrection(CorrectionInfo paramCorrectionInfo) {
    try {
      this.mIInputContext.commitCorrection(paramCorrectionInfo);
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean setSelection(int paramInt1, int paramInt2) {
    try {
      this.mIInputContext.setSelection(paramInt1, paramInt2);
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean performEditorAction(int paramInt) {
    try {
      this.mIInputContext.performEditorAction(paramInt);
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean performContextMenuAction(int paramInt) {
    try {
      this.mIInputContext.performContextMenuAction(paramInt);
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean setComposingRegion(int paramInt1, int paramInt2) {
    if (isMethodMissing(2))
      return false; 
    try {
      this.mIInputContext.setComposingRegion(paramInt1, paramInt2);
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean setComposingText(CharSequence paramCharSequence, int paramInt) {
    try {
      this.mIInputContext.setComposingText(paramCharSequence, paramInt);
      notifyUserActionIfNecessary();
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean finishComposingText() {
    try {
      this.mIInputContext.finishComposingText();
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean beginBatchEdit() {
    try {
      this.mIInputContext.beginBatchEdit();
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean endBatchEdit() {
    try {
      this.mIInputContext.endBatchEdit();
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean sendKeyEvent(KeyEvent paramKeyEvent) {
    try {
      this.mIInputContext.sendKeyEvent(paramKeyEvent);
      notifyUserActionIfNecessary();
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean clearMetaKeyStates(int paramInt) {
    try {
      this.mIInputContext.clearMetaKeyStates(paramInt);
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean deleteSurroundingText(int paramInt1, int paramInt2) {
    try {
      this.mIInputContext.deleteSurroundingText(paramInt1, paramInt2);
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean deleteSurroundingTextInCodePoints(int paramInt1, int paramInt2) {
    if (isMethodMissing(16))
      return false; 
    try {
      this.mIInputContext.deleteSurroundingTextInCodePoints(paramInt1, paramInt2);
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean reportFullscreenMode(boolean paramBoolean) {
    return false;
  }
  
  public boolean performPrivateCommand(String paramString, Bundle paramBundle) {
    try {
      this.mIInputContext.performPrivateCommand(paramString, paramBundle);
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean requestCursorUpdates(int paramInt) {
    boolean bool = this.mCancellationGroup.isCanceled();
    boolean bool1 = false;
    if (bool)
      return false; 
    if (isMethodMissing(8))
      return false; 
    CancellationGroup.Completable.Int int_ = this.mCancellationGroup.createCompletableInt();
    try {
      IInputContext iInputContext = this.mIInputContext;
      IIntResultCallback.Stub stub = ResultCallbacks.of(int_);
      iInputContext.requestUpdateCursorAnchorInfo(paramInt, (IIntResultCallback)stub);
      if (getResultOrZero(int_, "requestUpdateCursorAnchorInfo()") != 0)
        bool1 = true; 
      return bool1;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public Handler getHandler() {
    return null;
  }
  
  public void closeConnection() {}
  
  public boolean commitContent(InputContentInfo paramInputContentInfo, int paramInt, Bundle paramBundle) {
    boolean bool = this.mCancellationGroup.isCanceled();
    boolean bool1 = false;
    if (bool)
      return false; 
    if (isMethodMissing(128))
      return false; 
    if ((paramInt & 0x1) != 0) {
      AbstractInputMethodService abstractInputMethodService = this.mInputMethodService.get();
      if (abstractInputMethodService == null)
        return false; 
      abstractInputMethodService.exposeContent(paramInputContentInfo, this);
    } 
    CancellationGroup.Completable.Int int_ = this.mCancellationGroup.createCompletableInt();
    try {
      this.mIInputContext.commitContent(paramInputContentInfo, paramInt, paramBundle, (IIntResultCallback)ResultCallbacks.of(int_));
      if (getResultOrZero(int_, "commitContent()") != 0)
        bool1 = true; 
      return bool1;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  private boolean isMethodMissing(int paramInt) {
    boolean bool;
    if ((this.mMissingMethods & paramInt) == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("InputConnectionWrapper{idHash=#");
    stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
    stringBuilder.append(" mMissingMethods=");
    int i = this.mMissingMethods;
    stringBuilder.append(InputConnectionInspector.getMissingMethodFlagsAsString(i));
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
}
