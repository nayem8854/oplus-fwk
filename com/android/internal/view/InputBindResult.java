package com.android.internal.view;

import android.graphics.Matrix;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.InputChannel;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class InputBindResult implements Parcelable {
  public Matrix getActivityViewToScreenMatrix() {
    if (this.mActivityViewToScreenMatrixValues == null)
      return null; 
    Matrix matrix = new Matrix();
    matrix.setValues(this.mActivityViewToScreenMatrixValues);
    return matrix;
  }
  
  public InputBindResult(int paramInt1, IInputMethodSession paramIInputMethodSession, InputChannel paramInputChannel, String paramString, int paramInt2, Matrix paramMatrix) {
    this.result = paramInt1;
    this.method = paramIInputMethodSession;
    this.channel = paramInputChannel;
    this.id = paramString;
    this.sequence = paramInt2;
    if (paramMatrix == null) {
      this.mActivityViewToScreenMatrixValues = null;
    } else {
      float[] arrayOfFloat = new float[9];
      paramMatrix.getValues(arrayOfFloat);
    } 
  }
  
  InputBindResult(Parcel paramParcel) {
    this.result = paramParcel.readInt();
    this.method = IInputMethodSession.Stub.asInterface(paramParcel.readStrongBinder());
    if (paramParcel.readInt() != 0) {
      this.channel = (InputChannel)InputChannel.CREATOR.createFromParcel(paramParcel);
    } else {
      this.channel = null;
    } 
    this.id = paramParcel.readString();
    this.sequence = paramParcel.readInt();
    this.mActivityViewToScreenMatrixValues = paramParcel.createFloatArray();
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("InputBindResult{result=");
    stringBuilder.append(getResultString());
    stringBuilder.append(" method=");
    stringBuilder.append(this.method);
    stringBuilder.append(" id=");
    stringBuilder.append(this.id);
    stringBuilder.append(" sequence=");
    stringBuilder.append(this.sequence);
    stringBuilder.append(" activityViewToScreenMatrix=");
    stringBuilder.append(getActivityViewToScreenMatrix());
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.result);
    paramParcel.writeStrongInterface(this.method);
    if (this.channel != null) {
      paramParcel.writeInt(1);
      this.channel.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeString(this.id);
    paramParcel.writeInt(this.sequence);
    paramParcel.writeFloatArray(this.mActivityViewToScreenMatrixValues);
  }
  
  public static final Parcelable.Creator<InputBindResult> CREATOR = new Parcelable.Creator<InputBindResult>() {
      public InputBindResult createFromParcel(Parcel param1Parcel) {
        return new InputBindResult(param1Parcel);
      }
      
      public InputBindResult[] newArray(int param1Int) {
        return new InputBindResult[param1Int];
      }
    };
  
  public static final InputBindResult DISPLAY_ID_MISMATCH;
  
  public static final InputBindResult IME_NOT_CONNECTED;
  
  public static final InputBindResult INVALID_CLIENT;
  
  public static final InputBindResult INVALID_DISPLAY_ID;
  
  public static final InputBindResult INVALID_PACKAGE_NAME;
  
  public static final InputBindResult INVALID_USER;
  
  public static final InputBindResult NOT_IME_TARGET_WINDOW;
  
  public static final InputBindResult NO_EDITOR;
  
  public static final InputBindResult NO_IME;
  
  public int describeContents() {
    boolean bool;
    InputChannel inputChannel = this.channel;
    if (inputChannel != null) {
      bool = inputChannel.describeContents();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String getResultString() {
    StringBuilder stringBuilder;
    switch (this.result) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown(");
        stringBuilder.append(this.result);
        stringBuilder.append(")");
        return stringBuilder.toString();
      case 16:
        return "ERROR_INVALID_CLIENT";
      case 15:
        return "ERROR_INVALID_DISPLAY_ID";
      case 14:
        return "ERROR_DISPLAY_ID_MISMATCH";
      case 13:
        return "ERROR_NO_EDITOR";
      case 12:
        return "ERROR_NOT_IME_TARGET_WINDOW";
      case 11:
        return "ERROR_NULL_EDITOR_INFO";
      case 10:
        return "ERROR_INVALID_USER";
      case 9:
        return "ERROR_IME_NOT_CONNECTED";
      case 8:
        return "ERROR_SYSTEM_NOT_READY";
      case 7:
        return "ERROR_INVALID_PACKAGE_NAME";
      case 6:
        return "ERROR_NO_IME";
      case 5:
        return "ERROR_NULL";
      case 4:
        return "SUCCESS_REPORT_WINDOW_FOCUS_ONLY";
      case 3:
        return "SUCCESS_WAITING_USER_SWITCHING";
      case 2:
        return "SUCCESS_WAITING_IME_BINDING";
      case 1:
        return "SUCCESS_WAITING_IME_SESSION";
      case 0:
        break;
    } 
    return "SUCCESS_WITH_IME_SESSION";
  }
  
  private static InputBindResult error(int paramInt) {
    return new InputBindResult(paramInt, null, null, null, -1, null);
  }
  
  public static final InputBindResult NULL = error(5);
  
  public static final InputBindResult NULL_EDITOR_INFO;
  
  public static final InputBindResult USER_SWITCHING;
  
  public final InputChannel channel;
  
  public final String id;
  
  private final float[] mActivityViewToScreenMatrixValues;
  
  public final IInputMethodSession method;
  
  public final int result;
  
  public final int sequence;
  
  static {
    NO_IME = error(6);
    NO_EDITOR = error(13);
    INVALID_PACKAGE_NAME = error(7);
    NULL_EDITOR_INFO = error(11);
    NOT_IME_TARGET_WINDOW = error(12);
    IME_NOT_CONNECTED = error(9);
    INVALID_USER = error(10);
    DISPLAY_ID_MISMATCH = error(14);
    INVALID_DISPLAY_ID = error(15);
    INVALID_CLIENT = error(16);
    USER_SWITCHING = error(3);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ResultCode implements Annotation {
    public static final int ERROR_DISPLAY_ID_MISMATCH = 14;
    
    public static final int ERROR_IME_NOT_CONNECTED = 9;
    
    public static final int ERROR_INVALID_CLIENT = 16;
    
    public static final int ERROR_INVALID_DISPLAY_ID = 15;
    
    public static final int ERROR_INVALID_PACKAGE_NAME = 7;
    
    public static final int ERROR_INVALID_USER = 10;
    
    public static final int ERROR_NOT_IME_TARGET_WINDOW = 12;
    
    public static final int ERROR_NO_EDITOR = 13;
    
    public static final int ERROR_NO_IME = 6;
    
    public static final int ERROR_NULL = 5;
    
    public static final int ERROR_NULL_EDITOR_INFO = 11;
    
    public static final int ERROR_SYSTEM_NOT_READY = 8;
    
    public static final int SUCCESS_REPORT_WINDOW_FOCUS_ONLY = 4;
    
    public static final int SUCCESS_WAITING_IME_BINDING = 2;
    
    public static final int SUCCESS_WAITING_IME_SESSION = 1;
    
    public static final int SUCCESS_WAITING_USER_SWITCHING = 3;
    
    public static final int SUCCESS_WITH_IME_SESSION = 0;
  }
}
