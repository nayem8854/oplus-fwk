package com.android.internal.view.menu;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.widget.ActionMenuView;
import android.widget.ForwardingListener;
import android.widget.TextView;
import com.android.internal.R;

public class ActionMenuItemView extends TextView implements MenuView.ItemView, View.OnClickListener, ActionMenuView.ActionMenuChildView {
  private static final int MAX_ICON_SIZE = 32;
  
  private static final String TAG = "ActionMenuItemView";
  
  private boolean mAllowTextWithIcon;
  
  private boolean mExpandedFormat;
  
  private ForwardingListener mForwardingListener;
  
  private Drawable mIcon;
  
  private MenuItemImpl mItemData;
  
  private MenuBuilder.ItemInvoker mItemInvoker;
  
  private int mMaxIconSize;
  
  private int mMinWidth;
  
  private PopupCallback mPopupCallback;
  
  private int mSavedPaddingLeft;
  
  private CharSequence mTitle;
  
  public ActionMenuItemView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public ActionMenuItemView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public ActionMenuItemView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public ActionMenuItemView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    Resources resources = paramContext.getResources();
    this.mAllowTextWithIcon = shouldAllowTextWithIcon();
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ActionMenuItemView, paramInt1, paramInt2);
    this.mMinWidth = typedArray.getDimensionPixelSize(0, 0);
    typedArray.recycle();
    float f = (resources.getDisplayMetrics()).density;
    this.mMaxIconSize = (int)(32.0F * f + 0.5F);
    setOnClickListener(this);
    this.mSavedPaddingLeft = -1;
    setSaveEnabled(false);
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration) {
    super.onConfigurationChanged(paramConfiguration);
    this.mAllowTextWithIcon = shouldAllowTextWithIcon();
    updateTextButtonVisibility();
  }
  
  private boolean shouldAllowTextWithIcon() {
    Configuration configuration = getContext().getResources().getConfiguration();
    int i = configuration.screenWidthDp;
    int j = configuration.screenHeightDp;
    return (i >= 480 || (i >= 640 && j >= 480) || configuration.orientation == 2);
  }
  
  public void setPadding(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mSavedPaddingLeft = paramInt1;
    super.setPadding(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public MenuItemImpl getItemData() {
    return this.mItemData;
  }
  
  public void initialize(MenuItemImpl paramMenuItemImpl, int paramInt) {
    this.mItemData = paramMenuItemImpl;
    setIcon(paramMenuItemImpl.getIcon());
    setTitle(paramMenuItemImpl.getTitleForItemView(this));
    setId(paramMenuItemImpl.getItemId());
    if (paramMenuItemImpl.isVisible()) {
      paramInt = 0;
    } else {
      paramInt = 8;
    } 
    setVisibility(paramInt);
    setEnabled(paramMenuItemImpl.isEnabled());
    if (paramMenuItemImpl.hasSubMenu() && 
      this.mForwardingListener == null)
      this.mForwardingListener = new ActionMenuItemForwardingListener(); 
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    if (this.mItemData.hasSubMenu()) {
      ForwardingListener forwardingListener = this.mForwardingListener;
      if (forwardingListener != null && 
        forwardingListener.onTouch((View)this, paramMotionEvent))
        return true; 
    } 
    return super.onTouchEvent(paramMotionEvent);
  }
  
  public void onClick(View paramView) {
    MenuBuilder.ItemInvoker itemInvoker = this.mItemInvoker;
    if (itemInvoker != null)
      itemInvoker.invokeItem(this.mItemData); 
  }
  
  public void setItemInvoker(MenuBuilder.ItemInvoker paramItemInvoker) {
    this.mItemInvoker = paramItemInvoker;
  }
  
  public void setPopupCallback(PopupCallback paramPopupCallback) {
    this.mPopupCallback = paramPopupCallback;
  }
  
  public boolean prefersCondensedTitle() {
    return true;
  }
  
  public void setCheckable(boolean paramBoolean) {}
  
  public void setChecked(boolean paramBoolean) {}
  
  public void setExpandedFormat(boolean paramBoolean) {
    if (this.mExpandedFormat != paramBoolean) {
      this.mExpandedFormat = paramBoolean;
      MenuItemImpl menuItemImpl = this.mItemData;
      if (menuItemImpl != null)
        menuItemImpl.actionFormatChanged(); 
    } 
  }
  
  private void updateTextButtonVisibility() {
    boolean bool = TextUtils.isEmpty(this.mTitle);
    int i = 1;
    if (this.mIcon != null) {
      charSequence1 = (CharSequence)this.mItemData;
      if (!charSequence1.showsTextAsAction() || (!this.mAllowTextWithIcon && !this.mExpandedFormat))
        i = 0; 
    } 
    i = (bool ^ true) & i;
    CharSequence charSequence2 = null;
    if (i != 0) {
      charSequence1 = this.mTitle;
    } else {
      charSequence1 = null;
    } 
    setText(charSequence1);
    CharSequence charSequence1 = this.mItemData.getContentDescription();
    if (TextUtils.isEmpty(charSequence1)) {
      if (i != 0) {
        charSequence1 = null;
      } else {
        charSequence1 = this.mItemData.getTitle();
      } 
      setContentDescription(charSequence1);
    } else {
      setContentDescription(charSequence1);
    } 
    charSequence1 = this.mItemData.getTooltipText();
    if (TextUtils.isEmpty(charSequence1)) {
      if (i != 0) {
        charSequence1 = charSequence2;
      } else {
        charSequence1 = this.mItemData.getTitle();
      } 
      setTooltipText(charSequence1);
    } else {
      setTooltipText(charSequence1);
    } 
  }
  
  public void setIcon(Drawable paramDrawable) {
    this.mIcon = paramDrawable;
    if (paramDrawable != null) {
      int i = paramDrawable.getIntrinsicWidth();
      int j = paramDrawable.getIntrinsicHeight();
      int k = this.mMaxIconSize, m = i, n = j;
      if (i > k) {
        float f = k / i;
        m = this.mMaxIconSize;
        n = (int)(j * f);
      } 
      k = this.mMaxIconSize;
      i = m;
      j = n;
      if (n > k) {
        float f = k / n;
        j = this.mMaxIconSize;
        i = (int)(m * f);
      } 
      paramDrawable.setBounds(0, 0, i, j);
    } 
    setCompoundDrawables(paramDrawable, null, null, null);
    updateTextButtonVisibility();
  }
  
  public boolean hasText() {
    return TextUtils.isEmpty(getText()) ^ true;
  }
  
  public void setShortcut(boolean paramBoolean, char paramChar) {}
  
  public void setTitle(CharSequence paramCharSequence) {
    this.mTitle = paramCharSequence;
    updateTextButtonVisibility();
  }
  
  public boolean dispatchPopulateAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    onPopulateAccessibilityEvent(paramAccessibilityEvent);
    return true;
  }
  
  public void onPopulateAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    super.onPopulateAccessibilityEventInternal(paramAccessibilityEvent);
    CharSequence charSequence = getContentDescription();
    if (!TextUtils.isEmpty(charSequence))
      paramAccessibilityEvent.getText().add(charSequence); 
  }
  
  public boolean dispatchHoverEvent(MotionEvent paramMotionEvent) {
    return onHoverEvent(paramMotionEvent);
  }
  
  public boolean showsIcon() {
    return true;
  }
  
  public boolean needsDividerBefore() {
    boolean bool;
    if (hasText() && this.mItemData.getIcon() == null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean needsDividerAfter() {
    return hasText();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    boolean bool = hasText();
    if (bool) {
      int k = this.mSavedPaddingLeft;
      if (k >= 0) {
        int m = getPaddingTop();
        int n = getPaddingRight(), i1 = getPaddingBottom();
        super.setPadding(k, m, n, i1);
      } 
    } 
    super.onMeasure(paramInt1, paramInt2);
    int j = View.MeasureSpec.getMode(paramInt1);
    paramInt1 = View.MeasureSpec.getSize(paramInt1);
    int i = getMeasuredWidth();
    if (j == Integer.MIN_VALUE) {
      paramInt1 = Math.min(paramInt1, this.mMinWidth);
    } else {
      paramInt1 = this.mMinWidth;
    } 
    if (j != 1073741824 && this.mMinWidth > 0 && i < paramInt1)
      super.onMeasure(View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824), paramInt2); 
    if (!bool && this.mIcon != null) {
      paramInt1 = getMeasuredWidth();
      paramInt2 = this.mIcon.getBounds().width();
      super.setPadding((paramInt1 - paramInt2) / 2, getPaddingTop(), getPaddingRight(), getPaddingBottom());
    } 
  }
  
  class ActionMenuItemForwardingListener extends ForwardingListener {
    final ActionMenuItemView this$0;
    
    public ActionMenuItemForwardingListener() {
      super((View)ActionMenuItemView.this);
    }
    
    public ShowableListMenu getPopup() {
      if (ActionMenuItemView.this.mPopupCallback != null)
        return ActionMenuItemView.this.mPopupCallback.getPopup(); 
      return null;
    }
    
    protected boolean onForwardingStarted() {
      MenuBuilder.ItemInvoker itemInvoker = ActionMenuItemView.this.mItemInvoker;
      boolean bool = false;
      if (itemInvoker != null && ActionMenuItemView.this.mItemInvoker.invokeItem(ActionMenuItemView.this.mItemData)) {
        ShowableListMenu showableListMenu = getPopup();
        boolean bool1 = bool;
        if (showableListMenu != null) {
          bool1 = bool;
          if (showableListMenu.isShowing())
            bool1 = true; 
        } 
        return bool1;
      } 
      return false;
    }
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    super.onRestoreInstanceState(null);
  }
  
  class PopupCallback {
    public abstract ShowableListMenu getPopup();
  }
}
