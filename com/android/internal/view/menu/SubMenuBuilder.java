package com.android.internal.view.menu;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

public class SubMenuBuilder extends MenuBuilder implements SubMenu {
  private MenuItemImpl mItem;
  
  private MenuBuilder mParentMenu;
  
  public SubMenuBuilder(Context paramContext, MenuBuilder paramMenuBuilder, MenuItemImpl paramMenuItemImpl) {
    super(paramContext);
    this.mParentMenu = paramMenuBuilder;
    this.mItem = paramMenuItemImpl;
  }
  
  public void setQwertyMode(boolean paramBoolean) {
    this.mParentMenu.setQwertyMode(paramBoolean);
  }
  
  public boolean isQwertyMode() {
    return this.mParentMenu.isQwertyMode();
  }
  
  public void setShortcutsVisible(boolean paramBoolean) {
    this.mParentMenu.setShortcutsVisible(paramBoolean);
  }
  
  public boolean isShortcutsVisible() {
    return this.mParentMenu.isShortcutsVisible();
  }
  
  public Menu getParentMenu() {
    return this.mParentMenu;
  }
  
  public MenuItem getItem() {
    return this.mItem;
  }
  
  public void setCallback(MenuBuilder.Callback paramCallback) {
    this.mParentMenu.setCallback(paramCallback);
  }
  
  public MenuBuilder getRootMenu() {
    return this.mParentMenu.getRootMenu();
  }
  
  boolean dispatchMenuItemSelected(MenuBuilder paramMenuBuilder, MenuItem paramMenuItem) {
    if (!super.dispatchMenuItemSelected(paramMenuBuilder, paramMenuItem)) {
      MenuBuilder menuBuilder = this.mParentMenu;
      return 
        menuBuilder.dispatchMenuItemSelected(paramMenuBuilder, paramMenuItem);
    } 
    return true;
  }
  
  public SubMenu setIcon(Drawable paramDrawable) {
    this.mItem.setIcon(paramDrawable);
    return this;
  }
  
  public SubMenu setIcon(int paramInt) {
    this.mItem.setIcon(paramInt);
    return this;
  }
  
  public SubMenu setHeaderIcon(Drawable paramDrawable) {
    return (SubMenu)setHeaderIconInt(paramDrawable);
  }
  
  public SubMenu setHeaderIcon(int paramInt) {
    return (SubMenu)setHeaderIconInt(paramInt);
  }
  
  public SubMenu setHeaderTitle(CharSequence paramCharSequence) {
    return (SubMenu)setHeaderTitleInt(paramCharSequence);
  }
  
  public SubMenu setHeaderTitle(int paramInt) {
    return (SubMenu)setHeaderTitleInt(paramInt);
  }
  
  public SubMenu setHeaderView(View paramView) {
    return (SubMenu)setHeaderViewInt(paramView);
  }
  
  public boolean expandItemActionView(MenuItemImpl paramMenuItemImpl) {
    return this.mParentMenu.expandItemActionView(paramMenuItemImpl);
  }
  
  public boolean collapseItemActionView(MenuItemImpl paramMenuItemImpl) {
    return this.mParentMenu.collapseItemActionView(paramMenuItemImpl);
  }
  
  public String getActionViewStatesKey() {
    boolean bool;
    MenuItemImpl menuItemImpl = this.mItem;
    if (menuItemImpl != null) {
      bool = menuItemImpl.getItemId();
    } else {
      bool = false;
    } 
    if (!bool)
      return null; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(super.getActionViewStatesKey());
    stringBuilder.append(":");
    stringBuilder.append(bool);
    return stringBuilder.toString();
  }
  
  public void setGroupDividerEnabled(boolean paramBoolean) {
    this.mParentMenu.setGroupDividerEnabled(paramBoolean);
  }
  
  public boolean isGroupDividerEnabled() {
    return this.mParentMenu.isGroupDividerEnabled();
  }
}
