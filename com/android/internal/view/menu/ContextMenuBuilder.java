package com.android.internal.view.menu;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.IBinder;
import android.util.EventLog;
import android.view.ContextMenu;
import android.view.View;

public class ContextMenuBuilder extends MenuBuilder implements ContextMenu {
  public ContextMenuBuilder(Context paramContext) {
    super(paramContext);
  }
  
  public ContextMenu setHeaderIcon(Drawable paramDrawable) {
    return (ContextMenu)setHeaderIconInt(paramDrawable);
  }
  
  public ContextMenu setHeaderIcon(int paramInt) {
    return (ContextMenu)setHeaderIconInt(paramInt);
  }
  
  public ContextMenu setHeaderTitle(CharSequence paramCharSequence) {
    return (ContextMenu)setHeaderTitleInt(paramCharSequence);
  }
  
  public ContextMenu setHeaderTitle(int paramInt) {
    return (ContextMenu)setHeaderTitleInt(paramInt);
  }
  
  public ContextMenu setHeaderView(View paramView) {
    return (ContextMenu)setHeaderViewInt(paramView);
  }
  
  public MenuDialogHelper showDialog(View paramView, IBinder paramIBinder) {
    if (paramView != null)
      paramView.createContextMenu(this); 
    if (getVisibleItems().size() > 0) {
      EventLog.writeEvent(50001, 1);
      MenuDialogHelper menuDialogHelper = new MenuDialogHelper(this);
      menuDialogHelper.show(paramIBinder);
      return menuDialogHelper;
    } 
    return null;
  }
  
  public MenuPopupHelper showPopup(Context paramContext, View paramView, float paramFloat1, float paramFloat2) {
    if (paramView != null)
      paramView.createContextMenu(this); 
    if (getVisibleItems().size() > 0) {
      EventLog.writeEvent(50001, 1);
      int[] arrayOfInt = new int[2];
      paramView.getLocationOnScreen(arrayOfInt);
      MenuPopupHelper menuPopupHelper = new MenuPopupHelper(paramContext, this, paramView, false, 16844033);
      menuPopupHelper.show(Math.round(paramFloat1), Math.round(paramFloat2));
      return menuPopupHelper;
    } 
    return null;
  }
}
