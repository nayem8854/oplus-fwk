package com.android.internal.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Parcelable;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.MenuItemHoverListener;
import android.widget.MenuPopupWindow;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

final class CascadingMenuPopup extends MenuPopup implements MenuPresenter, View.OnKeyListener, PopupWindow.OnDismissListener {
  private final List<MenuBuilder> mPendingMenus = new LinkedList<>();
  
  private final List<CascadingMenuInfo> mShowingMenus = new ArrayList<>();
  
  private final ViewTreeObserver.OnGlobalLayoutListener mGlobalLayoutListener = (ViewTreeObserver.OnGlobalLayoutListener)new Object(this);
  
  private final View.OnAttachStateChangeListener mAttachStateChangeListener = (View.OnAttachStateChangeListener)new Object(this);
  
  private final MenuItemHoverListener mMenuItemHoverListener = (MenuItemHoverListener)new Object(this);
  
  private int mRawDropDownGravity = 0;
  
  private int mDropDownGravity = 0;
  
  private static final int HORIZ_POSITION_LEFT = 0;
  
  private static final int HORIZ_POSITION_RIGHT = 1;
  
  private static final int ITEM_LAYOUT = 17367116;
  
  private static final int SUBMENU_TIMEOUT_MS = 200;
  
  private View mAnchorView;
  
  private final Context mContext;
  
  private boolean mForceShowIcon;
  
  private boolean mHasXOffset;
  
  private boolean mHasYOffset;
  
  private int mLastPosition;
  
  private final int mMenuMaxWidth;
  
  private PopupWindow.OnDismissListener mOnDismissListener;
  
  private final boolean mOverflowOnly;
  
  private final int mPopupStyleAttr;
  
  private final int mPopupStyleRes;
  
  private MenuPresenter.Callback mPresenterCallback;
  
  private boolean mShouldCloseImmediately;
  
  private boolean mShowTitle;
  
  private View mShownAnchorView;
  
  private final Handler mSubMenuHoverHandler;
  
  private ViewTreeObserver mTreeObserver;
  
  private int mXOffset;
  
  private int mYOffset;
  
  public CascadingMenuPopup(Context paramContext, View paramView, int paramInt1, int paramInt2, boolean paramBoolean) {
    this.mContext = Preconditions.<Context>checkNotNull(paramContext);
    this.mAnchorView = Preconditions.<View>checkNotNull(paramView);
    this.mPopupStyleAttr = paramInt1;
    this.mPopupStyleRes = paramInt2;
    this.mOverflowOnly = paramBoolean;
    this.mForceShowIcon = false;
    this.mLastPosition = getInitialMenuPosition();
    Resources resources = paramContext.getResources();
    paramInt1 = (resources.getDisplayMetrics()).widthPixels / 2;
    paramInt2 = resources.getDimensionPixelSize(17105073);
    this.mMenuMaxWidth = Math.max(paramInt1, paramInt2);
    this.mSubMenuHoverHandler = new Handler();
  }
  
  public void setForceShowIcon(boolean paramBoolean) {
    this.mForceShowIcon = paramBoolean;
  }
  
  private MenuPopupWindow createPopupWindow() {
    MenuPopupWindow menuPopupWindow = new MenuPopupWindow(this.mContext, null, this.mPopupStyleAttr, this.mPopupStyleRes);
    menuPopupWindow.setHoverListener(this.mMenuItemHoverListener);
    menuPopupWindow.setOnItemClickListener(this);
    menuPopupWindow.setOnDismissListener(this);
    menuPopupWindow.setAnchorView(this.mAnchorView);
    menuPopupWindow.setDropDownGravity(this.mDropDownGravity);
    menuPopupWindow.setModal(true);
    menuPopupWindow.setInputMethodMode(2);
    return menuPopupWindow;
  }
  
  public void show() {
    if (isShowing())
      return; 
    for (MenuBuilder menuBuilder : this.mPendingMenus)
      showMenu(menuBuilder); 
    this.mPendingMenus.clear();
    View view = this.mAnchorView;
    if (view != null) {
      boolean bool;
      if (this.mTreeObserver == null) {
        bool = true;
      } else {
        bool = false;
      } 
      ViewTreeObserver viewTreeObserver = this.mShownAnchorView.getViewTreeObserver();
      if (bool)
        viewTreeObserver.addOnGlobalLayoutListener(this.mGlobalLayoutListener); 
      this.mShownAnchorView.addOnAttachStateChangeListener(this.mAttachStateChangeListener);
    } 
  }
  
  public void dismiss() {
    int i = this.mShowingMenus.size();
    if (i > 0) {
      List<CascadingMenuInfo> list = this.mShowingMenus;
      CascadingMenuInfo[] arrayOfCascadingMenuInfo = new CascadingMenuInfo[i];
      arrayOfCascadingMenuInfo = list.<CascadingMenuInfo>toArray(arrayOfCascadingMenuInfo);
      for (; --i >= 0; i--) {
        CascadingMenuInfo cascadingMenuInfo = arrayOfCascadingMenuInfo[i];
        if (cascadingMenuInfo.window.isShowing())
          cascadingMenuInfo.window.dismiss(); 
      } 
    } 
  }
  
  public boolean onKey(View paramView, int paramInt, KeyEvent paramKeyEvent) {
    if (paramKeyEvent.getAction() == 1 && paramInt == 82) {
      dismiss();
      return true;
    } 
    return false;
  }
  
  private int getInitialMenuPosition() {
    int i = this.mAnchorView.getLayoutDirection();
    boolean bool = true;
    if (i == 1)
      bool = false; 
    return bool;
  }
  
  private int getNextMenuPosition(int paramInt) {
    List<CascadingMenuInfo> list = this.mShowingMenus;
    ListView listView = ((CascadingMenuInfo)list.get(list.size() - 1)).getListView();
    int[] arrayOfInt = new int[2];
    listView.getLocationOnScreen(arrayOfInt);
    Rect rect = new Rect();
    this.mShownAnchorView.getWindowVisibleDisplayFrame(rect);
    if (this.mLastPosition == 1) {
      int j = arrayOfInt[0], k = listView.getWidth();
      if (j + k + paramInt > rect.right)
        return 0; 
      return 1;
    } 
    int i = arrayOfInt[0];
    if (i - paramInt < 0)
      return 1; 
    return 0;
  }
  
  public void addMenu(MenuBuilder paramMenuBuilder) {
    paramMenuBuilder.addMenuPresenter(this, this.mContext);
    if (isShowing()) {
      showMenu(paramMenuBuilder);
    } else {
      this.mPendingMenus.add(paramMenuBuilder);
    } 
  }
  
  private void showMenu(MenuBuilder paramMenuBuilder) {
    View view;
    LayoutInflater layoutInflater = LayoutInflater.from(this.mContext);
    MenuAdapter menuAdapter = new MenuAdapter(paramMenuBuilder, layoutInflater, this.mOverflowOnly, 17367116);
    if (!isShowing() && this.mForceShowIcon) {
      menuAdapter.setForceShowIcon(true);
    } else if (isShowing()) {
      menuAdapter.setForceShowIcon(MenuPopup.shouldPreserveIconSpacing(paramMenuBuilder));
    } 
    int i = measureIndividualMenuWidth((ListAdapter)menuAdapter, null, this.mContext, this.mMenuMaxWidth);
    MenuPopupWindow menuPopupWindow = createPopupWindow();
    menuPopupWindow.setAdapter((ListAdapter)menuAdapter);
    menuPopupWindow.setContentWidth(i);
    menuPopupWindow.setDropDownGravity(this.mDropDownGravity);
    if (this.mShowingMenus.size() > 0) {
      List<CascadingMenuInfo> list = this.mShowingMenus;
      CascadingMenuInfo cascadingMenuInfo1 = list.get(list.size() - 1);
      view = findParentViewForSubmenu(cascadingMenuInfo1, paramMenuBuilder);
    } else {
      menuAdapter = null;
      view = null;
    } 
    if (view != null) {
      int k;
      menuPopupWindow.setAnchorView(view);
      menuPopupWindow.setTouchModal(false);
      menuPopupWindow.setEnterTransition(null);
      int j = getNextMenuPosition(i);
      if (j == 1) {
        k = 1;
      } else {
        k = 0;
      } 
      this.mLastPosition = j;
      if ((this.mDropDownGravity & 0x5) == 5) {
        if (k) {
          k = i;
        } else {
          k = -view.getWidth();
        } 
      } else if (k != 0) {
        k = view.getWidth();
      } else {
        k = -i;
      } 
      menuPopupWindow.setHorizontalOffset(k);
      menuPopupWindow.setOverlapAnchor(true);
      menuPopupWindow.setVerticalOffset(0);
    } else {
      if (this.mHasXOffset)
        menuPopupWindow.setHorizontalOffset(this.mXOffset); 
      if (this.mHasYOffset)
        menuPopupWindow.setVerticalOffset(this.mYOffset); 
      Rect rect = getEpicenterBounds();
      menuPopupWindow.setEpicenterBounds(rect);
    } 
    CascadingMenuInfo cascadingMenuInfo = new CascadingMenuInfo(menuPopupWindow, paramMenuBuilder, this.mLastPosition);
    this.mShowingMenus.add(cascadingMenuInfo);
    menuPopupWindow.show();
    ListView listView = menuPopupWindow.getListView();
    listView.setOnKeyListener(this);
    if (menuAdapter == null && this.mShowTitle && paramMenuBuilder.getHeaderTitle() != null) {
      FrameLayout frameLayout = (FrameLayout)layoutInflater.inflate(17367234, (ViewGroup)listView, false);
      TextView textView = (TextView)frameLayout.findViewById(16908310);
      frameLayout.setEnabled(false);
      textView.setText(paramMenuBuilder.getHeaderTitle());
      listView.addHeaderView((View)frameLayout, null, false);
      menuPopupWindow.show();
    } 
  }
  
  private MenuItem findMenuItemForSubmenu(MenuBuilder paramMenuBuilder1, MenuBuilder paramMenuBuilder2) {
    byte b;
    int i;
    for (b = 0, i = paramMenuBuilder1.size(); b < i; b++) {
      MenuItem menuItem = paramMenuBuilder1.getItem(b);
      if (menuItem.hasSubMenu() && paramMenuBuilder2 == menuItem.getSubMenu())
        return menuItem; 
    } 
    return null;
  }
  
  private View findParentViewForSubmenu(CascadingMenuInfo paramCascadingMenuInfo, MenuBuilder paramMenuBuilder) {
    MenuAdapter menuAdapter;
    byte b;
    int k;
    MenuItem menuItem = findMenuItemForSubmenu(paramCascadingMenuInfo.menu, paramMenuBuilder);
    if (menuItem == null)
      return null; 
    ListView listView = paramCascadingMenuInfo.getListView();
    ListAdapter listAdapter = listView.getAdapter();
    if (listAdapter instanceof HeaderViewListAdapter) {
      HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter)listAdapter;
      b = headerViewListAdapter.getHeadersCount();
      menuAdapter = (MenuAdapter)headerViewListAdapter.getWrappedAdapter();
    } else {
      b = 0;
      menuAdapter = menuAdapter;
    } 
    byte b1 = -1;
    int i = 0, j = menuAdapter.getCount();
    while (true) {
      k = b1;
      if (i < j) {
        if (menuItem == menuAdapter.getItem(i)) {
          k = i;
          break;
        } 
        i++;
        continue;
      } 
      break;
    } 
    if (k == -1)
      return null; 
    i = k + b - listView.getFirstVisiblePosition();
    if (i < 0 || i >= listView.getChildCount())
      return null; 
    return listView.getChildAt(i);
  }
  
  public boolean isShowing() {
    int i = this.mShowingMenus.size();
    boolean bool1 = false, bool2 = bool1;
    if (i > 0) {
      bool2 = bool1;
      if (((CascadingMenuInfo)this.mShowingMenus.get(0)).window.isShowing())
        bool2 = true; 
    } 
    return bool2;
  }
  
  public void onDismiss() {
    CascadingMenuInfo cascadingMenuInfo2, cascadingMenuInfo1 = null;
    byte b = 0;
    int i = this.mShowingMenus.size();
    while (true) {
      cascadingMenuInfo2 = cascadingMenuInfo1;
      if (b < i) {
        cascadingMenuInfo2 = this.mShowingMenus.get(b);
        if (!cascadingMenuInfo2.window.isShowing())
          break; 
        b++;
        continue;
      } 
      break;
    } 
    if (cascadingMenuInfo2 != null)
      cascadingMenuInfo2.menu.close(false); 
  }
  
  public void updateMenuView(boolean paramBoolean) {
    for (CascadingMenuInfo cascadingMenuInfo : this.mShowingMenus)
      toMenuAdapter(cascadingMenuInfo.getListView().getAdapter()).notifyDataSetChanged(); 
  }
  
  public void setCallback(MenuPresenter.Callback paramCallback) {
    this.mPresenterCallback = paramCallback;
  }
  
  public boolean onSubMenuSelected(SubMenuBuilder paramSubMenuBuilder) {
    for (CascadingMenuInfo cascadingMenuInfo : this.mShowingMenus) {
      if (paramSubMenuBuilder == cascadingMenuInfo.menu) {
        cascadingMenuInfo.getListView().requestFocus();
        return true;
      } 
    } 
    if (paramSubMenuBuilder.hasVisibleItems()) {
      addMenu(paramSubMenuBuilder);
      MenuPresenter.Callback callback = this.mPresenterCallback;
      if (callback != null)
        callback.onOpenSubMenu(paramSubMenuBuilder); 
      return true;
    } 
    return false;
  }
  
  private int findIndexOfAddedMenu(MenuBuilder paramMenuBuilder) {
    byte b;
    int i;
    for (b = 0, i = this.mShowingMenus.size(); b < i; b++) {
      CascadingMenuInfo cascadingMenuInfo = this.mShowingMenus.get(b);
      if (paramMenuBuilder == cascadingMenuInfo.menu)
        return b; 
    } 
    return -1;
  }
  
  public void onCloseMenu(MenuBuilder paramMenuBuilder, boolean paramBoolean) {
    int i = findIndexOfAddedMenu(paramMenuBuilder);
    if (i < 0)
      return; 
    int j = i + 1;
    if (j < this.mShowingMenus.size()) {
      CascadingMenuInfo cascadingMenuInfo1 = this.mShowingMenus.get(j);
      cascadingMenuInfo1.menu.close(false);
    } 
    CascadingMenuInfo cascadingMenuInfo = this.mShowingMenus.remove(i);
    cascadingMenuInfo.menu.removeMenuPresenter(this);
    if (this.mShouldCloseImmediately) {
      cascadingMenuInfo.window.setExitTransition(null);
      cascadingMenuInfo.window.setAnimationStyle(0);
    } 
    cascadingMenuInfo.window.dismiss();
    i = this.mShowingMenus.size();
    if (i > 0) {
      this.mLastPosition = ((CascadingMenuInfo)this.mShowingMenus.get(i - 1)).position;
    } else {
      this.mLastPosition = getInitialMenuPosition();
    } 
    if (i == 0) {
      dismiss();
      MenuPresenter.Callback callback = this.mPresenterCallback;
      if (callback != null)
        callback.onCloseMenu(paramMenuBuilder, true); 
      ViewTreeObserver viewTreeObserver = this.mTreeObserver;
      if (viewTreeObserver != null) {
        if (viewTreeObserver.isAlive())
          this.mTreeObserver.removeGlobalOnLayoutListener(this.mGlobalLayoutListener); 
        this.mTreeObserver = null;
      } 
      this.mShownAnchorView.removeOnAttachStateChangeListener(this.mAttachStateChangeListener);
      this.mOnDismissListener.onDismiss();
    } else if (paramBoolean) {
      CascadingMenuInfo cascadingMenuInfo1 = this.mShowingMenus.get(0);
      cascadingMenuInfo1.menu.close(false);
    } 
  }
  
  public boolean flagActionItems() {
    return false;
  }
  
  public Parcelable onSaveInstanceState() {
    return null;
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {}
  
  public void setGravity(int paramInt) {
    if (this.mRawDropDownGravity != paramInt) {
      this.mRawDropDownGravity = paramInt;
      View view = this.mAnchorView;
      int i = view.getLayoutDirection();
      this.mDropDownGravity = Gravity.getAbsoluteGravity(paramInt, i);
    } 
  }
  
  public void setAnchorView(View paramView) {
    if (this.mAnchorView != paramView) {
      this.mAnchorView = paramView;
      int i = this.mRawDropDownGravity;
      int j = paramView.getLayoutDirection();
      this.mDropDownGravity = Gravity.getAbsoluteGravity(i, j);
    } 
  }
  
  public void setOnDismissListener(PopupWindow.OnDismissListener paramOnDismissListener) {
    this.mOnDismissListener = paramOnDismissListener;
  }
  
  public ListView getListView() {
    ListView listView;
    if (this.mShowingMenus.isEmpty()) {
      listView = null;
    } else {
      List<CascadingMenuInfo> list = this.mShowingMenus;
      listView = ((CascadingMenuInfo)list.get(list.size() - 1)).getListView();
    } 
    return listView;
  }
  
  public void setHorizontalOffset(int paramInt) {
    this.mHasXOffset = true;
    this.mXOffset = paramInt;
  }
  
  public void setVerticalOffset(int paramInt) {
    this.mHasYOffset = true;
    this.mYOffset = paramInt;
  }
  
  public void setShowTitle(boolean paramBoolean) {
    this.mShowTitle = paramBoolean;
  }
  
  class CascadingMenuInfo {
    public final MenuBuilder menu;
    
    public final int position;
    
    public final MenuPopupWindow window;
    
    public CascadingMenuInfo(CascadingMenuPopup this$0, MenuBuilder param1MenuBuilder, int param1Int) {
      this.window = (MenuPopupWindow)this$0;
      this.menu = param1MenuBuilder;
      this.position = param1Int;
    }
    
    public ListView getListView() {
      return this.window.getListView();
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class HorizPosition implements Annotation {}
}
