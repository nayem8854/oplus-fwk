package com.android.internal.view.menu;

public interface MenuHelper {
  void dismiss();
  
  void setPresenterCallback(MenuPresenter.Callback paramCallback);
}
