package com.android.internal.view.menu;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import java.util.ArrayList;
import java.util.List;

public class ActionMenu implements Menu {
  private Context mContext;
  
  private boolean mIsQwerty;
  
  private ArrayList<ActionMenuItem> mItems;
  
  public ActionMenu(Context paramContext) {
    this.mContext = paramContext;
    this.mItems = new ArrayList<>();
  }
  
  public Context getContext() {
    return this.mContext;
  }
  
  public MenuItem add(CharSequence paramCharSequence) {
    return add(0, 0, 0, paramCharSequence);
  }
  
  public MenuItem add(int paramInt) {
    return add(0, 0, 0, paramInt);
  }
  
  public MenuItem add(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    return add(paramInt1, paramInt2, paramInt3, this.mContext.getResources().getString(paramInt4));
  }
  
  public MenuItem add(int paramInt1, int paramInt2, int paramInt3, CharSequence paramCharSequence) {
    ActionMenuItem actionMenuItem = new ActionMenuItem(getContext(), paramInt1, paramInt2, 0, paramInt3, paramCharSequence);
    this.mItems.add(paramInt3, actionMenuItem);
    return actionMenuItem;
  }
  
  public int addIntentOptions(int paramInt1, int paramInt2, int paramInt3, ComponentName paramComponentName, Intent[] paramArrayOfIntent, Intent paramIntent, int paramInt4, MenuItem[] paramArrayOfMenuItem) {
    PackageManager packageManager = this.mContext.getPackageManager();
    int i = 0;
    List<ResolveInfo> list = packageManager.queryIntentActivityOptions(paramComponentName, paramArrayOfIntent, paramIntent, 0);
    if (list != null)
      i = list.size(); 
    if ((paramInt4 & 0x1) == 0)
      removeGroup(paramInt1); 
    for (paramInt4 = 0; paramInt4 < i; paramInt4++) {
      ResolveInfo resolveInfo = list.get(paramInt4);
      if (resolveInfo.specificIndex < 0) {
        intent = paramIntent;
      } else {
        intent = paramArrayOfIntent[resolveInfo.specificIndex];
      } 
      Intent intent = new Intent(intent);
      intent.setComponent(new ComponentName(resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name));
      MenuItem menuItem2 = add(paramInt1, paramInt2, paramInt3, resolveInfo.loadLabel(packageManager));
      menuItem2 = menuItem2.setIcon(resolveInfo.loadIcon(packageManager));
      MenuItem menuItem1 = menuItem2.setIntent(intent);
      if (paramArrayOfMenuItem != null && resolveInfo.specificIndex >= 0)
        paramArrayOfMenuItem[resolveInfo.specificIndex] = menuItem1; 
    } 
    return i;
  }
  
  public SubMenu addSubMenu(CharSequence paramCharSequence) {
    return null;
  }
  
  public SubMenu addSubMenu(int paramInt) {
    return null;
  }
  
  public SubMenu addSubMenu(int paramInt1, int paramInt2, int paramInt3, CharSequence paramCharSequence) {
    return null;
  }
  
  public SubMenu addSubMenu(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    return null;
  }
  
  public void clear() {
    this.mItems.clear();
  }
  
  public void close() {}
  
  private int findItemIndex(int paramInt) {
    ArrayList<ActionMenuItem> arrayList = this.mItems;
    int i = arrayList.size();
    for (byte b = 0; b < i; b++) {
      if (((ActionMenuItem)arrayList.get(b)).getItemId() == paramInt)
        return b; 
    } 
    return -1;
  }
  
  public MenuItem findItem(int paramInt) {
    return this.mItems.get(findItemIndex(paramInt));
  }
  
  public MenuItem getItem(int paramInt) {
    return this.mItems.get(paramInt);
  }
  
  public boolean hasVisibleItems() {
    ArrayList<ActionMenuItem> arrayList = this.mItems;
    int i = arrayList.size();
    for (byte b = 0; b < i; b++) {
      if (((ActionMenuItem)arrayList.get(b)).isVisible())
        return true; 
    } 
    return false;
  }
  
  private ActionMenuItem findItemWithShortcut(int paramInt, KeyEvent paramKeyEvent) {
    boolean bool = this.mIsQwerty;
    ArrayList<ActionMenuItem> arrayList = this.mItems;
    int i = arrayList.size();
    int j = paramKeyEvent.getModifiers();
    for (byte b = 0; b < i; b++) {
      char c;
      int k;
      ActionMenuItem actionMenuItem = arrayList.get(b);
      if (bool) {
        c = actionMenuItem.getAlphabeticShortcut();
      } else {
        c = actionMenuItem.getNumericShortcut();
      } 
      if (bool) {
        k = actionMenuItem.getAlphabeticModifiers();
      } else {
        k = actionMenuItem.getNumericModifiers();
      } 
      if ((j & 0x1100F) == (0x1100F & k)) {
        k = 1;
      } else {
        k = 0;
      } 
      if (paramInt == c && k != 0)
        return actionMenuItem; 
    } 
    return null;
  }
  
  public boolean isShortcutKey(int paramInt, KeyEvent paramKeyEvent) {
    boolean bool;
    if (findItemWithShortcut(paramInt, paramKeyEvent) != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean performIdentifierAction(int paramInt1, int paramInt2) {
    paramInt1 = findItemIndex(paramInt1);
    if (paramInt1 < 0)
      return false; 
    return ((ActionMenuItem)this.mItems.get(paramInt1)).invoke();
  }
  
  public boolean performShortcut(int paramInt1, KeyEvent paramKeyEvent, int paramInt2) {
    ActionMenuItem actionMenuItem = findItemWithShortcut(paramInt1, paramKeyEvent);
    if (actionMenuItem == null)
      return false; 
    return actionMenuItem.invoke();
  }
  
  public void removeGroup(int paramInt) {
    ArrayList<ActionMenuItem> arrayList = this.mItems;
    int i = arrayList.size();
    byte b = 0;
    while (b < i) {
      if (((ActionMenuItem)arrayList.get(b)).getGroupId() == paramInt) {
        arrayList.remove(b);
        i--;
        continue;
      } 
      b++;
    } 
  }
  
  public void removeItem(int paramInt) {
    this.mItems.remove(findItemIndex(paramInt));
  }
  
  public void setGroupCheckable(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    ArrayList<ActionMenuItem> arrayList = this.mItems;
    int i = arrayList.size();
    for (byte b = 0; b < i; b++) {
      ActionMenuItem actionMenuItem = arrayList.get(b);
      if (actionMenuItem.getGroupId() == paramInt) {
        actionMenuItem.setCheckable(paramBoolean1);
        actionMenuItem.setExclusiveCheckable(paramBoolean2);
      } 
    } 
  }
  
  public void setGroupEnabled(int paramInt, boolean paramBoolean) {
    ArrayList<ActionMenuItem> arrayList = this.mItems;
    int i = arrayList.size();
    for (byte b = 0; b < i; b++) {
      ActionMenuItem actionMenuItem = arrayList.get(b);
      if (actionMenuItem.getGroupId() == paramInt)
        actionMenuItem.setEnabled(paramBoolean); 
    } 
  }
  
  public void setGroupVisible(int paramInt, boolean paramBoolean) {
    ArrayList<ActionMenuItem> arrayList = this.mItems;
    int i = arrayList.size();
    for (byte b = 0; b < i; b++) {
      ActionMenuItem actionMenuItem = arrayList.get(b);
      if (actionMenuItem.getGroupId() == paramInt)
        actionMenuItem.setVisible(paramBoolean); 
    } 
  }
  
  public void setQwertyMode(boolean paramBoolean) {
    this.mIsQwerty = paramBoolean;
  }
  
  public int size() {
    return this.mItems.size();
  }
}
