package com.android.internal.view.menu;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public class IconMenuPresenter extends BaseMenuPresenter {
  private static final String OPEN_SUBMENU_KEY = "android:menu:icon:submenu";
  
  private static final String VIEWS_TAG = "android:menu:icon";
  
  private int mMaxItems = -1;
  
  private IconMenuItemView mMoreView;
  
  MenuDialogHelper mOpenSubMenu;
  
  int mOpenSubMenuId;
  
  SubMenuPresenterCallback mSubMenuPresenterCallback = new SubMenuPresenterCallback();
  
  public IconMenuPresenter(Context paramContext) {
    super((Context)new ContextThemeWrapper(paramContext, 16974878), 17367173, 17367172);
  }
  
  public void initForMenu(Context paramContext, MenuBuilder paramMenuBuilder) {
    super.initForMenu(paramContext, paramMenuBuilder);
    this.mMaxItems = -1;
  }
  
  public void bindItemView(MenuItemImpl paramMenuItemImpl, MenuView.ItemView paramItemView) {
    byte b;
    paramItemView = paramItemView;
    paramItemView.setItemData(paramMenuItemImpl);
    paramItemView.initialize(paramMenuItemImpl.getTitleForItemView(paramItemView), paramMenuItemImpl.getIcon());
    if (paramMenuItemImpl.isVisible()) {
      b = 0;
    } else {
      b = 8;
    } 
    paramItemView.setVisibility(b);
    paramItemView.setEnabled(paramItemView.isEnabled());
    paramItemView.setLayoutParams((ViewGroup.LayoutParams)paramItemView.getTextAppropriateLayoutParams());
  }
  
  public boolean shouldIncludeItem(int paramInt, MenuItemImpl paramMenuItemImpl) {
    ArrayList<MenuItemImpl> arrayList = this.mMenu.getNonActionItems();
    int i = arrayList.size(), j = this.mMaxItems;
    boolean bool1 = false;
    if ((i == j && paramInt < j) || paramInt < this.mMaxItems - 1) {
      paramInt = 1;
    } else {
      paramInt = 0;
    } 
    boolean bool2 = bool1;
    if (paramInt != 0) {
      bool2 = bool1;
      if (!paramMenuItemImpl.isActionButton())
        bool2 = true; 
    } 
    return bool2;
  }
  
  protected void addItemView(View paramView, int paramInt) {
    IconMenuItemView iconMenuItemView = (IconMenuItemView)paramView;
    IconMenuView iconMenuView = (IconMenuView)this.mMenuView;
    iconMenuItemView.setIconMenuView(iconMenuView);
    iconMenuItemView.setItemInvoker(iconMenuView);
    iconMenuItemView.setBackgroundDrawable(iconMenuView.getItemBackgroundDrawable());
    super.addItemView(paramView, paramInt);
  }
  
  public boolean onSubMenuSelected(SubMenuBuilder paramSubMenuBuilder) {
    if (!paramSubMenuBuilder.hasVisibleItems())
      return false; 
    MenuDialogHelper menuDialogHelper = new MenuDialogHelper(paramSubMenuBuilder);
    menuDialogHelper.setPresenterCallback(this.mSubMenuPresenterCallback);
    menuDialogHelper.show(null);
    this.mOpenSubMenu = menuDialogHelper;
    this.mOpenSubMenuId = paramSubMenuBuilder.getItem().getItemId();
    super.onSubMenuSelected(paramSubMenuBuilder);
    return true;
  }
  
  public void updateMenuView(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mMenuView : Lcom/android/internal/view/menu/MenuView;
    //   4: checkcast com/android/internal/view/menu/IconMenuView
    //   7: astore_2
    //   8: aload_0
    //   9: getfield mMaxItems : I
    //   12: ifge -> 23
    //   15: aload_0
    //   16: aload_2
    //   17: invokevirtual getMaxItems : ()I
    //   20: putfield mMaxItems : I
    //   23: aload_0
    //   24: getfield mMenu : Lcom/android/internal/view/menu/MenuBuilder;
    //   27: invokevirtual getNonActionItems : ()Ljava/util/ArrayList;
    //   30: astore_3
    //   31: aload_3
    //   32: invokevirtual size : ()I
    //   35: aload_0
    //   36: getfield mMaxItems : I
    //   39: if_icmple -> 48
    //   42: iconst_1
    //   43: istore #4
    //   45: goto -> 51
    //   48: iconst_0
    //   49: istore #4
    //   51: aload_0
    //   52: iload_1
    //   53: invokespecial updateMenuView : (Z)V
    //   56: iload #4
    //   58: ifeq -> 120
    //   61: aload_0
    //   62: getfield mMoreView : Lcom/android/internal/view/menu/IconMenuItemView;
    //   65: astore #5
    //   67: aload #5
    //   69: ifnull -> 81
    //   72: aload #5
    //   74: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   77: aload_2
    //   78: if_acmpeq -> 120
    //   81: aload_0
    //   82: getfield mMoreView : Lcom/android/internal/view/menu/IconMenuItemView;
    //   85: ifnonnull -> 109
    //   88: aload_2
    //   89: invokevirtual createMoreItemView : ()Lcom/android/internal/view/menu/IconMenuItemView;
    //   92: astore #5
    //   94: aload_0
    //   95: aload #5
    //   97: putfield mMoreView : Lcom/android/internal/view/menu/IconMenuItemView;
    //   100: aload #5
    //   102: aload_2
    //   103: invokevirtual getItemBackgroundDrawable : ()Landroid/graphics/drawable/Drawable;
    //   106: invokevirtual setBackgroundDrawable : (Landroid/graphics/drawable/Drawable;)V
    //   109: aload_2
    //   110: aload_0
    //   111: getfield mMoreView : Lcom/android/internal/view/menu/IconMenuItemView;
    //   114: invokevirtual addView : (Landroid/view/View;)V
    //   117: goto -> 142
    //   120: iload #4
    //   122: ifne -> 142
    //   125: aload_0
    //   126: getfield mMoreView : Lcom/android/internal/view/menu/IconMenuItemView;
    //   129: astore #5
    //   131: aload #5
    //   133: ifnull -> 142
    //   136: aload_2
    //   137: aload #5
    //   139: invokevirtual removeView : (Landroid/view/View;)V
    //   142: iload #4
    //   144: ifeq -> 158
    //   147: aload_0
    //   148: getfield mMaxItems : I
    //   151: iconst_1
    //   152: isub
    //   153: istore #4
    //   155: goto -> 164
    //   158: aload_3
    //   159: invokevirtual size : ()I
    //   162: istore #4
    //   164: aload_2
    //   165: iload #4
    //   167: invokevirtual setNumActualItemsShown : (I)V
    //   170: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #106	-> 0
    //   #107	-> 8
    //   #108	-> 23
    //   #109	-> 31
    //   #110	-> 51
    //   #112	-> 56
    //   #113	-> 81
    //   #114	-> 88
    //   #115	-> 100
    //   #117	-> 109
    //   #118	-> 120
    //   #119	-> 136
    //   #122	-> 142
    //   #123	-> 170
  }
  
  protected boolean filterLeftoverView(ViewGroup paramViewGroup, int paramInt) {
    if (paramViewGroup.getChildAt(paramInt) != this.mMoreView)
      return super.filterLeftoverView(paramViewGroup, paramInt); 
    return false;
  }
  
  public int getNumActualItemsShown() {
    return ((IconMenuView)this.mMenuView).getNumActualItemsShown();
  }
  
  public void saveHierarchyState(Bundle paramBundle) {
    SparseArray sparseArray = new SparseArray();
    if (this.mMenuView != null)
      ((View)this.mMenuView).saveHierarchyState(sparseArray); 
    paramBundle.putSparseParcelableArray("android:menu:icon", sparseArray);
  }
  
  public void restoreHierarchyState(Bundle paramBundle) {
    SparseArray sparseArray = paramBundle.getSparseParcelableArray("android:menu:icon");
    if (sparseArray != null)
      ((View)this.mMenuView).restoreHierarchyState(sparseArray); 
    int i = paramBundle.getInt("android:menu:icon:submenu", 0);
    if (i > 0 && this.mMenu != null) {
      MenuItem menuItem = this.mMenu.findItem(i);
      if (menuItem != null)
        onSubMenuSelected((SubMenuBuilder)menuItem.getSubMenu()); 
    } 
  }
  
  public Parcelable onSaveInstanceState() {
    if (this.mMenuView == null)
      return null; 
    Bundle bundle = new Bundle();
    saveHierarchyState(bundle);
    int i = this.mOpenSubMenuId;
    if (i > 0)
      bundle.putInt("android:menu:icon:submenu", i); 
    return (Parcelable)bundle;
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    restoreHierarchyState((Bundle)paramParcelable);
  }
  
  class SubMenuPresenterCallback implements MenuPresenter.Callback {
    final IconMenuPresenter this$0;
    
    public void onCloseMenu(MenuBuilder param1MenuBuilder, boolean param1Boolean) {
      IconMenuPresenter.this.mOpenSubMenuId = 0;
      if (IconMenuPresenter.this.mOpenSubMenu != null) {
        IconMenuPresenter.this.mOpenSubMenu.dismiss();
        IconMenuPresenter.this.mOpenSubMenu = null;
      } 
    }
    
    public boolean onOpenSubMenu(MenuBuilder param1MenuBuilder) {
      if (param1MenuBuilder != null)
        IconMenuPresenter.this.mOpenSubMenuId = ((SubMenuBuilder)param1MenuBuilder).getItem().getItemId(); 
      return false;
    }
  }
}
