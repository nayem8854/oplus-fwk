package com.android.internal.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import com.android.internal.R;
import java.util.ArrayList;

public final class IconMenuView extends ViewGroup implements MenuBuilder.ItemInvoker, MenuView, Runnable {
  private static final int ITEM_CAPTION_CYCLE_DELAY = 1000;
  
  private int mAnimations;
  
  private boolean mHasStaleChildren;
  
  private Drawable mHorizontalDivider;
  
  private int mHorizontalDividerHeight;
  
  private ArrayList<Rect> mHorizontalDividerRects;
  
  private Drawable mItemBackground;
  
  private boolean mLastChildrenCaptionMode;
  
  private int[] mLayout;
  
  private int mLayoutNumRows;
  
  private int mMaxItems;
  
  private int mMaxItemsPerRow;
  
  private int mMaxRows;
  
  private MenuBuilder mMenu;
  
  private boolean mMenuBeingLongpressed = false;
  
  private Drawable mMoreIcon;
  
  private int mNumActualItemsShown;
  
  private int mRowHeight;
  
  private Drawable mVerticalDivider;
  
  private ArrayList<Rect> mVerticalDividerRects;
  
  private int mVerticalDividerWidth;
  
  public IconMenuView(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    int[] arrayOfInt = R.styleable.IconMenuView;
    TypedArray typedArray2 = paramContext.obtainStyledAttributes(paramAttributeSet, arrayOfInt, 0, 0);
    this.mRowHeight = typedArray2.getDimensionPixelSize(0, 64);
    this.mMaxRows = typedArray2.getInt(1, 2);
    this.mMaxItems = typedArray2.getInt(4, 6);
    this.mMaxItemsPerRow = typedArray2.getInt(2, 3);
    this.mMoreIcon = typedArray2.getDrawable(3);
    typedArray2.recycle();
    TypedArray typedArray1 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.MenuView, 0, 0);
    this.mItemBackground = typedArray1.getDrawable(5);
    this.mHorizontalDivider = typedArray1.getDrawable(2);
    this.mHorizontalDividerRects = new ArrayList<>();
    this.mVerticalDivider = typedArray1.getDrawable(3);
    this.mVerticalDividerRects = new ArrayList<>();
    this.mAnimations = typedArray1.getResourceId(0, 0);
    typedArray1.recycle();
    Drawable drawable = this.mHorizontalDivider;
    if (drawable != null) {
      int i = drawable.getIntrinsicHeight();
      if (i == -1)
        this.mHorizontalDividerHeight = 1; 
    } 
    drawable = this.mVerticalDivider;
    if (drawable != null) {
      int i = drawable.getIntrinsicWidth();
      if (i == -1)
        this.mVerticalDividerWidth = 1; 
    } 
    this.mLayout = new int[this.mMaxRows];
    setWillNotDraw(false);
    setFocusableInTouchMode(true);
    setDescendantFocusability(262144);
  }
  
  int getMaxItems() {
    return this.mMaxItems;
  }
  
  private void layoutItems(int paramInt) {
    int i = getChildCount();
    if (i == 0) {
      this.mLayoutNumRows = 0;
      return;
    } 
    double d = (i / this.mMaxItemsPerRow);
    paramInt = Math.min((int)Math.ceil(d), this.mMaxRows);
    for (; paramInt <= this.mMaxRows; paramInt++) {
      layoutItemsUsingGravity(paramInt, i);
      if (paramInt >= i)
        break; 
      if (doItemsFit())
        break; 
    } 
  }
  
  private void layoutItemsUsingGravity(int paramInt1, int paramInt2) {
    int i = paramInt2 / paramInt1;
    int[] arrayOfInt = this.mLayout;
    for (byte b = 0; b < paramInt1; b++) {
      arrayOfInt[b] = i;
      if (b >= paramInt1 - paramInt2 % paramInt1)
        arrayOfInt[b] = arrayOfInt[b] + 1; 
    } 
    this.mLayoutNumRows = paramInt1;
  }
  
  private boolean doItemsFit() {
    byte b1 = 0;
    int[] arrayOfInt = this.mLayout;
    int i = this.mLayoutNumRows;
    for (byte b2 = 0; b2 < i; b2++) {
      int j = arrayOfInt[b2];
      if (j == 1) {
        b1++;
      } else {
        int k = j;
        byte b = b1;
        while (true) {
          b1 = b;
          if (k > 0) {
            View view = getChildAt(b);
            LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
            if (layoutParams.maxNumItemsOnRow < j)
              return false; 
            k--;
            b++;
            continue;
          } 
          break;
        } 
      } 
    } 
    return true;
  }
  
  Drawable getItemBackgroundDrawable() {
    return this.mItemBackground.getConstantState().newDrawable(getContext().getResources());
  }
  
  IconMenuItemView createMoreItemView() {
    Context context = getContext();
    LayoutInflater layoutInflater = LayoutInflater.from(context);
    IconMenuItemView iconMenuItemView = (IconMenuItemView)layoutInflater.inflate(17367172, null);
    Resources resources = context.getResources();
    iconMenuItemView.initialize(resources.getText(17040674), this.mMoreIcon);
    iconMenuItemView.setOnClickListener((View.OnClickListener)new Object(this));
    return iconMenuItemView;
  }
  
  public void initialize(MenuBuilder paramMenuBuilder) {
    this.mMenu = paramMenuBuilder;
  }
  
  private void positionChildren(int paramInt1, int paramInt2) {
    if (this.mHorizontalDivider != null)
      this.mHorizontalDividerRects.clear(); 
    if (this.mVerticalDivider != null)
      this.mVerticalDividerRects.clear(); 
    int i = this.mLayoutNumRows;
    int[] arrayOfInt = this.mLayout;
    byte b1 = 0;
    View view = null;
    float f1 = 0.0F;
    float f2 = (paramInt2 - this.mHorizontalDividerHeight * (i - 1)) / i;
    for (byte b2 = 0; b2 < paramInt2; b2++) {
      LayoutParams layoutParams;
      float f3 = 0.0F;
      float f4 = (paramInt1 - this.mVerticalDividerWidth * (arrayOfInt[b2] - 1)) / arrayOfInt[b2];
      for (byte b = 0; b < arrayOfInt[b2]; b++) {
        view = getChildAt(b1);
        int j = View.MeasureSpec.makeMeasureSpec((int)f4, 1073741824), k = (int)f2;
        k = View.MeasureSpec.makeMeasureSpec(k, 1073741824);
        view.measure(j, k);
        layoutParams = (LayoutParams)view.getLayoutParams();
        layoutParams.left = (int)f3;
        layoutParams.right = (int)(f3 + f4);
        layoutParams.top = (int)f1;
        layoutParams.bottom = (int)(f1 + f2);
        f3 += f4;
        b1++;
        if (this.mVerticalDivider != null)
          this.mVerticalDividerRects.add(new Rect((int)f3, (int)f1, (int)(this.mVerticalDividerWidth + f3), (int)(f1 + f2))); 
        f3 += this.mVerticalDividerWidth;
      } 
      if (layoutParams != null)
        layoutParams.right = paramInt1; 
      f3 = f1 + f2;
      f1 = f3;
      if (this.mHorizontalDivider != null) {
        f1 = f3;
        if (b2 < i - 1) {
          this.mHorizontalDividerRects.add(new Rect(0, (int)f3, paramInt1, (int)(this.mHorizontalDividerHeight + f3)));
          f1 = f3 + this.mHorizontalDividerHeight;
        } 
      } 
    } 
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    paramInt1 = resolveSize(2147483647, paramInt1);
    calculateItemFittingMetadata(paramInt1);
    layoutItems(paramInt1);
    int i = this.mLayoutNumRows;
    int j = this.mRowHeight, k = this.mHorizontalDividerHeight;
    paramInt2 = resolveSize((j + k) * i - k, paramInt2);
    setMeasuredDimension(paramInt1, paramInt2);
    if (i > 0)
      positionChildren(getMeasuredWidth(), getMeasuredHeight()); 
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    for (paramInt1 = getChildCount() - 1; paramInt1 >= 0; paramInt1--) {
      View view = getChildAt(paramInt1);
      LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
      view.layout(layoutParams.left, layoutParams.top, layoutParams.right, layoutParams.bottom);
    } 
  }
  
  protected void onDraw(Canvas paramCanvas) {
    Drawable drawable = this.mHorizontalDivider;
    if (drawable != null) {
      ArrayList<Rect> arrayList = this.mHorizontalDividerRects;
      for (int i = arrayList.size() - 1; i >= 0; i--) {
        drawable.setBounds(arrayList.get(i));
        drawable.draw(paramCanvas);
      } 
    } 
    drawable = this.mVerticalDivider;
    if (drawable != null) {
      ArrayList<Rect> arrayList = this.mVerticalDividerRects;
      for (int i = arrayList.size() - 1; i >= 0; i--) {
        drawable.setBounds(arrayList.get(i));
        drawable.draw(paramCanvas);
      } 
    } 
  }
  
  public boolean invokeItem(MenuItemImpl paramMenuItemImpl) {
    return this.mMenu.performItemAction(paramMenuItemImpl, 0);
  }
  
  public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  void markStaleChildren() {
    if (!this.mHasStaleChildren) {
      this.mHasStaleChildren = true;
      requestLayout();
    } 
  }
  
  int getNumActualItemsShown() {
    return this.mNumActualItemsShown;
  }
  
  void setNumActualItemsShown(int paramInt) {
    this.mNumActualItemsShown = paramInt;
  }
  
  public int getWindowAnimations() {
    return this.mAnimations;
  }
  
  public int[] getLayout() {
    return this.mLayout;
  }
  
  public int getLayoutNumRows() {
    return this.mLayoutNumRows;
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent) {
    if (paramKeyEvent.getKeyCode() == 82)
      if (paramKeyEvent.getAction() == 0 && paramKeyEvent.getRepeatCount() == 0) {
        removeCallbacks(this);
        postDelayed(this, ViewConfiguration.getLongPressTimeout());
      } else if (paramKeyEvent.getAction() == 1) {
        if (this.mMenuBeingLongpressed) {
          setCycleShortcutCaptionMode(false);
          return true;
        } 
        removeCallbacks(this);
      }  
    return super.dispatchKeyEvent(paramKeyEvent);
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    requestFocus();
  }
  
  protected void onDetachedFromWindow() {
    setCycleShortcutCaptionMode(false);
    super.onDetachedFromWindow();
  }
  
  public void onWindowFocusChanged(boolean paramBoolean) {
    if (!paramBoolean)
      setCycleShortcutCaptionMode(false); 
    super.onWindowFocusChanged(paramBoolean);
  }
  
  private void setCycleShortcutCaptionMode(boolean paramBoolean) {
    if (!paramBoolean) {
      removeCallbacks(this);
      setChildrenCaptionMode(false);
      this.mMenuBeingLongpressed = false;
    } else {
      setChildrenCaptionMode(true);
    } 
  }
  
  public void run() {
    if (this.mMenuBeingLongpressed) {
      setChildrenCaptionMode(this.mLastChildrenCaptionMode ^ true);
    } else {
      this.mMenuBeingLongpressed = true;
      setCycleShortcutCaptionMode(true);
    } 
    postDelayed(this, 1000L);
  }
  
  private void setChildrenCaptionMode(boolean paramBoolean) {
    this.mLastChildrenCaptionMode = paramBoolean;
    for (int i = getChildCount() - 1; i >= 0; i--)
      ((IconMenuItemView)getChildAt(i)).setCaptionMode(paramBoolean); 
  }
  
  private void calculateItemFittingMetadata(int paramInt) {
    int i = this.mMaxItemsPerRow;
    int j = getChildCount();
    for (byte b = 0; b < j; b++) {
      LayoutParams layoutParams = (LayoutParams)getChildAt(b).getLayoutParams();
      layoutParams.maxNumItemsOnRow = 1;
      for (int k = i; k > 0; 
        k--) {
        if (layoutParams.desiredWidth < paramInt / k) {
          layoutParams.maxNumItemsOnRow = k;
          break;
        } 
      } 
    } 
  }
  
  protected Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    View view = getFocusedChild();
    for (int i = getChildCount() - 1; i >= 0; i--) {
      if (getChildAt(i) == view)
        return (Parcelable)new SavedState(i); 
    } 
    return (Parcelable)new SavedState(-1);
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable) {
    SavedState savedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(savedState.getSuperState());
    if (savedState.focusedPosition >= getChildCount())
      return; 
    View view = getChildAt(savedState.focusedPosition);
    if (view != null)
      view.requestFocus(); 
  }
  
  class SavedState extends View.BaseSavedState {
    public SavedState(int param1Int) {
      super((Parcelable)this$0);
      this.focusedPosition = param1Int;
    }
    
    private SavedState(IconMenuView this$0) {
      super((Parcel)this$0);
      this.focusedPosition = this$0.readInt();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeInt(this.focusedPosition);
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    int focusedPosition;
  }
  
  class LayoutParams extends ViewGroup.MarginLayoutParams {
    int bottom;
    
    int desiredWidth;
    
    int left;
    
    int maxNumItemsOnRow;
    
    int right;
    
    int top;
    
    public LayoutParams(IconMenuView this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
    }
    
    public LayoutParams(IconMenuView this$0, int param1Int1) {
      super(this$0, param1Int1);
    }
  }
}
