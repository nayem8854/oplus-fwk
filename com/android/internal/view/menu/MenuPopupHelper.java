package com.android.internal.view.menu;

import android.content.Context;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;

public class MenuPopupHelper implements MenuHelper {
  private static final int TOUCH_EPICENTER_SIZE_DP = 48;
  
  private View mAnchorView;
  
  private final Context mContext;
  
  private int mDropDownGravity = 8388611;
  
  private boolean mForceShowIcon;
  
  private final PopupWindow.OnDismissListener mInternalOnDismissListener;
  
  private final MenuBuilder mMenu;
  
  private PopupWindow.OnDismissListener mOnDismissListener;
  
  private final boolean mOverflowOnly;
  
  private MenuPopup mPopup;
  
  private final int mPopupStyleAttr;
  
  private final int mPopupStyleRes;
  
  private MenuPresenter.Callback mPresenterCallback;
  
  public MenuPopupHelper(Context paramContext, MenuBuilder paramMenuBuilder) {
    this(paramContext, paramMenuBuilder, null, false, 16843520, 0);
  }
  
  public MenuPopupHelper(Context paramContext, MenuBuilder paramMenuBuilder, View paramView) {
    this(paramContext, paramMenuBuilder, paramView, false, 16843520, 0);
  }
  
  public MenuPopupHelper(Context paramContext, MenuBuilder paramMenuBuilder, View paramView, boolean paramBoolean, int paramInt) {
    this(paramContext, paramMenuBuilder, paramView, paramBoolean, paramInt, 0);
  }
  
  public void setOnDismissListener(PopupWindow.OnDismissListener paramOnDismissListener) {
    this.mOnDismissListener = paramOnDismissListener;
  }
  
  public void setAnchorView(View paramView) {
    this.mAnchorView = paramView;
  }
  
  public void setForceShowIcon(boolean paramBoolean) {
    this.mForceShowIcon = paramBoolean;
    MenuPopup menuPopup = this.mPopup;
    if (menuPopup != null)
      menuPopup.setForceShowIcon(paramBoolean); 
  }
  
  public void setGravity(int paramInt) {
    this.mDropDownGravity = paramInt;
  }
  
  public int getGravity() {
    return this.mDropDownGravity;
  }
  
  public void show() {
    if (tryShow())
      return; 
    throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
  }
  
  public void show(int paramInt1, int paramInt2) {
    if (tryShow(paramInt1, paramInt2))
      return; 
    throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
  }
  
  public MenuPopup getPopup() {
    if (this.mPopup == null)
      this.mPopup = createPopup(); 
    return this.mPopup;
  }
  
  public boolean tryShow() {
    if (isShowing())
      return true; 
    if (this.mAnchorView == null)
      return false; 
    showPopup(0, 0, false, false);
    return true;
  }
  
  public boolean tryShow(int paramInt1, int paramInt2) {
    if (isShowing())
      return true; 
    if (this.mAnchorView == null)
      return false; 
    showPopup(paramInt1, paramInt2, true, true);
    return true;
  }
  
  private MenuPopup createPopup() {
    StandardMenuPopup standardMenuPopup;
    WindowManager windowManager = (WindowManager)this.mContext.getSystemService(WindowManager.class);
    Rect rect = windowManager.getMaximumWindowMetrics().getBounds();
    int i = Math.min(rect.width(), rect.height());
    int j = this.mContext.getResources().getDimensionPixelSize(17105029);
    if (i >= j) {
      j = 1;
    } else {
      j = 0;
    } 
    if (j != 0) {
      CascadingMenuPopup cascadingMenuPopup = new CascadingMenuPopup(this.mContext, this.mAnchorView, this.mPopupStyleAttr, this.mPopupStyleRes, this.mOverflowOnly);
    } else {
      standardMenuPopup = new StandardMenuPopup(this.mContext, this.mMenu, this.mAnchorView, this.mPopupStyleAttr, this.mPopupStyleRes, this.mOverflowOnly);
    } 
    standardMenuPopup.addMenu(this.mMenu);
    standardMenuPopup.setOnDismissListener(this.mInternalOnDismissListener);
    standardMenuPopup.setAnchorView(this.mAnchorView);
    standardMenuPopup.setCallback(this.mPresenterCallback);
    standardMenuPopup.setForceShowIcon(this.mForceShowIcon);
    standardMenuPopup.setGravity(this.mDropDownGravity);
    return standardMenuPopup;
  }
  
  private void showPopup(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2) {
    MenuPopup menuPopup = getPopup();
    menuPopup.setShowTitle(paramBoolean2);
    if (paramBoolean1) {
      int i = this.mDropDownGravity;
      View view = this.mAnchorView;
      int j = view.getLayoutDirection();
      j = Gravity.getAbsoluteGravity(i, j);
      i = paramInt1;
      if ((j & 0x7) == 5)
        i = paramInt1 - this.mAnchorView.getWidth(); 
      menuPopup.setHorizontalOffset(i);
      menuPopup.setVerticalOffset(paramInt2);
      float f = (this.mContext.getResources().getDisplayMetrics()).density;
      paramInt1 = (int)(48.0F * f / 2.0F);
      Rect rect = new Rect(i - paramInt1, paramInt2 - paramInt1, i + paramInt1, paramInt2 + paramInt1);
      menuPopup.setEpicenterBounds(rect);
    } 
    menuPopup.show();
  }
  
  public void dismiss() {
    if (isShowing())
      this.mPopup.dismiss(); 
  }
  
  protected void onDismiss() {
    this.mPopup = null;
    PopupWindow.OnDismissListener onDismissListener = this.mOnDismissListener;
    if (onDismissListener != null)
      onDismissListener.onDismiss(); 
  }
  
  public boolean isShowing() {
    boolean bool;
    MenuPopup menuPopup = this.mPopup;
    if (menuPopup != null && menuPopup.isShowing()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setPresenterCallback(MenuPresenter.Callback paramCallback) {
    this.mPresenterCallback = paramCallback;
    MenuPopup menuPopup = this.mPopup;
    if (menuPopup != null)
      menuPopup.setCallback(paramCallback); 
  }
  
  public MenuPopupHelper(Context paramContext, MenuBuilder paramMenuBuilder, View paramView, boolean paramBoolean, int paramInt1, int paramInt2) {
    this.mInternalOnDismissListener = new PopupWindow.OnDismissListener() {
        final MenuPopupHelper this$0;
        
        public void onDismiss() {
          MenuPopupHelper.this.onDismiss();
        }
      };
    this.mContext = paramContext;
    this.mMenu = paramMenuBuilder;
    this.mAnchorView = paramView;
    this.mOverflowOnly = paramBoolean;
    this.mPopupStyleAttr = paramInt1;
    this.mPopupStyleRes = paramInt2;
  }
}
