package com.android.internal.view.menu;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

public class MenuAdapter extends BaseAdapter {
  MenuBuilder mAdapterMenu;
  
  private int mExpandedIndex = -1;
  
  private boolean mForceShowIcon;
  
  private final LayoutInflater mInflater;
  
  private final int mItemLayoutRes;
  
  private final boolean mOverflowOnly;
  
  public MenuAdapter(MenuBuilder paramMenuBuilder, LayoutInflater paramLayoutInflater, boolean paramBoolean, int paramInt) {
    this.mOverflowOnly = paramBoolean;
    this.mInflater = paramLayoutInflater;
    this.mAdapterMenu = paramMenuBuilder;
    this.mItemLayoutRes = paramInt;
    findExpandedIndex();
  }
  
  public boolean getForceShowIcon() {
    return this.mForceShowIcon;
  }
  
  public void setForceShowIcon(boolean paramBoolean) {
    this.mForceShowIcon = paramBoolean;
  }
  
  public int getCount() {
    ArrayList<MenuItemImpl> arrayList;
    if (this.mOverflowOnly) {
      arrayList = this.mAdapterMenu.getNonActionItems();
    } else {
      arrayList = this.mAdapterMenu.getVisibleItems();
    } 
    if (this.mExpandedIndex < 0)
      return arrayList.size(); 
    return arrayList.size() - 1;
  }
  
  public MenuBuilder getAdapterMenu() {
    return this.mAdapterMenu;
  }
  
  public MenuItemImpl getItem(int paramInt) {
    ArrayList<MenuItemImpl> arrayList;
    if (this.mOverflowOnly) {
      arrayList = this.mAdapterMenu.getNonActionItems();
    } else {
      arrayList = this.mAdapterMenu.getVisibleItems();
    } 
    int i = this.mExpandedIndex, j = paramInt;
    if (i >= 0) {
      j = paramInt;
      if (paramInt >= i)
        j = paramInt + 1; 
    } 
    return arrayList.get(j);
  }
  
  public long getItemId(int paramInt) {
    return paramInt;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    int j;
    boolean bool;
    View view = paramView;
    if (paramView == null)
      view = this.mInflater.inflate(this.mItemLayoutRes, paramViewGroup, false); 
    int i = getItem(paramInt).getGroupId();
    if (paramInt - 1 >= 0) {
      j = getItem(paramInt - 1).getGroupId();
    } else {
      j = i;
    } 
    ListMenuItemView listMenuItemView = (ListMenuItemView)view;
    MenuBuilder menuBuilder = this.mAdapterMenu;
    if (menuBuilder.isGroupDividerEnabled() && i != j) {
      bool = true;
    } else {
      bool = false;
    } 
    listMenuItemView.setGroupDividerEnabled(bool);
    MenuView.ItemView itemView = (MenuView.ItemView)view;
    if (this.mForceShowIcon)
      ((ListMenuItemView)view).setForceShowIcon(true); 
    itemView.initialize(getItem(paramInt), 0);
    return view;
  }
  
  void findExpandedIndex() {
    MenuItemImpl menuItemImpl = this.mAdapterMenu.getExpandedItem();
    if (menuItemImpl != null) {
      ArrayList<MenuItemImpl> arrayList = this.mAdapterMenu.getNonActionItems();
      int i = arrayList.size();
      for (byte b = 0; b < i; b++) {
        MenuItemImpl menuItemImpl1 = arrayList.get(b);
        if (menuItemImpl1 == menuItemImpl) {
          this.mExpandedIndex = b;
          return;
        } 
      } 
    } 
    this.mExpandedIndex = -1;
  }
  
  public void notifyDataSetChanged() {
    findExpandedIndex();
    super.notifyDataSetChanged();
  }
}
