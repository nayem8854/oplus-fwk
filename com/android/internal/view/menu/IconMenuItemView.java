package com.android.internal.view.menu;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.text.TextDirectionHeuristic;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewDebug.CapturedViewProperty;
import android.view.ViewGroup;
import android.widget.TextView;
import com.android.internal.R;

public final class IconMenuItemView extends TextView implements MenuView.ItemView {
  private static final int NO_ALPHA = 255;
  
  private static String sPrependShortcutLabel;
  
  private float mDisabledAlpha;
  
  private Drawable mIcon;
  
  private IconMenuView mIconMenuView;
  
  private MenuItemImpl mItemData;
  
  private MenuBuilder.ItemInvoker mItemInvoker;
  
  private Rect mPositionIconAvailable = new Rect();
  
  private Rect mPositionIconOutput = new Rect();
  
  private String mShortcutCaption;
  
  private boolean mShortcutCaptionMode;
  
  private int mTextAppearance;
  
  private Context mTextAppearanceContext;
  
  public IconMenuItemView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    if (sPrependShortcutLabel == null)
      sPrependShortcutLabel = getResources().getString(17041125); 
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.MenuView, paramInt1, paramInt2);
    this.mDisabledAlpha = typedArray.getFloat(6, 0.8F);
    this.mTextAppearance = typedArray.getResourceId(1, -1);
    this.mTextAppearanceContext = paramContext;
    typedArray.recycle();
  }
  
  public IconMenuItemView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public IconMenuItemView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  void initialize(CharSequence paramCharSequence, Drawable paramDrawable) {
    setClickable(true);
    setFocusable(true);
    int i = this.mTextAppearance;
    if (i != -1)
      setTextAppearance(this.mTextAppearanceContext, i); 
    setTitle(paramCharSequence);
    setIcon(paramDrawable);
    MenuItemImpl menuItemImpl = this.mItemData;
    if (menuItemImpl != null) {
      CharSequence charSequence = menuItemImpl.getContentDescription();
      if (TextUtils.isEmpty(charSequence)) {
        setContentDescription(paramCharSequence);
      } else {
        setContentDescription(charSequence);
      } 
      setTooltipText(this.mItemData.getTooltipText());
    } 
  }
  
  public void initialize(MenuItemImpl paramMenuItemImpl, int paramInt) {
    this.mItemData = paramMenuItemImpl;
    initialize(paramMenuItemImpl.getTitleForItemView(this), paramMenuItemImpl.getIcon());
    if (paramMenuItemImpl.isVisible()) {
      paramInt = 0;
    } else {
      paramInt = 8;
    } 
    setVisibility(paramInt);
    setEnabled(paramMenuItemImpl.isEnabled());
  }
  
  public void setItemData(MenuItemImpl paramMenuItemImpl) {
    this.mItemData = paramMenuItemImpl;
  }
  
  public boolean performClick() {
    if (super.performClick())
      return true; 
    MenuBuilder.ItemInvoker itemInvoker = this.mItemInvoker;
    if (itemInvoker != null && itemInvoker.invokeItem(this.mItemData)) {
      playSoundEffect(0);
      return true;
    } 
    return false;
  }
  
  public void setTitle(CharSequence paramCharSequence) {
    if (this.mShortcutCaptionMode) {
      setCaptionMode(true);
    } else if (paramCharSequence != null) {
      setText(paramCharSequence);
    } 
  }
  
  void setCaptionMode(boolean paramBoolean) {
    MenuItemImpl menuItemImpl = this.mItemData;
    if (menuItemImpl == null)
      return; 
    if (paramBoolean && menuItemImpl.shouldShowShortcut()) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    } 
    this.mShortcutCaptionMode = paramBoolean;
    CharSequence charSequence = this.mItemData.getTitleForItemView(this);
    if (this.mShortcutCaptionMode) {
      if (this.mShortcutCaption == null)
        this.mShortcutCaption = this.mItemData.getShortcutLabel(); 
      charSequence = this.mShortcutCaption;
    } 
    setText(charSequence);
  }
  
  public void setIcon(Drawable paramDrawable) {
    this.mIcon = paramDrawable;
    if (paramDrawable != null) {
      paramDrawable.setBounds(0, 0, paramDrawable.getIntrinsicWidth(), paramDrawable.getIntrinsicHeight());
      setCompoundDrawables(null, paramDrawable, null, null);
      setGravity(81);
      requestLayout();
    } else {
      setCompoundDrawables(null, null, null, null);
      setGravity(17);
    } 
  }
  
  public void setItemInvoker(MenuBuilder.ItemInvoker paramItemInvoker) {
    this.mItemInvoker = paramItemInvoker;
  }
  
  @CapturedViewProperty(retrieveReturn = true)
  public MenuItemImpl getItemData() {
    return this.mItemData;
  }
  
  public void setVisibility(int paramInt) {
    super.setVisibility(paramInt);
    IconMenuView iconMenuView = this.mIconMenuView;
    if (iconMenuView != null)
      iconMenuView.markStaleChildren(); 
  }
  
  void setIconMenuView(IconMenuView paramIconMenuView) {
    this.mIconMenuView = paramIconMenuView;
  }
  
  protected void drawableStateChanged() {
    super.drawableStateChanged();
    MenuItemImpl menuItemImpl = this.mItemData;
    if (menuItemImpl != null && this.mIcon != null) {
      int i;
      if (!menuItemImpl.isEnabled() && (isPressed() || !isFocused())) {
        i = 1;
      } else {
        i = 0;
      } 
      Drawable drawable = this.mIcon;
      if (i) {
        i = (int)(this.mDisabledAlpha * 255.0F);
      } else {
        i = 255;
      } 
      drawable.setAlpha(i);
    } 
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    positionIcon();
  }
  
  protected void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
    super.onTextChanged(paramCharSequence, paramInt1, paramInt2, paramInt3);
    setLayoutParams((ViewGroup.LayoutParams)getTextAppropriateLayoutParams());
  }
  
  IconMenuView.LayoutParams getTextAppropriateLayoutParams() {
    IconMenuView.LayoutParams layoutParams1 = (IconMenuView.LayoutParams)getLayoutParams();
    IconMenuView.LayoutParams layoutParams2 = layoutParams1;
    if (layoutParams1 == null)
      layoutParams2 = new IconMenuView.LayoutParams(-1, -1); 
    CharSequence charSequence = getText();
    int i = getText().length();
    TextPaint textPaint = getPaint();
    TextDirectionHeuristic textDirectionHeuristic = getTextDirectionHeuristic();
    layoutParams2.desiredWidth = (int)Layout.getDesiredWidth(charSequence, 0, i, textPaint, textDirectionHeuristic);
    return layoutParams2;
  }
  
  private void positionIcon() {
    if (this.mIcon == null)
      return; 
    Rect rect2 = this.mPositionIconOutput;
    getLineBounds(0, rect2);
    this.mPositionIconAvailable.set(0, 0, getWidth(), rect2.top);
    int i = getLayoutDirection();
    int j = this.mIcon.getIntrinsicWidth();
    Drawable drawable = this.mIcon;
    int k = drawable.getIntrinsicHeight();
    Rect rect1 = this.mPositionIconAvailable, rect3 = this.mPositionIconOutput;
    Gravity.apply(8388627, j, k, rect1, rect3, i);
    this.mIcon.setBounds(this.mPositionIconOutput);
  }
  
  public void setCheckable(boolean paramBoolean) {}
  
  public void setChecked(boolean paramBoolean) {}
  
  public void setShortcut(boolean paramBoolean, char paramChar) {
    if (this.mShortcutCaptionMode) {
      this.mShortcutCaption = null;
      setCaptionMode(true);
    } 
  }
  
  public boolean prefersCondensedTitle() {
    return true;
  }
  
  public boolean showsIcon() {
    return true;
  }
}
