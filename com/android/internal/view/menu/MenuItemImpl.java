package com.android.internal.view.menu;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewDebug.CapturedViewProperty;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public final class MenuItemImpl implements MenuItem {
  private int mShortcutNumericModifiers = 4096;
  
  private int mShortcutAlphabeticModifiers = 4096;
  
  private int mIconResId = 0;
  
  private ColorStateList mIconTintList = null;
  
  private PorterDuff.Mode mIconTintMode = null;
  
  private boolean mHasIconTint = false;
  
  private boolean mHasIconTintMode = false;
  
  private boolean mNeedToApplyIconTint = false;
  
  private int mFlags = 16;
  
  private int mShowAsAction = 0;
  
  private boolean mIsActionViewExpanded = false;
  
  private static final int CHECKABLE = 1;
  
  private static final int CHECKED = 2;
  
  private static final int ENABLED = 16;
  
  private static final int EXCLUSIVE = 4;
  
  private static final int HIDDEN = 8;
  
  private static final int IS_ACTION = 32;
  
  static final int NO_ICON = 0;
  
  private static final int SHOW_AS_ACTION_MASK = 3;
  
  private static final String TAG = "MenuItemImpl";
  
  private ActionProvider mActionProvider;
  
  private View mActionView;
  
  private final int mCategoryOrder;
  
  private MenuItem.OnMenuItemClickListener mClickListener;
  
  private CharSequence mContentDescription;
  
  private final int mGroup;
  
  private Drawable mIconDrawable;
  
  private final int mId;
  
  private Intent mIntent;
  
  private Runnable mItemCallback;
  
  private MenuBuilder mMenu;
  
  private ContextMenu.ContextMenuInfo mMenuInfo;
  
  private MenuItem.OnActionExpandListener mOnActionExpandListener;
  
  private final int mOrdering;
  
  private char mShortcutAlphabeticChar;
  
  private char mShortcutNumericChar;
  
  private SubMenuBuilder mSubMenu;
  
  private CharSequence mTitle;
  
  private CharSequence mTitleCondensed;
  
  private CharSequence mTooltipText;
  
  MenuItemImpl(MenuBuilder paramMenuBuilder, int paramInt1, int paramInt2, int paramInt3, int paramInt4, CharSequence paramCharSequence, int paramInt5) {
    this.mMenu = paramMenuBuilder;
    this.mId = paramInt2;
    this.mGroup = paramInt1;
    this.mCategoryOrder = paramInt3;
    this.mOrdering = paramInt4;
    this.mTitle = paramCharSequence;
    this.mShowAsAction = paramInt5;
  }
  
  public boolean invoke() {
    MenuItem.OnMenuItemClickListener onMenuItemClickListener = this.mClickListener;
    if (onMenuItemClickListener != null && 
      onMenuItemClickListener.onMenuItemClick(this))
      return true; 
    MenuBuilder menuBuilder = this.mMenu;
    if (menuBuilder.dispatchMenuItemSelected(menuBuilder, this))
      return true; 
    Runnable runnable = this.mItemCallback;
    if (runnable != null) {
      runnable.run();
      return true;
    } 
    if (this.mIntent != null)
      try {
        this.mMenu.getContext().startActivity(this.mIntent);
        return true;
      } catch (ActivityNotFoundException activityNotFoundException) {
        Log.e("MenuItemImpl", "Can't find activity to handle intent; ignoring", (Throwable)activityNotFoundException);
      }  
    ActionProvider actionProvider = this.mActionProvider;
    if (actionProvider != null && actionProvider.onPerformDefaultAction())
      return true; 
    return false;
  }
  
  public boolean isEnabled() {
    boolean bool;
    if ((this.mFlags & 0x10) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public MenuItem setEnabled(boolean paramBoolean) {
    if (paramBoolean) {
      this.mFlags |= 0x10;
    } else {
      this.mFlags &= 0xFFFFFFEF;
    } 
    this.mMenu.onItemsChanged(false);
    return this;
  }
  
  public int getGroupId() {
    return this.mGroup;
  }
  
  @CapturedViewProperty
  public int getItemId() {
    return this.mId;
  }
  
  public int getOrder() {
    return this.mCategoryOrder;
  }
  
  public int getOrdering() {
    return this.mOrdering;
  }
  
  public Intent getIntent() {
    return this.mIntent;
  }
  
  public MenuItem setIntent(Intent paramIntent) {
    this.mIntent = paramIntent;
    return this;
  }
  
  Runnable getCallback() {
    return this.mItemCallback;
  }
  
  public MenuItem setCallback(Runnable paramRunnable) {
    this.mItemCallback = paramRunnable;
    return this;
  }
  
  public char getAlphabeticShortcut() {
    return this.mShortcutAlphabeticChar;
  }
  
  public int getAlphabeticModifiers() {
    return this.mShortcutAlphabeticModifiers;
  }
  
  public MenuItem setAlphabeticShortcut(char paramChar) {
    if (this.mShortcutAlphabeticChar == paramChar)
      return this; 
    this.mShortcutAlphabeticChar = Character.toLowerCase(paramChar);
    this.mMenu.onItemsChanged(false);
    return this;
  }
  
  public MenuItem setAlphabeticShortcut(char paramChar, int paramInt) {
    if (this.mShortcutAlphabeticChar == paramChar && this.mShortcutAlphabeticModifiers == paramInt)
      return this; 
    this.mShortcutAlphabeticChar = Character.toLowerCase(paramChar);
    this.mShortcutAlphabeticModifiers = KeyEvent.normalizeMetaState(paramInt);
    this.mMenu.onItemsChanged(false);
    return this;
  }
  
  public char getNumericShortcut() {
    return this.mShortcutNumericChar;
  }
  
  public int getNumericModifiers() {
    return this.mShortcutNumericModifiers;
  }
  
  public MenuItem setNumericShortcut(char paramChar) {
    if (this.mShortcutNumericChar == paramChar)
      return this; 
    this.mShortcutNumericChar = paramChar;
    this.mMenu.onItemsChanged(false);
    return this;
  }
  
  public MenuItem setNumericShortcut(char paramChar, int paramInt) {
    if (this.mShortcutNumericChar == paramChar && this.mShortcutNumericModifiers == paramInt)
      return this; 
    this.mShortcutNumericChar = paramChar;
    this.mShortcutNumericModifiers = KeyEvent.normalizeMetaState(paramInt);
    this.mMenu.onItemsChanged(false);
    return this;
  }
  
  public MenuItem setShortcut(char paramChar1, char paramChar2) {
    this.mShortcutNumericChar = paramChar1;
    this.mShortcutAlphabeticChar = Character.toLowerCase(paramChar2);
    this.mMenu.onItemsChanged(false);
    return this;
  }
  
  public MenuItem setShortcut(char paramChar1, char paramChar2, int paramInt1, int paramInt2) {
    this.mShortcutNumericChar = paramChar1;
    this.mShortcutNumericModifiers = KeyEvent.normalizeMetaState(paramInt1);
    this.mShortcutAlphabeticChar = Character.toLowerCase(paramChar2);
    this.mShortcutAlphabeticModifiers = KeyEvent.normalizeMetaState(paramInt2);
    this.mMenu.onItemsChanged(false);
    return this;
  }
  
  char getShortcut() {
    char c;
    if (this.mMenu.isQwertyMode()) {
      c = this.mShortcutAlphabeticChar;
    } else {
      c = this.mShortcutNumericChar;
    } 
    return c;
  }
  
  String getShortcutLabel() {
    int i;
    char c = getShortcut();
    if (c == '\000')
      return ""; 
    Resources resources = this.mMenu.getContext().getResources();
    StringBuilder stringBuilder = new StringBuilder();
    if (ViewConfiguration.get(this.mMenu.getContext()).hasPermanentMenuKey())
      stringBuilder.append(resources.getString(17041125)); 
    if (this.mMenu.isQwertyMode()) {
      i = this.mShortcutAlphabeticModifiers;
    } else {
      i = this.mShortcutNumericModifiers;
    } 
    appendModifier(stringBuilder, i, 65536, resources.getString(17040631));
    appendModifier(stringBuilder, i, 4096, resources.getString(17040627));
    appendModifier(stringBuilder, i, 2, resources.getString(17040626));
    appendModifier(stringBuilder, i, 1, resources.getString(17040632));
    appendModifier(stringBuilder, i, 4, resources.getString(17040634));
    appendModifier(stringBuilder, i, 8, resources.getString(17040630));
    if (c != '\b') {
      if (c != '\n') {
        if (c != ' ') {
          stringBuilder.append(c);
        } else {
          stringBuilder.append(resources.getString(17040633));
        } 
      } else {
        stringBuilder.append(resources.getString(17040629));
      } 
    } else {
      stringBuilder.append(resources.getString(17040628));
    } 
    return stringBuilder.toString();
  }
  
  private static void appendModifier(StringBuilder paramStringBuilder, int paramInt1, int paramInt2, String paramString) {
    if ((paramInt1 & paramInt2) == paramInt2)
      paramStringBuilder.append(paramString); 
  }
  
  boolean shouldShowShortcut() {
    boolean bool;
    if (this.mMenu.isShortcutsVisible() && getShortcut() != '\000') {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public SubMenu getSubMenu() {
    return this.mSubMenu;
  }
  
  public boolean hasSubMenu() {
    boolean bool;
    if (this.mSubMenu != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  void setSubMenu(SubMenuBuilder paramSubMenuBuilder) {
    this.mSubMenu = paramSubMenuBuilder;
    paramSubMenuBuilder.setHeaderTitle(getTitle());
  }
  
  @CapturedViewProperty
  public CharSequence getTitle() {
    return this.mTitle;
  }
  
  CharSequence getTitleForItemView(MenuView.ItemView paramItemView) {
    CharSequence charSequence;
    if (paramItemView != null && paramItemView.prefersCondensedTitle()) {
      charSequence = getTitleCondensed();
    } else {
      charSequence = getTitle();
    } 
    return charSequence;
  }
  
  public MenuItem setTitle(CharSequence paramCharSequence) {
    this.mTitle = paramCharSequence;
    this.mMenu.onItemsChanged(false);
    SubMenuBuilder subMenuBuilder = this.mSubMenu;
    if (subMenuBuilder != null)
      subMenuBuilder.setHeaderTitle(paramCharSequence); 
    return this;
  }
  
  public MenuItem setTitle(int paramInt) {
    return setTitle(this.mMenu.getContext().getString(paramInt));
  }
  
  public CharSequence getTitleCondensed() {
    CharSequence charSequence = this.mTitleCondensed;
    if (charSequence == null)
      charSequence = this.mTitle; 
    return charSequence;
  }
  
  public MenuItem setTitleCondensed(CharSequence paramCharSequence) {
    this.mTitleCondensed = paramCharSequence;
    if (paramCharSequence == null)
      paramCharSequence = this.mTitle; 
    this.mMenu.onItemsChanged(false);
    return this;
  }
  
  public Drawable getIcon() {
    Drawable drawable = this.mIconDrawable;
    if (drawable != null)
      return applyIconTintIfNecessary(drawable); 
    if (this.mIconResId != 0) {
      drawable = this.mMenu.getContext().getDrawable(this.mIconResId);
      this.mIconResId = 0;
      this.mIconDrawable = drawable;
      return applyIconTintIfNecessary(drawable);
    } 
    return null;
  }
  
  public MenuItem setIcon(Drawable paramDrawable) {
    this.mIconResId = 0;
    this.mIconDrawable = paramDrawable;
    this.mNeedToApplyIconTint = true;
    this.mMenu.onItemsChanged(false);
    return this;
  }
  
  public MenuItem setIcon(int paramInt) {
    this.mIconDrawable = null;
    this.mIconResId = paramInt;
    this.mNeedToApplyIconTint = true;
    this.mMenu.onItemsChanged(false);
    return this;
  }
  
  public MenuItem setIconTintList(ColorStateList paramColorStateList) {
    this.mIconTintList = paramColorStateList;
    this.mHasIconTint = true;
    this.mNeedToApplyIconTint = true;
    this.mMenu.onItemsChanged(false);
    return this;
  }
  
  public ColorStateList getIconTintList() {
    return this.mIconTintList;
  }
  
  public MenuItem setIconTintMode(PorterDuff.Mode paramMode) {
    this.mIconTintMode = paramMode;
    this.mHasIconTintMode = true;
    this.mNeedToApplyIconTint = true;
    this.mMenu.onItemsChanged(false);
    return this;
  }
  
  public PorterDuff.Mode getIconTintMode() {
    return this.mIconTintMode;
  }
  
  private Drawable applyIconTintIfNecessary(Drawable paramDrawable) {
    // Byte code:
    //   0: aload_1
    //   1: astore_2
    //   2: aload_1
    //   3: ifnull -> 71
    //   6: aload_1
    //   7: astore_2
    //   8: aload_0
    //   9: getfield mNeedToApplyIconTint : Z
    //   12: ifeq -> 71
    //   15: aload_0
    //   16: getfield mHasIconTint : Z
    //   19: ifne -> 31
    //   22: aload_1
    //   23: astore_2
    //   24: aload_0
    //   25: getfield mHasIconTintMode : Z
    //   28: ifeq -> 71
    //   31: aload_1
    //   32: invokevirtual mutate : ()Landroid/graphics/drawable/Drawable;
    //   35: astore_2
    //   36: aload_0
    //   37: getfield mHasIconTint : Z
    //   40: ifeq -> 51
    //   43: aload_2
    //   44: aload_0
    //   45: getfield mIconTintList : Landroid/content/res/ColorStateList;
    //   48: invokevirtual setTintList : (Landroid/content/res/ColorStateList;)V
    //   51: aload_0
    //   52: getfield mHasIconTintMode : Z
    //   55: ifeq -> 66
    //   58: aload_2
    //   59: aload_0
    //   60: getfield mIconTintMode : Landroid/graphics/PorterDuff$Mode;
    //   63: invokevirtual setTintMode : (Landroid/graphics/PorterDuff$Mode;)V
    //   66: aload_0
    //   67: iconst_0
    //   68: putfield mNeedToApplyIconTint : Z
    //   71: aload_2
    //   72: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #540	-> 0
    //   #541	-> 31
    //   #543	-> 36
    //   #544	-> 43
    //   #547	-> 51
    //   #548	-> 58
    //   #551	-> 66
    //   #554	-> 71
  }
  
  public boolean isCheckable() {
    int i = this.mFlags;
    boolean bool = true;
    if ((i & 0x1) != 1)
      bool = false; 
    return bool;
  }
  
  public MenuItem setCheckable(boolean paramBoolean) {
    int i = this.mFlags;
    int j = this.mFlags & 0xFFFFFFFE | paramBoolean;
    if (i != j)
      this.mMenu.onItemsChanged(false); 
    return this;
  }
  
  public void setExclusiveCheckable(boolean paramBoolean) {
    boolean bool;
    int i = this.mFlags;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mFlags = i & 0xFFFFFFFB | bool;
  }
  
  public boolean isExclusiveCheckable() {
    boolean bool;
    if ((this.mFlags & 0x4) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isChecked() {
    boolean bool;
    if ((this.mFlags & 0x2) == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public MenuItem setChecked(boolean paramBoolean) {
    if ((this.mFlags & 0x4) != 0) {
      this.mMenu.setExclusiveItemChecked(this);
    } else {
      setCheckedInt(paramBoolean);
    } 
    return this;
  }
  
  void setCheckedInt(boolean paramBoolean) {
    int i = this.mFlags;
    int j = this.mFlags;
    if (paramBoolean) {
      k = 2;
    } else {
      k = 0;
    } 
    int k = j & 0xFFFFFFFD | k;
    if (i != k)
      this.mMenu.onItemsChanged(false); 
  }
  
  public boolean isVisible() {
    ActionProvider actionProvider = this.mActionProvider;
    boolean bool1 = true, bool2 = true;
    if (actionProvider != null && actionProvider.overridesItemVisibility()) {
      if ((this.mFlags & 0x8) != 0 || !this.mActionProvider.isVisible())
        bool2 = false; 
      return bool2;
    } 
    if ((this.mFlags & 0x8) == 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    return bool2;
  }
  
  boolean setVisibleInt(boolean paramBoolean) {
    int i = this.mFlags;
    int j = this.mFlags;
    boolean bool = false;
    if (paramBoolean) {
      k = 0;
    } else {
      k = 8;
    } 
    int k = j & 0xFFFFFFF7 | k;
    paramBoolean = bool;
    if (i != k)
      paramBoolean = true; 
    return paramBoolean;
  }
  
  public MenuItem setVisible(boolean paramBoolean) {
    if (setVisibleInt(paramBoolean))
      this.mMenu.onItemVisibleChanged(this); 
    return this;
  }
  
  public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener paramOnMenuItemClickListener) {
    this.mClickListener = paramOnMenuItemClickListener;
    return this;
  }
  
  public String toString() {
    CharSequence charSequence = this.mTitle;
    if (charSequence != null) {
      charSequence = charSequence.toString();
    } else {
      charSequence = null;
    } 
    return (String)charSequence;
  }
  
  void setMenuInfo(ContextMenu.ContextMenuInfo paramContextMenuInfo) {
    this.mMenuInfo = paramContextMenuInfo;
  }
  
  public ContextMenu.ContextMenuInfo getMenuInfo() {
    return this.mMenuInfo;
  }
  
  public void actionFormatChanged() {
    this.mMenu.onItemActionRequestChanged(this);
  }
  
  public boolean shouldShowIcon() {
    return this.mMenu.getOptionalIconsVisible();
  }
  
  public boolean isActionButton() {
    boolean bool;
    if ((this.mFlags & 0x20) == 32) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean requestsActionButton() {
    int i = this.mShowAsAction;
    boolean bool = true;
    if ((i & 0x1) != 1)
      bool = false; 
    return bool;
  }
  
  public boolean requiresActionButton() {
    boolean bool;
    if ((this.mShowAsAction & 0x2) == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean requiresOverflow() {
    boolean bool;
    if (!requiresActionButton() && !requestsActionButton()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setIsActionButton(boolean paramBoolean) {
    if (paramBoolean) {
      this.mFlags |= 0x20;
    } else {
      this.mFlags &= 0xFFFFFFDF;
    } 
  }
  
  public boolean showsTextAsAction() {
    boolean bool;
    if ((this.mShowAsAction & 0x4) == 4) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setShowAsAction(int paramInt) {
    int i = paramInt & 0x3;
    if (i == 0 || i == 1 || i == 2) {
      this.mShowAsAction = paramInt;
      this.mMenu.onItemActionRequestChanged(this);
      return;
    } 
    throw new IllegalArgumentException("SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive.");
  }
  
  public MenuItem setActionView(View paramView) {
    this.mActionView = paramView;
    this.mActionProvider = null;
    if (paramView != null && paramView.getId() == -1) {
      int i = this.mId;
      if (i > 0)
        paramView.setId(i); 
    } 
    this.mMenu.onItemActionRequestChanged(this);
    return this;
  }
  
  public MenuItem setActionView(int paramInt) {
    Context context = this.mMenu.getContext();
    LayoutInflater layoutInflater = LayoutInflater.from(context);
    setActionView(layoutInflater.inflate(paramInt, (ViewGroup)new LinearLayout(context), false));
    return this;
  }
  
  public View getActionView() {
    View view = this.mActionView;
    if (view != null)
      return view; 
    ActionProvider actionProvider = this.mActionProvider;
    if (actionProvider != null) {
      View view1 = actionProvider.onCreateActionView(this);
      return view1;
    } 
    return null;
  }
  
  public ActionProvider getActionProvider() {
    return this.mActionProvider;
  }
  
  public MenuItem setActionProvider(ActionProvider paramActionProvider) {
    ActionProvider actionProvider = this.mActionProvider;
    if (actionProvider != null)
      actionProvider.reset(); 
    this.mActionView = null;
    this.mActionProvider = paramActionProvider;
    this.mMenu.onItemsChanged(true);
    paramActionProvider = this.mActionProvider;
    if (paramActionProvider != null)
      paramActionProvider.setVisibilityListener(new ActionProvider.VisibilityListener() {
            final MenuItemImpl this$0;
            
            public void onActionProviderVisibilityChanged(boolean param1Boolean) {
              MenuItemImpl.this.mMenu.onItemVisibleChanged(MenuItemImpl.this);
            }
          }); 
    return this;
  }
  
  public MenuItem setShowAsActionFlags(int paramInt) {
    setShowAsAction(paramInt);
    return this;
  }
  
  public boolean expandActionView() {
    if (!hasCollapsibleActionView())
      return false; 
    MenuItem.OnActionExpandListener onActionExpandListener = this.mOnActionExpandListener;
    if (onActionExpandListener == null || 
      onActionExpandListener.onMenuItemActionExpand(this))
      return this.mMenu.expandItemActionView(this); 
    return false;
  }
  
  public boolean collapseActionView() {
    if ((this.mShowAsAction & 0x8) == 0)
      return false; 
    if (this.mActionView == null)
      return true; 
    MenuItem.OnActionExpandListener onActionExpandListener = this.mOnActionExpandListener;
    if (onActionExpandListener == null || 
      onActionExpandListener.onMenuItemActionCollapse(this))
      return this.mMenu.collapseItemActionView(this); 
    return false;
  }
  
  public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener paramOnActionExpandListener) {
    this.mOnActionExpandListener = paramOnActionExpandListener;
    return this;
  }
  
  public boolean hasCollapsibleActionView() {
    int i = this.mShowAsAction;
    boolean bool = false;
    if ((i & 0x8) != 0) {
      if (this.mActionView == null) {
        ActionProvider actionProvider = this.mActionProvider;
        if (actionProvider != null)
          this.mActionView = actionProvider.onCreateActionView(this); 
      } 
      if (this.mActionView != null)
        bool = true; 
      return bool;
    } 
    return false;
  }
  
  public void setActionViewExpanded(boolean paramBoolean) {
    this.mIsActionViewExpanded = paramBoolean;
    this.mMenu.onItemsChanged(false);
  }
  
  public boolean isActionViewExpanded() {
    return this.mIsActionViewExpanded;
  }
  
  public MenuItem setContentDescription(CharSequence paramCharSequence) {
    this.mContentDescription = paramCharSequence;
    this.mMenu.onItemsChanged(false);
    return this;
  }
  
  public CharSequence getContentDescription() {
    return this.mContentDescription;
  }
  
  public MenuItem setTooltipText(CharSequence paramCharSequence) {
    this.mTooltipText = paramCharSequence;
    this.mMenu.onItemsChanged(false);
    return this;
  }
  
  public CharSequence getTooltipText() {
    return this.mTooltipText;
  }
}
