package com.android.internal.view.menu;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import java.util.ArrayList;

public class ListMenuPresenter implements MenuPresenter, AdapterView.OnItemClickListener {
  private static final String TAG = "ListMenuPresenter";
  
  public static final String VIEWS_TAG = "android:menu:list";
  
  MenuAdapter mAdapter;
  
  private MenuPresenter.Callback mCallback;
  
  Context mContext;
  
  private int mId;
  
  LayoutInflater mInflater;
  
  private int mItemIndexOffset;
  
  int mItemLayoutRes;
  
  MenuBuilder mMenu;
  
  ExpandedMenuView mMenuView;
  
  int mThemeRes;
  
  public ListMenuPresenter(Context paramContext, int paramInt) {
    this(paramInt, 0);
    this.mContext = paramContext;
    this.mInflater = LayoutInflater.from(paramContext);
  }
  
  public ListMenuPresenter(int paramInt1, int paramInt2) {
    this.mItemLayoutRes = paramInt1;
    this.mThemeRes = paramInt2;
  }
  
  public void initForMenu(Context paramContext, MenuBuilder paramMenuBuilder) {
    ContextThemeWrapper contextThemeWrapper;
    if (this.mThemeRes != 0) {
      this.mContext = (Context)(contextThemeWrapper = new ContextThemeWrapper(paramContext, this.mThemeRes));
      this.mInflater = LayoutInflater.from((Context)contextThemeWrapper);
    } else if (this.mContext != null) {
      this.mContext = (Context)contextThemeWrapper;
      if (this.mInflater == null)
        this.mInflater = LayoutInflater.from((Context)contextThemeWrapper); 
    } 
    this.mMenu = paramMenuBuilder;
    MenuAdapter menuAdapter = this.mAdapter;
    if (menuAdapter != null)
      menuAdapter.notifyDataSetChanged(); 
  }
  
  public MenuView getMenuView(ViewGroup paramViewGroup) {
    if (this.mMenuView == null) {
      this.mMenuView = (ExpandedMenuView)this.mInflater.inflate(17367157, paramViewGroup, false);
      if (this.mAdapter == null)
        this.mAdapter = new MenuAdapter(); 
      this.mMenuView.setAdapter((ListAdapter)this.mAdapter);
      this.mMenuView.setOnItemClickListener(this);
    } 
    return this.mMenuView;
  }
  
  public ListAdapter getAdapter() {
    if (this.mAdapter == null)
      this.mAdapter = new MenuAdapter(); 
    return (ListAdapter)this.mAdapter;
  }
  
  public void updateMenuView(boolean paramBoolean) {
    MenuAdapter menuAdapter = this.mAdapter;
    if (menuAdapter != null)
      menuAdapter.notifyDataSetChanged(); 
  }
  
  public void setCallback(MenuPresenter.Callback paramCallback) {
    this.mCallback = paramCallback;
  }
  
  public boolean onSubMenuSelected(SubMenuBuilder paramSubMenuBuilder) {
    if (!paramSubMenuBuilder.hasVisibleItems())
      return false; 
    (new MenuDialogHelper(paramSubMenuBuilder)).show(null);
    MenuPresenter.Callback callback = this.mCallback;
    if (callback != null)
      callback.onOpenSubMenu(paramSubMenuBuilder); 
    return true;
  }
  
  public void onCloseMenu(MenuBuilder paramMenuBuilder, boolean paramBoolean) {
    MenuPresenter.Callback callback = this.mCallback;
    if (callback != null)
      callback.onCloseMenu(paramMenuBuilder, paramBoolean); 
  }
  
  int getItemIndexOffset() {
    return this.mItemIndexOffset;
  }
  
  public void setItemIndexOffset(int paramInt) {
    this.mItemIndexOffset = paramInt;
    if (this.mMenuView != null)
      updateMenuView(false); 
  }
  
  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong) {
    this.mMenu.performItemAction(this.mAdapter.getItem(paramInt), this, 0);
  }
  
  public boolean flagActionItems() {
    return false;
  }
  
  public boolean expandItemActionView(MenuBuilder paramMenuBuilder, MenuItemImpl paramMenuItemImpl) {
    return false;
  }
  
  public boolean collapseItemActionView(MenuBuilder paramMenuBuilder, MenuItemImpl paramMenuItemImpl) {
    return false;
  }
  
  public void saveHierarchyState(Bundle paramBundle) {
    SparseArray sparseArray = new SparseArray();
    ExpandedMenuView expandedMenuView = this.mMenuView;
    if (expandedMenuView != null)
      expandedMenuView.saveHierarchyState(sparseArray); 
    paramBundle.putSparseParcelableArray("android:menu:list", sparseArray);
  }
  
  public void restoreHierarchyState(Bundle paramBundle) {
    SparseArray sparseArray = paramBundle.getSparseParcelableArray("android:menu:list");
    if (sparseArray != null)
      this.mMenuView.restoreHierarchyState(sparseArray); 
  }
  
  public void setId(int paramInt) {
    this.mId = paramInt;
  }
  
  public int getId() {
    return this.mId;
  }
  
  public Parcelable onSaveInstanceState() {
    if (this.mMenuView == null)
      return null; 
    Bundle bundle = new Bundle();
    saveHierarchyState(bundle);
    return (Parcelable)bundle;
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    restoreHierarchyState((Bundle)paramParcelable);
  }
  
  class MenuAdapter extends BaseAdapter {
    private int mExpandedIndex = -1;
    
    final ListMenuPresenter this$0;
    
    public MenuAdapter() {
      findExpandedIndex();
    }
    
    public int getCount() {
      ArrayList<MenuItemImpl> arrayList = ListMenuPresenter.this.mMenu.getNonActionItems();
      int i = arrayList.size() - ListMenuPresenter.this.mItemIndexOffset;
      if (this.mExpandedIndex < 0)
        return i; 
      return i - 1;
    }
    
    public MenuItemImpl getItem(int param1Int) {
      ArrayList<MenuItemImpl> arrayList = ListMenuPresenter.this.mMenu.getNonActionItems();
      int i = param1Int + ListMenuPresenter.this.mItemIndexOffset;
      int j = this.mExpandedIndex;
      param1Int = i;
      if (j >= 0) {
        param1Int = i;
        if (i >= j)
          param1Int = i + 1; 
      } 
      return arrayList.get(param1Int);
    }
    
    public long getItemId(int param1Int) {
      return param1Int;
    }
    
    public View getView(int param1Int, View param1View, ViewGroup param1ViewGroup) {
      View view = param1View;
      if (param1View == null)
        view = ListMenuPresenter.this.mInflater.inflate(ListMenuPresenter.this.mItemLayoutRes, param1ViewGroup, false); 
      MenuView.ItemView itemView = (MenuView.ItemView)view;
      itemView.initialize(getItem(param1Int), 0);
      return view;
    }
    
    void findExpandedIndex() {
      MenuItemImpl menuItemImpl = ListMenuPresenter.this.mMenu.getExpandedItem();
      if (menuItemImpl != null) {
        ArrayList<MenuItemImpl> arrayList = ListMenuPresenter.this.mMenu.getNonActionItems();
        int i = arrayList.size();
        for (byte b = 0; b < i; b++) {
          MenuItemImpl menuItemImpl1 = arrayList.get(b);
          if (menuItemImpl1 == menuItemImpl) {
            this.mExpandedIndex = b;
            return;
          } 
        } 
      } 
      this.mExpandedIndex = -1;
    }
    
    public void notifyDataSetChanged() {
      findExpandedIndex();
      super.notifyDataSetChanged();
    }
  }
}
