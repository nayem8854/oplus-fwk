package com.android.internal.view.menu;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class MenuBuilder implements Menu {
  private static final int[] sCategoryToOrder = new int[] { 1, 4, 5, 3, 2, 0 };
  
  private int mDefaultShowAsAction = 0;
  
  private boolean mPreventDispatchingItemsChanged = false;
  
  private boolean mItemsChangedWhileDispatchPrevented = false;
  
  private boolean mOptionalIconsVisible = false;
  
  private boolean mIsClosing = false;
  
  private ArrayList<MenuItemImpl> mTempShortcutItemList = new ArrayList<>();
  
  private CopyOnWriteArrayList<WeakReference<MenuPresenter>> mPresenters = new CopyOnWriteArrayList<>();
  
  private boolean mGroupDividerEnabled = false;
  
  private static final String ACTION_VIEW_STATES_KEY = "android:menu:actionviewstates";
  
  private static final String EXPANDED_ACTION_VIEW_ID = "android:menu:expandedactionview";
  
  private static final String PRESENTER_KEY = "android:menu:presenters";
  
  private static final String TAG = "MenuBuilder";
  
  private ArrayList<MenuItemImpl> mActionItems;
  
  private Callback mCallback;
  
  private final Context mContext;
  
  private ContextMenu.ContextMenuInfo mCurrentMenuInfo;
  
  private MenuItemImpl mExpandedItem;
  
  private SparseArray<Parcelable> mFrozenViewStates;
  
  Drawable mHeaderIcon;
  
  CharSequence mHeaderTitle;
  
  View mHeaderView;
  
  private boolean mIsActionItemsStale;
  
  private boolean mIsVisibleItemsStale;
  
  private ArrayList<MenuItemImpl> mItems;
  
  private ArrayList<MenuItemImpl> mNonActionItems;
  
  private boolean mQwertyMode;
  
  private final Resources mResources;
  
  private boolean mShortcutsVisible;
  
  private ArrayList<MenuItemImpl> mVisibleItems;
  
  public MenuBuilder(Context paramContext) {
    this.mContext = paramContext;
    this.mResources = paramContext.getResources();
    this.mItems = new ArrayList<>();
    this.mVisibleItems = new ArrayList<>();
    this.mIsVisibleItemsStale = true;
    this.mActionItems = new ArrayList<>();
    this.mNonActionItems = new ArrayList<>();
    this.mIsActionItemsStale = true;
    setShortcutsVisibleInner(true);
  }
  
  public MenuBuilder setDefaultShowAsAction(int paramInt) {
    this.mDefaultShowAsAction = paramInt;
    return this;
  }
  
  public void addMenuPresenter(MenuPresenter paramMenuPresenter) {
    addMenuPresenter(paramMenuPresenter, this.mContext);
  }
  
  public void addMenuPresenter(MenuPresenter paramMenuPresenter, Context paramContext) {
    this.mPresenters.add(new WeakReference<>(paramMenuPresenter));
    paramMenuPresenter.initForMenu(paramContext, this);
    this.mIsActionItemsStale = true;
  }
  
  public void removeMenuPresenter(MenuPresenter paramMenuPresenter) {
    for (WeakReference<MenuPresenter> weakReference : this.mPresenters) {
      MenuPresenter menuPresenter = weakReference.get();
      if (menuPresenter == null || menuPresenter == paramMenuPresenter)
        this.mPresenters.remove(weakReference); 
    } 
  }
  
  private void dispatchPresenterUpdate(boolean paramBoolean) {
    if (this.mPresenters.isEmpty())
      return; 
    stopDispatchingItemsChanged();
    for (WeakReference<MenuPresenter> weakReference : this.mPresenters) {
      MenuPresenter menuPresenter = weakReference.get();
      if (menuPresenter == null) {
        this.mPresenters.remove(weakReference);
        continue;
      } 
      menuPresenter.updateMenuView(paramBoolean);
    } 
    startDispatchingItemsChanged();
  }
  
  private boolean dispatchSubMenuSelected(SubMenuBuilder paramSubMenuBuilder, MenuPresenter paramMenuPresenter) {
    if (this.mPresenters.isEmpty())
      return false; 
    boolean bool1 = false;
    if (paramMenuPresenter != null)
      bool1 = paramMenuPresenter.onSubMenuSelected(paramSubMenuBuilder); 
    boolean bool2;
    for (Iterator<WeakReference<MenuPresenter>> iterator = this.mPresenters.iterator(); iterator.hasNext(); ) {
      WeakReference<MenuPresenter> weakReference = iterator.next();
      paramMenuPresenter = weakReference.get();
      if (paramMenuPresenter == null) {
        this.mPresenters.remove(weakReference);
        bool1 = bool2;
      } else {
        bool1 = bool2;
        if (!bool2)
          bool1 = paramMenuPresenter.onSubMenuSelected(paramSubMenuBuilder); 
      } 
      bool2 = bool1;
    } 
    return bool2;
  }
  
  private void dispatchSaveInstanceState(Bundle paramBundle) {
    if (this.mPresenters.isEmpty())
      return; 
    SparseArray sparseArray = new SparseArray();
    for (WeakReference<MenuPresenter> weakReference : this.mPresenters) {
      MenuPresenter menuPresenter = weakReference.get();
      if (menuPresenter == null) {
        this.mPresenters.remove(weakReference);
        continue;
      } 
      int i = menuPresenter.getId();
      if (i > 0) {
        Parcelable parcelable = menuPresenter.onSaveInstanceState();
        if (parcelable != null)
          sparseArray.put(i, parcelable); 
      } 
    } 
    paramBundle.putSparseParcelableArray("android:menu:presenters", sparseArray);
  }
  
  private void dispatchRestoreInstanceState(Bundle paramBundle) {
    SparseArray sparseArray = paramBundle.getSparseParcelableArray("android:menu:presenters");
    if (sparseArray == null || this.mPresenters.isEmpty())
      return; 
    for (WeakReference<MenuPresenter> weakReference : this.mPresenters) {
      MenuPresenter menuPresenter = weakReference.get();
      if (menuPresenter == null) {
        this.mPresenters.remove(weakReference);
        continue;
      } 
      int i = menuPresenter.getId();
      if (i > 0) {
        Parcelable parcelable = (Parcelable)sparseArray.get(i);
        if (parcelable != null)
          menuPresenter.onRestoreInstanceState(parcelable); 
      } 
    } 
  }
  
  public void savePresenterStates(Bundle paramBundle) {
    dispatchSaveInstanceState(paramBundle);
  }
  
  public void restorePresenterStates(Bundle paramBundle) {
    dispatchRestoreInstanceState(paramBundle);
  }
  
  public void saveActionViewStates(Bundle paramBundle) {
    SparseArray sparseArray = null;
    int i = size();
    for (byte b = 0; b < i; b++, sparseArray = sparseArray1) {
      MenuItem menuItem = getItem(b);
      View view = menuItem.getActionView();
      SparseArray sparseArray1 = sparseArray;
      if (view != null) {
        sparseArray1 = sparseArray;
        if (view.getId() != -1) {
          SparseArray sparseArray2 = sparseArray;
          if (sparseArray == null)
            sparseArray2 = new SparseArray(); 
          view.saveHierarchyState(sparseArray2);
          sparseArray1 = sparseArray2;
          if (menuItem.isActionViewExpanded()) {
            paramBundle.putInt("android:menu:expandedactionview", menuItem.getItemId());
            sparseArray1 = sparseArray2;
          } 
        } 
      } 
      if (menuItem.hasSubMenu()) {
        SubMenuBuilder subMenuBuilder = (SubMenuBuilder)menuItem.getSubMenu();
        subMenuBuilder.saveActionViewStates(paramBundle);
      } 
    } 
    if (sparseArray != null)
      paramBundle.putSparseParcelableArray(getActionViewStatesKey(), sparseArray); 
  }
  
  public void restoreActionViewStates(Bundle paramBundle) {
    if (paramBundle == null)
      return; 
    String str = getActionViewStatesKey();
    SparseArray sparseArray = paramBundle.getSparseParcelableArray(str);
    int i = size();
    int j;
    for (j = 0; j < i; j++) {
      MenuItem menuItem = getItem(j);
      View view = menuItem.getActionView();
      if (view != null && view.getId() != -1)
        view.restoreHierarchyState(sparseArray); 
      if (menuItem.hasSubMenu()) {
        SubMenuBuilder subMenuBuilder = (SubMenuBuilder)menuItem.getSubMenu();
        subMenuBuilder.restoreActionViewStates(paramBundle);
      } 
    } 
    j = paramBundle.getInt("android:menu:expandedactionview");
    if (j > 0) {
      MenuItem menuItem = findItem(j);
      if (menuItem != null)
        menuItem.expandActionView(); 
    } 
  }
  
  protected String getActionViewStatesKey() {
    return "android:menu:actionviewstates";
  }
  
  public void setCallback(Callback paramCallback) {
    this.mCallback = paramCallback;
  }
  
  private MenuItem addInternal(int paramInt1, int paramInt2, int paramInt3, CharSequence paramCharSequence) {
    int i = getOrdering(paramInt3);
    MenuItemImpl menuItemImpl = createNewMenuItem(paramInt1, paramInt2, paramInt3, i, paramCharSequence, this.mDefaultShowAsAction);
    ContextMenu.ContextMenuInfo contextMenuInfo = this.mCurrentMenuInfo;
    if (contextMenuInfo != null)
      menuItemImpl.setMenuInfo(contextMenuInfo); 
    ArrayList<MenuItemImpl> arrayList = this.mItems;
    arrayList.add(findInsertIndex(arrayList, i), menuItemImpl);
    onItemsChanged(true);
    return menuItemImpl;
  }
  
  private MenuItemImpl createNewMenuItem(int paramInt1, int paramInt2, int paramInt3, int paramInt4, CharSequence paramCharSequence, int paramInt5) {
    return new MenuItemImpl(this, paramInt1, paramInt2, paramInt3, paramInt4, paramCharSequence, paramInt5);
  }
  
  public MenuItem add(CharSequence paramCharSequence) {
    return addInternal(0, 0, 0, paramCharSequence);
  }
  
  public MenuItem add(int paramInt) {
    return addInternal(0, 0, 0, this.mResources.getString(paramInt));
  }
  
  public MenuItem add(int paramInt1, int paramInt2, int paramInt3, CharSequence paramCharSequence) {
    return addInternal(paramInt1, paramInt2, paramInt3, paramCharSequence);
  }
  
  public MenuItem add(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    return addInternal(paramInt1, paramInt2, paramInt3, this.mResources.getString(paramInt4));
  }
  
  public SubMenu addSubMenu(CharSequence paramCharSequence) {
    return addSubMenu(0, 0, 0, paramCharSequence);
  }
  
  public SubMenu addSubMenu(int paramInt) {
    return addSubMenu(0, 0, 0, this.mResources.getString(paramInt));
  }
  
  public SubMenu addSubMenu(int paramInt1, int paramInt2, int paramInt3, CharSequence paramCharSequence) {
    MenuItemImpl menuItemImpl = (MenuItemImpl)addInternal(paramInt1, paramInt2, paramInt3, paramCharSequence);
    SubMenuBuilder subMenuBuilder = new SubMenuBuilder(this.mContext, this, menuItemImpl);
    menuItemImpl.setSubMenu(subMenuBuilder);
    return subMenuBuilder;
  }
  
  public SubMenu addSubMenu(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    return addSubMenu(paramInt1, paramInt2, paramInt3, this.mResources.getString(paramInt4));
  }
  
  public void setGroupDividerEnabled(boolean paramBoolean) {
    this.mGroupDividerEnabled = paramBoolean;
  }
  
  public boolean isGroupDividerEnabled() {
    return this.mGroupDividerEnabled;
  }
  
  public int addIntentOptions(int paramInt1, int paramInt2, int paramInt3, ComponentName paramComponentName, Intent[] paramArrayOfIntent, Intent paramIntent, int paramInt4, MenuItem[] paramArrayOfMenuItem) {
    PackageManager packageManager = this.mContext.getPackageManager();
    int i = 0;
    List<ResolveInfo> list = packageManager.queryIntentActivityOptions(paramComponentName, paramArrayOfIntent, paramIntent, 0);
    if (list != null)
      i = list.size(); 
    if ((paramInt4 & 0x1) == 0)
      removeGroup(paramInt1); 
    for (paramInt4 = 0; paramInt4 < i; paramInt4++) {
      ResolveInfo resolveInfo = list.get(paramInt4);
      if (resolveInfo.specificIndex < 0) {
        intent = paramIntent;
      } else {
        intent = paramArrayOfIntent[resolveInfo.specificIndex];
      } 
      Intent intent = new Intent(intent);
      intent.setComponent(new ComponentName(resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name));
      MenuItem menuItem2 = add(paramInt1, paramInt2, paramInt3, resolveInfo.loadLabel(packageManager));
      menuItem2 = menuItem2.setIcon(resolveInfo.loadIcon(packageManager));
      MenuItem menuItem1 = menuItem2.setIntent(intent);
      if (paramArrayOfMenuItem != null && resolveInfo.specificIndex >= 0)
        paramArrayOfMenuItem[resolveInfo.specificIndex] = menuItem1; 
    } 
    return i;
  }
  
  public void removeItem(int paramInt) {
    removeItemAtInt(findItemIndex(paramInt), true);
  }
  
  public void removeGroup(int paramInt) {
    int i = findGroupIndex(paramInt);
    if (i >= 0) {
      int j = this.mItems.size();
      byte b = 0;
      while (b < j - i && ((MenuItemImpl)this.mItems.get(i)).getGroupId() == paramInt) {
        removeItemAtInt(i, false);
        b++;
      } 
      onItemsChanged(true);
    } 
  }
  
  private void removeItemAtInt(int paramInt, boolean paramBoolean) {
    if (paramInt < 0 || paramInt >= this.mItems.size())
      return; 
    this.mItems.remove(paramInt);
    if (paramBoolean)
      onItemsChanged(true); 
  }
  
  public void removeItemAt(int paramInt) {
    removeItemAtInt(paramInt, true);
  }
  
  public void clearAll() {
    this.mPreventDispatchingItemsChanged = true;
    clear();
    clearHeader();
    this.mPresenters.clear();
    this.mPreventDispatchingItemsChanged = false;
    this.mItemsChangedWhileDispatchPrevented = false;
    onItemsChanged(true);
  }
  
  public void clear() {
    MenuItemImpl menuItemImpl = this.mExpandedItem;
    if (menuItemImpl != null)
      collapseItemActionView(menuItemImpl); 
    this.mItems.clear();
    onItemsChanged(true);
  }
  
  void setExclusiveItemChecked(MenuItem paramMenuItem) {
    int i = paramMenuItem.getGroupId();
    int j = this.mItems.size();
    for (byte b = 0; b < j; b++) {
      MenuItemImpl menuItemImpl = this.mItems.get(b);
      if (menuItemImpl.getGroupId() == i && 
        menuItemImpl.isExclusiveCheckable() && 
        menuItemImpl.isCheckable()) {
        boolean bool;
        if (menuItemImpl == paramMenuItem) {
          bool = true;
        } else {
          bool = false;
        } 
        menuItemImpl.setCheckedInt(bool);
      } 
    } 
  }
  
  public void setGroupCheckable(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    int i = this.mItems.size();
    for (byte b = 0; b < i; b++) {
      MenuItemImpl menuItemImpl = this.mItems.get(b);
      if (menuItemImpl.getGroupId() == paramInt) {
        menuItemImpl.setExclusiveCheckable(paramBoolean2);
        menuItemImpl.setCheckable(paramBoolean1);
      } 
    } 
  }
  
  public void setGroupVisible(int paramInt, boolean paramBoolean) {
    int i = this.mItems.size();
    boolean bool = false;
    for (byte b = 0; b < i; b++, bool = bool1) {
      MenuItemImpl menuItemImpl = this.mItems.get(b);
      boolean bool1 = bool;
      if (menuItemImpl.getGroupId() == paramInt) {
        bool1 = bool;
        if (menuItemImpl.setVisibleInt(paramBoolean))
          bool1 = true; 
      } 
    } 
    if (bool)
      onItemsChanged(true); 
  }
  
  public void setGroupEnabled(int paramInt, boolean paramBoolean) {
    int i = this.mItems.size();
    for (byte b = 0; b < i; b++) {
      MenuItemImpl menuItemImpl = this.mItems.get(b);
      if (menuItemImpl.getGroupId() == paramInt)
        menuItemImpl.setEnabled(paramBoolean); 
    } 
  }
  
  public boolean hasVisibleItems() {
    int i = size();
    for (byte b = 0; b < i; b++) {
      MenuItemImpl menuItemImpl = this.mItems.get(b);
      if (menuItemImpl.isVisible())
        return true; 
    } 
    return false;
  }
  
  public MenuItem findItem(int paramInt) {
    int i = size();
    for (byte b = 0; b < i; b++) {
      MenuItemImpl menuItemImpl = this.mItems.get(b);
      if (menuItemImpl.getItemId() == paramInt)
        return menuItemImpl; 
      if (menuItemImpl.hasSubMenu()) {
        MenuItem menuItem = menuItemImpl.getSubMenu().findItem(paramInt);
        if (menuItem != null)
          return menuItem; 
      } 
    } 
    return null;
  }
  
  public int findItemIndex(int paramInt) {
    int i = size();
    for (byte b = 0; b < i; b++) {
      MenuItemImpl menuItemImpl = this.mItems.get(b);
      if (menuItemImpl.getItemId() == paramInt)
        return b; 
    } 
    return -1;
  }
  
  public int findGroupIndex(int paramInt) {
    return findGroupIndex(paramInt, 0);
  }
  
  public int findGroupIndex(int paramInt1, int paramInt2) {
    int i = size();
    int j = paramInt2;
    if (paramInt2 < 0)
      j = 0; 
    for (; j < i; j++) {
      MenuItemImpl menuItemImpl = this.mItems.get(j);
      if (menuItemImpl.getGroupId() == paramInt1)
        return j; 
    } 
    return -1;
  }
  
  public int size() {
    return this.mItems.size();
  }
  
  public MenuItem getItem(int paramInt) {
    return this.mItems.get(paramInt);
  }
  
  public boolean isShortcutKey(int paramInt, KeyEvent paramKeyEvent) {
    boolean bool;
    if (findItemWithShortcutForKey(paramInt, paramKeyEvent) != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setQwertyMode(boolean paramBoolean) {
    this.mQwertyMode = paramBoolean;
    onItemsChanged(false);
  }
  
  private static int getOrdering(int paramInt) {
    int i = (0xFFFF0000 & paramInt) >> 16;
    if (i >= 0) {
      int[] arrayOfInt = sCategoryToOrder;
      if (i < arrayOfInt.length)
        return arrayOfInt[i] << 16 | 0xFFFF & paramInt; 
    } 
    throw new IllegalArgumentException("order does not contain a valid category.");
  }
  
  boolean isQwertyMode() {
    return this.mQwertyMode;
  }
  
  public void setShortcutsVisible(boolean paramBoolean) {
    if (this.mShortcutsVisible == paramBoolean)
      return; 
    setShortcutsVisibleInner(paramBoolean);
    onItemsChanged(false);
  }
  
  private void setShortcutsVisibleInner(boolean paramBoolean) {
    // Byte code:
    //   0: iconst_1
    //   1: istore_2
    //   2: iload_1
    //   3: ifeq -> 42
    //   6: aload_0
    //   7: getfield mResources : Landroid/content/res/Resources;
    //   10: astore_3
    //   11: aload_3
    //   12: invokevirtual getConfiguration : ()Landroid/content/res/Configuration;
    //   15: getfield keyboard : I
    //   18: iconst_1
    //   19: if_icmpeq -> 42
    //   22: aload_0
    //   23: getfield mContext : Landroid/content/Context;
    //   26: astore_3
    //   27: aload_3
    //   28: invokestatic get : (Landroid/content/Context;)Landroid/view/ViewConfiguration;
    //   31: invokevirtual shouldShowMenuShortcutsWhenKeyboardPresent : ()Z
    //   34: ifeq -> 42
    //   37: iload_2
    //   38: istore_1
    //   39: goto -> 44
    //   42: iconst_0
    //   43: istore_1
    //   44: aload_0
    //   45: iload_1
    //   46: putfield mShortcutsVisible : Z
    //   49: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #765	-> 0
    //   #766	-> 11
    //   #767	-> 27
    //   #768	-> 49
  }
  
  public boolean isShortcutsVisible() {
    return this.mShortcutsVisible;
  }
  
  Resources getResources() {
    return this.mResources;
  }
  
  public Context getContext() {
    return this.mContext;
  }
  
  boolean dispatchMenuItemSelected(MenuBuilder paramMenuBuilder, MenuItem paramMenuItem) {
    boolean bool;
    Callback callback = this.mCallback;
    if (callback != null && callback.onMenuItemSelected(paramMenuBuilder, paramMenuItem)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void changeMenuMode() {
    Callback callback = this.mCallback;
    if (callback != null)
      callback.onMenuModeChange(this); 
  }
  
  private static int findInsertIndex(ArrayList<MenuItemImpl> paramArrayList, int paramInt) {
    for (int i = paramArrayList.size() - 1; i >= 0; i--) {
      MenuItemImpl menuItemImpl = paramArrayList.get(i);
      if (menuItemImpl.getOrdering() <= paramInt)
        return i + 1; 
    } 
    return 0;
  }
  
  public boolean performShortcut(int paramInt1, KeyEvent paramKeyEvent, int paramInt2) {
    MenuItemImpl menuItemImpl = findItemWithShortcutForKey(paramInt1, paramKeyEvent);
    boolean bool = false;
    if (menuItemImpl != null)
      bool = performItemAction(menuItemImpl, paramInt2); 
    if ((paramInt2 & 0x2) != 0)
      close(true); 
    return bool;
  }
  
  void findItemsWithShortcutForKey(List<MenuItemImpl> paramList, int paramInt, KeyEvent paramKeyEvent) {
    boolean bool1 = isQwertyMode();
    int i = paramKeyEvent.getModifiers();
    KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
    boolean bool2 = paramKeyEvent.getKeyData(keyData);
    if (!bool2 && paramInt != 67)
      return; 
    int j = this.mItems.size();
    for (byte b = 0; b < j; b++) {
      char c;
      int k;
      MenuItemImpl menuItemImpl = this.mItems.get(b);
      if (menuItemImpl.hasSubMenu())
        ((MenuBuilder)menuItemImpl.getSubMenu()).findItemsWithShortcutForKey(paramList, paramInt, paramKeyEvent); 
      if (bool1) {
        c = menuItemImpl.getAlphabeticShortcut();
      } else {
        c = menuItemImpl.getNumericShortcut();
      } 
      if (bool1) {
        k = menuItemImpl.getAlphabeticModifiers();
      } else {
        k = menuItemImpl.getNumericModifiers();
      } 
      if ((i & 0x1100F) == (0x1100F & k)) {
        k = 1;
      } else {
        k = 0;
      } 
      if (k != 0 && c != '\000' && (c == keyData.meta[0] || c == keyData.meta[2] || (bool1 && c == '\b' && paramInt == 67)))
        if (menuItemImpl.isEnabled())
          paramList.add(menuItemImpl);  
    } 
  }
  
  MenuItemImpl findItemWithShortcutForKey(int paramInt, KeyEvent paramKeyEvent) {
    ArrayList<MenuItemImpl> arrayList = this.mTempShortcutItemList;
    arrayList.clear();
    findItemsWithShortcutForKey(arrayList, paramInt, paramKeyEvent);
    if (arrayList.isEmpty())
      return null; 
    int i = paramKeyEvent.getMetaState();
    KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
    paramKeyEvent.getKeyData(keyData);
    int j = arrayList.size();
    if (j == 1)
      return arrayList.get(0); 
    boolean bool = isQwertyMode();
    for (byte b = 0; b < j; b++) {
      char c;
      MenuItemImpl menuItemImpl = arrayList.get(b);
      if (bool) {
        c = menuItemImpl.getAlphabeticShortcut();
      } else {
        c = menuItemImpl.getNumericShortcut();
      } 
      if ((c == keyData.meta[0] && (i & 0x2) == 0) || (c == keyData.meta[2] && (i & 0x2) != 0) || (bool && c == '\b' && paramInt == 67))
        return menuItemImpl; 
    } 
    return null;
  }
  
  public boolean performIdentifierAction(int paramInt1, int paramInt2) {
    return performItemAction(findItem(paramInt1), paramInt2);
  }
  
  public boolean performItemAction(MenuItem paramMenuItem, int paramInt) {
    return performItemAction(paramMenuItem, null, paramInt);
  }
  
  public boolean performItemAction(MenuItem paramMenuItem, MenuPresenter paramMenuPresenter, int paramInt) {
    boolean bool4;
    MenuItemImpl menuItemImpl = (MenuItemImpl)paramMenuItem;
    boolean bool1 = false;
    if (menuItemImpl == null || !menuItemImpl.isEnabled())
      return false; 
    boolean bool2 = menuItemImpl.invoke();
    ActionProvider actionProvider = paramMenuItem.getActionProvider();
    boolean bool3 = bool1;
    if (actionProvider != null) {
      bool3 = bool1;
      if (actionProvider.hasSubMenu())
        bool3 = true; 
    } 
    if (menuItemImpl.hasCollapsibleActionView()) {
      bool2 |= menuItemImpl.expandActionView();
      bool4 = bool2;
      if (bool2) {
        close(true);
        bool4 = bool2;
      } 
    } else {
      if (menuItemImpl.hasSubMenu() || bool3) {
        if (!menuItemImpl.hasSubMenu())
          menuItemImpl.setSubMenu(new SubMenuBuilder(getContext(), this, menuItemImpl)); 
        SubMenuBuilder subMenuBuilder = (SubMenuBuilder)menuItemImpl.getSubMenu();
        if (bool3)
          actionProvider.onPrepareSubMenu(subMenuBuilder); 
        boolean bool = bool2 | dispatchSubMenuSelected(subMenuBuilder, paramMenuPresenter);
        if (!bool)
          close(true); 
        return bool;
      } 
      bool4 = bool2;
      if ((paramInt & 0x1) == 0) {
        close(true);
        bool4 = bool2;
      } 
    } 
    return bool4;
  }
  
  public final void close(boolean paramBoolean) {
    if (this.mIsClosing)
      return; 
    this.mIsClosing = true;
    for (WeakReference<MenuPresenter> weakReference : this.mPresenters) {
      MenuPresenter menuPresenter = weakReference.get();
      if (menuPresenter == null) {
        this.mPresenters.remove(weakReference);
        continue;
      } 
      menuPresenter.onCloseMenu(this, paramBoolean);
    } 
    this.mIsClosing = false;
  }
  
  public void close() {
    close(true);
  }
  
  public void onItemsChanged(boolean paramBoolean) {
    if (!this.mPreventDispatchingItemsChanged) {
      if (paramBoolean) {
        this.mIsVisibleItemsStale = true;
        this.mIsActionItemsStale = true;
      } 
      dispatchPresenterUpdate(paramBoolean);
    } else {
      this.mItemsChangedWhileDispatchPrevented = true;
    } 
  }
  
  public void stopDispatchingItemsChanged() {
    if (!this.mPreventDispatchingItemsChanged) {
      this.mPreventDispatchingItemsChanged = true;
      this.mItemsChangedWhileDispatchPrevented = false;
    } 
  }
  
  public void startDispatchingItemsChanged() {
    this.mPreventDispatchingItemsChanged = false;
    if (this.mItemsChangedWhileDispatchPrevented) {
      this.mItemsChangedWhileDispatchPrevented = false;
      onItemsChanged(true);
    } 
  }
  
  void onItemVisibleChanged(MenuItemImpl paramMenuItemImpl) {
    this.mIsVisibleItemsStale = true;
    onItemsChanged(true);
  }
  
  void onItemActionRequestChanged(MenuItemImpl paramMenuItemImpl) {
    this.mIsActionItemsStale = true;
    onItemsChanged(true);
  }
  
  public ArrayList<MenuItemImpl> getVisibleItems() {
    if (!this.mIsVisibleItemsStale)
      return this.mVisibleItems; 
    this.mVisibleItems.clear();
    int i = this.mItems.size();
    for (byte b = 0; b < i; b++) {
      MenuItemImpl menuItemImpl = this.mItems.get(b);
      if (menuItemImpl.isVisible())
        this.mVisibleItems.add(menuItemImpl); 
    } 
    this.mIsVisibleItemsStale = false;
    this.mIsActionItemsStale = true;
    return this.mVisibleItems;
  }
  
  public void flagActionItems() {
    ArrayList<MenuItemImpl> arrayList = getVisibleItems();
    if (!this.mIsActionItemsStale)
      return; 
    boolean bool = false;
    for (WeakReference<MenuPresenter> weakReference : this.mPresenters) {
      MenuPresenter menuPresenter = weakReference.get();
      if (menuPresenter == null) {
        this.mPresenters.remove(weakReference);
        continue;
      } 
      bool |= menuPresenter.flagActionItems();
    } 
    if (bool) {
      this.mActionItems.clear();
      this.mNonActionItems.clear();
      int i = arrayList.size();
      for (bool = false; bool < i; bool++) {
        MenuItemImpl menuItemImpl = arrayList.get(bool);
        if (menuItemImpl.isActionButton()) {
          this.mActionItems.add(menuItemImpl);
        } else {
          this.mNonActionItems.add(menuItemImpl);
        } 
      } 
    } else {
      this.mActionItems.clear();
      this.mNonActionItems.clear();
      this.mNonActionItems.addAll(getVisibleItems());
    } 
    this.mIsActionItemsStale = false;
  }
  
  public ArrayList<MenuItemImpl> getActionItems() {
    flagActionItems();
    return this.mActionItems;
  }
  
  public ArrayList<MenuItemImpl> getNonActionItems() {
    flagActionItems();
    return this.mNonActionItems;
  }
  
  public void clearHeader() {
    this.mHeaderIcon = null;
    this.mHeaderTitle = null;
    this.mHeaderView = null;
    onItemsChanged(false);
  }
  
  private void setHeaderInternal(int paramInt1, CharSequence paramCharSequence, int paramInt2, Drawable paramDrawable, View paramView) {
    Resources resources = getResources();
    if (paramView != null) {
      this.mHeaderView = paramView;
      this.mHeaderTitle = null;
      this.mHeaderIcon = null;
    } else {
      if (paramInt1 > 0) {
        this.mHeaderTitle = resources.getText(paramInt1);
      } else if (paramCharSequence != null) {
        this.mHeaderTitle = paramCharSequence;
      } 
      if (paramInt2 > 0) {
        this.mHeaderIcon = getContext().getDrawable(paramInt2);
      } else if (paramDrawable != null) {
        this.mHeaderIcon = paramDrawable;
      } 
      this.mHeaderView = null;
    } 
    onItemsChanged(false);
  }
  
  protected MenuBuilder setHeaderTitleInt(CharSequence paramCharSequence) {
    setHeaderInternal(0, paramCharSequence, 0, null, null);
    return this;
  }
  
  protected MenuBuilder setHeaderTitleInt(int paramInt) {
    setHeaderInternal(paramInt, null, 0, null, null);
    return this;
  }
  
  protected MenuBuilder setHeaderIconInt(Drawable paramDrawable) {
    setHeaderInternal(0, null, 0, paramDrawable, null);
    return this;
  }
  
  protected MenuBuilder setHeaderIconInt(int paramInt) {
    setHeaderInternal(0, null, paramInt, null, null);
    return this;
  }
  
  protected MenuBuilder setHeaderViewInt(View paramView) {
    setHeaderInternal(0, null, 0, null, paramView);
    return this;
  }
  
  public CharSequence getHeaderTitle() {
    return this.mHeaderTitle;
  }
  
  public Drawable getHeaderIcon() {
    return this.mHeaderIcon;
  }
  
  public View getHeaderView() {
    return this.mHeaderView;
  }
  
  public MenuBuilder getRootMenu() {
    return this;
  }
  
  public void setCurrentMenuInfo(ContextMenu.ContextMenuInfo paramContextMenuInfo) {
    this.mCurrentMenuInfo = paramContextMenuInfo;
  }
  
  void setOptionalIconsVisible(boolean paramBoolean) {
    this.mOptionalIconsVisible = paramBoolean;
  }
  
  boolean getOptionalIconsVisible() {
    return this.mOptionalIconsVisible;
  }
  
  public boolean expandItemActionView(MenuItemImpl paramMenuItemImpl) {
    boolean bool2;
    if (this.mPresenters.isEmpty())
      return false; 
    boolean bool1 = false;
    stopDispatchingItemsChanged();
    Iterator<WeakReference<MenuPresenter>> iterator = this.mPresenters.iterator();
    while (true) {
      bool2 = bool1;
      if (iterator.hasNext()) {
        WeakReference<MenuPresenter> weakReference = iterator.next();
        MenuPresenter menuPresenter = weakReference.get();
        if (menuPresenter == null) {
          this.mPresenters.remove(weakReference);
          bool2 = bool1;
        } else {
          boolean bool = menuPresenter.expandItemActionView(this, paramMenuItemImpl);
          if (bool) {
            bool2 = bool1;
            break;
          } 
        } 
        bool1 = bool2;
        continue;
      } 
      break;
    } 
    startDispatchingItemsChanged();
    if (bool2)
      this.mExpandedItem = paramMenuItemImpl; 
    return bool2;
  }
  
  public boolean collapseItemActionView(MenuItemImpl paramMenuItemImpl) {
    boolean bool2;
    if (this.mPresenters.isEmpty() || this.mExpandedItem != paramMenuItemImpl)
      return false; 
    boolean bool1 = false;
    stopDispatchingItemsChanged();
    Iterator<WeakReference<MenuPresenter>> iterator = this.mPresenters.iterator();
    while (true) {
      bool2 = bool1;
      if (iterator.hasNext()) {
        WeakReference<MenuPresenter> weakReference = iterator.next();
        MenuPresenter menuPresenter = weakReference.get();
        if (menuPresenter == null) {
          this.mPresenters.remove(weakReference);
          bool2 = bool1;
        } else {
          boolean bool = menuPresenter.collapseItemActionView(this, paramMenuItemImpl);
          if (bool) {
            bool2 = bool1;
            break;
          } 
        } 
        bool1 = bool2;
        continue;
      } 
      break;
    } 
    startDispatchingItemsChanged();
    if (bool2)
      this.mExpandedItem = null; 
    return bool2;
  }
  
  public MenuItemImpl getExpandedItem() {
    return this.mExpandedItem;
  }
  
  class Callback {
    public abstract boolean onMenuItemSelected(MenuBuilder param1MenuBuilder, MenuItem param1MenuItem);
    
    public abstract void onMenuModeChange(MenuBuilder param1MenuBuilder);
  }
  
  class ItemInvoker {
    public abstract boolean invokeItem(MenuItemImpl param1MenuItemImpl);
  }
}
