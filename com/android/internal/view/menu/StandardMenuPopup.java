package com.android.internal.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcelable;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.MenuPopupWindow;
import android.widget.PopupWindow;
import android.widget.TextView;
import java.util.Objects;

final class StandardMenuPopup extends MenuPopup implements PopupWindow.OnDismissListener, AdapterView.OnItemClickListener, MenuPresenter, View.OnKeyListener {
  private final ViewTreeObserver.OnGlobalLayoutListener mGlobalLayoutListener = (ViewTreeObserver.OnGlobalLayoutListener)new Object(this);
  
  private final View.OnAttachStateChangeListener mAttachStateChangeListener = (View.OnAttachStateChangeListener)new Object(this);
  
  private int mDropDownGravity = 0;
  
  private static final int ITEM_LAYOUT = 17367235;
  
  private final MenuAdapter mAdapter;
  
  private View mAnchorView;
  
  private int mContentWidth;
  
  private final Context mContext;
  
  private boolean mHasContentWidth;
  
  private final MenuBuilder mMenu;
  
  private PopupWindow.OnDismissListener mOnDismissListener;
  
  private final boolean mOverflowOnly;
  
  private final MenuPopupWindow mPopup;
  
  private final int mPopupMaxWidth;
  
  private final int mPopupStyleAttr;
  
  private final int mPopupStyleRes;
  
  private MenuPresenter.Callback mPresenterCallback;
  
  private boolean mShowTitle;
  
  private View mShownAnchorView;
  
  private ViewTreeObserver mTreeObserver;
  
  private boolean mWasDismissed;
  
  public StandardMenuPopup(Context paramContext, MenuBuilder paramMenuBuilder, View paramView, int paramInt1, int paramInt2, boolean paramBoolean) {
    Objects.requireNonNull(paramContext);
    this.mContext = paramContext;
    this.mMenu = paramMenuBuilder;
    this.mOverflowOnly = paramBoolean;
    LayoutInflater layoutInflater = LayoutInflater.from(paramContext);
    this.mAdapter = new MenuAdapter(paramMenuBuilder, layoutInflater, this.mOverflowOnly, 17367235);
    this.mPopupStyleAttr = paramInt1;
    this.mPopupStyleRes = paramInt2;
    Resources resources = paramContext.getResources();
    paramInt2 = (resources.getDisplayMetrics()).widthPixels / 2;
    paramInt1 = resources.getDimensionPixelSize(17105073);
    this.mPopupMaxWidth = Math.max(paramInt2, paramInt1);
    this.mAnchorView = paramView;
    this.mPopup = new MenuPopupWindow(this.mContext, null, this.mPopupStyleAttr, this.mPopupStyleRes);
    paramMenuBuilder.addMenuPresenter(this, paramContext);
  }
  
  public void setForceShowIcon(boolean paramBoolean) {
    this.mAdapter.setForceShowIcon(paramBoolean);
  }
  
  public void setGravity(int paramInt) {
    this.mDropDownGravity = paramInt;
  }
  
  private boolean tryShow() {
    if (isShowing())
      return true; 
    if (!this.mWasDismissed) {
      View view = this.mAnchorView;
      if (view != null) {
        boolean bool;
        this.mShownAnchorView = view;
        this.mPopup.setOnDismissListener(this);
        this.mPopup.setOnItemClickListener(this);
        this.mPopup.setAdapter((ListAdapter)this.mAdapter);
        this.mPopup.setModal(true);
        view = this.mShownAnchorView;
        if (this.mTreeObserver == null) {
          bool = true;
        } else {
          bool = false;
        } 
        ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
        if (bool)
          viewTreeObserver.addOnGlobalLayoutListener(this.mGlobalLayoutListener); 
        view.addOnAttachStateChangeListener(this.mAttachStateChangeListener);
        this.mPopup.setAnchorView(view);
        this.mPopup.setDropDownGravity(this.mDropDownGravity);
        if (!this.mHasContentWidth) {
          this.mContentWidth = measureIndividualMenuWidth((ListAdapter)this.mAdapter, null, this.mContext, this.mPopupMaxWidth);
          this.mHasContentWidth = true;
        } 
        this.mPopup.setContentWidth(this.mContentWidth);
        this.mPopup.setInputMethodMode(2);
        this.mPopup.setEpicenterBounds(getEpicenterBounds());
        this.mPopup.show();
        ListView listView = this.mPopup.getListView();
        listView.setOnKeyListener(this);
        if (this.mShowTitle && this.mMenu.getHeaderTitle() != null) {
          Context context = this.mContext;
          FrameLayout frameLayout = (FrameLayout)LayoutInflater.from(context).inflate(17367234, (ViewGroup)listView, false);
          TextView textView = (TextView)frameLayout.findViewById(16908310);
          if (textView != null)
            textView.setText(this.mMenu.getHeaderTitle()); 
          frameLayout.setEnabled(false);
          listView.addHeaderView((View)frameLayout, null, false);
          this.mPopup.show();
        } 
        return true;
      } 
    } 
    return false;
  }
  
  public void show() {
    if (tryShow())
      return; 
    throw new IllegalStateException("StandardMenuPopup cannot be used without an anchor");
  }
  
  public void dismiss() {
    if (isShowing())
      this.mPopup.dismiss(); 
  }
  
  public void addMenu(MenuBuilder paramMenuBuilder) {}
  
  public boolean isShowing() {
    boolean bool;
    if (!this.mWasDismissed && this.mPopup.isShowing()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void onDismiss() {
    this.mWasDismissed = true;
    this.mMenu.close();
    ViewTreeObserver viewTreeObserver = this.mTreeObserver;
    if (viewTreeObserver != null) {
      if (!viewTreeObserver.isAlive())
        this.mTreeObserver = this.mShownAnchorView.getViewTreeObserver(); 
      this.mTreeObserver.removeGlobalOnLayoutListener(this.mGlobalLayoutListener);
      this.mTreeObserver = null;
    } 
    this.mShownAnchorView.removeOnAttachStateChangeListener(this.mAttachStateChangeListener);
    PopupWindow.OnDismissListener onDismissListener = this.mOnDismissListener;
    if (onDismissListener != null)
      onDismissListener.onDismiss(); 
  }
  
  public void updateMenuView(boolean paramBoolean) {
    this.mHasContentWidth = false;
    MenuAdapter menuAdapter = this.mAdapter;
    if (menuAdapter != null)
      menuAdapter.notifyDataSetChanged(); 
  }
  
  public void setCallback(MenuPresenter.Callback paramCallback) {
    this.mPresenterCallback = paramCallback;
  }
  
  public boolean onSubMenuSelected(SubMenuBuilder paramSubMenuBuilder) {
    if (paramSubMenuBuilder.hasVisibleItems()) {
      MenuPopupHelper menuPopupHelper = new MenuPopupHelper(this.mContext, paramSubMenuBuilder, this.mShownAnchorView, this.mOverflowOnly, this.mPopupStyleAttr, this.mPopupStyleRes);
      menuPopupHelper.setPresenterCallback(this.mPresenterCallback);
      menuPopupHelper.setForceShowIcon(MenuPopup.shouldPreserveIconSpacing(paramSubMenuBuilder));
      menuPopupHelper.setOnDismissListener(this.mOnDismissListener);
      this.mOnDismissListener = null;
      this.mMenu.close(false);
      int i = this.mPopup.getHorizontalOffset();
      int j = this.mPopup.getVerticalOffset();
      int k = this.mDropDownGravity;
      View view = this.mAnchorView;
      int m = view.getLayoutDirection();
      k = Gravity.getAbsoluteGravity(k, m);
      m = i;
      if ((k & 0x7) == 5)
        m = i + this.mAnchorView.getWidth(); 
      if (menuPopupHelper.tryShow(m, j)) {
        MenuPresenter.Callback callback = this.mPresenterCallback;
        if (callback != null)
          callback.onOpenSubMenu(paramSubMenuBuilder); 
        return true;
      } 
    } 
    return false;
  }
  
  public void onCloseMenu(MenuBuilder paramMenuBuilder, boolean paramBoolean) {
    if (paramMenuBuilder != this.mMenu)
      return; 
    dismiss();
    MenuPresenter.Callback callback = this.mPresenterCallback;
    if (callback != null)
      callback.onCloseMenu(paramMenuBuilder, paramBoolean); 
  }
  
  public boolean flagActionItems() {
    return false;
  }
  
  public Parcelable onSaveInstanceState() {
    return null;
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {}
  
  public void setAnchorView(View paramView) {
    this.mAnchorView = paramView;
  }
  
  public boolean onKey(View paramView, int paramInt, KeyEvent paramKeyEvent) {
    if (paramKeyEvent.getAction() == 1 && paramInt == 82) {
      dismiss();
      return true;
    } 
    return false;
  }
  
  public void setOnDismissListener(PopupWindow.OnDismissListener paramOnDismissListener) {
    this.mOnDismissListener = paramOnDismissListener;
  }
  
  public ListView getListView() {
    return this.mPopup.getListView();
  }
  
  public void setHorizontalOffset(int paramInt) {
    this.mPopup.setHorizontalOffset(paramInt);
  }
  
  public void setVerticalOffset(int paramInt) {
    this.mPopup.setVerticalOffset(paramInt);
  }
  
  public void setShowTitle(boolean paramBoolean) {
    this.mShowTitle = paramBoolean;
  }
}
