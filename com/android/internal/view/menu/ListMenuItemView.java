package com.android.internal.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import com.android.internal.R;

public class ListMenuItemView extends LinearLayout implements MenuView.ItemView, AbsListView.SelectionBoundsAdjuster {
  private static final String TAG = "ListMenuItemView";
  
  private Drawable mBackground;
  
  private CheckBox mCheckBox;
  
  private LinearLayout mContent;
  
  private boolean mForceShowIcon;
  
  private ImageView mGroupDivider;
  
  private boolean mHasListDivider;
  
  private ImageView mIconView;
  
  private LayoutInflater mInflater;
  
  private MenuItemImpl mItemData;
  
  private int mMenuType;
  
  private boolean mPreserveIconSpacing;
  
  private RadioButton mRadioButton;
  
  private TextView mShortcutView;
  
  private Drawable mSubMenuArrow;
  
  private ImageView mSubMenuArrowView;
  
  private int mTextAppearance;
  
  private Context mTextAppearanceContext;
  
  private TextView mTitleView;
  
  public ListMenuItemView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray2 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.MenuView, paramInt1, paramInt2);
    this.mBackground = typedArray2.getDrawable(5);
    this.mTextAppearance = typedArray2.getResourceId(1, -1);
    this.mPreserveIconSpacing = typedArray2.getBoolean(8, false);
    this.mTextAppearanceContext = paramContext;
    this.mSubMenuArrow = typedArray2.getDrawable(7);
    Resources.Theme theme = paramContext.getTheme();
    TypedArray typedArray1 = theme.obtainStyledAttributes(null, new int[] { 16843049 }, 16842861, 0);
    this.mHasListDivider = typedArray1.hasValue(0);
    typedArray2.recycle();
    typedArray1.recycle();
  }
  
  public ListMenuItemView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public ListMenuItemView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16844018);
  }
  
  protected void onFinishInflate() {
    super.onFinishInflate();
    setBackgroundDrawable(this.mBackground);
    TextView textView = (TextView)findViewById(16908310);
    int i = this.mTextAppearance;
    if (i != -1)
      textView.setTextAppearance(this.mTextAppearanceContext, i); 
    this.mShortcutView = (TextView)findViewById(16909426);
    ImageView imageView = (ImageView)findViewById(16909489);
    if (imageView != null)
      imageView.setImageDrawable(this.mSubMenuArrow); 
    this.mGroupDivider = (ImageView)findViewById(16909024);
    this.mContent = (LinearLayout)findViewById(16908290);
  }
  
  public void initialize(MenuItemImpl paramMenuItemImpl, int paramInt) {
    this.mItemData = paramMenuItemImpl;
    this.mMenuType = paramInt;
    if (paramMenuItemImpl.isVisible()) {
      paramInt = 0;
    } else {
      paramInt = 8;
    } 
    setVisibility(paramInt);
    setTitle(paramMenuItemImpl.getTitleForItemView(this));
    setCheckable(paramMenuItemImpl.isCheckable());
    setShortcut(paramMenuItemImpl.shouldShowShortcut(), paramMenuItemImpl.getShortcut());
    setIcon(paramMenuItemImpl.getIcon());
    setEnabled(paramMenuItemImpl.isEnabled());
    setSubMenuArrowVisible(paramMenuItemImpl.hasSubMenu());
    setContentDescription(paramMenuItemImpl.getContentDescription());
  }
  
  private void addContentView(View paramView) {
    addContentView(paramView, -1);
  }
  
  private void addContentView(View paramView, int paramInt) {
    LinearLayout linearLayout = this.mContent;
    if (linearLayout != null) {
      linearLayout.addView(paramView, paramInt);
    } else {
      addView(paramView, paramInt);
    } 
  }
  
  public void setForceShowIcon(boolean paramBoolean) {
    this.mForceShowIcon = paramBoolean;
    this.mPreserveIconSpacing = paramBoolean;
  }
  
  public void setTitle(CharSequence paramCharSequence) {
    if (paramCharSequence != null) {
      this.mTitleView.setText(paramCharSequence);
      if (this.mTitleView.getVisibility() != 0)
        this.mTitleView.setVisibility(0); 
    } else if (this.mTitleView.getVisibility() != 8) {
      this.mTitleView.setVisibility(8);
    } 
  }
  
  public MenuItemImpl getItemData() {
    return this.mItemData;
  }
  
  public void setCheckable(boolean paramBoolean) {
    CheckBox checkBox;
    RadioButton radioButton;
    if (!paramBoolean && this.mRadioButton == null && this.mCheckBox == null)
      return; 
    if (this.mItemData.isExclusiveCheckable()) {
      if (this.mRadioButton == null)
        insertRadioButton(); 
      RadioButton radioButton1 = this.mRadioButton;
      CheckBox checkBox1 = this.mCheckBox;
    } else {
      if (this.mCheckBox == null)
        insertCheckBox(); 
      checkBox = this.mCheckBox;
      radioButton = this.mRadioButton;
    } 
    if (paramBoolean) {
      byte b;
      checkBox.setChecked(this.mItemData.isChecked());
      if (paramBoolean) {
        b = 0;
      } else {
        b = 8;
      } 
      if (checkBox.getVisibility() != b)
        checkBox.setVisibility(b); 
      if (radioButton != null && radioButton.getVisibility() != 8)
        radioButton.setVisibility(8); 
    } else {
      CheckBox checkBox1 = this.mCheckBox;
      if (checkBox1 != null)
        checkBox1.setVisibility(8); 
      RadioButton radioButton1 = this.mRadioButton;
      if (radioButton1 != null)
        radioButton1.setVisibility(8); 
    } 
  }
  
  public void setChecked(boolean paramBoolean) {
    CheckBox checkBox;
    if (this.mItemData.isExclusiveCheckable()) {
      if (this.mRadioButton == null)
        insertRadioButton(); 
      RadioButton radioButton = this.mRadioButton;
    } else {
      if (this.mCheckBox == null)
        insertCheckBox(); 
      checkBox = this.mCheckBox;
    } 
    checkBox.setChecked(paramBoolean);
  }
  
  private void setSubMenuArrowVisible(boolean paramBoolean) {
    ImageView imageView = this.mSubMenuArrowView;
    if (imageView != null) {
      byte b;
      if (paramBoolean) {
        b = 0;
      } else {
        b = 8;
      } 
      imageView.setVisibility(b);
    } 
  }
  
  public void setShortcut(boolean paramBoolean, char paramChar) {
    if (paramBoolean && this.mItemData.shouldShowShortcut()) {
      paramChar = Character.MIN_VALUE;
    } else {
      paramChar = '\b';
    } 
    if (paramChar == '\000')
      this.mShortcutView.setText(this.mItemData.getShortcutLabel()); 
    if (this.mShortcutView.getVisibility() != paramChar)
      this.mShortcutView.setVisibility(paramChar); 
  }
  
  public void setIcon(Drawable paramDrawable) {
    boolean bool;
    if (this.mItemData.shouldShowIcon() || this.mForceShowIcon) {
      bool = true;
    } else {
      bool = false;
    } 
    if (!bool && !this.mPreserveIconSpacing)
      return; 
    if (this.mIconView == null && paramDrawable == null && !this.mPreserveIconSpacing)
      return; 
    if (this.mIconView == null)
      insertIconView(); 
    if (paramDrawable != null || this.mPreserveIconSpacing) {
      ImageView imageView = this.mIconView;
      if (!bool)
        paramDrawable = null; 
      imageView.setImageDrawable(paramDrawable);
      if (this.mIconView.getVisibility() != 0)
        this.mIconView.setVisibility(0); 
      return;
    } 
    this.mIconView.setVisibility(8);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    if (this.mIconView != null && this.mPreserveIconSpacing) {
      ViewGroup.LayoutParams layoutParams = getLayoutParams();
      LinearLayout.LayoutParams layoutParams1 = (LinearLayout.LayoutParams)this.mIconView.getLayoutParams();
      if (layoutParams.height > 0 && layoutParams1.width <= 0)
        layoutParams1.width = layoutParams.height; 
    } 
    super.onMeasure(paramInt1, paramInt2);
  }
  
  private void insertIconView() {
    LayoutInflater layoutInflater = getInflater();
    ImageView imageView = (ImageView)layoutInflater.inflate(17367189, (ViewGroup)this, false);
    addContentView((View)imageView, 0);
  }
  
  private void insertRadioButton() {
    LayoutInflater layoutInflater = getInflater();
    RadioButton radioButton = (RadioButton)layoutInflater.inflate(17367191, (ViewGroup)this, false);
    addContentView((View)radioButton);
  }
  
  private void insertCheckBox() {
    LayoutInflater layoutInflater = getInflater();
    CheckBox checkBox = (CheckBox)layoutInflater.inflate(17367188, (ViewGroup)this, false);
    addContentView((View)checkBox);
  }
  
  public boolean prefersCondensedTitle() {
    return false;
  }
  
  public boolean showsIcon() {
    return this.mForceShowIcon;
  }
  
  private LayoutInflater getInflater() {
    if (this.mInflater == null)
      this.mInflater = LayoutInflater.from(this.mContext); 
    return this.mInflater;
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    MenuItemImpl menuItemImpl = this.mItemData;
    if (menuItemImpl != null && menuItemImpl.hasSubMenu())
      paramAccessibilityNodeInfo.setCanOpenPopup(true); 
  }
  
  public void setGroupDividerEnabled(boolean paramBoolean) {
    ImageView imageView = this.mGroupDivider;
    if (imageView != null) {
      byte b;
      if (!this.mHasListDivider && paramBoolean) {
        b = 0;
      } else {
        b = 8;
      } 
      imageView.setVisibility(b);
    } 
  }
  
  public void adjustListItemSelectionBounds(Rect paramRect) {
    ImageView imageView = this.mGroupDivider;
    if (imageView != null && imageView.getVisibility() == 0) {
      LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)this.mGroupDivider.getLayoutParams();
      paramRect.top += this.mGroupDivider.getHeight() + layoutParams.topMargin + layoutParams.bottomMargin;
    } 
  }
}
