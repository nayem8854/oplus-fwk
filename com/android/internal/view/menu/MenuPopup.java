package com.android.internal.view.menu;

import android.content.Context;
import android.graphics.Rect;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.PopupWindow;

public abstract class MenuPopup implements ShowableListMenu, MenuPresenter, AdapterView.OnItemClickListener {
  private Rect mEpicenterBounds;
  
  public void setEpicenterBounds(Rect paramRect) {
    this.mEpicenterBounds = paramRect;
  }
  
  public Rect getEpicenterBounds() {
    return this.mEpicenterBounds;
  }
  
  public void initForMenu(Context paramContext, MenuBuilder paramMenuBuilder) {}
  
  public MenuView getMenuView(ViewGroup paramViewGroup) {
    throw new UnsupportedOperationException("MenuPopups manage their own views");
  }
  
  public boolean expandItemActionView(MenuBuilder paramMenuBuilder, MenuItemImpl paramMenuItemImpl) {
    return false;
  }
  
  public boolean collapseItemActionView(MenuBuilder paramMenuBuilder, MenuItemImpl paramMenuItemImpl) {
    return false;
  }
  
  public int getId() {
    return 0;
  }
  
  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong) {
    ListAdapter listAdapter = (ListAdapter)paramAdapterView.getAdapter();
    MenuAdapter menuAdapter = toMenuAdapter(listAdapter);
    menuAdapter.mAdapterMenu.performItemAction((MenuItem)listAdapter.getItem(paramInt), 0);
  }
  
  protected static int measureIndividualMenuWidth(ListAdapter paramListAdapter, ViewGroup paramViewGroup, Context paramContext, int paramInt) {
    int i = 0;
    ViewGroup viewGroup1 = null;
    int j = 0;
    int k = View.MeasureSpec.makeMeasureSpec(0, 0);
    int m = View.MeasureSpec.makeMeasureSpec(0, 0);
    int n = paramListAdapter.getCount();
    byte b;
    ViewGroup viewGroup2;
    for (b = 0, viewGroup2 = paramViewGroup, paramViewGroup = viewGroup1; b < n; b++, i = j, j = i2, frameLayout2 = frameLayout1) {
      FrameLayout frameLayout1, frameLayout2;
      int i1 = paramListAdapter.getItemViewType(b);
      int i2 = j;
      if (i1 != j) {
        i2 = i1;
        paramViewGroup = null;
      } 
      viewGroup1 = viewGroup2;
      if (viewGroup2 == null)
        frameLayout1 = new FrameLayout(paramContext); 
      View view = paramListAdapter.getView(b, (View)paramViewGroup, (ViewGroup)frameLayout1);
      view.measure(k, m);
      i1 = view.getMeasuredWidth();
      if (i1 >= paramInt)
        return paramInt; 
      j = i;
      if (i1 > i)
        j = i1; 
    } 
    return i;
  }
  
  protected static MenuAdapter toMenuAdapter(ListAdapter paramListAdapter) {
    if (paramListAdapter instanceof HeaderViewListAdapter)
      return (MenuAdapter)((HeaderViewListAdapter)paramListAdapter).getWrappedAdapter(); 
    return (MenuAdapter)paramListAdapter;
  }
  
  protected static boolean shouldPreserveIconSpacing(MenuBuilder paramMenuBuilder) {
    boolean bool2, bool1 = false;
    int i = paramMenuBuilder.size();
    byte b = 0;
    while (true) {
      bool2 = bool1;
      if (b < i) {
        MenuItem menuItem = paramMenuBuilder.getItem(b);
        if (menuItem.isVisible() && menuItem.getIcon() != null) {
          bool2 = true;
          break;
        } 
        b++;
        continue;
      } 
      break;
    } 
    return bool2;
  }
  
  public abstract void addMenu(MenuBuilder paramMenuBuilder);
  
  public abstract void setAnchorView(View paramView);
  
  public abstract void setForceShowIcon(boolean paramBoolean);
  
  public abstract void setGravity(int paramInt);
  
  public abstract void setHorizontalOffset(int paramInt);
  
  public abstract void setOnDismissListener(PopupWindow.OnDismissListener paramOnDismissListener);
  
  public abstract void setShowTitle(boolean paramBoolean);
  
  public abstract void setVerticalOffset(int paramInt);
}
