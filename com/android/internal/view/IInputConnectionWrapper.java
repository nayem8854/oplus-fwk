package com.android.internal.view;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CorrectionInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionInspector;
import android.view.inputmethod.InputContentInfo;
import com.android.internal.inputmethod.ICharSequenceResultCallback;
import com.android.internal.inputmethod.IExtractedTextResultCallback;
import com.android.internal.inputmethod.IIntResultCallback;
import com.android.internal.os.SomeArgs;

public abstract class IInputConnectionWrapper extends IInputContext.Stub {
  private Looper mMainLooper;
  
  private Object mLock = new Object();
  
  private InputConnection mInputConnection;
  
  private Handler mH;
  
  private boolean mFinished = false;
  
  private static final String TAG = "IInputConnectionWrapper";
  
  private static final int DO_SET_SELECTION = 57;
  
  private static final int DO_SET_COMPOSING_TEXT = 60;
  
  private static final int DO_SET_COMPOSING_REGION = 63;
  
  private static final int DO_SEND_KEY_EVENT = 70;
  
  private static final int DO_REQUEST_UPDATE_CURSOR_ANCHOR_INFO = 140;
  
  private static final int DO_PERFORM_PRIVATE_COMMAND = 120;
  
  private static final int DO_PERFORM_EDITOR_ACTION = 58;
  
  private static final int DO_PERFORM_CONTEXT_MENU_ACTION = 59;
  
  private static final int DO_GET_TEXT_BEFORE_CURSOR = 20;
  
  private static final int DO_GET_TEXT_AFTER_CURSOR = 10;
  
  private static final int DO_GET_SELECTED_TEXT = 25;
  
  private static final int DO_GET_EXTRACTED_TEXT = 40;
  
  private static final int DO_GET_CURSOR_CAPS_MODE = 30;
  
  private static final int DO_FINISH_COMPOSING_TEXT = 65;
  
  private static final int DO_END_BATCH_EDIT = 95;
  
  private static final int DO_DELETE_SURROUNDING_TEXT_IN_CODE_POINTS = 81;
  
  private static final int DO_DELETE_SURROUNDING_TEXT = 80;
  
  private static final int DO_COMMIT_TEXT = 50;
  
  private static final int DO_COMMIT_CORRECTION = 56;
  
  private static final int DO_COMMIT_CONTENT = 160;
  
  private static final int DO_COMMIT_COMPLETION = 55;
  
  private static final int DO_CLOSE_CONNECTION = 150;
  
  private static final int DO_CLEAR_META_KEY_STATES = 130;
  
  private static final int DO_BEGIN_BATCH_EDIT = 90;
  
  private static final boolean DEBUG = false;
  
  class MyHandler extends Handler {
    final IInputConnectionWrapper this$0;
    
    MyHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      IInputConnectionWrapper.this.executeMessage(param1Message);
    }
  }
  
  public IInputConnectionWrapper(Looper paramLooper, InputConnection paramInputConnection) {
    this.mInputConnection = paramInputConnection;
    this.mMainLooper = paramLooper;
    this.mH = new MyHandler(this.mMainLooper);
  }
  
  public InputConnection getInputConnection() {
    synchronized (this.mLock) {
      return this.mInputConnection;
    } 
  }
  
  protected boolean isFinished() {
    synchronized (this.mLock) {
      return this.mFinished;
    } 
  }
  
  public void getTextAfterCursor(int paramInt1, int paramInt2, ICharSequenceResultCallback paramICharSequenceResultCallback) {
    dispatchMessage(this.mH.obtainMessage(10, paramInt1, paramInt2, paramICharSequenceResultCallback));
  }
  
  public void getTextBeforeCursor(int paramInt1, int paramInt2, ICharSequenceResultCallback paramICharSequenceResultCallback) {
    dispatchMessage(this.mH.obtainMessage(20, paramInt1, paramInt2, paramICharSequenceResultCallback));
  }
  
  public void getSelectedText(int paramInt, ICharSequenceResultCallback paramICharSequenceResultCallback) {
    dispatchMessage(this.mH.obtainMessage(25, paramInt, 0, paramICharSequenceResultCallback));
  }
  
  public void getCursorCapsMode(int paramInt, IIntResultCallback paramIIntResultCallback) {
    Handler handler = this.mH;
    Message message = handler.obtainMessage(30, paramInt, 0, paramIIntResultCallback);
    dispatchMessage(message);
  }
  
  public void getExtractedText(ExtractedTextRequest paramExtractedTextRequest, int paramInt, IExtractedTextResultCallback paramIExtractedTextResultCallback) {
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.arg1 = paramExtractedTextRequest;
    someArgs.arg2 = paramIExtractedTextResultCallback;
    dispatchMessage(this.mH.obtainMessage(40, paramInt, 0, someArgs));
  }
  
  public void commitText(CharSequence paramCharSequence, int paramInt) {
    dispatchMessage(obtainMessageIO(50, paramInt, paramCharSequence));
  }
  
  public void commitCompletion(CompletionInfo paramCompletionInfo) {
    dispatchMessage(obtainMessageO(55, paramCompletionInfo));
  }
  
  public void commitCorrection(CorrectionInfo paramCorrectionInfo) {
    dispatchMessage(obtainMessageO(56, paramCorrectionInfo));
  }
  
  public void setSelection(int paramInt1, int paramInt2) {
    dispatchMessage(obtainMessageII(57, paramInt1, paramInt2));
  }
  
  public void performEditorAction(int paramInt) {
    dispatchMessage(obtainMessageII(58, paramInt, 0));
  }
  
  public void performContextMenuAction(int paramInt) {
    dispatchMessage(obtainMessageII(59, paramInt, 0));
  }
  
  public void setComposingRegion(int paramInt1, int paramInt2) {
    dispatchMessage(obtainMessageII(63, paramInt1, paramInt2));
  }
  
  public void setComposingText(CharSequence paramCharSequence, int paramInt) {
    dispatchMessage(obtainMessageIO(60, paramInt, paramCharSequence));
  }
  
  public void finishComposingText() {
    dispatchMessage(obtainMessage(65));
  }
  
  public void sendKeyEvent(KeyEvent paramKeyEvent) {
    dispatchMessage(obtainMessageO(70, paramKeyEvent));
  }
  
  public void clearMetaKeyStates(int paramInt) {
    dispatchMessage(obtainMessageII(130, paramInt, 0));
  }
  
  public void deleteSurroundingText(int paramInt1, int paramInt2) {
    dispatchMessage(obtainMessageII(80, paramInt1, paramInt2));
  }
  
  public void deleteSurroundingTextInCodePoints(int paramInt1, int paramInt2) {
    dispatchMessage(obtainMessageII(81, paramInt1, paramInt2));
  }
  
  public void beginBatchEdit() {
    dispatchMessage(obtainMessage(90));
  }
  
  public void endBatchEdit() {
    dispatchMessage(obtainMessage(95));
  }
  
  public void performPrivateCommand(String paramString, Bundle paramBundle) {
    dispatchMessage(obtainMessageOO(120, paramString, paramBundle));
  }
  
  public void requestUpdateCursorAnchorInfo(int paramInt, IIntResultCallback paramIIntResultCallback) {
    dispatchMessage(this.mH.obtainMessage(140, paramInt, 0, paramIIntResultCallback));
  }
  
  public void closeConnection() {
    dispatchMessage(obtainMessage(150));
  }
  
  public void commitContent(InputContentInfo paramInputContentInfo, int paramInt, Bundle paramBundle, IIntResultCallback paramIIntResultCallback) {
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.arg1 = paramInputContentInfo;
    someArgs.arg2 = paramBundle;
    someArgs.arg3 = paramIIntResultCallback;
    dispatchMessage(this.mH.obtainMessage(160, paramInt, 0, someArgs));
  }
  
  void dispatchMessage(Message paramMessage) {
    if (Looper.myLooper() == this.mMainLooper) {
      executeMessage(paramMessage);
      paramMessage.recycle();
      return;
    } 
    this.mH.sendMessage(paramMessage);
  }
  
  void executeMessage(Message paramMessage) {
    CharSequence charSequence;
    int i = paramMessage.what;
    if (i != 80) {
      if (i != 81) {
        SomeArgs someArgs2;
        StringBuilder stringBuilder1;
        SomeArgs someArgs1;
        StringBuilder stringBuilder2;
        IIntResultCallback iIntResultCallback2;
        InputConnection inputConnection2;
        SomeArgs someArgs3;
        IIntResultCallback iIntResultCallback1;
        boolean bool;
        int j = 0, k = 0;
        switch (i) {
          default:
            switch (i) {
              default:
                stringBuilder2 = new StringBuilder();
                stringBuilder2.append("Unhandled message code: ");
                stringBuilder2.append(paramMessage.what);
                Log.w("IInputConnectionWrapper", stringBuilder2.toString());
                return;
              case 60:
                null = getInputConnection();
                if (null == null || !isActive()) {
                  Log.w("IInputConnectionWrapper", "setComposingText on inactive InputConnection");
                  return;
                } 
                null.setComposingText((CharSequence)paramMessage.obj, paramMessage.arg1);
                return;
              case 59:
                null = getInputConnection();
                if (null == null || !isActive()) {
                  Log.w("IInputConnectionWrapper", "performContextMenuAction on inactive InputConnection");
                  return;
                } 
                null.performContextMenuAction(paramMessage.arg1);
                return;
              case 58:
                null = getInputConnection();
                if (null == null || !isActive()) {
                  Log.w("IInputConnectionWrapper", "performEditorAction on inactive InputConnection");
                  return;
                } 
                null.performEditorAction(paramMessage.arg1);
                return;
              case 57:
                null = getInputConnection();
                if (null == null || !isActive()) {
                  Log.w("IInputConnectionWrapper", "setSelection on inactive InputConnection");
                  return;
                } 
                null.setSelection(paramMessage.arg1, paramMessage.arg2);
                return;
              case 56:
                null = getInputConnection();
                if (null == null || !isActive()) {
                  Log.w("IInputConnectionWrapper", "commitCorrection on inactive InputConnection");
                  return;
                } 
                null.commitCorrection((CorrectionInfo)paramMessage.obj);
                return;
              case 55:
                break;
            } 
            null = getInputConnection();
            if (null == null || !isActive()) {
              Log.w("IInputConnectionWrapper", "commitCompletion on inactive InputConnection");
              return;
            } 
            null.commitCompletion((CompletionInfo)paramMessage.obj);
            return;
          case 160:
            j = paramMessage.arg1;
            someArgs2 = (SomeArgs)paramMessage.obj;
            try {
              IIntResultCallback iIntResultCallback = (IIntResultCallback)someArgs2.arg3;
              InputConnection inputConnection4 = getInputConnection();
              if (inputConnection4 == null || !isActive()) {
                Log.w("IInputConnectionWrapper", "commitContent on inactive InputConnection");
                bool = false;
              } else {
                StringBuilder stringBuilder;
                InputContentInfo inputContentInfo = (InputContentInfo)someArgs2.arg1;
                if (inputContentInfo == null || !inputContentInfo.validate()) {
                  stringBuilder = new StringBuilder();
                  this();
                  stringBuilder.append("commitContent with invalid inputContentInfo=");
                  stringBuilder.append(inputContentInfo);
                  Log.w("IInputConnectionWrapper", stringBuilder.toString());
                  bool = false;
                } else {
                  bool = stringBuilder.commitContent(inputContentInfo, j, (Bundle)someArgs2.arg2);
                } 
              } 
              if (bool)
                k = 1; 
              try {
                iIntResultCallback.onResult(k);
              } catch (RemoteException remoteException1) {
                StringBuilder stringBuilder = new StringBuilder();
                this();
                stringBuilder.append("Failed to return the result to commitContent(). result=");
                stringBuilder.append(bool);
                Log.w("IInputConnectionWrapper", stringBuilder.toString(), (Throwable)remoteException1);
              } 
              return;
            } finally {
              someArgs2.recycle();
            } 
          case 150:
            if (isFinished())
              return; 
            try {
              null = getInputConnection();
              if (null == null)
                synchronized (this.mLock) {
                  this.mInputConnection = null;
                  return;
                }  
              k = InputConnectionInspector.getMissingMethodFlags(null);
              if ((k & 0x40) == 0)
                null.closeConnection(); 
            } finally {
              stringBuilder2 = null;
            } 
          case 140:
            iIntResultCallback2 = (IIntResultCallback)((Message)someArgs2).obj;
            inputConnection3 = getInputConnection();
            if (inputConnection3 == null || !isActive()) {
              Log.w("IInputConnectionWrapper", "requestCursorAnchorInfo on inactive InputConnection");
              bool = false;
            } else {
              bool = inputConnection3.requestCursorUpdates(((Message)someArgs2).arg1);
            } 
            k = j;
            if (bool)
              k = 1; 
            try {
              iIntResultCallback2.onResult(k);
            } catch (RemoteException remoteException1) {
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append("Failed to return the result to requestCursorUpdates(). result=");
              stringBuilder1.append(bool);
              Log.w("IInputConnectionWrapper", stringBuilder1.toString(), (Throwable)remoteException1);
            } 
            return;
          case 130:
            null = getInputConnection();
            if (null == null || !isActive()) {
              Log.w("IInputConnectionWrapper", "clearMetaKeyStates on inactive InputConnection");
              return;
            } 
            null.clearMetaKeyStates(((Message)stringBuilder1).arg1);
            return;
          case 120:
            someArgs1 = (SomeArgs)((Message)stringBuilder1).obj;
            try {
              String str = (String)someArgs1.arg1;
              Bundle bundle = (Bundle)someArgs1.arg2;
              InputConnection inputConnection4 = getInputConnection();
              if (inputConnection4 == null || !isActive()) {
                Log.w("IInputConnectionWrapper", "performPrivateCommand on inactive InputConnection");
                return;
              } 
              inputConnection4.performPrivateCommand(str, bundle);
              return;
            } finally {
              someArgs1.recycle();
            } 
          case 95:
            null = getInputConnection();
            if (null == null || !isActive()) {
              Log.w("IInputConnectionWrapper", "endBatchEdit on inactive InputConnection");
              return;
            } 
            null.endBatchEdit();
            return;
          case 90:
            null = getInputConnection();
            if (null == null || !isActive()) {
              Log.w("IInputConnectionWrapper", "beginBatchEdit on inactive InputConnection");
              return;
            } 
            null.beginBatchEdit();
            return;
          case 70:
            inputConnection2 = getInputConnection();
            if (inputConnection2 == null || !isActive()) {
              Log.w("IInputConnectionWrapper", "sendKeyEvent on inactive InputConnection");
              return;
            } 
            inputConnection2.sendKeyEvent((KeyEvent)((Message)null).obj);
            return;
          case 65:
            if (isFinished())
              return; 
            null = getInputConnection();
            if (null == null) {
              Log.w("IInputConnectionWrapper", "finishComposingText on inactive InputConnection");
              return;
            } 
            null.finishComposingText();
            return;
          case 63:
            inputConnection2 = getInputConnection();
            if (inputConnection2 == null || !isActive()) {
              Log.w("IInputConnectionWrapper", "setComposingRegion on inactive InputConnection");
              return;
            } 
            inputConnection2.setComposingRegion(((Message)null).arg1, ((Message)null).arg2);
            return;
          case 50:
            inputConnection2 = getInputConnection();
            if (inputConnection2 == null || !isActive()) {
              Log.w("IInputConnectionWrapper", "commitText on inactive InputConnection");
              return;
            } 
            inputConnection2.commitText((CharSequence)((Message)null).obj, ((Message)null).arg1);
            return;
          case 40:
            someArgs3 = (SomeArgs)((Message)null).obj;
            try {
              ExtractedText extractedText;
              ExtractedTextRequest extractedTextRequest = (ExtractedTextRequest)someArgs3.arg1;
              IExtractedTextResultCallback iExtractedTextResultCallback = (IExtractedTextResultCallback)someArgs3.arg2;
              InputConnection inputConnection4 = getInputConnection();
              if (inputConnection4 == null || !isActive()) {
                Log.w("IInputConnectionWrapper", "getExtractedText on inactive InputConnection");
                null = null;
              } else {
                extractedText = inputConnection4.getExtractedText(extractedTextRequest, ((Message)null).arg1);
              } 
              try {
                iExtractedTextResultCallback.onResult(extractedText);
              } catch (RemoteException remoteException1) {
                StringBuilder stringBuilder = new StringBuilder();
                this();
                stringBuilder.append("Failed to return the result to getExtractedText(). result=");
                stringBuilder.append(extractedText);
                Log.w("IInputConnectionWrapper", stringBuilder.toString(), (Throwable)remoteException1);
              } 
              return;
            } finally {
              someArgs3.recycle();
            } 
          case 30:
            iIntResultCallback1 = (IIntResultCallback)((Message)someArgs1).obj;
            inputConnection3 = getInputConnection();
            if (inputConnection3 == null || !isActive()) {
              Log.w("IInputConnectionWrapper", "getCursorCapsMode on inactive InputConnection");
              k = 0;
            } else {
              k = inputConnection3.getCursorCapsMode(((Message)someArgs1).arg1);
            } 
            try {
              iIntResultCallback1.onResult(k);
            } catch (RemoteException remoteException) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Failed to return the result to getCursorCapsMode(). result=");
              stringBuilder.append(k);
              Log.w("IInputConnectionWrapper", stringBuilder.toString(), (Throwable)remoteException);
            } 
            return;
          case 25:
            iCharSequenceResultCallback = (ICharSequenceResultCallback)((Message)remoteException).obj;
            inputConnection3 = getInputConnection();
            if (inputConnection3 == null || !isActive()) {
              Log.w("IInputConnectionWrapper", "getSelectedText on inactive InputConnection");
              remoteException = null;
            } else {
              charSequence = inputConnection3.getSelectedText(((Message)remoteException).arg1);
            } 
            try {
              iCharSequenceResultCallback.onResult(charSequence);
            } catch (RemoteException remoteException1) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Failed to return the result to getSelectedText(). result=");
              stringBuilder.append(charSequence);
              Log.w("IInputConnectionWrapper", stringBuilder.toString(), (Throwable)remoteException1);
            } 
            return;
          case 20:
            iCharSequenceResultCallback = (ICharSequenceResultCallback)((Message)charSequence).obj;
            inputConnection3 = getInputConnection();
            if (inputConnection3 == null || !isActive()) {
              Log.w("IInputConnectionWrapper", "getTextBeforeCursor on inactive InputConnection");
              charSequence = null;
            } else {
              charSequence = inputConnection3.getTextBeforeCursor(((Message)charSequence).arg1, ((Message)charSequence).arg2);
            } 
            try {
              iCharSequenceResultCallback.onResult(charSequence);
            } catch (RemoteException remoteException1) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Failed to return the result to getTextBeforeCursor(). result=");
              stringBuilder.append(charSequence);
              Log.w("IInputConnectionWrapper", stringBuilder.toString(), (Throwable)remoteException1);
            } 
            return;
          case 10:
            break;
        } 
        ICharSequenceResultCallback iCharSequenceResultCallback = (ICharSequenceResultCallback)((Message)charSequence).obj;
        InputConnection inputConnection3 = getInputConnection();
        if (inputConnection3 == null || !isActive()) {
          Log.w("IInputConnectionWrapper", "getTextAfterCursor on inactive InputConnection");
          charSequence = null;
        } else {
          charSequence = inputConnection3.getTextAfterCursor(((Message)charSequence).arg1, ((Message)charSequence).arg2);
        } 
        try {
          iCharSequenceResultCallback.onResult(charSequence);
        } catch (RemoteException remoteException1) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Failed to return the result to getTextAfterCursor(). result=");
          stringBuilder.append(charSequence);
          Log.w("IInputConnectionWrapper", stringBuilder.toString(), (Throwable)remoteException1);
        } 
        return;
      } 
      InputConnection inputConnection1 = getInputConnection();
      if (inputConnection1 == null || !isActive()) {
        Log.w("IInputConnectionWrapper", "deleteSurroundingTextInCodePoints on inactive InputConnection");
        return;
      } 
      inputConnection1.deleteSurroundingTextInCodePoints(((Message)charSequence).arg1, ((Message)charSequence).arg2);
      return;
    } 
    InputConnection inputConnection = getInputConnection();
    if (inputConnection == null || !isActive()) {
      Log.w("IInputConnectionWrapper", "deleteSurroundingText on inactive InputConnection");
      return;
    } 
    inputConnection.deleteSurroundingText(((Message)charSequence).arg1, ((Message)charSequence).arg2);
  }
  
  Message obtainMessage(int paramInt) {
    return this.mH.obtainMessage(paramInt);
  }
  
  Message obtainMessageII(int paramInt1, int paramInt2, int paramInt3) {
    return this.mH.obtainMessage(paramInt1, paramInt2, paramInt3);
  }
  
  Message obtainMessageO(int paramInt, Object paramObject) {
    return this.mH.obtainMessage(paramInt, 0, 0, paramObject);
  }
  
  Message obtainMessageIO(int paramInt1, int paramInt2, Object paramObject) {
    return this.mH.obtainMessage(paramInt1, paramInt2, 0, paramObject);
  }
  
  Message obtainMessageOO(int paramInt, Object paramObject1, Object paramObject2) {
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.arg1 = paramObject1;
    someArgs.arg2 = paramObject2;
    return this.mH.obtainMessage(paramInt, 0, 0, someArgs);
  }
  
  protected abstract boolean isActive();
}
