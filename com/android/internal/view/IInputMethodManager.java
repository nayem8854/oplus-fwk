package com.android.internal.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodSubtype;
import java.util.List;

public interface IInputMethodManager extends IInterface {
  void addClient(IInputMethodClient paramIInputMethodClient, IInputContext paramIInputContext, int paramInt) throws RemoteException;
  
  InputMethodSubtype getCurrentInputMethodSubtype() throws RemoteException;
  
  List<InputMethodInfo> getEnabledInputMethodList(int paramInt) throws RemoteException;
  
  List<InputMethodSubtype> getEnabledInputMethodSubtypeList(String paramString, boolean paramBoolean) throws RemoteException;
  
  List<InputMethodInfo> getInputMethodList(int paramInt) throws RemoteException;
  
  int getInputMethodWindowVisibleHeight() throws RemoteException;
  
  InputMethodSubtype getLastInputMethodSubtype() throws RemoteException;
  
  boolean hideSoftInput(IInputMethodClient paramIInputMethodClient, IBinder paramIBinder, int paramInt, ResultReceiver paramResultReceiver) throws RemoteException;
  
  boolean isInputMethodPickerShownForTest() throws RemoteException;
  
  void removeImeSurface() throws RemoteException;
  
  void removeImeSurfaceFromWindow(IBinder paramIBinder) throws RemoteException;
  
  void reportActivityView(IInputMethodClient paramIInputMethodClient, int paramInt, float[] paramArrayOffloat) throws RemoteException;
  
  void reportPerceptible(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  void setAdditionalInputMethodSubtypes(String paramString, InputMethodSubtype[] paramArrayOfInputMethodSubtype) throws RemoteException;
  
  void showInputMethodAndSubtypeEnablerFromClient(IInputMethodClient paramIInputMethodClient, String paramString) throws RemoteException;
  
  void showInputMethodPickerFromClient(IInputMethodClient paramIInputMethodClient, int paramInt) throws RemoteException;
  
  void showInputMethodPickerFromSystem(IInputMethodClient paramIInputMethodClient, int paramInt1, int paramInt2) throws RemoteException;
  
  boolean showSoftInput(IInputMethodClient paramIInputMethodClient, IBinder paramIBinder, int paramInt, ResultReceiver paramResultReceiver) throws RemoteException;
  
  InputBindResult startInputOrWindowGainedFocus(int paramInt1, IInputMethodClient paramIInputMethodClient, IBinder paramIBinder, int paramInt2, int paramInt3, int paramInt4, EditorInfo paramEditorInfo, IInputContext paramIInputContext, int paramInt5, int paramInt6) throws RemoteException;
  
  class Default implements IInputMethodManager {
    public void addClient(IInputMethodClient param1IInputMethodClient, IInputContext param1IInputContext, int param1Int) throws RemoteException {}
    
    public List<InputMethodInfo> getInputMethodList(int param1Int) throws RemoteException {
      return null;
    }
    
    public List<InputMethodInfo> getEnabledInputMethodList(int param1Int) throws RemoteException {
      return null;
    }
    
    public List<InputMethodSubtype> getEnabledInputMethodSubtypeList(String param1String, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public InputMethodSubtype getLastInputMethodSubtype() throws RemoteException {
      return null;
    }
    
    public boolean showSoftInput(IInputMethodClient param1IInputMethodClient, IBinder param1IBinder, int param1Int, ResultReceiver param1ResultReceiver) throws RemoteException {
      return false;
    }
    
    public boolean hideSoftInput(IInputMethodClient param1IInputMethodClient, IBinder param1IBinder, int param1Int, ResultReceiver param1ResultReceiver) throws RemoteException {
      return false;
    }
    
    public InputBindResult startInputOrWindowGainedFocus(int param1Int1, IInputMethodClient param1IInputMethodClient, IBinder param1IBinder, int param1Int2, int param1Int3, int param1Int4, EditorInfo param1EditorInfo, IInputContext param1IInputContext, int param1Int5, int param1Int6) throws RemoteException {
      return null;
    }
    
    public void showInputMethodPickerFromClient(IInputMethodClient param1IInputMethodClient, int param1Int) throws RemoteException {}
    
    public void showInputMethodPickerFromSystem(IInputMethodClient param1IInputMethodClient, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void showInputMethodAndSubtypeEnablerFromClient(IInputMethodClient param1IInputMethodClient, String param1String) throws RemoteException {}
    
    public boolean isInputMethodPickerShownForTest() throws RemoteException {
      return false;
    }
    
    public InputMethodSubtype getCurrentInputMethodSubtype() throws RemoteException {
      return null;
    }
    
    public void setAdditionalInputMethodSubtypes(String param1String, InputMethodSubtype[] param1ArrayOfInputMethodSubtype) throws RemoteException {}
    
    public int getInputMethodWindowVisibleHeight() throws RemoteException {
      return 0;
    }
    
    public void reportActivityView(IInputMethodClient param1IInputMethodClient, int param1Int, float[] param1ArrayOffloat) throws RemoteException {}
    
    public void reportPerceptible(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {}
    
    public void removeImeSurface() throws RemoteException {}
    
    public void removeImeSurfaceFromWindow(IBinder param1IBinder) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInputMethodManager {
    private static final String DESCRIPTOR = "com.android.internal.view.IInputMethodManager";
    
    static final int TRANSACTION_addClient = 1;
    
    static final int TRANSACTION_getCurrentInputMethodSubtype = 13;
    
    static final int TRANSACTION_getEnabledInputMethodList = 3;
    
    static final int TRANSACTION_getEnabledInputMethodSubtypeList = 4;
    
    static final int TRANSACTION_getInputMethodList = 2;
    
    static final int TRANSACTION_getInputMethodWindowVisibleHeight = 15;
    
    static final int TRANSACTION_getLastInputMethodSubtype = 5;
    
    static final int TRANSACTION_hideSoftInput = 7;
    
    static final int TRANSACTION_isInputMethodPickerShownForTest = 12;
    
    static final int TRANSACTION_removeImeSurface = 18;
    
    static final int TRANSACTION_removeImeSurfaceFromWindow = 19;
    
    static final int TRANSACTION_reportActivityView = 16;
    
    static final int TRANSACTION_reportPerceptible = 17;
    
    static final int TRANSACTION_setAdditionalInputMethodSubtypes = 14;
    
    static final int TRANSACTION_showInputMethodAndSubtypeEnablerFromClient = 11;
    
    static final int TRANSACTION_showInputMethodPickerFromClient = 9;
    
    static final int TRANSACTION_showInputMethodPickerFromSystem = 10;
    
    static final int TRANSACTION_showSoftInput = 6;
    
    static final int TRANSACTION_startInputOrWindowGainedFocus = 8;
    
    public Stub() {
      attachInterface(this, "com.android.internal.view.IInputMethodManager");
    }
    
    public static IInputMethodManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.view.IInputMethodManager");
      if (iInterface != null && iInterface instanceof IInputMethodManager)
        return (IInputMethodManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 19:
          return "removeImeSurfaceFromWindow";
        case 18:
          return "removeImeSurface";
        case 17:
          return "reportPerceptible";
        case 16:
          return "reportActivityView";
        case 15:
          return "getInputMethodWindowVisibleHeight";
        case 14:
          return "setAdditionalInputMethodSubtypes";
        case 13:
          return "getCurrentInputMethodSubtype";
        case 12:
          return "isInputMethodPickerShownForTest";
        case 11:
          return "showInputMethodAndSubtypeEnablerFromClient";
        case 10:
          return "showInputMethodPickerFromSystem";
        case 9:
          return "showInputMethodPickerFromClient";
        case 8:
          return "startInputOrWindowGainedFocus";
        case 7:
          return "hideSoftInput";
        case 6:
          return "showSoftInput";
        case 5:
          return "getLastInputMethodSubtype";
        case 4:
          return "getEnabledInputMethodSubtypeList";
        case 3:
          return "getEnabledInputMethodList";
        case 2:
          return "getInputMethodList";
        case 1:
          break;
      } 
      return "addClient";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IBinder iBinder;
      if (param1Int1 != 1598968902) {
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        IBinder iBinder1;
        float[] arrayOfFloat;
        InputMethodSubtype arrayOfInputMethodSubtype[], inputMethodSubtype2;
        String str1;
        InputBindResult inputBindResult;
        InputMethodSubtype inputMethodSubtype1;
        List<InputMethodSubtype> list;
        IInputMethodClient iInputMethodClient2;
        String str3;
        IInputMethodClient iInputMethodClient1;
        String str2;
        int m;
        IInputMethodClient iInputMethodClient4;
        IBinder iBinder2, iBinder3;
        int n, i1, i2;
        IInputContext iInputContext2;
        boolean bool = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 19:
            param1Parcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
            iBinder1 = param1Parcel1.readStrongBinder();
            removeImeSurfaceFromWindow(iBinder1);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            iBinder1.enforceInterface("com.android.internal.view.IInputMethodManager");
            removeImeSurface();
            param1Parcel2.writeNoException();
            return true;
          case 17:
            iBinder1.enforceInterface("com.android.internal.view.IInputMethodManager");
            iBinder = iBinder1.readStrongBinder();
            if (iBinder1.readInt() != 0)
              bool = true; 
            reportPerceptible(iBinder, bool);
            return true;
          case 16:
            iBinder1.enforceInterface("com.android.internal.view.IInputMethodManager");
            iInputMethodClient2 = IInputMethodClient.Stub.asInterface(iBinder1.readStrongBinder());
            param1Int1 = iBinder1.readInt();
            arrayOfFloat = iBinder1.createFloatArray();
            reportActivityView(iInputMethodClient2, param1Int1, arrayOfFloat);
            iBinder.writeNoException();
            return true;
          case 15:
            arrayOfFloat.enforceInterface("com.android.internal.view.IInputMethodManager");
            param1Int1 = getInputMethodWindowVisibleHeight();
            iBinder.writeNoException();
            iBinder.writeInt(param1Int1);
            return true;
          case 14:
            arrayOfFloat.enforceInterface("com.android.internal.view.IInputMethodManager");
            str3 = arrayOfFloat.readString();
            arrayOfInputMethodSubtype = (InputMethodSubtype[])arrayOfFloat.createTypedArray(InputMethodSubtype.CREATOR);
            setAdditionalInputMethodSubtypes(str3, arrayOfInputMethodSubtype);
            iBinder.writeNoException();
            return true;
          case 13:
            arrayOfInputMethodSubtype.enforceInterface("com.android.internal.view.IInputMethodManager");
            inputMethodSubtype2 = getCurrentInputMethodSubtype();
            iBinder.writeNoException();
            if (inputMethodSubtype2 != null) {
              iBinder.writeInt(1);
              inputMethodSubtype2.writeToParcel((Parcel)iBinder, 1);
            } else {
              iBinder.writeInt(0);
            } 
            return true;
          case 12:
            inputMethodSubtype2.enforceInterface("com.android.internal.view.IInputMethodManager");
            bool3 = isInputMethodPickerShownForTest();
            iBinder.writeNoException();
            iBinder.writeInt(bool3);
            return true;
          case 11:
            inputMethodSubtype2.enforceInterface("com.android.internal.view.IInputMethodManager");
            iInputMethodClient1 = IInputMethodClient.Stub.asInterface(inputMethodSubtype2.readStrongBinder());
            str1 = inputMethodSubtype2.readString();
            showInputMethodAndSubtypeEnablerFromClient(iInputMethodClient1, str1);
            iBinder.writeNoException();
            return true;
          case 10:
            str1.enforceInterface("com.android.internal.view.IInputMethodManager");
            iInputMethodClient1 = IInputMethodClient.Stub.asInterface(str1.readStrongBinder());
            k = str1.readInt();
            param1Int2 = str1.readInt();
            showInputMethodPickerFromSystem(iInputMethodClient1, k, param1Int2);
            iBinder.writeNoException();
            return true;
          case 9:
            str1.enforceInterface("com.android.internal.view.IInputMethodManager");
            iInputMethodClient1 = IInputMethodClient.Stub.asInterface(str1.readStrongBinder());
            k = str1.readInt();
            showInputMethodPickerFromClient(iInputMethodClient1, k);
            iBinder.writeNoException();
            return true;
          case 8:
            str1.enforceInterface("com.android.internal.view.IInputMethodManager");
            m = str1.readInt();
            iInputMethodClient4 = IInputMethodClient.Stub.asInterface(str1.readStrongBinder());
            iBinder3 = str1.readStrongBinder();
            n = str1.readInt();
            i1 = str1.readInt();
            i2 = str1.readInt();
            if (str1.readInt() != 0) {
              EditorInfo editorInfo = (EditorInfo)EditorInfo.CREATOR.createFromParcel((Parcel)str1);
            } else {
              iInputMethodClient1 = null;
            } 
            iInputContext2 = IInputContext.Stub.asInterface(str1.readStrongBinder());
            k = str1.readInt();
            param1Int2 = str1.readInt();
            inputBindResult = startInputOrWindowGainedFocus(m, iInputMethodClient4, iBinder3, n, i1, i2, (EditorInfo)iInputMethodClient1, iInputContext2, k, param1Int2);
            iBinder.writeNoException();
            if (inputBindResult != null) {
              iBinder.writeInt(1);
              inputBindResult.writeToParcel((Parcel)iBinder, 1);
            } else {
              iBinder.writeInt(0);
            } 
            return true;
          case 7:
            inputBindResult.enforceInterface("com.android.internal.view.IInputMethodManager");
            iInputMethodClient1 = IInputMethodClient.Stub.asInterface(inputBindResult.readStrongBinder());
            iBinder2 = inputBindResult.readStrongBinder();
            k = inputBindResult.readInt();
            if (inputBindResult.readInt() != 0) {
              ResultReceiver resultReceiver = (ResultReceiver)ResultReceiver.CREATOR.createFromParcel((Parcel)inputBindResult);
            } else {
              inputBindResult = null;
            } 
            bool2 = hideSoftInput(iInputMethodClient1, iBinder2, k, (ResultReceiver)inputBindResult);
            iBinder.writeNoException();
            iBinder.writeInt(bool2);
            return true;
          case 6:
            inputBindResult.enforceInterface("com.android.internal.view.IInputMethodManager");
            iInputMethodClient1 = IInputMethodClient.Stub.asInterface(inputBindResult.readStrongBinder());
            iBinder2 = inputBindResult.readStrongBinder();
            j = inputBindResult.readInt();
            if (inputBindResult.readInt() != 0) {
              ResultReceiver resultReceiver = (ResultReceiver)ResultReceiver.CREATOR.createFromParcel((Parcel)inputBindResult);
            } else {
              inputBindResult = null;
            } 
            bool1 = showSoftInput(iInputMethodClient1, iBinder2, j, (ResultReceiver)inputBindResult);
            iBinder.writeNoException();
            iBinder.writeInt(bool1);
            return true;
          case 5:
            inputBindResult.enforceInterface("com.android.internal.view.IInputMethodManager");
            inputMethodSubtype1 = getLastInputMethodSubtype();
            iBinder.writeNoException();
            if (inputMethodSubtype1 != null) {
              iBinder.writeInt(1);
              inputMethodSubtype1.writeToParcel((Parcel)iBinder, 1);
            } else {
              iBinder.writeInt(0);
            } 
            return true;
          case 4:
            inputMethodSubtype1.enforceInterface("com.android.internal.view.IInputMethodManager");
            str2 = inputMethodSubtype1.readString();
            if (inputMethodSubtype1.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            list = getEnabledInputMethodSubtypeList(str2, bool);
            iBinder.writeNoException();
            iBinder.writeTypedList(list);
            return true;
          case 3:
            list.enforceInterface("com.android.internal.view.IInputMethodManager");
            i = list.readInt();
            list = (List)getEnabledInputMethodList(i);
            iBinder.writeNoException();
            iBinder.writeTypedList(list);
            return true;
          case 2:
            list.enforceInterface("com.android.internal.view.IInputMethodManager");
            i = list.readInt();
            list = (List)getInputMethodList(i);
            iBinder.writeNoException();
            iBinder.writeTypedList(list);
            return true;
          case 1:
            break;
        } 
        list.enforceInterface("com.android.internal.view.IInputMethodManager");
        IInputMethodClient iInputMethodClient3 = IInputMethodClient.Stub.asInterface(list.readStrongBinder());
        IInputContext iInputContext1 = IInputContext.Stub.asInterface(list.readStrongBinder());
        int i = list.readInt();
        addClient(iInputMethodClient3, iInputContext1, i);
        iBinder.writeNoException();
        return true;
      } 
      iBinder.writeString("com.android.internal.view.IInputMethodManager");
      return true;
    }
    
    private static class Proxy implements IInputMethodManager {
      public static IInputMethodManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.view.IInputMethodManager";
      }
      
      public void addClient(IInputMethodClient param2IInputMethodClient, IInputContext param2IInputContext, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
          IBinder iBinder1 = null;
          if (param2IInputMethodClient != null) {
            iBinder2 = param2IInputMethodClient.asBinder();
          } else {
            iBinder2 = null;
          } 
          parcel1.writeStrongBinder(iBinder2);
          IBinder iBinder2 = iBinder1;
          if (param2IInputContext != null)
            iBinder2 = param2IInputContext.asBinder(); 
          parcel1.writeStrongBinder(iBinder2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IInputMethodManager.Stub.getDefaultImpl() != null) {
            IInputMethodManager.Stub.getDefaultImpl().addClient(param2IInputMethodClient, param2IInputContext, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<InputMethodInfo> getInputMethodList(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IInputMethodManager.Stub.getDefaultImpl() != null)
            return IInputMethodManager.Stub.getDefaultImpl().getInputMethodList(param2Int); 
          parcel2.readException();
          return parcel2.createTypedArrayList(InputMethodInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<InputMethodInfo> getEnabledInputMethodList(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IInputMethodManager.Stub.getDefaultImpl() != null)
            return IInputMethodManager.Stub.getDefaultImpl().getEnabledInputMethodList(param2Int); 
          parcel2.readException();
          return parcel2.createTypedArrayList(InputMethodInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<InputMethodSubtype> getEnabledInputMethodSubtypeList(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool1 && IInputMethodManager.Stub.getDefaultImpl() != null)
            return IInputMethodManager.Stub.getDefaultImpl().getEnabledInputMethodSubtypeList(param2String, param2Boolean); 
          parcel2.readException();
          return parcel2.createTypedArrayList(InputMethodSubtype.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public InputMethodSubtype getLastInputMethodSubtype() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          InputMethodSubtype inputMethodSubtype;
          parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IInputMethodManager.Stub.getDefaultImpl() != null) {
            inputMethodSubtype = IInputMethodManager.Stub.getDefaultImpl().getLastInputMethodSubtype();
            return inputMethodSubtype;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            inputMethodSubtype = (InputMethodSubtype)InputMethodSubtype.CREATOR.createFromParcel(parcel2);
          } else {
            inputMethodSubtype = null;
          } 
          return inputMethodSubtype;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean showSoftInput(IInputMethodClient param2IInputMethodClient, IBinder param2IBinder, int param2Int, ResultReceiver param2ResultReceiver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
          if (param2IInputMethodClient != null) {
            iBinder = param2IInputMethodClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2ResultReceiver != null) {
            parcel1.writeInt(1);
            param2ResultReceiver.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IInputMethodManager.Stub.getDefaultImpl() != null) {
            bool1 = IInputMethodManager.Stub.getDefaultImpl().showSoftInput(param2IInputMethodClient, param2IBinder, param2Int, param2ResultReceiver);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hideSoftInput(IInputMethodClient param2IInputMethodClient, IBinder param2IBinder, int param2Int, ResultReceiver param2ResultReceiver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
          if (param2IInputMethodClient != null) {
            iBinder = param2IInputMethodClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2ResultReceiver != null) {
            parcel1.writeInt(1);
            param2ResultReceiver.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IInputMethodManager.Stub.getDefaultImpl() != null) {
            bool1 = IInputMethodManager.Stub.getDefaultImpl().hideSoftInput(param2IInputMethodClient, param2IBinder, param2Int, param2ResultReceiver);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public InputBindResult startInputOrWindowGainedFocus(int param2Int1, IInputMethodClient param2IInputMethodClient, IBinder param2IBinder, int param2Int2, int param2Int3, int param2Int4, EditorInfo param2EditorInfo, IInputContext param2IInputContext, int param2Int5, int param2Int6) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
          try {
            parcel1.writeInt(param2Int1);
            IBinder iBinder1 = null;
            if (param2IInputMethodClient != null) {
              iBinder2 = param2IInputMethodClient.asBinder();
            } else {
              iBinder2 = null;
            } 
            parcel1.writeStrongBinder(iBinder2);
            parcel1.writeStrongBinder(param2IBinder);
            parcel1.writeInt(param2Int2);
            parcel1.writeInt(param2Int3);
            parcel1.writeInt(param2Int4);
            if (param2EditorInfo != null) {
              parcel1.writeInt(1);
              param2EditorInfo.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            IBinder iBinder2 = iBinder1;
            if (param2IInputContext != null)
              iBinder2 = param2IInputContext.asBinder(); 
            parcel1.writeStrongBinder(iBinder2);
            parcel1.writeInt(param2Int5);
            parcel1.writeInt(param2Int6);
            boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
            if (!bool && IInputMethodManager.Stub.getDefaultImpl() != null) {
              InputBindResult inputBindResult = IInputMethodManager.Stub.getDefaultImpl().startInputOrWindowGainedFocus(param2Int1, param2IInputMethodClient, param2IBinder, param2Int2, param2Int3, param2Int4, param2EditorInfo, param2IInputContext, param2Int5, param2Int6);
              parcel2.recycle();
              parcel1.recycle();
              return inputBindResult;
            } 
            parcel2.readException();
            if (parcel2.readInt() != 0) {
              InputBindResult inputBindResult = (InputBindResult)InputBindResult.CREATOR.createFromParcel(parcel2);
            } else {
              param2IInputMethodClient = null;
            } 
            parcel2.recycle();
            parcel1.recycle();
            return (InputBindResult)param2IInputMethodClient;
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IInputMethodClient;
      }
      
      public void showInputMethodPickerFromClient(IInputMethodClient param2IInputMethodClient, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
          if (param2IInputMethodClient != null) {
            iBinder = param2IInputMethodClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IInputMethodManager.Stub.getDefaultImpl() != null) {
            IInputMethodManager.Stub.getDefaultImpl().showInputMethodPickerFromClient(param2IInputMethodClient, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void showInputMethodPickerFromSystem(IInputMethodClient param2IInputMethodClient, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
          if (param2IInputMethodClient != null) {
            iBinder = param2IInputMethodClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IInputMethodManager.Stub.getDefaultImpl() != null) {
            IInputMethodManager.Stub.getDefaultImpl().showInputMethodPickerFromSystem(param2IInputMethodClient, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void showInputMethodAndSubtypeEnablerFromClient(IInputMethodClient param2IInputMethodClient, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
          if (param2IInputMethodClient != null) {
            iBinder = param2IInputMethodClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IInputMethodManager.Stub.getDefaultImpl() != null) {
            IInputMethodManager.Stub.getDefaultImpl().showInputMethodAndSubtypeEnablerFromClient(param2IInputMethodClient, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isInputMethodPickerShownForTest() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(12, parcel1, parcel2, 0);
          if (!bool2 && IInputMethodManager.Stub.getDefaultImpl() != null) {
            bool1 = IInputMethodManager.Stub.getDefaultImpl().isInputMethodPickerShownForTest();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public InputMethodSubtype getCurrentInputMethodSubtype() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          InputMethodSubtype inputMethodSubtype;
          parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IInputMethodManager.Stub.getDefaultImpl() != null) {
            inputMethodSubtype = IInputMethodManager.Stub.getDefaultImpl().getCurrentInputMethodSubtype();
            return inputMethodSubtype;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            inputMethodSubtype = (InputMethodSubtype)InputMethodSubtype.CREATOR.createFromParcel(parcel2);
          } else {
            inputMethodSubtype = null;
          } 
          return inputMethodSubtype;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAdditionalInputMethodSubtypes(String param2String, InputMethodSubtype[] param2ArrayOfInputMethodSubtype) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
          parcel1.writeString(param2String);
          parcel1.writeTypedArray((Parcelable[])param2ArrayOfInputMethodSubtype, 0);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IInputMethodManager.Stub.getDefaultImpl() != null) {
            IInputMethodManager.Stub.getDefaultImpl().setAdditionalInputMethodSubtypes(param2String, param2ArrayOfInputMethodSubtype);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getInputMethodWindowVisibleHeight() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IInputMethodManager.Stub.getDefaultImpl() != null)
            return IInputMethodManager.Stub.getDefaultImpl().getInputMethodWindowVisibleHeight(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportActivityView(IInputMethodClient param2IInputMethodClient, int param2Int, float[] param2ArrayOffloat) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
          if (param2IInputMethodClient != null) {
            iBinder = param2IInputMethodClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          parcel1.writeFloatArray(param2ArrayOffloat);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IInputMethodManager.Stub.getDefaultImpl() != null) {
            IInputMethodManager.Stub.getDefaultImpl().reportActivityView(param2IInputMethodClient, param2Int, param2ArrayOffloat);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportPerceptible(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
          parcel.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(17, parcel, null, 1);
          if (!bool1 && IInputMethodManager.Stub.getDefaultImpl() != null) {
            IInputMethodManager.Stub.getDefaultImpl().reportPerceptible(param2IBinder, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeImeSurface() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IInputMethodManager.Stub.getDefaultImpl() != null) {
            IInputMethodManager.Stub.getDefaultImpl().removeImeSurface();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeImeSurfaceFromWindow(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IInputMethodManager.Stub.getDefaultImpl() != null) {
            IInputMethodManager.Stub.getDefaultImpl().removeImeSurfaceFromWindow(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInputMethodManager param1IInputMethodManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInputMethodManager != null) {
          Proxy.sDefaultImpl = param1IInputMethodManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInputMethodManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
