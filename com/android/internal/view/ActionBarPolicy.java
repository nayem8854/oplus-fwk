package com.android.internal.view;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import com.android.internal.R;

public class ActionBarPolicy {
  private Context mContext;
  
  public static ActionBarPolicy get(Context paramContext) {
    return new ActionBarPolicy(paramContext);
  }
  
  private ActionBarPolicy(Context paramContext) {
    this.mContext = paramContext;
  }
  
  public int getMaxActionButtons() {
    Configuration configuration = this.mContext.getResources().getConfiguration();
    int i = configuration.screenWidthDp;
    int j = configuration.screenHeightDp;
    int k = configuration.smallestScreenWidthDp;
    if (k > 600 || (i > 960 && j > 720) || (i > 720 && j > 960))
      return 5; 
    if (i >= 500 || (i > 640 && j > 480) || (i > 480 && j > 640))
      return 4; 
    if (i >= 360)
      return 3; 
    return 2;
  }
  
  public boolean showsOverflowMenuButton() {
    return true;
  }
  
  public int getEmbeddedMenuWidthLimit() {
    return (this.mContext.getResources().getDisplayMetrics()).widthPixels / 2;
  }
  
  public boolean hasEmbeddedTabs() {
    int i = (this.mContext.getApplicationInfo()).targetSdkVersion;
    if (i >= 16)
      return this.mContext.getResources().getBoolean(17891335); 
    Configuration configuration = this.mContext.getResources().getConfiguration();
    i = configuration.screenWidthDp;
    int j = configuration.screenHeightDp;
    return (configuration.orientation == 2 || i >= 480 || (i >= 640 && j >= 480));
  }
  
  public int getTabContainerHeight() {
    TypedArray typedArray = this.mContext.obtainStyledAttributes(null, R.styleable.ActionBar, 16843470, 0);
    int i = typedArray.getLayoutDimension(4, 0);
    Resources resources = this.mContext.getResources();
    int j = i;
    if (!hasEmbeddedTabs()) {
      j = resources.getDimensionPixelSize(17104922);
      j = Math.min(i, j);
    } 
    typedArray.recycle();
    return j;
  }
  
  public boolean enableHomeButtonByDefault() {
    boolean bool;
    if ((this.mContext.getApplicationInfo()).targetSdkVersion < 14) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getStackedTabMaxWidth() {
    return this.mContext.getResources().getDimensionPixelSize(17104923);
  }
}
