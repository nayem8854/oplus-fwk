package com.android.internal.view;

import android.graphics.Rect;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CursorAnchorInfo;
import android.view.inputmethod.ExtractedText;

public interface IInputMethodSession extends IInterface {
  void appPrivateCommand(String paramString, Bundle paramBundle) throws RemoteException;
  
  void displayCompletions(CompletionInfo[] paramArrayOfCompletionInfo) throws RemoteException;
  
  void finishSession() throws RemoteException;
  
  void notifyImeHidden() throws RemoteException;
  
  void removeImeSurface() throws RemoteException;
  
  void toggleSoftInput(int paramInt1, int paramInt2) throws RemoteException;
  
  void updateCursor(Rect paramRect) throws RemoteException;
  
  void updateCursorAnchorInfo(CursorAnchorInfo paramCursorAnchorInfo) throws RemoteException;
  
  void updateExtractedText(int paramInt, ExtractedText paramExtractedText) throws RemoteException;
  
  void updateSelection(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) throws RemoteException;
  
  void viewClicked(boolean paramBoolean) throws RemoteException;
  
  class Default implements IInputMethodSession {
    public void updateExtractedText(int param1Int, ExtractedText param1ExtractedText) throws RemoteException {}
    
    public void updateSelection(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6) throws RemoteException {}
    
    public void viewClicked(boolean param1Boolean) throws RemoteException {}
    
    public void updateCursor(Rect param1Rect) throws RemoteException {}
    
    public void displayCompletions(CompletionInfo[] param1ArrayOfCompletionInfo) throws RemoteException {}
    
    public void appPrivateCommand(String param1String, Bundle param1Bundle) throws RemoteException {}
    
    public void toggleSoftInput(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void finishSession() throws RemoteException {}
    
    public void updateCursorAnchorInfo(CursorAnchorInfo param1CursorAnchorInfo) throws RemoteException {}
    
    public void notifyImeHidden() throws RemoteException {}
    
    public void removeImeSurface() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInputMethodSession {
    private static final String DESCRIPTOR = "com.android.internal.view.IInputMethodSession";
    
    static final int TRANSACTION_appPrivateCommand = 6;
    
    static final int TRANSACTION_displayCompletions = 5;
    
    static final int TRANSACTION_finishSession = 8;
    
    static final int TRANSACTION_notifyImeHidden = 10;
    
    static final int TRANSACTION_removeImeSurface = 11;
    
    static final int TRANSACTION_toggleSoftInput = 7;
    
    static final int TRANSACTION_updateCursor = 4;
    
    static final int TRANSACTION_updateCursorAnchorInfo = 9;
    
    static final int TRANSACTION_updateExtractedText = 1;
    
    static final int TRANSACTION_updateSelection = 2;
    
    static final int TRANSACTION_viewClicked = 3;
    
    public Stub() {
      attachInterface(this, "com.android.internal.view.IInputMethodSession");
    }
    
    public static IInputMethodSession asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.view.IInputMethodSession");
      if (iInterface != null && iInterface instanceof IInputMethodSession)
        return (IInputMethodSession)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 11:
          return "removeImeSurface";
        case 10:
          return "notifyImeHidden";
        case 9:
          return "updateCursorAnchorInfo";
        case 8:
          return "finishSession";
        case 7:
          return "toggleSoftInput";
        case 6:
          return "appPrivateCommand";
        case 5:
          return "displayCompletions";
        case 4:
          return "updateCursor";
        case 3:
          return "viewClicked";
        case 2:
          return "updateSelection";
        case 1:
          break;
      } 
      return "updateExtractedText";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        CompletionInfo[] arrayOfCompletionInfo;
        boolean bool;
        int i, j, k, m;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 11:
            param1Parcel1.enforceInterface("com.android.internal.view.IInputMethodSession");
            removeImeSurface();
            return true;
          case 10:
            param1Parcel1.enforceInterface("com.android.internal.view.IInputMethodSession");
            notifyImeHidden();
            return true;
          case 9:
            param1Parcel1.enforceInterface("com.android.internal.view.IInputMethodSession");
            if (param1Parcel1.readInt() != 0) {
              CursorAnchorInfo cursorAnchorInfo = (CursorAnchorInfo)CursorAnchorInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            updateCursorAnchorInfo((CursorAnchorInfo)param1Parcel1);
            return true;
          case 8:
            param1Parcel1.enforceInterface("com.android.internal.view.IInputMethodSession");
            finishSession();
            return true;
          case 7:
            param1Parcel1.enforceInterface("com.android.internal.view.IInputMethodSession");
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            toggleSoftInput(param1Int2, param1Int1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("com.android.internal.view.IInputMethodSession");
            str = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            appPrivateCommand(str, (Bundle)param1Parcel1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("com.android.internal.view.IInputMethodSession");
            arrayOfCompletionInfo = (CompletionInfo[])param1Parcel1.createTypedArray(CompletionInfo.CREATOR);
            displayCompletions(arrayOfCompletionInfo);
            return true;
          case 4:
            arrayOfCompletionInfo.enforceInterface("com.android.internal.view.IInputMethodSession");
            if (arrayOfCompletionInfo.readInt() != 0) {
              Rect rect = (Rect)Rect.CREATOR.createFromParcel((Parcel)arrayOfCompletionInfo);
            } else {
              arrayOfCompletionInfo = null;
            } 
            updateCursor((Rect)arrayOfCompletionInfo);
            return true;
          case 3:
            arrayOfCompletionInfo.enforceInterface("com.android.internal.view.IInputMethodSession");
            if (arrayOfCompletionInfo.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            viewClicked(bool);
            return true;
          case 2:
            arrayOfCompletionInfo.enforceInterface("com.android.internal.view.IInputMethodSession");
            i = arrayOfCompletionInfo.readInt();
            j = arrayOfCompletionInfo.readInt();
            k = arrayOfCompletionInfo.readInt();
            m = arrayOfCompletionInfo.readInt();
            param1Int2 = arrayOfCompletionInfo.readInt();
            param1Int1 = arrayOfCompletionInfo.readInt();
            updateSelection(i, j, k, m, param1Int2, param1Int1);
            return true;
          case 1:
            break;
        } 
        arrayOfCompletionInfo.enforceInterface("com.android.internal.view.IInputMethodSession");
        param1Int1 = arrayOfCompletionInfo.readInt();
        if (arrayOfCompletionInfo.readInt() != 0) {
          ExtractedText extractedText = (ExtractedText)ExtractedText.CREATOR.createFromParcel((Parcel)arrayOfCompletionInfo);
        } else {
          arrayOfCompletionInfo = null;
        } 
        updateExtractedText(param1Int1, (ExtractedText)arrayOfCompletionInfo);
        return true;
      } 
      str.writeString("com.android.internal.view.IInputMethodSession");
      return true;
    }
    
    private static class Proxy implements IInputMethodSession {
      public static IInputMethodSession sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.view.IInputMethodSession";
      }
      
      public void updateExtractedText(int param2Int, ExtractedText param2ExtractedText) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
          parcel.writeInt(param2Int);
          if (param2ExtractedText != null) {
            parcel.writeInt(1);
            param2ExtractedText.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IInputMethodSession.Stub.getDefaultImpl() != null) {
            IInputMethodSession.Stub.getDefaultImpl().updateExtractedText(param2Int, param2ExtractedText);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void updateSelection(int param2Int1, int param2Int2, int param2Int3, int param2Int4, int param2Int5, int param2Int6) throws RemoteException {
        Exception exception;
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
          try {
            parcel.writeInt(param2Int1);
            try {
              parcel.writeInt(param2Int2);
              try {
                parcel.writeInt(param2Int3);
                try {
                  parcel.writeInt(param2Int4);
                  try {
                    parcel.writeInt(param2Int5);
                    try {
                      parcel.writeInt(param2Int6);
                      try {
                        boolean bool = this.mRemote.transact(2, parcel, null, 1);
                        if (!bool && IInputMethodSession.Stub.getDefaultImpl() != null) {
                          IInputMethodSession.Stub.getDefaultImpl().updateSelection(param2Int1, param2Int2, param2Int3, param2Int4, param2Int5, param2Int6);
                          parcel.recycle();
                          return;
                        } 
                        parcel.recycle();
                        return;
                      } finally {}
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw exception;
      }
      
      public void viewClicked(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel, null, 1);
          if (!bool1 && IInputMethodSession.Stub.getDefaultImpl() != null) {
            IInputMethodSession.Stub.getDefaultImpl().viewClicked(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void updateCursor(Rect param2Rect) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
          if (param2Rect != null) {
            parcel.writeInt(1);
            param2Rect.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IInputMethodSession.Stub.getDefaultImpl() != null) {
            IInputMethodSession.Stub.getDefaultImpl().updateCursor(param2Rect);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void displayCompletions(CompletionInfo[] param2ArrayOfCompletionInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
          parcel.writeTypedArray((Parcelable[])param2ArrayOfCompletionInfo, 0);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IInputMethodSession.Stub.getDefaultImpl() != null) {
            IInputMethodSession.Stub.getDefaultImpl().displayCompletions(param2ArrayOfCompletionInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void appPrivateCommand(String param2String, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
          parcel.writeString(param2String);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IInputMethodSession.Stub.getDefaultImpl() != null) {
            IInputMethodSession.Stub.getDefaultImpl().appPrivateCommand(param2String, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void toggleSoftInput(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IInputMethodSession.Stub.getDefaultImpl() != null) {
            IInputMethodSession.Stub.getDefaultImpl().toggleSoftInput(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void finishSession() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IInputMethodSession.Stub.getDefaultImpl() != null) {
            IInputMethodSession.Stub.getDefaultImpl().finishSession();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void updateCursorAnchorInfo(CursorAnchorInfo param2CursorAnchorInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
          if (param2CursorAnchorInfo != null) {
            parcel.writeInt(1);
            param2CursorAnchorInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IInputMethodSession.Stub.getDefaultImpl() != null) {
            IInputMethodSession.Stub.getDefaultImpl().updateCursorAnchorInfo(param2CursorAnchorInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyImeHidden() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IInputMethodSession.Stub.getDefaultImpl() != null) {
            IInputMethodSession.Stub.getDefaultImpl().notifyImeHidden();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeImeSurface() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IInputMethodSession.Stub.getDefaultImpl() != null) {
            IInputMethodSession.Stub.getDefaultImpl().removeImeSurface();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInputMethodSession param1IInputMethodSession) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInputMethodSession != null) {
          Proxy.sDefaultImpl = param1IInputMethodSession;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInputMethodSession getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
