package com.android.internal.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.view.InputChannel;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputBinding;
import android.view.inputmethod.InputMethodSubtype;
import com.android.internal.inputmethod.IInputMethodPrivilegedOperations;

public interface IInputMethod extends IInterface {
  void bindInput(InputBinding paramInputBinding) throws RemoteException;
  
  void changeInputMethodSubtype(InputMethodSubtype paramInputMethodSubtype) throws RemoteException;
  
  void createSession(InputChannel paramInputChannel, IInputSessionCallback paramIInputSessionCallback) throws RemoteException;
  
  void hideSoftInput(IBinder paramIBinder, int paramInt, ResultReceiver paramResultReceiver) throws RemoteException;
  
  void initializeInternal(IBinder paramIBinder, int paramInt, IInputMethodPrivilegedOperations paramIInputMethodPrivilegedOperations) throws RemoteException;
  
  void onCreateInlineSuggestionsRequest(InlineSuggestionsRequestInfo paramInlineSuggestionsRequestInfo, IInlineSuggestionsRequestCallback paramIInlineSuggestionsRequestCallback) throws RemoteException;
  
  void revokeSession(IInputMethodSession paramIInputMethodSession) throws RemoteException;
  
  void setSessionEnabled(IInputMethodSession paramIInputMethodSession, boolean paramBoolean) throws RemoteException;
  
  void showSoftInput(IBinder paramIBinder, int paramInt, ResultReceiver paramResultReceiver) throws RemoteException;
  
  void startInput(IBinder paramIBinder, IInputContext paramIInputContext, int paramInt, EditorInfo paramEditorInfo, boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void unbindInput() throws RemoteException;
  
  class Default implements IInputMethod {
    public void initializeInternal(IBinder param1IBinder, int param1Int, IInputMethodPrivilegedOperations param1IInputMethodPrivilegedOperations) throws RemoteException {}
    
    public void onCreateInlineSuggestionsRequest(InlineSuggestionsRequestInfo param1InlineSuggestionsRequestInfo, IInlineSuggestionsRequestCallback param1IInlineSuggestionsRequestCallback) throws RemoteException {}
    
    public void bindInput(InputBinding param1InputBinding) throws RemoteException {}
    
    public void unbindInput() throws RemoteException {}
    
    public void startInput(IBinder param1IBinder, IInputContext param1IInputContext, int param1Int, EditorInfo param1EditorInfo, boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public void createSession(InputChannel param1InputChannel, IInputSessionCallback param1IInputSessionCallback) throws RemoteException {}
    
    public void setSessionEnabled(IInputMethodSession param1IInputMethodSession, boolean param1Boolean) throws RemoteException {}
    
    public void revokeSession(IInputMethodSession param1IInputMethodSession) throws RemoteException {}
    
    public void showSoftInput(IBinder param1IBinder, int param1Int, ResultReceiver param1ResultReceiver) throws RemoteException {}
    
    public void hideSoftInput(IBinder param1IBinder, int param1Int, ResultReceiver param1ResultReceiver) throws RemoteException {}
    
    public void changeInputMethodSubtype(InputMethodSubtype param1InputMethodSubtype) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInputMethod {
    private static final String DESCRIPTOR = "com.android.internal.view.IInputMethod";
    
    static final int TRANSACTION_bindInput = 3;
    
    static final int TRANSACTION_changeInputMethodSubtype = 11;
    
    static final int TRANSACTION_createSession = 6;
    
    static final int TRANSACTION_hideSoftInput = 10;
    
    static final int TRANSACTION_initializeInternal = 1;
    
    static final int TRANSACTION_onCreateInlineSuggestionsRequest = 2;
    
    static final int TRANSACTION_revokeSession = 8;
    
    static final int TRANSACTION_setSessionEnabled = 7;
    
    static final int TRANSACTION_showSoftInput = 9;
    
    static final int TRANSACTION_startInput = 5;
    
    static final int TRANSACTION_unbindInput = 4;
    
    public Stub() {
      attachInterface(this, "com.android.internal.view.IInputMethod");
    }
    
    public static IInputMethod asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.view.IInputMethod");
      if (iInterface != null && iInterface instanceof IInputMethod)
        return (IInputMethod)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 11:
          return "changeInputMethodSubtype";
        case 10:
          return "hideSoftInput";
        case 9:
          return "showSoftInput";
        case 8:
          return "revokeSession";
        case 7:
          return "setSessionEnabled";
        case 6:
          return "createSession";
        case 5:
          return "startInput";
        case 4:
          return "unbindInput";
        case 3:
          return "bindInput";
        case 2:
          return "onCreateInlineSuggestionsRequest";
        case 1:
          break;
      } 
      return "initializeInternal";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IBinder iBinder;
      if (param1Int1 != 1598968902) {
        IInputMethodSession iInputMethodSession1;
        IInputSessionCallback iInputSessionCallback;
        IInlineSuggestionsRequestCallback iInlineSuggestionsRequestCallback;
        IBinder iBinder1;
        IInputMethodSession iInputMethodSession2;
        IBinder iBinder2;
        IInputContext iInputContext;
        boolean bool2, bool1 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 11:
            param1Parcel1.enforceInterface("com.android.internal.view.IInputMethod");
            if (param1Parcel1.readInt() != 0) {
              InputMethodSubtype inputMethodSubtype = (InputMethodSubtype)InputMethodSubtype.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            changeInputMethodSubtype((InputMethodSubtype)param1Parcel1);
            return true;
          case 10:
            param1Parcel1.enforceInterface("com.android.internal.view.IInputMethod");
            iBinder1 = param1Parcel1.readStrongBinder();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              ResultReceiver resultReceiver = (ResultReceiver)ResultReceiver.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            hideSoftInput(iBinder1, param1Int1, (ResultReceiver)param1Parcel1);
            return true;
          case 9:
            param1Parcel1.enforceInterface("com.android.internal.view.IInputMethod");
            iBinder1 = param1Parcel1.readStrongBinder();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              ResultReceiver resultReceiver = (ResultReceiver)ResultReceiver.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            showSoftInput(iBinder1, param1Int1, (ResultReceiver)param1Parcel1);
            return true;
          case 8:
            param1Parcel1.enforceInterface("com.android.internal.view.IInputMethod");
            iInputMethodSession1 = IInputMethodSession.Stub.asInterface(param1Parcel1.readStrongBinder());
            revokeSession(iInputMethodSession1);
            return true;
          case 7:
            iInputMethodSession1.enforceInterface("com.android.internal.view.IInputMethod");
            iInputMethodSession2 = IInputMethodSession.Stub.asInterface(iInputMethodSession1.readStrongBinder());
            if (iInputMethodSession1.readInt() != 0)
              bool1 = true; 
            setSessionEnabled(iInputMethodSession2, bool1);
            return true;
          case 6:
            iInputMethodSession1.enforceInterface("com.android.internal.view.IInputMethod");
            if (iInputMethodSession1.readInt() != 0) {
              InputChannel inputChannel = (InputChannel)InputChannel.CREATOR.createFromParcel((Parcel)iInputMethodSession1);
            } else {
              iInputMethodSession2 = null;
            } 
            iInputSessionCallback = IInputSessionCallback.Stub.asInterface(iInputMethodSession1.readStrongBinder());
            createSession((InputChannel)iInputMethodSession2, iInputSessionCallback);
            return true;
          case 5:
            iInputSessionCallback.enforceInterface("com.android.internal.view.IInputMethod");
            iBinder2 = iInputSessionCallback.readStrongBinder();
            iInputContext = IInputContext.Stub.asInterface(iInputSessionCallback.readStrongBinder());
            param1Int1 = iInputSessionCallback.readInt();
            if (iInputSessionCallback.readInt() != 0) {
              EditorInfo editorInfo = (EditorInfo)EditorInfo.CREATOR.createFromParcel((Parcel)iInputSessionCallback);
            } else {
              iInputMethodSession2 = null;
            } 
            if (iInputSessionCallback.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            if (iInputSessionCallback.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            startInput(iBinder2, iInputContext, param1Int1, (EditorInfo)iInputMethodSession2, bool1, bool2);
            return true;
          case 4:
            iInputSessionCallback.enforceInterface("com.android.internal.view.IInputMethod");
            unbindInput();
            return true;
          case 3:
            iInputSessionCallback.enforceInterface("com.android.internal.view.IInputMethod");
            if (iInputSessionCallback.readInt() != 0) {
              InputBinding inputBinding = (InputBinding)InputBinding.CREATOR.createFromParcel((Parcel)iInputSessionCallback);
            } else {
              iInputSessionCallback = null;
            } 
            bindInput((InputBinding)iInputSessionCallback);
            return true;
          case 2:
            iInputSessionCallback.enforceInterface("com.android.internal.view.IInputMethod");
            if (iInputSessionCallback.readInt() != 0) {
              InlineSuggestionsRequestInfo inlineSuggestionsRequestInfo = (InlineSuggestionsRequestInfo)InlineSuggestionsRequestInfo.CREATOR.createFromParcel((Parcel)iInputSessionCallback);
            } else {
              iInputMethodSession2 = null;
            } 
            iInlineSuggestionsRequestCallback = IInlineSuggestionsRequestCallback.Stub.asInterface(iInputSessionCallback.readStrongBinder());
            onCreateInlineSuggestionsRequest((InlineSuggestionsRequestInfo)iInputMethodSession2, iInlineSuggestionsRequestCallback);
            return true;
          case 1:
            break;
        } 
        iInlineSuggestionsRequestCallback.enforceInterface("com.android.internal.view.IInputMethod");
        iBinder = iInlineSuggestionsRequestCallback.readStrongBinder();
        param1Int1 = iInlineSuggestionsRequestCallback.readInt();
        IInputMethodPrivilegedOperations iInputMethodPrivilegedOperations = IInputMethodPrivilegedOperations.Stub.asInterface(iInlineSuggestionsRequestCallback.readStrongBinder());
        initializeInternal(iBinder, param1Int1, iInputMethodPrivilegedOperations);
        return true;
      } 
      iBinder.writeString("com.android.internal.view.IInputMethod");
      return true;
    }
    
    private static class Proxy implements IInputMethod {
      public static IInputMethod sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.view.IInputMethod";
      }
      
      public void initializeInternal(IBinder param2IBinder, int param2Int, IInputMethodPrivilegedOperations param2IInputMethodPrivilegedOperations) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeInt(param2Int);
          if (param2IInputMethodPrivilegedOperations != null) {
            iBinder = param2IInputMethodPrivilegedOperations.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IInputMethod.Stub.getDefaultImpl() != null) {
            IInputMethod.Stub.getDefaultImpl().initializeInternal(param2IBinder, param2Int, param2IInputMethodPrivilegedOperations);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCreateInlineSuggestionsRequest(InlineSuggestionsRequestInfo param2InlineSuggestionsRequestInfo, IInlineSuggestionsRequestCallback param2IInlineSuggestionsRequestCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
          if (param2InlineSuggestionsRequestInfo != null) {
            parcel.writeInt(1);
            param2InlineSuggestionsRequestInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IInlineSuggestionsRequestCallback != null) {
            iBinder = param2IInlineSuggestionsRequestCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IInputMethod.Stub.getDefaultImpl() != null) {
            IInputMethod.Stub.getDefaultImpl().onCreateInlineSuggestionsRequest(param2InlineSuggestionsRequestInfo, param2IInlineSuggestionsRequestCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void bindInput(InputBinding param2InputBinding) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
          if (param2InputBinding != null) {
            parcel.writeInt(1);
            param2InputBinding.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IInputMethod.Stub.getDefaultImpl() != null) {
            IInputMethod.Stub.getDefaultImpl().bindInput(param2InputBinding);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void unbindInput() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IInputMethod.Stub.getDefaultImpl() != null) {
            IInputMethod.Stub.getDefaultImpl().unbindInput();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void startInput(IBinder param2IBinder, IInputContext param2IInputContext, int param2Int, EditorInfo param2EditorInfo, boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
          try {
            IBinder iBinder;
            parcel.writeStrongBinder(param2IBinder);
            if (param2IInputContext != null) {
              iBinder = param2IInputContext.asBinder();
            } else {
              iBinder = null;
            } 
            parcel.writeStrongBinder(iBinder);
            try {
              parcel.writeInt(param2Int);
              boolean bool1 = false;
              if (param2EditorInfo != null) {
                parcel.writeInt(1);
                param2EditorInfo.writeToParcel(parcel, 0);
              } else {
                parcel.writeInt(0);
              } 
              if (param2Boolean1) {
                bool2 = true;
              } else {
                bool2 = false;
              } 
              parcel.writeInt(bool2);
              boolean bool2 = bool1;
              if (param2Boolean2)
                bool2 = true; 
              parcel.writeInt(bool2);
              try {
                boolean bool = this.mRemote.transact(5, parcel, null, 1);
                if (!bool && IInputMethod.Stub.getDefaultImpl() != null) {
                  IInputMethod.Stub.getDefaultImpl().startInput(param2IBinder, param2IInputContext, param2Int, param2EditorInfo, param2Boolean1, param2Boolean2);
                  parcel.recycle();
                  return;
                } 
                parcel.recycle();
                return;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2IBinder;
      }
      
      public void createSession(InputChannel param2InputChannel, IInputSessionCallback param2IInputSessionCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
          if (param2InputChannel != null) {
            parcel.writeInt(1);
            param2InputChannel.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IInputSessionCallback != null) {
            iBinder = param2IInputSessionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IInputMethod.Stub.getDefaultImpl() != null) {
            IInputMethod.Stub.getDefaultImpl().createSession(param2InputChannel, param2IInputSessionCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setSessionEnabled(IInputMethodSession param2IInputMethodSession, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
          if (param2IInputMethodSession != null) {
            iBinder = param2IInputMethodSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(7, parcel, null, 1);
          if (!bool1 && IInputMethod.Stub.getDefaultImpl() != null) {
            IInputMethod.Stub.getDefaultImpl().setSessionEnabled(param2IInputMethodSession, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void revokeSession(IInputMethodSession param2IInputMethodSession) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
          if (param2IInputMethodSession != null) {
            iBinder = param2IInputMethodSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IInputMethod.Stub.getDefaultImpl() != null) {
            IInputMethod.Stub.getDefaultImpl().revokeSession(param2IInputMethodSession);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void showSoftInput(IBinder param2IBinder, int param2Int, ResultReceiver param2ResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeInt(param2Int);
          if (param2ResultReceiver != null) {
            parcel.writeInt(1);
            param2ResultReceiver.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IInputMethod.Stub.getDefaultImpl() != null) {
            IInputMethod.Stub.getDefaultImpl().showSoftInput(param2IBinder, param2Int, param2ResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void hideSoftInput(IBinder param2IBinder, int param2Int, ResultReceiver param2ResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeInt(param2Int);
          if (param2ResultReceiver != null) {
            parcel.writeInt(1);
            param2ResultReceiver.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IInputMethod.Stub.getDefaultImpl() != null) {
            IInputMethod.Stub.getDefaultImpl().hideSoftInput(param2IBinder, param2Int, param2ResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void changeInputMethodSubtype(InputMethodSubtype param2InputMethodSubtype) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
          if (param2InputMethodSubtype != null) {
            parcel.writeInt(1);
            param2InputMethodSubtype.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IInputMethod.Stub.getDefaultImpl() != null) {
            IInputMethod.Stub.getDefaultImpl().changeInputMethodSubtype(param2InputMethodSubtype);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInputMethod param1IInputMethod) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInputMethod != null) {
          Proxy.sDefaultImpl = param1IInputMethod;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInputMethod getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
