package com.android.internal.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IInputSessionCallback extends IInterface {
  void sessionCreated(IInputMethodSession paramIInputMethodSession) throws RemoteException;
  
  class Default implements IInputSessionCallback {
    public void sessionCreated(IInputMethodSession param1IInputMethodSession) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInputSessionCallback {
    private static final String DESCRIPTOR = "com.android.internal.view.IInputSessionCallback";
    
    static final int TRANSACTION_sessionCreated = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.view.IInputSessionCallback");
    }
    
    public static IInputSessionCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.view.IInputSessionCallback");
      if (iInterface != null && iInterface instanceof IInputSessionCallback)
        return (IInputSessionCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "sessionCreated";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.view.IInputSessionCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.view.IInputSessionCallback");
      IInputMethodSession iInputMethodSession = IInputMethodSession.Stub.asInterface(param1Parcel1.readStrongBinder());
      sessionCreated(iInputMethodSession);
      return true;
    }
    
    private static class Proxy implements IInputSessionCallback {
      public static IInputSessionCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.view.IInputSessionCallback";
      }
      
      public void sessionCreated(IInputMethodSession param2IInputMethodSession) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.view.IInputSessionCallback");
          if (param2IInputMethodSession != null) {
            iBinder = param2IInputMethodSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IInputSessionCallback.Stub.getDefaultImpl() != null) {
            IInputSessionCallback.Stub.getDefaultImpl().sessionCreated(param2IInputMethodSession);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInputSessionCallback param1IInputSessionCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInputSessionCallback != null) {
          Proxy.sDefaultImpl = param1IInputSessionCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInputSessionCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
