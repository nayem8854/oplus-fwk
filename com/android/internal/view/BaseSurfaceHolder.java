package com.android.internal.view;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.SystemClock;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

public abstract class BaseSurfaceHolder implements SurfaceHolder {
  public final ArrayList<SurfaceHolder.Callback> mCallbacks = new ArrayList<>();
  
  public final ReentrantLock mSurfaceLock = new ReentrantLock();
  
  public Surface mSurface = new Surface();
  
  int mRequestedWidth = -1;
  
  int mRequestedHeight = -1;
  
  protected int mRequestedFormat = -1;
  
  int mRequestedType = -1;
  
  long mLastLockTime = 0L;
  
  int mType = -1;
  
  final Rect mSurfaceFrame = new Rect();
  
  static final boolean DEBUG = false;
  
  private static final String TAG = "BaseSurfaceHolder";
  
  SurfaceHolder.Callback[] mGottenCallbacks;
  
  boolean mHaveGottenCallbacks;
  
  Rect mTmpDirty;
  
  public int getRequestedWidth() {
    return this.mRequestedWidth;
  }
  
  public int getRequestedHeight() {
    return this.mRequestedHeight;
  }
  
  public int getRequestedFormat() {
    return this.mRequestedFormat;
  }
  
  public int getRequestedType() {
    return this.mRequestedType;
  }
  
  public void addCallback(SurfaceHolder.Callback paramCallback) {
    synchronized (this.mCallbacks) {
      if (!this.mCallbacks.contains(paramCallback))
        this.mCallbacks.add(paramCallback); 
      return;
    } 
  }
  
  public void removeCallback(SurfaceHolder.Callback paramCallback) {
    synchronized (this.mCallbacks) {
      this.mCallbacks.remove(paramCallback);
      return;
    } 
  }
  
  public SurfaceHolder.Callback[] getCallbacks() {
    if (this.mHaveGottenCallbacks)
      return this.mGottenCallbacks; 
    synchronized (this.mCallbacks) {
      int i = this.mCallbacks.size();
      if (i > 0) {
        if (this.mGottenCallbacks == null || this.mGottenCallbacks.length != i)
          this.mGottenCallbacks = new SurfaceHolder.Callback[i]; 
        this.mCallbacks.toArray(this.mGottenCallbacks);
      } else {
        this.mGottenCallbacks = null;
      } 
      this.mHaveGottenCallbacks = true;
      return this.mGottenCallbacks;
    } 
  }
  
  public void ungetCallbacks() {
    this.mHaveGottenCallbacks = false;
  }
  
  public void setFixedSize(int paramInt1, int paramInt2) {
    if (this.mRequestedWidth != paramInt1 || this.mRequestedHeight != paramInt2) {
      this.mRequestedWidth = paramInt1;
      this.mRequestedHeight = paramInt2;
      onRelayoutContainer();
    } 
  }
  
  public void setSizeFromLayout() {
    if (this.mRequestedWidth != -1 || this.mRequestedHeight != -1) {
      this.mRequestedHeight = -1;
      this.mRequestedWidth = -1;
      onRelayoutContainer();
    } 
  }
  
  public void setFormat(int paramInt) {
    if (this.mRequestedFormat != paramInt) {
      this.mRequestedFormat = paramInt;
      onUpdateSurface();
    } 
  }
  
  public void setType(int paramInt) {
    if (paramInt == 1 || paramInt == 2)
      paramInt = 0; 
    if (paramInt == 0 || paramInt == 3)
      if (this.mRequestedType != paramInt) {
        this.mRequestedType = paramInt;
        onUpdateSurface();
      }  
  }
  
  public Canvas lockCanvas() {
    return internalLockCanvas(null, false);
  }
  
  public Canvas lockCanvas(Rect paramRect) {
    return internalLockCanvas(paramRect, false);
  }
  
  public Canvas lockHardwareCanvas() {
    return internalLockCanvas(null, true);
  }
  
  private final Canvas internalLockCanvas(Rect paramRect, boolean paramBoolean) {
    if (this.mType != 3) {
      Canvas canvas;
      this.mSurfaceLock.lock();
      Rect rect1 = null;
      Rect rect2 = rect1;
      if (onAllowLockCanvas()) {
        rect2 = paramRect;
        if (paramRect == null) {
          if (this.mTmpDirty == null)
            this.mTmpDirty = new Rect(); 
          this.mTmpDirty.set(this.mSurfaceFrame);
          rect2 = this.mTmpDirty;
        } 
        if (paramBoolean) {
          try {
            Canvas canvas1 = this.mSurface.lockHardwareCanvas();
            canvas = canvas1;
          } catch (Exception exception) {
            Log.e("BaseSurfaceHolder", "Exception locking surface", exception);
            rect2 = rect1;
          } 
        } else {
          Canvas canvas1 = this.mSurface.lockCanvas(rect2);
          canvas = canvas1;
        } 
      } 
      if (canvas != null) {
        this.mLastLockTime = SystemClock.uptimeMillis();
        return canvas;
      } 
      long l1 = SystemClock.uptimeMillis();
      long l2 = this.mLastLockTime + 100L;
      long l3 = l1;
      if (l2 > l1) {
        try {
          Thread.sleep(l2 - l1);
        } catch (InterruptedException interruptedException) {}
        l3 = SystemClock.uptimeMillis();
      } 
      this.mLastLockTime = l3;
      this.mSurfaceLock.unlock();
      return null;
    } 
    throw new SurfaceHolder.BadSurfaceTypeException("Surface type is SURFACE_TYPE_PUSH_BUFFERS");
  }
  
  public void unlockCanvasAndPost(Canvas paramCanvas) {
    this.mSurface.unlockCanvasAndPost(paramCanvas);
    this.mSurfaceLock.unlock();
  }
  
  public Surface getSurface() {
    return this.mSurface;
  }
  
  public Rect getSurfaceFrame() {
    return this.mSurfaceFrame;
  }
  
  public void setSurfaceFrameSize(int paramInt1, int paramInt2) {
    this.mSurfaceFrame.top = 0;
    this.mSurfaceFrame.left = 0;
    this.mSurfaceFrame.right = paramInt1;
    this.mSurfaceFrame.bottom = paramInt2;
  }
  
  public abstract boolean onAllowLockCanvas();
  
  public abstract void onRelayoutContainer();
  
  public abstract void onUpdateSurface();
}
