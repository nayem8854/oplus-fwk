package com.android.internal.view;

import android.graphics.Point;
import android.graphics.Rect;
import android.view.ScrollCaptureCallback;
import android.view.View;
import android.view.ViewGroup;

public class ScrollCaptureInternal {
  private static final int DOWN = 1;
  
  private static final String TAG = "ScrollCaptureInternal";
  
  public static final int TYPE_FIXED = 0;
  
  public static final int TYPE_RECYCLING = 2;
  
  public static final int TYPE_SCROLLING = 1;
  
  private static final int UP = -1;
  
  public static int detectScrollingType(View paramView) {
    if (!(paramView instanceof ViewGroup))
      return 0; 
    if (!paramView.canScrollVertically(1) && !paramView.canScrollVertically(-1))
      return 0; 
    if (((ViewGroup)paramView).getChildCount() > 1)
      return 2; 
    if (paramView.getScrollY() != 0)
      return 1; 
    if (paramView.canScrollVertically(-1))
      return 2; 
    paramView.scrollTo(paramView.getScrollX(), 1);
    if (paramView.getScrollY() == 1) {
      paramView.scrollTo(paramView.getScrollX(), 0);
      return 1;
    } 
    return 2;
  }
  
  public ScrollCaptureCallback requestCallback(View paramView, Rect paramRect, Point paramPoint) {
    int i = detectScrollingType(paramView);
    if (i != 1)
      return null; 
    return new ScrollCaptureViewSupport<>((ViewGroup)paramView, new ScrollViewCaptureHelper());
  }
}
