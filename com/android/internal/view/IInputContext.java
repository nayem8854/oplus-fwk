package com.android.internal.view;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CorrectionInfo;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputContentInfo;
import com.android.internal.inputmethod.ICharSequenceResultCallback;
import com.android.internal.inputmethod.IExtractedTextResultCallback;
import com.android.internal.inputmethod.IIntResultCallback;

public interface IInputContext extends IInterface {
  void beginBatchEdit() throws RemoteException;
  
  void clearMetaKeyStates(int paramInt) throws RemoteException;
  
  void commitCompletion(CompletionInfo paramCompletionInfo) throws RemoteException;
  
  void commitContent(InputContentInfo paramInputContentInfo, int paramInt, Bundle paramBundle, IIntResultCallback paramIIntResultCallback) throws RemoteException;
  
  void commitCorrection(CorrectionInfo paramCorrectionInfo) throws RemoteException;
  
  void commitText(CharSequence paramCharSequence, int paramInt) throws RemoteException;
  
  void deleteSurroundingText(int paramInt1, int paramInt2) throws RemoteException;
  
  void deleteSurroundingTextInCodePoints(int paramInt1, int paramInt2) throws RemoteException;
  
  void endBatchEdit() throws RemoteException;
  
  void finishComposingText() throws RemoteException;
  
  void getCursorCapsMode(int paramInt, IIntResultCallback paramIIntResultCallback) throws RemoteException;
  
  void getExtractedText(ExtractedTextRequest paramExtractedTextRequest, int paramInt, IExtractedTextResultCallback paramIExtractedTextResultCallback) throws RemoteException;
  
  void getSelectedText(int paramInt, ICharSequenceResultCallback paramICharSequenceResultCallback) throws RemoteException;
  
  void getTextAfterCursor(int paramInt1, int paramInt2, ICharSequenceResultCallback paramICharSequenceResultCallback) throws RemoteException;
  
  void getTextBeforeCursor(int paramInt1, int paramInt2, ICharSequenceResultCallback paramICharSequenceResultCallback) throws RemoteException;
  
  void performContextMenuAction(int paramInt) throws RemoteException;
  
  void performEditorAction(int paramInt) throws RemoteException;
  
  void performPrivateCommand(String paramString, Bundle paramBundle) throws RemoteException;
  
  void requestUpdateCursorAnchorInfo(int paramInt, IIntResultCallback paramIIntResultCallback) throws RemoteException;
  
  void sendKeyEvent(KeyEvent paramKeyEvent) throws RemoteException;
  
  void setComposingRegion(int paramInt1, int paramInt2) throws RemoteException;
  
  void setComposingText(CharSequence paramCharSequence, int paramInt) throws RemoteException;
  
  void setSelection(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements IInputContext {
    public void getTextBeforeCursor(int param1Int1, int param1Int2, ICharSequenceResultCallback param1ICharSequenceResultCallback) throws RemoteException {}
    
    public void getTextAfterCursor(int param1Int1, int param1Int2, ICharSequenceResultCallback param1ICharSequenceResultCallback) throws RemoteException {}
    
    public void getCursorCapsMode(int param1Int, IIntResultCallback param1IIntResultCallback) throws RemoteException {}
    
    public void getExtractedText(ExtractedTextRequest param1ExtractedTextRequest, int param1Int, IExtractedTextResultCallback param1IExtractedTextResultCallback) throws RemoteException {}
    
    public void deleteSurroundingText(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void deleteSurroundingTextInCodePoints(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void setComposingText(CharSequence param1CharSequence, int param1Int) throws RemoteException {}
    
    public void finishComposingText() throws RemoteException {}
    
    public void commitText(CharSequence param1CharSequence, int param1Int) throws RemoteException {}
    
    public void commitCompletion(CompletionInfo param1CompletionInfo) throws RemoteException {}
    
    public void commitCorrection(CorrectionInfo param1CorrectionInfo) throws RemoteException {}
    
    public void setSelection(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void performEditorAction(int param1Int) throws RemoteException {}
    
    public void performContextMenuAction(int param1Int) throws RemoteException {}
    
    public void beginBatchEdit() throws RemoteException {}
    
    public void endBatchEdit() throws RemoteException {}
    
    public void sendKeyEvent(KeyEvent param1KeyEvent) throws RemoteException {}
    
    public void clearMetaKeyStates(int param1Int) throws RemoteException {}
    
    public void performPrivateCommand(String param1String, Bundle param1Bundle) throws RemoteException {}
    
    public void setComposingRegion(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void getSelectedText(int param1Int, ICharSequenceResultCallback param1ICharSequenceResultCallback) throws RemoteException {}
    
    public void requestUpdateCursorAnchorInfo(int param1Int, IIntResultCallback param1IIntResultCallback) throws RemoteException {}
    
    public void commitContent(InputContentInfo param1InputContentInfo, int param1Int, Bundle param1Bundle, IIntResultCallback param1IIntResultCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInputContext {
    private static final String DESCRIPTOR = "com.android.internal.view.IInputContext";
    
    static final int TRANSACTION_beginBatchEdit = 15;
    
    static final int TRANSACTION_clearMetaKeyStates = 18;
    
    static final int TRANSACTION_commitCompletion = 10;
    
    static final int TRANSACTION_commitContent = 23;
    
    static final int TRANSACTION_commitCorrection = 11;
    
    static final int TRANSACTION_commitText = 9;
    
    static final int TRANSACTION_deleteSurroundingText = 5;
    
    static final int TRANSACTION_deleteSurroundingTextInCodePoints = 6;
    
    static final int TRANSACTION_endBatchEdit = 16;
    
    static final int TRANSACTION_finishComposingText = 8;
    
    static final int TRANSACTION_getCursorCapsMode = 3;
    
    static final int TRANSACTION_getExtractedText = 4;
    
    static final int TRANSACTION_getSelectedText = 21;
    
    static final int TRANSACTION_getTextAfterCursor = 2;
    
    static final int TRANSACTION_getTextBeforeCursor = 1;
    
    static final int TRANSACTION_performContextMenuAction = 14;
    
    static final int TRANSACTION_performEditorAction = 13;
    
    static final int TRANSACTION_performPrivateCommand = 19;
    
    static final int TRANSACTION_requestUpdateCursorAnchorInfo = 22;
    
    static final int TRANSACTION_sendKeyEvent = 17;
    
    static final int TRANSACTION_setComposingRegion = 20;
    
    static final int TRANSACTION_setComposingText = 7;
    
    static final int TRANSACTION_setSelection = 12;
    
    public Stub() {
      attachInterface(this, "com.android.internal.view.IInputContext");
    }
    
    public static IInputContext asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.view.IInputContext");
      if (iInterface != null && iInterface instanceof IInputContext)
        return (IInputContext)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 23:
          return "commitContent";
        case 22:
          return "requestUpdateCursorAnchorInfo";
        case 21:
          return "getSelectedText";
        case 20:
          return "setComposingRegion";
        case 19:
          return "performPrivateCommand";
        case 18:
          return "clearMetaKeyStates";
        case 17:
          return "sendKeyEvent";
        case 16:
          return "endBatchEdit";
        case 15:
          return "beginBatchEdit";
        case 14:
          return "performContextMenuAction";
        case 13:
          return "performEditorAction";
        case 12:
          return "setSelection";
        case 11:
          return "commitCorrection";
        case 10:
          return "commitCompletion";
        case 9:
          return "commitText";
        case 8:
          return "finishComposingText";
        case 7:
          return "setComposingText";
        case 6:
          return "deleteSurroundingTextInCodePoints";
        case 5:
          return "deleteSurroundingText";
        case 4:
          return "getExtractedText";
        case 3:
          return "getCursorCapsMode";
        case 2:
          return "getTextAfterCursor";
        case 1:
          break;
      } 
      return "getTextBeforeCursor";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        IIntResultCallback iIntResultCallback2;
        ICharSequenceResultCallback iCharSequenceResultCallback2;
        IExtractedTextResultCallback iExtractedTextResultCallback;
        IIntResultCallback iIntResultCallback1;
        Bundle bundle;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 23:
            param1Parcel1.enforceInterface("com.android.internal.view.IInputContext");
            if (param1Parcel1.readInt() != 0) {
              InputContentInfo inputContentInfo = (InputContentInfo)InputContentInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              bundle = null;
            } 
            iIntResultCallback2 = IIntResultCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            commitContent((InputContentInfo)param1Parcel2, param1Int1, bundle, iIntResultCallback2);
            return true;
          case 22:
            iIntResultCallback2.enforceInterface("com.android.internal.view.IInputContext");
            param1Int1 = iIntResultCallback2.readInt();
            iIntResultCallback2 = IIntResultCallback.Stub.asInterface(iIntResultCallback2.readStrongBinder());
            requestUpdateCursorAnchorInfo(param1Int1, iIntResultCallback2);
            return true;
          case 21:
            iIntResultCallback2.enforceInterface("com.android.internal.view.IInputContext");
            param1Int1 = iIntResultCallback2.readInt();
            iCharSequenceResultCallback2 = ICharSequenceResultCallback.Stub.asInterface(iIntResultCallback2.readStrongBinder());
            getSelectedText(param1Int1, iCharSequenceResultCallback2);
            return true;
          case 20:
            iCharSequenceResultCallback2.enforceInterface("com.android.internal.view.IInputContext");
            param1Int2 = iCharSequenceResultCallback2.readInt();
            param1Int1 = iCharSequenceResultCallback2.readInt();
            setComposingRegion(param1Int2, param1Int1);
            return true;
          case 19:
            iCharSequenceResultCallback2.enforceInterface("com.android.internal.view.IInputContext");
            str = iCharSequenceResultCallback2.readString();
            if (iCharSequenceResultCallback2.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iCharSequenceResultCallback2);
            } else {
              iCharSequenceResultCallback2 = null;
            } 
            performPrivateCommand(str, (Bundle)iCharSequenceResultCallback2);
            return true;
          case 18:
            iCharSequenceResultCallback2.enforceInterface("com.android.internal.view.IInputContext");
            param1Int1 = iCharSequenceResultCallback2.readInt();
            clearMetaKeyStates(param1Int1);
            return true;
          case 17:
            iCharSequenceResultCallback2.enforceInterface("com.android.internal.view.IInputContext");
            if (iCharSequenceResultCallback2.readInt() != 0) {
              KeyEvent keyEvent = (KeyEvent)KeyEvent.CREATOR.createFromParcel((Parcel)iCharSequenceResultCallback2);
            } else {
              iCharSequenceResultCallback2 = null;
            } 
            sendKeyEvent((KeyEvent)iCharSequenceResultCallback2);
            return true;
          case 16:
            iCharSequenceResultCallback2.enforceInterface("com.android.internal.view.IInputContext");
            endBatchEdit();
            return true;
          case 15:
            iCharSequenceResultCallback2.enforceInterface("com.android.internal.view.IInputContext");
            beginBatchEdit();
            return true;
          case 14:
            iCharSequenceResultCallback2.enforceInterface("com.android.internal.view.IInputContext");
            param1Int1 = iCharSequenceResultCallback2.readInt();
            performContextMenuAction(param1Int1);
            return true;
          case 13:
            iCharSequenceResultCallback2.enforceInterface("com.android.internal.view.IInputContext");
            param1Int1 = iCharSequenceResultCallback2.readInt();
            performEditorAction(param1Int1);
            return true;
          case 12:
            iCharSequenceResultCallback2.enforceInterface("com.android.internal.view.IInputContext");
            param1Int1 = iCharSequenceResultCallback2.readInt();
            param1Int2 = iCharSequenceResultCallback2.readInt();
            setSelection(param1Int1, param1Int2);
            return true;
          case 11:
            iCharSequenceResultCallback2.enforceInterface("com.android.internal.view.IInputContext");
            if (iCharSequenceResultCallback2.readInt() != 0) {
              CorrectionInfo correctionInfo = (CorrectionInfo)CorrectionInfo.CREATOR.createFromParcel((Parcel)iCharSequenceResultCallback2);
            } else {
              iCharSequenceResultCallback2 = null;
            } 
            commitCorrection((CorrectionInfo)iCharSequenceResultCallback2);
            return true;
          case 10:
            iCharSequenceResultCallback2.enforceInterface("com.android.internal.view.IInputContext");
            if (iCharSequenceResultCallback2.readInt() != 0) {
              CompletionInfo completionInfo = (CompletionInfo)CompletionInfo.CREATOR.createFromParcel((Parcel)iCharSequenceResultCallback2);
            } else {
              iCharSequenceResultCallback2 = null;
            } 
            commitCompletion((CompletionInfo)iCharSequenceResultCallback2);
            return true;
          case 9:
            iCharSequenceResultCallback2.enforceInterface("com.android.internal.view.IInputContext");
            if (iCharSequenceResultCallback2.readInt() != 0) {
              CharSequence charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)iCharSequenceResultCallback2);
            } else {
              str = null;
            } 
            param1Int1 = iCharSequenceResultCallback2.readInt();
            commitText(str, param1Int1);
            return true;
          case 8:
            iCharSequenceResultCallback2.enforceInterface("com.android.internal.view.IInputContext");
            finishComposingText();
            return true;
          case 7:
            iCharSequenceResultCallback2.enforceInterface("com.android.internal.view.IInputContext");
            if (iCharSequenceResultCallback2.readInt() != 0) {
              CharSequence charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)iCharSequenceResultCallback2);
            } else {
              str = null;
            } 
            param1Int1 = iCharSequenceResultCallback2.readInt();
            setComposingText(str, param1Int1);
            return true;
          case 6:
            iCharSequenceResultCallback2.enforceInterface("com.android.internal.view.IInputContext");
            param1Int1 = iCharSequenceResultCallback2.readInt();
            param1Int2 = iCharSequenceResultCallback2.readInt();
            deleteSurroundingTextInCodePoints(param1Int1, param1Int2);
            return true;
          case 5:
            iCharSequenceResultCallback2.enforceInterface("com.android.internal.view.IInputContext");
            param1Int1 = iCharSequenceResultCallback2.readInt();
            param1Int2 = iCharSequenceResultCallback2.readInt();
            deleteSurroundingText(param1Int1, param1Int2);
            return true;
          case 4:
            iCharSequenceResultCallback2.enforceInterface("com.android.internal.view.IInputContext");
            if (iCharSequenceResultCallback2.readInt() != 0) {
              ExtractedTextRequest extractedTextRequest = (ExtractedTextRequest)ExtractedTextRequest.CREATOR.createFromParcel((Parcel)iCharSequenceResultCallback2);
            } else {
              str = null;
            } 
            param1Int1 = iCharSequenceResultCallback2.readInt();
            iExtractedTextResultCallback = IExtractedTextResultCallback.Stub.asInterface(iCharSequenceResultCallback2.readStrongBinder());
            getExtractedText((ExtractedTextRequest)str, param1Int1, iExtractedTextResultCallback);
            return true;
          case 3:
            iExtractedTextResultCallback.enforceInterface("com.android.internal.view.IInputContext");
            param1Int1 = iExtractedTextResultCallback.readInt();
            iIntResultCallback1 = IIntResultCallback.Stub.asInterface(iExtractedTextResultCallback.readStrongBinder());
            getCursorCapsMode(param1Int1, iIntResultCallback1);
            return true;
          case 2:
            iIntResultCallback1.enforceInterface("com.android.internal.view.IInputContext");
            param1Int1 = iIntResultCallback1.readInt();
            param1Int2 = iIntResultCallback1.readInt();
            iCharSequenceResultCallback1 = ICharSequenceResultCallback.Stub.asInterface(iIntResultCallback1.readStrongBinder());
            getTextAfterCursor(param1Int1, param1Int2, iCharSequenceResultCallback1);
            return true;
          case 1:
            break;
        } 
        iCharSequenceResultCallback1.enforceInterface("com.android.internal.view.IInputContext");
        param1Int2 = iCharSequenceResultCallback1.readInt();
        param1Int1 = iCharSequenceResultCallback1.readInt();
        ICharSequenceResultCallback iCharSequenceResultCallback1 = ICharSequenceResultCallback.Stub.asInterface(iCharSequenceResultCallback1.readStrongBinder());
        getTextBeforeCursor(param1Int2, param1Int1, iCharSequenceResultCallback1);
        return true;
      } 
      str.writeString("com.android.internal.view.IInputContext");
      return true;
    }
    
    private static class Proxy implements IInputContext {
      public static IInputContext sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.view.IInputContext";
      }
      
      public void getTextBeforeCursor(int param2Int1, int param2Int2, ICharSequenceResultCallback param2ICharSequenceResultCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2ICharSequenceResultCallback != null) {
            iBinder = param2ICharSequenceResultCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().getTextBeforeCursor(param2Int1, param2Int2, param2ICharSequenceResultCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getTextAfterCursor(int param2Int1, int param2Int2, ICharSequenceResultCallback param2ICharSequenceResultCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2ICharSequenceResultCallback != null) {
            iBinder = param2ICharSequenceResultCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().getTextAfterCursor(param2Int1, param2Int2, param2ICharSequenceResultCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getCursorCapsMode(int param2Int, IIntResultCallback param2IIntResultCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          parcel.writeInt(param2Int);
          if (param2IIntResultCallback != null) {
            iBinder = param2IIntResultCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().getCursorCapsMode(param2Int, param2IIntResultCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getExtractedText(ExtractedTextRequest param2ExtractedTextRequest, int param2Int, IExtractedTextResultCallback param2IExtractedTextResultCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          if (param2ExtractedTextRequest != null) {
            parcel.writeInt(1);
            param2ExtractedTextRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          if (param2IExtractedTextResultCallback != null) {
            iBinder = param2IExtractedTextResultCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().getExtractedText(param2ExtractedTextRequest, param2Int, param2IExtractedTextResultCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void deleteSurroundingText(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().deleteSurroundingText(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void deleteSurroundingTextInCodePoints(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().deleteSurroundingTextInCodePoints(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setComposingText(CharSequence param2CharSequence, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          if (param2CharSequence != null) {
            parcel.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().setComposingText(param2CharSequence, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void finishComposingText() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().finishComposingText();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void commitText(CharSequence param2CharSequence, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          if (param2CharSequence != null) {
            parcel.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().commitText(param2CharSequence, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void commitCompletion(CompletionInfo param2CompletionInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          if (param2CompletionInfo != null) {
            parcel.writeInt(1);
            param2CompletionInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().commitCompletion(param2CompletionInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void commitCorrection(CorrectionInfo param2CorrectionInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          if (param2CorrectionInfo != null) {
            parcel.writeInt(1);
            param2CorrectionInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().commitCorrection(param2CorrectionInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setSelection(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(12, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().setSelection(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void performEditorAction(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().performEditorAction(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void performContextMenuAction(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(14, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().performContextMenuAction(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void beginBatchEdit() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          boolean bool = this.mRemote.transact(15, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().beginBatchEdit();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void endBatchEdit() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          boolean bool = this.mRemote.transact(16, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().endBatchEdit();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendKeyEvent(KeyEvent param2KeyEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          if (param2KeyEvent != null) {
            parcel.writeInt(1);
            param2KeyEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(17, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().sendKeyEvent(param2KeyEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void clearMetaKeyStates(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(18, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().clearMetaKeyStates(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void performPrivateCommand(String param2String, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          parcel.writeString(param2String);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(19, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().performPrivateCommand(param2String, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setComposingRegion(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(20, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().setComposingRegion(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getSelectedText(int param2Int, ICharSequenceResultCallback param2ICharSequenceResultCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          parcel.writeInt(param2Int);
          if (param2ICharSequenceResultCallback != null) {
            iBinder = param2ICharSequenceResultCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(21, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().getSelectedText(param2Int, param2ICharSequenceResultCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestUpdateCursorAnchorInfo(int param2Int, IIntResultCallback param2IIntResultCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          parcel.writeInt(param2Int);
          if (param2IIntResultCallback != null) {
            iBinder = param2IIntResultCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(22, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().requestUpdateCursorAnchorInfo(param2Int, param2IIntResultCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void commitContent(InputContentInfo param2InputContentInfo, int param2Int, Bundle param2Bundle, IIntResultCallback param2IIntResultCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.view.IInputContext");
          if (param2InputContentInfo != null) {
            parcel.writeInt(1);
            param2InputContentInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IIntResultCallback != null) {
            iBinder = param2IIntResultCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(23, parcel, null, 1);
          if (!bool && IInputContext.Stub.getDefaultImpl() != null) {
            IInputContext.Stub.getDefaultImpl().commitContent(param2InputContentInfo, param2Int, param2Bundle, param2IIntResultCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInputContext param1IInputContext) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInputContext != null) {
          Proxy.sDefaultImpl = param1IInputContext;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInputContext getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
