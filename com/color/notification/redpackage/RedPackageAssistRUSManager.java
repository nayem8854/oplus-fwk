package com.color.notification.redpackage;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.FileObserver;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import com.oplus.util.OplusNavigationBarUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class RedPackageAssistRUSManager {
  private static final boolean DEBUG;
  
  public static final String ENVELOPE_CONTENT_TAG = "envelope_content_tag";
  
  public static final String ENVELOPE_FILTER_FIELD = "envelope_filter_field";
  
  public static final String ENVELOPE_FILTER_VALUE = "envelope_filter_value";
  
  public static final String ENVELOPE_GROUP_TAG = "envelope_group_tag";
  
  public static final String ENVELOPE_USER_FIELD = "envelope_user_field";
  
  public static final String ENVELOPE_USER_NAME_TAG_FIRST = "envelope_user_name_tag_first";
  
  public static final String ENVELOPE_USER_NAME_TAG_LAST = "envelope_user_name_tag_last";
  
  public static final int MESSAGE_DOWNLOAD_DATA_FROM_RUS = 2;
  
  public static final int MESSAGE_INIT_FILE = 1;
  
  public static final int MESSAGE_UPGRADE_DATA_FROM_LOCAL = 3;
  
  public static final String OPPO_REDPACKAGE_ASSIST_ATTRIBUTE_CONFIG_DIR = "/data/oppo/coloros/notification";
  
  public static final String OPPO_REDPACKAGE_ASSIST_ATTRIBUTE_CONFIG_FILE_PATH = "/data/oppo/coloros/notification/sys_systemui_redpackage_assist_config.xml";
  
  public static final String OPPO_REDPACKAGE_ASSIST_CONFIG_KEY = "sys_systemui_redpackage_assist_config";
  
  public static final String OPPO_REDPACKAGE_ASSIST_CONFIG_NAME = "sys_systemui_redpackage_assist_config.xml";
  
  public static final String PKG_VERSION = "pkg_version";
  
  private static final String TAG;
  
  private static final List<AdaptationEnvelopeInfo> mAdaptationEnvelopeInfoList;
  
  private static final String[] mDefaultEnvelopeInfo;
  
  private static final Object mLock;
  
  static {
    String str = RedPackageAssistRUSManager.class.getSimpleName();
    DEBUG = Log.isLoggable(str, 3);
  }
  
  private static final RedPackageAssistRUSManager sRedPackageAssistRUSManager = new RedPackageAssistRUSManager();
  
  private Context mContext;
  
  static {
    mLock = new Object();
    mAdaptationEnvelopeInfoList = new ArrayList<>();
    mDefaultEnvelopeInfo = new String[] { "0", "MainUI_User_Last_Msg_Type", "436207665", "Main_User", "@chatroom", "]", ": [微信红包]", "[微信红包]" };
  }
  
  private volatile int mCurrentIndex = -1;
  
  private HandlerThread mHandlerThread;
  
  private Handler mMainHandler;
  
  private RedPackageRUSReceiver mRedPackageRUSReceiver;
  
  private Handler mThreadHandler;
  
  public static RedPackageAssistRUSManager getInstance() {
    return sRedPackageAssistRUSManager;
  }
  
  public void init(Context paramContext) {
    this.mContext = paramContext;
    HandlerThread handlerThread = new HandlerThread(TAG);
    handlerThread.start();
    Object object = new Object(this, this.mHandlerThread.getLooper());
    object.sendEmptyMessage(1);
    this.mRedPackageRUSReceiver = (RedPackageRUSReceiver)(object = new RedPackageRUSReceiver());
    object.registerRedPackageRUSReceiver(this.mContext);
    this.mThreadHandler.sendEmptyMessage(3);
  }
  
  private void creatRedPackageConfigFile() {
    File file1 = new File("/data/oppo/coloros/notification");
    File file2 = new File("/data/oppo/coloros/notification/sys_systemui_redpackage_assist_config.xml");
    try {
      if (!file1.exists())
        file1.mkdirs(); 
      if (!file2.exists())
        file2.createNewFile(); 
    } catch (IOException iOException) {
      iOException.printStackTrace();
      if (DEBUG)
        Log.e(TAG, "init redpackage Dir failed!!!"); 
    } 
  }
  
  public void downloadDataFromRUS(Context paramContext, String paramString) {
    String str1 = RUSUpgradeUtils.getDataFromProvider(paramContext, paramString);
    String str2 = RUSUpgradeUtils.getConfigVersion(str1);
    String str3 = RUSUpgradeUtils.getRedPackageRUSVersion(paramContext);
    if (str3 == null || str2.compareTo(str3) > 0) {
      if (str1 != null)
        if (paramString.equals("sys_systemui_redpackage_assist_config")) {
          RUSUpgradeUtils.saveStrToFile(paramContext, "sys_systemui_redpackage_assist_config.xml", str1);
          List<AdaptationEnvelopeInfo> list = RUSUpgradeUtils.parseRedpackageString2List(str1);
          updateEnvelopeWhenRUSArrived(list);
        } else if (DEBUG) {
          Log.d(TAG, "we need do nothing because this RUS content is null\n");
        }  
      RUSUpgradeUtils.setRedPackageRUSVersion2Local(paramContext, str2);
    } 
  }
  
  public static String getRedPackageDataFromLocalFile() {
    FileNotFoundException fileNotFoundException;
    String str;
    FileInputStream fileInputStream1 = null, fileInputStream2 = null;
    File file = new File("/data/oppo/coloros/notification/sys_systemui_redpackage_assist_config.xml");
    if (file.exists()) {
      try {
        fileInputStream1 = new FileInputStream();
        this(file);
        fileInputStream2 = fileInputStream1;
      } catch (FileNotFoundException null) {
        null.printStackTrace();
      } 
    } else {
      fileNotFoundException = null;
      if (DEBUG) {
        Log.d(TAG, "redpackage local file is not exit!\n");
        fileNotFoundException = null;
      } 
    } 
    try {
      return RUSUpgradeUtils.inputStream2String((InputStream)fileNotFoundException);
    } finally {
      if (str != null)
        try {
          str.close();
        } catch (IOException iOException) {
          if (DEBUG) {
            str = TAG;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("redpackage local file:error:");
            stringBuilder.append(iOException.getMessage());
            Log.d(str, stringBuilder.toString());
          } 
        }  
    } 
  }
  
  public void updateEnvelopeWhenRUSArrived(List<AdaptationEnvelopeInfo> paramList) {
    if (!OplusNavigationBarUtil.getInstance().isHasInitialized())
      return; 
    if (paramList == null || paramList.isEmpty())
      return; 
    synchronized (mLock) {
      mAdaptationEnvelopeInfoList.clear();
      mAdaptationEnvelopeInfoList.addAll(paramList);
      if (DEBUG) {
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("we have upgrade the RUS Data and the value is ");
        stringBuilder.append(mAdaptationEnvelopeInfoList);
        stringBuilder.append("\n");
        Log.d(str, stringBuilder.toString());
      } 
      return;
    } 
  }
  
  public void updateRedpackageDataFromLocal() {
    if (!OplusNavigationBarUtil.getInstance().isHasInitialized())
      return; 
    String str = getRedPackageDataFromLocalFile();
    if (str == null || str.isEmpty()) {
      updateEnvelopeDefaultInfo();
      return;
    } 
    null = RUSUpgradeUtils.parseRedpackageString2List(str);
    if (null == null || null.isEmpty())
      return; 
    synchronized (mLock) {
      mAdaptationEnvelopeInfoList.clear();
      mAdaptationEnvelopeInfoList.addAll(null);
    } 
  }
  
  private void updateEnvelopeDefaultInfo() {
    synchronized (mLock) {
      AdaptationEnvelopeInfo adaptationEnvelopeInfo = new AdaptationEnvelopeInfo();
      this();
      adaptationEnvelopeInfo.setPkgVersion(mDefaultEnvelopeInfo[0]);
      adaptationEnvelopeInfo.setEnvelopeFilterField(mDefaultEnvelopeInfo[1]);
      adaptationEnvelopeInfo.setEnvelopeFilterValue(mDefaultEnvelopeInfo[2]);
      adaptationEnvelopeInfo.setEnvelopeUserField(mDefaultEnvelopeInfo[3]);
      adaptationEnvelopeInfo.setEnvelopeGroupTag(mDefaultEnvelopeInfo[4]);
      adaptationEnvelopeInfo.setEnvelopeUserNameTagFirst(mDefaultEnvelopeInfo[5]);
      adaptationEnvelopeInfo.setEnvelopeUserNameTagLast(mDefaultEnvelopeInfo[6]);
      adaptationEnvelopeInfo.setEnvelopeContentTag(mDefaultEnvelopeInfo[7]);
      mAdaptationEnvelopeInfoList.add(adaptationEnvelopeInfo);
      return;
    } 
  }
  
  private class RedPackageRUSReceiver {
    public static final String OPPO_COMPONENT_SAFE = "oppo.permission.OPPO_COMPONENT_SAFE";
    
    private static final String ROM_UPDATE_CONFIG_SUCCES_EXTRA = "ROM_UPDATE_CONFIG_LIST";
    
    private static final String TAG = "RomUpdateReceiver";
    
    private final boolean DEBUG = Log.isLoggable("RomUpdateReceiver", 3);
    
    private BroadcastReceiver mReceiver = (BroadcastReceiver)new Object(this);
    
    final RedPackageAssistRUSManager this$0;
    
    public void registerRedPackageRUSReceiver(Context param1Context) {
      IntentFilter intentFilter = new IntentFilter();
      intentFilter.addAction("oppo.intent.action.ROM_UPDATE_CONFIG_SUCCESS");
      param1Context.registerReceiver(this.mReceiver, intentFilter, "oppo.permission.OPPO_COMPONENT_SAFE", null);
    }
  }
  
  public static boolean compareVersion(String paramString1, String paramString2) {
    int i;
    String str = TAG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("A:");
    stringBuilder.append(paramString1);
    stringBuilder.append(" B:");
    stringBuilder.append(paramString2);
    Log.i(str, stringBuilder.toString());
    if (paramString1 == null || paramString1.equals("") || paramString2 == null || paramString2.equals(""))
      return false; 
    if (paramString1.equals(paramString2))
      return true; 
    String[] arrayOfString1 = paramString1.split("\\.");
    String[] arrayOfString2 = paramString2.split("\\.");
    if (arrayOfString1.length < arrayOfString2.length) {
      i = arrayOfString1.length;
    } else {
      i = arrayOfString2.length;
    } 
    for (byte b = 0; b < i; b++) {
      if (Integer.parseInt(arrayOfString2[b]) > Integer.parseInt(arrayOfString1[b])) {
        String str1 = TAG;
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("B:");
        stringBuilder1.append(Integer.parseInt(arrayOfString2[b]));
        stringBuilder1.append(" > A:");
        stringBuilder1.append(Integer.parseInt(arrayOfString1[b]));
        Log.d(str1, stringBuilder1.toString());
        return true;
      } 
      if (Integer.parseInt(arrayOfString2[b]) < Integer.parseInt(arrayOfString1[b])) {
        str = TAG;
        stringBuilder = new StringBuilder();
        stringBuilder.append("B:");
        stringBuilder.append(Integer.parseInt(arrayOfString2[b]));
        stringBuilder.append(" < A:");
        stringBuilder.append(Integer.parseInt(arrayOfString1[b]));
        Log.d(str, stringBuilder.toString());
        return false;
      } 
    } 
    return false;
  }
  
  public static String getVersion(Context paramContext, String paramString) {
    try {
      PackageManager packageManager = paramContext.getPackageManager();
      PackageInfo packageInfo = packageManager.getPackageInfo(paramString, 0);
      return packageInfo.versionName;
    } catch (Exception exception) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("GetVersion failed! e:");
      stringBuilder.append(exception.getMessage());
      Log.d(str, stringBuilder.toString(), exception);
      return null;
    } 
  }
  
  public boolean shouldSendRedpackageBroadcast(Notification paramNotification) {
    null = paramNotification.contentIntent;
    int i = -1;
    if (null == null) {
      Log.d(TAG, "contentIntent is null");
      return false;
    } 
    synchronized (mLock) {
      if (mAdaptationEnvelopeInfoList.size() == 0 || mAdaptationEnvelopeInfoList == null)
        updateEnvelopeDefaultInfo(); 
      for (byte b = 0; b < mAdaptationEnvelopeInfoList.size(); b++) {
        if (null.getIntent().getIntExtra(((AdaptationEnvelopeInfo)mAdaptationEnvelopeInfoList.get(b)).getEnvelopeFilterField(), -1) != -1) {
          i = null.getIntent().getIntExtra(((AdaptationEnvelopeInfo)mAdaptationEnvelopeInfoList.get(b)).getEnvelopeFilterField(), -1);
          if (i == Integer.parseInt(((AdaptationEnvelopeInfo)mAdaptationEnvelopeInfoList.get(b)).getEnvelopeFilterValue())) {
            this.mCurrentIndex = b;
            return true;
          } 
          i = -1;
        } 
      } 
      if (i == -1)
        return false; 
      return false;
    } 
  }
  
  public AdaptationEnvelopeInfo getCurrentRedpackageInfo() {
    return mAdaptationEnvelopeInfoList.get(this.mCurrentIndex);
  }
  
  class FileObserverPolicy extends FileObserver {
    private String mFocusPath;
    
    final RedPackageAssistRUSManager this$0;
    
    public FileObserverPolicy(String param1String) {
      super(param1String, 8);
      this.mFocusPath = param1String;
      if (RedPackageAssistRUSManager.DEBUG) {
        String str = RedPackageAssistRUSManager.TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("RedPackages--FileObserverPolicy_path = ");
        stringBuilder.append(param1String);
        Log.d(str, stringBuilder.toString());
      } 
    }
    
    public void onEvent(int param1Int, String param1String) {
      if (RedPackageAssistRUSManager.DEBUG) {
        String str = RedPackageAssistRUSManager.TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("RedPackages--onEvent: event = ");
        stringBuilder.append(param1Int);
        stringBuilder.append(",focusPath = ");
        stringBuilder.append(this.mFocusPath);
        Log.d(str, stringBuilder.toString());
      } 
      if (param1Int == 8 && 
        this.mFocusPath.equals("/data/oppo/coloros/notification/sys_systemui_redpackage_assist_config.xml") && RedPackageAssistRUSManager
        .DEBUG)
        Log.d(RedPackageAssistRUSManager.TAG, "RedPackages--onEvent: focusPath = OPPO_REDPACKAGE_ASSIST_ATTRIBUTE_CONFIG_FILE_PATH"); 
    }
  }
}
