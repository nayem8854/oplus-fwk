package com.color.notification.redpackage.xmlsax;

public interface XmlNode extends XmlAttribute {
  XmlAttribute[] getAllAttributes();
  
  XmlNode[] getAllChildNodes();
  
  XmlAttribute getAttribute(String paramString);
  
  XmlNode getChildNode(String paramString);
  
  XmlNode[] getChildNodes(String paramString);
  
  int numOfAllAttributes();
  
  int numOfAllChildNodes();
  
  String toXml();
}
