package com.color.notification.redpackage.xmlsax;

public interface Xml {
  XmlNode getRootNode();
  
  String toXml();
}
