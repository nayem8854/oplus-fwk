package com.color.notification.redpackage.xmlsax;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class XmlNodeImpl implements XmlNode {
  private List<XmlAttribute> mAttributes;
  
  private String mName;
  
  private List<XmlNode> mNodes;
  
  private XmlNodeImpl mParent;
  
  private String mValue;
  
  XmlNodeImpl(String paramString) {
    this.mName = paramString;
  }
  
  protected void addChildNode(XmlNodeImpl paramXmlNodeImpl) {
    if (this.mNodes == null)
      this.mNodes = new ArrayList<>(); 
    this.mNodes.add(paramXmlNodeImpl);
    paramXmlNodeImpl.mParent = this;
  }
  
  protected void addAttributes(XmlAttributeImpl paramXmlAttributeImpl) {
    if (this.mAttributes == null)
      this.mAttributes = new ArrayList<>(); 
    this.mAttributes.add(paramXmlAttributeImpl);
  }
  
  protected void setValue(String paramString) {
    this.mValue = paramString.trim();
  }
  
  public String getName() {
    return this.mName;
  }
  
  public String getValue() {
    return this.mValue;
  }
  
  public XmlNode[] getChildNodes(String paramString) {
    if (this.mNodes == null)
      return null; 
    ArrayList<XmlNode> arrayList = new ArrayList();
    Iterator<XmlNode> iterator = this.mNodes.iterator();
    while (iterator.hasNext()) {
      XmlNode xmlNode = iterator.next();
      if (xmlNode.getName().equals(paramString))
        arrayList.add(xmlNode); 
    } 
    XmlNode[] arrayOfXmlNode = new XmlNode[arrayList.size()];
    if (arrayOfXmlNode.length <= 0)
      return arrayOfXmlNode; 
    arrayList.toArray(arrayOfXmlNode);
    return arrayOfXmlNode;
  }
  
  public XmlNode getChildNode(String paramString) {
    List<XmlNode> list = this.mNodes;
    if (list == null)
      return null; 
    Iterator<XmlNode> iterator = list.iterator();
    while (iterator.hasNext()) {
      XmlNode xmlNode = iterator.next();
      if (xmlNode.getName().equals(paramString))
        return xmlNode; 
    } 
    return null;
  }
  
  public XmlNode[] getAllChildNodes() {
    List<XmlNode> list = this.mNodes;
    if (list == null)
      return new XmlNode[0]; 
    XmlNode[] arrayOfXmlNode = new XmlNode[list.size()];
    if (arrayOfXmlNode.length <= 0)
      return arrayOfXmlNode; 
    this.mNodes.toArray(arrayOfXmlNode);
    return arrayOfXmlNode;
  }
  
  public XmlAttribute getAttribute(String paramString) {
    List<XmlAttribute> list = this.mAttributes;
    if (list == null)
      return null; 
    Iterator<XmlAttribute> iterator = list.iterator();
    while (iterator.hasNext()) {
      XmlAttribute xmlAttribute = iterator.next();
      if (xmlAttribute.getName().equals(paramString))
        return xmlAttribute; 
    } 
    return null;
  }
  
  public XmlAttribute[] getAllAttributes() {
    List<XmlAttribute> list = this.mAttributes;
    if (list == null)
      return new XmlAttribute[0]; 
    XmlAttribute[] arrayOfXmlAttribute = new XmlAttribute[list.size()];
    if (arrayOfXmlAttribute.length <= 0)
      return arrayOfXmlAttribute; 
    this.mAttributes.toArray(arrayOfXmlAttribute);
    return arrayOfXmlAttribute;
  }
  
  public int numOfAllChildNodes() {
    List<XmlNode> list = this.mNodes;
    if (list == null)
      return 0; 
    return list.size();
  }
  
  public int numOfAllAttributes() {
    List<XmlAttribute> list = this.mAttributes;
    if (list == null)
      return 0; 
    return list.size();
  }
  
  public XmlNode getParentNode() {
    return this.mParent;
  }
  
  public String toXml() {
    return null;
  }
}
