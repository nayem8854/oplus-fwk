package com.color.notification.redpackage.xmlsax;

import android.util.Log;
import java.io.InputStream;
import java.io.StringReader;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class DefaultXmlParse extends XmlParse {
  XMLReader mXmlReader;
  
  XmlImpl mXml;
  
  String mTemp = "";
  
  private static final boolean DEBUG = Log.isLoggable(TAG, 3);
  
  public DefaultXmlParse() {
    try {
      SAXParserFactory sAXParserFactory = SAXParserFactory.newInstance();
      SAXParser sAXParser = sAXParserFactory.newSAXParser();
      this.mXmlReader = sAXParser.getXMLReader();
    } catch (SAXException sAXException) {
      Log.e(TAG, "SAXException ", sAXException);
    } catch (ParserConfigurationException parserConfigurationException) {
      Log.e(TAG, "ParserConfigurationException ", parserConfigurationException);
    } 
  }
  
  public Xml parse(InputStream paramInputStream) {
    try {
      XMLReader xMLReader = this.mXmlReader;
      XMLParseHandler xMLParseHandler = new XMLParseHandler();
      this(this);
      xMLReader.setContentHandler(xMLParseHandler);
      xMLReader = this.mXmlReader;
      InputSource inputSource = new InputSource();
      this(paramInputStream);
      xMLReader.parse(inputSource);
    } catch (Exception exception) {
      if (DEBUG) {
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("parse stream: ");
        stringBuilder.append(exception.getMessage());
        Log.d(str, stringBuilder.toString());
      } 
    } finally {}
    return this.mXml;
  }
  
  public Xml parse(String paramString) {
    try {
      XMLReader xMLReader = this.mXmlReader;
      XMLParseHandler xMLParseHandler = new XMLParseHandler();
      this(this);
      xMLReader.setContentHandler(xMLParseHandler);
      InputSource inputSource = new InputSource();
      StringReader stringReader = new StringReader();
      this(paramString);
      this(stringReader);
      this.mXmlReader.parse(inputSource);
    } catch (Exception exception) {
      if (DEBUG) {
        String str2 = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("parse string: ");
        stringBuilder.append(exception.getMessage());
        Log.d(str2, stringBuilder.toString());
        String str1 = TAG;
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("parse string: ");
        stringBuilder.append(paramString);
        Log.d(str1, stringBuilder.toString());
      } 
    } finally {}
    return this.mXml;
  }
  
  class XMLParseHandler extends DefaultHandler {
    XmlNodeImpl mXmlNode;
    
    final DefaultXmlParse this$0;
    
    public void startDocument() throws SAXException {}
    
    public void startElement(String param1String1, String param1String2, String param1String3, Attributes param1Attributes) throws SAXException {
      DefaultXmlParse.this.mTemp = "";
      if (this.mXmlNode == null) {
        this.mXmlNode = new XmlNodeImpl(param1String2);
      } else {
        XmlNodeImpl xmlNodeImpl = new XmlNodeImpl(param1String2);
        this.mXmlNode.addChildNode(xmlNodeImpl);
        this.mXmlNode = xmlNodeImpl;
      } 
      if (param1Attributes == null)
        return; 
      for (byte b = 0; b < param1Attributes.getLength(); b++) {
        param1String2 = param1Attributes.getLocalName(b);
        XmlAttributeImpl xmlAttributeImpl = new XmlAttributeImpl(param1String2);
        xmlAttributeImpl.setValue(param1Attributes.getValue(param1String2));
        this.mXmlNode.addAttributes(xmlAttributeImpl);
      } 
    }
    
    public void characters(char[] param1ArrayOfchar, int param1Int1, int param1Int2) throws SAXException {
      String str = new String(param1ArrayOfchar, param1Int1, param1Int2);
      if (!"".equals(str)) {
        StringBuilder stringBuilder = new StringBuilder();
        DefaultXmlParse defaultXmlParse = DefaultXmlParse.this;
        stringBuilder.append(defaultXmlParse.mTemp);
        stringBuilder.append(str);
        defaultXmlParse.mTemp = stringBuilder.toString();
      } 
    }
    
    public void endElement(String param1String1, String param1String2, String param1String3) throws SAXException {
      this.mXmlNode.setValue(DefaultXmlParse.this.mTemp);
      DefaultXmlParse.this.mTemp = "";
      XmlNodeImpl xmlNodeImpl = (XmlNodeImpl)this.mXmlNode.getParentNode();
      if (xmlNodeImpl == null)
        return; 
      this.mXmlNode = xmlNodeImpl;
    }
    
    public void endDocument() throws SAXException {
      if (DefaultXmlParse.this.mXml == null)
        DefaultXmlParse.this.mXml = new XmlImpl(); 
      DefaultXmlParse.this.mXml.setmXmlNode(this.mXmlNode);
    }
  }
}
