package com.color.notification.redpackage.xmlsax;

public interface XmlAttribute {
  String getName();
  
  XmlNode getParentNode();
  
  String getValue();
}
