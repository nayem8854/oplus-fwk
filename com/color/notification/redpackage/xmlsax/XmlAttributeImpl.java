package com.color.notification.redpackage.xmlsax;

class XmlAttributeImpl implements XmlAttribute {
  private String mName;
  
  private XmlNodeImpl mParent;
  
  private String mValue;
  
  XmlAttributeImpl(String paramString) {
    this.mName = paramString;
  }
  
  protected void setValue(String paramString) {
    this.mValue = paramString;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public String getValue() {
    return this.mValue;
  }
  
  public XmlNode getParentNode() {
    return this.mParent;
  }
  
  public void setParentNode(XmlNodeImpl paramXmlNodeImpl) {
    this.mParent = paramXmlNodeImpl;
  }
}
