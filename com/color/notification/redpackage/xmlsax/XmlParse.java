package com.color.notification.redpackage.xmlsax;

import java.io.InputStream;

public abstract class XmlParse {
  protected static final String TAG = XmlParse.class.getSimpleName();
  
  private static Class<DefaultXmlParse> sCalzz = DefaultXmlParse.class;
  
  public static XmlParse builder() {
    try {
      return sCalzz.newInstance();
    } catch (IllegalAccessException illegalAccessException) {
      illegalAccessException.printStackTrace();
    } catch (InstantiationException instantiationException) {
      instantiationException.printStackTrace();
    } 
    return null;
  }
  
  public abstract Xml parse(InputStream paramInputStream);
  
  public abstract Xml parse(String paramString);
}
