package com.color.notification.redpackage.xmlsax;

class XmlImpl implements Xml {
  private XmlNode mXmlNode;
  
  public XmlNode getRootNode() {
    return this.mXmlNode;
  }
  
  protected void setmXmlNode(XmlNode paramXmlNode) {
    this.mXmlNode = paramXmlNode;
  }
  
  public String toXml() {
    return this.mXmlNode.toXml();
  }
}
