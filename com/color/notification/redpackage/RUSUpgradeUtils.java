package com.color.notification.redpackage;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import com.color.notification.redpackage.xmlsax.Xml;
import com.color.notification.redpackage.xmlsax.XmlAttribute;
import com.color.notification.redpackage.xmlsax.XmlNode;
import com.color.notification.redpackage.xmlsax.XmlParse;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class RUSUpgradeUtils {
  public static final String ATTRIBUTE_CONTENT_TAG = "content_tag";
  
  public static final String ATTRIBUTE_FILTER_FIELD = "filter_field";
  
  public static final String ATTRIBUTE_FILTER_VALUE = "filter_value";
  
  public static final String ATTRIBUTE_GROUP_TAG = "group_tag";
  
  public static final String ATTRIBUTE_KEY_VERSION = "pkg_version";
  
  public static final String ATTRIBUTE_USER_FIELD = "user_field";
  
  public static final String ATTRIBUTE_USER_NAME_TAG_FIRST = "user_name_tag_first";
  
  public static final String ATTRIBUTE_USER_NAME_TAG_LAST = "user_name_tag_last";
  
  public static final String COLUMN_NAME_XML = "xml";
  
  private static final boolean DEBUG;
  
  public static final String KEY_CONFIG = "config";
  
  public static final String KEY_REDPACKAGE = "redpackage";
  
  static {
    String str = RUSUpgradeUtils.class.getSimpleName();
    DEBUG = Log.isLoggable(str, 3);
  }
  
  private static final Uri ROM_UPDATE_CONTENT_URI = Uri.parse("content://com.nearme.romupdate.provider.db/update_list");
  
  private static final String TAG;
  
  private static final String XML_KEY_VERSION = "version";
  
  public static String getDataFromProvider(Context paramContext, String paramString) {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aconst_null
    //   3: astore_3
    //   4: aload_2
    //   5: astore #4
    //   7: aload_0
    //   8: invokevirtual getContentResolver : ()Landroid/content/ContentResolver;
    //   11: astore #5
    //   13: aload_2
    //   14: astore #4
    //   16: getstatic com/color/notification/redpackage/RUSUpgradeUtils.ROM_UPDATE_CONTENT_URI : Landroid/net/Uri;
    //   19: astore_0
    //   20: aload_2
    //   21: astore #4
    //   23: new java/lang/StringBuilder
    //   26: astore #6
    //   28: aload_2
    //   29: astore #4
    //   31: aload #6
    //   33: invokespecial <init> : ()V
    //   36: aload_2
    //   37: astore #4
    //   39: aload #6
    //   41: ldc 'filtername="'
    //   43: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   46: pop
    //   47: aload_2
    //   48: astore #4
    //   50: aload #6
    //   52: aload_1
    //   53: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   56: pop
    //   57: aload_2
    //   58: astore #4
    //   60: aload #6
    //   62: ldc '"'
    //   64: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   67: pop
    //   68: aload_2
    //   69: astore #4
    //   71: aload #6
    //   73: invokevirtual toString : ()Ljava/lang/String;
    //   76: astore_1
    //   77: aload_2
    //   78: astore #4
    //   80: aload #5
    //   82: aload_0
    //   83: iconst_1
    //   84: anewarray java/lang/String
    //   87: dup
    //   88: iconst_0
    //   89: ldc 'xml'
    //   91: aastore
    //   92: aload_1
    //   93: aconst_null
    //   94: aconst_null
    //   95: invokevirtual query : (Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   98: astore_1
    //   99: aload_1
    //   100: ifnull -> 153
    //   103: aload_1
    //   104: astore #4
    //   106: aload_1
    //   107: invokeinterface getCount : ()I
    //   112: ifle -> 153
    //   115: aload_1
    //   116: astore #4
    //   118: aload_1
    //   119: ldc 'xml'
    //   121: invokeinterface getColumnIndex : (Ljava/lang/String;)I
    //   126: istore #7
    //   128: aload_1
    //   129: astore #4
    //   131: aload_1
    //   132: invokeinterface moveToNext : ()Z
    //   137: pop
    //   138: aload_1
    //   139: astore #4
    //   141: aload_1
    //   142: iload #7
    //   144: invokeinterface getString : (I)Ljava/lang/String;
    //   149: astore_0
    //   150: goto -> 167
    //   153: aload_1
    //   154: astore #4
    //   156: getstatic com/color/notification/redpackage/RUSUpgradeUtils.TAG : Ljava/lang/String;
    //   159: ldc 'The Filtrate app cursor is null !!!'
    //   161: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   164: pop
    //   165: aload_3
    //   166: astore_0
    //   167: aload_1
    //   168: ifnull -> 180
    //   171: aload_1
    //   172: astore #4
    //   174: aload_1
    //   175: invokeinterface close : ()V
    //   180: aload_0
    //   181: areturn
    //   182: astore_0
    //   183: aload #4
    //   185: ifnull -> 195
    //   188: aload #4
    //   190: invokeinterface close : ()V
    //   195: getstatic com/color/notification/redpackage/RUSUpgradeUtils.TAG : Ljava/lang/String;
    //   198: ldc 'We can not get Filtrate app data from provider'
    //   200: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   203: pop
    //   204: aconst_null
    //   205: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #72	-> 0
    //   #73	-> 2
    //   #75	-> 4
    //   #77	-> 4
    //   #80	-> 99
    //   #81	-> 115
    //   #82	-> 128
    //   #83	-> 138
    //   #84	-> 150
    //   #85	-> 153
    //   #88	-> 167
    //   #89	-> 171
    //   #90	-> 180
    //   #93	-> 180
    //   #94	-> 182
    //   #95	-> 183
    //   #96	-> 188
    //   #98	-> 195
    //   #99	-> 204
    // Exception table:
    //   from	to	target	type
    //   7	13	182	java/lang/Exception
    //   16	20	182	java/lang/Exception
    //   23	28	182	java/lang/Exception
    //   31	36	182	java/lang/Exception
    //   39	47	182	java/lang/Exception
    //   50	57	182	java/lang/Exception
    //   60	68	182	java/lang/Exception
    //   71	77	182	java/lang/Exception
    //   80	99	182	java/lang/Exception
    //   106	115	182	java/lang/Exception
    //   118	128	182	java/lang/Exception
    //   131	138	182	java/lang/Exception
    //   141	150	182	java/lang/Exception
    //   156	165	182	java/lang/Exception
    //   174	180	182	java/lang/Exception
  }
  
  public static String getDataVersionFromProvider(Context paramContext, String paramString) {
    String str1;
    Cursor cursor;
    String str2 = TAG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("getDataVersionFromProvider configName:");
    stringBuilder.append(paramString);
    Log.d(str2, stringBuilder.toString());
    String str4 = null;
    String str5 = "";
    str2 = str4;
    String str3 = str5;
    try {
      ContentResolver contentResolver = paramContext.getContentResolver();
      str2 = str4;
      str3 = str5;
      Uri uri = ROM_UPDATE_CONTENT_URI;
      str2 = str4;
      str3 = str5;
      StringBuilder stringBuilder1 = new StringBuilder();
      str2 = str4;
      str3 = str5;
      this();
      str2 = str4;
      str3 = str5;
      stringBuilder1.append("filtername=\"");
      str2 = str4;
      str3 = str5;
      stringBuilder1.append(paramString);
      str2 = str4;
      str3 = str5;
      stringBuilder1.append("\"");
      str2 = str4;
      str3 = str5;
      String str = stringBuilder1.toString();
      str2 = str4;
      str3 = str5;
      Cursor cursor1 = contentResolver.query(uri, new String[] { "version" }, str, null, null);
      str1 = str5;
      if (cursor1 != null) {
        str1 = str5;
        cursor = cursor1;
        str3 = str5;
        if (cursor1.getCount() > 0) {
          cursor = cursor1;
          str3 = str5;
          int i = cursor1.getColumnIndex("version");
          cursor = cursor1;
          str3 = str5;
          cursor1.moveToNext();
          cursor = cursor1;
          str3 = str5;
          str1 = cursor1.getString(i);
        } 
      } 
      if (cursor1 != null) {
        cursor = cursor1;
        str3 = str1;
        cursor1.close();
      } 
    } catch (Exception exception) {
      if (cursor != null)
        cursor.close(); 
      String str = TAG;
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("We can not get data version with ");
      stringBuilder1.append(paramString);
      stringBuilder1.append(" from provider");
      Log.d(str, stringBuilder1.toString());
      str1 = str3;
    } 
    return str1;
  }
  
  public static void setRedPackageRUSVersion2Local(Context paramContext, String paramString) {
    Settings.Secure.putStringForUser(paramContext.getContentResolver(), "sys_systemui_redpackage_assist_config", paramString, 0);
  }
  
  public static String getRedPackageRUSVersion(Context paramContext) {
    return Settings.Secure.getStringForUser(paramContext.getContentResolver(), "sys_systemui_redpackage_assist_config", 0);
  }
  
  public static List<AdaptationEnvelopeInfo> parseRedpackageString2List(String paramString) {
    ArrayList<AdaptationEnvelopeInfo> arrayList = new ArrayList();
    try {
      XmlParse xmlParse = XmlParse.builder();
      Xml xml = xmlParse.parse(paramString);
      XmlNode xmlNode = xml.getRootNode();
      for (XmlNode xmlNode1 : xmlNode.getAllChildNodes()) {
        String str;
        if ("redpackage".equals(xmlNode1.getName())) {
          XmlAttribute xmlAttribute = xmlNode1.getAttribute("pkg_version");
          if (xmlAttribute != null) {
            AdaptationEnvelopeInfo adaptationEnvelopeInfo = parseRedPackageAsTag(xmlNode1);
            if (DEBUG) {
              str = TAG;
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("the node AdaptationEnvelopeInfo is : ");
              stringBuilder.append(adaptationEnvelopeInfo);
              Log.d(str, stringBuilder.toString());
            } 
            arrayList.add(adaptationEnvelopeInfo);
          } else if (DEBUG) {
            String str1 = TAG;
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("parseConfigList: pkg is null:");
            stringBuilder.append(str.toXml());
            Log.d(str1, stringBuilder.toString());
          } 
        } else if (DEBUG) {
          String str1 = TAG;
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("parseConfigList:warning:unknown tag:");
          stringBuilder.append(str.toXml());
          Log.d(str1, stringBuilder.toString());
        } 
      } 
    } catch (Exception exception) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("parseConfigList:error:");
      stringBuilder.append(exception.getMessage());
      stringBuilder.append(",xml=");
      stringBuilder.append(paramString);
      Log.d(str, stringBuilder.toString(), exception);
    } 
    return arrayList;
  }
  
  public static AdaptationEnvelopeInfo parseRedPackageAsTag(XmlNode paramXmlNode) {
    AdaptationEnvelopeInfo adaptationEnvelopeInfo3, adaptationEnvelopeInfo1 = null;
    AdaptationEnvelopeInfo adaptationEnvelopeInfo2 = adaptationEnvelopeInfo1;
    try {
      adaptationEnvelopeInfo3 = new AdaptationEnvelopeInfo();
      adaptationEnvelopeInfo2 = adaptationEnvelopeInfo1;
      this();
      adaptationEnvelopeInfo2 = adaptationEnvelopeInfo3;
      if (paramXmlNode.getAttribute("pkg_version") != null) {
        adaptationEnvelopeInfo2 = adaptationEnvelopeInfo3;
        adaptationEnvelopeInfo3.setPkgVersion(paramXmlNode.getAttribute("pkg_version").getValue());
      } 
      adaptationEnvelopeInfo2 = adaptationEnvelopeInfo3;
      if (paramXmlNode.getAttribute("filter_field") != null) {
        adaptationEnvelopeInfo2 = adaptationEnvelopeInfo3;
        adaptationEnvelopeInfo3.setEnvelopeFilterField(paramXmlNode.getAttribute("filter_field").getValue());
      } 
      adaptationEnvelopeInfo2 = adaptationEnvelopeInfo3;
      if (paramXmlNode.getAttribute("filter_value") != null) {
        adaptationEnvelopeInfo2 = adaptationEnvelopeInfo3;
        adaptationEnvelopeInfo3.setEnvelopeFilterValue(paramXmlNode.getAttribute("filter_value").getValue());
      } 
      adaptationEnvelopeInfo2 = adaptationEnvelopeInfo3;
      if (paramXmlNode.getAttribute("user_field") != null) {
        adaptationEnvelopeInfo2 = adaptationEnvelopeInfo3;
        adaptationEnvelopeInfo3.setEnvelopeUserField(paramXmlNode.getAttribute("user_field").getValue());
      } 
      adaptationEnvelopeInfo2 = adaptationEnvelopeInfo3;
      if (paramXmlNode.getAttribute("group_tag") != null) {
        adaptationEnvelopeInfo2 = adaptationEnvelopeInfo3;
        adaptationEnvelopeInfo3.setEnvelopeGroupTag(paramXmlNode.getAttribute("group_tag").getValue());
      } 
      adaptationEnvelopeInfo2 = adaptationEnvelopeInfo3;
      if (paramXmlNode.getAttribute("user_name_tag_first") != null) {
        adaptationEnvelopeInfo2 = adaptationEnvelopeInfo3;
        adaptationEnvelopeInfo3.setEnvelopeUserNameTagFirst(paramXmlNode.getAttribute("user_name_tag_first").getValue());
      } 
      adaptationEnvelopeInfo2 = adaptationEnvelopeInfo3;
      if (paramXmlNode.getAttribute("user_name_tag_last") != null) {
        adaptationEnvelopeInfo2 = adaptationEnvelopeInfo3;
        adaptationEnvelopeInfo3.setEnvelopeUserNameTagLast(paramXmlNode.getAttribute("user_name_tag_last").getValue());
      } 
      adaptationEnvelopeInfo2 = adaptationEnvelopeInfo3;
      if (paramXmlNode.getAttribute("content_tag") != null) {
        adaptationEnvelopeInfo2 = adaptationEnvelopeInfo3;
        adaptationEnvelopeInfo3.setEnvelopeContentTag(paramXmlNode.getAttribute("content_tag").getValue());
      } 
    } catch (Exception exception) {
      adaptationEnvelopeInfo3 = adaptationEnvelopeInfo2;
      if (DEBUG) {
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("parseAsPackageTag:error:");
        stringBuilder.append(exception.getMessage());
        Log.d(str, stringBuilder.toString());
        adaptationEnvelopeInfo3 = adaptationEnvelopeInfo2;
      } 
    } 
    return adaptationEnvelopeInfo3;
  }
  
  public static String inputStream2String(InputStream paramInputStream) {
    try {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      this();
      try {
        while (true) {
          int i = paramInputStream.read();
          if (i != -1) {
            byteArrayOutputStream.write(i);
            continue;
          } 
          break;
        } 
        return byteArrayOutputStream.toString(Charset.defaultCharset().toString());
      } finally {
        try {
          byteArrayOutputStream.close();
        } finally {
          byteArrayOutputStream = null;
        } 
      } 
    } catch (Exception exception) {
      if (DEBUG) {
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("inputStream2String--e:");
        stringBuilder.append(exception.getMessage());
        Log.d(str, stringBuilder.toString());
      } 
      return "";
    } 
  }
  
  public static String getConfigVersion(String paramString) {
    String str = "";
    if (TextUtils.isEmpty(paramString))
      return ""; 
    try {
      XmlNode xmlNode2;
      XmlParse xmlParse = XmlParse.builder();
      Xml xml = xmlParse.parse(paramString);
      xmlParse = null;
      XmlNode xmlNode1 = xml.getRootNode();
      if (xmlNode1 != null)
        xmlNode2 = xmlNode1.getChildNode("version"); 
      String str1 = str;
      if (xmlNode2 != null)
        str1 = xmlNode2.getValue().trim(); 
    } catch (Exception exception) {
      paramString = str;
      if (DEBUG) {
        exception.printStackTrace();
        paramString = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("getConfigVersion--e:");
        stringBuilder.append(exception.getMessage());
        Log.d(paramString, stringBuilder.toString());
        paramString = str;
      } 
    } 
    if (DEBUG) {
      str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getConfigVersion:");
      stringBuilder.append(paramString);
      Log.d(str, stringBuilder.toString());
    } 
    return paramString;
  }
  
  public static void saveStrToFile(Context paramContext, String paramString1, String paramString2) {
    if (paramContext == null || paramString1 == null || paramString2 == null)
      return; 
    File file1 = new File("/data/oppo/coloros/notification");
    if (!file1.exists())
      try {
        file1.mkdirs();
      } catch (Exception exception) {
        String str1 = TAG;
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("failed create path, e = ");
        stringBuilder1.append(exception.getMessage());
        Log.d(str1, stringBuilder1.toString(), exception);
      }  
    String str = TAG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("saveStrToFile:");
    stringBuilder.append(file1);
    stringBuilder.append("/");
    stringBuilder.append(paramString1);
    Log.d(str, stringBuilder.toString());
    File file2 = new File(file1, paramString1);
    if (!file2.exists())
      try {
        file2.createNewFile();
      } catch (IOException iOException) {
        String str1 = TAG;
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("failed write file ");
        stringBuilder1.append(iOException);
        Log.d(str1, stringBuilder1.toString());
      }  
    writeStrToFile(file2, paramString2);
  }
  
  private static void writeStrToFile(File paramFile, String paramString) {
    String str;
    if (paramFile == null)
      return; 
    FileOutputStream fileOutputStream1 = null, fileOutputStream2 = null, fileOutputStream3 = null, fileOutputStream4 = null, fileOutputStream5 = null;
    FileOutputStream fileOutputStream6 = fileOutputStream5, fileOutputStream7 = fileOutputStream1, fileOutputStream8 = fileOutputStream2, fileOutputStream9 = fileOutputStream3, fileOutputStream10 = fileOutputStream4;
    try {
      FileOutputStream fileOutputStream12 = new FileOutputStream();
      fileOutputStream6 = fileOutputStream5;
      fileOutputStream7 = fileOutputStream1;
      fileOutputStream8 = fileOutputStream2;
      fileOutputStream9 = fileOutputStream3;
      fileOutputStream10 = fileOutputStream4;
      this(paramFile);
      FileOutputStream fileOutputStream11 = fileOutputStream12;
      fileOutputStream6 = fileOutputStream11;
      fileOutputStream7 = fileOutputStream11;
      fileOutputStream8 = fileOutputStream11;
      fileOutputStream9 = fileOutputStream11;
      fileOutputStream10 = fileOutputStream11;
      fileOutputStream11.write(paramString.getBytes(StandardCharsets.UTF_8));
      fileOutputStream6 = fileOutputStream11;
      fileOutputStream7 = fileOutputStream11;
      fileOutputStream8 = fileOutputStream11;
      fileOutputStream9 = fileOutputStream11;
      fileOutputStream10 = fileOutputStream11;
      fileOutputStream11.flush();
      try {
        fileOutputStream11.close();
      } catch (IOException iOException1) {
        paramString = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("failed write file ");
        stringBuilder.append(iOException1);
        Log.d(paramString, stringBuilder.toString());
      } 
    } catch (IllegalArgumentException illegalArgumentException) {
      fileOutputStream6 = fileOutputStream10;
      paramString = TAG;
      fileOutputStream6 = fileOutputStream10;
      StringBuilder stringBuilder = new StringBuilder();
      fileOutputStream6 = fileOutputStream10;
      this();
      fileOutputStream6 = fileOutputStream10;
      stringBuilder.append("failed write file ");
      fileOutputStream6 = fileOutputStream10;
      stringBuilder.append(illegalArgumentException);
      fileOutputStream6 = fileOutputStream10;
      Log.d(paramString, stringBuilder.toString());
      if (fileOutputStream10 != null)
        try {
          fileOutputStream10.close();
        } catch (IOException iOException1) {
          paramString = TAG;
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("failed write file ");
          stringBuilder1.append(iOException1);
          Log.d(paramString, stringBuilder1.toString());
        }  
    } catch (IllegalStateException illegalStateException) {
      fileOutputStream6 = fileOutputStream9;
      str = TAG;
      fileOutputStream6 = fileOutputStream9;
      StringBuilder stringBuilder = new StringBuilder();
      fileOutputStream6 = fileOutputStream9;
      this();
      fileOutputStream6 = fileOutputStream9;
      stringBuilder.append("failed write file ");
      fileOutputStream6 = fileOutputStream9;
      stringBuilder.append(illegalStateException);
      fileOutputStream6 = fileOutputStream9;
      Log.d(str, stringBuilder.toString());
      if (fileOutputStream9 != null)
        try {
          fileOutputStream9.close();
        } catch (IOException iOException1) {
          String str1 = TAG;
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("failed write file ");
          stringBuilder1.append(iOException1);
          Log.d(str1, stringBuilder1.toString());
        }  
    } catch (IOException iOException) {
      String str1 = str;
      paramString = TAG;
      str1 = str;
      StringBuilder stringBuilder = new StringBuilder();
      str1 = str;
      this();
      str1 = str;
      stringBuilder.append("failed write file ");
      str1 = str;
      stringBuilder.append(iOException);
      str1 = str;
      Log.d(paramString, stringBuilder.toString());
      if (str != null)
        try {
          str.close();
        } catch (IOException iOException1) {
          paramString = TAG;
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("failed write file ");
          stringBuilder1.append(iOException1);
          Log.d(paramString, stringBuilder1.toString());
        }  
    } catch (Exception exception) {
      IOException iOException1 = iOException;
      paramString = TAG;
      iOException1 = iOException;
      StringBuilder stringBuilder = new StringBuilder();
      iOException1 = iOException;
      this();
      iOException1 = iOException;
      stringBuilder.append("failed write file ");
      iOException1 = iOException;
      stringBuilder.append(exception);
      iOException1 = iOException;
      Log.d(paramString, stringBuilder.toString());
      if (iOException != null)
        try {
          iOException.close();
        } catch (IOException iOException2) {
          paramString = TAG;
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("failed write file ");
          stringBuilder1.append(iOException2);
          Log.d(paramString, stringBuilder1.toString());
        }  
    } finally {}
  }
}
