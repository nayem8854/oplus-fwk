package com.color.notification.redpackage;

public class AdaptationEnvelopeInfo {
  String mEnvelopeContentTag;
  
  String mEnvelopeFilterField;
  
  String mEnvelopeFilterValue;
  
  String mEnvelopeGroupTag;
  
  String mEnvelopeUserField;
  
  String mEnvelopeUserNameTagFirst;
  
  String mEnvelopeUserNameTagLast;
  
  String mPkgVersion;
  
  public String getPkgVersion() {
    return this.mPkgVersion;
  }
  
  public void setPkgVersion(String paramString) {
    this.mPkgVersion = paramString;
  }
  
  public String getEnvelopeFilterField() {
    return this.mEnvelopeFilterField;
  }
  
  public void setEnvelopeFilterField(String paramString) {
    this.mEnvelopeFilterField = paramString;
  }
  
  public String getEnvelopeFilterValue() {
    return this.mEnvelopeFilterValue;
  }
  
  public void setEnvelopeFilterValue(String paramString) {
    this.mEnvelopeFilterValue = paramString;
  }
  
  public String getEnvelopeUserField() {
    return this.mEnvelopeUserField;
  }
  
  public void setEnvelopeUserField(String paramString) {
    this.mEnvelopeUserField = paramString;
  }
  
  public String getEnvelopeGroupTag() {
    return this.mEnvelopeGroupTag;
  }
  
  public void setEnvelopeGroupTag(String paramString) {
    this.mEnvelopeGroupTag = paramString;
  }
  
  public String getEnvelopeUserNameTagFirst() {
    return this.mEnvelopeUserNameTagFirst;
  }
  
  public void setEnvelopeUserNameTagFirst(String paramString) {
    this.mEnvelopeUserNameTagFirst = paramString;
  }
  
  public String getEnvelopeUserNameTagLast() {
    return this.mEnvelopeUserNameTagLast;
  }
  
  public void setEnvelopeUserNameTagLast(String paramString) {
    this.mEnvelopeUserNameTagLast = paramString;
  }
  
  public String getEnvelopeContentTag() {
    return this.mEnvelopeContentTag;
  }
  
  public void setEnvelopeContentTag(String paramString) {
    this.mEnvelopeContentTag = paramString;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    String str = this.mPkgVersion;
    stringBuilder.append(str);
    str = this.mEnvelopeFilterField;
    stringBuilder.append(str);
    str = this.mEnvelopeFilterValue;
    stringBuilder.append(str);
    str = this.mEnvelopeUserField;
    stringBuilder.append(str);
    str = this.mEnvelopeGroupTag;
    stringBuilder.append(str);
    str = this.mEnvelopeUserNameTagFirst;
    stringBuilder.append(str);
    str = this.mEnvelopeUserNameTagLast;
    stringBuilder.append(str);
    str = this.mEnvelopeContentTag;
    stringBuilder.append(str);
    return stringBuilder.toString();
  }
}
