package com.color.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.oplus.internal.R;

public class OplusDragTextShadowContainer extends ViewGroup {
  private final float mCornerRadius;
  
  private final float mDeltaLength;
  
  private boolean mDrawShadow;
  
  private float mDx;
  
  private float mDy;
  
  private int mShadowColor;
  
  private float mShadowRadius;
  
  public OplusDragTextShadowContainer(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public OplusDragTextShadowContainer(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public OplusDragTextShadowContainer(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ColorGlobalDragTextShadowContainer);
    this.mShadowColor = typedArray.getColor(2, -65536);
    this.mShadowRadius = typedArray.getDimension(3, 0.0F);
    this.mDeltaLength = typedArray.getDimension(1, 0.0F);
    this.mCornerRadius = typedArray.getDimension(0, 0.0F);
    this.mDx = typedArray.getDimension(4, 0.0F);
    this.mDy = typedArray.getDimension(5, 0.0F);
    this.mDrawShadow = typedArray.getBoolean(6, true);
    typedArray.recycle();
  }
  
  protected void onFinishInflate() {
    super.onFinishInflate();
  }
  
  protected void dispatchDraw(Canvas paramCanvas) {
    if (this.mDrawShadow) {
      if (getLayerType() != 1)
        setLayerType(1, null); 
      Paint paint = new Paint();
      paint.setStyle(Paint.Style.FILL);
      paint.setAntiAlias(true);
      paint.setColor(this.mShadowColor);
      paint.setColor(0);
      paint.setShadowLayer(this.mShadowRadius, this.mDx, this.mDy, this.mShadowColor);
      View view = getChildAt(0);
      int i = view.getLeft();
      int j = view.getTop();
      int k = view.getRight();
      int m = view.getBottom();
      RectF rectF = new RectF(i, j, k, m);
      float f = this.mShadowRadius;
      paramCanvas.drawRoundRect(rectF, f, f, paint);
    } 
    super.dispatchDraw(paramCanvas);
  }
  
  public void setmDrawShadow(boolean paramBoolean) {
    if (this.mDrawShadow == paramBoolean)
      return; 
    this.mDrawShadow = paramBoolean;
    postInvalidate();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    super.onMeasure(paramInt1, paramInt2);
    if (getChildCount() == 1) {
      int i5, i = getMeasuredWidth();
      int j = getMeasuredHeight();
      int k = View.MeasureSpec.getMode(paramInt1);
      int m = View.MeasureSpec.getMode(paramInt2);
      View view = getChildAt(0);
      LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
      int n = (int)(Math.max(this.mDeltaLength, layoutParams.bottomMargin) + 1.0F);
      int i1 = (int)(Math.max(this.mDeltaLength, layoutParams.leftMargin) + 1.0F);
      int i2 = (int)(Math.max(this.mDeltaLength, layoutParams.rightMargin) + 1.0F);
      int i3 = (int)(Math.max(this.mDeltaLength, layoutParams.topMargin) + 1.0F);
      if (k == 0) {
        k = 0;
        i4 = View.MeasureSpec.getSize(paramInt1);
      } else if (layoutParams.width == -1) {
        k = 1073741824;
        i4 = i - i1 - i2;
      } else if (-2 == layoutParams.width) {
        k = Integer.MIN_VALUE;
        i4 = i - i1 - i2;
      } else {
        k = 1073741824;
        i4 = layoutParams.width;
      } 
      if (m == 0) {
        m = 0;
        i5 = View.MeasureSpec.getSize(paramInt2);
      } else if (layoutParams.height == -1) {
        m = 1073741824;
        i5 = j - n - i3;
      } else if (-2 == layoutParams.height) {
        m = Integer.MIN_VALUE;
        i5 = j - n - i3;
      } else {
        m = 1073741824;
        i5 = layoutParams.height;
      } 
      measureChild(view, View.MeasureSpec.makeMeasureSpec(i4, k), View.MeasureSpec.makeMeasureSpec(i5, m));
      m = View.MeasureSpec.getMode(paramInt1);
      paramInt1 = View.MeasureSpec.getMode(paramInt2);
      k = view.getMeasuredHeight();
      int i4 = view.getMeasuredWidth();
      if (paramInt1 == Integer.MIN_VALUE) {
        paramInt1 = k + i3 + n;
      } else {
        paramInt1 = j;
      } 
      if (m == Integer.MIN_VALUE) {
        paramInt2 = i4 + i2 + i1;
      } else {
        paramInt2 = i;
      } 
      float f1 = paramInt2, f2 = i4, f3 = this.mDeltaLength;
      if (f1 < f2 + f3 * 2.0F)
        paramInt2 = (int)(i4 + f3 * 2.0F); 
      f1 = paramInt1;
      f2 = k;
      f3 = this.mDeltaLength;
      if (f1 < f2 + f3 * 2.0F)
        paramInt1 = (int)(k + f3 * 2.0F); 
      if (paramInt1 != j || paramInt2 != i)
        setMeasuredDimension(paramInt2, paramInt1); 
      return;
    } 
    throw new IllegalStateException("only one child!");
  }
  
  class LayoutParams extends ViewGroup.MarginLayoutParams {
    public LayoutParams(OplusDragTextShadowContainer this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
    }
    
    public LayoutParams(OplusDragTextShadowContainer this$0, int param1Int1) {
      super(this$0, param1Int1);
    }
    
    public LayoutParams(OplusDragTextShadowContainer this$0) {
      super((ViewGroup.MarginLayoutParams)this$0);
    }
    
    public LayoutParams(OplusDragTextShadowContainer this$0) {
      super((ViewGroup.LayoutParams)this$0);
    }
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
    return (ViewGroup.LayoutParams)new LayoutParams(-2, -2);
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return (ViewGroup.LayoutParams)new LayoutParams(paramLayoutParams);
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return (ViewGroup.LayoutParams)new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    View view = getChildAt(0);
    paramInt2 = getMeasuredWidth();
    paramInt4 = getMeasuredHeight();
    paramInt3 = view.getMeasuredWidth();
    paramInt1 = view.getMeasuredHeight();
    view.layout((paramInt2 - paramInt3) / 2, (paramInt4 - paramInt1) / 2, (paramInt2 + paramInt3) / 2, (paramInt4 + paramInt1) / 2);
  }
}
