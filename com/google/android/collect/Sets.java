package com.google.android.collect;

import android.util.ArraySet;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.SortedSet;
import java.util.TreeSet;

public class Sets {
  public static <K> HashSet<K> newHashSet() {
    return new HashSet<>();
  }
  
  public static <E> HashSet<E> newHashSet(E... paramVarArgs) {
    int i = paramVarArgs.length * 4 / 3;
    HashSet<? super E> hashSet = new HashSet(i + 1);
    Collections.addAll(hashSet, paramVarArgs);
    return (HashSet)hashSet;
  }
  
  public static <E> SortedSet<E> newSortedSet() {
    return new TreeSet<>();
  }
  
  public static <E> SortedSet<E> newSortedSet(E... paramVarArgs) {
    TreeSet<? super E> treeSet = new TreeSet();
    Collections.addAll(treeSet, paramVarArgs);
    return (SortedSet)treeSet;
  }
  
  public static <E> ArraySet<E> newArraySet() {
    return new ArraySet();
  }
  
  public static <E> ArraySet<E> newArraySet(E... paramVarArgs) {
    int i = paramVarArgs.length * 4 / 3;
    ArraySet<E> arraySet = new ArraySet(i + 1);
    Collections.addAll((Collection<? super E>)arraySet, paramVarArgs);
    return arraySet;
  }
}
