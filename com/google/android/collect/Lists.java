package com.google.android.collect;

import java.util.ArrayList;
import java.util.Collections;

public class Lists {
  public static <E> ArrayList<E> newArrayList() {
    return new ArrayList<>();
  }
  
  public static <E> ArrayList<E> newArrayList(E... paramVarArgs) {
    int i = paramVarArgs.length * 110 / 100;
    ArrayList<? super E> arrayList = new ArrayList(i + 5);
    Collections.addAll(arrayList, paramVarArgs);
    return (ArrayList)arrayList;
  }
}
