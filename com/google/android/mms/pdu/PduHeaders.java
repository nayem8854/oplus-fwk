package com.google.android.mms.pdu;

import com.google.android.mms.InvalidHeaderValueException;
import java.util.ArrayList;
import java.util.HashMap;

public class PduHeaders {
  public static final int ADAPTATION_ALLOWED = 188;
  
  public static final int ADDITIONAL_HEADERS = 176;
  
  public static final int APPLIC_ID = 183;
  
  public static final int ATTRIBUTES = 168;
  
  public static final int AUX_APPLIC_ID = 185;
  
  public static final int BCC = 129;
  
  public static final int CANCEL_ID = 190;
  
  public static final int CANCEL_STATUS = 191;
  
  public static final int CANCEL_STATUS_REQUEST_CORRUPTED = 129;
  
  public static final int CANCEL_STATUS_REQUEST_SUCCESSFULLY_RECEIVED = 128;
  
  public static final int CC = 130;
  
  public static final int CONTENT = 174;
  
  public static final int CONTENT_CLASS = 186;
  
  public static final int CONTENT_CLASS_CONTENT_BASIC = 134;
  
  public static final int CONTENT_CLASS_CONTENT_RICH = 135;
  
  public static final int CONTENT_CLASS_IMAGE_BASIC = 129;
  
  public static final int CONTENT_CLASS_IMAGE_RICH = 130;
  
  public static final int CONTENT_CLASS_MEGAPIXEL = 133;
  
  public static final int CONTENT_CLASS_TEXT = 128;
  
  public static final int CONTENT_CLASS_VIDEO_BASIC = 131;
  
  public static final int CONTENT_CLASS_VIDEO_RICH = 132;
  
  public static final int CONTENT_LOCATION = 131;
  
  public static final int CONTENT_TYPE = 132;
  
  public static final int CURRENT_MMS_VERSION = 18;
  
  public static final int DATE = 133;
  
  public static final int DELIVERY_REPORT = 134;
  
  public static final int DELIVERY_TIME = 135;
  
  public static final int DISTRIBUTION_INDICATOR = 177;
  
  public static final int DRM_CONTENT = 187;
  
  public static final int ELEMENT_DESCRIPTOR = 178;
  
  public static final int EXPIRY = 136;
  
  public static final int FROM = 137;
  
  public static final int FROM_ADDRESS_PRESENT_TOKEN = 128;
  
  public static final String FROM_ADDRESS_PRESENT_TOKEN_STR = "address-present-token";
  
  public static final int FROM_INSERT_ADDRESS_TOKEN = 129;
  
  public static final String FROM_INSERT_ADDRESS_TOKEN_STR = "insert-address-token";
  
  public static final int LIMIT = 179;
  
  public static final int MBOX_QUOTAS = 172;
  
  public static final int MBOX_TOTALS = 170;
  
  public static final int MESSAGE_CLASS = 138;
  
  public static final int MESSAGE_CLASS_ADVERTISEMENT = 129;
  
  public static final String MESSAGE_CLASS_ADVERTISEMENT_STR = "advertisement";
  
  public static final int MESSAGE_CLASS_AUTO = 131;
  
  public static final String MESSAGE_CLASS_AUTO_STR = "auto";
  
  public static final int MESSAGE_CLASS_INFORMATIONAL = 130;
  
  public static final String MESSAGE_CLASS_INFORMATIONAL_STR = "informational";
  
  public static final int MESSAGE_CLASS_PERSONAL = 128;
  
  public static final String MESSAGE_CLASS_PERSONAL_STR = "personal";
  
  public static final int MESSAGE_COUNT = 173;
  
  public static final int MESSAGE_ID = 139;
  
  public static final int MESSAGE_SIZE = 142;
  
  public static final int MESSAGE_TYPE = 140;
  
  public static final int MESSAGE_TYPE_ACKNOWLEDGE_IND = 133;
  
  public static final int MESSAGE_TYPE_CANCEL_CONF = 151;
  
  public static final int MESSAGE_TYPE_CANCEL_REQ = 150;
  
  public static final int MESSAGE_TYPE_DELETE_CONF = 149;
  
  public static final int MESSAGE_TYPE_DELETE_REQ = 148;
  
  public static final int MESSAGE_TYPE_DELIVERY_IND = 134;
  
  public static final int MESSAGE_TYPE_FORWARD_CONF = 138;
  
  public static final int MESSAGE_TYPE_FORWARD_REQ = 137;
  
  public static final int MESSAGE_TYPE_MBOX_DELETE_CONF = 146;
  
  public static final int MESSAGE_TYPE_MBOX_DELETE_REQ = 145;
  
  public static final int MESSAGE_TYPE_MBOX_DESCR = 147;
  
  public static final int MESSAGE_TYPE_MBOX_STORE_CONF = 140;
  
  public static final int MESSAGE_TYPE_MBOX_STORE_REQ = 139;
  
  public static final int MESSAGE_TYPE_MBOX_UPLOAD_CONF = 144;
  
  public static final int MESSAGE_TYPE_MBOX_UPLOAD_REQ = 143;
  
  public static final int MESSAGE_TYPE_MBOX_VIEW_CONF = 142;
  
  public static final int MESSAGE_TYPE_MBOX_VIEW_REQ = 141;
  
  public static final int MESSAGE_TYPE_NOTIFICATION_IND = 130;
  
  public static final int MESSAGE_TYPE_NOTIFYRESP_IND = 131;
  
  public static final int MESSAGE_TYPE_READ_ORIG_IND = 136;
  
  public static final int MESSAGE_TYPE_READ_REC_IND = 135;
  
  public static final int MESSAGE_TYPE_RETRIEVE_CONF = 132;
  
  public static final int MESSAGE_TYPE_SEND_CONF = 129;
  
  public static final int MESSAGE_TYPE_SEND_REQ = 128;
  
  public static final int MMS_VERSION = 141;
  
  public static final int MMS_VERSION_1_0 = 16;
  
  public static final int MMS_VERSION_1_1 = 17;
  
  public static final int MMS_VERSION_1_2 = 18;
  
  public static final int MMS_VERSION_1_3 = 19;
  
  public static final int MM_FLAGS = 164;
  
  public static final int MM_FLAGS_ADD_TOKEN = 128;
  
  public static final int MM_FLAGS_FILTER_TOKEN = 130;
  
  public static final int MM_FLAGS_REMOVE_TOKEN = 129;
  
  public static final int MM_STATE = 163;
  
  public static final int MM_STATE_DRAFT = 128;
  
  public static final int MM_STATE_FORWARDED = 132;
  
  public static final int MM_STATE_NEW = 130;
  
  public static final int MM_STATE_RETRIEVED = 131;
  
  public static final int MM_STATE_SENT = 129;
  
  public static final int PREVIOUSLY_SENT_BY = 160;
  
  public static final int PREVIOUSLY_SENT_DATE = 161;
  
  public static final int PRIORITY = 143;
  
  public static final int PRIORITY_HIGH = 130;
  
  public static final int PRIORITY_LOW = 128;
  
  public static final int PRIORITY_NORMAL = 129;
  
  public static final int QUOTAS = 171;
  
  public static final int READ_REPLY = 144;
  
  public static final int READ_REPORT = 144;
  
  public static final int READ_STATUS = 155;
  
  public static final int READ_STATUS_READ = 128;
  
  public static final int READ_STATUS__DELETED_WITHOUT_BEING_READ = 129;
  
  public static final int RECOMMENDED_RETRIEVAL_MODE = 180;
  
  public static final int RECOMMENDED_RETRIEVAL_MODE_MANUAL = 128;
  
  public static final int RECOMMENDED_RETRIEVAL_MODE_TEXT = 181;
  
  public static final int REPLACE_ID = 189;
  
  public static final int REPLY_APPLIC_ID = 184;
  
  public static final int REPLY_CHARGING = 156;
  
  public static final int REPLY_CHARGING_ACCEPTED = 130;
  
  public static final int REPLY_CHARGING_ACCEPTED_TEXT_ONLY = 131;
  
  public static final int REPLY_CHARGING_DEADLINE = 157;
  
  public static final int REPLY_CHARGING_ID = 158;
  
  public static final int REPLY_CHARGING_REQUESTED = 128;
  
  public static final int REPLY_CHARGING_REQUESTED_TEXT_ONLY = 129;
  
  public static final int REPLY_CHARGING_SIZE = 159;
  
  public static final int REPORT_ALLOWED = 145;
  
  public static final int RESPONSE_STATUS = 146;
  
  public static final int RESPONSE_STATUS_ERROR_CONTENT_NOT_ACCEPTED = 135;
  
  public static final int RESPONSE_STATUS_ERROR_MESSAGE_FORMAT_CORRUPT = 131;
  
  public static final int RESPONSE_STATUS_ERROR_MESSAGE_NOT_FOUND = 133;
  
  public static final int RESPONSE_STATUS_ERROR_NETWORK_PROBLEM = 134;
  
  public static final int RESPONSE_STATUS_ERROR_PERMANENT_ADDRESS_HIDING_NOT_SUPPORTED = 234;
  
  public static final int RESPONSE_STATUS_ERROR_PERMANENT_CONTENT_NOT_ACCEPTED = 229;
  
  public static final int RESPONSE_STATUS_ERROR_PERMANENT_END = 255;
  
  public static final int RESPONSE_STATUS_ERROR_PERMANENT_FAILURE = 224;
  
  public static final int RESPONSE_STATUS_ERROR_PERMANENT_LACK_OF_PREPAID = 235;
  
  public static final int RESPONSE_STATUS_ERROR_PERMANENT_MESSAGE_FORMAT_CORRUPT = 226;
  
  public static final int RESPONSE_STATUS_ERROR_PERMANENT_MESSAGE_NOT_FOUND = 228;
  
  public static final int RESPONSE_STATUS_ERROR_PERMANENT_REPLY_CHARGING_FORWARDING_DENIED = 232;
  
  public static final int RESPONSE_STATUS_ERROR_PERMANENT_REPLY_CHARGING_LIMITATIONS_NOT_MET = 230;
  
  public static final int RESPONSE_STATUS_ERROR_PERMANENT_REPLY_CHARGING_NOT_SUPPORTED = 233;
  
  public static final int RESPONSE_STATUS_ERROR_PERMANENT_REPLY_CHARGING_REQUEST_NOT_ACCEPTED = 230;
  
  public static final int RESPONSE_STATUS_ERROR_PERMANENT_SENDING_ADDRESS_UNRESOLVED = 227;
  
  public static final int RESPONSE_STATUS_ERROR_PERMANENT_SERVICE_DENIED = 225;
  
  public static final int RESPONSE_STATUS_ERROR_SENDING_ADDRESS_UNRESOLVED = 132;
  
  public static final int RESPONSE_STATUS_ERROR_SERVICE_DENIED = 130;
  
  public static final int RESPONSE_STATUS_ERROR_TRANSIENT_FAILURE = 192;
  
  public static final int RESPONSE_STATUS_ERROR_TRANSIENT_MESSAGE_NOT_FOUND = 194;
  
  public static final int RESPONSE_STATUS_ERROR_TRANSIENT_NETWORK_PROBLEM = 195;
  
  public static final int RESPONSE_STATUS_ERROR_TRANSIENT_PARTIAL_SUCCESS = 196;
  
  public static final int RESPONSE_STATUS_ERROR_TRANSIENT_SENDNG_ADDRESS_UNRESOLVED = 193;
  
  public static final int RESPONSE_STATUS_ERROR_UNSPECIFIED = 129;
  
  public static final int RESPONSE_STATUS_ERROR_UNSUPPORTED_MESSAGE = 136;
  
  public static final int RESPONSE_STATUS_OK = 128;
  
  public static final int RESPONSE_TEXT = 147;
  
  public static final int RETRIEVE_STATUS = 153;
  
  public static final int RETRIEVE_STATUS_ERROR_END = 255;
  
  public static final int RETRIEVE_STATUS_ERROR_PERMANENT_CONTENT_UNSUPPORTED = 227;
  
  public static final int RETRIEVE_STATUS_ERROR_PERMANENT_FAILURE = 224;
  
  public static final int RETRIEVE_STATUS_ERROR_PERMANENT_MESSAGE_NOT_FOUND = 226;
  
  public static final int RETRIEVE_STATUS_ERROR_PERMANENT_SERVICE_DENIED = 225;
  
  public static final int RETRIEVE_STATUS_ERROR_TRANSIENT_FAILURE = 192;
  
  public static final int RETRIEVE_STATUS_ERROR_TRANSIENT_MESSAGE_NOT_FOUND = 193;
  
  public static final int RETRIEVE_STATUS_ERROR_TRANSIENT_NETWORK_PROBLEM = 194;
  
  public static final int RETRIEVE_STATUS_OK = 128;
  
  public static final int RETRIEVE_TEXT = 154;
  
  public static final int SENDER_VISIBILITY = 148;
  
  public static final int SENDER_VISIBILITY_HIDE = 128;
  
  public static final int SENDER_VISIBILITY_SHOW = 129;
  
  public static final int START = 175;
  
  public static final int STATUS = 149;
  
  public static final int STATUS_DEFERRED = 131;
  
  public static final int STATUS_EXPIRED = 128;
  
  public static final int STATUS_FORWARDED = 134;
  
  public static final int STATUS_INDETERMINATE = 133;
  
  public static final int STATUS_REJECTED = 130;
  
  public static final int STATUS_RETRIEVED = 129;
  
  public static final int STATUS_TEXT = 182;
  
  public static final int STATUS_UNREACHABLE = 135;
  
  public static final int STATUS_UNRECOGNIZED = 132;
  
  public static final int STORE = 162;
  
  public static final int STORED = 167;
  
  public static final int STORE_STATUS = 165;
  
  public static final int STORE_STATUS_ERROR_END = 255;
  
  public static final int STORE_STATUS_ERROR_PERMANENT_FAILURE = 224;
  
  public static final int STORE_STATUS_ERROR_PERMANENT_MESSAGE_FORMAT_CORRUPT = 226;
  
  public static final int STORE_STATUS_ERROR_PERMANENT_MESSAGE_NOT_FOUND = 227;
  
  public static final int STORE_STATUS_ERROR_PERMANENT_MMBOX_FULL = 228;
  
  public static final int STORE_STATUS_ERROR_PERMANENT_SERVICE_DENIED = 225;
  
  public static final int STORE_STATUS_ERROR_TRANSIENT_FAILURE = 192;
  
  public static final int STORE_STATUS_ERROR_TRANSIENT_NETWORK_PROBLEM = 193;
  
  public static final int STORE_STATUS_SUCCESS = 128;
  
  public static final int STORE_STATUS_TEXT = 166;
  
  public static final int SUBJECT = 150;
  
  public static final int TO = 151;
  
  public static final int TOTALS = 169;
  
  public static final int TRANSACTION_ID = 152;
  
  public static final int VALUE_ABSOLUTE_TOKEN = 128;
  
  public static final int VALUE_NO = 129;
  
  public static final int VALUE_RELATIVE_TOKEN = 129;
  
  public static final int VALUE_YES = 128;
  
  private HashMap<Integer, Object> mHeaderMap = null;
  
  public PduHeaders() {
    this.mHeaderMap = new HashMap<>();
  }
  
  protected int getOctet(int paramInt) {
    Integer integer = (Integer)this.mHeaderMap.get(Integer.valueOf(paramInt));
    if (integer == null)
      return 0; 
    return integer.intValue();
  }
  
  protected void setOctet(int paramInt1, int paramInt2) throws InvalidHeaderValueException {
    // Byte code:
    //   0: iload_2
    //   1: sipush #134
    //   4: if_icmpeq -> 708
    //   7: iload_2
    //   8: sipush #153
    //   11: if_icmpeq -> 629
    //   14: iload_2
    //   15: sipush #165
    //   18: if_icmpeq -> 550
    //   21: iload_2
    //   22: sipush #167
    //   25: if_icmpeq -> 708
    //   28: iload_2
    //   29: sipush #169
    //   32: if_icmpeq -> 708
    //   35: iload_2
    //   36: sipush #171
    //   39: if_icmpeq -> 708
    //   42: iload_2
    //   43: sipush #177
    //   46: if_icmpeq -> 708
    //   49: iload_2
    //   50: sipush #180
    //   53: if_icmpeq -> 527
    //   56: iload_2
    //   57: sipush #191
    //   60: if_icmpeq -> 495
    //   63: iload_2
    //   64: sipush #140
    //   67: if_icmpeq -> 465
    //   70: iload_2
    //   71: sipush #141
    //   74: if_icmpeq -> 445
    //   77: iload_2
    //   78: sipush #148
    //   81: if_icmpeq -> 708
    //   84: iload_2
    //   85: sipush #149
    //   88: if_icmpeq -> 415
    //   91: iload_2
    //   92: sipush #155
    //   95: if_icmpeq -> 383
    //   98: iload_2
    //   99: sipush #156
    //   102: if_icmpeq -> 353
    //   105: iload_2
    //   106: sipush #162
    //   109: if_icmpeq -> 708
    //   112: iload_2
    //   113: sipush #163
    //   116: if_icmpeq -> 323
    //   119: iload_2
    //   120: tableswitch default -> 152, 143 -> 293, 144 -> 708, 145 -> 708, 146 -> 221
    //   152: iload_2
    //   153: tableswitch default -> 180, 186 -> 191, 187 -> 708, 188 -> 708
    //   180: new java/lang/RuntimeException
    //   183: dup
    //   184: ldc_w 'Invalid header field!'
    //   187: invokespecial <init> : (Ljava/lang/String;)V
    //   190: athrow
    //   191: iload_1
    //   192: sipush #128
    //   195: if_icmplt -> 210
    //   198: iload_1
    //   199: sipush #135
    //   202: if_icmpgt -> 210
    //   205: iload_1
    //   206: istore_3
    //   207: goto -> 740
    //   210: new com/google/android/mms/InvalidHeaderValueException
    //   213: dup
    //   214: ldc_w 'Invalid Octet value!'
    //   217: invokespecial <init> : (Ljava/lang/String;)V
    //   220: athrow
    //   221: iload_1
    //   222: sipush #196
    //   225: if_icmple -> 242
    //   228: iload_1
    //   229: sipush #224
    //   232: if_icmpge -> 242
    //   235: sipush #192
    //   238: istore_3
    //   239: goto -> 740
    //   242: iload_1
    //   243: sipush #235
    //   246: if_icmple -> 256
    //   249: iload_1
    //   250: sipush #255
    //   253: if_icmple -> 286
    //   256: iload_1
    //   257: sipush #128
    //   260: if_icmplt -> 286
    //   263: iload_1
    //   264: sipush #136
    //   267: if_icmple -> 277
    //   270: iload_1
    //   271: sipush #192
    //   274: if_icmplt -> 286
    //   277: iload_1
    //   278: istore_3
    //   279: iload_1
    //   280: sipush #255
    //   283: if_icmple -> 740
    //   286: sipush #224
    //   289: istore_3
    //   290: goto -> 740
    //   293: iload_1
    //   294: sipush #128
    //   297: if_icmplt -> 312
    //   300: iload_1
    //   301: sipush #130
    //   304: if_icmpgt -> 312
    //   307: iload_1
    //   308: istore_3
    //   309: goto -> 740
    //   312: new com/google/android/mms/InvalidHeaderValueException
    //   315: dup
    //   316: ldc_w 'Invalid Octet value!'
    //   319: invokespecial <init> : (Ljava/lang/String;)V
    //   322: athrow
    //   323: iload_1
    //   324: sipush #128
    //   327: if_icmplt -> 342
    //   330: iload_1
    //   331: sipush #132
    //   334: if_icmpgt -> 342
    //   337: iload_1
    //   338: istore_3
    //   339: goto -> 740
    //   342: new com/google/android/mms/InvalidHeaderValueException
    //   345: dup
    //   346: ldc_w 'Invalid Octet value!'
    //   349: invokespecial <init> : (Ljava/lang/String;)V
    //   352: athrow
    //   353: iload_1
    //   354: sipush #128
    //   357: if_icmplt -> 372
    //   360: iload_1
    //   361: sipush #131
    //   364: if_icmpgt -> 372
    //   367: iload_1
    //   368: istore_3
    //   369: goto -> 740
    //   372: new com/google/android/mms/InvalidHeaderValueException
    //   375: dup
    //   376: ldc_w 'Invalid Octet value!'
    //   379: invokespecial <init> : (Ljava/lang/String;)V
    //   382: athrow
    //   383: iload_1
    //   384: istore_3
    //   385: sipush #128
    //   388: iload_1
    //   389: if_icmpeq -> 740
    //   392: sipush #129
    //   395: iload_1
    //   396: if_icmpne -> 404
    //   399: iload_1
    //   400: istore_3
    //   401: goto -> 740
    //   404: new com/google/android/mms/InvalidHeaderValueException
    //   407: dup
    //   408: ldc_w 'Invalid Octet value!'
    //   411: invokespecial <init> : (Ljava/lang/String;)V
    //   414: athrow
    //   415: iload_1
    //   416: sipush #128
    //   419: if_icmplt -> 434
    //   422: iload_1
    //   423: sipush #135
    //   426: if_icmpgt -> 434
    //   429: iload_1
    //   430: istore_3
    //   431: goto -> 740
    //   434: new com/google/android/mms/InvalidHeaderValueException
    //   437: dup
    //   438: ldc_w 'Invalid Octet value!'
    //   441: invokespecial <init> : (Ljava/lang/String;)V
    //   444: athrow
    //   445: iload_1
    //   446: bipush #16
    //   448: if_icmplt -> 459
    //   451: iload_1
    //   452: istore_3
    //   453: iload_1
    //   454: bipush #19
    //   456: if_icmple -> 740
    //   459: bipush #18
    //   461: istore_3
    //   462: goto -> 740
    //   465: iload_1
    //   466: sipush #128
    //   469: if_icmplt -> 484
    //   472: iload_1
    //   473: sipush #151
    //   476: if_icmpgt -> 484
    //   479: iload_1
    //   480: istore_3
    //   481: goto -> 740
    //   484: new com/google/android/mms/InvalidHeaderValueException
    //   487: dup
    //   488: ldc_w 'Invalid Octet value!'
    //   491: invokespecial <init> : (Ljava/lang/String;)V
    //   494: athrow
    //   495: iload_1
    //   496: istore_3
    //   497: sipush #128
    //   500: iload_1
    //   501: if_icmpeq -> 740
    //   504: sipush #129
    //   507: iload_1
    //   508: if_icmpne -> 516
    //   511: iload_1
    //   512: istore_3
    //   513: goto -> 740
    //   516: new com/google/android/mms/InvalidHeaderValueException
    //   519: dup
    //   520: ldc_w 'Invalid Octet value!'
    //   523: invokespecial <init> : (Ljava/lang/String;)V
    //   526: athrow
    //   527: sipush #128
    //   530: iload_1
    //   531: if_icmpne -> 539
    //   534: iload_1
    //   535: istore_3
    //   536: goto -> 740
    //   539: new com/google/android/mms/InvalidHeaderValueException
    //   542: dup
    //   543: ldc_w 'Invalid Octet value!'
    //   546: invokespecial <init> : (Ljava/lang/String;)V
    //   549: athrow
    //   550: iload_1
    //   551: sipush #193
    //   554: if_icmple -> 571
    //   557: iload_1
    //   558: sipush #224
    //   561: if_icmpge -> 571
    //   564: sipush #192
    //   567: istore_3
    //   568: goto -> 740
    //   571: iload_1
    //   572: sipush #228
    //   575: if_icmple -> 592
    //   578: iload_1
    //   579: sipush #255
    //   582: if_icmpgt -> 592
    //   585: sipush #224
    //   588: istore_3
    //   589: goto -> 740
    //   592: iload_1
    //   593: sipush #128
    //   596: if_icmplt -> 622
    //   599: iload_1
    //   600: sipush #128
    //   603: if_icmple -> 613
    //   606: iload_1
    //   607: sipush #192
    //   610: if_icmplt -> 622
    //   613: iload_1
    //   614: istore_3
    //   615: iload_1
    //   616: sipush #255
    //   619: if_icmple -> 740
    //   622: sipush #224
    //   625: istore_3
    //   626: goto -> 740
    //   629: iload_1
    //   630: sipush #194
    //   633: if_icmple -> 650
    //   636: iload_1
    //   637: sipush #224
    //   640: if_icmpge -> 650
    //   643: sipush #192
    //   646: istore_3
    //   647: goto -> 740
    //   650: iload_1
    //   651: sipush #227
    //   654: if_icmple -> 671
    //   657: iload_1
    //   658: sipush #255
    //   661: if_icmpgt -> 671
    //   664: sipush #224
    //   667: istore_3
    //   668: goto -> 740
    //   671: iload_1
    //   672: sipush #128
    //   675: if_icmplt -> 701
    //   678: iload_1
    //   679: sipush #128
    //   682: if_icmple -> 692
    //   685: iload_1
    //   686: sipush #192
    //   689: if_icmplt -> 701
    //   692: iload_1
    //   693: istore_3
    //   694: iload_1
    //   695: sipush #255
    //   698: if_icmple -> 740
    //   701: sipush #224
    //   704: istore_3
    //   705: goto -> 740
    //   708: iload_1
    //   709: istore_3
    //   710: sipush #128
    //   713: iload_1
    //   714: if_icmpeq -> 740
    //   717: sipush #129
    //   720: iload_1
    //   721: if_icmpne -> 729
    //   724: iload_1
    //   725: istore_3
    //   726: goto -> 740
    //   729: new com/google/android/mms/InvalidHeaderValueException
    //   732: dup
    //   733: ldc_w 'Invalid Octet value!'
    //   736: invokespecial <init> : (Ljava/lang/String;)V
    //   739: athrow
    //   740: aload_0
    //   741: getfield mHeaderMap : Ljava/util/HashMap;
    //   744: iload_2
    //   745: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   748: iload_3
    //   749: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   752: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   755: pop
    //   756: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #369	-> 0
    //   #495	-> 180
    //   #432	-> 191
    //   #435	-> 210
    //   #470	-> 221
    //   #472	-> 235
    //   #473	-> 242
    //   #479	-> 286
    //   #401	-> 293
    //   #403	-> 312
    //   #420	-> 323
    //   #422	-> 342
    //   #413	-> 353
    //   #416	-> 372
    //   #387	-> 383
    //   #390	-> 404
    //   #407	-> 415
    //   #409	-> 434
    //   #483	-> 445
    //   #484	-> 459
    //   #488	-> 465
    //   #490	-> 484
    //   #394	-> 495
    //   #397	-> 516
    //   #426	-> 527
    //   #428	-> 539
    //   #455	-> 550
    //   #457	-> 564
    //   #458	-> 571
    //   #460	-> 585
    //   #461	-> 592
    //   #465	-> 622
    //   #440	-> 629
    //   #442	-> 643
    //   #443	-> 650
    //   #445	-> 664
    //   #446	-> 671
    //   #450	-> 701
    //   #381	-> 708
    //   #383	-> 729
    //   #497	-> 740
    //   #498	-> 756
  }
  
  protected byte[] getTextString(int paramInt) {
    return (byte[])this.mHeaderMap.get(Integer.valueOf(paramInt));
  }
  
  protected void setTextString(byte[] paramArrayOfbyte, int paramInt) {
    if (paramArrayOfbyte != null) {
      if (paramInt != 131 && paramInt != 132 && paramInt != 138 && paramInt != 139 && paramInt != 152 && paramInt != 158 && paramInt != 189 && paramInt != 190)
        switch (paramInt) {
          default:
            throw new RuntimeException("Invalid header field!");
          case 183:
          case 184:
          case 185:
            break;
        }  
      this.mHeaderMap.put(Integer.valueOf(paramInt), paramArrayOfbyte);
      return;
    } 
    throw null;
  }
  
  protected EncodedStringValue getEncodedStringValue(int paramInt) {
    return (EncodedStringValue)this.mHeaderMap.get(Integer.valueOf(paramInt));
  }
  
  protected EncodedStringValue[] getEncodedStringValues(int paramInt) {
    HashMap<Integer, Object> hashMap = this.mHeaderMap;
    ArrayList arrayList = (ArrayList)hashMap.get(Integer.valueOf(paramInt));
    if (arrayList == null)
      return null; 
    EncodedStringValue[] arrayOfEncodedStringValue = new EncodedStringValue[arrayList.size()];
    return (EncodedStringValue[])arrayList.toArray((Object[])arrayOfEncodedStringValue);
  }
  
  protected void setEncodedStringValue(EncodedStringValue paramEncodedStringValue, int paramInt) {
    if (paramEncodedStringValue != null) {
      if (paramInt == 137 || paramInt == 147 || paramInt == 150 || paramInt == 154 || paramInt == 160 || paramInt == 164 || paramInt == 166 || paramInt == 181 || paramInt == 182) {
        this.mHeaderMap.put(Integer.valueOf(paramInt), paramEncodedStringValue);
        return;
      } 
      throw new RuntimeException("Invalid header field!");
    } 
    throw null;
  }
  
  protected void setEncodedStringValues(EncodedStringValue[] paramArrayOfEncodedStringValue, int paramInt) {
    if (paramArrayOfEncodedStringValue != null) {
      if (paramInt == 129 || paramInt == 130 || paramInt == 151) {
        ArrayList<EncodedStringValue> arrayList = new ArrayList();
        for (byte b = 0; b < paramArrayOfEncodedStringValue.length; b++)
          arrayList.add(paramArrayOfEncodedStringValue[b]); 
        this.mHeaderMap.put(Integer.valueOf(paramInt), arrayList);
        return;
      } 
      throw new RuntimeException("Invalid header field!");
    } 
    throw null;
  }
  
  protected void appendEncodedStringValue(EncodedStringValue paramEncodedStringValue, int paramInt) {
    if (paramEncodedStringValue != null) {
      if (paramInt == 129 || paramInt == 130 || paramInt == 151) {
        HashMap<Integer, Object> hashMap = this.mHeaderMap;
        ArrayList<EncodedStringValue> arrayList2 = (ArrayList)hashMap.get(Integer.valueOf(paramInt));
        ArrayList<EncodedStringValue> arrayList1 = arrayList2;
        if (arrayList2 == null)
          arrayList1 = new ArrayList(); 
        arrayList1.add(paramEncodedStringValue);
        this.mHeaderMap.put(Integer.valueOf(paramInt), arrayList1);
        return;
      } 
      throw new RuntimeException("Invalid header field!");
    } 
    throw null;
  }
  
  protected long getLongInteger(int paramInt) {
    Long long_ = (Long)this.mHeaderMap.get(Integer.valueOf(paramInt));
    if (long_ == null)
      return -1L; 
    return long_.longValue();
  }
  
  protected void setLongInteger(long paramLong, int paramInt) {
    if (paramInt == 133 || paramInt == 142 || paramInt == 157 || paramInt == 159 || paramInt == 161 || paramInt == 173 || paramInt == 175 || paramInt == 179 || paramInt == 135 || paramInt == 136) {
      this.mHeaderMap.put(Integer.valueOf(paramInt), Long.valueOf(paramLong));
      return;
    } 
    throw new RuntimeException("Invalid header field!");
  }
}
