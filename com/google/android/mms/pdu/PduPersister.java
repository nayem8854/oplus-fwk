package com.google.android.mms.pdu;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.drm.DrmManagerClient;
import android.net.Uri;
import android.provider.Telephony;
import android.telephony.PhoneNumberUtils;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.mms.InvalidHeaderValueException;
import com.google.android.mms.MmsException;
import com.google.android.mms.util.PduCache;
import com.google.android.mms.util.SqliteWrapper;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class PduPersister {
  static final boolean $assertionsDisabled = false;
  
  private static final int[] ADDRESS_FIELDS = new int[] { 129, 130, 137, 151 };
  
  private static final HashMap<Integer, Integer> CHARSET_COLUMN_INDEX_MAP;
  
  private static final HashMap<Integer, String> CHARSET_COLUMN_NAME_MAP;
  
  private static final boolean DEBUG = false;
  
  private static final long DUMMY_THREAD_ID = 9223372036854775807L;
  
  private static final HashMap<Integer, Integer> ENCODED_STRING_COLUMN_INDEX_MAP;
  
  private static final HashMap<Integer, String> ENCODED_STRING_COLUMN_NAME_MAP;
  
  private static final boolean LOCAL_LOGV = false;
  
  private static final HashMap<Integer, Integer> LONG_COLUMN_INDEX_MAP;
  
  private static final HashMap<Integer, String> LONG_COLUMN_NAME_MAP;
  
  private static final HashMap<Uri, Integer> MESSAGE_BOX_MAP;
  
  private static final HashMap<Integer, Integer> OCTET_COLUMN_INDEX_MAP;
  
  private static final HashMap<Integer, String> OCTET_COLUMN_NAME_MAP;
  
  private static final int PART_COLUMN_CHARSET = 1;
  
  private static final int PART_COLUMN_CONTENT_DISPOSITION = 2;
  
  private static final int PART_COLUMN_CONTENT_ID = 3;
  
  private static final int PART_COLUMN_CONTENT_LOCATION = 4;
  
  private static final int PART_COLUMN_CONTENT_TYPE = 5;
  
  private static final int PART_COLUMN_FILENAME = 6;
  
  private static final int PART_COLUMN_ID = 0;
  
  private static final int PART_COLUMN_NAME = 7;
  
  private static final int PART_COLUMN_TEXT = 8;
  
  private static final String[] PART_PROJECTION;
  
  private static final PduCache PDU_CACHE_INSTANCE;
  
  private static final int PDU_COLUMN_CONTENT_CLASS = 11;
  
  private static final int PDU_COLUMN_CONTENT_LOCATION = 5;
  
  private static final int PDU_COLUMN_CONTENT_TYPE = 6;
  
  private static final int PDU_COLUMN_DATE = 21;
  
  private static final int PDU_COLUMN_DELIVERY_REPORT = 12;
  
  private static final int PDU_COLUMN_DELIVERY_TIME = 22;
  
  private static final int PDU_COLUMN_EXPIRY = 23;
  
  private static final int PDU_COLUMN_ID = 0;
  
  private static final int PDU_COLUMN_MESSAGE_BOX = 1;
  
  private static final int PDU_COLUMN_MESSAGE_CLASS = 7;
  
  private static final int PDU_COLUMN_MESSAGE_ID = 8;
  
  private static final int PDU_COLUMN_MESSAGE_SIZE = 24;
  
  private static final int PDU_COLUMN_MESSAGE_TYPE = 13;
  
  private static final int PDU_COLUMN_MMS_VERSION = 14;
  
  private static final int PDU_COLUMN_PRIORITY = 15;
  
  private static final int PDU_COLUMN_READ_REPORT = 16;
  
  private static final int PDU_COLUMN_READ_STATUS = 17;
  
  private static final int PDU_COLUMN_REPORT_ALLOWED = 18;
  
  private static final int PDU_COLUMN_RESPONSE_TEXT = 9;
  
  private static final int PDU_COLUMN_RETRIEVE_STATUS = 19;
  
  private static final int PDU_COLUMN_RETRIEVE_TEXT = 3;
  
  private static final int PDU_COLUMN_RETRIEVE_TEXT_CHARSET = 26;
  
  private static final int PDU_COLUMN_STATUS = 20;
  
  private static final int PDU_COLUMN_SUBJECT = 4;
  
  private static final int PDU_COLUMN_SUBJECT_CHARSET = 25;
  
  private static final int PDU_COLUMN_THREAD_ID = 2;
  
  private static final int PDU_COLUMN_TRANSACTION_ID = 10;
  
  private static final String[] PDU_PROJECTION = new String[] { 
      "_id", "msg_box", "thread_id", "retr_txt", "sub", "ct_l", "ct_t", "m_cls", "m_id", "resp_txt", 
      "tr_id", "ct_cls", "d_rpt", "m_type", "v", "pri", "rr", "read_status", "rpt_a", "retr_st", 
      "st", "date", "d_tm", "exp", "m_size", "sub_cs", "retr_txt_cs" };
  
  public static final int PROC_STATUS_COMPLETED = 3;
  
  public static final int PROC_STATUS_PERMANENTLY_FAILURE = 2;
  
  public static final int PROC_STATUS_TRANSIENT_FAILURE = 1;
  
  private static final String TAG = "PduPersister";
  
  public static final String TEMPORARY_DRM_OBJECT_URI = "content://mms/9223372036854775807/part";
  
  private static final HashMap<Integer, Integer> TEXT_STRING_COLUMN_INDEX_MAP;
  
  private static final HashMap<Integer, String> TEXT_STRING_COLUMN_NAME_MAP;
  
  private static PduPersister sPersister;
  
  private final ContentResolver mContentResolver;
  
  private final Context mContext;
  
  private final DrmManagerClient mDrmManagerClient;
  
  static {
    PART_PROJECTION = new String[] { "_id", "chset", "cd", "cid", "cl", "ct", "fn", "name", "text" };
    HashMap<Object, Object> hashMap = new HashMap<>();
    hashMap.put(Telephony.Mms.Inbox.CONTENT_URI, Integer.valueOf(1));
    MESSAGE_BOX_MAP.put(Telephony.Mms.Sent.CONTENT_URI, Integer.valueOf(2));
    MESSAGE_BOX_MAP.put(Telephony.Mms.Draft.CONTENT_URI, Integer.valueOf(3));
    MESSAGE_BOX_MAP.put(Telephony.Mms.Outbox.CONTENT_URI, Integer.valueOf(4));
    CHARSET_COLUMN_INDEX_MAP = (HashMap)(hashMap = new HashMap<>());
    hashMap.put(Integer.valueOf(150), Integer.valueOf(25));
    CHARSET_COLUMN_INDEX_MAP.put(Integer.valueOf(154), Integer.valueOf(26));
    CHARSET_COLUMN_NAME_MAP = (HashMap)(hashMap = new HashMap<>());
    hashMap.put(Integer.valueOf(150), "sub_cs");
    CHARSET_COLUMN_NAME_MAP.put(Integer.valueOf(154), "retr_txt_cs");
    ENCODED_STRING_COLUMN_INDEX_MAP = (HashMap)(hashMap = new HashMap<>());
    hashMap.put(Integer.valueOf(154), Integer.valueOf(3));
    ENCODED_STRING_COLUMN_INDEX_MAP.put(Integer.valueOf(150), Integer.valueOf(4));
    ENCODED_STRING_COLUMN_NAME_MAP = (HashMap)(hashMap = new HashMap<>());
    hashMap.put(Integer.valueOf(154), "retr_txt");
    ENCODED_STRING_COLUMN_NAME_MAP.put(Integer.valueOf(150), "sub");
    TEXT_STRING_COLUMN_INDEX_MAP = (HashMap)(hashMap = new HashMap<>());
    hashMap.put(Integer.valueOf(131), Integer.valueOf(5));
    TEXT_STRING_COLUMN_INDEX_MAP.put(Integer.valueOf(132), Integer.valueOf(6));
    TEXT_STRING_COLUMN_INDEX_MAP.put(Integer.valueOf(138), Integer.valueOf(7));
    TEXT_STRING_COLUMN_INDEX_MAP.put(Integer.valueOf(139), Integer.valueOf(8));
    TEXT_STRING_COLUMN_INDEX_MAP.put(Integer.valueOf(147), Integer.valueOf(9));
    TEXT_STRING_COLUMN_INDEX_MAP.put(Integer.valueOf(152), Integer.valueOf(10));
    TEXT_STRING_COLUMN_NAME_MAP = (HashMap)(hashMap = new HashMap<>());
    hashMap.put(Integer.valueOf(131), "ct_l");
    TEXT_STRING_COLUMN_NAME_MAP.put(Integer.valueOf(132), "ct_t");
    TEXT_STRING_COLUMN_NAME_MAP.put(Integer.valueOf(138), "m_cls");
    TEXT_STRING_COLUMN_NAME_MAP.put(Integer.valueOf(139), "m_id");
    TEXT_STRING_COLUMN_NAME_MAP.put(Integer.valueOf(147), "resp_txt");
    TEXT_STRING_COLUMN_NAME_MAP.put(Integer.valueOf(152), "tr_id");
    OCTET_COLUMN_INDEX_MAP = (HashMap)(hashMap = new HashMap<>());
    hashMap.put(Integer.valueOf(186), Integer.valueOf(11));
    OCTET_COLUMN_INDEX_MAP.put(Integer.valueOf(134), Integer.valueOf(12));
    OCTET_COLUMN_INDEX_MAP.put(Integer.valueOf(140), Integer.valueOf(13));
    OCTET_COLUMN_INDEX_MAP.put(Integer.valueOf(141), Integer.valueOf(14));
    OCTET_COLUMN_INDEX_MAP.put(Integer.valueOf(143), Integer.valueOf(15));
    OCTET_COLUMN_INDEX_MAP.put(Integer.valueOf(144), Integer.valueOf(16));
    OCTET_COLUMN_INDEX_MAP.put(Integer.valueOf(155), Integer.valueOf(17));
    OCTET_COLUMN_INDEX_MAP.put(Integer.valueOf(145), Integer.valueOf(18));
    OCTET_COLUMN_INDEX_MAP.put(Integer.valueOf(153), Integer.valueOf(19));
    OCTET_COLUMN_INDEX_MAP.put(Integer.valueOf(149), Integer.valueOf(20));
    OCTET_COLUMN_NAME_MAP = (HashMap)(hashMap = new HashMap<>());
    hashMap.put(Integer.valueOf(186), "ct_cls");
    OCTET_COLUMN_NAME_MAP.put(Integer.valueOf(134), "d_rpt");
    OCTET_COLUMN_NAME_MAP.put(Integer.valueOf(140), "m_type");
    OCTET_COLUMN_NAME_MAP.put(Integer.valueOf(141), "v");
    OCTET_COLUMN_NAME_MAP.put(Integer.valueOf(143), "pri");
    OCTET_COLUMN_NAME_MAP.put(Integer.valueOf(144), "rr");
    OCTET_COLUMN_NAME_MAP.put(Integer.valueOf(155), "read_status");
    OCTET_COLUMN_NAME_MAP.put(Integer.valueOf(145), "rpt_a");
    OCTET_COLUMN_NAME_MAP.put(Integer.valueOf(153), "retr_st");
    OCTET_COLUMN_NAME_MAP.put(Integer.valueOf(149), "st");
    LONG_COLUMN_INDEX_MAP = (HashMap)(hashMap = new HashMap<>());
    hashMap.put(Integer.valueOf(133), Integer.valueOf(21));
    LONG_COLUMN_INDEX_MAP.put(Integer.valueOf(135), Integer.valueOf(22));
    LONG_COLUMN_INDEX_MAP.put(Integer.valueOf(136), Integer.valueOf(23));
    LONG_COLUMN_INDEX_MAP.put(Integer.valueOf(142), Integer.valueOf(24));
    LONG_COLUMN_NAME_MAP = (HashMap)(hashMap = new HashMap<>());
    hashMap.put(Integer.valueOf(133), "date");
    LONG_COLUMN_NAME_MAP.put(Integer.valueOf(135), "d_tm");
    LONG_COLUMN_NAME_MAP.put(Integer.valueOf(136), "exp");
    LONG_COLUMN_NAME_MAP.put(Integer.valueOf(142), "m_size");
    PDU_CACHE_INSTANCE = PduCache.getInstance();
  }
  
  private PduPersister(Context paramContext) {
    this.mContext = paramContext;
    this.mContentResolver = paramContext.getContentResolver();
    this.mDrmManagerClient = new DrmManagerClient(paramContext);
  }
  
  public static PduPersister getPduPersister(Context paramContext) {
    PduPersister pduPersister = sPersister;
    if (pduPersister == null) {
      sPersister = new PduPersister(paramContext);
    } else if (!paramContext.equals(pduPersister.mContext)) {
      sPersister.release();
      sPersister = new PduPersister(paramContext);
    } 
    return sPersister;
  }
  
  private void setEncodedStringValueToHeaders(Cursor paramCursor, int paramInt1, PduHeaders paramPduHeaders, int paramInt2) {
    String str = paramCursor.getString(paramInt1);
    if (str != null && str.length() > 0) {
      paramInt1 = ((Integer)CHARSET_COLUMN_INDEX_MAP.get(Integer.valueOf(paramInt2))).intValue();
      paramInt1 = paramCursor.getInt(paramInt1);
      EncodedStringValue encodedStringValue = new EncodedStringValue(paramInt1, getBytes(str));
      paramPduHeaders.setEncodedStringValue(encodedStringValue, paramInt2);
    } 
  }
  
  private void setTextStringToHeaders(Cursor paramCursor, int paramInt1, PduHeaders paramPduHeaders, int paramInt2) {
    String str = paramCursor.getString(paramInt1);
    if (str != null)
      paramPduHeaders.setTextString(getBytes(str), paramInt2); 
  }
  
  private void setOctetToHeaders(Cursor paramCursor, int paramInt1, PduHeaders paramPduHeaders, int paramInt2) throws InvalidHeaderValueException {
    if (!paramCursor.isNull(paramInt1)) {
      paramInt1 = paramCursor.getInt(paramInt1);
      paramPduHeaders.setOctet(paramInt1, paramInt2);
    } 
  }
  
  private void setLongToHeaders(Cursor paramCursor, int paramInt1, PduHeaders paramPduHeaders, int paramInt2) {
    if (!paramCursor.isNull(paramInt1)) {
      long l = paramCursor.getLong(paramInt1);
      paramPduHeaders.setLongInteger(l, paramInt2);
    } 
  }
  
  private Integer getIntegerFromPartColumn(Cursor paramCursor, int paramInt) {
    if (!paramCursor.isNull(paramInt))
      return Integer.valueOf(paramCursor.getInt(paramInt)); 
    return null;
  }
  
  private byte[] getByteArrayFromPartColumn(Cursor paramCursor, int paramInt) {
    if (!paramCursor.isNull(paramInt))
      return getBytes(paramCursor.getString(paramInt)); 
    return null;
  }
  
  private PduPart[] loadParts(long paramLong) throws MmsException {
    // Byte code:
    //   0: aload_0
    //   1: getfield mContext : Landroid/content/Context;
    //   4: astore_3
    //   5: aload_0
    //   6: getfield mContentResolver : Landroid/content/ContentResolver;
    //   9: astore #4
    //   11: new java/lang/StringBuilder
    //   14: dup
    //   15: invokespecial <init> : ()V
    //   18: astore #5
    //   20: aload #5
    //   22: ldc_w 'content://mms/'
    //   25: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   28: pop
    //   29: aload #5
    //   31: lload_1
    //   32: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   35: pop
    //   36: aload #5
    //   38: ldc_w '/part'
    //   41: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   44: pop
    //   45: aload #5
    //   47: invokevirtual toString : ()Ljava/lang/String;
    //   50: astore #5
    //   52: aload #5
    //   54: invokestatic parse : (Ljava/lang/String;)Landroid/net/Uri;
    //   57: astore #5
    //   59: getstatic com/google/android/mms/pdu/PduPersister.PART_PROJECTION : [Ljava/lang/String;
    //   62: astore #6
    //   64: aload_3
    //   65: aload #4
    //   67: aload #5
    //   69: aload #6
    //   71: aconst_null
    //   72: aconst_null
    //   73: aconst_null
    //   74: invokestatic query : (Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   77: astore #7
    //   79: aload #7
    //   81: ifnull -> 743
    //   84: aload #7
    //   86: invokeinterface getCount : ()I
    //   91: ifne -> 97
    //   94: goto -> 743
    //   97: aload #7
    //   99: invokeinterface getCount : ()I
    //   104: istore #8
    //   106: iload #8
    //   108: anewarray com/google/android/mms/pdu/PduPart
    //   111: astore #9
    //   113: iconst_0
    //   114: istore #10
    //   116: aload #7
    //   118: invokeinterface moveToNext : ()Z
    //   123: ifeq -> 711
    //   126: new com/google/android/mms/pdu/PduPart
    //   129: astore #11
    //   131: aload #11
    //   133: invokespecial <init> : ()V
    //   136: aload_0
    //   137: aload #7
    //   139: iconst_1
    //   140: invokespecial getIntegerFromPartColumn : (Landroid/database/Cursor;I)Ljava/lang/Integer;
    //   143: astore #4
    //   145: aload #4
    //   147: ifnull -> 160
    //   150: aload #11
    //   152: aload #4
    //   154: invokevirtual intValue : ()I
    //   157: invokevirtual setCharset : (I)V
    //   160: aload_0
    //   161: aload #7
    //   163: iconst_2
    //   164: invokespecial getByteArrayFromPartColumn : (Landroid/database/Cursor;I)[B
    //   167: astore #4
    //   169: aload #4
    //   171: ifnull -> 181
    //   174: aload #11
    //   176: aload #4
    //   178: invokevirtual setContentDisposition : ([B)V
    //   181: aload_0
    //   182: aload #7
    //   184: iconst_3
    //   185: invokespecial getByteArrayFromPartColumn : (Landroid/database/Cursor;I)[B
    //   188: astore #4
    //   190: aload #4
    //   192: ifnull -> 202
    //   195: aload #11
    //   197: aload #4
    //   199: invokevirtual setContentId : ([B)V
    //   202: aload_0
    //   203: aload #7
    //   205: iconst_4
    //   206: invokespecial getByteArrayFromPartColumn : (Landroid/database/Cursor;I)[B
    //   209: astore #4
    //   211: aload #4
    //   213: ifnull -> 223
    //   216: aload #11
    //   218: aload #4
    //   220: invokevirtual setContentLocation : ([B)V
    //   223: aload_0
    //   224: aload #7
    //   226: iconst_5
    //   227: invokespecial getByteArrayFromPartColumn : (Landroid/database/Cursor;I)[B
    //   230: astore #4
    //   232: aload #4
    //   234: ifnull -> 695
    //   237: aload #11
    //   239: aload #4
    //   241: invokevirtual setContentType : ([B)V
    //   244: aload_0
    //   245: aload #7
    //   247: bipush #6
    //   249: invokespecial getByteArrayFromPartColumn : (Landroid/database/Cursor;I)[B
    //   252: astore_3
    //   253: aload_3
    //   254: ifnull -> 263
    //   257: aload #11
    //   259: aload_3
    //   260: invokevirtual setFilename : ([B)V
    //   263: aload_0
    //   264: aload #7
    //   266: bipush #7
    //   268: invokespecial getByteArrayFromPartColumn : (Landroid/database/Cursor;I)[B
    //   271: astore_3
    //   272: aload_3
    //   273: ifnull -> 282
    //   276: aload #11
    //   278: aload_3
    //   279: invokevirtual setName : ([B)V
    //   282: aload #7
    //   284: iconst_0
    //   285: invokeinterface getLong : (I)J
    //   290: lstore_1
    //   291: new java/lang/StringBuilder
    //   294: astore_3
    //   295: aload_3
    //   296: invokespecial <init> : ()V
    //   299: aload_3
    //   300: ldc_w 'content://mms/part/'
    //   303: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   306: pop
    //   307: aload_3
    //   308: lload_1
    //   309: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   312: pop
    //   313: aload_3
    //   314: invokevirtual toString : ()Ljava/lang/String;
    //   317: invokestatic parse : (Ljava/lang/String;)Landroid/net/Uri;
    //   320: astore #5
    //   322: aload #11
    //   324: aload #5
    //   326: invokevirtual setDataUri : (Landroid/net/Uri;)V
    //   329: aload #4
    //   331: invokestatic toIsoString : ([B)Ljava/lang/String;
    //   334: astore #6
    //   336: aload #6
    //   338: invokestatic isImageType : (Ljava/lang/String;)Z
    //   341: ifne -> 682
    //   344: aload #6
    //   346: invokestatic isAudioType : (Ljava/lang/String;)Z
    //   349: ifne -> 679
    //   352: aload #6
    //   354: invokestatic isVideoType : (Ljava/lang/String;)Z
    //   357: ifne -> 676
    //   360: new java/io/ByteArrayOutputStream
    //   363: astore #4
    //   365: aload #4
    //   367: invokespecial <init> : ()V
    //   370: aload #4
    //   372: astore_3
    //   373: aconst_null
    //   374: astore #12
    //   376: aconst_null
    //   377: astore #13
    //   379: ldc_w 'text/plain'
    //   382: aload #6
    //   384: invokevirtual equals : (Ljava/lang/Object;)Z
    //   387: ifne -> 616
    //   390: ldc_w 'application/smil'
    //   393: aload #6
    //   395: invokevirtual equals : (Ljava/lang/Object;)Z
    //   398: ifne -> 616
    //   401: ldc_w 'text/html'
    //   404: aload #6
    //   406: invokevirtual equals : (Ljava/lang/Object;)Z
    //   409: istore #14
    //   411: iload #14
    //   413: ifeq -> 419
    //   416: goto -> 616
    //   419: aload_0
    //   420: getfield mContentResolver : Landroid/content/ContentResolver;
    //   423: aload #5
    //   425: invokevirtual openInputStream : (Landroid/net/Uri;)Ljava/io/InputStream;
    //   428: astore #4
    //   430: sipush #256
    //   433: newarray byte
    //   435: astore #12
    //   437: aload #4
    //   439: aload #12
    //   441: invokevirtual read : ([B)I
    //   444: istore #15
    //   446: iload #15
    //   448: iflt -> 488
    //   451: aload_3
    //   452: aload #12
    //   454: iconst_0
    //   455: iload #15
    //   457: invokevirtual write : ([BII)V
    //   460: aload #4
    //   462: aload #12
    //   464: invokevirtual read : ([B)I
    //   467: istore #15
    //   469: goto -> 446
    //   472: astore #5
    //   474: aload #4
    //   476: astore_3
    //   477: aload #5
    //   479: astore #4
    //   481: goto -> 591
    //   484: astore_3
    //   485: goto -> 551
    //   488: aload #4
    //   490: ifnull -> 514
    //   493: aload #4
    //   495: invokevirtual close : ()V
    //   498: goto -> 514
    //   501: astore #4
    //   503: ldc 'PduPersister'
    //   505: ldc_w 'Failed to close stream'
    //   508: aload #4
    //   510: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   513: pop
    //   514: aload_3
    //   515: astore #4
    //   517: goto -> 663
    //   520: astore_3
    //   521: aload #4
    //   523: astore #5
    //   525: aload_3
    //   526: astore #4
    //   528: aload #5
    //   530: astore_3
    //   531: goto -> 591
    //   534: astore_3
    //   535: goto -> 551
    //   538: astore #4
    //   540: aload #12
    //   542: astore_3
    //   543: goto -> 591
    //   546: astore_3
    //   547: aload #13
    //   549: astore #4
    //   551: ldc 'PduPersister'
    //   553: ldc_w 'Failed to load part data'
    //   556: aload_3
    //   557: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   560: pop
    //   561: aload #7
    //   563: invokeinterface close : ()V
    //   568: new com/google/android/mms/MmsException
    //   571: astore #5
    //   573: aload #5
    //   575: aload_3
    //   576: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   579: aload #5
    //   581: athrow
    //   582: astore #5
    //   584: aload #4
    //   586: astore_3
    //   587: aload #5
    //   589: astore #4
    //   591: aload_3
    //   592: ifnull -> 613
    //   595: aload_3
    //   596: invokevirtual close : ()V
    //   599: goto -> 613
    //   602: astore_3
    //   603: ldc 'PduPersister'
    //   605: ldc_w 'Failed to close stream'
    //   608: aload_3
    //   609: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   612: pop
    //   613: aload #4
    //   615: athrow
    //   616: aload #7
    //   618: bipush #8
    //   620: invokeinterface getString : (I)Ljava/lang/String;
    //   625: astore_3
    //   626: new com/google/android/mms/pdu/EncodedStringValue
    //   629: astore #5
    //   631: aload_3
    //   632: ifnull -> 638
    //   635: goto -> 642
    //   638: ldc_w ''
    //   641: astore_3
    //   642: aload #5
    //   644: aload_3
    //   645: invokespecial <init> : (Ljava/lang/String;)V
    //   648: aload #5
    //   650: invokevirtual getTextString : ()[B
    //   653: astore_3
    //   654: aload #4
    //   656: aload_3
    //   657: iconst_0
    //   658: aload_3
    //   659: arraylength
    //   660: invokevirtual write : ([BII)V
    //   663: aload #11
    //   665: aload #4
    //   667: invokevirtual toByteArray : ()[B
    //   670: invokevirtual setData : ([B)V
    //   673: goto -> 682
    //   676: goto -> 682
    //   679: goto -> 682
    //   682: aload #9
    //   684: iload #10
    //   686: aload #11
    //   688: aastore
    //   689: iinc #10, 1
    //   692: goto -> 116
    //   695: new com/google/android/mms/MmsException
    //   698: astore #4
    //   700: aload #4
    //   702: ldc_w 'Content-Type must be set.'
    //   705: invokespecial <init> : (Ljava/lang/String;)V
    //   708: aload #4
    //   710: athrow
    //   711: aload #7
    //   713: ifnull -> 723
    //   716: aload #7
    //   718: invokeinterface close : ()V
    //   723: aload #9
    //   725: areturn
    //   726: astore #4
    //   728: aload #7
    //   730: ifnull -> 740
    //   733: aload #7
    //   735: invokeinterface close : ()V
    //   740: aload #4
    //   742: athrow
    //   743: aload #7
    //   745: ifnull -> 755
    //   748: aload #7
    //   750: invokeinterface close : ()V
    //   755: aconst_null
    //   756: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #371	-> 0
    //   #372	-> 52
    //   #371	-> 64
    //   #375	-> 79
    //   #378	-> 79
    //   #385	-> 97
    //   #386	-> 106
    //   #387	-> 106
    //   #388	-> 116
    //   #389	-> 126
    //   #390	-> 136
    //   #392	-> 145
    //   #393	-> 150
    //   #396	-> 160
    //   #398	-> 169
    //   #399	-> 174
    //   #402	-> 181
    //   #404	-> 190
    //   #405	-> 195
    //   #408	-> 202
    //   #410	-> 211
    //   #411	-> 216
    //   #414	-> 223
    //   #416	-> 232
    //   #417	-> 237
    //   #422	-> 244
    //   #424	-> 253
    //   #425	-> 257
    //   #428	-> 263
    //   #430	-> 272
    //   #431	-> 276
    //   #435	-> 282
    //   #436	-> 291
    //   #437	-> 322
    //   #441	-> 329
    //   #442	-> 336
    //   #443	-> 344
    //   #444	-> 352
    //   #445	-> 360
    //   #446	-> 373
    //   #451	-> 379
    //   #452	-> 401
    //   #460	-> 419
    //   #462	-> 430
    //   #463	-> 437
    //   #464	-> 446
    //   #465	-> 451
    //   #466	-> 460
    //   #473	-> 472
    //   #468	-> 484
    //   #464	-> 488
    //   #473	-> 488
    //   #475	-> 493
    //   #478	-> 498
    //   #476	-> 501
    //   #477	-> 503
    //   #478	-> 514
    //   #482	-> 514
    //   #473	-> 520
    //   #468	-> 534
    //   #473	-> 538
    //   #468	-> 546
    //   #469	-> 551
    //   #470	-> 561
    //   #471	-> 568
    //   #473	-> 582
    //   #475	-> 595
    //   #478	-> 599
    //   #476	-> 602
    //   #477	-> 603
    //   #480	-> 613
    //   #451	-> 616
    //   #453	-> 616
    //   #454	-> 626
    //   #455	-> 648
    //   #456	-> 654
    //   #457	-> 663
    //   #482	-> 663
    //   #444	-> 676
    //   #443	-> 679
    //   #442	-> 682
    //   #484	-> 682
    //   #485	-> 689
    //   #419	-> 695
    //   #388	-> 711
    //   #487	-> 711
    //   #488	-> 716
    //   #492	-> 723
    //   #487	-> 726
    //   #488	-> 733
    //   #490	-> 740
    //   #382	-> 743
    //   #487	-> 743
    //   #488	-> 748
    //   #382	-> 755
    // Exception table:
    //   from	to	target	type
    //   84	94	726	finally
    //   97	106	726	finally
    //   106	113	726	finally
    //   116	126	726	finally
    //   126	136	726	finally
    //   136	145	726	finally
    //   150	160	726	finally
    //   160	169	726	finally
    //   174	181	726	finally
    //   181	190	726	finally
    //   195	202	726	finally
    //   202	211	726	finally
    //   216	223	726	finally
    //   223	232	726	finally
    //   237	244	726	finally
    //   244	253	726	finally
    //   257	263	726	finally
    //   263	272	726	finally
    //   276	282	726	finally
    //   282	291	726	finally
    //   291	322	726	finally
    //   322	329	726	finally
    //   329	336	726	finally
    //   336	344	726	finally
    //   344	352	726	finally
    //   352	360	726	finally
    //   360	370	726	finally
    //   379	401	726	finally
    //   401	411	726	finally
    //   419	430	546	java/io/IOException
    //   419	430	538	finally
    //   430	437	534	java/io/IOException
    //   430	437	520	finally
    //   437	446	534	java/io/IOException
    //   437	446	520	finally
    //   451	460	484	java/io/IOException
    //   451	460	472	finally
    //   460	469	484	java/io/IOException
    //   460	469	472	finally
    //   493	498	501	java/io/IOException
    //   493	498	726	finally
    //   503	514	726	finally
    //   551	561	582	finally
    //   561	568	582	finally
    //   568	582	582	finally
    //   595	599	602	java/io/IOException
    //   595	599	726	finally
    //   603	613	726	finally
    //   613	616	726	finally
    //   616	626	726	finally
    //   626	631	726	finally
    //   642	648	726	finally
    //   648	654	726	finally
    //   654	663	726	finally
    //   663	673	726	finally
    //   695	711	726	finally
  }
  
  private void loadAddress(long paramLong, PduHeaders paramPduHeaders) {
    Context context = this.mContext;
    ContentResolver contentResolver = this.mContentResolver;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("content://mms/");
    stringBuilder.append(paramLong);
    stringBuilder.append("/addr");
    String str = stringBuilder.toString();
    Uri uri = Uri.parse(str);
    Cursor cursor = SqliteWrapper.query(context, contentResolver, uri, new String[] { "address", "charset", "type" }, null, null, null);
    if (cursor != null)
      try {
        while (cursor.moveToNext()) {
          String str1 = cursor.getString(0);
          if (!TextUtils.isEmpty(str1)) {
            StringBuilder stringBuilder1;
            int i = cursor.getInt(2);
            if (i != 129 && i != 130)
              if (i != 137) {
                if (i != 151) {
                  stringBuilder1 = new StringBuilder();
                  this();
                  stringBuilder1.append("Unknown address type: ");
                  stringBuilder1.append(i);
                  Log.e("PduPersister", stringBuilder1.toString());
                  continue;
                } 
              } else {
                EncodedStringValue encodedStringValue1 = new EncodedStringValue();
                this(cursor.getInt(1), getBytes((String)stringBuilder1));
                paramPduHeaders.setEncodedStringValue(encodedStringValue1, i);
                continue;
              }  
            EncodedStringValue encodedStringValue = new EncodedStringValue();
            this(cursor.getInt(1), getBytes((String)stringBuilder1));
            paramPduHeaders.appendEncodedStringValue(encodedStringValue, i);
          } 
        } 
      } finally {
        cursor.close();
      }  
  }
  
  public GenericPdu load(Uri paramUri) throws MmsException {
    // Byte code:
    //   0: iconst_0
    //   1: istore_2
    //   2: iconst_0
    //   3: istore_3
    //   4: iconst_0
    //   5: istore #4
    //   7: ldc2_w -1
    //   10: lstore #5
    //   12: getstatic com/google/android/mms/pdu/PduPersister.PDU_CACHE_INSTANCE : Lcom/google/android/mms/util/PduCache;
    //   15: astore #7
    //   17: aload #7
    //   19: monitorenter
    //   20: getstatic com/google/android/mms/pdu/PduPersister.PDU_CACHE_INSTANCE : Lcom/google/android/mms/util/PduCache;
    //   23: aload_1
    //   24: invokevirtual isUpdating : (Landroid/net/Uri;)Z
    //   27: istore #8
    //   29: iload #8
    //   31: ifeq -> 149
    //   34: getstatic com/google/android/mms/pdu/PduPersister.PDU_CACHE_INSTANCE : Lcom/google/android/mms/util/PduCache;
    //   37: invokevirtual wait : ()V
    //   40: goto -> 56
    //   43: astore #9
    //   45: ldc 'PduPersister'
    //   47: ldc_w 'load: '
    //   50: aload #9
    //   52: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   55: pop
    //   56: getstatic com/google/android/mms/pdu/PduPersister.PDU_CACHE_INSTANCE : Lcom/google/android/mms/util/PduCache;
    //   59: aload_1
    //   60: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   63: checkcast com/google/android/mms/util/PduCacheEntry
    //   66: astore #9
    //   68: aload #9
    //   70: ifnull -> 146
    //   73: aload #9
    //   75: invokevirtual getPdu : ()Lcom/google/android/mms/pdu/GenericPdu;
    //   78: astore #9
    //   80: aload #7
    //   82: monitorexit
    //   83: getstatic com/google/android/mms/pdu/PduPersister.PDU_CACHE_INSTANCE : Lcom/google/android/mms/util/PduCache;
    //   86: astore #7
    //   88: aload #7
    //   90: monitorenter
    //   91: iconst_0
    //   92: ifeq -> 120
    //   95: new com/google/android/mms/util/PduCacheEntry
    //   98: astore #10
    //   100: aload #10
    //   102: aconst_null
    //   103: iconst_0
    //   104: ldc2_w -1
    //   107: invokespecial <init> : (Lcom/google/android/mms/pdu/GenericPdu;IJ)V
    //   110: getstatic com/google/android/mms/pdu/PduPersister.PDU_CACHE_INSTANCE : Lcom/google/android/mms/util/PduCache;
    //   113: aload_1
    //   114: aload #10
    //   116: invokevirtual put : (Landroid/net/Uri;Lcom/google/android/mms/util/PduCacheEntry;)Z
    //   119: pop
    //   120: getstatic com/google/android/mms/pdu/PduPersister.PDU_CACHE_INSTANCE : Lcom/google/android/mms/util/PduCache;
    //   123: aload_1
    //   124: iconst_0
    //   125: invokevirtual setUpdating : (Landroid/net/Uri;Z)V
    //   128: getstatic com/google/android/mms/pdu/PduPersister.PDU_CACHE_INSTANCE : Lcom/google/android/mms/util/PduCache;
    //   131: invokevirtual notifyAll : ()V
    //   134: aload #7
    //   136: monitorexit
    //   137: aload #9
    //   139: areturn
    //   140: astore_1
    //   141: aload #7
    //   143: monitorexit
    //   144: aload_1
    //   145: athrow
    //   146: goto -> 152
    //   149: aconst_null
    //   150: astore #9
    //   152: getstatic com/google/android/mms/pdu/PduPersister.PDU_CACHE_INSTANCE : Lcom/google/android/mms/util/PduCache;
    //   155: aload_1
    //   156: iconst_1
    //   157: invokevirtual setUpdating : (Landroid/net/Uri;Z)V
    //   160: aload #7
    //   162: monitorexit
    //   163: iload_2
    //   164: istore_3
    //   165: lload #5
    //   167: lstore #11
    //   169: aload_0
    //   170: getfield mContext : Landroid/content/Context;
    //   173: aload_0
    //   174: getfield mContentResolver : Landroid/content/ContentResolver;
    //   177: aload_1
    //   178: getstatic com/google/android/mms/pdu/PduPersister.PDU_PROJECTION : [Ljava/lang/String;
    //   181: aconst_null
    //   182: aconst_null
    //   183: aconst_null
    //   184: invokestatic query : (Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   187: astore #9
    //   189: iload_2
    //   190: istore_3
    //   191: lload #5
    //   193: lstore #11
    //   195: new com/google/android/mms/pdu/PduHeaders
    //   198: astore #7
    //   200: iload_2
    //   201: istore_3
    //   202: lload #5
    //   204: lstore #11
    //   206: aload #7
    //   208: invokespecial <init> : ()V
    //   211: iload_2
    //   212: istore_3
    //   213: lload #5
    //   215: lstore #11
    //   217: aload_1
    //   218: invokestatic parseId : (Landroid/net/Uri;)J
    //   221: lstore #13
    //   223: aload #9
    //   225: ifnull -> 1567
    //   228: iload #4
    //   230: istore_2
    //   231: lload #5
    //   233: lstore #15
    //   235: aload #9
    //   237: invokeinterface getCount : ()I
    //   242: iconst_1
    //   243: if_icmpne -> 1567
    //   246: iload #4
    //   248: istore_2
    //   249: lload #5
    //   251: lstore #15
    //   253: aload #9
    //   255: invokeinterface moveToFirst : ()Z
    //   260: ifeq -> 1567
    //   263: iload #4
    //   265: istore_2
    //   266: lload #5
    //   268: lstore #15
    //   270: aload #9
    //   272: iconst_1
    //   273: invokeinterface getInt : (I)I
    //   278: istore #4
    //   280: iload #4
    //   282: istore_2
    //   283: lload #5
    //   285: lstore #15
    //   287: aload #9
    //   289: iconst_2
    //   290: invokeinterface getLong : (I)J
    //   295: lstore #5
    //   297: iload #4
    //   299: istore_2
    //   300: lload #5
    //   302: lstore #15
    //   304: getstatic com/google/android/mms/pdu/PduPersister.ENCODED_STRING_COLUMN_INDEX_MAP : Ljava/util/HashMap;
    //   307: invokevirtual entrySet : ()Ljava/util/Set;
    //   310: astore #10
    //   312: iload #4
    //   314: istore_2
    //   315: lload #5
    //   317: lstore #15
    //   319: aload #10
    //   321: invokeinterface iterator : ()Ljava/util/Iterator;
    //   326: astore #10
    //   328: iload #4
    //   330: istore_2
    //   331: lload #5
    //   333: lstore #15
    //   335: aload #10
    //   337: invokeinterface hasNext : ()Z
    //   342: ifeq -> 428
    //   345: iload #4
    //   347: istore_2
    //   348: lload #5
    //   350: lstore #15
    //   352: aload #10
    //   354: invokeinterface next : ()Ljava/lang/Object;
    //   359: checkcast java/util/Map$Entry
    //   362: astore #17
    //   364: iload #4
    //   366: istore_2
    //   367: lload #5
    //   369: lstore #15
    //   371: aload #17
    //   373: invokeinterface getValue : ()Ljava/lang/Object;
    //   378: checkcast java/lang/Integer
    //   381: invokevirtual intValue : ()I
    //   384: istore_3
    //   385: iload #4
    //   387: istore_2
    //   388: lload #5
    //   390: lstore #15
    //   392: aload #17
    //   394: invokeinterface getKey : ()Ljava/lang/Object;
    //   399: checkcast java/lang/Integer
    //   402: invokevirtual intValue : ()I
    //   405: istore #18
    //   407: iload #4
    //   409: istore_2
    //   410: lload #5
    //   412: lstore #15
    //   414: aload_0
    //   415: aload #9
    //   417: iload_3
    //   418: aload #7
    //   420: iload #18
    //   422: invokespecial setEncodedStringValueToHeaders : (Landroid/database/Cursor;ILcom/google/android/mms/pdu/PduHeaders;I)V
    //   425: goto -> 328
    //   428: iload #4
    //   430: istore_2
    //   431: lload #5
    //   433: lstore #15
    //   435: getstatic com/google/android/mms/pdu/PduPersister.TEXT_STRING_COLUMN_INDEX_MAP : Ljava/util/HashMap;
    //   438: invokevirtual entrySet : ()Ljava/util/Set;
    //   441: astore #10
    //   443: iload #4
    //   445: istore_2
    //   446: lload #5
    //   448: lstore #15
    //   450: aload #10
    //   452: invokeinterface iterator : ()Ljava/util/Iterator;
    //   457: astore #10
    //   459: iload #4
    //   461: istore_2
    //   462: lload #5
    //   464: lstore #15
    //   466: aload #10
    //   468: invokeinterface hasNext : ()Z
    //   473: ifeq -> 559
    //   476: iload #4
    //   478: istore_2
    //   479: lload #5
    //   481: lstore #15
    //   483: aload #10
    //   485: invokeinterface next : ()Ljava/lang/Object;
    //   490: checkcast java/util/Map$Entry
    //   493: astore #17
    //   495: iload #4
    //   497: istore_2
    //   498: lload #5
    //   500: lstore #15
    //   502: aload #17
    //   504: invokeinterface getValue : ()Ljava/lang/Object;
    //   509: checkcast java/lang/Integer
    //   512: invokevirtual intValue : ()I
    //   515: istore #18
    //   517: iload #4
    //   519: istore_2
    //   520: lload #5
    //   522: lstore #15
    //   524: aload #17
    //   526: invokeinterface getKey : ()Ljava/lang/Object;
    //   531: checkcast java/lang/Integer
    //   534: invokevirtual intValue : ()I
    //   537: istore_3
    //   538: iload #4
    //   540: istore_2
    //   541: lload #5
    //   543: lstore #15
    //   545: aload_0
    //   546: aload #9
    //   548: iload #18
    //   550: aload #7
    //   552: iload_3
    //   553: invokespecial setTextStringToHeaders : (Landroid/database/Cursor;ILcom/google/android/mms/pdu/PduHeaders;I)V
    //   556: goto -> 459
    //   559: iload #4
    //   561: istore_2
    //   562: lload #5
    //   564: lstore #15
    //   566: getstatic com/google/android/mms/pdu/PduPersister.OCTET_COLUMN_INDEX_MAP : Ljava/util/HashMap;
    //   569: invokevirtual entrySet : ()Ljava/util/Set;
    //   572: astore #10
    //   574: iload #4
    //   576: istore_2
    //   577: lload #5
    //   579: lstore #15
    //   581: aload #10
    //   583: invokeinterface iterator : ()Ljava/util/Iterator;
    //   588: astore #10
    //   590: iload #4
    //   592: istore_2
    //   593: lload #5
    //   595: lstore #15
    //   597: aload #10
    //   599: invokeinterface hasNext : ()Z
    //   604: ifeq -> 690
    //   607: iload #4
    //   609: istore_2
    //   610: lload #5
    //   612: lstore #15
    //   614: aload #10
    //   616: invokeinterface next : ()Ljava/lang/Object;
    //   621: checkcast java/util/Map$Entry
    //   624: astore #17
    //   626: iload #4
    //   628: istore_2
    //   629: lload #5
    //   631: lstore #15
    //   633: aload #17
    //   635: invokeinterface getValue : ()Ljava/lang/Object;
    //   640: checkcast java/lang/Integer
    //   643: invokevirtual intValue : ()I
    //   646: istore_3
    //   647: iload #4
    //   649: istore_2
    //   650: lload #5
    //   652: lstore #15
    //   654: aload #17
    //   656: invokeinterface getKey : ()Ljava/lang/Object;
    //   661: checkcast java/lang/Integer
    //   664: invokevirtual intValue : ()I
    //   667: istore #18
    //   669: iload #4
    //   671: istore_2
    //   672: lload #5
    //   674: lstore #15
    //   676: aload_0
    //   677: aload #9
    //   679: iload_3
    //   680: aload #7
    //   682: iload #18
    //   684: invokespecial setOctetToHeaders : (Landroid/database/Cursor;ILcom/google/android/mms/pdu/PduHeaders;I)V
    //   687: goto -> 590
    //   690: iload #4
    //   692: istore_2
    //   693: lload #5
    //   695: lstore #15
    //   697: getstatic com/google/android/mms/pdu/PduPersister.LONG_COLUMN_INDEX_MAP : Ljava/util/HashMap;
    //   700: invokevirtual entrySet : ()Ljava/util/Set;
    //   703: astore #10
    //   705: iload #4
    //   707: istore_2
    //   708: lload #5
    //   710: lstore #15
    //   712: aload #10
    //   714: invokeinterface iterator : ()Ljava/util/Iterator;
    //   719: astore #10
    //   721: iload #4
    //   723: istore_2
    //   724: lload #5
    //   726: lstore #15
    //   728: aload #10
    //   730: invokeinterface hasNext : ()Z
    //   735: ifeq -> 821
    //   738: iload #4
    //   740: istore_2
    //   741: lload #5
    //   743: lstore #15
    //   745: aload #10
    //   747: invokeinterface next : ()Ljava/lang/Object;
    //   752: checkcast java/util/Map$Entry
    //   755: astore #17
    //   757: iload #4
    //   759: istore_2
    //   760: lload #5
    //   762: lstore #15
    //   764: aload #17
    //   766: invokeinterface getValue : ()Ljava/lang/Object;
    //   771: checkcast java/lang/Integer
    //   774: invokevirtual intValue : ()I
    //   777: istore_3
    //   778: iload #4
    //   780: istore_2
    //   781: lload #5
    //   783: lstore #15
    //   785: aload #17
    //   787: invokeinterface getKey : ()Ljava/lang/Object;
    //   792: checkcast java/lang/Integer
    //   795: invokevirtual intValue : ()I
    //   798: istore #18
    //   800: iload #4
    //   802: istore_2
    //   803: lload #5
    //   805: lstore #15
    //   807: aload_0
    //   808: aload #9
    //   810: iload_3
    //   811: aload #7
    //   813: iload #18
    //   815: invokespecial setLongToHeaders : (Landroid/database/Cursor;ILcom/google/android/mms/pdu/PduHeaders;I)V
    //   818: goto -> 721
    //   821: aload #9
    //   823: ifnull -> 840
    //   826: iload #4
    //   828: istore_3
    //   829: lload #5
    //   831: lstore #11
    //   833: aload #9
    //   835: invokeinterface close : ()V
    //   840: lload #13
    //   842: ldc2_w -1
    //   845: lcmp
    //   846: ifeq -> 1530
    //   849: iload #4
    //   851: istore_3
    //   852: lload #5
    //   854: lstore #11
    //   856: aload_0
    //   857: lload #13
    //   859: aload #7
    //   861: invokespecial loadAddress : (JLcom/google/android/mms/pdu/PduHeaders;)V
    //   864: iload #4
    //   866: istore_3
    //   867: lload #5
    //   869: lstore #11
    //   871: aload #7
    //   873: sipush #140
    //   876: invokevirtual getOctet : (I)I
    //   879: istore #18
    //   881: iload #4
    //   883: istore_3
    //   884: lload #5
    //   886: lstore #11
    //   888: new com/google/android/mms/pdu/PduBody
    //   891: astore #9
    //   893: iload #4
    //   895: istore_3
    //   896: lload #5
    //   898: lstore #11
    //   900: aload #9
    //   902: invokespecial <init> : ()V
    //   905: iload #18
    //   907: sipush #132
    //   910: if_icmpeq -> 927
    //   913: iload #18
    //   915: sipush #128
    //   918: if_icmpne -> 924
    //   921: goto -> 927
    //   924: goto -> 990
    //   927: iload #4
    //   929: istore_3
    //   930: lload #5
    //   932: lstore #11
    //   934: aload_0
    //   935: lload #13
    //   937: invokespecial loadParts : (J)[Lcom/google/android/mms/pdu/PduPart;
    //   940: astore #10
    //   942: aload #10
    //   944: ifnull -> 990
    //   947: iload #4
    //   949: istore_3
    //   950: lload #5
    //   952: lstore #11
    //   954: aload #10
    //   956: arraylength
    //   957: istore #19
    //   959: iconst_0
    //   960: istore_2
    //   961: iload_2
    //   962: iload #19
    //   964: if_icmpge -> 990
    //   967: iload #4
    //   969: istore_3
    //   970: lload #5
    //   972: lstore #11
    //   974: aload #9
    //   976: aload #10
    //   978: iload_2
    //   979: aaload
    //   980: invokevirtual addPart : (Lcom/google/android/mms/pdu/PduPart;)Z
    //   983: pop
    //   984: iinc #2, 1
    //   987: goto -> 961
    //   990: iload #18
    //   992: tableswitch default -> 1104, 128 -> 1365, 129 -> 1268, 130 -> 1247, 131 -> 1226, 132 -> 1203, 133 -> 1182, 134 -> 1161, 135 -> 1140, 136 -> 1119, 137 -> 1268, 138 -> 1268, 139 -> 1268, 140 -> 1268, 141 -> 1268, 142 -> 1268, 143 -> 1268, 144 -> 1268, 145 -> 1268, 146 -> 1268, 147 -> 1268, 148 -> 1268, 149 -> 1268, 150 -> 1268, 151 -> 1268
    //   1104: iload #4
    //   1106: istore_3
    //   1107: lload #5
    //   1109: lstore #11
    //   1111: new com/google/android/mms/MmsException
    //   1114: astore #9
    //   1116: goto -> 1445
    //   1119: iload #4
    //   1121: istore_3
    //   1122: lload #5
    //   1124: lstore #11
    //   1126: new com/google/android/mms/pdu/ReadOrigInd
    //   1129: dup
    //   1130: aload #7
    //   1132: invokespecial <init> : (Lcom/google/android/mms/pdu/PduHeaders;)V
    //   1135: astore #9
    //   1137: goto -> 1385
    //   1140: iload #4
    //   1142: istore_3
    //   1143: lload #5
    //   1145: lstore #11
    //   1147: new com/google/android/mms/pdu/ReadRecInd
    //   1150: dup
    //   1151: aload #7
    //   1153: invokespecial <init> : (Lcom/google/android/mms/pdu/PduHeaders;)V
    //   1156: astore #9
    //   1158: goto -> 1385
    //   1161: iload #4
    //   1163: istore_3
    //   1164: lload #5
    //   1166: lstore #11
    //   1168: new com/google/android/mms/pdu/DeliveryInd
    //   1171: dup
    //   1172: aload #7
    //   1174: invokespecial <init> : (Lcom/google/android/mms/pdu/PduHeaders;)V
    //   1177: astore #9
    //   1179: goto -> 1385
    //   1182: iload #4
    //   1184: istore_3
    //   1185: lload #5
    //   1187: lstore #11
    //   1189: new com/google/android/mms/pdu/AcknowledgeInd
    //   1192: dup
    //   1193: aload #7
    //   1195: invokespecial <init> : (Lcom/google/android/mms/pdu/PduHeaders;)V
    //   1198: astore #9
    //   1200: goto -> 1385
    //   1203: iload #4
    //   1205: istore_3
    //   1206: lload #5
    //   1208: lstore #11
    //   1210: new com/google/android/mms/pdu/RetrieveConf
    //   1213: dup
    //   1214: aload #7
    //   1216: aload #9
    //   1218: invokespecial <init> : (Lcom/google/android/mms/pdu/PduHeaders;Lcom/google/android/mms/pdu/PduBody;)V
    //   1221: astore #9
    //   1223: goto -> 1385
    //   1226: iload #4
    //   1228: istore_3
    //   1229: lload #5
    //   1231: lstore #11
    //   1233: new com/google/android/mms/pdu/NotifyRespInd
    //   1236: dup
    //   1237: aload #7
    //   1239: invokespecial <init> : (Lcom/google/android/mms/pdu/PduHeaders;)V
    //   1242: astore #9
    //   1244: goto -> 1385
    //   1247: iload #4
    //   1249: istore_3
    //   1250: lload #5
    //   1252: lstore #11
    //   1254: new com/google/android/mms/pdu/NotificationInd
    //   1257: dup
    //   1258: aload #7
    //   1260: invokespecial <init> : (Lcom/google/android/mms/pdu/PduHeaders;)V
    //   1263: astore #9
    //   1265: goto -> 1385
    //   1268: iload #4
    //   1270: istore_3
    //   1271: lload #5
    //   1273: lstore #11
    //   1275: new com/google/android/mms/MmsException
    //   1278: astore #9
    //   1280: iload #4
    //   1282: istore_3
    //   1283: lload #5
    //   1285: lstore #11
    //   1287: new java/lang/StringBuilder
    //   1290: astore #7
    //   1292: iload #4
    //   1294: istore_3
    //   1295: lload #5
    //   1297: lstore #11
    //   1299: aload #7
    //   1301: invokespecial <init> : ()V
    //   1304: iload #4
    //   1306: istore_3
    //   1307: lload #5
    //   1309: lstore #11
    //   1311: aload #7
    //   1313: ldc_w 'Unsupported PDU type: '
    //   1316: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1319: pop
    //   1320: iload #4
    //   1322: istore_3
    //   1323: lload #5
    //   1325: lstore #11
    //   1327: aload #7
    //   1329: iload #18
    //   1331: invokestatic toHexString : (I)Ljava/lang/String;
    //   1334: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1337: pop
    //   1338: iload #4
    //   1340: istore_3
    //   1341: lload #5
    //   1343: lstore #11
    //   1345: aload #9
    //   1347: aload #7
    //   1349: invokevirtual toString : ()Ljava/lang/String;
    //   1352: invokespecial <init> : (Ljava/lang/String;)V
    //   1355: iload #4
    //   1357: istore_3
    //   1358: lload #5
    //   1360: lstore #11
    //   1362: aload #9
    //   1364: athrow
    //   1365: iload #4
    //   1367: istore_3
    //   1368: lload #5
    //   1370: lstore #11
    //   1372: new com/google/android/mms/pdu/SendReq
    //   1375: dup
    //   1376: aload #7
    //   1378: aload #9
    //   1380: invokespecial <init> : (Lcom/google/android/mms/pdu/PduHeaders;Lcom/google/android/mms/pdu/PduBody;)V
    //   1383: astore #9
    //   1385: getstatic com/google/android/mms/pdu/PduPersister.PDU_CACHE_INSTANCE : Lcom/google/android/mms/util/PduCache;
    //   1388: astore #7
    //   1390: aload #7
    //   1392: monitorenter
    //   1393: new com/google/android/mms/util/PduCacheEntry
    //   1396: astore #10
    //   1398: aload #10
    //   1400: aload #9
    //   1402: iload #4
    //   1404: lload #5
    //   1406: invokespecial <init> : (Lcom/google/android/mms/pdu/GenericPdu;IJ)V
    //   1409: getstatic com/google/android/mms/pdu/PduPersister.PDU_CACHE_INSTANCE : Lcom/google/android/mms/util/PduCache;
    //   1412: aload_1
    //   1413: aload #10
    //   1415: invokevirtual put : (Landroid/net/Uri;Lcom/google/android/mms/util/PduCacheEntry;)Z
    //   1418: pop
    //   1419: getstatic com/google/android/mms/pdu/PduPersister.PDU_CACHE_INSTANCE : Lcom/google/android/mms/util/PduCache;
    //   1422: aload_1
    //   1423: iconst_0
    //   1424: invokevirtual setUpdating : (Landroid/net/Uri;Z)V
    //   1427: getstatic com/google/android/mms/pdu/PduPersister.PDU_CACHE_INSTANCE : Lcom/google/android/mms/util/PduCache;
    //   1430: invokevirtual notifyAll : ()V
    //   1433: aload #7
    //   1435: monitorexit
    //   1436: aload #9
    //   1438: areturn
    //   1439: astore_1
    //   1440: aload #7
    //   1442: monitorexit
    //   1443: aload_1
    //   1444: athrow
    //   1445: iload #4
    //   1447: istore_3
    //   1448: lload #5
    //   1450: lstore #11
    //   1452: new java/lang/StringBuilder
    //   1455: astore #7
    //   1457: iload #4
    //   1459: istore_3
    //   1460: lload #5
    //   1462: lstore #11
    //   1464: aload #7
    //   1466: invokespecial <init> : ()V
    //   1469: iload #4
    //   1471: istore_3
    //   1472: lload #5
    //   1474: lstore #11
    //   1476: aload #7
    //   1478: ldc_w 'Unrecognized PDU type: '
    //   1481: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1484: pop
    //   1485: iload #4
    //   1487: istore_3
    //   1488: lload #5
    //   1490: lstore #11
    //   1492: aload #7
    //   1494: iload #18
    //   1496: invokestatic toHexString : (I)Ljava/lang/String;
    //   1499: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1502: pop
    //   1503: iload #4
    //   1505: istore_3
    //   1506: lload #5
    //   1508: lstore #11
    //   1510: aload #9
    //   1512: aload #7
    //   1514: invokevirtual toString : ()Ljava/lang/String;
    //   1517: invokespecial <init> : (Ljava/lang/String;)V
    //   1520: iload #4
    //   1522: istore_3
    //   1523: lload #5
    //   1525: lstore #11
    //   1527: aload #9
    //   1529: athrow
    //   1530: iload #4
    //   1532: istore_3
    //   1533: lload #5
    //   1535: lstore #11
    //   1537: new com/google/android/mms/MmsException
    //   1540: astore #9
    //   1542: iload #4
    //   1544: istore_3
    //   1545: lload #5
    //   1547: lstore #11
    //   1549: aload #9
    //   1551: ldc_w 'Error! ID of the message: -1.'
    //   1554: invokespecial <init> : (Ljava/lang/String;)V
    //   1557: iload #4
    //   1559: istore_3
    //   1560: lload #5
    //   1562: lstore #11
    //   1564: aload #9
    //   1566: athrow
    //   1567: iload #4
    //   1569: istore_2
    //   1570: lload #5
    //   1572: lstore #15
    //   1574: new com/google/android/mms/MmsException
    //   1577: astore #7
    //   1579: iload #4
    //   1581: istore_2
    //   1582: lload #5
    //   1584: lstore #15
    //   1586: new java/lang/StringBuilder
    //   1589: astore #10
    //   1591: iload #4
    //   1593: istore_2
    //   1594: lload #5
    //   1596: lstore #15
    //   1598: aload #10
    //   1600: invokespecial <init> : ()V
    //   1603: iload #4
    //   1605: istore_2
    //   1606: lload #5
    //   1608: lstore #15
    //   1610: aload #10
    //   1612: ldc_w 'Bad uri: '
    //   1615: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1618: pop
    //   1619: iload #4
    //   1621: istore_2
    //   1622: lload #5
    //   1624: lstore #15
    //   1626: aload #10
    //   1628: aload_1
    //   1629: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1632: pop
    //   1633: iload #4
    //   1635: istore_2
    //   1636: lload #5
    //   1638: lstore #15
    //   1640: aload #7
    //   1642: aload #10
    //   1644: invokevirtual toString : ()Ljava/lang/String;
    //   1647: invokespecial <init> : (Ljava/lang/String;)V
    //   1650: iload #4
    //   1652: istore_2
    //   1653: lload #5
    //   1655: lstore #15
    //   1657: aload #7
    //   1659: athrow
    //   1660: astore #7
    //   1662: aload #9
    //   1664: ifnull -> 1680
    //   1667: iload_2
    //   1668: istore_3
    //   1669: lload #15
    //   1671: lstore #11
    //   1673: aload #9
    //   1675: invokeinterface close : ()V
    //   1680: iload_2
    //   1681: istore_3
    //   1682: lload #15
    //   1684: lstore #11
    //   1686: aload #7
    //   1688: athrow
    //   1689: astore #9
    //   1691: goto -> 1713
    //   1694: astore #9
    //   1696: goto -> 1701
    //   1699: astore #9
    //   1701: aload #7
    //   1703: monitorexit
    //   1704: aload #9
    //   1706: athrow
    //   1707: astore #9
    //   1709: lload #5
    //   1711: lstore #11
    //   1713: getstatic com/google/android/mms/pdu/PduPersister.PDU_CACHE_INSTANCE : Lcom/google/android/mms/util/PduCache;
    //   1716: astore #7
    //   1718: aload #7
    //   1720: monitorenter
    //   1721: iconst_0
    //   1722: ifeq -> 1749
    //   1725: new com/google/android/mms/util/PduCacheEntry
    //   1728: astore #10
    //   1730: aload #10
    //   1732: aconst_null
    //   1733: iload_3
    //   1734: lload #11
    //   1736: invokespecial <init> : (Lcom/google/android/mms/pdu/GenericPdu;IJ)V
    //   1739: getstatic com/google/android/mms/pdu/PduPersister.PDU_CACHE_INSTANCE : Lcom/google/android/mms/util/PduCache;
    //   1742: aload_1
    //   1743: aload #10
    //   1745: invokevirtual put : (Landroid/net/Uri;Lcom/google/android/mms/util/PduCacheEntry;)Z
    //   1748: pop
    //   1749: getstatic com/google/android/mms/pdu/PduPersister.PDU_CACHE_INSTANCE : Lcom/google/android/mms/util/PduCache;
    //   1752: aload_1
    //   1753: iconst_0
    //   1754: invokevirtual setUpdating : (Landroid/net/Uri;Z)V
    //   1757: getstatic com/google/android/mms/pdu/PduPersister.PDU_CACHE_INSTANCE : Lcom/google/android/mms/util/PduCache;
    //   1760: invokevirtual notifyAll : ()V
    //   1763: aload #7
    //   1765: monitorexit
    //   1766: aload #9
    //   1768: athrow
    //   1769: astore_1
    //   1770: aload #7
    //   1772: monitorexit
    //   1773: aload_1
    //   1774: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #541	-> 0
    //   #542	-> 0
    //   #543	-> 0
    //   #544	-> 7
    //   #546	-> 12
    //   #547	-> 20
    //   #552	-> 34
    //   #555	-> 40
    //   #553	-> 43
    //   #554	-> 45
    //   #556	-> 56
    //   #557	-> 68
    //   #558	-> 73
    //   #682	-> 83
    //   #683	-> 91
    //   #684	-> 95
    //   #686	-> 95
    //   #687	-> 110
    //   #689	-> 120
    //   #690	-> 128
    //   #691	-> 134
    //   #558	-> 137
    //   #691	-> 140
    //   #557	-> 146
    //   #547	-> 149
    //   #563	-> 152
    //   #564	-> 160
    //   #566	-> 163
    //   #568	-> 189
    //   #570	-> 211
    //   #573	-> 223
    //   #577	-> 263
    //   #578	-> 280
    //   #580	-> 297
    //   #581	-> 312
    //   #582	-> 364
    //   #583	-> 364
    //   #582	-> 407
    //   #584	-> 425
    //   #586	-> 428
    //   #587	-> 443
    //   #588	-> 495
    //   #589	-> 495
    //   #588	-> 538
    //   #590	-> 556
    //   #592	-> 559
    //   #593	-> 574
    //   #594	-> 626
    //   #595	-> 626
    //   #594	-> 669
    //   #596	-> 687
    //   #598	-> 690
    //   #599	-> 705
    //   #600	-> 757
    //   #601	-> 757
    //   #600	-> 800
    //   #602	-> 818
    //   #604	-> 821
    //   #605	-> 826
    //   #610	-> 840
    //   #615	-> 849
    //   #617	-> 864
    //   #618	-> 881
    //   #622	-> 905
    //   #624	-> 927
    //   #625	-> 942
    //   #626	-> 947
    //   #627	-> 959
    //   #628	-> 967
    //   #627	-> 984
    //   #625	-> 990
    //   #633	-> 990
    //   #678	-> 1104
    //   #641	-> 1119
    //   #642	-> 1137
    //   #656	-> 1140
    //   #657	-> 1158
    //   #638	-> 1161
    //   #639	-> 1179
    //   #650	-> 1182
    //   #651	-> 1200
    //   #644	-> 1203
    //   #645	-> 1223
    //   #653	-> 1226
    //   #654	-> 1244
    //   #635	-> 1247
    //   #636	-> 1265
    //   #674	-> 1268
    //   #675	-> 1320
    //   #647	-> 1365
    //   #648	-> 1385
    //   #682	-> 1385
    //   #683	-> 1393
    //   #684	-> 1393
    //   #686	-> 1393
    //   #687	-> 1409
    //   #689	-> 1419
    //   #690	-> 1427
    //   #691	-> 1433
    //   #692	-> 1436
    //   #693	-> 1436
    //   #691	-> 1439
    //   #678	-> 1445
    //   #679	-> 1485
    //   #611	-> 1530
    //   #574	-> 1567
    //   #604	-> 1660
    //   #605	-> 1667
    //   #607	-> 1680
    //   #682	-> 1689
    //   #564	-> 1694
    //   #682	-> 1707
    //   #683	-> 1721
    //   #684	-> 1725
    //   #686	-> 1725
    //   #687	-> 1739
    //   #689	-> 1749
    //   #690	-> 1757
    //   #691	-> 1763
    //   #692	-> 1766
    //   #691	-> 1769
    // Exception table:
    //   from	to	target	type
    //   12	20	1707	finally
    //   20	29	1699	finally
    //   34	40	43	java/lang/InterruptedException
    //   34	40	1699	finally
    //   45	56	1699	finally
    //   56	68	1699	finally
    //   73	83	1699	finally
    //   95	110	140	finally
    //   110	120	140	finally
    //   120	128	140	finally
    //   128	134	140	finally
    //   134	137	140	finally
    //   141	144	140	finally
    //   152	160	1694	finally
    //   160	163	1694	finally
    //   169	189	1689	finally
    //   195	200	1689	finally
    //   206	211	1689	finally
    //   217	223	1689	finally
    //   235	246	1660	finally
    //   253	263	1660	finally
    //   270	280	1660	finally
    //   287	297	1660	finally
    //   304	312	1660	finally
    //   319	328	1660	finally
    //   335	345	1660	finally
    //   352	364	1660	finally
    //   371	385	1660	finally
    //   392	407	1660	finally
    //   414	425	1660	finally
    //   435	443	1660	finally
    //   450	459	1660	finally
    //   466	476	1660	finally
    //   483	495	1660	finally
    //   502	517	1660	finally
    //   524	538	1660	finally
    //   545	556	1660	finally
    //   566	574	1660	finally
    //   581	590	1660	finally
    //   597	607	1660	finally
    //   614	626	1660	finally
    //   633	647	1660	finally
    //   654	669	1660	finally
    //   676	687	1660	finally
    //   697	705	1660	finally
    //   712	721	1660	finally
    //   728	738	1660	finally
    //   745	757	1660	finally
    //   764	778	1660	finally
    //   785	800	1660	finally
    //   807	818	1660	finally
    //   833	840	1689	finally
    //   856	864	1689	finally
    //   871	881	1689	finally
    //   888	893	1689	finally
    //   900	905	1689	finally
    //   934	942	1689	finally
    //   954	959	1689	finally
    //   974	984	1689	finally
    //   1111	1116	1689	finally
    //   1126	1137	1689	finally
    //   1147	1158	1689	finally
    //   1168	1179	1689	finally
    //   1189	1200	1689	finally
    //   1210	1223	1689	finally
    //   1233	1244	1689	finally
    //   1254	1265	1689	finally
    //   1275	1280	1689	finally
    //   1287	1292	1689	finally
    //   1299	1304	1689	finally
    //   1311	1320	1689	finally
    //   1327	1338	1689	finally
    //   1345	1355	1689	finally
    //   1362	1365	1689	finally
    //   1372	1385	1689	finally
    //   1393	1409	1439	finally
    //   1409	1419	1439	finally
    //   1419	1427	1439	finally
    //   1427	1433	1439	finally
    //   1433	1436	1439	finally
    //   1440	1443	1439	finally
    //   1452	1457	1689	finally
    //   1464	1469	1689	finally
    //   1476	1485	1689	finally
    //   1492	1503	1689	finally
    //   1510	1520	1689	finally
    //   1527	1530	1689	finally
    //   1537	1542	1689	finally
    //   1549	1557	1689	finally
    //   1564	1567	1689	finally
    //   1574	1579	1660	finally
    //   1586	1591	1660	finally
    //   1598	1603	1660	finally
    //   1610	1619	1660	finally
    //   1626	1633	1660	finally
    //   1640	1650	1660	finally
    //   1657	1660	1660	finally
    //   1673	1680	1689	finally
    //   1686	1689	1689	finally
    //   1701	1704	1699	finally
    //   1704	1707	1707	finally
    //   1725	1739	1769	finally
    //   1739	1749	1769	finally
    //   1749	1757	1769	finally
    //   1757	1763	1769	finally
    //   1763	1766	1769	finally
    //   1770	1773	1769	finally
  }
  
  private void persistAddress(long paramLong, int paramInt, EncodedStringValue[] paramArrayOfEncodedStringValue) {
    ContentValues contentValues = new ContentValues(3);
    int i;
    byte b;
    for (i = paramArrayOfEncodedStringValue.length, b = 0; b < i; ) {
      EncodedStringValue encodedStringValue = paramArrayOfEncodedStringValue[b];
      contentValues.clear();
      contentValues.put("address", toIsoString(encodedStringValue.getTextString()));
      contentValues.put("charset", Integer.valueOf(encodedStringValue.getCharacterSet()));
      contentValues.put("type", Integer.valueOf(paramInt));
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("content://mms/");
      stringBuilder.append(paramLong);
      stringBuilder.append("/addr");
      Uri uri = Uri.parse(stringBuilder.toString());
      SqliteWrapper.insert(this.mContext, this.mContentResolver, uri, contentValues);
      b++;
    } 
  }
  
  private static String getPartContentType(PduPart paramPduPart) {
    String str;
    if (paramPduPart.getContentType() == null) {
      paramPduPart = null;
    } else {
      str = toIsoString(paramPduPart.getContentType());
    } 
    return str;
  }
  
  public Uri persistPart(PduPart paramPduPart, long paramLong, HashMap<Uri, InputStream> paramHashMap) throws MmsException {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("content://mms/");
    stringBuilder.append(paramLong);
    stringBuilder.append("/part");
    Uri uri = Uri.parse(stringBuilder.toString());
    ContentValues contentValues = new ContentValues(8);
    int i = paramPduPart.getCharset();
    if (i != 0)
      contentValues.put("chset", Integer.valueOf(i)); 
    String str = getPartContentType(paramPduPart);
    if (str != null) {
      String str1 = str;
      if ("image/jpg".equals(str))
        str1 = "image/jpeg"; 
      contentValues.put("ct", str1);
      if ("application/smil".equals(str1))
        contentValues.put("seq", Integer.valueOf(-1)); 
      if (paramPduPart.getFilename() != null) {
        str = new String(paramPduPart.getFilename());
        contentValues.put("fn", str);
      } 
      if (paramPduPart.getName() != null) {
        str = new String(paramPduPart.getName());
        contentValues.put("name", str);
      } 
      if (paramPduPart.getContentDisposition() != null) {
        str = toIsoString(paramPduPart.getContentDisposition());
        contentValues.put("cd", str);
      } 
      if (paramPduPart.getContentId() != null) {
        str = toIsoString(paramPduPart.getContentId());
        contentValues.put("cid", str);
      } 
      if (paramPduPart.getContentLocation() != null) {
        str = toIsoString(paramPduPart.getContentLocation());
        contentValues.put("cl", str);
      } 
      Uri uri1 = SqliteWrapper.insert(this.mContext, this.mContentResolver, uri, contentValues);
      if (uri1 != null) {
        persistData(paramPduPart, uri1, str1, paramHashMap);
        paramPduPart.setDataUri(uri1);
        return uri1;
      } 
      throw new MmsException("Failed to persist part, return null.");
    } 
    throw new MmsException("MIME type of the part must be set.");
  }
  
  private void persistData(PduPart paramPduPart, Uri paramUri, String paramString, HashMap<Uri, InputStream> paramHashMap) throws MmsException {
    // Byte code:
    //   0: aconst_null
    //   1: astore #5
    //   3: aconst_null
    //   4: astore #6
    //   6: aconst_null
    //   7: astore #7
    //   9: aconst_null
    //   10: astore #8
    //   12: aconst_null
    //   13: astore #9
    //   15: aconst_null
    //   16: astore #10
    //   18: aconst_null
    //   19: astore #11
    //   21: aconst_null
    //   22: astore #12
    //   24: aconst_null
    //   25: astore #13
    //   27: aconst_null
    //   28: astore #14
    //   30: aconst_null
    //   31: astore #15
    //   33: aconst_null
    //   34: astore #16
    //   36: aconst_null
    //   37: astore #17
    //   39: aconst_null
    //   40: astore #18
    //   42: aconst_null
    //   43: astore #19
    //   45: aconst_null
    //   46: astore #20
    //   48: aconst_null
    //   49: astore #21
    //   51: aconst_null
    //   52: astore #22
    //   54: aconst_null
    //   55: astore #23
    //   57: aconst_null
    //   58: astore #24
    //   60: aconst_null
    //   61: astore #25
    //   63: aconst_null
    //   64: astore #26
    //   66: aconst_null
    //   67: astore #27
    //   69: aconst_null
    //   70: astore #28
    //   72: aconst_null
    //   73: astore #29
    //   75: aconst_null
    //   76: astore #30
    //   78: aconst_null
    //   79: astore #31
    //   81: aconst_null
    //   82: astore #32
    //   84: aconst_null
    //   85: astore #33
    //   87: aconst_null
    //   88: astore #34
    //   90: aconst_null
    //   91: astore #35
    //   93: aload_1
    //   94: invokevirtual getData : ()[B
    //   97: astore #36
    //   99: ldc_w 'text/plain'
    //   102: aload_3
    //   103: invokevirtual equals : (Ljava/lang/Object;)Z
    //   106: ifne -> 1704
    //   109: ldc_w 'application/smil'
    //   112: aload_3
    //   113: invokevirtual equals : (Ljava/lang/Object;)Z
    //   116: ifne -> 1701
    //   119: ldc_w 'text/html'
    //   122: aload_3
    //   123: invokevirtual equals : (Ljava/lang/Object;)Z
    //   126: ifeq -> 132
    //   129: goto -> 1704
    //   132: aload_3
    //   133: invokestatic isDrmConvertNeeded : (Ljava/lang/String;)Z
    //   136: istore #37
    //   138: iload #37
    //   140: ifeq -> 708
    //   143: aload_2
    //   144: ifnull -> 535
    //   147: aload_0
    //   148: getfield mContentResolver : Landroid/content/ContentResolver;
    //   151: astore #27
    //   153: aload #27
    //   155: aload_2
    //   156: ldc_w 'r'
    //   159: invokevirtual openFileDescriptor : (Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    //   162: astore #27
    //   164: aload #27
    //   166: invokevirtual getStatSize : ()J
    //   169: lstore #38
    //   171: lload #38
    //   173: lconst_0
    //   174: lcmp
    //   175: ifle -> 328
    //   178: aload #27
    //   180: ifnull -> 220
    //   183: aload #7
    //   185: astore #6
    //   187: aload #14
    //   189: astore #10
    //   191: aload #25
    //   193: astore #9
    //   195: aload #8
    //   197: astore #23
    //   199: aload #15
    //   201: astore #24
    //   203: aload #19
    //   205: astore #34
    //   207: aload #27
    //   209: invokevirtual close : ()V
    //   212: goto -> 220
    //   215: astore #27
    //   217: goto -> 430
    //   220: iconst_0
    //   221: ifeq -> 269
    //   224: new java/lang/NullPointerException
    //   227: dup
    //   228: invokespecial <init> : ()V
    //   231: athrow
    //   232: astore_1
    //   233: new java/lang/StringBuilder
    //   236: dup
    //   237: invokespecial <init> : ()V
    //   240: astore_2
    //   241: aload_2
    //   242: ldc_w 'IOException while closing: '
    //   245: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   248: pop
    //   249: aload_2
    //   250: aconst_null
    //   251: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   254: pop
    //   255: ldc 'PduPersister'
    //   257: aload_2
    //   258: invokevirtual toString : ()Ljava/lang/String;
    //   261: aload_1
    //   262: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   265: pop
    //   266: goto -> 269
    //   269: iconst_0
    //   270: ifeq -> 315
    //   273: new java/lang/NullPointerException
    //   276: dup
    //   277: invokespecial <init> : ()V
    //   280: athrow
    //   281: astore_2
    //   282: new java/lang/StringBuilder
    //   285: dup
    //   286: invokespecial <init> : ()V
    //   289: astore_1
    //   290: aload_1
    //   291: ldc_w 'IOException while closing: '
    //   294: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   297: pop
    //   298: aload_1
    //   299: aconst_null
    //   300: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   303: pop
    //   304: ldc 'PduPersister'
    //   306: aload_1
    //   307: invokevirtual toString : ()Ljava/lang/String;
    //   310: aload_2
    //   311: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   314: pop
    //   315: iconst_0
    //   316: ifeq -> 327
    //   319: new java/lang/NullPointerException
    //   322: dup
    //   323: invokespecial <init> : ()V
    //   326: athrow
    //   327: return
    //   328: aload #27
    //   330: ifnull -> 366
    //   333: aload #13
    //   335: astore #11
    //   337: aload #22
    //   339: astore #34
    //   341: aload #5
    //   343: astore #10
    //   345: aload #23
    //   347: astore #33
    //   349: aload #6
    //   351: astore #9
    //   353: aload #27
    //   355: invokevirtual close : ()V
    //   358: goto -> 366
    //   361: astore #27
    //   363: goto -> 430
    //   366: goto -> 535
    //   369: astore #10
    //   371: aload #27
    //   373: ifnull -> 408
    //   376: aload #27
    //   378: invokevirtual close : ()V
    //   381: goto -> 408
    //   384: astore #27
    //   386: aload #28
    //   388: astore #34
    //   390: aload #29
    //   392: astore #11
    //   394: aload #30
    //   396: astore #33
    //   398: aload #10
    //   400: aload #27
    //   402: invokevirtual addSuppressed : (Ljava/lang/Throwable;)V
    //   405: goto -> 408
    //   408: aload #28
    //   410: astore #34
    //   412: aload #29
    //   414: astore #11
    //   416: aload #30
    //   418: astore #33
    //   420: aload #10
    //   422: athrow
    //   423: astore #27
    //   425: goto -> 430
    //   428: astore #27
    //   430: aload #28
    //   432: astore #34
    //   434: aload #29
    //   436: astore #11
    //   438: aload #30
    //   440: astore #33
    //   442: new java/lang/StringBuilder
    //   445: astore #10
    //   447: aload #28
    //   449: astore #34
    //   451: aload #29
    //   453: astore #11
    //   455: aload #30
    //   457: astore #33
    //   459: aload #10
    //   461: invokespecial <init> : ()V
    //   464: aload #28
    //   466: astore #34
    //   468: aload #29
    //   470: astore #11
    //   472: aload #30
    //   474: astore #33
    //   476: aload #10
    //   478: ldc_w 'Can't get file info for: '
    //   481: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   484: pop
    //   485: aload #28
    //   487: astore #34
    //   489: aload #29
    //   491: astore #11
    //   493: aload #30
    //   495: astore #33
    //   497: aload #10
    //   499: aload_1
    //   500: invokevirtual getDataUri : ()Landroid/net/Uri;
    //   503: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   506: pop
    //   507: aload #28
    //   509: astore #34
    //   511: aload #29
    //   513: astore #11
    //   515: aload #30
    //   517: astore #33
    //   519: ldc 'PduPersister'
    //   521: aload #10
    //   523: invokevirtual toString : ()Ljava/lang/String;
    //   526: aload #27
    //   528: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   531: pop
    //   532: goto -> 535
    //   535: aload #28
    //   537: astore #34
    //   539: aload #29
    //   541: astore #11
    //   543: aload #30
    //   545: astore #33
    //   547: aload_0
    //   548: getfield mContext : Landroid/content/Context;
    //   551: aload_3
    //   552: invokestatic open : (Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/mms/util/DrmConvertSession;
    //   555: astore #27
    //   557: aload #27
    //   559: ifnull -> 568
    //   562: aload #27
    //   564: astore_3
    //   565: goto -> 711
    //   568: aload #27
    //   570: astore #34
    //   572: aload #27
    //   574: astore #11
    //   576: aload #27
    //   578: astore #33
    //   580: new com/google/android/mms/MmsException
    //   583: astore_1
    //   584: aload #27
    //   586: astore #34
    //   588: aload #27
    //   590: astore #11
    //   592: aload #27
    //   594: astore #33
    //   596: new java/lang/StringBuilder
    //   599: astore_2
    //   600: aload #27
    //   602: astore #34
    //   604: aload #27
    //   606: astore #11
    //   608: aload #27
    //   610: astore #33
    //   612: aload_2
    //   613: invokespecial <init> : ()V
    //   616: aload #27
    //   618: astore #34
    //   620: aload #27
    //   622: astore #11
    //   624: aload #27
    //   626: astore #33
    //   628: aload_2
    //   629: ldc_w 'Mimetype '
    //   632: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   635: pop
    //   636: aload #27
    //   638: astore #34
    //   640: aload #27
    //   642: astore #11
    //   644: aload #27
    //   646: astore #33
    //   648: aload_2
    //   649: aload_3
    //   650: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   653: pop
    //   654: aload #27
    //   656: astore #34
    //   658: aload #27
    //   660: astore #11
    //   662: aload #27
    //   664: astore #33
    //   666: aload_2
    //   667: ldc_w ' can not be converted.'
    //   670: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   673: pop
    //   674: aload #27
    //   676: astore #34
    //   678: aload #27
    //   680: astore #11
    //   682: aload #27
    //   684: astore #33
    //   686: aload_1
    //   687: aload_2
    //   688: invokevirtual toString : ()Ljava/lang/String;
    //   691: invokespecial <init> : (Ljava/lang/String;)V
    //   694: aload #27
    //   696: astore #34
    //   698: aload #27
    //   700: astore #11
    //   702: aload #27
    //   704: astore #33
    //   706: aload_1
    //   707: athrow
    //   708: aload #35
    //   710: astore_3
    //   711: aload_3
    //   712: astore #34
    //   714: aload_3
    //   715: astore #11
    //   717: aload_3
    //   718: astore #33
    //   720: aload_0
    //   721: getfield mContentResolver : Landroid/content/ContentResolver;
    //   724: aload_2
    //   725: invokevirtual openOutputStream : (Landroid/net/Uri;)Ljava/io/OutputStream;
    //   728: astore #27
    //   730: aload #36
    //   732: ifnonnull -> 1427
    //   735: aload #27
    //   737: astore #11
    //   739: aload_3
    //   740: astore #34
    //   742: aload #27
    //   744: astore #10
    //   746: aload_3
    //   747: astore #33
    //   749: aload #27
    //   751: astore #9
    //   753: aload_3
    //   754: astore #24
    //   756: aload_1
    //   757: invokevirtual getDataUri : ()Landroid/net/Uri;
    //   760: astore #16
    //   762: aload #16
    //   764: ifnull -> 1159
    //   767: aload #27
    //   769: astore #6
    //   771: aload #14
    //   773: astore #10
    //   775: aload_3
    //   776: astore #9
    //   778: aload #27
    //   780: astore #23
    //   782: aload #15
    //   784: astore #24
    //   786: aload_3
    //   787: astore #26
    //   789: aload #27
    //   791: astore #11
    //   793: aload #19
    //   795: astore #34
    //   797: aload_3
    //   798: astore #33
    //   800: aload #16
    //   802: aload_2
    //   803: invokevirtual equals : (Ljava/lang/Object;)Z
    //   806: ifeq -> 812
    //   809: goto -> 1159
    //   812: aload #21
    //   814: astore_1
    //   815: aload #4
    //   817: ifnull -> 910
    //   820: aload #21
    //   822: astore_1
    //   823: aload #27
    //   825: astore #6
    //   827: aload #14
    //   829: astore #10
    //   831: aload_3
    //   832: astore #9
    //   834: aload #27
    //   836: astore #23
    //   838: aload #15
    //   840: astore #24
    //   842: aload_3
    //   843: astore #26
    //   845: aload #27
    //   847: astore #11
    //   849: aload #19
    //   851: astore #34
    //   853: aload_3
    //   854: astore #33
    //   856: aload #4
    //   858: aload #16
    //   860: invokevirtual containsKey : (Ljava/lang/Object;)Z
    //   863: ifeq -> 910
    //   866: aload #27
    //   868: astore #6
    //   870: aload #14
    //   872: astore #10
    //   874: aload_3
    //   875: astore #9
    //   877: aload #27
    //   879: astore #23
    //   881: aload #15
    //   883: astore #24
    //   885: aload_3
    //   886: astore #26
    //   888: aload #27
    //   890: astore #11
    //   892: aload #19
    //   894: astore #34
    //   896: aload_3
    //   897: astore #33
    //   899: aload #4
    //   901: aload #16
    //   903: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   906: checkcast java/io/InputStream
    //   909: astore_1
    //   910: aload_1
    //   911: astore_2
    //   912: aload_1
    //   913: ifnonnull -> 956
    //   916: aload #27
    //   918: astore #6
    //   920: aload_1
    //   921: astore #10
    //   923: aload_3
    //   924: astore #9
    //   926: aload #27
    //   928: astore #23
    //   930: aload_1
    //   931: astore #24
    //   933: aload_3
    //   934: astore #26
    //   936: aload #27
    //   938: astore #11
    //   940: aload_1
    //   941: astore #34
    //   943: aload_3
    //   944: astore #33
    //   946: aload_0
    //   947: getfield mContentResolver : Landroid/content/ContentResolver;
    //   950: aload #16
    //   952: invokevirtual openInputStream : (Landroid/net/Uri;)Ljava/io/InputStream;
    //   955: astore_2
    //   956: aload_2
    //   957: astore #34
    //   959: aload_2
    //   960: astore #11
    //   962: aload_2
    //   963: astore #4
    //   965: sipush #8192
    //   968: newarray byte
    //   970: astore #17
    //   972: aload_2
    //   973: astore_1
    //   974: aload_1
    //   975: astore #34
    //   977: aload_1
    //   978: astore #11
    //   980: aload_1
    //   981: astore #4
    //   983: aload_1
    //   984: aload #17
    //   986: invokevirtual read : ([B)I
    //   989: istore #40
    //   991: iload #40
    //   993: iconst_m1
    //   994: if_icmpeq -> 1135
    //   997: iload #37
    //   999: ifne -> 1045
    //   1002: aload #27
    //   1004: astore #6
    //   1006: aload_1
    //   1007: astore #10
    //   1009: aload_3
    //   1010: astore #9
    //   1012: aload #27
    //   1014: astore #23
    //   1016: aload_1
    //   1017: astore #24
    //   1019: aload_3
    //   1020: astore #26
    //   1022: aload #27
    //   1024: astore #11
    //   1026: aload_1
    //   1027: astore #34
    //   1029: aload_3
    //   1030: astore #33
    //   1032: aload #27
    //   1034: aload #17
    //   1036: iconst_0
    //   1037: iload #40
    //   1039: invokevirtual write : ([BII)V
    //   1042: goto -> 1089
    //   1045: aload_1
    //   1046: astore #34
    //   1048: aload_1
    //   1049: astore #11
    //   1051: aload_1
    //   1052: astore #4
    //   1054: aload_3
    //   1055: aload #17
    //   1057: iload #40
    //   1059: invokevirtual convert : ([BI)[B
    //   1062: astore_2
    //   1063: aload_2
    //   1064: ifnull -> 1092
    //   1067: aload_1
    //   1068: astore #34
    //   1070: aload_1
    //   1071: astore #11
    //   1073: aload_1
    //   1074: astore #4
    //   1076: aload_2
    //   1077: arraylength
    //   1078: istore #40
    //   1080: aload #27
    //   1082: aload_2
    //   1083: iconst_0
    //   1084: iload #40
    //   1086: invokevirtual write : ([BII)V
    //   1089: goto -> 974
    //   1092: new com/google/android/mms/MmsException
    //   1095: astore_2
    //   1096: aload_2
    //   1097: ldc_w 'Error converting drm data.'
    //   1100: invokespecial <init> : (Ljava/lang/String;)V
    //   1103: aload_2
    //   1104: athrow
    //   1105: astore #4
    //   1107: aload_1
    //   1108: astore_2
    //   1109: aload #4
    //   1111: astore_1
    //   1112: goto -> 2427
    //   1115: astore #4
    //   1117: aload_1
    //   1118: astore_2
    //   1119: aload #4
    //   1121: astore_1
    //   1122: goto -> 2277
    //   1125: astore #4
    //   1127: aload_1
    //   1128: astore_2
    //   1129: aload #4
    //   1131: astore_1
    //   1132: goto -> 2352
    //   1135: goto -> 1899
    //   1138: astore_1
    //   1139: aload #34
    //   1141: astore_2
    //   1142: goto -> 2427
    //   1145: astore_1
    //   1146: aload #11
    //   1148: astore_2
    //   1149: goto -> 2277
    //   1152: astore_1
    //   1153: aload #4
    //   1155: astore_2
    //   1156: goto -> 2352
    //   1159: aload #27
    //   1161: astore #6
    //   1163: aload #14
    //   1165: astore #10
    //   1167: aload_3
    //   1168: astore #9
    //   1170: aload #27
    //   1172: astore #23
    //   1174: aload #15
    //   1176: astore #24
    //   1178: aload_3
    //   1179: astore #26
    //   1181: aload #27
    //   1183: astore #11
    //   1185: aload #19
    //   1187: astore #34
    //   1189: aload_3
    //   1190: astore #33
    //   1192: ldc 'PduPersister'
    //   1194: ldc_w 'Can't find data for this part.'
    //   1197: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1200: pop
    //   1201: aload #27
    //   1203: ifnull -> 1249
    //   1206: aload #27
    //   1208: invokevirtual close : ()V
    //   1211: goto -> 1249
    //   1214: astore_2
    //   1215: new java/lang/StringBuilder
    //   1218: dup
    //   1219: invokespecial <init> : ()V
    //   1222: astore_1
    //   1223: aload_1
    //   1224: ldc_w 'IOException while closing: '
    //   1227: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1230: pop
    //   1231: aload_1
    //   1232: aload #27
    //   1234: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1237: pop
    //   1238: ldc 'PduPersister'
    //   1240: aload_1
    //   1241: invokevirtual toString : ()Ljava/lang/String;
    //   1244: aload_2
    //   1245: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   1248: pop
    //   1249: iconst_0
    //   1250: ifeq -> 1295
    //   1253: new java/lang/NullPointerException
    //   1256: dup
    //   1257: invokespecial <init> : ()V
    //   1260: athrow
    //   1261: astore_1
    //   1262: new java/lang/StringBuilder
    //   1265: dup
    //   1266: invokespecial <init> : ()V
    //   1269: astore_2
    //   1270: aload_2
    //   1271: ldc_w 'IOException while closing: '
    //   1274: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1277: pop
    //   1278: aload_2
    //   1279: aconst_null
    //   1280: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1283: pop
    //   1284: ldc 'PduPersister'
    //   1286: aload_2
    //   1287: invokevirtual toString : ()Ljava/lang/String;
    //   1290: aload_1
    //   1291: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   1294: pop
    //   1295: aload_3
    //   1296: ifnull -> 1384
    //   1299: aload_3
    //   1300: aconst_null
    //   1301: invokevirtual close : (Ljava/lang/String;)I
    //   1304: pop
    //   1305: new java/io/File
    //   1308: dup
    //   1309: aconst_null
    //   1310: invokespecial <init> : (Ljava/lang/String;)V
    //   1313: astore #4
    //   1315: new android/content/ContentValues
    //   1318: dup
    //   1319: iconst_0
    //   1320: invokespecial <init> : (I)V
    //   1323: astore_2
    //   1324: aload_0
    //   1325: getfield mContext : Landroid/content/Context;
    //   1328: astore_3
    //   1329: aload_0
    //   1330: getfield mContentResolver : Landroid/content/ContentResolver;
    //   1333: astore_1
    //   1334: new java/lang/StringBuilder
    //   1337: dup
    //   1338: invokespecial <init> : ()V
    //   1341: astore #27
    //   1343: aload #27
    //   1345: ldc_w 'content://mms/resetFilePerm/'
    //   1348: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1351: pop
    //   1352: aload #27
    //   1354: aload #4
    //   1356: invokevirtual getName : ()Ljava/lang/String;
    //   1359: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1362: pop
    //   1363: aload #27
    //   1365: invokevirtual toString : ()Ljava/lang/String;
    //   1368: invokestatic parse : (Ljava/lang/String;)Landroid/net/Uri;
    //   1371: astore #4
    //   1373: aload_3
    //   1374: aload_1
    //   1375: aload #4
    //   1377: aload_2
    //   1378: aconst_null
    //   1379: aconst_null
    //   1380: invokestatic update : (Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   1383: pop
    //   1384: return
    //   1385: astore_1
    //   1386: aload #11
    //   1388: astore #27
    //   1390: aload #20
    //   1392: astore_2
    //   1393: aload #34
    //   1395: astore_3
    //   1396: goto -> 2427
    //   1399: astore_1
    //   1400: aload #10
    //   1402: astore #27
    //   1404: aload #17
    //   1406: astore_2
    //   1407: aload #33
    //   1409: astore_3
    //   1410: goto -> 2277
    //   1413: astore_1
    //   1414: aload #9
    //   1416: astore #27
    //   1418: aload #18
    //   1420: astore_2
    //   1421: aload #24
    //   1423: astore_3
    //   1424: goto -> 2352
    //   1427: iload #37
    //   1429: ifne -> 1466
    //   1432: aload #27
    //   1434: astore #11
    //   1436: aload_3
    //   1437: astore #34
    //   1439: aload #27
    //   1441: astore #10
    //   1443: aload_3
    //   1444: astore #33
    //   1446: aload #27
    //   1448: astore #9
    //   1450: aload_3
    //   1451: astore #24
    //   1453: aload #27
    //   1455: aload #36
    //   1457: invokevirtual write : ([B)V
    //   1460: aload #16
    //   1462: astore_1
    //   1463: goto -> 1899
    //   1466: aload #27
    //   1468: astore #6
    //   1470: aload #14
    //   1472: astore #10
    //   1474: aload_3
    //   1475: astore #9
    //   1477: aload #27
    //   1479: astore #23
    //   1481: aload #15
    //   1483: astore #24
    //   1485: aload_3
    //   1486: astore #26
    //   1488: aload #27
    //   1490: astore #11
    //   1492: aload #19
    //   1494: astore #34
    //   1496: aload_3
    //   1497: astore #33
    //   1499: aload_3
    //   1500: aload #36
    //   1502: aload #36
    //   1504: arraylength
    //   1505: invokevirtual convert : ([BI)[B
    //   1508: astore_1
    //   1509: aload_1
    //   1510: ifnull -> 1561
    //   1513: aload #27
    //   1515: astore #6
    //   1517: aload #14
    //   1519: astore #10
    //   1521: aload_3
    //   1522: astore #9
    //   1524: aload #27
    //   1526: astore #23
    //   1528: aload #15
    //   1530: astore #24
    //   1532: aload_3
    //   1533: astore #26
    //   1535: aload #27
    //   1537: astore #11
    //   1539: aload #19
    //   1541: astore #34
    //   1543: aload_3
    //   1544: astore #33
    //   1546: aload #27
    //   1548: aload_1
    //   1549: iconst_0
    //   1550: aload_1
    //   1551: arraylength
    //   1552: invokevirtual write : ([BII)V
    //   1555: aload #16
    //   1557: astore_1
    //   1558: goto -> 1899
    //   1561: aload #27
    //   1563: astore #6
    //   1565: aload #14
    //   1567: astore #10
    //   1569: aload_3
    //   1570: astore #9
    //   1572: aload #27
    //   1574: astore #23
    //   1576: aload #15
    //   1578: astore #24
    //   1580: aload_3
    //   1581: astore #26
    //   1583: aload #27
    //   1585: astore #11
    //   1587: aload #19
    //   1589: astore #34
    //   1591: aload_3
    //   1592: astore #33
    //   1594: new com/google/android/mms/MmsException
    //   1597: astore_1
    //   1598: aload #27
    //   1600: astore #6
    //   1602: aload #14
    //   1604: astore #10
    //   1606: aload_3
    //   1607: astore #9
    //   1609: aload #27
    //   1611: astore #23
    //   1613: aload #15
    //   1615: astore #24
    //   1617: aload_3
    //   1618: astore #26
    //   1620: aload #27
    //   1622: astore #11
    //   1624: aload #19
    //   1626: astore #34
    //   1628: aload_3
    //   1629: astore #33
    //   1631: aload_1
    //   1632: ldc_w 'Error converting drm data.'
    //   1635: invokespecial <init> : (Ljava/lang/String;)V
    //   1638: aload #27
    //   1640: astore #6
    //   1642: aload #14
    //   1644: astore #10
    //   1646: aload_3
    //   1647: astore #9
    //   1649: aload #27
    //   1651: astore #23
    //   1653: aload #15
    //   1655: astore #24
    //   1657: aload_3
    //   1658: astore #26
    //   1660: aload #27
    //   1662: astore #11
    //   1664: aload #19
    //   1666: astore #34
    //   1668: aload_3
    //   1669: astore #33
    //   1671: aload_1
    //   1672: athrow
    //   1673: astore_1
    //   1674: aload #6
    //   1676: astore #27
    //   1678: aload #10
    //   1680: astore_2
    //   1681: aload #9
    //   1683: astore_3
    //   1684: goto -> 2277
    //   1687: astore_1
    //   1688: aload #23
    //   1690: astore #27
    //   1692: aload #24
    //   1694: astore_2
    //   1695: aload #26
    //   1697: astore_3
    //   1698: goto -> 2352
    //   1701: goto -> 1704
    //   1704: aload #28
    //   1706: astore #34
    //   1708: aload #29
    //   1710: astore #11
    //   1712: aload #30
    //   1714: astore #33
    //   1716: new android/content/ContentValues
    //   1719: astore_3
    //   1720: aload #28
    //   1722: astore #34
    //   1724: aload #29
    //   1726: astore #11
    //   1728: aload #30
    //   1730: astore #33
    //   1732: aload_3
    //   1733: invokespecial <init> : ()V
    //   1736: aload #36
    //   1738: astore_1
    //   1739: aload #36
    //   1741: ifnonnull -> 1799
    //   1744: aload #28
    //   1746: astore #34
    //   1748: aload #29
    //   1750: astore #11
    //   1752: aload #30
    //   1754: astore #33
    //   1756: new java/lang/String
    //   1759: astore_1
    //   1760: aload #28
    //   1762: astore #34
    //   1764: aload #29
    //   1766: astore #11
    //   1768: aload #30
    //   1770: astore #33
    //   1772: aload_1
    //   1773: ldc_w ''
    //   1776: invokespecial <init> : (Ljava/lang/String;)V
    //   1779: aload #28
    //   1781: astore #34
    //   1783: aload #29
    //   1785: astore #11
    //   1787: aload #30
    //   1789: astore #33
    //   1791: aload_1
    //   1792: ldc_w 'utf-8'
    //   1795: invokevirtual getBytes : (Ljava/lang/String;)[B
    //   1798: astore_1
    //   1799: aload #28
    //   1801: astore #34
    //   1803: aload #29
    //   1805: astore #11
    //   1807: aload #30
    //   1809: astore #33
    //   1811: new com/google/android/mms/pdu/EncodedStringValue
    //   1814: astore #4
    //   1816: aload #28
    //   1818: astore #34
    //   1820: aload #29
    //   1822: astore #11
    //   1824: aload #30
    //   1826: astore #33
    //   1828: aload #4
    //   1830: aload_1
    //   1831: invokespecial <init> : ([B)V
    //   1834: aload #28
    //   1836: astore #34
    //   1838: aload #29
    //   1840: astore #11
    //   1842: aload #30
    //   1844: astore #33
    //   1846: aload_3
    //   1847: ldc 'text'
    //   1849: aload #4
    //   1851: invokevirtual getString : ()Ljava/lang/String;
    //   1854: invokevirtual put : (Ljava/lang/String;Ljava/lang/String;)V
    //   1857: aload #28
    //   1859: astore #34
    //   1861: aload #29
    //   1863: astore #11
    //   1865: aload #30
    //   1867: astore #33
    //   1869: aload_0
    //   1870: getfield mContentResolver : Landroid/content/ContentResolver;
    //   1873: aload_2
    //   1874: aload_3
    //   1875: aconst_null
    //   1876: aconst_null
    //   1877: invokevirtual update : (Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   1880: istore #40
    //   1882: iload #40
    //   1884: iconst_1
    //   1885: if_icmpne -> 2090
    //   1888: aconst_null
    //   1889: astore_2
    //   1890: aload #27
    //   1892: astore_3
    //   1893: aload #16
    //   1895: astore_1
    //   1896: aload_2
    //   1897: astore #27
    //   1899: aload #27
    //   1901: ifnull -> 1951
    //   1904: aload #27
    //   1906: invokevirtual close : ()V
    //   1909: goto -> 1951
    //   1912: astore_2
    //   1913: new java/lang/StringBuilder
    //   1916: dup
    //   1917: invokespecial <init> : ()V
    //   1920: astore #4
    //   1922: aload #4
    //   1924: ldc_w 'IOException while closing: '
    //   1927: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1930: pop
    //   1931: aload #4
    //   1933: aload #27
    //   1935: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1938: pop
    //   1939: ldc 'PduPersister'
    //   1941: aload #4
    //   1943: invokevirtual toString : ()Ljava/lang/String;
    //   1946: aload_2
    //   1947: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   1950: pop
    //   1951: aload_1
    //   1952: ifnull -> 2000
    //   1955: aload_1
    //   1956: invokevirtual close : ()V
    //   1959: goto -> 2000
    //   1962: astore_2
    //   1963: new java/lang/StringBuilder
    //   1966: dup
    //   1967: invokespecial <init> : ()V
    //   1970: astore #4
    //   1972: aload #4
    //   1974: ldc_w 'IOException while closing: '
    //   1977: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1980: pop
    //   1981: aload #4
    //   1983: aload_1
    //   1984: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1987: pop
    //   1988: ldc 'PduPersister'
    //   1990: aload #4
    //   1992: invokevirtual toString : ()Ljava/lang/String;
    //   1995: aload_2
    //   1996: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   1999: pop
    //   2000: aload_3
    //   2001: ifnull -> 2089
    //   2004: aload_3
    //   2005: aconst_null
    //   2006: invokevirtual close : (Ljava/lang/String;)I
    //   2009: pop
    //   2010: new java/io/File
    //   2013: dup
    //   2014: aconst_null
    //   2015: invokespecial <init> : (Ljava/lang/String;)V
    //   2018: astore #4
    //   2020: new android/content/ContentValues
    //   2023: dup
    //   2024: iconst_0
    //   2025: invokespecial <init> : (I)V
    //   2028: astore_3
    //   2029: aload_0
    //   2030: getfield mContext : Landroid/content/Context;
    //   2033: astore_1
    //   2034: aload_0
    //   2035: getfield mContentResolver : Landroid/content/ContentResolver;
    //   2038: astore_2
    //   2039: new java/lang/StringBuilder
    //   2042: dup
    //   2043: invokespecial <init> : ()V
    //   2046: astore #27
    //   2048: aload #27
    //   2050: ldc_w 'content://mms/resetFilePerm/'
    //   2053: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2056: pop
    //   2057: aload #27
    //   2059: aload #4
    //   2061: invokevirtual getName : ()Ljava/lang/String;
    //   2064: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2067: pop
    //   2068: aload #27
    //   2070: invokevirtual toString : ()Ljava/lang/String;
    //   2073: invokestatic parse : (Ljava/lang/String;)Landroid/net/Uri;
    //   2076: astore #4
    //   2078: aload_1
    //   2079: aload_2
    //   2080: aload #4
    //   2082: aload_3
    //   2083: aconst_null
    //   2084: aconst_null
    //   2085: invokestatic update : (Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   2088: pop
    //   2089: return
    //   2090: aload #28
    //   2092: astore #34
    //   2094: aload #29
    //   2096: astore #11
    //   2098: aload #30
    //   2100: astore #33
    //   2102: new com/google/android/mms/MmsException
    //   2105: astore_3
    //   2106: aload #28
    //   2108: astore #34
    //   2110: aload #29
    //   2112: astore #11
    //   2114: aload #30
    //   2116: astore #33
    //   2118: new java/lang/StringBuilder
    //   2121: astore_1
    //   2122: aload #28
    //   2124: astore #34
    //   2126: aload #29
    //   2128: astore #11
    //   2130: aload #30
    //   2132: astore #33
    //   2134: aload_1
    //   2135: invokespecial <init> : ()V
    //   2138: aload #28
    //   2140: astore #34
    //   2142: aload #29
    //   2144: astore #11
    //   2146: aload #30
    //   2148: astore #33
    //   2150: aload_1
    //   2151: ldc_w 'unable to update '
    //   2154: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2157: pop
    //   2158: aload #28
    //   2160: astore #34
    //   2162: aload #29
    //   2164: astore #11
    //   2166: aload #30
    //   2168: astore #33
    //   2170: aload_1
    //   2171: aload_2
    //   2172: invokevirtual toString : ()Ljava/lang/String;
    //   2175: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2178: pop
    //   2179: aload #28
    //   2181: astore #34
    //   2183: aload #29
    //   2185: astore #11
    //   2187: aload #30
    //   2189: astore #33
    //   2191: aload_3
    //   2192: aload_1
    //   2193: invokevirtual toString : ()Ljava/lang/String;
    //   2196: invokespecial <init> : (Ljava/lang/String;)V
    //   2199: aload #28
    //   2201: astore #34
    //   2203: aload #29
    //   2205: astore #11
    //   2207: aload #30
    //   2209: astore #33
    //   2211: aload_3
    //   2212: athrow
    //   2213: astore_1
    //   2214: aconst_null
    //   2215: astore #27
    //   2217: aload #20
    //   2219: astore_2
    //   2220: aload #34
    //   2222: astore_3
    //   2223: goto -> 2427
    //   2226: astore_1
    //   2227: aconst_null
    //   2228: astore #27
    //   2230: aload #17
    //   2232: astore_2
    //   2233: aload #11
    //   2235: astore_3
    //   2236: goto -> 2277
    //   2239: astore_1
    //   2240: aconst_null
    //   2241: astore #27
    //   2243: aload #18
    //   2245: astore_2
    //   2246: aload #33
    //   2248: astore_3
    //   2249: goto -> 2352
    //   2252: astore_1
    //   2253: aload #12
    //   2255: astore #27
    //   2257: aload #20
    //   2259: astore_2
    //   2260: aload #34
    //   2262: astore_3
    //   2263: goto -> 2427
    //   2266: astore_1
    //   2267: aload #31
    //   2269: astore_3
    //   2270: aload #17
    //   2272: astore_2
    //   2273: aload #9
    //   2275: astore #27
    //   2277: aload #27
    //   2279: astore #11
    //   2281: aload_2
    //   2282: astore #34
    //   2284: aload_3
    //   2285: astore #33
    //   2287: ldc 'PduPersister'
    //   2289: ldc_w 'Failed to read/write data.'
    //   2292: aload_1
    //   2293: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   2296: pop
    //   2297: aload #27
    //   2299: astore #11
    //   2301: aload_2
    //   2302: astore #34
    //   2304: aload_3
    //   2305: astore #33
    //   2307: new com/google/android/mms/MmsException
    //   2310: astore #4
    //   2312: aload #27
    //   2314: astore #11
    //   2316: aload_2
    //   2317: astore #34
    //   2319: aload_3
    //   2320: astore #33
    //   2322: aload #4
    //   2324: aload_1
    //   2325: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   2328: aload #27
    //   2330: astore #11
    //   2332: aload_2
    //   2333: astore #34
    //   2335: aload_3
    //   2336: astore #33
    //   2338: aload #4
    //   2340: athrow
    //   2341: astore_1
    //   2342: aload #32
    //   2344: astore_3
    //   2345: aload #18
    //   2347: astore_2
    //   2348: aload #10
    //   2350: astore #27
    //   2352: aload #27
    //   2354: astore #11
    //   2356: aload_2
    //   2357: astore #34
    //   2359: aload_3
    //   2360: astore #33
    //   2362: ldc 'PduPersister'
    //   2364: ldc_w 'Failed to open Input/Output stream.'
    //   2367: aload_1
    //   2368: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   2371: pop
    //   2372: aload #27
    //   2374: astore #11
    //   2376: aload_2
    //   2377: astore #34
    //   2379: aload_3
    //   2380: astore #33
    //   2382: new com/google/android/mms/MmsException
    //   2385: astore #4
    //   2387: aload #27
    //   2389: astore #11
    //   2391: aload_2
    //   2392: astore #34
    //   2394: aload_3
    //   2395: astore #33
    //   2397: aload #4
    //   2399: aload_1
    //   2400: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   2403: aload #27
    //   2405: astore #11
    //   2407: aload_2
    //   2408: astore #34
    //   2410: aload_3
    //   2411: astore #33
    //   2413: aload #4
    //   2415: athrow
    //   2416: astore_1
    //   2417: aload #33
    //   2419: astore_3
    //   2420: aload #34
    //   2422: astore_2
    //   2423: aload #11
    //   2425: astore #27
    //   2427: aload #27
    //   2429: ifnull -> 2481
    //   2432: aload #27
    //   2434: invokevirtual close : ()V
    //   2437: goto -> 2481
    //   2440: astore #34
    //   2442: new java/lang/StringBuilder
    //   2445: dup
    //   2446: invokespecial <init> : ()V
    //   2449: astore #4
    //   2451: aload #4
    //   2453: ldc_w 'IOException while closing: '
    //   2456: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2459: pop
    //   2460: aload #4
    //   2462: aload #27
    //   2464: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   2467: pop
    //   2468: ldc 'PduPersister'
    //   2470: aload #4
    //   2472: invokevirtual toString : ()Ljava/lang/String;
    //   2475: aload #34
    //   2477: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   2480: pop
    //   2481: aload_2
    //   2482: ifnull -> 2532
    //   2485: aload_2
    //   2486: invokevirtual close : ()V
    //   2489: goto -> 2532
    //   2492: astore #4
    //   2494: new java/lang/StringBuilder
    //   2497: dup
    //   2498: invokespecial <init> : ()V
    //   2501: astore #27
    //   2503: aload #27
    //   2505: ldc_w 'IOException while closing: '
    //   2508: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2511: pop
    //   2512: aload #27
    //   2514: aload_2
    //   2515: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   2518: pop
    //   2519: ldc 'PduPersister'
    //   2521: aload #27
    //   2523: invokevirtual toString : ()Ljava/lang/String;
    //   2526: aload #4
    //   2528: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   2531: pop
    //   2532: aload_3
    //   2533: ifnull -> 2623
    //   2536: aload_3
    //   2537: aconst_null
    //   2538: invokevirtual close : (Ljava/lang/String;)I
    //   2541: pop
    //   2542: new java/io/File
    //   2545: dup
    //   2546: aconst_null
    //   2547: invokespecial <init> : (Ljava/lang/String;)V
    //   2550: astore #27
    //   2552: new android/content/ContentValues
    //   2555: dup
    //   2556: iconst_0
    //   2557: invokespecial <init> : (I)V
    //   2560: astore #4
    //   2562: aload_0
    //   2563: getfield mContext : Landroid/content/Context;
    //   2566: astore_3
    //   2567: aload_0
    //   2568: getfield mContentResolver : Landroid/content/ContentResolver;
    //   2571: astore_2
    //   2572: new java/lang/StringBuilder
    //   2575: dup
    //   2576: invokespecial <init> : ()V
    //   2579: astore #34
    //   2581: aload #34
    //   2583: ldc_w 'content://mms/resetFilePerm/'
    //   2586: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2589: pop
    //   2590: aload #34
    //   2592: aload #27
    //   2594: invokevirtual getName : ()Ljava/lang/String;
    //   2597: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2600: pop
    //   2601: aload #34
    //   2603: invokevirtual toString : ()Ljava/lang/String;
    //   2606: invokestatic parse : (Ljava/lang/String;)Landroid/net/Uri;
    //   2609: astore #27
    //   2611: aload_3
    //   2612: aload_2
    //   2613: aload #27
    //   2615: aload #4
    //   2617: aconst_null
    //   2618: aconst_null
    //   2619: invokestatic update : (Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   2622: pop
    //   2623: aload_1
    //   2624: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #801	-> 0
    //   #802	-> 27
    //   #803	-> 51
    //   #804	-> 93
    //   #805	-> 93
    //   #808	-> 93
    //   #809	-> 99
    //   #810	-> 109
    //   #811	-> 119
    //   #821	-> 132
    //   #822	-> 138
    //   #823	-> 143
    //   #824	-> 147
    //   #825	-> 153
    //   #826	-> 164
    //   #831	-> 178
    //   #901	-> 220
    //   #903	-> 224
    //   #906	-> 232
    //   #904	-> 232
    //   #905	-> 233
    //   #901	-> 269
    //   #908	-> 269
    //   #910	-> 273
    //   #913	-> 281
    //   #911	-> 281
    //   #912	-> 282
    //   #915	-> 315
    //   #916	-> 319
    //   #829	-> 327
    //   #831	-> 328
    //   #833	-> 366
    //   #824	-> 369
    //   #831	-> 423
    //   #832	-> 430
    //   #823	-> 535
    //   #836	-> 535
    //   #837	-> 557
    //   #838	-> 568
    //   #822	-> 708
    //   #844	-> 711
    //   #845	-> 730
    //   #846	-> 735
    //   #847	-> 762
    //   #853	-> 812
    //   #854	-> 866
    //   #856	-> 910
    //   #857	-> 916
    //   #864	-> 956
    //   #865	-> 972
    //   #866	-> 997
    //   #867	-> 1002
    //   #869	-> 1045
    //   #870	-> 1063
    //   #871	-> 1067
    //   #875	-> 1089
    //   #865	-> 1089
    //   #873	-> 1092
    //   #901	-> 1105
    //   #897	-> 1115
    //   #894	-> 1125
    //   #865	-> 1135
    //   #877	-> 1135
    //   #901	-> 1138
    //   #897	-> 1145
    //   #894	-> 1152
    //   #848	-> 1159
    //   #901	-> 1201
    //   #903	-> 1206
    //   #906	-> 1211
    //   #904	-> 1214
    //   #905	-> 1215
    //   #908	-> 1249
    //   #910	-> 1253
    //   #913	-> 1261
    //   #911	-> 1261
    //   #912	-> 1262
    //   #915	-> 1295
    //   #916	-> 1299
    //   #920	-> 1305
    //   #921	-> 1315
    //   #922	-> 1324
    //   #923	-> 1352
    //   #922	-> 1373
    //   #849	-> 1384
    //   #901	-> 1385
    //   #897	-> 1399
    //   #894	-> 1413
    //   #881	-> 1427
    //   #882	-> 1432
    //   #884	-> 1466
    //   #885	-> 1466
    //   #886	-> 1509
    //   #887	-> 1513
    //   #889	-> 1561
    //   #897	-> 1673
    //   #894	-> 1687
    //   #810	-> 1701
    //   #809	-> 1704
    //   #812	-> 1704
    //   #813	-> 1736
    //   #814	-> 1744
    //   #816	-> 1799
    //   #817	-> 1857
    //   #820	-> 1888
    //   #901	-> 1899
    //   #903	-> 1904
    //   #906	-> 1909
    //   #904	-> 1912
    //   #905	-> 1913
    //   #908	-> 1951
    //   #910	-> 1955
    //   #913	-> 1959
    //   #911	-> 1962
    //   #912	-> 1963
    //   #915	-> 2000
    //   #916	-> 2004
    //   #920	-> 2010
    //   #921	-> 2020
    //   #922	-> 2029
    //   #923	-> 2057
    //   #922	-> 2078
    //   #927	-> 2089
    //   #818	-> 2090
    //   #901	-> 2213
    //   #897	-> 2226
    //   #894	-> 2239
    //   #901	-> 2252
    //   #897	-> 2266
    //   #898	-> 2277
    //   #899	-> 2297
    //   #894	-> 2341
    //   #895	-> 2352
    //   #896	-> 2372
    //   #901	-> 2416
    //   #903	-> 2432
    //   #906	-> 2437
    //   #904	-> 2440
    //   #905	-> 2442
    //   #908	-> 2481
    //   #910	-> 2485
    //   #913	-> 2489
    //   #911	-> 2492
    //   #912	-> 2494
    //   #915	-> 2532
    //   #916	-> 2536
    //   #920	-> 2542
    //   #921	-> 2552
    //   #922	-> 2562
    //   #923	-> 2590
    //   #922	-> 2611
    //   #926	-> 2623
    // Exception table:
    //   from	to	target	type
    //   93	99	2341	java/io/FileNotFoundException
    //   93	99	2266	java/io/IOException
    //   93	99	2252	finally
    //   99	109	2341	java/io/FileNotFoundException
    //   99	109	2266	java/io/IOException
    //   99	109	2252	finally
    //   109	119	2341	java/io/FileNotFoundException
    //   109	119	2266	java/io/IOException
    //   109	119	2252	finally
    //   119	129	2341	java/io/FileNotFoundException
    //   119	129	2266	java/io/IOException
    //   119	129	2252	finally
    //   132	138	2341	java/io/FileNotFoundException
    //   132	138	2266	java/io/IOException
    //   132	138	2252	finally
    //   147	153	428	java/lang/Exception
    //   147	153	2341	java/io/FileNotFoundException
    //   147	153	2266	java/io/IOException
    //   147	153	2252	finally
    //   153	164	428	java/lang/Exception
    //   153	164	2341	java/io/FileNotFoundException
    //   153	164	2266	java/io/IOException
    //   153	164	2252	finally
    //   164	171	369	finally
    //   207	212	215	java/lang/Exception
    //   207	212	1687	java/io/FileNotFoundException
    //   207	212	1673	java/io/IOException
    //   207	212	2416	finally
    //   224	232	232	java/io/IOException
    //   273	281	281	java/io/IOException
    //   353	358	361	java/lang/Exception
    //   353	358	1413	java/io/FileNotFoundException
    //   353	358	1399	java/io/IOException
    //   353	358	1385	finally
    //   376	381	384	finally
    //   398	405	423	java/lang/Exception
    //   398	405	2239	java/io/FileNotFoundException
    //   398	405	2226	java/io/IOException
    //   398	405	2213	finally
    //   420	423	423	java/lang/Exception
    //   420	423	2239	java/io/FileNotFoundException
    //   420	423	2226	java/io/IOException
    //   420	423	2213	finally
    //   442	447	2239	java/io/FileNotFoundException
    //   442	447	2226	java/io/IOException
    //   442	447	2213	finally
    //   459	464	2239	java/io/FileNotFoundException
    //   459	464	2226	java/io/IOException
    //   459	464	2213	finally
    //   476	485	2239	java/io/FileNotFoundException
    //   476	485	2226	java/io/IOException
    //   476	485	2213	finally
    //   497	507	2239	java/io/FileNotFoundException
    //   497	507	2226	java/io/IOException
    //   497	507	2213	finally
    //   519	532	2239	java/io/FileNotFoundException
    //   519	532	2226	java/io/IOException
    //   519	532	2213	finally
    //   547	557	2239	java/io/FileNotFoundException
    //   547	557	2226	java/io/IOException
    //   547	557	2213	finally
    //   580	584	2239	java/io/FileNotFoundException
    //   580	584	2226	java/io/IOException
    //   580	584	2213	finally
    //   596	600	2239	java/io/FileNotFoundException
    //   596	600	2226	java/io/IOException
    //   596	600	2213	finally
    //   612	616	2239	java/io/FileNotFoundException
    //   612	616	2226	java/io/IOException
    //   612	616	2213	finally
    //   628	636	2239	java/io/FileNotFoundException
    //   628	636	2226	java/io/IOException
    //   628	636	2213	finally
    //   648	654	2239	java/io/FileNotFoundException
    //   648	654	2226	java/io/IOException
    //   648	654	2213	finally
    //   666	674	2239	java/io/FileNotFoundException
    //   666	674	2226	java/io/IOException
    //   666	674	2213	finally
    //   686	694	2239	java/io/FileNotFoundException
    //   686	694	2226	java/io/IOException
    //   686	694	2213	finally
    //   706	708	2239	java/io/FileNotFoundException
    //   706	708	2226	java/io/IOException
    //   706	708	2213	finally
    //   720	730	2239	java/io/FileNotFoundException
    //   720	730	2226	java/io/IOException
    //   720	730	2213	finally
    //   756	762	1413	java/io/FileNotFoundException
    //   756	762	1399	java/io/IOException
    //   756	762	1385	finally
    //   800	809	1687	java/io/FileNotFoundException
    //   800	809	1673	java/io/IOException
    //   800	809	2416	finally
    //   856	866	1687	java/io/FileNotFoundException
    //   856	866	1673	java/io/IOException
    //   856	866	2416	finally
    //   899	910	1687	java/io/FileNotFoundException
    //   899	910	1673	java/io/IOException
    //   899	910	2416	finally
    //   946	956	1687	java/io/FileNotFoundException
    //   946	956	1673	java/io/IOException
    //   946	956	2416	finally
    //   965	972	1152	java/io/FileNotFoundException
    //   965	972	1145	java/io/IOException
    //   965	972	1138	finally
    //   983	991	1152	java/io/FileNotFoundException
    //   983	991	1145	java/io/IOException
    //   983	991	1138	finally
    //   1032	1042	1687	java/io/FileNotFoundException
    //   1032	1042	1673	java/io/IOException
    //   1032	1042	2416	finally
    //   1054	1063	1152	java/io/FileNotFoundException
    //   1054	1063	1145	java/io/IOException
    //   1054	1063	1138	finally
    //   1076	1080	1152	java/io/FileNotFoundException
    //   1076	1080	1145	java/io/IOException
    //   1076	1080	1138	finally
    //   1080	1089	1125	java/io/FileNotFoundException
    //   1080	1089	1115	java/io/IOException
    //   1080	1089	1105	finally
    //   1092	1105	1125	java/io/FileNotFoundException
    //   1092	1105	1115	java/io/IOException
    //   1092	1105	1105	finally
    //   1192	1201	1687	java/io/FileNotFoundException
    //   1192	1201	1673	java/io/IOException
    //   1192	1201	2416	finally
    //   1206	1211	1214	java/io/IOException
    //   1253	1261	1261	java/io/IOException
    //   1453	1460	1413	java/io/FileNotFoundException
    //   1453	1460	1399	java/io/IOException
    //   1453	1460	1385	finally
    //   1499	1509	1687	java/io/FileNotFoundException
    //   1499	1509	1673	java/io/IOException
    //   1499	1509	2416	finally
    //   1546	1555	1687	java/io/FileNotFoundException
    //   1546	1555	1673	java/io/IOException
    //   1546	1555	2416	finally
    //   1594	1598	1687	java/io/FileNotFoundException
    //   1594	1598	1673	java/io/IOException
    //   1594	1598	2416	finally
    //   1631	1638	1687	java/io/FileNotFoundException
    //   1631	1638	1673	java/io/IOException
    //   1631	1638	2416	finally
    //   1671	1673	1687	java/io/FileNotFoundException
    //   1671	1673	1673	java/io/IOException
    //   1671	1673	2416	finally
    //   1716	1720	2239	java/io/FileNotFoundException
    //   1716	1720	2226	java/io/IOException
    //   1716	1720	2213	finally
    //   1732	1736	2239	java/io/FileNotFoundException
    //   1732	1736	2226	java/io/IOException
    //   1732	1736	2213	finally
    //   1756	1760	2239	java/io/FileNotFoundException
    //   1756	1760	2226	java/io/IOException
    //   1756	1760	2213	finally
    //   1772	1779	2239	java/io/FileNotFoundException
    //   1772	1779	2226	java/io/IOException
    //   1772	1779	2213	finally
    //   1791	1799	2239	java/io/FileNotFoundException
    //   1791	1799	2226	java/io/IOException
    //   1791	1799	2213	finally
    //   1811	1816	2239	java/io/FileNotFoundException
    //   1811	1816	2226	java/io/IOException
    //   1811	1816	2213	finally
    //   1828	1834	2239	java/io/FileNotFoundException
    //   1828	1834	2226	java/io/IOException
    //   1828	1834	2213	finally
    //   1846	1857	2239	java/io/FileNotFoundException
    //   1846	1857	2226	java/io/IOException
    //   1846	1857	2213	finally
    //   1869	1882	2239	java/io/FileNotFoundException
    //   1869	1882	2226	java/io/IOException
    //   1869	1882	2213	finally
    //   1904	1909	1912	java/io/IOException
    //   1955	1959	1962	java/io/IOException
    //   2102	2106	2239	java/io/FileNotFoundException
    //   2102	2106	2226	java/io/IOException
    //   2102	2106	2213	finally
    //   2118	2122	2239	java/io/FileNotFoundException
    //   2118	2122	2226	java/io/IOException
    //   2118	2122	2213	finally
    //   2134	2138	2239	java/io/FileNotFoundException
    //   2134	2138	2226	java/io/IOException
    //   2134	2138	2213	finally
    //   2150	2158	2239	java/io/FileNotFoundException
    //   2150	2158	2226	java/io/IOException
    //   2150	2158	2213	finally
    //   2170	2179	2239	java/io/FileNotFoundException
    //   2170	2179	2226	java/io/IOException
    //   2170	2179	2213	finally
    //   2191	2199	2239	java/io/FileNotFoundException
    //   2191	2199	2226	java/io/IOException
    //   2191	2199	2213	finally
    //   2211	2213	2239	java/io/FileNotFoundException
    //   2211	2213	2226	java/io/IOException
    //   2211	2213	2213	finally
    //   2287	2297	2416	finally
    //   2307	2312	2416	finally
    //   2322	2328	2416	finally
    //   2338	2341	2416	finally
    //   2362	2372	2416	finally
    //   2382	2387	2416	finally
    //   2397	2403	2416	finally
    //   2413	2416	2416	finally
    //   2432	2437	2440	java/io/IOException
    //   2485	2489	2492	java/io/IOException
  }
  
  private void updateAddress(long paramLong, int paramInt, EncodedStringValue[] paramArrayOfEncodedStringValue) {
    Context context = this.mContext;
    ContentResolver contentResolver = this.mContentResolver;
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("content://mms/");
    stringBuilder1.append(paramLong);
    stringBuilder1.append("/addr");
    String str1 = stringBuilder1.toString();
    Uri uri = Uri.parse(str1);
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("type=");
    stringBuilder2.append(paramInt);
    String str2 = stringBuilder2.toString();
    SqliteWrapper.delete(context, contentResolver, uri, str2, null);
    persistAddress(paramLong, paramInt, paramArrayOfEncodedStringValue);
  }
  
  public void updateHeaders(Uri paramUri, SendReq paramSendReq) {
    synchronized (PDU_CACHE_INSTANCE) {
      boolean bool = PDU_CACHE_INSTANCE.isUpdating(paramUri);
      if (bool)
        try {
          PDU_CACHE_INSTANCE.wait();
        } catch (InterruptedException interruptedException) {
          Log.e("PduPersister", "updateHeaders: ", interruptedException);
        }  
      PDU_CACHE_INSTANCE.purge(paramUri);
      ContentValues contentValues = new ContentValues(10);
      byte[] arrayOfByte = paramSendReq.getContentType();
      if (arrayOfByte != null)
        contentValues.put("ct_t", toIsoString(arrayOfByte)); 
      long l = paramSendReq.getDate();
      if (l != -1L)
        contentValues.put("date", Long.valueOf(l)); 
      null = paramSendReq.getDeliveryReport();
      if (null != 0)
        contentValues.put("d_rpt", Integer.valueOf(null)); 
      l = paramSendReq.getExpiry();
      if (l != -1L)
        contentValues.put("exp", Long.valueOf(l)); 
      arrayOfByte = paramSendReq.getMessageClass();
      if (arrayOfByte != null)
        contentValues.put("m_cls", toIsoString(arrayOfByte)); 
      null = paramSendReq.getPriority();
      if (null != 0)
        contentValues.put("pri", Integer.valueOf(null)); 
      null = paramSendReq.getReadReport();
      if (null != 0)
        contentValues.put("rr", Integer.valueOf(null)); 
      arrayOfByte = paramSendReq.getTransactionId();
      if (arrayOfByte != null)
        contentValues.put("tr_id", toIsoString(arrayOfByte)); 
      EncodedStringValue encodedStringValue = paramSendReq.getSubject();
      if (encodedStringValue != null) {
        contentValues.put("sub", toIsoString(encodedStringValue.getTextString()));
        contentValues.put("sub_cs", Integer.valueOf(encodedStringValue.getCharacterSet()));
      } else {
        contentValues.put("sub", "");
      } 
      l = paramSendReq.getMessageSize();
      if (l > 0L)
        contentValues.put("m_size", Long.valueOf(l)); 
      PduHeaders pduHeaders = paramSendReq.getPduHeaders();
      HashSet<String> hashSet = new HashSet();
      for (int i : ADDRESS_FIELDS) {
        EncodedStringValue[] arrayOfEncodedStringValue;
        paramSendReq = null;
        if (i == 137) {
          EncodedStringValue encodedStringValue1 = pduHeaders.getEncodedStringValue(i);
          if (encodedStringValue1 != null)
            arrayOfEncodedStringValue = new EncodedStringValue[] { encodedStringValue1 }; 
        } else {
          arrayOfEncodedStringValue = pduHeaders.getEncodedStringValues(i);
        } 
        if (arrayOfEncodedStringValue != null) {
          l = ContentUris.parseId(paramUri);
          updateAddress(l, i, arrayOfEncodedStringValue);
          if (i == 151)
            for (int j = arrayOfEncodedStringValue.length; i < j; ) {
              EncodedStringValue encodedStringValue1 = arrayOfEncodedStringValue[i];
              if (encodedStringValue1 != null)
                hashSet.add(encodedStringValue1.getString()); 
              i++;
            }  
        } 
      } 
      if (!hashSet.isEmpty()) {
        l = Telephony.Threads.getOrCreateThreadId(this.mContext, hashSet);
        contentValues.put("thread_id", Long.valueOf(l));
      } 
      SqliteWrapper.update(this.mContext, this.mContentResolver, paramUri, contentValues, null, null);
      return;
    } 
  }
  
  private void updatePart(Uri paramUri, PduPart paramPduPart, HashMap<Uri, InputStream> paramHashMap) throws MmsException {
    ContentValues contentValues = new ContentValues(7);
    int i = paramPduPart.getCharset();
    if (i != 0)
      contentValues.put("chset", Integer.valueOf(i)); 
    if (paramPduPart.getContentType() != null) {
      String str1 = toIsoString(paramPduPart.getContentType());
      contentValues.put("ct", str1);
      if (paramPduPart.getFilename() != null) {
        String str = new String(paramPduPart.getFilename());
        contentValues.put("fn", str);
      } 
      if (paramPduPart.getName() != null) {
        String str = new String(paramPduPart.getName());
        contentValues.put("name", str);
      } 
      String str2 = null;
      if (paramPduPart.getContentDisposition() != null) {
        str2 = toIsoString(paramPduPart.getContentDisposition());
        contentValues.put("cd", str2);
      } 
      if (paramPduPart.getContentId() != null) {
        str2 = toIsoString(paramPduPart.getContentId());
        contentValues.put("cid", str2);
      } 
      if (paramPduPart.getContentLocation() != null) {
        str2 = toIsoString(paramPduPart.getContentLocation());
        contentValues.put("cl", str2);
      } 
      SqliteWrapper.update(this.mContext, this.mContentResolver, paramUri, contentValues, null, null);
      if (paramPduPart.getData() != null || 
        !paramUri.equals(paramPduPart.getDataUri()))
        persistData(paramPduPart, paramUri, str1, paramHashMap); 
      return;
    } 
    throw new MmsException("MIME type of the part must be set.");
  }
  
  public void updateParts(Uri paramUri, PduBody paramPduBody, HashMap<Uri, InputStream> paramHashMap) throws MmsException {
    try {
      PduCache pduCache;
    } finally {
      paramHashMap = null;
    } 
  }
  
  public Uri persist(GenericPdu paramGenericPdu, Uri paramUri, boolean paramBoolean1, boolean paramBoolean2, HashMap<Uri, InputStream> paramHashMap) throws MmsException {
    if (paramUri != null) {
      long l;
      int i;
      try {
        l = ContentUris.parseId(paramUri);
      } catch (NumberFormatException numberFormatException) {
        l = -1L;
      } 
      if (l != -1L) {
        i = 1;
      } else {
        i = 0;
      } 
      if (i || MESSAGE_BOX_MAP.get(paramUri) != null) {
        PduCache pduCache;
        ContentResolver contentResolver;
        synchronized (PDU_CACHE_INSTANCE) {
          Uri uri1;
          boolean bool = PDU_CACHE_INSTANCE.isUpdating(paramUri);
          if (bool)
            try {
              PDU_CACHE_INSTANCE.wait();
            } catch (InterruptedException interruptedException) {
              Log.e("PduPersister", "persist1: ", interruptedException);
            }  
          PDU_CACHE_INSTANCE.purge(paramUri);
          PduHeaders pduHeaders = paramGenericPdu.getPduHeaders();
          ContentValues contentValues2 = new ContentValues();
          Set<Map.Entry<Integer, String>> set = ENCODED_STRING_COLUMN_NAME_MAP.entrySet();
          for (Map.Entry<Integer, String> entry : set) {
            int k = ((Integer)entry.getKey()).intValue();
            EncodedStringValue encodedStringValue = pduHeaders.getEncodedStringValue(k);
            if (encodedStringValue != null) {
              String str1 = CHARSET_COLUMN_NAME_MAP.get(Integer.valueOf(k));
              contentValues2.put((String)entry.getValue(), toIsoString(encodedStringValue.getTextString()));
              contentValues2.put(str1, Integer.valueOf(encodedStringValue.getCharacterSet()));
            } 
          } 
          set = TEXT_STRING_COLUMN_NAME_MAP.entrySet();
          for (Map.Entry<Integer, String> entry : set) {
            byte[] arrayOfByte = pduHeaders.getTextString(((Integer)entry.getKey()).intValue());
            if (arrayOfByte != null)
              contentValues2.put((String)entry.getValue(), toIsoString(arrayOfByte)); 
          } 
          set = OCTET_COLUMN_NAME_MAP.entrySet();
          for (Map.Entry<Integer, String> entry : set) {
            int k = pduHeaders.getOctet(((Integer)entry.getKey()).intValue());
            if (k != 0)
              contentValues2.put((String)entry.getValue(), Integer.valueOf(k)); 
          } 
          set = LONG_COLUMN_NAME_MAP.entrySet();
          for (Map.Entry<Integer, String> entry : set) {
            long l2 = pduHeaders.getLongInteger(((Integer)entry.getKey()).intValue());
            if (l2 != -1L)
              contentValues2.put((String)entry.getValue(), Long.valueOf(l2)); 
          } 
          HashMap<Object, Object> hashMap = new HashMap<>(ADDRESS_FIELDS.length);
          for (int k : ADDRESS_FIELDS) {
            EncodedStringValue[] arrayOfEncodedStringValue;
            set = null;
            if (k == 137) {
              EncodedStringValue encodedStringValue = pduHeaders.getEncodedStringValue(k);
              if (encodedStringValue != null)
                arrayOfEncodedStringValue = new EncodedStringValue[] { encodedStringValue }; 
            } else {
              arrayOfEncodedStringValue = pduHeaders.getEncodedStringValues(k);
            } 
            hashMap.put(Integer.valueOf(k), arrayOfEncodedStringValue);
          } 
          set = new HashSet<>();
          int j = paramGenericPdu.getMessageType();
          if (j == 130 || j == 132 || j == 128) {
            if (j != 128) {
              if (j == 130 || j == 132) {
                loadRecipients(137, (HashSet)set, (HashMap)hashMap, false);
                if (paramBoolean2) {
                  loadRecipients(151, (HashSet)set, (HashMap)hashMap, true);
                  loadRecipients(130, (HashSet)set, (HashMap)hashMap, true);
                } 
              } 
            } else {
              loadRecipients(151, (HashSet)set, (HashMap)hashMap, false);
            } 
            long l3 = 0L;
            long l2 = l3;
            if (paramBoolean1) {
              l2 = l3;
              if (!set.isEmpty())
                l2 = Telephony.Threads.getOrCreateThreadId(this.mContext, set); 
            } 
            contentValues2.put("thread_id", Long.valueOf(l2));
          } 
          long l1 = System.currentTimeMillis();
          null = 0;
          if (paramGenericPdu instanceof MultimediaMessagePdu) {
            PduBody pduBody = ((MultimediaMessagePdu)paramGenericPdu).getBody();
            if (pduBody != null) {
              int k = pduBody.getPartsNum();
              if (k > 2) {
                j = 0;
              } else {
                j = 1;
              } 
              for (byte b = 0; b < k; b++, j = m) {
                PduPart pduPart = pduBody.getPart(b);
                null += pduPart.getDataLength();
                persistPart(pduPart, l1, paramHashMap);
                String str1 = getPartContentType(pduPart);
                int m = j;
                if (str1 != null) {
                  m = j;
                  if (!"application/smil".equals(str1)) {
                    m = j;
                    if (!"text/plain".equals(str1))
                      m = 0; 
                  } 
                } 
              } 
            } else {
              j = 1;
              null = 0;
            } 
          } else {
            paramGenericPdu = null;
            j = 1;
            null = 0;
          } 
          contentValues2.put("text_only", Integer.valueOf(j));
          if (contentValues2.getAsInteger("m_size") == null)
            contentValues2.put("m_size", Integer.valueOf(null)); 
          if (i) {
            SqliteWrapper.update(this.mContext, this.mContentResolver, paramUri, contentValues2, null, null);
            uri1 = paramUri;
          } else {
            uri1 = SqliteWrapper.insert(this.mContext, this.mContentResolver, paramUri, contentValues2);
            if (uri1 != null) {
              l = ContentUris.parseId(uri1);
            } else {
              throw new MmsException("persist() failed: return null.");
            } 
          } 
          ContentValues contentValues1 = new ContentValues(1);
          contentValues1.put("mid", Long.valueOf(l));
          Context context = this.mContext;
          contentResolver = this.mContentResolver;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("content://mms/");
          stringBuilder.append(l1);
          stringBuilder.append("/part");
          String str = stringBuilder.toString();
          Uri uri2 = Uri.parse(str);
          SqliteWrapper.update(context, contentResolver, uri2, contentValues1, null, null);
          if (!i) {
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append(paramUri);
            stringBuilder1.append("/");
            stringBuilder1.append(l);
            uri1 = Uri.parse(stringBuilder1.toString());
          } 
          for (int k : ADDRESS_FIELDS) {
            EncodedStringValue[] arrayOfEncodedStringValue = (EncodedStringValue[])hashMap.get(Integer.valueOf(k));
            if (arrayOfEncodedStringValue != null)
              persistAddress(l, k, arrayOfEncodedStringValue); 
          } 
          return uri1;
        } 
      } 
      throw new MmsException("Bad destination, must be one of content://mms/inbox, content://mms/sent, content://mms/drafts, content://mms/outbox, content://mms/temp.");
    } 
    throw new MmsException("Uri may not be null.");
  }
  
  private void loadRecipients(int paramInt, HashSet<String> paramHashSet, HashMap<Integer, EncodedStringValue[]> paramHashMap, boolean paramBoolean) {
    EncodedStringValue[] arrayOfEncodedStringValue = paramHashMap.get(Integer.valueOf(paramInt));
    if (arrayOfEncodedStringValue == null)
      return; 
    if (paramBoolean && arrayOfEncodedStringValue.length == 1)
      return; 
    SubscriptionManager subscriptionManager = SubscriptionManager.from(this.mContext);
    HashSet<String> hashSet = new HashSet();
    if (paramBoolean)
      for (SubscriptionInfo subscriptionInfo : subscriptionManager.getActiveSubscriptionInfoList()) {
        TelephonyManager telephonyManager = (TelephonyManager)this.mContext.getSystemService(TelephonyManager.class);
        String str = telephonyManager.createForSubscriptionId(subscriptionInfo.getSubscriptionId()).getLine1Number();
        if (str != null)
          hashSet.add(str); 
      }  
    for (int i = arrayOfEncodedStringValue.length; paramInt < i; ) {
      EncodedStringValue encodedStringValue = arrayOfEncodedStringValue[paramInt];
      if (encodedStringValue != null) {
        String str = encodedStringValue.getString();
        if (paramBoolean) {
          for (String str1 : hashSet) {
            if (!PhoneNumberUtils.compare(str, str1) && 
              !paramHashSet.contains(str)) {
              paramHashSet.add(str);
              break;
            } 
          } 
        } else if (!paramHashSet.contains(str)) {
          paramHashSet.add(str);
        } 
      } 
      paramInt++;
    } 
  }
  
  public Uri move(Uri paramUri1, Uri paramUri2) throws MmsException {
    long l = ContentUris.parseId(paramUri1);
    if (l != -1L) {
      Integer integer = MESSAGE_BOX_MAP.get(paramUri2);
      if (integer != null) {
        ContentValues contentValues = new ContentValues(1);
        contentValues.put("msg_box", integer);
        SqliteWrapper.update(this.mContext, this.mContentResolver, paramUri1, contentValues, null, null);
        return ContentUris.withAppendedId(paramUri2, l);
      } 
      throw new MmsException("Bad destination, must be one of content://mms/inbox, content://mms/sent, content://mms/drafts, content://mms/outbox, content://mms/temp.");
    } 
    throw new MmsException("Error! ID of the message: -1.");
  }
  
  public static String toIsoString(byte[] paramArrayOfbyte) {
    try {
      return new String(paramArrayOfbyte, "iso-8859-1");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      Log.e("PduPersister", "ISO_8859_1 must be supported!", unsupportedEncodingException);
      return "";
    } 
  }
  
  public static byte[] getBytes(String paramString) {
    try {
      return paramString.getBytes("iso-8859-1");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      Log.e("PduPersister", "ISO_8859_1 must be supported!", unsupportedEncodingException);
      return new byte[0];
    } 
  }
  
  public void release() {
    Uri uri = Uri.parse("content://mms/9223372036854775807/part");
    SqliteWrapper.delete(this.mContext, this.mContentResolver, uri, null, null);
    this.mDrmManagerClient.release();
  }
  
  public Cursor getPendingMessages(long paramLong) {
    Uri.Builder builder = Telephony.MmsSms.PendingMessages.CONTENT_URI.buildUpon();
    builder.appendQueryParameter("protocol", "mms");
    Context context = this.mContext;
    ContentResolver contentResolver = this.mContentResolver;
    Uri uri = builder.build();
    return SqliteWrapper.query(context, contentResolver, uri, null, "err_type < ? AND due_time <= ?", new String[] { String.valueOf(10), String.valueOf(paramLong) }, "due_time");
  }
}
