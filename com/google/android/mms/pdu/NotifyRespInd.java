package com.google.android.mms.pdu;

import com.google.android.mms.InvalidHeaderValueException;

public class NotifyRespInd extends GenericPdu {
  public NotifyRespInd(int paramInt1, byte[] paramArrayOfbyte, int paramInt2) throws InvalidHeaderValueException {
    setMessageType(131);
    setMmsVersion(paramInt1);
    setTransactionId(paramArrayOfbyte);
    setStatus(paramInt2);
  }
  
  NotifyRespInd(PduHeaders paramPduHeaders) {
    super(paramPduHeaders);
  }
  
  public int getReportAllowed() {
    return this.mPduHeaders.getOctet(145);
  }
  
  public void setReportAllowed(int paramInt) throws InvalidHeaderValueException {
    this.mPduHeaders.setOctet(paramInt, 145);
  }
  
  public void setStatus(int paramInt) throws InvalidHeaderValueException {
    this.mPduHeaders.setOctet(paramInt, 149);
  }
  
  public int getStatus() {
    return this.mPduHeaders.getOctet(149);
  }
  
  public byte[] getTransactionId() {
    return this.mPduHeaders.getTextString(152);
  }
  
  public void setTransactionId(byte[] paramArrayOfbyte) {
    this.mPduHeaders.setTextString(paramArrayOfbyte, 152);
  }
}
