package com.google.android.mms.pdu;

import android.content.ContentResolver;
import android.content.Context;
import android.text.TextUtils;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

public class PduComposer {
  protected ByteArrayOutputStream mMessage = null;
  
  private GenericPdu mPdu = null;
  
  protected int mPosition = 0;
  
  private BufferStack mStack = null;
  
  private PduHeaders mPduHeader = null;
  
  private static HashMap<String, Integer> mContentTypeMap = null;
  
  static final boolean $assertionsDisabled = false;
  
  private static final int END_STRING_FLAG = 0;
  
  private static final int LENGTH_QUOTE = 31;
  
  private static final int LONG_INTEGER_LENGTH_MAX = 8;
  
  private static final int PDU_COMPOSER_BLOCK_SIZE = 1024;
  
  private static final int PDU_COMPOSE_CONTENT_ERROR = 1;
  
  private static final int PDU_COMPOSE_FIELD_NOT_SET = 2;
  
  private static final int PDU_COMPOSE_FIELD_NOT_SUPPORTED = 3;
  
  private static final int PDU_COMPOSE_SUCCESS = 0;
  
  private static final int PDU_EMAIL_ADDRESS_TYPE = 2;
  
  private static final int PDU_IPV4_ADDRESS_TYPE = 3;
  
  private static final int PDU_IPV6_ADDRESS_TYPE = 4;
  
  private static final int PDU_PHONE_NUMBER_ADDRESS_TYPE = 1;
  
  private static final int PDU_UNKNOWN_ADDRESS_TYPE = 5;
  
  private static final int QUOTED_STRING_FLAG = 34;
  
  static final String REGEXP_EMAIL_ADDRESS_TYPE = "[a-zA-Z| ]*\\<{0,1}[a-zA-Z| ]+@{1}[a-zA-Z| ]+\\.{1}[a-zA-Z| ]+\\>{0,1}";
  
  static final String REGEXP_IPV4_ADDRESS_TYPE = "[0-9]{1,3}\\.{1}[0-9]{1,3}\\.{1}[0-9]{1,3}\\.{1}[0-9]{1,3}";
  
  static final String REGEXP_IPV6_ADDRESS_TYPE = "[a-fA-F]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}";
  
  static final String REGEXP_PHONE_NUMBER_ADDRESS_TYPE = "\\+?[0-9|\\.|\\-]+";
  
  private static final int SHORT_INTEGER_MAX = 127;
  
  static final String STRING_IPV4_ADDRESS_TYPE = "/TYPE=IPV4";
  
  static final String STRING_IPV6_ADDRESS_TYPE = "/TYPE=IPV6";
  
  static final String STRING_PHONE_NUMBER_ADDRESS_TYPE = "/TYPE=PLMN";
  
  private static final int TEXT_MAX = 127;
  
  private final ContentResolver mResolver;
  
  static {
    mContentTypeMap = new HashMap<>();
    for (byte b = 0; b < PduContentTypes.contentTypes.length; b++)
      mContentTypeMap.put(PduContentTypes.contentTypes[b], Integer.valueOf(b)); 
  }
  
  public PduComposer(Context paramContext, GenericPdu paramGenericPdu) {
    this.mPdu = paramGenericPdu;
    this.mResolver = paramContext.getContentResolver();
    this.mPduHeader = paramGenericPdu.getPduHeaders();
    this.mStack = new BufferStack();
    this.mMessage = new ByteArrayOutputStream();
    this.mPosition = 0;
  }
  
  public byte[] make() {
    int i = this.mPdu.getMessageType();
    if (i != 128)
      if (i != 135) {
        switch (i) {
          default:
            return null;
          case 133:
            if (makeAckInd() != 0)
              return null; 
            return this.mMessage.toByteArray();
          case 131:
            if (makeNotifyResp() != 0)
              return null; 
            return this.mMessage.toByteArray();
          case 132:
            break;
        } 
      } else {
        if (makeReadRecInd() != 0)
          return null; 
        return this.mMessage.toByteArray();
      }  
    if (makeSendRetrievePdu(i) != 0)
      return null; 
    return this.mMessage.toByteArray();
  }
  
  protected void arraycopy(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    this.mMessage.write(paramArrayOfbyte, paramInt1, paramInt2);
    this.mPosition += paramInt2;
  }
  
  protected void append(int paramInt) {
    this.mMessage.write(paramInt);
    this.mPosition++;
  }
  
  protected void appendShortInteger(int paramInt) {
    append((paramInt | 0x80) & 0xFF);
  }
  
  protected void appendOctet(int paramInt) {
    append(paramInt);
  }
  
  protected void appendShortLength(int paramInt) {
    append(paramInt);
  }
  
  protected void appendLongInteger(long paramLong) {
    long l = paramLong;
    byte b1;
    for (b1 = 0; l != 0L && b1 < 8; b1++)
      l >>>= 8L; 
    appendShortLength(b1);
    int i = (b1 - 1) * 8;
    for (byte b2 = 0; b2 < b1; b2++) {
      append((int)(paramLong >>> i & 0xFFL));
      i -= 8;
    } 
  }
  
  protected void appendTextString(byte[] paramArrayOfbyte) {
    if ((paramArrayOfbyte[0] & 0xFF) > 127)
      append(127); 
    arraycopy(paramArrayOfbyte, 0, paramArrayOfbyte.length);
    append(0);
  }
  
  protected void appendTextString(String paramString) {
    appendTextString(paramString.getBytes());
  }
  
  protected void appendEncodedString(EncodedStringValue paramEncodedStringValue) {
    int i = paramEncodedStringValue.getCharacterSet();
    byte[] arrayOfByte = paramEncodedStringValue.getTextString();
    if (arrayOfByte == null)
      return; 
    this.mStack.newbuf();
    PositionMarker positionMarker = this.mStack.mark();
    appendShortInteger(i);
    appendTextString(arrayOfByte);
    i = positionMarker.getLength();
    this.mStack.pop();
    appendValueLength(i);
    this.mStack.copy();
  }
  
  protected void appendUintvarInteger(long paramLong) {
    byte b2;
    long l = 127L;
    byte b1 = 0;
    while (true) {
      b2 = b1;
      if (b1 < 5) {
        if (paramLong < l) {
          b2 = b1;
          break;
        } 
        l = l << 7L | 0x7FL;
        b1++;
        continue;
      } 
      break;
    } 
    while (b2 > 0) {
      append((int)((0x80L | paramLong >>> b2 * 7 & 0x7FL) & 0xFFL));
      b2--;
    } 
    append((int)(paramLong & 0x7FL));
  }
  
  protected void appendDateValue(long paramLong) {
    appendLongInteger(paramLong);
  }
  
  protected void appendValueLength(long paramLong) {
    if (paramLong < 31L) {
      appendShortLength((int)paramLong);
      return;
    } 
    append(31);
    appendUintvarInteger(paramLong);
  }
  
  protected void appendQuotedString(byte[] paramArrayOfbyte) {
    append(34);
    arraycopy(paramArrayOfbyte, 0, paramArrayOfbyte.length);
    append(0);
  }
  
  protected void appendQuotedString(String paramString) {
    appendQuotedString(paramString.getBytes());
  }
  
  private EncodedStringValue appendAddressType(EncodedStringValue paramEncodedStringValue) {
    try {
      int i = checkAddressType(paramEncodedStringValue.getString());
      paramEncodedStringValue = EncodedStringValue.copy(paramEncodedStringValue);
      if (1 == i) {
        paramEncodedStringValue.appendTextString("/TYPE=PLMN".getBytes());
      } else if (3 == i) {
        paramEncodedStringValue.appendTextString("/TYPE=IPV4".getBytes());
      } else if (4 == i) {
        paramEncodedStringValue.appendTextString("/TYPE=IPV6".getBytes());
      } 
      return paramEncodedStringValue;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  private int appendHeader(int paramInt) {
    PduHeaders pduHeaders;
    EncodedStringValue encodedStringValue1;
    byte[] arrayOfByte1;
    PositionMarker positionMarker;
    byte[] arrayOfByte2;
    EncodedStringValue encodedStringValue2;
    long l;
    switch (paramInt) {
      default:
        return 3;
      case 150:
      case 154:
        pduHeaders = this.mPduHeader;
        encodedStringValue1 = pduHeaders.getEncodedStringValue(paramInt);
        if (encodedStringValue1 == null)
          return 2; 
        appendOctet(paramInt);
        appendEncodedString(encodedStringValue1);
        return 0;
      case 141:
        appendOctet(paramInt);
        paramInt = this.mPduHeader.getOctet(paramInt);
        if (paramInt == 0) {
          appendShortInteger(18);
        } else {
          appendShortInteger(paramInt);
        } 
        return 0;
      case 139:
      case 152:
        arrayOfByte1 = this.mPduHeader.getTextString(paramInt);
        if (arrayOfByte1 == null)
          return 2; 
        appendOctet(paramInt);
        appendTextString(arrayOfByte1);
        return 0;
      case 138:
        arrayOfByte1 = this.mPduHeader.getTextString(paramInt);
        if (arrayOfByte1 == null)
          return 2; 
        appendOctet(paramInt);
        arrayOfByte2 = "advertisement".getBytes();
        if (Arrays.equals(arrayOfByte1, arrayOfByte2)) {
          appendOctet(129);
        } else {
          arrayOfByte2 = "auto".getBytes();
          if (Arrays.equals(arrayOfByte1, arrayOfByte2)) {
            appendOctet(131);
          } else {
            arrayOfByte2 = "personal".getBytes();
            if (Arrays.equals(arrayOfByte1, arrayOfByte2)) {
              appendOctet(128);
            } else {
              arrayOfByte2 = "informational".getBytes();
              if (Arrays.equals(arrayOfByte1, arrayOfByte2)) {
                appendOctet(130);
              } else {
                appendTextString(arrayOfByte1);
              } 
            } 
          } 
        } 
        return 0;
      case 137:
        appendOctet(paramInt);
        encodedStringValue2 = this.mPduHeader.getEncodedStringValue(paramInt);
        if (encodedStringValue2 == null || TextUtils.isEmpty(encodedStringValue2.getString()) || (new String(encodedStringValue2.getTextString())).equals("insert-address-token")) {
          append(1);
          append(129);
          return 0;
        } 
        this.mStack.newbuf();
        positionMarker = this.mStack.mark();
        append(128);
        encodedStringValue2 = appendAddressType(encodedStringValue2);
        if (encodedStringValue2 == null)
          return 1; 
        appendEncodedString(encodedStringValue2);
        paramInt = positionMarker.getLength();
        this.mStack.pop();
        appendValueLength(paramInt);
        this.mStack.copy();
        return 0;
      case 136:
        l = this.mPduHeader.getLongInteger(paramInt);
        if (-1L == l)
          return 2; 
        appendOctet(paramInt);
        this.mStack.newbuf();
        positionMarker = this.mStack.mark();
        append(129);
        appendLongInteger(l);
        paramInt = positionMarker.getLength();
        this.mStack.pop();
        appendValueLength(paramInt);
        this.mStack.copy();
        return 0;
      case 134:
      case 143:
      case 144:
      case 145:
      case 149:
      case 153:
      case 155:
        i = this.mPduHeader.getOctet(paramInt);
        if (i == 0)
          return 2; 
        appendOctet(paramInt);
        appendOctet(i);
        return 0;
      case 133:
        l = this.mPduHeader.getLongInteger(paramInt);
        if (-1L == l)
          return 2; 
        appendOctet(paramInt);
        appendDateValue(l);
        return 0;
      case 129:
      case 130:
      case 151:
        break;
    } 
    EncodedStringValue[] arrayOfEncodedStringValue = this.mPduHeader.getEncodedStringValues(paramInt);
    if (arrayOfEncodedStringValue == null)
      return 2; 
    for (int i = 0; i < arrayOfEncodedStringValue.length; i++) {
      EncodedStringValue encodedStringValue = appendAddressType(arrayOfEncodedStringValue[i]);
      if (encodedStringValue == null)
        return 1; 
      appendOctet(paramInt);
      appendEncodedString(encodedStringValue);
    } 
    return 0;
  }
  
  private int makeReadRecInd() {
    if (this.mMessage == null) {
      this.mMessage = new ByteArrayOutputStream();
      this.mPosition = 0;
    } 
    appendOctet(140);
    appendOctet(135);
    if (appendHeader(141) != 0)
      return 1; 
    if (appendHeader(139) != 0)
      return 1; 
    if (appendHeader(151) != 0)
      return 1; 
    if (appendHeader(137) != 0)
      return 1; 
    appendHeader(133);
    if (appendHeader(155) != 0)
      return 1; 
    return 0;
  }
  
  private int makeNotifyResp() {
    if (this.mMessage == null) {
      this.mMessage = new ByteArrayOutputStream();
      this.mPosition = 0;
    } 
    appendOctet(140);
    appendOctet(131);
    if (appendHeader(152) != 0)
      return 1; 
    if (appendHeader(141) != 0)
      return 1; 
    if (appendHeader(149) != 0)
      return 1; 
    return 0;
  }
  
  private int makeAckInd() {
    if (this.mMessage == null) {
      this.mMessage = new ByteArrayOutputStream();
      this.mPosition = 0;
    } 
    appendOctet(140);
    appendOctet(133);
    if (appendHeader(152) != 0)
      return 1; 
    if (appendHeader(141) != 0)
      return 1; 
    appendHeader(145);
    return 0;
  }
  
  private int makeSendRetrievePdu(int paramInt) {
    if (this.mMessage == null) {
      this.mMessage = new ByteArrayOutputStream();
      this.mPosition = 0;
    } 
    appendOctet(140);
    appendOctet(paramInt);
    appendOctet(152);
    byte[] arrayOfByte = this.mPduHeader.getTextString(152);
    if (arrayOfByte != null) {
      appendTextString(arrayOfByte);
      if (appendHeader(141) != 0)
        return 1; 
      appendHeader(133);
      if (appendHeader(137) != 0)
        return 1; 
      boolean bool = false;
      if (appendHeader(151) != 1)
        bool = true; 
      if (appendHeader(130) != 1)
        bool = true; 
      if (appendHeader(129) != 1)
        bool = true; 
      if (!bool)
        return 1; 
      appendHeader(150);
      appendHeader(138);
      appendHeader(136);
      appendHeader(143);
      appendHeader(134);
      appendHeader(144);
      if (paramInt == 132) {
        appendHeader(153);
        appendHeader(154);
      } 
      appendOctet(132);
      return makeMessageBody(paramInt);
    } 
    throw new IllegalArgumentException("Transaction-ID is null.");
  }
  
  private int makeMessageBody(int paramInt) {
    PduBody pduBody1;
    String str1 = ">", str2 = "<";
    this.mStack.newbuf();
    PositionMarker positionMarker = this.mStack.mark();
    String str3 = new String(this.mPduHeader.getTextString(132));
    Integer integer = mContentTypeMap.get(str3);
    if (integer == null)
      return 1; 
    appendShortInteger(integer.intValue());
    if (paramInt == 132) {
      pduBody1 = ((RetrieveConf)this.mPdu).getBody();
    } else {
      pduBody1 = ((SendReq)this.mPdu).getBody();
    } 
    if (pduBody1 == null || pduBody1.getPartsNum() == 0) {
      appendUintvarInteger(0L);
      this.mStack.pop();
      this.mStack.copy();
      return 0;
    } 
    try {
      PduPart pduPart = pduBody1.getPart(0);
      byte[] arrayOfByte = pduPart.getContentId();
      if (arrayOfByte != null) {
        appendOctet(138);
        if (60 == arrayOfByte[0] && 62 == arrayOfByte[arrayOfByte.length - 1]) {
          appendTextString(arrayOfByte);
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("<");
          String str = new String();
          this(arrayOfByte);
          stringBuilder.append(str);
          stringBuilder.append(">");
          appendTextString(stringBuilder.toString());
        } 
      } 
      appendOctet(137);
      appendTextString(pduPart.getContentType());
    } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
      arrayIndexOutOfBoundsException.printStackTrace();
    } 
    paramInt = positionMarker.getLength();
    this.mStack.pop();
    appendValueLength(paramInt);
    this.mStack.copy();
    int i = pduBody1.getPartsNum();
    appendUintvarInteger(i);
    PduBody pduBody2;
    byte b;
    for (b = 0, pduBody2 = pduBody1; b < i; ) {
      ByteArrayOutputStream byteArrayOutputStream1, byteArrayOutputStream2;
      byte[] arrayOfByte3;
      PduPart pduPart = pduBody2.getPart(b);
      this.mStack.newbuf();
      PositionMarker positionMarker2 = this.mStack.mark();
      this.mStack.newbuf();
      PositionMarker positionMarker1 = this.mStack.mark();
      byte[] arrayOfByte1 = pduPart.getContentType();
      if (arrayOfByte1 == null)
        return 1; 
      HashMap<String, Integer> hashMap = mContentTypeMap;
      String str = new String(arrayOfByte1);
      Integer integer1 = hashMap.get(str);
      if (integer1 == null) {
        appendTextString(arrayOfByte1);
      } else {
        appendShortInteger(integer1.intValue());
      } 
      arrayOfByte1 = pduPart.getName();
      if (arrayOfByte1 == null) {
        arrayOfByte1 = pduPart.getFilename();
        if (arrayOfByte1 == null) {
          arrayOfByte1 = pduPart.getContentLocation();
          if (arrayOfByte1 == null)
            return 1; 
        } 
      } 
      appendOctet(133);
      appendTextString(arrayOfByte1);
      paramInt = pduPart.getCharset();
      if (paramInt != 0) {
        appendOctet(129);
        appendShortInteger(paramInt);
      } 
      paramInt = positionMarker1.getLength();
      this.mStack.pop();
      appendValueLength(paramInt);
      this.mStack.copy();
      arrayOfByte1 = pduPart.getContentId();
      if (arrayOfByte1 != null) {
        appendOctet(192);
        if (60 == arrayOfByte1[0] && 62 == arrayOfByte1[arrayOfByte1.length - 1]) {
          appendQuotedString(arrayOfByte1);
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(str2);
          stringBuilder.append(new String(arrayOfByte1));
          stringBuilder.append(str1);
          appendQuotedString(stringBuilder.toString());
        } 
      } 
      byte[] arrayOfByte4 = pduPart.getContentLocation();
      if (arrayOfByte4 != null) {
        appendOctet(142);
        appendTextString(arrayOfByte4);
      } 
      int j = positionMarker2.getLength();
      paramInt = 0;
      byte[] arrayOfByte2 = pduPart.getData();
      if (arrayOfByte2 != null) {
        arraycopy(arrayOfByte2, 0, arrayOfByte2.length);
        paramInt = arrayOfByte2.length;
        continue;
      } 
      byte[] arrayOfByte5 = null;
      integer1 = null;
      arrayOfByte2 = null;
      hashMap = null;
      try {
        byte[] arrayOfByte = new byte[1024];
        return 1;
      } catch (FileNotFoundException fileNotFoundException) {
        return 1;
      } catch (IOException iOException) {
        return 1;
      } catch (RuntimeException runtimeException) {
        return 1;
      } finally {
        arrayOfByte1 = null;
        byteArrayOutputStream2 = byteArrayOutputStream1;
      } 
    } 
    return 0;
  }
  
  private static class LengthRecordNode {
    private LengthRecordNode() {}
    
    ByteArrayOutputStream currentMessage = null;
    
    public int currentPosition = 0;
    
    public LengthRecordNode next = null;
  }
  
  private class PositionMarker {
    private int c_pos;
    
    private int currentStackSize;
    
    final PduComposer this$0;
    
    private PositionMarker() {}
    
    int getLength() {
      if (this.currentStackSize == PduComposer.this.mStack.stackSize)
        return PduComposer.this.mPosition - this.c_pos; 
      throw new RuntimeException("BUG: Invalid call to getLength()");
    }
  }
  
  private class BufferStack {
    private PduComposer.LengthRecordNode stack = null;
    
    private PduComposer.LengthRecordNode toCopy = null;
    
    int stackSize = 0;
    
    final PduComposer this$0;
    
    void newbuf() {
      if (this.toCopy == null) {
        PduComposer.LengthRecordNode lengthRecordNode = new PduComposer.LengthRecordNode();
        lengthRecordNode.currentMessage = PduComposer.this.mMessage;
        lengthRecordNode.currentPosition = PduComposer.this.mPosition;
        lengthRecordNode.next = this.stack;
        this.stack = lengthRecordNode;
        this.stackSize++;
        PduComposer.this.mMessage = new ByteArrayOutputStream();
        PduComposer.this.mPosition = 0;
        return;
      } 
      throw new RuntimeException("BUG: Invalid newbuf() before copy()");
    }
    
    void pop() {
      ByteArrayOutputStream byteArrayOutputStream = PduComposer.this.mMessage;
      int i = PduComposer.this.mPosition;
      PduComposer.this.mMessage = this.stack.currentMessage;
      PduComposer.this.mPosition = this.stack.currentPosition;
      PduComposer.LengthRecordNode lengthRecordNode = this.stack;
      this.stack = lengthRecordNode.next;
      this.stackSize--;
      this.toCopy.currentMessage = byteArrayOutputStream;
      this.toCopy.currentPosition = i;
    }
    
    void copy() {
      PduComposer.this.arraycopy(this.toCopy.currentMessage.toByteArray(), 0, this.toCopy.currentPosition);
      this.toCopy = null;
    }
    
    PduComposer.PositionMarker mark() {
      PduComposer.PositionMarker positionMarker = new PduComposer.PositionMarker();
      PduComposer.PositionMarker.access$402(positionMarker, PduComposer.this.mPosition);
      PduComposer.PositionMarker.access$502(positionMarker, this.stackSize);
      return positionMarker;
    }
    
    private BufferStack() {}
  }
  
  protected static int checkAddressType(String paramString) {
    if (paramString == null)
      return 5; 
    if (paramString.matches("[0-9]{1,3}\\.{1}[0-9]{1,3}\\.{1}[0-9]{1,3}\\.{1}[0-9]{1,3}"))
      return 3; 
    if (paramString.matches("\\+?[0-9|\\.|\\-]+"))
      return 1; 
    if (paramString.matches("[a-zA-Z| ]*\\<{0,1}[a-zA-Z| ]+@{1}[a-zA-Z| ]+\\.{1}[a-zA-Z| ]+\\>{0,1}"))
      return 2; 
    if (paramString.matches("[a-fA-F]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}"))
      return 4; 
    return 5;
  }
}
