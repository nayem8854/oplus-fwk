package com.google.android.mms.pdu;

import java.io.ByteArrayOutputStream;

public class QuotedPrintable {
  private static byte ESCAPE_CHAR = 61;
  
  public static final byte[] decodeQuotedPrintable(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte == null)
      return null; 
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    for (byte b = 0; b < paramArrayOfbyte.length; b++) {
      int i = paramArrayOfbyte[b];
      if (i == ESCAPE_CHAR) {
        if ('\r' == (char)paramArrayOfbyte[b + 1] && '\n' == (char)paramArrayOfbyte[b + 2]) {
          b += 2;
        } else {
          b++;
          try {
            i = Character.digit((char)paramArrayOfbyte[b], 16);
            int j = Character.digit((char)paramArrayOfbyte[++b], 16);
            if (i == -1 || j == -1)
              return null; 
            byteArrayOutputStream.write((char)((i << 4) + j));
          } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
            return null;
          } 
        } 
      } else {
        byteArrayOutputStream.write(i);
      } 
    } 
    return byteArrayOutputStream.toByteArray();
  }
}
