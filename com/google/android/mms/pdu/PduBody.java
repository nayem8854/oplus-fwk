package com.google.android.mms.pdu;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class PduBody {
  private Vector<PduPart> mParts = null;
  
  private Map<String, PduPart> mPartMapByContentId = null;
  
  private Map<String, PduPart> mPartMapByContentLocation = null;
  
  private Map<String, PduPart> mPartMapByName = null;
  
  private Map<String, PduPart> mPartMapByFileName = null;
  
  public PduBody() {
    this.mParts = new Vector<>();
    this.mPartMapByContentId = new HashMap<>();
    this.mPartMapByContentLocation = new HashMap<>();
    this.mPartMapByName = new HashMap<>();
    this.mPartMapByFileName = new HashMap<>();
  }
  
  private void putPartToMaps(PduPart paramPduPart) {
    byte[] arrayOfByte = paramPduPart.getContentId();
    if (arrayOfByte != null)
      this.mPartMapByContentId.put(new String(arrayOfByte), paramPduPart); 
    arrayOfByte = paramPduPart.getContentLocation();
    if (arrayOfByte != null) {
      String str = new String(arrayOfByte);
      this.mPartMapByContentLocation.put(str, paramPduPart);
    } 
    arrayOfByte = paramPduPart.getName();
    if (arrayOfByte != null) {
      String str = new String(arrayOfByte);
      this.mPartMapByName.put(str, paramPduPart);
    } 
    arrayOfByte = paramPduPart.getFilename();
    if (arrayOfByte != null) {
      String str = new String(arrayOfByte);
      this.mPartMapByFileName.put(str, paramPduPart);
    } 
  }
  
  public boolean addPart(PduPart paramPduPart) {
    if (paramPduPart != null) {
      putPartToMaps(paramPduPart);
      return this.mParts.add(paramPduPart);
    } 
    throw null;
  }
  
  public void addPart(int paramInt, PduPart paramPduPart) {
    if (paramPduPart != null) {
      putPartToMaps(paramPduPart);
      this.mParts.add(paramInt, paramPduPart);
      return;
    } 
    throw null;
  }
  
  public PduPart removePart(int paramInt) {
    return this.mParts.remove(paramInt);
  }
  
  public void removeAll() {
    this.mParts.clear();
  }
  
  public PduPart getPart(int paramInt) {
    return this.mParts.get(paramInt);
  }
  
  public int getPartIndex(PduPart paramPduPart) {
    return this.mParts.indexOf(paramPduPart);
  }
  
  public int getPartsNum() {
    return this.mParts.size();
  }
  
  public PduPart getPartByContentId(String paramString) {
    return this.mPartMapByContentId.get(paramString);
  }
  
  public PduPart getPartByContentLocation(String paramString) {
    return this.mPartMapByContentLocation.get(paramString);
  }
  
  public PduPart getPartByName(String paramString) {
    return this.mPartMapByName.get(paramString);
  }
  
  public PduPart getPartByFileName(String paramString) {
    return this.mPartMapByFileName.get(paramString);
  }
}
