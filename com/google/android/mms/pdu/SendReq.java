package com.google.android.mms.pdu;

import android.util.Log;
import com.google.android.mms.InvalidHeaderValueException;

public class SendReq extends MultimediaMessagePdu {
  private static final String TAG = "SendReq";
  
  public SendReq() {
    try {
      setMessageType(128);
      setMmsVersion(18);
      setContentType("application/vnd.wap.multipart.related".getBytes());
      EncodedStringValue encodedStringValue = new EncodedStringValue();
      this("insert-address-token".getBytes());
      setFrom(encodedStringValue);
      setTransactionId(generateTransactionId());
      return;
    } catch (InvalidHeaderValueException invalidHeaderValueException) {
      Log.e("SendReq", "Unexpected InvalidHeaderValueException.", invalidHeaderValueException);
      throw new RuntimeException(invalidHeaderValueException);
    } 
  }
  
  private byte[] generateTransactionId() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("T");
    stringBuilder.append(Long.toHexString(System.currentTimeMillis()));
    String str = stringBuilder.toString();
    return str.getBytes();
  }
  
  public SendReq(byte[] paramArrayOfbyte1, EncodedStringValue paramEncodedStringValue, int paramInt, byte[] paramArrayOfbyte2) throws InvalidHeaderValueException {
    setMessageType(128);
    setContentType(paramArrayOfbyte1);
    setFrom(paramEncodedStringValue);
    setMmsVersion(paramInt);
    setTransactionId(paramArrayOfbyte2);
  }
  
  SendReq(PduHeaders paramPduHeaders) {
    super(paramPduHeaders);
  }
  
  SendReq(PduHeaders paramPduHeaders, PduBody paramPduBody) {
    super(paramPduHeaders, paramPduBody);
  }
  
  public EncodedStringValue[] getBcc() {
    return this.mPduHeaders.getEncodedStringValues(129);
  }
  
  public void addBcc(EncodedStringValue paramEncodedStringValue) {
    this.mPduHeaders.appendEncodedStringValue(paramEncodedStringValue, 129);
  }
  
  public void setBcc(EncodedStringValue[] paramArrayOfEncodedStringValue) {
    this.mPduHeaders.setEncodedStringValues(paramArrayOfEncodedStringValue, 129);
  }
  
  public EncodedStringValue[] getCc() {
    return this.mPduHeaders.getEncodedStringValues(130);
  }
  
  public void addCc(EncodedStringValue paramEncodedStringValue) {
    this.mPduHeaders.appendEncodedStringValue(paramEncodedStringValue, 130);
  }
  
  public void setCc(EncodedStringValue[] paramArrayOfEncodedStringValue) {
    this.mPduHeaders.setEncodedStringValues(paramArrayOfEncodedStringValue, 130);
  }
  
  public byte[] getContentType() {
    return this.mPduHeaders.getTextString(132);
  }
  
  public void setContentType(byte[] paramArrayOfbyte) {
    this.mPduHeaders.setTextString(paramArrayOfbyte, 132);
  }
  
  public int getDeliveryReport() {
    return this.mPduHeaders.getOctet(134);
  }
  
  public void setDeliveryReport(int paramInt) throws InvalidHeaderValueException {
    this.mPduHeaders.setOctet(paramInt, 134);
  }
  
  public long getExpiry() {
    return this.mPduHeaders.getLongInteger(136);
  }
  
  public void setExpiry(long paramLong) {
    this.mPduHeaders.setLongInteger(paramLong, 136);
  }
  
  public long getMessageSize() {
    return this.mPduHeaders.getLongInteger(142);
  }
  
  public void setMessageSize(long paramLong) {
    this.mPduHeaders.setLongInteger(paramLong, 142);
  }
  
  public byte[] getMessageClass() {
    return this.mPduHeaders.getTextString(138);
  }
  
  public void setMessageClass(byte[] paramArrayOfbyte) {
    this.mPduHeaders.setTextString(paramArrayOfbyte, 138);
  }
  
  public int getReadReport() {
    return this.mPduHeaders.getOctet(144);
  }
  
  public void setReadReport(int paramInt) throws InvalidHeaderValueException {
    this.mPduHeaders.setOctet(paramInt, 144);
  }
  
  public void setTo(EncodedStringValue[] paramArrayOfEncodedStringValue) {
    this.mPduHeaders.setEncodedStringValues(paramArrayOfEncodedStringValue, 151);
  }
  
  public byte[] getTransactionId() {
    return this.mPduHeaders.getTextString(152);
  }
  
  public void setTransactionId(byte[] paramArrayOfbyte) {
    this.mPduHeaders.setTextString(paramArrayOfbyte, 152);
  }
}
