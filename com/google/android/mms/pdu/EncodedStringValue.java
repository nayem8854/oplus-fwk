package com.google.android.mms.pdu;

import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class EncodedStringValue implements Cloneable {
  private static final boolean DEBUG = false;
  
  private static final boolean LOCAL_LOGV = false;
  
  private static final String TAG = "EncodedStringValue";
  
  private int mCharacterSet;
  
  private byte[] mData;
  
  public EncodedStringValue(int paramInt, byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null) {
      this.mCharacterSet = paramInt;
      byte[] arrayOfByte = new byte[paramArrayOfbyte.length];
      System.arraycopy(paramArrayOfbyte, 0, arrayOfByte, 0, paramArrayOfbyte.length);
      return;
    } 
    throw new NullPointerException("EncodedStringValue: Text-string is null.");
  }
  
  public EncodedStringValue(byte[] paramArrayOfbyte) {
    this(106, paramArrayOfbyte);
  }
  
  public EncodedStringValue(String paramString) {
    try {
      this.mData = paramString.getBytes("utf-8");
      this.mCharacterSet = 106;
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      Log.e("EncodedStringValue", "Default encoding must be supported.", unsupportedEncodingException);
    } 
  }
  
  public int getCharacterSet() {
    return this.mCharacterSet;
  }
  
  public void setCharacterSet(int paramInt) {
    this.mCharacterSet = paramInt;
  }
  
  public byte[] getTextString() {
    byte[] arrayOfByte1 = this.mData, arrayOfByte2 = new byte[arrayOfByte1.length];
    System.arraycopy(arrayOfByte1, 0, arrayOfByte2, 0, arrayOfByte1.length);
    return arrayOfByte2;
  }
  
  public void setTextString(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null) {
      byte[] arrayOfByte = new byte[paramArrayOfbyte.length];
      System.arraycopy(paramArrayOfbyte, 0, arrayOfByte, 0, paramArrayOfbyte.length);
      return;
    } 
    throw new NullPointerException("EncodedStringValue: Text-string is null.");
  }
  
  public String getString() {
    int i = this.mCharacterSet;
    if (i == 0)
      return new String(this.mData); 
    try {
      null = CharacterSets.getMimeName(i);
      return new String(this.mData, null);
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      try {
        return new String(this.mData, "iso-8859-1");
      } catch (UnsupportedEncodingException unsupportedEncodingException1) {
        return new String(this.mData);
      } 
    } 
  }
  
  public void appendTextString(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null) {
      if (this.mData == null) {
        byte[] arrayOfByte = new byte[paramArrayOfbyte.length];
        System.arraycopy(paramArrayOfbyte, 0, arrayOfByte, 0, paramArrayOfbyte.length);
      } else {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
          byteArrayOutputStream.write(this.mData);
          byteArrayOutputStream.write(paramArrayOfbyte);
          this.mData = byteArrayOutputStream.toByteArray();
          return;
        } catch (IOException iOException) {
          iOException.printStackTrace();
          throw new NullPointerException("appendTextString: failed when write a new Text-string");
        } 
      } 
      return;
    } 
    throw new NullPointerException("Text-string is null.");
  }
  
  public Object clone() throws CloneNotSupportedException {
    super.clone();
    byte[] arrayOfByte1 = this.mData;
    int i = arrayOfByte1.length;
    byte[] arrayOfByte2 = new byte[i];
    System.arraycopy(arrayOfByte1, 0, arrayOfByte2, 0, i);
    try {
      return new EncodedStringValue(this.mCharacterSet, arrayOfByte2);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("failed to clone an EncodedStringValue: ");
      stringBuilder.append(this);
      Log.e("EncodedStringValue", stringBuilder.toString());
      exception.printStackTrace();
      throw new CloneNotSupportedException(exception.getMessage());
    } 
  }
  
  public EncodedStringValue[] split(String paramString) {
    String[] arrayOfString = getString().split(paramString);
    EncodedStringValue[] arrayOfEncodedStringValue = new EncodedStringValue[arrayOfString.length];
    for (byte b = 0; b < arrayOfEncodedStringValue.length;) {
      try {
        int i = this.mCharacterSet;
        String str = arrayOfString[b];
        arrayOfEncodedStringValue[b] = new EncodedStringValue(i, str.getBytes());
        b++;
      } catch (NullPointerException nullPointerException) {
        return null;
      } 
    } 
    return (EncodedStringValue[])nullPointerException;
  }
  
  public static EncodedStringValue[] extract(String paramString) {
    String[] arrayOfString = paramString.split(";");
    ArrayList<EncodedStringValue> arrayList = new ArrayList();
    int i;
    for (i = 0; i < arrayOfString.length; i++) {
      if (arrayOfString[i].length() > 0)
        arrayList.add(new EncodedStringValue(arrayOfString[i])); 
    } 
    i = arrayList.size();
    if (i > 0)
      return arrayList.<EncodedStringValue>toArray(new EncodedStringValue[i]); 
    return null;
  }
  
  public static String concat(EncodedStringValue[] paramArrayOfEncodedStringValue) {
    StringBuilder stringBuilder = new StringBuilder();
    int i = paramArrayOfEncodedStringValue.length - 1;
    for (byte b = 0; b <= i; b++) {
      stringBuilder.append(paramArrayOfEncodedStringValue[b].getString());
      if (b < i)
        stringBuilder.append(";"); 
    } 
    return stringBuilder.toString();
  }
  
  public static EncodedStringValue copy(EncodedStringValue paramEncodedStringValue) {
    if (paramEncodedStringValue == null)
      return null; 
    return new EncodedStringValue(paramEncodedStringValue.mCharacterSet, paramEncodedStringValue.mData);
  }
  
  public static EncodedStringValue[] encodeStrings(String[] paramArrayOfString) {
    int i = paramArrayOfString.length;
    if (i > 0) {
      EncodedStringValue[] arrayOfEncodedStringValue = new EncodedStringValue[i];
      for (byte b = 0; b < i; b++)
        arrayOfEncodedStringValue[b] = new EncodedStringValue(paramArrayOfString[b]); 
      return arrayOfEncodedStringValue;
    } 
    return null;
  }
}
