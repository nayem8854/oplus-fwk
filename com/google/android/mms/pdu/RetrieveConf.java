package com.google.android.mms.pdu;

import com.google.android.mms.InvalidHeaderValueException;

public class RetrieveConf extends MultimediaMessagePdu {
  public RetrieveConf() throws InvalidHeaderValueException {
    setMessageType(132);
  }
  
  RetrieveConf(PduHeaders paramPduHeaders) {
    super(paramPduHeaders);
  }
  
  RetrieveConf(PduHeaders paramPduHeaders, PduBody paramPduBody) {
    super(paramPduHeaders, paramPduBody);
  }
  
  public EncodedStringValue[] getCc() {
    return this.mPduHeaders.getEncodedStringValues(130);
  }
  
  public void addCc(EncodedStringValue paramEncodedStringValue) {
    this.mPduHeaders.appendEncodedStringValue(paramEncodedStringValue, 130);
  }
  
  public byte[] getContentType() {
    return this.mPduHeaders.getTextString(132);
  }
  
  public void setContentType(byte[] paramArrayOfbyte) {
    this.mPduHeaders.setTextString(paramArrayOfbyte, 132);
  }
  
  public int getDeliveryReport() {
    return this.mPduHeaders.getOctet(134);
  }
  
  public void setDeliveryReport(int paramInt) throws InvalidHeaderValueException {
    this.mPduHeaders.setOctet(paramInt, 134);
  }
  
  public EncodedStringValue getFrom() {
    return this.mPduHeaders.getEncodedStringValue(137);
  }
  
  public void setFrom(EncodedStringValue paramEncodedStringValue) {
    this.mPduHeaders.setEncodedStringValue(paramEncodedStringValue, 137);
  }
  
  public byte[] getMessageClass() {
    return this.mPduHeaders.getTextString(138);
  }
  
  public void setMessageClass(byte[] paramArrayOfbyte) {
    this.mPduHeaders.setTextString(paramArrayOfbyte, 138);
  }
  
  public byte[] getMessageId() {
    return this.mPduHeaders.getTextString(139);
  }
  
  public void setMessageId(byte[] paramArrayOfbyte) {
    this.mPduHeaders.setTextString(paramArrayOfbyte, 139);
  }
  
  public int getReadReport() {
    return this.mPduHeaders.getOctet(144);
  }
  
  public void setReadReport(int paramInt) throws InvalidHeaderValueException {
    this.mPduHeaders.setOctet(paramInt, 144);
  }
  
  public int getRetrieveStatus() {
    return this.mPduHeaders.getOctet(153);
  }
  
  public void setRetrieveStatus(int paramInt) throws InvalidHeaderValueException {
    this.mPduHeaders.setOctet(paramInt, 153);
  }
  
  public EncodedStringValue getRetrieveText() {
    return this.mPduHeaders.getEncodedStringValue(154);
  }
  
  public void setRetrieveText(EncodedStringValue paramEncodedStringValue) {
    this.mPduHeaders.setEncodedStringValue(paramEncodedStringValue, 154);
  }
  
  public byte[] getTransactionId() {
    return this.mPduHeaders.getTextString(152);
  }
  
  public void setTransactionId(byte[] paramArrayOfbyte) {
    this.mPduHeaders.setTextString(paramArrayOfbyte, 152);
  }
}
