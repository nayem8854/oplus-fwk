package com.google.android.mms.pdu;

import android.util.Log;
import com.google.android.mms.InvalidHeaderValueException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.HashMap;

public class PduParser {
  private ByteArrayInputStream mPduDataStream = null;
  
  private final boolean mParseContentDisposition;
  
  private PduHeaders mHeaders = null;
  
  private PduBody mBody = null;
  
  private static byte[] mTypeParam = null;
  
  private static byte[] mStartParam;
  
  private static final int TYPE_TOKEN_STRING = 2;
  
  private static final int TYPE_TEXT_STRING = 0;
  
  private static final int TYPE_QUOTED_STRING = 1;
  
  private static final int THE_LAST_PART = 1;
  
  private static final int THE_FIRST_PART = 0;
  
  private static final int TEXT_MIN = 32;
  
  private static final int TEXT_MAX = 127;
  
  private static final int SHORT_LENGTH_MAX = 30;
  
  private static final int SHORT_INTEGER_MAX = 127;
  
  private static final int QUOTED_STRING_FLAG = 34;
  
  private static final int QUOTE = 127;
  
  private static final int LONG_INTEGER_LENGTH_MAX = 8;
  
  private static final String LOG_TAG = "PduParser";
  
  private static final boolean LOCAL_LOGV = false;
  
  private static final int LENGTH_QUOTE = 31;
  
  private static final int END_STRING_FLAG = 0;
  
  private static final boolean DEBUG = false;
  
  static final boolean $assertionsDisabled = false;
  
  static {
    mStartParam = null;
  }
  
  public PduParser(byte[] paramArrayOfbyte, boolean paramBoolean) {
    this.mPduDataStream = new ByteArrayInputStream(paramArrayOfbyte);
    this.mParseContentDisposition = paramBoolean;
  }
  
  public GenericPdu parse() {
    ReadOrigInd readOrigInd;
    ReadRecInd readRecInd;
    DeliveryInd deliveryInd;
    AcknowledgeInd acknowledgeInd;
    RetrieveConf retrieveConf;
    NotifyRespInd notifyRespInd;
    NotificationInd notificationInd;
    byte[] arrayOfByte;
    String str;
    ByteArrayInputStream byteArrayInputStream = this.mPduDataStream;
    if (byteArrayInputStream == null)
      return null; 
    PduHeaders pduHeaders = parseHeaders(byteArrayInputStream);
    if (pduHeaders == null)
      return null; 
    int i = pduHeaders.getOctet(140);
    if (!checkMandatoryHeader(this.mHeaders)) {
      log("check mandatory headers failed!");
      return null;
    } 
    if (128 == i || 132 == i) {
      PduBody pduBody = parseParts(this.mPduDataStream);
      if (pduBody == null)
        return null; 
    } 
    switch (i) {
      default:
        log("Parser doesn't support this message type in this version!");
        return null;
      case 136:
        return new ReadOrigInd(this.mHeaders);
      case 135:
        return new ReadRecInd(this.mHeaders);
      case 134:
        return new DeliveryInd(this.mHeaders);
      case 133:
        return new AcknowledgeInd(this.mHeaders);
      case 132:
        retrieveConf = new RetrieveConf(this.mHeaders, this.mBody);
        arrayOfByte = retrieveConf.getContentType();
        if (arrayOfByte == null)
          return null; 
        str = new String(arrayOfByte);
        if (str.equals("application/vnd.wap.multipart.mixed") || str.equals("application/vnd.wap.multipart.related") || str.equals("application/vnd.wap.multipart.alternative"))
          return retrieveConf; 
        if (str.equals("application/vnd.wap.multipart.alternative")) {
          PduPart pduPart = this.mBody.getPart(0);
          this.mBody.removeAll();
          this.mBody.addPart(0, pduPart);
          return retrieveConf;
        } 
        return null;
      case 131:
        return new NotifyRespInd(this.mHeaders);
      case 130:
        return new NotificationInd(this.mHeaders);
      case 129:
        return new SendConf(this.mHeaders);
      case 128:
        break;
    } 
    return new SendReq(this.mHeaders, this.mBody);
  }
  
  protected PduHeaders parseHeaders(ByteArrayInputStream paramByteArrayInputStream) {
    if (paramByteArrayInputStream == null)
      return null; 
    PduHeaders pduHeaders = new PduHeaders();
    int i = 1;
    while (i && paramByteArrayInputStream.available() > 0) {
      StringBuilder stringBuilder2, stringBuilder1;
      int k;
      EncodedStringValue encodedStringValue1;
      byte[] arrayOfByte2;
      HashMap<Object, Object> hashMap;
      byte[] arrayOfByte1, arrayOfByte3;
      EncodedStringValue encodedStringValue2;
      paramByteArrayInputStream.mark(1);
      int j = extractByteValue(paramByteArrayInputStream);
      if (j >= 32 && j <= 127) {
        paramByteArrayInputStream.reset();
        parseWapString(paramByteArrayInputStream, 0);
        continue;
      } 
      switch (j) {
        default:
          log("Unknown header");
          k = i;
          break;
        case 178:
          parseContentType(paramByteArrayInputStream, null);
          k = i;
          break;
        case 173:
        case 175:
        case 179:
          try {
            long l = parseIntegerValue(paramByteArrayInputStream);
            pduHeaders.setLongInteger(l, j);
            k = i;
          } catch (RuntimeException null) {
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append(j);
            stringBuilder2.append("is not Long-Integer header field!");
            log(stringBuilder2.toString());
            return null;
          } 
          break;
        case 170:
        case 172:
          parseValueLength((ByteArrayInputStream)stringBuilder2);
          extractByteValue((ByteArrayInputStream)stringBuilder2);
          try {
            parseIntegerValue((ByteArrayInputStream)stringBuilder2);
            k = i;
          } catch (RuntimeException null) {
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append(j);
            stringBuilder2.append(" is not Integer-Value");
            log(stringBuilder2.toString());
            return null;
          } 
          break;
        case 164:
          parseValueLength((ByteArrayInputStream)stringBuilder2);
          extractByteValue((ByteArrayInputStream)stringBuilder2);
          parseEncodedStringValue((ByteArrayInputStream)stringBuilder2);
          k = i;
          break;
        case 161:
          parseValueLength((ByteArrayInputStream)stringBuilder2);
          try {
            parseIntegerValue((ByteArrayInputStream)stringBuilder2);
            try {
              long l = parseLongInteger((ByteArrayInputStream)stringBuilder2);
              pduHeaders.setLongInteger(l, 161);
              k = i;
            } catch (RuntimeException runtimeException1) {
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append(j);
              stringBuilder1.append("is not Long-Integer header field!");
              log(stringBuilder1.toString());
              return null;
            } 
          } catch (RuntimeException null) {
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append(j);
            stringBuilder2.append(" is not Integer-Value");
            log(stringBuilder2.toString());
            return null;
          } 
          break;
        case 160:
          parseValueLength((ByteArrayInputStream)stringBuilder2);
          try {
            parseIntegerValue((ByteArrayInputStream)stringBuilder2);
            EncodedStringValue encodedStringValue = parseEncodedStringValue((ByteArrayInputStream)stringBuilder2);
            k = i;
            if (encodedStringValue != null) {
              try {
                pduHeaders.setEncodedStringValue(encodedStringValue, 160);
              } catch (NullPointerException null) {
                log("null pointer error!");
              } catch (RuntimeException runtimeException1) {
                stringBuilder1 = new StringBuilder();
                stringBuilder1.append(j);
                stringBuilder1.append("is not Encoded-String-Value header field!");
                log(stringBuilder1.toString());
                return null;
              } 
              k = i;
            } 
          } catch (RuntimeException null) {
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append(j);
            stringBuilder2.append(" is not Integer-Value");
            log(stringBuilder2.toString());
            return null;
          } 
          break;
        case 147:
        case 150:
        case 154:
        case 166:
        case 181:
        case 182:
          encodedStringValue1 = parseEncodedStringValue((ByteArrayInputStream)stringBuilder2);
          k = i;
          if (encodedStringValue1 != null) {
            try {
              pduHeaders.setEncodedStringValue(encodedStringValue1, j);
            } catch (NullPointerException null) {
              log("null pointer error!");
            } catch (RuntimeException null) {
              stringBuilder2 = new StringBuilder();
              stringBuilder2.append(j);
              stringBuilder2.append("is not Encoded-String-Value header field!");
              log(stringBuilder2.toString());
              return null;
            } 
            k = i;
          } 
          break;
        case 141:
          k = parseShortInteger((ByteArrayInputStream)stringBuilder2);
          try {
            pduHeaders.setOctet(k, 141);
            k = i;
          } catch (InvalidHeaderValueException invalidHeaderValueException) {
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append("Set invalid Octet value: ");
            stringBuilder1.append(k);
            stringBuilder1.append(" into the header filed: ");
            stringBuilder1.append(j);
            log(stringBuilder1.toString());
            return null;
          } catch (RuntimeException null) {
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append(j);
            stringBuilder2.append("is not Octet header field!");
            log(stringBuilder2.toString());
            return null;
          } 
          break;
        case 140:
          k = extractByteValue((ByteArrayInputStream)stringBuilder2);
          switch (k) {
            default:
              try {
                pduHeaders.setOctet(k, j);
              } catch (InvalidHeaderValueException invalidHeaderValueException) {
                stringBuilder1 = new StringBuilder();
                stringBuilder1.append("Set invalid Octet value: ");
                stringBuilder1.append(k);
                stringBuilder1.append(" into the header filed: ");
                stringBuilder1.append(j);
                log(stringBuilder1.toString());
                return null;
              } catch (RuntimeException null) {
                stringBuilder2 = new StringBuilder();
                stringBuilder2.append(j);
                stringBuilder2.append("is not Octet header field!");
                log(stringBuilder2.toString());
                return null;
              } 
              break;
            case 137:
            case 138:
            case 139:
            case 140:
            case 141:
            case 142:
            case 143:
            case 144:
            case 145:
            case 146:
            case 147:
            case 148:
            case 149:
            case 150:
            case 151:
              return null;
          } 
          k = i;
          break;
        case 138:
          stringBuilder2.mark(1);
          k = extractByteValue((ByteArrayInputStream)stringBuilder2);
          if (k >= 128) {
            if (128 == k) {
              try {
                byte[] arrayOfByte = "personal".getBytes();
                pduHeaders.setTextString(arrayOfByte, 138);
              } catch (NullPointerException null) {
                log("null pointer error!");
              } catch (RuntimeException runtimeException) {}
            } else if (129 == k) {
              byte[] arrayOfByte = "advertisement".getBytes();
              pduHeaders.setTextString(arrayOfByte, 138);
            } else if (130 == k) {
              byte[] arrayOfByte = "informational".getBytes();
              pduHeaders.setTextString(arrayOfByte, 138);
            } else if (131 == k) {
              byte[] arrayOfByte = "auto".getBytes();
              pduHeaders.setTextString(arrayOfByte, 138);
            } 
            k = i;
            break;
          } 
          runtimeException.reset();
          arrayOfByte2 = parseWapString((ByteArrayInputStream)runtimeException, 0);
          if (arrayOfByte2 != null)
            try {
              pduHeaders.setTextString(arrayOfByte2, 138);
            } catch (NullPointerException null) {
              log("null pointer error!");
            } catch (RuntimeException runtimeException1) {
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append(j);
              stringBuilder1.append("is not Text-String header field!");
              log(stringBuilder1.toString());
              return null;
            }  
          k = i;
          break;
        case 137:
          parseValueLength((ByteArrayInputStream)stringBuilder1);
          k = extractByteValue((ByteArrayInputStream)stringBuilder1);
          if (128 == k) {
            EncodedStringValue encodedStringValue4 = parseEncodedStringValue((ByteArrayInputStream)stringBuilder1);
            EncodedStringValue encodedStringValue3 = encodedStringValue4;
            if (encodedStringValue4 != null) {
              byte[] arrayOfByte = encodedStringValue4.getTextString();
              if (arrayOfByte != null) {
                String str = new String(arrayOfByte);
                k = str.indexOf("/");
                if (k > 0)
                  str = str.substring(0, k); 
                try {
                  encodedStringValue4.setTextString(str.getBytes());
                } catch (NullPointerException nullPointerException1) {
                  log("null pointer error!");
                  return null;
                } 
              } 
              EncodedStringValue encodedStringValue = encodedStringValue4;
            } 
          } else {
            try {
              EncodedStringValue encodedStringValue = new EncodedStringValue("insert-address-token".getBytes());
              try {
                pduHeaders.setEncodedStringValue(encodedStringValue, 137);
              } catch (NullPointerException nullPointerException) {
                log("null pointer error!");
              } catch (RuntimeException runtimeException1) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(j);
                stringBuilder.append("is not Encoded-String-Value header field!");
                log(stringBuilder.toString());
                return null;
              } 
              k = i;
            } catch (NullPointerException nullPointerException1) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append(j);
              stringBuilder.append("is not Encoded-String-Value header field!");
              log(stringBuilder.toString());
              return null;
            } 
            break;
          } 
          try {
            pduHeaders.setEncodedStringValue((EncodedStringValue)nullPointerException, 137);
          } catch (NullPointerException nullPointerException1) {
            log("null pointer error!");
          } catch (RuntimeException runtimeException1) {
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append(j);
            stringBuilder1.append("is not Encoded-String-Value header field!");
            log(stringBuilder1.toString());
            return null;
          } 
          k = i;
        case 135:
        case 136:
        case 157:
          parseValueLength((ByteArrayInputStream)stringBuilder1);
          k = extractByteValue((ByteArrayInputStream)stringBuilder1);
          try {
            long l2 = parseLongInteger((ByteArrayInputStream)stringBuilder1);
            long l1 = l2;
            if (129 == k)
              l1 = l2 + System.currentTimeMillis() / 1000L; 
            try {
              pduHeaders.setLongInteger(l1, j);
              k = i;
            } catch (RuntimeException runtimeException1) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append(j);
              stringBuilder.append("is not Long-Integer header field!");
              log(stringBuilder.toString());
              return null;
            } 
          } catch (RuntimeException runtimeException1) {
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append(j);
            stringBuilder1.append("is not Long-Integer header field!");
            log(stringBuilder1.toString());
            return null;
          } 
          break;
        case 134:
        case 143:
        case 144:
        case 145:
        case 146:
        case 148:
        case 149:
        case 153:
        case 155:
        case 156:
        case 162:
        case 163:
        case 165:
        case 167:
        case 169:
        case 171:
        case 177:
        case 180:
        case 186:
        case 187:
        case 188:
        case 191:
          k = extractByteValue((ByteArrayInputStream)stringBuilder1);
          try {
            pduHeaders.setOctet(k, j);
            k = i;
          } catch (InvalidHeaderValueException invalidHeaderValueException) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Set invalid Octet value: ");
            stringBuilder.append(k);
            stringBuilder.append(" into the header filed: ");
            stringBuilder.append(j);
            log(stringBuilder.toString());
            return null;
          } catch (RuntimeException runtimeException1) {
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append(j);
            stringBuilder1.append("is not Octet header field!");
            log(stringBuilder1.toString());
            return null;
          } 
          break;
        case 133:
        case 142:
        case 159:
          try {
            long l = parseLongInteger((ByteArrayInputStream)stringBuilder1);
            pduHeaders.setLongInteger(l, j);
            k = i;
          } catch (RuntimeException runtimeException1) {
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append(j);
            stringBuilder1.append("is not Long-Integer header field!");
            log(stringBuilder1.toString());
            return null;
          } 
          break;
        case 132:
          hashMap = new HashMap<>();
          arrayOfByte3 = parseContentType((ByteArrayInputStream)stringBuilder1, (HashMap)hashMap);
          if (arrayOfByte3 != null)
            try {
              pduHeaders.setTextString(arrayOfByte3, 132);
            } catch (NullPointerException nullPointerException1) {
              log("null pointer error!");
            } catch (RuntimeException runtimeException1) {
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append(j);
              stringBuilder1.append("is not Text-String header field!");
              log(stringBuilder1.toString());
              return null;
            }  
          mStartParam = (byte[])hashMap.get(Integer.valueOf(153));
          mTypeParam = (byte[])hashMap.get(Integer.valueOf(131));
          k = 0;
          break;
        case 131:
        case 139:
        case 152:
        case 158:
        case 183:
        case 184:
        case 185:
        case 189:
        case 190:
          arrayOfByte1 = parseWapString((ByteArrayInputStream)stringBuilder1, 0);
          k = i;
          if (arrayOfByte1 != null) {
            try {
              pduHeaders.setTextString(arrayOfByte1, j);
            } catch (NullPointerException nullPointerException1) {
              log("null pointer error!");
            } catch (RuntimeException runtimeException1) {
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append(j);
              stringBuilder1.append("is not Text-String header field!");
              log(stringBuilder1.toString());
              return null;
            } 
            k = i;
          } 
          break;
        case 129:
        case 130:
        case 151:
          encodedStringValue2 = parseEncodedStringValue((ByteArrayInputStream)stringBuilder1);
          k = i;
          if (encodedStringValue2 != null) {
            arrayOfByte1 = encodedStringValue2.getTextString();
            if (arrayOfByte1 != null) {
              String str = new String(arrayOfByte1);
              k = str.indexOf("/");
              if (k > 0)
                str = str.substring(0, k); 
              try {
                encodedStringValue2.setTextString(str.getBytes());
              } catch (NullPointerException nullPointerException1) {
                log("null pointer error!");
                return null;
              } 
            } 
            try {
              pduHeaders.appendEncodedStringValue(encodedStringValue2, j);
            } catch (NullPointerException nullPointerException1) {
              log("null pointer error!");
            } catch (RuntimeException runtimeException1) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append(j);
              stringBuilder.append("is not Encoded-String-Value header field!");
              log(stringBuilder.toString());
              return null;
            } 
            k = i;
          } 
          break;
      } 
      i = k;
    } 
    return pduHeaders;
  }
  
  protected PduBody parseParts(ByteArrayInputStream paramByteArrayInputStream) {
    if (paramByteArrayInputStream == null)
      return null; 
    int i = parseUnsignedInt(paramByteArrayInputStream);
    PduBody pduBody = new PduBody();
    for (byte b = 0; b < i; b++) {
      PduPart pduPart2;
      int j = parseUnsignedInt(paramByteArrayInputStream);
      int k = parseUnsignedInt(paramByteArrayInputStream);
      PduPart pduPart1 = new PduPart();
      int m = paramByteArrayInputStream.available();
      if (m <= 0)
        return null; 
      HashMap<Object, Object> hashMap = new HashMap<>();
      byte[] arrayOfByte = parseContentType(paramByteArrayInputStream, (HashMap)hashMap);
      if (arrayOfByte != null) {
        pduPart1.setContentType(arrayOfByte);
      } else {
        pduPart1.setContentType(PduContentTypes.contentTypes[0].getBytes());
      } 
      arrayOfByte = (byte[])hashMap.get(Integer.valueOf(151));
      if (arrayOfByte != null)
        pduPart1.setName(arrayOfByte); 
      Integer integer = (Integer)hashMap.get(Integer.valueOf(129));
      if (integer != null)
        pduPart1.setCharset(integer.intValue()); 
      int n = paramByteArrayInputStream.available();
      j -= m - n;
      if (j > 0) {
        if (!parsePartHeaders(paramByteArrayInputStream, pduPart1, j))
          return null; 
      } else if (j < 0) {
        return null;
      } 
      if (pduPart1.getContentLocation() == null && 
        pduPart1.getName() == null && 
        pduPart1.getFilename() == null && 
        pduPart1.getContentId() == null) {
        long l = System.currentTimeMillis();
        String str = Long.toOctalString(l);
        byte[] arrayOfByte1 = str.getBytes();
        pduPart1.setContentLocation(arrayOfByte1);
      } 
      if (k > 0) {
        arrayOfByte = new byte[k];
        String str = new String(pduPart1.getContentType());
        paramByteArrayInputStream.read(arrayOfByte, 0, k);
        if (str.equalsIgnoreCase("application/vnd.wap.multipart.alternative")) {
          PduBody pduBody1 = parseParts(new ByteArrayInputStream(arrayOfByte));
          pduPart2 = pduBody1.getPart(0);
        } else {
          byte[] arrayOfByte1 = pduPart1.getContentTransferEncoding();
          if (arrayOfByte1 != null) {
            String str1 = new String(arrayOfByte1);
            if (str1.equalsIgnoreCase("base64")) {
              arrayOfByte1 = Base64.decodeBase64(arrayOfByte);
            } else {
              arrayOfByte1 = arrayOfByte;
              if (str1.equalsIgnoreCase("quoted-printable"))
                arrayOfByte1 = QuotedPrintable.decodeQuotedPrintable(arrayOfByte); 
            } 
          } else {
            arrayOfByte1 = arrayOfByte;
          } 
          if (arrayOfByte1 == null) {
            log("Decode part data error!");
            return null;
          } 
          pduPart1.setData(arrayOfByte1);
          pduPart2 = pduPart1;
        } 
      } else {
        pduPart2 = pduPart1;
      } 
      if (checkPartPosition(pduPart2) == 0) {
        pduBody.addPart(0, pduPart2);
      } else {
        pduBody.addPart(pduPart2);
      } 
    } 
    return pduBody;
  }
  
  private static void log(String paramString) {}
  
  protected static int parseUnsignedInt(ByteArrayInputStream paramByteArrayInputStream) {
    int i = 0;
    int j = paramByteArrayInputStream.read();
    int k = j;
    if (j == -1)
      return j; 
    while ((k & 0x80) != 0) {
      i = i << 7 | k & 0x7F;
      j = paramByteArrayInputStream.read();
      k = j;
      if (j == -1)
        return j; 
    } 
    return i << 7 | k & 0x7F;
  }
  
  protected static int parseValueLength(ByteArrayInputStream paramByteArrayInputStream) {
    int i = paramByteArrayInputStream.read();
    i &= 0xFF;
    if (i <= 30)
      return i; 
    if (i == 31)
      return parseUnsignedInt(paramByteArrayInputStream); 
    throw new RuntimeException("Value length > LENGTH_QUOTE!");
  }
  
  protected static EncodedStringValue parseEncodedStringValue(ByteArrayInputStream paramByteArrayInputStream) {
    paramByteArrayInputStream.mark(1);
    int i = 0;
    int j = paramByteArrayInputStream.read();
    j &= 0xFF;
    if (j == 0)
      return new EncodedStringValue(""); 
    paramByteArrayInputStream.reset();
    if (j < 32) {
      parseValueLength(paramByteArrayInputStream);
      i = parseShortInteger(paramByteArrayInputStream);
    } 
    byte[] arrayOfByte = parseWapString(paramByteArrayInputStream, 0);
    if (i != 0)
      try {
        return new EncodedStringValue(i, arrayOfByte);
      } catch (Exception exception) {
        return null;
      }  
    return new EncodedStringValue((byte[])exception);
  }
  
  protected static byte[] parseWapString(ByteArrayInputStream paramByteArrayInputStream, int paramInt) {
    paramByteArrayInputStream.mark(1);
    int i = paramByteArrayInputStream.read();
    if (1 == paramInt && 34 == i) {
      paramByteArrayInputStream.mark(1);
    } else if (paramInt == 0 && 127 == i) {
      paramByteArrayInputStream.mark(1);
    } else {
      paramByteArrayInputStream.reset();
    } 
    return getWapString(paramByteArrayInputStream, paramInt);
  }
  
  protected static boolean isTokenCharacter(int paramInt) {
    if (paramInt < 33 || paramInt > 126)
      return false; 
    if (paramInt != 34 && paramInt != 44 && paramInt != 47 && paramInt != 123 && paramInt != 125 && paramInt != 40 && paramInt != 41)
      switch (paramInt) {
        default:
          switch (paramInt) {
            default:
              return true;
            case 91:
            case 92:
            case 93:
              break;
          } 
          break;
        case 58:
        case 59:
        case 60:
        case 61:
        case 62:
        case 63:
        case 64:
          break;
      }  
    return false;
  }
  
  protected static boolean isText(int paramInt) {
    if ((paramInt >= 32 && paramInt <= 126) || (paramInt >= 128 && paramInt <= 255))
      return true; 
    if (paramInt != 9 && paramInt != 10 && paramInt != 13)
      return false; 
    return true;
  }
  
  protected static byte[] getWapString(ByteArrayInputStream paramByteArrayInputStream, int paramInt) {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    int i = paramByteArrayInputStream.read();
    while (-1 != i && i != 0) {
      if (paramInt == 2) {
        if (isTokenCharacter(i))
          byteArrayOutputStream.write(i); 
      } else if (isText(i)) {
        byteArrayOutputStream.write(i);
      } 
      i = paramByteArrayInputStream.read();
    } 
    if (byteArrayOutputStream.size() > 0)
      return byteArrayOutputStream.toByteArray(); 
    return null;
  }
  
  protected static int extractByteValue(ByteArrayInputStream paramByteArrayInputStream) {
    int i = paramByteArrayInputStream.read();
    return i & 0xFF;
  }
  
  protected static int parseShortInteger(ByteArrayInputStream paramByteArrayInputStream) {
    int i = paramByteArrayInputStream.read();
    return i & 0x7F;
  }
  
  protected static long parseLongInteger(ByteArrayInputStream paramByteArrayInputStream) {
    int i = paramByteArrayInputStream.read();
    int j = i & 0xFF;
    if (j <= 8) {
      long l = 0L;
      for (i = 0; i < j; i++) {
        int k = paramByteArrayInputStream.read();
        l = (l << 8L) + (k & 0xFF);
      } 
      return l;
    } 
    throw new RuntimeException("Octet count greater than 8 and I can't represent that!");
  }
  
  protected static long parseIntegerValue(ByteArrayInputStream paramByteArrayInputStream) {
    paramByteArrayInputStream.mark(1);
    int i = paramByteArrayInputStream.read();
    paramByteArrayInputStream.reset();
    if (i > 127)
      return parseShortInteger(paramByteArrayInputStream); 
    return parseLongInteger(paramByteArrayInputStream);
  }
  
  protected static int skipWapValue(ByteArrayInputStream paramByteArrayInputStream, int paramInt) {
    byte[] arrayOfByte = new byte[paramInt];
    int i = paramByteArrayInputStream.read(arrayOfByte, 0, paramInt);
    if (i < paramInt)
      return -1; 
    return i;
  }
  
  protected static void parseContentTypeParams(ByteArrayInputStream paramByteArrayInputStream, HashMap<Integer, Object> paramHashMap, Integer paramInteger) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual available : ()I
    //   4: istore_3
    //   5: aload_2
    //   6: invokevirtual intValue : ()I
    //   9: istore #4
    //   11: iload #4
    //   13: ifle -> 494
    //   16: aload_0
    //   17: invokevirtual read : ()I
    //   20: istore #5
    //   22: iinc #4, -1
    //   25: iload #5
    //   27: sipush #129
    //   30: if_icmpeq -> 331
    //   33: iload #5
    //   35: sipush #131
    //   38: if_icmpeq -> 214
    //   41: iload #5
    //   43: sipush #133
    //   46: if_icmpeq -> 161
    //   49: iload #5
    //   51: sipush #151
    //   54: if_icmpeq -> 161
    //   57: iload #5
    //   59: sipush #153
    //   62: if_icmpeq -> 108
    //   65: iload #5
    //   67: sipush #137
    //   70: if_icmpeq -> 214
    //   73: iload #5
    //   75: sipush #138
    //   78: if_icmpeq -> 108
    //   81: iconst_m1
    //   82: aload_0
    //   83: iload #4
    //   85: invokestatic skipWapValue : (Ljava/io/ByteArrayInputStream;I)I
    //   88: if_icmpne -> 102
    //   91: ldc 'PduParser'
    //   93: ldc 'Corrupt Content-Type'
    //   95: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   98: pop
    //   99: goto -> 491
    //   102: iconst_0
    //   103: istore #4
    //   105: goto -> 491
    //   108: aload_0
    //   109: iconst_0
    //   110: invokestatic parseWapString : (Ljava/io/ByteArrayInputStream;I)[B
    //   113: astore #6
    //   115: aload #6
    //   117: ifnull -> 137
    //   120: aload_1
    //   121: ifnull -> 137
    //   124: aload_1
    //   125: sipush #153
    //   128: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   131: aload #6
    //   133: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   136: pop
    //   137: aload_0
    //   138: invokevirtual available : ()I
    //   141: istore #5
    //   143: aload_2
    //   144: invokevirtual intValue : ()I
    //   147: istore #4
    //   149: iload #4
    //   151: iload_3
    //   152: iload #5
    //   154: isub
    //   155: isub
    //   156: istore #4
    //   158: goto -> 491
    //   161: aload_0
    //   162: iconst_0
    //   163: invokestatic parseWapString : (Ljava/io/ByteArrayInputStream;I)[B
    //   166: astore #6
    //   168: aload #6
    //   170: ifnull -> 190
    //   173: aload_1
    //   174: ifnull -> 190
    //   177: aload_1
    //   178: sipush #151
    //   181: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   184: aload #6
    //   186: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   189: pop
    //   190: aload_0
    //   191: invokevirtual available : ()I
    //   194: istore #5
    //   196: aload_2
    //   197: invokevirtual intValue : ()I
    //   200: istore #4
    //   202: iload #4
    //   204: iload_3
    //   205: iload #5
    //   207: isub
    //   208: isub
    //   209: istore #4
    //   211: goto -> 491
    //   214: aload_0
    //   215: iconst_1
    //   216: invokevirtual mark : (I)V
    //   219: aload_0
    //   220: invokestatic extractByteValue : (Ljava/io/ByteArrayInputStream;)I
    //   223: istore #4
    //   225: aload_0
    //   226: invokevirtual reset : ()V
    //   229: iload #4
    //   231: bipush #127
    //   233: if_icmple -> 278
    //   236: aload_0
    //   237: invokestatic parseShortInteger : (Ljava/io/ByteArrayInputStream;)I
    //   240: istore #4
    //   242: iload #4
    //   244: getstatic com/google/android/mms/pdu/PduContentTypes.contentTypes : [Ljava/lang/String;
    //   247: arraylength
    //   248: if_icmpge -> 275
    //   251: getstatic com/google/android/mms/pdu/PduContentTypes.contentTypes : [Ljava/lang/String;
    //   254: iload #4
    //   256: aaload
    //   257: invokevirtual getBytes : ()[B
    //   260: astore #6
    //   262: aload_1
    //   263: sipush #131
    //   266: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   269: aload #6
    //   271: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   274: pop
    //   275: goto -> 307
    //   278: aload_0
    //   279: iconst_0
    //   280: invokestatic parseWapString : (Ljava/io/ByteArrayInputStream;I)[B
    //   283: astore #6
    //   285: aload #6
    //   287: ifnull -> 307
    //   290: aload_1
    //   291: ifnull -> 307
    //   294: aload_1
    //   295: sipush #131
    //   298: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   301: aload #6
    //   303: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   306: pop
    //   307: aload_0
    //   308: invokevirtual available : ()I
    //   311: istore #4
    //   313: aload_2
    //   314: invokevirtual intValue : ()I
    //   317: istore #5
    //   319: iload #5
    //   321: iload_3
    //   322: iload #4
    //   324: isub
    //   325: isub
    //   326: istore #4
    //   328: goto -> 491
    //   331: aload_0
    //   332: iconst_1
    //   333: invokevirtual mark : (I)V
    //   336: aload_0
    //   337: invokestatic extractByteValue : (Ljava/io/ByteArrayInputStream;)I
    //   340: istore #4
    //   342: aload_0
    //   343: invokevirtual reset : ()V
    //   346: iload #4
    //   348: bipush #32
    //   350: if_icmple -> 360
    //   353: iload #4
    //   355: bipush #127
    //   357: if_icmplt -> 365
    //   360: iload #4
    //   362: ifne -> 443
    //   365: aload_0
    //   366: iconst_0
    //   367: invokestatic parseWapString : (Ljava/io/ByteArrayInputStream;I)[B
    //   370: astore #6
    //   372: new java/lang/String
    //   375: astore #7
    //   377: aload #7
    //   379: aload #6
    //   381: invokespecial <init> : ([B)V
    //   384: aload #7
    //   386: invokestatic getMibEnumValue : (Ljava/lang/String;)I
    //   389: istore #4
    //   391: aload_1
    //   392: sipush #129
    //   395: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   398: iload #4
    //   400: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   403: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   406: pop
    //   407: goto -> 440
    //   410: astore #7
    //   412: ldc 'PduParser'
    //   414: aload #6
    //   416: invokestatic toString : ([B)Ljava/lang/String;
    //   419: aload #7
    //   421: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   424: pop
    //   425: aload_1
    //   426: sipush #129
    //   429: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   432: iconst_0
    //   433: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   436: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   439: pop
    //   440: goto -> 470
    //   443: aload_0
    //   444: invokestatic parseIntegerValue : (Ljava/io/ByteArrayInputStream;)J
    //   447: l2i
    //   448: istore #4
    //   450: aload_1
    //   451: ifnull -> 470
    //   454: aload_1
    //   455: sipush #129
    //   458: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   461: iload #4
    //   463: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   466: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   469: pop
    //   470: aload_0
    //   471: invokevirtual available : ()I
    //   474: istore #5
    //   476: aload_2
    //   477: invokevirtual intValue : ()I
    //   480: istore #4
    //   482: iload #4
    //   484: iload_3
    //   485: iload #5
    //   487: isub
    //   488: isub
    //   489: istore #4
    //   491: goto -> 11
    //   494: iload #4
    //   496: ifeq -> 507
    //   499: ldc 'PduParser'
    //   501: ldc 'Corrupt Content-Type'
    //   503: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   506: pop
    //   507: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1368	-> 0
    //   #1369	-> 0
    //   #1371	-> 0
    //   #1372	-> 5
    //   #1373	-> 5
    //   #1374	-> 11
    //   #1375	-> 16
    //   #1376	-> 22
    //   #1377	-> 22
    //   #1379	-> 25
    //   #1510	-> 81
    //   #1511	-> 91
    //   #1513	-> 102
    //   #1435	-> 108
    //   #1436	-> 115
    //   #1437	-> 124
    //   #1440	-> 137
    //   #1441	-> 143
    //   #1442	-> 149
    //   #1498	-> 161
    //   #1499	-> 168
    //   #1500	-> 177
    //   #1503	-> 190
    //   #1504	-> 196
    //   #1505	-> 202
    //   #1395	-> 214
    //   #1396	-> 219
    //   #1397	-> 225
    //   #1398	-> 229
    //   #1400	-> 236
    //   #1402	-> 242
    //   #1403	-> 251
    //   #1404	-> 262
    //   #1408	-> 275
    //   #1410	-> 278
    //   #1411	-> 285
    //   #1412	-> 294
    //   #1416	-> 307
    //   #1417	-> 313
    //   #1418	-> 319
    //   #1459	-> 331
    //   #1460	-> 336
    //   #1461	-> 342
    //   #1463	-> 346
    //   #1466	-> 365
    //   #1468	-> 372
    //   #1470	-> 391
    //   #1475	-> 407
    //   #1471	-> 410
    //   #1473	-> 412
    //   #1474	-> 425
    //   #1476	-> 440
    //   #1478	-> 443
    //   #1479	-> 450
    //   #1480	-> 454
    //   #1484	-> 470
    //   #1485	-> 476
    //   #1486	-> 482
    //   #1517	-> 491
    //   #1519	-> 494
    //   #1520	-> 499
    //   #1522	-> 507
    // Exception table:
    //   from	to	target	type
    //   372	391	410	java/io/UnsupportedEncodingException
    //   391	407	410	java/io/UnsupportedEncodingException
  }
  
  protected static byte[] parseContentType(ByteArrayInputStream paramByteArrayInputStream, HashMap<Integer, Object> paramHashMap) {
    byte[] arrayOfByte;
    paramByteArrayInputStream.mark(1);
    int i = paramByteArrayInputStream.read();
    paramByteArrayInputStream.reset();
    i &= 0xFF;
    if (i < 32) {
      i = parseValueLength(paramByteArrayInputStream);
      int j = paramByteArrayInputStream.available();
      paramByteArrayInputStream.mark(1);
      int k = paramByteArrayInputStream.read();
      paramByteArrayInputStream.reset();
      k &= 0xFF;
      if (k >= 32 && k <= 127) {
        arrayOfByte = parseWapString(paramByteArrayInputStream, 0);
      } else if (k > 127) {
        k = parseShortInteger(paramByteArrayInputStream);
        if (k < PduContentTypes.contentTypes.length) {
          arrayOfByte = PduContentTypes.contentTypes[k].getBytes();
        } else {
          paramByteArrayInputStream.reset();
          arrayOfByte = parseWapString(paramByteArrayInputStream, 0);
        } 
      } else {
        Log.e("PduParser", "Corrupt content-type");
        return PduContentTypes.contentTypes[0].getBytes();
      } 
      k = paramByteArrayInputStream.available();
      i -= j - k;
      if (i > 0)
        parseContentTypeParams(paramByteArrayInputStream, paramHashMap, Integer.valueOf(i)); 
      if (i < 0) {
        Log.e("PduParser", "Corrupt MMS message");
        return PduContentTypes.contentTypes[0].getBytes();
      } 
    } else if (i <= 127) {
      arrayOfByte = parseWapString(paramByteArrayInputStream, 0);
    } else {
      String[] arrayOfString = PduContentTypes.contentTypes;
      arrayOfByte = arrayOfString[parseShortInteger(paramByteArrayInputStream)].getBytes();
    } 
    return arrayOfByte;
  }
  
  protected boolean parsePartHeaders(ByteArrayInputStream paramByteArrayInputStream, PduPart paramPduPart, int paramInt) {
    int i = paramByteArrayInputStream.available();
    int j = paramInt;
    while (j > 0) {
      int k = paramByteArrayInputStream.read();
      j--;
      if (k > 127) {
        if (k != 142) {
          if (k != 174)
            if (k != 192) {
              if (k != 197) {
                if (-1 == skipWapValue(paramByteArrayInputStream, j)) {
                  Log.e("PduParser", "Corrupt Part headers");
                  return false;
                } 
                j = 0;
                continue;
              } 
            } else {
              byte[] arrayOfByte1 = parseWapString(paramByteArrayInputStream, 1);
              if (arrayOfByte1 != null)
                paramPduPart.setContentId(arrayOfByte1); 
              j = paramByteArrayInputStream.available();
              j = paramInt - i - j;
              continue;
            }  
          if (this.mParseContentDisposition) {
            k = parseValueLength(paramByteArrayInputStream);
            paramByteArrayInputStream.mark(1);
            j = paramByteArrayInputStream.available();
            int m = paramByteArrayInputStream.read();
            if (m == 128) {
              paramPduPart.setContentDisposition(PduPart.DISPOSITION_FROM_DATA);
            } else if (m == 129) {
              paramPduPart.setContentDisposition(PduPart.DISPOSITION_ATTACHMENT);
            } else if (m == 130) {
              paramPduPart.setContentDisposition(PduPart.DISPOSITION_INLINE);
            } else {
              paramByteArrayInputStream.reset();
              paramPduPart.setContentDisposition(parseWapString(paramByteArrayInputStream, 0));
            } 
            m = paramByteArrayInputStream.available();
            if (j - m < k) {
              m = paramByteArrayInputStream.read();
              if (m == 152)
                paramPduPart.setFilename(parseWapString(paramByteArrayInputStream, 0)); 
              m = paramByteArrayInputStream.available();
              if (j - m < k) {
                j = k - j - m;
                byte[] arrayOfByte1 = new byte[j];
                paramByteArrayInputStream.read(arrayOfByte1, 0, j);
              } 
            } 
            j = paramByteArrayInputStream.available();
            j = paramInt - i - j;
          } 
          continue;
        } 
        byte[] arrayOfByte = parseWapString(paramByteArrayInputStream, 0);
        if (arrayOfByte != null)
          paramPduPart.setContentLocation(arrayOfByte); 
        j = paramByteArrayInputStream.available();
        j = paramInt - i - j;
        continue;
      } 
      if (k >= 32 && k <= 127) {
        byte[] arrayOfByte2 = parseWapString(paramByteArrayInputStream, 0);
        byte[] arrayOfByte1 = parseWapString(paramByteArrayInputStream, 0);
        String str = new String(arrayOfByte2);
        if (true == "Content-Transfer-Encoding".equalsIgnoreCase(str))
          paramPduPart.setContentTransferEncoding(arrayOfByte1); 
        j = paramByteArrayInputStream.available();
        j = paramInt - i - j;
        continue;
      } 
      if (-1 == skipWapValue(paramByteArrayInputStream, j)) {
        Log.e("PduParser", "Corrupt Part headers");
        return false;
      } 
      j = 0;
    } 
    if (j != 0) {
      Log.e("PduParser", "Corrupt Part headers");
      return false;
    } 
    return true;
  }
  
  private static int checkPartPosition(PduPart paramPduPart) {
    byte[] arrayOfByte;
    if (mTypeParam == null && mStartParam == null)
      return 1; 
    if (mStartParam != null) {
      arrayOfByte = paramPduPart.getContentId();
      if (arrayOfByte != null && true == 
        Arrays.equals(mStartParam, arrayOfByte))
        return 0; 
      return 1;
    } 
    if (mTypeParam != null) {
      arrayOfByte = arrayOfByte.getContentType();
      if (arrayOfByte != null && true == 
        Arrays.equals(mTypeParam, arrayOfByte))
        return 0; 
    } 
    return 1;
  }
  
  protected static boolean checkMandatoryHeader(PduHeaders paramPduHeaders) {
    EncodedStringValue[] arrayOfEncodedStringValue;
    long l;
    EncodedStringValue encodedStringValue3;
    byte[] arrayOfByte3;
    EncodedStringValue encodedStringValue2;
    if (paramPduHeaders == null)
      return false; 
    int i = paramPduHeaders.getOctet(140);
    int j = paramPduHeaders.getOctet(141);
    if (j == 0)
      return false; 
    switch (i) {
      default:
        return false;
      case 136:
        l = paramPduHeaders.getLongInteger(133);
        if (-1L == l)
          return false; 
        encodedStringValue3 = paramPduHeaders.getEncodedStringValue(137);
        if (encodedStringValue3 == null)
          return false; 
        arrayOfByte3 = paramPduHeaders.getTextString(139);
        if (arrayOfByte3 == null)
          return false; 
        j = paramPduHeaders.getOctet(155);
        if (j == 0)
          return false; 
        arrayOfEncodedStringValue = paramPduHeaders.getEncodedStringValues(151);
        if (arrayOfEncodedStringValue == null)
          return false; 
        return true;
      case 135:
        encodedStringValue2 = arrayOfEncodedStringValue.getEncodedStringValue(137);
        if (encodedStringValue2 == null)
          return false; 
        arrayOfByte2 = arrayOfEncodedStringValue.getTextString(139);
        if (arrayOfByte2 == null)
          return false; 
        j = arrayOfEncodedStringValue.getOctet(155);
        if (j == 0)
          return false; 
        arrayOfEncodedStringValue = arrayOfEncodedStringValue.getEncodedStringValues(151);
        if (arrayOfEncodedStringValue == null)
          return false; 
        return true;
      case 134:
        l = arrayOfEncodedStringValue.getLongInteger(133);
        if (-1L == l)
          return false; 
        arrayOfByte2 = arrayOfEncodedStringValue.getTextString(139);
        if (arrayOfByte2 == null)
          return false; 
        j = arrayOfEncodedStringValue.getOctet(149);
        if (j == 0)
          return false; 
        arrayOfEncodedStringValue = arrayOfEncodedStringValue.getEncodedStringValues(151);
        if (arrayOfEncodedStringValue == null)
          return false; 
        return true;
      case 133:
        arrayOfByte1 = arrayOfEncodedStringValue.getTextString(152);
        if (arrayOfByte1 == null)
          return false; 
        return true;
      case 132:
        arrayOfByte2 = arrayOfByte1.getTextString(132);
        if (arrayOfByte2 == null)
          return false; 
        l = arrayOfByte1.getLongInteger(133);
        if (-1L == l)
          return false; 
        return true;
      case 131:
        j = arrayOfByte1.getOctet(149);
        if (j == 0)
          return false; 
        arrayOfByte1 = arrayOfByte1.getTextString(152);
        if (arrayOfByte1 == null)
          return false; 
        return true;
      case 130:
        arrayOfByte2 = arrayOfByte1.getTextString(131);
        if (arrayOfByte2 == null)
          return false; 
        l = arrayOfByte1.getLongInteger(136);
        if (-1L == l)
          return false; 
        arrayOfByte2 = arrayOfByte1.getTextString(138);
        if (arrayOfByte2 == null)
          return false; 
        l = arrayOfByte1.getLongInteger(142);
        if (-1L == l)
          return false; 
        arrayOfByte1 = arrayOfByte1.getTextString(152);
        if (arrayOfByte1 == null)
          return false; 
        return true;
      case 129:
        j = arrayOfByte1.getOctet(146);
        if (j == 0)
          return false; 
        arrayOfByte1 = arrayOfByte1.getTextString(152);
        if (arrayOfByte1 == null)
          return false; 
        return true;
      case 128:
        break;
    } 
    byte[] arrayOfByte2 = arrayOfByte1.getTextString(132);
    if (arrayOfByte2 == null)
      return false; 
    EncodedStringValue encodedStringValue1 = arrayOfByte1.getEncodedStringValue(137);
    if (encodedStringValue1 == null)
      return false; 
    byte[] arrayOfByte1 = arrayOfByte1.getTextString(152);
    if (arrayOfByte1 == null)
      return false; 
    return true;
  }
}
