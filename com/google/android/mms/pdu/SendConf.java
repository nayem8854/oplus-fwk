package com.google.android.mms.pdu;

import com.google.android.mms.InvalidHeaderValueException;

public class SendConf extends GenericPdu {
  public SendConf() throws InvalidHeaderValueException {
    setMessageType(129);
  }
  
  SendConf(PduHeaders paramPduHeaders) {
    super(paramPduHeaders);
  }
  
  public byte[] getMessageId() {
    return this.mPduHeaders.getTextString(139);
  }
  
  public void setMessageId(byte[] paramArrayOfbyte) {
    this.mPduHeaders.setTextString(paramArrayOfbyte, 139);
  }
  
  public int getResponseStatus() {
    return this.mPduHeaders.getOctet(146);
  }
  
  public void setResponseStatus(int paramInt) throws InvalidHeaderValueException {
    this.mPduHeaders.setOctet(paramInt, 146);
  }
  
  public byte[] getTransactionId() {
    return this.mPduHeaders.getTextString(152);
  }
  
  public void setTransactionId(byte[] paramArrayOfbyte) {
    this.mPduHeaders.setTextString(paramArrayOfbyte, 152);
  }
}
