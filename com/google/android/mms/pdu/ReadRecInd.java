package com.google.android.mms.pdu;

import com.google.android.mms.InvalidHeaderValueException;

public class ReadRecInd extends GenericPdu {
  public ReadRecInd(EncodedStringValue paramEncodedStringValue, byte[] paramArrayOfbyte, int paramInt1, int paramInt2, EncodedStringValue[] paramArrayOfEncodedStringValue) throws InvalidHeaderValueException {
    setMessageType(135);
    setFrom(paramEncodedStringValue);
    setMessageId(paramArrayOfbyte);
    setMmsVersion(paramInt1);
    setTo(paramArrayOfEncodedStringValue);
    setReadStatus(paramInt2);
  }
  
  ReadRecInd(PduHeaders paramPduHeaders) {
    super(paramPduHeaders);
  }
  
  public long getDate() {
    return this.mPduHeaders.getLongInteger(133);
  }
  
  public void setDate(long paramLong) {
    this.mPduHeaders.setLongInteger(paramLong, 133);
  }
  
  public byte[] getMessageId() {
    return this.mPduHeaders.getTextString(139);
  }
  
  public void setMessageId(byte[] paramArrayOfbyte) {
    this.mPduHeaders.setTextString(paramArrayOfbyte, 139);
  }
  
  public EncodedStringValue[] getTo() {
    return this.mPduHeaders.getEncodedStringValues(151);
  }
  
  public void setTo(EncodedStringValue[] paramArrayOfEncodedStringValue) {
    this.mPduHeaders.setEncodedStringValues(paramArrayOfEncodedStringValue, 151);
  }
  
  public int getReadStatus() {
    return this.mPduHeaders.getOctet(155);
  }
  
  public void setReadStatus(int paramInt) throws InvalidHeaderValueException {
    this.mPduHeaders.setOctet(paramInt, 155);
  }
}
