package com.google.android.mms.pdu;

import android.net.Uri;
import java.util.HashMap;
import java.util.Map;

public class PduPart {
  static final byte[] DISPOSITION_FROM_DATA = "from-data".getBytes();
  
  static {
    DISPOSITION_ATTACHMENT = "attachment".getBytes();
    DISPOSITION_INLINE = "inline".getBytes();
  }
  
  private Map<Integer, Object> mPartHeader = null;
  
  private Uri mUri = null;
  
  private byte[] mPartData = null;
  
  public static final String CONTENT_TRANSFER_ENCODING = "Content-Transfer-Encoding";
  
  static final byte[] DISPOSITION_ATTACHMENT;
  
  static final byte[] DISPOSITION_INLINE;
  
  public static final String P_7BIT = "7bit";
  
  public static final String P_8BIT = "8bit";
  
  public static final String P_BASE64 = "base64";
  
  public static final String P_BINARY = "binary";
  
  public static final int P_CHARSET = 129;
  
  public static final int P_COMMENT = 155;
  
  public static final int P_CONTENT_DISPOSITION = 197;
  
  public static final int P_CONTENT_ID = 192;
  
  public static final int P_CONTENT_LOCATION = 142;
  
  public static final int P_CONTENT_TRANSFER_ENCODING = 200;
  
  public static final int P_CONTENT_TYPE = 145;
  
  public static final int P_CREATION_DATE = 147;
  
  public static final int P_CT_MR_TYPE = 137;
  
  public static final int P_DEP_COMMENT = 140;
  
  public static final int P_DEP_CONTENT_DISPOSITION = 174;
  
  public static final int P_DEP_DOMAIN = 141;
  
  public static final int P_DEP_FILENAME = 134;
  
  public static final int P_DEP_NAME = 133;
  
  public static final int P_DEP_PATH = 143;
  
  public static final int P_DEP_START = 138;
  
  public static final int P_DEP_START_INFO = 139;
  
  public static final int P_DIFFERENCES = 135;
  
  public static final int P_DISPOSITION_ATTACHMENT = 129;
  
  public static final int P_DISPOSITION_FROM_DATA = 128;
  
  public static final int P_DISPOSITION_INLINE = 130;
  
  public static final int P_DOMAIN = 156;
  
  public static final int P_FILENAME = 152;
  
  public static final int P_LEVEL = 130;
  
  public static final int P_MAC = 146;
  
  public static final int P_MAX_AGE = 142;
  
  public static final int P_MODIFICATION_DATE = 148;
  
  public static final int P_NAME = 151;
  
  public static final int P_PADDING = 136;
  
  public static final int P_PATH = 157;
  
  public static final int P_Q = 128;
  
  public static final String P_QUOTED_PRINTABLE = "quoted-printable";
  
  public static final int P_READ_DATE = 149;
  
  public static final int P_SEC = 145;
  
  public static final int P_SECURE = 144;
  
  public static final int P_SIZE = 150;
  
  public static final int P_START = 153;
  
  public static final int P_START_INFO = 154;
  
  public static final int P_TYPE = 131;
  
  private static final String TAG = "PduPart";
  
  public PduPart() {
    this.mPartHeader = new HashMap<>();
  }
  
  public void setData(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte == null)
      return; 
    byte[] arrayOfByte = new byte[paramArrayOfbyte.length];
    System.arraycopy(paramArrayOfbyte, 0, arrayOfByte, 0, paramArrayOfbyte.length);
  }
  
  public byte[] getData() {
    byte[] arrayOfByte1 = this.mPartData;
    if (arrayOfByte1 == null)
      return null; 
    byte[] arrayOfByte2 = new byte[arrayOfByte1.length];
    System.arraycopy(arrayOfByte1, 0, arrayOfByte2, 0, arrayOfByte1.length);
    return arrayOfByte2;
  }
  
  public int getDataLength() {
    byte[] arrayOfByte = this.mPartData;
    if (arrayOfByte != null)
      return arrayOfByte.length; 
    return 0;
  }
  
  public void setDataUri(Uri paramUri) {
    this.mUri = paramUri;
  }
  
  public Uri getDataUri() {
    return this.mUri;
  }
  
  public void setContentId(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null && paramArrayOfbyte.length != 0) {
      if (paramArrayOfbyte.length > 1 && (char)paramArrayOfbyte[0] == '<' && (char)paramArrayOfbyte[paramArrayOfbyte.length - 1] == '>') {
        this.mPartHeader.put(Integer.valueOf(192), paramArrayOfbyte);
        return;
      } 
      byte[] arrayOfByte = new byte[paramArrayOfbyte.length + 2];
      arrayOfByte[0] = 60;
      arrayOfByte[arrayOfByte.length - 1] = 62;
      System.arraycopy(paramArrayOfbyte, 0, arrayOfByte, 1, paramArrayOfbyte.length);
      this.mPartHeader.put(Integer.valueOf(192), arrayOfByte);
      return;
    } 
    throw new IllegalArgumentException("Content-Id may not be null or empty.");
  }
  
  public byte[] getContentId() {
    return (byte[])this.mPartHeader.get(Integer.valueOf(192));
  }
  
  public void setCharset(int paramInt) {
    this.mPartHeader.put(Integer.valueOf(129), Integer.valueOf(paramInt));
  }
  
  public int getCharset() {
    Integer integer = (Integer)this.mPartHeader.get(Integer.valueOf(129));
    if (integer == null)
      return 0; 
    return integer.intValue();
  }
  
  public void setContentLocation(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null) {
      this.mPartHeader.put(Integer.valueOf(142), paramArrayOfbyte);
      return;
    } 
    throw new NullPointerException("null content-location");
  }
  
  public byte[] getContentLocation() {
    return (byte[])this.mPartHeader.get(Integer.valueOf(142));
  }
  
  public void setContentDisposition(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null) {
      this.mPartHeader.put(Integer.valueOf(197), paramArrayOfbyte);
      return;
    } 
    throw new NullPointerException("null content-disposition");
  }
  
  public byte[] getContentDisposition() {
    return (byte[])this.mPartHeader.get(Integer.valueOf(197));
  }
  
  public void setContentType(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null) {
      this.mPartHeader.put(Integer.valueOf(145), paramArrayOfbyte);
      return;
    } 
    throw new NullPointerException("null content-type");
  }
  
  public byte[] getContentType() {
    return (byte[])this.mPartHeader.get(Integer.valueOf(145));
  }
  
  public void setContentTransferEncoding(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null) {
      this.mPartHeader.put(Integer.valueOf(200), paramArrayOfbyte);
      return;
    } 
    throw new NullPointerException("null content-transfer-encoding");
  }
  
  public byte[] getContentTransferEncoding() {
    return (byte[])this.mPartHeader.get(Integer.valueOf(200));
  }
  
  public void setName(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null) {
      this.mPartHeader.put(Integer.valueOf(151), paramArrayOfbyte);
      return;
    } 
    throw new NullPointerException("null content-id");
  }
  
  public byte[] getName() {
    return (byte[])this.mPartHeader.get(Integer.valueOf(151));
  }
  
  public void setFilename(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null) {
      this.mPartHeader.put(Integer.valueOf(152), paramArrayOfbyte);
      return;
    } 
    throw new NullPointerException("null content-id");
  }
  
  public byte[] getFilename() {
    return (byte[])this.mPartHeader.get(Integer.valueOf(152));
  }
  
  public String generateLocation() {
    StringBuilder stringBuilder;
    byte[] arrayOfByte1 = (byte[])this.mPartHeader.get(Integer.valueOf(151));
    byte[] arrayOfByte2 = arrayOfByte1;
    if (arrayOfByte1 == null) {
      arrayOfByte1 = (byte[])this.mPartHeader.get(Integer.valueOf(152));
      arrayOfByte2 = arrayOfByte1;
      if (arrayOfByte1 == null)
        arrayOfByte2 = (byte[])this.mPartHeader.get(Integer.valueOf(142)); 
    } 
    if (arrayOfByte2 == null) {
      arrayOfByte1 = (byte[])this.mPartHeader.get(Integer.valueOf(192));
      stringBuilder = new StringBuilder();
      stringBuilder.append("cid:");
      stringBuilder.append(new String(arrayOfByte1));
      return stringBuilder.toString();
    } 
    return new String((byte[])stringBuilder);
  }
}
