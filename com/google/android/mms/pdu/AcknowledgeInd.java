package com.google.android.mms.pdu;

import com.google.android.mms.InvalidHeaderValueException;

public class AcknowledgeInd extends GenericPdu {
  public AcknowledgeInd(int paramInt, byte[] paramArrayOfbyte) throws InvalidHeaderValueException {
    setMessageType(133);
    setMmsVersion(paramInt);
    setTransactionId(paramArrayOfbyte);
  }
  
  AcknowledgeInd(PduHeaders paramPduHeaders) {
    super(paramPduHeaders);
  }
  
  public int getReportAllowed() {
    return this.mPduHeaders.getOctet(145);
  }
  
  public void setReportAllowed(int paramInt) throws InvalidHeaderValueException {
    this.mPduHeaders.setOctet(paramInt, 145);
  }
  
  public byte[] getTransactionId() {
    return this.mPduHeaders.getTextString(152);
  }
  
  public void setTransactionId(byte[] paramArrayOfbyte) {
    this.mPduHeaders.setTextString(paramArrayOfbyte, 152);
  }
}
