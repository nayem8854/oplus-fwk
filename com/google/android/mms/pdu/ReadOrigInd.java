package com.google.android.mms.pdu;

import com.google.android.mms.InvalidHeaderValueException;

public class ReadOrigInd extends GenericPdu {
  public ReadOrigInd() throws InvalidHeaderValueException {
    setMessageType(136);
  }
  
  ReadOrigInd(PduHeaders paramPduHeaders) {
    super(paramPduHeaders);
  }
  
  public long getDate() {
    return this.mPduHeaders.getLongInteger(133);
  }
  
  public void setDate(long paramLong) {
    this.mPduHeaders.setLongInteger(paramLong, 133);
  }
  
  public EncodedStringValue getFrom() {
    return this.mPduHeaders.getEncodedStringValue(137);
  }
  
  public void setFrom(EncodedStringValue paramEncodedStringValue) {
    this.mPduHeaders.setEncodedStringValue(paramEncodedStringValue, 137);
  }
  
  public byte[] getMessageId() {
    return this.mPduHeaders.getTextString(139);
  }
  
  public void setMessageId(byte[] paramArrayOfbyte) {
    this.mPduHeaders.setTextString(paramArrayOfbyte, 139);
  }
  
  public int getReadStatus() {
    return this.mPduHeaders.getOctet(155);
  }
  
  public void setReadStatus(int paramInt) throws InvalidHeaderValueException {
    this.mPduHeaders.setOctet(paramInt, 155);
  }
  
  public EncodedStringValue[] getTo() {
    return this.mPduHeaders.getEncodedStringValues(151);
  }
  
  public void setTo(EncodedStringValue[] paramArrayOfEncodedStringValue) {
    this.mPduHeaders.setEncodedStringValues(paramArrayOfEncodedStringValue, 151);
  }
}
