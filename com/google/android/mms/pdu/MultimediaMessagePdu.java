package com.google.android.mms.pdu;

import com.google.android.mms.InvalidHeaderValueException;

public class MultimediaMessagePdu extends GenericPdu {
  private PduBody mMessageBody;
  
  public MultimediaMessagePdu() {}
  
  public MultimediaMessagePdu(PduHeaders paramPduHeaders, PduBody paramPduBody) {
    super(paramPduHeaders);
    this.mMessageBody = paramPduBody;
  }
  
  MultimediaMessagePdu(PduHeaders paramPduHeaders) {
    super(paramPduHeaders);
  }
  
  public PduBody getBody() {
    return this.mMessageBody;
  }
  
  public void setBody(PduBody paramPduBody) {
    this.mMessageBody = paramPduBody;
  }
  
  public EncodedStringValue getSubject() {
    return this.mPduHeaders.getEncodedStringValue(150);
  }
  
  public void setSubject(EncodedStringValue paramEncodedStringValue) {
    this.mPduHeaders.setEncodedStringValue(paramEncodedStringValue, 150);
  }
  
  public EncodedStringValue[] getTo() {
    return this.mPduHeaders.getEncodedStringValues(151);
  }
  
  public void addTo(EncodedStringValue paramEncodedStringValue) {
    this.mPduHeaders.appendEncodedStringValue(paramEncodedStringValue, 151);
  }
  
  public int getPriority() {
    return this.mPduHeaders.getOctet(143);
  }
  
  public void setPriority(int paramInt) throws InvalidHeaderValueException {
    this.mPduHeaders.setOctet(paramInt, 143);
  }
  
  public long getDate() {
    return this.mPduHeaders.getLongInteger(133);
  }
  
  public void setDate(long paramLong) {
    this.mPduHeaders.setLongInteger(paramLong, 133);
  }
}
