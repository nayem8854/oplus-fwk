package com.google.android.mms.pdu;

public class Base64 {
  static final int BASELENGTH = 255;
  
  static final int FOURBYTE = 4;
  
  static final byte PAD = 61;
  
  private static byte[] base64Alphabet = new byte[255];
  
  static {
    byte b;
    for (b = 0; b < 'ÿ'; b++)
      base64Alphabet[b] = -1; 
    for (b = 90; b >= 65; b--)
      base64Alphabet[b] = (byte)(b - 65); 
    for (b = 122; b >= 97; b--)
      base64Alphabet[b] = (byte)(b - 97 + 26); 
    for (b = 57; b >= 48; b--)
      base64Alphabet[b] = (byte)(b - 48 + 52); 
    byte[] arrayOfByte = base64Alphabet;
    arrayOfByte[43] = 62;
    arrayOfByte[47] = 63;
  }
  
  public static byte[] decodeBase64(byte[] paramArrayOfbyte) {
    byte[] arrayOfByte = discardNonBase64(paramArrayOfbyte);
    if (arrayOfByte.length == 0)
      return new byte[0]; 
    int i = arrayOfByte.length / 4;
    byte b = 0;
    int j = arrayOfByte.length;
    while (arrayOfByte[j - 1] == 61) {
      int k = j - 1;
      if (k == 0)
        return new byte[0]; 
    } 
    paramArrayOfbyte = new byte[j - i];
    for (j = 0; j < i; j++) {
      int k = j * 4;
      byte b2 = arrayOfByte[k + 2];
      byte b3 = arrayOfByte[k + 3];
      byte arrayOfByte1[] = base64Alphabet, b1 = arrayOfByte1[arrayOfByte[k]];
      k = arrayOfByte1[arrayOfByte[k + 1]];
      if (b2 != 61 && b3 != 61) {
        b2 = arrayOfByte1[b2];
        b3 = arrayOfByte1[b3];
        paramArrayOfbyte[b] = (byte)(b1 << 2 | k >> 4);
        paramArrayOfbyte[b + 1] = (byte)((k & 0xF) << 4 | b2 >> 2 & 0xF);
        paramArrayOfbyte[b + 2] = (byte)(b2 << 6 | b3);
      } else if (b2 == 61) {
        paramArrayOfbyte[b] = (byte)(b1 << 2 | k >> 4);
      } else if (b3 == 61) {
        b3 = base64Alphabet[b2];
        paramArrayOfbyte[b] = (byte)(b1 << 2 | k >> 4);
        paramArrayOfbyte[b + 1] = (byte)((k & 0xF) << 4 | b3 >> 2 & 0xF);
      } 
      b += 3;
    } 
    return paramArrayOfbyte;
  }
  
  private static boolean isBase64(byte paramByte) {
    if (paramByte == 61)
      return true; 
    if (base64Alphabet[paramByte] == -1)
      return false; 
    return true;
  }
  
  static byte[] discardNonBase64(byte[] paramArrayOfbyte) {
    byte[] arrayOfByte = new byte[paramArrayOfbyte.length];
    int i = 0;
    for (byte b = 0; b < paramArrayOfbyte.length; b++, i = j) {
      int j = i;
      if (isBase64(paramArrayOfbyte[b])) {
        arrayOfByte[i] = paramArrayOfbyte[b];
        j = i + 1;
      } 
    } 
    paramArrayOfbyte = new byte[i];
    System.arraycopy(arrayOfByte, 0, paramArrayOfbyte, 0, i);
    return paramArrayOfbyte;
  }
}
