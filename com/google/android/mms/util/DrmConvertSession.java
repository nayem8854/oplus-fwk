package com.google.android.mms.util;

import android.content.Context;
import android.drm.DrmConvertedStatus;
import android.drm.DrmManagerClient;
import android.util.Log;

public class DrmConvertSession {
  public static final int STATUS_FILE_ERROR = 492;
  
  public static final int STATUS_NOT_ACCEPTABLE = 406;
  
  public static final int STATUS_SUCCESS = 200;
  
  public static final int STATUS_UNKNOWN_ERROR = 491;
  
  private static final String TAG = "DrmConvertSession";
  
  private int mConvertSessionId;
  
  private DrmManagerClient mDrmClient;
  
  private DrmConvertSession(DrmManagerClient paramDrmManagerClient, int paramInt) {
    this.mDrmClient = paramDrmManagerClient;
    this.mConvertSessionId = paramInt;
  }
  
  public static DrmConvertSession open(Context paramContext, String paramString) {
    DrmManagerClient drmManagerClient1 = null, drmManagerClient2 = null, drmManagerClient3 = null;
    byte b = -1;
    DrmManagerClient drmManagerClient4 = drmManagerClient2;
    int i = b;
    if (paramContext != null) {
      drmManagerClient4 = drmManagerClient2;
      i = b;
      if (paramString != null) {
        drmManagerClient4 = drmManagerClient2;
        i = b;
        if (!paramString.equals("")) {
          drmManagerClient2 = drmManagerClient3;
          drmManagerClient4 = drmManagerClient1;
          try {
            DrmManagerClient drmManagerClient6 = new DrmManagerClient();
            drmManagerClient2 = drmManagerClient3;
            drmManagerClient4 = drmManagerClient1;
            this(paramContext);
            DrmManagerClient drmManagerClient5 = drmManagerClient6;
            try {
              i = drmManagerClient5.openConvertSession(paramString);
              drmManagerClient4 = drmManagerClient5;
            } catch (IllegalArgumentException illegalArgumentException) {
              drmManagerClient2 = drmManagerClient5;
              drmManagerClient4 = drmManagerClient5;
              StringBuilder stringBuilder = new StringBuilder();
              drmManagerClient2 = drmManagerClient5;
              drmManagerClient4 = drmManagerClient5;
              this();
              drmManagerClient2 = drmManagerClient5;
              drmManagerClient4 = drmManagerClient5;
              stringBuilder.append("Conversion of Mimetype: ");
              drmManagerClient2 = drmManagerClient5;
              drmManagerClient4 = drmManagerClient5;
              stringBuilder.append(paramString);
              drmManagerClient2 = drmManagerClient5;
              drmManagerClient4 = drmManagerClient5;
              stringBuilder.append(" is not supported.");
              drmManagerClient2 = drmManagerClient5;
              drmManagerClient4 = drmManagerClient5;
              Log.w("DrmConvertSession", stringBuilder.toString(), illegalArgumentException);
              drmManagerClient4 = drmManagerClient5;
              i = b;
            } catch (IllegalStateException illegalStateException) {
              drmManagerClient2 = drmManagerClient5;
              drmManagerClient4 = drmManagerClient5;
              Log.w("DrmConvertSession", "Could not access Open DrmFramework.", illegalStateException);
              drmManagerClient4 = drmManagerClient5;
              i = b;
            } 
          } catch (IllegalArgumentException illegalArgumentException) {
            Log.w("DrmConvertSession", "DrmManagerClient instance could not be created, context is Illegal.");
            i = b;
          } catch (IllegalStateException illegalStateException) {
            Log.w("DrmConvertSession", "DrmManagerClient didn't initialize properly.");
            drmManagerClient4 = drmManagerClient2;
            i = b;
          } 
        } 
      } 
    } 
    if (drmManagerClient4 == null || i < 0)
      return null; 
    return new DrmConvertSession(drmManagerClient4, i);
  }
  
  public byte[] convert(byte[] paramArrayOfbyte, int paramInt) {
    IllegalStateException illegalStateException = null;
    byte[] arrayOfByte = null;
    if (paramArrayOfbyte != null) {
      try {
        DrmConvertedStatus drmConvertedStatus;
        if (paramInt != paramArrayOfbyte.length) {
          byte[] arrayOfByte1 = new byte[paramInt];
          System.arraycopy(paramArrayOfbyte, 0, arrayOfByte1, 0, paramInt);
          drmConvertedStatus = this.mDrmClient.convertData(this.mConvertSessionId, arrayOfByte1);
        } else {
          drmConvertedStatus = this.mDrmClient.convertData(this.mConvertSessionId, paramArrayOfbyte);
        } 
        paramArrayOfbyte = arrayOfByte;
        if (drmConvertedStatus != null) {
          paramArrayOfbyte = arrayOfByte;
          if (drmConvertedStatus.statusCode == 1) {
            paramArrayOfbyte = arrayOfByte;
            if (drmConvertedStatus.convertedData != null)
              paramArrayOfbyte = drmConvertedStatus.convertedData; 
          } 
        } 
      } catch (IllegalArgumentException illegalArgumentException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Buffer with data to convert is illegal. Convertsession: ");
        stringBuilder.append(this.mConvertSessionId);
        Log.w("DrmConvertSession", stringBuilder.toString(), illegalArgumentException);
        byte[] arrayOfByte1 = arrayOfByte;
      } catch (IllegalStateException illegalStateException1) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Could not convert data. Convertsession: ");
        stringBuilder.append(this.mConvertSessionId);
        Log.w("DrmConvertSession", stringBuilder.toString(), illegalStateException1);
        illegalStateException1 = illegalStateException;
      } 
      return (byte[])illegalStateException1;
    } 
    throw new IllegalArgumentException("Parameter inBuffer is null");
  }
  
  public int close(String paramString) {
    // Byte code:
    //   0: sipush #491
    //   3: istore_2
    //   4: aload_0
    //   5: getfield mDrmClient : Landroid/drm/DrmManagerClient;
    //   8: astore_3
    //   9: iload_2
    //   10: istore #4
    //   12: aload_3
    //   13: ifnull -> 1140
    //   16: aload_0
    //   17: getfield mConvertSessionId : I
    //   20: istore #5
    //   22: iload_2
    //   23: istore #4
    //   25: iload #5
    //   27: iflt -> 1140
    //   30: iload_2
    //   31: istore #4
    //   33: aload_3
    //   34: iload #5
    //   36: invokevirtual closeConvertSession : (I)Landroid/drm/DrmConvertedStatus;
    //   39: astore #6
    //   41: aload #6
    //   43: ifnull -> 1096
    //   46: iload_2
    //   47: istore #4
    //   49: aload #6
    //   51: getfield statusCode : I
    //   54: iconst_1
    //   55: if_icmpne -> 1096
    //   58: iload_2
    //   59: istore #4
    //   61: aload #6
    //   63: getfield convertedData : [B
    //   66: astore_3
    //   67: aload_3
    //   68: ifnonnull -> 74
    //   71: goto -> 1096
    //   74: aconst_null
    //   75: astore #7
    //   77: aconst_null
    //   78: astore #8
    //   80: aconst_null
    //   81: astore #9
    //   83: aconst_null
    //   84: astore #10
    //   86: aconst_null
    //   87: astore #11
    //   89: iload_2
    //   90: istore #5
    //   92: aload #11
    //   94: astore_3
    //   95: aload #7
    //   97: astore #12
    //   99: aload #8
    //   101: astore #13
    //   103: aload #9
    //   105: astore #14
    //   107: aload #10
    //   109: astore #15
    //   111: new java/io/RandomAccessFile
    //   114: astore #16
    //   116: iload_2
    //   117: istore #5
    //   119: aload #11
    //   121: astore_3
    //   122: aload #7
    //   124: astore #12
    //   126: aload #8
    //   128: astore #13
    //   130: aload #9
    //   132: astore #14
    //   134: aload #10
    //   136: astore #15
    //   138: aload #16
    //   140: aload_1
    //   141: ldc 'rw'
    //   143: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;)V
    //   146: aload #16
    //   148: astore #11
    //   150: iload_2
    //   151: istore #5
    //   153: aload #11
    //   155: astore_3
    //   156: aload #11
    //   158: astore #12
    //   160: aload #11
    //   162: astore #13
    //   164: aload #11
    //   166: astore #14
    //   168: aload #11
    //   170: astore #15
    //   172: aload #11
    //   174: aload #6
    //   176: getfield offset : I
    //   179: i2l
    //   180: invokevirtual seek : (J)V
    //   183: iload_2
    //   184: istore #5
    //   186: aload #11
    //   188: astore_3
    //   189: aload #11
    //   191: astore #12
    //   193: aload #11
    //   195: astore #13
    //   197: aload #11
    //   199: astore #14
    //   201: aload #11
    //   203: astore #15
    //   205: aload #11
    //   207: aload #6
    //   209: getfield convertedData : [B
    //   212: invokevirtual write : ([B)V
    //   215: sipush #200
    //   218: istore #5
    //   220: iload #5
    //   222: istore #4
    //   224: aload #11
    //   226: invokevirtual close : ()V
    //   229: iload #5
    //   231: istore #4
    //   233: goto -> 1101
    //   236: astore_3
    //   237: sipush #492
    //   240: istore #5
    //   242: iload #5
    //   244: istore #4
    //   246: new java/lang/StringBuilder
    //   249: astore #11
    //   251: iload #5
    //   253: istore #4
    //   255: aload #11
    //   257: invokespecial <init> : ()V
    //   260: iload #5
    //   262: istore #4
    //   264: aload #11
    //   266: ldc 'Failed to close File:'
    //   268: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   271: pop
    //   272: iload #5
    //   274: istore #4
    //   276: aload #11
    //   278: aload_1
    //   279: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   282: pop
    //   283: iload #5
    //   285: istore #4
    //   287: aload #11
    //   289: ldc '.'
    //   291: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   294: pop
    //   295: iload #5
    //   297: istore #4
    //   299: aload #11
    //   301: invokevirtual toString : ()Ljava/lang/String;
    //   304: astore_1
    //   305: goto -> 488
    //   308: astore #11
    //   310: goto -> 995
    //   313: astore #11
    //   315: iload_2
    //   316: istore #5
    //   318: aload #12
    //   320: astore_3
    //   321: new java/lang/StringBuilder
    //   324: astore #14
    //   326: iload_2
    //   327: istore #5
    //   329: aload #12
    //   331: astore_3
    //   332: aload #14
    //   334: invokespecial <init> : ()V
    //   337: iload_2
    //   338: istore #5
    //   340: aload #12
    //   342: astore_3
    //   343: aload #14
    //   345: ldc 'Access to File: '
    //   347: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   350: pop
    //   351: iload_2
    //   352: istore #5
    //   354: aload #12
    //   356: astore_3
    //   357: aload #14
    //   359: aload_1
    //   360: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   363: pop
    //   364: iload_2
    //   365: istore #5
    //   367: aload #12
    //   369: astore_3
    //   370: aload #14
    //   372: ldc ' was denied denied by SecurityManager.'
    //   374: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   377: pop
    //   378: iload_2
    //   379: istore #5
    //   381: aload #12
    //   383: astore_3
    //   384: ldc 'DrmConvertSession'
    //   386: aload #14
    //   388: invokevirtual toString : ()Ljava/lang/String;
    //   391: aload #11
    //   393: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   396: pop
    //   397: iload_2
    //   398: istore #4
    //   400: aload #12
    //   402: ifnull -> 1101
    //   405: iload_2
    //   406: istore #4
    //   408: aload #12
    //   410: invokevirtual close : ()V
    //   413: iload_2
    //   414: istore #4
    //   416: goto -> 233
    //   419: astore_3
    //   420: sipush #492
    //   423: istore #5
    //   425: iload #5
    //   427: istore #4
    //   429: new java/lang/StringBuilder
    //   432: astore #11
    //   434: iload #5
    //   436: istore #4
    //   438: aload #11
    //   440: invokespecial <init> : ()V
    //   443: iload #5
    //   445: istore #4
    //   447: aload #11
    //   449: ldc 'Failed to close File:'
    //   451: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   454: pop
    //   455: iload #5
    //   457: istore #4
    //   459: aload #11
    //   461: aload_1
    //   462: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   465: pop
    //   466: iload #5
    //   468: istore #4
    //   470: aload #11
    //   472: ldc '.'
    //   474: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   477: pop
    //   478: iload #5
    //   480: istore #4
    //   482: aload #11
    //   484: invokevirtual toString : ()Ljava/lang/String;
    //   487: astore_1
    //   488: sipush #492
    //   491: istore #5
    //   493: iload #5
    //   495: istore #4
    //   497: ldc 'DrmConvertSession'
    //   499: aload_1
    //   500: aload_3
    //   501: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   504: pop
    //   505: goto -> 624
    //   508: astore #11
    //   510: sipush #492
    //   513: istore_2
    //   514: iload_2
    //   515: istore #5
    //   517: aload #13
    //   519: astore_3
    //   520: ldc 'DrmConvertSession'
    //   522: ldc 'Could not open file in mode: rw'
    //   524: aload #11
    //   526: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   529: pop
    //   530: iload_2
    //   531: istore #4
    //   533: aload #13
    //   535: ifnull -> 1101
    //   538: iload_2
    //   539: istore #4
    //   541: aload #13
    //   543: invokevirtual close : ()V
    //   546: iload_2
    //   547: istore #4
    //   549: goto -> 233
    //   552: astore_3
    //   553: sipush #492
    //   556: istore #5
    //   558: iload #5
    //   560: istore #4
    //   562: new java/lang/StringBuilder
    //   565: astore #11
    //   567: iload #5
    //   569: istore #4
    //   571: aload #11
    //   573: invokespecial <init> : ()V
    //   576: iload #5
    //   578: istore #4
    //   580: aload #11
    //   582: ldc 'Failed to close File:'
    //   584: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   587: pop
    //   588: iload #5
    //   590: istore #4
    //   592: aload #11
    //   594: aload_1
    //   595: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   598: pop
    //   599: iload #5
    //   601: istore #4
    //   603: aload #11
    //   605: ldc '.'
    //   607: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   610: pop
    //   611: iload #5
    //   613: istore #4
    //   615: aload #11
    //   617: invokevirtual toString : ()Ljava/lang/String;
    //   620: astore_1
    //   621: goto -> 488
    //   624: iload #5
    //   626: istore #4
    //   628: goto -> 1101
    //   631: astore #11
    //   633: sipush #492
    //   636: istore_2
    //   637: iload_2
    //   638: istore #5
    //   640: aload #14
    //   642: astore_3
    //   643: new java/lang/StringBuilder
    //   646: astore #15
    //   648: iload_2
    //   649: istore #5
    //   651: aload #14
    //   653: astore_3
    //   654: aload #15
    //   656: invokespecial <init> : ()V
    //   659: iload_2
    //   660: istore #5
    //   662: aload #14
    //   664: astore_3
    //   665: aload #15
    //   667: ldc 'Could not access File: '
    //   669: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   672: pop
    //   673: iload_2
    //   674: istore #5
    //   676: aload #14
    //   678: astore_3
    //   679: aload #15
    //   681: aload_1
    //   682: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   685: pop
    //   686: iload_2
    //   687: istore #5
    //   689: aload #14
    //   691: astore_3
    //   692: aload #15
    //   694: ldc ' .'
    //   696: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   699: pop
    //   700: iload_2
    //   701: istore #5
    //   703: aload #14
    //   705: astore_3
    //   706: ldc 'DrmConvertSession'
    //   708: aload #15
    //   710: invokevirtual toString : ()Ljava/lang/String;
    //   713: aload #11
    //   715: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   718: pop
    //   719: iload_2
    //   720: istore #4
    //   722: aload #14
    //   724: ifnull -> 1101
    //   727: iload_2
    //   728: istore #4
    //   730: aload #14
    //   732: invokevirtual close : ()V
    //   735: iload_2
    //   736: istore #4
    //   738: goto -> 233
    //   741: astore_3
    //   742: sipush #492
    //   745: istore #5
    //   747: iload #5
    //   749: istore #4
    //   751: new java/lang/StringBuilder
    //   754: astore #11
    //   756: iload #5
    //   758: istore #4
    //   760: aload #11
    //   762: invokespecial <init> : ()V
    //   765: iload #5
    //   767: istore #4
    //   769: aload #11
    //   771: ldc 'Failed to close File:'
    //   773: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   776: pop
    //   777: iload #5
    //   779: istore #4
    //   781: aload #11
    //   783: aload_1
    //   784: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   787: pop
    //   788: iload #5
    //   790: istore #4
    //   792: aload #11
    //   794: ldc '.'
    //   796: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   799: pop
    //   800: iload #5
    //   802: istore #4
    //   804: aload #11
    //   806: invokevirtual toString : ()Ljava/lang/String;
    //   809: astore_1
    //   810: goto -> 488
    //   813: astore #14
    //   815: sipush #492
    //   818: istore_2
    //   819: iload_2
    //   820: istore #5
    //   822: aload #15
    //   824: astore_3
    //   825: new java/lang/StringBuilder
    //   828: astore #11
    //   830: iload_2
    //   831: istore #5
    //   833: aload #15
    //   835: astore_3
    //   836: aload #11
    //   838: invokespecial <init> : ()V
    //   841: iload_2
    //   842: istore #5
    //   844: aload #15
    //   846: astore_3
    //   847: aload #11
    //   849: ldc 'File: '
    //   851: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   854: pop
    //   855: iload_2
    //   856: istore #5
    //   858: aload #15
    //   860: astore_3
    //   861: aload #11
    //   863: aload_1
    //   864: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   867: pop
    //   868: iload_2
    //   869: istore #5
    //   871: aload #15
    //   873: astore_3
    //   874: aload #11
    //   876: ldc ' could not be found.'
    //   878: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   881: pop
    //   882: iload_2
    //   883: istore #5
    //   885: aload #15
    //   887: astore_3
    //   888: ldc 'DrmConvertSession'
    //   890: aload #11
    //   892: invokevirtual toString : ()Ljava/lang/String;
    //   895: aload #14
    //   897: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   900: pop
    //   901: iload_2
    //   902: istore #4
    //   904: aload #15
    //   906: ifnull -> 1101
    //   909: iload_2
    //   910: istore #4
    //   912: aload #15
    //   914: invokevirtual close : ()V
    //   917: iload_2
    //   918: istore #4
    //   920: goto -> 233
    //   923: astore_3
    //   924: sipush #492
    //   927: istore #5
    //   929: iload #5
    //   931: istore #4
    //   933: new java/lang/StringBuilder
    //   936: astore #11
    //   938: iload #5
    //   940: istore #4
    //   942: aload #11
    //   944: invokespecial <init> : ()V
    //   947: iload #5
    //   949: istore #4
    //   951: aload #11
    //   953: ldc 'Failed to close File:'
    //   955: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   958: pop
    //   959: iload #5
    //   961: istore #4
    //   963: aload #11
    //   965: aload_1
    //   966: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   969: pop
    //   970: iload #5
    //   972: istore #4
    //   974: aload #11
    //   976: ldc '.'
    //   978: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   981: pop
    //   982: iload #5
    //   984: istore #4
    //   986: aload #11
    //   988: invokevirtual toString : ()Ljava/lang/String;
    //   991: astore_1
    //   992: goto -> 488
    //   995: iload #5
    //   997: istore #4
    //   999: aload_3
    //   1000: ifnull -> 1093
    //   1003: iload #5
    //   1005: istore #4
    //   1007: aload_3
    //   1008: invokevirtual close : ()V
    //   1011: iload #5
    //   1013: istore #4
    //   1015: goto -> 1093
    //   1018: astore #14
    //   1020: sipush #492
    //   1023: istore #5
    //   1025: iload #5
    //   1027: istore #4
    //   1029: new java/lang/StringBuilder
    //   1032: astore_3
    //   1033: iload #5
    //   1035: istore #4
    //   1037: aload_3
    //   1038: invokespecial <init> : ()V
    //   1041: iload #5
    //   1043: istore #4
    //   1045: aload_3
    //   1046: ldc 'Failed to close File:'
    //   1048: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1051: pop
    //   1052: iload #5
    //   1054: istore #4
    //   1056: aload_3
    //   1057: aload_1
    //   1058: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1061: pop
    //   1062: iload #5
    //   1064: istore #4
    //   1066: aload_3
    //   1067: ldc '.'
    //   1069: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1072: pop
    //   1073: iload #5
    //   1075: istore #4
    //   1077: ldc 'DrmConvertSession'
    //   1079: aload_3
    //   1080: invokevirtual toString : ()Ljava/lang/String;
    //   1083: aload #14
    //   1085: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   1088: pop
    //   1089: iload #5
    //   1091: istore #4
    //   1093: aload #11
    //   1095: athrow
    //   1096: sipush #406
    //   1099: istore #4
    //   1101: goto -> 1140
    //   1104: astore_1
    //   1105: new java/lang/StringBuilder
    //   1108: dup
    //   1109: invokespecial <init> : ()V
    //   1112: astore_3
    //   1113: aload_3
    //   1114: ldc 'Could not close convertsession. Convertsession: '
    //   1116: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1119: pop
    //   1120: aload_3
    //   1121: aload_0
    //   1122: getfield mConvertSessionId : I
    //   1125: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1128: pop
    //   1129: ldc 'DrmConvertSession'
    //   1131: aload_3
    //   1132: invokevirtual toString : ()Ljava/lang/String;
    //   1135: aload_1
    //   1136: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   1139: pop
    //   1140: iload #4
    //   1142: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #135	-> 0
    //   #136	-> 0
    //   #137	-> 4
    //   #139	-> 30
    //   #140	-> 41
    //   #145	-> 74
    //   #147	-> 89
    //   #148	-> 150
    //   #149	-> 183
    //   #150	-> 215
    //   #164	-> 220
    //   #166	-> 220
    //   #171	-> 233
    //   #167	-> 236
    //   #168	-> 237
    //   #169	-> 242
    //   #164	-> 308
    //   #160	-> 313
    //   #161	-> 315
    //   #164	-> 397
    //   #166	-> 405
    //   #167	-> 419
    //   #168	-> 420
    //   #169	-> 425
    //   #157	-> 508
    //   #158	-> 510
    //   #159	-> 514
    //   #164	-> 530
    //   #166	-> 538
    //   #167	-> 552
    //   #168	-> 553
    //   #169	-> 558
    //   #171	-> 624
    //   #154	-> 631
    //   #155	-> 633
    //   #156	-> 637
    //   #164	-> 719
    //   #166	-> 727
    //   #167	-> 741
    //   #168	-> 742
    //   #169	-> 747
    //   #151	-> 813
    //   #152	-> 815
    //   #153	-> 819
    //   #164	-> 901
    //   #166	-> 909
    //   #167	-> 923
    //   #168	-> 924
    //   #169	-> 929
    //   #164	-> 995
    //   #166	-> 1003
    //   #171	-> 1011
    //   #167	-> 1018
    //   #168	-> 1020
    //   #169	-> 1025
    //   #173	-> 1093
    //   #143	-> 1096
    //   #178	-> 1101
    //   #175	-> 1104
    //   #176	-> 1105
    //   #180	-> 1140
    // Exception table:
    //   from	to	target	type
    //   33	41	1104	java/lang/IllegalStateException
    //   49	58	1104	java/lang/IllegalStateException
    //   61	67	1104	java/lang/IllegalStateException
    //   111	116	813	java/io/FileNotFoundException
    //   111	116	631	java/io/IOException
    //   111	116	508	java/lang/IllegalArgumentException
    //   111	116	313	java/lang/SecurityException
    //   111	116	308	finally
    //   138	146	813	java/io/FileNotFoundException
    //   138	146	631	java/io/IOException
    //   138	146	508	java/lang/IllegalArgumentException
    //   138	146	313	java/lang/SecurityException
    //   138	146	308	finally
    //   172	183	813	java/io/FileNotFoundException
    //   172	183	631	java/io/IOException
    //   172	183	508	java/lang/IllegalArgumentException
    //   172	183	313	java/lang/SecurityException
    //   172	183	308	finally
    //   205	215	813	java/io/FileNotFoundException
    //   205	215	631	java/io/IOException
    //   205	215	508	java/lang/IllegalArgumentException
    //   205	215	313	java/lang/SecurityException
    //   205	215	308	finally
    //   224	229	236	java/io/IOException
    //   224	229	1104	java/lang/IllegalStateException
    //   246	251	1104	java/lang/IllegalStateException
    //   255	260	1104	java/lang/IllegalStateException
    //   264	272	1104	java/lang/IllegalStateException
    //   276	283	1104	java/lang/IllegalStateException
    //   287	295	1104	java/lang/IllegalStateException
    //   299	305	1104	java/lang/IllegalStateException
    //   321	326	308	finally
    //   332	337	308	finally
    //   343	351	308	finally
    //   357	364	308	finally
    //   370	378	308	finally
    //   384	397	308	finally
    //   408	413	419	java/io/IOException
    //   408	413	1104	java/lang/IllegalStateException
    //   429	434	1104	java/lang/IllegalStateException
    //   438	443	1104	java/lang/IllegalStateException
    //   447	455	1104	java/lang/IllegalStateException
    //   459	466	1104	java/lang/IllegalStateException
    //   470	478	1104	java/lang/IllegalStateException
    //   482	488	1104	java/lang/IllegalStateException
    //   497	505	1104	java/lang/IllegalStateException
    //   520	530	308	finally
    //   541	546	552	java/io/IOException
    //   541	546	1104	java/lang/IllegalStateException
    //   562	567	1104	java/lang/IllegalStateException
    //   571	576	1104	java/lang/IllegalStateException
    //   580	588	1104	java/lang/IllegalStateException
    //   592	599	1104	java/lang/IllegalStateException
    //   603	611	1104	java/lang/IllegalStateException
    //   615	621	1104	java/lang/IllegalStateException
    //   643	648	308	finally
    //   654	659	308	finally
    //   665	673	308	finally
    //   679	686	308	finally
    //   692	700	308	finally
    //   706	719	308	finally
    //   730	735	741	java/io/IOException
    //   730	735	1104	java/lang/IllegalStateException
    //   751	756	1104	java/lang/IllegalStateException
    //   760	765	1104	java/lang/IllegalStateException
    //   769	777	1104	java/lang/IllegalStateException
    //   781	788	1104	java/lang/IllegalStateException
    //   792	800	1104	java/lang/IllegalStateException
    //   804	810	1104	java/lang/IllegalStateException
    //   825	830	308	finally
    //   836	841	308	finally
    //   847	855	308	finally
    //   861	868	308	finally
    //   874	882	308	finally
    //   888	901	308	finally
    //   912	917	923	java/io/IOException
    //   912	917	1104	java/lang/IllegalStateException
    //   933	938	1104	java/lang/IllegalStateException
    //   942	947	1104	java/lang/IllegalStateException
    //   951	959	1104	java/lang/IllegalStateException
    //   963	970	1104	java/lang/IllegalStateException
    //   974	982	1104	java/lang/IllegalStateException
    //   986	992	1104	java/lang/IllegalStateException
    //   1007	1011	1018	java/io/IOException
    //   1007	1011	1104	java/lang/IllegalStateException
    //   1029	1033	1104	java/lang/IllegalStateException
    //   1037	1041	1104	java/lang/IllegalStateException
    //   1045	1052	1104	java/lang/IllegalStateException
    //   1056	1062	1104	java/lang/IllegalStateException
    //   1066	1073	1104	java/lang/IllegalStateException
    //   1077	1089	1104	java/lang/IllegalStateException
    //   1093	1096	1104	java/lang/IllegalStateException
  }
}
