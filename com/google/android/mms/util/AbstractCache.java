package com.google.android.mms.util;

import java.util.HashMap;

public abstract class AbstractCache<K, V> {
  private static final boolean DEBUG = false;
  
  private static final boolean LOCAL_LOGV = false;
  
  private static final int MAX_CACHED_ITEMS = 500;
  
  private static final String TAG = "AbstractCache";
  
  private final HashMap<K, CacheEntry<V>> mCacheMap = new HashMap<>();
  
  public boolean put(K paramK, V paramV) {
    if (this.mCacheMap.size() >= 500)
      return false; 
    if (paramK != null) {
      CacheEntry<V> cacheEntry = new CacheEntry();
      cacheEntry.value = paramV;
      this.mCacheMap.put(paramK, cacheEntry);
      return true;
    } 
    return false;
  }
  
  public V get(K paramK) {
    if (paramK != null) {
      CacheEntry cacheEntry = this.mCacheMap.get(paramK);
      if (cacheEntry != null) {
        cacheEntry.hit++;
        return cacheEntry.value;
      } 
    } 
    return null;
  }
  
  public V purge(K paramK) {
    CacheEntry cacheEntry = this.mCacheMap.remove(paramK);
    if (cacheEntry != null) {
      V v = cacheEntry.value;
    } else {
      cacheEntry = null;
    } 
    return (V)cacheEntry;
  }
  
  public void purgeAll() {
    this.mCacheMap.clear();
  }
  
  public int size() {
    return this.mCacheMap.size();
  }
  
  private static class CacheEntry<V> {
    int hit;
    
    V value;
    
    private CacheEntry() {}
  }
}
