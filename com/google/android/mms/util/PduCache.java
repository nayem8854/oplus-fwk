package com.google.android.mms.util;

import android.content.UriMatcher;
import android.net.Uri;
import android.provider.Telephony;
import java.util.HashMap;
import java.util.HashSet;

public final class PduCache extends AbstractCache<Uri, PduCacheEntry> {
  private static final boolean DEBUG = false;
  
  private static final boolean LOCAL_LOGV = false;
  
  private static final HashMap<Integer, Integer> MATCH_TO_MSGBOX_ID_MAP;
  
  private static final int MMS_ALL = 0;
  
  private static final int MMS_ALL_ID = 1;
  
  private static final int MMS_CONVERSATION = 10;
  
  private static final int MMS_CONVERSATION_ID = 11;
  
  private static final int MMS_DRAFTS = 6;
  
  private static final int MMS_DRAFTS_ID = 7;
  
  private static final int MMS_INBOX = 2;
  
  private static final int MMS_INBOX_ID = 3;
  
  private static final int MMS_OUTBOX = 8;
  
  private static final int MMS_OUTBOX_ID = 9;
  
  private static final int MMS_SENT = 4;
  
  private static final int MMS_SENT_ID = 5;
  
  private static final String TAG = "PduCache";
  
  private static final UriMatcher URI_MATCHER;
  
  private static PduCache sInstance;
  
  static {
    UriMatcher uriMatcher1 = new UriMatcher(-1);
    uriMatcher1.addURI("mms", null, 0);
    URI_MATCHER.addURI("mms", "#", 1);
    UriMatcher uriMatcher2 = URI_MATCHER;
    Integer integer1 = Integer.valueOf(2);
    uriMatcher2.addURI("mms", "inbox", 2);
    URI_MATCHER.addURI("mms", "inbox/#", 3);
    UriMatcher uriMatcher3 = URI_MATCHER;
    Integer integer2 = Integer.valueOf(4);
    uriMatcher3.addURI("mms", "sent", 4);
    URI_MATCHER.addURI("mms", "sent/#", 5);
    URI_MATCHER.addURI("mms", "drafts", 6);
    URI_MATCHER.addURI("mms", "drafts/#", 7);
    URI_MATCHER.addURI("mms", "outbox", 8);
    URI_MATCHER.addURI("mms", "outbox/#", 9);
    URI_MATCHER.addURI("mms-sms", "conversations", 10);
    URI_MATCHER.addURI("mms-sms", "conversations/#", 11);
    HashMap<Object, Object> hashMap = new HashMap<>();
    hashMap.put(integer1, Integer.valueOf(1));
    MATCH_TO_MSGBOX_ID_MAP.put(integer2, integer1);
    MATCH_TO_MSGBOX_ID_MAP.put(Integer.valueOf(6), Integer.valueOf(3));
    MATCH_TO_MSGBOX_ID_MAP.put(Integer.valueOf(8), integer2);
  }
  
  private final HashMap<Integer, HashSet<Uri>> mMessageBoxes = new HashMap<>();
  
  private final HashMap<Long, HashSet<Uri>> mThreads = new HashMap<>();
  
  private final HashSet<Uri> mUpdating = new HashSet<>();
  
  public static final PduCache getInstance() {
    // Byte code:
    //   0: ldc com/google/android/mms/util/PduCache
    //   2: monitorenter
    //   3: getstatic com/google/android/mms/util/PduCache.sInstance : Lcom/google/android/mms/util/PduCache;
    //   6: ifnonnull -> 21
    //   9: new com/google/android/mms/util/PduCache
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic com/google/android/mms/util/PduCache.sInstance : Lcom/google/android/mms/util/PduCache;
    //   21: getstatic com/google/android/mms/util/PduCache.sInstance : Lcom/google/android/mms/util/PduCache;
    //   24: astore_0
    //   25: ldc com/google/android/mms/util/PduCache
    //   27: monitorexit
    //   28: aload_0
    //   29: areturn
    //   30: astore_0
    //   31: ldc com/google/android/mms/util/PduCache
    //   33: monitorexit
    //   34: aload_0
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #88	-> 3
    //   #92	-> 9
    //   #94	-> 21
    //   #87	-> 30
    // Exception table:
    //   from	to	target	type
    //   3	9	30	finally
    //   9	21	30	finally
    //   21	25	30	finally
  }
  
  public boolean put(Uri paramUri, PduCacheEntry paramPduCacheEntry) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_2
    //   3: invokevirtual getMessageBox : ()I
    //   6: istore_3
    //   7: aload_0
    //   8: getfield mMessageBoxes : Ljava/util/HashMap;
    //   11: iload_3
    //   12: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   15: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   18: checkcast java/util/HashSet
    //   21: astore #4
    //   23: aload #4
    //   25: astore #5
    //   27: aload #4
    //   29: ifnonnull -> 56
    //   32: new java/util/HashSet
    //   35: astore #5
    //   37: aload #5
    //   39: invokespecial <init> : ()V
    //   42: aload_0
    //   43: getfield mMessageBoxes : Ljava/util/HashMap;
    //   46: iload_3
    //   47: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   50: aload #5
    //   52: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   55: pop
    //   56: aload_2
    //   57: invokevirtual getThreadId : ()J
    //   60: lstore #6
    //   62: aload_0
    //   63: getfield mThreads : Ljava/util/HashMap;
    //   66: lload #6
    //   68: invokestatic valueOf : (J)Ljava/lang/Long;
    //   71: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   74: checkcast java/util/HashSet
    //   77: astore #8
    //   79: aload #8
    //   81: astore #4
    //   83: aload #8
    //   85: ifnonnull -> 113
    //   88: new java/util/HashSet
    //   91: astore #4
    //   93: aload #4
    //   95: invokespecial <init> : ()V
    //   98: aload_0
    //   99: getfield mThreads : Ljava/util/HashMap;
    //   102: lload #6
    //   104: invokestatic valueOf : (J)Ljava/lang/Long;
    //   107: aload #4
    //   109: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   112: pop
    //   113: aload_0
    //   114: aload_1
    //   115: invokespecial normalizeKey : (Landroid/net/Uri;)Landroid/net/Uri;
    //   118: astore #8
    //   120: aload_0
    //   121: aload #8
    //   123: aload_2
    //   124: invokespecial put : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   127: istore #9
    //   129: iload #9
    //   131: ifeq -> 150
    //   134: aload #5
    //   136: aload #8
    //   138: invokevirtual add : (Ljava/lang/Object;)Z
    //   141: pop
    //   142: aload #4
    //   144: aload #8
    //   146: invokevirtual add : (Ljava/lang/Object;)Z
    //   149: pop
    //   150: aload_0
    //   151: aload_1
    //   152: iconst_0
    //   153: invokevirtual setUpdating : (Landroid/net/Uri;Z)V
    //   156: aload_0
    //   157: monitorexit
    //   158: iload #9
    //   160: ireturn
    //   161: astore_1
    //   162: aload_0
    //   163: monitorexit
    //   164: aload_1
    //   165: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #99	-> 2
    //   #100	-> 7
    //   #101	-> 23
    //   #102	-> 32
    //   #103	-> 42
    //   #106	-> 56
    //   #107	-> 62
    //   #108	-> 79
    //   #109	-> 88
    //   #110	-> 98
    //   #113	-> 113
    //   #114	-> 120
    //   #115	-> 129
    //   #116	-> 134
    //   #117	-> 142
    //   #119	-> 150
    //   #120	-> 156
    //   #98	-> 161
    // Exception table:
    //   from	to	target	type
    //   2	7	161	finally
    //   7	23	161	finally
    //   32	42	161	finally
    //   42	56	161	finally
    //   56	62	161	finally
    //   62	79	161	finally
    //   88	98	161	finally
    //   98	113	161	finally
    //   113	120	161	finally
    //   120	129	161	finally
    //   134	142	161	finally
    //   142	150	161	finally
    //   150	156	161	finally
  }
  
  public void setUpdating(Uri paramUri, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iload_2
    //   3: ifeq -> 18
    //   6: aload_0
    //   7: getfield mUpdating : Ljava/util/HashSet;
    //   10: aload_1
    //   11: invokevirtual add : (Ljava/lang/Object;)Z
    //   14: pop
    //   15: goto -> 27
    //   18: aload_0
    //   19: getfield mUpdating : Ljava/util/HashSet;
    //   22: aload_1
    //   23: invokevirtual remove : (Ljava/lang/Object;)Z
    //   26: pop
    //   27: aload_0
    //   28: monitorexit
    //   29: return
    //   30: astore_1
    //   31: aload_0
    //   32: monitorexit
    //   33: aload_1
    //   34: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #124	-> 2
    //   #125	-> 6
    //   #127	-> 18
    //   #129	-> 27
    //   #123	-> 30
    // Exception table:
    //   from	to	target	type
    //   6	15	30	finally
    //   18	27	30	finally
  }
  
  public boolean isUpdating(Uri paramUri) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mUpdating : Ljava/util/HashSet;
    //   6: aload_1
    //   7: invokevirtual contains : (Ljava/lang/Object;)Z
    //   10: istore_2
    //   11: aload_0
    //   12: monitorexit
    //   13: iload_2
    //   14: ireturn
    //   15: astore_1
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_1
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #133	-> 2
    //   #133	-> 15
    // Exception table:
    //   from	to	target	type
    //   2	11	15	finally
  }
  
  public PduCacheEntry purge(Uri paramUri) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic com/google/android/mms/util/PduCache.URI_MATCHER : Landroid/content/UriMatcher;
    //   5: aload_1
    //   6: invokevirtual match : (Landroid/net/Uri;)I
    //   9: istore_2
    //   10: iload_2
    //   11: tableswitch default -> 72, 0 -> 140, 1 -> 130, 2 -> 109, 3 -> 88, 4 -> 109, 5 -> 88, 6 -> 109, 7 -> 88, 8 -> 109, 9 -> 88, 10 -> 140, 11 -> 76
    //   72: aload_0
    //   73: monitorexit
    //   74: aconst_null
    //   75: areturn
    //   76: aload_0
    //   77: aload_1
    //   78: invokestatic parseId : (Landroid/net/Uri;)J
    //   81: invokespecial purgeByThreadId : (J)V
    //   84: aload_0
    //   85: monitorexit
    //   86: aconst_null
    //   87: areturn
    //   88: aload_1
    //   89: invokevirtual getLastPathSegment : ()Ljava/lang/String;
    //   92: astore_1
    //   93: aload_0
    //   94: getstatic android/provider/Telephony$Mms.CONTENT_URI : Landroid/net/Uri;
    //   97: aload_1
    //   98: invokestatic withAppendedPath : (Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    //   101: invokespecial purgeSingleEntry : (Landroid/net/Uri;)Lcom/google/android/mms/util/PduCacheEntry;
    //   104: astore_1
    //   105: aload_0
    //   106: monitorexit
    //   107: aload_1
    //   108: areturn
    //   109: aload_0
    //   110: getstatic com/google/android/mms/util/PduCache.MATCH_TO_MSGBOX_ID_MAP : Ljava/util/HashMap;
    //   113: iload_2
    //   114: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   117: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   120: checkcast java/lang/Integer
    //   123: invokespecial purgeByMessageBox : (Ljava/lang/Integer;)V
    //   126: aload_0
    //   127: monitorexit
    //   128: aconst_null
    //   129: areturn
    //   130: aload_0
    //   131: aload_1
    //   132: invokespecial purgeSingleEntry : (Landroid/net/Uri;)Lcom/google/android/mms/util/PduCacheEntry;
    //   135: astore_1
    //   136: aload_0
    //   137: monitorexit
    //   138: aload_1
    //   139: areturn
    //   140: aload_0
    //   141: invokevirtual purgeAll : ()V
    //   144: aload_0
    //   145: monitorexit
    //   146: aconst_null
    //   147: areturn
    //   148: astore_1
    //   149: aload_0
    //   150: monitorexit
    //   151: aload_1
    //   152: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #139	-> 2
    //   #140	-> 10
    //   #164	-> 72
    //   #161	-> 76
    //   #162	-> 84
    //   #147	-> 88
    //   #148	-> 93
    //   #158	-> 109
    //   #159	-> 126
    //   #142	-> 130
    //   #152	-> 140
    //   #153	-> 144
    //   #138	-> 148
    // Exception table:
    //   from	to	target	type
    //   2	10	148	finally
    //   76	84	148	finally
    //   88	93	148	finally
    //   93	105	148	finally
    //   109	126	148	finally
    //   130	136	148	finally
    //   140	144	148	finally
  }
  
  private PduCacheEntry purgeSingleEntry(Uri paramUri) {
    this.mUpdating.remove(paramUri);
    PduCacheEntry pduCacheEntry = super.purge(paramUri);
    if (pduCacheEntry != null) {
      removeFromThreads(paramUri, pduCacheEntry);
      removeFromMessageBoxes(paramUri, pduCacheEntry);
      return pduCacheEntry;
    } 
    return null;
  }
  
  public void purgeAll() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial purgeAll : ()V
    //   6: aload_0
    //   7: getfield mMessageBoxes : Ljava/util/HashMap;
    //   10: invokevirtual clear : ()V
    //   13: aload_0
    //   14: getfield mThreads : Ljava/util/HashMap;
    //   17: invokevirtual clear : ()V
    //   20: aload_0
    //   21: getfield mUpdating : Ljava/util/HashSet;
    //   24: invokevirtual clear : ()V
    //   27: aload_0
    //   28: monitorexit
    //   29: return
    //   30: astore_1
    //   31: aload_0
    //   32: monitorexit
    //   33: aload_1
    //   34: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #182	-> 2
    //   #184	-> 6
    //   #185	-> 13
    //   #186	-> 20
    //   #187	-> 27
    //   #181	-> 30
    // Exception table:
    //   from	to	target	type
    //   2	6	30	finally
    //   6	13	30	finally
    //   13	20	30	finally
    //   20	27	30	finally
  }
  
  private Uri normalizeKey(Uri paramUri) {
    Uri uri;
    int i = URI_MATCHER.match(paramUri);
    if (i != 1) {
      if (i != 3 && i != 5 && i != 7 && i != 9)
        return null; 
      String str = paramUri.getLastPathSegment();
      uri = Uri.withAppendedPath(Telephony.Mms.CONTENT_URI, str);
    } 
    return uri;
  }
  
  private void purgeByMessageBox(Integer paramInteger) {
    if (paramInteger != null) {
      HashSet hashSet = this.mMessageBoxes.remove(paramInteger);
      if (hashSet != null)
        for (Uri uri : hashSet) {
          this.mUpdating.remove(uri);
          PduCacheEntry pduCacheEntry = super.purge(uri);
          if (pduCacheEntry != null)
            removeFromThreads(uri, pduCacheEntry); 
        }  
    } 
  }
  
  private void removeFromThreads(Uri paramUri, PduCacheEntry paramPduCacheEntry) {
    HashSet hashSet = this.mThreads.get(Long.valueOf(paramPduCacheEntry.getThreadId()));
    if (hashSet != null)
      hashSet.remove(paramUri); 
  }
  
  private void purgeByThreadId(long paramLong) {
    HashSet hashSet = this.mThreads.remove(Long.valueOf(paramLong));
    if (hashSet != null)
      for (Uri uri : hashSet) {
        this.mUpdating.remove(uri);
        PduCacheEntry pduCacheEntry = super.purge(uri);
        if (pduCacheEntry != null)
          removeFromMessageBoxes(uri, pduCacheEntry); 
      }  
  }
  
  private void removeFromMessageBoxes(Uri paramUri, PduCacheEntry paramPduCacheEntry) {
    HashSet hashSet = this.mThreads.get(Long.valueOf(paramPduCacheEntry.getMessageBox()));
    if (hashSet != null)
      hashSet.remove(paramUri); 
  }
}
