package com.google.android.gles_jni;

import javax.microedition.khronos.egl.EGLDisplay;

public class EGLDisplayImpl extends EGLDisplay {
  long mEGLDisplay;
  
  public EGLDisplayImpl(long paramLong) {
    this.mEGLDisplay = paramLong;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mEGLDisplay != ((EGLDisplayImpl)paramObject).mEGLDisplay)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    long l = this.mEGLDisplay;
    int i = (int)(l ^ l >>> 32L);
    return 17 * 31 + i;
  }
}
