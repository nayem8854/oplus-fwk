package com.google.android.gles_jni;

import javax.microedition.khronos.egl.EGLSurface;

public class EGLSurfaceImpl extends EGLSurface {
  long mEGLSurface;
  
  public EGLSurfaceImpl() {
    this.mEGLSurface = 0L;
  }
  
  public EGLSurfaceImpl(long paramLong) {
    this.mEGLSurface = paramLong;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mEGLSurface != ((EGLSurfaceImpl)paramObject).mEGLSurface)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    long l = this.mEGLSurface;
    int i = (int)(l ^ l >>> 32L);
    return 17 * 31 + i;
  }
}
