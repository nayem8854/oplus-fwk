package com.google.android.gles_jni;

import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.opengles.GL;

public class EGLContextImpl extends EGLContext {
  long mEGLContext;
  
  private GLImpl mGLContext;
  
  public EGLContextImpl(long paramLong) {
    this.mEGLContext = paramLong;
    this.mGLContext = new GLImpl();
  }
  
  public GL getGL() {
    return this.mGLContext;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mEGLContext != ((EGLContextImpl)paramObject).mEGLContext)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    long l = this.mEGLContext;
    int i = (int)(l ^ l >>> 32L);
    return 17 * 31 + i;
  }
}
