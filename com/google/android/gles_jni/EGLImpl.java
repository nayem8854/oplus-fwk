package com.google.android.gles_jni;

import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

public class EGLImpl implements EGL10 {
  private EGLContextImpl mContext = new EGLContextImpl(-1L);
  
  private EGLDisplayImpl mDisplay = new EGLDisplayImpl(-1L);
  
  private EGLSurfaceImpl mSurface = new EGLSurfaceImpl(-1L);
  
  public EGLContext eglCreateContext(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, EGLContext paramEGLContext, int[] paramArrayOfint) {
    long l = _eglCreateContext(paramEGLDisplay, paramEGLConfig, paramEGLContext, paramArrayOfint);
    if (l == 0L)
      return EGL10.EGL_NO_CONTEXT; 
    return new EGLContextImpl(l);
  }
  
  public EGLSurface eglCreatePbufferSurface(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, int[] paramArrayOfint) {
    long l = _eglCreatePbufferSurface(paramEGLDisplay, paramEGLConfig, paramArrayOfint);
    if (l == 0L)
      return EGL10.EGL_NO_SURFACE; 
    return new EGLSurfaceImpl(l);
  }
  
  public EGLSurface eglCreatePixmapSurface(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, Object paramObject, int[] paramArrayOfint) {
    EGLSurfaceImpl eGLSurfaceImpl = new EGLSurfaceImpl();
    _eglCreatePixmapSurface(eGLSurfaceImpl, paramEGLDisplay, paramEGLConfig, paramObject, paramArrayOfint);
    if (eGLSurfaceImpl.mEGLSurface == 0L)
      return EGL10.EGL_NO_SURFACE; 
    return eGLSurfaceImpl;
  }
  
  public EGLSurface eglCreateWindowSurface(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, Object paramObject, int[] paramArrayOfint) {
    Surface surface;
    long l;
    SurfaceView surfaceView = null;
    if (paramObject instanceof SurfaceView) {
      surfaceView = (SurfaceView)paramObject;
      surface = surfaceView.getHolder().getSurface();
    } else if (paramObject instanceof SurfaceHolder) {
      SurfaceHolder surfaceHolder = (SurfaceHolder)paramObject;
      surface = surfaceHolder.getSurface();
    } else if (paramObject instanceof Surface) {
      surface = (Surface)paramObject;
    } 
    if (surface != null) {
      l = _eglCreateWindowSurface(paramEGLDisplay, paramEGLConfig, surface, paramArrayOfint);
    } else if (paramObject instanceof android.graphics.SurfaceTexture) {
      l = _eglCreateWindowSurfaceTexture(paramEGLDisplay, paramEGLConfig, paramObject, paramArrayOfint);
    } else {
      throw new UnsupportedOperationException("eglCreateWindowSurface() can only be called with an instance of Surface, SurfaceView, SurfaceHolder or SurfaceTexture at the moment.");
    } 
    if (l == 0L)
      return EGL10.EGL_NO_SURFACE; 
    return new EGLSurfaceImpl(l);
  }
  
  public EGLDisplay eglGetDisplay(Object paramObject) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokespecial _eglGetDisplay : (Ljava/lang/Object;)J
    //   7: lstore_2
    //   8: lload_2
    //   9: lconst_0
    //   10: lcmp
    //   11: ifne -> 22
    //   14: getstatic javax/microedition/khronos/egl/EGL10.EGL_NO_DISPLAY : Ljavax/microedition/khronos/egl/EGLDisplay;
    //   17: astore_1
    //   18: aload_0
    //   19: monitorexit
    //   20: aload_1
    //   21: areturn
    //   22: aload_0
    //   23: getfield mDisplay : Lcom/google/android/gles_jni/EGLDisplayImpl;
    //   26: getfield mEGLDisplay : J
    //   29: lload_2
    //   30: lcmp
    //   31: ifeq -> 48
    //   34: new com/google/android/gles_jni/EGLDisplayImpl
    //   37: astore_1
    //   38: aload_1
    //   39: lload_2
    //   40: invokespecial <init> : (J)V
    //   43: aload_0
    //   44: aload_1
    //   45: putfield mDisplay : Lcom/google/android/gles_jni/EGLDisplayImpl;
    //   48: aload_0
    //   49: getfield mDisplay : Lcom/google/android/gles_jni/EGLDisplayImpl;
    //   52: astore_1
    //   53: aload_0
    //   54: monitorexit
    //   55: aload_1
    //   56: areturn
    //   57: astore_1
    //   58: aload_0
    //   59: monitorexit
    //   60: aload_1
    //   61: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #118	-> 2
    //   #119	-> 8
    //   #120	-> 14
    //   #122	-> 22
    //   #123	-> 34
    //   #124	-> 48
    //   #117	-> 57
    // Exception table:
    //   from	to	target	type
    //   2	8	57	finally
    //   14	18	57	finally
    //   22	34	57	finally
    //   34	48	57	finally
    //   48	53	57	finally
  }
  
  public EGLContext eglGetCurrentContext() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial _eglGetCurrentContext : ()J
    //   6: lstore_1
    //   7: lload_1
    //   8: lconst_0
    //   9: lcmp
    //   10: ifne -> 21
    //   13: getstatic javax/microedition/khronos/egl/EGL10.EGL_NO_CONTEXT : Ljavax/microedition/khronos/egl/EGLContext;
    //   16: astore_3
    //   17: aload_0
    //   18: monitorexit
    //   19: aload_3
    //   20: areturn
    //   21: aload_0
    //   22: getfield mContext : Lcom/google/android/gles_jni/EGLContextImpl;
    //   25: getfield mEGLContext : J
    //   28: lload_1
    //   29: lcmp
    //   30: ifeq -> 47
    //   33: new com/google/android/gles_jni/EGLContextImpl
    //   36: astore_3
    //   37: aload_3
    //   38: lload_1
    //   39: invokespecial <init> : (J)V
    //   42: aload_0
    //   43: aload_3
    //   44: putfield mContext : Lcom/google/android/gles_jni/EGLContextImpl;
    //   47: aload_0
    //   48: getfield mContext : Lcom/google/android/gles_jni/EGLContextImpl;
    //   51: astore_3
    //   52: aload_0
    //   53: monitorexit
    //   54: aload_3
    //   55: areturn
    //   56: astore_3
    //   57: aload_0
    //   58: monitorexit
    //   59: aload_3
    //   60: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #128	-> 2
    //   #129	-> 7
    //   #130	-> 13
    //   #132	-> 21
    //   #133	-> 33
    //   #134	-> 47
    //   #127	-> 56
    // Exception table:
    //   from	to	target	type
    //   2	7	56	finally
    //   13	17	56	finally
    //   21	33	56	finally
    //   33	47	56	finally
    //   47	52	56	finally
  }
  
  public EGLDisplay eglGetCurrentDisplay() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial _eglGetCurrentDisplay : ()J
    //   6: lstore_1
    //   7: lload_1
    //   8: lconst_0
    //   9: lcmp
    //   10: ifne -> 21
    //   13: getstatic javax/microedition/khronos/egl/EGL10.EGL_NO_DISPLAY : Ljavax/microedition/khronos/egl/EGLDisplay;
    //   16: astore_3
    //   17: aload_0
    //   18: monitorexit
    //   19: aload_3
    //   20: areturn
    //   21: aload_0
    //   22: getfield mDisplay : Lcom/google/android/gles_jni/EGLDisplayImpl;
    //   25: getfield mEGLDisplay : J
    //   28: lload_1
    //   29: lcmp
    //   30: ifeq -> 47
    //   33: new com/google/android/gles_jni/EGLDisplayImpl
    //   36: astore_3
    //   37: aload_3
    //   38: lload_1
    //   39: invokespecial <init> : (J)V
    //   42: aload_0
    //   43: aload_3
    //   44: putfield mDisplay : Lcom/google/android/gles_jni/EGLDisplayImpl;
    //   47: aload_0
    //   48: getfield mDisplay : Lcom/google/android/gles_jni/EGLDisplayImpl;
    //   51: astore_3
    //   52: aload_0
    //   53: monitorexit
    //   54: aload_3
    //   55: areturn
    //   56: astore_3
    //   57: aload_0
    //   58: monitorexit
    //   59: aload_3
    //   60: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #138	-> 2
    //   #139	-> 7
    //   #140	-> 13
    //   #142	-> 21
    //   #143	-> 33
    //   #144	-> 47
    //   #137	-> 56
    // Exception table:
    //   from	to	target	type
    //   2	7	56	finally
    //   13	17	56	finally
    //   21	33	56	finally
    //   33	47	56	finally
    //   47	52	56	finally
  }
  
  public EGLSurface eglGetCurrentSurface(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iload_1
    //   4: invokespecial _eglGetCurrentSurface : (I)J
    //   7: lstore_2
    //   8: lload_2
    //   9: lconst_0
    //   10: lcmp
    //   11: ifne -> 24
    //   14: getstatic javax/microedition/khronos/egl/EGL10.EGL_NO_SURFACE : Ljavax/microedition/khronos/egl/EGLSurface;
    //   17: astore #4
    //   19: aload_0
    //   20: monitorexit
    //   21: aload #4
    //   23: areturn
    //   24: aload_0
    //   25: getfield mSurface : Lcom/google/android/gles_jni/EGLSurfaceImpl;
    //   28: getfield mEGLSurface : J
    //   31: lload_2
    //   32: lcmp
    //   33: ifeq -> 53
    //   36: new com/google/android/gles_jni/EGLSurfaceImpl
    //   39: astore #4
    //   41: aload #4
    //   43: lload_2
    //   44: invokespecial <init> : (J)V
    //   47: aload_0
    //   48: aload #4
    //   50: putfield mSurface : Lcom/google/android/gles_jni/EGLSurfaceImpl;
    //   53: aload_0
    //   54: getfield mSurface : Lcom/google/android/gles_jni/EGLSurfaceImpl;
    //   57: astore #4
    //   59: aload_0
    //   60: monitorexit
    //   61: aload #4
    //   63: areturn
    //   64: astore #4
    //   66: aload_0
    //   67: monitorexit
    //   68: aload #4
    //   70: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #148	-> 2
    //   #149	-> 8
    //   #150	-> 14
    //   #152	-> 24
    //   #153	-> 36
    //   #154	-> 53
    //   #147	-> 64
    // Exception table:
    //   from	to	target	type
    //   2	8	64	finally
    //   14	19	64	finally
    //   24	36	64	finally
    //   36	53	64	finally
    //   53	59	64	finally
  }
  
  static {
    _nativeClassInit();
  }
  
  private native long _eglCreateContext(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, EGLContext paramEGLContext, int[] paramArrayOfint);
  
  private native long _eglCreatePbufferSurface(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, int[] paramArrayOfint);
  
  private native void _eglCreatePixmapSurface(EGLSurface paramEGLSurface, EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, Object paramObject, int[] paramArrayOfint);
  
  private native long _eglCreateWindowSurface(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, Object paramObject, int[] paramArrayOfint);
  
  private native long _eglCreateWindowSurfaceTexture(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, Object paramObject, int[] paramArrayOfint);
  
  private native long _eglGetCurrentContext();
  
  private native long _eglGetCurrentDisplay();
  
  private native long _eglGetCurrentSurface(int paramInt);
  
  private native long _eglGetDisplay(Object paramObject);
  
  private static native void _nativeClassInit();
  
  public static native int getInitCount(EGLDisplay paramEGLDisplay);
  
  public native boolean eglChooseConfig(EGLDisplay paramEGLDisplay, int[] paramArrayOfint1, EGLConfig[] paramArrayOfEGLConfig, int paramInt, int[] paramArrayOfint2);
  
  public native boolean eglCopyBuffers(EGLDisplay paramEGLDisplay, EGLSurface paramEGLSurface, Object paramObject);
  
  public native boolean eglDestroyContext(EGLDisplay paramEGLDisplay, EGLContext paramEGLContext);
  
  public native boolean eglDestroySurface(EGLDisplay paramEGLDisplay, EGLSurface paramEGLSurface);
  
  public native boolean eglGetConfigAttrib(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, int paramInt, int[] paramArrayOfint);
  
  public native boolean eglGetConfigs(EGLDisplay paramEGLDisplay, EGLConfig[] paramArrayOfEGLConfig, int paramInt, int[] paramArrayOfint);
  
  public native int eglGetError();
  
  public native boolean eglInitialize(EGLDisplay paramEGLDisplay, int[] paramArrayOfint);
  
  public native boolean eglMakeCurrent(EGLDisplay paramEGLDisplay, EGLSurface paramEGLSurface1, EGLSurface paramEGLSurface2, EGLContext paramEGLContext);
  
  public native boolean eglQueryContext(EGLDisplay paramEGLDisplay, EGLContext paramEGLContext, int paramInt, int[] paramArrayOfint);
  
  public native String eglQueryString(EGLDisplay paramEGLDisplay, int paramInt);
  
  public native boolean eglQuerySurface(EGLDisplay paramEGLDisplay, EGLSurface paramEGLSurface, int paramInt, int[] paramArrayOfint);
  
  public native boolean eglReleaseThread();
  
  public native boolean eglSwapBuffers(EGLDisplay paramEGLDisplay, EGLSurface paramEGLSurface);
  
  public native boolean eglTerminate(EGLDisplay paramEGLDisplay);
  
  public native boolean eglWaitGL();
  
  public native boolean eglWaitNative(int paramInt, Object paramObject);
}
