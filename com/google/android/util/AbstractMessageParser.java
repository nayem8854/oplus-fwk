package com.google.android.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractMessageParser {
  public static final String musicNote = "♫ ";
  
  private HashMap<Character, Format> formatStart;
  
  private int nextChar;
  
  private int nextClass;
  
  private boolean parseAcronyms;
  
  private boolean parseFormatting;
  
  private boolean parseMeText;
  
  private boolean parseMusic;
  
  private boolean parseSmilies;
  
  private boolean parseUrls;
  
  private ArrayList<Part> parts;
  
  private String text;
  
  private ArrayList<Token> tokens;
  
  public AbstractMessageParser(String paramString) {
    this(paramString, true, true, true, true, true, true);
  }
  
  public AbstractMessageParser(String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6) {
    this.text = paramString;
    this.nextChar = 0;
    this.nextClass = 10;
    this.parts = new ArrayList<>();
    this.tokens = new ArrayList<>();
    this.formatStart = new HashMap<>();
    this.parseSmilies = paramBoolean1;
    this.parseAcronyms = paramBoolean2;
    this.parseFormatting = paramBoolean3;
    this.parseUrls = paramBoolean4;
    this.parseMusic = paramBoolean5;
    this.parseMeText = paramBoolean6;
  }
  
  public final String getRawText() {
    return this.text;
  }
  
  public final int getPartCount() {
    return this.parts.size();
  }
  
  public final Part getPart(int paramInt) {
    return this.parts.get(paramInt);
  }
  
  public final List<Part> getParts() {
    return this.parts;
  }
  
  public void parse() {
    if (parseMusicTrack()) {
      buildParts(null);
      return;
    } 
    String str1 = null;
    String str2 = str1;
    if (this.parseMeText) {
      str2 = str1;
      if (this.text.startsWith("/me")) {
        str2 = str1;
        if (this.text.length() > 3) {
          String str = this.text;
          str2 = str1;
          if (Character.isWhitespace(str.charAt(3))) {
            str2 = this.text.substring(0, 4);
            this.text = this.text.substring(4);
          } 
        } 
      } 
    } 
    int i = 0;
    while (this.nextChar < this.text.length()) {
      if (isWordBreak(this.nextChar) || (
        i && isSmileyBreak(this.nextChar))) {
        if (parseSmiley()) {
          i = 1;
          continue;
        } 
        byte b = 0;
        i = b;
        if (!parseAcronym()) {
          i = b;
          if (!parseURL()) {
            i = b;
            if (!parseFormatting()) {
              parseText();
              i = b;
            } 
          } 
        } 
        continue;
      } 
      throw new AssertionError("last chunk did not end at word break");
    } 
    for (i = 0; i < this.tokens.size(); i++) {
      if (((Token)this.tokens.get(i)).isMedia()) {
        if (i > 0 && this.tokens.get(i - 1) instanceof Html)
          ((Html)this.tokens.get(i - 1)).trimLeadingWhitespace(); 
        if (i + 1 < this.tokens.size() && this.tokens.get(i + 1) instanceof Html)
          ((Html)this.tokens.get(i + 1)).trimTrailingWhitespace(); 
      } 
    } 
    for (i = 0; i < this.tokens.size(); i = j + 1) {
      int j = i;
      if (((Token)this.tokens.get(i)).isHtml()) {
        ArrayList<Token> arrayList = this.tokens;
        j = i;
        if (((Token)arrayList.get(i)).toHtml(true).length() == 0) {
          this.tokens.remove(i);
          j = i - 1;
        } 
      } 
    } 
    buildParts(str2);
  }
  
  public static Token tokenForUrl(String paramString1, String paramString2) {
    if (paramString1 == null)
      return null; 
    Video video = Video.matchURL(paramString1, paramString2);
    if (video != null)
      return video; 
    YouTubeVideo youTubeVideo = YouTubeVideo.matchURL(paramString1, paramString2);
    if (youTubeVideo != null)
      return youTubeVideo; 
    Photo photo = Photo.matchURL(paramString1, paramString2);
    if (photo != null)
      return photo; 
    FlickrPhoto flickrPhoto = FlickrPhoto.matchURL(paramString1, paramString2);
    if (flickrPhoto != null)
      return flickrPhoto; 
    return new Link(paramString1, paramString2);
  }
  
  private void buildParts(String paramString) {
    for (byte b = 0; b < this.tokens.size(); b++) {
      Token token = this.tokens.get(b);
      if (token.isMedia() || this.parts.size() == 0 || lastPart().isMedia())
        this.parts.add(new Part()); 
      lastPart().add(token);
    } 
    if (this.parts.size() > 0)
      ((Part)this.parts.get(0)).setMeText(paramString); 
  }
  
  private Part lastPart() {
    ArrayList<Part> arrayList = this.parts;
    return arrayList.get(arrayList.size() - 1);
  }
  
  private boolean parseMusicTrack() {
    if (this.parseMusic && this.text.startsWith("♫ ")) {
      addToken(new MusicTrack(this.text.substring("♫ ".length())));
      this.nextChar = this.text.length();
      return true;
    } 
    return false;
  }
  
  private void parseText() {
    StringBuilder stringBuilder = new StringBuilder();
    int i = this.nextChar;
    do {
      String str = this.text;
      int j = this.nextChar;
      this.nextChar = j + 1;
      char c = str.charAt(j);
      if (c != '\n') {
        if (c != '"') {
          if (c != '<') {
            if (c != '>') {
              if (c != '&') {
                if (c != '\'') {
                  stringBuilder.append(c);
                } else {
                  stringBuilder.append("&apos;");
                } 
              } else {
                stringBuilder.append("&amp;");
              } 
            } else {
              stringBuilder.append("&gt;");
            } 
          } else {
            stringBuilder.append("&lt;");
          } 
        } else {
          stringBuilder.append("&quot;");
        } 
      } else {
        stringBuilder.append("<br>");
      } 
    } while (!isWordBreak(this.nextChar));
    addToken(new Html(this.text.substring(i, this.nextChar), stringBuilder.toString()));
  }
  
  private boolean parseSmiley() {
    if (!this.parseSmilies)
      return false; 
    TrieNode trieNode = longestMatch(getResources().getSmileys(), this, this.nextChar, true);
    if (trieNode == null)
      return false; 
    int i = getCharClass(this.nextChar - 1);
    int j = getCharClass(this.nextChar + trieNode.getText().length());
    if ((i == 2 || i == 3) && (j == 2 || j == 3))
      return false; 
    addToken(new Smiley(trieNode.getText()));
    this.nextChar += trieNode.getText().length();
    return true;
  }
  
  private boolean parseAcronym() {
    if (!this.parseAcronyms)
      return false; 
    TrieNode trieNode = longestMatch(getResources().getAcronyms(), this, this.nextChar);
    if (trieNode == null)
      return false; 
    addToken(new Acronym(trieNode.getText(), trieNode.getValue()));
    this.nextChar += trieNode.getText().length();
    return true;
  }
  
  private boolean isDomainChar(char paramChar) {
    return (paramChar == '-' || Character.isLetter(paramChar) || Character.isDigit(paramChar));
  }
  
  private boolean isValidDomain(String paramString) {
    if (matches(getResources().getDomainSuffixes(), reverse(paramString)))
      return true; 
    return false;
  }
  
  private boolean parseURL() {
    // Byte code:
    //   0: aload_0
    //   1: getfield parseUrls : Z
    //   4: ifeq -> 580
    //   7: aload_0
    //   8: aload_0
    //   9: getfield nextChar : I
    //   12: invokespecial isURLBreak : (I)Z
    //   15: ifne -> 21
    //   18: goto -> 580
    //   21: aload_0
    //   22: getfield nextChar : I
    //   25: istore_1
    //   26: iload_1
    //   27: istore_2
    //   28: iload_2
    //   29: aload_0
    //   30: getfield text : Ljava/lang/String;
    //   33: invokevirtual length : ()I
    //   36: if_icmpge -> 60
    //   39: aload_0
    //   40: aload_0
    //   41: getfield text : Ljava/lang/String;
    //   44: iload_2
    //   45: invokevirtual charAt : (I)C
    //   48: invokespecial isDomainChar : (C)Z
    //   51: ifeq -> 60
    //   54: iinc #2, 1
    //   57: goto -> 28
    //   60: ldc_w ''
    //   63: astore_3
    //   64: iconst_0
    //   65: istore #4
    //   67: iconst_0
    //   68: istore #5
    //   70: iconst_0
    //   71: istore #6
    //   73: iload_2
    //   74: aload_0
    //   75: getfield text : Ljava/lang/String;
    //   78: invokevirtual length : ()I
    //   81: if_icmpne -> 86
    //   84: iconst_0
    //   85: ireturn
    //   86: aload_0
    //   87: getfield text : Ljava/lang/String;
    //   90: iload_2
    //   91: invokevirtual charAt : (I)C
    //   94: bipush #58
    //   96: if_icmpne -> 141
    //   99: aload_0
    //   100: getfield text : Ljava/lang/String;
    //   103: aload_0
    //   104: getfield nextChar : I
    //   107: iload_2
    //   108: invokevirtual substring : (II)Ljava/lang/String;
    //   111: astore #7
    //   113: aload_0
    //   114: invokevirtual getResources : ()Lcom/google/android/util/AbstractMessageParser$Resources;
    //   117: invokeinterface getSchemes : ()Ljava/util/Set;
    //   122: aload #7
    //   124: invokeinterface contains : (Ljava/lang/Object;)Z
    //   129: ifne -> 134
    //   132: iconst_0
    //   133: ireturn
    //   134: iload #5
    //   136: istore #4
    //   138: goto -> 472
    //   141: aload_0
    //   142: getfield text : Ljava/lang/String;
    //   145: iload_2
    //   146: invokevirtual charAt : (I)C
    //   149: bipush #46
    //   151: if_icmpne -> 578
    //   154: iload_2
    //   155: aload_0
    //   156: getfield text : Ljava/lang/String;
    //   159: invokevirtual length : ()I
    //   162: if_icmpge -> 200
    //   165: aload_0
    //   166: getfield text : Ljava/lang/String;
    //   169: iload_2
    //   170: invokevirtual charAt : (I)C
    //   173: istore #8
    //   175: iload #8
    //   177: bipush #46
    //   179: if_icmpeq -> 194
    //   182: aload_0
    //   183: iload #8
    //   185: invokespecial isDomainChar : (C)Z
    //   188: ifne -> 194
    //   191: goto -> 200
    //   194: iinc #2, 1
    //   197: goto -> 154
    //   200: aload_0
    //   201: getfield text : Ljava/lang/String;
    //   204: aload_0
    //   205: getfield nextChar : I
    //   208: iload_2
    //   209: invokevirtual substring : (II)Ljava/lang/String;
    //   212: astore_3
    //   213: aload_0
    //   214: aload_3
    //   215: invokespecial isValidDomain : (Ljava/lang/String;)Z
    //   218: ifne -> 223
    //   221: iconst_0
    //   222: ireturn
    //   223: iload_2
    //   224: istore #5
    //   226: iload_2
    //   227: iconst_1
    //   228: iadd
    //   229: aload_0
    //   230: getfield text : Ljava/lang/String;
    //   233: invokevirtual length : ()I
    //   236: if_icmpge -> 320
    //   239: iload_2
    //   240: istore #5
    //   242: aload_0
    //   243: getfield text : Ljava/lang/String;
    //   246: iload_2
    //   247: invokevirtual charAt : (I)C
    //   250: bipush #58
    //   252: if_icmpne -> 320
    //   255: aload_0
    //   256: getfield text : Ljava/lang/String;
    //   259: iload_2
    //   260: iconst_1
    //   261: iadd
    //   262: invokevirtual charAt : (I)C
    //   265: istore #8
    //   267: iload_2
    //   268: istore #5
    //   270: iload #8
    //   272: invokestatic isDigit : (C)Z
    //   275: ifeq -> 320
    //   278: iinc #2, 1
    //   281: iload_2
    //   282: istore #5
    //   284: iload_2
    //   285: aload_0
    //   286: getfield text : Ljava/lang/String;
    //   289: invokevirtual length : ()I
    //   292: if_icmpge -> 320
    //   295: aload_0
    //   296: getfield text : Ljava/lang/String;
    //   299: astore_3
    //   300: iload_2
    //   301: istore #5
    //   303: aload_3
    //   304: iload_2
    //   305: invokevirtual charAt : (I)C
    //   308: invokestatic isDigit : (C)Z
    //   311: ifeq -> 320
    //   314: iinc #2, 1
    //   317: goto -> 281
    //   320: iload #5
    //   322: aload_0
    //   323: getfield text : Ljava/lang/String;
    //   326: invokevirtual length : ()I
    //   329: if_icmpne -> 337
    //   332: iconst_1
    //   333: istore_2
    //   334: goto -> 462
    //   337: aload_0
    //   338: getfield text : Ljava/lang/String;
    //   341: iload #5
    //   343: invokevirtual charAt : (I)C
    //   346: istore #8
    //   348: iload #8
    //   350: bipush #63
    //   352: if_icmpne -> 411
    //   355: iload #5
    //   357: iconst_1
    //   358: iadd
    //   359: aload_0
    //   360: getfield text : Ljava/lang/String;
    //   363: invokevirtual length : ()I
    //   366: if_icmpne -> 374
    //   369: iconst_1
    //   370: istore_2
    //   371: goto -> 462
    //   374: aload_0
    //   375: getfield text : Ljava/lang/String;
    //   378: iload #5
    //   380: iconst_1
    //   381: iadd
    //   382: invokevirtual charAt : (I)C
    //   385: istore #8
    //   387: iload #8
    //   389: invokestatic isWhitespace : (C)Z
    //   392: ifne -> 406
    //   395: iload #6
    //   397: istore_2
    //   398: iload #8
    //   400: invokestatic isPunctuation : (C)Z
    //   403: ifeq -> 408
    //   406: iconst_1
    //   407: istore_2
    //   408: goto -> 462
    //   411: iload #8
    //   413: invokestatic isPunctuation : (C)Z
    //   416: ifeq -> 424
    //   419: iconst_1
    //   420: istore_2
    //   421: goto -> 462
    //   424: iload #8
    //   426: invokestatic isWhitespace : (C)Z
    //   429: ifeq -> 437
    //   432: iconst_1
    //   433: istore_2
    //   434: goto -> 462
    //   437: iload #4
    //   439: istore_2
    //   440: iload #8
    //   442: bipush #47
    //   444: if_icmpeq -> 462
    //   447: iload #8
    //   449: bipush #35
    //   451: if_icmpne -> 460
    //   454: iload #4
    //   456: istore_2
    //   457: goto -> 462
    //   460: iconst_0
    //   461: ireturn
    //   462: ldc_w 'http://'
    //   465: astore_3
    //   466: iload_2
    //   467: istore #4
    //   469: iload #5
    //   471: istore_2
    //   472: iload_2
    //   473: istore #5
    //   475: iload #4
    //   477: ifne -> 521
    //   480: iload_2
    //   481: istore #5
    //   483: iload_2
    //   484: aload_0
    //   485: getfield text : Ljava/lang/String;
    //   488: invokevirtual length : ()I
    //   491: if_icmpge -> 521
    //   494: aload_0
    //   495: getfield text : Ljava/lang/String;
    //   498: astore #7
    //   500: iload_2
    //   501: istore #5
    //   503: aload #7
    //   505: iload_2
    //   506: invokevirtual charAt : (I)C
    //   509: invokestatic isWhitespace : (C)Z
    //   512: ifne -> 521
    //   515: iinc #2, 1
    //   518: goto -> 480
    //   521: aload_0
    //   522: getfield text : Ljava/lang/String;
    //   525: iload_1
    //   526: iload #5
    //   528: invokevirtual substring : (II)Ljava/lang/String;
    //   531: astore #7
    //   533: new java/lang/StringBuilder
    //   536: dup
    //   537: invokespecial <init> : ()V
    //   540: astore #9
    //   542: aload #9
    //   544: aload_3
    //   545: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   548: pop
    //   549: aload #9
    //   551: aload #7
    //   553: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   556: pop
    //   557: aload #9
    //   559: invokevirtual toString : ()Ljava/lang/String;
    //   562: astore_3
    //   563: aload_0
    //   564: aload_3
    //   565: aload #7
    //   567: invokespecial addURLToken : (Ljava/lang/String;Ljava/lang/String;)V
    //   570: aload_0
    //   571: iload #5
    //   573: putfield nextChar : I
    //   576: iconst_1
    //   577: ireturn
    //   578: iconst_0
    //   579: ireturn
    //   580: iconst_0
    //   581: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #348	-> 0
    //   #352	-> 21
    //   #355	-> 26
    //   #356	-> 28
    //   #357	-> 54
    //   #360	-> 60
    //   #361	-> 64
    //   #363	-> 73
    //   #364	-> 84
    //   #365	-> 86
    //   #367	-> 99
    //   #368	-> 113
    //   #369	-> 132
    //   #371	-> 134
    //   #373	-> 154
    //   #374	-> 165
    //   #375	-> 175
    //   #376	-> 191
    //   #378	-> 194
    //   #380	-> 197
    //   #384	-> 200
    //   #385	-> 213
    //   #386	-> 221
    //   #391	-> 223
    //   #392	-> 255
    //   #393	-> 267
    //   #394	-> 278
    //   #395	-> 281
    //   #396	-> 300
    //   #397	-> 314
    //   #408	-> 320
    //   #409	-> 332
    //   #411	-> 337
    //   #412	-> 348
    //   #415	-> 355
    //   #416	-> 369
    //   #418	-> 374
    //   #419	-> 387
    //   #420	-> 406
    //   #422	-> 408
    //   #423	-> 411
    //   #424	-> 419
    //   #425	-> 424
    //   #426	-> 432
    //   #427	-> 437
    //   #431	-> 460
    //   #438	-> 462
    //   #439	-> 466
    //   #445	-> 472
    //   #446	-> 480
    //   #447	-> 500
    //   #448	-> 515
    //   #452	-> 521
    //   #453	-> 533
    //   #456	-> 563
    //   #458	-> 570
    //   #459	-> 576
    //   #440	-> 578
    //   #349	-> 580
  }
  
  private void addURLToken(String paramString1, String paramString2) {
    addToken(tokenForUrl(paramString1, paramString2));
  }
  
  private boolean parseFormatting() {
    if (!this.parseFormatting)
      return false; 
    int i = this.nextChar;
    while (i < this.text.length() && isFormatChar(this.text.charAt(i)))
      i++; 
    if (i == this.nextChar || !isWordBreak(i))
      return false; 
    LinkedHashMap<Object, Object> linkedHashMap = new LinkedHashMap<>();
    for (int j = this.nextChar; j < i; j++) {
      char c = this.text.charAt(j);
      Character character = Character.valueOf(c);
      if (linkedHashMap.containsKey(character)) {
        addToken(new Format(c, false));
      } else {
        Format format = this.formatStart.get(character);
        if (format != null) {
          format.setMatched(true);
          this.formatStart.remove(character);
          linkedHashMap.put(character, Boolean.TRUE);
        } else {
          format = new Format(c, true);
          this.formatStart.put(character, format);
          addToken(format);
          linkedHashMap.put(character, Boolean.FALSE);
        } 
      } 
    } 
    for (Character character : linkedHashMap.keySet()) {
      if (linkedHashMap.get(character) == Boolean.TRUE) {
        Format format = new Format(character.charValue(), false);
        format.setMatched(true);
        addToken(format);
      } 
    } 
    this.nextChar = i;
    return true;
  }
  
  private boolean isWordBreak(int paramInt) {
    boolean bool;
    if (getCharClass(paramInt - 1) != getCharClass(paramInt)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isSmileyBreak(int paramInt) {
    if (paramInt > 0 && paramInt < this.text.length() && 
      isSmileyBreak(this.text.charAt(paramInt - 1), this.text.charAt(paramInt)))
      return true; 
    return false;
  }
  
  private boolean isURLBreak(int paramInt) {
    paramInt = getCharClass(paramInt - 1);
    if (paramInt != 2 && paramInt != 3 && paramInt != 4)
      return true; 
    return false;
  }
  
  private int getCharClass(int paramInt) {
    if (paramInt < 0 || this.text.length() <= paramInt)
      return 0; 
    char c = this.text.charAt(paramInt);
    if (Character.isWhitespace(c))
      return 1; 
    if (Character.isLetter(c))
      return 2; 
    if (Character.isDigit(c))
      return 3; 
    if (isPunctuation(c)) {
      this.nextClass = paramInt = this.nextClass + 1;
      return paramInt;
    } 
    return 4;
  }
  
  private static boolean isSmileyBreak(char paramChar1, char paramChar2) {
    if (paramChar1 != '$' && paramChar1 != '&' && paramChar1 != '-' && paramChar1 != '/' && paramChar1 != '@' && paramChar1 != '*' && paramChar1 != '+')
      switch (paramChar1) {
        default:
          switch (paramChar1) {
            default:
              switch (paramChar1) {
                default:
                  return false;
                case '|':
                case '}':
                case '~':
                  break;
              } 
              break;
            case '[':
            case '\\':
            case ']':
            case '^':
              break;
          } 
          break;
        case '<':
        case '=':
        case '>':
          break;
      }  
    if (paramChar2 != '*' && paramChar2 != '/' && paramChar2 != '@' && paramChar2 != '^' && paramChar2 != '~' && paramChar2 != '[' && paramChar2 != '\\')
      switch (paramChar2) {
        default:
          switch (paramChar2) {
            default:
            
            case '<':
            case '=':
            case '>':
              break;
          } 
          break;
        case '#':
        case '$':
        case '%':
          break;
      }  
    return true;
  }
  
  private static boolean isPunctuation(char paramChar) {
    if (paramChar != '!' && paramChar != '"' && paramChar != '(' && paramChar != ')' && paramChar != ',' && paramChar != '.' && paramChar != '?' && paramChar != ':' && paramChar != ';')
      return false; 
    return true;
  }
  
  private static boolean isFormatChar(char paramChar) {
    if (paramChar != '*' && paramChar != '^' && paramChar != '_')
      return false; 
    return true;
  }
  
  public static abstract class Token {
    protected String text;
    
    protected Type type;
    
    public enum Type {
      ACRONYM,
      FLICKR,
      FORMAT,
      GOOGLE_VIDEO,
      HTML("html"),
      LINK("html"),
      MUSIC("html"),
      PHOTO("html"),
      SMILEY("html"),
      YOUTUBE_VIDEO("html");
      
      private static final Type[] $VALUES;
      
      private String stringRep;
      
      static {
        GOOGLE_VIDEO = new Type("GOOGLE_VIDEO", 6, "v");
        YOUTUBE_VIDEO = new Type("YOUTUBE_VIDEO", 7, "yt");
        PHOTO = new Type("PHOTO", 8, "p");
        Type type = new Type("FLICKR", 9, "f");
        $VALUES = new Type[] { HTML, FORMAT, LINK, SMILEY, ACRONYM, MUSIC, GOOGLE_VIDEO, YOUTUBE_VIDEO, PHOTO, type };
      }
      
      Type(String param2String1) {
        this.stringRep = param2String1;
      }
      
      public String toString() {
        return this.stringRep;
      }
    }
    
    protected Token(Type param1Type, String param1String) {
      this.type = param1Type;
      this.text = param1String;
    }
    
    public Type getType() {
      return this.type;
    }
    
    public List<String> getInfo() {
      ArrayList<String> arrayList = new ArrayList();
      arrayList.add(getType().toString());
      return arrayList;
    }
    
    public String getRawText() {
      return this.text;
    }
    
    public boolean isMedia() {
      return false;
    }
    
    public boolean isArray() {
      return isHtml() ^ true;
    }
    
    public String toHtml(boolean param1Boolean) {
      throw new AssertionError("not html");
    }
    
    public boolean controlCaps() {
      return false;
    }
    
    public boolean setCaps() {
      return false;
    }
    
    public abstract boolean isHtml();
  }
  
  public enum Type {
    ACRONYM("html"),
    FLICKR("html"),
    FORMAT("html"),
    GOOGLE_VIDEO("html"),
    HTML("html"),
    LINK("html"),
    MUSIC("html"),
    PHOTO("html"),
    SMILEY("html"),
    YOUTUBE_VIDEO("html");
    
    private static final Type[] $VALUES;
    
    private String stringRep;
    
    static {
      ACRONYM = new Type("ACRONYM", 4, "a");
      MUSIC = new Type("MUSIC", 5, "m");
      GOOGLE_VIDEO = new Type("GOOGLE_VIDEO", 6, "v");
      YOUTUBE_VIDEO = new Type("YOUTUBE_VIDEO", 7, "yt");
      PHOTO = new Type("PHOTO", 8, "p");
      Type type = new Type("FLICKR", 9, "f");
      $VALUES = new Type[] { HTML, FORMAT, LINK, SMILEY, ACRONYM, MUSIC, GOOGLE_VIDEO, YOUTUBE_VIDEO, PHOTO, type };
    }
    
    Type(String param1String1) {
      this.stringRep = param1String1;
    }
    
    public String toString() {
      return this.stringRep;
    }
  }
  
  class Html extends Token {
    private String html;
    
    public Html(AbstractMessageParser this$0, String param1String1) {
      super(AbstractMessageParser.Token.Type.HTML, (String)this$0);
      this.html = param1String1;
    }
    
    public boolean isHtml() {
      return true;
    }
    
    public String toHtml(boolean param1Boolean) {
      String str1 = this.html, str2 = str1;
      if (param1Boolean)
        str2 = str1.toUpperCase(); 
      return str2;
    }
    
    public List<String> getInfo() {
      throw new UnsupportedOperationException();
    }
    
    public void trimLeadingWhitespace() {
      this.text = trimLeadingWhitespace(this.text);
      this.html = trimLeadingWhitespace(this.html);
    }
    
    public void trimTrailingWhitespace() {
      this.text = trimTrailingWhitespace(this.text);
      this.html = trimTrailingWhitespace(this.html);
    }
    
    private static String trimLeadingWhitespace(String param1String) {
      byte b = 0;
      while (b < param1String.length() && 
        Character.isWhitespace(param1String.charAt(b)))
        b++; 
      return param1String.substring(b);
    }
    
    public static String trimTrailingWhitespace(String param1String) {
      int i = param1String.length();
      while (i > 0 && Character.isWhitespace(param1String.charAt(i - 1)))
        i--; 
      return param1String.substring(0, i);
    }
  }
  
  class MusicTrack extends Token {
    private String track;
    
    public MusicTrack(AbstractMessageParser this$0) {
      super(AbstractMessageParser.Token.Type.MUSIC, (String)this$0);
      this.track = (String)this$0;
    }
    
    public String getTrack() {
      return this.track;
    }
    
    public boolean isHtml() {
      return false;
    }
    
    public List<String> getInfo() {
      List<String> list = super.getInfo();
      list.add(getTrack());
      return list;
    }
  }
  
  class Link extends Token {
    private String url;
    
    public Link(AbstractMessageParser this$0, String param1String1) {
      super(AbstractMessageParser.Token.Type.LINK, param1String1);
      this.url = (String)this$0;
    }
    
    public String getURL() {
      return this.url;
    }
    
    public boolean isHtml() {
      return false;
    }
    
    public List<String> getInfo() {
      List<String> list = super.getInfo();
      list.add(getURL());
      list.add(getRawText());
      return list;
    }
  }
  
  class Video extends Token {
    private static final Pattern URL_PATTERN = Pattern.compile("(?i)http://video\\.google\\.[a-z0-9]+(?:\\.[a-z0-9]+)?/videoplay\\?.*?\\bdocid=(-?\\d+).*");
    
    private String docid;
    
    public Video(AbstractMessageParser this$0, String param1String1) {
      super(AbstractMessageParser.Token.Type.GOOGLE_VIDEO, param1String1);
      this.docid = (String)this$0;
    }
    
    public String getDocID() {
      return this.docid;
    }
    
    public boolean isHtml() {
      return false;
    }
    
    public boolean isMedia() {
      return true;
    }
    
    public static Video matchURL(String param1String1, String param1String2) {
      Matcher matcher = URL_PATTERN.matcher(param1String1);
      if (matcher.matches())
        return new Video(matcher.group(1), param1String2); 
      return null;
    }
    
    public List<String> getInfo() {
      List<String> list = super.getInfo();
      list.add(getRssUrl(this.docid));
      list.add(getURL(this.docid));
      return list;
    }
    
    public static String getRssUrl(String param1String) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("http://video.google.com/videofeed?type=docid&output=rss&sourceid=gtalk&docid=");
      stringBuilder.append(param1String);
      return stringBuilder.toString();
    }
    
    public static String getURL(String param1String) {
      return getURL(param1String, null);
    }
    
    public static String getURL(String param1String1, String param1String2) {
      String str;
      if (param1String2 == null) {
        str = "";
      } else {
        str = param1String2;
        if (param1String2.length() > 0) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append(param1String2);
          stringBuilder1.append("&");
          str = stringBuilder1.toString();
        } 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("http://video.google.com/videoplay?");
      stringBuilder.append(str);
      stringBuilder.append("docid=");
      stringBuilder.append(param1String1);
      return stringBuilder.toString();
    }
  }
  
  class YouTubeVideo extends Token {
    private static final Pattern URL_PATTERN = Pattern.compile("(?i)http://(?:[a-z0-9]+\\.)?youtube\\.[a-z0-9]+(?:\\.[a-z0-9]+)?/watch\\?.*\\bv=([-_a-zA-Z0-9=]+).*");
    
    private String docid;
    
    public YouTubeVideo(AbstractMessageParser this$0, String param1String1) {
      super(AbstractMessageParser.Token.Type.YOUTUBE_VIDEO, param1String1);
      this.docid = (String)this$0;
    }
    
    public String getDocID() {
      return this.docid;
    }
    
    public boolean isHtml() {
      return false;
    }
    
    public boolean isMedia() {
      return true;
    }
    
    public static YouTubeVideo matchURL(String param1String1, String param1String2) {
      Matcher matcher = URL_PATTERN.matcher(param1String1);
      if (matcher.matches())
        return new YouTubeVideo(matcher.group(1), param1String2); 
      return null;
    }
    
    public List<String> getInfo() {
      List<String> list = super.getInfo();
      list.add(getRssUrl(this.docid));
      list.add(getURL(this.docid));
      return list;
    }
    
    public static String getRssUrl(String param1String) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("http://youtube.com/watch?v=");
      stringBuilder.append(param1String);
      return stringBuilder.toString();
    }
    
    public static String getURL(String param1String) {
      return getURL(param1String, null);
    }
    
    public static String getURL(String param1String1, String param1String2) {
      String str;
      if (param1String2 == null) {
        str = "";
      } else {
        str = param1String2;
        if (param1String2.length() > 0) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append(param1String2);
          stringBuilder1.append("&");
          str = stringBuilder1.toString();
        } 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("http://youtube.com/watch?");
      stringBuilder.append(str);
      stringBuilder.append("v=");
      stringBuilder.append(param1String1);
      return stringBuilder.toString();
    }
    
    public static String getPrefixedURL(boolean param1Boolean, String param1String1, String param1String2, String param1String3) {
      String str1, str2 = "";
      if (param1Boolean)
        str2 = "http://"; 
      String str3 = param1String1;
      if (param1String1 == null)
        str3 = ""; 
      if (param1String3 == null) {
        param1String1 = "";
      } else {
        param1String1 = param1String3;
        if (param1String3.length() > 0) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append(param1String3);
          stringBuilder1.append("&");
          str1 = stringBuilder1.toString();
        } 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append(str3);
      stringBuilder.append("youtube.com/watch?");
      stringBuilder.append(str1);
      stringBuilder.append("v=");
      stringBuilder.append(param1String2);
      return stringBuilder.toString();
    }
  }
  
  class Photo extends Token {
    private static final Pattern URL_PATTERN = Pattern.compile("http://picasaweb.google.com/([^/?#&]+)/+((?!searchbrowse)[^/?#&]+)(?:/|/photo)?(?:\\?[^#]*)?(?:#(.*))?");
    
    private String album;
    
    private String photo;
    
    private String user;
    
    public Photo(AbstractMessageParser this$0, String param1String1, String param1String2, String param1String3) {
      super(AbstractMessageParser.Token.Type.PHOTO, param1String3);
      this.user = (String)this$0;
      this.album = param1String1;
      this.photo = param1String2;
    }
    
    public String getUser() {
      return this.user;
    }
    
    public String getAlbum() {
      return this.album;
    }
    
    public String getPhoto() {
      return this.photo;
    }
    
    public boolean isHtml() {
      return false;
    }
    
    public boolean isMedia() {
      return true;
    }
    
    public static Photo matchURL(String param1String1, String param1String2) {
      Matcher matcher = URL_PATTERN.matcher(param1String1);
      if (matcher.matches())
        return new Photo(matcher.group(1), matcher.group(2), matcher.group(3), param1String2); 
      return null;
    }
    
    public List<String> getInfo() {
      List<String> list = super.getInfo();
      list.add(getRssUrl(getUser()));
      list.add(getAlbumURL(getUser(), getAlbum()));
      if (getPhoto() != null) {
        list.add(getPhotoURL(getUser(), getAlbum(), getPhoto()));
      } else {
        list.add((String)null);
      } 
      return list;
    }
    
    public static String getRssUrl(String param1String) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("http://picasaweb.google.com/data/feed/api/user/");
      stringBuilder.append(param1String);
      stringBuilder.append("?category=album&alt=rss");
      return stringBuilder.toString();
    }
    
    public static String getAlbumURL(String param1String1, String param1String2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("http://picasaweb.google.com/");
      stringBuilder.append(param1String1);
      stringBuilder.append("/");
      stringBuilder.append(param1String2);
      return stringBuilder.toString();
    }
    
    public static String getPhotoURL(String param1String1, String param1String2, String param1String3) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("http://picasaweb.google.com/");
      stringBuilder.append(param1String1);
      stringBuilder.append("/");
      stringBuilder.append(param1String2);
      stringBuilder.append("/photo#");
      stringBuilder.append(param1String3);
      return stringBuilder.toString();
    }
  }
  
  class FlickrPhoto extends Token {
    private static final Pattern GROUPING_PATTERN = Pattern.compile("http://(?:www.)?flickr.com/photos/([^/?#&]+)/(tags|sets)/([^/?#&]+)/?");
    
    private static final String SETS = "sets";
    
    private static final String TAGS = "tags";
    
    private static final Pattern URL_PATTERN = Pattern.compile("http://(?:www.)?flickr.com/photos/([^/?#&]+)/?([^/?#&]+)?/?.*");
    
    private String grouping;
    
    private String groupingId;
    
    private String photo;
    
    private String user;
    
    static {
    
    }
    
    public FlickrPhoto(AbstractMessageParser this$0, String param1String1, String param1String2, String param1String3, String param1String4) {
      super(AbstractMessageParser.Token.Type.FLICKR, param1String4);
      boolean bool = "tags".equals(this$0);
      param1String4 = null;
      if (!bool) {
        this.user = (String)this$0;
        String str = param1String4;
        if (!"show".equals(param1String1))
          str = param1String1; 
        this.photo = str;
        this.grouping = param1String2;
        this.groupingId = param1String3;
      } else {
        this.user = null;
        this.photo = null;
        this.grouping = "tags";
        this.groupingId = param1String1;
      } 
    }
    
    public String getUser() {
      return this.user;
    }
    
    public String getPhoto() {
      return this.photo;
    }
    
    public String getGrouping() {
      return this.grouping;
    }
    
    public String getGroupingId() {
      return this.groupingId;
    }
    
    public boolean isHtml() {
      return false;
    }
    
    public boolean isMedia() {
      return true;
    }
    
    public static FlickrPhoto matchURL(String param1String1, String param1String2) {
      Matcher matcher2 = GROUPING_PATTERN.matcher(param1String1);
      if (matcher2.matches())
        return new FlickrPhoto(matcher2.group(1), null, matcher2.group(2), matcher2.group(3), param1String2); 
      Matcher matcher1 = URL_PATTERN.matcher(param1String1);
      if (matcher1.matches())
        return new FlickrPhoto(matcher1.group(1), matcher1.group(2), null, null, param1String2); 
      return null;
    }
    
    public List<String> getInfo() {
      List<String> list = super.getInfo();
      list.add(getUrl());
      String str1 = getUser(), str2 = "";
      if (str1 != null) {
        str1 = getUser();
      } else {
        str1 = "";
      } 
      list.add(str1);
      if (getPhoto() != null) {
        str1 = getPhoto();
      } else {
        str1 = "";
      } 
      list.add(str1);
      if (getGrouping() != null) {
        str1 = getGrouping();
      } else {
        str1 = "";
      } 
      list.add(str1);
      str1 = str2;
      if (getGroupingId() != null)
        str1 = getGroupingId(); 
      list.add(str1);
      return list;
    }
    
    public String getUrl() {
      if ("sets".equals(this.grouping))
        return getUserSetsURL(this.user, this.groupingId); 
      if ("tags".equals(this.grouping)) {
        String str1 = this.user;
        if (str1 != null)
          return getUserTagsURL(str1, this.groupingId); 
        return getTagsURL(this.groupingId);
      } 
      String str = this.photo;
      if (str != null)
        return getPhotoURL(this.user, str); 
      return getUserURL(this.user);
    }
    
    public static String getRssUrl(String param1String) {
      return null;
    }
    
    public static String getTagsURL(String param1String) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("http://flickr.com/photos/tags/");
      stringBuilder.append(param1String);
      return stringBuilder.toString();
    }
    
    public static String getUserURL(String param1String) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("http://flickr.com/photos/");
      stringBuilder.append(param1String);
      return stringBuilder.toString();
    }
    
    public static String getPhotoURL(String param1String1, String param1String2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("http://flickr.com/photos/");
      stringBuilder.append(param1String1);
      stringBuilder.append("/");
      stringBuilder.append(param1String2);
      return stringBuilder.toString();
    }
    
    public static String getUserTagsURL(String param1String1, String param1String2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("http://flickr.com/photos/");
      stringBuilder.append(param1String1);
      stringBuilder.append("/tags/");
      stringBuilder.append(param1String2);
      return stringBuilder.toString();
    }
    
    public static String getUserSetsURL(String param1String1, String param1String2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("http://flickr.com/photos/");
      stringBuilder.append(param1String1);
      stringBuilder.append("/sets/");
      stringBuilder.append(param1String2);
      return stringBuilder.toString();
    }
  }
  
  class Smiley extends Token {
    public Smiley(AbstractMessageParser this$0) {
      super(AbstractMessageParser.Token.Type.SMILEY, (String)this$0);
    }
    
    public boolean isHtml() {
      return false;
    }
    
    public List<String> getInfo() {
      List<String> list = super.getInfo();
      list.add(getRawText());
      return list;
    }
  }
  
  class Acronym extends Token {
    private String value;
    
    public Acronym(AbstractMessageParser this$0, String param1String1) {
      super(AbstractMessageParser.Token.Type.ACRONYM, (String)this$0);
      this.value = param1String1;
    }
    
    public String getValue() {
      return this.value;
    }
    
    public boolean isHtml() {
      return false;
    }
    
    public List<String> getInfo() {
      List<String> list = super.getInfo();
      list.add(getRawText());
      list.add(getValue());
      return list;
    }
  }
  
  class Format extends Token {
    private char ch;
    
    private boolean matched;
    
    private boolean start;
    
    public Format(AbstractMessageParser this$0, boolean param1Boolean) {
      super(AbstractMessageParser.Token.Type.FORMAT, String.valueOf(this$0));
      this.ch = this$0;
      this.start = param1Boolean;
    }
    
    public void setMatched(boolean param1Boolean) {
      this.matched = param1Boolean;
    }
    
    public boolean isHtml() {
      return true;
    }
    
    public String toHtml(boolean param1Boolean) {
      String str;
      if (this.matched) {
        if (this.start) {
          str = getFormatStart(this.ch);
        } else {
          str = getFormatEnd(this.ch);
        } 
        return str;
      } 
      char c = this.ch;
      if (c == '"') {
        str = "&quot;";
      } else {
        str = String.valueOf(c);
      } 
      return str;
    }
    
    public List<String> getInfo() {
      throw new UnsupportedOperationException();
    }
    
    public boolean controlCaps() {
      boolean bool;
      if (this.ch == '^') {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean setCaps() {
      return this.start;
    }
    
    private String getFormatStart(char param1Char) {
      if (param1Char != '"') {
        if (param1Char != '*') {
          if (param1Char != '^') {
            if (param1Char == '_')
              return "<i>"; 
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("unknown format '");
            stringBuilder.append(param1Char);
            stringBuilder.append("'");
            throw new AssertionError(stringBuilder.toString());
          } 
          return "<b><font color=\"#005FFF\">";
        } 
        return "<b>";
      } 
      return "<font color=\"#999999\">“";
    }
    
    private String getFormatEnd(char param1Char) {
      if (param1Char != '"') {
        if (param1Char != '*') {
          if (param1Char != '^') {
            if (param1Char == '_')
              return "</i>"; 
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("unknown format '");
            stringBuilder.append(param1Char);
            stringBuilder.append("'");
            throw new AssertionError(stringBuilder.toString());
          } 
          return "</font></b>";
        } 
        return "</b>";
      } 
      return "”</font>";
    }
  }
  
  private void addToken(Token paramToken) {
    this.tokens.add(paramToken);
  }
  
  public String toHtml() {
    StringBuilder stringBuilder = new StringBuilder();
    for (Iterator<Part> iterator = this.parts.iterator(); iterator.hasNext(); ) {
      Part part = iterator.next();
      boolean bool = false;
      stringBuilder.append("<p>");
      for (Token token : part.getTokens()) {
        if (token.isHtml()) {
          stringBuilder.append(token.toHtml(bool));
        } else {
          StringBuilder stringBuilder1;
          Photo photo;
          String str2;
          YouTubeVideo youTubeVideo;
          String str1;
          Video video;
          String str3;
          switch (token.getType()) {
            default:
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append("unknown token type: ");
              stringBuilder1.append(token.getType());
              throw new AssertionError(stringBuilder1.toString());
            case FLICKR:
              photo = (Photo)token;
              stringBuilder.append("<a href=\"");
              stringBuilder.append(((FlickrPhoto)token).getUrl());
              stringBuilder.append("\">");
              stringBuilder.append(token.getRawText());
              stringBuilder.append("</a>");
              break;
            case PHOTO:
              stringBuilder.append("<a href=\"");
              photo = (Photo)token;
              str2 = photo.getUser();
              str3 = ((Photo)token).getAlbum();
              stringBuilder.append(Photo.getAlbumURL(str2, str3));
              stringBuilder.append("\">");
              stringBuilder.append(token.getRawText());
              stringBuilder.append("</a>");
              break;
            case YOUTUBE_VIDEO:
              stringBuilder.append("<a href=\"");
              youTubeVideo = (YouTubeVideo)token;
              youTubeVideo = (YouTubeVideo)token;
              str1 = youTubeVideo.getDocID();
              stringBuilder.append(YouTubeVideo.getURL(str1));
              stringBuilder.append("\">");
              stringBuilder.append(token.getRawText());
              stringBuilder.append("</a>");
              break;
            case GOOGLE_VIDEO:
              stringBuilder.append("<a href=\"");
              video = (Video)token;
              stringBuilder.append(Video.getURL(((Video)token).getDocID()));
              stringBuilder.append("\">");
              stringBuilder.append(token.getRawText());
              stringBuilder.append("</a>");
              break;
            case MUSIC:
              stringBuilder.append(((MusicTrack)token).getTrack());
              break;
            case ACRONYM:
              stringBuilder.append(token.getRawText());
              break;
            case SMILEY:
              stringBuilder.append(token.getRawText());
              break;
            case LINK:
              stringBuilder.append("<a href=\"");
              stringBuilder.append(((Link)token).getURL());
              stringBuilder.append("\">");
              stringBuilder.append(token.getRawText());
              stringBuilder.append("</a>");
              break;
          } 
        } 
        if (token.controlCaps())
          bool = token.setCaps(); 
      } 
      stringBuilder.append("</p>\n");
    } 
    return stringBuilder.toString();
  }
  
  protected static String reverse(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    for (int i = paramString.length() - 1; i >= 0; i--)
      stringBuilder.append(paramString.charAt(i)); 
    return stringBuilder.toString();
  }
  
  public static class TrieNode {
    private final HashMap<Character, TrieNode> children = new HashMap<>();
    
    private String text;
    
    private String value;
    
    public TrieNode() {
      this("");
    }
    
    public TrieNode(String param1String) {
      this.text = param1String;
    }
    
    public final boolean exists() {
      boolean bool;
      if (this.value != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public final String getText() {
      return this.text;
    }
    
    public final String getValue() {
      return this.value;
    }
    
    public void setValue(String param1String) {
      this.value = param1String;
    }
    
    public TrieNode getChild(char param1Char) {
      return this.children.get(Character.valueOf(param1Char));
    }
    
    public TrieNode getOrCreateChild(char param1Char) {
      Character character = Character.valueOf(param1Char);
      TrieNode trieNode1 = this.children.get(character);
      TrieNode trieNode2 = trieNode1;
      if (trieNode1 == null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.text);
        stringBuilder.append(String.valueOf(param1Char));
        trieNode2 = new TrieNode(stringBuilder.toString());
        this.children.put(character, trieNode2);
      } 
      return trieNode2;
    }
    
    public static void addToTrie(TrieNode param1TrieNode, String param1String1, String param1String2) {
      byte b = 0;
      while (b < param1String1.length()) {
        param1TrieNode = param1TrieNode.getOrCreateChild(param1String1.charAt(b));
        b++;
      } 
      param1TrieNode.setValue(param1String2);
    }
  }
  
  private static boolean matches(TrieNode paramTrieNode, String paramString) {
    byte b = 0;
    while (b < paramString.length()) {
      paramTrieNode = paramTrieNode.getChild(paramString.charAt(b));
      if (paramTrieNode == null)
        break; 
      if (paramTrieNode.exists())
        return true; 
      b++;
    } 
    return false;
  }
  
  private static TrieNode longestMatch(TrieNode paramTrieNode, AbstractMessageParser paramAbstractMessageParser, int paramInt) {
    return longestMatch(paramTrieNode, paramAbstractMessageParser, paramInt, false);
  }
  
  private static TrieNode longestMatch(TrieNode paramTrieNode, AbstractMessageParser paramAbstractMessageParser, int paramInt, boolean paramBoolean) {
    TrieNode trieNode = null;
    for (; paramInt < paramAbstractMessageParser.getRawText().length(); paramInt = i) {
      String str = paramAbstractMessageParser.getRawText();
      int i = paramInt + 1;
      paramTrieNode = paramTrieNode.getChild(str.charAt(paramInt));
      if (paramTrieNode == null)
        break; 
      if (paramTrieNode.exists()) {
        if (paramAbstractMessageParser.isWordBreak(i)) {
          trieNode = paramTrieNode;
          paramInt = i;
          continue;
        } 
        if (paramBoolean && paramAbstractMessageParser.isSmileyBreak(i)) {
          trieNode = paramTrieNode;
          paramInt = i;
          continue;
        } 
      } 
    } 
    return trieNode;
  }
  
  protected abstract Resources getResources();
  
  public static class Part {
    private String meText;
    
    private ArrayList<AbstractMessageParser.Token> tokens;
    
    public Part() {
      this.tokens = new ArrayList<>();
    }
    
    public String getType(boolean param1Boolean) {
      String str;
      StringBuilder stringBuilder = new StringBuilder();
      if (param1Boolean) {
        str = "s";
      } else {
        str = "r";
      } 
      stringBuilder.append(str);
      stringBuilder.append(getPartType());
      return stringBuilder.toString();
    }
    
    private String getPartType() {
      if (isMedia())
        return "d"; 
      if (this.meText != null)
        return "m"; 
      return "";
    }
    
    public boolean isMedia() {
      int i = this.tokens.size();
      boolean bool1 = false, bool2 = bool1;
      if (i == 1) {
        bool2 = bool1;
        if (((AbstractMessageParser.Token)this.tokens.get(0)).isMedia())
          bool2 = true; 
      } 
      return bool2;
    }
    
    public AbstractMessageParser.Token getMediaToken() {
      if (isMedia())
        return this.tokens.get(0); 
      return null;
    }
    
    public void add(AbstractMessageParser.Token param1Token) {
      if (!isMedia()) {
        this.tokens.add(param1Token);
        return;
      } 
      throw new AssertionError("media ");
    }
    
    public void setMeText(String param1String) {
      this.meText = param1String;
    }
    
    public String getRawText() {
      StringBuilder stringBuilder = new StringBuilder();
      String str = this.meText;
      if (str != null)
        stringBuilder.append(str); 
      for (byte b = 0; b < this.tokens.size(); b++)
        stringBuilder.append(((AbstractMessageParser.Token)this.tokens.get(b)).getRawText()); 
      return stringBuilder.toString();
    }
    
    public ArrayList<AbstractMessageParser.Token> getTokens() {
      return this.tokens;
    }
  }
  
  public static interface Resources {
    AbstractMessageParser.TrieNode getAcronyms();
    
    AbstractMessageParser.TrieNode getDomainSuffixes();
    
    Set<String> getSchemes();
    
    AbstractMessageParser.TrieNode getSmileys();
  }
}
