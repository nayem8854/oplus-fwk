package com.google.android.util;

import java.util.HashMap;
import java.util.Set;

public class SmileyResources implements AbstractMessageParser.Resources {
  private HashMap<String, Integer> mSmileyToRes = new HashMap<>();
  
  private final AbstractMessageParser.TrieNode smileys;
  
  public int getSmileyRes(String paramString) {
    Integer integer = this.mSmileyToRes.get(paramString);
    if (integer == null)
      return -1; 
    return integer.intValue();
  }
  
  public SmileyResources(String[] paramArrayOfString, int[] paramArrayOfint) {
    this.smileys = new AbstractMessageParser.TrieNode();
    for (byte b = 0; b < paramArrayOfString.length; b++) {
      AbstractMessageParser.TrieNode.addToTrie(this.smileys, paramArrayOfString[b], "");
      this.mSmileyToRes.put(paramArrayOfString[b], Integer.valueOf(paramArrayOfint[b]));
    } 
  }
  
  public Set<String> getSchemes() {
    return null;
  }
  
  public AbstractMessageParser.TrieNode getDomainSuffixes() {
    return null;
  }
  
  public AbstractMessageParser.TrieNode getSmileys() {
    return this.smileys;
  }
  
  public AbstractMessageParser.TrieNode getAcronyms() {
    return null;
  }
}
