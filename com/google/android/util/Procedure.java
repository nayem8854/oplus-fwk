package com.google.android.util;

public interface Procedure<T> {
  void apply(T paramT);
}
