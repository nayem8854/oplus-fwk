package com.google.android.util;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import java.util.ArrayList;

public class SmileyParser extends AbstractMessageParser {
  private SmileyResources mRes;
  
  public SmileyParser(String paramString, SmileyResources paramSmileyResources) {
    super(paramString, true, false, false, false, false, false);
    this.mRes = paramSmileyResources;
  }
  
  protected AbstractMessageParser.Resources getResources() {
    return this.mRes;
  }
  
  public CharSequence getSpannableString(Context paramContext) {
    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
    if (getPartCount() == 0)
      return ""; 
    AbstractMessageParser.Part part = getPart(0);
    ArrayList<AbstractMessageParser.Token> arrayList = part.getTokens();
    int i = arrayList.size();
    for (byte b = 0; b < i; b++) {
      AbstractMessageParser.Token token = arrayList.get(b);
      int j = spannableStringBuilder.length();
      spannableStringBuilder.append(token.getRawText());
      if (token.getType() == AbstractMessageParser.Token.Type.SMILEY) {
        int k = this.mRes.getSmileyRes(token.getRawText());
        if (k != -1) {
          ImageSpan imageSpan = new ImageSpan(paramContext, k);
          k = spannableStringBuilder.length();
          spannableStringBuilder.setSpan(imageSpan, j, k, 33);
        } 
      } 
    } 
    return (CharSequence)spannableStringBuilder;
  }
}
