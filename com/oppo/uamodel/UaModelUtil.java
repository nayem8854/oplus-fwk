package com.oppo.uamodel;

import android.os.Build;
import android.os.SystemProperties;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

public class UaModelUtil {
  private static final List<String> UAMODELLIST = Arrays.asList(new String[] { "com.tencent.mobileqq", "com.sina.weibo", "com.ruanmei.ithome", "com.qzone", "com.tencent.karaoke", "com.sinamobile.uagenerator" });
  
  private String mCustomizedThemeUuidFromFile = null;
  
  private String mOppoModel = SystemProperties.get("ro.product.oppo_model");
  
  private String mLbTheme = SystemProperties.get("persist.sys.oppo.theme");
  
  private String mColorTheme = SystemProperties.get("ro.hw.phone.color");
  
  private String mBarceCustom = SystemProperties.get("ro.product.oppo.custom.Barce");
  
  private String mGUNDAM = SystemProperties.get("ro.vendor.oplus.gundam");
  
  private boolean mUuidMatch = false;
  
  private static final String CUSTOMIZED_THEME_EXTENSION = ".theme";
  
  private static final String CUSTOMIZED_THEME_PREFIX = "Customized_";
  
  private static final String CUSTOMIZED_THEME_ROOT_PATH = "data/theme_bak/customized_theme/";
  
  private static final String TAG = "UAmodel";
  
  private static final String UUID = "FA2A296A2A6842A6A9E34B75384EC45A";
  
  private static final int UUID_LENGTH = 32;
  
  public boolean uaModelOk(String paramString) {
    boolean bool1 = false;
    boolean bool2 = this.mOppoModel.equals("");
    boolean bool3 = UAMODELLIST.contains(paramString);
    boolean bool4 = bool1;
    if (!bool2) {
      bool4 = bool1;
      if (bool3) {
        bool4 = bool1;
        if (this.mLbTheme.equals("lb_theme"))
          bool4 = true; 
      } 
    } 
    return bool4;
  }
  
  public boolean gundamCustomUaModelOk(String paramString) {
    boolean bool1 = false;
    boolean bool2 = this.mOppoModel.equals("");
    boolean bool3 = UAMODELLIST.contains(paramString);
    boolean bool4 = bool1;
    if (!bool2) {
      bool4 = bool1;
      if (bool3) {
        bool4 = bool1;
        if (this.mGUNDAM.equals("true"))
          bool4 = true; 
      } 
    } 
    return bool4;
  }
  
  public boolean barceCustomUaModelOk(String paramString) {
    boolean bool1 = false;
    boolean bool2 = this.mOppoModel.equals("");
    boolean bool3 = UAMODELLIST.contains(paramString);
    boolean bool4 = bool1;
    if (!bool2) {
      bool4 = bool1;
      if (bool3) {
        bool4 = bool1;
        if (this.mBarceCustom.equals("Barce"))
          bool4 = true; 
      } 
    } 
    return bool4;
  }
  
  public boolean renoZUaModelOk(String paramString) {
    boolean bool1 = false;
    boolean bool2 = this.mOppoModel.equals("");
    boolean bool3 = UAMODELLIST.contains(paramString);
    boolean bool4 = bool1;
    if (!bool2) {
      bool4 = bool1;
      if (bool3) {
        bool4 = bool1;
        if (this.mColorTheme.equals("FFFF3000"))
          bool4 = true; 
      } 
    } 
    return bool4;
  }
  
  public boolean findX2LBThemeUaModelOk(String paramString) {
    boolean bool1 = false;
    boolean bool2 = this.mOppoModel.equals("");
    boolean bool3 = UAMODELLIST.contains(paramString);
    boolean bool4 = bool1;
    if (!bool2) {
      bool4 = bool1;
      if (bool3) {
        bool4 = bool1;
        if (this.mColorTheme.equals("00FFF002"))
          bool4 = true; 
      } 
    } 
    return bool4;
  }
  
  public boolean renoACE2EVAUaModelOk(String paramString) {
    boolean bool1 = false;
    boolean bool2 = this.mOppoModel.equals("");
    boolean bool3 = UAMODELLIST.contains(paramString);
    boolean bool4 = bool1;
    if (!bool2) {
      bool4 = bool1;
      if (bool3) {
        bool4 = bool1;
        if (this.mColorTheme.equals("00FFF003"))
          bool4 = true; 
      } 
    } 
    return bool4;
  }
  
  public void isLbTheme() {
    String str1 = scanCustomizedTheme();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("data/theme_bak/customized_theme/");
    stringBuilder.append(str1);
    String str2 = getThemeUuidFromThemeFile(stringBuilder.toString());
    if (str2 != null)
      this.mUuidMatch = str2.equals("FA2A296A2A6842A6A9E34B75384EC45A"); 
    if (this.mUuidMatch) {
      SystemProperties.set("persist.sys.oppo.theme", "lb_theme");
    } else {
      SystemProperties.set("persist.sys.oppo.theme", "nlb_theme");
    } 
  }
  
  public void changeToSpecialModel() {
    try {
      Field field = Build.class.getField("MODEL");
      field.setAccessible(true);
      field.set(Build.class, this.mOppoModel);
    } catch (NoSuchFieldException|IllegalAccessException noSuchFieldException) {
      noSuchFieldException.printStackTrace();
    } 
  }
  
  private String getThemeUuidFromThemeFile(String paramString) {
    String str;
    ZipInputStream zipInputStream1 = null, zipInputStream2 = null, zipInputStream3 = null;
    if ((new File(paramString)).exists()) {
      BufferedInputStream bufferedInputStream1 = null, bufferedInputStream2 = null;
      ZipInputStream zipInputStream4 = null, zipInputStream5 = null;
      ZipFile zipFile1 = null, zipFile2 = null;
      BufferedInputStream bufferedInputStream3 = bufferedInputStream2;
      ZipInputStream zipInputStream6 = zipInputStream5;
      ZipFile zipFile3 = zipFile2;
      zipInputStream2 = zipInputStream1;
      BufferedInputStream bufferedInputStream4 = bufferedInputStream1;
      ZipInputStream zipInputStream7 = zipInputStream4;
      ZipFile zipFile4 = zipFile1;
      try {
        ZipFile zipFile = new ZipFile();
        bufferedInputStream3 = bufferedInputStream2;
        zipInputStream6 = zipInputStream5;
        zipFile3 = zipFile2;
        zipInputStream2 = zipInputStream1;
        bufferedInputStream4 = bufferedInputStream1;
        zipInputStream7 = zipInputStream4;
        zipFile4 = zipFile1;
        this(paramString);
        bufferedInputStream3 = bufferedInputStream2;
        zipInputStream6 = zipInputStream5;
        zipFile3 = zipFile;
        zipInputStream2 = zipInputStream1;
        bufferedInputStream4 = bufferedInputStream1;
        zipInputStream7 = zipInputStream4;
        zipFile4 = zipFile;
        BufferedInputStream bufferedInputStream = new BufferedInputStream();
        bufferedInputStream3 = bufferedInputStream2;
        zipInputStream6 = zipInputStream5;
        zipFile3 = zipFile;
        zipInputStream2 = zipInputStream1;
        bufferedInputStream4 = bufferedInputStream1;
        zipInputStream7 = zipInputStream4;
        zipFile4 = zipFile;
        FileInputStream fileInputStream = new FileInputStream();
        bufferedInputStream3 = bufferedInputStream2;
        zipInputStream6 = zipInputStream5;
        zipFile3 = zipFile;
        zipInputStream2 = zipInputStream1;
        bufferedInputStream4 = bufferedInputStream1;
        zipInputStream7 = zipInputStream4;
        zipFile4 = zipFile;
        this(paramString);
        bufferedInputStream3 = bufferedInputStream2;
        zipInputStream6 = zipInputStream5;
        zipFile3 = zipFile;
        zipInputStream2 = zipInputStream1;
        bufferedInputStream4 = bufferedInputStream1;
        zipInputStream7 = zipInputStream4;
        zipFile4 = zipFile;
        this(fileInputStream);
        bufferedInputStream2 = bufferedInputStream;
        bufferedInputStream3 = bufferedInputStream2;
        zipInputStream6 = zipInputStream5;
        zipFile3 = zipFile;
        zipInputStream2 = zipInputStream1;
        bufferedInputStream4 = bufferedInputStream2;
        zipInputStream7 = zipInputStream4;
        zipFile4 = zipFile;
        ZipInputStream zipInputStream = new ZipInputStream();
        bufferedInputStream3 = bufferedInputStream2;
        zipInputStream6 = zipInputStream5;
        zipFile3 = zipFile;
        zipInputStream2 = zipInputStream1;
        bufferedInputStream4 = bufferedInputStream2;
        zipInputStream7 = zipInputStream4;
        zipFile4 = zipFile;
        this(bufferedInputStream2);
        zipInputStream1 = zipInputStream;
        zipInputStream = zipInputStream3;
        while (true) {
          bufferedInputStream3 = bufferedInputStream2;
          zipInputStream6 = zipInputStream1;
          zipFile3 = zipFile;
          zipInputStream2 = zipInputStream;
          bufferedInputStream4 = bufferedInputStream2;
          zipInputStream7 = zipInputStream1;
          zipFile4 = zipFile;
          ZipEntry zipEntry = zipInputStream1.getNextEntry();
          zipInputStream2 = zipInputStream;
          if (zipEntry != null) {
            String str2;
            bufferedInputStream3 = bufferedInputStream2;
            zipInputStream6 = zipInputStream1;
            zipFile3 = zipFile;
            zipInputStream2 = zipInputStream;
            bufferedInputStream4 = bufferedInputStream2;
            zipInputStream7 = zipInputStream1;
            zipFile4 = zipFile;
            if (zipEntry.isDirectory())
              continue; 
            bufferedInputStream3 = bufferedInputStream2;
            zipInputStream6 = zipInputStream1;
            zipFile3 = zipFile;
            zipInputStream2 = zipInputStream;
            bufferedInputStream4 = bufferedInputStream2;
            zipInputStream7 = zipInputStream1;
            zipFile4 = zipFile;
            String str3 = zipEntry.getName();
            bufferedInputStream3 = bufferedInputStream2;
            zipInputStream6 = zipInputStream1;
            zipFile3 = zipFile;
            zipInputStream2 = zipInputStream;
            bufferedInputStream4 = bufferedInputStream2;
            zipInputStream7 = zipInputStream1;
            zipFile4 = zipFile;
            StringBuilder stringBuilder = new StringBuilder();
            bufferedInputStream3 = bufferedInputStream2;
            zipInputStream6 = zipInputStream1;
            zipFile3 = zipFile;
            zipInputStream2 = zipInputStream;
            bufferedInputStream4 = bufferedInputStream2;
            zipInputStream7 = zipInputStream1;
            zipFile4 = zipFile;
            this();
            bufferedInputStream3 = bufferedInputStream2;
            zipInputStream6 = zipInputStream1;
            zipFile3 = zipFile;
            zipInputStream2 = zipInputStream;
            bufferedInputStream4 = bufferedInputStream2;
            zipInputStream7 = zipInputStream1;
            zipFile4 = zipFile;
            stringBuilder.append("fileName = ");
            bufferedInputStream3 = bufferedInputStream2;
            zipInputStream6 = zipInputStream1;
            zipFile3 = zipFile;
            zipInputStream2 = zipInputStream;
            bufferedInputStream4 = bufferedInputStream2;
            zipInputStream7 = zipInputStream1;
            zipFile4 = zipFile;
            stringBuilder.append(str3);
            bufferedInputStream3 = bufferedInputStream2;
            zipInputStream6 = zipInputStream1;
            zipFile3 = zipFile;
            zipInputStream2 = zipInputStream;
            bufferedInputStream4 = bufferedInputStream2;
            zipInputStream7 = zipInputStream1;
            zipFile4 = zipFile;
            Log.i("UAmodel", stringBuilder.toString());
            bufferedInputStream3 = bufferedInputStream2;
            zipInputStream6 = zipInputStream1;
            zipFile3 = zipFile;
            zipInputStream2 = zipInputStream;
            bufferedInputStream4 = bufferedInputStream2;
            zipInputStream7 = zipInputStream1;
            zipFile4 = zipFile;
            if (str3.startsWith("previews")) {
              bufferedInputStream3 = bufferedInputStream2;
              zipInputStream6 = zipInputStream1;
              zipFile3 = zipFile;
              zipInputStream2 = zipInputStream;
              bufferedInputStream4 = bufferedInputStream2;
              zipInputStream7 = zipInputStream1;
              zipFile4 = zipFile;
              Log.i("UAmodel", "ignore preview directory to avoid preview picture file name length maybe 32 characters");
              continue;
            } 
            bufferedInputStream3 = bufferedInputStream2;
            zipInputStream6 = zipInputStream1;
            zipFile3 = zipFile;
            zipInputStream2 = zipInputStream;
            bufferedInputStream4 = bufferedInputStream2;
            zipInputStream7 = zipInputStream1;
            zipFile4 = zipFile;
            try {
              int i = str3.lastIndexOf("/");
              bufferedInputStream3 = bufferedInputStream2;
              zipInputStream6 = zipInputStream1;
              zipFile3 = zipFile;
              zipInputStream2 = zipInputStream;
              bufferedInputStream4 = bufferedInputStream2;
              zipInputStream7 = zipInputStream1;
              zipFile4 = zipFile;
              int j = str3.lastIndexOf(".");
              bufferedInputStream3 = bufferedInputStream2;
              zipInputStream6 = zipInputStream1;
              zipFile3 = zipFile;
              zipInputStream2 = zipInputStream;
              bufferedInputStream4 = bufferedInputStream2;
              zipInputStream7 = zipInputStream1;
              zipFile4 = zipFile;
              String str4 = str3.substring(i + 1, j);
            } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
              bufferedInputStream3 = bufferedInputStream2;
              zipInputStream6 = zipInputStream1;
              zipFile3 = zipFile;
              str = str2;
              bufferedInputStream4 = bufferedInputStream2;
              zipInputStream7 = zipInputStream1;
              zipFile4 = zipFile;
              Log.i("UAmodel", indexOutOfBoundsException.getMessage());
            } 
            if (str2 != null) {
              bufferedInputStream3 = bufferedInputStream2;
              zipInputStream6 = zipInputStream1;
              zipFile3 = zipFile;
              str = str2;
              bufferedInputStream4 = bufferedInputStream2;
              zipInputStream7 = zipInputStream1;
              zipFile4 = zipFile;
              int i = str2.length();
              if (i == 32) {
                str = str2;
                break;
              } 
            } 
            continue;
          } 
          break;
        } 
        try {
          zipInputStream1.close();
        } catch (IOException iOException) {
          iOException.printStackTrace();
        } 
        try {
          bufferedInputStream2.close();
        } catch (IOException iOException) {
          iOException.printStackTrace();
        } 
        String str1 = str;
        try {
          zipFile.close();
          str1 = str;
        } catch (IOException iOException) {
          iOException.printStackTrace();
          str = str1;
          str1 = str;
        } 
      } catch (IOException iOException) {
        bufferedInputStream3 = bufferedInputStream4;
        zipInputStream6 = zipInputStream7;
        zipFile3 = zipFile4;
        Log.i("UAmodel", iOException.getMessage());
        if (zipInputStream7 != null)
          try {
            zipInputStream7.close();
          } catch (IOException iOException1) {
            iOException1.printStackTrace();
          }  
        if (bufferedInputStream4 != null)
          try {
            bufferedInputStream4.close();
          } catch (IOException iOException1) {
            iOException1.printStackTrace();
          }  
        String str1 = str;
        if (zipFile4 != null) {
          str1 = str;
          zipFile4.close();
        } else {
          return str1;
        } 
        str1 = str;
      } finally {}
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("theme file not exists : ");
      stringBuilder.append(paramString);
      Log.e("UAmodel", stringBuilder.toString());
      paramString = str;
    } 
    return paramString;
  }
  
  private String scanCustomizedTheme() {
    File file = new File("data/theme_bak/customized_theme/");
    if (file.exists() && file.isDirectory() && (
      file.list()).length > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("themeRoot ");
      stringBuilder.append(file.toString());
      stringBuilder.append(" not empty...");
      Log.i("UAmodel", stringBuilder.toString());
      for (String str : file.list()) {
        if (str.startsWith("Customized_") && str.endsWith(".theme")) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("theme found : data/theme_bak/customized_theme/");
          stringBuilder1.append(str);
          Log.i("UAmodel", stringBuilder1.toString());
          return str;
        } 
      } 
    } 
    return null;
  }
}
