package com.oppo.uamodel;

import android.os.Build;
import android.os.SystemProperties;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

public class UaModelUtil_Michel {
  private String mOppoModel = SystemProperties.get("ro.product.oppo_model");
  
  private String mOppoModel_name = SystemProperties.get("ro.product.oppo_model_name");
  
  private String mMichel = SystemProperties.get("ro.product.device");
  
  private static List<String> uaModelList = Arrays.asList(new String[] { "com.android.uagenerator" });
  
  private String region = SystemProperties.get("ro.oppo.regionmark", "oppo");
  
  private String mMufasa = SystemProperties.get("ro.product.model");
  
  private static List<String> uaModelList_Mufasa = Arrays.asList(new String[] { "com.antutu.ABenchMark", "com.antutu.aibenchmark", "com.antutu.ABenchMark.lite", "com.uzywpq.cqlzahm", "com.antutu.benchmark.full", "club.antutu.benchmark" });
  
  private static final String TAG = "UAmodel";
  
  private static final String mOppoModel_name_Mufasa_RU = "OPPO Reno4 Pro";
  
  private static List<String> uaModelListMichel;
  
  private static List<String> uaModelListMichelName;
  
  static {
    uaModelListMichel = Arrays.asList(new String[] { 
          "com.sinamobile.uagenerator", "com.sina.weibo", "com.tencent.mobileqq", "com.qzone", "com.netease.newsreader.activity", "com.ludashi.benchmark", "com.ruanmei.ithome", "com.tencent.karaoke", "com.qiyi.video", "com.taobao.taobao", 
          "com.tmall.wireless", "tv.pps.mobile", "com.oppo.community" });
    uaModelListMichelName = Arrays.asList(new String[] { "com.antutu.ABenchMark", "com.ss.android.ugc.aweme", "com.ss.android.ugc.live", "com.ss.android.ugc.aweme.lite", "com.ss.android.article.video", "com.oppo.usercenter", "com.heytap.usercenter" });
  }
  
  public boolean Michel_UaModel_Ok(String paramString) {
    boolean bool;
    boolean bool1 = false;
    boolean bool2 = this.mOppoModel.equals("");
    if (uaModelList.contains(paramString) || uaModelListMichel.contains(paramString)) {
      bool = true;
    } else {
      bool = false;
    } 
    boolean bool3 = this.mOppoModel_name.equals("");
    boolean bool4 = uaModelListMichelName.contains(paramString);
    if (!bool2 && bool && this.mMichel.equals("OP4AB5")) {
      bool2 = true;
    } else {
      bool2 = bool1;
      if (!bool3) {
        bool2 = bool1;
        if (bool4) {
          bool2 = bool1;
          if (this.mMichel.equals("OP4AB5")) {
            this.mOppoModel = this.mOppoModel_name;
            bool2 = true;
          } 
        } 
      } 
    } 
    bool1 = bool2;
    if (this.region.equals("RU")) {
      bool1 = bool2;
      if (this.mMufasa.equals("CPH2089")) {
        bool1 = bool2;
        if (uaModelList_Mufasa.contains(paramString)) {
          this.mOppoModel = "OPPO Reno4 Pro";
          bool1 = true;
        } 
      } 
    } 
    return bool1;
  }
  
  public void changeToSpecialModel() {
    try {
      Field field = Build.class.getField("MODEL");
      field.setAccessible(true);
      field.set(Build.class, this.mOppoModel);
    } catch (NoSuchFieldException|IllegalAccessException noSuchFieldException) {
      noSuchFieldException.printStackTrace();
    } 
  }
}
