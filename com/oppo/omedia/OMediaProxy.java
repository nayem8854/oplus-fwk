package com.oppo.omedia;

import android.hardware.camera2.utils.SurfaceUtils;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.Size;
import android.util.Slog;
import android.view.Surface;
import java.util.Iterator;
import java.util.List;

public class OMediaProxy {
  private static final String DESCRIPTOR = "com.oppo.omedia.IOMediaService";
  
  private static final int NORMAL_OPERATING_MODE = 0;
  
  private static final int OMEDIA_OFF = 0;
  
  private static final String TAG = "OMediaProxy";
  
  private static final int TRANSACTION_CLOSE_SESSION = 6;
  
  private static final int TRANSACTION_GET_OPERATING_MODE = 5;
  
  private static OMediaProxy sMediaProxyService;
  
  private static int sOmediaSysEnabledProperty = SystemProperties.getInt("persist.sys.omedia.enable", 0);
  
  private IBinder.DeathRecipient mDeathRecipient;
  
  private IBinder mRemote;
  
  static {
    sMediaProxyService = null;
  }
  
  private OMediaProxy() {
    this.mDeathRecipient = (IBinder.DeathRecipient)new Object(this);
    connectService();
  }
  
  private boolean connectService() {
    boolean bool = false;
    if (sOmediaSysEnabledProperty == 0)
      return false; 
    IBinder iBinder = ServiceManager.checkService("omedia");
    if (iBinder != null)
      try {
        iBinder.linkToDeath(this.mDeathRecipient, 0);
        bool = true;
      } catch (RemoteException remoteException) {
        bool = false;
      }  
    return bool;
  }
  
  public static OMediaProxy getInstance() {
    // Byte code:
    //   0: ldc com/oppo/omedia/OMediaProxy
    //   2: monitorenter
    //   3: getstatic com/oppo/omedia/OMediaProxy.sMediaProxyService : Lcom/oppo/omedia/OMediaProxy;
    //   6: ifnonnull -> 21
    //   9: new com/oppo/omedia/OMediaProxy
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic com/oppo/omedia/OMediaProxy.sMediaProxyService : Lcom/oppo/omedia/OMediaProxy;
    //   21: getstatic com/oppo/omedia/OMediaProxy.sMediaProxyService : Lcom/oppo/omedia/OMediaProxy;
    //   24: astore_0
    //   25: ldc com/oppo/omedia/OMediaProxy
    //   27: monitorexit
    //   28: aload_0
    //   29: areturn
    //   30: astore_0
    //   31: ldc com/oppo/omedia/OMediaProxy
    //   33: monitorexit
    //   34: aload_0
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #88	-> 3
    //   #89	-> 9
    //   #91	-> 21
    //   #87	-> 30
    // Exception table:
    //   from	to	target	type
    //   3	9	30	finally
    //   9	21	30	finally
    //   21	25	30	finally
  }
  
  public int getOperatingMode(List<Surface> paramList, String paramString) {
    if (sOmediaSysEnabledProperty == 0 || (this.mRemote == null && !connectService()))
      return 0; 
    int i = 0;
    try {
      String str = getStreamInfoFromSurface(paramList, paramString);
      int j = getOperatingModeRemote(str);
      if (j > 0) {
        i = j;
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("omedia mode is ");
        stringBuilder.append(i);
        Slog.d("OMediaProxy", stringBuilder.toString());
      } 
    } catch (Exception exception) {
      i = 0;
      Slog.e("OMediaProxy", "catch a omedia 'get operating mode' Exception");
    } 
    return i;
  }
  
  private int getOperatingModeRemote(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("com.oppo.omedia.IOMediaService");
      parcel1.writeString(paramString);
      this.mRemote.transact(5, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readInt();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  private static String getStreamInfoFromSurface(List<Surface> paramList, String paramString) {
    String str;
    if (paramList.size() == 0)
      return "{}"; 
    try {
      String str1 = Integer.toString(paramList.size());
      String str2 = "";
      Iterator<Surface> iterator = paramList.iterator();
      while (true) {
        boolean bool = iterator.hasNext();
        if (bool) {
          Surface surface = iterator.next();
          Size size = SurfaceUtils.getSurfaceSize(surface);
          String str3 = str2;
          if (str2 != null) {
            str3 = str2;
            if (!str2.isEmpty()) {
              StringBuilder stringBuilder1 = new StringBuilder();
              this();
              stringBuilder1.append(str2);
              stringBuilder1.append(",");
              str3 = stringBuilder1.toString();
            } 
          } 
          int i = SurfaceUtils.getSurfaceFormat(surface);
          if (i != 256) {
            StringBuilder stringBuilder2;
            String str4;
            switch (i) {
              default:
                stringBuilder2 = new StringBuilder();
                this();
                stringBuilder2.append("UnKown");
                stringBuilder2.append(i);
                str4 = stringBuilder2.toString();
                break;
              case 35:
                str4 = "PreviewYuv";
                break;
              case 34:
                str4 = "PreviewSurface";
                break;
              case 33:
                str4 = "Jpeg";
                break;
            } 
            String str5 = str4;
            if (str3 != null) {
              str5 = str4;
              if (!str3.isEmpty()) {
                str5 = str4;
                if (str3.contains(str4)) {
                  StringBuilder stringBuilder3 = new StringBuilder();
                  this();
                  stringBuilder3.append(str4);
                  stringBuilder3.append(paramList.indexOf(surface));
                  str5 = stringBuilder3.toString();
                } 
              } 
            } 
            StringBuilder stringBuilder1 = new StringBuilder();
            this();
            stringBuilder1.append(str3);
            stringBuilder1.append("\"");
            stringBuilder1.append(str5);
            stringBuilder1.append("\":\"");
            stringBuilder1.append(size.getWidth());
            stringBuilder1.append("x");
            stringBuilder1.append(size.getHeight());
            stringBuilder1.append("\"");
            str2 = stringBuilder1.toString();
            continue;
          } 
        } 
        break;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("{\"CamId\":");
      stringBuilder.append(paramString);
      stringBuilder.append(",\"StreamCount\":");
      stringBuilder.append(str1);
      stringBuilder.append(",");
      stringBuilder.append(str2);
      stringBuilder.append("}");
      str = stringBuilder.toString();
    } catch (Exception exception) {
      exception.printStackTrace();
      str = "{}";
    } 
    return str;
  }
  
  public boolean sendCameraDeviceClose(String paramString) {
    int i = sOmediaSysEnabledProperty;
    boolean bool1 = false;
    if (i == 0 || (this.mRemote == null && !connectService()))
      return false; 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    boolean bool2 = false;
    try {
      parcel1.writeInterfaceToken("com.oppo.omedia.IOMediaService");
      parcel1.writeString(paramString);
      this.mRemote.transact(6, parcel1, parcel2, 0);
      parcel2.readException();
      i = parcel2.readInt();
      bool2 = bool1;
      if (i > 0)
        bool2 = true; 
    } catch (Exception exception) {
      Slog.e("OMediaProxy", "catch a omedia 'send close time' Exception");
    } finally {}
    parcel1.recycle();
    parcel2.recycle();
    return bool2;
  }
}
