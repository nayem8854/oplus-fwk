package com.oppo.benchmark;

import android.content.Context;
import android.os.Process;
import android.os.SystemProperties;
import android.util.Log;
import com.oplus.app.OplusAppSwitchManager;
import java.lang.reflect.Method;

public final class OppoBenchAppSwitchManager {
  private static final boolean DEBUG = SystemProperties.getBoolean("ro.build.release_type", false) ^ true;
  
  private static final String TAG = "OppoBenchAppSwitchManagerr";
  
  private static final Object mLock = new Object();
  
  private static OppoBenchAppSwitchManager sInstance = null;
  
  private Context mContext;
  
  private final OplusAppSwitchManager.OnAppSwitchObserver mDynamicObserver;
  
  private boolean mIsRegistered = false;
  
  public static OppoBenchAppSwitchManager getInstance(Context paramContext) {
    synchronized (mLock) {
      if (sInstance == null) {
        OppoBenchAppSwitchManager oppoBenchAppSwitchManager = new OppoBenchAppSwitchManager();
        this(paramContext);
        sInstance = oppoBenchAppSwitchManager;
        if (DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("new OppoBenchAppSwitchManager in pid: ");
          stringBuilder.append(Process.myPid());
          Log.d("OppoBenchAppSwitchManagerr", stringBuilder.toString());
        } 
      } 
      return sInstance;
    } 
  }
  
  private OppoBenchAppSwitchManager(Context paramContext) {
    this.mDynamicObserver = (OplusAppSwitchManager.OnAppSwitchObserver)new Object(this);
    this.mContext = paramContext;
  }
  
  public void registerBenchAppSwitchObserver(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIsRegistered : Z
    //   6: istore_2
    //   7: iload_2
    //   8: ifeq -> 14
    //   11: aload_0
    //   12: monitorexit
    //   13: return
    //   14: new java/util/ArrayList
    //   17: astore_3
    //   18: aload_3
    //   19: invokespecial <init> : ()V
    //   22: new com/oplus/app/OplusAppSwitchConfig
    //   25: astore #4
    //   27: aload #4
    //   29: invokespecial <init> : ()V
    //   32: aload_3
    //   33: aload_1
    //   34: invokeinterface add : (Ljava/lang/Object;)Z
    //   39: pop
    //   40: aload #4
    //   42: iconst_2
    //   43: aload_3
    //   44: invokevirtual addAppConfig : (ILjava/util/List;)V
    //   47: invokestatic getInstance : ()Lcom/oplus/app/OplusAppSwitchManager;
    //   50: aload_0
    //   51: getfield mContext : Landroid/content/Context;
    //   54: aload_0
    //   55: getfield mDynamicObserver : Lcom/oplus/app/OplusAppSwitchManager$OnAppSwitchObserver;
    //   58: aload #4
    //   60: invokevirtual registerAppSwitchObserver : (Landroid/content/Context;Lcom/oplus/app/OplusAppSwitchManager$OnAppSwitchObserver;Lcom/oplus/app/OplusAppSwitchConfig;)Z
    //   63: pop
    //   64: aload_0
    //   65: iconst_1
    //   66: putfield mIsRegistered : Z
    //   69: invokestatic getRuntime : ()Ldalvik/system/VMRuntime;
    //   72: invokevirtual requestConcurrentGC : ()V
    //   75: aload_0
    //   76: iconst_1
    //   77: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   80: invokespecial setIgnoreTargetFootprint : (Ljava/lang/Boolean;)V
    //   83: getstatic com/oppo/benchmark/OppoBenchAppSwitchManager.DEBUG : Z
    //   86: ifeq -> 120
    //   89: new java/lang/StringBuilder
    //   92: astore_1
    //   93: aload_1
    //   94: invokespecial <init> : ()V
    //   97: aload_1
    //   98: ldc 'register app switch observer: '
    //   100: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   103: pop
    //   104: aload_1
    //   105: aload_3
    //   106: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   109: pop
    //   110: ldc 'OppoBenchAppSwitchManagerr'
    //   112: aload_1
    //   113: invokevirtual toString : ()Ljava/lang/String;
    //   116: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   119: pop
    //   120: goto -> 164
    //   123: astore_3
    //   124: getstatic com/oppo/benchmark/OppoBenchAppSwitchManager.DEBUG : Z
    //   127: ifeq -> 164
    //   130: new java/lang/StringBuilder
    //   133: astore_1
    //   134: aload_1
    //   135: invokespecial <init> : ()V
    //   138: aload_1
    //   139: ldc 'Oops! Exception on register: '
    //   141: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   144: pop
    //   145: aload_1
    //   146: aload_3
    //   147: invokevirtual getMessage : ()Ljava/lang/String;
    //   150: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   153: pop
    //   154: ldc 'OppoBenchAppSwitchManagerr'
    //   156: aload_1
    //   157: invokevirtual toString : ()Ljava/lang/String;
    //   160: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   163: pop
    //   164: aload_0
    //   165: monitorexit
    //   166: return
    //   167: astore_1
    //   168: aload_0
    //   169: monitorexit
    //   170: aload_1
    //   171: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #111	-> 2
    //   #112	-> 14
    //   #113	-> 22
    //   #115	-> 32
    //   #117	-> 40
    //   #119	-> 47
    //   #120	-> 64
    //   #121	-> 69
    //   #122	-> 75
    //   #123	-> 83
    //   #126	-> 120
    //   #124	-> 123
    //   #125	-> 124
    //   #127	-> 164
    //   #110	-> 167
    // Exception table:
    //   from	to	target	type
    //   2	7	167	finally
    //   14	22	167	finally
    //   22	32	167	finally
    //   32	40	167	finally
    //   40	47	167	finally
    //   47	64	123	java/lang/Exception
    //   47	64	167	finally
    //   64	69	123	java/lang/Exception
    //   64	69	167	finally
    //   69	75	123	java/lang/Exception
    //   69	75	167	finally
    //   75	83	123	java/lang/Exception
    //   75	83	167	finally
    //   83	120	123	java/lang/Exception
    //   83	120	167	finally
    //   124	164	167	finally
  }
  
  public void unregisterBenchAppSwitchObserver() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIsRegistered : Z
    //   6: istore_1
    //   7: iload_1
    //   8: ifeq -> 89
    //   11: invokestatic getInstance : ()Lcom/oplus/app/OplusAppSwitchManager;
    //   14: aload_0
    //   15: getfield mContext : Landroid/content/Context;
    //   18: aload_0
    //   19: getfield mDynamicObserver : Lcom/oplus/app/OplusAppSwitchManager$OnAppSwitchObserver;
    //   22: invokevirtual unregisterAppSwitchObserver : (Landroid/content/Context;Lcom/oplus/app/OplusAppSwitchManager$OnAppSwitchObserver;)Z
    //   25: pop
    //   26: aload_0
    //   27: iconst_0
    //   28: putfield mIsRegistered : Z
    //   31: getstatic com/oppo/benchmark/OppoBenchAppSwitchManager.DEBUG : Z
    //   34: ifeq -> 45
    //   37: ldc 'OppoBenchAppSwitchManagerr'
    //   39: ldc 'unregister app switch observer: '
    //   41: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   44: pop
    //   45: goto -> 89
    //   48: astore_2
    //   49: getstatic com/oppo/benchmark/OppoBenchAppSwitchManager.DEBUG : Z
    //   52: ifeq -> 89
    //   55: new java/lang/StringBuilder
    //   58: astore_3
    //   59: aload_3
    //   60: invokespecial <init> : ()V
    //   63: aload_3
    //   64: ldc 'Oops! Exception on unregister: '
    //   66: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   69: pop
    //   70: aload_3
    //   71: aload_2
    //   72: invokevirtual getMessage : ()Ljava/lang/String;
    //   75: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   78: pop
    //   79: ldc 'OppoBenchAppSwitchManagerr'
    //   81: aload_3
    //   82: invokevirtual toString : ()Ljava/lang/String;
    //   85: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   88: pop
    //   89: aload_0
    //   90: monitorexit
    //   91: return
    //   92: astore_3
    //   93: aload_0
    //   94: monitorexit
    //   95: aload_3
    //   96: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #129	-> 2
    //   #131	-> 11
    //   #132	-> 26
    //   #133	-> 31
    //   #136	-> 45
    //   #134	-> 48
    //   #135	-> 49
    //   #138	-> 89
    //   #128	-> 92
    // Exception table:
    //   from	to	target	type
    //   2	7	92	finally
    //   11	26	48	java/lang/Exception
    //   11	26	92	finally
    //   26	31	48	java/lang/Exception
    //   26	31	92	finally
    //   31	45	48	java/lang/Exception
    //   31	45	92	finally
    //   49	89	92	finally
  }
  
  private void setIgnoreTargetFootprint(Boolean paramBoolean) {
    try {
      Class<?> clazz1 = Class.forName("dalvik.system.VMRuntime");
      Method method1 = clazz1.getDeclaredMethod("getRuntime", null);
      Object object = method1.invoke(null, null);
      Class<?> clazz2 = object.getClass();
      Method method2 = clazz2.getMethod("setIgnoreTargetFootprint", new Class[] { boolean.class });
      method2.invoke(object, new Object[] { paramBoolean });
      if (DEBUG) {
        Log.d("OppoBenchAppSwitchManagerr", "do setIgnoreTargetFootprint right");
        return;
      } 
    } catch (Exception exception) {
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("setIgnoreTargetFootprint exception : ");
        stringBuilder.append(exception.getMessage());
        Log.i("OppoBenchAppSwitchManagerr", stringBuilder.toString());
      } 
      if (DEBUG) {
        Log.d("OppoBenchAppSwitchManagerr", "do setIgnoreTargetFootprint right");
        return;
      } 
    } finally {}
  }
}
