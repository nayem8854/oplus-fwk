package com.oppo.atlas;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOppoAtlasAudioCallback extends IInterface {
  void onErrorVoiceChanger(int paramInt) throws RemoteException;
  
  void onPlaybackStateChanged(int paramInt1, int paramInt2) throws RemoteException;
  
  void onRecordingStateChanged(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements IOppoAtlasAudioCallback {
    public void onPlaybackStateChanged(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onRecordingStateChanged(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onErrorVoiceChanger(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOppoAtlasAudioCallback {
    private static final String DESCRIPTOR = "com.oppo.atlas.IOppoAtlasAudioCallback";
    
    static final int TRANSACTION_onErrorVoiceChanger = 3;
    
    static final int TRANSACTION_onPlaybackStateChanged = 1;
    
    static final int TRANSACTION_onRecordingStateChanged = 2;
    
    public Stub() {
      attachInterface(this, "com.oppo.atlas.IOppoAtlasAudioCallback");
    }
    
    public static IOppoAtlasAudioCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oppo.atlas.IOppoAtlasAudioCallback");
      if (iInterface != null && iInterface instanceof IOppoAtlasAudioCallback)
        return (IOppoAtlasAudioCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onErrorVoiceChanger";
        } 
        return "onRecordingStateChanged";
      } 
      return "onPlaybackStateChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.oppo.atlas.IOppoAtlasAudioCallback");
            return true;
          } 
          param1Parcel1.enforceInterface("com.oppo.atlas.IOppoAtlasAudioCallback");
          param1Int1 = param1Parcel1.readInt();
          onErrorVoiceChanger(param1Int1);
          param1Parcel2.writeNoException();
          return true;
        } 
        param1Parcel1.enforceInterface("com.oppo.atlas.IOppoAtlasAudioCallback");
        param1Int2 = param1Parcel1.readInt();
        param1Int1 = param1Parcel1.readInt();
        onRecordingStateChanged(param1Int2, param1Int1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel1.enforceInterface("com.oppo.atlas.IOppoAtlasAudioCallback");
      param1Int1 = param1Parcel1.readInt();
      param1Int2 = param1Parcel1.readInt();
      onPlaybackStateChanged(param1Int1, param1Int2);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IOppoAtlasAudioCallback {
      public static IOppoAtlasAudioCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oppo.atlas.IOppoAtlasAudioCallback";
      }
      
      public void onPlaybackStateChanged(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.atlas.IOppoAtlasAudioCallback");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOppoAtlasAudioCallback.Stub.getDefaultImpl() != null) {
            IOppoAtlasAudioCallback.Stub.getDefaultImpl().onPlaybackStateChanged(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onRecordingStateChanged(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.atlas.IOppoAtlasAudioCallback");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOppoAtlasAudioCallback.Stub.getDefaultImpl() != null) {
            IOppoAtlasAudioCallback.Stub.getDefaultImpl().onRecordingStateChanged(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onErrorVoiceChanger(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.atlas.IOppoAtlasAudioCallback");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOppoAtlasAudioCallback.Stub.getDefaultImpl() != null) {
            IOppoAtlasAudioCallback.Stub.getDefaultImpl().onErrorVoiceChanger(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOppoAtlasAudioCallback param1IOppoAtlasAudioCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOppoAtlasAudioCallback != null) {
          Proxy.sDefaultImpl = param1IOppoAtlasAudioCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOppoAtlasAudioCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
