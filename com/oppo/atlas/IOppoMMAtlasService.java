package com.oppo.atlas;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOppoMMAtlasService extends IInterface {
  String getParameters(String paramString) throws RemoteException;
  
  void registerAudioCallback(IOppoAtlasAudioCallback paramIOppoAtlasAudioCallback) throws RemoteException;
  
  void registerCallback(IOppoAtlasServiceCallback paramIOppoAtlasServiceCallback) throws RemoteException;
  
  void setEvent(int paramInt, String paramString) throws RemoteException;
  
  void setParameters(String paramString) throws RemoteException;
  
  void unRegisterAudioCallback(IOppoAtlasAudioCallback paramIOppoAtlasAudioCallback) throws RemoteException;
  
  void unRegisterCallback(IOppoAtlasServiceCallback paramIOppoAtlasServiceCallback) throws RemoteException;
  
  class Default implements IOppoMMAtlasService {
    public void setEvent(int param1Int, String param1String) throws RemoteException {}
    
    public void setParameters(String param1String) throws RemoteException {}
    
    public String getParameters(String param1String) throws RemoteException {
      return null;
    }
    
    public void registerCallback(IOppoAtlasServiceCallback param1IOppoAtlasServiceCallback) throws RemoteException {}
    
    public void unRegisterCallback(IOppoAtlasServiceCallback param1IOppoAtlasServiceCallback) throws RemoteException {}
    
    public void registerAudioCallback(IOppoAtlasAudioCallback param1IOppoAtlasAudioCallback) throws RemoteException {}
    
    public void unRegisterAudioCallback(IOppoAtlasAudioCallback param1IOppoAtlasAudioCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOppoMMAtlasService {
    private static final String DESCRIPTOR = "com.oppo.atlas.IOppoMMAtlasService";
    
    static final int TRANSACTION_getParameters = 3;
    
    static final int TRANSACTION_registerAudioCallback = 6;
    
    static final int TRANSACTION_registerCallback = 4;
    
    static final int TRANSACTION_setEvent = 1;
    
    static final int TRANSACTION_setParameters = 2;
    
    static final int TRANSACTION_unRegisterAudioCallback = 7;
    
    static final int TRANSACTION_unRegisterCallback = 5;
    
    public Stub() {
      attachInterface(this, "com.oppo.atlas.IOppoMMAtlasService");
    }
    
    public static IOppoMMAtlasService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oppo.atlas.IOppoMMAtlasService");
      if (iInterface != null && iInterface instanceof IOppoMMAtlasService)
        return (IOppoMMAtlasService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "unRegisterAudioCallback";
        case 6:
          return "registerAudioCallback";
        case 5:
          return "unRegisterCallback";
        case 4:
          return "registerCallback";
        case 3:
          return "getParameters";
        case 2:
          return "setParameters";
        case 1:
          break;
      } 
      return "setEvent";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        IOppoAtlasAudioCallback iOppoAtlasAudioCallback;
        IOppoAtlasServiceCallback iOppoAtlasServiceCallback;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("com.oppo.atlas.IOppoMMAtlasService");
            iOppoAtlasAudioCallback = IOppoAtlasAudioCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            unRegisterAudioCallback(iOppoAtlasAudioCallback);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            iOppoAtlasAudioCallback.enforceInterface("com.oppo.atlas.IOppoMMAtlasService");
            iOppoAtlasAudioCallback = IOppoAtlasAudioCallback.Stub.asInterface(iOppoAtlasAudioCallback.readStrongBinder());
            registerAudioCallback(iOppoAtlasAudioCallback);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            iOppoAtlasAudioCallback.enforceInterface("com.oppo.atlas.IOppoMMAtlasService");
            iOppoAtlasServiceCallback = IOppoAtlasServiceCallback.Stub.asInterface(iOppoAtlasAudioCallback.readStrongBinder());
            unRegisterCallback(iOppoAtlasServiceCallback);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            iOppoAtlasServiceCallback.enforceInterface("com.oppo.atlas.IOppoMMAtlasService");
            iOppoAtlasServiceCallback = IOppoAtlasServiceCallback.Stub.asInterface(iOppoAtlasServiceCallback.readStrongBinder());
            registerCallback(iOppoAtlasServiceCallback);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            iOppoAtlasServiceCallback.enforceInterface("com.oppo.atlas.IOppoMMAtlasService");
            str = iOppoAtlasServiceCallback.readString();
            str = getParameters(str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str);
            return true;
          case 2:
            str.enforceInterface("com.oppo.atlas.IOppoMMAtlasService");
            str = str.readString();
            setParameters(str);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        str.enforceInterface("com.oppo.atlas.IOppoMMAtlasService");
        param1Int1 = str.readInt();
        String str = str.readString();
        setEvent(param1Int1, str);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("com.oppo.atlas.IOppoMMAtlasService");
      return true;
    }
    
    private static class Proxy implements IOppoMMAtlasService {
      public static IOppoMMAtlasService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oppo.atlas.IOppoMMAtlasService";
      }
      
      public void setEvent(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.atlas.IOppoMMAtlasService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOppoMMAtlasService.Stub.getDefaultImpl() != null) {
            IOppoMMAtlasService.Stub.getDefaultImpl().setEvent(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setParameters(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.atlas.IOppoMMAtlasService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOppoMMAtlasService.Stub.getDefaultImpl() != null) {
            IOppoMMAtlasService.Stub.getDefaultImpl().setParameters(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getParameters(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.atlas.IOppoMMAtlasService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOppoMMAtlasService.Stub.getDefaultImpl() != null) {
            param2String = IOppoMMAtlasService.Stub.getDefaultImpl().getParameters(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerCallback(IOppoAtlasServiceCallback param2IOppoAtlasServiceCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oppo.atlas.IOppoMMAtlasService");
          if (param2IOppoAtlasServiceCallback != null) {
            iBinder = param2IOppoAtlasServiceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOppoMMAtlasService.Stub.getDefaultImpl() != null) {
            IOppoMMAtlasService.Stub.getDefaultImpl().registerCallback(param2IOppoAtlasServiceCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unRegisterCallback(IOppoAtlasServiceCallback param2IOppoAtlasServiceCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oppo.atlas.IOppoMMAtlasService");
          if (param2IOppoAtlasServiceCallback != null) {
            iBinder = param2IOppoAtlasServiceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOppoMMAtlasService.Stub.getDefaultImpl() != null) {
            IOppoMMAtlasService.Stub.getDefaultImpl().unRegisterCallback(param2IOppoAtlasServiceCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerAudioCallback(IOppoAtlasAudioCallback param2IOppoAtlasAudioCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oppo.atlas.IOppoMMAtlasService");
          if (param2IOppoAtlasAudioCallback != null) {
            iBinder = param2IOppoAtlasAudioCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IOppoMMAtlasService.Stub.getDefaultImpl() != null) {
            IOppoMMAtlasService.Stub.getDefaultImpl().registerAudioCallback(param2IOppoAtlasAudioCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unRegisterAudioCallback(IOppoAtlasAudioCallback param2IOppoAtlasAudioCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oppo.atlas.IOppoMMAtlasService");
          if (param2IOppoAtlasAudioCallback != null) {
            iBinder = param2IOppoAtlasAudioCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IOppoMMAtlasService.Stub.getDefaultImpl() != null) {
            IOppoMMAtlasService.Stub.getDefaultImpl().unRegisterAudioCallback(param2IOppoAtlasAudioCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOppoMMAtlasService param1IOppoMMAtlasService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOppoMMAtlasService != null) {
          Proxy.sDefaultImpl = param1IOppoMMAtlasService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOppoMMAtlasService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
