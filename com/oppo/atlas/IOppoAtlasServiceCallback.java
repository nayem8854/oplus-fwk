package com.oppo.atlas;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOppoAtlasServiceCallback extends IInterface {
  void notifyEvent(int paramInt, String paramString) throws RemoteException;
  
  class Default implements IOppoAtlasServiceCallback {
    public void notifyEvent(int param1Int, String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOppoAtlasServiceCallback {
    private static final String DESCRIPTOR = "com.oppo.atlas.IOppoAtlasServiceCallback";
    
    static final int TRANSACTION_notifyEvent = 1;
    
    public Stub() {
      attachInterface(this, "com.oppo.atlas.IOppoAtlasServiceCallback");
    }
    
    public static IOppoAtlasServiceCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oppo.atlas.IOppoAtlasServiceCallback");
      if (iInterface != null && iInterface instanceof IOppoAtlasServiceCallback)
        return (IOppoAtlasServiceCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "notifyEvent";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oppo.atlas.IOppoAtlasServiceCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oppo.atlas.IOppoAtlasServiceCallback");
      param1Int1 = param1Parcel1.readInt();
      String str = param1Parcel1.readString();
      notifyEvent(param1Int1, str);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IOppoAtlasServiceCallback {
      public static IOppoAtlasServiceCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oppo.atlas.IOppoAtlasServiceCallback";
      }
      
      public void notifyEvent(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.atlas.IOppoAtlasServiceCallback");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOppoAtlasServiceCallback.Stub.getDefaultImpl() != null) {
            IOppoAtlasServiceCallback.Stub.getDefaultImpl().notifyEvent(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOppoAtlasServiceCallback param1IOppoAtlasServiceCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOppoAtlasServiceCallback != null) {
          Proxy.sDefaultImpl = param1IOppoAtlasServiceCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOppoAtlasServiceCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
