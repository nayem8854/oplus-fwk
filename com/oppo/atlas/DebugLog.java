package com.oppo.atlas;

import android.os.SystemProperties;
import android.util.Log;

public final class DebugLog {
  private static final boolean DEBUG_MODE = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static final boolean LOGD = true;
  
  private static final boolean LOGE = true;
  
  private static final boolean LOGI = true;
  
  private static final boolean LOGV = true;
  
  private static final boolean LOGW = true;
  
  private static final boolean SHOWDETAILINFO = false;
  
  public static int v(String paramString1, String paramString2) {
    if (DEBUG_MODE)
      return Log.v(paramString1, paramString2); 
    return -1;
  }
  
  public static int v(String paramString1, String paramString2, Throwable paramThrowable) {
    if (DEBUG_MODE)
      return Log.v(paramString1, paramString2, paramThrowable); 
    return -1;
  }
  
  public static int v(String paramString1, boolean paramBoolean, String paramString2) {
    if (paramBoolean && DEBUG_MODE)
      return v(paramString1, paramString2); 
    return -1;
  }
  
  public static int d(String paramString1, String paramString2) {
    if (DEBUG_MODE)
      return Log.d(paramString1, paramString2); 
    return -1;
  }
  
  public static int d(String paramString1, boolean paramBoolean, String paramString2) {
    if (paramBoolean && DEBUG_MODE)
      return d(paramString1, paramString2); 
    return -1;
  }
  
  public static int d(String paramString1, String paramString2, Throwable paramThrowable) {
    if (DEBUG_MODE)
      return Log.d(paramString1, paramString2, paramThrowable); 
    return -1;
  }
  
  public static int i(String paramString1, String paramString2) {
    if (DEBUG_MODE)
      return Log.i(paramString1, paramString2); 
    return -1;
  }
  
  public static int i(String paramString1, String paramString2, Throwable paramThrowable) {
    if (DEBUG_MODE)
      return Log.i(paramString1, paramString2, paramThrowable); 
    return -1;
  }
  
  public static int w(String paramString1, String paramString2) {
    if (DEBUG_MODE)
      return Log.w(paramString1, paramString2); 
    return -1;
  }
  
  public static int w(String paramString1, String paramString2, Throwable paramThrowable) {
    if (DEBUG_MODE)
      return Log.w(paramString1, paramString2, paramThrowable); 
    return -1;
  }
  
  public static int e(String paramString1, String paramString2) {
    if (DEBUG_MODE)
      return Log.e(paramString1, paramString2); 
    return -1;
  }
  
  public static int e(String paramString1, String paramString2, Throwable paramThrowable) {
    if (DEBUG_MODE)
      return Log.e(paramString1, paramString2, paramThrowable); 
    return -1;
  }
  
  private static String getFunctionName() {
    StackTraceElement[] arrayOfStackTraceElement = Thread.currentThread().getStackTrace();
    if (arrayOfStackTraceElement == null)
      return null; 
    int i;
    byte b;
    for (i = arrayOfStackTraceElement.length, b = 0; b < i; ) {
      StackTraceElement stackTraceElement = arrayOfStackTraceElement[b];
      if (!stackTraceElement.isNativeMethod())
        if (!stackTraceElement.getClassName().equals(Thread.class.getName()))
          if (!stackTraceElement.getClassName().equals(DebugLog.class.getName())) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("[ ");
            stringBuilder.append(Thread.currentThread().getName());
            stringBuilder.append(": ");
            stringBuilder.append(stackTraceElement.getLineNumber());
            stringBuilder.append(" ");
            stringBuilder.append(stackTraceElement.getMethodName());
            stringBuilder.append(" ]");
            return stringBuilder.toString();
          }   
      b++;
    } 
    return null;
  }
}
