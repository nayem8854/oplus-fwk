package com.oppo.atlas;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;

public class OppoAtlasManager {
  public static final String SERVICE_NAME = "multimediaDaemon";
  
  private static final String TAG = "OppoAtlasManager";
  
  private static IOppoMMAtlasService mOppoAtlasService;
  
  private static volatile OppoAtlasManager sInstance = null;
  
  private static boolean sServiceEnable = true;
  
  private boolean mBindServiceFlag = false;
  
  private final Context mContext;
  
  private OppoAtlasManager(Context paramContext) {
    this.mContext = paramContext;
  }
  
  public static OppoAtlasManager getInstance(Context paramContext) {
    // Byte code:
    //   0: getstatic com/oppo/atlas/OppoAtlasManager.sInstance : Lcom/oppo/atlas/OppoAtlasManager;
    //   3: ifnonnull -> 40
    //   6: ldc com/oppo/atlas/OppoAtlasManager
    //   8: monitorenter
    //   9: getstatic com/oppo/atlas/OppoAtlasManager.sInstance : Lcom/oppo/atlas/OppoAtlasManager;
    //   12: ifnonnull -> 28
    //   15: new com/oppo/atlas/OppoAtlasManager
    //   18: astore_1
    //   19: aload_1
    //   20: aload_0
    //   21: invokespecial <init> : (Landroid/content/Context;)V
    //   24: aload_1
    //   25: putstatic com/oppo/atlas/OppoAtlasManager.sInstance : Lcom/oppo/atlas/OppoAtlasManager;
    //   28: ldc com/oppo/atlas/OppoAtlasManager
    //   30: monitorexit
    //   31: goto -> 40
    //   34: astore_0
    //   35: ldc com/oppo/atlas/OppoAtlasManager
    //   37: monitorexit
    //   38: aload_0
    //   39: athrow
    //   40: ldc 'persist.sys.multimedia.atlas.service'
    //   42: iconst_0
    //   43: invokestatic getBoolean : (Ljava/lang/String;Z)Z
    //   46: ifeq -> 56
    //   49: iconst_0
    //   50: putstatic com/oppo/atlas/OppoAtlasManager.sServiceEnable : Z
    //   53: goto -> 60
    //   56: iconst_1
    //   57: putstatic com/oppo/atlas/OppoAtlasManager.sServiceEnable : Z
    //   60: getstatic com/oppo/atlas/OppoAtlasManager.sInstance : Lcom/oppo/atlas/OppoAtlasManager;
    //   63: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #41	-> 0
    //   #42	-> 6
    //   #43	-> 9
    //   #44	-> 15
    //   #46	-> 28
    //   #49	-> 40
    //   #50	-> 49
    //   #52	-> 56
    //   #54	-> 60
    // Exception table:
    //   from	to	target	type
    //   9	15	34	finally
    //   15	28	34	finally
    //   28	31	34	finally
    //   35	38	34	finally
  }
  
  private static IOppoMMAtlasService getService() {
    if (!sServiceEnable) {
      DebugLog.d("OppoAtlasManager", "OppoAtlasService disabled");
      return null;
    } 
    IOppoMMAtlasService iOppoMMAtlasService2 = mOppoAtlasService;
    if (iOppoMMAtlasService2 != null)
      return iOppoMMAtlasService2; 
    IBinder iBinder = ServiceManager.getService("multimediaDaemon");
    IOppoMMAtlasService iOppoMMAtlasService1 = IOppoMMAtlasService.Stub.asInterface(iBinder);
    return iOppoMMAtlasService1;
  }
  
  public void sendMessage(String paramString) {
    StringBuilder stringBuilder;
    if (paramString == null)
      return; 
    String[] arrayOfString = paramString.split(":");
    if (arrayOfString.length != 2) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("info = ");
      stringBuilder.append(paramString);
      stringBuilder.append(" format is error,check it");
      DebugLog.d("OppoAtlasManager", stringBuilder.toString());
      return;
    } 
    try {
      int i = Integer.parseInt((String)stringBuilder[0]);
      setEvent(i, (String)stringBuilder[1]);
    } catch (NumberFormatException numberFormatException) {}
  }
  
  public void setEvent(int paramInt, String paramString) {
    IOppoMMAtlasService iOppoMMAtlasService = getService();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("setEventInfo info = ");
    stringBuilder.append(paramString);
    stringBuilder.append(" service = ");
    stringBuilder.append(iOppoMMAtlasService);
    DebugLog.d("OppoAtlasManager", stringBuilder.toString());
    if (iOppoMMAtlasService == null)
      return; 
    try {
      iOppoMMAtlasService.setEvent(paramInt, paramString);
    } catch (RemoteException remoteException) {
      DebugLog.e("OppoAtlasManager", "Dead object in info", (Throwable)remoteException);
    } 
  }
  
  public void setParameters(String paramString) {
    IOppoMMAtlasService iOppoMMAtlasService = getService();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("setParameters keyValuePairs = ");
    stringBuilder.append(paramString);
    stringBuilder.append(" service = ");
    stringBuilder.append(iOppoMMAtlasService);
    DebugLog.d("OppoAtlasManager", stringBuilder.toString());
    if (iOppoMMAtlasService == null)
      return; 
    try {
      iOppoMMAtlasService.setParameters(paramString);
    } catch (RemoteException remoteException) {
      DebugLog.e("OppoAtlasManager", "Dead object in info", (Throwable)remoteException);
    } 
  }
  
  public String getParameters(String paramString) {
    IOppoMMAtlasService iOppoMMAtlasService = getService();
    if (iOppoMMAtlasService == null)
      return null; 
    try {
      return iOppoMMAtlasService.getParameters(paramString);
    } catch (RemoteException remoteException) {
      DebugLog.e("OppoAtlasManager", "Dead object in info", (Throwable)remoteException);
      return null;
    } 
  }
  
  public void registerServiceCallback(IOppoAtlasServiceCallback paramIOppoAtlasServiceCallback) {
    IOppoMMAtlasService iOppoMMAtlasService = getService();
    if (iOppoMMAtlasService == null)
      return; 
    try {
      iOppoMMAtlasService.registerCallback(paramIOppoAtlasServiceCallback);
    } catch (RemoteException remoteException) {
      DebugLog.e("OppoAtlasManager", "Dead object in info", (Throwable)remoteException);
    } 
  }
  
  public void unRegisterServiceCallback(IOppoAtlasServiceCallback paramIOppoAtlasServiceCallback) {
    IOppoMMAtlasService iOppoMMAtlasService = getService();
    if (iOppoMMAtlasService == null)
      return; 
    try {
      iOppoMMAtlasService.unRegisterCallback(paramIOppoAtlasServiceCallback);
    } catch (RemoteException remoteException) {
      DebugLog.e("OppoAtlasManager", "Dead object in info", (Throwable)remoteException);
    } 
  }
  
  public void registerAudioCallback(IOppoAtlasAudioCallback paramIOppoAtlasAudioCallback) {
    IOppoMMAtlasService iOppoMMAtlasService = getService();
    if (iOppoMMAtlasService == null)
      return; 
    try {
      iOppoMMAtlasService.registerAudioCallback(paramIOppoAtlasAudioCallback);
    } catch (RemoteException remoteException) {
      DebugLog.e("OppoAtlasManager", "Dead object in info", (Throwable)remoteException);
    } 
  }
  
  public void unRegisterAudioCallback(IOppoAtlasAudioCallback paramIOppoAtlasAudioCallback) {
    IOppoMMAtlasService iOppoMMAtlasService = getService();
    if (iOppoMMAtlasService == null)
      return; 
    try {
      iOppoMMAtlasService.unRegisterAudioCallback(paramIOppoAtlasAudioCallback);
    } catch (RemoteException remoteException) {
      DebugLog.e("OppoAtlasManager", "Dead object in info", (Throwable)remoteException);
    } 
  }
}
