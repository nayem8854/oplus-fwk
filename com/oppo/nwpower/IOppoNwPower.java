package com.oppo.nwpower;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOppoNwPower extends IInterface {
  void oppoNwPowerLegacySocketDestroy() throws RemoteException;
  
  void oppoNwPowerSetDeviceIdleStatus(boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void oppoNwPowerSetFirewall(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void oppoNwPowerSetLightDeviceIdleStatus(boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void oppoNwPowerSetStaticWhiteList(int[] paramArrayOfint) throws RemoteException;
  
  void oppoNwPowerSocketDestroy(int paramInt) throws RemoteException;
  
  class Default implements IOppoNwPower {
    public void oppoNwPowerSetFirewall(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void oppoNwPowerSetStaticWhiteList(int[] param1ArrayOfint) throws RemoteException {}
    
    public void oppoNwPowerSocketDestroy(int param1Int) throws RemoteException {}
    
    public void oppoNwPowerLegacySocketDestroy() throws RemoteException {}
    
    public void oppoNwPowerSetDeviceIdleStatus(boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public void oppoNwPowerSetLightDeviceIdleStatus(boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOppoNwPower {
    private static final String DESCRIPTOR = "com.oppo.nwpower.IOppoNwPower";
    
    static final int TRANSACTION_oppoNwPowerLegacySocketDestroy = 4;
    
    static final int TRANSACTION_oppoNwPowerSetDeviceIdleStatus = 5;
    
    static final int TRANSACTION_oppoNwPowerSetFirewall = 1;
    
    static final int TRANSACTION_oppoNwPowerSetLightDeviceIdleStatus = 6;
    
    static final int TRANSACTION_oppoNwPowerSetStaticWhiteList = 2;
    
    static final int TRANSACTION_oppoNwPowerSocketDestroy = 3;
    
    public Stub() {
      attachInterface(this, "com.oppo.nwpower.IOppoNwPower");
    }
    
    public static IOppoNwPower asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oppo.nwpower.IOppoNwPower");
      if (iInterface != null && iInterface instanceof IOppoNwPower)
        return (IOppoNwPower)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "oppoNwPowerSetLightDeviceIdleStatus";
        case 5:
          return "oppoNwPowerSetDeviceIdleStatus";
        case 4:
          return "oppoNwPowerLegacySocketDestroy";
        case 3:
          return "oppoNwPowerSocketDestroy";
        case 2:
          return "oppoNwPowerSetStaticWhiteList";
        case 1:
          break;
      } 
      return "oppoNwPowerSetFirewall";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        int[] arrayOfInt;
        boolean bool1 = false, bool2 = false, bool3 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("com.oppo.nwpower.IOppoNwPower");
            if (param1Parcel1.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            bool1 = bool3;
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            oppoNwPowerSetLightDeviceIdleStatus(bool2, bool1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            param1Parcel1.enforceInterface("com.oppo.nwpower.IOppoNwPower");
            if (param1Parcel1.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            oppoNwPowerSetDeviceIdleStatus(bool2, bool1);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            param1Parcel1.enforceInterface("com.oppo.nwpower.IOppoNwPower");
            oppoNwPowerLegacySocketDestroy();
            param1Parcel2.writeNoException();
            return true;
          case 3:
            param1Parcel1.enforceInterface("com.oppo.nwpower.IOppoNwPower");
            param1Int1 = param1Parcel1.readInt();
            oppoNwPowerSocketDestroy(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            param1Parcel1.enforceInterface("com.oppo.nwpower.IOppoNwPower");
            arrayOfInt = param1Parcel1.createIntArray();
            oppoNwPowerSetStaticWhiteList(arrayOfInt);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        arrayOfInt.enforceInterface("com.oppo.nwpower.IOppoNwPower");
        param1Int1 = arrayOfInt.readInt();
        if (arrayOfInt.readInt() != 0)
          bool2 = true; 
        oppoNwPowerSetFirewall(param1Int1, bool2);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("com.oppo.nwpower.IOppoNwPower");
      return true;
    }
    
    private static class Proxy implements IOppoNwPower {
      public static IOppoNwPower sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oppo.nwpower.IOppoNwPower";
      }
      
      public void oppoNwPowerSetFirewall(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.oppo.nwpower.IOppoNwPower");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool1 && IOppoNwPower.Stub.getDefaultImpl() != null) {
            IOppoNwPower.Stub.getDefaultImpl().oppoNwPowerSetFirewall(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void oppoNwPowerSetStaticWhiteList(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.nwpower.IOppoNwPower");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOppoNwPower.Stub.getDefaultImpl() != null) {
            IOppoNwPower.Stub.getDefaultImpl().oppoNwPowerSetStaticWhiteList(param2ArrayOfint);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void oppoNwPowerSocketDestroy(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.nwpower.IOppoNwPower");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOppoNwPower.Stub.getDefaultImpl() != null) {
            IOppoNwPower.Stub.getDefaultImpl().oppoNwPowerSocketDestroy(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void oppoNwPowerLegacySocketDestroy() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.nwpower.IOppoNwPower");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOppoNwPower.Stub.getDefaultImpl() != null) {
            IOppoNwPower.Stub.getDefaultImpl().oppoNwPowerLegacySocketDestroy();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void oppoNwPowerSetDeviceIdleStatus(boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("com.oppo.nwpower.IOppoNwPower");
          boolean bool1 = true;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOppoNwPower.Stub.getDefaultImpl() != null) {
            IOppoNwPower.Stub.getDefaultImpl().oppoNwPowerSetDeviceIdleStatus(param2Boolean1, param2Boolean2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void oppoNwPowerSetLightDeviceIdleStatus(boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("com.oppo.nwpower.IOppoNwPower");
          boolean bool1 = true;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IOppoNwPower.Stub.getDefaultImpl() != null) {
            IOppoNwPower.Stub.getDefaultImpl().oppoNwPowerSetLightDeviceIdleStatus(param2Boolean1, param2Boolean2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOppoNwPower param1IOppoNwPower) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOppoNwPower != null) {
          Proxy.sDefaultImpl = param1IOppoNwPower;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOppoNwPower getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
