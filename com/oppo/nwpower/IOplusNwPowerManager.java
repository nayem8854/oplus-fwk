package com.oppo.nwpower;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;

public interface IOplusNwPowerManager extends IOplusCommonFeature {
  public static final IOplusNwPowerManager DEFAULT = (IOplusNwPowerManager)new Object();
  
  public static final String NAME = "IOplusNwPowerManager";
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusNwPowerManager;
  }
  
  default IOplusCommonFeature getDefault() {
    return DEFAULT;
  }
  
  default boolean getStaticNetControllerEnable() {
    return false;
  }
  
  default void setFirewall(int paramInt, boolean paramBoolean) {}
  
  default void setStaticWhiteList(int[] paramArrayOfint) {}
  
  default void socketDestroy(int paramInt) {}
  
  default void legacySocketDestroy() {}
  
  default void setDeviceIdleStatus(boolean paramBoolean1, boolean paramBoolean2) {}
  
  default void setLightDeviceIdleStatus(boolean paramBoolean1, boolean paramBoolean2) {}
}
