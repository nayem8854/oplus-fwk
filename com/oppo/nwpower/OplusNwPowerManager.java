package com.oppo.nwpower;

import android.os.RemoteException;
import android.os.ServiceManager;
import com.oplus.content.OplusFeatureConfigManager;

public class OplusNwPowerManager implements IOplusNwPowerManager {
  private static final String NWPOWER_SERVICE = "nwpower";
  
  private static final boolean NWPOWER_STATIC_NET_CONTROLLER_ENABLE = OplusFeatureConfigManager.getInstance().hasFeature("oplus.software.radio.nwpower_static_netcontroller_enabled");
  
  private static OplusNwPowerManager sInstance;
  
  public static OplusNwPowerManager getInstance() {
    // Byte code:
    //   0: ldc com/oppo/nwpower/OplusNwPowerManager
    //   2: monitorenter
    //   3: getstatic com/oppo/nwpower/OplusNwPowerManager.sInstance : Lcom/oppo/nwpower/OplusNwPowerManager;
    //   6: ifnonnull -> 21
    //   9: new com/oppo/nwpower/OplusNwPowerManager
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic com/oppo/nwpower/OplusNwPowerManager.sInstance : Lcom/oppo/nwpower/OplusNwPowerManager;
    //   21: getstatic com/oppo/nwpower/OplusNwPowerManager.sInstance : Lcom/oppo/nwpower/OplusNwPowerManager;
    //   24: astore_0
    //   25: ldc com/oppo/nwpower/OplusNwPowerManager
    //   27: monitorexit
    //   28: aload_0
    //   29: areturn
    //   30: astore_0
    //   31: ldc com/oppo/nwpower/OplusNwPowerManager
    //   33: monitorexit
    //   34: aload_0
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #37	-> 0
    //   #38	-> 3
    //   #39	-> 9
    //   #41	-> 21
    //   #42	-> 30
    // Exception table:
    //   from	to	target	type
    //   3	9	30	finally
    //   9	21	30	finally
    //   21	28	30	finally
    //   31	34	30	finally
  }
  
  private final IOppoNwPower mService = IOppoNwPower.Stub.asInterface(ServiceManager.getService("nwpower"));
  
  public boolean getStaticNetControllerEnable() {
    return NWPOWER_STATIC_NET_CONTROLLER_ENABLE;
  }
  
  public void setFirewall(int paramInt, boolean paramBoolean) {
    try {
      this.mService.oppoNwPowerSetFirewall(paramInt, paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setStaticWhiteList(int[] paramArrayOfint) {
    try {
      this.mService.oppoNwPowerSetStaticWhiteList(paramArrayOfint);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void socketDestroy(int paramInt) {
    try {
      this.mService.oppoNwPowerSocketDestroy(paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void legacySocketDestroy() {
    try {
      this.mService.oppoNwPowerLegacySocketDestroy();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setDeviceIdleStatus(boolean paramBoolean1, boolean paramBoolean2) {
    try {
      this.mService.oppoNwPowerSetDeviceIdleStatus(paramBoolean1, paramBoolean2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setLightDeviceIdleStatus(boolean paramBoolean1, boolean paramBoolean2) {
    try {
      this.mService.oppoNwPowerSetLightDeviceIdleStatus(paramBoolean1, paramBoolean2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
}
