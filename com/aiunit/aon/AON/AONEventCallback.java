package com.aiunit.aon.AON;

public interface AONEventCallback {
  void onAONEvent(int paramInt1, int paramInt2);
}
