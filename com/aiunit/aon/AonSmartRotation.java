package com.aiunit.aon;

import android.app.ActivityManagerNative;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.UserManager;
import android.provider.Settings;
import android.util.Log;
import com.aiunit.aon.utils.IAONEventListener;
import com.aiunit.aon.utils.IAONService;
import java.util.ArrayList;
import java.util.Arrays;

public class AonSmartRotation {
  public static int sTestFaceAngle = -2;
  
  public static float sTestOrientationHardwareValue = -1.0F;
  
  public static float sTestOrientationResult = 0.0F;
  
  static {
    sFaceAngle = -2;
    stuckInPreStatusCount = 0;
    stopCameraErrorCodes = new ArrayList<>(Arrays.asList(new Integer[] { Integer.valueOf(20513), Integer.valueOf(24577), Integer.valueOf(24578), Integer.valueOf(24579), Integer.valueOf(24580), Integer.valueOf(24581), Integer.valueOf(24582), Integer.valueOf(24583), Integer.valueOf(24624), Integer.valueOf(24625) }));
    sIsCameraOn = false;
    sAonSmartRotationConnectionCount = 0;
    sOrientationResult = 0.0F;
    sPreOpenCameraTime = 0L;
    sPreOpenEventFetchTime = 0L;
    sNormalStatusTime = 0L;
    sReceiveEventTime = 0L;
    DEBUG_CONFIGURATION = false;
    sIsWaitingFaceAngle = false;
    sLaunchCameraAhead = false;
    sIsTestMode = false;
    sIsLowLight = false;
    sAONService = null;
    sCanUseSmartRotation = false;
    sFaceEventTimeGap = 400;
    sCurrentUserId = 0;
    sAonlistener = (IAONEventListener)new Object();
    sDeathRecipient = (IBinder.DeathRecipient)new Object();
  }
  
  private int mSwitchValue = 0;
  
  private int mAutoRotationSwitchValue = 0;
  
  private int mSmartRotationSwitchValue = 0;
  
  private static final int ACCEPT_STATUS_NUM = 5;
  
  private static final int AON_FACE_DIRECTION_ENGINE = 393218;
  
  private static final int AON_LOW_LIGHT = 10000;
  
  private static final String AON_PKG_NAME = "com.aiunit.aon";
  
  private static final String AON_SERVICE = "com.aiunit.aon.AONservice";
  
  private static final int CLOSE_FUNCTION_TIME_GAP = 20;
  
  private static final String CLS_NAME = "com.aiunit.aon.AONService";
  
  private static boolean DEBUG_CONFIGURATION = false;
  
  public static final int DEFAULT_FACE_ANGLE = -2;
  
  private static final int DEFAULT_TIME_GAP = 400;
  
  private static final int ERR_LAUNCH_CAMERA_ONE = 20512;
  
  private static final int ERR_REMOTE_EXCEPTION = 4099;
  
  private static final float FACE_ORIENTATION_0 = 0.0F;
  
  private static final float FACE_ORIENTATION_1 = 1.0F;
  
  private static final float FACE_ORIENTATION_2 = 2.0F;
  
  private static final float FACE_ORIENTATION_3 = 3.0F;
  
  private static final int LANDSCAPE_BOUNDARY_LEFT = 120;
  
  private static final int LANDSCAPE_BOUNDARY_RIGHT = 240;
  
  private static final int LAY_DOWN = 4;
  
  private static final int LEGAL_BOUNDARY_LEFT = 0;
  
  private static final int LEGAL_BOUNDARY_RIGHT = 360;
  
  private static final int LOW_LIGHT_TIME_GAP = 100;
  
  private static final int MAX_ORIENTATION_STATUS = 9;
  
  private static final int MAX_TIME_GAP = 1000;
  
  private static final int MIN_TIME_GAP = 0;
  
  private static final int MSG_INIT_SUCCESS = 10001;
  
  private static final int MSG_REGISTER = 10003;
  
  private static final int MSG_START_CAMERA = 10005;
  
  private static final int MSG_STOP_CAMERA = 10006;
  
  private static final int MSG_TESTALL = 10002;
  
  private static final int MSG_UNREGISTER = 10004;
  
  private static final int NORMAL_PORTRAIT = 0;
  
  private static final int NORMAL_PORTRAIT_BOUNDARY_LEFT = 60;
  
  private static final int NORMAL_PORTRAIT_BOUNDARY_RIGHT = 300;
  
  private static final long PRE_OPEN_STATUS_TIME_GAP = 800L;
  
  private static final float PRE_STATUS_ONE = 6.0F;
  
  private static final float PRE_STATUS_THREE = 8.0F;
  
  private static final float PRE_STATUS_TWO = 7.0F;
  
  private static final String SMART_ROTATION_ENABLE_SWITCH = "persist.sys.oplus.smartrotation";
  
  private static final String SMART_ROTATION_TAG = "SmartRotationDebug";
  
  private static final String SMART_ROTATION_TESTEVENT = "com.aon.smartrotation.testevent";
  
  private static final String SMART_ROTATION_TESTMODE_STATUS = "sys.oplus.smartrotation.testmode";
  
  private static final String SMART_ROTATION_TIME_GAP = "persist.sys.oplus.smartrotation.timegap";
  
  private static final String WMS_LISTENER = "com.android.server.policy.WindowOrientationListener$OrientationSensorJudge";
  
  private static IAONService sAONService;
  
  public static int sAonSmartRotationConnectionCount;
  
  private static IAONEventListener sAonlistener;
  
  private static boolean sCanUseSmartRotation;
  
  private static int sCurrentUserId;
  
  private static IBinder.DeathRecipient sDeathRecipient;
  
  public static int sFaceAngle;
  
  private static int sFaceEventTimeGap;
  
  public static boolean sIsCameraOn;
  
  private static boolean sIsLowLight;
  
  private static boolean sIsTestMode;
  
  private static boolean sIsWaitingFaceAngle;
  
  private static boolean sLaunchCameraAhead;
  
  private static long sNormalStatusTime;
  
  private static float sOrientationResult;
  
  private static long sPreOpenCameraTime;
  
  private static long sPreOpenEventFetchTime;
  
  private static long sReceiveEventTime;
  
  private static ServiceConnection sServiceConnection;
  
  private static Handler sSmartRotationHandler;
  
  private static HandlerThread sSmartRotationHandlerThread;
  
  private static Looper sSmartRotationLooper;
  
  public static ArrayList<Integer> stopCameraErrorCodes;
  
  public static int stuckInPreStatusCount;
  
  private Context mContext;
  
  private BroadcastReceiver mSmartRotationEnableReceiver;
  
  private IntentFilter mSmartRotationFilter;
  
  private UserManager mUserManager;
  
  public int getStatus() {
    try {
      if (!this.mUserManager.isUserUnlocked()) {
        Log.d("SmartRotationDebug", "User change did not finish, upload old value.");
        return this.mSwitchValue;
      } 
      this.mAutoRotationSwitchValue = Settings.System.getIntForUser(this.mContext.getContentResolver(), "accelerometer_rotation", 0, -2);
      int i = Settings.System.getIntForUser(this.mContext.getContentResolver(), "device_orientation_intelligent_auto_rotation", 0, -2);
      if (this.mAutoRotationSwitchValue == 1 && i == 1) {
        this.mSwitchValue = 1;
      } else {
        if (this.mSwitchValue == 1) {
          while (sAonSmartRotationConnectionCount > 0)
            destroySmartRotationConnection(); 
          printDetailLog("Ready to shut down AON smart rotation, reset sAonSmartRotationConnectionCount and the connections.");
        } 
        this.mSwitchValue = 0;
      } 
      if (this.mUserManager.isUserUnlocked() && sCurrentUserId != (ActivityManagerNative.getDefault().getCurrentUser()).id) {
        sCurrentUserId = (ActivityManagerNative.getDefault().getCurrentUser()).id;
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("User handle changed to ");
        stringBuilder.append(sCurrentUserId);
        Log.d("SmartRotationDebug", stringBuilder.toString());
        while (sAonSmartRotationConnectionCount > 0)
          destroySmartRotationConnection(); 
        if (sAONService != null) {
          dropAONSubThread();
          try {
            Thread.sleep(20L);
          } catch (InterruptedException interruptedException) {
            Log.e("SmartRotationDebug", "sleep 20ms error.");
          } 
          unbindAONService();
        } 
        if (this.mSwitchValue == 1)
          createSmartRotationConnection(); 
      } 
    } catch (Exception exception) {
      Log.e("SmartRotationDebug", "getStatus failed.");
    } 
    return this.mSwitchValue;
  }
  
  public static void printDetailLog(String paramString) {
    if (paramString == null)
      return; 
    if (DEBUG_CONFIGURATION)
      Log.d("SmartRotationDebug", paramString); 
  }
  
  public AonSmartRotation(Context paramContext) {
    this.mSmartRotationEnableReceiver = (BroadcastReceiver)new Object(this);
    this.mContext = paramContext;
    try {
      int i = Integer.parseInt(SystemProperties.get("persist.sys.oplus.smartrotation.timegap", "400"));
      if (i > 1000) {
        sFaceEventTimeGap = 400;
        Log.w("SmartRotationDebug", "sFaceEventTimeGap can not be too long, reset it as 400ms.");
      } 
      DEBUG_CONFIGURATION = SystemProperties.get("persist.sys.assert.panic", "false").equals("true");
      this.mUserManager = (UserManager)this.mContext.getSystemService("user");
      sCurrentUserId = (ActivityManagerNative.getDefault().getCurrentUser()).id;
    } catch (Exception exception) {
      Log.e("SmartRotationDebug", "Fetch time gap failed, use default value.");
    } 
    Log.d("SmartRotationDebug", "Init AonSmartRotation successed.");
  }
  
  public void registerSmartRotationReceiver() {
    if (this.mSmartRotationFilter == null)
      this.mSmartRotationFilter = new IntentFilter("com.aon.smartrotation.testevent"); 
    this.mContext.registerReceiver(this.mSmartRotationEnableReceiver, this.mSmartRotationFilter);
  }
  
  public void unRegisterSmartRotationReceiver() {
    BroadcastReceiver broadcastReceiver = this.mSmartRotationEnableReceiver;
    if (broadcastReceiver != null)
      try {
        this.mContext.unregisterReceiver(broadcastReceiver);
      } catch (Exception exception) {} 
  }
  
  public void updateTimeGap(int paramInt) {
    if (paramInt >= 0 && paramInt <= 1000) {
      sFaceEventTimeGap = paramInt;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Update Smart Rotation sFaceEventTimeGap as ");
      stringBuilder.append(sFaceEventTimeGap);
      printDetailLog(stringBuilder.toString());
    } 
  }
  
  public void updateDebugConfiguration(boolean paramBoolean) {
    DEBUG_CONFIGURATION = paramBoolean;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Update Smart Rotation DEBUG_CONFIGURATION as ");
    stringBuilder.append(DEBUG_CONFIGURATION);
    printDetailLog(stringBuilder.toString());
  }
  
  public float createSmartRotationConnection() {
    try {
      printDetailLog("Register SmartRotation Connection.");
      int i = Integer.parseInt(SystemProperties.get("persist.sys.oplus.smartrotation.timegap", "400"));
      if (i > 1000) {
        sFaceEventTimeGap = 400;
        Log.w("SmartRotationDebug", "sFaceEventTimeGap can not be too long, reset it as 400ms.");
      } 
      DEBUG_CONFIGURATION = SystemProperties.get("persist.sys.assert.panic", "false").equals("true");
      sAonSmartRotationConnectionCount++;
      if (sSmartRotationHandlerThread == null) {
        initAONSubThread();
        printDetailLog("First AonSmartRotation Connection init.");
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("We have ");
        stringBuilder.append(sAonSmartRotationConnectionCount);
        stringBuilder.append(" AonSmartRotation Connections here.");
        printDetailLog(stringBuilder.toString());
      } 
      if (sAONService == null && sServiceConnection == null) {
        bindAONService();
      } else {
        printDetailLog("Already have AONService here, do not bind it again.");
      } 
      if (!sIsTestMode) {
        boolean bool = SystemProperties.get("sys.oplus.smartrotation.testmode", "false").equals("true");
        if (bool) {
          registerSmartRotationReceiver();
          Log.w("SmartRotationDebug", "TestMode, AON smart rotation testmode on.");
        } 
      } 
      printDetailLog("Finish Register SmartRotation Connection.");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("AON smart rotation error in createSmartRotationConnection(), e = ");
      stringBuilder.append(exception.toString());
      Log.e("SmartRotationDebug", stringBuilder.toString());
    } 
    return 0.0F;
  }
  
  public float destroySmartRotationConnection() {
    try {
      printDetailLog("Unregister SmartRotation Connection.");
      if (sAonSmartRotationConnectionCount == 0) {
        printDetailLog("No thing left while unregister, finish it.");
        return 0.0F;
      } 
      int i = sAonSmartRotationConnectionCount - 1;
      if (i == 0) {
        if (sIsTestMode) {
          sIsTestMode = false;
          unRegisterSmartRotationReceiver();
          Log.w("SmartRotationDebug", "TestMode, AON smart rotation testmode off.");
        } 
        dropAONSubThread();
        try {
          Thread.sleep(20L);
        } catch (InterruptedException interruptedException) {
          Log.e("SmartRotationDebug", "sleep 20ms error.");
        } 
        unbindAONService();
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("We have ");
        stringBuilder.append(sAonSmartRotationConnectionCount);
        stringBuilder.append(" AonSmartRotation Connections left.");
        printDetailLog(stringBuilder.toString());
      } 
      printDetailLog("Finish Unregister SmartRotation Connection.");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("AON smart rotation error in destroySmartRotationConnection(), e = ");
      stringBuilder.append(exception.toString());
      Log.e("SmartRotationDebug", stringBuilder.toString());
    } 
    return 0.0F;
  }
  
  public float makeDecisionBySmartRotation(float[] paramArrayOffloat) {
    try {
      if (sAONService != null) {
        if (sIsTestMode)
          return testModeData(paramArrayOffloat); 
        if ((int)paramArrayOffloat[0] % 5 == 4 || (int)paramArrayOffloat[0] % 5 == 0) {
          printDetailLog("nash_debug, Ignore lay down, 0 and their pre status.");
          return paramArrayOffloat[0];
        } 
        if (paramArrayOffloat[0] == 6.0F || paramArrayOffloat[0] == 7.0F || paramArrayOffloat[0] == 8.0F) {
          if (!sLaunchCameraAhead) {
            printDetailLog("nash_debug, pre status for orientation, begin launch camera.");
            sPreOpenCameraTime = System.currentTimeMillis();
            launchCamera();
            sLaunchCameraAhead = true;
          } 
          return paramArrayOffloat[0];
        } 
        sNormalStatusTime = System.currentTimeMillis();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("nash_debug, sNormalStatusTime: ");
        stringBuilder.append(sNormalStatusTime);
        stringBuilder.append(", orientation: ");
        stringBuilder.append(paramArrayOffloat[0]);
        printDetailLog(stringBuilder.toString());
        if (!sLaunchCameraAhead) {
          printDetailLog("nash_debug, begin launch camera.");
          launchCamera();
          printDetailLog("nash_debug, finish launch camera, begin wait face event.");
        } 
        sIsWaitingFaceAngle = true;
        try {
          Thread.sleep(sFaceEventTimeGap);
        } catch (InterruptedException interruptedException) {
          Log.e("SmartRotationDebug", "sleep sFaceEventTimeGap error.");
        } 
        if (sIsLowLight) {
          printDetailLog("AON_LOW_LIGHT, wait another 100ms for low light situation.");
          try {
            Thread.sleep(100L);
          } catch (InterruptedException interruptedException) {
            Log.e("SmartRotationDebug", "sleep for AON_LOW_LIGHT error.");
          } 
          sIsLowLight = false;
        } 
        sIsWaitingFaceAngle = false;
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("nash_debug, time is up, sFaceAngle is ");
        stringBuilder.append(sFaceAngle);
        Log.d("SmartRotationDebug", stringBuilder.toString());
        if (sIsCameraOn) {
          releaseCamera();
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Normal Orientation: ");
          stringBuilder.append(paramArrayOffloat[0]);
          stringBuilder.append(" here, and we can not receive a face angle, stop camera for next data.");
          Log.w("SmartRotationDebug", stringBuilder.toString());
        } 
        if (sFaceAngle >= 0 && sFaceAngle <= 360) {
          sOrientationResult = paramArrayOffloat[0];
          if ((sFaceAngle >= 0 && sFaceAngle < 60) || (sFaceAngle >= 300 && sFaceAngle <= 360)) {
            stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("nash_debug, Hardware orientation: ");
            stringBuilder.append(paramArrayOffloat[0]);
            stringBuilder.append(" sFaceAngle: ");
            stringBuilder.append(sFaceAngle);
            stringBuilder.append(", may upload FACE_ORIENTATION_0");
            Log.d("SmartRotationDebug", stringBuilder.toString());
            sOrientationResult = 0.0F;
          } 
          sFaceAngle = -2;
          if (sLaunchCameraAhead)
            sLaunchCameraAhead = false; 
          return sOrientationResult;
        } 
        if (sFaceAngle == -1) {
          printDetailLog("nash_debug, we lost a face, maybe there is nobody here.");
        } else if (sFaceAngle == -2) {
          printDetailLog("nash_debug, Default face angle, use hardware data.");
        } else {
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Illegal sFaceAngle: ");
          stringBuilder.append(sFaceAngle);
          Log.w("SmartRotationDebug", stringBuilder.toString());
        } 
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("nash_debug, sAONService is not init, dispatch orientation directlly. values[0] = ");
        stringBuilder.append(paramArrayOffloat[0]);
        Log.d("SmartRotationDebug", stringBuilder.toString());
      } 
      if (sLaunchCameraAhead)
        sLaunchCameraAhead = false; 
      sFaceAngle = -2;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("nash_debug, smart rotation dispatch event error e = ");
      stringBuilder.append(exception.toString());
      Log.e("SmartRotationDebug", stringBuilder.toString());
    } 
    return paramArrayOffloat[0];
  }
  
  public static float testModeData(float[] paramArrayOffloat) {
    // Byte code:
    //   0: getstatic com/aiunit/aon/AonSmartRotation.sTestFaceAngle : I
    //   3: istore_1
    //   4: iload_1
    //   5: bipush #-2
    //   7: if_icmpeq -> 397
    //   10: getstatic com/aiunit/aon/AonSmartRotation.sTestOrientationHardwareValue : F
    //   13: fstore_2
    //   14: fload_2
    //   15: ldc -1.0
    //   17: fcmpl
    //   18: ifeq -> 397
    //   21: iload_1
    //   22: iflt -> 391
    //   25: iload_1
    //   26: sipush #360
    //   29: if_icmpgt -> 391
    //   32: fload_2
    //   33: ldc_w 9.0
    //   36: fcmpg
    //   37: ifgt -> 391
    //   40: fload_2
    //   41: fconst_0
    //   42: fcmpl
    //   43: iflt -> 391
    //   46: iload_1
    //   47: iflt -> 56
    //   50: iload_1
    //   51: bipush #60
    //   53: if_icmplt -> 74
    //   56: getstatic com/aiunit/aon/AonSmartRotation.sTestFaceAngle : I
    //   59: istore_1
    //   60: iload_1
    //   61: sipush #300
    //   64: if_icmplt -> 139
    //   67: iload_1
    //   68: sipush #360
    //   71: if_icmpgt -> 139
    //   74: new java/lang/StringBuilder
    //   77: dup
    //   78: invokespecial <init> : ()V
    //   81: astore_0
    //   82: aload_0
    //   83: ldc_w 'TestMode, sTestOrientationHardwareValue: '
    //   86: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   89: pop
    //   90: aload_0
    //   91: getstatic com/aiunit/aon/AonSmartRotation.sTestOrientationHardwareValue : F
    //   94: invokevirtual append : (F)Ljava/lang/StringBuilder;
    //   97: pop
    //   98: aload_0
    //   99: ldc_w ' sTestFaceAngle: '
    //   102: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   105: pop
    //   106: aload_0
    //   107: getstatic com/aiunit/aon/AonSmartRotation.sTestFaceAngle : I
    //   110: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   113: pop
    //   114: aload_0
    //   115: ldc_w ', may upload FACE_ORIENTATION_0'
    //   118: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   121: pop
    //   122: ldc 'SmartRotationDebug'
    //   124: aload_0
    //   125: invokevirtual toString : ()Ljava/lang/String;
    //   128: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   131: pop
    //   132: fconst_0
    //   133: putstatic com/aiunit/aon/AonSmartRotation.sTestOrientationResult : F
    //   136: goto -> 367
    //   139: getstatic com/aiunit/aon/AonSmartRotation.sTestFaceAngle : I
    //   142: istore_1
    //   143: iload_1
    //   144: sipush #240
    //   147: if_icmplt -> 223
    //   150: iload_1
    //   151: sipush #300
    //   154: if_icmpge -> 223
    //   157: new java/lang/StringBuilder
    //   160: dup
    //   161: invokespecial <init> : ()V
    //   164: astore_0
    //   165: aload_0
    //   166: ldc_w 'TestMode, sTestOrientationHardwareValue: '
    //   169: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   172: pop
    //   173: aload_0
    //   174: getstatic com/aiunit/aon/AonSmartRotation.sTestOrientationHardwareValue : F
    //   177: invokevirtual append : (F)Ljava/lang/StringBuilder;
    //   180: pop
    //   181: aload_0
    //   182: ldc_w ' sTestFaceAngle: '
    //   185: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   188: pop
    //   189: aload_0
    //   190: getstatic com/aiunit/aon/AonSmartRotation.sTestFaceAngle : I
    //   193: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   196: pop
    //   197: aload_0
    //   198: ldc_w ', may upload FACE_ORIENTATION_3'
    //   201: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   204: pop
    //   205: ldc 'SmartRotationDebug'
    //   207: aload_0
    //   208: invokevirtual toString : ()Ljava/lang/String;
    //   211: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   214: pop
    //   215: ldc 3.0
    //   217: putstatic com/aiunit/aon/AonSmartRotation.sTestOrientationResult : F
    //   220: goto -> 367
    //   223: getstatic com/aiunit/aon/AonSmartRotation.sTestFaceAngle : I
    //   226: istore_1
    //   227: iload_1
    //   228: bipush #120
    //   230: if_icmplt -> 305
    //   233: iload_1
    //   234: sipush #240
    //   237: if_icmpge -> 305
    //   240: new java/lang/StringBuilder
    //   243: dup
    //   244: invokespecial <init> : ()V
    //   247: astore_0
    //   248: aload_0
    //   249: ldc_w 'TestMode, sTestOrientationHardwareValue: '
    //   252: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   255: pop
    //   256: aload_0
    //   257: getstatic com/aiunit/aon/AonSmartRotation.sTestOrientationHardwareValue : F
    //   260: invokevirtual append : (F)Ljava/lang/StringBuilder;
    //   263: pop
    //   264: aload_0
    //   265: ldc_w ' sTestFaceAngle: '
    //   268: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   271: pop
    //   272: aload_0
    //   273: getstatic com/aiunit/aon/AonSmartRotation.sTestFaceAngle : I
    //   276: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   279: pop
    //   280: aload_0
    //   281: ldc_w ', may upload FACE_ORIENTATION_2'
    //   284: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   287: pop
    //   288: ldc 'SmartRotationDebug'
    //   290: aload_0
    //   291: invokevirtual toString : ()Ljava/lang/String;
    //   294: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   297: pop
    //   298: fconst_2
    //   299: putstatic com/aiunit/aon/AonSmartRotation.sTestOrientationResult : F
    //   302: goto -> 367
    //   305: new java/lang/StringBuilder
    //   308: dup
    //   309: invokespecial <init> : ()V
    //   312: astore_0
    //   313: aload_0
    //   314: ldc_w 'TestMode, sTestOrientationHardwareValue: '
    //   317: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   320: pop
    //   321: aload_0
    //   322: getstatic com/aiunit/aon/AonSmartRotation.sTestOrientationHardwareValue : F
    //   325: invokevirtual append : (F)Ljava/lang/StringBuilder;
    //   328: pop
    //   329: aload_0
    //   330: ldc_w ' sTestFaceAngle: '
    //   333: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   336: pop
    //   337: aload_0
    //   338: getstatic com/aiunit/aon/AonSmartRotation.sTestFaceAngle : I
    //   341: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   344: pop
    //   345: aload_0
    //   346: ldc_w ', may upload FACE_ORIENTATION_1'
    //   349: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   352: pop
    //   353: ldc 'SmartRotationDebug'
    //   355: aload_0
    //   356: invokevirtual toString : ()Ljava/lang/String;
    //   359: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   362: pop
    //   363: fconst_1
    //   364: putstatic com/aiunit/aon/AonSmartRotation.sTestOrientationResult : F
    //   367: bipush #-2
    //   369: putstatic com/aiunit/aon/AonSmartRotation.sTestFaceAngle : I
    //   372: ldc -1.0
    //   374: putstatic com/aiunit/aon/AonSmartRotation.sTestOrientationHardwareValue : F
    //   377: getstatic com/aiunit/aon/AonSmartRotation.sLaunchCameraAhead : Z
    //   380: ifeq -> 387
    //   383: iconst_0
    //   384: putstatic com/aiunit/aon/AonSmartRotation.sLaunchCameraAhead : Z
    //   387: getstatic com/aiunit/aon/AonSmartRotation.sTestOrientationResult : F
    //   390: freturn
    //   391: ldc_w 'TestMode, Ignore Illegal test data.'
    //   394: invokestatic printDetailLog : (Ljava/lang/String;)V
    //   397: bipush #-2
    //   399: putstatic com/aiunit/aon/AonSmartRotation.sTestFaceAngle : I
    //   402: ldc -1.0
    //   404: putstatic com/aiunit/aon/AonSmartRotation.sTestOrientationHardwareValue : F
    //   407: fconst_0
    //   408: putstatic com/aiunit/aon/AonSmartRotation.sTestOrientationResult : F
    //   411: getstatic com/aiunit/aon/AonSmartRotation.sLaunchCameraAhead : Z
    //   414: ifeq -> 421
    //   417: iconst_0
    //   418: putstatic com/aiunit/aon/AonSmartRotation.sLaunchCameraAhead : Z
    //   421: new java/lang/StringBuilder
    //   424: dup
    //   425: invokespecial <init> : ()V
    //   428: astore_3
    //   429: aload_3
    //   430: ldc_w 'TestMode, No test data, upload real hardware value: '
    //   433: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   436: pop
    //   437: aload_3
    //   438: aload_0
    //   439: iconst_0
    //   440: faload
    //   441: invokevirtual append : (F)Ljava/lang/StringBuilder;
    //   444: pop
    //   445: aload_3
    //   446: invokevirtual toString : ()Ljava/lang/String;
    //   449: invokestatic printDetailLog : (Ljava/lang/String;)V
    //   452: aload_0
    //   453: iconst_0
    //   454: faload
    //   455: freturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #462	-> 0
    //   #463	-> 21
    //   #464	-> 46
    //   #465	-> 74
    //   #466	-> 132
    //   #467	-> 139
    //   #468	-> 157
    //   #469	-> 215
    //   #470	-> 223
    //   #471	-> 240
    //   #472	-> 298
    //   #475	-> 305
    //   #476	-> 363
    //   #478	-> 367
    //   #479	-> 372
    //   #480	-> 377
    //   #481	-> 383
    //   #483	-> 387
    //   #485	-> 391
    //   #487	-> 397
    //   #488	-> 402
    //   #489	-> 407
    //   #490	-> 411
    //   #491	-> 417
    //   #493	-> 421
    //   #494	-> 452
  }
  
  private void bindAONService() {
    Intent intent = new Intent();
    intent.setComponent(new ComponentName("com.aiunit.aon", "com.aiunit.aon.AONService"));
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("nash_debug, Bind AON service intent: ");
    stringBuilder.append(intent);
    printDetailLog(stringBuilder.toString());
    this.mContext.bindServiceAsUser(intent, new AonSmartRotationServiceConnection(), 1, UserHandle.of(sCurrentUserId));
  }
  
  private void unbindAONService() {
    printDetailLog("nash_debug, Unbind AON service.");
    sAONService.asBinder().unlinkToDeath(sDeathRecipient, 0);
    Context context = this.mContext;
    if (context != null) {
      ServiceConnection serviceConnection = sServiceConnection;
      if (serviceConnection != null)
        context.unbindService(serviceConnection); 
    } 
    sAONService = null;
    sServiceConnection = null;
  }
  
  class AonSmartRotationServiceConnection implements ServiceConnection {
    final AonSmartRotation this$0;
    
    private AonSmartRotationServiceConnection() {}
    
    public void onServiceConnected(ComponentName param1ComponentName, IBinder param1IBinder) {
      Log.d("SmartRotationDebug", "AON onServiceConnected");
      AonSmartRotation.access$202(this);
      AonSmartRotation.access$302(IAONService.Stub.asInterface(param1IBinder));
      try {
        if (AonSmartRotation.sAONService != null) {
          AonSmartRotation.printDetailLog("nash_debug, fetch AON Service successed.");
          AonSmartRotation.sAONService.asBinder().linkToDeath(AonSmartRotation.sDeathRecipient, 0);
          if (AonSmartRotation.sSmartRotationHandlerThread == null)
            AonSmartRotation.initAONSubThread(); 
          AonSmartRotation.askForConnection();
        } else {
          Log.w("SmartRotationDebug", "fetch AON Service failed.");
        } 
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("AON smart rotation on Service connected error e = ");
        stringBuilder.append(exception.toString());
        Log.e("SmartRotationDebug", stringBuilder.toString());
      } 
    }
    
    public void onServiceDisconnected(ComponentName param1ComponentName) {
      AonSmartRotation.access$202(null);
      AonSmartRotation.access$302(null);
      AonSmartRotation.printDetailLog("AON onServiceDisconnected");
    }
  }
  
  class SmartRotationHandler extends Handler {
    SmartRotationHandler(AonSmartRotation this$0) {
      super((Looper)this$0);
    }
    
    public void handleMessage(Message param1Message) {
      super.handleMessage(param1Message);
      switch (param1Message.what) {
        default:
          return;
        case 10006:
          AonSmartRotation.releaseCameraInner();
        case 10005:
          AonSmartRotation.launchCameraInner();
        case 10004:
          AonSmartRotation.dropConnectionInner();
        case 10003:
          AonSmartRotation.askForConnectionInner();
        case 10002:
          AonSmartRotation.useAllProtocol();
        case 10001:
          break;
      } 
      AonSmartRotation.printDetailLog("init SmartRotationHandler success");
    }
  }
  
  public static void initAONSubThread() {
    initHandlerThread();
    Message message = Message.obtain(sSmartRotationHandler, 10001);
    message.sendToTarget();
  }
  
  public static void initHandlerThread() {
    if (sSmartRotationHandlerThread == null) {
      printDetailLog("init SmartRotationHandlerThread");
      HandlerThread handlerThread = new HandlerThread("SmartRotationHandlerThread");
      handlerThread.start();
      sSmartRotationLooper = sSmartRotationHandlerThread.getLooper();
      sSmartRotationHandler = new SmartRotationHandler(sSmartRotationLooper);
    } 
  }
  
  public static void dropAONSubThread() {
    printDetailLog("nash_debug, AON disconnect AON Service.");
    dropConnection();
    destroyHandlerThread();
  }
  
  public static void destroyHandlerThread() {
    try {
      if (sSmartRotationLooper != null) {
        sSmartRotationLooper.quitSafely();
        sSmartRotationLooper = null;
      } 
      if (sSmartRotationHandlerThread != null) {
        sSmartRotationHandlerThread.quitSafely();
        sSmartRotationHandlerThread = null;
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("stop SmartRotationHandlerThread error e = ");
      stringBuilder.append(exception.toString());
      Log.w("SmartRotationDebug", stringBuilder.toString());
    } 
  }
  
  public static int registerDirection() throws RemoteException {
    printDetailLog("nash_debug, registerDirection");
    IAONService iAONService = sAONService;
    if (iAONService == null) {
      Log.w("SmartRotationDebug", "nash_debug, sAONService is null, registerDirection failed");
      return -1;
    } 
    try {
      return iAONService.registerListener(sAonlistener, 393218);
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
      return 4099;
    } 
  }
  
  public static int unRegisterDirection() throws RemoteException {
    printDetailLog("nash_debug, unRegisterDirection");
    IAONService iAONService = sAONService;
    if (iAONService == null) {
      Log.w("SmartRotationDebug", "nash_debug, sAONService is null, unRegisterDirection failed");
      return -1;
    } 
    try {
      return iAONService.unRegisterListener(sAonlistener, 393218);
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
      return 4099;
    } 
  }
  
  public static int start() {
    printDetailLog("nash_debug, start camera");
    IAONService iAONService = sAONService;
    if (iAONService == null) {
      Log.w("SmartRotationDebug", "nash_debug, sAONService is null, start failed");
      return -1;
    } 
    try {
      sIsCameraOn = true;
      return iAONService.start(393218);
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
      return 4099;
    } 
  }
  
  public static int stop() {
    printDetailLog("nash_debug, stop camera");
    IAONService iAONService = sAONService;
    if (iAONService == null) {
      Log.w("SmartRotationDebug", "nash_debug, sAONService is null, stop failed");
      return -1;
    } 
    try {
      sIsCameraOn = false;
      return iAONService.stop(393218);
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
      return 4099;
    } 
  }
  
  public static void testAllProtocol() {
    printDetailLog("nash_debug, test All protocols.");
    Message message = Message.obtain(sSmartRotationHandler, 10002);
    message.sendToTarget();
  }
  
  private static void useAllProtocol() {
    try {
      int i = registerDirection();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("nash_debug, registerDirection result is ");
      stringBuilder.append(i);
      printDetailLog(stringBuilder.toString());
      i = start();
      stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("nash_debug, start result is ");
      stringBuilder.append(i);
      printDetailLog(stringBuilder.toString());
      i = stop();
      stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("nash_debug, stop result is ");
      stringBuilder.append(i);
      printDetailLog(stringBuilder.toString());
      i = unRegisterDirection();
      stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("nash_debug, unRegisterDirection result is ");
      stringBuilder.append(i);
      printDetailLog(stringBuilder.toString());
    } catch (RemoteException remoteException) {
      Log.e("SmartRotationDebug", "use All protocols catch RemoteException.");
      remoteException.printStackTrace();
    } 
    printDetailLog("nash_debug, use All protocols finished.");
  }
  
  public static void askForConnection() {
    printDetailLog("nash_debug, ask for connection.");
    Message message = Message.obtain(sSmartRotationHandler, 10003);
    message.sendToTarget();
  }
  
  private static void askForConnectionInner() {
    try {
      int i = registerDirection();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("nash_debug, registerDirection result is ");
      stringBuilder.append(i);
      printDetailLog(stringBuilder.toString());
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("nash_debug, AON smart rotation askForConnectionInner error e = ");
      stringBuilder.append(exception.toString());
      Log.e("SmartRotationDebug", stringBuilder.toString());
    } 
  }
  
  public static void dropConnection() {
    printDetailLog("nash_debug, drop connection.");
    Message message = Message.obtain(sSmartRotationHandler, 10004);
    message.sendToTarget();
  }
  
  private static void dropConnectionInner() {
    try {
      if (sIsCameraOn) {
        Log.w("SmartRotationDebug", "nash_debug, About to dropConnection but camera still on, stop it.");
        stop();
      } 
      int i = unRegisterDirection();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("nash_debug, unRegisterDirection result is ");
      stringBuilder.append(i);
      printDetailLog(stringBuilder.toString());
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("nash_debug, AON smart rotation dropConnectionInner error e = ");
      stringBuilder.append(exception.toString());
      Log.e("SmartRotationDebug", stringBuilder.toString());
    } 
  }
  
  public static void launchCamera() {
    printDetailLog("nash_debug, launch Camera.");
    Message message = Message.obtain(sSmartRotationHandler, 10005);
    message.sendToTarget();
  }
  
  private static void launchCameraInner() {
    try {
      int i = start();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("nash_debug, start result is ");
      stringBuilder.append(i);
      printDetailLog(stringBuilder.toString());
      if (isErrorCodeNeedStopCamera(i)) {
        releaseCamera();
        Log.w("SmartRotationDebug", "Stop camera for special AON error code.");
      } 
      if (i == 20512) {
        askForConnection();
        Log.w("SmartRotationDebug", "Start before register a connection, ask for a new one.");
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("nash_debug, AON smart rotation launchCameraInner error e = ");
      stringBuilder.append(exception.toString());
      Log.e("SmartRotationDebug", stringBuilder.toString());
    } 
  }
  
  public static void releaseCamera() {
    printDetailLog("nash_debug, release Camera.");
    Message message = Message.obtain(sSmartRotationHandler, 10006);
    message.sendToTarget();
  }
  
  private static void releaseCameraInner() {
    try {
      int i = stop();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("nash_debug, stop result is ");
      stringBuilder.append(i);
      printDetailLog(stringBuilder.toString());
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("nash_debug, AON smart rotation releaseCameraInner error e = ");
      stringBuilder.append(exception.toString());
      Log.e("SmartRotationDebug", stringBuilder.toString());
    } 
  }
  
  public static boolean isErrorCodeNeedStopCamera(int paramInt) {
    if (stopCameraErrorCodes.contains(Integer.valueOf(paramInt))) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("nash_debug, AON return error, errorCode is ");
      stringBuilder.append(paramInt);
      Log.w("SmartRotationDebug", stringBuilder.toString());
      return true;
    } 
    return false;
  }
}
