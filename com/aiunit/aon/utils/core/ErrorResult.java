package com.aiunit.aon.utils.core;

import android.os.Parcel;
import android.os.Parcelable;

public class ErrorResult implements Parcelable {
  public static final Parcelable.Creator<ErrorResult> CREATOR = new Parcelable.Creator<ErrorResult>() {
      public ErrorResult createFromParcel(Parcel param1Parcel) {
        return new ErrorResult(param1Parcel);
      }
      
      public ErrorResult[] newArray(int param1Int) {
        return new ErrorResult[param1Int];
      }
    };
  
  private int mResultCode;
  
  public ErrorResult(int paramInt) {
    this.mResultCode = paramInt;
  }
  
  protected ErrorResult(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mResultCode = paramParcel.readInt();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public int getResultCode() {
    return this.mResultCode;
  }
  
  public void setResultCode(int paramInt) {
    this.mResultCode = paramInt;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mResultCode);
  }
}
