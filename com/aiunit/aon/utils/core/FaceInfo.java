package com.aiunit.aon.utils.core;

import android.os.Parcel;
import android.os.Parcelable;

public class FaceInfo implements Parcelable {
  private static final String CHARSET_UTF8 = "UTF-8";
  
  public static final Parcelable.Creator<FaceInfo> CREATOR = new Parcelable.Creator<FaceInfo>() {
      public FaceInfo createFromParcel(Parcel param1Parcel) {
        return new FaceInfo(param1Parcel.readInt(), param1Parcel.readInt(), param1Parcel.readFloat(), param1Parcel.readFloat(), param1Parcel.readFloat());
      }
      
      public FaceInfo[] newArray(int param1Int) {
        return new FaceInfo[param1Int];
      }
    };
  
  private int mHeight;
  
  private float mPitch;
  
  private float mRoll;
  
  private int mWidth;
  
  private float mYaw;
  
  public FaceInfo(int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, float paramFloat3) {
    this.mWidth = paramInt1;
    this.mHeight = paramInt2;
    this.mYaw = paramFloat1;
    this.mPitch = paramFloat2;
    this.mRoll = paramFloat3;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mWidth);
    paramParcel.writeInt(this.mHeight);
    paramParcel.writeFloat(this.mYaw);
    paramParcel.writeFloat(this.mPitch);
    paramParcel.writeFloat(this.mRoll);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mWidth = paramParcel.readInt();
    this.mHeight = paramParcel.readInt();
    this.mYaw = paramParcel.readFloat();
    this.mPitch = paramParcel.readInt();
    this.mRoll = paramParcel.readInt();
  }
  
  public int getmWidth() {
    return this.mWidth;
  }
  
  public void setmWidth(int paramInt) {
    this.mWidth = paramInt;
  }
  
  public int getmHeight() {
    return this.mHeight;
  }
  
  public void setmHeight(int paramInt) {
    this.mHeight = paramInt;
  }
  
  public float getmYaw() {
    return this.mYaw;
  }
  
  public void setmYaw(float paramFloat) {
    this.mYaw = paramFloat;
  }
  
  public float getmPitch() {
    return this.mPitch;
  }
  
  public void setmPitch(float paramFloat) {
    this.mPitch = paramFloat;
  }
  
  public float getmRoll() {
    return this.mRoll;
  }
  
  public void setmRoll(float paramFloat) {
    this.mRoll = paramFloat;
  }
}
