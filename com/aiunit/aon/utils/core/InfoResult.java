package com.aiunit.aon.utils.core;

import android.os.Parcel;
import android.os.Parcelable;

public class InfoResult implements Parcelable {
  public static final Parcelable.Creator<InfoResult> CREATOR = new Parcelable.Creator<InfoResult>() {
      public InfoResult createFromParcel(Parcel param1Parcel) {
        return new InfoResult(param1Parcel);
      }
      
      public InfoResult[] newArray(int param1Int) {
        return new InfoResult[param1Int];
      }
    };
  
  private String mInfoResult;
  
  protected InfoResult(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public InfoResult(String paramString) {
    this.mInfoResult = paramString;
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mInfoResult = paramParcel.readString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String getInfoResult() {
    return this.mInfoResult;
  }
  
  public void setInfoResult(String paramString) {
    this.mInfoResult = paramString;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mInfoResult);
  }
}
