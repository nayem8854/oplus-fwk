package com.aiunit.aon.utils.core;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;

public class CommentResult implements Parcelable {
  private static final String CHARSET_UTF8 = "UTF-8";
  
  public static final Parcelable.Creator<CommentResult> CREATOR = new Parcelable.Creator<CommentResult>() {
      public CommentResult createFromParcel(Parcel param1Parcel) {
        return new CommentResult(param1Parcel);
      }
      
      public CommentResult[] newArray(int param1Int) {
        return new CommentResult[param1Int];
      }
    };
  
  private static final int SHARED_MEMORY_MIN_SIZE = 409600;
  
  private static final String TAG = "CommentResult";
  
  private Bitmap mBitmap;
  
  private MemShare mMemShare;
  
  private String mResult;
  
  public CommentResult() {
    this.mResult = "";
    this.mBitmap = null;
  }
  
  protected CommentResult(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public CommentResult(String paramString) {
    this.mResult = paramString;
    this.mBitmap = null;
  }
  
  public CommentResult(String paramString, Bitmap paramBitmap) {
    this.mResult = paramString;
    this.mBitmap = paramBitmap;
  }
  
  private void readFromMemory() {
    try {
      byte[] arrayOfByte = this.mMemShare.getData();
      if (arrayOfByte == null) {
        Log.w("CommentResult", "get data null");
        this.mResult = null;
        return;
      } 
      String str = new String();
      this(arrayOfByte, "UTF-8");
      this.mResult = str;
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("UnsupportedEncodingException ");
      stringBuilder.append(unsupportedEncodingException.getMessage());
      Log.e("CommentResult", stringBuilder.toString());
    } 
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mResult = paramParcel.readString();
    this.mBitmap = (Bitmap)paramParcel.readParcelable(Bitmap.class.getClassLoader());
    MemShare memShare = (MemShare)paramParcel.readParcelable(MemShare.class.getClassLoader());
    if (memShare != null)
      readFromMemory(); 
  }
  
  private void writeToMemory(String paramString) {
    try {
      byte[] arrayOfByte = paramString.getBytes("UTF-8");
      MemShare memShare = new MemShare();
      this();
      this.mMemShare = memShare;
      memShare.setData(arrayOfByte);
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("UnsupportedEncodingException ");
      stringBuilder.append(unsupportedEncodingException.getMessage());
      Log.e("CommentResult", stringBuilder.toString());
    } 
  }
  
  public void appendResult(String paramString1, String paramString2) {
    if (this.mResult != null)
      try {
        JSONObject jSONObject = new JSONObject();
        this(this.mResult);
        jSONObject.put(paramString1, paramString2);
        this.mResult = jSONObject.toString();
      } catch (JSONException jSONException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("appendResult error ");
        stringBuilder.append(jSONException.getMessage());
        Log.e("CommentResult", stringBuilder.toString());
      }  
  }
  
  public int describeContents() {
    return 0;
  }
  
  public Bitmap getBitmap() {
    return this.mBitmap;
  }
  
  public String getResult() {
    return this.mResult;
  }
  
  public void setBitmap(Bitmap paramBitmap) {
    this.mBitmap = paramBitmap;
  }
  
  public void setResult(String paramString) {
    this.mResult = paramString;
  }
  
  public String toString() {
    String str;
    if (this.mResult != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("CommentResult{");
      stringBuilder.append(this.mResult);
      stringBuilder.append('}');
      str = stringBuilder.toString();
    } else {
      str = "CommentResult NULL";
    } 
    return str;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    String str = this.mResult;
    if (str == null || str.length() <= 409600) {
      paramParcel.writeString(this.mResult);
    } else {
      paramParcel.writeString("CommentResult");
      writeToMemory(this.mResult);
    } 
    paramParcel.writeParcelable((Parcelable)this.mBitmap, paramInt);
    paramParcel.writeParcelable(this.mMemShare, paramInt);
  }
}
