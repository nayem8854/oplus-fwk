package com.aiunit.aon.utils.core;

import android.os.Build;
import android.os.MemoryFile;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.SharedMemory;
import android.system.ErrnoException;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;

public class MemShare implements Parcelable {
  public static final Parcelable.Creator<MemShare> CREATOR = new Parcelable.Creator<MemShare>() {
      public MemShare createFromParcel(Parcel param1Parcel) {
        return new MemShare(param1Parcel);
      }
      
      public MemShare[] newArray(int param1Int) {
        return new MemShare[param1Int];
      }
    };
  
  private static final String DEFAULT_MEMORY_NAME = "MemShare";
  
  private static final int SHARE_DATA_SIZE = 819200;
  
  private static final String TAG = "MemShare";
  
  private static final int TYPE_MEMORY_FILE = 2;
  
  private static final int TYPE_SHARED_MEMORY = 1;
  
  private byte[] mData;
  
  private int mDataLen;
  
  private ByteBuffer mMapping;
  
  private MemoryFile mMemoryFile;
  
  private ParcelFileDescriptor mPfd;
  
  private SharedMemory mSharedMemory;
  
  private int mSharedType;
  
  public MemShare() {
    this.mDataLen = 0;
  }
  
  private MemShare(Parcel paramParcel) {
    this.mDataLen = 0;
    readFromParcel(paramParcel);
  }
  
  private int checkSharedType() {
    byte b;
    if (Build.VERSION.SDK_INT > 26) {
      b = 1;
    } else {
      b = 2;
    } 
    return b;
  }
  
  private ParcelFileDescriptor getPfd() {
    try {
      return ParcelFileDescriptor.dup((FileDescriptor)MemoryFile.class.getDeclaredMethod("getFileDescriptor", new Class[0]).invoke(this.mMemoryFile, new Object[0]));
    } catch (NoSuchMethodException noSuchMethodException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("NoSuchMethodException ");
      stringBuilder.append(noSuchMethodException.getMessage());
      Log.w("MemShare", stringBuilder.toString());
    } catch (InvocationTargetException invocationTargetException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("InvocationTargetException ");
      stringBuilder.append(invocationTargetException.getMessage());
      Log.w("MemShare", stringBuilder.toString());
    } catch (IllegalAccessException illegalAccessException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("IllegalAccessException ");
      stringBuilder.append(illegalAccessException.getMessage());
      Log.w("MemShare", stringBuilder.toString());
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("IOException ");
      stringBuilder.append(iOException.getMessage());
      Log.w("MemShare", stringBuilder.toString());
    } 
    return null;
  }
  
  private void readFromMemoryFile(Parcel paramParcel) {}
  
  private void readFromParcel(Parcel paramParcel) {
    this.mSharedType = paramParcel.readInt();
    this.mDataLen = paramParcel.readInt();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("readFromParcel type=");
    stringBuilder.append(this.mSharedType);
    stringBuilder.append(", dataLen=");
    stringBuilder.append(this.mDataLen);
    Log.d("MemShare", stringBuilder.toString());
    if (this.mDataLen == 0) {
      Log.w("MemShare", "readFromParcel data len is 0");
    } else {
      int i = this.mSharedType;
      if (i == 1) {
        readFromSharedMemory(paramParcel);
      } else if (i == 2) {
        readFromMemoryFile(paramParcel);
      } else {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("memory share type error ");
        stringBuilder1.append(this.mSharedType);
        Log.w("MemShare", stringBuilder1.toString());
      } 
    } 
  }
  
  private void readFromSharedMemory(Parcel paramParcel) {
    this.mSharedMemory = null = (SharedMemory)paramParcel.readParcelable(SharedMemory.class.getClassLoader());
    if (null == null) {
      Log.w("MemShare", "readParcelable error");
      return;
    } 
    try {
      ByteBuffer byteBuffer = null.mapReadOnly();
      if (byteBuffer != null) {
        int i = this.mSharedMemory.getSize();
        this.mData = new byte[i];
        for (byte b = 0; b < i; b++)
          this.mData[b] = this.mMapping.get(b); 
      } 
    } catch (ErrnoException errnoException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ErrnoException ");
      stringBuilder.append(errnoException.getMessage());
      Log.w("MemShare", stringBuilder.toString());
    } finally {
      close();
    } 
  }
  
  private void writeToMemoryFile(Parcel paramParcel) {
    try {
      MemoryFile memoryFile = new MemoryFile();
      this("MemShare", this.mDataLen);
      this.mMemoryFile = memoryFile;
      memoryFile.writeBytes(this.mData, 0, 0, this.mDataLen);
      ParcelFileDescriptor parcelFileDescriptor = getPfd();
      if (parcelFileDescriptor == null) {
        Log.w("MemShare", "getPfd null");
        return;
      } 
    } catch (IOException iOException) {
      Log.w("MemShare", "create memory file error");
    } 
    paramParcel.writeFileDescriptor(this.mPfd.getFileDescriptor());
  }
  
  private void writeToSharedMemory(Parcel paramParcel, int paramInt) {
    try {
      SharedMemory sharedMemory = SharedMemory.create("MemShare", this.mDataLen);
      ByteBuffer byteBuffer = sharedMemory.mapReadWrite();
      byteBuffer.put(this.mData);
    } catch (ErrnoException errnoException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ErrnoException ");
      stringBuilder.append(errnoException.getMessage());
      Log.e("MemShare", stringBuilder.toString());
    } 
    paramParcel.writeParcelable((Parcelable)this.mSharedMemory, paramInt);
  }
  
  public void close() {
    if (this.mSharedType == 1 && this.mSharedMemory != null) {
      SharedMemory sharedMemory = this.mSharedMemory;
      SharedMemory.unmap(this.mMapping);
      this.mSharedMemory.close();
      this.mMapping = null;
      this.mSharedMemory = null;
    } else if (this.mSharedType == 2) {
      MemoryFile memoryFile = this.mMemoryFile;
      if (memoryFile != null) {
        memoryFile.close();
        this.mMemoryFile = null;
      } 
      ParcelFileDescriptor parcelFileDescriptor = this.mPfd;
      if (parcelFileDescriptor != null) {
        try {
          parcelFileDescriptor.close();
        } catch (IOException iOException) {
          Log.w("MemShare", "fd close error");
        } 
        this.mPfd = null;
      } 
    } 
  }
  
  public int describeContents() {
    return 0;
  }
  
  protected void finalize() throws Throwable {
    Log.d("MemShare", "finalize");
    close();
    super.finalize();
  }
  
  public byte[] getData() {
    byte[] arrayOfByte = this.mData;
    if (arrayOfByte == null) {
      arrayOfByte = null;
    } else {
      arrayOfByte = (byte[])arrayOfByte.clone();
    } 
    return arrayOfByte;
  }
  
  public void release() {
    close();
    this.mData = null;
  }
  
  public void setData(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte == null) {
      this.mData = null;
      this.mDataLen = 0;
      return;
    } 
    this.mData = (byte[])paramArrayOfbyte.clone();
    this.mDataLen = paramArrayOfbyte.length;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.mSharedType = checkSharedType();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("writeToParcel memory share type ");
    stringBuilder.append(this.mSharedType);
    Log.d("MemShare", stringBuilder.toString());
    paramParcel.writeInt(this.mSharedType);
    paramParcel.writeInt(this.mDataLen);
    if (this.mDataLen == 0) {
      Log.w("MemShare", "writeToParcel data size is 0");
    } else {
      int i = this.mSharedType;
      if (i == 1) {
        writeToSharedMemory(paramParcel, paramInt);
      } else if (i == 2) {
        writeToMemoryFile(paramParcel);
      } 
    } 
  }
}
