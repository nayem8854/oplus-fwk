package com.aiunit.aon.utils;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.aiunit.aon.utils.core.CommentResult;
import com.aiunit.aon.utils.core.ErrorResult;
import com.aiunit.aon.utils.core.InfoResult;

public interface IAONCallback extends IInterface {
  String getRequestID() throws RemoteException;
  
  void onDetectedError(ErrorResult paramErrorResult) throws RemoteException;
  
  void onDetectedInfo(InfoResult paramInfoResult) throws RemoteException;
  
  void onDetectedResult(CommentResult paramCommentResult) throws RemoteException;
  
  class Default implements IAONCallback {
    public String getRequestID() throws RemoteException {
      return null;
    }
    
    public void onDetectedError(ErrorResult param1ErrorResult) throws RemoteException {}
    
    public void onDetectedInfo(InfoResult param1InfoResult) throws RemoteException {}
    
    public void onDetectedResult(CommentResult param1CommentResult) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAONCallback {
    private static final String DESCRIPTOR = "com.aiunit.aon.utils.IAONCallback";
    
    static final int TRANSACTION_getRequestID = 1;
    
    static final int TRANSACTION_onDetectedError = 2;
    
    static final int TRANSACTION_onDetectedInfo = 3;
    
    static final int TRANSACTION_onDetectedResult = 4;
    
    public Stub() {
      attachInterface(this, "com.aiunit.aon.utils.IAONCallback");
    }
    
    public static IAONCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.aiunit.aon.utils.IAONCallback");
      if (iInterface != null && iInterface instanceof IAONCallback)
        return (IAONCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onDetectedResult";
          } 
          return "onDetectedInfo";
        } 
        return "onDetectedError";
      } 
      return "getRequestID";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("com.aiunit.aon.utils.IAONCallback");
              return true;
            } 
            param1Parcel1.enforceInterface("com.aiunit.aon.utils.IAONCallback");
            if (param1Parcel1.readInt() != 0) {
              CommentResult commentResult = (CommentResult)CommentResult.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onDetectedResult((CommentResult)param1Parcel1);
            param1Parcel2.writeNoException();
            if (param1Parcel1 != null) {
              param1Parcel2.writeInt(1);
              param1Parcel1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          } 
          param1Parcel1.enforceInterface("com.aiunit.aon.utils.IAONCallback");
          if (param1Parcel1.readInt() != 0) {
            InfoResult infoResult = (InfoResult)InfoResult.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          onDetectedInfo((InfoResult)param1Parcel1);
          param1Parcel2.writeNoException();
          if (param1Parcel1 != null) {
            param1Parcel2.writeInt(1);
            param1Parcel1.writeToParcel(param1Parcel2, 1);
          } else {
            param1Parcel2.writeInt(0);
          } 
          return true;
        } 
        param1Parcel1.enforceInterface("com.aiunit.aon.utils.IAONCallback");
        if (param1Parcel1.readInt() != 0) {
          ErrorResult errorResult = (ErrorResult)ErrorResult.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onDetectedError((ErrorResult)param1Parcel1);
        param1Parcel2.writeNoException();
        if (param1Parcel1 != null) {
          param1Parcel2.writeInt(1);
          param1Parcel1.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      param1Parcel1.enforceInterface("com.aiunit.aon.utils.IAONCallback");
      String str = getRequestID();
      param1Parcel2.writeNoException();
      param1Parcel2.writeString(str);
      return true;
    }
    
    private static class Proxy implements IAONCallback {
      public static IAONCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.aiunit.aon.utils.IAONCallback";
      }
      
      public String getRequestID() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.aiunit.aon.utils.IAONCallback");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IAONCallback.Stub.getDefaultImpl() != null)
            return IAONCallback.Stub.getDefaultImpl().getRequestID(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onDetectedError(ErrorResult param2ErrorResult) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.aiunit.aon.utils.IAONCallback");
          if (param2ErrorResult != null) {
            parcel1.writeInt(1);
            param2ErrorResult.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IAONCallback.Stub.getDefaultImpl() != null) {
            IAONCallback.Stub.getDefaultImpl().onDetectedError(param2ErrorResult);
            return;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0)
            param2ErrorResult.readFromParcel(parcel2); 
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onDetectedInfo(InfoResult param2InfoResult) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.aiunit.aon.utils.IAONCallback");
          if (param2InfoResult != null) {
            parcel1.writeInt(1);
            param2InfoResult.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IAONCallback.Stub.getDefaultImpl() != null) {
            IAONCallback.Stub.getDefaultImpl().onDetectedInfo(param2InfoResult);
            return;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0)
            param2InfoResult.readFromParcel(parcel2); 
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onDetectedResult(CommentResult param2CommentResult) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.aiunit.aon.utils.IAONCallback");
          if (param2CommentResult != null) {
            parcel1.writeInt(1);
            param2CommentResult.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IAONCallback.Stub.getDefaultImpl() != null) {
            IAONCallback.Stub.getDefaultImpl().onDetectedResult(param2CommentResult);
            return;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0)
            param2CommentResult.readFromParcel(parcel2); 
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAONCallback param1IAONCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAONCallback != null) {
          Proxy.sDefaultImpl = param1IAONCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAONCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
