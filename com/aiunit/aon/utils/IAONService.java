package com.aiunit.aon.utils;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAONService extends IInterface {
  int getServerError(int paramInt) throws RemoteException;
  
  String getServerState() throws RemoteException;
  
  String getVersionInfo() throws RemoteException;
  
  int registerListener(IAONEventListener paramIAONEventListener, int paramInt) throws RemoteException;
  
  int start(int paramInt) throws RemoteException;
  
  int stop(int paramInt) throws RemoteException;
  
  int unRegisterListener(IAONEventListener paramIAONEventListener, int paramInt) throws RemoteException;
  
  class Default implements IAONService {
    public String getServerState() throws RemoteException {
      return null;
    }
    
    public String getVersionInfo() throws RemoteException {
      return null;
    }
    
    public int getServerError(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int start(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int stop(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int registerListener(IAONEventListener param1IAONEventListener, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int unRegisterListener(IAONEventListener param1IAONEventListener, int param1Int) throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAONService {
    private static final String DESCRIPTOR = "com.aiunit.aon.utils.IAONService";
    
    static final int TRANSACTION_getServerError = 3;
    
    static final int TRANSACTION_getServerState = 1;
    
    static final int TRANSACTION_getVersionInfo = 2;
    
    static final int TRANSACTION_registerListener = 6;
    
    static final int TRANSACTION_start = 4;
    
    static final int TRANSACTION_stop = 5;
    
    static final int TRANSACTION_unRegisterListener = 7;
    
    public Stub() {
      attachInterface(this, "com.aiunit.aon.utils.IAONService");
    }
    
    public static IAONService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.aiunit.aon.utils.IAONService");
      if (iInterface != null && iInterface instanceof IAONService)
        return (IAONService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "unRegisterListener";
        case 6:
          return "registerListener";
        case 5:
          return "stop";
        case 4:
          return "start";
        case 3:
          return "getServerError";
        case 2:
          return "getVersionInfo";
        case 1:
          break;
      } 
      return "getServerState";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        IAONEventListener iAONEventListener;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("com.aiunit.aon.utils.IAONService");
            iAONEventListener = IAONEventListener.Stub.asInterface(param1Parcel1.readStrongBinder());
            param1Int1 = param1Parcel1.readInt();
            param1Int1 = unRegisterListener(iAONEventListener, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("com.aiunit.aon.utils.IAONService");
            iAONEventListener = IAONEventListener.Stub.asInterface(param1Parcel1.readStrongBinder());
            param1Int1 = param1Parcel1.readInt();
            param1Int1 = registerListener(iAONEventListener, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("com.aiunit.aon.utils.IAONService");
            param1Int1 = param1Parcel1.readInt();
            param1Int1 = stop(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("com.aiunit.aon.utils.IAONService");
            param1Int1 = param1Parcel1.readInt();
            param1Int1 = start(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 3:
            param1Parcel1.enforceInterface("com.aiunit.aon.utils.IAONService");
            param1Int1 = param1Parcel1.readInt();
            param1Int1 = getServerError(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 2:
            param1Parcel1.enforceInterface("com.aiunit.aon.utils.IAONService");
            str = getVersionInfo();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str);
            return true;
          case 1:
            break;
        } 
        str.enforceInterface("com.aiunit.aon.utils.IAONService");
        String str = getServerState();
        param1Parcel2.writeNoException();
        param1Parcel2.writeString(str);
        return true;
      } 
      param1Parcel2.writeString("com.aiunit.aon.utils.IAONService");
      return true;
    }
    
    private static class Proxy implements IAONService {
      public static IAONService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.aiunit.aon.utils.IAONService";
      }
      
      public String getServerState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.aiunit.aon.utils.IAONService");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IAONService.Stub.getDefaultImpl() != null)
            return IAONService.Stub.getDefaultImpl().getServerState(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getVersionInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.aiunit.aon.utils.IAONService");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IAONService.Stub.getDefaultImpl() != null)
            return IAONService.Stub.getDefaultImpl().getVersionInfo(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getServerError(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.aiunit.aon.utils.IAONService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IAONService.Stub.getDefaultImpl() != null) {
            param2Int = IAONService.Stub.getDefaultImpl().getServerError(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int start(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.aiunit.aon.utils.IAONService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IAONService.Stub.getDefaultImpl() != null) {
            param2Int = IAONService.Stub.getDefaultImpl().start(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int stop(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.aiunit.aon.utils.IAONService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IAONService.Stub.getDefaultImpl() != null) {
            param2Int = IAONService.Stub.getDefaultImpl().stop(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int registerListener(IAONEventListener param2IAONEventListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.aiunit.aon.utils.IAONService");
          if (param2IAONEventListener != null) {
            iBinder = param2IAONEventListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IAONService.Stub.getDefaultImpl() != null) {
            param2Int = IAONService.Stub.getDefaultImpl().registerListener(param2IAONEventListener, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int unRegisterListener(IAONEventListener param2IAONEventListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.aiunit.aon.utils.IAONService");
          if (param2IAONEventListener != null) {
            iBinder = param2IAONEventListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IAONService.Stub.getDefaultImpl() != null) {
            param2Int = IAONService.Stub.getDefaultImpl().unRegisterListener(param2IAONEventListener, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAONService param1IAONService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAONService != null) {
          Proxy.sDefaultImpl = param1IAONService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAONService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
