package com.aiunit.aon.utils;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.aiunit.aon.utils.core.FaceInfo;

public interface IAONEventListener extends IInterface {
  void onEvent(int paramInt1, int paramInt2) throws RemoteException;
  
  void onEventParam(int paramInt1, int paramInt2, FaceInfo paramFaceInfo) throws RemoteException;
  
  class Default implements IAONEventListener {
    public void onEvent(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onEventParam(int param1Int1, int param1Int2, FaceInfo param1FaceInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAONEventListener {
    private static final String DESCRIPTOR = "com.aiunit.aon.utils.IAONEventListener";
    
    static final int TRANSACTION_onEvent = 1;
    
    static final int TRANSACTION_onEventParam = 2;
    
    public Stub() {
      attachInterface(this, "com.aiunit.aon.utils.IAONEventListener");
    }
    
    public static IAONEventListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.aiunit.aon.utils.IAONEventListener");
      if (iInterface != null && iInterface instanceof IAONEventListener)
        return (IAONEventListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onEventParam";
      } 
      return "onEvent";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("com.aiunit.aon.utils.IAONEventListener");
          return true;
        } 
        param1Parcel1.enforceInterface("com.aiunit.aon.utils.IAONEventListener");
        param1Int1 = param1Parcel1.readInt();
        param1Int2 = param1Parcel1.readInt();
        if (param1Parcel1.readInt() != 0) {
          FaceInfo faceInfo = (FaceInfo)FaceInfo.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onEventParam(param1Int1, param1Int2, (FaceInfo)param1Parcel1);
        param1Parcel2.writeNoException();
        if (param1Parcel1 != null) {
          param1Parcel2.writeInt(1);
          param1Parcel1.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      param1Parcel1.enforceInterface("com.aiunit.aon.utils.IAONEventListener");
      param1Int2 = param1Parcel1.readInt();
      param1Int1 = param1Parcel1.readInt();
      onEvent(param1Int2, param1Int1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IAONEventListener {
      public static IAONEventListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.aiunit.aon.utils.IAONEventListener";
      }
      
      public void onEvent(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.aiunit.aon.utils.IAONEventListener");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IAONEventListener.Stub.getDefaultImpl() != null) {
            IAONEventListener.Stub.getDefaultImpl().onEvent(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onEventParam(int param2Int1, int param2Int2, FaceInfo param2FaceInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.aiunit.aon.utils.IAONEventListener");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2FaceInfo != null) {
            parcel1.writeInt(1);
            param2FaceInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IAONEventListener.Stub.getDefaultImpl() != null) {
            IAONEventListener.Stub.getDefaultImpl().onEventParam(param2Int1, param2Int2, param2FaceInfo);
            return;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0)
            param2FaceInfo.readFromParcel(parcel2); 
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAONEventListener param1IAONEventListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAONEventListener != null) {
          Proxy.sDefaultImpl = param1IAONEventListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAONEventListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
