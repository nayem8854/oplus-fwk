package com.oplus.customize.coreapp.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusCoreAppService extends IInterface {
  IBinder getManager(String paramString) throws RemoteException;
  
  boolean isPackageContainsOplusCertificates(String paramString) throws RemoteException;
  
  void onBootPhase(int paramInt) throws RemoteException;
  
  class Default implements IOplusCoreAppService {
    public IBinder getManager(String param1String) throws RemoteException {
      return null;
    }
    
    public boolean isPackageContainsOplusCertificates(String param1String) throws RemoteException {
      return false;
    }
    
    public void onBootPhase(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusCoreAppService {
    private static final String DESCRIPTOR = "com.oplus.customize.coreapp.aidl.IOplusCoreAppService";
    
    static final int TRANSACTION_getManager = 1;
    
    static final int TRANSACTION_isPackageContainsOplusCertificates = 2;
    
    static final int TRANSACTION_onBootPhase = 3;
    
    public Stub() {
      attachInterface(this, "com.oplus.customize.coreapp.aidl.IOplusCoreAppService");
    }
    
    public static IOplusCoreAppService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.customize.coreapp.aidl.IOplusCoreAppService");
      if (iInterface != null && iInterface instanceof IOplusCoreAppService)
        return (IOplusCoreAppService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onBootPhase";
        } 
        return "isPackageContainsOplusCertificates";
      } 
      return "getManager";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.oplus.customize.coreapp.aidl.IOplusCoreAppService");
            return true;
          } 
          param1Parcel1.enforceInterface("com.oplus.customize.coreapp.aidl.IOplusCoreAppService");
          param1Int1 = param1Parcel1.readInt();
          onBootPhase(param1Int1);
          return true;
        } 
        param1Parcel1.enforceInterface("com.oplus.customize.coreapp.aidl.IOplusCoreAppService");
        str = param1Parcel1.readString();
        boolean bool = isPackageContainsOplusCertificates(str);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool);
        return true;
      } 
      str.enforceInterface("com.oplus.customize.coreapp.aidl.IOplusCoreAppService");
      String str = str.readString();
      IBinder iBinder = getManager(str);
      param1Parcel2.writeNoException();
      param1Parcel2.writeStrongBinder(iBinder);
      return true;
    }
    
    private static class Proxy implements IOplusCoreAppService {
      public static IOplusCoreAppService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.customize.coreapp.aidl.IOplusCoreAppService";
      }
      
      public IBinder getManager(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.customize.coreapp.aidl.IOplusCoreAppService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusCoreAppService.Stub.getDefaultImpl() != null)
            return IOplusCoreAppService.Stub.getDefaultImpl().getManager(param2String); 
          parcel2.readException();
          return parcel2.readStrongBinder();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isPackageContainsOplusCertificates(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.customize.coreapp.aidl.IOplusCoreAppService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IOplusCoreAppService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCoreAppService.Stub.getDefaultImpl().isPackageContainsOplusCertificates(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onBootPhase(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.customize.coreapp.aidl.IOplusCoreAppService");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IOplusCoreAppService.Stub.getDefaultImpl() != null) {
            IOplusCoreAppService.Stub.getDefaultImpl().onBootPhase(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusCoreAppService param1IOplusCoreAppService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusCoreAppService != null) {
          Proxy.sDefaultImpl = param1IOplusCoreAppService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusCoreAppService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
