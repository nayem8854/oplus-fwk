package com.oplus.eventhub.sdk.aidl;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.oplus.deepthinker.sdk.common.utils.SDKLog;

public class DeviceEventResult implements Parcelable {
  public DeviceEventResult(int paramInt1, int paramInt2, int paramInt3, String paramString, Bundle paramBundle) {
    this.mEventType = paramInt1;
    this.mEventStateType = paramInt2;
    this.mPid = paramInt3;
    this.mPkgName = paramString;
    this.mExtraData = paramBundle;
  }
  
  public int getEventType() {
    return this.mEventType;
  }
  
  public int getEventStateType() {
    return this.mEventStateType;
  }
  
  public int getPid() {
    if (this.mPid == -1)
      SDKLog.e("DeviceEventResult", "This event is not supported."); 
    return this.mPid;
  }
  
  public String getPkgName() {
    if (this.mPkgName == null)
      SDKLog.e("DeviceEventResult", "This event is not supported."); 
    return this.mPkgName;
  }
  
  public Bundle getExtraData() {
    if (this.mExtraData == null)
      SDKLog.e("DeviceEventResult", "This event is not supported."); 
    return this.mExtraData;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("DeviceEventResult :");
    stringBuilder.append("\teventType is :");
    stringBuilder.append(this.mEventType);
    stringBuilder.append("\teventStateType is :");
    stringBuilder.append(this.mEventStateType);
    stringBuilder.append("\tpid is : ");
    stringBuilder.append(this.mPid);
    stringBuilder.append("\t\tpackageName is : ");
    stringBuilder.append(this.mPkgName);
    if (this.mExtraData != null) {
      stringBuilder.append("\tExtraData is : ");
      stringBuilder.append(this.mExtraData.toString());
    } 
    return stringBuilder.toString();
  }
  
  public DeviceEventResult(Parcel paramParcel) {
    this.mEventType = paramParcel.readInt();
    this.mEventStateType = paramParcel.readInt();
    this.mPid = paramParcel.readInt();
    this.mPkgName = paramParcel.readString();
    this.mExtraData = paramParcel.readBundle(getClass().getClassLoader());
  }
  
  public static final Parcelable.Creator<DeviceEventResult> CREATOR = new Parcelable.Creator<DeviceEventResult>() {
      public DeviceEventResult createFromParcel(Parcel param1Parcel) {
        return new DeviceEventResult(param1Parcel);
      }
      
      public DeviceEventResult[] newArray(int param1Int) {
        return new DeviceEventResult[param1Int];
      }
    };
  
  private static final String TAG = "DeviceEventResult";
  
  private int mEventStateType;
  
  private int mEventType;
  
  private Bundle mExtraData;
  
  private int mPid;
  
  private String mPkgName;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mEventType);
    paramParcel.writeInt(this.mEventStateType);
    paramParcel.writeInt(this.mPid);
    paramParcel.writeString(this.mPkgName);
    paramParcel.writeBundle(this.mExtraData);
  }
}
