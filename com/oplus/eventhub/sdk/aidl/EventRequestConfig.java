package com.oplus.eventhub.sdk.aidl;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.ArraySet;
import java.util.HashSet;

public class EventRequestConfig implements Parcelable {
  public EventRequestConfig(ArraySet<DeviceEvent> paramArraySet) {
    this.mDeviceEventSet = new ArraySet();
    if (paramArraySet != null && !paramArraySet.isEmpty())
      this.mDeviceEventSet.addAll(paramArraySet); 
  }
  
  public ArraySet<DeviceEvent> getDeviceEventSet() {
    if (this.mDeviceEventSet == null)
      this.mDeviceEventSet = new ArraySet(); 
    return this.mDeviceEventSet;
  }
  
  public HashSet<Integer> getAllEvents() {
    HashSet<Integer> hashSet = new HashSet();
    ArraySet<DeviceEvent> arraySet = this.mDeviceEventSet;
    if (arraySet != null && !arraySet.isEmpty())
      for (DeviceEvent deviceEvent : this.mDeviceEventSet)
        hashSet.add(Integer.valueOf(deviceEvent.getEventType()));  
    return hashSet;
  }
  
  public EventRequestConfig(Parcel paramParcel) {
    ClassLoader classLoader = EventRequestConfig.class.getClassLoader();
    this.mDeviceEventSet = paramParcel.readArraySet(classLoader);
  }
  
  public static final Parcelable.Creator<EventRequestConfig> CREATOR = new Parcelable.Creator<EventRequestConfig>() {
      public EventRequestConfig createFromParcel(Parcel param1Parcel) {
        return new EventRequestConfig(param1Parcel);
      }
      
      public EventRequestConfig[] newArray(int param1Int) {
        return new EventRequestConfig[param1Int];
      }
    };
  
  private ArraySet<DeviceEvent> mDeviceEventSet;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeArraySet(this.mDeviceEventSet);
  }
}
