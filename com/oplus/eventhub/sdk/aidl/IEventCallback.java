package com.oplus.eventhub.sdk.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IEventCallback extends IInterface {
  void onEventStateChanged(DeviceEventResult paramDeviceEventResult) throws RemoteException;
  
  class Default implements IEventCallback {
    public void onEventStateChanged(DeviceEventResult param1DeviceEventResult) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IEventCallback {
    private static final String DESCRIPTOR = "com.oplus.eventhub.sdk.aidl.IEventCallback";
    
    static final int TRANSACTION_onEventStateChanged = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.eventhub.sdk.aidl.IEventCallback");
    }
    
    public static IEventCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.eventhub.sdk.aidl.IEventCallback");
      if (iInterface != null && iInterface instanceof IEventCallback)
        return (IEventCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onEventStateChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.eventhub.sdk.aidl.IEventCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.eventhub.sdk.aidl.IEventCallback");
      if (param1Parcel1.readInt() != 0) {
        DeviceEventResult deviceEventResult = (DeviceEventResult)DeviceEventResult.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onEventStateChanged((DeviceEventResult)param1Parcel1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IEventCallback {
      public static IEventCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.eventhub.sdk.aidl.IEventCallback";
      }
      
      public void onEventStateChanged(DeviceEventResult param2DeviceEventResult) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.eventhub.sdk.aidl.IEventCallback");
          if (param2DeviceEventResult != null) {
            parcel1.writeInt(1);
            param2DeviceEventResult.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IEventCallback.Stub.getDefaultImpl() != null) {
            IEventCallback.Stub.getDefaultImpl().onEventStateChanged(param2DeviceEventResult);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IEventCallback param1IEventCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IEventCallback != null) {
          Proxy.sDefaultImpl = param1IEventCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IEventCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
