package com.oplus.eventhub.sdk.aidl;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IEventHandleService extends IInterface {
  Bundle call(Bundle paramBundle) throws RemoteException;
  
  List<Event> getAvailableEvent() throws RemoteException;
  
  boolean isAvailableEvent(Event paramEvent) throws RemoteException;
  
  boolean registerCallback(IEventCallback paramIEventCallback, int paramInt, EventRequestConfig paramEventRequestConfig) throws RemoteException;
  
  int registerEventCallback(String paramString, IEventCallback paramIEventCallback, EventConfig paramEventConfig) throws RemoteException;
  
  void triggerHookEvent(TriggerEvent paramTriggerEvent) throws RemoteException;
  
  boolean unregisterCallback(int paramInt) throws RemoteException;
  
  int unregisterEventCallback(String paramString) throws RemoteException;
  
  class Default implements IEventHandleService {
    public void triggerHookEvent(TriggerEvent param1TriggerEvent) throws RemoteException {}
    
    public boolean registerCallback(IEventCallback param1IEventCallback, int param1Int, EventRequestConfig param1EventRequestConfig) throws RemoteException {
      return false;
    }
    
    public boolean unregisterCallback(int param1Int) throws RemoteException {
      return false;
    }
    
    public int registerEventCallback(String param1String, IEventCallback param1IEventCallback, EventConfig param1EventConfig) throws RemoteException {
      return 0;
    }
    
    public int unregisterEventCallback(String param1String) throws RemoteException {
      return 0;
    }
    
    public List<Event> getAvailableEvent() throws RemoteException {
      return null;
    }
    
    public boolean isAvailableEvent(Event param1Event) throws RemoteException {
      return false;
    }
    
    public Bundle call(Bundle param1Bundle) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IEventHandleService {
    private static final String DESCRIPTOR = "com.oplus.eventhub.sdk.aidl.IEventHandleService";
    
    static final int TRANSACTION_call = 8;
    
    static final int TRANSACTION_getAvailableEvent = 6;
    
    static final int TRANSACTION_isAvailableEvent = 7;
    
    static final int TRANSACTION_registerCallback = 2;
    
    static final int TRANSACTION_registerEventCallback = 4;
    
    static final int TRANSACTION_triggerHookEvent = 1;
    
    static final int TRANSACTION_unregisterCallback = 3;
    
    static final int TRANSACTION_unregisterEventCallback = 5;
    
    public Stub() {
      attachInterface(this, "com.oplus.eventhub.sdk.aidl.IEventHandleService");
    }
    
    public static IEventHandleService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.eventhub.sdk.aidl.IEventHandleService");
      if (iInterface != null && iInterface instanceof IEventHandleService)
        return (IEventHandleService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "call";
        case 7:
          return "isAvailableEvent";
        case 6:
          return "getAvailableEvent";
        case 5:
          return "unregisterEventCallback";
        case 4:
          return "registerEventCallback";
        case 3:
          return "unregisterCallback";
        case 2:
          return "registerCallback";
        case 1:
          break;
      } 
      return "triggerHookEvent";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool3;
        int j;
        boolean bool2;
        int i;
        boolean bool1;
        Bundle bundle;
        List<Event> list;
        String str1, str2;
        IEventCallback iEventCallback;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("com.oplus.eventhub.sdk.aidl.IEventHandleService");
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            bundle = call((Bundle)param1Parcel1);
            param1Parcel2.writeNoException();
            if (bundle != null) {
              param1Parcel2.writeInt(1);
              bundle.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 7:
            bundle.enforceInterface("com.oplus.eventhub.sdk.aidl.IEventHandleService");
            if (bundle.readInt() != 0) {
              Event event = (Event)Event.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              bundle = null;
            } 
            bool3 = isAvailableEvent((Event)bundle);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 6:
            bundle.enforceInterface("com.oplus.eventhub.sdk.aidl.IEventHandleService");
            list = getAvailableEvent();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 5:
            list.enforceInterface("com.oplus.eventhub.sdk.aidl.IEventHandleService");
            str1 = list.readString();
            j = unregisterEventCallback(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 4:
            str1.enforceInterface("com.oplus.eventhub.sdk.aidl.IEventHandleService");
            str2 = str1.readString();
            iEventCallback = IEventCallback.Stub.asInterface(str1.readStrongBinder());
            if (str1.readInt() != 0) {
              EventConfig eventConfig = (EventConfig)EventConfig.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            j = registerEventCallback(str2, iEventCallback, (EventConfig)str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 3:
            str1.enforceInterface("com.oplus.eventhub.sdk.aidl.IEventHandleService");
            j = str1.readInt();
            bool2 = unregisterCallback(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 2:
            str1.enforceInterface("com.oplus.eventhub.sdk.aidl.IEventHandleService");
            iEventCallback = IEventCallback.Stub.asInterface(str1.readStrongBinder());
            i = str1.readInt();
            if (str1.readInt() != 0) {
              EventRequestConfig eventRequestConfig = (EventRequestConfig)EventRequestConfig.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            bool1 = registerCallback(iEventCallback, i, (EventRequestConfig)str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("com.oplus.eventhub.sdk.aidl.IEventHandleService");
        if (str1.readInt() != 0) {
          TriggerEvent triggerEvent = (TriggerEvent)TriggerEvent.CREATOR.createFromParcel((Parcel)str1);
        } else {
          str1 = null;
        } 
        triggerHookEvent((TriggerEvent)str1);
        return true;
      } 
      param1Parcel2.writeString("com.oplus.eventhub.sdk.aidl.IEventHandleService");
      return true;
    }
    
    private static class Proxy implements IEventHandleService {
      public static IEventHandleService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.eventhub.sdk.aidl.IEventHandleService";
      }
      
      public void triggerHookEvent(TriggerEvent param2TriggerEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.eventhub.sdk.aidl.IEventHandleService");
          if (param2TriggerEvent != null) {
            parcel.writeInt(1);
            param2TriggerEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IEventHandleService.Stub.getDefaultImpl() != null) {
            IEventHandleService.Stub.getDefaultImpl().triggerHookEvent(param2TriggerEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean registerCallback(IEventCallback param2IEventCallback, int param2Int, EventRequestConfig param2EventRequestConfig) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oplus.eventhub.sdk.aidl.IEventHandleService");
          if (param2IEventCallback != null) {
            iBinder = param2IEventCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2EventRequestConfig != null) {
            parcel1.writeInt(1);
            param2EventRequestConfig.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IEventHandleService.Stub.getDefaultImpl() != null) {
            bool1 = IEventHandleService.Stub.getDefaultImpl().registerCallback(param2IEventCallback, param2Int, param2EventRequestConfig);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean unregisterCallback(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.eventhub.sdk.aidl.IEventHandleService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IEventHandleService.Stub.getDefaultImpl() != null) {
            bool1 = IEventHandleService.Stub.getDefaultImpl().unregisterCallback(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int registerEventCallback(String param2String, IEventCallback param2IEventCallback, EventConfig param2EventConfig) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oplus.eventhub.sdk.aidl.IEventHandleService");
          parcel1.writeString(param2String);
          if (param2IEventCallback != null) {
            iBinder = param2IEventCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2EventConfig != null) {
            parcel1.writeInt(1);
            param2EventConfig.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IEventHandleService.Stub.getDefaultImpl() != null)
            return IEventHandleService.Stub.getDefaultImpl().registerEventCallback(param2String, param2IEventCallback, param2EventConfig); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int unregisterEventCallback(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.eventhub.sdk.aidl.IEventHandleService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IEventHandleService.Stub.getDefaultImpl() != null)
            return IEventHandleService.Stub.getDefaultImpl().unregisterEventCallback(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<Event> getAvailableEvent() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.eventhub.sdk.aidl.IEventHandleService");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IEventHandleService.Stub.getDefaultImpl() != null)
            return IEventHandleService.Stub.getDefaultImpl().getAvailableEvent(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(Event.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAvailableEvent(Event param2Event) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.eventhub.sdk.aidl.IEventHandleService");
          boolean bool1 = true;
          if (param2Event != null) {
            parcel1.writeInt(1);
            param2Event.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IEventHandleService.Stub.getDefaultImpl() != null) {
            bool1 = IEventHandleService.Stub.getDefaultImpl().isAvailableEvent(param2Event);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle call(Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.eventhub.sdk.aidl.IEventHandleService");
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IEventHandleService.Stub.getDefaultImpl() != null) {
            param2Bundle = IEventHandleService.Stub.getDefaultImpl().call(param2Bundle);
            return param2Bundle;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            param2Bundle = (Bundle)Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            param2Bundle = null;
          } 
          return param2Bundle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IEventHandleService param1IEventHandleService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IEventHandleService != null) {
          Proxy.sDefaultImpl = param1IEventHandleService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IEventHandleService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
