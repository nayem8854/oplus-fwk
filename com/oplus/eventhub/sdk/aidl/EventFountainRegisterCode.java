package com.oplus.eventhub.sdk.aidl;

public class EventFountainRegisterCode {
  public static final int BINDER_TRANSACTION_ERROR = 128;
  
  public static final int EVENT_NOT_AVAILABLE = 2;
  
  public static final int INVALID_PARAMETERS = 16;
  
  public static final int OPLUS_VERSION_NOT_SUPPORT = 8;
  
  public static final int PID_REGISTER_LIMITED = 4;
  
  public static final int REGISTER_SUCCESS = 1;
  
  public static final int SERVER_INTERVAL_ERROR = 32;
  
  public static final int UNSUPPORTED_PARAMETER = 64;
  
  public static String resultCodeToString(int paramInt) {
    String str;
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 4) {
          if (paramInt != 8) {
            if (paramInt != 16) {
              if (paramInt != 32) {
                if (paramInt != 64) {
                  if (paramInt != 128) {
                    str = "UNKNOWN_RESULT_CODE";
                  } else {
                    str = "BINDER_TRANSACTION_ERROR";
                  } 
                } else {
                  str = "UNSUPPORTED_PARAMETER";
                } 
              } else {
                str = "SERVER_INTERVAL_ERROR";
              } 
            } else {
              str = "INVALID_PARAMETERS";
            } 
          } else {
            str = "OPLUSOS_VERSION_NOT_SUPPORT";
          } 
        } else {
          str = "PID_REGISTER_LIMITED";
        } 
      } else {
        str = "EVENT_NOT_AVAILABLE";
      } 
    } else {
      str = "REGISTER_SUCCESS";
    } 
    return str;
  }
}
