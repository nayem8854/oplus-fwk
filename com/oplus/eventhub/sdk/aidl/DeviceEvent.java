package com.oplus.eventhub.sdk.aidl;

import android.os.Parcel;
import android.os.Parcelable;
import com.oplus.deepthinker.sdk.common.utils.SDKLog;
import java.util.Arrays;

public class DeviceEvent implements Parcelable {
  class Builder {
    private int mEventType = -1;
    
    private int mEventStateType = -1;
    
    public Builder setEventType(int param1Int) {
      if (!EventType.sEventTypes.contains(Integer.valueOf(param1Int)))
        SDKLog.e("DeviceEvent", "Invalid event type, not yet supported."); 
      this.mEventType = param1Int;
      return this;
    }
    
    public Builder setEventStateType(int param1Int) {
      if (param1Int != 0 && 
        param1Int != 1)
        SDKLog.e("DeviceEvent", "Invalid stateType."); 
      this.mEventStateType = param1Int;
      return this;
    }
    
    public DeviceEvent build() {
      if (this.mEventType == -1) {
        SDKLog.e("DeviceEvent", "EventType not yet configured.");
      } else if (this.mEventStateType == -1) {
        SDKLog.w("DeviceEvent", "use default state type.");
        this.mEventStateType = 0;
      } 
      return new DeviceEvent(this.mEventType, this.mEventStateType);
    }
  }
  
  private DeviceEvent(int paramInt1, int paramInt2) {
    this.mEventType = paramInt1;
    this.mEventStateType = paramInt2;
  }
  
  public int getEventType() {
    return this.mEventType;
  }
  
  public int getEventStateType() {
    return this.mEventStateType;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    if (paramObject == null)
      return false; 
    if (this == paramObject)
      return true; 
    if (paramObject instanceof DeviceEvent) {
      paramObject = paramObject;
      if (this.mEventType == paramObject.getEventType()) {
        int i = this.mEventStateType;
        if (i == paramObject.getEventStateType())
          bool = true; 
      } 
      return bool;
    } 
    return false;
  }
  
  public int hashCode() {
    return Arrays.hashCode(new Object[] { Integer.valueOf(this.mEventType), Integer.valueOf(this.mEventStateType) });
  }
  
  public DeviceEvent(Parcel paramParcel) {
    this.mEventType = paramParcel.readInt();
    this.mEventStateType = paramParcel.readInt();
  }
  
  public static final Parcelable.Creator<DeviceEvent> CREATOR = new Parcelable.Creator<DeviceEvent>() {
      public DeviceEvent createFromParcel(Parcel param1Parcel) {
        return new DeviceEvent(param1Parcel);
      }
      
      public DeviceEvent[] newArray(int param1Int) {
        return new DeviceEvent[param1Int];
      }
    };
  
  private static final String TAG = "DeviceEvent";
  
  private int mEventStateType;
  
  private int mEventType;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mEventType);
    paramParcel.writeInt(this.mEventStateType);
  }
}
