package com.oplus.eventhub.sdk.aidl;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

public class Event implements Parcelable {
  public Event(int paramInt, Bundle paramBundle) {
    this.mEventType = paramInt;
    this.mExtra = paramBundle;
  }
  
  public int getEventType() {
    return this.mEventType;
  }
  
  public Bundle getExtra() {
    return this.mExtra;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    if (paramObject == null)
      return false; 
    if (this == paramObject)
      return true; 
    if (paramObject instanceof Event) {
      paramObject = paramObject;
      if (this.mEventType == paramObject.getEventType())
        bool = true; 
      return bool;
    } 
    return false;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Event{mEventType=");
    stringBuilder.append(this.mEventType);
    stringBuilder.append(", mExtra=");
    Bundle bundle1 = this.mExtra, bundle2 = bundle1;
    if (bundle1 == null)
      null = "null"; 
    stringBuilder.append(null);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public int hashCode() {
    return Arrays.hashCode(new Object[] { Integer.valueOf(this.mEventType) });
  }
  
  public Event(Parcel paramParcel) {
    this.mEventType = paramParcel.readInt();
    this.mExtra = paramParcel.readBundle(getClass().getClassLoader());
  }
  
  public static final Parcelable.Creator<Event> CREATOR = new Parcelable.Creator<Event>() {
      public Event createFromParcel(Parcel param1Parcel) {
        return new Event(param1Parcel);
      }
      
      public Event[] newArray(int param1Int) {
        return new Event[param1Int];
      }
    };
  
  private static final String TAG = "Event";
  
  private int mEventType;
  
  private Bundle mExtra;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mEventType);
    paramParcel.writeBundle(this.mExtra);
  }
}
