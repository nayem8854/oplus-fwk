package com.oplus.eventhub.sdk.aidl;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class EventConfig implements Parcelable {
  public EventConfig(HashSet<Event> paramHashSet) {
    this.mEventSet = new HashSet<>();
    if (paramHashSet != null && !paramHashSet.isEmpty())
      this.mEventSet.addAll(paramHashSet); 
  }
  
  public Set<Event> getEventSet() {
    if (this.mEventSet == null)
      this.mEventSet = new HashSet<>(); 
    return this.mEventSet;
  }
  
  public Set<Integer> getAllEvents() {
    HashSet<Integer> hashSet = new HashSet();
    Set<Event> set = this.mEventSet;
    if (set != null && !set.isEmpty())
      for (Event event : this.mEventSet)
        hashSet.add(Integer.valueOf(event.getEventType()));  
    return hashSet;
  }
  
  public void addEvent(Event paramEvent) {
    if (this.mEventSet == null)
      this.mEventSet = new HashSet<>(); 
    this.mEventSet.add(paramEvent);
  }
  
  public String toString() {
    Set<Event> set1 = this.mEventSet;
    if (set1 == null || set1.isEmpty())
      return "EventConfig empty."; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("EventConfig{mEventSet=");
    Set<Event> set2 = this.mEventSet;
    stringBuilder.append(Arrays.toString(set2.toArray()));
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public EventConfig(Parcel paramParcel) {
    ClassLoader classLoader = EventConfig.class.getClassLoader();
    ArrayList<? extends Event> arrayList = new ArrayList();
    paramParcel.readList(arrayList, classLoader);
    this.mEventSet = new HashSet<>();
    if (!arrayList.isEmpty())
      this.mEventSet.addAll(arrayList); 
  }
  
  public static final Parcelable.Creator<EventConfig> CREATOR = new Parcelable.Creator<EventConfig>() {
      public EventConfig createFromParcel(Parcel param1Parcel) {
        return new EventConfig(param1Parcel);
      }
      
      public EventConfig[] newArray(int param1Int) {
        return new EventConfig[param1Int];
      }
    };
  
  private Set<Event> mEventSet;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (this.mEventSet != null)
      paramParcel.writeList(new ArrayList<>(this.mEventSet)); 
  }
}
