package com.oplus.eventhub.sdk;

import com.oplus.eventhub.sdk.aidl.DeviceEventResult;
import com.oplus.eventhub.sdk.aidl.IEventCallback;

public abstract class EventCallback extends IEventCallback.Stub {
  public abstract void onEventStateChanged(DeviceEventResult paramDeviceEventResult);
}
