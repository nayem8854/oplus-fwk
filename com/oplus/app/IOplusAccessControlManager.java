package com.oplus.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface IOplusAccessControlManager extends IInterface {
  void addEncryptPass(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  Map getAccessControlAppsInfo(String paramString, int paramInt) throws RemoteException;
  
  boolean getAccessControlEnabled(String paramString, int paramInt) throws RemoteException;
  
  boolean isEncryptPass(String paramString, int paramInt) throws RemoteException;
  
  boolean isEncryptedPackage(String paramString, int paramInt) throws RemoteException;
  
  boolean registerAccessControlObserver(String paramString, IOplusAccessControlObserver paramIOplusAccessControlObserver) throws RemoteException;
  
  void setAccessControlAppsInfo(String paramString, Map paramMap, int paramInt) throws RemoteException;
  
  void setAccessControlEnabled(String paramString, boolean paramBoolean, int paramInt) throws RemoteException;
  
  boolean unregisterAccessControlObserver(String paramString, IOplusAccessControlObserver paramIOplusAccessControlObserver) throws RemoteException;
  
  void updateRusList(int paramInt, List<String> paramList1, List<String> paramList2) throws RemoteException;
  
  class Default implements IOplusAccessControlManager {
    public void setAccessControlAppsInfo(String param1String, Map param1Map, int param1Int) throws RemoteException {}
    
    public Map getAccessControlAppsInfo(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public void setAccessControlEnabled(String param1String, boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public boolean getAccessControlEnabled(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public void addEncryptPass(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public boolean isEncryptPass(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isEncryptedPackage(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean registerAccessControlObserver(String param1String, IOplusAccessControlObserver param1IOplusAccessControlObserver) throws RemoteException {
      return false;
    }
    
    public boolean unregisterAccessControlObserver(String param1String, IOplusAccessControlObserver param1IOplusAccessControlObserver) throws RemoteException {
      return false;
    }
    
    public void updateRusList(int param1Int, List<String> param1List1, List<String> param1List2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusAccessControlManager {
    private static final String DESCRIPTOR = "com.oplus.app.IOplusAccessControlManager";
    
    static final int TRANSACTION_addEncryptPass = 5;
    
    static final int TRANSACTION_getAccessControlAppsInfo = 2;
    
    static final int TRANSACTION_getAccessControlEnabled = 4;
    
    static final int TRANSACTION_isEncryptPass = 6;
    
    static final int TRANSACTION_isEncryptedPackage = 7;
    
    static final int TRANSACTION_registerAccessControlObserver = 8;
    
    static final int TRANSACTION_setAccessControlAppsInfo = 1;
    
    static final int TRANSACTION_setAccessControlEnabled = 3;
    
    static final int TRANSACTION_unregisterAccessControlObserver = 9;
    
    static final int TRANSACTION_updateRusList = 10;
    
    public Stub() {
      attachInterface(this, "com.oplus.app.IOplusAccessControlManager");
    }
    
    public static IOplusAccessControlManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.app.IOplusAccessControlManager");
      if (iInterface != null && iInterface instanceof IOplusAccessControlManager)
        return (IOplusAccessControlManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 10:
          return "updateRusList";
        case 9:
          return "unregisterAccessControlObserver";
        case 8:
          return "registerAccessControlObserver";
        case 7:
          return "isEncryptedPackage";
        case 6:
          return "isEncryptPass";
        case 5:
          return "addEncryptPass";
        case 4:
          return "getAccessControlEnabled";
        case 3:
          return "setAccessControlEnabled";
        case 2:
          return "getAccessControlAppsInfo";
        case 1:
          break;
      } 
      return "setAccessControlAppsInfo";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        ArrayList<String> arrayList1;
        IOplusAccessControlObserver iOplusAccessControlObserver;
        Map map;
        ArrayList<String> arrayList2;
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 10:
            param1Parcel1.enforceInterface("com.oplus.app.IOplusAccessControlManager");
            param1Int1 = param1Parcel1.readInt();
            arrayList2 = param1Parcel1.createStringArrayList();
            arrayList1 = param1Parcel1.createStringArrayList();
            updateRusList(param1Int1, arrayList2, arrayList1);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            arrayList1.enforceInterface("com.oplus.app.IOplusAccessControlManager");
            str = arrayList1.readString();
            iOplusAccessControlObserver = IOplusAccessControlObserver.Stub.asInterface(arrayList1.readStrongBinder());
            bool4 = unregisterAccessControlObserver(str, iOplusAccessControlObserver);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 8:
            iOplusAccessControlObserver.enforceInterface("com.oplus.app.IOplusAccessControlManager");
            str = iOplusAccessControlObserver.readString();
            iOplusAccessControlObserver = IOplusAccessControlObserver.Stub.asInterface(iOplusAccessControlObserver.readStrongBinder());
            bool4 = registerAccessControlObserver(str, iOplusAccessControlObserver);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 7:
            iOplusAccessControlObserver.enforceInterface("com.oplus.app.IOplusAccessControlManager");
            str = iOplusAccessControlObserver.readString();
            m = iOplusAccessControlObserver.readInt();
            bool3 = isEncryptedPackage(str, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 6:
            iOplusAccessControlObserver.enforceInterface("com.oplus.app.IOplusAccessControlManager");
            str = iOplusAccessControlObserver.readString();
            k = iOplusAccessControlObserver.readInt();
            bool2 = isEncryptPass(str, k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 5:
            iOplusAccessControlObserver.enforceInterface("com.oplus.app.IOplusAccessControlManager");
            str = iOplusAccessControlObserver.readString();
            j = iOplusAccessControlObserver.readInt();
            param1Int2 = iOplusAccessControlObserver.readInt();
            addEncryptPass(str, j, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            iOplusAccessControlObserver.enforceInterface("com.oplus.app.IOplusAccessControlManager");
            str = iOplusAccessControlObserver.readString();
            j = iOplusAccessControlObserver.readInt();
            bool1 = getAccessControlEnabled(str, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 3:
            iOplusAccessControlObserver.enforceInterface("com.oplus.app.IOplusAccessControlManager");
            str = iOplusAccessControlObserver.readString();
            if (iOplusAccessControlObserver.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            i = iOplusAccessControlObserver.readInt();
            setAccessControlEnabled(str, bool, i);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            iOplusAccessControlObserver.enforceInterface("com.oplus.app.IOplusAccessControlManager");
            str = iOplusAccessControlObserver.readString();
            i = iOplusAccessControlObserver.readInt();
            map = getAccessControlAppsInfo(str, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeMap(map);
            return true;
          case 1:
            break;
        } 
        map.enforceInterface("com.oplus.app.IOplusAccessControlManager");
        String str = map.readString();
        ClassLoader classLoader = getClass().getClassLoader();
        HashMap hashMap = map.readHashMap(classLoader);
        int i = map.readInt();
        setAccessControlAppsInfo(str, hashMap, i);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("com.oplus.app.IOplusAccessControlManager");
      return true;
    }
    
    private static class Proxy implements IOplusAccessControlManager {
      public static IOplusAccessControlManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.app.IOplusAccessControlManager";
      }
      
      public void setAccessControlAppsInfo(String param2String, Map param2Map, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.app.IOplusAccessControlManager");
          parcel1.writeString(param2String);
          parcel1.writeMap(param2Map);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusAccessControlManager.Stub.getDefaultImpl() != null) {
            IOplusAccessControlManager.Stub.getDefaultImpl().setAccessControlAppsInfo(param2String, param2Map, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Map getAccessControlAppsInfo(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.app.IOplusAccessControlManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusAccessControlManager.Stub.getDefaultImpl() != null)
            return IOplusAccessControlManager.Stub.getDefaultImpl().getAccessControlAppsInfo(param2String, param2Int); 
          parcel2.readException();
          ClassLoader classLoader = getClass().getClassLoader();
          return parcel2.readHashMap(classLoader);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAccessControlEnabled(String param2String, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.oplus.app.IOplusAccessControlManager");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool1 && IOplusAccessControlManager.Stub.getDefaultImpl() != null) {
            IOplusAccessControlManager.Stub.getDefaultImpl().setAccessControlEnabled(param2String, param2Boolean, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getAccessControlEnabled(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.app.IOplusAccessControlManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IOplusAccessControlManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusAccessControlManager.Stub.getDefaultImpl().getAccessControlEnabled(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addEncryptPass(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.app.IOplusAccessControlManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOplusAccessControlManager.Stub.getDefaultImpl() != null) {
            IOplusAccessControlManager.Stub.getDefaultImpl().addEncryptPass(param2String, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isEncryptPass(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.app.IOplusAccessControlManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IOplusAccessControlManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusAccessControlManager.Stub.getDefaultImpl().isEncryptPass(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isEncryptedPackage(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.app.IOplusAccessControlManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IOplusAccessControlManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusAccessControlManager.Stub.getDefaultImpl().isEncryptedPackage(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean registerAccessControlObserver(String param2String, IOplusAccessControlObserver param2IOplusAccessControlObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.app.IOplusAccessControlManager");
          parcel1.writeString(param2String);
          if (param2IOplusAccessControlObserver != null) {
            iBinder = param2IOplusAccessControlObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(8, parcel1, parcel2, 0);
          if (!bool2 && IOplusAccessControlManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusAccessControlManager.Stub.getDefaultImpl().registerAccessControlObserver(param2String, param2IOplusAccessControlObserver);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean unregisterAccessControlObserver(String param2String, IOplusAccessControlObserver param2IOplusAccessControlObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.app.IOplusAccessControlManager");
          parcel1.writeString(param2String);
          if (param2IOplusAccessControlObserver != null) {
            iBinder = param2IOplusAccessControlObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IOplusAccessControlManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusAccessControlManager.Stub.getDefaultImpl().unregisterAccessControlObserver(param2String, param2IOplusAccessControlObserver);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateRusList(int param2Int, List<String> param2List1, List<String> param2List2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.app.IOplusAccessControlManager");
          parcel1.writeInt(param2Int);
          parcel1.writeStringList(param2List1);
          parcel1.writeStringList(param2List2);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IOplusAccessControlManager.Stub.getDefaultImpl() != null) {
            IOplusAccessControlManager.Stub.getDefaultImpl().updateRusList(param2Int, param2List1, param2List2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusAccessControlManager param1IOplusAccessControlManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusAccessControlManager != null) {
          Proxy.sDefaultImpl = param1IOplusAccessControlManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusAccessControlManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
