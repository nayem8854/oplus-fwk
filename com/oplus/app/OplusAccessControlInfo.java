package com.oplus.app;

import android.os.Parcel;
import android.os.Parcelable;

public final class OplusAccessControlInfo implements Parcelable {
  public OplusAccessControlInfo() {}
  
  public OplusAccessControlInfo(Parcel paramParcel) {
    boolean bool2;
    this.mName = paramParcel.readString();
    this.userId = paramParcel.readInt();
    byte b = paramParcel.readByte();
    boolean bool1 = true;
    if (b != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.isEncrypted = bool2;
    if (paramParcel.readByte() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.isHideIcon = bool2;
    if (paramParcel.readByte() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.isHideInRecent = bool2;
    if (paramParcel.readByte() != 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.isHideNotice = bool2;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mName);
    paramParcel.writeInt(this.userId);
    paramParcel.writeByte((byte)this.isEncrypted);
    paramParcel.writeByte((byte)this.isHideIcon);
    paramParcel.writeByte((byte)this.isHideInRecent);
    paramParcel.writeByte((byte)this.isHideNotice);
  }
  
  public static final Parcelable.Creator<OplusAccessControlInfo> CREATOR = new Parcelable.Creator<OplusAccessControlInfo>() {
      public OplusAccessControlInfo createFromParcel(Parcel param1Parcel) {
        return new OplusAccessControlInfo(param1Parcel);
      }
      
      public OplusAccessControlInfo[] newArray(int param1Int) {
        return new OplusAccessControlInfo[param1Int];
      }
    };
  
  public boolean isEncrypted;
  
  public boolean isHideIcon;
  
  public boolean isHideInRecent;
  
  public boolean isHideNotice;
  
  public String mName;
  
  public int userId;
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("OplusAccessControlInfo = { ");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" mName = ");
    stringBuilder2.append(this.mName);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" userId = ");
    stringBuilder2.append(this.userId);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" isEncrypted = ");
    stringBuilder2.append(this.isEncrypted);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" isHideIcon = ");
    stringBuilder2.append(this.isHideIcon);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" isHideInRecent = ");
    stringBuilder2.append(this.isHideInRecent);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" isHideNotice = ");
    stringBuilder2.append(this.isHideNotice);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append("}");
    return stringBuilder1.toString();
  }
}
