package com.oplus.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import java.util.HashSet;
import java.util.List;

public final class OplusAppSwitchConfig implements Parcelable {
  private SparseArray<List<String>> mConfigs = new SparseArray();
  
  public HashSet<String> mActivitySet = new HashSet<>();
  
  public HashSet<String> mPackageSet = new HashSet<>();
  
  public OplusAppSwitchConfig(Parcel paramParcel) {
    SparseArray<List<String>> sparseArray = paramParcel.readSparseArray(null);
    if (sparseArray == null)
      this.mConfigs = new SparseArray(); 
    initSearchSet(1);
    initSearchSet(2);
    this.observerFingerPrint = paramParcel.readInt();
  }
  
  private void initSearchSet(int paramInt) {
    List<? extends String> list = (List)this.mConfigs.get(paramInt);
    if (paramInt != 1) {
      if (paramInt == 2) {
        this.mPackageSet.clear();
        if (list != null && list.size() != 0)
          this.mPackageSet.addAll(list); 
      } 
    } else {
      this.mActivitySet.clear();
      if (list != null && list.size() != 0)
        this.mActivitySet.addAll(list); 
    } 
  }
  
  public List<String> getConfigs(int paramInt) {
    return (List<String>)this.mConfigs.get(paramInt);
  }
  
  public void addAppConfig(int paramInt, List<String> paramList) {
    this.mConfigs.put(paramInt, paramList);
    initSearchSet(paramInt);
  }
  
  public void removeAppConfig(int paramInt) {
    this.mConfigs.remove(paramInt);
    initSearchSet(paramInt);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeSparseArray(this.mConfigs);
    paramParcel.writeInt(this.observerFingerPrint);
  }
  
  public static final Parcelable.Creator<OplusAppSwitchConfig> CREATOR = new Parcelable.Creator<OplusAppSwitchConfig>() {
      public OplusAppSwitchConfig createFromParcel(Parcel param1Parcel) {
        return new OplusAppSwitchConfig(param1Parcel);
      }
      
      public OplusAppSwitchConfig[] newArray(int param1Int) {
        return new OplusAppSwitchConfig[param1Int];
      }
    };
  
  public static final int TYPE_ACTIVITY = 1;
  
  public static final int TYPE_PACKAGE = 2;
  
  public int observerFingerPrint;
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder(128);
    stringBuilder1.append("OplusAppSwitchConfig = { ");
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append("");
    stringBuilder3.append(this.mConfigs);
    String str1 = stringBuilder3.toString();
    str1 = str1.replace(".", "@@");
    str1 = str1.replace("com", "TOM");
    str1 = str1.replace("coloros", "CO");
    str1 = str1.replace("nearme", "NM");
    String str2 = str1.replace("oppo", "OP");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" mConfigs = ");
    stringBuilder2.append(str2);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" observerFingerPrint = ");
    stringBuilder2.append(this.observerFingerPrint);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append("}");
    return stringBuilder1.toString();
  }
  
  public OplusAppSwitchConfig() {}
}
