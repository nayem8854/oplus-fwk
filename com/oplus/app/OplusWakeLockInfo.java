package com.oplus.app;

import android.os.Parcel;
import android.os.Parcelable;

public class OplusWakeLockInfo implements Parcelable {
  public OplusWakeLockInfo() {}
  
  public OplusWakeLockInfo(Parcel paramParcel) {
    this.binderhash = paramParcel.readInt();
    this.flags = paramParcel.readInt();
    this.tag = paramParcel.readString();
    this.duration = paramParcel.readLong();
    this.starttime = paramParcel.readLong();
    this.packageName = paramParcel.readString();
    this.uid = paramParcel.readInt();
    this.pid = paramParcel.readInt();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.binderhash);
    paramParcel.writeInt(this.flags);
    paramParcel.writeString(this.tag);
    paramParcel.writeLong(this.duration);
    paramParcel.writeLong(this.starttime);
    paramParcel.writeString(this.packageName);
    paramParcel.writeInt(this.uid);
    paramParcel.writeInt(this.pid);
  }
  
  public static final Parcelable.Creator<OplusWakeLockInfo> CREATOR = new Parcelable.Creator<OplusWakeLockInfo>() {
      public OplusWakeLockInfo createFromParcel(Parcel param1Parcel) {
        return new OplusWakeLockInfo(param1Parcel);
      }
      
      public OplusWakeLockInfo[] newArray(int param1Int) {
        return new OplusWakeLockInfo[param1Int];
      }
    };
  
  public int binderhash;
  
  public long duration;
  
  public int flags;
  
  public String packageName;
  
  public int pid;
  
  public long starttime;
  
  public String tag;
  
  public int uid;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("OplusWakeLockInfo{binderhash=");
    stringBuilder.append(this.binderhash);
    stringBuilder.append("flags=");
    stringBuilder.append(this.flags);
    stringBuilder.append(", tag=");
    stringBuilder.append(this.tag);
    stringBuilder.append(", duration=");
    stringBuilder.append(this.duration);
    stringBuilder.append(", starttime=");
    stringBuilder.append(this.starttime);
    stringBuilder.append(", packageName='");
    stringBuilder.append(this.packageName);
    stringBuilder.append('\'');
    stringBuilder.append(", uid=");
    stringBuilder.append(this.uid);
    stringBuilder.append(", pid=");
    stringBuilder.append(this.pid);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
}
