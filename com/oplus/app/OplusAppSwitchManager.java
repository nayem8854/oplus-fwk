package com.oplus.app;

import android.app.OplusActivityTaskManager;
import android.content.Context;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.util.ArrayMap;
import android.util.Log;
import java.util.Map;

public class OplusAppSwitchManager {
  public static int APP_SWITCH_VERSION = 1;
  
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  public static final String EXTRA_NOTIFY_TYPE = "extra_notify_type";
  
  public static final String EXTRA_SWITCH_INFO = "extyra_switch_info";
  
  public static final String INTENT_OPLUS_APP_SWITCH = "oppo.intent.action.APP_SWITCH";
  
  public static final int NOTIFY_TYPE_ACTIVITY_ENTER = 3;
  
  public static final int NOTIFY_TYPE_ACTIVITY_EXIT = 4;
  
  public static final int NOTIFY_TYPE_APP_ENTER = 1;
  
  public static final int NOTIFY_TYPE_APP_EXIT = 2;
  
  public static final String OPLUS_APP_SWITCH_SAFE_PERMISSIONS = "oppo.permission.OPPO_COMPONENT_SAFE";
  
  private static final String TAG = "OplusAppSwitchManager";
  
  private static OplusAppSwitchManager sInstance;
  
  private final Map<OnAppSwitchObserver, IOplusAppSwitchObserver> mAppSwitchObservers = (Map<OnAppSwitchObserver, IOplusAppSwitchObserver>)new ArrayMap();
  
  private OplusActivityTaskManager mOAms;
  
  public static OplusAppSwitchManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/app/OplusAppSwitchManager.sInstance : Lcom/oplus/app/OplusAppSwitchManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/app/OplusAppSwitchManager
    //   8: monitorenter
    //   9: getstatic com/oplus/app/OplusAppSwitchManager.sInstance : Lcom/oplus/app/OplusAppSwitchManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/app/OplusAppSwitchManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/app/OplusAppSwitchManager.sInstance : Lcom/oplus/app/OplusAppSwitchManager;
    //   27: ldc com/oplus/app/OplusAppSwitchManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/app/OplusAppSwitchManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/app/OplusAppSwitchManager.sInstance : Lcom/oplus/app/OplusAppSwitchManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #53	-> 0
    //   #54	-> 6
    //   #55	-> 9
    //   #56	-> 15
    //   #58	-> 27
    //   #60	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  private OplusAppSwitchManager() {
    this.mOAms = new OplusActivityTaskManager();
  }
  
  public boolean registerAppSwitchObserver(Context paramContext, OnAppSwitchObserver paramOnAppSwitchObserver, OplusAppSwitchConfig paramOplusAppSwitchConfig) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("registerAppSwitchObserver observer = ");
      stringBuilder.append(paramOnAppSwitchObserver);
      stringBuilder.append(" config = ");
      stringBuilder.append(paramOplusAppSwitchConfig);
      Log.i("OplusAppSwitchManager", stringBuilder.toString());
    } 
    if (paramOnAppSwitchObserver == null || paramContext == null)
      return false; 
    synchronized (this.mAppSwitchObservers) {
      if (this.mAppSwitchObservers.get(paramOnAppSwitchObserver) != null) {
        Log.e("OplusAppSwitchManager", "already register before");
        return false;
      } 
      OplusAppSwitchConfig oplusAppSwitchConfig = paramOplusAppSwitchConfig;
      if (paramOplusAppSwitchConfig == null) {
        oplusAppSwitchConfig = new OplusAppSwitchConfig();
        this();
      } 
      oplusAppSwitchConfig.observerFingerPrint = paramOnAppSwitchObserver.hashCode();
      OnAppSwitchObserverDelegate onAppSwitchObserverDelegate = new OnAppSwitchObserverDelegate();
      this(this, paramOnAppSwitchObserver);
      try {
        if (this.mOAms != null) {
          boolean bool = this.mOAms.registerAppSwitchObserver(paramContext.getPackageName(), onAppSwitchObserverDelegate, oplusAppSwitchConfig);
          if (bool)
            this.mAppSwitchObservers.put(paramOnAppSwitchObserver, onAppSwitchObserverDelegate); 
          return bool;
        } 
      } catch (RemoteException remoteException) {
        Log.e("OplusAppSwitchManager", "registerAppSwitchObserver remoteException ");
        remoteException.printStackTrace();
      } 
      return false;
    } 
  }
  
  public boolean unregisterAppSwitchObserver(Context paramContext, OnAppSwitchObserver paramOnAppSwitchObserver) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("unRegisterAppSwitchObserver observer = ");
      stringBuilder.append(paramOnAppSwitchObserver);
      Log.i("OplusAppSwitchManager", stringBuilder.toString());
    } 
    if (paramOnAppSwitchObserver == null || paramContext == null)
      return false; 
    synchronized (this.mAppSwitchObservers) {
      IOplusAppSwitchObserver iOplusAppSwitchObserver = this.mAppSwitchObservers.get(paramOnAppSwitchObserver);
      if (iOplusAppSwitchObserver != null)
        try {
          if (this.mOAms != null) {
            this.mAppSwitchObservers.remove(paramOnAppSwitchObserver);
            OplusAppSwitchConfig oplusAppSwitchConfig = new OplusAppSwitchConfig();
            this();
            oplusAppSwitchConfig.observerFingerPrint = paramOnAppSwitchObserver.hashCode();
            return this.mOAms.unregisterAppSwitchObserver(paramContext.getPackageName(), oplusAppSwitchConfig);
          } 
        } catch (RemoteException remoteException) {
          Log.e("OplusAppSwitchManager", "removeOnConfigChangedListener remoteException ");
          remoteException.printStackTrace();
        }  
      return false;
    } 
  }
  
  public static interface OnAppSwitchObserver {
    void onActivityEnter(OplusAppEnterInfo param1OplusAppEnterInfo);
    
    void onActivityExit(OplusAppExitInfo param1OplusAppExitInfo);
    
    void onAppEnter(OplusAppEnterInfo param1OplusAppEnterInfo);
    
    void onAppExit(OplusAppExitInfo param1OplusAppExitInfo);
  }
  
  class OnAppSwitchObserverDelegate extends IOplusAppSwitchObserver.Stub {
    private final OplusAppSwitchManager.OnAppSwitchObserver mObserver;
    
    final OplusAppSwitchManager this$0;
    
    public OnAppSwitchObserverDelegate(OplusAppSwitchManager.OnAppSwitchObserver param1OnAppSwitchObserver) {
      this.mObserver = param1OnAppSwitchObserver;
    }
    
    public void onActivityEnter(OplusAppEnterInfo param1OplusAppEnterInfo) {
      if (OplusAppSwitchManager.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onActivityEnter info = ");
        stringBuilder.append(param1OplusAppEnterInfo);
        Log.d("OplusAppSwitchManager", stringBuilder.toString());
      } 
      this.mObserver.onActivityEnter(param1OplusAppEnterInfo);
    }
    
    public void onActivityExit(OplusAppExitInfo param1OplusAppExitInfo) {
      if (OplusAppSwitchManager.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onActivityExit info = ");
        stringBuilder.append(param1OplusAppExitInfo);
        Log.d("OplusAppSwitchManager", stringBuilder.toString());
      } 
      this.mObserver.onActivityExit(param1OplusAppExitInfo);
    }
    
    public void onAppEnter(OplusAppEnterInfo param1OplusAppEnterInfo) {
      if (OplusAppSwitchManager.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onAppEnter info = ");
        stringBuilder.append(param1OplusAppEnterInfo);
        Log.d("OplusAppSwitchManager", stringBuilder.toString());
      } 
      this.mObserver.onAppEnter(param1OplusAppEnterInfo);
    }
    
    public void onAppExit(OplusAppExitInfo param1OplusAppExitInfo) {
      if (OplusAppSwitchManager.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onAppExit info = ");
        stringBuilder.append(param1OplusAppExitInfo);
        Log.d("OplusAppSwitchManager", stringBuilder.toString());
      } 
      this.mObserver.onAppExit(param1OplusAppExitInfo);
    }
  }
}
