package com.oplus.app;

import android.content.ComponentName;
import android.content.pm.ApplicationInfo;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public class OplusAppInfo implements Parcelable {
  public Rect appBounds = new Rect();
  
  public int orientation = -1;
  
  public boolean isRootActivity = false;
  
  public Bundle extension = new Bundle();
  
  public OplusAppInfo(OplusAppInfo paramOplusAppInfo) {
    if (paramOplusAppInfo != null) {
      this.windowingMode = paramOplusAppInfo.windowingMode;
      this.activityType = paramOplusAppInfo.activityType;
      this.appBounds = paramOplusAppInfo.appBounds;
      this.taskId = paramOplusAppInfo.taskId;
      this.topActivity = paramOplusAppInfo.topActivity;
      this.displayId = paramOplusAppInfo.displayId;
      this.orientation = paramOplusAppInfo.orientation;
      this.userId = paramOplusAppInfo.userId;
      this.launchedFromPackage = paramOplusAppInfo.launchedFromPackage;
      this.isRootActivity = paramOplusAppInfo.isRootActivity;
      this.extension = paramOplusAppInfo.extension;
    } 
  }
  
  public OplusAppInfo(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.windowingMode = paramParcel.readInt();
    this.activityType = paramParcel.readInt();
    this.taskId = paramParcel.readInt();
    this.appBounds = (Rect)paramParcel.readParcelable(Rect.class.getClassLoader());
    if (paramParcel.readInt() != 0)
      this.appInfo = (ApplicationInfo)ApplicationInfo.CREATOR.createFromParcel(paramParcel); 
    if (paramParcel.readInt() != 0)
      this.topActivity = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel); 
    this.displayId = paramParcel.readInt();
    this.orientation = paramParcel.readInt();
    this.userId = paramParcel.readInt();
    this.launchedFromPackage = paramParcel.readString();
    this.isRootActivity = paramParcel.readBoolean();
    this.extension = paramParcel.readBundle();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.windowingMode);
    paramParcel.writeInt(this.activityType);
    paramParcel.writeInt(this.taskId);
    paramParcel.writeParcelable((Parcelable)this.appBounds, paramInt);
    if (this.appInfo != null) {
      paramParcel.writeInt(1);
      this.appInfo.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeInt(0);
    } 
    if (this.topActivity != null) {
      paramParcel.writeInt(1);
      this.topActivity.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeInt(this.displayId);
    paramParcel.writeInt(this.orientation);
    paramParcel.writeInt(this.userId);
    paramParcel.writeString(this.launchedFromPackage);
    paramParcel.writeBoolean(this.isRootActivity);
    paramParcel.writeBundle(this.extension);
  }
  
  public static final Parcelable.Creator<OplusAppInfo> CREATOR = new Parcelable.Creator<OplusAppInfo>() {
      public OplusAppInfo createFromParcel(Parcel param1Parcel) {
        return new OplusAppInfo(param1Parcel);
      }
      
      public OplusAppInfo[] newArray(int param1Int) {
        return new OplusAppInfo[param1Int];
      }
    };
  
  public static final int ACTIVITY_TYPE_ASSISTANT = 4;
  
  public static final int ACTIVITY_TYPE_HOME = 2;
  
  public static final int ACTIVITY_TYPE_RECENTS = 3;
  
  public static final int ACTIVITY_TYPE_STANDARD = 1;
  
  public static final int ACTIVITY_TYPE_UNDEFINED = 0;
  
  public static final int WINDOWING_MODE_FREEFORM = 5;
  
  public static final int WINDOWING_MODE_FULLSCREEN = 1;
  
  public static final int WINDOWING_MODE_PINNED = 2;
  
  public static final int WINDOWING_MODE_SPLIT_SCREEN_PRIMARY = 3;
  
  public static final int WINDOWING_MODE_SPLIT_SCREEN_SECONDARY = 4;
  
  public static final int WINDOWING_MODE_UNDEFINED = 0;
  
  public int activityType;
  
  public ApplicationInfo appInfo;
  
  public int displayId;
  
  public String launchedFromPackage;
  
  public int taskId;
  
  public ComponentName topActivity;
  
  public int userId;
  
  public int windowingMode;
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("OplusAppInfo{");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("windowingMode=");
    stringBuilder2.append(this.windowingMode);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", activityType=");
    stringBuilder2.append(this.activityType);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", taskId=");
    stringBuilder2.append(this.taskId);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", appBounds=");
    stringBuilder2.append(this.appBounds);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", appInfo=");
    stringBuilder2.append(this.appInfo);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", topActivity=");
    stringBuilder2.append(this.topActivity);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", displayId=");
    stringBuilder2.append(this.displayId);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", orientation=");
    stringBuilder2.append(this.orientation);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", userId=");
    stringBuilder2.append(this.userId);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", launchedFromPackage=");
    stringBuilder2.append(this.launchedFromPackage);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", isRootActivity=");
    stringBuilder2.append(this.isRootActivity);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", extension=");
    stringBuilder2.append(this.extension);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append("}");
    return stringBuilder1.toString();
  }
  
  public OplusAppInfo() {}
}
