package com.oplus.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusAccessControlObserver extends IInterface {
  void onEncryptEnableChange(boolean paramBoolean) throws RemoteException;
  
  void onEncryptStateChange(OplusAccessControlInfo paramOplusAccessControlInfo) throws RemoteException;
  
  void onHideEnableChange(boolean paramBoolean) throws RemoteException;
  
  void onHideStateChange(OplusAccessControlInfo paramOplusAccessControlInfo) throws RemoteException;
  
  class Default implements IOplusAccessControlObserver {
    public void onEncryptStateChange(OplusAccessControlInfo param1OplusAccessControlInfo) throws RemoteException {}
    
    public void onHideStateChange(OplusAccessControlInfo param1OplusAccessControlInfo) throws RemoteException {}
    
    public void onEncryptEnableChange(boolean param1Boolean) throws RemoteException {}
    
    public void onHideEnableChange(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusAccessControlObserver {
    private static final String DESCRIPTOR = "com.oplus.app.IOplusAccessControlObserver";
    
    static final int TRANSACTION_onEncryptEnableChange = 3;
    
    static final int TRANSACTION_onEncryptStateChange = 1;
    
    static final int TRANSACTION_onHideEnableChange = 4;
    
    static final int TRANSACTION_onHideStateChange = 2;
    
    public Stub() {
      attachInterface(this, "com.oplus.app.IOplusAccessControlObserver");
    }
    
    public static IOplusAccessControlObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.app.IOplusAccessControlObserver");
      if (iInterface != null && iInterface instanceof IOplusAccessControlObserver)
        return (IOplusAccessControlObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onHideEnableChange";
          } 
          return "onEncryptEnableChange";
        } 
        return "onHideStateChange";
      } 
      return "onEncryptStateChange";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          boolean bool1 = false, bool2 = false;
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("com.oplus.app.IOplusAccessControlObserver");
              return true;
            } 
            param1Parcel1.enforceInterface("com.oplus.app.IOplusAccessControlObserver");
            bool1 = bool2;
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            onHideEnableChange(bool1);
            return true;
          } 
          param1Parcel1.enforceInterface("com.oplus.app.IOplusAccessControlObserver");
          if (param1Parcel1.readInt() != 0)
            bool1 = true; 
          onEncryptEnableChange(bool1);
          return true;
        } 
        param1Parcel1.enforceInterface("com.oplus.app.IOplusAccessControlObserver");
        if (param1Parcel1.readInt() != 0) {
          OplusAccessControlInfo oplusAccessControlInfo = (OplusAccessControlInfo)OplusAccessControlInfo.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onHideStateChange((OplusAccessControlInfo)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.app.IOplusAccessControlObserver");
      if (param1Parcel1.readInt() != 0) {
        OplusAccessControlInfo oplusAccessControlInfo = (OplusAccessControlInfo)OplusAccessControlInfo.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onEncryptStateChange((OplusAccessControlInfo)param1Parcel1);
      return true;
    }
    
    class Proxy implements IOplusAccessControlObserver {
      public static IOplusAccessControlObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IOplusAccessControlObserver.Stub this$0) {
        this.mRemote = (IBinder)this$0;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.app.IOplusAccessControlObserver";
      }
      
      public void onEncryptStateChange(OplusAccessControlInfo param2OplusAccessControlInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.app.IOplusAccessControlObserver");
          if (param2OplusAccessControlInfo != null) {
            parcel.writeInt(1);
            param2OplusAccessControlInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusAccessControlObserver.Stub.getDefaultImpl() != null) {
            IOplusAccessControlObserver.Stub.getDefaultImpl().onEncryptStateChange(param2OplusAccessControlInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onHideStateChange(OplusAccessControlInfo param2OplusAccessControlInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.app.IOplusAccessControlObserver");
          if (param2OplusAccessControlInfo != null) {
            parcel.writeInt(1);
            param2OplusAccessControlInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IOplusAccessControlObserver.Stub.getDefaultImpl() != null) {
            IOplusAccessControlObserver.Stub.getDefaultImpl().onHideStateChange(param2OplusAccessControlInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onEncryptEnableChange(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.oplus.app.IOplusAccessControlObserver");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel, null, 1);
          if (!bool1 && IOplusAccessControlObserver.Stub.getDefaultImpl() != null) {
            IOplusAccessControlObserver.Stub.getDefaultImpl().onEncryptEnableChange(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onHideEnableChange(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.oplus.app.IOplusAccessControlObserver");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(4, parcel, null, 1);
          if (!bool1 && IOplusAccessControlObserver.Stub.getDefaultImpl() != null) {
            IOplusAccessControlObserver.Stub.getDefaultImpl().onHideEnableChange(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusAccessControlObserver param1IOplusAccessControlObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusAccessControlObserver != null) {
          Proxy.sDefaultImpl = param1IOplusAccessControlObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusAccessControlObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
