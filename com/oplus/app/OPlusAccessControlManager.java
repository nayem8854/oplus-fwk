package com.oplus.app;

import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.util.Log;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OPlusAccessControlManager {
  public static final String ACCESS_CONTROL_LOCK_ENABLED = "access_control_lock_enabled";
  
  public static final String ACCESS_CONTROL_LOCK_MODE = "access_control_lock_mode";
  
  public static final String ACCESS_CONTROL_PACKAGE_NAME = "Access_Control_Package_Name";
  
  public static final String ACCESS_CONTROL_PACKAGE_USERID = "Access_Control_Package_UserId";
  
  public static final int FLAG_ENCRYPTED = 8;
  
  public static final int FLAG_HIDE_ICON = 1;
  
  public static final int FLAG_HIDE_IN_RECENT = 2;
  
  public static final int FLAG_HIDE_NOTICE = 4;
  
  public static final String LAUNCH_WINDOWING_MODE = "Launch_Windowing_Mode";
  
  public static final int MODE_EACH = 0;
  
  public static final int MODE_LOCK_SCREEN = 1;
  
  public static final int RUS_TYPE_FILTER = 0;
  
  public static final int RUS_TYPE_HIDE_KEYGUARD_LOCK = 1;
  
  public static final String SHOW_WHEN_LOCK = "show_when_lock";
  
  private static final String TAG = "OPlusAccessControlManager";
  
  public static final String TASK_ID = "task_id";
  
  public static final String TYPE_ENCRYPT = "type_encrypt";
  
  public static final String TYPE_ENCRYPT_IGNORE_ENABLE = "type_encrypt_ignore_enable";
  
  public static final String TYPE_HIDE = "type_hide";
  
  public static final String TYPE_HIDE_IGNORE_ENABLE = "type_hide_ignore_enable";
  
  public static final int USER_CURRENT;
  
  public static final int USER_XSPACE = 999;
  
  private static volatile OPlusAccessControlManager sInstance = null;
  
  private final IOplusAccessControlManager mService;
  
  static {
    USER_CURRENT = UserHandle.myUserId();
  }
  
  private OPlusAccessControlManager() {
    IBinder iBinder = ServiceManager.getService("color_accesscontrol");
    this.mService = IOplusAccessControlManager.Stub.asInterface(iBinder);
  }
  
  public static OPlusAccessControlManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/app/OPlusAccessControlManager.sInstance : Lcom/oplus/app/OPlusAccessControlManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/app/OPlusAccessControlManager
    //   8: monitorenter
    //   9: getstatic com/oplus/app/OPlusAccessControlManager.sInstance : Lcom/oplus/app/OPlusAccessControlManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/app/OPlusAccessControlManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/app/OPlusAccessControlManager.sInstance : Lcom/oplus/app/OPlusAccessControlManager;
    //   27: ldc com/oplus/app/OPlusAccessControlManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/app/OPlusAccessControlManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/app/OPlusAccessControlManager.sInstance : Lcom/oplus/app/OPlusAccessControlManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #81	-> 0
    //   #82	-> 6
    //   #83	-> 9
    //   #84	-> 15
    //   #86	-> 27
    //   #88	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public void setAccessControlAppsInfo(String paramString, Map<String, Integer> paramMap, int paramInt) {
    int i = paramInt;
    if (paramInt != 999)
      try {
        i = UserHandle.myUserId();
        this.mService.setAccessControlAppsInfo(paramString, paramMap, i);
        return;
      } catch (RemoteException remoteException) {
        throw new RuntimeException("setAccessControlAppsInfo exception", remoteException);
      }  
    this.mService.setAccessControlAppsInfo((String)remoteException, paramMap, i);
  }
  
  public Map<String, Integer> getAccessControlAppsInfo(String paramString, int paramInt) {
    int i = paramInt;
    if (paramInt != 999)
      try {
        i = UserHandle.myUserId();
        return this.mService.getAccessControlAppsInfo(paramString, i);
      } catch (RemoteException remoteException) {
        throw new RuntimeException("getAccessControlAppsInfo exception", remoteException);
      }  
    return this.mService.getAccessControlAppsInfo((String)remoteException, i);
  }
  
  public void setAccessControlEnabled(String paramString, boolean paramBoolean, int paramInt) {
    int i = paramInt;
    if (paramInt != 999)
      try {
        i = UserHandle.myUserId();
        this.mService.setAccessControlEnabled(paramString, paramBoolean, i);
        return;
      } catch (RemoteException remoteException) {
        throw new RuntimeException("setAccessControlEnabled exception", remoteException);
      }  
    this.mService.setAccessControlEnabled((String)remoteException, paramBoolean, i);
  }
  
  public boolean getAccessControlEnabled(String paramString, int paramInt) {
    int i = paramInt;
    if (paramInt != 999)
      try {
        i = UserHandle.myUserId();
        return this.mService.getAccessControlEnabled(paramString, i);
      } catch (RemoteException remoteException) {
        throw new RuntimeException("getAccessControlEnabled exception", remoteException);
      }  
    return this.mService.getAccessControlEnabled((String)remoteException, i);
  }
  
  public void addEncryptPass(String paramString, int paramInt1, int paramInt2) {
    int i = paramInt2;
    if (paramInt2 != 999)
      try {
        i = UserHandle.myUserId();
        this.mService.addEncryptPass(paramString, paramInt1, i);
        return;
      } catch (RemoteException remoteException) {
        throw new RuntimeException("addEncryptPass exception", remoteException);
      }  
    this.mService.addEncryptPass((String)remoteException, paramInt1, i);
  }
  
  public boolean isEncryptPass(String paramString, int paramInt) {
    int i = paramInt;
    if (paramInt != 999)
      try {
        i = UserHandle.myUserId();
        return this.mService.isEncryptPass(paramString, i);
      } catch (RemoteException remoteException) {
        throw new RuntimeException("isEncryptPass exception", remoteException);
      }  
    return this.mService.isEncryptPass((String)remoteException, i);
  }
  
  public boolean isEncryptedPackage(String paramString, int paramInt) {
    int i = paramInt;
    if (paramInt != 999)
      try {
        i = UserHandle.myUserId();
        return this.mService.isEncryptedPackage(paramString, i);
      } catch (RemoteException remoteException) {
        throw new RuntimeException("isEncryptedPackage remoteException ", remoteException);
      }  
    return this.mService.isEncryptedPackage((String)remoteException, i);
  }
  
  public boolean registerAccessControlObserver(String paramString, IOplusAccessControlObserver paramIOplusAccessControlObserver) {
    try {
      return this.mService.registerAccessControlObserver(paramString, paramIOplusAccessControlObserver);
    } catch (RemoteException remoteException) {
      Log.e("OPlusAccessControlManager", "registerAccessControlObserver remoteException ");
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public boolean unregisterAccessControlObserver(String paramString, IOplusAccessControlObserver paramIOplusAccessControlObserver) {
    try {
      return this.mService.unregisterAccessControlObserver(paramString, paramIOplusAccessControlObserver);
    } catch (RemoteException remoteException) {
      Log.e("OPlusAccessControlManager", "unregisterAccessControlObserver remoteException ");
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public void updateRusList(int paramInt, List<String> paramList1, List<String> paramList2) {
    try {
      this.mService.updateRusList(paramInt, paramList1, paramList2);
      return;
    } catch (RemoteException remoteException) {
      throw new RuntimeException("updateRusList exception", remoteException);
    } 
  }
  
  public void setPrivacyAppsInfoForUser(Map<String, Integer> paramMap, boolean paramBoolean, int paramInt) {}
  
  public boolean getApplicationAccessControlEnabledAsUser(String paramString, int paramInt) {
    return false;
  }
  
  public void addAccessControlPassForUser(String paramString, int paramInt1, int paramInt2) {}
  
  public Map<String, Integer> getPrivacyAppInfo(int paramInt) {
    return (Map)new HashMap<>();
  }
  
  public boolean isAccessControlPassForUser(String paramString, int paramInt) {
    return false;
  }
}
