package com.oplus.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusPermissionRecordController extends IInterface {
  void notifyPermissionRecordInfo(String[] paramArrayOfString1, String[] paramArrayOfString2, long[] paramArrayOflong, int[] paramArrayOfint) throws RemoteException;
  
  class Default implements IOplusPermissionRecordController {
    public void notifyPermissionRecordInfo(String[] param1ArrayOfString1, String[] param1ArrayOfString2, long[] param1ArrayOflong, int[] param1ArrayOfint) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusPermissionRecordController {
    private static final String DESCRIPTOR = "com.oplus.app.IOplusPermissionRecordController";
    
    static final int TRANSACTION_notifyPermissionRecordInfo = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.app.IOplusPermissionRecordController");
    }
    
    public static IOplusPermissionRecordController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.app.IOplusPermissionRecordController");
      if (iInterface != null && iInterface instanceof IOplusPermissionRecordController)
        return (IOplusPermissionRecordController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "notifyPermissionRecordInfo";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.app.IOplusPermissionRecordController");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.app.IOplusPermissionRecordController");
      String[] arrayOfString1 = param1Parcel1.createStringArray();
      String[] arrayOfString2 = param1Parcel1.createStringArray();
      long[] arrayOfLong = param1Parcel1.createLongArray();
      int[] arrayOfInt = param1Parcel1.createIntArray();
      notifyPermissionRecordInfo(arrayOfString1, arrayOfString2, arrayOfLong, arrayOfInt);
      return true;
    }
    
    private static class Proxy implements IOplusPermissionRecordController {
      public static IOplusPermissionRecordController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.app.IOplusPermissionRecordController";
      }
      
      public void notifyPermissionRecordInfo(String[] param2ArrayOfString1, String[] param2ArrayOfString2, long[] param2ArrayOflong, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.app.IOplusPermissionRecordController");
          parcel.writeStringArray(param2ArrayOfString1);
          parcel.writeStringArray(param2ArrayOfString2);
          parcel.writeLongArray(param2ArrayOflong);
          parcel.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusPermissionRecordController.Stub.getDefaultImpl() != null) {
            IOplusPermissionRecordController.Stub.getDefaultImpl().notifyPermissionRecordInfo(param2ArrayOfString1, param2ArrayOfString2, param2ArrayOflong, param2ArrayOfint);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusPermissionRecordController param1IOplusPermissionRecordController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusPermissionRecordController != null) {
          Proxy.sDefaultImpl = param1IOplusPermissionRecordController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusPermissionRecordController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
