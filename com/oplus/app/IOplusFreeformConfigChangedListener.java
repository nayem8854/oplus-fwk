package com.oplus.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusFreeformConfigChangedListener extends IInterface {
  void onConfigSwitchChanged(boolean paramBoolean) throws RemoteException;
  
  void onConfigTypeChanged(int paramInt) throws RemoteException;
  
  class Default implements IOplusFreeformConfigChangedListener {
    public void onConfigTypeChanged(int param1Int) throws RemoteException {}
    
    public void onConfigSwitchChanged(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusFreeformConfigChangedListener {
    private static final String DESCRIPTOR = "com.oplus.app.IOplusFreeformConfigChangedListener";
    
    static final int TRANSACTION_onConfigSwitchChanged = 2;
    
    static final int TRANSACTION_onConfigTypeChanged = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.app.IOplusFreeformConfigChangedListener");
    }
    
    public static IOplusFreeformConfigChangedListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.app.IOplusFreeformConfigChangedListener");
      if (iInterface != null && iInterface instanceof IOplusFreeformConfigChangedListener)
        return (IOplusFreeformConfigChangedListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onConfigSwitchChanged";
      } 
      return "onConfigTypeChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        boolean bool;
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("com.oplus.app.IOplusFreeformConfigChangedListener");
          return true;
        } 
        param1Parcel1.enforceInterface("com.oplus.app.IOplusFreeformConfigChangedListener");
        if (param1Parcel1.readInt() != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        onConfigSwitchChanged(bool);
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.app.IOplusFreeformConfigChangedListener");
      param1Int1 = param1Parcel1.readInt();
      onConfigTypeChanged(param1Int1);
      return true;
    }
    
    private static class Proxy implements IOplusFreeformConfigChangedListener {
      public static IOplusFreeformConfigChangedListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.app.IOplusFreeformConfigChangedListener";
      }
      
      public void onConfigTypeChanged(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.app.IOplusFreeformConfigChangedListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusFreeformConfigChangedListener.Stub.getDefaultImpl() != null) {
            IOplusFreeformConfigChangedListener.Stub.getDefaultImpl().onConfigTypeChanged(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onConfigSwitchChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.oplus.app.IOplusFreeformConfigChangedListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel, null, 1);
          if (!bool1 && IOplusFreeformConfigChangedListener.Stub.getDefaultImpl() != null) {
            IOplusFreeformConfigChangedListener.Stub.getDefaultImpl().onConfigSwitchChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusFreeformConfigChangedListener param1IOplusFreeformConfigChangedListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusFreeformConfigChangedListener != null) {
          Proxy.sDefaultImpl = param1IOplusFreeformConfigChangedListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusFreeformConfigChangedListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
