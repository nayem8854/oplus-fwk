package com.oplus.app;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusSplitScreenObserver extends IInterface {
  void onStateChanged(String paramString, Bundle paramBundle) throws RemoteException;
  
  class Default implements IOplusSplitScreenObserver {
    public void onStateChanged(String param1String, Bundle param1Bundle) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusSplitScreenObserver {
    private static final String DESCRIPTOR = "com.oplus.app.IOplusSplitScreenObserver";
    
    static final int TRANSACTION_onStateChanged = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.app.IOplusSplitScreenObserver");
    }
    
    public static IOplusSplitScreenObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.app.IOplusSplitScreenObserver");
      if (iInterface != null && iInterface instanceof IOplusSplitScreenObserver)
        return (IOplusSplitScreenObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onStateChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.app.IOplusSplitScreenObserver");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.app.IOplusSplitScreenObserver");
      String str = param1Parcel1.readString();
      if (param1Parcel1.readInt() != 0) {
        Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onStateChanged(str, (Bundle)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IOplusSplitScreenObserver {
      public static IOplusSplitScreenObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.app.IOplusSplitScreenObserver";
      }
      
      public void onStateChanged(String param2String, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.app.IOplusSplitScreenObserver");
          parcel.writeString(param2String);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusSplitScreenObserver.Stub.getDefaultImpl() != null) {
            IOplusSplitScreenObserver.Stub.getDefaultImpl().onStateChanged(param2String, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusSplitScreenObserver param1IOplusSplitScreenObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusSplitScreenObserver != null) {
          Proxy.sDefaultImpl = param1IOplusSplitScreenObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusSplitScreenObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
