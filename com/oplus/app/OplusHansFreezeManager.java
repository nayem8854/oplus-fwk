package com.oplus.app;

import android.app.OplusActivityManager;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.util.ArrayMap;
import android.util.Log;
import java.util.ArrayList;

public class OplusHansFreezeManager {
  private static final String BUNDLE_KEY_LIST = "list";
  
  private static final String BUNDLE_KEY_TYPE = "type";
  
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  public static final String FREEZE_LEVEL = "level";
  
  public static final int FREEZE_LEVEL_FOUR = 4;
  
  public static final int FREEZE_LEVEL_ONE = 1;
  
  public static final int FREEZE_LEVEL_THREE = 3;
  
  public static final int FREEZE_LEVEL_TWO = 2;
  
  public static final String FREEZE_TYPE_ADD = "add";
  
  public static final String FREEZE_TYPE_RM = "rm";
  
  public static final String PACKAGE = "pkg";
  
  private static final String TAG = "OplusHansFreezeManager";
  
  public static final String UID = "uid";
  
  private static OplusHansFreezeManager sInstance;
  
  private final ArrayMap<OplusHansCallBack, ColorHansListenerDelegate> mColorHansCallBackMap = new ArrayMap();
  
  private OplusActivityManager mOAms;
  
  public static OplusHansFreezeManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/app/OplusHansFreezeManager.sInstance : Lcom/oplus/app/OplusHansFreezeManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/app/OplusHansFreezeManager
    //   8: monitorenter
    //   9: getstatic com/oplus/app/OplusHansFreezeManager.sInstance : Lcom/oplus/app/OplusHansFreezeManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/app/OplusHansFreezeManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/app/OplusHansFreezeManager.sInstance : Lcom/oplus/app/OplusHansFreezeManager;
    //   27: ldc com/oplus/app/OplusHansFreezeManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/app/OplusHansFreezeManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/app/OplusHansFreezeManager.sInstance : Lcom/oplus/app/OplusHansFreezeManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #55	-> 0
    //   #56	-> 6
    //   #57	-> 9
    //   #58	-> 15
    //   #60	-> 27
    //   #62	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  private OplusHansFreezeManager() {
    this.mOAms = new OplusActivityManager();
  }
  
  public boolean registerHansListener(Context paramContext, OplusHansCallBack paramOplusHansCallBack) {
    if (paramOplusHansCallBack == null || paramContext == null)
      return false; 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("registerHansListener callBack = ");
      stringBuilder.append(paramOplusHansCallBack);
      Log.i("OplusHansFreezeManager", stringBuilder.toString());
    } 
    synchronized (this.mColorHansCallBackMap) {
      if (this.mColorHansCallBackMap.get(paramOplusHansCallBack) != null) {
        Log.e("OplusHansFreezeManager", "already register before");
        return false;
      } 
      ColorHansListenerDelegate colorHansListenerDelegate = new ColorHansListenerDelegate();
      this(this, paramOplusHansCallBack);
      try {
        if (this.mOAms != null) {
          boolean bool = this.mOAms.registerHansListener(paramContext.getPackageName(), colorHansListenerDelegate);
          if (bool)
            this.mColorHansCallBackMap.put(paramOplusHansCallBack, colorHansListenerDelegate); 
          return bool;
        } 
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("registerHansListener remoteException ");
        stringBuilder.append(remoteException);
        Log.e("OplusHansFreezeManager", stringBuilder.toString());
      } 
      return true;
    } 
  }
  
  public boolean unregisterHansListener(Context paramContext, OplusHansCallBack paramOplusHansCallBack) {
    if (paramContext == null || paramOplusHansCallBack == null)
      return false; 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("unregisterHansListener callBack = ");
      stringBuilder.append(paramOplusHansCallBack);
      Log.i("OplusHansFreezeManager", stringBuilder.toString());
    } 
    synchronized (this.mColorHansCallBackMap) {
      ColorHansListenerDelegate colorHansListenerDelegate = (ColorHansListenerDelegate)this.mColorHansCallBackMap.get(paramOplusHansCallBack);
      if (colorHansListenerDelegate != null)
        try {
          if (this.mOAms != null) {
            this.mColorHansCallBackMap.remove(colorHansListenerDelegate);
            return this.mOAms.unregisterHansListener(paramContext.getPackageName(), colorHansListenerDelegate);
          } 
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("unregisterHansListener remoteException ");
          stringBuilder.append(remoteException);
          Log.e("OplusHansFreezeManager", stringBuilder.toString());
        }  
      return true;
    } 
  }
  
  public boolean setAppFreeze(Context paramContext, ArrayList<Bundle> paramArrayList, String paramString) {
    if (paramContext == null || paramArrayList == null || paramString == null)
      return false; 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setAppFreeze list = ");
      stringBuilder.append(paramArrayList);
      stringBuilder.append(" ");
      stringBuilder.append(paramString);
      Log.i("OplusHansFreezeManager", stringBuilder.toString());
    } 
    try {
      if (this.mOAms != null) {
        Bundle bundle = new Bundle();
        this();
        bundle.putString("type", paramString);
        bundle.putParcelableArrayList("list", paramArrayList);
        return this.mOAms.setAppFreeze(paramContext.getPackageName(), bundle);
      } 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("registerHansListener remoteException ");
      stringBuilder.append(remoteException);
      Log.e("OplusHansFreezeManager", stringBuilder.toString());
    } 
    return false;
  }
  
  class ColorHansListenerDelegate extends IOplusHansListener.Stub {
    private final OplusHansFreezeManager.OplusHansCallBack mCallBack;
    
    final OplusHansFreezeManager this$0;
    
    public ColorHansListenerDelegate(OplusHansFreezeManager.OplusHansCallBack param1OplusHansCallBack) {
      this.mCallBack = param1OplusHansCallBack;
    }
    
    public void notifyRecordData(Bundle param1Bundle, String param1String) {
      this.mCallBack.notifyRecordData(param1Bundle, param1String);
    }
  }
  
  public void enterFastFreezer(Context paramContext, int[] paramArrayOfint, long paramLong, String paramString) {
    if (paramContext == null) {
      Log.w("OplusHansFreezeManager", "enterFastFreezer context is null");
      return;
    } 
    if (DEBUG)
      Log.i("OplusHansFreezeManager", "enterFastFreezer"); 
    try {
      if (this.mOAms != null)
        this.mOAms.enterFastFreezer(paramContext.getPackageName(), paramArrayOfint, paramLong, paramString); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("enterFastFreezer remoteException ");
      stringBuilder.append(remoteException);
      Log.e("OplusHansFreezeManager", stringBuilder.toString());
    } 
  }
  
  public void exitFastFreezer(Context paramContext, String paramString) {
    if (paramContext == null) {
      Log.w("OplusHansFreezeManager", "exitFastFreezer context is null");
      return;
    } 
    if (DEBUG)
      Log.i("OplusHansFreezeManager", "exitFastFreezer"); 
    try {
      if (this.mOAms != null)
        this.mOAms.exitFastFreezer(paramContext.getPackageName(), paramString); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("exitFastFreezer remoteException ");
      stringBuilder.append(remoteException);
      Log.e("OplusHansFreezeManager", stringBuilder.toString());
    } 
  }
  
  public static interface OplusHansCallBack {
    void notifyRecordData(Bundle param1Bundle, String param1String);
  }
}
