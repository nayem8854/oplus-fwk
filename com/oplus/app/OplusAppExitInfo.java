package com.oplus.app;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public final class OplusAppExitInfo implements Parcelable {
  public static final int APP_SWITCH_VERSION = 1;
  
  public OplusAppExitInfo() {
    this.extension = new Bundle();
  }
  
  public OplusAppExitInfo(Parcel paramParcel) {
    boolean bool2;
    this.extension = new Bundle();
    this.targetName = paramParcel.readString();
    byte b = paramParcel.readByte();
    boolean bool1 = true;
    if (b != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.hasResumingActivity = bool2;
    this.resumingPackageName = paramParcel.readString();
    this.resumingActivityName = paramParcel.readString();
    this.resumingWindowMode = paramParcel.readInt();
    if (paramParcel.readByte() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.isResumingMultiApp = bool2;
    if (paramParcel.readByte() != 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.isResumingFirstStart = bool2;
    this.extension = paramParcel.readBundle();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.targetName);
    paramParcel.writeByte((byte)this.hasResumingActivity);
    paramParcel.writeString(this.resumingPackageName);
    paramParcel.writeString(this.resumingActivityName);
    paramParcel.writeInt(this.resumingWindowMode);
    paramParcel.writeByte((byte)this.isResumingMultiApp);
    paramParcel.writeByte((byte)this.isResumingFirstStart);
    paramParcel.writeBundle(this.extension);
  }
  
  public static final Parcelable.Creator<OplusAppExitInfo> CREATOR = new Parcelable.Creator<OplusAppExitInfo>() {
      public OplusAppExitInfo createFromParcel(Parcel param1Parcel) {
        return new OplusAppExitInfo(param1Parcel);
      }
      
      public OplusAppExitInfo[] newArray(int param1Int) {
        return new OplusAppExitInfo[param1Int];
      }
    };
  
  public static final int SWITCH_TYPE_ACTIVITY = 1;
  
  public static final int SWITCH_TYPE_APP = 2;
  
  public Bundle extension;
  
  public boolean hasResumingActivity;
  
  public boolean isResumingFirstStart;
  
  public boolean isResumingMultiApp;
  
  public String resumingActivityName;
  
  public String resumingPackageName;
  
  public int resumingWindowMode;
  
  public String targetName;
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("OplusAppExitInfo = { ");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" targetName = ");
    stringBuilder2.append(this.targetName);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" hasResumingActivity = ");
    stringBuilder2.append(this.hasResumingActivity);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" resumingPackageName = ");
    stringBuilder2.append(this.resumingPackageName);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" resumingActivityName = ");
    stringBuilder2.append(this.resumingActivityName);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" resumingWindowMode = ");
    stringBuilder2.append(this.resumingWindowMode);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" isResumingMultiApp = ");
    stringBuilder2.append(this.isResumingMultiApp);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" isResumingFirstStart = ");
    stringBuilder2.append(this.isResumingFirstStart);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" extension = ");
    stringBuilder2.append(this.extension);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append("}");
    return stringBuilder1.toString();
  }
}
