package com.oplus.app;

import android.content.pm.OplusPackageManager;
import android.database.sqlite.SQLiteDebug;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OplusAppDynamicFeatureManager implements IOplusAppDynamicFeatureManager {
  private static final String BACKSLASH_SYMBOL = "/";
  
  private static final String BLANK_SPACE_SYMBOL = " ";
  
  private static final String COLON_SYMBOL = ":";
  
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static final String LEFT_BRACE_SYMBOL = "{";
  
  private static final int MAX_SIZE = 100;
  
  private static final String POINT_SYMBOL = ".";
  
  private static final int RESOURCE_ID_POSITION = 6;
  
  private static final String RIGHT_BRACE_SYMBOL = "}";
  
  private static final String TAG = "OplusAppDynamicFeatureManager";
  
  private static volatile OplusAppDynamicFeatureManager sInstance = null;
  
  private OplusPackageManager mOppoPm = null;
  
  private ExecutorService mThreadPool = null;
  
  public static OplusAppDynamicFeatureManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/app/OplusAppDynamicFeatureManager.sInstance : Lcom/oplus/app/OplusAppDynamicFeatureManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/app/OplusAppDynamicFeatureManager
    //   8: monitorenter
    //   9: getstatic com/oplus/app/OplusAppDynamicFeatureManager.sInstance : Lcom/oplus/app/OplusAppDynamicFeatureManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/app/OplusAppDynamicFeatureManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/app/OplusAppDynamicFeatureManager.sInstance : Lcom/oplus/app/OplusAppDynamicFeatureManager;
    //   27: ldc com/oplus/app/OplusAppDynamicFeatureManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/app/OplusAppDynamicFeatureManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/app/OplusAppDynamicFeatureManager.sInstance : Lcom/oplus/app/OplusAppDynamicFeatureManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #60	-> 0
    //   #61	-> 6
    //   #62	-> 9
    //   #63	-> 15
    //   #65	-> 27
    //   #68	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  private OplusAppDynamicFeatureManager() {
    this.mOppoPm = new OplusPackageManager();
    this.mThreadPool = Executors.newSingleThreadExecutor();
  }
  
  public void parseAppDynamicInfo(final String packageName, final String activityName, final View view) {
    checkThreadPool();
    this.mThreadPool.execute(new Runnable() {
          final OplusAppDynamicFeatureManager this$0;
          
          final String val$activityName;
          
          final String val$packageName;
          
          final View val$view;
          
          public void run() {
            try {
              OplusAppDynamicFeatureManager.this.startToParseAppDynamicInfo(packageName, activityName, view);
            } catch (Exception exception) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("parse app dynamic info exception: ");
              stringBuilder.append(exception.getMessage());
              Log.e("OplusAppDynamicFeatureManager", stringBuilder.toString());
            } 
          }
        });
  }
  
  private void startToParseAppDynamicInfo(String paramString1, String paramString2, View paramView) {
    if (paramView == null) {
      Log.w("OplusAppDynamicFeatureManager", "view is null");
      return;
    } 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("parse view hierarchy: ");
      stringBuilder.append(paramString1);
      stringBuilder.append("/");
      stringBuilder.append(paramString2);
      Log.d("OplusAppDynamicFeatureManager", stringBuilder.toString());
    } 
    long l = System.currentTimeMillis();
    OplusAppDynamicFeatureData oplusAppDynamicFeatureData = new OplusAppDynamicFeatureData();
    oplusAppDynamicFeatureData.setPackageName(paramString1);
    oplusAppDynamicFeatureData.setActivityName(paramString2);
    ArrayMap arrayMap2 = new ArrayMap();
    ArrayMap arrayMap1 = new ArrayMap();
    parseViewHierarchyInfo(paramView, (Map<String, Integer>)arrayMap2, (Map<String, Integer>)arrayMap1);
    if (arrayMap2.size() >= 100 || arrayMap1.size() >= 100) {
      if (DEBUG)
        Log.w("OplusAppDynamicFeatureManager", "parse view size is too big!"); 
      return;
    } 
    oplusAppDynamicFeatureData.setComponentNames((Map)arrayMap2);
    oplusAppDynamicFeatureData.setIdNames((Map)arrayMap1);
    onGetAppDynamicFeatureFinished(oplusAppDynamicFeatureData);
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("get app dynamic feature cost: ");
      stringBuilder.append(System.currentTimeMillis() - l);
      stringBuilder.append(" ms");
      Log.d("OplusAppDynamicFeatureManager", stringBuilder.toString());
    } 
  }
  
  private void parseViewHierarchyInfo(View paramView, Map<String, Integer> paramMap1, Map<String, Integer> paramMap2) {
    if (!(paramView instanceof ViewGroup))
      return; 
    Stack<View> stack = new Stack();
    ViewGroup viewGroup = (ViewGroup)paramView;
    int i = viewGroup.getChildCount();
    if (i <= 0)
      return; 
    byte b;
    for (b = 0; b < i; b++) {
      paramView = viewGroup.getChildAt(b);
      if (paramView != null)
        stack.push(paramView); 
    } 
    while (!stack.empty()) {
      paramView = stack.pop();
      parseViewString(paramView.toString(), paramMap1, paramMap2);
      if (!(paramView instanceof ViewGroup))
        continue; 
      ViewGroup viewGroup1 = (ViewGroup)paramView;
      i = viewGroup1.getChildCount();
      if (i <= 0)
        continue; 
      for (b = 0; b < i; b++) {
        if (viewGroup1.getChildAt(b) != null)
          stack.push(viewGroup1.getChildAt(b)); 
      } 
    } 
  }
  
  private void parseViewString(String paramString, Map<String, Integer> paramMap1, Map<String, Integer> paramMap2) {
    if (TextUtils.isEmpty(paramString))
      return; 
    int i = paramString.lastIndexOf("{");
    int j = paramString.lastIndexOf("}");
    if (i == -1 || j == -1 || i > j)
      return; 
    String str2 = parseComponentName(paramString.substring(0, i));
    if (str2 != null)
      if (paramMap1.containsKey(str2)) {
        Integer integer = paramMap1.get(str2);
        if (integer != null) {
          paramMap1.put(str2, Integer.valueOf(integer.intValue() + 1));
        } else {
          paramMap1.put(str2, Integer.valueOf(1));
        } 
      } else {
        paramMap1.put(str2, Integer.valueOf(1));
      }  
    String str1 = parseResourceIdName(paramString.substring(i + 1, j));
    if (str1 != null)
      if (paramMap2.containsKey(str1)) {
        Integer integer = paramMap2.get(str1);
        if (integer != null) {
          paramMap2.put(str1, Integer.valueOf(integer.intValue() + 1));
        } else {
          paramMap2.put(str1, Integer.valueOf(1));
        } 
      } else {
        paramMap2.put(str1, Integer.valueOf(1));
      }  
  }
  
  private String parseComponentName(String paramString) {
    if (TextUtils.isEmpty(paramString))
      return null; 
    int i = paramString.lastIndexOf(".");
    if (i == -1)
      return null; 
    return paramString.substring(i + 1);
  }
  
  private String parseResourceIdName(String paramString) {
    if (TextUtils.isEmpty(paramString))
      return null; 
    String[] arrayOfString = paramString.split(" ");
    if (arrayOfString.length >= 6)
      return arrayOfString[5]; 
    return null;
  }
  
  private void parseTextFromView(View paramView, List<String> paramList) {
    if (paramView == null)
      return; 
    if (paramView instanceof TextView) {
      TextView textView = (TextView)paramView;
      CharSequence charSequence = textView.getText();
      if (charSequence != null) {
        charSequence = charSequence.toString();
        paramList.add(charSequence);
      } 
    } 
  }
  
  private List<String> getDatabaseInfo(String paramString) {
    long l = System.currentTimeMillis();
    ArrayList<String> arrayList = new ArrayList();
    SQLiteDebug.PagerStats pagerStats = SQLiteDebug.getDatabaseInfo();
    if (pagerStats != null)
      for (byte b = 0; b < pagerStats.dbStats.size(); b++) {
        SQLiteDebug.DbStats dbStats = pagerStats.dbStats.get(b);
        if (dbStats != null)
          arrayList.add(dbStats.dbName); 
      }  
    if (DEBUG) {
      long l1 = System.currentTimeMillis();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("get database info for ");
      stringBuilder.append(paramString);
      stringBuilder.append(" cost: ");
      stringBuilder.append(l1 - l);
      stringBuilder.append(" ms");
      Log.d("OplusAppDynamicFeatureManager", stringBuilder.toString());
    } 
    return arrayList;
  }
  
  private void onGetAppDynamicFeatureFinished(OplusAppDynamicFeatureData paramOplusAppDynamicFeatureData) {
    checkOppoPackageManager();
    OplusPackageManager oplusPackageManager = this.mOppoPm;
    if (oplusPackageManager != null)
      try {
        oplusPackageManager.dynamicDetectApp(paramOplusAppDynamicFeatureData);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("RemoteException: ");
        stringBuilder.append(remoteException.getMessage());
        Log.e("OplusAppDynamicFeatureManager", stringBuilder.toString());
      }  
  }
  
  private void checkOppoPackageManager() {
    if (this.mOppoPm == null)
      this.mOppoPm = new OplusPackageManager(); 
  }
  
  private void checkThreadPool() {
    if (this.mThreadPool == null)
      this.mThreadPool = Executors.newSingleThreadExecutor(); 
  }
}
