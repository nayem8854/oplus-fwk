package com.oplus.app;

import android.app.IActivityController;
import android.content.Intent;
import android.os.RemoteException;

public class OplusAppProtectController extends IActivityController.Stub {
  public boolean activityStarting(Intent paramIntent, String paramString) {
    return true;
  }
  
  public boolean activityResuming(String paramString) {
    return true;
  }
  
  public boolean appCrashed(String paramString1, int paramInt, String paramString2, String paramString3, long paramLong, String paramString4) {
    return true;
  }
  
  public int appNotResponding(String paramString1, int paramInt, String paramString2) {
    return 0;
  }
  
  public int appEarlyNotResponding(String paramString1, int paramInt, String paramString2) throws RemoteException {
    return 0;
  }
  
  public int systemNotResponding(String paramString) {
    return -1;
  }
}
