package com.oplus.app;

import android.app.ProgressDialog;
import android.common.OplusFeatureCache;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.widget.TextView;
import com.oplus.theme.IOplusThemeStyle;
import com.oplus.util.OplusContextUtil;
import com.oplus.util.OplusLog;

public class OplusBootMessageDialog extends ProgressDialog {
  private static final boolean DBG = false;
  
  private static final int SPLIT_COUNT = 2;
  
  private static final String TAG = "OplusBootMessageDialog";
  
  private final int mIdProgressPercent;
  
  protected OplusBootMessageDialog(Context paramContext) {
    super(paramContext, ((IOplusThemeStyle)OplusFeatureCache.getOrCreate(IOplusThemeStyle.DEFAULT, new Object[0])).getDialogBootMessageThemeStyle(201523270));
    OplusLog.i("OplusBootMessageDialog", "new OplusBootMessageDialog");
    this.mIdProgressPercent = OplusContextUtil.getResId(getContext(), 201457672);
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent) {
    return true;
  }
  
  public boolean dispatchKeyShortcutEvent(KeyEvent paramKeyEvent) {
    return true;
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent) {
    return true;
  }
  
  public boolean dispatchTrackballEvent(MotionEvent paramMotionEvent) {
    return true;
  }
  
  public boolean dispatchGenericMotionEvent(MotionEvent paramMotionEvent) {
    return true;
  }
  
  public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    return true;
  }
  
  protected void onCreate(Bundle paramBundle) {
    initWindow(getWindow());
    super.onCreate(paramBundle);
  }
  
  public void setMessage(CharSequence paramCharSequence) {
    if (paramCharSequence == null)
      return; 
    String[] arrayOfString = paramCharSequence.toString().split("\\|");
    int i = arrayOfString.length;
    if (i > 2)
      try {
        int j = Integer.parseInt(arrayOfString[0].trim());
        int k = Integer.parseInt(arrayOfString[1].trim());
        if (k > 0) {
          int m = getMax();
          m = m * j / k;
          setProgress(m);
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("setProgress ERROR : current=");
          stringBuilder.append(j);
          stringBuilder.append(", total=");
          stringBuilder.append(k);
          OplusLog.e("OplusBootMessageDialog", stringBuilder.toString());
        } 
      } catch (NumberFormatException numberFormatException) {
        OplusLog.e("OplusBootMessageDialog", numberFormatException.toString());
      }  
    if (i > 0) {
      int j;
      if (isMessage(i)) {
        j = 16908299;
      } else {
        j = this.mIdProgressPercent;
      } 
      String str = arrayOfString[i - 1].trim();
      TextView textView = (TextView)findViewById(j);
      if (textView != null)
        textView.setText(str); 
    } 
  }
  
  public static ProgressDialog create(Context paramContext) {
    OplusBootMessageDialog oplusBootMessageDialog = new OplusBootMessageDialog(paramContext);
    oplusBootMessageDialog.setProgressStyle(1);
    oplusBootMessageDialog.setCancelable(false);
    return oplusBootMessageDialog;
  }
  
  protected void onInitWindowParams(WindowManager.LayoutParams paramLayoutParams) {
    paramLayoutParams.type = 2021;
  }
  
  protected int getWindowFlags() {
    return 1280;
  }
  
  private void initWindow(Window paramWindow) {
    WindowManager.LayoutParams layoutParams = paramWindow.getAttributes();
    layoutParams.gravity = 119;
    layoutParams.screenOrientation = 5;
    onInitWindowParams(layoutParams);
    paramWindow.setAttributes(layoutParams);
    paramWindow.addFlags(getWindowFlags());
  }
  
  private boolean isMessage(int paramInt) {
    boolean bool = true;
    if (paramInt > 2)
      return true; 
    if (getProgress() >= getMax())
      bool = false; 
    return bool;
  }
}
