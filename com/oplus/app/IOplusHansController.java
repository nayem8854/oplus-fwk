package com.oplus.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IOplusHansController extends IInterface {
  void notifyRecordData(List<String> paramList, String paramString) throws RemoteException;
  
  class Default implements IOplusHansController {
    public void notifyRecordData(List<String> param1List, String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusHansController {
    private static final String DESCRIPTOR = "com.oplus.app.IOplusHansController";
    
    static final int TRANSACTION_notifyRecordData = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.app.IOplusHansController");
    }
    
    public static IOplusHansController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.app.IOplusHansController");
      if (iInterface != null && iInterface instanceof IOplusHansController)
        return (IOplusHansController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "notifyRecordData";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.app.IOplusHansController");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.app.IOplusHansController");
      ArrayList<String> arrayList = param1Parcel1.createStringArrayList();
      String str = param1Parcel1.readString();
      notifyRecordData(arrayList, str);
      return true;
    }
    
    private static class Proxy implements IOplusHansController {
      public static IOplusHansController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.app.IOplusHansController";
      }
      
      public void notifyRecordData(List<String> param2List, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.app.IOplusHansController");
          parcel.writeStringList(param2List);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusHansController.Stub.getDefaultImpl() != null) {
            IOplusHansController.Stub.getDefaultImpl().notifyRecordData(param2List, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusHansController param1IOplusHansController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusHansController != null) {
          Proxy.sDefaultImpl = param1IOplusHansController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusHansController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
