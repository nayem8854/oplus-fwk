package com.oplus.app;

import android.content.Intent;
import android.os.Bundle;

public class OplusGameSpaceController extends IOplusGameSpaceController.Stub {
  public void gameStarting(Intent paramIntent, String paramString, boolean paramBoolean) {}
  
  public void gameExiting(String paramString) {}
  
  public void videoStarting(Intent paramIntent, String paramString) {}
  
  public void dispatchGameDock(Bundle paramBundle) {}
  
  public boolean isGameDockAllowed() {
    return true;
  }
}
