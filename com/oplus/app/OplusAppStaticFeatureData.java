package com.oplus.app;

import android.util.ArraySet;

public class OplusAppStaticFeatureData {
  private ArraySet<String> mPermissions = new ArraySet();
  
  private ArraySet<String> mFeatures = new ArraySet();
  
  private ArraySet<String> mActivities = new ArraySet();
  
  private ArraySet<String> mServices = new ArraySet();
  
  private ArraySet<String> mReceivers = new ArraySet();
  
  private ArraySet<String> mProviders = new ArraySet();
  
  private ArraySet<String> mLibraries = new ArraySet();
  
  private ArraySet<String> mProcesses = new ArraySet();
  
  private String mApplicationName;
  
  private float[] mIconHistogram;
  
  private String mPackageName;
  
  private String mSignature;
  
  public void setPackageName(String paramString) {
    this.mPackageName = paramString;
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
  
  public void setSignature(String paramString) {
    this.mSignature = paramString;
  }
  
  public String getSignature() {
    return this.mSignature;
  }
  
  public void setApplicationName(String paramString) {
    this.mApplicationName = paramString;
  }
  
  public String getApplicationName() {
    return this.mApplicationName;
  }
  
  public void setPermissions(ArraySet<String> paramArraySet) {
    this.mPermissions.clear();
    this.mPermissions.addAll(paramArraySet);
  }
  
  public ArraySet<String> getPermissions() {
    return this.mPermissions;
  }
  
  public void setFeatures(ArraySet<String> paramArraySet) {
    this.mFeatures.clear();
    this.mFeatures.addAll(paramArraySet);
  }
  
  public ArraySet<String> getFeatures() {
    return this.mFeatures;
  }
  
  public void setActivities(ArraySet<String> paramArraySet) {
    this.mActivities.clear();
    this.mActivities.addAll(paramArraySet);
  }
  
  public ArraySet<String> getActivities() {
    return this.mActivities;
  }
  
  public void setServices(ArraySet<String> paramArraySet) {
    this.mServices.clear();
    this.mServices.addAll(paramArraySet);
  }
  
  public ArraySet<String> getServices() {
    return this.mServices;
  }
  
  public void setReceivers(ArraySet<String> paramArraySet) {
    this.mReceivers.clear();
    this.mReceivers.addAll(paramArraySet);
  }
  
  public ArraySet<String> getReceivers() {
    return this.mReceivers;
  }
  
  public void setProviders(ArraySet<String> paramArraySet) {
    this.mProviders.clear();
    this.mProviders.addAll(paramArraySet);
  }
  
  public ArraySet<String> getProviders() {
    return this.mProviders;
  }
  
  public void setLibraries(ArraySet<String> paramArraySet) {
    this.mLibraries.clear();
    this.mLibraries.addAll(paramArraySet);
  }
  
  public ArraySet<String> getLibraries() {
    return this.mLibraries;
  }
  
  public void setProcesses(ArraySet<String> paramArraySet) {
    this.mProcesses.clear();
    this.mProcesses.addAll(paramArraySet);
  }
  
  public ArraySet<String> getProcesses() {
    return this.mProcesses;
  }
  
  public void setIconHistogram(float[] paramArrayOffloat) {
    this.mIconHistogram = paramArrayOffloat;
  }
  
  public float[] getIconHistogram() {
    return this.mIconHistogram;
  }
}
