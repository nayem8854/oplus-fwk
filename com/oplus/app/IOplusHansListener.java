package com.oplus.app;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusHansListener extends IInterface {
  void notifyRecordData(Bundle paramBundle, String paramString) throws RemoteException;
  
  class Default implements IOplusHansListener {
    public void notifyRecordData(Bundle param1Bundle, String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusHansListener {
    private static final String DESCRIPTOR = "com.oplus.app.IOplusHansListener";
    
    static final int TRANSACTION_notifyRecordData = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.app.IOplusHansListener");
    }
    
    public static IOplusHansListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.app.IOplusHansListener");
      if (iInterface != null && iInterface instanceof IOplusHansListener)
        return (IOplusHansListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "notifyRecordData";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.app.IOplusHansListener");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.app.IOplusHansListener");
      if (param1Parcel1.readInt() != 0) {
        Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      String str = param1Parcel1.readString();
      notifyRecordData((Bundle)param1Parcel2, str);
      return true;
    }
    
    private static class Proxy implements IOplusHansListener {
      public static IOplusHansListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.app.IOplusHansListener";
      }
      
      public void notifyRecordData(Bundle param2Bundle, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.app.IOplusHansListener");
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusHansListener.Stub.getDefaultImpl() != null) {
            IOplusHansListener.Stub.getDefaultImpl().notifyRecordData(param2Bundle, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusHansListener param1IOplusHansListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusHansListener != null) {
          Proxy.sDefaultImpl = param1IOplusHansListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusHansListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
