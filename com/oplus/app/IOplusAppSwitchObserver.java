package com.oplus.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusAppSwitchObserver extends IInterface {
  void onActivityEnter(OplusAppEnterInfo paramOplusAppEnterInfo) throws RemoteException;
  
  void onActivityExit(OplusAppExitInfo paramOplusAppExitInfo) throws RemoteException;
  
  void onAppEnter(OplusAppEnterInfo paramOplusAppEnterInfo) throws RemoteException;
  
  void onAppExit(OplusAppExitInfo paramOplusAppExitInfo) throws RemoteException;
  
  class Default implements IOplusAppSwitchObserver {
    public void onAppEnter(OplusAppEnterInfo param1OplusAppEnterInfo) throws RemoteException {}
    
    public void onAppExit(OplusAppExitInfo param1OplusAppExitInfo) throws RemoteException {}
    
    public void onActivityEnter(OplusAppEnterInfo param1OplusAppEnterInfo) throws RemoteException {}
    
    public void onActivityExit(OplusAppExitInfo param1OplusAppExitInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusAppSwitchObserver {
    private static final String DESCRIPTOR = "com.oplus.app.IOplusAppSwitchObserver";
    
    static final int TRANSACTION_onActivityEnter = 3;
    
    static final int TRANSACTION_onActivityExit = 4;
    
    static final int TRANSACTION_onAppEnter = 1;
    
    static final int TRANSACTION_onAppExit = 2;
    
    public Stub() {
      attachInterface(this, "com.oplus.app.IOplusAppSwitchObserver");
    }
    
    public static IOplusAppSwitchObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.app.IOplusAppSwitchObserver");
      if (iInterface != null && iInterface instanceof IOplusAppSwitchObserver)
        return (IOplusAppSwitchObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onActivityExit";
          } 
          return "onActivityEnter";
        } 
        return "onAppExit";
      } 
      return "onAppEnter";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("com.oplus.app.IOplusAppSwitchObserver");
              return true;
            } 
            param1Parcel1.enforceInterface("com.oplus.app.IOplusAppSwitchObserver");
            if (param1Parcel1.readInt() != 0) {
              OplusAppExitInfo oplusAppExitInfo = (OplusAppExitInfo)OplusAppExitInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onActivityExit((OplusAppExitInfo)param1Parcel1);
            return true;
          } 
          param1Parcel1.enforceInterface("com.oplus.app.IOplusAppSwitchObserver");
          if (param1Parcel1.readInt() != 0) {
            OplusAppEnterInfo oplusAppEnterInfo = (OplusAppEnterInfo)OplusAppEnterInfo.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          onActivityEnter((OplusAppEnterInfo)param1Parcel1);
          return true;
        } 
        param1Parcel1.enforceInterface("com.oplus.app.IOplusAppSwitchObserver");
        if (param1Parcel1.readInt() != 0) {
          OplusAppExitInfo oplusAppExitInfo = (OplusAppExitInfo)OplusAppExitInfo.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onAppExit((OplusAppExitInfo)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.app.IOplusAppSwitchObserver");
      if (param1Parcel1.readInt() != 0) {
        OplusAppEnterInfo oplusAppEnterInfo = (OplusAppEnterInfo)OplusAppEnterInfo.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onAppEnter((OplusAppEnterInfo)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IOplusAppSwitchObserver {
      public static IOplusAppSwitchObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.app.IOplusAppSwitchObserver";
      }
      
      public void onAppEnter(OplusAppEnterInfo param2OplusAppEnterInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.app.IOplusAppSwitchObserver");
          if (param2OplusAppEnterInfo != null) {
            parcel.writeInt(1);
            param2OplusAppEnterInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusAppSwitchObserver.Stub.getDefaultImpl() != null) {
            IOplusAppSwitchObserver.Stub.getDefaultImpl().onAppEnter(param2OplusAppEnterInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAppExit(OplusAppExitInfo param2OplusAppExitInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.app.IOplusAppSwitchObserver");
          if (param2OplusAppExitInfo != null) {
            parcel.writeInt(1);
            param2OplusAppExitInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IOplusAppSwitchObserver.Stub.getDefaultImpl() != null) {
            IOplusAppSwitchObserver.Stub.getDefaultImpl().onAppExit(param2OplusAppExitInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onActivityEnter(OplusAppEnterInfo param2OplusAppEnterInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.app.IOplusAppSwitchObserver");
          if (param2OplusAppEnterInfo != null) {
            parcel.writeInt(1);
            param2OplusAppEnterInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IOplusAppSwitchObserver.Stub.getDefaultImpl() != null) {
            IOplusAppSwitchObserver.Stub.getDefaultImpl().onActivityEnter(param2OplusAppEnterInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onActivityExit(OplusAppExitInfo param2OplusAppExitInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.app.IOplusAppSwitchObserver");
          if (param2OplusAppExitInfo != null) {
            parcel.writeInt(1);
            param2OplusAppExitInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IOplusAppSwitchObserver.Stub.getDefaultImpl() != null) {
            IOplusAppSwitchObserver.Stub.getDefaultImpl().onActivityExit(param2OplusAppExitInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusAppSwitchObserver param1IOplusAppSwitchObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusAppSwitchObserver != null) {
          Proxy.sDefaultImpl = param1IOplusAppSwitchObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusAppSwitchObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
