package com.oplus.app;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public final class OplusAppEnterInfo implements Parcelable {
  public OplusAppEnterInfo() {
    this.extension = new Bundle();
  }
  
  public OplusAppEnterInfo(Parcel paramParcel) {
    boolean bool2;
    this.extension = new Bundle();
    this.intent = (Intent)paramParcel.readParcelable(Intent.class.getClassLoader());
    this.windowMode = paramParcel.readInt();
    this.targetName = paramParcel.readString();
    byte b = paramParcel.readByte();
    boolean bool1 = true;
    if (b != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.multiApp = bool2;
    if (paramParcel.readByte() != 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.firstStart = bool2;
    this.launchedFromPackage = paramParcel.readString();
    this.extension = paramParcel.readBundle();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.intent, paramInt);
    paramParcel.writeInt(this.windowMode);
    paramParcel.writeString(this.targetName);
    paramParcel.writeByte((byte)this.multiApp);
    paramParcel.writeByte((byte)this.firstStart);
    paramParcel.writeString(this.launchedFromPackage);
    paramParcel.writeBundle(this.extension);
  }
  
  public static final Parcelable.Creator<OplusAppEnterInfo> CREATOR = new Parcelable.Creator<OplusAppEnterInfo>() {
      public OplusAppEnterInfo createFromParcel(Parcel param1Parcel) {
        return new OplusAppEnterInfo(param1Parcel);
      }
      
      public OplusAppEnterInfo[] newArray(int param1Int) {
        return new OplusAppEnterInfo[param1Int];
      }
    };
  
  public static final int SWITCH_TYPE_ACTIVITY = 1;
  
  public static final int SWITCH_TYPE_APP = 2;
  
  public Bundle extension;
  
  public boolean firstStart;
  
  public Intent intent;
  
  public String launchedFromPackage;
  
  public boolean multiApp;
  
  public String targetName;
  
  public int windowMode;
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("OplusAppEnterInfo = { ");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" windowMode = ");
    stringBuilder2.append(this.windowMode);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" targetName = ");
    stringBuilder2.append(this.targetName);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" multiApp = ");
    stringBuilder2.append(this.multiApp);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" firstStart = ");
    stringBuilder2.append(this.firstStart);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" launchedFromPackage = ");
    stringBuilder2.append(this.launchedFromPackage);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" intent = ");
    stringBuilder2.append(this.intent);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" extension = ");
    stringBuilder2.append(this.extension);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append("}");
    return stringBuilder1.toString();
  }
}
