package com.oplus.app;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.view.View;

public interface IOplusAppDynamicFeatureManager extends IOplusCommonFeature {
  public static final IOplusAppDynamicFeatureManager DEFAULT = (IOplusAppDynamicFeatureManager)new Object();
  
  default IOplusAppDynamicFeatureManager getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusAppDynamicFeatureManager;
  }
  
  default void parseAppDynamicInfo(String paramString1, String paramString2, View paramView) {}
}
