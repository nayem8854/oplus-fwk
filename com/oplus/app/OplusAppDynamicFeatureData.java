package com.oplus.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.ArrayMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OplusAppDynamicFeatureData implements Parcelable {
  public Map<String, Integer> mComponentNames = (Map<String, Integer>)new ArrayMap();
  
  public Map<String, Integer> mIdNames = (Map<String, Integer>)new ArrayMap();
  
  public List<String> mDatabaseNames = new ArrayList<>();
  
  public List<String> mTexts = new ArrayList<>();
  
  public OplusAppDynamicFeatureData(OplusAppDynamicFeatureData paramOplusAppDynamicFeatureData) {
    if (paramOplusAppDynamicFeatureData != null) {
      this.mPackageName = paramOplusAppDynamicFeatureData.mPackageName;
      this.mActivityName = paramOplusAppDynamicFeatureData.mActivityName;
      this.mComponentNames.putAll(paramOplusAppDynamicFeatureData.mComponentNames);
      this.mIdNames.putAll(paramOplusAppDynamicFeatureData.mIdNames);
      this.mDatabaseNames.addAll(paramOplusAppDynamicFeatureData.mDatabaseNames);
      this.mTexts.addAll(paramOplusAppDynamicFeatureData.mTexts);
    } 
  }
  
  protected OplusAppDynamicFeatureData(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mPackageName = paramParcel.readString();
    this.mActivityName = paramParcel.readString();
    paramParcel.readMap(this.mComponentNames, Map.class.getClassLoader());
    paramParcel.readMap(this.mIdNames, Map.class.getClassLoader());
    paramParcel.readStringList(this.mDatabaseNames);
    paramParcel.readStringList(this.mTexts);
  }
  
  public static final Parcelable.Creator<OplusAppDynamicFeatureData> CREATOR = new Parcelable.Creator<OplusAppDynamicFeatureData>() {
      public OplusAppDynamicFeatureData createFromParcel(Parcel param1Parcel) {
        return new OplusAppDynamicFeatureData(param1Parcel);
      }
      
      public OplusAppDynamicFeatureData[] newArray(int param1Int) {
        return new OplusAppDynamicFeatureData[param1Int];
      }
    };
  
  public String mActivityName;
  
  public String mPackageName;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mPackageName);
    paramParcel.writeString(this.mActivityName);
    paramParcel.writeMap(this.mComponentNames);
    paramParcel.writeMap(this.mIdNames);
    paramParcel.writeStringList(this.mDatabaseNames);
    paramParcel.writeStringList(this.mTexts);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("OplusAppDynamicFeatureData{package=");
    stringBuilder.append(this.mPackageName);
    stringBuilder.append(", activity=");
    stringBuilder.append(this.mActivityName);
    stringBuilder.append(", componentName=");
    stringBuilder.append(this.mComponentNames);
    stringBuilder.append(", idName=");
    stringBuilder.append(this.mIdNames);
    stringBuilder.append(", databaseName=");
    stringBuilder.append(this.mDatabaseNames);
    stringBuilder.append(", text=");
    stringBuilder.append(this.mTexts);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void setPackageName(String paramString) {
    this.mPackageName = paramString;
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
  
  public void setActivityName(String paramString) {
    this.mActivityName = paramString;
  }
  
  public String getActivityName() {
    return this.mActivityName;
  }
  
  public void setComponentNames(Map<String, Integer> paramMap) {
    this.mComponentNames.clear();
    this.mComponentNames.putAll(paramMap);
  }
  
  public Map<String, Integer> getComponentNames() {
    return this.mComponentNames;
  }
  
  public void setIdNames(Map<String, Integer> paramMap) {
    this.mIdNames.clear();
    this.mIdNames.putAll(paramMap);
  }
  
  public Map<String, Integer> getIdNames() {
    return this.mIdNames;
  }
  
  public void setDatabaseNames(List<String> paramList) {
    this.mDatabaseNames.clear();
    this.mDatabaseNames.addAll(paramList);
  }
  
  public List<String> getDatabaseNames() {
    return this.mDatabaseNames;
  }
  
  public void setTexts(List<String> paramList) {
    this.mTexts.clear();
    this.mTexts.addAll(paramList);
  }
  
  public List<String> getTexts() {
    return this.mTexts;
  }
  
  public OplusAppDynamicFeatureData() {}
}
