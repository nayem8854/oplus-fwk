package com.oplus.app;

import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusGameSpaceController extends IInterface {
  void dispatchGameDock(Bundle paramBundle) throws RemoteException;
  
  void gameExiting(String paramString) throws RemoteException;
  
  void gameStarting(Intent paramIntent, String paramString, boolean paramBoolean) throws RemoteException;
  
  boolean isGameDockAllowed() throws RemoteException;
  
  void videoStarting(Intent paramIntent, String paramString) throws RemoteException;
  
  class Default implements IOplusGameSpaceController {
    public void gameStarting(Intent param1Intent, String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void gameExiting(String param1String) throws RemoteException {}
    
    public void videoStarting(Intent param1Intent, String param1String) throws RemoteException {}
    
    public void dispatchGameDock(Bundle param1Bundle) throws RemoteException {}
    
    public boolean isGameDockAllowed() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusGameSpaceController {
    private static final String DESCRIPTOR = "com.oplus.app.IOplusGameSpaceController";
    
    static final int TRANSACTION_dispatchGameDock = 4;
    
    static final int TRANSACTION_gameExiting = 2;
    
    static final int TRANSACTION_gameStarting = 1;
    
    static final int TRANSACTION_isGameDockAllowed = 5;
    
    static final int TRANSACTION_videoStarting = 3;
    
    public Stub() {
      attachInterface(this, "com.oplus.app.IOplusGameSpaceController");
    }
    
    public static IOplusGameSpaceController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.app.IOplusGameSpaceController");
      if (iInterface != null && iInterface instanceof IOplusGameSpaceController)
        return (IOplusGameSpaceController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "isGameDockAllowed";
            } 
            return "dispatchGameDock";
          } 
          return "videoStarting";
        } 
        return "gameExiting";
      } 
      return "gameStarting";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str1;
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("com.oplus.app.IOplusGameSpaceController");
                return true;
              } 
              param1Parcel1.enforceInterface("com.oplus.app.IOplusGameSpaceController");
              boolean bool1 = isGameDockAllowed();
              param1Parcel2.writeNoException();
              param1Parcel2.writeInt(bool1);
              return true;
            } 
            param1Parcel1.enforceInterface("com.oplus.app.IOplusGameSpaceController");
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            dispatchGameDock((Bundle)param1Parcel1);
            return true;
          } 
          param1Parcel1.enforceInterface("com.oplus.app.IOplusGameSpaceController");
          if (param1Parcel1.readInt() != 0) {
            Intent intent = (Intent)Intent.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel2 = null;
          } 
          str1 = param1Parcel1.readString();
          videoStarting((Intent)param1Parcel2, str1);
          return true;
        } 
        str1.enforceInterface("com.oplus.app.IOplusGameSpaceController");
        str1 = str1.readString();
        gameExiting(str1);
        return true;
      } 
      str1.enforceInterface("com.oplus.app.IOplusGameSpaceController");
      if (str1.readInt() != 0) {
        Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)str1);
      } else {
        param1Parcel2 = null;
      } 
      String str2 = str1.readString();
      if (str1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      gameStarting((Intent)param1Parcel2, str2, bool);
      return true;
    }
    
    private static class Proxy implements IOplusGameSpaceController {
      public static IOplusGameSpaceController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.app.IOplusGameSpaceController";
      }
      
      public void gameStarting(Intent param2Intent, String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.app.IOplusGameSpaceController");
          boolean bool = false;
          if (param2Intent != null) {
            parcel.writeInt(1);
            param2Intent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeString(param2String);
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && IOplusGameSpaceController.Stub.getDefaultImpl() != null) {
            IOplusGameSpaceController.Stub.getDefaultImpl().gameStarting(param2Intent, param2String, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void gameExiting(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.app.IOplusGameSpaceController");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IOplusGameSpaceController.Stub.getDefaultImpl() != null) {
            IOplusGameSpaceController.Stub.getDefaultImpl().gameExiting(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void videoStarting(Intent param2Intent, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.app.IOplusGameSpaceController");
          if (param2Intent != null) {
            parcel.writeInt(1);
            param2Intent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IOplusGameSpaceController.Stub.getDefaultImpl() != null) {
            IOplusGameSpaceController.Stub.getDefaultImpl().videoStarting(param2Intent, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dispatchGameDock(Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.app.IOplusGameSpaceController");
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IOplusGameSpaceController.Stub.getDefaultImpl() != null) {
            IOplusGameSpaceController.Stub.getDefaultImpl().dispatchGameDock(param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean isGameDockAllowed() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.app.IOplusGameSpaceController");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IOplusGameSpaceController.Stub.getDefaultImpl() != null) {
            bool1 = IOplusGameSpaceController.Stub.getDefaultImpl().isGameDockAllowed();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusGameSpaceController param1IOplusGameSpaceController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusGameSpaceController != null) {
          Proxy.sDefaultImpl = param1IOplusGameSpaceController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusGameSpaceController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
