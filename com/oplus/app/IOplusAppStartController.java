package com.oplus.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusAppStartController extends IInterface {
  void appStartMonitor(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5) throws RemoteException;
  
  void notifyPreventIndulge(String paramString) throws RemoteException;
  
  void preventStartMonitor(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5) throws RemoteException;
  
  class Default implements IOplusAppStartController {
    public void appStartMonitor(String param1String1, String param1String2, String param1String3, String param1String4, String param1String5) throws RemoteException {}
    
    public void preventStartMonitor(String param1String1, String param1String2, String param1String3, String param1String4, String param1String5) throws RemoteException {}
    
    public void notifyPreventIndulge(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusAppStartController {
    private static final String DESCRIPTOR = "com.oplus.app.IOplusAppStartController";
    
    static final int TRANSACTION_appStartMonitor = 1;
    
    static final int TRANSACTION_notifyPreventIndulge = 3;
    
    static final int TRANSACTION_preventStartMonitor = 2;
    
    public Stub() {
      attachInterface(this, "com.oplus.app.IOplusAppStartController");
    }
    
    public static IOplusAppStartController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.app.IOplusAppStartController");
      if (iInterface != null && iInterface instanceof IOplusAppStartController)
        return (IOplusAppStartController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "notifyPreventIndulge";
        } 
        return "preventStartMonitor";
      } 
      return "appStartMonitor";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.oplus.app.IOplusAppStartController");
            return true;
          } 
          param1Parcel1.enforceInterface("com.oplus.app.IOplusAppStartController");
          str1 = param1Parcel1.readString();
          notifyPreventIndulge(str1);
          return true;
        } 
        str1.enforceInterface("com.oplus.app.IOplusAppStartController");
        String str7 = str1.readString();
        String str8 = str1.readString();
        String str6 = str1.readString();
        String str9 = str1.readString();
        str1 = str1.readString();
        preventStartMonitor(str7, str8, str6, str9, str1);
        return true;
      } 
      str1.enforceInterface("com.oplus.app.IOplusAppStartController");
      String str2 = str1.readString();
      String str4 = str1.readString();
      String str3 = str1.readString();
      String str5 = str1.readString();
      String str1 = str1.readString();
      appStartMonitor(str2, str4, str3, str5, str1);
      return true;
    }
    
    private static class Proxy implements IOplusAppStartController {
      public static IOplusAppStartController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.app.IOplusAppStartController";
      }
      
      public void appStartMonitor(String param2String1, String param2String2, String param2String3, String param2String4, String param2String5) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.app.IOplusAppStartController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeString(param2String3);
          parcel.writeString(param2String4);
          parcel.writeString(param2String5);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusAppStartController.Stub.getDefaultImpl() != null) {
            IOplusAppStartController.Stub.getDefaultImpl().appStartMonitor(param2String1, param2String2, param2String3, param2String4, param2String5);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void preventStartMonitor(String param2String1, String param2String2, String param2String3, String param2String4, String param2String5) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.app.IOplusAppStartController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeString(param2String3);
          parcel.writeString(param2String4);
          parcel.writeString(param2String5);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IOplusAppStartController.Stub.getDefaultImpl() != null) {
            IOplusAppStartController.Stub.getDefaultImpl().preventStartMonitor(param2String1, param2String2, param2String3, param2String4, param2String5);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyPreventIndulge(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.app.IOplusAppStartController");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IOplusAppStartController.Stub.getDefaultImpl() != null) {
            IOplusAppStartController.Stub.getDefaultImpl().notifyPreventIndulge(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusAppStartController param1IOplusAppStartController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusAppStartController != null) {
          Proxy.sDefaultImpl = param1IOplusAppStartController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusAppStartController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
