package com.oplus.app;

import android.os.Parcel;
import android.os.Parcelable;

public class OplusAlarmInfo implements Parcelable {
  public OplusAlarmInfo() {}
  
  public OplusAlarmInfo(Parcel paramParcel) {
    this.type = paramParcel.readInt();
    this.timeStamp = paramParcel.readLong();
    this.packageName = paramParcel.readString();
    this.callingUid = paramParcel.readInt();
    this.callingPid = paramParcel.readInt();
    this.statsTag = paramParcel.readString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.type);
    paramParcel.writeLong(this.timeStamp);
    paramParcel.writeString(this.packageName);
    paramParcel.writeInt(this.callingUid);
    paramParcel.writeInt(this.callingPid);
    paramParcel.writeString(this.statsTag);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<OplusAlarmInfo> CREATOR = new Parcelable.Creator<OplusAlarmInfo>() {
      public OplusAlarmInfo createFromParcel(Parcel param1Parcel) {
        return new OplusAlarmInfo(param1Parcel);
      }
      
      public OplusAlarmInfo[] newArray(int param1Int) {
        return new OplusAlarmInfo[param1Int];
      }
    };
  
  public int callingPid;
  
  public int callingUid;
  
  public String packageName;
  
  public String statsTag;
  
  public long timeStamp;
  
  public int type;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("AlarmInfo{type=");
    stringBuilder.append(this.type);
    stringBuilder.append(", timeStamp=");
    stringBuilder.append(this.timeStamp);
    stringBuilder.append(", packageName='");
    stringBuilder.append(this.packageName);
    stringBuilder.append('\'');
    stringBuilder.append(", callingUid=");
    stringBuilder.append(this.callingUid);
    stringBuilder.append(", callingPid=");
    stringBuilder.append(this.callingPid);
    stringBuilder.append(", statsTag='");
    stringBuilder.append(this.statsTag);
    stringBuilder.append('\'');
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
}
