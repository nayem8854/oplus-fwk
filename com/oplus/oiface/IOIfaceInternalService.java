package com.oplus.oiface;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOIfaceInternalService extends IInterface {
  void bindGameTask(int paramInt1, int paramInt2) throws RemoteException;
  
  void currentNetwork(int paramInt) throws RemoteException;
  
  void currentPkgStatus(int paramInt, String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  void enableHQV(int paramInt) throws RemoteException;
  
  void enableHapticScreenCaptureService(int paramInt) throws RemoteException;
  
  String generalOifaceSignal(String paramString) throws RemoteException;
  
  String getAllLoadInfo(String paramString) throws RemoteException;
  
  float getBatteryCurrentNow() throws RemoteException;
  
  int getBatteryFCC() throws RemoteException;
  
  int getBatteryRemain() throws RemoteException;
  
  String getChipName() throws RemoteException;
  
  long[] getCpuAvailableFreqTable(int paramInt) throws RemoteException;
  
  int[] getCpuClusterInfo() throws RemoteException;
  
  int getCpuClusterNum() throws RemoteException;
  
  long[] getCpuCurrentFreq(int paramInt) throws RemoteException;
  
  long[] getCpuLimitedFreqs(int paramInt) throws RemoteException;
  
  float[] getCpuLoads(int paramInt) throws RemoteException;
  
  String getCpuTimeInState() throws RemoteException;
  
  String getCurrentGamePackage() throws RemoteException;
  
  String getDeviceID() throws RemoteException;
  
  int getFPS(String paramString, int paramInt) throws RemoteException;
  
  float[] getGPASystemInfo() throws RemoteException;
  
  int getGameModeStatus() throws RemoteException;
  
  long[] getGpuAvailableFreqTable() throws RemoteException;
  
  long getGpuCurrentFreq() throws RemoteException;
  
  long[] getGpuLimitedFreqs() throws RemoteException;
  
  float getGpuLoad() throws RemoteException;
  
  String[] getInstalledGameList() throws RemoteException;
  
  int getSuperVOOCStatus() throws RemoteException;
  
  String getSupportGameStartPackage() throws RemoteException;
  
  float[] getThermalTemps(int paramInt) throws RemoteException;
  
  void notifyScreenEvent(int paramInt) throws RemoteException;
  
  void oifaceControl(String paramString) throws RemoteException;
  
  void oifaceDecision(String paramString) throws RemoteException;
  
  int registerClientThroughCosa(IOIfaceCallback paramIOIfaceCallback, String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void registerGameRoleListener(int paramInt, String paramString, IOIfaceCallback paramIOIfaceCallback) throws RemoteException;
  
  String registerHQV(String paramString1, int paramInt, String paramString2) throws RemoteException;
  
  void registerNetworkListener(int paramInt1, int paramInt2, IOIfaceCallback paramIOIfaceCallback) throws RemoteException;
  
  void registerOifaceCallback(IOIfaceCallback paramIOIfaceCallback) throws RemoteException;
  
  void setCoolExFilterType(int paramInt, String paramString) throws RemoteException;
  
  String setGCPEffectMode(int paramInt) throws RemoteException;
  
  void setGameModeStatus(int paramInt, String paramString) throws RemoteException;
  
  String setGeneralSignalCosa(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void setGyroscopeLevel(int paramInt) throws RemoteException;
  
  void setHalfHQV(int paramInt) throws RemoteException;
  
  void setInstalledGameList(String[] paramArrayOfString) throws RemoteException;
  
  void setPerfMode(int paramInt) throws RemoteException;
  
  void setTouchProtection(boolean paramBoolean) throws RemoteException;
  
  void setTouchResponsiveness(int paramInt) throws RemoteException;
  
  void setTouchSensibility(int paramInt) throws RemoteException;
  
  String triggerFrameStat(String paramString1, String paramString2) throws RemoteException;
  
  void unRegisterGameRoleListener(IOIfaceCallback paramIOIfaceCallback) throws RemoteException;
  
  class Default implements IOIfaceInternalService {
    public void currentNetwork(int param1Int) throws RemoteException {}
    
    public void bindGameTask(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void enableHQV(int param1Int) throws RemoteException {}
    
    public String registerHQV(String param1String1, int param1Int, String param1String2) throws RemoteException {
      return null;
    }
    
    public void setHalfHQV(int param1Int) throws RemoteException {}
    
    public int registerClientThroughCosa(IOIfaceCallback param1IOIfaceCallback, String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public String setGeneralSignalCosa(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public void registerNetworkListener(int param1Int1, int param1Int2, IOIfaceCallback param1IOIfaceCallback) throws RemoteException {}
    
    public void registerGameRoleListener(int param1Int, String param1String, IOIfaceCallback param1IOIfaceCallback) throws RemoteException {}
    
    public void unRegisterGameRoleListener(IOIfaceCallback param1IOIfaceCallback) throws RemoteException {}
    
    public String getSupportGameStartPackage() throws RemoteException {
      return null;
    }
    
    public String getAllLoadInfo(String param1String) throws RemoteException {
      return null;
    }
    
    public String setGCPEffectMode(int param1Int) throws RemoteException {
      return null;
    }
    
    public void setPerfMode(int param1Int) throws RemoteException {}
    
    public void notifyScreenEvent(int param1Int) throws RemoteException {}
    
    public String getDeviceID() throws RemoteException {
      return null;
    }
    
    public void currentPkgStatus(int param1Int, String param1String1, String param1String2, String param1String3) throws RemoteException {}
    
    public int getFPS(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public String generalOifaceSignal(String param1String) throws RemoteException {
      return null;
    }
    
    public void oifaceDecision(String param1String) throws RemoteException {}
    
    public void oifaceControl(String param1String) throws RemoteException {}
    
    public int getCpuClusterNum() throws RemoteException {
      return 0;
    }
    
    public long[] getCpuAvailableFreqTable(int param1Int) throws RemoteException {
      return null;
    }
    
    public long[] getCpuLimitedFreqs(int param1Int) throws RemoteException {
      return null;
    }
    
    public long[] getCpuCurrentFreq(int param1Int) throws RemoteException {
      return null;
    }
    
    public float[] getCpuLoads(int param1Int) throws RemoteException {
      return null;
    }
    
    public long[] getGpuAvailableFreqTable() throws RemoteException {
      return null;
    }
    
    public long[] getGpuLimitedFreqs() throws RemoteException {
      return null;
    }
    
    public long getGpuCurrentFreq() throws RemoteException {
      return 0L;
    }
    
    public float getGpuLoad() throws RemoteException {
      return 0.0F;
    }
    
    public float[] getThermalTemps(int param1Int) throws RemoteException {
      return null;
    }
    
    public void enableHapticScreenCaptureService(int param1Int) throws RemoteException {}
    
    public void setTouchSensibility(int param1Int) throws RemoteException {}
    
    public void setTouchResponsiveness(int param1Int) throws RemoteException {}
    
    public void setGyroscopeLevel(int param1Int) throws RemoteException {}
    
    public void setTouchProtection(boolean param1Boolean) throws RemoteException {}
    
    public void registerOifaceCallback(IOIfaceCallback param1IOIfaceCallback) throws RemoteException {}
    
    public int getBatteryRemain() throws RemoteException {
      return 0;
    }
    
    public float getBatteryCurrentNow() throws RemoteException {
      return 0.0F;
    }
    
    public int getSuperVOOCStatus() throws RemoteException {
      return 0;
    }
    
    public float[] getGPASystemInfo() throws RemoteException {
      return null;
    }
    
    public void setCoolExFilterType(int param1Int, String param1String) throws RemoteException {}
    
    public void setGameModeStatus(int param1Int, String param1String) throws RemoteException {}
    
    public int getGameModeStatus() throws RemoteException {
      return 0;
    }
    
    public String getCurrentGamePackage() throws RemoteException {
      return null;
    }
    
    public void setInstalledGameList(String[] param1ArrayOfString) throws RemoteException {}
    
    public String[] getInstalledGameList() throws RemoteException {
      return null;
    }
    
    public String getCpuTimeInState() throws RemoteException {
      return null;
    }
    
    public String triggerFrameStat(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getChipName() throws RemoteException {
      return null;
    }
    
    public int[] getCpuClusterInfo() throws RemoteException {
      return null;
    }
    
    public int getBatteryFCC() throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOIfaceInternalService {
    private static final String DESCRIPTOR = "com.oppo.oiface.IOIfaceService";
    
    static final int TRANSACTION_bindGameTask = 7;
    
    static final int TRANSACTION_currentNetwork = 1;
    
    static final int TRANSACTION_currentPkgStatus = 803;
    
    static final int TRANSACTION_enableHQV = 11;
    
    static final int TRANSACTION_enableHapticScreenCaptureService = 901;
    
    static final int TRANSACTION_generalOifaceSignal = 852;
    
    static final int TRANSACTION_getAllLoadInfo = 212;
    
    static final int TRANSACTION_getBatteryCurrentNow = 1003;
    
    static final int TRANSACTION_getBatteryFCC = 1016;
    
    static final int TRANSACTION_getBatteryRemain = 1002;
    
    static final int TRANSACTION_getChipName = 1014;
    
    static final int TRANSACTION_getCpuAvailableFreqTable = 856;
    
    static final int TRANSACTION_getCpuClusterInfo = 1015;
    
    static final int TRANSACTION_getCpuClusterNum = 855;
    
    static final int TRANSACTION_getCpuCurrentFreq = 858;
    
    static final int TRANSACTION_getCpuLimitedFreqs = 857;
    
    static final int TRANSACTION_getCpuLoads = 859;
    
    static final int TRANSACTION_getCpuTimeInState = 1012;
    
    static final int TRANSACTION_getCurrentGamePackage = 1009;
    
    static final int TRANSACTION_getDeviceID = 504;
    
    static final int TRANSACTION_getFPS = 851;
    
    static final int TRANSACTION_getGPASystemInfo = 1005;
    
    static final int TRANSACTION_getGameModeStatus = 1008;
    
    static final int TRANSACTION_getGpuAvailableFreqTable = 860;
    
    static final int TRANSACTION_getGpuCurrentFreq = 862;
    
    static final int TRANSACTION_getGpuLimitedFreqs = 861;
    
    static final int TRANSACTION_getGpuLoad = 863;
    
    static final int TRANSACTION_getInstalledGameList = 1011;
    
    static final int TRANSACTION_getSuperVOOCStatus = 1004;
    
    static final int TRANSACTION_getSupportGameStartPackage = 206;
    
    static final int TRANSACTION_getThermalTemps = 864;
    
    static final int TRANSACTION_notifyScreenEvent = 503;
    
    static final int TRANSACTION_oifaceControl = 854;
    
    static final int TRANSACTION_oifaceDecision = 853;
    
    static final int TRANSACTION_registerClientThroughCosa = 161;
    
    static final int TRANSACTION_registerGameRoleListener = 204;
    
    static final int TRANSACTION_registerHQV = 12;
    
    static final int TRANSACTION_registerNetworkListener = 202;
    
    static final int TRANSACTION_registerOifaceCallback = 1001;
    
    static final int TRANSACTION_setCoolExFilterType = 1006;
    
    static final int TRANSACTION_setGCPEffectMode = 211;
    
    static final int TRANSACTION_setGameModeStatus = 1007;
    
    static final int TRANSACTION_setGeneralSignalCosa = 162;
    
    static final int TRANSACTION_setGyroscopeLevel = 904;
    
    static final int TRANSACTION_setHalfHQV = 13;
    
    static final int TRANSACTION_setInstalledGameList = 1010;
    
    static final int TRANSACTION_setPerfMode = 502;
    
    static final int TRANSACTION_setTouchProtection = 905;
    
    static final int TRANSACTION_setTouchResponsiveness = 903;
    
    static final int TRANSACTION_setTouchSensibility = 902;
    
    static final int TRANSACTION_triggerFrameStat = 1013;
    
    static final int TRANSACTION_unRegisterGameRoleListener = 205;
    
    public Stub() {
      attachInterface(this, "com.oppo.oiface.IOIfaceService");
    }
    
    public static IOIfaceInternalService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oppo.oiface.IOIfaceService");
      if (iInterface != null && iInterface instanceof IOIfaceInternalService)
        return (IOIfaceInternalService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IOIfaceCallback iOIfaceCallback;
      if (param1Int1 != 1) {
        if (param1Int1 != 7) {
          String str;
          if (param1Int1 != 202) {
            if (param1Int1 != 803) {
              if (param1Int1 != 1598968902) {
                if (param1Int1 != 161) {
                  if (param1Int1 != 162) {
                    if (param1Int1 != 211) {
                      if (param1Int1 != 212) {
                        int[] arrayOfInt;
                        String str7, arrayOfString[], str6;
                        float[] arrayOfFloat3;
                        IOIfaceCallback iOIfaceCallback3;
                        float[] arrayOfFloat2;
                        long[] arrayOfLong2;
                        float[] arrayOfFloat1;
                        long[] arrayOfLong1;
                        String str5;
                        IOIfaceCallback iOIfaceCallback2;
                        String str8;
                        float f;
                        boolean bool;
                        long l;
                        switch (param1Int1) {
                          default:
                            switch (param1Int1) {
                              default:
                                switch (param1Int1) {
                                  default:
                                    switch (param1Int1) {
                                      default:
                                        switch (param1Int1) {
                                          default:
                                            switch (param1Int1) {
                                              default:
                                                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
                                              case 1016:
                                                param1Parcel1.enforceInterface("com.oppo.oiface.IOIfaceService");
                                                param1Int1 = getBatteryFCC();
                                                param1Parcel2.writeNoException();
                                                param1Parcel2.writeInt(param1Int1);
                                                return true;
                                              case 1015:
                                                param1Parcel1.enforceInterface("com.oppo.oiface.IOIfaceService");
                                                arrayOfInt = getCpuClusterInfo();
                                                param1Parcel2.writeNoException();
                                                param1Parcel2.writeIntArray(arrayOfInt);
                                                return true;
                                              case 1014:
                                                arrayOfInt.enforceInterface("com.oppo.oiface.IOIfaceService");
                                                str7 = getChipName();
                                                param1Parcel2.writeNoException();
                                                param1Parcel2.writeString(str7);
                                                return true;
                                              case 1013:
                                                str7.enforceInterface("com.oppo.oiface.IOIfaceService");
                                                str8 = str7.readString();
                                                str7 = str7.readString();
                                                str7 = triggerFrameStat(str8, str7);
                                                param1Parcel2.writeNoException();
                                                param1Parcel2.writeString(str7);
                                                return true;
                                              case 1012:
                                                str7.enforceInterface("com.oppo.oiface.IOIfaceService");
                                                str7 = getCpuTimeInState();
                                                param1Parcel2.writeNoException();
                                                param1Parcel2.writeString(str7);
                                                return true;
                                              case 1011:
                                                str7.enforceInterface("com.oppo.oiface.IOIfaceService");
                                                arrayOfString = getInstalledGameList();
                                                param1Parcel2.writeNoException();
                                                param1Parcel2.writeStringArray(arrayOfString);
                                                return true;
                                              case 1010:
                                                arrayOfString.enforceInterface("com.oppo.oiface.IOIfaceService");
                                                arrayOfString = arrayOfString.createStringArray();
                                                setInstalledGameList(arrayOfString);
                                                param1Parcel2.writeNoException();
                                                return true;
                                              case 1009:
                                                arrayOfString.enforceInterface("com.oppo.oiface.IOIfaceService");
                                                str6 = getCurrentGamePackage();
                                                param1Parcel2.writeNoException();
                                                param1Parcel2.writeString(str6);
                                                return true;
                                              case 1008:
                                                str6.enforceInterface("com.oppo.oiface.IOIfaceService");
                                                param1Int1 = getGameModeStatus();
                                                param1Parcel2.writeNoException();
                                                param1Parcel2.writeInt(param1Int1);
                                                return true;
                                              case 1007:
                                                str6.enforceInterface("com.oppo.oiface.IOIfaceService");
                                                param1Int1 = str6.readInt();
                                                str6 = str6.readString();
                                                setGameModeStatus(param1Int1, str6);
                                                param1Parcel2.writeNoException();
                                                return true;
                                              case 1006:
                                                str6.enforceInterface("com.oppo.oiface.IOIfaceService");
                                                param1Int1 = str6.readInt();
                                                str6 = str6.readString();
                                                setCoolExFilterType(param1Int1, str6);
                                                param1Parcel2.writeNoException();
                                                return true;
                                              case 1005:
                                                str6.enforceInterface("com.oppo.oiface.IOIfaceService");
                                                arrayOfFloat3 = getGPASystemInfo();
                                                param1Parcel2.writeNoException();
                                                param1Parcel2.writeFloatArray(arrayOfFloat3);
                                                return true;
                                              case 1004:
                                                arrayOfFloat3.enforceInterface("com.oppo.oiface.IOIfaceService");
                                                param1Int1 = getSuperVOOCStatus();
                                                param1Parcel2.writeNoException();
                                                param1Parcel2.writeInt(param1Int1);
                                                return true;
                                              case 1003:
                                                arrayOfFloat3.enforceInterface("com.oppo.oiface.IOIfaceService");
                                                f = getBatteryCurrentNow();
                                                param1Parcel2.writeNoException();
                                                param1Parcel2.writeFloat(f);
                                                return true;
                                              case 1002:
                                                arrayOfFloat3.enforceInterface("com.oppo.oiface.IOIfaceService");
                                                param1Int1 = getBatteryRemain();
                                                param1Parcel2.writeNoException();
                                                param1Parcel2.writeInt(param1Int1);
                                                return true;
                                              case 1001:
                                                break;
                                            } 
                                            arrayOfFloat3.enforceInterface("com.oppo.oiface.IOIfaceService");
                                            iOIfaceCallback3 = IOIfaceCallback.Stub.asInterface(arrayOfFloat3.readStrongBinder());
                                            registerOifaceCallback(iOIfaceCallback3);
                                            param1Parcel2.writeNoException();
                                            return true;
                                          case 905:
                                            iOIfaceCallback3.enforceInterface("com.oppo.oiface.IOIfaceService");
                                            if (iOIfaceCallback3.readInt() != 0) {
                                              bool = true;
                                            } else {
                                              bool = false;
                                            } 
                                            setTouchProtection(bool);
                                            param1Parcel2.writeNoException();
                                            return true;
                                          case 904:
                                            iOIfaceCallback3.enforceInterface("com.oppo.oiface.IOIfaceService");
                                            param1Int1 = iOIfaceCallback3.readInt();
                                            setGyroscopeLevel(param1Int1);
                                            param1Parcel2.writeNoException();
                                            return true;
                                          case 903:
                                            iOIfaceCallback3.enforceInterface("com.oppo.oiface.IOIfaceService");
                                            param1Int1 = iOIfaceCallback3.readInt();
                                            setTouchResponsiveness(param1Int1);
                                            param1Parcel2.writeNoException();
                                            return true;
                                          case 902:
                                            iOIfaceCallback3.enforceInterface("com.oppo.oiface.IOIfaceService");
                                            param1Int1 = iOIfaceCallback3.readInt();
                                            setTouchSensibility(param1Int1);
                                            param1Parcel2.writeNoException();
                                            return true;
                                          case 901:
                                            break;
                                        } 
                                        iOIfaceCallback3.enforceInterface("com.oppo.oiface.IOIfaceService");
                                        param1Int1 = iOIfaceCallback3.readInt();
                                        enableHapticScreenCaptureService(param1Int1);
                                        param1Parcel2.writeNoException();
                                        return true;
                                      case 864:
                                        iOIfaceCallback3.enforceInterface("com.oppo.oiface.IOIfaceService");
                                        param1Int1 = iOIfaceCallback3.readInt();
                                        arrayOfFloat2 = getThermalTemps(param1Int1);
                                        param1Parcel2.writeNoException();
                                        param1Parcel2.writeFloatArray(arrayOfFloat2);
                                        return true;
                                      case 863:
                                        arrayOfFloat2.enforceInterface("com.oppo.oiface.IOIfaceService");
                                        f = getGpuLoad();
                                        param1Parcel2.writeNoException();
                                        param1Parcel2.writeFloat(f);
                                        return true;
                                      case 862:
                                        arrayOfFloat2.enforceInterface("com.oppo.oiface.IOIfaceService");
                                        l = getGpuCurrentFreq();
                                        param1Parcel2.writeNoException();
                                        param1Parcel2.writeLong(l);
                                        return true;
                                      case 861:
                                        arrayOfFloat2.enforceInterface("com.oppo.oiface.IOIfaceService");
                                        arrayOfLong2 = getGpuLimitedFreqs();
                                        param1Parcel2.writeNoException();
                                        param1Parcel2.writeLongArray(arrayOfLong2);
                                        return true;
                                      case 860:
                                        arrayOfLong2.enforceInterface("com.oppo.oiface.IOIfaceService");
                                        arrayOfLong2 = getGpuAvailableFreqTable();
                                        param1Parcel2.writeNoException();
                                        param1Parcel2.writeLongArray(arrayOfLong2);
                                        return true;
                                      case 859:
                                        arrayOfLong2.enforceInterface("com.oppo.oiface.IOIfaceService");
                                        param1Int1 = arrayOfLong2.readInt();
                                        arrayOfFloat1 = getCpuLoads(param1Int1);
                                        param1Parcel2.writeNoException();
                                        param1Parcel2.writeFloatArray(arrayOfFloat1);
                                        return true;
                                      case 858:
                                        arrayOfFloat1.enforceInterface("com.oppo.oiface.IOIfaceService");
                                        param1Int1 = arrayOfFloat1.readInt();
                                        arrayOfLong1 = getCpuCurrentFreq(param1Int1);
                                        param1Parcel2.writeNoException();
                                        param1Parcel2.writeLongArray(arrayOfLong1);
                                        return true;
                                      case 857:
                                        arrayOfLong1.enforceInterface("com.oppo.oiface.IOIfaceService");
                                        param1Int1 = arrayOfLong1.readInt();
                                        arrayOfLong1 = getCpuLimitedFreqs(param1Int1);
                                        param1Parcel2.writeNoException();
                                        param1Parcel2.writeLongArray(arrayOfLong1);
                                        return true;
                                      case 856:
                                        arrayOfLong1.enforceInterface("com.oppo.oiface.IOIfaceService");
                                        param1Int1 = arrayOfLong1.readInt();
                                        arrayOfLong1 = getCpuAvailableFreqTable(param1Int1);
                                        param1Parcel2.writeNoException();
                                        param1Parcel2.writeLongArray(arrayOfLong1);
                                        return true;
                                      case 855:
                                        arrayOfLong1.enforceInterface("com.oppo.oiface.IOIfaceService");
                                        param1Int1 = getCpuClusterNum();
                                        param1Parcel2.writeNoException();
                                        param1Parcel2.writeInt(param1Int1);
                                        return true;
                                      case 854:
                                        arrayOfLong1.enforceInterface("com.oppo.oiface.IOIfaceService");
                                        str5 = arrayOfLong1.readString();
                                        oifaceControl(str5);
                                        param1Parcel2.writeNoException();
                                        return true;
                                      case 853:
                                        str5.enforceInterface("com.oppo.oiface.IOIfaceService");
                                        str5 = str5.readString();
                                        oifaceDecision(str5);
                                        param1Parcel2.writeNoException();
                                        return true;
                                      case 852:
                                        str5.enforceInterface("com.oppo.oiface.IOIfaceService");
                                        str5 = str5.readString();
                                        str5 = generalOifaceSignal(str5);
                                        param1Parcel2.writeNoException();
                                        param1Parcel2.writeString(str5);
                                        return true;
                                      case 851:
                                        break;
                                    } 
                                    str5.enforceInterface("com.oppo.oiface.IOIfaceService");
                                    str8 = str5.readString();
                                    param1Int1 = str5.readInt();
                                    param1Int1 = getFPS(str8, param1Int1);
                                    param1Parcel2.writeNoException();
                                    param1Parcel2.writeInt(param1Int1);
                                    return true;
                                  case 504:
                                    str5.enforceInterface("com.oppo.oiface.IOIfaceService");
                                    str5 = getDeviceID();
                                    param1Parcel2.writeNoException();
                                    param1Parcel2.writeString(str5);
                                    return true;
                                  case 503:
                                    str5.enforceInterface("com.oppo.oiface.IOIfaceService");
                                    param1Int1 = str5.readInt();
                                    notifyScreenEvent(param1Int1);
                                    param1Parcel2.writeNoException();
                                    return true;
                                  case 502:
                                    break;
                                } 
                                str5.enforceInterface("com.oppo.oiface.IOIfaceService");
                                param1Int1 = str5.readInt();
                                setPerfMode(param1Int1);
                                param1Parcel2.writeNoException();
                                return true;
                              case 206:
                                str5.enforceInterface("com.oppo.oiface.IOIfaceService");
                                str5 = getSupportGameStartPackage();
                                param1Parcel2.writeNoException();
                                param1Parcel2.writeString(str5);
                                return true;
                              case 205:
                                str5.enforceInterface("com.oppo.oiface.IOIfaceService");
                                iOIfaceCallback2 = IOIfaceCallback.Stub.asInterface(str5.readStrongBinder());
                                unRegisterGameRoleListener(iOIfaceCallback2);
                                param1Parcel2.writeNoException();
                                return true;
                              case 204:
                                break;
                            } 
                            iOIfaceCallback2.enforceInterface("com.oppo.oiface.IOIfaceService");
                            param1Int1 = iOIfaceCallback2.readInt();
                            str8 = iOIfaceCallback2.readString();
                            iOIfaceCallback2 = IOIfaceCallback.Stub.asInterface(iOIfaceCallback2.readStrongBinder());
                            registerGameRoleListener(param1Int1, str8, iOIfaceCallback2);
                            param1Parcel2.writeNoException();
                            return true;
                          case 13:
                            iOIfaceCallback2.enforceInterface("com.oppo.oiface.IOIfaceService");
                            param1Int1 = iOIfaceCallback2.readInt();
                            setHalfHQV(param1Int1);
                            param1Parcel2.writeNoException();
                            return true;
                          case 12:
                            iOIfaceCallback2.enforceInterface("com.oppo.oiface.IOIfaceService");
                            str8 = iOIfaceCallback2.readString();
                            param1Int1 = iOIfaceCallback2.readInt();
                            str = iOIfaceCallback2.readString();
                            str = registerHQV(str8, param1Int1, str);
                            param1Parcel2.writeNoException();
                            param1Parcel2.writeString(str);
                            return true;
                          case 11:
                            break;
                        } 
                        str.enforceInterface("com.oppo.oiface.IOIfaceService");
                        param1Int1 = str.readInt();
                        enableHQV(param1Int1);
                        param1Parcel2.writeNoException();
                        return true;
                      } 
                      str.enforceInterface("com.oppo.oiface.IOIfaceService");
                      str = str.readString();
                      str = getAllLoadInfo(str);
                      param1Parcel2.writeNoException();
                      param1Parcel2.writeString(str);
                      return true;
                    } 
                    str.enforceInterface("com.oppo.oiface.IOIfaceService");
                    param1Int1 = str.readInt();
                    str = setGCPEffectMode(param1Int1);
                    param1Parcel2.writeNoException();
                    param1Parcel2.writeString(str);
                    return true;
                  } 
                  str.enforceInterface("com.oppo.oiface.IOIfaceService");
                  String str4 = str.readString();
                  param1Int1 = str.readInt();
                  param1Int2 = str.readInt();
                  str = setGeneralSignalCosa(str4, param1Int1, param1Int2);
                  param1Parcel2.writeNoException();
                  param1Parcel2.writeString(str);
                  return true;
                } 
                str.enforceInterface("com.oppo.oiface.IOIfaceService");
                IOIfaceCallback iOIfaceCallback1 = IOIfaceCallback.Stub.asInterface(str.readStrongBinder());
                String str3 = str.readString();
                param1Int1 = str.readInt();
                param1Int2 = str.readInt();
                param1Int1 = registerClientThroughCosa(iOIfaceCallback1, str3, param1Int1, param1Int2);
                param1Parcel2.writeNoException();
                param1Parcel2.writeInt(param1Int1);
                return true;
              } 
              param1Parcel2.writeString("com.oppo.oiface.IOIfaceService");
              return true;
            } 
            str.enforceInterface("com.oppo.oiface.IOIfaceService");
            param1Int1 = str.readInt();
            String str1 = str.readString();
            String str2 = str.readString();
            str = str.readString();
            currentPkgStatus(param1Int1, str1, str2, str);
            param1Parcel2.writeNoException();
            return true;
          } 
          str.enforceInterface("com.oppo.oiface.IOIfaceService");
          param1Int2 = str.readInt();
          param1Int1 = str.readInt();
          iOIfaceCallback = IOIfaceCallback.Stub.asInterface(str.readStrongBinder());
          registerNetworkListener(param1Int2, param1Int1, iOIfaceCallback);
          param1Parcel2.writeNoException();
          return true;
        } 
        iOIfaceCallback.enforceInterface("com.oppo.oiface.IOIfaceService");
        param1Int1 = iOIfaceCallback.readInt();
        param1Int2 = iOIfaceCallback.readInt();
        bindGameTask(param1Int1, param1Int2);
        param1Parcel2.writeNoException();
        return true;
      } 
      iOIfaceCallback.enforceInterface("com.oppo.oiface.IOIfaceService");
      param1Int1 = iOIfaceCallback.readInt();
      currentNetwork(param1Int1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IOIfaceInternalService {
      public static IOIfaceInternalService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oppo.oiface.IOIfaceService";
      }
      
      public void currentNetwork(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().currentNetwork(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void bindGameTask(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().bindGameTask(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enableHQV(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().enableHQV(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String registerHQV(String param2String1, int param2Int, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            param2String1 = IOIfaceInternalService.Stub.getDefaultImpl().registerHQV(param2String1, param2Int, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setHalfHQV(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().setHalfHQV(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int registerClientThroughCosa(IOIfaceCallback param2IOIfaceCallback, String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          if (param2IOIfaceCallback != null) {
            iBinder = param2IOIfaceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(161, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            param2Int1 = IOIfaceInternalService.Stub.getDefaultImpl().registerClientThroughCosa(param2IOIfaceCallback, param2String, param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String setGeneralSignalCosa(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(162, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            param2String = IOIfaceInternalService.Stub.getDefaultImpl().setGeneralSignalCosa(param2String, param2Int1, param2Int2);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerNetworkListener(int param2Int1, int param2Int2, IOIfaceCallback param2IOIfaceCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2IOIfaceCallback != null) {
            iBinder = param2IOIfaceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(202, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().registerNetworkListener(param2Int1, param2Int2, param2IOIfaceCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerGameRoleListener(int param2Int, String param2String, IOIfaceCallback param2IOIfaceCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          if (param2IOIfaceCallback != null) {
            iBinder = param2IOIfaceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(204, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().registerGameRoleListener(param2Int, param2String, param2IOIfaceCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unRegisterGameRoleListener(IOIfaceCallback param2IOIfaceCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          if (param2IOIfaceCallback != null) {
            iBinder = param2IOIfaceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(205, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().unRegisterGameRoleListener(param2IOIfaceCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSupportGameStartPackage() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          boolean bool = this.mRemote.transact(206, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getSupportGameStartPackage(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getAllLoadInfo(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(212, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            param2String = IOIfaceInternalService.Stub.getDefaultImpl().getAllLoadInfo(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String setGCPEffectMode(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(211, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().setGCPEffectMode(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPerfMode(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(502, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().setPerfMode(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyScreenEvent(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(503, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().notifyScreenEvent(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDeviceID() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          boolean bool = this.mRemote.transact(504, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getDeviceID(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void currentPkgStatus(int param2Int, String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(803, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().currentPkgStatus(param2Int, param2String1, param2String2, param2String3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getFPS(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(851, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            param2Int = IOIfaceInternalService.Stub.getDefaultImpl().getFPS(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String generalOifaceSignal(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(852, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            param2String = IOIfaceInternalService.Stub.getDefaultImpl().generalOifaceSignal(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void oifaceDecision(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(853, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().oifaceDecision(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void oifaceControl(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(854, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().oifaceControl(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCpuClusterNum() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          boolean bool = this.mRemote.transact(855, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getCpuClusterNum(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long[] getCpuAvailableFreqTable(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(856, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getCpuAvailableFreqTable(param2Int); 
          parcel2.readException();
          return parcel2.createLongArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long[] getCpuLimitedFreqs(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(857, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getCpuLimitedFreqs(param2Int); 
          parcel2.readException();
          return parcel2.createLongArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long[] getCpuCurrentFreq(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(858, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getCpuCurrentFreq(param2Int); 
          parcel2.readException();
          return parcel2.createLongArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public float[] getCpuLoads(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(859, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getCpuLoads(param2Int); 
          parcel2.readException();
          return parcel2.createFloatArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long[] getGpuAvailableFreqTable() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          boolean bool = this.mRemote.transact(860, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getGpuAvailableFreqTable(); 
          parcel2.readException();
          return parcel2.createLongArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long[] getGpuLimitedFreqs() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          boolean bool = this.mRemote.transact(861, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getGpuLimitedFreqs(); 
          parcel2.readException();
          return parcel2.createLongArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getGpuCurrentFreq() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          boolean bool = this.mRemote.transact(862, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getGpuCurrentFreq(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public float getGpuLoad() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          boolean bool = this.mRemote.transact(863, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getGpuLoad(); 
          parcel2.readException();
          return parcel2.readFloat();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public float[] getThermalTemps(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(864, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getThermalTemps(param2Int); 
          parcel2.readException();
          return parcel2.createFloatArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enableHapticScreenCaptureService(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(901, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().enableHapticScreenCaptureService(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTouchSensibility(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(902, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().setTouchSensibility(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTouchResponsiveness(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(903, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().setTouchResponsiveness(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setGyroscopeLevel(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(904, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().setGyroscopeLevel(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTouchProtection(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(905, parcel1, parcel2, 0);
          if (!bool1 && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().setTouchProtection(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerOifaceCallback(IOIfaceCallback param2IOIfaceCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          if (param2IOIfaceCallback != null) {
            iBinder = param2IOIfaceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1001, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().registerOifaceCallback(param2IOIfaceCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getBatteryRemain() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          boolean bool = this.mRemote.transact(1002, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getBatteryRemain(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public float getBatteryCurrentNow() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          boolean bool = this.mRemote.transact(1003, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getBatteryCurrentNow(); 
          parcel2.readException();
          return parcel2.readFloat();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSuperVOOCStatus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          boolean bool = this.mRemote.transact(1004, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getSuperVOOCStatus(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public float[] getGPASystemInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          boolean bool = this.mRemote.transact(1005, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getGPASystemInfo(); 
          parcel2.readException();
          return parcel2.createFloatArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setCoolExFilterType(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1006, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().setCoolExFilterType(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setGameModeStatus(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1007, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().setGameModeStatus(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getGameModeStatus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          boolean bool = this.mRemote.transact(1008, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getGameModeStatus(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getCurrentGamePackage() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          boolean bool = this.mRemote.transact(1009, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getCurrentGamePackage(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInstalledGameList(String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(1010, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            IOIfaceInternalService.Stub.getDefaultImpl().setInstalledGameList(param2ArrayOfString);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getInstalledGameList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          boolean bool = this.mRemote.transact(1011, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getInstalledGameList(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getCpuTimeInState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          boolean bool = this.mRemote.transact(1012, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getCpuTimeInState(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String triggerFrameStat(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(1013, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null) {
            param2String1 = IOIfaceInternalService.Stub.getDefaultImpl().triggerFrameStat(param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getChipName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          boolean bool = this.mRemote.transact(1014, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getChipName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getCpuClusterInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          boolean bool = this.mRemote.transact(1015, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getCpuClusterInfo(); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getBatteryFCC() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
          boolean bool = this.mRemote.transact(1016, parcel1, parcel2, 0);
          if (!bool && IOIfaceInternalService.Stub.getDefaultImpl() != null)
            return IOIfaceInternalService.Stub.getDefaultImpl().getBatteryFCC(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOIfaceInternalService param1IOIfaceInternalService) {
      if (Proxy.sDefaultImpl == null && param1IOIfaceInternalService != null) {
        Proxy.sDefaultImpl = param1IOIfaceInternalService;
        return true;
      } 
      return false;
    }
    
    public static IOIfaceInternalService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
