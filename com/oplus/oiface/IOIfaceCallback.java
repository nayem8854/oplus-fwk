package com.oplus.oiface;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOIfaceCallback extends IInterface {
  void onEngineBoostINfo(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  String onFBNotification(int paramInt) throws RemoteException;
  
  String onGPANotification(String paramString) throws RemoteException;
  
  void onGameJitter(String paramString, int paramInt) throws RemoteException;
  
  void onGameStatusChanged(String paramString1, String paramString2) throws RemoteException;
  
  void onHyperBoostInfo(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void onNetworkChanged(String paramString, int paramInt) throws RemoteException;
  
  void onOifaceGeneralInfo(String paramString, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void onSystemNotify(String paramString) throws RemoteException;
  
  void onTGPAInfo(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void onThermalStatusChanged(String paramString) throws RemoteException;
  
  class Default implements IOIfaceCallback {
    public String onFBNotification(int param1Int) throws RemoteException {
      return null;
    }
    
    public String onGPANotification(String param1String) throws RemoteException {
      return null;
    }
    
    public void onTGPAInfo(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onHyperBoostInfo(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onEngineBoostINfo(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onOifaceGeneralInfo(String param1String, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void onGameStatusChanged(String param1String1, String param1String2) throws RemoteException {}
    
    public void onNetworkChanged(String param1String, int param1Int) throws RemoteException {}
    
    public void onSystemNotify(String param1String) throws RemoteException {}
    
    public void onThermalStatusChanged(String param1String) throws RemoteException {}
    
    public void onGameJitter(String param1String, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOIfaceCallback {
    private static final String DESCRIPTOR = "com.oplus.oiface.IOIfaceCallback";
    
    static final int TRANSACTION_onEngineBoostINfo = 5;
    
    static final int TRANSACTION_onFBNotification = 1;
    
    static final int TRANSACTION_onGPANotification = 2;
    
    static final int TRANSACTION_onGameJitter = 11;
    
    static final int TRANSACTION_onGameStatusChanged = 7;
    
    static final int TRANSACTION_onHyperBoostInfo = 4;
    
    static final int TRANSACTION_onNetworkChanged = 8;
    
    static final int TRANSACTION_onOifaceGeneralInfo = 6;
    
    static final int TRANSACTION_onSystemNotify = 9;
    
    static final int TRANSACTION_onTGPAInfo = 3;
    
    static final int TRANSACTION_onThermalStatusChanged = 10;
    
    public Stub() {
      attachInterface(this, "com.oplus.oiface.IOIfaceCallback");
    }
    
    public static IOIfaceCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.oiface.IOIfaceCallback");
      if (iInterface != null && iInterface instanceof IOIfaceCallback)
        return (IOIfaceCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 11:
          return "onGameJitter";
        case 10:
          return "onThermalStatusChanged";
        case 9:
          return "onSystemNotify";
        case 8:
          return "onNetworkChanged";
        case 7:
          return "onGameStatusChanged";
        case 6:
          return "onOifaceGeneralInfo";
        case 5:
          return "onEngineBoostINfo";
        case 4:
          return "onHyperBoostInfo";
        case 3:
          return "onTGPAInfo";
        case 2:
          return "onGPANotification";
        case 1:
          break;
      } 
      return "onFBNotification";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String str2;
        int i;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 11:
            param1Parcel1.enforceInterface("com.oplus.oiface.IOIfaceCallback");
            str2 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            onGameJitter(str2, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            param1Parcel1.enforceInterface("com.oplus.oiface.IOIfaceCallback");
            str1 = param1Parcel1.readString();
            onThermalStatusChanged(str1);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            str1.enforceInterface("com.oplus.oiface.IOIfaceCallback");
            str1 = str1.readString();
            onSystemNotify(str1);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            str1.enforceInterface("com.oplus.oiface.IOIfaceCallback");
            str2 = str1.readString();
            param1Int1 = str1.readInt();
            onNetworkChanged(str2, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            str1.enforceInterface("com.oplus.oiface.IOIfaceCallback");
            str2 = str1.readString();
            str1 = str1.readString();
            onGameStatusChanged(str2, str1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            str1.enforceInterface("com.oplus.oiface.IOIfaceCallback");
            str2 = str1.readString();
            i = str1.readInt();
            param1Int1 = str1.readInt();
            param1Int2 = str1.readInt();
            onOifaceGeneralInfo(str2, i, param1Int1, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            str1.enforceInterface("com.oplus.oiface.IOIfaceCallback");
            str2 = str1.readString();
            param1Int2 = str1.readInt();
            param1Int1 = str1.readInt();
            onEngineBoostINfo(str2, param1Int2, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str1.enforceInterface("com.oplus.oiface.IOIfaceCallback");
            str2 = str1.readString();
            param1Int1 = str1.readInt();
            param1Int2 = str1.readInt();
            onHyperBoostInfo(str2, param1Int1, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            str1.enforceInterface("com.oplus.oiface.IOIfaceCallback");
            str2 = str1.readString();
            param1Int2 = str1.readInt();
            param1Int1 = str1.readInt();
            onTGPAInfo(str2, param1Int2, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str1.enforceInterface("com.oplus.oiface.IOIfaceCallback");
            str1 = str1.readString();
            str1 = onGPANotification(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("com.oplus.oiface.IOIfaceCallback");
        param1Int1 = str1.readInt();
        String str1 = onFBNotification(param1Int1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeString(str1);
        return true;
      } 
      param1Parcel2.writeString("com.oplus.oiface.IOIfaceCallback");
      return true;
    }
    
    private static class Proxy implements IOIfaceCallback {
      public static IOIfaceCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.oiface.IOIfaceCallback";
      }
      
      public String onFBNotification(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oiface.IOIfaceCallback");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOIfaceCallback.Stub.getDefaultImpl() != null)
            return IOIfaceCallback.Stub.getDefaultImpl().onFBNotification(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String onGPANotification(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oiface.IOIfaceCallback");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOIfaceCallback.Stub.getDefaultImpl() != null) {
            param2String = IOIfaceCallback.Stub.getDefaultImpl().onGPANotification(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onTGPAInfo(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oiface.IOIfaceCallback");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOIfaceCallback.Stub.getDefaultImpl() != null) {
            IOIfaceCallback.Stub.getDefaultImpl().onTGPAInfo(param2String, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onHyperBoostInfo(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oiface.IOIfaceCallback");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOIfaceCallback.Stub.getDefaultImpl() != null) {
            IOIfaceCallback.Stub.getDefaultImpl().onHyperBoostInfo(param2String, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onEngineBoostINfo(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oiface.IOIfaceCallback");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOIfaceCallback.Stub.getDefaultImpl() != null) {
            IOIfaceCallback.Stub.getDefaultImpl().onEngineBoostINfo(param2String, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onOifaceGeneralInfo(String param2String, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oiface.IOIfaceCallback");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IOIfaceCallback.Stub.getDefaultImpl() != null) {
            IOIfaceCallback.Stub.getDefaultImpl().onOifaceGeneralInfo(param2String, param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onGameStatusChanged(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oiface.IOIfaceCallback");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IOIfaceCallback.Stub.getDefaultImpl() != null) {
            IOIfaceCallback.Stub.getDefaultImpl().onGameStatusChanged(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onNetworkChanged(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oiface.IOIfaceCallback");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IOIfaceCallback.Stub.getDefaultImpl() != null) {
            IOIfaceCallback.Stub.getDefaultImpl().onNetworkChanged(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onSystemNotify(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oiface.IOIfaceCallback");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IOIfaceCallback.Stub.getDefaultImpl() != null) {
            IOIfaceCallback.Stub.getDefaultImpl().onSystemNotify(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onThermalStatusChanged(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oiface.IOIfaceCallback");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IOIfaceCallback.Stub.getDefaultImpl() != null) {
            IOIfaceCallback.Stub.getDefaultImpl().onThermalStatusChanged(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onGameJitter(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oiface.IOIfaceCallback");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IOIfaceCallback.Stub.getDefaultImpl() != null) {
            IOIfaceCallback.Stub.getDefaultImpl().onGameJitter(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOIfaceCallback param1IOIfaceCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOIfaceCallback != null) {
          Proxy.sDefaultImpl = param1IOIfaceCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOIfaceCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
