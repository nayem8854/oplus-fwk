package com.oplus.oiface;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.Log;

public class OifaceProxyUtils {
  private static final String DESCRIPTOR = "com.oppo.oiface.IOIfaceService";
  
  private static Boolean INIT_NET_STATS;
  
  public static final int NET_OFF_WLAN = 1;
  
  public static final int NET_OFF_WWLAN = 3;
  
  public static final int NET_ON_WLAN = 0;
  
  public static final int NET_ON_WWLAN = 2;
  
  private static volatile int NET_STATUS = 0;
  
  private static final int OIFACE_ON = 0;
  
  private static final String TAG = "OifaceUtil";
  
  private static final int TRANSACTION_CURRENT_NETWORK = 1;
  
  private static final int TRANSACTION_CURRENT_PACKAGE = 2;
  
  private static OifaceProxyUtils sInstance;
  
  private static int sOifaceProp = SystemProperties.getInt("persist.sys.oiface.enable", 0);
  
  private IBinder.DeathRecipient mDeathRecipient;
  
  private IBinder mRemote;
  
  static {
    NET_STATUS = -1;
    INIT_NET_STATS = Boolean.valueOf(true);
  }
  
  public static OifaceProxyUtils getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/oiface/OifaceProxyUtils.sInstance : Lcom/oplus/oiface/OifaceProxyUtils;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/oiface/OifaceProxyUtils
    //   8: monitorenter
    //   9: getstatic com/oplus/oiface/OifaceProxyUtils.sInstance : Lcom/oplus/oiface/OifaceProxyUtils;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/oiface/OifaceProxyUtils
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/oiface/OifaceProxyUtils.sInstance : Lcom/oplus/oiface/OifaceProxyUtils;
    //   27: ldc com/oplus/oiface/OifaceProxyUtils
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/oiface/OifaceProxyUtils
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/oiface/OifaceProxyUtils.sInstance : Lcom/oplus/oiface/OifaceProxyUtils;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #50	-> 0
    //   #51	-> 6
    //   #52	-> 9
    //   #53	-> 15
    //   #55	-> 27
    //   #56	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  private OifaceProxyUtils() {
    this.mDeathRecipient = (IBinder.DeathRecipient)new Object(this);
    connectOifaceDataService();
  }
  
  private IBinder connectOifaceDataService() {
    IBinder iBinder = ServiceManager.checkService("oiface");
    if (iBinder != null)
      try {
        iBinder.linkToDeath(this.mDeathRecipient, 0);
      } catch (RemoteException remoteException) {
        this.mRemote = null;
      }  
    return this.mRemote;
  }
  
  public void currentNetwork(int paramInt) {
    if (!oifaceEnable())
      return; 
    NET_STATUS = paramInt;
    Parcel parcel = Parcel.obtain();
    try {
      parcel.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
      parcel.writeInt(paramInt);
      this.mRemote.transact(1, parcel, null, 1);
      parcel.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("currentNetwork has Exception : ");
      stringBuilder.append(exception);
      Log.d("OifaceUtil", stringBuilder.toString());
      parcel.recycle();
    } finally {
      Exception exception;
    } 
  }
  
  public void currentPackage(String paramString, int paramInt1, int paramInt2) {
    if (!oifaceEnable())
      return; 
    if (!INIT_NET_STATS.booleanValue()) {
      INIT_NET_STATS = Boolean.valueOf(true);
      currentNetwork(NET_STATUS);
    } 
    Parcel parcel = Parcel.obtain();
    try {
      parcel.writeInterfaceToken("com.oppo.oiface.IOIfaceService");
      parcel.writeString(paramString);
      parcel.writeInt(paramInt1);
      parcel.writeInt(paramInt2);
      this.mRemote.transact(2, parcel, null, 1);
      parcel.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("currentPackage has Exception : ");
      stringBuilder.append(exception);
      Log.d("OifaceUtil", stringBuilder.toString());
      parcel.recycle();
    } finally {}
  }
  
  private boolean oifaceEnable() {
    if (this.mRemote == null && connectOifaceDataService() == null) {
      Log.e("OifaceUtil", "Cannot connect to OifaceService");
      return false;
    } 
    return true;
  }
  
  public void initNetworkState(Context paramContext) {
    if (NET_STATUS != -1)
      return; 
    int i = getNetworkState(paramContext);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("initNetworkState:");
    stringBuilder.append(NET_STATUS);
    Log.d("OifaceUtil", stringBuilder.toString());
    currentNetwork(i);
  }
  
  public int getNetworkState(Context paramContext) {
    if (isWifiConn(paramContext)) {
      NET_STATUS = 0;
    } else if (isMobileConn(paramContext)) {
      NET_STATUS = 2;
    } else {
      NET_STATUS = 1;
    } 
    return NET_STATUS;
  }
  
  public static boolean isWifiConn(Context paramContext) {
    ConnectivityManager connectivityManager = (ConnectivityManager)paramContext.getSystemService("connectivity");
    boolean bool = true;
    NetworkInfo networkInfo = connectivityManager.getNetworkInfo(1);
    if (networkInfo == null || !networkInfo.isConnected())
      bool = false; 
    return bool;
  }
  
  public static boolean isMobileConn(Context paramContext) {
    ConnectivityManager connectivityManager = (ConnectivityManager)paramContext.getSystemService("connectivity");
    boolean bool1 = false;
    NetworkInfo networkInfo = connectivityManager.getNetworkInfo(0);
    boolean bool2 = bool1;
    if (networkInfo != null) {
      bool2 = bool1;
      if (networkInfo.isConnected())
        bool2 = true; 
    } 
    return bool2;
  }
}
