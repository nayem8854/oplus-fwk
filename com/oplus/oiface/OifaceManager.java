package com.oplus.oiface;

import android.os.IBinder;
import android.os.ServiceManager;
import android.util.Log;

public class OifaceManager {
  private static volatile OifaceManager sInstance = null;
  
  private IOIfaceCallback mOifaceCallback = null;
  
  private IBinder.DeathRecipient mDeathRecipient = (IBinder.DeathRecipient)new Object(this);
  
  public static final int APP_ENTER = 7;
  
  public static final int APP_PIP_CLOSED = 9;
  
  public static final int APP_PIP_OPENED = 8;
  
  public static final int APP_PIP_SWITCH = 10;
  
  public static final int BIND_CPU_CLUSTER_ALL = 0;
  
  public static final int CPU_CLUSTER_GOLD = 1;
  
  public static final int CPU_CLUSTER_PRIME = 3;
  
  public static final int CPU_CLUSTER_SILVER = 2;
  
  private static final String DISCONNECTED = "{\"oiface\":\"disconnected\"}";
  
  public static final int FPS_PER_SECOND = 0;
  
  public static final int FPS_RAW_DATA = 1;
  
  public static final int GPA_CPU_CLUSTER_GOLD = 2;
  
  public static final int GPA_CPU_CLUSTER_PRIME = 4;
  
  public static final int GPA_CPU_CLUSTER_SILVER = 1;
  
  public static final int NETWORK_STATUS_DATA = 2;
  
  public static final int NETWORK_STATUS_DATA_OFF = 3;
  
  public static final int NETWORK_STATUS_WIFI = 0;
  
  public static final int NETWORK_STATUS_WIFI_OFF = 1;
  
  public static final int PERF_MODE_NORMAL = 0;
  
  public static final int PERF_MODE_PERFORMANCE = 2;
  
  public static final int PERF_MODE_POWER_SAVE = 1;
  
  public static final int REGISTER_GAME_SCENE = 1;
  
  public static final int REGISTER_GAME_STATUS = 2;
  
  public static final int SCREEN_OFF = 0;
  
  public static final int SCREEN_ON = 1;
  
  private static final String TAG = "OifaceManager";
  
  public static final int THERMAL_TEMP_BACK = 2;
  
  public static final int THERMAL_TEMP_FRONT = 1;
  
  public static final int THERMAL_TEMP_GOLD = 16;
  
  public static final int THERMAL_TEMP_GPU = 64;
  
  public static final int THERMAL_TEMP_PRIME = 32;
  
  public static final int THERMAL_TEMP_SIDE = 4;
  
  public static final int THERMAL_TEMP_SILVER = 8;
  
  private String mIdentity;
  
  private IBinder mRemote;
  
  private IOIfaceInternalService mService;
  
  private void checkService() {
    if (this.mService == null) {
      IBinder iBinder = ServiceManager.getService("oiface");
      if (iBinder == null) {
        Log.d("OifaceManager", "unable to getService oiface");
        return;
      } 
      IOIfaceInternalService iOIfaceInternalService = IOIfaceInternalService.Stub.asInterface(iBinder);
      if (iOIfaceInternalService != null) {
        try {
          this.mRemote.linkToDeath(this.mDeathRecipient, 0);
        } catch (Exception exception) {
          Log.e("OifaceManager", "connect to oiface failed");
          this.mService = null;
        } 
      } else {
        Log.d("OifaceManager", "connect to oiface failed");
      } 
    } 
  }
  
  private OifaceManager(String paramString) {
    this.mIdentity = paramString;
    checkService();
  }
  
  public static OifaceManager getInstance(String paramString) {
    // Byte code:
    //   0: getstatic com/oplus/oiface/OifaceManager.sInstance : Lcom/oplus/oiface/OifaceManager;
    //   3: ifnonnull -> 40
    //   6: ldc com/oplus/oiface/OifaceManager
    //   8: monitorenter
    //   9: getstatic com/oplus/oiface/OifaceManager.sInstance : Lcom/oplus/oiface/OifaceManager;
    //   12: ifnonnull -> 28
    //   15: new com/oplus/oiface/OifaceManager
    //   18: astore_1
    //   19: aload_1
    //   20: aload_0
    //   21: invokespecial <init> : (Ljava/lang/String;)V
    //   24: aload_1
    //   25: putstatic com/oplus/oiface/OifaceManager.sInstance : Lcom/oplus/oiface/OifaceManager;
    //   28: ldc com/oplus/oiface/OifaceManager
    //   30: monitorexit
    //   31: goto -> 40
    //   34: astore_0
    //   35: ldc com/oplus/oiface/OifaceManager
    //   37: monitorexit
    //   38: aload_0
    //   39: athrow
    //   40: getstatic com/oplus/oiface/OifaceManager.sInstance : Lcom/oplus/oiface/OifaceManager;
    //   43: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #128	-> 0
    //   #129	-> 6
    //   #130	-> 9
    //   #131	-> 15
    //   #133	-> 28
    //   #136	-> 40
    // Exception table:
    //   from	to	target	type
    //   9	15	34	finally
    //   15	28	34	finally
    //   28	31	34	finally
    //   35	38	34	finally
  }
  
  public boolean currentNetwork(int paramInt) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.currentNetwork(paramInt);
        return true;
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        exception.printStackTrace();
        return false;
      }  
    return false;
  }
  
  public boolean bindGameTask(int paramInt1, int paramInt2) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.bindGameTask(paramInt1, paramInt2);
        return true;
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return false;
      }  
    return false;
  }
  
  public boolean enableHQV(int paramInt) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.enableHQV(paramInt);
        return true;
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return false;
      }  
    return false;
  }
  
  public String registerHQV(String paramString1, int paramInt, String paramString2) {
    checkService();
    String str1 = null;
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    String str2 = str1;
    if (iOIfaceInternalService != null)
      try {
        str2 = iOIfaceInternalService.registerHQV(paramString1, paramInt, paramString2);
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        str2 = str1;
      }  
    return str2;
  }
  
  public boolean setHalfHQV(int paramInt) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.setHalfHQV(paramInt);
        return true;
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return false;
      }  
    return false;
  }
  
  public boolean registerClientThroughCosa(IOIfaceCallback paramIOIfaceCallback, String paramString, int paramInt1, int paramInt2) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    boolean bool = false;
    if (iOIfaceInternalService != null)
      try {
        paramInt1 = iOIfaceInternalService.registerClientThroughCosa(paramIOIfaceCallback, paramString, paramInt1, paramInt2);
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("registerClientThroughCosa return ");
        stringBuilder.append(paramInt1);
        Log.e("OifaceManager", stringBuilder.toString());
        if (paramInt1 > 0)
          bool = true; 
        return bool;
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return false;
      }  
    return false;
  }
  
  public String setGeneralSignalCosa(String paramString, int paramInt1, int paramInt2) {
    checkService();
    String str1 = null;
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    String str2 = str1;
    if (iOIfaceInternalService != null)
      try {
        str2 = iOIfaceInternalService.setGeneralSignalCosa(paramString, paramInt1, paramInt2);
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        str2 = str1;
      }  
    return str2;
  }
  
  public boolean registerNetworkListener(int paramInt1, int paramInt2, IOIfaceCallback paramIOIfaceCallback) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.registerNetworkListener(paramInt1, paramInt2, paramIOIfaceCallback);
        return true;
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return false;
      }  
    return false;
  }
  
  public boolean registerGameRoleListener(int paramInt, String paramString, IOIfaceCallback paramIOIfaceCallback) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.registerGameRoleListener(paramInt, paramString, paramIOIfaceCallback);
        return true;
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return false;
      }  
    return false;
  }
  
  public boolean unRegisterGameRoleListener(IOIfaceCallback paramIOIfaceCallback) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.unRegisterGameRoleListener(paramIOIfaceCallback);
        return true;
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return false;
      }  
    return false;
  }
  
  public String getSupportGameStartPackage() {
    checkService();
    String str1 = null;
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    String str2 = str1;
    if (iOIfaceInternalService != null)
      try {
        str2 = iOIfaceInternalService.getSupportGameStartPackage();
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        str2 = str1;
      }  
    return str2;
  }
  
  public String getAllLoadInfo(String paramString) {
    checkService();
    String str1 = null;
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    String str2 = str1;
    if (iOIfaceInternalService != null)
      try {
        str2 = iOIfaceInternalService.getAllLoadInfo(paramString);
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        str2 = str1;
      }  
    return str2;
  }
  
  public boolean setGCPEffectMode(int paramInt) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        String str = iOIfaceInternalService.setGCPEffectMode(paramInt);
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("setGCPEffectMode return ");
        stringBuilder.append(str);
        Log.e("OifaceManager", stringBuilder.toString());
        return true;
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return false;
      }  
    return false;
  }
  
  public String getDeviceID() {
    checkService();
    String str1 = null;
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    String str2 = str1;
    if (iOIfaceInternalService != null)
      try {
        str2 = iOIfaceInternalService.getDeviceID();
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        str2 = str1;
      }  
    return str2;
  }
  
  public boolean setPerfMode(int paramInt) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.setPerfMode(paramInt);
        return true;
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return false;
      }  
    return false;
  }
  
  public boolean notifyScreenEvent(int paramInt) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.notifyScreenEvent(paramInt);
        return true;
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return false;
      }  
    return false;
  }
  
  public boolean currentPkgStatus(int paramInt, String paramString1, String paramString2, String paramString3) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.currentPkgStatus(paramInt, paramString1, paramString2, paramString3);
        return true;
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return false;
      }  
    return false;
  }
  
  public int getFPS(String paramString, int paramInt) {
    checkService();
    byte b = 0;
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    int i = b;
    if (iOIfaceInternalService != null)
      try {
        i = iOIfaceInternalService.getFPS(paramString, paramInt);
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        i = b;
      }  
    return i;
  }
  
  public String generalOifaceSignal(String paramString) {
    checkService();
    String str1 = null;
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    String str2 = str1;
    if (iOIfaceInternalService != null)
      try {
        str2 = iOIfaceInternalService.generalOifaceSignal(paramString);
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        str2 = str1;
      }  
    return str2;
  }
  
  public boolean oifaceDecision(String paramString) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.oifaceDecision(paramString);
        return true;
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return false;
      }  
    return false;
  }
  
  public boolean oifaceControl(String paramString) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.oifaceControl(paramString);
        return true;
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return false;
      }  
    return false;
  }
  
  public int getCpuClusterNum() {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.getCpuClusterNum();
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return -1;
      }  
    return -1;
  }
  
  public long[] getCpuAvailableFreqTable(int paramInt) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.getCpuAvailableFreqTable(paramInt);
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return null;
      }  
    return null;
  }
  
  public long[] getCpuLimitedFreqs(int paramInt) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.getCpuLimitedFreqs(paramInt);
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return null;
      }  
    return null;
  }
  
  public long[] getCpuCurrentFreq(int paramInt) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.getCpuCurrentFreq(paramInt);
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return null;
      }  
    return null;
  }
  
  public float[] getCpuLoads(int paramInt) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.getCpuLoads(paramInt);
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return null;
      }  
    return null;
  }
  
  public long[] getGpuAvailableFreqTable() {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.getGpuAvailableFreqTable();
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return null;
      }  
    return null;
  }
  
  public long[] getGpuLimitedFreqs() {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.getGpuLimitedFreqs();
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return null;
      }  
    return null;
  }
  
  public long getGpuCurrentFreq() {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.getGpuCurrentFreq();
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return -1L;
      }  
    return -1L;
  }
  
  public float getGpuLoad() {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.getGpuLoad();
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return -1.0F;
      }  
    return -1.0F;
  }
  
  public float[] getThermalTemps(int paramInt) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.getThermalTemps(paramInt);
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return null;
      }  
    return null;
  }
  
  public boolean enableHapticScreenCaptureService(int paramInt) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.enableHapticScreenCaptureService(paramInt);
        return true;
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return false;
      }  
    return false;
  }
  
  public boolean setTouchSensibility(int paramInt) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.setTouchSensibility(paramInt);
        return true;
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return false;
      }  
    return false;
  }
  
  public boolean setTouchResponsiveness(int paramInt) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.setTouchResponsiveness(paramInt);
        return true;
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return false;
      }  
    return false;
  }
  
  public boolean setGyroscopeLevel(int paramInt) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.setGyroscopeLevel(paramInt);
        return true;
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return false;
      }  
    return false;
  }
  
  public boolean setTouchProtection(boolean paramBoolean) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.setTouchProtection(paramBoolean);
        return true;
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return false;
      }  
    return false;
  }
  
  public boolean registerOifaceCallback(IOIfaceCallback paramIOIfaceCallback) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.registerOifaceCallback(paramIOIfaceCallback);
        this.mOifaceCallback = paramIOIfaceCallback;
        return true;
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return false;
      }  
    return false;
  }
  
  public int getBatteryRemain() {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.getBatteryRemain();
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return -1;
      }  
    return -1;
  }
  
  public float getBatteryCurrentNow() {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.getBatteryCurrentNow();
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return -1.0F;
      }  
    return -1.0F;
  }
  
  public int getSuperVOOCStatus() {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.getSuperVOOCStatus();
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return -1;
      }  
    return -1;
  }
  
  public float[] getGPASystemInfo() {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.getGPASystemInfo();
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return null;
      }  
    return null;
  }
  
  public void setCoolExFilterType(int paramInt, String paramString) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.setCoolExFilterType(paramInt, paramString);
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
      }  
  }
  
  public void setGameModeStatus(int paramInt, String paramString) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.setGameModeStatus(paramInt, paramString);
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
      }  
  }
  
  public int getGameModeStatus() {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.getGameModeStatus();
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return -1;
      }  
    return -1;
  }
  
  public String getCurrentGamePackage() {
    checkService();
    String str1 = null;
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    String str2 = str1;
    if (iOIfaceInternalService != null)
      try {
        str2 = iOIfaceInternalService.getCurrentGamePackage();
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        str2 = str1;
      }  
    return str2;
  }
  
  public void setInstalledGameList(String[] paramArrayOfString) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        iOIfaceInternalService.setInstalledGameList(paramArrayOfString);
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
      }  
  }
  
  public String[] getInstalledGameList() {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.getInstalledGameList();
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return null;
      }  
    return null;
  }
  
  public String getCpuTimeInState() {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.getCpuTimeInState();
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return null;
      }  
    return null;
  }
  
  public String triggerFrameStat(String paramString1, String paramString2) {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.triggerFrameStat(paramString1, paramString2);
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return null;
      }  
    return null;
  }
  
  public String getChipName() {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.getChipName();
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return null;
      }  
    return null;
  }
  
  public int[] getCpuClusterInfo() {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.getCpuClusterInfo();
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return null;
      }  
    return null;
  }
  
  public int getBatteryFCC() {
    checkService();
    IOIfaceInternalService iOIfaceInternalService = this.mService;
    if (iOIfaceInternalService != null)
      try {
        return iOIfaceInternalService.getBatteryFCC();
      } catch (Exception exception) {
        Log.e("OifaceManager", exception.toString());
        return -1;
      }  
    return -1;
  }
}
