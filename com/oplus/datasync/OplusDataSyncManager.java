package com.oplus.datasync;

import android.app.OplusActivityManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

public class OplusDataSyncManager {
  public static final String MODULE_INTERCEPT_SCREEN_WINDOW = "module_intercept_screen_window";
  
  private static final String TAG = "OplusDataSyncManager";
  
  private static volatile OplusDataSyncManager sInstance;
  
  public static OplusDataSyncManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/datasync/OplusDataSyncManager.sInstance : Lcom/oplus/datasync/OplusDataSyncManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/datasync/OplusDataSyncManager
    //   8: monitorenter
    //   9: getstatic com/oplus/datasync/OplusDataSyncManager.sInstance : Lcom/oplus/datasync/OplusDataSyncManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/datasync/OplusDataSyncManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/datasync/OplusDataSyncManager.sInstance : Lcom/oplus/datasync/OplusDataSyncManager;
    //   27: ldc com/oplus/datasync/OplusDataSyncManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/datasync/OplusDataSyncManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/datasync/OplusDataSyncManager.sInstance : Lcom/oplus/datasync/OplusDataSyncManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #36	-> 0
    //   #37	-> 6
    //   #38	-> 9
    //   #39	-> 15
    //   #41	-> 27
    //   #43	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  private OplusActivityManager mOAms = new OplusActivityManager();
  
  public boolean updateAppData(String paramString, Bundle paramBundle) {
    try {
      return this.mOAms.updateAppData(paramString, paramBundle);
    } catch (RemoteException remoteException) {
      Log.e("OplusDataSyncManager", "updateAppData remoteException ");
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public boolean registerSysStateChangeObserver(String paramString, ISysStateChangeCallback paramISysStateChangeCallback) {
    try {
      return this.mOAms.registerSysStateChangeObserver(paramString, paramISysStateChangeCallback);
    } catch (RemoteException remoteException) {
      Log.e("OplusDataSyncManager", "registerSysStateChangeObserver remoteException ");
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public boolean unregisterSysStateChangeObserver(String paramString, ISysStateChangeCallback paramISysStateChangeCallback) {
    try {
      return this.mOAms.unregisterSysStateChangeObserver(paramString, paramISysStateChangeCallback);
    } catch (RemoteException remoteException) {
      Log.e("OplusDataSyncManager", "unregisterSysStateChangeObserver remoteException ");
      remoteException.printStackTrace();
      return false;
    } 
  }
}
