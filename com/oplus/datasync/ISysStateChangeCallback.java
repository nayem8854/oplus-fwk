package com.oplus.datasync;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISysStateChangeCallback extends IInterface {
  void onSysStateChanged(Bundle paramBundle) throws RemoteException;
  
  class Default implements ISysStateChangeCallback {
    public void onSysStateChanged(Bundle param1Bundle) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISysStateChangeCallback {
    private static final String DESCRIPTOR = "com.oplus.datasync.ISysStateChangeCallback";
    
    static final int TRANSACTION_onSysStateChanged = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.datasync.ISysStateChangeCallback");
    }
    
    public static ISysStateChangeCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.datasync.ISysStateChangeCallback");
      if (iInterface != null && iInterface instanceof ISysStateChangeCallback)
        return (ISysStateChangeCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onSysStateChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.datasync.ISysStateChangeCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.datasync.ISysStateChangeCallback");
      if (param1Parcel1.readInt() != 0) {
        Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onSysStateChanged((Bundle)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements ISysStateChangeCallback {
      public static ISysStateChangeCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.datasync.ISysStateChangeCallback";
      }
      
      public void onSysStateChanged(Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.datasync.ISysStateChangeCallback");
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ISysStateChangeCallback.Stub.getDefaultImpl() != null) {
            ISysStateChangeCallback.Stub.getDefaultImpl().onSysStateChanged(param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISysStateChangeCallback param1ISysStateChangeCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISysStateChangeCallback != null) {
          Proxy.sDefaultImpl = param1ISysStateChangeCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISysStateChangeCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
