package com.oplus.multiuser;

import android.content.pm.UserInfo;
import android.os.IBinder;
import android.os.IUserManager;
import android.os.Parcel;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.util.Singleton;
import java.util.ArrayList;
import java.util.List;

public class OplusMultiUserManager {
  private IBinder mRemote = null;
  
  static {
    ArrayList<String> arrayList = new ArrayList();
    arrayList.add("com.heytap.cloud");
    sForbiddenPkgList.add("com.coloros.findphone.client2");
    sForbiddenPkgList.add("com.coloros.findmyphone");
  }
  
  private static final Singleton<OplusMultiUserManager> sOplusMultiUserManagerSingleton = (Singleton<OplusMultiUserManager>)new Object();
  
  private static final Singleton<IUserManager> sUserManagerServiceSingleton = (Singleton<IUserManager>)new Object();
  
  public static final int FLAG_MULTI_SYSTEM = 536870912;
  
  public static final int GET_MULTI_SYSTEM_USERID_FLAGS = 1000002;
  
  private static final int OPLUS_CALL_TRANSACTION_INDEX = 1000000;
  
  private static final int OPLUS_FIRST_CALL_TRANSACTION = 1000001;
  
  public static final String USER_MANAGER_SERVICE_DESCRIPTOR = "android.hardware.fingerprint.IFingerprintService";
  
  private static final List<String> sForbiddenPkgList;
  
  private static IUserManager getUserManagerService() {
    return (IUserManager)sUserManagerServiceSingleton.get();
  }
  
  public static OplusMultiUserManager getInstance() {
    return (OplusMultiUserManager)sOplusMultiUserManagerSingleton.get();
  }
  
  public OplusMultiUserManager() {
    ensureRemoteUserService();
  }
  
  private void ensureRemoteUserService() {
    if (this.mRemote == null)
      this.mRemote = ServiceManager.getService("user"); 
  }
  
  public boolean hasMultiSystemUser() {
    boolean bool;
    if (-10000 != getMultiSystemUserIdNoCheck()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getMultiSystemUserId() {
    IUserManager iUserManager = getUserManagerService();
    if (iUserManager != null)
      try {
        List list = iUserManager.getUsers(true, true, true);
        for (UserInfo userInfo : list) {
          if (isMultiSystemUser(userInfo))
            return userInfo.id; 
        } 
      } catch (Exception exception) {
        exception.printStackTrace();
      }  
    return -10000;
  }
  
  public boolean isMultiSystemUserId(int paramInt) {
    int i = getMultiSystemUserIdNoCheck();
    if (-10000 != i && paramInt == i)
      return true; 
    return false;
  }
  
  public boolean isMultiSystemUserHandle(UserHandle paramUserHandle) {
    if (paramUserHandle == null)
      return false; 
    return isMultiSystemUserId(paramUserHandle.getIdentifier());
  }
  
  public boolean isMultiSystemUser(UserInfo paramUserInfo) {
    if (paramUserInfo != null && (paramUserInfo.flags & 0x20000000) != 0)
      return true; 
    return false;
  }
  
  public static boolean isMultiSystemForbiddenPkg(String paramString) {
    return sForbiddenPkgList.contains(paramString);
  }
  
  public static List<String> getForbiddenPkgList() {
    return sForbiddenPkgList;
  }
  
  private int getMultiSystemUserIdNoCheck() {
    ensureRemoteUserService();
    int i = -10000;
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
      this.mRemote.transact(1000002, parcel1, parcel2, 0);
      parcel2.readException();
      int j = parcel2.readInt();
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      Exception exception;
    } 
    parcel1.recycle();
    parcel2.recycle();
    return i;
  }
}
