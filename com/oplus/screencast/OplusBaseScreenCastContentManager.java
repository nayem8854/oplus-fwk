package com.oplus.screencast;

import android.os.IBinder;
import android.os.ServiceManager;

public abstract class OplusBaseScreenCastContentManager implements IOplusScreenCastContentManager {
  protected IBinder mRemote;
  
  public OplusBaseScreenCastContentManager(String paramString) {
    this(ServiceManager.getService(paramString));
  }
  
  private OplusBaseScreenCastContentManager(IBinder paramIBinder) {
    this.mRemote = paramIBinder;
  }
}
