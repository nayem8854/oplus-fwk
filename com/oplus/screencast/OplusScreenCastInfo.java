package com.oplus.screencast;

import android.os.Parcel;
import android.os.Parcelable;

public final class OplusScreenCastInfo implements Parcelable {
  public OplusScreenCastInfo() {}
  
  public OplusScreenCastInfo(Parcel paramParcel) {
    boolean bool;
    this.castMode = paramParcel.readInt();
    this.castFlag = paramParcel.readInt();
    this.castPkg = paramParcel.readString();
    if (paramParcel.readByte() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.appDied = bool;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.castMode);
    paramParcel.writeInt(this.castFlag);
    paramParcel.writeString(this.castPkg);
    paramParcel.writeByte((byte)this.appDied);
  }
  
  public static final Parcelable.Creator<OplusScreenCastInfo> CREATOR = new Parcelable.Creator<OplusScreenCastInfo>() {
      public OplusScreenCastInfo createFromParcel(Parcel param1Parcel) {
        return new OplusScreenCastInfo(param1Parcel);
      }
      
      public OplusScreenCastInfo[] newArray(int param1Int) {
        return new OplusScreenCastInfo[param1Int];
      }
    };
  
  public boolean appDied;
  
  public int castFlag;
  
  public int castMode;
  
  public String castPkg;
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("OplusScreenCastInfo = { ");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" castMode = ");
    stringBuilder2.append(this.castMode);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" castFlag = ");
    stringBuilder2.append(this.castFlag);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" castPkg = ");
    stringBuilder2.append(this.castPkg);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" appDied = ");
    stringBuilder2.append(this.appDied);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append("}");
    return stringBuilder1.toString();
  }
}
