package com.oplus.screencast;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.ArrayMap;
import android.util.Log;
import java.util.Map;

public class OplusScreenCastContentManager extends OplusBaseScreenCastContentManager {
  private static final boolean CAST_DEBUG = true;
  
  public static final int FLAG_MASK = 65535;
  
  public static final int FLAG_POWER_SAVE = 1;
  
  public static final int FLAG_PRIVACY_PROTECTION = 16;
  
  public static final int MODE_MASK = -65536;
  
  public static final int MODE_MIRROR_CAST = 16777216;
  
  public static final int MODE_SINGLE_APP_CAST = 33554432;
  
  private static final String TAG = "OplusScreenCastContentManager";
  
  private static volatile OplusScreenCastContentManager sInstance;
  
  private final Map<OnScreenCastStateObserver, IOplusScreenCastStateObserver> mScreencastStateObservers = (Map<OnScreenCastStateObserver, IOplusScreenCastStateObserver>)new ArrayMap();
  
  public static OplusScreenCastContentManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/screencast/OplusScreenCastContentManager.sInstance : Lcom/oplus/screencast/OplusScreenCastContentManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/screencast/OplusScreenCastContentManager
    //   8: monitorenter
    //   9: getstatic com/oplus/screencast/OplusScreenCastContentManager.sInstance : Lcom/oplus/screencast/OplusScreenCastContentManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/screencast/OplusScreenCastContentManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/screencast/OplusScreenCastContentManager.sInstance : Lcom/oplus/screencast/OplusScreenCastContentManager;
    //   27: ldc com/oplus/screencast/OplusScreenCastContentManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/screencast/OplusScreenCastContentManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/screencast/OplusScreenCastContentManager.sInstance : Lcom/oplus/screencast/OplusScreenCastContentManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #60	-> 0
    //   #61	-> 6
    //   #62	-> 9
    //   #63	-> 15
    //   #65	-> 27
    //   #67	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  private OplusScreenCastContentManager() {
    super("activity_task");
  }
  
  public boolean requestScreenCastContentMode(int paramInt1, int paramInt2) throws RemoteException {
    return requestScreenCastContentMode(paramInt1, paramInt2, null);
  }
  
  public boolean requestScreenCastContentMode(int paramInt1, int paramInt2, Bundle paramBundle) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      if (paramBundle != null) {
        parcel1.writeInt(1);
        parcel1.writeBundle(paramBundle);
      } else {
        parcel1.writeInt(0);
      } 
      this.mRemote.transact(30002, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void resetScreenCastContentMode() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(30003, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public OplusScreenCastInfo getScreenCastContentMode() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(30004, parcel1, parcel2, 0);
      parcel2.readException();
      return (OplusScreenCastInfo)OplusScreenCastInfo.CREATOR.createFromParcel(parcel2);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean registerScreenCastStateObserver(Context paramContext, OnScreenCastStateObserver paramOnScreenCastStateObserver) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("registerScreenCastStateObserver observer = ");
    stringBuilder.append(paramOnScreenCastStateObserver);
    Log.i("OplusScreenCastContentManager", stringBuilder.toString());
    if (paramOnScreenCastStateObserver == null || paramContext == null)
      return false; 
    synchronized (this.mScreencastStateObservers) {
      if (this.mScreencastStateObservers.get(paramOnScreenCastStateObserver) != null) {
        Log.e("OplusScreenCastContentManager", "already register before");
        return false;
      } 
      OnScreenCastStateObserverDelegate onScreenCastStateObserverDelegate = new OnScreenCastStateObserverDelegate();
      this(this, paramOnScreenCastStateObserver);
      try {
        boolean bool = registerScreenCastStateObserverInner(paramContext.getPackageName(), onScreenCastStateObserverDelegate);
        if (bool)
          this.mScreencastStateObservers.put(paramOnScreenCastStateObserver, onScreenCastStateObserverDelegate); 
        return bool;
      } catch (RemoteException remoteException) {
        Log.e("OplusScreenCastContentManager", "registerScreenCastStateObserver remoteException ");
        remoteException.printStackTrace();
        return false;
      } 
    } 
  }
  
  public boolean unregisterScreenCastStateObserver(Context paramContext, OnScreenCastStateObserver paramOnScreenCastStateObserver) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("unregisterScreenCastStateObserver observer = ");
    stringBuilder.append(paramOnScreenCastStateObserver);
    Log.i("OplusScreenCastContentManager", stringBuilder.toString());
    if (paramOnScreenCastStateObserver == null || paramContext == null)
      return false; 
    synchronized (this.mScreencastStateObservers) {
      IOplusScreenCastStateObserver iOplusScreenCastStateObserver = this.mScreencastStateObservers.get(paramOnScreenCastStateObserver);
      if (iOplusScreenCastStateObserver != null)
        try {
          this.mScreencastStateObservers.remove(paramOnScreenCastStateObserver);
          return unregisterScreenCastStateObserverInner(paramContext.getPackageName(), iOplusScreenCastStateObserver);
        } catch (RemoteException remoteException) {
          Log.e("OplusScreenCastContentManager", "unregisterScreenCastStateObserver remoteException ");
          remoteException.printStackTrace();
        }  
      return false;
    } 
  }
  
  private boolean registerScreenCastStateObserverInner(String paramString, IOplusScreenCastStateObserver paramIOplusScreenCastStateObserver) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString);
      if (paramIOplusScreenCastStateObserver != null) {
        IBinder iBinder = paramIOplusScreenCastStateObserver.asBinder();
      } else {
        paramString = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramString);
      this.mRemote.transact(30005, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  private boolean unregisterScreenCastStateObserverInner(String paramString, IOplusScreenCastStateObserver paramIOplusScreenCastStateObserver) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString);
      if (paramIOplusScreenCastStateObserver != null) {
        IBinder iBinder = paramIOplusScreenCastStateObserver.asBinder();
      } else {
        paramString = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramString);
      this.mRemote.transact(30006, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  class OnScreenCastStateObserver {
    public abstract void onContentConfigChanged(OplusScreenCastInfo param1OplusScreenCastInfo);
  }
  
  class OnScreenCastStateObserverDelegate extends IOplusScreenCastStateObserver.Stub {
    private final OplusScreenCastContentManager.OnScreenCastStateObserver mObserver;
    
    final OplusScreenCastContentManager this$0;
    
    public OnScreenCastStateObserverDelegate(OplusScreenCastContentManager.OnScreenCastStateObserver param1OnScreenCastStateObserver) {
      this.mObserver = param1OnScreenCastStateObserver;
    }
    
    public void onContentConfigChanged(OplusScreenCastInfo param1OplusScreenCastInfo) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onContentConfigChanged info = ");
      stringBuilder.append(param1OplusScreenCastInfo);
      Log.d("OplusScreenCastContentManager", stringBuilder.toString());
      this.mObserver.onContentConfigChanged(param1OplusScreenCastInfo);
    }
  }
}
