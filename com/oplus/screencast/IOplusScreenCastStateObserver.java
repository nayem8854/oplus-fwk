package com.oplus.screencast;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusScreenCastStateObserver extends IInterface {
  void onContentConfigChanged(OplusScreenCastInfo paramOplusScreenCastInfo) throws RemoteException;
  
  class Default implements IOplusScreenCastStateObserver {
    public void onContentConfigChanged(OplusScreenCastInfo param1OplusScreenCastInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusScreenCastStateObserver {
    private static final String DESCRIPTOR = "com.oplus.screencast.IOplusScreenCastStateObserver";
    
    static final int TRANSACTION_onContentConfigChanged = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.screencast.IOplusScreenCastStateObserver");
    }
    
    public static IOplusScreenCastStateObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.screencast.IOplusScreenCastStateObserver");
      if (iInterface != null && iInterface instanceof IOplusScreenCastStateObserver)
        return (IOplusScreenCastStateObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onContentConfigChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.screencast.IOplusScreenCastStateObserver");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.screencast.IOplusScreenCastStateObserver");
      if (param1Parcel1.readInt() != 0) {
        OplusScreenCastInfo oplusScreenCastInfo = (OplusScreenCastInfo)OplusScreenCastInfo.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onContentConfigChanged((OplusScreenCastInfo)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IOplusScreenCastStateObserver {
      public static IOplusScreenCastStateObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.screencast.IOplusScreenCastStateObserver";
      }
      
      public void onContentConfigChanged(OplusScreenCastInfo param2OplusScreenCastInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.screencast.IOplusScreenCastStateObserver");
          if (param2OplusScreenCastInfo != null) {
            parcel.writeInt(1);
            param2OplusScreenCastInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusScreenCastStateObserver.Stub.getDefaultImpl() != null) {
            IOplusScreenCastStateObserver.Stub.getDefaultImpl().onContentConfigChanged(param2OplusScreenCastInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusScreenCastStateObserver param1IOplusScreenCastStateObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusScreenCastStateObserver != null) {
          Proxy.sDefaultImpl = param1IOplusScreenCastStateObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusScreenCastStateObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
