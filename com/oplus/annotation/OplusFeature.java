package com.oplus.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface OplusFeature {
  OplusFeatureType value();
  
  public enum OplusFeatureType {
    MEMORY_FEATURE, MEMORY_NATIVE_FEATURE, PERSIST_FEATURE, PERSIST_NATIVE_FEATURE, READONLY_FEATURE, READONLY_NATIVE_FEATURE;
    
    private static final OplusFeatureType[] $VALUES;
    
    static {
      MEMORY_NATIVE_FEATURE = new OplusFeatureType("MEMORY_NATIVE_FEATURE", 4);
      OplusFeatureType oplusFeatureType = new OplusFeatureType("PERSIST_NATIVE_FEATURE", 5);
      $VALUES = new OplusFeatureType[] { READONLY_FEATURE, MEMORY_FEATURE, PERSIST_FEATURE, READONLY_NATIVE_FEATURE, MEMORY_NATIVE_FEATURE, oplusFeatureType };
    }
  }
}
