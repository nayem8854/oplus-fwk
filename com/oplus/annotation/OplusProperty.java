package com.oplus.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface OplusProperty {
  OplusPropertyType value();
  
  public enum OplusPropertyType {
    PERSIST_PROPERTY, RO_PROPERTY, SYS_PROPERTY;
    
    private static final OplusPropertyType[] $VALUES;
    
    static {
      OplusPropertyType oplusPropertyType = new OplusPropertyType("SYS_PROPERTY", 2);
      $VALUES = new OplusPropertyType[] { RO_PROPERTY, PERSIST_PROPERTY, oplusPropertyType };
    }
  }
}
