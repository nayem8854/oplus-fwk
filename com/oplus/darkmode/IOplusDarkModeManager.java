package com.oplus.darkmode;

import android.app.Application;
import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.NinePatch;
import android.graphics.OplusBaseBaseCanvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RecordingCanvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.OplusBaseDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.io.InputStream;

public interface IOplusDarkModeManager extends IOplusCommonFeature {
  public static final IOplusDarkModeManager DEFAULT = (IOplusDarkModeManager)new Object();
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusDarkModeManager;
  }
  
  default IOplusDarkModeManager getDefault() {
    return DEFAULT;
  }
  
  default void init(Context paramContext) {}
  
  default boolean forceDarkAllowedDefault(Context paramContext, boolean paramBoolean) {
    return paramBoolean;
  }
  
  default void changeUsageForceDarkAlgorithmType(View paramView, int paramInt) {}
  
  default int hideAutoChangeUiMode(int paramInt) {
    return paramInt;
  }
  
  default boolean useForcePowerSave() {
    return true;
  }
  
  default void logForceDarkAllowedStatus(Context paramContext, boolean paramBoolean) {}
  
  default void logConfigurationNightError(Context paramContext, boolean paramBoolean) {}
  
  default void setDarkModeProgress(View paramView) {}
  
  default boolean forceDarkWithoutTheme(Context paramContext, boolean paramBoolean) {
    return paramBoolean;
  }
  
  default void startDelayInjectJS(WebView paramWebView) {}
  
  default WebViewClient createWebViewClientWrapper(WebView paramWebView, WebViewClient paramWebViewClient) {
    return paramWebViewClient;
  }
  
  default void refreshForceDark(View paramView) {}
  
  default int changeSystemUiVisibility(int paramInt) {
    return paramInt;
  }
  
  default void refreshForceDark(View paramView, int paramInt, float paramFloat1, float paramFloat2, float paramFloat3) {}
  
  default int getDarkModeData(String paramString) {
    return 0;
  }
  
  default boolean isDarkModePage(String paramString, boolean paramBoolean) {
    return paramBoolean;
  }
  
  default float getDarkModeDialogBgMaxL(String paramString) {
    return -1.0F;
  }
  
  default float getDarkModeBackgroundMaxL(String paramString) {
    return -1.0F;
  }
  
  default float getDarkModeForegroundMinL(String paramString) {
    return -1.0F;
  }
  
  default IOplusDarkModeManager newOplusDarkModeManager() {
    return this;
  }
  
  default void forceDarkWithoutTheme(Context paramContext, View paramView, boolean paramBoolean) {}
  
  default boolean shouldInterceptConfigRelaunch(int paramInt, Configuration paramConfiguration) {
    return false;
  }
  
  default boolean setDarkModeProgress(View paramView, Configuration paramConfiguration) {
    return false;
  }
  
  default boolean ensureWebSettingDarkMode(WebView paramWebView) {
    return false;
  }
  
  default void refreshForceDark(View paramView, OplusDarkModeData paramOplusDarkModeData) {}
  
  default void markViewTypeBySize(View paramView) {}
  
  default void initDarkModeStatus(Application paramApplication) {}
  
  default boolean darkenSplitScreenDrawable(View paramView, Drawable paramDrawable, int paramInt1, int paramInt2, int paramInt3, int paramInt4, RecordingCanvas paramRecordingCanvas) {
    return true;
  }
  
  default int handleEraseColor(int paramInt) {
    return paramInt;
  }
  
  default boolean shouldIntercept() {
    return false;
  }
  
  default Bitmap handleDecodeStream(InputStream paramInputStream, Rect paramRect, BitmapFactory.Options paramOptions) {
    return null;
  }
  
  default Shader getDarkModeLinearGradient(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int[] paramArrayOfint, float[] paramArrayOffloat, int paramInt1, int paramInt2, Shader.TileMode paramTileMode) {
    return null;
  }
  
  default Shader getDarkModeRadialGradient(float paramFloat1, float paramFloat2, float paramFloat3, int[] paramArrayOfint, int paramInt1, float[] paramArrayOffloat, int paramInt2, Shader.TileMode paramTileMode) {
    return null;
  }
  
  default Shader getDarkModeSweepGradient(float paramFloat1, float paramFloat2, int[] paramArrayOfint, float[] paramArrayOffloat, int paramInt1, int paramInt2) {
    return null;
  }
  
  default int getVectorColor(int paramInt) {
    return paramInt;
  }
  
  default void changeColorFilterInDarkMode(ColorFilter paramColorFilter) {}
  
  default boolean isInDarkMode(boolean paramBoolean) {
    return false;
  }
  
  default OplusBaseBaseCanvas.RealPaintState getRealPaintState(Paint paramPaint) {
    return null;
  }
  
  default void changePaintWhenDrawText(Paint paramPaint) {}
  
  default void resetRealPaintIfNeed(Paint paramPaint, OplusBaseBaseCanvas.RealPaintState paramRealPaintState) {}
  
  default void changePaintWhenDrawArea(Paint paramPaint, RectF paramRectF) {}
  
  default void changePaintWhenDrawArea(Paint paramPaint, RectF paramRectF, Path paramPath) {}
  
  default void changePaintWhenDrawPatch(NinePatch paramNinePatch, Paint paramPaint, RectF paramRectF) {}
  
  default int changeWhenDrawColor(int paramInt, boolean paramBoolean) {
    return paramInt;
  }
  
  default void changePaintWhenDrawBitmap(Paint paramPaint, Bitmap paramBitmap, RectF paramRectF) {}
  
  default int[] getDarkModeColors(int[] paramArrayOfint) {
    return paramArrayOfint;
  }
  
  default Paint getPaintWhenDrawPatch(NinePatch paramNinePatch, Paint paramPaint, RectF paramRectF) {
    return null;
  }
  
  default Paint getPaintWhenDrawBitmap(Paint paramPaint, Bitmap paramBitmap, RectF paramRectF) {
    return null;
  }
  
  default void markDrawChild(ViewGroup paramViewGroup, View paramView, Canvas paramCanvas) {}
  
  default void markDispatchDraw(ViewGroup paramViewGroup, Canvas paramCanvas) {}
  
  default void markBackground(View paramView, Canvas paramCanvas) {}
  
  default void markForeground(View paramView, Canvas paramCanvas) {}
  
  default void markOnDraw(View paramView, Canvas paramCanvas) {}
  
  default void markDrawFadingEdge(View paramView, Canvas paramCanvas) {}
  
  default void changePaintWhenDrawArea(Paint paramPaint, RectF paramRectF, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {}
  
  default void changePaintWhenDrawArea(Paint paramPaint, RectF paramRectF, Path paramPath, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {}
  
  default void changePaintWhenDrawPatch(NinePatch paramNinePatch, Paint paramPaint, RectF paramRectF, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {}
  
  default int changeWhenDrawColor(int paramInt, boolean paramBoolean, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    return paramInt;
  }
  
  default void changePaintWhenDrawBitmap(Paint paramPaint, Bitmap paramBitmap, RectF paramRectF, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {}
  
  default int[] getDarkModeColors(int[] paramArrayOfint, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    return paramArrayOfint;
  }
  
  default Paint getPaintWhenDrawPatch(NinePatch paramNinePatch, Paint paramPaint, RectF paramRectF, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    return null;
  }
  
  default Paint getPaintWhenDrawBitmap(Paint paramPaint, Bitmap paramBitmap, RectF paramRectF, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    return null;
  }
  
  default void changePaintWhenDrawText(Paint paramPaint, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {}
  
  default ColorFilter getColorFilterWhenDrawVectorDrawable(OplusBaseDrawable.SumEntity paramSumEntity1, OplusBaseDrawable.SumEntity paramSumEntity2, OplusBaseDrawable.SumEntity paramSumEntity3) {
    return null;
  }
}
