package com.oplus.darkmode;

import android.app.Application;
import android.app.OplusActivityManager;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.OplusBaseConfiguration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.HardwareRenderer;
import android.graphics.NinePatch;
import android.graphics.OplusBaseBaseCanvas;
import android.graphics.OplusBaseHardwareRenderer;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RecordingCanvas;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.OplusBaseDrawable;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.util.Log;
import android.view.OplusBaseView;
import android.view.ThreadedRenderer;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.android.internal.R;
import com.google.android.collect.Sets;
import com.oplus.util.OplusTypeCastingHelper;
import java.util.Set;

public class OplusDarkModeManager implements IOplusDarkModeManager {
  static {
    boolean bool = SystemProperties.getBoolean("persist.sys.assert.panic", false);
    DEBUG = bool | false;
  }
  
  private boolean mIsChangeSystemUiVisibility = false;
  
  private static final Set<String> WHITE_LIST_PACKAGE = Sets.newHashSet((Object[])new String[] { 
        "com.android.systemui", "com.android.settings", "com.android.browser", "com.android.phone", "com.android.wallpaper.livepicker", "com.android.calculator2", "com.android.calendar", "com.android.contacts", "com.android.mms", "com.android.stk", 
        "com.android.packageinstaller", "com.android.permissioncontroller", "com.coloros.eyeprotect", "system_process", "com.finshell.wallet", "android", "com.redteamobile.roaming", "com.coloros.floatassistant" });
  
  private float mDialogBgMaxL = 27.0F;
  
  private float mBackgroundMaxL = 0.0F;
  
  private float mForegroundMinL = 100.0F;
  
  private boolean isColorOSForceDarkCustom = false;
  
  private int mSelectForceDarkType = -1;
  
  private static boolean mIsHidden = false;
  
  private static boolean mShouldInvalidWorld = false;
  
  private static final String ATTRS_COLOROS_FORCE_DARK_CUSTOM = "enableFollowSystemForceDarkRank";
  
  private static final String ATTRS_COLOROS_SELECT_FORCE_DARK_TYPE = "selectSystemForceDarkType";
  
  private static final boolean DBG;
  
  private static final boolean DEBUG;
  
  private static final float DEFAULT_BACKGROUNDMAXL = 0.0F;
  
  private static final float DEFAULT_DIALOGBGMAXL = 27.0F;
  
  private static final float DEFAULT_FOREGROUNDMINL = 100.0F;
  
  private static final float GENTLE_BACKGROUNDMAXL = 19.0F;
  
  private static final float MIDDLE_BACKGROUNDMAXL = 9.0F;
  
  private static final String TAG = "OplusDarkModeManager";
  
  private static int mSupportStyle;
  
  private static boolean mUseThirdPartyInvert;
  
  private OplusActivityManager mOplusActivityManager;
  
  private static class Holder {
    private static final OplusDarkModeManager INSTANCE = new OplusDarkModeManager();
  }
  
  public static OplusDarkModeManager getInstance() {
    return Holder.INSTANCE;
  }
  
  public boolean useForcePowerSave() {
    return false;
  }
  
  public int changeSystemUiVisibility(int paramInt) {
    if (mUseThirdPartyInvert) {
      if ((paramInt & 0x2000) != 0)
        this.mIsChangeSystemUiVisibility = true; 
      return paramInt & 0xFFFFDFFF & 0xFFFFFFEF;
    } 
    if (this.mIsChangeSystemUiVisibility) {
      this.mIsChangeSystemUiVisibility = false;
      return paramInt | 0x2000 | 0x10;
    } 
    return paramInt;
  }
  
  public boolean shouldInterceptConfigRelaunch(int paramInt, Configuration paramConfiguration) {
    if ((0x10000000 & paramInt) != 0) {
      OplusBaseConfiguration oplusBaseConfiguration = (OplusBaseConfiguration)OplusTypeCastingHelper.typeCasting(OplusBaseConfiguration.class, paramConfiguration);
      if (oplusBaseConfiguration != null && 
        (oplusBaseConfiguration.getOplusExtraConfiguration()).mOplusConfigType == 1L) {
        boolean bool;
        if ((0xEFFFFFFF & paramInt) != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        if (bool) {
          if (DEBUG) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("shouldInterceptConfigRelaunch-->has dark mode rank change but also other diff-->diff:");
            stringBuilder.append(paramInt);
            Log.d("OplusDarkModeManager", stringBuilder.toString());
          } 
          return false;
        } 
        if (DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("shouldInterceptConfigRelaunch-->success-->diff:");
          stringBuilder.append(paramInt);
          Log.d("OplusDarkModeManager", stringBuilder.toString());
        } 
        return true;
      } 
    } 
    return false;
  }
  
  private boolean setDarkStyleArgs(Configuration paramConfiguration) {
    OplusBaseConfiguration oplusBaseConfiguration = (OplusBaseConfiguration)OplusTypeCastingHelper.typeCasting(OplusBaseConfiguration.class, paramConfiguration);
    if (oplusBaseConfiguration != null) {
      this.mDialogBgMaxL = (oplusBaseConfiguration.getOplusExtraConfiguration()).mDarkModeDialogBgMaxL;
      this.mBackgroundMaxL = (oplusBaseConfiguration.getOplusExtraConfiguration()).mDarkModeBackgroundMaxL;
      this.mForegroundMinL = (oplusBaseConfiguration.getOplusExtraConfiguration()).mDarkModeForegroundMinL;
      if (this.mDialogBgMaxL == -1.0F)
        this.mDialogBgMaxL = 27.0F; 
      if (this.mBackgroundMaxL == -1.0F)
        this.mBackgroundMaxL = 0.0F; 
      if (this.mForegroundMinL == -1.0F)
        this.mForegroundMinL = 100.0F; 
      return true;
    } 
    return false;
  }
  
  public boolean setDarkModeProgress(View paramView, Configuration paramConfiguration) {
    if (paramView == null)
      return false; 
    boolean bool = false;
    if (setDarkStyleArgs(paramConfiguration)) {
      StringBuilder stringBuilder;
      boolean bool1;
      ThreadedRenderer threadedRenderer = paramView.getThreadedRenderer();
      if (this.isColorOSForceDarkCustom) {
        bool = setForceDarkArgs((HardwareRenderer)threadedRenderer, this.mDialogBgMaxL, this.mBackgroundMaxL, this.mForegroundMinL);
        bool1 = bool;
        if (DEBUG) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("setDarkModeProgress-->");
          stringBuilder.append(paramView.getContext().getPackageName());
          stringBuilder.append("-->isColorOSForceDarkCustom-->mDialogBgMaxL:");
          stringBuilder.append(this.mDialogBgMaxL);
          stringBuilder.append("-->mBackgroundMaxL:");
          stringBuilder.append(this.mBackgroundMaxL);
          stringBuilder.append(",mForegroundMinL:");
          stringBuilder.append(this.mForegroundMinL);
          Log.d("OplusDarkModeManager", stringBuilder.toString());
          bool1 = bool;
        } 
      } else {
        bool1 = setForceDarkArgs((HardwareRenderer)stringBuilder, 27.0F, 0.0F, 100.0F);
      } 
      bool = bool1;
      if (mUseThirdPartyInvert) {
        checkThirdInvertArgs();
        bool = bool1;
        if (mShouldInvalidWorld) {
          invalidateWorld(paramView);
          mShouldInvalidWorld = false;
          bool = bool1;
          if (DEBUG) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("setDarkModeProgress-->");
            stringBuilder.append(paramView.getContext().getPackageName());
            stringBuilder.append("-->checkThirdInvertArgs to invalidateWorld");
            Log.d("OplusDarkModeManager", stringBuilder.toString());
            bool = bool1;
          } 
        } 
      } 
    } 
    return bool;
  }
  
  private void checkThirdInvertArgs() {
    float f1 = OplusDarkModeThirdInvertManager.getInstance().getBackgroundMaxL();
    float f2 = OplusDarkModeThirdInvertManager.getInstance().getForegroundMinL();
    int i = mSupportStyle;
    if (i != 2) {
      if (i != 3) {
        if (i == 4) {
          this.mBackgroundMaxL = 19.0F;
          this.mForegroundMinL = 100.0F;
        } 
      } else {
        this.mBackgroundMaxL = 9.0F;
        this.mForegroundMinL = 100.0F;
      } 
    } else {
      this.mBackgroundMaxL = 0.0F;
      this.mForegroundMinL = 100.0F;
    } 
    OplusDarkModeThirdInvertManager.getInstance().setBackgroundMaxL(this.mBackgroundMaxL);
    OplusDarkModeThirdInvertManager.getInstance().setForegroundMinL(this.mForegroundMinL);
    if (f1 != this.mBackgroundMaxL || f2 != this.mForegroundMinL)
      mShouldInvalidWorld = true; 
  }
  
  public void refreshForceDark(View paramView, OplusDarkModeData paramOplusDarkModeData) {
    if (paramView == null)
      return; 
    int i = this.mSelectForceDarkType;
    if (i != -1) {
      changeUsageForceDarkAlgorithmType(paramView, i);
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("refreshForceDark-->");
        stringBuilder.append(paramView.getContext().getPackageName());
        stringBuilder.append("-->changeUsageForceDarkAlgorithmType-->mSelectForceDarkType:");
        stringBuilder.append(this.mSelectForceDarkType);
        Log.d("OplusDarkModeManager", stringBuilder.toString());
      } 
    } 
    boolean bool = mUseThirdPartyInvert;
    if (this.isColorOSForceDarkCustom) {
      mUseThirdPartyInvert = false;
      OplusDarkModeThirdInvertManager.getInstance().setIsSupportDarkModeStatus(0);
    } else {
      mUseThirdPartyInvert = parseOpenByDarkModeData(paramView.getContext(), paramOplusDarkModeData);
      OplusDarkModeThirdInvertManager.getInstance().setIsSupportDarkModeStatus(mUseThirdPartyInvert);
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("refreshForceDark-->");
        stringBuilder.append(paramView.getContext().getPackageName());
        stringBuilder.append("-->mUseThirdPartyInvert:");
        stringBuilder.append(mUseThirdPartyInvert);
        Log.d("OplusDarkModeManager", stringBuilder.toString());
      } 
      if (mUseThirdPartyInvert) {
        setDarkStyleArgs(paramView.getContext().getResources().getConfiguration());
        checkThirdInvertArgs();
      } 
    } 
    if (bool != mUseThirdPartyInvert) {
      i = 1;
    } else {
      i = 0;
    } 
    if (!mShouldInvalidWorld && i != 0)
      mShouldInvalidWorld = true; 
    if (mShouldInvalidWorld) {
      invalidateWorld(paramView);
      mShouldInvalidWorld = false;
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("refreshForceDark-->");
        stringBuilder.append(paramView.getContext().getPackageName());
        stringBuilder.append("-->invalidateWorld");
        Log.d("OplusDarkModeManager", stringBuilder.toString());
      } 
    } 
  }
  
  private boolean setForceDarkArgs(HardwareRenderer paramHardwareRenderer, float paramFloat1, float paramFloat2, float paramFloat3) {
    OplusBaseHardwareRenderer oplusBaseHardwareRenderer = (OplusBaseHardwareRenderer)OplusTypeCastingHelper.typeCasting(OplusBaseHardwareRenderer.class, paramHardwareRenderer);
    if (oplusBaseHardwareRenderer != null)
      return oplusBaseHardwareRenderer.setForceDarkArgs(paramFloat1, paramFloat2, paramFloat3); 
    return false;
  }
  
  public boolean ensureWebSettingDarkMode(WebView paramWebView) {
    OplusBaseView oplusBaseView = (OplusBaseView)OplusTypeCastingHelper.typeCasting(OplusBaseView.class, paramWebView);
    if (oplusBaseView != null) {
      StringBuilder stringBuilder;
      if (mUseThirdPartyInvert) {
        int i = oplusBaseView.getOriginWebSettingForceDark();
        if (i == -1) {
          i = paramWebView.getSettings().getForceDark();
          oplusBaseView.setOriginWebSettingForceDark(i);
        } 
        paramWebView.getSettings().setForceDark(2);
        if (DEBUG) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("ensureWebSettingDarkMode-->");
          stringBuilder.append(paramWebView.getContext().getPackageName());
          stringBuilder.append("-->ON");
          Log.d("OplusDarkModeManager", stringBuilder.toString());
        } 
        return true;
      } 
      if (stringBuilder.getOriginWebSettingForceDark() != -1) {
        paramWebView.getSettings().setForceDark(stringBuilder.getOriginWebSettingForceDark());
        stringBuilder.setOriginWebSettingForceDark(-1);
        if (DEBUG) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("ensureWebSettingDarkMode-->");
          stringBuilder.append(paramWebView.getContext().getPackageName());
          stringBuilder.append("-->RESTORE");
          Log.d("OplusDarkModeManager", stringBuilder.toString());
        } 
        return true;
      } 
    } 
    return false;
  }
  
  void invalidateWorld(View paramView) {
    paramView.invalidate();
    if (paramView instanceof ViewGroup) {
      if (paramView instanceof WebView)
        ensureWebSettingDarkMode((WebView)paramView); 
      ViewGroup viewGroup = (ViewGroup)paramView;
      for (byte b = 0; b < viewGroup.getChildCount(); b++)
        invalidateWorld(viewGroup.getChildAt(b)); 
    } 
  }
  
  public void logForceDarkAllowedStatus(Context paramContext, boolean paramBoolean) {
    if (DEBUG) {
      TypedArray typedArray = paramContext.obtainStyledAttributes(R.styleable.Theme);
      boolean bool = true;
      boolean bool1 = typedArray.getBoolean(279, true);
      boolean bool2 = typedArray.getBoolean(278, paramBoolean);
      if (bool1 && bool2) {
        paramBoolean = bool;
      } else {
        paramBoolean = false;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("logForceDarkAllowedStatus-->");
      stringBuilder.append(paramContext.getPackageName());
      stringBuilder.append(",isLightTheme:");
      stringBuilder.append(bool1);
      stringBuilder.append(",forceDarkAllowed:");
      stringBuilder.append(bool2);
      stringBuilder.append(",useAutoDark:");
      stringBuilder.append(paramBoolean);
      Log.d("OplusDarkModeManager", stringBuilder.toString());
      typedArray.recycle();
    } 
  }
  
  public void logConfigurationNightError(Context paramContext, boolean paramBoolean) {
    if (paramBoolean) {
      Resources resources = paramContext.getResources();
      String str = paramContext.getPackageName();
      int i = resources.getIdentifier("enableFollowSystemForceDarkRank", "attr", str);
      int j = resources.getIdentifier("selectSystemForceDarkType", "attr", str);
      TypedArray typedArray = paramContext.getTheme().obtainStyledAttributes(new int[] { i, j });
      this.isColorOSForceDarkCustom = typedArray.getBoolean(0, false);
      this.mSelectForceDarkType = typedArray.getInt(1, -1);
      typedArray.recycle();
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("packageName:");
        stringBuilder.append(str);
        stringBuilder.append(",isColorOSForceDarkCustom:");
        stringBuilder.append(this.isColorOSForceDarkCustom);
        Log.d("OplusDarkModeManager", stringBuilder.toString());
      } 
    } 
  }
  
  private static boolean isSystemApp(String paramString) {
    if (WHITE_LIST_PACKAGE.contains(paramString) || 
      paramString.startsWith("com.coloros.") || 
      paramString.startsWith("com.heytap.") || 
      paramString.startsWith("com.oppo.") || 
      paramString.startsWith("com.nearme.") || 
      paramString.startsWith("com.google.android."))
      return true; 
    return false;
  }
  
  private static boolean isSystemApp(Context paramContext) {
    int i = (paramContext.getApplicationInfo()).flags;
    boolean bool = true;
    if ((i & 0x1) <= 0)
      bool = false; 
    return bool;
  }
  
  public void forceDarkWithoutTheme(Context paramContext, View paramView, boolean paramBoolean) {
    StringBuilder stringBuilder;
    if (paramContext == null) {
      if (DEBUG)
        Log.d("OplusDarkModeManager", "forceDarkWithoutTheme-->context is null!"); 
      return;
    } 
    if (this.isColorOSForceDarkCustom)
      return; 
    if (paramBoolean) {
      mUseThirdPartyInvert = false;
      OplusDarkModeThirdInvertManager.getInstance().setIsSupportDarkModeStatus(0);
      if (DEBUG) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("forceDarkWithoutTheme-->");
        stringBuilder.append(paramContext.getPackageName());
        stringBuilder.append("-->app use owner force dark!");
        Log.d("OplusDarkModeManager", stringBuilder.toString());
      } 
      return;
    } 
    boolean bool = mUseThirdPartyInvert;
    paramBoolean = true;
    mUseThirdPartyInvert = shouldUseColorForceDark(paramContext, true);
    OplusDarkModeThirdInvertManager.getInstance().setIsSupportDarkModeStatus(mUseThirdPartyInvert);
    if (DEBUG) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("forceDarkWithoutTheme-->");
      stringBuilder1.append(paramContext.getPackageName());
      stringBuilder1.append("-->mUseThirdPartyInvert:");
      stringBuilder1.append(mUseThirdPartyInvert);
      Log.d("OplusDarkModeManager", stringBuilder1.toString());
    } 
    if (!mShouldInvalidWorld) {
      if (bool == mUseThirdPartyInvert)
        paramBoolean = false; 
      mShouldInvalidWorld = paramBoolean;
    } 
    if (mShouldInvalidWorld) {
      invalidateWorld((View)stringBuilder);
      mShouldInvalidWorld = false;
      if (DEBUG) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("forceDarkWithoutTheme-->");
        stringBuilder.append(paramContext.getPackageName());
        stringBuilder.append("-->invalidateWorld");
        Log.d("OplusDarkModeManager", stringBuilder.toString());
      } 
    } 
  }
  
  public boolean shouldUseColorForceDark(Context paramContext, boolean paramBoolean) {
    if (paramContext == null || isSystemApp(paramContext))
      return false; 
    return parseOpenByDarkModeData(paramContext, getDarkModeData(paramContext, paramContext.getPackageName(), paramBoolean));
  }
  
  private boolean parseOpenByDarkModeData(Context paramContext, OplusDarkModeData paramOplusDarkModeData) {
    boolean bool4;
    int i;
    boolean bool1 = false;
    if (paramOplusDarkModeData == null || paramContext == null)
      return false; 
    boolean bool2 = paramOplusDarkModeData.mAlreadyClickByUser;
    boolean bool3 = paramOplusDarkModeData.mOpenByUser;
    long l = (paramContext.getApplicationInfo()).longVersionCode;
    if (paramOplusDarkModeData.mVersionCode == -1L || l >= paramOplusDarkModeData.mVersionCode) {
      bool4 = true;
    } else {
      bool4 = false;
    } 
    boolean bool5 = mIsHidden;
    if (bool4) {
      i = paramOplusDarkModeData.mCurType;
    } else {
      i = paramOplusDarkModeData.mOldType;
    } 
    int j = mSupportStyle;
    if (i != 1) {
      if (i != 2 && i != 3 && i != 4) {
        mSupportStyle = 0;
        mIsHidden = false;
      } else {
        mSupportStyle = i;
        mIsHidden = false;
      } 
    } else {
      mSupportStyle = 0;
      mIsHidden = true;
      bool3 = false;
    } 
    if (!mShouldInvalidWorld && (
      j != mSupportStyle || bool5 != mIsHidden))
      mShouldInvalidWorld = true; 
    bool5 = bool3;
    if (!mIsHidden) {
      bool5 = bool3;
      if (!bool3) {
        bool5 = bool3;
        if (!bool2)
          if (paramOplusDarkModeData.mIsRecommend == 3) {
            bool5 = true;
          } else if (paramOplusDarkModeData.mIsRecommend == 1) {
            bool5 = bool4;
          } else {
            bool5 = bool3;
            if (paramOplusDarkModeData.mIsRecommend == 2) {
              bool5 = bool1;
              if (!bool4)
                bool5 = true; 
            } 
          }  
      } 
    } 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("parseOpenByDarkModeData-->");
      stringBuilder.append(paramContext.getPackageName());
      stringBuilder.append("-->ver:");
      stringBuilder.append(paramOplusDarkModeData.mVersionCode);
      stringBuilder.append("-->isRec:");
      stringBuilder.append(paramOplusDarkModeData.mIsRecommend);
      stringBuilder.append("-->oldType:");
      stringBuilder.append(paramOplusDarkModeData.mOldType);
      stringBuilder.append("-->curType:");
      stringBuilder.append(paramOplusDarkModeData.mCurType);
      stringBuilder.append("-->mAlreadyClickByUser:");
      stringBuilder.append(paramOplusDarkModeData.mAlreadyClickByUser);
      stringBuilder.append("-->openByUser:");
      stringBuilder.append(paramOplusDarkModeData.mOpenByUser);
      stringBuilder.append("-->open:");
      stringBuilder.append(bool5);
      Log.d("OplusDarkModeManager", stringBuilder.toString());
    } 
    return bool5;
  }
  
  public OplusDarkModeData getDarkModeData(Context paramContext, String paramString, boolean paramBoolean) {
    OplusDarkModeData oplusDarkModeData;
    if (paramString == null)
      return null; 
    if (this.mOplusActivityManager == null)
      this.mOplusActivityManager = new OplusActivityManager(); 
    paramContext = null;
    try {
      OplusDarkModeData oplusDarkModeData1 = this.mOplusActivityManager.getDarkModeData(paramString);
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
    } 
    return oplusDarkModeData;
  }
  
  public void changeUsageForceDarkAlgorithmType(View paramView, int paramInt) {
    if (paramView != null) {
      OplusBaseView oplusBaseView = (OplusBaseView)OplusTypeCastingHelper.typeCasting(OplusBaseView.class, paramView);
      if (oplusBaseView != null)
        oplusBaseView.setUsageForceDarkAlgorithmType(paramInt); 
    } 
  }
  
  public IOplusDarkModeManager newOplusDarkModeManager() {
    return new OplusDarkModeManager();
  }
  
  public boolean darkenSplitScreenDrawable(View paramView, Drawable paramDrawable, int paramInt1, int paramInt2, int paramInt3, int paramInt4, RecordingCanvas paramRecordingCanvas) {
    Configuration configuration = paramView.getResources().getConfiguration();
    if ((configuration.uiMode & 0x30) == 32) {
      Drawable drawable = paramDrawable;
      if (paramDrawable instanceof android.graphics.drawable.StateListDrawable)
        drawable = paramDrawable.getCurrent(); 
      if (drawable instanceof ColorDrawable && 
        drawable.getAlpha() != 0) {
        ColorDrawable colorDrawable = new ColorDrawable(-16777216);
        colorDrawable.setBounds(paramInt1, paramInt2, paramInt3, paramInt4);
        colorDrawable.draw((Canvas)paramRecordingCanvas);
        return false;
      } 
    } 
    return true;
  }
  
  public void initDarkModeStatus(Application paramApplication) {
    OplusDarkModeThirdInvertManager.attachApplication(paramApplication);
  }
  
  public void changeColorFilterInDarkMode(ColorFilter paramColorFilter) {
    OplusDarkModeThirdInvertManager.getInstance().changeColorFilterInDarkMode(paramColorFilter);
  }
  
  public boolean isInDarkMode(boolean paramBoolean) {
    return OplusDarkModeThirdInvertManager.getInstance().isInDarkMode(paramBoolean);
  }
  
  public OplusBaseBaseCanvas.RealPaintState getRealPaintState(Paint paramPaint) {
    return OplusDarkModeThirdInvertManager.getInstance().getRealPaintState(paramPaint);
  }
  
  public void changePaintWhenDrawText(Paint paramPaint, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    OplusDarkModeThirdInvertManager.getInstance().changePaintWhenDrawText(paramPaint, paramOplusBaseBaseCanvas);
  }
  
  public void resetRealPaintIfNeed(Paint paramPaint, OplusBaseBaseCanvas.RealPaintState paramRealPaintState) {
    OplusDarkModeThirdInvertManager.getInstance().resetRealPaintIfNeed(paramPaint, paramRealPaintState);
  }
  
  public void changePaintWhenDrawArea(Paint paramPaint, RectF paramRectF, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    OplusDarkModeThirdInvertManager.getInstance().changePaintWhenDrawArea(paramPaint, paramRectF, paramOplusBaseBaseCanvas);
  }
  
  public void changePaintWhenDrawArea(Paint paramPaint, RectF paramRectF, Path paramPath, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    OplusDarkModeThirdInvertManager.getInstance().changePaintWhenDrawArea(paramPaint, paramRectF, paramPath, paramOplusBaseBaseCanvas);
  }
  
  public void changePaintWhenDrawPatch(NinePatch paramNinePatch, Paint paramPaint, RectF paramRectF, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    OplusDarkModeThirdInvertManager.getInstance().changePaintWhenDrawPatch(paramNinePatch, paramPaint, paramRectF, paramOplusBaseBaseCanvas);
  }
  
  public int changeWhenDrawColor(int paramInt, boolean paramBoolean, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    return OplusDarkModeThirdInvertManager.getInstance().changeWhenDrawColor(paramInt, paramBoolean, paramOplusBaseBaseCanvas);
  }
  
  public void changePaintWhenDrawBitmap(Paint paramPaint, Bitmap paramBitmap, RectF paramRectF, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    OplusDarkModeThirdInvertManager.getInstance().changePaintWhenDrawBitmap(paramPaint, paramBitmap, paramRectF, paramOplusBaseBaseCanvas);
  }
  
  public int[] getDarkModeColors(int[] paramArrayOfint, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    return OplusDarkModeThirdInvertManager.getInstance().getDarkModeColors(paramArrayOfint, paramOplusBaseBaseCanvas);
  }
  
  public Paint getPaintWhenDrawPatch(NinePatch paramNinePatch, Paint paramPaint, RectF paramRectF, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    return OplusDarkModeThirdInvertManager.getInstance().changePaintWhenDrawPatch(paramNinePatch, paramPaint, paramRectF, paramOplusBaseBaseCanvas);
  }
  
  public Paint getPaintWhenDrawBitmap(Paint paramPaint, Bitmap paramBitmap, RectF paramRectF, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    return OplusDarkModeThirdInvertManager.getInstance().changePaintWhenDrawBitmap(paramPaint, paramBitmap, paramRectF, paramOplusBaseBaseCanvas);
  }
  
  public void markDispatchDraw(ViewGroup paramViewGroup, Canvas paramCanvas) {
    OplusDarkModeThirdInvertManager.getInstance().markDispatchDraw(paramViewGroup, paramCanvas);
  }
  
  public void markDrawChild(ViewGroup paramViewGroup, View paramView, Canvas paramCanvas) {
    OplusDarkModeThirdInvertManager.getInstance().markDrawChild(paramViewGroup, paramView, paramCanvas);
  }
  
  public void markBackground(View paramView, Canvas paramCanvas) {
    OplusDarkModeThirdInvertManager.getInstance().markBackground(paramView, paramCanvas);
  }
  
  public void markForeground(View paramView, Canvas paramCanvas) {
    OplusDarkModeThirdInvertManager.getInstance().markForeground(paramView, paramCanvas);
  }
  
  public void markOnDraw(View paramView, Canvas paramCanvas) {
    OplusDarkModeThirdInvertManager.getInstance().markOnDraw(paramView, paramCanvas);
  }
  
  public void markDrawFadingEdge(View paramView, Canvas paramCanvas) {
    OplusDarkModeThirdInvertManager.getInstance().markDrawFadingEdge(paramView, paramCanvas);
  }
  
  public void markViewTypeBySize(View paramView) {
    OplusDarkModeThirdInvertManager.getInstance().markViewTypeBySize(paramView);
  }
  
  public ColorFilter getColorFilterWhenDrawVectorDrawable(OplusBaseDrawable.SumEntity paramSumEntity1, OplusBaseDrawable.SumEntity paramSumEntity2, OplusBaseDrawable.SumEntity paramSumEntity3) {
    return OplusDarkModeThirdInvertManager.getInstance().getColorFilterWhenDrawVectorDrawable(paramSumEntity1, paramSumEntity2, paramSumEntity3);
  }
}
