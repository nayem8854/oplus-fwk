package com.oplus.darkmode;

import android.view.OplusWindowManager;

public class OplusDarkModeHelper {
  public static void registerOnUiModeConfigurationChangeFinishListener(IOplusDarkModeListener paramIOplusDarkModeListener) {
    try {
      OplusWindowManager oplusWindowManager = new OplusWindowManager();
      this();
      oplusWindowManager.registerOnUiModeConfigurationChangeFinishListener(paramIOplusDarkModeListener);
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public static void unregisterOnUiModeConfigurationChangeFinishListener(IOplusDarkModeListener paramIOplusDarkModeListener) {
    try {
      OplusWindowManager oplusWindowManager = new OplusWindowManager();
      this();
      oplusWindowManager.unregisterOnUiModeConfigurationChangeFinishListener(paramIOplusDarkModeListener);
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
}
