package com.oplus.darkmode;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.BlendModeColorFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.NinePatch;
import android.graphics.OplusBaseBaseCanvas;
import android.graphics.OplusBaseShader;
import android.graphics.OplusDarkModeThirdPartyFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.OplusBaseDrawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.android.internal.graphics.ColorUtils;
import com.oplus.util.OplusTypeCastingHelper;
import java.util.ArrayList;
import java.util.List;

public class OplusDarkModeThirdInvertManager {
  private boolean mIsAppSupportDarkMode = false;
  
  private float mBackgroundMaxL = -1.0F;
  
  private float mForegroundMinL = -1.0F;
  
  private OplusDarkModeThirdPartyFilter[] mCacheThirdPartyFilter = new OplusDarkModeThirdPartyFilter[5];
  
  private static class Holder {
    static final OplusDarkModeThirdInvertManager INSTANCE = new OplusDarkModeThirdInvertManager();
  }
  
  public static OplusDarkModeThirdInvertManager getInstance() {
    return Holder.INSTANCE;
  }
  
  public void setBackgroundMaxL(float paramFloat) {
    this.mBackgroundMaxL = paramFloat;
  }
  
  public float getBackgroundMaxL() {
    return this.mBackgroundMaxL;
  }
  
  public float getForegroundMinL() {
    return this.mForegroundMinL;
  }
  
  public void setForegroundMinL(float paramFloat) {
    this.mForegroundMinL = paramFloat;
  }
  
  public static void attachApplication(Application paramApplication) {
    sApplication = paramApplication;
  }
  
  public void setIsSupportDarkModeStatus(int paramInt) {
    boolean bool = true;
    if (paramInt != 1)
      bool = false; 
    this.mIsAppSupportDarkMode = bool;
  }
  
  private ColorFilter getBitmapFilter(int paramInt) {
    int i;
    OplusDarkModeThirdPartyFilter oplusDarkModeThirdPartyFilter = this.mCacheThirdPartyFilter[paramInt];
    if (oplusDarkModeThirdPartyFilter == null || oplusDarkModeThirdPartyFilter.getLABBgMaxL() != this.mBackgroundMaxL || 
      oplusDarkModeThirdPartyFilter.getLABFgMinL() != this.mForegroundMinL) {
      i = 1;
    } else {
      i = 0;
    } 
    if (i) {
      i = ColorUtils.LABToColor(this.mBackgroundMaxL, 0.0D, 0.0D);
      float[] arrayOfFloat = new float[3];
      ColorUtils.colorToHSL(i, arrayOfFloat);
      float f = arrayOfFloat[2];
      i = ColorUtils.LABToColor(this.mForegroundMinL, 0.0D, 0.0D);
      ColorUtils.colorToHSL(i, arrayOfFloat);
      oplusDarkModeThirdPartyFilter = new OplusDarkModeThirdPartyFilter(paramInt, f, arrayOfFloat[2], this.mBackgroundMaxL, this.mForegroundMinL);
      this.mCacheThirdPartyFilter[paramInt] = oplusDarkModeThirdPartyFilter;
      return (ColorFilter)oplusDarkModeThirdPartyFilter;
    } 
    return (ColorFilter)oplusDarkModeThirdPartyFilter;
  }
  
  public boolean isInDarkMode() {
    return this.mIsAppSupportDarkMode;
  }
  
  public boolean isInDarkMode(boolean paramBoolean) {
    return this.mIsAppSupportDarkMode;
  }
  
  private void calculateBitmapColor(Bitmap paramBitmap, boolean paramBoolean, RectF paramRectF, int paramInt) {
    if (paramBitmap == null)
      return; 
    if (OplusDarkModeUtils.hasCalculatedColor(paramBitmap))
      return; 
    if (paramBitmap.getConfig() == Bitmap.Config.HARDWARE || paramBitmap.getWidth() == 0 || paramBitmap.getHeight() == 0) {
      OplusDarkModeUtils.setHasCalculatedColor(paramBitmap, true);
      return;
    } 
    int i = 0;
    int j = 0;
    byte b = 0;
    int k = 0;
    int m = Math.max(1, paramBitmap.getHeight() / 10);
    int n = Math.max(1, paramBitmap.getWidth() / 10);
    int i1 = 0;
    float[] arrayOfFloat = new float[3];
    paramInt = 0;
    int i2 = paramBitmap.getWidth();
    int i3 = 0;
    int i4 = paramBitmap.getHeight();
    float f1 = 0.0F;
    float f2 = 0.0F;
    float f3 = 0.0F;
    float f4 = 0.0F;
    float f5 = 0.0F;
    float f6 = 0.0F;
    float f7 = 0.0F;
    if (paramBoolean) {
      paramInt = 1;
      i2 = paramBitmap.getWidth() - 1;
      i3 = 1;
      i4 = paramBitmap.getHeight() - 1;
    } 
    int i5 = paramInt, i6 = i2, i7 = paramInt;
    i2 = j;
    while (true) {
      j = 0;
      if (i5 < i6) {
        float f10;
        float f11;
        float f12;
        for (j = i3, paramInt = i, f10 = f7, f11 = f6, f6 = f5, f5 = f3, f12 = f1, i = j; i < i4; i += m, i1++, f2 = f1, f5 = f3, f6 = f7) {
          int i8 = paramBitmap.getPixel(i5, i);
          j = Color.alpha(i8);
          if (j < 76.5D) {
            k++;
            f1 = f2;
            f3 = f5;
            f7 = f6;
          } else {
            ColorUtils.colorToHSL(i8, arrayOfFloat);
            if (OplusDarkModeUtils.isMaybeWhiteColor(arrayOfFloat)) {
              paramInt++;
            } else if (OplusDarkModeUtils.isMaybeBlackColor(arrayOfFloat)) {
              i2++;
            } else {
              b++;
            } 
            if (arrayOfFloat[0] > f2) {
              f1 = arrayOfFloat[0];
              if (f5 == 0.0F) {
                f3 = f1;
                f5 = f1;
                f1 = f3;
                f3 = f5;
              } else {
                f3 = f5;
              } 
            } else {
              f1 = f2;
              f3 = f5;
              if (arrayOfFloat[0] < f5) {
                f3 = arrayOfFloat[0];
                f1 = f2;
              } 
            } 
            if (arrayOfFloat[1] > f4) {
              f5 = arrayOfFloat[1];
              if (f6 == 0.0F) {
                f7 = f5;
              } else {
                f7 = f6;
              } 
            } else {
              f5 = f4;
              f7 = f6;
              if (arrayOfFloat[1] < f6) {
                f7 = arrayOfFloat[1];
                f5 = f4;
              } 
            } 
            if (arrayOfFloat[2] > f11) {
              f6 = arrayOfFloat[2];
              if (f10 == 0.0F) {
                f4 = f6;
              } else {
                f4 = f10;
              } 
            } else {
              f6 = f11;
              f4 = f10;
              if (arrayOfFloat[2] < f10) {
                f4 = arrayOfFloat[2];
                f6 = f11;
              } 
            } 
            f12 += arrayOfFloat[2];
            f10 = f4;
            f11 = f6;
            f4 = f5;
          } 
        } 
        i5 += n;
        i = paramInt;
        f1 = f12;
        f3 = f5;
        f5 = f6;
        f6 = f11;
        f7 = f10;
        continue;
      } 
      break;
    } 
    if (!paramBoolean && i1 == 0) {
      OplusDarkModeUtils.setHasCalculatedColor(paramBitmap, true);
      return;
    } 
    i4 = 0;
    i5 = paramBitmap.getWidth();
    paramInt = i4;
    i3 = i5;
    if (paramBoolean) {
      paramInt = i4;
      i3 = i5;
      if (i1 != 0) {
        paramInt = 1;
        i3 = paramBitmap.getWidth() - 1;
      } 
    } 
    i6 = paramBitmap.getHeight() / 2;
    float f8, f9;
    for (i4 = paramInt, i5 = k, k = i, f8 = f4, f9 = f2, f2 = f1, i = i6; i4 < i3; i4 += n, i1++, f9 = f4, f3 = f1) {
      i7 = paramBitmap.getPixel(i4, i);
      i6 = Color.alpha(i7);
      if (i6 < 76.5D) {
        i5++;
        f4 = f9;
        f1 = f3;
      } else {
        ColorUtils.colorToHSL(i7, arrayOfFloat);
        if (OplusDarkModeUtils.isMaybeWhiteColor(arrayOfFloat)) {
          k++;
        } else if (OplusDarkModeUtils.isMaybeBlackColor(arrayOfFloat)) {
          i2++;
        } else {
          b++;
        } 
        if (arrayOfFloat[0] > f9) {
          f9 = arrayOfFloat[0];
          f4 = f9;
          f1 = f3;
          if (f3 == 0.0F) {
            f1 = f9;
            f4 = f9;
          } 
        } else {
          f4 = f9;
          f1 = f3;
          if (arrayOfFloat[0] < f3) {
            f1 = arrayOfFloat[0];
            f4 = f9;
          } 
        } 
        if (arrayOfFloat[1] > f8) {
          f8 = arrayOfFloat[1];
          f9 = f8;
          f3 = f5;
          if (f5 == 0.0F) {
            f3 = f8;
            f9 = f8;
          } 
        } else {
          f9 = f8;
          f3 = f5;
          if (arrayOfFloat[1] < f5) {
            f3 = arrayOfFloat[1];
            f9 = f8;
          } 
        } 
        if (arrayOfFloat[2] > f6) {
          f6 = arrayOfFloat[2];
          f8 = f6;
          f5 = f7;
          if (f7 == 0.0F) {
            f5 = f6;
            f8 = f6;
          } 
        } else {
          f8 = f6;
          f5 = f7;
          if (arrayOfFloat[2] < f7) {
            f5 = arrayOfFloat[2];
            f8 = f6;
          } 
        } 
        f2 += arrayOfFloat[2];
        f7 = f5;
        f6 = f8;
        f5 = f3;
        f8 = f9;
      } 
    } 
    if (i5 == i1) {
      OplusDarkModeUtils.setHasCalculatedColor(paramBitmap, true);
      return;
    } 
    double d1 = (i1 - i5);
    double d2 = b * 1.0D / d1;
    double d3 = k * 1.0D / d1;
    double d4 = i2 * 1.0D / d1;
    d1 = f2 * 1.0D / d1;
    f1 = f8 - f5;
    if (f9 - f3 <= 30.0F && f1 <= 0.1D) {
      b = 1;
    } else {
      b = 0;
    } 
    if ((f6 - f7) >= 0.8D && f1 <= 0.15D) {
      k = 1;
    } else {
      k = 0;
    } 
    if (OplusDarkModeUtils.isWeiBo(sApplication)) {
      if (d2 <= 0.01D && d1 >= 0.995D) {
        paramInt = 1;
      } else {
        paramInt = 0;
      } 
    } else if (d2 <= 0.2D && d1 >= 0.85D) {
      paramInt = 1;
    } else {
      paramInt = 0;
    } 
    i1 = j;
    if (d2 <= 0.2D) {
      i1 = j;
      if (d1 <= 0.15D)
        i1 = 1; 
    } 
    if (paramBoolean) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("colorfulPixelPercent:");
      stringBuilder.append(d2);
      stringBuilder.append(",whitePixelPercent:");
      stringBuilder.append(d3);
      stringBuilder.append(",blackPixelPercent:");
      stringBuilder.append(d4);
      stringBuilder.append(",averageLightness:");
      stringBuilder.append(d1);
      Log.i("tag1", stringBuilder.toString());
    } 
    if ((b != 0 || i1 != 0) && d1 <= 0.3D) {
      OplusDarkModeUtils.setColorState(paramBitmap, 2);
    } else if (b != 0 && paramInt != 0 && d1 >= 0.7D && k == 0) {
      OplusDarkModeUtils.setColorState(paramBitmap, 1);
    } else if (d2 >= 0.3D) {
      OplusDarkModeUtils.setColorState(paramBitmap, 8);
    } else if (d3 == 1.0D) {
      OplusDarkModeUtils.setColorState(paramBitmap, 1);
    } else if (paramBoolean && d3 >= 0.25D) {
      OplusDarkModeUtils.setColorState(paramBitmap, 1);
    } else if (d3 > 0.7D) {
      OplusDarkModeUtils.setColorState(paramBitmap, 4);
    } else if (d4 == 1.0D) {
      OplusDarkModeUtils.setColorState(paramBitmap, 2);
    } else if (d4 > 0.7D) {
      OplusDarkModeUtils.setColorState(paramBitmap, 5);
    } else if (Math.abs(d3 - i2) < 0.3D) {
      OplusDarkModeUtils.setColorState(paramBitmap, 6);
    } else if (d3 > 0.0D) {
      OplusDarkModeUtils.setColorState(paramBitmap, 7);
    } 
    OplusDarkModeUtils.setHasCalculatedColor(paramBitmap, true);
  }
  
  private Paint changePaintWhenDrawBitmap(Paint paramPaint, Bitmap paramBitmap, RectF paramRectF, boolean paramBoolean, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    if (paramBitmap == null || paramBitmap.isRecycled())
      return paramPaint; 
    if (paramPaint != null && makeColorFilterColor(paramPaint.getColorFilter(), paramOplusBaseBaseCanvas.getTransformType(), paramOplusBaseBaseCanvas)) {
      OplusDarkModeUtils.setHasCalculatedColor(paramBitmap, true);
      return paramPaint;
    } 
    calculateBitmapColor(paramBitmap, paramBoolean, paramRectF, paramOplusBaseBaseCanvas.getTransformType());
    int i = OplusDarkModeUtils.getColorState(paramBitmap);
    int j = paramOplusBaseBaseCanvas.getTransformType();
    if (paramBoolean && (
      i == 1 || i == 4 || i == 7 || i == 6)) {
      if (paramPaint == null)
        paramPaint = new Paint(); 
      paramPaint.setColorFilter(getBitmapFilter(3));
      return paramPaint;
    } 
    int k = paramBitmap.getWidth();
    int m = paramBitmap.getHeight();
    double d1 = paramBitmap.getDensity() * 1.0D / 160.0D;
    double d2 = k / d1;
    d1 = m / d1;
    float f = OplusDarkModeUtils.getScreenWidth(sApplication);
    if (j != 1) {
      if (j == 2)
        if (d1 <= 40.0D && d2 <= 40.0D) {
          if (i == 2 || i == 5) {
            if (paramPaint == null)
              paramPaint = new Paint(); 
            paramPaint.setColorFilter(getBitmapFilter(4));
            return paramPaint;
          } 
          if (i == 6) {
            if (paramPaint == null)
              paramPaint = new Paint(); 
            paramPaint.setColorFilter(getBitmapFilter(1));
            return paramPaint;
          } 
        } else if (k >= f * 0.8D && 
          i == 1) {
          if (paramPaint == null)
            paramPaint = new Paint(); 
          paramPaint.setColorFilter(getBitmapFilter(3));
          return paramPaint;
        }  
    } else if (d1 <= 40.0D && d2 <= 40.0D) {
      if (i == 2 || i == 5) {
        if (paramPaint == null)
          paramPaint = new Paint(); 
        paramPaint.setColorFilter(getBitmapFilter(4));
        return paramPaint;
      } 
      if (i == 1 || i == 4) {
        if (paramPaint == null)
          paramPaint = new Paint(); 
        paramPaint.setColorFilter(getBitmapFilter(3));
        return paramPaint;
      } 
      if (i == 7 || i == 6) {
        if (paramPaint == null)
          paramPaint = new Paint(); 
        paramPaint.setColorFilter(getBitmapFilter(2));
        return paramPaint;
      } 
    } else if (i == 1) {
      if (paramPaint == null)
        paramPaint = new Paint(); 
      paramPaint.setColorFilter(getBitmapFilter(3));
      return paramPaint;
    } 
    return paramPaint;
  }
  
  public void changePaintWhenDrawText(Paint paramPaint, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    if (isInDarkMode()) {
      int i = paramPaint.getColor();
      if (OplusDarkModeUtils.isBiliBili(sApplication)) {
        float f = paramPaint.getStrokeWidth();
        Paint.Style style = paramPaint.getStyle();
        if (style == Paint.Style.FILL_AND_STROKE && 
          f <= OplusDarkModeUtils.getDpDensity(sApplication) * 3.0F)
          return; 
      } 
      paramPaint.setColor(getDarkModeColor(i, 2));
    } 
  }
  
  public int changeWhenDrawColor(int paramInt, boolean paramBoolean, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    int i = paramInt;
    if (paramBoolean)
      i = getDarkModeColor(paramInt, 1); 
    return i;
  }
  
  public void changePaintWhenDrawArea(Paint paramPaint, RectF paramRectF, Path paramPath, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    boolean bool;
    Shader shader = paramPaint.getShader();
    if (shader != null) {
      handleShader(paramPaint, paramRectF, shader, paramOplusBaseBaseCanvas);
      return;
    } 
    ColorFilter colorFilter = paramPaint.getColorFilter();
    if (paramPath != null) {
      bool = true;
    } else {
      bool = true;
    } 
    if (!makeColorFilterColor(colorFilter, bool, paramOplusBaseBaseCanvas))
      handleAreaColor(paramPaint, paramRectF, paramPath, paramOplusBaseBaseCanvas); 
  }
  
  public void changePaintWhenDrawArea(Paint paramPaint, RectF paramRectF, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    changePaintWhenDrawArea(paramPaint, paramRectF, null, paramOplusBaseBaseCanvas);
  }
  
  private void handleAreaColor(Paint paramPaint, RectF paramRectF, Path paramPath, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    int i = paramPaint.getColor();
    if (paramPath != null) {
      j = 1;
    } else {
      j = 0;
    } 
    int k = paramOplusBaseBaseCanvas.getTransformType();
    if (j) {
      j = k;
      if (OplusDarkModeUtils.isMaybeWhiteColor(paramPaint.getColor())) {
        j = k;
        if (k == 2)
          j = 1; 
      } 
      paramPaint.setColor(getDarkModeColor(i, j));
      return;
    } 
    if (paramPaint.getStyle() == Paint.Style.STROKE && 
      paramPaint.getStrokeWidth() <= OplusDarkModeUtils.getDpDensity(sApplication) * 3.0F) {
      paramPaint.setColor(getDarkModeColor(paramPaint.getColor(), 2));
      return;
    } 
    k = changeAreaTransformType(paramRectF, paramOplusBaseBaseCanvas, k);
    int j = k;
    if (OplusDarkModeUtils.isMaybeWhiteColor(paramPaint.getColor())) {
      j = k;
      if (k == 2)
        j = 1; 
    } 
    paramPaint.setColor(getDarkModeColor(i, j));
  }
  
  private int changeAreaTransformType(RectF paramRectF, OplusBaseBaseCanvas paramOplusBaseBaseCanvas, int paramInt) {
    int i = paramOplusBaseBaseCanvas.getOplusViewType();
    if (i == 2)
      paramInt = 1; 
    return paramInt;
  }
  
  private void handleShader(Paint paramPaint, RectF paramRectF, Shader paramShader, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    if (paramShader instanceof BitmapShader) {
      changePaintWhenDrawBitmap(paramPaint, ((BitmapShader)paramShader).mBitmap, paramRectF, paramOplusBaseBaseCanvas);
    } else {
      long[] arrayOfLong;
      if (paramShader instanceof android.graphics.LinearGradient || paramShader instanceof android.graphics.SweepGradient || paramShader instanceof android.graphics.RadialGradient) {
        OplusBaseShader oplusBaseShader = (OplusBaseShader)OplusTypeCastingHelper.typeCasting(OplusBaseShader.class, paramShader);
        if (oplusBaseShader != null) {
          arrayOfLong = oplusBaseShader.getColorLongs();
          if (arrayOfLong != null) {
            arrayOfLong = (long[])arrayOfLong.clone();
            int i = changeAreaTransformType(paramRectF, paramOplusBaseBaseCanvas, paramOplusBaseBaseCanvas.getTransformType());
            getShaderDarkModeColors(arrayOfLong, i);
            oplusBaseShader.setColors(arrayOfLong);
          } 
        } 
        return;
      } 
      if (arrayOfLong instanceof android.graphics.ComposeShader) {
        OplusBaseShader oplusBaseShader = (OplusBaseShader)OplusTypeCastingHelper.typeCasting(OplusBaseShader.class, arrayOfLong);
        if (oplusBaseShader != null) {
          List<?> list = oplusBaseShader.getComposeShaderColor();
          if (list != null && list.size() == 2) {
            int i = changeAreaTransformType(paramRectF, paramOplusBaseBaseCanvas, paramOplusBaseBaseCanvas.getTransformType());
            ArrayList<long[]> arrayList = new ArrayList(list);
            for (byte b = 0; b < arrayList.size(); b++)
              getShaderDarkModeColors(arrayList.get(b), i); 
            oplusBaseShader.setComposeShaderColor(arrayList);
          } 
        } 
      } 
    } 
  }
  
  public Paint changePaintWhenDrawBitmap(Paint paramPaint, Bitmap paramBitmap, RectF paramRectF, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    return changePaintWhenDrawBitmap(paramPaint, paramBitmap, paramRectF, false, paramOplusBaseBaseCanvas);
  }
  
  public Paint changePaintWhenDrawPatch(NinePatch paramNinePatch, Paint paramPaint, RectF paramRectF, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    return changePaintWhenDrawBitmap(paramPaint, paramNinePatch.getBitmap(), paramRectF, true, paramOplusBaseBaseCanvas);
  }
  
  public void changeColorFilterInDarkMode(ColorFilter paramColorFilter) {
    makeColorFilterColor(paramColorFilter, 2);
  }
  
  private boolean makeColorFilterColor(ColorFilter paramColorFilter, int paramInt) {
    return makeColorFilterColor(paramColorFilter, paramInt, null);
  }
  
  private boolean makeColorFilterColor(ColorFilter paramColorFilter, int paramInt, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    boolean bool = false;
    int i = -1;
    if (paramColorFilter instanceof PorterDuffColorFilter) {
      i = ((PorterDuffColorFilter)paramColorFilter).getColor();
      bool = true;
    } else if (paramColorFilter instanceof BlendModeColorFilter) {
      i = ((BlendModeColorFilter)paramColorFilter).getColor();
      bool = true;
    } 
    if (bool) {
      int j = i;
      int k = paramInt;
      if (paramOplusBaseBaseCanvas != null) {
        k = paramOplusBaseBaseCanvas.getOplusViewType();
        if (k != 1) {
          k = paramInt;
        } else {
          k = paramInt;
          if (paramInt == 1) {
            k = paramInt;
            if (OplusDarkModeUtils.isMaybeBlackColor(i))
              k = 2; 
          } 
        } 
      } 
      if (k != 1) {
        if (k != 2) {
          paramInt = j;
        } else {
          paramInt = OplusDarkModeUtils.makeSVGColorForeground(i, this.mForegroundMinL);
        } 
      } else {
        paramInt = OplusDarkModeUtils.makeSVGColorBackground(i, this.mBackgroundMaxL);
      } 
      if (paramInt != i)
        OplusDarkModeUtils.setColorFilterColor(paramColorFilter, paramInt); 
    } 
    return bool;
  }
  
  public void getShaderDarkModeColors(long[] paramArrayOflong, int paramInt) {
    if (paramArrayOflong == null)
      return; 
    int i = paramArrayOflong.length;
    for (byte b = 0; b < i; b++) {
      long l = paramArrayOflong[b];
      paramArrayOflong[b] = Color.pack(getDarkModeColor(Color.argb(Color.alpha(l), Color.red(l), Color.green(l), Color.blue(l)), paramInt));
    } 
  }
  
  public int[] getDarkModeColors(int[] paramArrayOfint, OplusBaseBaseCanvas paramOplusBaseBaseCanvas) {
    if (paramArrayOfint == null)
      return null; 
    int[] arrayOfInt = (int[])paramArrayOfint.clone();
    for (byte b = 0; b < paramArrayOfint.length; b++)
      arrayOfInt[b] = getDarkModeColor(arrayOfInt[b], paramOplusBaseBaseCanvas.getTransformType()); 
    return arrayOfInt;
  }
  
  private int getDarkModeColor(int paramInt1, int paramInt2) {
    if (paramInt2 != 1) {
      if (paramInt2 != 2)
        return paramInt1; 
      return OplusDarkModeUtils.makeColorForeground(paramInt1, this.mForegroundMinL);
    } 
    return OplusDarkModeUtils.makeColorBackground(paramInt1, this.mBackgroundMaxL);
  }
  
  public OplusBaseBaseCanvas.RealPaintState getRealPaintState(Paint paramPaint) {
    OplusBaseShader oplusBaseShader;
    if (paramPaint == null)
      return null; 
    OplusBaseBaseCanvas.RealPaintState realPaintState = new OplusBaseBaseCanvas.RealPaintState();
    realPaintState.color = paramPaint.getColor();
    realPaintState.colorFilter = paramPaint.getColorFilter();
    Shader shader = paramPaint.getShader();
    if (shader instanceof android.graphics.LinearGradient || shader instanceof android.graphics.SweepGradient || shader instanceof android.graphics.RadialGradient) {
      oplusBaseShader = (OplusBaseShader)OplusTypeCastingHelper.typeCasting(OplusBaseShader.class, shader);
      if (oplusBaseShader != null)
        realPaintState.shaderColors = oplusBaseShader.getColorLongs(); 
    } else if (oplusBaseShader instanceof android.graphics.ComposeShader) {
      oplusBaseShader = (OplusBaseShader)OplusTypeCastingHelper.typeCasting(OplusBaseShader.class, oplusBaseShader);
      if (oplusBaseShader != null)
        realPaintState.composeShaderColors = oplusBaseShader.getComposeShaderColor(); 
    } 
    if (paramPaint.getColorFilter() instanceof PorterDuffColorFilter) {
      realPaintState.colorFilterColor = ((PorterDuffColorFilter)paramPaint.getColorFilter()).getColor();
    } else if (paramPaint.getColorFilter() instanceof BlendModeColorFilter) {
      realPaintState.colorFilterColor = ((BlendModeColorFilter)paramPaint.getColorFilter()).getColor();
    } 
    return realPaintState;
  }
  
  public void resetRealPaintIfNeed(Paint paramPaint, OplusBaseBaseCanvas.RealPaintState paramRealPaintState) {
    OplusBaseShader oplusBaseShader;
    if (paramPaint == null || paramRealPaintState == null)
      return; 
    paramPaint.setColor(paramRealPaintState.color);
    paramPaint.setColorFilter(paramRealPaintState.colorFilter);
    Shader shader = paramPaint.getShader();
    if (shader instanceof android.graphics.LinearGradient || shader instanceof android.graphics.SweepGradient || shader instanceof android.graphics.RadialGradient) {
      oplusBaseShader = (OplusBaseShader)OplusTypeCastingHelper.typeCasting(OplusBaseShader.class, shader);
      if (oplusBaseShader != null && paramRealPaintState.shaderColors != null)
        oplusBaseShader.setColors(paramRealPaintState.shaderColors); 
    } else if (oplusBaseShader instanceof android.graphics.ComposeShader) {
      oplusBaseShader = (OplusBaseShader)OplusTypeCastingHelper.typeCasting(OplusBaseShader.class, oplusBaseShader);
      if (oplusBaseShader != null && paramRealPaintState.composeShaderColors != null)
        oplusBaseShader.setComposeShaderColor(paramRealPaintState.composeShaderColors); 
    } 
    if (paramPaint.getColorFilter() instanceof PorterDuffColorFilter) {
      OplusDarkModeUtils.setColorFilterColor(paramPaint.getColorFilter(), paramRealPaintState.colorFilterColor);
    } else if (paramPaint.getColorFilter() instanceof BlendModeColorFilter) {
      OplusDarkModeUtils.setColorFilterColor(paramPaint.getColorFilter(), paramRealPaintState.colorFilterColor);
    } 
  }
  
  private OplusDarkModeThirdInvertManager() {
    this.mDrawnBounds = new RectF();
    this.mBounds = new RectF();
  }
  
  private boolean isCrudeView(View paramView, Canvas paramCanvas) {
    int i = OplusDarkModeUtils.getCrudeState(paramView);
    boolean bool = true;
    if (i == 1)
      return true; 
    if (paramView instanceof ViewGroup) {
      ViewGroup viewGroup = (ViewGroup)paramView;
      if (viewGroup.getChildCount() <= 0)
        bool = false; 
      return bool;
    } 
    return false;
  }
  
  public void markBackground(View paramView, Canvas paramCanvas) {
    if (this.mIsAppSupportDarkMode) {
      int i = OplusDarkModeUtils.getViewType(paramView);
      OplusDarkModeUtils.setViewType(paramCanvas, i);
      i = paramView.getWidth();
      int j = paramView.getHeight();
      OplusDarkModeUtils.setCanvasTransformType(paramCanvas, 1, i, j);
    } 
  }
  
  private void markCrude(View paramView, Canvas paramCanvas) {
    if (paramView instanceof ViewGroup) {
      ViewGroup viewGroup = (ViewGroup)paramView;
      int i = viewGroup.getChildCount();
      this.mBounds.setEmpty();
      this.mDrawnBounds.setEmpty();
      if (i > 1)
        for (; --i >= 0; i--) {
          View view = viewGroup.getChildAt(i);
          this.mBounds.set(view.getX(), view.getY(), view.getX() + view.getWidth(), view.getY() + view.getHeight());
          if (!this.mDrawnBounds.isEmpty() && this.mBounds.contains(this.mDrawnBounds))
            OplusDarkModeUtils.setCrudeState(view, 1); 
          this.mDrawnBounds.union(this.mBounds);
        }  
    } 
  }
  
  public void markDispatchDraw(ViewGroup paramViewGroup, Canvas paramCanvas) {
    if (this.mIsAppSupportDarkMode)
      markCrude((View)paramViewGroup, paramCanvas); 
  }
  
  public void markViewTypeBySize(View paramView) {
    float f1 = OplusDarkModeUtils.getDpDensity(paramView);
    float f2 = OplusDarkModeUtils.getScreenWidth(paramView);
    int i = paramView.getHeight();
    int j = paramView.getWidth();
    if (i <= 80.0F * f1 && j <= f2 / 2.0F) {
      OplusDarkModeUtils.setViewType(paramView, 1);
    } else if (j >= f2 * 0.8D) {
      if (i >= 8.0F * f1) {
        OplusDarkModeUtils.setViewType(paramView, 2);
      } else {
        OplusDarkModeUtils.setViewType(paramView, 3);
      } 
    } 
  }
  
  public void markDrawChild(ViewGroup paramViewGroup, View paramView, Canvas paramCanvas) {
    if (this.mIsAppSupportDarkMode) {
      int i = OplusDarkModeUtils.getViewType(paramView);
      OplusDarkModeUtils.setViewType(paramCanvas, i);
      int j = paramView.getWidth();
      i = paramView.getHeight();
      OplusDarkModeUtils.setCanvasTransformType(paramCanvas, 1, j, i);
    } 
  }
  
  public void markForeground(View paramView, Canvas paramCanvas) {
    if (this.mIsAppSupportDarkMode) {
      int i = OplusDarkModeUtils.getViewType(paramView);
      OplusDarkModeUtils.setViewType(paramCanvas, i);
      if (isCrudeView(paramView, paramCanvas)) {
        i = 1;
      } else {
        i = 2;
      } 
      int j = paramView.getWidth(), k = paramView.getHeight();
      OplusDarkModeUtils.setCanvasTransformType(paramCanvas, i, j, k);
    } 
  }
  
  public void markOnDraw(View paramView, Canvas paramCanvas) {
    markForeground(paramView, paramCanvas);
  }
  
  public void markDrawFadingEdge(View paramView, Canvas paramCanvas) {
    if (this.mIsAppSupportDarkMode) {
      int i = paramView.getWidth(), j = paramView.getHeight();
      OplusDarkModeUtils.setCanvasTransformType(paramCanvas, 1, i, j);
    } 
  }
  
  private static final ColorFilter INVERT_FILTER = (ColorFilter)new ColorMatrixColorFilter(new float[] { 
        -1.0F, 0.0F, 0.0F, 0.0F, 255.0F, 0.0F, -1.0F, 0.0F, 0.0F, 255.0F, 
        0.0F, 0.0F, -1.0F, 0.0F, 255.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F });
  
  private static final int COLORFUL_BITMAP = 8;
  
  private static final int FOREGROUND_ICON_SIZE = 40;
  
  private static final int FULL_DARK_BITMAP = 2;
  
  private static final int FULL_LIGHT_BITMAP = 1;
  
  private static final int HAS_WHITE_BITMAP = 7;
  
  private static final int MORE_BLACK_BITMAP = 5;
  
  private static final int MORE_WHITE_BITMAP = 4;
  
  private static final int WHITE_BLACK_BALANCE_BITMAP = 6;
  
  private static final boolean isDebug = false;
  
  private static Application sApplication;
  
  private RectF mBounds;
  
  private RectF mDrawnBounds;
  
  public ColorFilter getColorFilterWhenDrawVectorDrawable(OplusBaseDrawable.SumEntity paramSumEntity1, OplusBaseDrawable.SumEntity paramSumEntity2, OplusBaseDrawable.SumEntity paramSumEntity3) {
    if (paramSumEntity3.count() > 0 && 
      paramSumEntity1.delta() <= 20.0F && paramSumEntity2.delta() <= 0.1F && 
      paramSumEntity3.average() <= 0.5F)
      return INVERT_FILTER; 
    return null;
  }
}
