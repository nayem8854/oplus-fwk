package com.oplus.darkmode;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.OplusBaseBaseCanvas;
import android.graphics.OplusBaseBitmap;
import android.graphics.OplusBaseOplusFilter;
import android.graphics.OplusBasePath;
import android.graphics.Path;
import android.util.DisplayMetrics;
import android.view.OplusBaseView;
import android.view.View;
import com.android.internal.graphics.ColorUtils;
import com.oplus.util.OplusTypeCastingHelper;

public class OplusDarkModeUtils {
  public static boolean isMaybeBlackColor(int paramInt) {
    float[] arrayOfFloat = new float[3];
    ColorUtils.colorToHSL(paramInt, arrayOfFloat);
    return isMaybeBlackColor(arrayOfFloat);
  }
  
  public static boolean isMaybeWhiteColor(int paramInt) {
    float[] arrayOfFloat = new float[3];
    ColorUtils.colorToHSL(paramInt, arrayOfFloat);
    return isMaybeWhiteColor(arrayOfFloat);
  }
  
  public static boolean isMaybeWhiteColor(float[] paramArrayOffloat) {
    // Byte code:
    //   0: aload_0
    //   1: iconst_2
    //   2: faload
    //   3: f2d
    //   4: dstore_1
    //   5: iconst_1
    //   6: istore_3
    //   7: iload_3
    //   8: istore #4
    //   10: dload_1
    //   11: ldc2_w 0.96
    //   14: dcmpl
    //   15: ifge -> 71
    //   18: aload_0
    //   19: iconst_1
    //   20: faload
    //   21: fconst_0
    //   22: fcmpl
    //   23: ifne -> 40
    //   26: iload_3
    //   27: istore #4
    //   29: aload_0
    //   30: iconst_2
    //   31: faload
    //   32: f2d
    //   33: ldc2_w 0.8
    //   36: dcmpl
    //   37: ifge -> 71
    //   40: aload_0
    //   41: iconst_1
    //   42: faload
    //   43: f2d
    //   44: ldc2_w 0.05
    //   47: dcmpg
    //   48: ifgt -> 68
    //   51: aload_0
    //   52: iconst_2
    //   53: faload
    //   54: f2d
    //   55: ldc2_w 0.9
    //   58: dcmpl
    //   59: iflt -> 68
    //   62: iload_3
    //   63: istore #4
    //   65: goto -> 71
    //   68: iconst_0
    //   69: istore #4
    //   71: iload #4
    //   73: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #27	-> 0
  }
  
  public static boolean isMaybeBlackColor(float[] paramArrayOffloat) {
    double d = paramArrayOffloat[2];
    boolean bool = false;
    if (d > 0.04D && (paramArrayOffloat[1] != 0.0F || paramArrayOffloat[2] > 0.21D) && (paramArrayOffloat[0] != 180.0F || paramArrayOffloat[1] > 0.1D || paramArrayOffloat[2] > 0.21D)) {
      boolean bool1 = bool;
      if (paramArrayOffloat[1] <= 0.05D) {
        bool1 = bool;
        if (paramArrayOffloat[2] <= 0.11D)
          return true; 
      } 
      return bool1;
    } 
    return true;
  }
  
  public static float getDpDensity(Application paramApplication) {
    if (paramApplication != null)
      try {
        int i = (paramApplication.getApplicationContext().getResources().getDisplayMetrics()).densityDpi;
        return i / 160.0F;
      } catch (Exception exception) {} 
    return DisplayMetrics.DENSITY_DEVICE_STABLE / 160.0F;
  }
  
  public static float getScreenWidth(Application paramApplication) {
    if (paramApplication != null)
      try {
        int i = (paramApplication.getApplicationContext().getResources().getDisplayMetrics()).widthPixels;
        return i;
      } catch (Exception exception) {} 
    return 1440.0F;
  }
  
  public static float getDpDensity(View paramView) {
    if (paramView != null)
      try {
        int i = (paramView.getContext().getResources().getDisplayMetrics()).densityDpi;
        return i / 160.0F;
      } catch (Exception exception) {} 
    return DisplayMetrics.DENSITY_DEVICE_STABLE / 160.0F;
  }
  
  public static float getScreenWidth(View paramView) {
    if (paramView != null)
      try {
        int i = (paramView.getContext().getResources().getDisplayMetrics()).widthPixels;
        return i;
      } catch (Exception exception) {} 
    return 1440.0F;
  }
  
  public static int makeColorBackground(int paramInt, float paramFloat) {
    double[] arrayOfDouble = new double[3];
    ColorUtils.colorToLAB(paramInt, arrayOfDouble);
    double d = 100.0D - arrayOfDouble[0];
    if (d < arrayOfDouble[0]) {
      double d1 = d;
      if (paramFloat != -1.0F)
        d1 = paramFloat + d / 50.0D * (50.0F - paramFloat); 
      arrayOfDouble[0] = d1;
      int i = ColorUtils.LABToColor(arrayOfDouble[0], arrayOfDouble[1], arrayOfDouble[2]);
      int j = Color.alpha(paramInt), k = Color.red(i);
      paramInt = Color.green(i);
      i = Color.blue(i);
      return Color.argb(j, k, paramInt, i);
    } 
    return paramInt;
  }
  
  public static int makeColorForeground(int paramInt, float paramFloat) {
    double[] arrayOfDouble = new double[3];
    ColorUtils.colorToLAB(paramInt, arrayOfDouble);
    double d = 100.0D - arrayOfDouble[0];
    if (d > arrayOfDouble[0]) {
      double d1 = d;
      if (paramFloat != -1.0F)
        d1 = (d - 50.0D) / 50.0D * (paramFloat - 50.0F) + 50.0D; 
      arrayOfDouble[0] = d1;
      int i = ColorUtils.LABToColor(arrayOfDouble[0], arrayOfDouble[1], arrayOfDouble[2]);
      int j = Color.alpha(paramInt), k = Color.red(i);
      paramInt = Color.green(i);
      i = Color.blue(i);
      return Color.argb(j, k, paramInt, i);
    } 
    return paramInt;
  }
  
  public static int makeSVGColorForeground(int paramInt, float paramFloat) {
    float[] arrayOfFloat = new float[3];
    ColorUtils.colorToHSL(paramInt, arrayOfFloat);
    if (isMaybeBlackColor(arrayOfFloat)) {
      arrayOfFloat[2] = 1.0F - arrayOfFloat[2];
      int i = ColorUtils.HSLToColor(arrayOfFloat);
      paramInt = Color.alpha(paramInt);
      int j = Color.red(i), k = Color.green(i);
      i = Color.blue(i);
      return Color.argb(paramInt, j, k, i);
    } 
    return paramInt;
  }
  
  public static int makeSVGColorBackground(int paramInt, float paramFloat) {
    float[] arrayOfFloat = new float[3];
    ColorUtils.colorToHSL(paramInt, arrayOfFloat);
    if (isMaybeWhiteColor(arrayOfFloat)) {
      arrayOfFloat[2] = 1.0F - arrayOfFloat[2];
      int i = ColorUtils.HSLToColor(arrayOfFloat);
      int j = Color.alpha(paramInt);
      paramInt = Color.red(i);
      int k = Color.green(i);
      i = Color.blue(i);
      return Color.argb(j, paramInt, k, i);
    } 
    return paramInt;
  }
  
  public static void setCanvasTransformType(Canvas paramCanvas, int paramInt1, int paramInt2, int paramInt3) {
    OplusBaseBaseCanvas oplusBaseBaseCanvas = getOppoCanvas(paramCanvas);
    if (oplusBaseBaseCanvas != null) {
      oplusBaseBaseCanvas.setTransformType(paramInt1);
      oplusBaseBaseCanvas.setViewArea(paramInt2, paramInt3);
    } 
  }
  
  public static boolean hasCalculatedColor(Bitmap paramBitmap) {
    boolean bool;
    OplusBaseBitmap oplusBaseBitmap = getOppoBitmap(paramBitmap);
    if (oplusBaseBitmap != null && oplusBaseBitmap.hasCalculatedColor()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static void setHasCalculatedColor(Bitmap paramBitmap, boolean paramBoolean) {
    OplusBaseBitmap oplusBaseBitmap = getOppoBitmap(paramBitmap);
    if (oplusBaseBitmap != null)
      oplusBaseBitmap.setHasCalculatedColor(paramBoolean); 
  }
  
  public static boolean isAddArea(Path paramPath) {
    OplusBasePath oplusBasePath = (OplusBasePath)OplusTypeCastingHelper.typeCasting(OplusBasePath.class, paramPath);
    if (oplusBasePath != null)
      return oplusBasePath.isAddArea(); 
    return false;
  }
  
  public static void setColorFilterColor(ColorFilter paramColorFilter, int paramInt) {
    OplusBaseOplusFilter oplusBaseOplusFilter = (OplusBaseOplusFilter)OplusTypeCastingHelper.typeCasting(OplusBaseOplusFilter.class, paramColorFilter);
    if (oplusBaseOplusFilter != null)
      oplusBaseOplusFilter.setColor(paramInt); 
  }
  
  public static boolean isViewSrc(Bitmap paramBitmap) {
    boolean bool;
    OplusBaseBitmap oplusBaseBitmap = getOppoBitmap(paramBitmap);
    if (oplusBaseBitmap != null && oplusBaseBitmap.isViewSrc()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isCanvasBaseBitmap(Bitmap paramBitmap) {
    boolean bool;
    OplusBaseBitmap oplusBaseBitmap = getOppoBitmap(paramBitmap);
    if (oplusBaseBitmap != null && oplusBaseBitmap.isCanvasBaseBitmap()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isAssetSource(Bitmap paramBitmap) {
    boolean bool;
    OplusBaseBitmap oplusBaseBitmap = getOppoBitmap(paramBitmap);
    if (oplusBaseBitmap != null && oplusBaseBitmap.isAssetSource()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static int getColorState(Bitmap paramBitmap) {
    OplusBaseBitmap oplusBaseBitmap = getOppoBitmap(paramBitmap);
    if (oplusBaseBitmap != null)
      return oplusBaseBitmap.getColorState(); 
    return 0;
  }
  
  public static void setColorState(Bitmap paramBitmap, int paramInt) {
    OplusBaseBitmap oplusBaseBitmap = getOppoBitmap(paramBitmap);
    if (oplusBaseBitmap != null)
      oplusBaseBitmap.setColorState(paramInt); 
  }
  
  public static void setCrudeState(View paramView, int paramInt) {
    OplusBaseView oplusBaseView = getOppoView(paramView);
    if (oplusBaseView != null)
      oplusBaseView.setCrudeState(paramInt); 
  }
  
  public static int getCrudeState(View paramView) {
    OplusBaseView oplusBaseView = getOppoView(paramView);
    if (oplusBaseView != null)
      return oplusBaseView.getCrudeState(); 
    return 0;
  }
  
  public static int getViewType(View paramView) {
    OplusBaseView oplusBaseView = getOppoView(paramView);
    if (oplusBaseView != null)
      return oplusBaseView.getOplusViewType(); 
    return 0;
  }
  
  public static void setViewType(View paramView, int paramInt) {
    OplusBaseView oplusBaseView = getOppoView(paramView);
    if (oplusBaseView != null)
      oplusBaseView.setOplusViewTypeLocked(paramInt); 
  }
  
  public static int getViewType(Canvas paramCanvas) {
    OplusBaseBaseCanvas oplusBaseBaseCanvas = getOppoCanvas(paramCanvas);
    if (oplusBaseBaseCanvas != null)
      return oplusBaseBaseCanvas.getOplusViewType(); 
    return 0;
  }
  
  public static void setViewType(Canvas paramCanvas, int paramInt) {
    OplusBaseBaseCanvas oplusBaseBaseCanvas = getOppoCanvas(paramCanvas);
    if (oplusBaseBaseCanvas != null)
      oplusBaseBaseCanvas.setOplusViewTypeLocked(paramInt); 
  }
  
  private static OplusBaseBaseCanvas getOppoCanvas(Canvas paramCanvas) {
    return (OplusBaseBaseCanvas)OplusTypeCastingHelper.typeCasting(OplusBaseBaseCanvas.class, paramCanvas);
  }
  
  private static OplusBaseBitmap getOppoBitmap(Bitmap paramBitmap) {
    return (OplusBaseBitmap)OplusTypeCastingHelper.typeCasting(OplusBaseBitmap.class, paramBitmap);
  }
  
  private static OplusBaseView getOppoView(View paramView) {
    return (OplusBaseView)OplusTypeCastingHelper.typeCasting(OplusBaseView.class, paramView);
  }
  
  public static boolean isBiliBili(Application paramApplication) {
    if (paramApplication != null)
      try {
        if (paramApplication.getApplicationContext() != null)
          return "tv.danmaku.bili".equals(paramApplication.getApplicationContext().getPackageName()); 
      } catch (Exception exception) {} 
    return false;
  }
  
  public static boolean isWeiBo(Application paramApplication) {
    if (paramApplication != null)
      try {
        if (paramApplication.getApplicationContext() != null)
          return "com.sina.weibo".equals(paramApplication.getApplicationContext().getPackageName()); 
      } catch (Exception exception) {} 
    return false;
  }
}
