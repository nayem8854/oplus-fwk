package com.oplus.darkmode;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusDarkModeListener extends IInterface {
  void onUiModeConfigurationChangeFinish() throws RemoteException;
  
  class Default implements IOplusDarkModeListener {
    public void onUiModeConfigurationChangeFinish() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusDarkModeListener {
    private static final String DESCRIPTOR = "com.oplus.darkmode.IOplusDarkModeListener";
    
    static final int TRANSACTION_onUiModeConfigurationChangeFinish = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.darkmode.IOplusDarkModeListener");
    }
    
    public static IOplusDarkModeListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.darkmode.IOplusDarkModeListener");
      if (iInterface != null && iInterface instanceof IOplusDarkModeListener)
        return (IOplusDarkModeListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onUiModeConfigurationChangeFinish";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.darkmode.IOplusDarkModeListener");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.darkmode.IOplusDarkModeListener");
      onUiModeConfigurationChangeFinish();
      return true;
    }
    
    private static class Proxy implements IOplusDarkModeListener {
      public static IOplusDarkModeListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.darkmode.IOplusDarkModeListener";
      }
      
      public void onUiModeConfigurationChangeFinish() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.darkmode.IOplusDarkModeListener");
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusDarkModeListener.Stub.getDefaultImpl() != null) {
            IOplusDarkModeListener.Stub.getDefaultImpl().onUiModeConfigurationChangeFinish();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusDarkModeListener param1IOplusDarkModeListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusDarkModeListener != null) {
          Proxy.sDefaultImpl = param1IOplusDarkModeListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusDarkModeListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
