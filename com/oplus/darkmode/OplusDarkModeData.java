package com.oplus.darkmode;

import android.os.Parcel;
import android.os.Parcelable;

public class OplusDarkModeData implements Parcelable {
  public long mVersionCode = -1L;
  
  public int mIsRecommend = 0;
  
  public int mOldType = 0;
  
  public int mCurType = 0;
  
  public boolean mOpenByUser = false;
  
  public boolean mAlreadyClickByUser = false;
  
  private OplusDarkModeData(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mVersionCode);
    paramParcel.writeInt(this.mIsRecommend);
    paramParcel.writeInt(this.mOldType);
    paramParcel.writeInt(this.mCurType);
    paramParcel.writeBoolean(this.mOpenByUser);
    paramParcel.writeBoolean(this.mAlreadyClickByUser);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mVersionCode = paramParcel.readLong();
    this.mIsRecommend = paramParcel.readInt();
    this.mOldType = paramParcel.readInt();
    this.mCurType = paramParcel.readInt();
    this.mOpenByUser = paramParcel.readBoolean();
    this.mAlreadyClickByUser = paramParcel.readBoolean();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<OplusDarkModeData> CREATOR = new Parcelable.Creator<OplusDarkModeData>() {
      public OplusDarkModeData createFromParcel(Parcel param1Parcel) {
        return new OplusDarkModeData(param1Parcel);
      }
      
      public OplusDarkModeData[] newArray(int param1Int) {
        return new OplusDarkModeData[param1Int];
      }
    };
  
  public OplusDarkModeData() {}
}
