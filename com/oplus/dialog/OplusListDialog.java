package com.oplus.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class OplusListDialog implements DialogInterface {
  private ListAdapter mAdapter;
  
  private AlertDialog.Builder mBuilder;
  
  private Context mContext;
  
  private int mCustomRes;
  
  private View mCustomView;
  
  private AlertDialog mDialog;
  
  private boolean mHasCustom;
  
  private CharSequence[] mItems;
  
  private CharSequence mMessage;
  
  private TextView mMessageView;
  
  private DialogInterface.OnClickListener mOnClickListener;
  
  private int[] mTextAppearances;
  
  public OplusListDialog(Context paramContext) {
    this.mContext = paramContext;
    this.mBuilder = new AlertDialog.Builder(paramContext);
  }
  
  public OplusListDialog(Context paramContext, int paramInt) {
    this.mContext = paramContext;
    this.mBuilder = new AlertDialog.Builder(paramContext, paramInt);
  }
  
  public OplusListDialog setItems(CharSequence[] paramArrayOfCharSequence, int[] paramArrayOfint, DialogInterface.OnClickListener paramOnClickListener) {
    this.mItems = paramArrayOfCharSequence;
    this.mTextAppearances = paramArrayOfint;
    this.mOnClickListener = paramOnClickListener;
    return this;
  }
  
  public OplusListDialog setTitle(CharSequence paramCharSequence) {
    this.mBuilder.setTitle(paramCharSequence);
    return this;
  }
  
  public OplusListDialog setMessage(CharSequence paramCharSequence) {
    this.mMessage = paramCharSequence;
    return this;
  }
  
  public OplusListDialog setCustomView(int paramInt) {
    this.mCustomRes = paramInt;
    this.mHasCustom = true;
    return this;
  }
  
  public OplusListDialog setCustomView(View paramView) {
    this.mCustomView = paramView;
    this.mHasCustom = true;
    return this;
  }
  
  public void show() {
    if (this.mDialog == null)
      this.mDialog = create(); 
    this.mDialog.show();
  }
  
  public AlertDialog create() {
    View view = LayoutInflater.from(this.mContext).inflate(202440715, null);
    setupMessage(view);
    setupCustomPanel(view);
    if (this.mItems != null || this.mAdapter != null)
      setupListPanel(view); 
    this.mBuilder.setView(view);
    return this.mBuilder.create();
  }
  
  public void cancel() {
    AlertDialog alertDialog = this.mDialog;
    if (alertDialog != null)
      alertDialog.cancel(); 
  }
  
  public AlertDialog getDialog() {
    if (this.mDialog == null)
      this.mDialog = create(); 
    return this.mDialog;
  }
  
  public void dismiss() {
    AlertDialog alertDialog = this.mDialog;
    if (alertDialog != null)
      alertDialog.dismiss(); 
  }
  
  public boolean isShowing() {
    boolean bool;
    AlertDialog alertDialog = this.mDialog;
    if (alertDialog != null && alertDialog.isShowing()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void setupMessage(View paramView) {
    TextView textView = (TextView)paramView.findViewById(201457752);
    textView.setText(this.mMessage);
    if (TextUtils.isEmpty(this.mMessage)) {
      this.mMessageView.setVisibility(8);
    } else {
      paramView = paramView.findViewById(201457747);
      paramView.setVisibility(0);
      this.mMessageView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            final OplusListDialog this$0;
            
            public void onGlobalLayout() {
              int i = OplusListDialog.this.mMessageView.getLineCount();
              if (i > 1) {
                OplusListDialog.this.mMessageView.setTextAlignment(2);
              } else {
                OplusListDialog.this.mMessageView.setTextAlignment(4);
              } 
              OplusListDialog.this.mMessageView.setText(OplusListDialog.this.mMessageView.getText());
              OplusListDialog.this.mMessageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
          });
    } 
  }
  
  private void setupCustomPanel(View paramView) {
    if (this.mHasCustom) {
      FrameLayout frameLayout = (FrameLayout)paramView.findViewById(201457712);
      View view = this.mCustomView;
      if (view != null) {
        frameLayout.addView(view);
      } else {
        view = LayoutInflater.from(this.mContext).inflate(this.mCustomRes, null);
        frameLayout.addView(view);
      } 
    } 
  }
  
  private void setupListPanel(View paramView) {
    ListView listView = (ListView)paramView.findViewById(201457749);
    listView.setAdapter(getAdapter());
    if (this.mOnClickListener != null)
      listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            final OplusListDialog this$0;
            
            public void onItemClick(AdapterView<?> param1AdapterView, View param1View, int param1Int, long param1Long) {
              OplusListDialog.this.mOnClickListener.onClick((DialogInterface)OplusListDialog.this.mDialog, param1Int);
            }
          }); 
  }
  
  private ListAdapter getAdapter() {
    Adapter adapter;
    ListAdapter listAdapter1 = this.mAdapter, listAdapter2 = listAdapter1;
    if (listAdapter1 == null)
      adapter = new Adapter(this.mContext, this.mItems, this.mTextAppearances); 
    return (ListAdapter)adapter;
  }
  
  public OplusListDialog setAdapter(ListAdapter paramListAdapter) {
    this.mAdapter = paramListAdapter;
    return this;
  }
  
  class Adapter extends BaseAdapter {
    private static final int LAYOUT = 202440716;
    
    private Context mContext;
    
    private CharSequence[] mItems;
    
    private int[] mTextAppearances;
    
    Adapter(OplusListDialog this$0, CharSequence[] param1ArrayOfCharSequence, int[] param1ArrayOfint) {
      this.mContext = (Context)this$0;
      this.mItems = param1ArrayOfCharSequence;
      this.mTextAppearances = param1ArrayOfint;
    }
    
    public int getCount() {
      int i;
      CharSequence[] arrayOfCharSequence = this.mItems;
      if (arrayOfCharSequence == null) {
        i = 0;
      } else {
        i = arrayOfCharSequence.length;
      } 
      return i;
    }
    
    public CharSequence getItem(int param1Int) {
      CharSequence charSequence, arrayOfCharSequence[] = this.mItems;
      if (arrayOfCharSequence == null) {
        arrayOfCharSequence = null;
      } else {
        charSequence = arrayOfCharSequence[param1Int];
      } 
      return charSequence;
    }
    
    public long getItemId(int param1Int) {
      return param1Int;
    }
    
    public View getView(int param1Int, View param1View, ViewGroup param1ViewGroup) {
      param1View = getViewInternal(param1Int, param1View, param1ViewGroup);
      resetPadding(param1Int, param1View);
      return param1View;
    }
    
    private View getViewInternal(int param1Int, View param1View, ViewGroup param1ViewGroup) {
      OplusListDialog.ViewHolder viewHolder1, viewHolder2;
      if (param1View == null) {
        View view = LayoutInflater.from(this.mContext).inflate(202440716, param1ViewGroup, false);
        TextView textView = (TextView)view.findViewById(16908308);
        viewHolder1 = new OplusListDialog.ViewHolder();
        viewHolder1.mTextView = textView;
        view.setTag(viewHolder1);
      } else {
        OplusListDialog.ViewHolder viewHolder = (OplusListDialog.ViewHolder)viewHolder1.getTag();
        viewHolder2 = viewHolder1;
        viewHolder1 = viewHolder;
      } 
      viewHolder1.mTextView.setText(getItem(param1Int));
      int[] arrayOfInt = this.mTextAppearances;
      if (arrayOfInt != null) {
        param1Int = arrayOfInt[param1Int];
        if (param1Int > 0) {
          viewHolder1.mTextView.setTextAppearance(this.mContext, param1Int);
        } else {
          viewHolder1.mTextView.setTextAppearance(this.mContext, 201523242);
        } 
      } 
      return (View)viewHolder2;
    }
    
    private void resetPadding(int param1Int, View param1View) {
      int i = this.mContext.getResources().getDimensionPixelSize(201654372);
      int j = this.mContext.getResources().getDimensionPixelSize(201654377);
      int k = this.mContext.getResources().getDimensionPixelSize(201654375);
      int m = this.mContext.getResources().getDimensionPixelSize(201654374);
      int n = this.mContext.getResources().getDimensionPixelSize(201654376);
      int i1 = this.mContext.getResources().getDimensionPixelSize(201654373);
      if (getCount() > 1)
        if (param1Int == getCount() - 1) {
          param1View.setPadding(k, j, n, m + i);
          param1View.setMinimumHeight(i1 + i);
        } else if (param1Int == 0) {
          param1View.setPadding(k, j + i, n, m);
          param1View.setMinimumHeight(i1 + i);
        } else {
          param1View.setPadding(k, j, n, m);
          param1View.setMinimumHeight(i1);
        }  
    }
  }
  
  class ViewHolder {
    TextView mTextView;
    
    private ViewHolder() {}
  }
}
