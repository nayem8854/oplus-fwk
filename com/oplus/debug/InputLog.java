package com.oplus.debug;

import android.os.FileObserver;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.util.HashSet;
import java.util.Set;

public class InputLog {
  public static boolean DEBUG = false;
  
  private static boolean ERROR = false;
  
  private static boolean INFO = false;
  
  private static final boolean IS_DEBUGING = false;
  
  public static final int KEY_DISPATCHING_TIMEOUT = 5000;
  
  private static int LOGCAT_LEVEL = 0;
  
  private static final String LOGSWITCH_DIR_PATH = "/data/logswitch";
  
  private static final String LOGSWITCH_FILE_NAME = "switch.xml";
  
  private static final String LOGSWITCH_FILE_PATH = "/data/logswitch/switch.xml";
  
  private static final int LOG_LEVEL_ALL = 0;
  
  private static final int LOG_LEVEL_DEBUG = 2;
  
  private static final int LOG_LEVEL_DEFAULT = 16;
  
  private static final int LOG_LEVEL_ERROR = 16;
  
  private static final int LOG_LEVEL_INFO = 4;
  
  private static final int LOG_LEVEL_VERBOSE = 0;
  
  private static final int LOG_LEVEL_WARN = 8;
  
  private static final String LOG_TAG_STRING = "InputLog";
  
  private static final String SWITCH_OFF_VALUE = "off";
  
  private static final String SWITCH_ON_VALUE = "on";
  
  private static boolean VERBOSE;
  
  private static boolean WARN;
  
  private static final Set<PosixFilePermission> mPerms;
  
  private static boolean sInited = false;
  
  private static LogSwitchObserver sLogSwitchObserver;
  
  static {
    boolean bool1 = false;
  }
  
  static {
    LOGCAT_LEVEL = 16;
    if (16 <= 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    VERBOSE = bool2;
    if (LOGCAT_LEVEL <= 2) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    DEBUG = bool2;
    if (LOGCAT_LEVEL <= 4) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    INFO = bool2;
    if (LOGCAT_LEVEL <= 8) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    WARN = bool2;
    boolean bool2 = bool1;
    if (LOGCAT_LEVEL <= 16)
      bool2 = true; 
    ERROR = bool2;
    mPerms = new HashSet<>();
    startWatching();
    initInputlogPermission();
  }
  
  public static void initInputlogPermission() {
    mPerms.add(PosixFilePermission.OWNER_EXECUTE);
    mPerms.add(PosixFilePermission.OWNER_READ);
    mPerms.add(PosixFilePermission.OWNER_WRITE);
    mPerms.add(PosixFilePermission.GROUP_EXECUTE);
    mPerms.add(PosixFilePermission.GROUP_READ);
    mPerms.add(PosixFilePermission.GROUP_WRITE);
    mPerms.add(PosixFilePermission.OTHERS_EXECUTE);
    mPerms.add(PosixFilePermission.OTHERS_READ);
    mPerms.add(PosixFilePermission.OTHERS_WRITE);
  }
  
  class LogSwitchObserver extends FileObserver {
    public LogSwitchObserver(InputLog this$0) {
      super((String)this$0, 10);
    }
    
    public void onEvent(int param1Int, String param1String) {
      InputLog.updateLogLevel();
    }
  }
  
  public static boolean isOpenAllLog() {
    boolean bool;
    if (LOGCAT_LEVEL == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static void dumpEventType(int paramInt) {
    paramInt &= 0xFFF;
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 4) {
          if (paramInt != 8) {
            if (paramInt != 16) {
              if (paramInt != 32) {
                if (paramInt != 64) {
                  if (paramInt != 128) {
                    if (paramInt != 256) {
                      if (paramInt != 512) {
                        if (paramInt != 1024) {
                          if (paramInt == 2048)
                            Log.d("InputLog", "FileObserver.MOVE_SELF "); 
                        } else {
                          Log.d("InputLog", "FileObserver.DELETE_SELF ");
                        } 
                      } else {
                        Log.d("InputLog", "FileObserver.DELETE ");
                      } 
                    } else {
                      Log.d("InputLog", "FileObserver.CREATE ");
                    } 
                  } else {
                    Log.d("InputLog", "FileObserver.MOVED_TO ");
                  } 
                } else {
                  Log.d("InputLog", "FileObserver.MOVED_FROM ");
                } 
              } else {
                Log.d("InputLog", "FileObserver.OPEN ");
              } 
            } else {
              Log.d("InputLog", "FileObserver.CLOSE_NOWRITE ");
            } 
          } else {
            Log.d("InputLog", "FileObserver.CLOSE_WRITE ");
          } 
        } else {
          Log.d("InputLog", "FileObserver.ATTRIB ");
        } 
      } else {
        Log.d("InputLog", "FileObserver.MODIFY ");
      } 
    } else {
      Log.d("InputLog", "FileObserver.ACCESS ");
    } 
  }
  
  public static void startWatching() {
    if (!sInited && checkLogSwitchDirExist() && "on".equals(getCurrentLogSwitchValue())) {
      LogSwitchObserver logSwitchObserver = new LogSwitchObserver("/data/logswitch");
      if (logSwitchObserver != null) {
        logSwitchObserver.startWatching();
        sInited = true;
        updateLogLevel();
      } 
    } 
  }
  
  public static void restoreToDefaltLogLevel() {
    Log.d("InputLog", " restoreToDefaltLogLevel");
    File file = new File("/data/logswitch/switch.xml");
    if (file.exists()) {
      file.delete();
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(file);
      stringBuilder.append(" still not exists");
      Log.d("InputLog", stringBuilder.toString());
    } 
  }
  
  public static boolean checkLogSwitchDirExist() {
    File file = new File("/data/logswitch");
    if (!file.exists())
      return false; 
    return true;
  }
  
  private static boolean changeFileAttr(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("changeFileAttr filePath = ");
    stringBuilder.append(paramString);
    Log.d("InputLog", stringBuilder.toString());
    File file = new File(paramString);
    try {
      if (!file.exists()) {
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("changeFileAttr filePath failed!!!! filePath:");
        stringBuilder1.append(paramString);
        Log.d("InputLog", stringBuilder1.toString());
      } 
      Files.setPosixFilePermissions(file.toPath(), mPerms);
      return true;
    } catch (IOException iOException) {
      Log.i("InputLog", "changeFileAttr filePath exec failed !!!!");
      return false;
    } 
  }
  
  public static boolean initLogSwitchDir() {
    Log.d("InputLog", "initLogSwitchDir Begin");
    File file = new File("/data/logswitch");
    boolean bool = file.exists();
    boolean bool1 = true;
    if (!bool) {
      boolean bool2 = file.mkdir();
      bool = true;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(file);
      stringBuilder.append("initLogSwitchDir  mkdir , ok = ");
      stringBuilder.append(bool2);
      Log.d("InputLog", stringBuilder.toString());
      if (bool2)
        bool = changeFileAttr("/data/logswitch"); 
      if (bool2 && bool) {
        bool = bool1;
      } else {
        bool = false;
      } 
      return bool;
    } 
    return true;
  }
  
  public static boolean initLogSwitch() {
    if (initLogSwitchDir())
      return initLogSwitchFile(); 
    return false;
  }
  
  public static boolean initLogSwitchFile() {
    Log.d("InputLog", " initLogSwitchFile ");
    File file = new File("/data/logswitch/switch.xml");
    if (!file.exists()) {
      boolean bool;
      try {
        bool = file.createNewFile();
      } catch (IOException iOException) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(file);
        stringBuilder.append(" createNewFile failed in initLogSwitchFile(), ");
        stringBuilder.append(iOException);
        Log.d("InputLog", stringBuilder.toString());
        bool = false;
      } finally {}
      if (bool) {
        changeFileAttr("/data/logswitch/switch.xml");
        return true;
      } 
      return false;
    } 
    return true;
  }
  
  public static void OpenAllLogLevel() {
    Log.d("InputLog", " OpenAllLogLevel 22");
    File file = new File("/data/logswitch/switch.xml");
    try {
      if (!file.exists()) {
        file.createNewFile();
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(file);
        stringBuilder.append(" still exists or open failed, ");
        Log.d("InputLog", stringBuilder.toString());
      } 
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append(file);
      stringBuilder.append(" createNewFile failed in OpenAllLogLevel(), ");
      stringBuilder.append(iOException);
      Log.d("InputLog", stringBuilder.toString());
    } finally {}
  }
  
  private static void updateLogLevel() {
    byte b;
    String str = readString("/data/logswitch/switch.xml");
    boolean bool = "on".equals(str);
    boolean bool1 = false;
    if (bool) {
      b = 0;
    } else {
      b = 16;
    } 
    LOGCAT_LEVEL = b;
    if (b <= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    VERBOSE = bool;
    if (LOGCAT_LEVEL <= 2) {
      bool = true;
    } else {
      bool = false;
    } 
    DEBUG = bool;
    if (LOGCAT_LEVEL <= 4) {
      bool = true;
    } else {
      bool = false;
    } 
    INFO = bool;
    if (LOGCAT_LEVEL <= 8) {
      bool = true;
    } else {
      bool = false;
    } 
    WARN = bool;
    bool = bool1;
    if (LOGCAT_LEVEL <= 16)
      bool = true; 
    ERROR = bool;
  }
  
  public static String getLogLevelString() {
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("LOGCAT_LEVEL = ");
    stringBuilder2.append(LOGCAT_LEVEL);
    String str1 = stringBuilder2.toString();
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append(str1);
    stringBuilder3.append(", VERBOSE = ");
    stringBuilder3.append(VERBOSE);
    str1 = stringBuilder3.toString();
    stringBuilder3 = new StringBuilder();
    stringBuilder3.append(str1);
    stringBuilder3.append(", DEBUG = ");
    stringBuilder3.append(DEBUG);
    str1 = stringBuilder3.toString();
    stringBuilder3 = new StringBuilder();
    stringBuilder3.append(str1);
    stringBuilder3.append(", INFO = ");
    stringBuilder3.append(INFO);
    String str2 = stringBuilder3.toString();
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(str2);
    stringBuilder1.append(", WARN = ");
    stringBuilder1.append(WARN);
    str2 = stringBuilder1.toString();
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append(str2);
    stringBuilder1.append(", ERROR = ");
    stringBuilder1.append(ERROR);
    return stringBuilder1.toString();
  }
  
  public static void i(String paramString1, String paramString2) {
    if (INFO) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(" : ");
      stringBuilder.append(paramString2);
      Log.i("InputLog", stringBuilder.toString());
    } 
  }
  
  public static void i(String paramString1, String paramString2, Throwable paramThrowable) {
    if (INFO) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(" : ");
      stringBuilder.append(paramString2);
      Log.i("InputLog", stringBuilder.toString(), paramThrowable);
    } 
  }
  
  public static void v(String paramString1, String paramString2) {
    if (VERBOSE) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(" : ");
      stringBuilder.append(paramString2);
      Log.v("InputLog", stringBuilder.toString());
    } 
  }
  
  public static void v(String paramString1, String paramString2, Throwable paramThrowable) {
    if (VERBOSE) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(" : ");
      stringBuilder.append(paramString2);
      Log.v("InputLog", stringBuilder.toString(), paramThrowable);
    } 
  }
  
  public static void d(String paramString1, String paramString2) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(" : ");
      stringBuilder.append(paramString2);
      Log.d("InputLog", stringBuilder.toString());
    } 
  }
  
  public static void d(String paramString1, String paramString2, Throwable paramThrowable) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(" : ");
      stringBuilder.append(paramString2);
      Log.d("InputLog", stringBuilder.toString(), paramThrowable);
    } 
  }
  
  public static void w(String paramString1, String paramString2) {
    if (WARN) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(" : ");
      stringBuilder.append(paramString2);
      Log.w("InputLog", stringBuilder.toString());
    } 
  }
  
  public static void w(String paramString1, String paramString2, Throwable paramThrowable) {
    if (WARN) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(" : ");
      stringBuilder.append(paramString2);
      Log.w("InputLog", stringBuilder.toString(), paramThrowable);
    } 
  }
  
  public static void e(String paramString1, String paramString2) {
    if (ERROR) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(" : ");
      stringBuilder.append(paramString2);
      Log.e("InputLog", stringBuilder.toString());
    } 
  }
  
  public static void e(String paramString1, String paramString2, Throwable paramThrowable) {
    if (ERROR) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(" : ");
      stringBuilder.append(paramString2);
      Log.e("InputLog", stringBuilder.toString(), paramThrowable);
    } 
  }
  
  public static void wtf(String paramString1, String paramString2) {
    if (ERROR) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(" : ");
      stringBuilder.append(paramString2);
      Log.wtf("InputLog", stringBuilder.toString());
    } 
  }
  
  public static void wtf(String paramString1, String paramString2, Throwable paramThrowable) {
    if (ERROR) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(" : ");
      stringBuilder.append(paramString2);
      Log.wtf("InputLog", stringBuilder.toString(), paramThrowable);
    } 
  }
  
  public static void dynamicLog(boolean paramBoolean) {
    String str;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("dynamicLog ,  openAll = ");
    stringBuilder.append(paramBoolean);
    Log.d("/data/logswitch/switch.xml", stringBuilder.toString());
    if (paramBoolean) {
      str = "on";
    } else {
      str = "off";
    } 
    writeString("/data/logswitch/switch.xml", str);
  }
  
  private static boolean writeString(String paramString1, String paramString2) {
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("writeString value = ");
    stringBuilder2.append(paramString2);
    stringBuilder2.append(" to ");
    stringBuilder2.append(paramString1);
    Log.d("InputLog", stringBuilder2.toString());
    FileWriter fileWriter1 = null;
    StringBuilder stringBuilder3 = null;
    boolean bool = true;
    stringBuilder2 = stringBuilder3;
    FileWriter fileWriter2 = fileWriter1;
    try {
      FileWriter fileWriter4 = new FileWriter();
      stringBuilder2 = stringBuilder3;
      fileWriter2 = fileWriter1;
      this(paramString1);
      FileWriter fileWriter3 = fileWriter4;
      fileWriter2 = fileWriter4;
      fileWriter4.write(paramString2);
      fileWriter3 = fileWriter4;
      fileWriter2 = fileWriter4;
      stringBuilder3 = new StringBuilder();
      fileWriter3 = fileWriter4;
      fileWriter2 = fileWriter4;
      this();
      fileWriter3 = fileWriter4;
      fileWriter2 = fileWriter4;
      stringBuilder3.append("writeString ");
      fileWriter3 = fileWriter4;
      fileWriter2 = fileWriter4;
      stringBuilder3.append(paramString2);
      fileWriter3 = fileWriter4;
      fileWriter2 = fileWriter4;
      stringBuilder3.append(" to ");
      fileWriter3 = fileWriter4;
      fileWriter2 = fileWriter4;
      stringBuilder3.append(paramString1);
      fileWriter3 = fileWriter4;
      fileWriter2 = fileWriter4;
      stringBuilder3.append(" ok");
      fileWriter3 = fileWriter4;
      fileWriter2 = fileWriter4;
      Log.d("InputLog", stringBuilder3.toString());
      try {
        fileWriter4.close();
      } catch (IOException iOException) {
        bool = false;
      } 
    } catch (Exception exception) {
      boolean bool1 = false;
      FileWriter fileWriter = fileWriter2;
      Log.d("InputLog", "writeString failed, ", exception);
      bool = bool1;
      if (fileWriter2 != null) {
        fileWriter2.close();
        bool = bool1;
      } 
    } finally {}
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("writeString sucuess = ");
    stringBuilder1.append(bool);
    Log.d("InputLog", stringBuilder1.toString());
    return bool;
  }
  
  private static String readString(String paramString) {
    String str1;
    File file = new File(paramString);
    BufferedReader bufferedReader1 = null, bufferedReader2 = null;
    String str2 = "";
    BufferedReader bufferedReader3 = bufferedReader2, bufferedReader4 = bufferedReader1;
    paramString = str2;
    try {
      BufferedReader bufferedReader = new BufferedReader();
      bufferedReader3 = bufferedReader2;
      bufferedReader4 = bufferedReader1;
      paramString = str2;
      FileReader fileReader = new FileReader();
      bufferedReader3 = bufferedReader2;
      bufferedReader4 = bufferedReader1;
      paramString = str2;
      this(file);
      bufferedReader3 = bufferedReader2;
      bufferedReader4 = bufferedReader1;
      paramString = str2;
      this(fileReader);
      bufferedReader2 = bufferedReader;
      while (true) {
        bufferedReader3 = bufferedReader2;
        bufferedReader4 = bufferedReader2;
        paramString = str2;
        String str3 = bufferedReader2.readLine();
        if (str3 != null) {
          bufferedReader3 = bufferedReader2;
          bufferedReader4 = bufferedReader2;
          paramString = str2;
          StringBuilder stringBuilder = new StringBuilder();
          bufferedReader3 = bufferedReader2;
          bufferedReader4 = bufferedReader2;
          paramString = str2;
          this();
          bufferedReader3 = bufferedReader2;
          bufferedReader4 = bufferedReader2;
          paramString = str2;
          stringBuilder.append(str2);
          bufferedReader3 = bufferedReader2;
          bufferedReader4 = bufferedReader2;
          paramString = str2;
          stringBuilder.append(str3);
          bufferedReader3 = bufferedReader2;
          bufferedReader4 = bufferedReader2;
          paramString = str2;
          str2 = stringBuilder.toString();
          continue;
        } 
        break;
      } 
      bufferedReader3 = bufferedReader2;
      bufferedReader4 = bufferedReader2;
      paramString = str2;
      bufferedReader2.close();
      String str = str2;
      try {
        bufferedReader2.close();
        paramString = str2;
        str2 = paramString;
      } catch (IOException iOException) {
        str1 = str;
        str2 = str1;
      } 
    } catch (IOException iOException) {
      bufferedReader3 = bufferedReader4;
      iOException.printStackTrace();
      bufferedReader3 = bufferedReader4;
      Log.d("InputLog", "readString failed ");
      str2 = str1;
      if (bufferedReader4 != null) {
        String str = str1;
        bufferedReader4.close();
      } else {
        return str2;
      } 
      str2 = str1;
    } finally {}
    return str2;
  }
  
  public static String getCurrentLogSwitchValue() {
    return readString("/data/logswitch/switch.xml");
  }
  
  protected void finalize() throws Throwable {
    LogSwitchObserver logSwitchObserver = sLogSwitchObserver;
    if (logSwitchObserver != null) {
      logSwitchObserver.stopWatching();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this);
      stringBuilder.append(" finalized, and sLogSwitchObserver.stopWatching ");
      Log.d("InputLog", stringBuilder.toString());
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" when ");
      stringBuilder.append(this);
      stringBuilder.append(" finalized, sLogSwitchObserver already been null.");
      Log.d("InputLog", stringBuilder.toString());
    } 
    super.finalize();
  }
  
  public static boolean isVolumeKey(int paramInt) {
    if (paramInt != 24 && paramInt != 25 && paramInt != 164)
      return false; 
    return true;
  }
}
