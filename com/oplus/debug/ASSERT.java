package com.oplus.debug;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.OplusAssertTip;
import android.os.SystemProperties;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ASSERT {
  private static final String ASSERT_STATE = "persist.sys.assert.state";
  
  private static final String DESTDIR = "/data/oppo_log/anr_binder_info/binder_info_";
  
  private static final int IS_GZIPPED = 4;
  
  private static final String SOURCEDIR = "/sys/kernel/debug/binder/state";
  
  private static final String TAG = "java.lang.ASSERT";
  
  private static OplusAssertTip mFunctionProxy;
  
  private static final Runtime rt = Runtime.getRuntime();
  
  static {
    mFunctionProxy = null;
  }
  
  public static boolean epitaph(File paramFile, String paramString, int paramInt, Context paramContext) {
    // Byte code:
    //   0: aload_0
    //   1: ifnonnull -> 6
    //   4: iconst_0
    //   5: ireturn
    //   6: aconst_null
    //   7: astore #4
    //   9: aconst_null
    //   10: astore #5
    //   12: aconst_null
    //   13: astore_1
    //   14: aconst_null
    //   15: astore #6
    //   17: aconst_null
    //   18: astore #7
    //   20: aconst_null
    //   21: astore #8
    //   23: aconst_null
    //   24: astore #9
    //   26: aconst_null
    //   27: astore #10
    //   29: aconst_null
    //   30: astore #11
    //   32: ldc 'NONE'
    //   34: astore #12
    //   36: aload #5
    //   38: astore #13
    //   40: aload_1
    //   41: astore #14
    //   43: ldc 'persist.sys.thridpart.debug'
    //   45: ldc 'false'
    //   47: invokestatic get : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   50: astore #15
    //   52: aload #5
    //   54: astore #13
    //   56: aload_1
    //   57: astore #14
    //   59: ldc 'true'
    //   61: aload #15
    //   63: invokevirtual equals : (Ljava/lang/Object;)Z
    //   66: istore #16
    //   68: iload #16
    //   70: ifeq -> 81
    //   73: sipush #4096
    //   76: istore #17
    //   78: goto -> 86
    //   81: sipush #1024
    //   84: istore #17
    //   86: aload #6
    //   88: astore_1
    //   89: aload #4
    //   91: astore #13
    //   93: new java/lang/StringBuilder
    //   96: astore #5
    //   98: aload #6
    //   100: astore_1
    //   101: aload #4
    //   103: astore #13
    //   105: aload #5
    //   107: iload #17
    //   109: invokespecial <init> : (I)V
    //   112: aload #6
    //   114: astore_1
    //   115: aload #4
    //   117: astore #13
    //   119: new java/io/FileInputStream
    //   122: astore #14
    //   124: aload #6
    //   126: astore_1
    //   127: aload #4
    //   129: astore #13
    //   131: aload #14
    //   133: aload_0
    //   134: invokespecial <init> : (Ljava/io/File;)V
    //   137: aload #14
    //   139: astore_1
    //   140: aload_1
    //   141: astore #14
    //   143: iload_2
    //   144: iconst_4
    //   145: iand
    //   146: ifeq -> 176
    //   149: aload_1
    //   150: astore #13
    //   152: aload_1
    //   153: astore #14
    //   155: new java/util/zip/GZIPInputStream
    //   158: astore #6
    //   160: aload_1
    //   161: astore #13
    //   163: aload_1
    //   164: astore #14
    //   166: aload #6
    //   168: aload_1
    //   169: invokespecial <init> : (Ljava/io/InputStream;)V
    //   172: aload #6
    //   174: astore #14
    //   176: aload #14
    //   178: astore_1
    //   179: aload #14
    //   181: astore #13
    //   183: new java/io/BufferedReader
    //   186: astore #9
    //   188: aload #14
    //   190: astore_1
    //   191: aload #14
    //   193: astore #13
    //   195: new java/io/InputStreamReader
    //   198: astore #6
    //   200: aload #14
    //   202: astore_1
    //   203: aload #14
    //   205: astore #13
    //   207: aload #6
    //   209: aload #14
    //   211: invokespecial <init> : (Ljava/io/InputStream;)V
    //   214: aload #14
    //   216: astore_1
    //   217: aload #14
    //   219: astore #13
    //   221: aload #9
    //   223: aload #6
    //   225: invokespecial <init> : (Ljava/io/Reader;)V
    //   228: aload #9
    //   230: astore #7
    //   232: iconst_0
    //   233: istore #18
    //   235: aload #11
    //   237: astore_1
    //   238: aload #10
    //   240: astore #13
    //   242: iload #18
    //   244: iload #17
    //   246: if_icmpge -> 802
    //   249: aload #7
    //   251: invokevirtual readLine : ()Ljava/lang/String;
    //   254: astore #11
    //   256: aload #11
    //   258: ifnull -> 746
    //   261: aload #12
    //   263: astore #8
    //   265: aload #13
    //   267: astore #10
    //   269: aload #13
    //   271: astore #10
    //   273: aload #13
    //   275: astore #10
    //   277: aload #11
    //   279: ldc_w '-----'
    //   282: iconst_0
    //   283: invokevirtual startsWith : (Ljava/lang/String;I)Z
    //   286: ifeq -> 292
    //   289: goto -> 802
    //   292: aload #13
    //   294: astore #10
    //   296: aload #13
    //   298: astore #10
    //   300: aload #13
    //   302: astore #10
    //   304: aload #11
    //   306: ldc_w 'Process: '
    //   309: iconst_0
    //   310: invokevirtual startsWith : (Ljava/lang/String;I)Z
    //   313: istore #16
    //   315: aload #13
    //   317: astore #10
    //   319: iload #16
    //   321: ifeq -> 363
    //   324: aload #11
    //   326: aload #11
    //   328: ldc_w ':'
    //   331: invokevirtual indexOf : (Ljava/lang/String;)I
    //   334: iconst_1
    //   335: iadd
    //   336: invokevirtual substring : (I)Ljava/lang/String;
    //   339: invokevirtual trim : ()Ljava/lang/String;
    //   342: astore #10
    //   344: goto -> 363
    //   347: astore_0
    //   348: aload #14
    //   350: astore_1
    //   351: goto -> 1533
    //   354: astore_0
    //   355: goto -> 1467
    //   358: astore #12
    //   360: goto -> 770
    //   363: aload #8
    //   365: astore #13
    //   367: aload #8
    //   369: astore #12
    //   371: aload #8
    //   373: astore #12
    //   375: aload #8
    //   377: astore #12
    //   379: aload #11
    //   381: ldc_w 'PID: '
    //   384: iconst_0
    //   385: invokevirtual startsWith : (Ljava/lang/String;I)Z
    //   388: ifeq -> 423
    //   391: aload #8
    //   393: astore #12
    //   395: aload #8
    //   397: astore #12
    //   399: aload #8
    //   401: astore #12
    //   403: aload #11
    //   405: aload #11
    //   407: ldc_w ':'
    //   410: invokevirtual indexOf : (Ljava/lang/String;)I
    //   413: iconst_1
    //   414: iadd
    //   415: invokevirtual substring : (I)Ljava/lang/String;
    //   418: invokevirtual trim : ()Ljava/lang/String;
    //   421: astore #13
    //   423: aload #13
    //   425: astore #12
    //   427: aload #13
    //   429: astore #12
    //   431: aload #13
    //   433: astore #12
    //   435: aload #11
    //   437: ldc_w 'Package: '
    //   440: iconst_0
    //   441: invokevirtual startsWith : (Ljava/lang/String;I)Z
    //   444: ifeq -> 632
    //   447: aload #13
    //   449: astore #12
    //   451: aload #13
    //   453: astore #12
    //   455: aload #13
    //   457: astore #12
    //   459: aload #11
    //   461: aload #11
    //   463: ldc_w ':'
    //   466: invokevirtual indexOf : (Ljava/lang/String;)I
    //   469: iconst_2
    //   470: iadd
    //   471: invokevirtual substring : (I)Ljava/lang/String;
    //   474: astore #8
    //   476: aload #13
    //   478: astore #12
    //   480: aload #13
    //   482: astore #12
    //   484: aload #13
    //   486: astore #12
    //   488: aload #8
    //   490: ldc_w ' '
    //   493: invokevirtual indexOf : (Ljava/lang/String;)I
    //   496: istore #19
    //   498: iload #19
    //   500: ifle -> 534
    //   503: aload #13
    //   505: astore #12
    //   507: aload #13
    //   509: astore #12
    //   511: aload #13
    //   513: astore #12
    //   515: aload #8
    //   517: iconst_0
    //   518: iload #19
    //   520: invokevirtual substring : (II)Ljava/lang/String;
    //   523: invokevirtual trim : ()Ljava/lang/String;
    //   526: astore #8
    //   528: aload #8
    //   530: astore_1
    //   531: goto -> 632
    //   534: aload #13
    //   536: astore #12
    //   538: aload #13
    //   540: astore #12
    //   542: aload #13
    //   544: astore #12
    //   546: new java/lang/StringBuilder
    //   549: astore #8
    //   551: aload #13
    //   553: astore #12
    //   555: aload #13
    //   557: astore #12
    //   559: aload #13
    //   561: astore #12
    //   563: aload #8
    //   565: invokespecial <init> : ()V
    //   568: aload #13
    //   570: astore #12
    //   572: aload #13
    //   574: astore #12
    //   576: aload #13
    //   578: astore #12
    //   580: aload #8
    //   582: ldc_w 'pacakge line = '
    //   585: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   588: pop
    //   589: aload #13
    //   591: astore #12
    //   593: aload #13
    //   595: astore #12
    //   597: aload #13
    //   599: astore #12
    //   601: aload #8
    //   603: aload #11
    //   605: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   608: pop
    //   609: aload #13
    //   611: astore #12
    //   613: aload #13
    //   615: astore #12
    //   617: aload #13
    //   619: astore #12
    //   621: ldc 'java.lang.ASSERT'
    //   623: aload #8
    //   625: invokevirtual toString : ()Ljava/lang/String;
    //   628: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   631: pop
    //   632: aload #5
    //   634: aload #11
    //   636: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   639: pop
    //   640: aload #5
    //   642: ldc_w '\\r\\n'
    //   645: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   648: pop
    //   649: aload #11
    //   651: invokevirtual length : ()I
    //   654: istore #19
    //   656: iload #18
    //   658: iload #19
    //   660: iadd
    //   661: istore #18
    //   663: aload #13
    //   665: astore #12
    //   667: aload #10
    //   669: astore #13
    //   671: goto -> 242
    //   674: astore_0
    //   675: aload #14
    //   677: astore_1
    //   678: goto -> 1533
    //   681: astore_0
    //   682: goto -> 1467
    //   685: astore #12
    //   687: aload #13
    //   689: astore #8
    //   691: aload #10
    //   693: astore #13
    //   695: goto -> 770
    //   698: astore_0
    //   699: aload #14
    //   701: astore_1
    //   702: goto -> 1533
    //   705: astore_0
    //   706: goto -> 1467
    //   709: astore #13
    //   711: aload #12
    //   713: astore #8
    //   715: aload #13
    //   717: astore #12
    //   719: aload #10
    //   721: astore #13
    //   723: goto -> 770
    //   726: astore_0
    //   727: aload #14
    //   729: astore_1
    //   730: goto -> 1533
    //   733: astore_0
    //   734: goto -> 1467
    //   737: astore #12
    //   739: aload #10
    //   741: astore #13
    //   743: goto -> 770
    //   746: goto -> 802
    //   749: astore_0
    //   750: aload #14
    //   752: astore_1
    //   753: goto -> 1533
    //   756: astore_0
    //   757: goto -> 1467
    //   760: astore #10
    //   762: aload #12
    //   764: astore #8
    //   766: aload #10
    //   768: astore #12
    //   770: ldc 'java.lang.ASSERT'
    //   772: ldc_w 'epitaph failed.'
    //   775: aload #12
    //   777: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   780: pop
    //   781: aload_1
    //   782: astore #12
    //   784: aload #8
    //   786: astore #10
    //   788: goto -> 809
    //   791: astore_0
    //   792: aload #14
    //   794: astore_1
    //   795: goto -> 1533
    //   798: astore_0
    //   799: goto -> 1467
    //   802: aload #12
    //   804: astore #10
    //   806: aload_1
    //   807: astore #12
    //   809: iload #18
    //   811: ifne -> 852
    //   814: aload #14
    //   816: invokevirtual close : ()V
    //   819: goto -> 832
    //   822: astore_0
    //   823: ldc 'java.lang.ASSERT'
    //   825: ldc_w 'finally close is failed.'
    //   828: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   831: pop
    //   832: aload #7
    //   834: invokevirtual close : ()V
    //   837: goto -> 850
    //   840: astore_0
    //   841: ldc 'java.lang.ASSERT'
    //   843: ldc_w 'finally close br failed.'
    //   846: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   849: pop
    //   850: iconst_0
    //   851: ireturn
    //   852: aload #14
    //   854: invokevirtual close : ()V
    //   857: goto -> 870
    //   860: astore_1
    //   861: ldc 'java.lang.ASSERT'
    //   863: ldc_w 'finally close is failed.'
    //   866: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   869: pop
    //   870: aload #7
    //   872: invokevirtual close : ()V
    //   875: goto -> 888
    //   878: astore_1
    //   879: ldc 'java.lang.ASSERT'
    //   881: ldc_w 'finally close br failed.'
    //   884: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   887: pop
    //   888: aload #13
    //   890: ifnonnull -> 900
    //   893: ldc 'NONE'
    //   895: astore #14
    //   897: goto -> 904
    //   900: aload #13
    //   902: astore #14
    //   904: invokestatic getInstance : ()Landroid/os/OplusAssertTip;
    //   907: putstatic com/oplus/debug/ASSERT.mFunctionProxy : Landroid/os/OplusAssertTip;
    //   910: aconst_null
    //   911: astore #7
    //   913: aconst_null
    //   914: astore #8
    //   916: iconst_m1
    //   917: istore #17
    //   919: aload #8
    //   921: astore #13
    //   923: aload #7
    //   925: astore_1
    //   926: new java/io/FileInputStream
    //   929: astore #11
    //   931: aload #8
    //   933: astore #13
    //   935: aload #7
    //   937: astore_1
    //   938: aload #11
    //   940: aload_0
    //   941: invokespecial <init> : (Ljava/io/File;)V
    //   944: aload #11
    //   946: astore #8
    //   948: aload #8
    //   950: astore_0
    //   951: iload_2
    //   952: iconst_4
    //   953: iand
    //   954: ifeq -> 981
    //   957: aload #8
    //   959: astore #13
    //   961: aload #8
    //   963: astore_1
    //   964: new java/util/zip/GZIPInputStream
    //   967: astore_0
    //   968: aload #8
    //   970: astore #13
    //   972: aload #8
    //   974: astore_1
    //   975: aload_0
    //   976: aload #8
    //   978: invokespecial <init> : (Ljava/io/InputStream;)V
    //   981: aload_0
    //   982: astore #13
    //   984: aload_0
    //   985: astore_1
    //   986: aload #14
    //   988: bipush #58
    //   990: bipush #95
    //   992: invokevirtual replace : (CC)Ljava/lang/String;
    //   995: astore #8
    //   997: aload_0
    //   998: astore #13
    //   1000: aload_0
    //   1001: astore_1
    //   1002: new java/lang/StringBuilder
    //   1005: astore #14
    //   1007: aload_0
    //   1008: astore #13
    //   1010: aload_0
    //   1011: astore_1
    //   1012: aload #14
    //   1014: invokespecial <init> : ()V
    //   1017: aload_0
    //   1018: astore #13
    //   1020: aload_0
    //   1021: astore_1
    //   1022: aload #14
    //   1024: ldc_w 'after replace ':' with '_' ,the ProcessName is '
    //   1027: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1030: pop
    //   1031: aload_0
    //   1032: astore #13
    //   1034: aload_0
    //   1035: astore_1
    //   1036: aload #14
    //   1038: aload #8
    //   1040: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1043: pop
    //   1044: aload_0
    //   1045: astore #13
    //   1047: aload_0
    //   1048: astore_1
    //   1049: ldc 'java.lang.ASSERT'
    //   1051: aload #14
    //   1053: invokevirtual toString : ()Ljava/lang/String;
    //   1056: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   1059: pop
    //   1060: aload_0
    //   1061: astore #13
    //   1063: aload_0
    //   1064: astore_1
    //   1065: new java/lang/StringBuilder
    //   1068: astore #14
    //   1070: aload_0
    //   1071: astore #13
    //   1073: aload_0
    //   1074: astore_1
    //   1075: aload #14
    //   1077: invokespecial <init> : ()V
    //   1080: aload_0
    //   1081: astore #13
    //   1083: aload_0
    //   1084: astore_1
    //   1085: aload #14
    //   1087: aload #8
    //   1089: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1092: pop
    //   1093: aload_0
    //   1094: astore #13
    //   1096: aload_0
    //   1097: astore_1
    //   1098: aload #14
    //   1100: ldc '-'
    //   1102: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1105: pop
    //   1106: aload_0
    //   1107: astore #13
    //   1109: aload_0
    //   1110: astore_1
    //   1111: aload #14
    //   1113: aload #10
    //   1115: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1118: pop
    //   1119: aload_0
    //   1120: astore #13
    //   1122: aload_0
    //   1123: astore_1
    //   1124: aload_0
    //   1125: aload #14
    //   1127: invokevirtual toString : ()Ljava/lang/String;
    //   1130: invokestatic copyAssert : (Ljava/io/InputStream;Ljava/lang/String;)Z
    //   1133: pop
    //   1134: aload_0
    //   1135: astore #13
    //   1137: aload_0
    //   1138: astore_1
    //   1139: aload_0
    //   1140: invokevirtual close : ()V
    //   1143: aconst_null
    //   1144: astore_0
    //   1145: aconst_null
    //   1146: astore #8
    //   1148: aconst_null
    //   1149: astore #14
    //   1151: aconst_null
    //   1152: astore #7
    //   1154: aload #7
    //   1156: astore #13
    //   1158: aload_0
    //   1159: astore_1
    //   1160: aload #5
    //   1162: invokevirtual toString : ()Ljava/lang/String;
    //   1165: astore #10
    //   1167: aload #7
    //   1169: astore #13
    //   1171: aload_0
    //   1172: astore_1
    //   1173: ldc 'persist.sys.assert.state'
    //   1175: ldc 'true'
    //   1177: invokestatic get : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   1180: ldc 'false'
    //   1182: invokevirtual equals : (Ljava/lang/Object;)Z
    //   1185: ifeq -> 1209
    //   1188: aload #7
    //   1190: astore #13
    //   1192: aload_0
    //   1193: astore_1
    //   1194: aload #5
    //   1196: ldc_w 'assert state is close'
    //   1199: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1202: pop
    //   1203: iload #17
    //   1205: istore_2
    //   1206: goto -> 1317
    //   1209: aload #14
    //   1211: astore_0
    //   1212: aload_3
    //   1213: aload #12
    //   1215: invokestatic getAppName : (Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    //   1218: astore_1
    //   1219: aload_1
    //   1220: ifnull -> 1291
    //   1223: aload #14
    //   1225: astore_0
    //   1226: aload_1
    //   1227: invokevirtual isEmpty : ()Z
    //   1230: ifeq -> 1236
    //   1233: goto -> 1291
    //   1236: aload #14
    //   1238: astore_0
    //   1239: new java/lang/StringBuilder
    //   1242: astore_3
    //   1243: aload #14
    //   1245: astore_0
    //   1246: aload_3
    //   1247: invokespecial <init> : ()V
    //   1250: aload #14
    //   1252: astore_0
    //   1253: aload_3
    //   1254: aload_1
    //   1255: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1258: pop
    //   1259: aload #14
    //   1261: astore_0
    //   1262: aload_3
    //   1263: ldc_w '\\n'
    //   1266: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1269: pop
    //   1270: aload #14
    //   1272: astore_0
    //   1273: aload_3
    //   1274: aload #10
    //   1276: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1279: pop
    //   1280: aload #14
    //   1282: astore_0
    //   1283: aload_3
    //   1284: invokevirtual toString : ()Ljava/lang/String;
    //   1287: astore_1
    //   1288: goto -> 1306
    //   1291: aload #14
    //   1293: astore_0
    //   1294: ldc 'java.lang.ASSERT'
    //   1296: ldc_w 'can not get the app name'
    //   1299: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   1302: pop
    //   1303: aload #10
    //   1305: astore_1
    //   1306: aload #14
    //   1308: astore_0
    //   1309: getstatic com/oplus/debug/ASSERT.mFunctionProxy : Landroid/os/OplusAssertTip;
    //   1312: aload_1
    //   1313: invokevirtual requestShowAssertMessage : (Ljava/lang/String;)I
    //   1316: istore_2
    //   1317: iconst_0
    //   1318: ifeq -> 1345
    //   1321: new java/lang/NullPointerException
    //   1324: dup
    //   1325: invokespecial <init> : ()V
    //   1328: athrow
    //   1329: goto -> 1345
    //   1332: astore_0
    //   1333: ldc 'java.lang.ASSERT'
    //   1335: ldc_w 'finally close isForCopyAssert failed.'
    //   1338: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1341: pop
    //   1342: goto -> 1329
    //   1345: iconst_m1
    //   1346: iload_2
    //   1347: if_icmpeq -> 1356
    //   1350: iconst_1
    //   1351: istore #16
    //   1353: goto -> 1359
    //   1356: iconst_0
    //   1357: istore #16
    //   1359: iload #16
    //   1361: ireturn
    //   1362: astore_3
    //   1363: aload #8
    //   1365: astore_1
    //   1366: goto -> 1377
    //   1369: astore_1
    //   1370: aload #13
    //   1372: astore_0
    //   1373: goto -> 1416
    //   1376: astore_3
    //   1377: aload_1
    //   1378: astore_0
    //   1379: ldc 'java.lang.ASSERT'
    //   1381: ldc_w 'epitaph failed.'
    //   1384: aload_3
    //   1385: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   1388: pop
    //   1389: aload_1
    //   1390: ifnull -> 1410
    //   1393: aload_1
    //   1394: invokevirtual close : ()V
    //   1397: goto -> 1410
    //   1400: astore_0
    //   1401: ldc 'java.lang.ASSERT'
    //   1403: ldc_w 'finally close isForCopyAssert failed.'
    //   1406: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1409: pop
    //   1410: iconst_0
    //   1411: ireturn
    //   1412: astore_1
    //   1413: goto -> 1373
    //   1416: aload_0
    //   1417: ifnull -> 1437
    //   1420: aload_0
    //   1421: invokevirtual close : ()V
    //   1424: goto -> 1437
    //   1427: astore_0
    //   1428: ldc 'java.lang.ASSERT'
    //   1430: ldc_w 'finally close isForCopyAssert failed.'
    //   1433: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1436: pop
    //   1437: aload_1
    //   1438: athrow
    //   1439: astore_0
    //   1440: aload #8
    //   1442: astore #7
    //   1444: goto -> 1533
    //   1447: astore_0
    //   1448: aload #13
    //   1450: astore #14
    //   1452: goto -> 1467
    //   1455: astore_0
    //   1456: aload #9
    //   1458: astore #7
    //   1460: aload #13
    //   1462: astore_1
    //   1463: goto -> 1533
    //   1466: astore_0
    //   1467: aload_0
    //   1468: invokevirtual printStackTrace : ()V
    //   1471: aload #14
    //   1473: ifnull -> 1497
    //   1476: aload #14
    //   1478: invokevirtual close : ()V
    //   1481: goto -> 1497
    //   1484: astore_0
    //   1485: ldc 'java.lang.ASSERT'
    //   1487: ldc_w 'finally close is failed.'
    //   1490: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1493: pop
    //   1494: goto -> 1497
    //   1497: aload #7
    //   1499: ifnull -> 1523
    //   1502: aload #7
    //   1504: invokevirtual close : ()V
    //   1507: goto -> 1523
    //   1510: astore_0
    //   1511: ldc 'java.lang.ASSERT'
    //   1513: ldc_w 'finally close br failed.'
    //   1516: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1519: pop
    //   1520: goto -> 1523
    //   1523: iconst_0
    //   1524: ireturn
    //   1525: astore_0
    //   1526: aload #14
    //   1528: astore #13
    //   1530: goto -> 1460
    //   1533: aload_1
    //   1534: ifnull -> 1557
    //   1537: aload_1
    //   1538: invokevirtual close : ()V
    //   1541: goto -> 1557
    //   1544: astore_1
    //   1545: ldc 'java.lang.ASSERT'
    //   1547: ldc_w 'finally close is failed.'
    //   1550: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1553: pop
    //   1554: goto -> 1557
    //   1557: aload #7
    //   1559: ifnull -> 1583
    //   1562: aload #7
    //   1564: invokevirtual close : ()V
    //   1567: goto -> 1583
    //   1570: astore_1
    //   1571: ldc 'java.lang.ASSERT'
    //   1573: ldc_w 'finally close br failed.'
    //   1576: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1579: pop
    //   1580: goto -> 1583
    //   1583: aload_0
    //   1584: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #61	-> 0
    //   #62	-> 4
    //   #66	-> 6
    //   #67	-> 17
    //   #68	-> 26
    //   #69	-> 29
    //   #71	-> 32
    //   #72	-> 36
    //   #73	-> 36
    //   #76	-> 36
    //   #77	-> 52
    //   #78	-> 73
    //   #80	-> 81
    //   #82	-> 86
    //   #83	-> 112
    //   #84	-> 140
    //   #85	-> 149
    //   #87	-> 176
    //   #89	-> 232
    //   #91	-> 242
    //   #92	-> 261
    //   #93	-> 289
    //   #95	-> 292
    //   #96	-> 324
    //   #127	-> 347
    //   #123	-> 354
    //   #115	-> 358
    //   #99	-> 363
    //   #100	-> 391
    //   #102	-> 423
    //   #103	-> 447
    //   #104	-> 476
    //   #105	-> 498
    //   #106	-> 503
    //   #108	-> 534
    //   #111	-> 632
    //   #112	-> 640
    //   #113	-> 649
    //   #127	-> 674
    //   #123	-> 681
    //   #115	-> 685
    //   #127	-> 698
    //   #123	-> 705
    //   #115	-> 709
    //   #127	-> 726
    //   #123	-> 733
    //   #115	-> 737
    //   #91	-> 746
    //   #127	-> 749
    //   #123	-> 756
    //   #115	-> 760
    //   #116	-> 770
    //   #127	-> 791
    //   #123	-> 798
    //   #91	-> 802
    //   #117	-> 802
    //   #119	-> 809
    //   #121	-> 814
    //   #128	-> 814
    //   #129	-> 814
    //   #133	-> 819
    //   #131	-> 822
    //   #132	-> 823
    //   #135	-> 832
    //   #136	-> 832
    //   #140	-> 837
    //   #138	-> 840
    //   #139	-> 841
    //   #121	-> 850
    //   #128	-> 852
    //   #129	-> 852
    //   #133	-> 857
    //   #131	-> 860
    //   #132	-> 861
    //   #135	-> 870
    //   #136	-> 870
    //   #140	-> 875
    //   #138	-> 878
    //   #139	-> 879
    //   #141	-> 888
    //   #143	-> 888
    //   #144	-> 893
    //   #143	-> 900
    //   #146	-> 904
    //   #147	-> 910
    //   #148	-> 916
    //   #150	-> 919
    //   #151	-> 948
    //   #152	-> 957
    //   #155	-> 981
    //   #156	-> 997
    //   #157	-> 997
    //   #158	-> 1060
    //   #159	-> 1134
    //   #160	-> 1143
    //   #162	-> 1154
    //   #163	-> 1167
    //   #164	-> 1188
    //   #166	-> 1209
    //   #167	-> 1219
    //   #170	-> 1236
    //   #168	-> 1291
    //   #172	-> 1306
    //   #178	-> 1317
    //   #180	-> 1321
    //   #183	-> 1329
    //   #181	-> 1332
    //   #182	-> 1333
    //   #186	-> 1345
    //   #174	-> 1362
    //   #178	-> 1369
    //   #174	-> 1376
    //   #175	-> 1377
    //   #176	-> 1389
    //   #178	-> 1389
    //   #180	-> 1393
    //   #183	-> 1397
    //   #181	-> 1400
    //   #182	-> 1401
    //   #176	-> 1410
    //   #178	-> 1412
    //   #180	-> 1420
    //   #183	-> 1424
    //   #181	-> 1427
    //   #182	-> 1428
    //   #185	-> 1437
    //   #127	-> 1439
    //   #123	-> 1447
    //   #127	-> 1455
    //   #123	-> 1466
    //   #124	-> 1467
    //   #125	-> 1471
    //   #128	-> 1471
    //   #129	-> 1476
    //   #131	-> 1484
    //   #132	-> 1485
    //   #133	-> 1497
    //   #135	-> 1497
    //   #136	-> 1502
    //   #138	-> 1510
    //   #139	-> 1511
    //   #140	-> 1523
    //   #125	-> 1523
    //   #127	-> 1525
    //   #128	-> 1533
    //   #129	-> 1537
    //   #131	-> 1544
    //   #132	-> 1545
    //   #133	-> 1557
    //   #135	-> 1557
    //   #136	-> 1562
    //   #138	-> 1570
    //   #139	-> 1571
    //   #140	-> 1583
    //   #141	-> 1583
    // Exception table:
    //   from	to	target	type
    //   43	52	1466	java/io/IOException
    //   43	52	1455	finally
    //   59	68	1466	java/io/IOException
    //   59	68	1455	finally
    //   93	98	1447	java/io/IOException
    //   93	98	1439	finally
    //   105	112	1447	java/io/IOException
    //   105	112	1439	finally
    //   119	124	1447	java/io/IOException
    //   119	124	1439	finally
    //   131	137	1447	java/io/IOException
    //   131	137	1439	finally
    //   155	160	1466	java/io/IOException
    //   155	160	1455	finally
    //   166	172	1466	java/io/IOException
    //   166	172	1455	finally
    //   183	188	1447	java/io/IOException
    //   183	188	1439	finally
    //   195	200	1447	java/io/IOException
    //   195	200	1439	finally
    //   207	214	1447	java/io/IOException
    //   207	214	1439	finally
    //   221	228	1447	java/io/IOException
    //   221	228	1439	finally
    //   249	256	760	java/lang/Exception
    //   249	256	756	java/io/IOException
    //   249	256	749	finally
    //   277	289	737	java/lang/Exception
    //   277	289	733	java/io/IOException
    //   277	289	726	finally
    //   304	315	737	java/lang/Exception
    //   304	315	733	java/io/IOException
    //   304	315	726	finally
    //   324	344	358	java/lang/Exception
    //   324	344	354	java/io/IOException
    //   324	344	347	finally
    //   379	391	709	java/lang/Exception
    //   379	391	705	java/io/IOException
    //   379	391	698	finally
    //   403	423	709	java/lang/Exception
    //   403	423	705	java/io/IOException
    //   403	423	698	finally
    //   435	447	709	java/lang/Exception
    //   435	447	705	java/io/IOException
    //   435	447	698	finally
    //   459	476	709	java/lang/Exception
    //   459	476	705	java/io/IOException
    //   459	476	698	finally
    //   488	498	709	java/lang/Exception
    //   488	498	705	java/io/IOException
    //   488	498	698	finally
    //   515	528	709	java/lang/Exception
    //   515	528	705	java/io/IOException
    //   515	528	698	finally
    //   546	551	709	java/lang/Exception
    //   546	551	705	java/io/IOException
    //   546	551	698	finally
    //   563	568	709	java/lang/Exception
    //   563	568	705	java/io/IOException
    //   563	568	698	finally
    //   580	589	709	java/lang/Exception
    //   580	589	705	java/io/IOException
    //   580	589	698	finally
    //   601	609	709	java/lang/Exception
    //   601	609	705	java/io/IOException
    //   601	609	698	finally
    //   621	632	709	java/lang/Exception
    //   621	632	705	java/io/IOException
    //   621	632	698	finally
    //   632	640	685	java/lang/Exception
    //   632	640	681	java/io/IOException
    //   632	640	674	finally
    //   640	649	685	java/lang/Exception
    //   640	649	681	java/io/IOException
    //   640	649	674	finally
    //   649	656	685	java/lang/Exception
    //   649	656	681	java/io/IOException
    //   649	656	674	finally
    //   770	781	798	java/io/IOException
    //   770	781	791	finally
    //   814	819	822	java/lang/Exception
    //   832	837	840	java/lang/Exception
    //   852	857	860	java/lang/Exception
    //   870	875	878	java/lang/Exception
    //   926	931	1376	java/lang/Exception
    //   926	931	1369	finally
    //   938	944	1376	java/lang/Exception
    //   938	944	1369	finally
    //   964	968	1376	java/lang/Exception
    //   964	968	1369	finally
    //   975	981	1376	java/lang/Exception
    //   975	981	1369	finally
    //   986	997	1376	java/lang/Exception
    //   986	997	1369	finally
    //   1002	1007	1376	java/lang/Exception
    //   1002	1007	1369	finally
    //   1012	1017	1376	java/lang/Exception
    //   1012	1017	1369	finally
    //   1022	1031	1376	java/lang/Exception
    //   1022	1031	1369	finally
    //   1036	1044	1376	java/lang/Exception
    //   1036	1044	1369	finally
    //   1049	1060	1376	java/lang/Exception
    //   1049	1060	1369	finally
    //   1065	1070	1376	java/lang/Exception
    //   1065	1070	1369	finally
    //   1075	1080	1376	java/lang/Exception
    //   1075	1080	1369	finally
    //   1085	1093	1376	java/lang/Exception
    //   1085	1093	1369	finally
    //   1098	1106	1376	java/lang/Exception
    //   1098	1106	1369	finally
    //   1111	1119	1376	java/lang/Exception
    //   1111	1119	1369	finally
    //   1124	1134	1376	java/lang/Exception
    //   1124	1134	1369	finally
    //   1139	1143	1376	java/lang/Exception
    //   1139	1143	1369	finally
    //   1160	1167	1376	java/lang/Exception
    //   1160	1167	1369	finally
    //   1173	1188	1376	java/lang/Exception
    //   1173	1188	1369	finally
    //   1194	1203	1376	java/lang/Exception
    //   1194	1203	1369	finally
    //   1212	1219	1362	java/lang/Exception
    //   1212	1219	1412	finally
    //   1226	1233	1362	java/lang/Exception
    //   1226	1233	1412	finally
    //   1239	1243	1362	java/lang/Exception
    //   1239	1243	1412	finally
    //   1246	1250	1362	java/lang/Exception
    //   1246	1250	1412	finally
    //   1253	1259	1362	java/lang/Exception
    //   1253	1259	1412	finally
    //   1262	1270	1362	java/lang/Exception
    //   1262	1270	1412	finally
    //   1273	1280	1362	java/lang/Exception
    //   1273	1280	1412	finally
    //   1283	1288	1362	java/lang/Exception
    //   1283	1288	1412	finally
    //   1294	1303	1362	java/lang/Exception
    //   1294	1303	1412	finally
    //   1309	1317	1362	java/lang/Exception
    //   1309	1317	1412	finally
    //   1321	1329	1332	java/lang/Exception
    //   1379	1389	1412	finally
    //   1393	1397	1400	java/lang/Exception
    //   1420	1424	1427	java/lang/Exception
    //   1467	1471	1525	finally
    //   1476	1481	1484	java/lang/Exception
    //   1502	1507	1510	java/lang/Exception
    //   1537	1541	1544	java/lang/Exception
    //   1562	1567	1570	java/lang/Exception
  }
  
  public static void CopyTombstone(String paramString) {
    Log.v("java.lang.ASSERT", "in copyTombstone");
    if (SystemProperties.get("persist.sys.assert.panic", "false").equals("true") || 
      SystemProperties.get("persist.sys.assert.panic.camera", "false").equals("true")) {
      SystemProperties.set("sys.tombstone.file", paramString);
      SystemProperties.set("ctl.start", "tranfer_tomb");
    } 
  }
  
  public static boolean copyAssert(InputStream paramInputStream, String paramString) {
    if (SystemProperties.get("persist.sys.assert.panic", "false").equals("true") || 
      SystemProperties.get("persist.sys.assert.panic.camera", "false").equals("true")) {
      Date date = new Date();
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
      String str = simpleDateFormat.format(date);
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append(paramString);
      stringBuilder2.append("-");
      stringBuilder2.append(str);
      stringBuilder2.append(".txt");
      str = stringBuilder2.toString();
      paramString = SystemProperties.get("sys.oppo.logkit.assertlog", "");
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append(paramString);
      stringBuilder2.append("/");
      stringBuilder2.append(str);
      File file = new File(stringBuilder2.toString());
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("copyAssert destFile=");
      stringBuilder1.append(file);
      Log.d("java.lang.ASSERT", stringBuilder1.toString());
      try {
        if (file.exists())
          file.delete(); 
        FileOutputStream fileOutputStream = new FileOutputStream();
        this(file);
        try {
          byte[] arrayOfByte = new byte[4096];
          while (true) {
            int i = paramInputStream.read(arrayOfByte);
            if (i >= 0) {
              fileOutputStream.write(arrayOfByte, 0, i);
              fileOutputStream.flush();
              continue;
            } 
            break;
          } 
          return true;
        } finally {
          fileOutputStream.close();
        } 
      } catch (IOException iOException) {
        iOException.printStackTrace();
        return false;
      } 
    } 
    return true;
  }
  
  public static void copyAnr(String paramString1, String paramString2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("copyAnr filePath = ");
    stringBuilder.append(paramString1);
    Log.v("java.lang.ASSERT", stringBuilder.toString());
    if (SystemProperties.get("persist.sys.assert.panic", "false").equals("true") || 
      SystemProperties.get("persist.sys.assert.panic.camera", "false").equals("true")) {
      SystemProperties.set("sys.anr.srcfile", paramString1);
      SystemProperties.set("sys.anr.destfile", paramString2);
      SystemProperties.set("ctl.start", "tranfer_anr");
      copyBinderInfo();
    } 
  }
  
  private static String getAppName(Context paramContext, String paramString) {
    if (paramString == null) {
      Log.v("java.lang.ASSERT", "can not get the pacakge");
      return null;
    } 
    PackageManager packageManager = paramContext.getPackageManager();
    try {
      ApplicationInfo applicationInfo = packageManager.getApplicationInfo(paramString, 0);
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append(applicationInfo.loadLabel(packageManager).toString());
      stringBuilder.append(" ");
      return stringBuilder.toString();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getAppName e = ");
      stringBuilder.append(exception);
      Log.v("java.lang.ASSERT", stringBuilder.toString());
      return null;
    } 
  }
  
  private static String getTimeStamp() {
    Date date = new Date();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
    return simpleDateFormat.format(date);
  }
  
  public static void copyBinderInfo() {
    SystemProperties.set("ctl.start", "copybinderinfo");
  }
  
  private static void binderStateRead() {
    try {
      Log.i("java.lang.ASSERT", "Collecting Binder Transaction Status Information");
      BufferedReader bufferedReader = new BufferedReader();
      FileReader fileReader = new FileReader();
      this("/sys/kernel/debug/binder/state");
      this(fileReader);
      FileWriter fileWriter = new FileWriter();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("/data/oppo_log/anr_binder_info/binder_info_");
      stringBuilder.append(getTimeStamp());
      stringBuilder.append(".txt");
      this(stringBuilder.toString());
      while (true) {
        String str = bufferedReader.readLine();
        if (str != null) {
          fileWriter.write(str);
          fileWriter.write(10);
          continue;
        } 
        break;
      } 
      bufferedReader.close();
      fileWriter.close();
    } catch (IOException iOException) {
      Log.w("java.lang.ASSERT", "Failed to collect binder state file", iOException);
    } 
  }
  
  private static void doZip(String paramString1, String paramString2) throws IOException {
    ZipOutputStream zipOutputStream1 = null, zipOutputStream2 = null;
    ZipOutputStream zipOutputStream3 = zipOutputStream2, zipOutputStream4 = zipOutputStream1;
    try {
      File file2 = new File();
      zipOutputStream3 = zipOutputStream2;
      zipOutputStream4 = zipOutputStream1;
      this(paramString2);
      zipOutputStream3 = zipOutputStream2;
      zipOutputStream4 = zipOutputStream1;
      File file1 = new File();
      zipOutputStream3 = zipOutputStream2;
      zipOutputStream4 = zipOutputStream1;
      this(paramString1);
      zipOutputStream3 = zipOutputStream2;
      zipOutputStream4 = zipOutputStream1;
      boolean bool = file1.exists();
      if (!bool) {
        if (false)
          try {
            throw new NullPointerException();
          } catch (IOException iOException) {
            iOException.printStackTrace();
          }  
        return;
      } 
      zipOutputStream3 = zipOutputStream2;
      zipOutputStream4 = zipOutputStream1;
      ZipOutputStream zipOutputStream = new ZipOutputStream();
      zipOutputStream3 = zipOutputStream2;
      zipOutputStream4 = zipOutputStream1;
      FileOutputStream fileOutputStream = new FileOutputStream();
      zipOutputStream3 = zipOutputStream2;
      zipOutputStream4 = zipOutputStream1;
      this(file2);
      zipOutputStream3 = zipOutputStream2;
      zipOutputStream4 = zipOutputStream1;
      this(fileOutputStream);
      zipOutputStream3 = zipOutputStream;
      zipOutputStream4 = zipOutputStream;
      bool = file1.isFile();
      if (bool) {
        zipOutputStream3 = zipOutputStream;
        zipOutputStream4 = zipOutputStream;
        zipFileOrDirectory(zipOutputStream, file1, "");
      } else {
        zipOutputStream3 = zipOutputStream;
        zipOutputStream4 = zipOutputStream;
        File[] arrayOfFile = file1.listFiles();
        byte b = 0;
        while (true) {
          zipOutputStream3 = zipOutputStream;
          zipOutputStream4 = zipOutputStream;
          if (b < arrayOfFile.length) {
            zipOutputStream3 = zipOutputStream;
            zipOutputStream4 = zipOutputStream;
            zipFileOrDirectory(zipOutputStream, arrayOfFile[b], "");
            b++;
            continue;
          } 
          break;
        } 
      } 
      try {
        zipOutputStream.close();
      } catch (IOException iOException) {
        iOException.printStackTrace();
      } 
    } catch (Exception exception) {
      zipOutputStream3 = zipOutputStream4;
      exception.printStackTrace();
      if (zipOutputStream4 != null)
        zipOutputStream4.close(); 
    } finally {}
  }
  
  private static void zipFileOrDirectory(ZipOutputStream paramZipOutputStream, File paramFile, String paramString) throws IOException {
    StringBuilder stringBuilder;
    FileInputStream fileInputStream1 = null, fileInputStream2 = null;
    byte[] arrayOfByte = null;
    FileInputStream fileInputStream3 = fileInputStream1, fileInputStream4 = fileInputStream2;
    try {
      ZipEntry zipEntry;
      StringBuilder stringBuilder1;
      StringBuilder stringBuilder2;
      if (!paramFile.isDirectory()) {
        fileInputStream3 = fileInputStream1;
        fileInputStream4 = fileInputStream2;
        arrayOfByte = new byte[4096];
        fileInputStream3 = fileInputStream1;
        fileInputStream4 = fileInputStream2;
        FileInputStream fileInputStream = new FileInputStream();
        fileInputStream3 = fileInputStream1;
        fileInputStream4 = fileInputStream2;
        this(paramFile);
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        zipEntry = new ZipEntry();
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        stringBuilder1 = new StringBuilder();
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        this();
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        stringBuilder1.append(paramString);
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        stringBuilder1.append(paramFile.getName());
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        this(stringBuilder1.toString());
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        paramZipOutputStream.putNextEntry(zipEntry);
        while (true) {
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          int i = fileInputStream.read(arrayOfByte);
          if (i != -1) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            paramZipOutputStream.write(arrayOfByte, 0, i);
            continue;
          } 
          break;
        } 
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        paramZipOutputStream.closeEntry();
      } else {
        ZipEntry zipEntry1 = zipEntry;
        stringBuilder = stringBuilder1;
        File[] arrayOfFile = paramFile.listFiles();
        byte b = 0;
        while (true) {
          byte[] arrayOfByte1 = arrayOfByte;
          zipEntry1 = zipEntry;
          stringBuilder = stringBuilder1;
          if (b < arrayOfFile.length) {
            File file = arrayOfFile[b];
            zipEntry1 = zipEntry;
            stringBuilder = stringBuilder1;
            stringBuilder2 = new StringBuilder();
            zipEntry1 = zipEntry;
            stringBuilder = stringBuilder1;
            this();
            zipEntry1 = zipEntry;
            stringBuilder = stringBuilder1;
            stringBuilder2.append(paramString);
            zipEntry1 = zipEntry;
            stringBuilder = stringBuilder1;
            stringBuilder2.append(paramFile.getName());
            zipEntry1 = zipEntry;
            stringBuilder = stringBuilder1;
            stringBuilder2.append("/");
            zipEntry1 = zipEntry;
            stringBuilder = stringBuilder1;
            zipFileOrDirectory(paramZipOutputStream, file, stringBuilder2.toString());
            b++;
            continue;
          } 
          break;
        } 
      } 
      if (stringBuilder2 != null)
        try {
          stringBuilder2.close();
        } catch (IOException iOException) {
          iOException.printStackTrace();
        }  
    } catch (IOException iOException) {
      StringBuilder stringBuilder1 = stringBuilder;
      iOException.printStackTrace();
      if (stringBuilder != null)
        stringBuilder.close(); 
    } finally {}
  }
}
