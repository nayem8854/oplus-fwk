package com.oplus.orms;

import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import com.oplus.orms.info.OrmsCtrlDataParam;
import com.oplus.orms.info.OrmsNotifyParam;
import com.oplus.orms.info.OrmsSaParam;

public class OplusResourceManager {
  private static boolean DEBUG = false;
  
  private static final int STACK_BEGIN = 4;
  
  private static final String TAG = "Orms_Manager";
  
  private static final int sDurationOffSet = 100;
  
  private static String sIdentity = "";
  
  private static final int sMaxVelocity = 24000;
  
  private static final int sMinDiffX = 150;
  
  private static final int sMinVelocity = 150;
  
  private static final int sMoveExtendSlop = 60;
  
  private static final int sMoveSlop = 40;
  
  private static final int sSlowSwipExtendTime = 200;
  
  private static final int sTimeOutZero = 0;
  
  private static final int sUnits = 1000;
  
  private static VelocityTracker sVelocityTracker = null;
  
  private long mRequest;
  
  private int mStartX;
  
  private int mStartY;
  
  protected OplusResourceManager(Class paramClass) {
    this.mStartX = 0;
    sIdentity = paramClass.getPackage().getName();
  }
  
  public static OplusResourceManager getInstance(Class paramClass) {
    // Byte code:
    //   0: ldc com/oplus/orms/OplusResourceManager
    //   2: monitorenter
    //   3: aload_0
    //   4: invokestatic verifyClazzName : (Ljava/lang/Class;)Z
    //   7: ifne -> 56
    //   10: new java/lang/StringBuilder
    //   13: astore_1
    //   14: aload_1
    //   15: invokespecial <init> : ()V
    //   18: aload_1
    //   19: ldc 'Class: '
    //   21: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   24: pop
    //   25: aload_1
    //   26: aload_0
    //   27: invokevirtual getName : ()Ljava/lang/String;
    //   30: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   33: pop
    //   34: aload_1
    //   35: ldc ' verify failed!'
    //   37: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   40: pop
    //   41: ldc 'Orms_Manager'
    //   43: aload_1
    //   44: invokevirtual toString : ()Ljava/lang/String;
    //   47: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   50: pop
    //   51: ldc com/oplus/orms/OplusResourceManager
    //   53: monitorexit
    //   54: aconst_null
    //   55: areturn
    //   56: invokestatic getInstance : ()Lcom/oplus/orms/OplusResourceManagerInner;
    //   59: pop
    //   60: aload_0
    //   61: invokevirtual getPackage : ()Ljava/lang/Package;
    //   64: invokevirtual getName : ()Ljava/lang/String;
    //   67: invokestatic checkAccessPermission : (Ljava/lang/String;)Z
    //   70: ifne -> 119
    //   73: new java/lang/StringBuilder
    //   76: astore_1
    //   77: aload_1
    //   78: invokespecial <init> : ()V
    //   81: aload_1
    //   82: ldc 'Class: '
    //   84: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   87: pop
    //   88: aload_1
    //   89: aload_0
    //   90: invokevirtual getName : ()Ljava/lang/String;
    //   93: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   96: pop
    //   97: aload_1
    //   98: ldc ' there is some exception that check permission failed!'
    //   100: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   103: pop
    //   104: ldc 'Orms_Manager'
    //   106: aload_1
    //   107: invokevirtual toString : ()Ljava/lang/String;
    //   110: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   113: pop
    //   114: ldc com/oplus/orms/OplusResourceManager
    //   116: monitorexit
    //   117: aconst_null
    //   118: areturn
    //   119: new com/oplus/orms/OplusResourceManager
    //   122: dup
    //   123: aload_0
    //   124: invokespecial <init> : (Ljava/lang/Class;)V
    //   127: astore_0
    //   128: ldc com/oplus/orms/OplusResourceManager
    //   130: monitorexit
    //   131: aload_0
    //   132: areturn
    //   133: astore_0
    //   134: ldc com/oplus/orms/OplusResourceManager
    //   136: monitorexit
    //   137: aload_0
    //   138: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #55	-> 3
    //   #56	-> 10
    //   #57	-> 51
    //   #60	-> 56
    //   #61	-> 73
    //   #62	-> 114
    //   #64	-> 119
    //   #54	-> 133
    // Exception table:
    //   from	to	target	type
    //   3	10	133	finally
    //   10	51	133	finally
    //   56	73	133	finally
    //   73	114	133	finally
    //   119	128	133	finally
  }
  
  private static boolean verifyClazzName(Class paramClass) {
    boolean bool2, bool1 = false;
    StackTraceElement[] arrayOfStackTraceElement = Thread.currentThread().getStackTrace();
    byte b = 4;
    while (true) {
      bool2 = bool1;
      if (b < arrayOfStackTraceElement.length) {
        if (arrayOfStackTraceElement[b].getClassName().contains(paramClass.getPackage().getName())) {
          bool2 = true;
          break;
        } 
        b++;
        continue;
      } 
      break;
    } 
    return bool2;
  }
  
  public long ormsSetSceneAction(OrmsSaParam paramOrmsSaParam) {
    if (paramOrmsSaParam == null || paramOrmsSaParam.scene == null || paramOrmsSaParam.action == null) {
      Log.e("Orms_Manager", "sceneAction is illegal! ");
      return -1L;
    } 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ormsSetSceneAction: ");
      stringBuilder.append(paramOrmsSaParam.toString());
      Log.i("Orms_Manager", stringBuilder.toString());
    } 
    return OplusResourceManagerInner.getInstance().setSceneAction(sIdentity, paramOrmsSaParam);
  }
  
  public void ormsClrSceneAction(long paramLong) {
    if (paramLong < 0L)
      return; 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ormsClrSceneAction: request=");
      stringBuilder.append(paramLong);
      Log.i("Orms_Manager", stringBuilder.toString());
    } 
    OplusResourceManagerInner.getInstance().clrSceneAction(sIdentity, paramLong);
  }
  
  public void ormsSetNotification(OrmsNotifyParam paramOrmsNotifyParam) {
    if (paramOrmsNotifyParam == null || paramOrmsNotifyParam.param4 == null) {
      Log.e("Orms_Manager", "notification param is illegal! ");
      return;
    } 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ormsSetNotification: ");
      stringBuilder.append(paramOrmsNotifyParam.toString());
      Log.i("Orms_Manager", stringBuilder.toString());
    } 
    OplusResourceManagerInner.getInstance().setNotification(sIdentity, paramOrmsNotifyParam);
  }
  
  public void ormsSetCtrlData(OrmsCtrlDataParam paramOrmsCtrlDataParam) {
    if (paramOrmsCtrlDataParam == null) {
      Log.e("Orms_Manager", "ormsCtrlData param is illegal! ");
      return;
    } 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ormsSetCtrlData by ");
      stringBuilder.append(sIdentity);
      Log.i("Orms_Manager", stringBuilder.toString());
    } 
    OplusResourceManagerInner.getInstance().setCtrlData(sIdentity, paramOrmsCtrlDataParam);
  }
  
  public void ormsClrCtrlData() {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ormsClrCtrlData by ");
      stringBuilder.append(sIdentity);
      Log.i("Orms_Manager", stringBuilder.toString());
    } 
    OplusResourceManagerInner.getInstance().clrCtrlData(sIdentity);
  }
  
  public int ormsGetModeStatus(int paramInt) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ormsGetModeStatus: identity=");
      stringBuilder.append(sIdentity);
      stringBuilder.append(" mode=");
      stringBuilder.append(paramInt);
      Log.i("Orms_Manager", stringBuilder.toString());
    } 
    return OplusResourceManagerInner.getInstance().getModeStatus(sIdentity, paramInt);
  }
  
  public long[][][] ormsGetPerfLimit() {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ormsGetPerfLimit: identity=");
      stringBuilder.append(sIdentity);
      Log.i("Orms_Manager", stringBuilder.toString());
    } 
    return OplusResourceManagerInner.getInstance().getPerfLimit(sIdentity);
  }
  
  public void ormsSendFling(MotionEvent paramMotionEvent, int paramInt) {
    try {
      StringBuilder stringBuilder;
      int i = paramMotionEvent.getActionMasked();
      int j = paramMotionEvent.getActionIndex();
      int k = paramMotionEvent.getPointerId(j);
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            if (i == 3) {
              long l = this.mRequest;
              if (l != -1L)
                ormsClrSceneAction(l); 
            } 
          } else {
            VelocityTracker velocityTracker = sVelocityTracker;
            if (velocityTracker != null) {
              velocityTracker.addMovement(paramMotionEvent);
              if (this.mRequest == -1L) {
                paramInt = Math.abs((int)paramMotionEvent.getX(j) - this.mStartX);
                i = Math.abs((int)paramMotionEvent.getY(j) - this.mStartY);
                if (Math.max(paramInt, i) > 40)
                  if (paramInt > i) {
                    this.mRequest = ormsSetSceneAction(new OrmsSaParam("", "ORMS_ACTION_SWIPE_SLOW_H", 0));
                  } else {
                    this.mRequest = ormsSetSceneAction(new OrmsSaParam("", "ORMS_ACTION_SWIPE_SLOW_V", 0));
                  }  
              } 
            } 
          } 
        } else {
          VelocityTracker velocityTracker = sVelocityTracker;
          if (velocityTracker != null) {
            velocityTracker.addMovement(paramMotionEvent);
            sVelocityTracker.computeCurrentVelocity(1000, 24000.0F);
            int m = Math.abs((int)sVelocityTracker.getYVelocity(k));
            i = 0;
            k = Math.abs((int)paramMotionEvent.getX(j) - this.mStartX);
            j = Math.abs((int)paramMotionEvent.getY(j) - this.mStartY);
            if (m > 150) {
              if (DEBUG) {
                stringBuilder = new StringBuilder();
                stringBuilder.append("diffX is:");
                stringBuilder.append(k);
                Log.d("orms", stringBuilder.toString());
              } 
              paramInt = (int)(paramInt * m * 1.0F / 150.0F);
              if (k > 150) {
                ormsSetSceneAction(new OrmsSaParam("", "ORMS_ACTION_SWIPE_H", paramInt + 100));
              } else {
                ormsSetSceneAction(new OrmsSaParam("", "ORMS_ACTION_SWIPE_V", paramInt));
              } 
              paramInt = 1;
            } else {
              paramInt = i;
            } 
            if (Math.max(k, j) > 60 && paramInt == 0)
              ormsSetSceneAction(new OrmsSaParam("", "ORMS_ACTION_SWIPE_SLOW_EXTEND", 200)); 
            long l = this.mRequest;
            if (l != -1L) {
              ormsClrSceneAction(l);
              this.mRequest = -1L;
            } 
          } 
        } 
      } else {
        this.mStartX = (int)stringBuilder.getX(j);
        this.mStartY = (int)stringBuilder.getY(j);
        VelocityTracker velocityTracker = sVelocityTracker;
        if (velocityTracker == null) {
          sVelocityTracker = VelocityTracker.obtain();
        } else {
          velocityTracker.clear();
        } 
        velocityTracker = sVelocityTracker;
        if (velocityTracker != null) {
          velocityTracker.addMovement((MotionEvent)stringBuilder);
          this.mRequest = -1L;
        } 
      } 
      return;
    } catch (IllegalArgumentException illegalArgumentException) {
      Log.w("Orms_Manager", "java.lang.IllegalArgumentException");
      return;
    } 
  }
}
