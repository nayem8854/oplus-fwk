package com.oplus.orms.info;

import android.os.Parcel;
import android.os.Parcelable;

public class OrmsCtrlDataParam implements Parcelable {
  public static final Parcelable.Creator<OrmsCtrlDataParam> CREATOR = new Parcelable.Creator<OrmsCtrlDataParam>() {
      public OrmsCtrlDataParam createFromParcel(Parcel param1Parcel) {
        return new OrmsCtrlDataParam(param1Parcel);
      }
      
      public OrmsCtrlDataParam[] newArray(int param1Int) {
        return new OrmsCtrlDataParam[param1Int];
      }
    };
  
  public int cpuClusterNum = -1;
  
  public int[][] cpuCoreCtrlData;
  
  public int[] cpuCtrlType;
  
  public int[][] cpuFreqCtrlData;
  
  public int[][] cpuMigData;
  
  public int gpuClusterNum = -1;
  
  public int[][] gpuCoreCtrlData;
  
  public int[] gpuCtrlType;
  
  public int[][] gpuFreqCtrlData;
  
  protected OrmsCtrlDataParam(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.cpuClusterNum);
    paramParcel.writeInt(this.gpuClusterNum);
    for (paramInt = 0; paramInt < this.cpuClusterNum; paramInt++) {
      paramParcel.writeInt(this.cpuCoreCtrlData[paramInt][0]);
      paramParcel.writeInt(this.cpuCoreCtrlData[paramInt][1]);
      paramParcel.writeInt(this.cpuFreqCtrlData[paramInt][0]);
      paramParcel.writeInt(this.cpuFreqCtrlData[paramInt][1]);
      paramParcel.writeInt(this.cpuCtrlType[paramInt]);
    } 
    for (paramInt = 0; paramInt < this.gpuClusterNum; paramInt++) {
      paramParcel.writeInt(this.gpuCoreCtrlData[paramInt][0]);
      paramParcel.writeInt(this.gpuCoreCtrlData[paramInt][1]);
      paramParcel.writeInt(this.gpuFreqCtrlData[paramInt][0]);
      paramParcel.writeInt(this.gpuFreqCtrlData[paramInt][1]);
      paramParcel.writeInt(this.gpuCtrlType[paramInt]);
    } 
    for (paramInt = 0; paramInt < this.cpuClusterNum - 1; paramInt++) {
      paramParcel.writeInt(this.cpuMigData[paramInt][0]);
      paramParcel.writeInt(this.cpuMigData[paramInt][1]);
    } 
  }
  
  public void readFromParcel(Parcel paramParcel) {
    paramParcel.setDataPosition(0);
    this.cpuClusterNum = paramParcel.readInt();
    this.gpuClusterNum = paramParcel.readInt();
    this.cpuCoreCtrlData = new int[this.cpuClusterNum][2];
    this.cpuFreqCtrlData = new int[this.cpuClusterNum][2];
    this.cpuCtrlType = new int[this.cpuClusterNum];
    this.gpuCoreCtrlData = new int[this.gpuClusterNum][2];
    this.gpuFreqCtrlData = new int[this.gpuClusterNum][2];
    this.gpuCtrlType = new int[this.gpuClusterNum];
    this.cpuMigData = new int[this.cpuClusterNum - 1][2];
    byte b;
    for (b = 0; b < this.cpuClusterNum; b++) {
      this.cpuCoreCtrlData[b][0] = paramParcel.readInt();
      this.cpuCoreCtrlData[b][1] = paramParcel.readInt();
      this.cpuFreqCtrlData[b][0] = paramParcel.readInt();
      this.cpuFreqCtrlData[b][1] = paramParcel.readInt();
      this.cpuCtrlType[b] = paramParcel.readInt();
    } 
    for (b = 0; b < this.gpuClusterNum; b++) {
      this.gpuCoreCtrlData[b][0] = paramParcel.readInt();
      this.gpuCoreCtrlData[b][1] = paramParcel.readInt();
      this.gpuFreqCtrlData[b][0] = paramParcel.readInt();
      this.gpuFreqCtrlData[b][1] = paramParcel.readInt();
      this.gpuCtrlType[b] = paramParcel.readInt();
    } 
    for (b = 0; b < this.cpuClusterNum - 1; b++) {
      this.cpuMigData[b][0] = paramParcel.readInt();
      this.cpuMigData[b][1] = paramParcel.readInt();
    } 
  }
  
  public OrmsCtrlDataParam() {}
}
