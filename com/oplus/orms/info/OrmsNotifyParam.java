package com.oplus.orms.info;

import android.os.Parcel;
import android.os.Parcelable;

public class OrmsNotifyParam implements Parcelable {
  public static final Parcelable.Creator<OrmsNotifyParam> CREATOR = new Parcelable.Creator<OrmsNotifyParam>() {
      public OrmsNotifyParam createFromParcel(Parcel param1Parcel) {
        return new OrmsNotifyParam(param1Parcel);
      }
      
      public OrmsNotifyParam[] newArray(int param1Int) {
        return new OrmsNotifyParam[param1Int];
      }
    };
  
  public int msgSrc = -1;
  
  public int msgType = -1;
  
  public int param1 = -1;
  
  public int param2 = -1;
  
  public int param3 = -1;
  
  public String param4 = "";
  
  public OrmsNotifyParam(int paramInt1, int paramInt2) {
    this.msgSrc = paramInt1;
    this.msgType = paramInt2;
  }
  
  public OrmsNotifyParam(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, String paramString) {
    this.msgSrc = paramInt1;
    this.msgType = paramInt2;
    this.param1 = paramInt3;
    this.param2 = paramInt4;
    this.param3 = paramInt5;
    this.param4 = paramString;
  }
  
  protected OrmsNotifyParam(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.msgSrc);
    paramParcel.writeInt(this.msgType);
    paramParcel.writeInt(this.param1);
    paramParcel.writeInt(this.param2);
    paramParcel.writeInt(this.param3);
    paramParcel.writeString(this.param4);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    paramParcel.setDataPosition(0);
    this.msgSrc = paramParcel.readInt();
    this.msgType = paramParcel.readInt();
    this.param1 = paramParcel.readInt();
    this.param2 = paramParcel.readInt();
    this.param3 = paramParcel.readInt();
    this.param4 = paramParcel.readString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("OrmsNotifyParam{msgSrc=");
    stringBuilder.append(this.msgSrc);
    stringBuilder.append(", msgType=");
    stringBuilder.append(this.msgType);
    stringBuilder.append(", param1=");
    stringBuilder.append(this.param1);
    stringBuilder.append(", param2=");
    stringBuilder.append(this.param2);
    stringBuilder.append(", param3=");
    stringBuilder.append(this.param3);
    stringBuilder.append(", param4='");
    stringBuilder.append(this.param4);
    stringBuilder.append('\'');
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public OrmsNotifyParam() {}
}
