package com.oplus.orms.info;

import android.os.Parcel;
import android.os.Parcelable;

public class OrmsSaParam implements Parcelable {
  public static final Parcelable.Creator<OrmsSaParam> CREATOR = new Parcelable.Creator<OrmsSaParam>() {
      public OrmsSaParam createFromParcel(Parcel param1Parcel) {
        return new OrmsSaParam(param1Parcel);
      }
      
      public OrmsSaParam[] newArray(int param1Int) {
        return new OrmsSaParam[param1Int];
      }
    };
  
  public String scene = "";
  
  public String action = "";
  
  public int timeout = -1;
  
  public OrmsSaParam(String paramString1, String paramString2, int paramInt) {
    this.scene = paramString1;
    this.action = paramString2;
    this.timeout = paramInt;
  }
  
  protected OrmsSaParam(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.scene);
    paramParcel.writeString(this.action);
    paramParcel.writeInt(this.timeout);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    paramParcel.setDataPosition(0);
    this.scene = paramParcel.readString();
    this.action = paramParcel.readString();
    this.timeout = paramParcel.readInt();
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("OrmsSaParam{scene='");
    stringBuilder.append(this.scene);
    stringBuilder.append('\'');
    stringBuilder.append(", action='");
    stringBuilder.append(this.action);
    stringBuilder.append('\'');
    stringBuilder.append(", timeout=");
    stringBuilder.append(this.timeout);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public OrmsSaParam() {}
}
