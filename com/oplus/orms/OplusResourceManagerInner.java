package com.oplus.orms;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Slog;
import com.oplus.orms.info.OrmsCtrlDataParam;
import com.oplus.orms.info.OrmsNotifyParam;
import com.oplus.orms.info.OrmsSaParam;
import java.security.SecureRandom;
import java.util.Arrays;

class OplusResourceManagerInner {
  private static final String DESCRIPTOR = "com.oplus.orms.OplusResourceManagerService";
  
  private static final int ORMS_BINDER_CHECK_ACCESS_PERMISSION = 1;
  
  private static final int ORMS_BINDER_CLEAR_SCENE_ACTION = 3;
  
  private static final int ORMS_BINDER_GET_MODE_STATUS = 9;
  
  private static final int ORMS_BINDER_GET_PERF_LIMIT = 10;
  
  private static final int ORMS_BINDER_READ_FILE = 7;
  
  private static final int ORMS_BINDER_READ_FILE_LIST = 8;
  
  private static final int ORMS_BINDER_RESET_CTRL_DATA = 6;
  
  private static final int ORMS_BINDER_SET_CTRL_DATA = 5;
  
  private static final int ORMS_BINDER_SET_NOTIFICATION = 4;
  
  private static final int ORMS_BINDER_SET_SCENE_ACTION = 2;
  
  private static final String ORMS_SERVICE = "OplusResourceManagerService";
  
  private static final String TAG = "Orms_ManagerInner";
  
  private static IBinder.DeathRecipient sDeathRecipient;
  
  private static OplusResourceManagerInner sOrmsManagerInner = null;
  
  private static IBinder sRemote;
  
  static {
    sDeathRecipient = (IBinder.DeathRecipient)new Object();
  }
  
  private OplusResourceManagerInner() {
    connectOrmsCoreService();
  }
  
  protected static OplusResourceManagerInner getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/orms/OplusResourceManagerInner.sOrmsManagerInner : Lcom/oplus/orms/OplusResourceManagerInner;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/orms/OplusResourceManagerInner
    //   8: monitorenter
    //   9: getstatic com/oplus/orms/OplusResourceManagerInner.sOrmsManagerInner : Lcom/oplus/orms/OplusResourceManagerInner;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/orms/OplusResourceManagerInner
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/orms/OplusResourceManagerInner.sOrmsManagerInner : Lcom/oplus/orms/OplusResourceManagerInner;
    //   27: ldc com/oplus/orms/OplusResourceManagerInner
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/orms/OplusResourceManagerInner
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/orms/OplusResourceManagerInner.sOrmsManagerInner : Lcom/oplus/orms/OplusResourceManagerInner;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #64	-> 0
    //   #65	-> 6
    //   #66	-> 9
    //   #67	-> 15
    //   #69	-> 27
    //   #71	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  private static IBinder connectOrmsCoreService() {
    IBinder iBinder = ServiceManager.checkService("OplusResourceManagerService");
    if (iBinder != null)
      try {
        iBinder.linkToDeath(sDeathRecipient, 0);
      } catch (RemoteException remoteException) {
        sRemote = null;
      }  
    return sRemote;
  }
  
  private static boolean ormsCoreEnable() {
    if (sRemote == null && connectOrmsCoreService() == null) {
      Slog.e("Orms_ManagerInner", "cannot connect to orms core service!");
      return false;
    } 
    return true;
  }
  
  protected static boolean checkAccessPermission(String paramString) {
    boolean bool = false;
    if (!ormsCoreEnable())
      return false; 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("com.oplus.orms.OplusResourceManagerService");
      parcel1.writeString(paramString);
      IBinder iBinder = sRemote;
      boolean bool1 = false;
      iBinder.transact(1, parcel1, parcel2, 0);
      int i = parcel2.readInt();
      bool = bool1;
      if (i == 1)
        bool = true; 
      parcel1.recycle();
      parcel2.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("check access permission failed! because ");
      stringBuilder.append(exception.getMessage());
      Slog.e("Orms_ManagerInner", stringBuilder.toString());
      exception.printStackTrace();
      parcel1.recycle();
      parcel2.recycle();
    } finally {}
    return bool;
  }
  
  private long obtainRandomHandle() {
    SecureRandom secureRandom = new SecureRandom();
    byte[] arrayOfByte = new byte[20];
    secureRandom.nextBytes(arrayOfByte);
    return Math.abs(secureRandom.nextLong());
  }
  
  protected long setSceneAction(String paramString, OrmsSaParam paramOrmsSaParam) {
    if (!ormsCoreEnable())
      return -1L; 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    long l = obtainRandomHandle();
    try {
      parcel1.writeInterfaceToken("com.oplus.orms.OplusResourceManagerService");
      parcel1.writeString(paramString);
      parcel1.writeString(paramOrmsSaParam.scene);
      parcel1.writeString(paramOrmsSaParam.action);
      parcel1.writeInt(paramOrmsSaParam.timeout);
      parcel1.writeLong(l);
      sRemote.transact(2, parcel1, parcel2, 1);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("try to set scene action failed! because ");
      stringBuilder.append(exception.getMessage());
      Slog.e("Orms_ManagerInner", stringBuilder.toString());
      exception.printStackTrace();
    } finally {}
    parcel1.recycle();
    parcel2.recycle();
    return l;
  }
  
  protected void clrSceneAction(String paramString, long paramLong) {
    if (!ormsCoreEnable())
      return; 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("com.oplus.orms.OplusResourceManagerService");
      parcel1.writeString(paramString);
      parcel1.writeLong(paramLong);
      sRemote.transact(3, parcel1, parcel2, 1);
      parcel1.recycle();
      parcel2.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("try to clear scene action failed! because ");
      stringBuilder.append(exception.getMessage());
      Slog.e("Orms_ManagerInner", stringBuilder.toString());
      exception.printStackTrace();
      parcel1.recycle();
      parcel2.recycle();
    } finally {}
  }
  
  protected void setNotification(String paramString, OrmsNotifyParam paramOrmsNotifyParam) {
    if (!ormsCoreEnable())
      return; 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("com.oplus.orms.OplusResourceManagerService");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramOrmsNotifyParam.msgSrc);
      parcel1.writeInt(paramOrmsNotifyParam.msgType);
      parcel1.writeInt(paramOrmsNotifyParam.param1);
      parcel1.writeInt(paramOrmsNotifyParam.param2);
      parcel1.writeInt(paramOrmsNotifyParam.param3);
      parcel1.writeString(paramOrmsNotifyParam.param4);
      sRemote.transact(4, parcel1, parcel2, 1);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("try to set notification failed! because ");
      stringBuilder.append(exception.getMessage());
      Slog.e("Orms_ManagerInner", stringBuilder.toString());
      exception.printStackTrace();
    } finally {}
    parcel1.recycle();
    parcel2.recycle();
  }
  
  protected void setCtrlData(String paramString, OrmsCtrlDataParam paramOrmsCtrlDataParam) {
    if (!ormsCoreEnable())
      return; 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("com.oplus.orms.OplusResourceManagerService");
      parcel1.writeString(paramString);
      int i = paramOrmsCtrlDataParam.cpuClusterNum;
      int j = paramOrmsCtrlDataParam.gpuClusterNum;
      parcel1.writeInt(i);
      parcel1.writeInt(j);
      byte b;
      for (b = 0; b < i; b++) {
        parcel1.writeInt(paramOrmsCtrlDataParam.cpuCoreCtrlData[b][0]);
        parcel1.writeInt(paramOrmsCtrlDataParam.cpuCoreCtrlData[b][1]);
        parcel1.writeInt(paramOrmsCtrlDataParam.cpuFreqCtrlData[b][0]);
        parcel1.writeInt(paramOrmsCtrlDataParam.cpuFreqCtrlData[b][1]);
        parcel1.writeInt(paramOrmsCtrlDataParam.cpuCtrlType[b]);
      } 
      for (b = 0; b < j; b++) {
        parcel1.writeInt(paramOrmsCtrlDataParam.gpuCoreCtrlData[b][0]);
        parcel1.writeInt(paramOrmsCtrlDataParam.gpuCoreCtrlData[b][1]);
        parcel1.writeInt(paramOrmsCtrlDataParam.gpuFreqCtrlData[b][0]);
        parcel1.writeInt(paramOrmsCtrlDataParam.gpuFreqCtrlData[b][1]);
        parcel1.writeInt(paramOrmsCtrlDataParam.gpuCtrlType[b]);
      } 
      for (b = 0; b < i - 1; b++) {
        parcel1.writeInt(paramOrmsCtrlDataParam.cpuMigData[b][0]);
        parcel1.writeInt(paramOrmsCtrlDataParam.cpuMigData[b][1]);
      } 
      sRemote.transact(5, parcel1, parcel2, 1);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("try to set ctrl data failed! because ");
      stringBuilder.append(exception.getMessage());
      Slog.e("Orms_ManagerInner", stringBuilder.toString());
      exception.printStackTrace();
    } finally {}
    parcel1.recycle();
    parcel2.recycle();
  }
  
  protected void clrCtrlData(String paramString) {
    if (!ormsCoreEnable())
      return; 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("com.oplus.orms.OplusResourceManagerService");
      parcel1.writeString(paramString);
      sRemote.transact(6, parcel1, parcel2, 1);
      parcel1.recycle();
      parcel2.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("try to clear ctrl data failed! because ");
      stringBuilder.append(exception.getMessage());
      Slog.e("Orms_ManagerInner", stringBuilder.toString());
      exception.printStackTrace();
      parcel1.recycle();
      parcel2.recycle();
    } finally {}
  }
  
  protected int getModeStatus(String paramString, int paramInt) {
    byte b = -1;
    if (!ormsCoreEnable())
      return -1; 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("com.oplus.orms.OplusResourceManagerService");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      sRemote.transact(9, parcel1, parcel2, 0);
      paramInt = parcel2.readInt();
      parcel1.recycle();
      parcel2.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("try to get mode status failed! because ");
      stringBuilder.append(exception.getMessage());
      Slog.e("Orms_ManagerInner", stringBuilder.toString());
      exception.printStackTrace();
      paramInt = b;
      parcel1.recycle();
      parcel2.recycle();
    } finally {}
    return paramInt;
  }
  
  protected long[][][] getPerfLimit(String paramString) {
    long arrayOfLong1[], arrayOfLong2[], arrayOfLong[][][] = new long[0][][];
    if (!ormsCoreEnable())
      return null; 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("com.oplus.orms.OplusResourceManagerService");
      try {
        long[] arrayOfLong3;
        parcel1.writeString(paramString);
        sRemote.transact(10, parcel1, parcel2, 0);
        parcel2.readException();
        long[] arrayOfLong4 = parcel2.createLongArray();
        parcel1.recycle();
        parcel2.recycle();
        if (arrayOfLong4 != null)
          try {
            int i = arrayOfLong4.length;
            if (i >= 5) {
              i = 0 + 1;
              long l = arrayOfLong4[0];
              int j = (int)l;
              int k = i + 1;
              l = arrayOfLong4[i];
              int m = (int)l;
              i = k + 1;
              l = arrayOfLong4[k];
              int n = (int)l;
              k = i + 1;
              l = arrayOfLong4[i];
              int i1 = (int)l;
              i = k + 1;
              int i2 = (int)arrayOfLong4[k];
              int i3 = i;
              try {
                long[][][] arrayOfLong5 = new long[i2][][];
                l = ((m * 4 + j + n * 4 + i1 * 2) * i2 + 5);
                i3 = i;
                if (arrayOfLong4.length == l)
                  for (k = 0, i3 = i; k < i2; ) {
                    int i4 = i3 + j, i5 = i4;
                    try {
                      long[] arrayOfLong6 = Arrays.copyOfRange(arrayOfLong4, i3, i4);
                      i = i3 = i4 = i5 + m;
                      long[] arrayOfLong7 = Arrays.copyOfRange(arrayOfLong4, i5, i4);
                      i4 = i3 + m;
                      try {
                        long[] arrayOfLong8 = Arrays.copyOfRange(arrayOfLong4, i3, i4);
                        i = i5 = i3 = i4 + m;
                        try {
                          long[] arrayOfLong9 = Arrays.copyOfRange(arrayOfLong4, i4, i3);
                          i3 = i = i4 = i5 + m;
                          try {
                            long[] arrayOfLong10 = Arrays.copyOfRange(arrayOfLong4, i5, i4);
                            i3 = i5 = i4 = i + n;
                            arrayOfLong3 = Arrays.copyOfRange(arrayOfLong4, i, i4);
                            i = i3 = i4 = i5 + n;
                            long[] arrayOfLong11 = Arrays.copyOfRange(arrayOfLong4, i5, i4);
                            i = i5 = i4 = i3 + n;
                            long[] arrayOfLong12 = Arrays.copyOfRange(arrayOfLong4, i3, i4);
                            i = i3 = i4 = i5 + n;
                            long[] arrayOfLong13 = Arrays.copyOfRange(arrayOfLong4, i5, i4);
                            i = i5 = i4 = i3 + i1;
                            arrayOfLong1 = Arrays.copyOfRange(arrayOfLong4, i3, i4);
                            i3 = i = i4 = i5 + i1;
                            arrayOfLong2 = Arrays.copyOfRange(arrayOfLong4, i5, i4);
                            i3 = i;
                            (new long[11][])[0] = arrayOfLong6;
                            (new long[11][])[1] = arrayOfLong7;
                            (new long[11][])[2] = arrayOfLong8;
                            (new long[11][])[3] = arrayOfLong9;
                            (new long[11][])[4] = arrayOfLong10;
                            (new long[11][])[5] = arrayOfLong3;
                            (new long[11][])[6] = arrayOfLong11;
                            (new long[11][])[7] = arrayOfLong12;
                            (new long[11][])[8] = arrayOfLong13;
                            (new long[11][])[9] = arrayOfLong1;
                            (new long[11][])[10] = arrayOfLong2;
                            arrayOfLong5[k] = new long[11][];
                            k++;
                            i3 = i;
                          } catch (Exception null) {}
                        } catch (Exception null) {}
                        // Byte code: goto -> 616
                      } catch (Exception null) {}
                      // Byte code: goto -> 616
                    } catch (Exception null) {
                      // Byte code: goto -> 616
                    } 
                  }  
              } catch (Exception exception) {
                exception.printStackTrace();
                return null;
              } 
              return (long[][][])exception;
            } 
          } catch (Exception exception) {
            exception.printStackTrace();
            return null;
          }  
        return (long[][][])arrayOfLong3;
      } catch (Exception exception) {
      
      } finally {}
    } catch (Exception exception) {
    
    } finally {}
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("try to get performance limit! because ");
      stringBuilder.append(paramString.getMessage());
      Slog.e("Orms_ManagerInner", stringBuilder.toString());
      paramString.printStackTrace();
      arrayOfLong1.recycle();
      arrayOfLong2.recycle();
      return null;
    } finally {}
    arrayOfLong1.recycle();
    arrayOfLong2.recycle();
    throw paramString;
  }
}
