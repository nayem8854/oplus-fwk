package com.oplus.orms;

import com.oplus.orms.info.OrmsCtrlDataParam;
import com.oplus.orms.info.OrmsNotifyParam;
import com.oplus.orms.info.OrmsSaParam;

public interface IOplusResourceManager {
  void ormsClrCtrlData();
  
  void ormsClrSceneAction(long paramLong);
  
  void ormsGetModeStatus(int paramInt);
  
  void ormsSetCtrlData(OrmsCtrlDataParam paramOrmsCtrlDataParam);
  
  void ormsSetNotification(OrmsNotifyParam paramOrmsNotifyParam);
  
  long ormsSetSceneAction(OrmsSaParam paramOrmsSaParam);
}
