package com.oplus.romupdate;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemProperties;
import android.util.Log;
import android.util.Pair;

public class RomUpdateHelper {
  private static final String BROADCAST_ACTION_ROM_UPDATE_CONFIG_SUCCES = "oppo.intent.action.ROM_UPDATE_CONFIG_SUCCESS";
  
  private static final String COLUMN_NAME_CONTENT = "xml";
  
  private static final String COLUMN_NAME_VERSION = "version";
  
  private static final Uri CONTENT_URI_WHITE_LIST;
  
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static final String OPPO_COMPONENT_SAFE_PERMISSION = "oppo.permission.OPPO_COMPONENT_SAFE";
  
  private static final String ROM_UPDATE_CONFIG_LIST = "ROM_UPDATE_CONFIG_LIST";
  
  private static final String TAG = "RomUpdateHelper";
  
  private Context mContext;
  
  private String mFilterName;
  
  private Handler mHandler;
  
  private UpdateInfoListener mListener;
  
  static {
    CONTENT_URI_WHITE_LIST = Uri.parse("content://com.nearme.romupdate.provider.db/update_list");
  }
  
  public RomUpdateHelper(Context paramContext, String paramString) {
    if (paramContext != null && paramString != null) {
      this.mContext = paramContext;
      this.mFilterName = paramString;
      this.mHandler = new Handler(paramContext.getMainLooper());
      return;
    } 
    throw new IllegalArgumentException("The parameters must not be null");
  }
  
  public void setUpdateInfoListener(UpdateInfoListener paramUpdateInfoListener) {
    this.mListener = paramUpdateInfoListener;
  }
  
  public void registerUpdateBroadcastReceiver() {
    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction("oppo.intent.action.ROM_UPDATE_CONFIG_SUCCESS");
    this.mContext.registerReceiver((BroadcastReceiver)new Object(this), intentFilter, "oppo.permission.OPPO_COMPONENT_SAFE", null);
  }
  
  public Pair<Integer, String> getDataFromProvider() {
    ContentResolver contentResolver4;
    int j;
    byte b = -1;
    ContentResolver contentResolver1 = null, contentResolver2 = null;
    int i = b;
    ContentResolver contentResolver3 = contentResolver1;
    try {
      contentResolver4 = this.mContext.getContentResolver();
      i = b;
      contentResolver3 = contentResolver1;
      Uri uri = CONTENT_URI_WHITE_LIST;
      i = b;
      contentResolver3 = contentResolver1;
      StringBuilder stringBuilder = new StringBuilder();
      i = b;
      contentResolver3 = contentResolver1;
      this();
      i = b;
      contentResolver3 = contentResolver1;
      stringBuilder.append("filtername=\"");
      i = b;
      contentResolver3 = contentResolver1;
      stringBuilder.append(this.mFilterName);
      i = b;
      contentResolver3 = contentResolver1;
      stringBuilder.append("\"");
      i = b;
      contentResolver3 = contentResolver1;
      String str = stringBuilder.toString();
      i = b;
      contentResolver3 = contentResolver1;
      Cursor cursor = contentResolver4.query(uri, new String[] { "version", "xml" }, str, null, null);
      j = b;
      contentResolver4 = contentResolver2;
      if (cursor != null)
        int k = b; 
      if (cursor != null) {
        i = j;
        contentResolver3 = contentResolver4;
        cursor.close();
      } 
    } catch (Exception exception) {
      Log.e("RomUpdateHelper", "getDataFromProvider", exception);
      contentResolver4 = contentResolver3;
      j = i;
    } 
    return new Pair(Integer.valueOf(j), contentResolver4);
  }
  
  public static interface UpdateInfoListener {
    void onUpdateInfoChanged(String param1String);
  }
}
