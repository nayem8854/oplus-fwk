package com.oplus.ota;

import android.content.Context;
import android.os.IBinder;
import android.os.IRecoverySystem;
import android.os.PowerManager;
import android.os.RecoverySystem;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.text.TextUtils;
import android.util.Log;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

public class OplusRecoverySystem {
  private static final File LOG_FILE;
  
  private static final int REAL_PATH_START_NUM = 4;
  
  private static final File RECOVERY_DIR;
  
  private static final String TAG = "OplusRecoverySystem";
  
  private static File sCOMMANDFILE;
  
  private static File sLOGFILE;
  
  private static File sRECOVERYDIR = new File("/cache/recovery");
  
  static {
    sCOMMANDFILE = new File(sRECOVERYDIR, "command");
    sLOGFILE = new File(sRECOVERYDIR, "log");
    RECOVERY_DIR = new File("/cache/recovery");
    LOG_FILE = new File(RECOVERY_DIR, "log");
  }
  
  public static void installOplusOtaPackage(Context paramContext, ArrayList<File> paramArrayList) throws IOException {
    if (paramArrayList == null) {
      Log.e("OplusRecoverySystem", "installOplusOtaPackage failed before reboot, packageFileList is null!!!");
      return;
    } 
    File file = paramArrayList.get(0);
    RecoverySystem.installPackage(paramContext, file);
  }
  
  public static void installOplusOtaPackageSpecial(Context paramContext, ArrayList<File> paramArrayList) throws IOException {
    paramContext.enforceCallingOrSelfPermission("android.permission.RECOVERY", null);
    if (paramArrayList == null) {
      Log.e("OplusRecoverySystem", "installOplusOtaPackage failed before reboot, packageFileList is null!!!");
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    boolean bool = false;
    for (File file : paramArrayList) {
      String str2 = file.getCanonicalPath();
      if (str2 != null && str2.equals("/--wipe_data")) {
        bool = true;
        continue;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("filename=");
      stringBuilder1.append(str2);
      Log.i("OplusRecoverySystem", stringBuilder1.toString());
      String str1 = str2;
      if (str2 != null) {
        str1 = str2;
        if (str2.startsWith("/mnt"))
          str1 = str2.substring(4); 
      } 
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("!!! REBOOT TO INSTALL ");
      stringBuilder2.append(str1);
      stringBuilder2.append(" !!!");
      Log.w("OplusRecoverySystem", stringBuilder2.toString());
      if (str1 != null) {
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append("--special_update_package=");
        stringBuilder2.append(str1);
        str1 = stringBuilder2.toString();
        stringBuilder.append(str1);
        stringBuilder.append("\n");
      } 
    } 
    if (bool) {
      Log.w("OplusRecoverySystem", "!!!WIPE DATA FOR OTA!!!");
      stringBuilder.append("--wipe_data");
      stringBuilder.append("\n");
    } 
    rebootToRecoveryWithCommand(paramContext, new String[] { stringBuilder.toString() });
    throw new IOException("Reboot failed (no permissions?)");
  }
  
  public static void oplusInstallPackageSpecial(Context paramContext, File paramFile) throws IOException {
    String str2;
    paramContext.enforceCallingOrSelfPermission("android.permission.RECOVERY", null);
    String str1 = paramFile.getCanonicalPath();
    FileWriter fileWriter = new FileWriter(new File(sRECOVERYDIR, "uncrypt_file"));
    try {
      StringBuilder stringBuilder2 = new StringBuilder();
      this();
      stringBuilder2.append(str1);
      stringBuilder2.append("\n");
      fileWriter.write(stringBuilder2.toString());
      fileWriter.close();
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("!!! REBOOTING TO INSTALL ");
      stringBuilder1.append(str1);
      stringBuilder1.append(" !!!");
      Log.w("OplusRecoverySystem", stringBuilder1.toString());
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("--special_update_package=");
      stringBuilder1.append(str1);
      str1 = stringBuilder1.toString();
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("--locale=");
      stringBuilder1.append(Locale.getDefault().toString());
      str2 = stringBuilder1.toString();
      return;
    } finally {
      str2.close();
    } 
  }
  
  public static void oplusInstallPackageList(Context paramContext, ArrayList<File> paramArrayList) throws IOException {
    if (paramArrayList == null) {
      Log.e("OplusRecoverySystem", "oplusInstallPackageList failed before reboot, packageFileList is null!!!");
      return;
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    for (File file : paramArrayList) {
      String str1 = file.getCanonicalPath();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("!!! REBOOT TO INSTALL ");
      stringBuilder.append(str1);
      stringBuilder.append(" !!!");
      Log.w("OplusRecoverySystem", stringBuilder.toString());
      if (!TextUtils.isEmpty(str1)) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("--special_update_package=");
        stringBuilder.append(str1);
        str1 = stringBuilder.toString();
        stringBuilder2.append(str1);
        stringBuilder2.append("\n");
      } 
    } 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("--locale=");
    stringBuilder1.append(Locale.getDefault().toString());
    String str = stringBuilder1.toString();
    if (!TextUtils.isEmpty(str)) {
      stringBuilder2.append(str);
      stringBuilder2.append("\n");
    } 
    rebootToRecoveryWithCommand(paramContext, new String[] { stringBuilder2.toString() });
    throw new IOException("Reboot failed (no permissions?)");
  }
  
  private static void setupBcb(Context paramContext, String... paramVarArgs) {
    LOG_FILE.delete();
    StringBuilder stringBuilder = new StringBuilder();
    int i;
    byte b;
    for (i = paramVarArgs.length, b = 0; b < i; ) {
      String str = paramVarArgs[b];
      if (!TextUtils.isEmpty(str)) {
        stringBuilder.append(str);
        stringBuilder.append("\n");
      } 
      b++;
    } 
    try {
      IBinder iBinder = ServiceManager.getService("recovery");
      IRecoverySystem iRecoverySystem = IRecoverySystem.Stub.asInterface(iBinder);
      iRecoverySystem.setupBcb(stringBuilder.toString());
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
    } 
  }
  
  private static void rebootToRecoveryWithCommand(Context paramContext, String... paramVarArgs) throws IOException {
    LOG_FILE.delete();
    StringBuilder stringBuilder = new StringBuilder();
    int i;
    byte b;
    for (i = paramVarArgs.length, b = 0; b < i; ) {
      String str = paramVarArgs[b];
      if (!TextUtils.isEmpty(str)) {
        stringBuilder.append(str);
        stringBuilder.append("\n");
      } 
      b++;
    } 
    try {
      IBinder iBinder = ServiceManager.getService("recovery");
      IRecoverySystem iRecoverySystem = IRecoverySystem.Stub.asInterface(iBinder);
      iRecoverySystem.rebootRecoveryWithCommand(stringBuilder.toString());
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
    } 
    throw new IOException("Reboot failed (no permissions?)");
  }
  
  private static void bootCommand(Context paramContext, String... paramVarArgs) throws IOException {
    paramContext.enforceCallingOrSelfPermission("android.permission.RECOVERY", null);
    sRECOVERYDIR.mkdirs();
    sCOMMANDFILE.delete();
    sLOGFILE.delete();
    FileWriter fileWriter = new FileWriter(sCOMMANDFILE);
    try {
      int i;
      byte b;
      for (i = paramVarArgs.length, b = 0; b < i; ) {
        String str = paramVarArgs[b];
        if (!TextUtils.isEmpty(str)) {
          fileWriter.write(str);
          fileWriter.write("\n");
        } 
        b++;
      } 
      fileWriter.close();
      PowerManager powerManager = (PowerManager)paramContext.getSystemService("power");
      powerManager.reboot("recovery");
      throw new IOException("Reboot failed (no permissions?)");
    } finally {
      fileWriter.close();
    } 
  }
  
  public static void installOplusSauPackageSpecial(Context paramContext, ArrayList<File> paramArrayList) throws IOException {
    paramContext.enforceCallingOrSelfPermission("android.permission.RECOVERY", null);
    if (paramArrayList == null) {
      Log.e("OplusRecoverySystem", "installOplusSauPackageSpecial failed before reboot, packageFileList is null!!!");
      return;
    } 
    sLOGFILE.delete();
    StringBuilder stringBuilder = new StringBuilder();
    boolean bool = false;
    for (File file : paramArrayList) {
      String str2 = file.getCanonicalPath();
      if (str2 != null && str2.equals("/--wipe_data")) {
        bool = true;
        continue;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("filename=");
      stringBuilder1.append(str2);
      Log.i("OplusRecoverySystem", stringBuilder1.toString());
      String str1 = str2;
      if (str2 != null) {
        str1 = str2;
        if (str2.startsWith("/mnt"))
          str1 = str2.substring(4); 
      } 
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("!!! REBOOT TO INSTALL ");
      stringBuilder2.append(str1);
      stringBuilder2.append(" !!!");
      Log.w("OplusRecoverySystem", stringBuilder2.toString());
      if (str1 != null) {
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append("--special_update_package=");
        stringBuilder2.append(str1);
        str1 = stringBuilder2.toString();
        stringBuilder.append(str1);
        stringBuilder.append("\n");
      } 
    } 
    if (bool) {
      Log.w("OplusRecoverySystem", "!!!WIPE DATA FOR OTA!!!");
      stringBuilder.append("--wipe_data");
      stringBuilder.append("\n");
    } 
    setupBcb(paramContext, new String[] { stringBuilder.toString() });
    PowerManager powerManager = (PowerManager)paramContext.getSystemService("power");
    powerManager.reboot("sau");
    throw new IOException("Reboot failed (no permissions?)");
  }
  
  public static void installOplusSauPackage(Context paramContext, ArrayList<File> paramArrayList) throws IOException {
    paramContext.enforceCallingOrSelfPermission("android.permission.RECOVERY", null);
    if (paramArrayList == null) {
      Log.e("OplusRecoverySystem", "installOplusSauPackage failed before reboot, packageFileList is null!!!");
      return;
    } 
    sRECOVERYDIR.mkdirs();
    sCOMMANDFILE.delete();
    sLOGFILE.delete();
    FileWriter fileWriter = new FileWriter(sCOMMANDFILE);
    boolean bool = false;
    try {
      Iterator<File> iterator = paramArrayList.iterator();
      while (true) {
        boolean bool1 = iterator.hasNext();
        if (bool1) {
          File file = iterator.next();
          String str2 = file.getCanonicalPath();
          if (str2 != null && str2.equals("/--wipe_data")) {
            bool = true;
            continue;
          } 
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("installOplusSauPackage filename=");
          stringBuilder1.append(str2);
          Log.i("OplusRecoverySystem", stringBuilder1.toString());
          String str1 = str2;
          if (str2 != null) {
            str1 = str2;
            if (str2.startsWith("/mnt"))
              str1 = str2.substring(4); 
          } 
          StringBuilder stringBuilder2 = new StringBuilder();
          this();
          stringBuilder2.append("!!! REBOOT TO INSTALL ");
          stringBuilder2.append(str1);
          stringBuilder2.append(" !!!");
          Log.w("OplusRecoverySystem", stringBuilder2.toString());
          if (str1 != null) {
            stringBuilder2 = new StringBuilder();
            this();
            stringBuilder2.append("--update_package=");
            stringBuilder2.append(str1);
            str1 = stringBuilder2.toString();
            fileWriter.write(str1);
            fileWriter.write("\n");
          } 
          continue;
        } 
        break;
      } 
      if (bool) {
        Log.w("OplusRecoverySystem", "!!!WIPE DATA FOR OTA!!!");
        fileWriter.write("--wipe_data");
        fileWriter.write("\n");
      } 
      fileWriter.close();
      PowerManager powerManager = (PowerManager)paramContext.getSystemService("power");
      powerManager.reboot("sau");
      throw new IOException("Reboot failed (no permissions?)");
    } finally {
      fileWriter.close();
    } 
  }
  
  public static void restoreOplusModem(Context paramContext, String paramString, int paramInt) throws IOException {
    paramContext.enforceCallingOrSelfPermission("android.permission.RECOVERY", null);
    if (paramString == null) {
      Log.e("OplusRecoverySystem", "restoreOplusModem failed before reboot, packageFileList is null!!!");
      return;
    } 
    sRECOVERYDIR.mkdirs();
    sCOMMANDFILE.delete();
    sLOGFILE.delete();
    FileWriter fileWriter = new FileWriter(sCOMMANDFILE);
    try {
      File file = new File();
      this(paramString);
      String str = file.getCanonicalPath();
      paramString = str;
      if (str != null) {
        paramString = str;
        if (str.startsWith("/mnt"))
          paramString = str.substring(4); 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("!!! REBOOT TO RESTORE ");
      stringBuilder.append(paramString);
      stringBuilder.append(" !!!");
      Log.w("OplusRecoverySystem", stringBuilder.toString());
      if (paramString != null) {
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("--restore_modem=");
        stringBuilder.append(paramString);
        paramString = stringBuilder.toString();
        fileWriter.write(paramString);
        fileWriter.write("\n");
      } 
      fileWriter.close();
      PowerManager powerManager = (PowerManager)paramContext.getSystemService("power");
      powerManager.reboot("recovery");
      throw new IOException("Reboot failed (no permissions?)");
    } finally {
      fileWriter.close();
    } 
  }
  
  public static void oplusInstallPackageFromNvId(Context paramContext, String paramString) throws IOException {
    paramContext.enforceCallingOrSelfPermission("android.permission.RECOVERY", null);
    if (!TextUtils.isEmpty(paramString)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("!!! REBOOTING TO OTG INSTALL, nvID= ");
      stringBuilder.append(paramString);
      stringBuilder.append(" !!!");
      Log.w("OplusRecoverySystem", stringBuilder.toString());
      stringBuilder = new StringBuilder();
      stringBuilder.append("--update_package_nv_id=");
      stringBuilder.append(paramString);
      paramString = stringBuilder.toString();
      stringBuilder = new StringBuilder();
      stringBuilder.append("--locale=");
      stringBuilder.append(Locale.getDefault().toString());
      String str = stringBuilder.toString();
      rebootToRecoveryWithCommand(paramContext, new String[] { paramString, str });
    } 
  }
}
