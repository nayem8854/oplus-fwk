package com.oplus.ota;

import android.content.Context;
import android.content.Intent;
import android.os.SystemProperties;
import android.util.Slog;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class OplusOtaUtils {
  private static final String OTA_UPDATE_FAILED = "1";
  
  private static final String OTA_UPDATE_OK = "0";
  
  private static final String RECOVER_UPDATE_FAILED = "3";
  
  private static final String RECOVER_UPDATE_OK = "2";
  
  private static final String TAG = "OplusOtaUtils";
  
  public static void notifyOTAUpdateResult(Context paramContext) {
    boolean bool1 = false;
    File file = new File("/cache/recovery/last_install");
    boolean bool2 = bool1;
    if (file.exists()) {
      String str = readOTAUpdateResult("/cache/recovery/last_install");
      bool2 = bool1;
      if (str != null) {
        bool2 = bool1;
        if (str.contains("/.SAU/zip/"))
          bool2 = true; 
      } 
    } 
    file = new File("/cache/recovery/intent");
    Slog.d("OplusOtaUtils", "check /cache/recovery/intent");
    if (file.exists()) {
      Slog.i("OplusOtaUtils", "/cache/recovery/intent file is exist!!!");
      String str = readOTAUpdateResult("/cache/recovery/intent");
      if ("0".equals(str)) {
        Slog.i("OplusOtaUtils", "OTA update successed!!!");
        if (bool2) {
          str = "oppo.intent.action.OPPO_SAU_UPDATE_SUCCESSED";
        } else {
          str = "oppo.intent.action.OPPO_OTA_UPDATE_SUCCESSED";
        } 
        Intent intent = new Intent(str);
        intent.addFlags(16777216);
        str = readFFUUpdateResult();
        if (str != null)
          intent.putExtra("ffuresult", str); 
        paramContext.sendBroadcast(intent);
        SystemProperties.set("persist.sys.panictime", Integer.toString(0));
      } else {
        Intent intent;
        if ("1".equals(str)) {
          Slog.i("OplusOtaUtils", "OTA update failed!!!");
          if (bool2) {
            str = "oppo.intent.action.OPPO_SAU_UPDATE_FAILED";
          } else {
            str = "oppo.intent.action.OPPO_OTA_UPDATE_FAILED";
          } 
          intent = new Intent(str);
          intent.addFlags(16777216);
          sendOTAFailLogIntent(paramContext, intent);
        } else {
          String str1;
          if ("2".equals(intent)) {
            Slog.i("OplusOtaUtils", "Recover update ok!!!");
            Intent intent1 = new Intent("oppo.intent.action.OPPO_RECOVER_UPDATE_SUCCESSED");
            str1 = readFFUUpdateResult();
            if (str1 != null)
              intent1.putExtra("ffuresult", str1); 
            intent1.addFlags(16777216);
            paramContext.sendBroadcast(intent1);
          } else if ("3".equals(str1)) {
            Slog.i("OplusOtaUtils", "Recover update failed!!!");
            Intent intent1 = new Intent("oppo.intent.action.OPPO_RECOVER_UPDATE_FAILED");
            intent1.addFlags(16777216);
            sendOTAFailLogIntent(paramContext, intent1);
          } else {
            Slog.i("OplusOtaUtils", "OTA update file's date is invalid!!!");
          } 
        } 
      } 
    } 
  }
  
  private static void sendOTAFailLogIntent(Context paramContext, Intent paramIntent) {
    // Byte code:
    //   0: ldc 'read_last_log close the reader failed!!!'
    //   2: astore_2
    //   3: ldc 'ERROR: Open file'
    //   5: astore_3
    //   6: ldc 'signature verification failed'
    //   8: astore #4
    //   10: bipush #-5
    //   12: istore #5
    //   14: new java/io/File
    //   17: dup
    //   18: ldc '/cache/recovery/last_log'
    //   20: invokespecial <init> : (Ljava/lang/String;)V
    //   23: astore #6
    //   25: ldc 'OplusOtaUtils'
    //   27: ldc 'check /cache/recovery/last_log'
    //   29: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   32: pop
    //   33: aload #6
    //   35: invokevirtual exists : ()Z
    //   38: ifeq -> 920
    //   41: ldc 'OplusOtaUtils'
    //   43: ldc '/cache/recovery/last_log file is exist!!!'
    //   45: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   48: pop
    //   49: aconst_null
    //   50: astore #7
    //   52: aconst_null
    //   53: astore #8
    //   55: aconst_null
    //   56: astore #9
    //   58: aconst_null
    //   59: astore #10
    //   61: iconst_0
    //   62: istore #11
    //   64: new java/io/BufferedReader
    //   67: astore #12
    //   69: aload_2
    //   70: astore #9
    //   72: aload_3
    //   73: astore #8
    //   75: aload #4
    //   77: astore #8
    //   79: iload #5
    //   81: istore #13
    //   83: aload #6
    //   85: astore #8
    //   87: aload #10
    //   89: astore #8
    //   91: aload_2
    //   92: astore #14
    //   94: aload_3
    //   95: astore #15
    //   97: aload #4
    //   99: astore #15
    //   101: iload #5
    //   103: istore #13
    //   105: aload #6
    //   107: astore #15
    //   109: aload #7
    //   111: astore #15
    //   113: new java/io/FileReader
    //   116: astore #16
    //   118: aload_2
    //   119: astore #9
    //   121: aload_3
    //   122: astore #8
    //   124: aload #4
    //   126: astore #8
    //   128: iload #5
    //   130: istore #13
    //   132: aload #6
    //   134: astore #8
    //   136: aload #10
    //   138: astore #8
    //   140: aload_2
    //   141: astore #14
    //   143: aload_3
    //   144: astore #15
    //   146: aload #4
    //   148: astore #15
    //   150: iload #5
    //   152: istore #13
    //   154: aload #6
    //   156: astore #15
    //   158: aload #7
    //   160: astore #15
    //   162: aload #16
    //   164: aload #6
    //   166: invokespecial <init> : (Ljava/io/File;)V
    //   169: aload_2
    //   170: astore #9
    //   172: aload_3
    //   173: astore #8
    //   175: aload #4
    //   177: astore #8
    //   179: iload #5
    //   181: istore #13
    //   183: aload #6
    //   185: astore #8
    //   187: aload #10
    //   189: astore #8
    //   191: aload_2
    //   192: astore #14
    //   194: aload_3
    //   195: astore #15
    //   197: aload #4
    //   199: astore #15
    //   201: iload #5
    //   203: istore #13
    //   205: aload #6
    //   207: astore #15
    //   209: aload #7
    //   211: astore #15
    //   213: aload #12
    //   215: aload #16
    //   217: invokespecial <init> : (Ljava/io/Reader;)V
    //   220: aload #12
    //   222: astore #10
    //   224: aload_2
    //   225: astore #9
    //   227: aload_3
    //   228: astore #8
    //   230: aload #4
    //   232: astore #8
    //   234: iload #5
    //   236: istore #13
    //   238: aload #6
    //   240: astore #8
    //   242: aload #10
    //   244: astore #8
    //   246: aload_2
    //   247: astore #14
    //   249: aload_3
    //   250: astore #15
    //   252: aload #4
    //   254: astore #15
    //   256: iload #5
    //   258: istore #13
    //   260: aload #6
    //   262: astore #15
    //   264: aload #10
    //   266: astore #15
    //   268: aload #10
    //   270: invokevirtual readLine : ()Ljava/lang/String;
    //   273: astore #12
    //   275: aload #12
    //   277: ifnull -> 781
    //   280: aload #12
    //   282: aload_3
    //   283: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   286: istore #17
    //   288: iload #17
    //   290: ifeq -> 382
    //   293: ldc 'OplusOtaUtils'
    //   295: ldc 'update package not found!!!!!!'
    //   297: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   300: pop
    //   301: aload_1
    //   302: ldc 'errType'
    //   304: iconst_m1
    //   305: invokevirtual putExtra : (Ljava/lang/String;I)Landroid/content/Intent;
    //   308: pop
    //   309: aload_1
    //   310: ldc 'errLine'
    //   312: aload #12
    //   314: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    //   317: pop
    //   318: aload_0
    //   319: aload_1
    //   320: invokevirtual sendBroadcast : (Landroid/content/Intent;)V
    //   323: new java/lang/StringBuilder
    //   326: astore #4
    //   328: aload #4
    //   330: invokespecial <init> : ()V
    //   333: aload #4
    //   335: ldc 'error log is "'
    //   337: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   340: pop
    //   341: aload #4
    //   343: aload #12
    //   345: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   348: pop
    //   349: aload #4
    //   351: ldc '"'
    //   353: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   356: pop
    //   357: ldc 'OplusOtaUtils'
    //   359: aload #4
    //   361: invokevirtual toString : ()Ljava/lang/String;
    //   364: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   367: pop
    //   368: iconst_1
    //   369: istore #5
    //   371: goto -> 785
    //   374: astore_0
    //   375: goto -> 896
    //   378: astore_0
    //   379: goto -> 861
    //   382: aload #12
    //   384: aload #4
    //   386: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   389: istore #17
    //   391: iload #17
    //   393: ifeq -> 478
    //   396: ldc 'OplusOtaUtils'
    //   398: ldc 'signature verification failed!!!!!!'
    //   400: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   403: pop
    //   404: aload_1
    //   405: ldc 'errType'
    //   407: bipush #-2
    //   409: invokevirtual putExtra : (Ljava/lang/String;I)Landroid/content/Intent;
    //   412: pop
    //   413: aload_1
    //   414: ldc 'errLine'
    //   416: aload #12
    //   418: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    //   421: pop
    //   422: aload_0
    //   423: aload_1
    //   424: invokevirtual sendBroadcast : (Landroid/content/Intent;)V
    //   427: new java/lang/StringBuilder
    //   430: astore #4
    //   432: aload #4
    //   434: invokespecial <init> : ()V
    //   437: aload #4
    //   439: ldc 'error log is "'
    //   441: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   444: pop
    //   445: aload #4
    //   447: aload #12
    //   449: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   452: pop
    //   453: aload #4
    //   455: ldc '"'
    //   457: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   460: pop
    //   461: ldc 'OplusOtaUtils'
    //   463: aload #4
    //   465: invokevirtual toString : ()Ljava/lang/String;
    //   468: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   471: pop
    //   472: iconst_1
    //   473: istore #5
    //   475: goto -> 785
    //   478: aload #12
    //   480: ldc 'has unexpected contents'
    //   482: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   485: istore #17
    //   487: iload #17
    //   489: ifeq -> 574
    //   492: ldc 'OplusOtaUtils'
    //   494: ldc 'some file not match original!!!!!!'
    //   496: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   499: pop
    //   500: aload_1
    //   501: ldc 'errType'
    //   503: bipush #-3
    //   505: invokevirtual putExtra : (Ljava/lang/String;I)Landroid/content/Intent;
    //   508: pop
    //   509: aload_1
    //   510: ldc 'errLine'
    //   512: aload #12
    //   514: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    //   517: pop
    //   518: aload_0
    //   519: aload_1
    //   520: invokevirtual sendBroadcast : (Landroid/content/Intent;)V
    //   523: new java/lang/StringBuilder
    //   526: astore #4
    //   528: aload #4
    //   530: invokespecial <init> : ()V
    //   533: aload #4
    //   535: ldc 'error log is "'
    //   537: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   540: pop
    //   541: aload #4
    //   543: aload #12
    //   545: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   548: pop
    //   549: aload #4
    //   551: ldc '"'
    //   553: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   556: pop
    //   557: ldc 'OplusOtaUtils'
    //   559: aload #4
    //   561: invokevirtual toString : ()Ljava/lang/String;
    //   564: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   567: pop
    //   568: iconst_1
    //   569: istore #5
    //   571: goto -> 785
    //   574: aload #12
    //   576: ldc 'Not enough free space on'
    //   578: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   581: istore #17
    //   583: iload #17
    //   585: ifeq -> 670
    //   588: ldc 'OplusOtaUtils'
    //   590: ldc 'cache have no enough space!!!!!!'
    //   592: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   595: pop
    //   596: aload_1
    //   597: ldc 'errType'
    //   599: bipush #-4
    //   601: invokevirtual putExtra : (Ljava/lang/String;I)Landroid/content/Intent;
    //   604: pop
    //   605: aload_1
    //   606: ldc 'errLine'
    //   608: aload #12
    //   610: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    //   613: pop
    //   614: aload_0
    //   615: aload_1
    //   616: invokevirtual sendBroadcast : (Landroid/content/Intent;)V
    //   619: new java/lang/StringBuilder
    //   622: astore #4
    //   624: aload #4
    //   626: invokespecial <init> : ()V
    //   629: aload #4
    //   631: ldc 'error log is "'
    //   633: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   636: pop
    //   637: aload #4
    //   639: aload #12
    //   641: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   644: pop
    //   645: aload #4
    //   647: ldc '"'
    //   649: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   652: pop
    //   653: ldc 'OplusOtaUtils'
    //   655: aload #4
    //   657: invokevirtual toString : ()Ljava/lang/String;
    //   660: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   663: pop
    //   664: iconst_1
    //   665: istore #5
    //   667: goto -> 785
    //   670: aload #12
    //   672: ldc 'decryptFile file fail, stop install'
    //   674: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   677: ifeq -> 762
    //   680: ldc 'OplusOtaUtils'
    //   682: ldc 'package decrypt fail!!!!!!'
    //   684: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   687: pop
    //   688: aload_1
    //   689: ldc 'errType'
    //   691: iload #5
    //   693: invokevirtual putExtra : (Ljava/lang/String;I)Landroid/content/Intent;
    //   696: pop
    //   697: aload_1
    //   698: ldc 'errLine'
    //   700: aload #12
    //   702: invokevirtual putExtra : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    //   705: pop
    //   706: aload_0
    //   707: aload_1
    //   708: invokevirtual sendBroadcast : (Landroid/content/Intent;)V
    //   711: new java/lang/StringBuilder
    //   714: astore #4
    //   716: aload #4
    //   718: invokespecial <init> : ()V
    //   721: aload #4
    //   723: ldc 'error log is "'
    //   725: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   728: pop
    //   729: aload #4
    //   731: aload #12
    //   733: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   736: pop
    //   737: aload #4
    //   739: ldc '"'
    //   741: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   744: pop
    //   745: ldc 'OplusOtaUtils'
    //   747: aload #4
    //   749: invokevirtual toString : ()Ljava/lang/String;
    //   752: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   755: pop
    //   756: iconst_1
    //   757: istore #5
    //   759: goto -> 785
    //   762: goto -> 224
    //   765: astore_0
    //   766: goto -> 896
    //   769: astore_0
    //   770: goto -> 861
    //   773: astore_0
    //   774: goto -> 896
    //   777: astore_0
    //   778: goto -> 861
    //   781: iload #11
    //   783: istore #5
    //   785: iload #5
    //   787: ifne -> 806
    //   790: aload_0
    //   791: aload_1
    //   792: invokevirtual sendBroadcast : (Landroid/content/Intent;)V
    //   795: goto -> 806
    //   798: astore_0
    //   799: goto -> 896
    //   802: astore_0
    //   803: goto -> 861
    //   806: aload #10
    //   808: invokevirtual close : ()V
    //   811: goto -> 920
    //   814: astore_0
    //   815: ldc 'OplusOtaUtils'
    //   817: aload_2
    //   818: aload_0
    //   819: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   822: pop
    //   823: goto -> 920
    //   826: astore_0
    //   827: aload #9
    //   829: astore_2
    //   830: aload #8
    //   832: astore #10
    //   834: goto -> 896
    //   837: astore_0
    //   838: aload #14
    //   840: astore_2
    //   841: aload #15
    //   843: astore #10
    //   845: goto -> 861
    //   848: astore_0
    //   849: aload #9
    //   851: astore #10
    //   853: goto -> 896
    //   856: astore_0
    //   857: aload #8
    //   859: astore #10
    //   861: ldc 'OplusOtaUtils'
    //   863: ldc 'get OTA error message failed!!!'
    //   865: aload_0
    //   866: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   869: pop
    //   870: aload #10
    //   872: ifnull -> 920
    //   875: aload #10
    //   877: invokevirtual close : ()V
    //   880: goto -> 920
    //   883: astore_0
    //   884: ldc 'OplusOtaUtils'
    //   886: aload_2
    //   887: aload_0
    //   888: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   891: pop
    //   892: goto -> 920
    //   895: astore_0
    //   896: aload #10
    //   898: ifnull -> 918
    //   901: aload #10
    //   903: invokevirtual close : ()V
    //   906: goto -> 918
    //   909: astore_1
    //   910: ldc 'OplusOtaUtils'
    //   912: aload_2
    //   913: aload_1
    //   914: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   917: pop
    //   918: aload_0
    //   919: athrow
    //   920: ldc 'OplusOtaUtils'
    //   922: ldc 'deal ota log pass!!!'
    //   924: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   927: pop
    //   928: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #116	-> 0
    //   #117	-> 6
    //   #118	-> 10
    //   #119	-> 10
    //   #120	-> 10
    //   #122	-> 10
    //   #123	-> 10
    //   #124	-> 10
    //   #125	-> 10
    //   #126	-> 10
    //   #128	-> 14
    //   #129	-> 14
    //   #131	-> 25
    //   #132	-> 33
    //   #133	-> 41
    //   #135	-> 49
    //   #136	-> 49
    //   #137	-> 61
    //   #140	-> 64
    //   #141	-> 224
    //   #142	-> 280
    //   #143	-> 293
    //   #145	-> 301
    //   #146	-> 309
    //   #147	-> 318
    //   #148	-> 323
    //   #149	-> 368
    //   #150	-> 368
    //   #195	-> 374
    //   #192	-> 378
    //   #151	-> 382
    //   #152	-> 396
    //   #154	-> 404
    //   #155	-> 413
    //   #156	-> 422
    //   #157	-> 427
    //   #158	-> 472
    //   #159	-> 472
    //   #160	-> 478
    //   #161	-> 492
    //   #163	-> 500
    //   #164	-> 509
    //   #165	-> 518
    //   #166	-> 523
    //   #167	-> 568
    //   #168	-> 568
    //   #169	-> 574
    //   #170	-> 588
    //   #172	-> 596
    //   #173	-> 605
    //   #174	-> 614
    //   #175	-> 619
    //   #176	-> 664
    //   #177	-> 664
    //   #178	-> 670
    //   #179	-> 680
    //   #181	-> 688
    //   #182	-> 697
    //   #183	-> 706
    //   #184	-> 711
    //   #185	-> 756
    //   #186	-> 756
    //   #178	-> 762
    //   #195	-> 765
    //   #192	-> 769
    //   #195	-> 773
    //   #192	-> 777
    //   #141	-> 781
    //   #189	-> 785
    //   #190	-> 790
    //   #195	-> 798
    //   #192	-> 802
    //   #195	-> 806
    //   #197	-> 806
    //   #200	-> 811
    //   #198	-> 814
    //   #199	-> 815
    //   #200	-> 823
    //   #195	-> 826
    //   #192	-> 837
    //   #195	-> 848
    //   #192	-> 856
    //   #193	-> 861
    //   #195	-> 870
    //   #197	-> 875
    //   #200	-> 880
    //   #198	-> 883
    //   #199	-> 884
    //   #200	-> 892
    //   #195	-> 895
    //   #197	-> 901
    //   #200	-> 906
    //   #198	-> 909
    //   #199	-> 910
    //   #202	-> 918
    //   #132	-> 920
    //   #204	-> 920
    //   #205	-> 928
    // Exception table:
    //   from	to	target	type
    //   64	69	856	java/io/IOException
    //   64	69	848	finally
    //   113	118	837	java/io/IOException
    //   113	118	826	finally
    //   162	169	837	java/io/IOException
    //   162	169	826	finally
    //   213	220	837	java/io/IOException
    //   213	220	826	finally
    //   268	275	837	java/io/IOException
    //   268	275	826	finally
    //   280	288	777	java/io/IOException
    //   280	288	773	finally
    //   293	301	378	java/io/IOException
    //   293	301	374	finally
    //   301	309	378	java/io/IOException
    //   301	309	374	finally
    //   309	318	378	java/io/IOException
    //   309	318	374	finally
    //   318	323	378	java/io/IOException
    //   318	323	374	finally
    //   323	368	378	java/io/IOException
    //   323	368	374	finally
    //   382	391	769	java/io/IOException
    //   382	391	765	finally
    //   396	404	378	java/io/IOException
    //   396	404	374	finally
    //   404	413	378	java/io/IOException
    //   404	413	374	finally
    //   413	422	378	java/io/IOException
    //   413	422	374	finally
    //   422	427	378	java/io/IOException
    //   422	427	374	finally
    //   427	472	378	java/io/IOException
    //   427	472	374	finally
    //   478	487	769	java/io/IOException
    //   478	487	765	finally
    //   492	500	378	java/io/IOException
    //   492	500	374	finally
    //   500	509	378	java/io/IOException
    //   500	509	374	finally
    //   509	518	378	java/io/IOException
    //   509	518	374	finally
    //   518	523	378	java/io/IOException
    //   518	523	374	finally
    //   523	568	378	java/io/IOException
    //   523	568	374	finally
    //   574	583	769	java/io/IOException
    //   574	583	765	finally
    //   588	596	378	java/io/IOException
    //   588	596	374	finally
    //   596	605	378	java/io/IOException
    //   596	605	374	finally
    //   605	614	378	java/io/IOException
    //   605	614	374	finally
    //   614	619	378	java/io/IOException
    //   614	619	374	finally
    //   619	664	378	java/io/IOException
    //   619	664	374	finally
    //   670	680	769	java/io/IOException
    //   670	680	765	finally
    //   680	688	769	java/io/IOException
    //   680	688	765	finally
    //   688	697	802	java/io/IOException
    //   688	697	798	finally
    //   697	706	802	java/io/IOException
    //   697	706	798	finally
    //   706	711	802	java/io/IOException
    //   706	711	798	finally
    //   711	756	802	java/io/IOException
    //   711	756	798	finally
    //   790	795	802	java/io/IOException
    //   790	795	798	finally
    //   806	811	814	java/io/IOException
    //   861	870	895	finally
    //   875	880	883	java/io/IOException
    //   901	906	909	java/io/IOException
  }
  
  private static String readOTAUpdateResult(String paramString) {
    String str1, str6, str2 = null, str3 = null, str4 = null;
    String str5 = null;
    BufferedReader bufferedReader1 = null;
    File file = new File(paramString);
    BufferedReader bufferedReader2 = bufferedReader1;
    paramString = str5;
    try {
      BufferedReader bufferedReader4 = new BufferedReader();
      bufferedReader2 = bufferedReader1;
      paramString = str5;
      FileReader fileReader = new FileReader();
      bufferedReader2 = bufferedReader1;
      paramString = str5;
      this(file);
      bufferedReader2 = bufferedReader1;
      paramString = str5;
      this(fileReader);
      bufferedReader1 = bufferedReader4;
      bufferedReader2 = bufferedReader1;
      BufferedReader bufferedReader3 = bufferedReader1;
      str1 = str5 = bufferedReader1.readLine();
      str6 = str1;
      try {
        bufferedReader1.close();
        str6 = str1;
      } catch (IOException iOException) {
        Slog.e("OplusOtaUtils", "readOTAUpdateResult close the reader failed!!!", iOException);
        str1 = str6;
        str6 = str1;
      } 
    } catch (IOException iOException) {
      str6 = str1;
      Slog.e("OplusOtaUtils", "readOTAUpdateResult failed!!!", iOException);
      str6 = str3;
      if (str1 != null) {
        str6 = str2;
        str1.close();
        str1 = str4;
      } else {
        return str6;
      } 
      str6 = str1;
    } finally {}
    return str6;
  }
  
  private static String readFFUUpdateResult() {
    String str1;
    BufferedReader bufferedReader1 = null;
    String str2 = null;
    BufferedReader bufferedReader2 = null, bufferedReader3 = null;
    File file = new File("/cache/recovery/last_ffu");
    if (file.exists()) {
      bufferedReader1 = bufferedReader3;
      String str = str2;
      BufferedReader bufferedReader = bufferedReader2;
      try {
        BufferedReader bufferedReader4 = new BufferedReader();
        bufferedReader1 = bufferedReader3;
        str = str2;
        bufferedReader = bufferedReader2;
        FileReader fileReader = new FileReader();
        bufferedReader1 = bufferedReader3;
        str = str2;
        bufferedReader = bufferedReader2;
        this(file);
        bufferedReader1 = bufferedReader3;
        str = str2;
        bufferedReader = bufferedReader2;
        this(fileReader);
        bufferedReader3 = bufferedReader4;
        bufferedReader1 = bufferedReader3;
        str = str2;
        bufferedReader = bufferedReader3;
        str2 = bufferedReader3.readLine();
        bufferedReader1 = bufferedReader3;
        str = str2;
        bufferedReader = bufferedReader3;
        StringBuilder stringBuilder = new StringBuilder();
        bufferedReader1 = bufferedReader3;
        str = str2;
        bufferedReader = bufferedReader3;
        this();
        bufferedReader1 = bufferedReader3;
        str = str2;
        bufferedReader = bufferedReader3;
        stringBuilder.append("readFFUUpdateResult resultStr=");
        bufferedReader1 = bufferedReader3;
        str = str2;
        bufferedReader = bufferedReader3;
        stringBuilder.append(str2);
        bufferedReader1 = bufferedReader3;
        str = str2;
        bufferedReader = bufferedReader3;
        Slog.i("OplusOtaUtils", stringBuilder.toString());
        str1 = str2;
        try {
          bufferedReader3.close();
          str = str2;
          str1 = str;
        } catch (IOException iOException) {
          Slog.e("OplusOtaUtils", "readFFUUpdateResult close the reader failed!!!", iOException);
          str = str1;
          str1 = str;
        } 
      } catch (IOException iOException) {
        bufferedReader1 = bufferedReader;
        Slog.e("OplusOtaUtils", "readFFUUpdateResult failed!!!", iOException);
        str1 = str;
        if (bufferedReader != null) {
          str1 = str;
          bufferedReader.close();
        } else {
          return str1;
        } 
        str1 = str;
      } finally {}
    } 
    return str1;
  }
}
