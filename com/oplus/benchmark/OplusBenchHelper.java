package com.oplus.benchmark;

import android.app.ActivityThread;
import android.common.OplusFeatureCache;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.PerformanceManager;
import android.os.SystemProperties;
import android.util.Log;
import com.oplus.screenmode.IOplusScreenModeFeature;
import java.io.FileWriter;

public final class OplusBenchHelper {
  private static final String BENCH_MARK_ANTUTU = "com.antutu.ABenchMark";
  
  private static final String BENCH_MARK_ANTUTU_3D = "com.antutu.benchmark.full";
  
  private static final int BENCH_MARK_ANTUTU_FINISHED = -2;
  
  private static final int BENCH_MARK_ANTUTU_FIRST_STEP = 31;
  
  static final int BENCH_MARK_ANTUTU_MULTITASK = 11;
  
  static final int BENCH_MARK_ANTUTU_MULTITHREAD = 10;
  
  private static final int BENCH_MARK_ANTUTU_UX_FIRST_STEP = 6;
  
  static final String BENCH_MARK_COMPACT_MEMORY_PROC = "/proc/sys/vm/compact_memory";
  
  private static final String BENCH_MARK_LUDASHI = "com.ludashi.benchmark";
  
  private static final String BENCH_MODE_DISABLE = "0";
  
  private static final String BENCH_MODE_ENABLE = "1";
  
  private static final String BENCH_MODE_ENABLE_WITH_JPEG_MUTIL = "2";
  
  private static final long BITMAP_CACHE_TIMEOUT = 1000L;
  
  private static final boolean DEBUG = SystemProperties.getBoolean("ro.build.release_type", false) ^ true;
  
  private static final String SYSTEM_PROPERTIES_SPEC = "sys.oplus.high.performance.spec";
  
  private static final String TAG = "OplusBenchHelper";
  
  private static Bitmap bitmapCache;
  
  private static int lastResId;
  
  private static String lastResStr = "";
  
  private static long lastTimestamp;
  
  private static final Object mLock;
  
  private static OplusBenchHelper sInstance;
  
  static {
    lastResId = -999;
    lastTimestamp = -999L;
    bitmapCache = null;
    sInstance = null;
    mLock = new Object();
  }
  
  public static OplusBenchHelper getInstance() {
    synchronized (mLock) {
      if (sInstance == null) {
        OplusBenchHelper oplusBenchHelper = new OplusBenchHelper();
        this();
        sInstance = oplusBenchHelper;
      } 
      return sInstance;
    } 
  }
  
  public boolean isEnableBitmapCache() {
    String str = SystemProperties.get("sys.oplus.high.performance.spec", "0");
    if ("1".equals(str) && 
      "com.ludashi.benchmark".equals(ActivityThread.currentPackageName()))
      return true; 
    Bitmap bitmap = bitmapCache;
    if (bitmap != null) {
      bitmap.recycle();
      bitmapCache = null;
    } 
    return false;
  }
  
  public Bitmap getBitmapCache(Resources paramResources, int paramInt, BitmapFactory.Options paramOptions) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual isEnableBitmapCache : ()Z
    //   4: ifne -> 9
    //   7: aconst_null
    //   8: areturn
    //   9: getstatic com/oplus/benchmark/OplusBenchHelper.mLock : Ljava/lang/Object;
    //   12: astore #4
    //   14: aload #4
    //   16: monitorenter
    //   17: aload_3
    //   18: ifnonnull -> 96
    //   21: getstatic com/oplus/benchmark/OplusBenchHelper.lastResId : I
    //   24: iload_2
    //   25: if_icmpne -> 96
    //   28: getstatic com/oplus/benchmark/OplusBenchHelper.lastResStr : Ljava/lang/String;
    //   31: astore_3
    //   32: aload_3
    //   33: aload_1
    //   34: invokevirtual toString : ()Ljava/lang/String;
    //   37: invokevirtual equals : (Ljava/lang/Object;)Z
    //   40: ifeq -> 96
    //   43: getstatic com/oplus/benchmark/OplusBenchHelper.bitmapCache : Landroid/graphics/Bitmap;
    //   46: ifnull -> 96
    //   49: invokestatic currentTimeMillis : ()J
    //   52: getstatic com/oplus/benchmark/OplusBenchHelper.lastTimestamp : J
    //   55: lsub
    //   56: ldc2_w 1000
    //   59: lcmp
    //   60: ifge -> 96
    //   63: getstatic com/oplus/benchmark/OplusBenchHelper.DEBUG : Z
    //   66: ifeq -> 78
    //   69: ldc 'OplusBenchHelper'
    //   71: ldc_w 'using bitmap cache'
    //   74: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   77: pop
    //   78: invokestatic currentTimeMillis : ()J
    //   81: putstatic com/oplus/benchmark/OplusBenchHelper.lastTimestamp : J
    //   84: getstatic com/oplus/benchmark/OplusBenchHelper.bitmapCache : Landroid/graphics/Bitmap;
    //   87: invokestatic createBitmap : (Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    //   90: astore_1
    //   91: aload #4
    //   93: monitorexit
    //   94: aload_1
    //   95: areturn
    //   96: aload #4
    //   98: monitorexit
    //   99: aconst_null
    //   100: areturn
    //   101: astore_1
    //   102: aload #4
    //   104: monitorexit
    //   105: aload_1
    //   106: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #106	-> 0
    //   #107	-> 7
    //   #110	-> 9
    //   #111	-> 17
    //   #113	-> 32
    //   #115	-> 49
    //   #117	-> 63
    //   #118	-> 69
    //   #120	-> 78
    //   #121	-> 84
    //   #123	-> 96
    //   #125	-> 101
    // Exception table:
    //   from	to	target	type
    //   21	32	101	finally
    //   32	49	101	finally
    //   49	63	101	finally
    //   63	69	101	finally
    //   69	78	101	finally
    //   78	84	101	finally
    //   84	94	101	finally
    //   96	99	101	finally
    //   102	105	101	finally
  }
  
  public void setBitmapCache(Bitmap paramBitmap, Resources paramResources, int paramInt) {
    if (!isEnableBitmapCache())
      return; 
    synchronized (mLock) {
      lastResStr = paramResources.toString();
      lastResId = paramInt;
      bitmapCache = paramBitmap;
      lastTimestamp = System.currentTimeMillis();
      if (DEBUG)
        Log.i("OplusBenchHelper", "caching bitmap"); 
      return;
    } 
  }
  
  public static boolean isInBenchMode() {
    String str = SystemProperties.get("sys.oplus.high.performance.spec", "0");
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isInBenchMode:");
      stringBuilder.append(str);
      Log.d("OplusBenchHelper", stringBuilder.toString());
    } 
    if ("0".equals(str))
      return false; 
    return true;
  }
  
  public static boolean isAntutuApp(String paramString) {
    if (isAntutuMainApp(paramString) || isAntutu3DApp(paramString))
      return true; 
    return false;
  }
  
  public static boolean isAntutuMainApp(String paramString) {
    if ("com.antutu.ABenchMark".equals(paramString))
      return true; 
    return false;
  }
  
  public static boolean isAntutu3DApp(String paramString) {
    if ("com.antutu.benchmark.full".equals(paramString))
      return true; 
    return false;
  }
  
  private boolean isEnableMultiThreadOptimize(Context paramContext) {
    try {
      PackageManager packageManager = paramContext.getPackageManager();
      PackageInfo packageInfo = packageManager.getPackageInfo("com.antutu.ABenchMark", 131072);
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("bench app version: ");
        stringBuilder.append(packageInfo.versionName);
        Log.d("OplusBenchHelper", stringBuilder.toString());
      } 
      boolean bool = packageInfo.versionName.startsWith("8.");
      if (bool)
        return true; 
      return false;
    } catch (Exception exception) {
      Log.e("OplusBenchHelper", "get app version failed");
      return false;
    } 
  }
  
  public void benchStepCheck(Context paramContext, Intent paramIntent) {
    String str = paramIntent.getPackage();
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("benchMode:");
      stringBuilder.append(isInBenchMode());
      stringBuilder.append("; pkgName");
      stringBuilder.append(str);
      Log.d("OplusBenchHelper", stringBuilder.toString());
    } 
    if (!isInBenchMode() || !isAntutuMainApp(str))
      return; 
    if (!paramIntent.hasExtra("uid"))
      return; 
    int i = paramIntent.getIntExtra("uid", -999);
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("bm_uid:");
      stringBuilder.append(i);
      Log.d("OplusBenchHelper", stringBuilder.toString());
    } 
    if (i != -2) {
      if (i != 6) {
        if (i != 31) {
          if (i != 10) {
            if (i != 11)
              return; 
            if (isEnableMultiThreadOptimize(paramContext))
              PerformanceManager.disableMultiThreadOptimize(); 
          } else if (isEnableMultiThreadOptimize(paramContext)) {
            PerformanceManager.enableMultiThreadOptimize();
          } 
        } else {
          ((IOplusScreenModeFeature)OplusFeatureCache.get(IOplusScreenModeFeature.DEFAULT)).enterPSModeOnRate(true, 60);
          SystemProperties.set("sys.oplus.high.performance.spec", "1");
          if (DEBUG)
            Log.d("OplusBenchHelper", "BENCH_MARK_ANTUTU_FIRST_STEP"); 
        } 
      } else {
        ((IOplusScreenModeFeature)OplusFeatureCache.get(IOplusScreenModeFeature.DEFAULT)).enterPSMode(true);
        SystemProperties.set("sys.oplus.high.performance.spec", "2");
        if (DEBUG)
          Log.d("OplusBenchHelper", "BENCH_MARK_ANTUTU_UX_FIRST_STEP"); 
      } 
    } else {
      ((IOplusScreenModeFeature)OplusFeatureCache.get(IOplusScreenModeFeature.DEFAULT)).enterPSMode(false);
      SystemProperties.set("sys.oplus.high.performance.spec", "1");
      if (DEBUG)
        Log.d("OplusBenchHelper", "BENCH_MARK_ANTUTU_FINISHED"); 
    } 
  }
  
  public static void handleCompactMemory() {
    FileWriter fileWriter1 = null, fileWriter2 = null;
    FileWriter fileWriter3 = fileWriter2, fileWriter4 = fileWriter1;
    try {
      FileWriter fileWriter = new FileWriter();
      fileWriter3 = fileWriter2;
      fileWriter4 = fileWriter1;
      this("/proc/sys/vm/compact_memory");
      fileWriter3 = fileWriter;
      fileWriter4 = fileWriter;
      fileWriter.write("1");
      fileWriter3 = fileWriter;
      fileWriter4 = fileWriter;
      if (DEBUG) {
        fileWriter3 = fileWriter;
        fileWriter4 = fileWriter;
        Log.e("OplusBenchHelper", "handle compact mem success");
      } 
      try {
        fileWriter.close();
      } catch (Exception exception) {}
    } catch (Exception exception1) {
      exception1 = exception;
      if (DEBUG) {
        exception1 = exception;
        Log.e("OplusBenchHelper", "handle compact mem error");
      } 
      if (exception != null)
        exception.close(); 
    } finally {}
  }
}
