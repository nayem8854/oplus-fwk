package com.oplus.exsystemservice;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusExSystemService extends IInterface {
  void onBootPhase(int paramInt) throws RemoteException;
  
  class Default implements IOplusExSystemService {
    public void onBootPhase(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusExSystemService {
    private static final String DESCRIPTOR = "com.oplus.exsystemservice.IOplusExSystemService";
    
    static final int TRANSACTION_onBootPhase = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.exsystemservice.IOplusExSystemService");
    }
    
    public static IOplusExSystemService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.exsystemservice.IOplusExSystemService");
      if (iInterface != null && iInterface instanceof IOplusExSystemService)
        return (IOplusExSystemService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onBootPhase";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.exsystemservice.IOplusExSystemService");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.exsystemservice.IOplusExSystemService");
      param1Int1 = param1Parcel1.readInt();
      onBootPhase(param1Int1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IOplusExSystemService {
      public static IOplusExSystemService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.exsystemservice.IOplusExSystemService";
      }
      
      public void onBootPhase(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.exsystemservice.IOplusExSystemService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusExSystemService.Stub.getDefaultImpl() != null) {
            IOplusExSystemService.Stub.getDefaultImpl().onBootPhase(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusExSystemService param1IOplusExSystemService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusExSystemService != null) {
          Proxy.sDefaultImpl = param1IOplusExSystemService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusExSystemService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
