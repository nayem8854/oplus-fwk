package com.oplus.edgetouch;

import android.app.OplusActivityTaskManager;
import android.content.Context;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.util.Log;
import java.util.List;
import java.util.Map;

public class OplusEdgeTouchManager {
  private static final boolean DEBUG;
  
  public static int EDGE_TOUCH_VERSION = 1;
  
  private static final String TAG = "OplusEdgeTouchManager";
  
  private static OplusEdgeTouchManager sInstance;
  
  static {
    DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  }
  
  public static OplusEdgeTouchManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/edgetouch/OplusEdgeTouchManager.sInstance : Lcom/oplus/edgetouch/OplusEdgeTouchManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/edgetouch/OplusEdgeTouchManager
    //   8: monitorenter
    //   9: getstatic com/oplus/edgetouch/OplusEdgeTouchManager.sInstance : Lcom/oplus/edgetouch/OplusEdgeTouchManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/edgetouch/OplusEdgeTouchManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/edgetouch/OplusEdgeTouchManager.sInstance : Lcom/oplus/edgetouch/OplusEdgeTouchManager;
    //   27: ldc com/oplus/edgetouch/OplusEdgeTouchManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/edgetouch/OplusEdgeTouchManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/edgetouch/OplusEdgeTouchManager.sInstance : Lcom/oplus/edgetouch/OplusEdgeTouchManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #39	-> 0
    //   #40	-> 6
    //   #41	-> 9
    //   #42	-> 15
    //   #44	-> 27
    //   #46	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  private OplusActivityTaskManager mOAms = new OplusActivityTaskManager();
  
  public boolean isSupport() {
    if (DEBUG)
      Log.i("OplusEdgeTouchManager", "isSupport"); 
    try {
      if (this.mOAms != null)
        return this.mOAms.isSupportEdgeTouchPrevent(); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isSupport remoteException ");
      stringBuilder.append(remoteException);
      Log.e("OplusEdgeTouchManager", stringBuilder.toString());
      remoteException.printStackTrace();
    } 
    return false;
  }
  
  public boolean writeParam(Context paramContext, String paramString, List<String> paramList) {
    if (paramContext == null)
      return false; 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setParam  callPkg = ");
      stringBuilder.append(paramContext.getPackageName());
      stringBuilder.append("  scenePkg = ");
      stringBuilder.append(paramString);
      stringBuilder.append(" paramCmdList = \n");
      stringBuilder.append(paramList);
      Log.i("OplusEdgeTouchManager", stringBuilder.toString());
    } 
    if (paramList == null || paramList.isEmpty())
      return false; 
    try {
      if (this.mOAms != null)
        return this.mOAms.writeEdgeTouchPreventParam(paramContext.getPackageName(), paramString, paramList); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setParam remoteException ");
      stringBuilder.append(remoteException);
      Log.e("OplusEdgeTouchManager", stringBuilder.toString());
      remoteException.printStackTrace();
    } 
    return false;
  }
  
  public void setDefaultParam(Context paramContext, List<String> paramList) {
    if (paramContext == null)
      return; 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setDefaultParam  callPkg = ");
      stringBuilder.append(paramContext.getPackageName());
      stringBuilder.append("  paramCmdList = \n");
      stringBuilder.append(paramList);
      Log.i("OplusEdgeTouchManager", stringBuilder.toString());
    } 
    if (paramList == null || paramList.isEmpty())
      return; 
    try {
      if (this.mOAms != null)
        this.mOAms.setDefaultEdgeTouchPreventParam(paramContext.getPackageName(), paramList); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setDefaultParam remoteException ");
      stringBuilder.append(remoteException);
      Log.e("OplusEdgeTouchManager", stringBuilder.toString());
      remoteException.printStackTrace();
    } 
  }
  
  public boolean resetDefaultParam(Context paramContext) {
    if (paramContext == null)
      return false; 
    if (DEBUG)
      Log.i("OplusEdgeTouchManager", "resetDefaultParam "); 
    try {
      if (this.mOAms != null)
        return this.mOAms.resetDefaultEdgeTouchPreventParam(paramContext.getPackageName()); 
    } catch (RemoteException remoteException) {
      Log.e("OplusEdgeTouchManager", "resetDefaultParam remoteException ");
      remoteException.printStackTrace();
    } 
    return false;
  }
  
  public void setRules(Context paramContext, Map<String, List<String>> paramMap) {
    if (paramContext == null)
      return; 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setRules ");
      stringBuilder.append(paramMap);
      Log.i("OplusEdgeTouchManager", stringBuilder.toString());
    } 
    if (paramMap == null || paramMap.isEmpty())
      return; 
    try {
      if (this.mOAms != null)
        this.mOAms.setEdgeTouchCallRules(paramContext.getPackageName(), paramMap); 
    } catch (RemoteException remoteException) {
      Log.e("OplusEdgeTouchManager", "resetDefaultParam remoteException ");
      remoteException.printStackTrace();
    } 
  }
}
