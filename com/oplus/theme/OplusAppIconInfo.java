package com.oplus.theme;

import android.content.pm.ApplicationInfo;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class OplusAppIconInfo {
  public static final String ALL_APPS = "allApps.xml";
  
  private static ArrayList<String> mAllIconNames;
  
  private static ArrayList<String> mAllPackageNames;
  
  private static String mCurrentTag = null;
  
  private static Map<String, String> mDiffPackages;
  
  private static boolean sParsered;
  
  static {
    mAllIconNames = new ArrayList<>();
    mAllPackageNames = new ArrayList<>();
    HashMap<Object, Object> hashMap = new HashMap<>();
    sParsered = false;
    hashMap.put("ic_launcher_stk.png", "com.android.stk");
    mDiffPackages.put("ic_launcher_wallet.png", "com.finshell.wallet");
    mDiffPackages.put("ic_launcher_bbs.png", "com.coloros.bbs");
    mDiffPackages.put("ic_launcher_calender.png", "com.coloros.calendar");
    mDiffPackages.put("ic_launcher_encryptiont.png", "com.coloros.encryptiont");
    mDiffPackages.put("ic_launcher_findmyphone.png", "com.coloros.findmyphone");
    mDiffPackages.put("ic_launcher_gamespace.png", "com.coloros.gamespaceui");
    mDiffPackages.put("ic_launcher_shortcuts.png", "com.coloros.shortcuts");
    mDiffPackages.put("ic_launcher_videoeditor.png", "com.coloros.videoeditor");
    mDiffPackages.put("ic_launcher_browser.png", "com.heytap.browser");
    mDiffPackages.put("ic_launcher_book_store.png", "com.heytap.book");
    mDiffPackages.put("ic_launcher_health.png", "com.heytap.health");
    mDiffPackages.put("ic_launcher_nearme_market.png", "com.heytap.market");
    mDiffPackages.put("ic_launcher_nearme_reader.png", "com.heytap.reader");
    mDiffPackages.put("ic_launcher_ohome.png", "com.heytap.smarthome");
    mDiffPackages.put("ic_launcher_speechassist.png", "com.heytap.speechassist");
    mDiffPackages.put("ic_launcher_themespace.png", "com.heytap.themestore");
    mDiffPackages.put("ic_launcher_nearme_usercenter.png", "com.heytap.usercenter");
    mDiffPackages.put("ic_launcher_yoli.png", "com.heytap.yoli");
    mDiffPackages.put("ic_launcher_store.png", "com.oppo.store");
    mDiffPackages.put("ic_launcher_music.png", "com.heytap.music");
  }
  
  static class IconXmlHandler extends DefaultHandler {
    public void startElement(String param1String1, String param1String2, String param1String3, Attributes param1Attributes) throws SAXException {
      OplusAppIconInfo.access$002(param1String2);
      if (OplusAppIconInfo.mCurrentTag.equalsIgnoreCase("icon")) {
        param1String1 = param1Attributes.getValue("name");
        if (param1String1 != null) {
          OplusAppIconInfo.mAllIconNames.add(param1String1);
        } else {
          OplusAppIconInfo.mAllIconNames.add("no_icon_name");
        } 
        param1String1 = param1Attributes.getValue("package");
        if (param1String1 != null) {
          OplusAppIconInfo.mAllPackageNames.add(param1String1);
        } else {
          OplusAppIconInfo.mAllIconNames.add("no_package_name");
        } 
      } 
    }
    
    public void characters(char[] param1ArrayOfchar, int param1Int1, int param1Int2) throws SAXException {}
    
    public void endElement(String param1String1, String param1String2, String param1String3) throws SAXException {
      OplusAppIconInfo.access$002(null);
    }
  }
  
  public static void parseXml(InputStream paramInputStream) throws Exception {
    SAXParserFactory sAXParserFactory = SAXParserFactory.newInstance();
    SAXParser sAXParser = sAXParserFactory.newSAXParser();
    sAXParser.parse(paramInputStream, new IconXmlHandler());
    paramInputStream.close();
  }
  
  public static boolean parseIconXml() {
    return false;
  }
  
  public static boolean parseIconXmlForUser(int paramInt) {
    if (sParsered)
      return true; 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(OplusThemeUtil.getDefaultThemePath());
    stringBuilder1.append("allApps.xml");
    String str = stringBuilder1.toString();
    StringBuilder stringBuilder2 = null;
    ZipFile zipFile1 = null, zipFile2 = null;
    InputStream inputStream2 = null, inputStream3 = null, inputStream4 = null, inputStream5 = null;
    mAllIconNames.clear();
    mAllPackageNames.clear();
    if (!OplusThirdPartUtil.mIsDefaultTheme) {
      FileInputStream fileInputStream;
      boolean bool1 = false, bool2 = false;
      stringBuilder1 = stringBuilder2;
      inputStream4 = inputStream5;
      ZipFile zipFile = zipFile1;
      inputStream3 = inputStream2;
      try {
        InputStream inputStream;
        FileInputStream fileInputStream2;
        ZipFile zipFile3;
        FileInputStream fileInputStream3;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder1 = stringBuilder2;
        inputStream4 = inputStream5;
        zipFile = zipFile1;
        inputStream3 = inputStream2;
        this();
        stringBuilder1 = stringBuilder2;
        inputStream4 = inputStream5;
        zipFile = zipFile1;
        inputStream3 = inputStream2;
        stringBuilder.append(OplusThirdPartUtil.getThemePathForUser(paramInt));
        stringBuilder1 = stringBuilder2;
        inputStream4 = inputStream5;
        zipFile = zipFile1;
        inputStream3 = inputStream2;
        stringBuilder.append("icons");
        stringBuilder1 = stringBuilder2;
        inputStream4 = inputStream5;
        zipFile = zipFile1;
        inputStream3 = inputStream2;
        String str1 = stringBuilder.toString();
        stringBuilder1 = stringBuilder2;
        inputStream4 = inputStream5;
        zipFile = zipFile1;
        inputStream3 = inputStream2;
        File file = new File();
        stringBuilder1 = stringBuilder2;
        inputStream4 = inputStream5;
        zipFile = zipFile1;
        inputStream3 = inputStream2;
        this(str1);
        stringBuilder1 = stringBuilder2;
        inputStream4 = inputStream5;
        zipFile = zipFile1;
        inputStream3 = inputStream2;
        if (file.exists()) {
          InputStream inputStream7;
          stringBuilder1 = stringBuilder2;
          inputStream4 = inputStream5;
          zipFile = zipFile1;
          inputStream3 = inputStream2;
          zipFile2 = new ZipFile();
          stringBuilder1 = stringBuilder2;
          inputStream4 = inputStream5;
          zipFile = zipFile1;
          inputStream3 = inputStream2;
          this(str1);
          ZipFile zipFile5 = zipFile2;
          ZipFile zipFile4 = zipFile5;
          inputStream4 = inputStream5;
          zipFile = zipFile5;
          inputStream3 = inputStream2;
          ZipEntry zipEntry = zipFile5.getEntry("allApps.xml");
          if (zipEntry == null) {
            zipFile4 = zipFile5;
            inputStream4 = inputStream5;
            zipFile = zipFile5;
            inputStream3 = inputStream2;
            fileInputStream2 = new FileInputStream();
            zipFile4 = zipFile5;
            inputStream4 = inputStream5;
            zipFile = zipFile5;
            inputStream3 = inputStream2;
            this(str);
            inputStream7 = fileInputStream2;
            paramInt = bool2;
          } else {
            zipFile4 = zipFile5;
            inputStream4 = inputStream5;
            zipFile = zipFile5;
            inputStream3 = inputStream2;
            inputStream2 = zipFile5.getInputStream((ZipEntry)fileInputStream2);
            paramInt = 1;
            inputStream7 = inputStream2;
          } 
          zipFile3 = zipFile5;
          inputStream = inputStream7;
        } else {
          InputStream inputStream7 = inputStream;
          inputStream4 = inputStream5;
          zipFile = zipFile1;
          ZipFile zipFile4 = zipFile3;
          inputStream = new FileInputStream(str);
          fileInputStream3 = fileInputStream2;
          paramInt = bool1;
        } 
        FileInputStream fileInputStream1 = fileInputStream3;
        inputStream4 = inputStream;
        fileInputStream = fileInputStream3;
        inputStream3 = inputStream;
        parseXml(inputStream);
        fileInputStream1 = fileInputStream3;
        inputStream4 = inputStream;
        fileInputStream = fileInputStream3;
        inputStream3 = inputStream;
        inputStream.close();
        if (fileInputStream3 != null) {
          fileInputStream1 = fileInputStream3;
          inputStream4 = inputStream;
          fileInputStream = fileInputStream3;
          inputStream3 = inputStream;
          fileInputStream3.close();
        } 
        if (paramInt != 0) {
          fileInputStream1 = fileInputStream3;
          inputStream4 = inputStream;
          fileInputStream = fileInputStream3;
          inputStream3 = inputStream;
          checkDiffPackages();
        } 
        if (inputStream != null)
          try {
            inputStream.close();
          } catch (Exception exception) {} 
        if (fileInputStream3 != null)
          fileInputStream3.close(); 
        sParsered = true;
        return true;
      } catch (Exception exception) {
        FileInputStream fileInputStream1 = fileInputStream;
        inputStream4 = inputStream3;
        Log.e("parseIconXml", "parseIconXml error");
        fileInputStream1 = fileInputStream;
        inputStream4 = inputStream3;
        exception.printStackTrace();
        if (inputStream3 != null)
          try {
            inputStream3.close();
          } catch (Exception exception1) {} 
        if (fileInputStream != null)
          fileInputStream.close(); 
        sParsered = true;
      } finally {}
      return false;
    } 
    InputStream inputStream1 = inputStream3, inputStream6 = inputStream4;
    try {
      FileInputStream fileInputStream = new FileInputStream();
      inputStream1 = inputStream3;
      inputStream6 = inputStream4;
      this(str);
      inputStream1 = fileInputStream;
      inputStream6 = fileInputStream;
      parseXml(fileInputStream);
      inputStream1 = fileInputStream;
      inputStream6 = fileInputStream;
      fileInputStream.close();
      try {
        fileInputStream.close();
        sParsered = true;
      } catch (Exception exception) {
        Log.e("parseIconXml", "input error");
      } 
      return true;
    } catch (Exception exception) {
      inputStream1 = inputStream6;
      Log.e("parseIconXml", "parseIconXml error");
      inputStream1 = inputStream6;
      exception.printStackTrace();
      if (inputStream6 != null) {
        try {
          inputStream6.close();
          sParsered = true;
        } catch (Exception exception1) {
          Log.e("parseIconXml", "input error");
        } 
        return false;
      } 
    } finally {}
    sParsered = true;
  }
  
  public static boolean isThirdPart(ApplicationInfo paramApplicationInfo) {
    if (mAllPackageNames.contains(paramApplicationInfo.packageName))
      return false; 
    return true;
  }
  
  public static boolean isThirdPartbyIconName(String paramString) {
    if (mAllIconNames.contains(paramString))
      return false; 
    return true;
  }
  
  public static int getAppsNumbers() {
    return mAllPackageNames.size();
  }
  
  public static int indexOfPackageName(String paramString) {
    return mAllPackageNames.indexOf(paramString);
  }
  
  public static String getPackageName(int paramInt) {
    return mAllPackageNames.get(paramInt);
  }
  
  public static int indexOfIconName(String paramString) {
    return mAllIconNames.indexOf(paramString);
  }
  
  public static String getIconName(int paramInt) {
    return mAllIconNames.get(paramInt);
  }
  
  public static void reset() {
    sParsered = false;
  }
  
  private static void checkDiffPackages() {
    Set<Map.Entry<String, String>> set = mDiffPackages.entrySet();
    Iterator<Map.Entry<String, String>> iterator = set.iterator();
    while (iterator.hasNext()) {
      Map.Entry entry = iterator.next();
      if (entry == null)
        continue; 
      String str2 = (String)entry.getKey();
      String str1 = (String)entry.getValue();
      int i = mAllIconNames.indexOf(str2);
      int j = mAllPackageNames.indexOf(str1);
      if (i < 0 || j < 0) {
        mAllIconNames.add(str2);
        mAllPackageNames.add(str1);
      } 
    } 
  }
}
