package com.oplus.theme;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.InputStream;

public class OplusMaskBitmapUtilities {
  private static final String TAG = "MaskBitmapUtilities";
  
  private static OplusMaskBitmapUtilities sMaskBitmapUtilities = null;
  
  private BitmapFactory.Options mOpt;
  
  static {
    System.loadLibrary("oplusgraphic");
    nativeInit();
  }
  
  public static OplusMaskBitmapUtilities getInstance() {
    // Byte code:
    //   0: ldc com/oplus/theme/OplusMaskBitmapUtilities
    //   2: monitorenter
    //   3: getstatic com/oplus/theme/OplusMaskBitmapUtilities.sMaskBitmapUtilities : Lcom/oplus/theme/OplusMaskBitmapUtilities;
    //   6: ifnonnull -> 21
    //   9: new com/oplus/theme/OplusMaskBitmapUtilities
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic com/oplus/theme/OplusMaskBitmapUtilities.sMaskBitmapUtilities : Lcom/oplus/theme/OplusMaskBitmapUtilities;
    //   21: getstatic com/oplus/theme/OplusMaskBitmapUtilities.sMaskBitmapUtilities : Lcom/oplus/theme/OplusMaskBitmapUtilities;
    //   24: astore_0
    //   25: ldc com/oplus/theme/OplusMaskBitmapUtilities
    //   27: monitorexit
    //   28: aload_0
    //   29: areturn
    //   30: astore_0
    //   31: ldc com/oplus/theme/OplusMaskBitmapUtilities
    //   33: monitorexit
    //   34: aload_0
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #50	-> 3
    //   #51	-> 9
    //   #53	-> 21
    //   #49	-> 30
    // Exception table:
    //   from	to	target	type
    //   3	9	30	finally
    //   9	21	30	finally
    //   21	25	30	finally
  }
  
  private static final native void nativeInit();
  
  private Bitmap readBitmap(Context paramContext, int paramInt) {
    BitmapFactory.Options options = new BitmapFactory.Options();
    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
    this.mOpt.inMutable = true;
    InputStream inputStream = paramContext.getResources().openRawResource(paramInt);
    Bitmap bitmap = BitmapFactory.decodeStream(inputStream, null, this.mOpt);
    try {
      inputStream.close();
    } catch (Exception exception) {}
    return bitmap;
  }
  
  public native Bitmap cutAndScaleBitmap(Bitmap paramBitmap);
  
  public native void releaseResouce();
  
  public native Bitmap scaleAndMaskBitmap(Context paramContext, int paramInt);
  
  public native Bitmap scaleAndMaskBitmap(Bitmap paramBitmap);
  
  public native void setCutAndScalePram(int paramInt1, int paramInt2);
  
  public native void setMaskBitmap(Context paramContext, int paramInt);
  
  public native void setMaskBitmap(Bitmap paramBitmap);
  
  public native void setMaskBitmap(Bitmap paramBitmap, int paramInt);
}
