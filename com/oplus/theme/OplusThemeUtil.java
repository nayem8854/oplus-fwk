package com.oplus.theme;

import android.os.OplusBaseEnvironment;
import java.io.File;
import java.util.ArrayList;

public class OplusThemeUtil {
  public static final String ACCESS_CHANGE_SETTING = "access_color_setting";
  
  public static final String CUSTOM_THEME_PATH;
  
  public static final String DATA_THEME_PATH = "/data/theme/";
  
  public static final float DEFAULT_DARKMODE_BACKGROUNDMAXL = 0.0F;
  
  public static final float DEFAULT_DARKMODE_DIALOGBGMAXL = 27.0F;
  
  public static final float DEFAULT_DARKMODE_FOREGROUNDMINL = 100.0F;
  
  public static final float DEFAULT_DETECT_MASK_BORDER_OFFSET = 0.065F;
  
  public static final String ICON_APCK_NAME = "icon_pack_name";
  
  public static final String MATERIAL_OPLUS_MODE = "material_color_mode";
  
  public static final String OPLUS_THEME_SETTING = "color_theme_setting";
  
  public static final String SYSTEM_THEME_DEFAULT_PATH;
  
  public static final String SYSTEM_THEME_SECOND_PATH;
  
  public static final ArrayList<String> THEME_CHANGED_IGNORE_PKGS;
  
  public static final String THEME_CUSTOM_MODE = "theme_change_mode";
  
  public static final String THEME_FLAG_SETTING = "theme_flag_setting";
  
  private static String mDefaultThemePath;
  
  static {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("");
    stringBuilder.append(OplusBaseEnvironment.getMyRegionDirectory().getAbsolutePath());
    stringBuilder.append("/media/theme/");
    SYSTEM_THEME_DEFAULT_PATH = stringBuilder.toString();
    stringBuilder = new StringBuilder();
    stringBuilder.append("");
    stringBuilder.append(OplusBaseEnvironment.getMyCarrierDirectory().getAbsolutePath());
    stringBuilder.append("/media/theme/");
    SYSTEM_THEME_SECOND_PATH = stringBuilder.toString();
    stringBuilder = new StringBuilder();
    stringBuilder.append("");
    stringBuilder.append(OplusBaseEnvironment.getMyCompanyDirectory().getAbsolutePath());
    stringBuilder.append("/media/theme/");
    CUSTOM_THEME_PATH = stringBuilder.toString();
    mDefaultThemePath = null;
    ArrayList<String> arrayList = new ArrayList();
    arrayList.add("com.coloros.bootreg");
  }
  
  public static String getDefaultThemePath() {
    String str = mDefaultThemePath;
    if (str != null)
      return str; 
    if ((new File(SYSTEM_THEME_DEFAULT_PATH)).exists()) {
      mDefaultThemePath = SYSTEM_THEME_DEFAULT_PATH;
    } else {
      mDefaultThemePath = SYSTEM_THEME_SECOND_PATH;
    } 
    return mDefaultThemePath;
  }
}
