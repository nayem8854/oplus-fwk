package com.oplus.theme;

import android.util.Log;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class OplusIconParam {
  public float mScale = 0.0F;
  
  public float mXOffsetPCT = 0.0F;
  
  public float mYOffsetPCT = 0.0F;
  
  public float mDetectMaskBorderOffset = 0.065F;
  
  private static final boolean LOGE = false;
  
  private static final String TAG = "ThemeDiscription";
  
  private static final String TAG_DETECT_MASK_BORDER_OFFSET = "DetectMaskBorderOffset";
  
  private static final String TAG_SCALE = "Scale";
  
  private static final String TAG_XOFFSETPCT = "XOffsetPCT";
  
  private static final String TAG_YOFFSETPCT = "YOffsetPCT";
  
  public String mCurrentTag;
  
  public String mPath;
  
  public OplusIconParam(String paramString) {
    this.mPath = paramString;
  }
  
  public void myLog(String paramString) {}
  
  public String getPath() {
    return this.mPath;
  }
  
  class ThemeXmlHandler extends DefaultHandler {
    final OplusIconParam this$0;
    
    public void startElement(String param1String1, String param1String2, String param1String3, Attributes param1Attributes) throws SAXException {
      OplusIconParam.this.mCurrentTag = param1String2;
    }
    
    public void characters(char[] param1ArrayOfchar, int param1Int1, int param1Int2) throws SAXException {
      String str = new String(param1ArrayOfchar, param1Int1, param1Int2);
      if ("Scale".equals(OplusIconParam.this.mCurrentTag)) {
        OplusIconParam.this.mScale = Float.parseFloat(str);
      } else if ("XOffsetPCT".equals(OplusIconParam.this.mCurrentTag)) {
        OplusIconParam.this.mXOffsetPCT = Float.parseFloat(str);
      } else if ("YOffsetPCT".equals(OplusIconParam.this.mCurrentTag)) {
        OplusIconParam.this.mYOffsetPCT = Float.parseFloat(str);
      } else if ("DetectMaskBorderOffset".equals(OplusIconParam.this.mCurrentTag)) {
        OplusIconParam.this.mDetectMaskBorderOffset = Float.parseFloat(str);
      } 
    }
    
    public void endElement(String param1String1, String param1String2, String param1String3) throws SAXException {
      OplusIconParam.this.mCurrentTag = null;
    }
  }
  
  public boolean parseXml() {
    String str;
    if (!OplusThirdPartUtil.mIsDefaultTheme) {
      str = OplusThirdPartUtil.sThemePath;
    } else {
      str = OplusThemeUtil.getDefaultThemePath();
    } 
    try {
      ZipFile zipFile = new ZipFile();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append(str);
      stringBuilder.append("com.oppo.launcher");
      this(stringBuilder.toString());
      ZipEntry zipEntry = zipFile.getEntry(this.mPath);
      if (zipEntry == null) {
        zipFile.close();
        myLog("parseXml:entry is null");
        return false;
      } 
      InputStream inputStream = zipFile.getInputStream(zipEntry);
      if (inputStream == null) {
        zipFile.close();
        myLog("parseXml:input is null");
        return false;
      } 
      SAXParserFactory sAXParserFactory = SAXParserFactory.newInstance();
      SAXParser sAXParser = sAXParserFactory.newSAXParser();
      ThemeXmlHandler themeXmlHandler = new ThemeXmlHandler();
      this(this);
      sAXParser.parse(inputStream, themeXmlHandler);
      inputStream.close();
      zipFile.close();
      return true;
    } catch (ZipException zipException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("parseXml:ZipFile is destroyed, mPath = ");
      stringBuilder.append(this.mPath);
      myLog(stringBuilder.toString());
      return false;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("parseXml. ex = ");
      stringBuilder.append(exception);
      Log.e("ThemeDiscription", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean parseXmlForUser(int paramInt) {
    String str;
    if (!OplusThirdPartUtil.mIsDefaultTheme) {
      str = OplusThirdPartUtil.getThemePathForUser(paramInt);
    } else {
      str = OplusThemeUtil.getDefaultThemePath();
    } 
    try {
      ZipFile zipFile = new ZipFile();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append(str);
      stringBuilder.append("com.oppo.launcher");
      this(stringBuilder.toString());
      ZipEntry zipEntry = zipFile.getEntry(this.mPath);
      if (zipEntry == null) {
        zipFile.close();
        myLog("parseXml:entry is null");
        return false;
      } 
      InputStream inputStream = zipFile.getInputStream(zipEntry);
      if (inputStream == null) {
        zipFile.close();
        myLog("parseXml:input is null");
        return false;
      } 
      SAXParserFactory sAXParserFactory = SAXParserFactory.newInstance();
      SAXParser sAXParser = sAXParserFactory.newSAXParser();
      ThemeXmlHandler themeXmlHandler = new ThemeXmlHandler();
      this(this);
      sAXParser.parse(inputStream, themeXmlHandler);
      inputStream.close();
      zipFile.close();
      return true;
    } catch (ZipException zipException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("parseXml:ZipFile is destroyed, mPath = ");
      stringBuilder.append(this.mPath);
      myLog(stringBuilder.toString());
      return false;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("parseXml. ex = ");
      stringBuilder.append(exception);
      Log.e("ThemeDiscription", stringBuilder.toString());
      return false;
    } 
  }
  
  public float getScale() {
    return this.mScale;
  }
  
  public float getXOffset() {
    return this.mXOffsetPCT;
  }
  
  public float getYOffset() {
    return this.mYOffsetPCT;
  }
  
  public float getDetectMaskBorderOffset() {
    return this.mDetectMaskBorderOffset;
  }
}
