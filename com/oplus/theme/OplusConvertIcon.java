package com.oplus.theme;

import android.content.res.Configuration;
import android.content.res.OplusBaseConfiguration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.DrawFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.util.Log;
import com.oplus.util.OplusTypeCastingHelper;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import oplus.content.res.OplusExtraConfiguration;

public final class OplusConvertIcon {
  private static final float COVER_ICON_RADIO = 0.62F;
  
  private static final boolean DEBUG_ENABLE = false;
  
  private static final boolean DEBUG_NORMAL = false;
  
  private static final float DEFAULT_RELATIVE_BRIGHTNESS = 0.84F;
  
  private static final float ICON_SIZE_RADIO_OVER_DENSITY_400 = 1.055F;
  
  private static final String IPHONE_STYLE_BG_NAME = "iphone_style_bg.png";
  
  private static final String IPHONE_STYLE_FG_NAME = "iphone_style_fg.png";
  
  private static final String LAUNCHER_ZIP_NAME = "com.oppo.launcher";
  
  private static final String NEW_IPHONE_STYLE_BG_NAME = "new_iphone_style_bg.png";
  
  private static final String NEW_IPHONE_STYLE_MASK_NAME = "new_iphone_style_mask.png";
  
  private static final int NORMAL_ICON_SIZE = 156;
  
  private static final int NORMAL_SCREEN_WIDTH = 1080;
  
  private static final String TAG = "OppoConvertIcon";
  
  private static int mUserId;
  
  private static final Canvas sCanvas;
  
  private static String sCoverBackgroundPic;
  
  private static LightingColorFilter sDarkModeColorFilter;
  
  private static int sDetectMaskBorderOffset;
  
  public enum IconBgType {
    COVER, MASK, SCALE;
    
    private static final IconBgType[] $VALUES;
    
    static {
      IconBgType iconBgType = new IconBgType("SCALE", 2);
      $VALUES = new IconBgType[] { MASK, COVER, iconBgType };
    }
  }
  
  private static final String[] sDrawableDirs = new String[] { "res/drawable-hdpi/", "res/drawable-xhdpi/", "res/drawable-xxhdpi/" };
  
  private static Drawable sIconBackground;
  
  private static IconBgType sIconBgType;
  
  private static Drawable sIconForeground;
  
  private static int sIconHeight;
  
  private static int sIconSize;
  
  private static int sIconWidth;
  
  private static String sMaskBackgroundPic;
  
  private static Bitmap sMaskBitmap;
  
  private static String sMaskForegroundPic;
  
  private static boolean sNeedDrawForeground;
  
  private static final Rect sOldBounds = new Rect();
  
  private static Paint sPaint;
  
  private static int sThemeParamScale;
  
  private static int sThemeParamXOffset;
  
  private static int sThemeParamYOffset;
  
  static {
    sCanvas = new Canvas();
    sThemeParamScale = 128;
    sThemeParamXOffset = 0;
    sThemeParamYOffset = 0;
    sDetectMaskBorderOffset = 10;
    sIconSize = -1;
    sIconWidth = -1;
    sIconHeight = -1;
    sMaskBackgroundPic = null;
    sMaskForegroundPic = null;
    sCoverBackgroundPic = null;
    sIconBackground = null;
    sIconForeground = null;
    sMaskBitmap = null;
    sNeedDrawForeground = false;
    sIconBgType = IconBgType.MASK;
    sPaint = new Paint();
    sCanvas.setDrawFilter((DrawFilter)new PaintFlagsDrawFilter(4, 2));
  }
  
  public static boolean hasInit() {
    if (sCoverBackgroundPic == null && sMaskBackgroundPic == null && sMaskForegroundPic == null)
      return false; 
    return true;
  }
  
  public static Bitmap convertIconBitmap(Drawable paramDrawable, Resources paramResources, boolean paramBoolean) {
    // Byte code:
    //   0: ldc com/oplus/theme/OplusConvertIcon
    //   2: monitorenter
    //   3: aload_0
    //   4: aload_1
    //   5: iload_2
    //   6: iconst_0
    //   7: invokestatic convertIconBitmap : (Landroid/graphics/drawable/Drawable;Landroid/content/res/Resources;ZZ)Landroid/graphics/Bitmap;
    //   10: astore_0
    //   11: ldc com/oplus/theme/OplusConvertIcon
    //   13: monitorexit
    //   14: aload_0
    //   15: areturn
    //   16: astore_0
    //   17: ldc com/oplus/theme/OplusConvertIcon
    //   19: monitorexit
    //   20: aload_0
    //   21: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #119	-> 3
    //   #119	-> 16
    // Exception table:
    //   from	to	target	type
    //   3	11	16	finally
  }
  
  public static Bitmap convertIconBitmap(Drawable paramDrawable, Resources paramResources, boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: ldc com/oplus/theme/OplusConvertIcon
    //   2: monitorenter
    //   3: aload_0
    //   4: ifnull -> 601
    //   7: aload_1
    //   8: ifnonnull -> 14
    //   11: goto -> 601
    //   14: getstatic com/oplus/theme/OplusConvertIcon.sCanvas : Landroid/graphics/Canvas;
    //   17: astore #4
    //   19: aload #4
    //   21: monitorenter
    //   22: getstatic com/oplus/theme/OplusConvertIcon.sIconWidth : I
    //   25: iconst_m1
    //   26: if_icmpne -> 33
    //   29: aload_1
    //   30: invokestatic initIconSize : (Landroid/content/res/Resources;)V
    //   33: getstatic com/oplus/theme/OplusConvertIcon.sIconWidth : I
    //   36: istore #5
    //   38: getstatic com/oplus/theme/OplusConvertIcon.sIconHeight : I
    //   41: istore #6
    //   43: aconst_null
    //   44: astore #7
    //   46: aconst_null
    //   47: astore #8
    //   49: aload_0
    //   50: instanceof android/graphics/drawable/PaintDrawable
    //   53: ifeq -> 83
    //   56: aload_0
    //   57: checkcast android/graphics/drawable/PaintDrawable
    //   60: astore #8
    //   62: aload #8
    //   64: iload #5
    //   66: invokevirtual setIntrinsicWidth : (I)V
    //   69: aload #8
    //   71: iload #6
    //   73: invokevirtual setIntrinsicHeight : (I)V
    //   76: aload #7
    //   78: astore #8
    //   80: goto -> 262
    //   83: aload_0
    //   84: instanceof android/graphics/drawable/BitmapDrawable
    //   87: ifeq -> 193
    //   90: aload_0
    //   91: checkcast android/graphics/drawable/BitmapDrawable
    //   94: astore #7
    //   96: aload #7
    //   98: invokevirtual getBitmap : ()Landroid/graphics/Bitmap;
    //   101: aload_1
    //   102: invokestatic computeDesity : (Landroid/graphics/Bitmap;Landroid/content/res/Resources;)Landroid/graphics/Bitmap;
    //   105: astore #7
    //   107: iload_2
    //   108: ifeq -> 190
    //   111: aload #7
    //   113: invokevirtual getConfig : ()Landroid/graphics/Bitmap$Config;
    //   116: astore #9
    //   118: aload #9
    //   120: ifnull -> 138
    //   123: aload #7
    //   125: astore #8
    //   127: getstatic android/graphics/Bitmap$Config.RGBA_F16 : Landroid/graphics/Bitmap$Config;
    //   130: aload #9
    //   132: invokevirtual equals : (Ljava/lang/Object;)Z
    //   135: ifeq -> 190
    //   138: new java/lang/StringBuilder
    //   141: astore #8
    //   143: aload #8
    //   145: invokespecial <init> : ()V
    //   148: aload #8
    //   150: ldc 'convertIconBitmap...set the bitmap config to ARGB_8888. bitmapConfig = '
    //   152: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   155: pop
    //   156: aload #8
    //   158: aload #9
    //   160: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   163: pop
    //   164: ldc 'OppoConvertIcon'
    //   166: aload #8
    //   168: invokevirtual toString : ()Ljava/lang/String;
    //   171: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   174: pop
    //   175: getstatic android/graphics/Bitmap$Config.ARGB_8888 : Landroid/graphics/Bitmap$Config;
    //   178: astore #8
    //   180: aload #7
    //   182: aload #8
    //   184: iconst_1
    //   185: invokevirtual copy : (Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
    //   188: astore #8
    //   190: goto -> 262
    //   193: aload_0
    //   194: instanceof android/graphics/drawable/NinePatchDrawable
    //   197: ifne -> 214
    //   200: aload_0
    //   201: instanceof android/graphics/drawable/VectorDrawable
    //   204: ifne -> 214
    //   207: aload_0
    //   208: instanceof android/graphics/drawable/AdaptiveIconDrawable
    //   211: ifeq -> 190
    //   214: aload_0
    //   215: invokevirtual getIntrinsicWidth : ()I
    //   218: aload_0
    //   219: invokevirtual getIntrinsicHeight : ()I
    //   222: getstatic android/graphics/Bitmap$Config.ARGB_8888 : Landroid/graphics/Bitmap$Config;
    //   225: invokestatic createBitmap : (IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    //   228: astore #8
    //   230: new android/graphics/Canvas
    //   233: astore #7
    //   235: aload #7
    //   237: aload #8
    //   239: invokespecial <init> : (Landroid/graphics/Bitmap;)V
    //   242: aload_0
    //   243: iconst_0
    //   244: iconst_0
    //   245: aload_0
    //   246: invokevirtual getIntrinsicWidth : ()I
    //   249: aload_0
    //   250: invokevirtual getIntrinsicHeight : ()I
    //   253: invokevirtual setBounds : (IIII)V
    //   256: aload_0
    //   257: aload #7
    //   259: invokevirtual draw : (Landroid/graphics/Canvas;)V
    //   262: getstatic com/oplus/theme/OplusConvertIcon.sIconWidth : I
    //   265: ifgt -> 276
    //   268: aload #4
    //   270: monitorexit
    //   271: ldc com/oplus/theme/OplusConvertIcon
    //   273: monitorexit
    //   274: aconst_null
    //   275: areturn
    //   276: getstatic com/oplus/theme/OplusConvertIcon.sIconWidth : I
    //   279: getstatic com/oplus/theme/OplusConvertIcon.sIconHeight : I
    //   282: getstatic android/graphics/Bitmap$Config.ARGB_8888 : Landroid/graphics/Bitmap$Config;
    //   285: invokestatic createBitmap : (IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    //   288: astore #7
    //   290: getstatic com/oplus/theme/OplusConvertIcon.sCanvas : Landroid/graphics/Canvas;
    //   293: astore #9
    //   295: aload #9
    //   297: aload #7
    //   299: invokevirtual setBitmap : (Landroid/graphics/Bitmap;)V
    //   302: iload_2
    //   303: ifeq -> 410
    //   306: aload_1
    //   307: invokestatic setDarkFilterToPaint : (Landroid/content/res/Resources;)V
    //   310: iload_3
    //   311: ifeq -> 326
    //   314: aload_0
    //   315: aload #8
    //   317: aload_1
    //   318: aload #9
    //   320: invokestatic cutAndScaleBitmap : (Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;Landroid/content/res/Resources;Landroid/graphics/Canvas;)V
    //   323: goto -> 580
    //   326: getstatic com/oplus/theme/OplusConvertIcon.sIconBgType : Lcom/oplus/theme/OplusConvertIcon$IconBgType;
    //   329: getstatic com/oplus/theme/OplusConvertIcon$IconBgType.COVER : Lcom/oplus/theme/OplusConvertIcon$IconBgType;
    //   332: if_acmpne -> 359
    //   335: aload_0
    //   336: aload #8
    //   338: aload_1
    //   339: aload #9
    //   341: invokestatic coverBitmap : (Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;Landroid/content/res/Resources;Landroid/graphics/Canvas;)V
    //   344: getstatic com/oplus/theme/OplusConvertIcon.sNeedDrawForeground : Z
    //   347: ifeq -> 580
    //   350: aload_1
    //   351: aload #9
    //   353: invokestatic drawForeground : (Landroid/content/res/Resources;Landroid/graphics/Canvas;)V
    //   356: goto -> 580
    //   359: getstatic com/oplus/theme/OplusConvertIcon.sIconBgType : Lcom/oplus/theme/OplusConvertIcon$IconBgType;
    //   362: getstatic com/oplus/theme/OplusConvertIcon$IconBgType.MASK : Lcom/oplus/theme/OplusConvertIcon$IconBgType;
    //   365: if_acmpne -> 398
    //   368: getstatic com/oplus/theme/OplusConvertIcon.sMaskBitmap : Landroid/graphics/Bitmap;
    //   371: ifnull -> 398
    //   374: aload_0
    //   375: aload #8
    //   377: aload_1
    //   378: aload #9
    //   380: invokestatic maskBitmap : (Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;Landroid/content/res/Resources;Landroid/graphics/Canvas;)V
    //   383: getstatic com/oplus/theme/OplusConvertIcon.sNeedDrawForeground : Z
    //   386: ifeq -> 580
    //   389: aload_1
    //   390: aload #9
    //   392: invokestatic drawForeground : (Landroid/content/res/Resources;Landroid/graphics/Canvas;)V
    //   395: goto -> 580
    //   398: aload_0
    //   399: aload #8
    //   401: aload_1
    //   402: aload #9
    //   404: invokestatic cutAndScaleBitmap : (Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;Landroid/content/res/Resources;Landroid/graphics/Canvas;)V
    //   407: goto -> 580
    //   410: aload_1
    //   411: aload_0
    //   412: invokestatic setDarkFilterToDrawable : (Landroid/content/res/Resources;Landroid/graphics/drawable/Drawable;)V
    //   415: aload_0
    //   416: invokevirtual getIntrinsicWidth : ()I
    //   419: istore #10
    //   421: aload_0
    //   422: invokevirtual getIntrinsicHeight : ()I
    //   425: istore #11
    //   427: iload #5
    //   429: istore #12
    //   431: iload #6
    //   433: istore #13
    //   435: iload #10
    //   437: ifle -> 513
    //   440: iload #5
    //   442: istore #12
    //   444: iload #6
    //   446: istore #13
    //   448: iload #11
    //   450: ifle -> 513
    //   453: iload #10
    //   455: i2f
    //   456: iload #11
    //   458: i2f
    //   459: fdiv
    //   460: fstore #14
    //   462: iload #10
    //   464: iload #11
    //   466: if_icmple -> 485
    //   469: iload #5
    //   471: i2f
    //   472: fload #14
    //   474: fdiv
    //   475: f2i
    //   476: istore #13
    //   478: iload #5
    //   480: istore #12
    //   482: goto -> 513
    //   485: iload #5
    //   487: istore #12
    //   489: iload #6
    //   491: istore #13
    //   493: iload #11
    //   495: iload #10
    //   497: if_icmple -> 513
    //   500: iload #6
    //   502: i2f
    //   503: fload #14
    //   505: fmul
    //   506: f2i
    //   507: istore #12
    //   509: iload #6
    //   511: istore #13
    //   513: getstatic com/oplus/theme/OplusConvertIcon.sIconWidth : I
    //   516: iload #12
    //   518: isub
    //   519: iconst_2
    //   520: idiv
    //   521: istore #6
    //   523: getstatic com/oplus/theme/OplusConvertIcon.sIconHeight : I
    //   526: iload #13
    //   528: isub
    //   529: iconst_2
    //   530: idiv
    //   531: istore #5
    //   533: getstatic com/oplus/theme/OplusConvertIcon.sOldBounds : Landroid/graphics/Rect;
    //   536: aload_0
    //   537: invokevirtual getBounds : ()Landroid/graphics/Rect;
    //   540: invokevirtual set : (Landroid/graphics/Rect;)V
    //   543: aload_0
    //   544: iload #6
    //   546: iload #5
    //   548: iload #6
    //   550: iload #12
    //   552: iadd
    //   553: iload #5
    //   555: iload #13
    //   557: iadd
    //   558: invokevirtual setBounds : (IIII)V
    //   561: aload_0
    //   562: aload #9
    //   564: invokevirtual draw : (Landroid/graphics/Canvas;)V
    //   567: aload_0
    //   568: getstatic com/oplus/theme/OplusConvertIcon.sOldBounds : Landroid/graphics/Rect;
    //   571: invokevirtual setBounds : (Landroid/graphics/Rect;)V
    //   574: aload #9
    //   576: aconst_null
    //   577: invokevirtual setBitmap : (Landroid/graphics/Bitmap;)V
    //   580: aload #4
    //   582: monitorexit
    //   583: ldc com/oplus/theme/OplusConvertIcon
    //   585: monitorexit
    //   586: aload #7
    //   588: areturn
    //   589: astore_0
    //   590: aload #4
    //   592: monitorexit
    //   593: aload_0
    //   594: athrow
    //   595: astore_0
    //   596: ldc com/oplus/theme/OplusConvertIcon
    //   598: monitorexit
    //   599: aload_0
    //   600: athrow
    //   601: ldc com/oplus/theme/OplusConvertIcon
    //   603: monitorexit
    //   604: aconst_null
    //   605: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #123	-> 3
    //   #127	-> 14
    //   #128	-> 22
    //   #129	-> 29
    //   #132	-> 33
    //   #133	-> 38
    //   #134	-> 43
    //   #135	-> 49
    //   #137	-> 56
    //   #138	-> 62
    //   #139	-> 69
    //   #140	-> 76
    //   #142	-> 90
    //   #143	-> 96
    //   #145	-> 107
    //   #146	-> 111
    //   #149	-> 111
    //   #152	-> 118
    //   #153	-> 138
    //   #154	-> 175
    //   #155	-> 180
    //   #160	-> 190
    //   #161	-> 214
    //   #162	-> 230
    //   #163	-> 242
    //   #164	-> 256
    //   #166	-> 262
    //   #167	-> 268
    //   #169	-> 276
    //   #170	-> 290
    //   #171	-> 295
    //   #173	-> 302
    //   #174	-> 306
    //   #177	-> 310
    //   #178	-> 314
    //   #181	-> 326
    //   #182	-> 335
    //   #183	-> 344
    //   #184	-> 350
    //   #186	-> 359
    //   #187	-> 374
    //   #188	-> 383
    //   #189	-> 389
    //   #192	-> 398
    //   #196	-> 410
    //   #197	-> 415
    //   #198	-> 421
    //   #199	-> 427
    //   #200	-> 453
    //   #201	-> 462
    //   #203	-> 469
    //   #204	-> 485
    //   #206	-> 500
    //   #210	-> 513
    //   #211	-> 523
    //   #213	-> 533
    //   #214	-> 543
    //   #215	-> 561
    //   #216	-> 567
    //   #217	-> 574
    //   #219	-> 580
    //   #220	-> 589
    //   #122	-> 595
    //   #124	-> 601
    // Exception table:
    //   from	to	target	type
    //   14	22	595	finally
    //   22	29	589	finally
    //   29	33	589	finally
    //   33	38	589	finally
    //   38	43	589	finally
    //   49	56	589	finally
    //   56	62	589	finally
    //   62	69	589	finally
    //   69	76	589	finally
    //   83	90	589	finally
    //   90	96	589	finally
    //   96	107	589	finally
    //   111	118	589	finally
    //   127	138	589	finally
    //   138	175	589	finally
    //   175	180	589	finally
    //   180	190	589	finally
    //   193	207	589	finally
    //   207	214	589	finally
    //   214	230	589	finally
    //   230	242	589	finally
    //   242	256	589	finally
    //   256	262	589	finally
    //   262	268	589	finally
    //   268	271	589	finally
    //   276	290	589	finally
    //   290	295	589	finally
    //   295	302	589	finally
    //   306	310	589	finally
    //   314	323	589	finally
    //   326	335	589	finally
    //   335	344	589	finally
    //   344	350	589	finally
    //   350	356	589	finally
    //   359	374	589	finally
    //   374	383	589	finally
    //   383	389	589	finally
    //   389	395	589	finally
    //   398	407	589	finally
    //   410	415	589	finally
    //   415	421	589	finally
    //   421	427	589	finally
    //   513	523	589	finally
    //   523	533	589	finally
    //   533	543	589	finally
    //   543	561	589	finally
    //   561	567	589	finally
    //   567	574	589	finally
    //   574	580	589	finally
    //   580	583	589	finally
    //   590	593	589	finally
    //   593	595	595	finally
  }
  
  static void coverBitmapNoCut(Drawable paramDrawable, Bitmap paramBitmap, Resources paramResources, Canvas paramCanvas) {
    if (paramBitmap != null) {
      Drawable drawable = OplusThirdPartUtil.getLauncherDrawableByName(paramResources, sCoverBackgroundPic);
      if (drawable != null) {
        sOldBounds.set(drawable.getBounds());
        drawable.setBounds(0, 0, sIconWidth, sIconHeight);
        drawable.draw(paramCanvas);
        drawable.setBounds(sOldBounds);
      } 
      float f1 = 1.0F;
      float f2 = f1;
      if ((paramResources.getDisplayMetrics()).xdpi > 400.0F) {
        f2 = f1;
        if (OplusThirdPartUtil.mIsDefaultTheme)
          f2 = 1.055F; 
      } 
      int i = paramDrawable.getIntrinsicWidth();
      int j = paramDrawable.getIntrinsicHeight();
      i = (int)(i * f2);
      int k = (int)(j * f2);
      j = (sIconWidth - i) / 2;
      int m = (sIconHeight - k) / 2;
      paramDrawable.setBounds(j, m, j + i, m + k);
      paramDrawable.draw(paramCanvas);
    } 
  }
  
  static void coverBitmap(Drawable paramDrawable, Bitmap paramBitmap, Resources paramResources, Canvas paramCanvas) {
    if (paramBitmap != null) {
      if (!paramBitmap.hasAlpha())
        paramBitmap.setHasAlpha(true); 
      if (sIconBackground == null)
        sIconBackground = OplusThirdPartUtil.getLauncherDrawableByNameForUser(paramResources, sCoverBackgroundPic, mUserId); 
      paramDrawable = sIconBackground;
      if (paramDrawable != null) {
        sOldBounds.set(paramDrawable.getBounds());
        sIconBackground.setBounds(0, 0, sIconWidth, sIconHeight);
        sIconBackground.draw(paramCanvas);
        sIconBackground.setBounds(sOldBounds);
      } 
      OplusMaskBitmapUtilities oplusMaskBitmapUtilities = OplusMaskBitmapUtilities.getInstance();
      if (paramBitmap.getConfig() != null)
        paramBitmap = oplusMaskBitmapUtilities.cutAndScaleBitmap(paramBitmap); 
      if (paramBitmap != null) {
        int i = paramBitmap.getWidth();
        int j = paramBitmap.getHeight();
        paramCanvas.drawBitmap(paramBitmap, ((sIconWidth - i) / 2 + sThemeParamXOffset), ((sIconHeight - j) / 2 + sThemeParamYOffset), sPaint);
      } else {
        Log.i("OppoConvertIcon", "coverBitmap -- scale == null");
      } 
    } 
  }
  
  static void cutAndScaleBitmap(Drawable paramDrawable, Bitmap paramBitmap, Resources paramResources, Canvas paramCanvas) {
    if (paramBitmap != null) {
      Bitmap bitmap;
      if (!paramBitmap.hasAlpha())
        paramBitmap.setHasAlpha(true); 
      OplusMaskBitmapUtilities oplusMaskBitmapUtilities = OplusMaskBitmapUtilities.getInstance();
      if (paramBitmap.getConfig() != null) {
        bitmap = oplusMaskBitmapUtilities.cutAndScaleBitmap(paramBitmap);
      } else {
        bitmap = paramBitmap;
      } 
      if (bitmap != null) {
        int i = bitmap.getWidth();
        int j = bitmap.getHeight();
        paramCanvas.drawBitmap(bitmap, ((sIconWidth - i) / 2), ((sIconHeight - j) / 2), sPaint);
      } else {
        Log.i("OppoConvertIcon", "cutAndScaleBitmap -- scale == null");
      } 
    } 
  }
  
  static void maskBitmap(Drawable paramDrawable, Bitmap paramBitmap, Resources paramResources, Canvas paramCanvas) {
    Bitmap bitmap = paramBitmap;
    if (paramBitmap == null) {
      bitmap = Bitmap.createBitmap(sIconWidth, sIconHeight, Bitmap.Config.ARGB_8888);
      paramCanvas.setBitmap(bitmap);
      sOldBounds.set(paramDrawable.getBounds());
      paramDrawable.setBounds(0, 0, sIconWidth, sIconHeight);
      paramDrawable.draw(paramCanvas);
      paramDrawable.setBounds(sOldBounds);
    } 
    if (bitmap != null) {
      if (!bitmap.hasAlpha())
        bitmap.setHasAlpha(true); 
      OplusMaskBitmapUtilities oplusMaskBitmapUtilities = OplusMaskBitmapUtilities.getInstance();
      paramBitmap = oplusMaskBitmapUtilities.scaleAndMaskBitmap(bitmap);
      if (sIconBackground == null)
        sIconBackground = OplusThirdPartUtil.getLauncherDrawableByNameForUser(paramResources, sMaskBackgroundPic, mUserId); 
      Drawable drawable = sIconBackground;
      if (drawable != null) {
        setDarkFilterToDrawable(paramResources, drawable);
        sOldBounds.set(sIconBackground.getBounds());
        sIconBackground.setBounds(0, 0, sIconWidth, sIconHeight);
        sIconBackground.draw(paramCanvas);
        sIconBackground.setBounds(sOldBounds);
      } else {
        Log.i("OppoConvertIcon", "maskBitmap -- sIconBackground == null");
      } 
      if (paramBitmap != null) {
        int i = paramBitmap.getWidth();
        int j = paramBitmap.getHeight();
        int k = sIconWidth, m = (i - k) / 2, n = sThemeParamXOffset;
        if (m + n > -1) {
          paramCanvas.drawBitmap(paramBitmap, ((k - i) / 2), ((sIconHeight - j) / 2), sPaint);
        } else {
          paramCanvas.drawBitmap(paramBitmap, ((k - i) / 2 + n), ((sIconHeight - j) / 2 + sThemeParamYOffset), sPaint);
        } 
      } else {
        Log.i("OppoConvertIcon", "maskBitmap -- scale == null");
      } 
    } else {
      Log.i("OppoConvertIcon", "maskBitmap -- originalBitmap == null");
    } 
  }
  
  public static void drawForeground(Resources paramResources, Canvas paramCanvas) {
    if (sIconForeground == null)
      sIconForeground = OplusThirdPartUtil.getLauncherDrawableByNameForUser(paramResources, sMaskForegroundPic, mUserId); 
    Drawable drawable = sIconForeground;
    if (drawable != null) {
      setDarkFilterToDrawable(paramResources, drawable);
      sOldBounds.set(sIconForeground.getBounds());
      sIconForeground.setBounds(0, 0, sIconWidth, sIconHeight);
      sIconForeground.draw(paramCanvas);
      sIconForeground.setBounds(sOldBounds);
    } 
  }
  
  public static void initThemeParamForUser(Resources paramResources, String paramString1, String paramString2, String paramString3, int paramInt) {
    OplusIconParam oplusIconParam = new OplusIconParam("themeInfo.xml");
    oplusIconParam.parseXmlForUser(paramInt);
    float f1 = oplusIconParam.getScale();
    float f2 = f1;
    if (f1 <= 0.0F)
      if (sIconBgType == IconBgType.COVER) {
        f2 = 0.62F;
      } else if (sIconBgType == IconBgType.SCALE) {
        f2 = 1.0F;
      } else {
        f2 = f1;
        if (sIconBgType == IconBgType.MASK)
          f2 = 1.0F; 
      }  
    if (sIconSize == -1)
      initIconSize(paramResources); 
    sThemeParamScale = (int)(sIconSize * f2);
    f2 = oplusIconParam.getXOffset();
    sThemeParamXOffset = (int)(sIconSize * f2);
    f2 = oplusIconParam.getYOffset();
    sThemeParamYOffset = (int)(sIconSize * f2);
    f2 = oplusIconParam.getDetectMaskBorderOffset();
    sDetectMaskBorderOffset = (int)(sIconSize * f2);
    setIconBgFgRes(paramString1, paramString2, paramString3);
  }
  
  public static void initThemeParam(Resources paramResources, String paramString1, String paramString2, String paramString3) {
    OplusIconParam oplusIconParam = new OplusIconParam("themeInfo.xml");
    oplusIconParam.parseXml();
    float f1 = oplusIconParam.getScale();
    float f2 = f1;
    if (f1 <= 0.0F)
      if (sIconBgType == IconBgType.COVER) {
        f2 = 0.62F;
      } else if (sIconBgType == IconBgType.SCALE) {
        f2 = 1.0F;
      } else {
        f2 = f1;
        if (sIconBgType == IconBgType.MASK)
          f2 = 1.0F; 
      }  
    if (sIconSize == -1)
      initIconSize(paramResources); 
    sThemeParamScale = (int)(sIconSize * f2);
    f2 = oplusIconParam.getXOffset();
    sThemeParamXOffset = (int)(sIconSize * f2);
    f2 = oplusIconParam.getYOffset();
    sThemeParamYOffset = (int)(sIconSize * f2);
    f2 = oplusIconParam.getDetectMaskBorderOffset();
    sDetectMaskBorderOffset = (int)(sIconSize * f2);
    setIconBgFgRes(paramString1, paramString2, paramString3);
  }
  
  public static IconBgType getIconBgType(Resources paramResources) {
    StringBuilder stringBuilder;
    String str = OplusThirdPartUtil.sThemePath;
    if (OplusThirdPartUtil.mIsDefaultTheme)
      str = OplusThemeUtil.getDefaultThemePath(); 
    ZipFile zipFile1 = null;
    Resources resources = null;
    paramResources = resources;
    ZipFile zipFile2 = zipFile1;
    try {
      ZipFile zipFile5 = new ZipFile();
      paramResources = resources;
      zipFile2 = zipFile1;
      StringBuilder stringBuilder1 = new StringBuilder();
      paramResources = resources;
      zipFile2 = zipFile1;
      this();
      paramResources = resources;
      zipFile2 = zipFile1;
      stringBuilder1.append(str);
      paramResources = resources;
      zipFile2 = zipFile1;
      stringBuilder1.append("com.oppo.launcher");
      paramResources = resources;
      zipFile2 = zipFile1;
      this(stringBuilder1.toString());
      ZipFile zipFile4 = zipFile5;
      ZipFile zipFile3 = zipFile4;
      zipFile2 = zipFile4;
      boolean bool = judgePicExist(zipFile4, "iphone_style_bg.png");
      if (bool) {
        zipFile3 = zipFile4;
        zipFile2 = zipFile4;
        if (judgePicExist(zipFile4, "iphone_style_fg.png")) {
          zipFile3 = zipFile4;
          zipFile2 = zipFile4;
          sNeedDrawForeground = true;
        } 
        zipFile3 = zipFile4;
        zipFile2 = zipFile4;
        zipFile4.close();
        zipFile3 = zipFile4;
        zipFile2 = zipFile4;
        IconBgType iconBgType1 = IconBgType.COVER;
        zipFile3 = zipFile4;
        zipFile2 = zipFile4;
        sIconBgType = iconBgType1;
        try {
          zipFile4.close();
        } catch (IOException iOException1) {
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("getIconBgType: e = ");
          stringBuilder2.append(iOException1);
          Log.e("OppoConvertIcon", stringBuilder2.toString());
        } 
        return iconBgType1;
      } 
      zipFile3 = zipFile4;
      zipFile2 = zipFile4;
      if (judgePicExist(zipFile4, "new_iphone_style_mask.png")) {
        zipFile3 = zipFile4;
        zipFile2 = zipFile4;
        if (judgePicExist(zipFile4, "iphone_style_fg.png")) {
          zipFile3 = zipFile4;
          zipFile2 = zipFile4;
          sNeedDrawForeground = true;
        } 
        zipFile3 = zipFile4;
        zipFile2 = zipFile4;
        zipFile4.close();
        zipFile3 = zipFile4;
        zipFile2 = zipFile4;
        IconBgType iconBgType1 = IconBgType.MASK;
        zipFile3 = zipFile4;
        zipFile2 = zipFile4;
        sIconBgType = iconBgType1;
        try {
          zipFile4.close();
        } catch (IOException iOException1) {
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("getIconBgType: e = ");
          stringBuilder2.append(iOException1);
          Log.e("OppoConvertIcon", stringBuilder2.toString());
        } 
        return iconBgType1;
      } 
      try {
        zipFile4.close();
      } catch (IOException iOException) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("getIconBgType: e = ");
        stringBuilder.append(iOException);
        Log.e("OppoConvertIcon", stringBuilder.toString());
      } 
      IconBgType iconBgType = IconBgType.SCALE;
      return iconBgType;
    } catch (IOException iOException2) {
      IOException iOException1 = iOException;
      StringBuilder stringBuilder1 = new StringBuilder();
      iOException1 = iOException;
      this();
      iOException1 = iOException;
      stringBuilder1.append("getIconBgType: e = ");
      iOException1 = iOException;
      stringBuilder1.append(iOException2);
      iOException1 = iOException;
      Log.e("OppoConvertIcon", stringBuilder1.toString());
      iOException1 = iOException;
      IconBgType iconBgType = IconBgType.MASK;
      iOException1 = iOException;
      sIconBgType = iconBgType;
      if (iOException != null)
        try {
          iOException.close();
        } catch (IOException iOException3) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("getIconBgType: e = ");
          stringBuilder.append(iOException3);
          Log.e("OppoConvertIcon", stringBuilder.toString());
        }  
      return iconBgType;
    } finally {}
    if (stringBuilder != null)
      try {
        stringBuilder.close();
      } catch (IOException iOException1) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("getIconBgType: e = ");
        stringBuilder1.append(iOException1);
        Log.e("OppoConvertIcon", stringBuilder1.toString());
      }  
    throw iOException;
  }
  
  public static IconBgType getIconBgTypeForUser(Resources paramResources, int paramInt) {
    StringBuilder stringBuilder;
    String str = OplusThirdPartUtil.getThemePathForUser(paramInt);
    if (OplusThirdPartUtil.mIsDefaultTheme)
      str = OplusThemeUtil.getDefaultThemePath(); 
    ZipFile zipFile1 = null;
    Resources resources = null;
    paramResources = resources;
    ZipFile zipFile2 = zipFile1;
    try {
      ZipFile zipFile5 = new ZipFile();
      paramResources = resources;
      zipFile2 = zipFile1;
      StringBuilder stringBuilder1 = new StringBuilder();
      paramResources = resources;
      zipFile2 = zipFile1;
      this();
      paramResources = resources;
      zipFile2 = zipFile1;
      stringBuilder1.append(str);
      paramResources = resources;
      zipFile2 = zipFile1;
      stringBuilder1.append("com.oppo.launcher");
      paramResources = resources;
      zipFile2 = zipFile1;
      this(stringBuilder1.toString());
      ZipFile zipFile4 = zipFile5;
      ZipFile zipFile3 = zipFile4;
      zipFile2 = zipFile4;
      boolean bool = judgePicExist(zipFile4, "iphone_style_bg.png");
      if (bool) {
        zipFile3 = zipFile4;
        zipFile2 = zipFile4;
        if (judgePicExist(zipFile4, "iphone_style_fg.png")) {
          zipFile3 = zipFile4;
          zipFile2 = zipFile4;
          sNeedDrawForeground = true;
        } 
        zipFile3 = zipFile4;
        zipFile2 = zipFile4;
        zipFile4.close();
        zipFile3 = zipFile4;
        zipFile2 = zipFile4;
        IconBgType iconBgType1 = IconBgType.COVER;
        zipFile3 = zipFile4;
        zipFile2 = zipFile4;
        sIconBgType = iconBgType1;
        try {
          zipFile4.close();
        } catch (IOException iOException1) {
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("getIconBgType: e = ");
          stringBuilder2.append(iOException1);
          Log.e("OppoConvertIcon", stringBuilder2.toString());
        } 
        return iconBgType1;
      } 
      zipFile3 = zipFile4;
      zipFile2 = zipFile4;
      if (judgePicExist(zipFile4, "new_iphone_style_mask.png")) {
        zipFile3 = zipFile4;
        zipFile2 = zipFile4;
        if (judgePicExist(zipFile4, "iphone_style_fg.png")) {
          zipFile3 = zipFile4;
          zipFile2 = zipFile4;
          sNeedDrawForeground = true;
        } 
        zipFile3 = zipFile4;
        zipFile2 = zipFile4;
        zipFile4.close();
        zipFile3 = zipFile4;
        zipFile2 = zipFile4;
        IconBgType iconBgType1 = IconBgType.MASK;
        zipFile3 = zipFile4;
        zipFile2 = zipFile4;
        sIconBgType = iconBgType1;
        try {
          zipFile4.close();
        } catch (IOException null) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("getIconBgType: e = ");
          stringBuilder.append(iOException);
          Log.e("OppoConvertIcon", stringBuilder.toString());
        } 
        return iconBgType1;
      } 
      try {
        zipFile4.close();
      } catch (IOException null) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("getIconBgType: e = ");
        stringBuilder.append(iOException);
        Log.e("OppoConvertIcon", stringBuilder.toString());
      } 
      IconBgType iconBgType = IconBgType.SCALE;
      return iconBgType;
    } catch (IOException iOException1) {
      StringBuilder stringBuilder1 = stringBuilder, stringBuilder2 = new StringBuilder();
      stringBuilder1 = stringBuilder;
      this();
      stringBuilder1 = stringBuilder;
      stringBuilder2.append("getIconBgType: e = ");
      stringBuilder1 = stringBuilder;
      stringBuilder2.append(iOException1);
      stringBuilder1 = stringBuilder;
      Log.e("OppoConvertIcon", stringBuilder2.toString());
      stringBuilder1 = stringBuilder;
      IconBgType iconBgType = IconBgType.MASK;
      stringBuilder1 = stringBuilder;
      sIconBgType = iconBgType;
      if (stringBuilder != null)
        try {
          stringBuilder.close();
        } catch (IOException iOException) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("getIconBgType: e = ");
          stringBuilder.append(iOException);
          Log.e("OppoConvertIcon", stringBuilder.toString());
        }  
      return iconBgType;
    } finally {}
    if (iOException != null)
      try {
        iOException.close();
      } catch (IOException iOException1) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("getIconBgType: e = ");
        stringBuilder1.append(iOException1);
        Log.e("OppoConvertIcon", stringBuilder1.toString());
      }  
    throw stringBuilder;
  }
  
  public static boolean judgePicExist(String paramString1, String paramString2) {
    boolean bool1 = false;
    boolean bool = false;
    boolean bool2 = false;
    ZipFile zipFile1 = null, zipFile2 = null;
    ZipFile zipFile3 = zipFile2, zipFile4 = zipFile1;
    try {
      ZipFile zipFile6 = new ZipFile();
      zipFile3 = zipFile2;
      zipFile4 = zipFile1;
      this(paramString1);
      ZipFile zipFile5 = zipFile6;
      zipFile3 = zipFile5;
      zipFile4 = zipFile5;
      boolean bool3 = judgePicExist(zipFile5, paramString2);
      try {
        zipFile5.close();
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("judgePicExist: Closing e ");
        stringBuilder.append(exception);
        Log.e("OppoConvertIcon", stringBuilder.toString());
      } 
    } catch (IOException iOException) {
      zipFile3 = zipFile4;
      StringBuilder stringBuilder = new StringBuilder();
      zipFile3 = zipFile4;
      this();
      zipFile3 = zipFile4;
      stringBuilder.append("judgePicExist: e ");
      zipFile3 = zipFile4;
      stringBuilder.append(iOException);
      zipFile3 = zipFile4;
      Log.e("OppoConvertIcon", stringBuilder.toString());
      if (zipFile4 != null)
        try {
          zipFile4.close();
          bool = bool2;
        } catch (Exception exception) {
          stringBuilder = new StringBuilder();
          bool = bool1;
          stringBuilder.append("judgePicExist: Closing e ");
          stringBuilder.append(exception);
          Log.e("OppoConvertIcon", stringBuilder.toString());
        }  
    } finally {}
    return bool;
  }
  
  public static boolean judgePicExist(ZipFile paramZipFile, String paramString) {
    for (int i = sDrawableDirs.length - 1; i >= 0; i--) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(sDrawableDirs[i]);
      stringBuilder.append(paramString);
      ZipEntry zipEntry = paramZipFile.getEntry(stringBuilder.toString());
      if (zipEntry != null)
        return true; 
    } 
    return false;
  }
  
  public static Bitmap getMaskBitmapForUser(Resources paramResources, String paramString, int paramInt) {
    Bitmap bitmap = sMaskBitmap;
    if (bitmap != null) {
      bitmap.recycle();
      sMaskBitmap = null;
    } 
    Drawable drawable = OplusThirdPartUtil.getLauncherDrawableByNameForUser(paramResources, paramString, paramInt);
    if (sIconWidth == -1)
      initIconSize(paramResources); 
    sMaskBitmap = bitmap = Bitmap.createBitmap(sIconWidth, sIconHeight, Bitmap.Config.ARGB_8888);
    Canvas canvas = sCanvas;
    canvas.setBitmap(bitmap);
    if (drawable != null) {
      drawable.setBounds(0, 0, sIconWidth, sIconHeight);
      drawable.draw(canvas);
    } else {
      canvas.drawColor(-16777216);
    } 
    canvas.setBitmap(null);
    return sMaskBitmap;
  }
  
  public static Bitmap getMaskBitmap(Resources paramResources, String paramString) {
    Bitmap bitmap2 = sMaskBitmap;
    if (bitmap2 != null) {
      bitmap2.recycle();
      sMaskBitmap = null;
    } 
    Drawable drawable = OplusThirdPartUtil.getLauncherDrawableByName(paramResources, paramString);
    if (sIconWidth == -1)
      initIconSize(paramResources); 
    Bitmap bitmap1 = Bitmap.createBitmap(sIconWidth, sIconHeight, Bitmap.Config.ARGB_8888);
    Canvas canvas = sCanvas;
    canvas.setBitmap(bitmap1);
    if (drawable != null) {
      drawable.setBounds(0, 0, sIconWidth, sIconHeight);
      drawable.draw(canvas);
    } else {
      canvas.drawColor(-16777216);
    } 
    canvas.setBitmap(null);
    return sMaskBitmap;
  }
  
  public static int getThemeParamScale() {
    return sThemeParamScale;
  }
  
  public static int getIconSize() {
    return sIconSize;
  }
  
  public static void initConvertIconForUser(Resources paramResources, int paramInt) {
    // Byte code:
    //   0: ldc com/oplus/theme/OplusConvertIcon
    //   2: monitorenter
    //   3: iload_1
    //   4: putstatic com/oplus/theme/OplusConvertIcon.mUserId : I
    //   7: iload_1
    //   8: invokestatic setDefaultTheme : (I)V
    //   11: aload_0
    //   12: iload_1
    //   13: invokestatic getIconBgTypeForUser : (Landroid/content/res/Resources;I)Lcom/oplus/theme/OplusConvertIcon$IconBgType;
    //   16: astore_2
    //   17: aload_0
    //   18: ldc 'new_iphone_style_bg.png'
    //   20: ldc 'iphone_style_fg.png'
    //   22: ldc 'iphone_style_bg.png'
    //   24: iload_1
    //   25: invokestatic initThemeParamForUser : (Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    //   28: aload_2
    //   29: getstatic com/oplus/theme/OplusConvertIcon$IconBgType.MASK : Lcom/oplus/theme/OplusConvertIcon$IconBgType;
    //   32: if_acmpne -> 51
    //   35: invokestatic getInstance : ()Lcom/oplus/theme/OplusMaskBitmapUtilities;
    //   38: aload_0
    //   39: ldc 'new_iphone_style_mask.png'
    //   41: iload_1
    //   42: invokestatic getMaskBitmapForUser : (Landroid/content/res/Resources;Ljava/lang/String;I)Landroid/graphics/Bitmap;
    //   45: getstatic com/oplus/theme/OplusConvertIcon.sDetectMaskBorderOffset : I
    //   48: invokevirtual setMaskBitmap : (Landroid/graphics/Bitmap;I)V
    //   51: invokestatic getInstance : ()Lcom/oplus/theme/OplusMaskBitmapUtilities;
    //   54: invokestatic getIconSize : ()I
    //   57: invokestatic getThemeParamScale : ()I
    //   60: invokevirtual setCutAndScalePram : (II)V
    //   63: ldc com/oplus/theme/OplusConvertIcon
    //   65: monitorexit
    //   66: return
    //   67: astore_0
    //   68: ldc com/oplus/theme/OplusConvertIcon
    //   70: monitorexit
    //   71: aload_0
    //   72: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #659	-> 3
    //   #660	-> 7
    //   #661	-> 11
    //   #662	-> 17
    //   #663	-> 28
    //   #664	-> 35
    //   #666	-> 51
    //   #667	-> 63
    //   #658	-> 67
    // Exception table:
    //   from	to	target	type
    //   3	7	67	finally
    //   7	11	67	finally
    //   11	17	67	finally
    //   17	28	67	finally
    //   28	35	67	finally
    //   35	51	67	finally
    //   51	63	67	finally
  }
  
  public static void initConvertIcon(Resources paramResources) {}
  
  private static void setIconBgFgRes(String paramString1, String paramString2, String paramString3) {
    sMaskBackgroundPic = paramString1;
    sMaskForegroundPic = paramString2;
    sCoverBackgroundPic = paramString3;
    sIconBackground = null;
    sIconForeground = null;
  }
  
  private static void initIconSize(Resources paramResources) {
    char c = '';
    int i = c;
    if (paramResources != null) {
      DisplayMetrics displayMetrics = paramResources.getDisplayMetrics();
      i = c;
      if (displayMetrics != null)
        i = displayMetrics.widthPixels * 156 / 1080; 
    } 
    sIconSize = i;
    sIconWidth = i;
    sIconHeight = i;
  }
  
  private static Bitmap computeDesity(Bitmap paramBitmap, Resources paramResources) {
    if (paramResources == null)
      return paramBitmap; 
    int i = (paramResources.getDisplayMetrics()).densityDpi;
    int j = paramBitmap.getDensity();
    float f = 0.0F;
    if (j != 0)
      f = i / j; 
    paramBitmap.setDensity(i);
    if (f <= 1.0F)
      return paramBitmap; 
    j = (int)(paramBitmap.getWidth() * f + 0.5F);
    i = (int)(paramBitmap.getHeight() * f + 0.5F);
    return Bitmap.createScaledBitmap(paramBitmap, j, i, true);
  }
  
  private static boolean needSetDarkFilter(Resources paramResources) {
    int i;
    if (((paramResources.getConfiguration()).uiMode & 0x30) == 32) {
      i = 1;
    } else {
      i = 0;
    } 
    if (i) {
      Configuration configuration = paramResources.getConfiguration();
      OplusBaseConfiguration oplusBaseConfiguration = OplusTypeCastingHelper.<OplusBaseConfiguration>typeCasting(OplusBaseConfiguration.class, configuration);
      if (oplusBaseConfiguration != null) {
        OplusExtraConfiguration oplusExtraConfiguration = oplusBaseConfiguration.getOplusExtraConfiguration();
        if (oplusExtraConfiguration != null) {
          long l = oplusExtraConfiguration.mUxIconConfig;
          i = Long.valueOf(Long.valueOf(l).longValue() >> 61L).intValue();
          if ((i & 0x1) == 1)
            return true; 
        } 
      } 
    } 
    return false;
  }
  
  private static void setDarkFilterToDrawable(Resources paramResources, Drawable paramDrawable) {
    if (needSetDarkFilter(paramResources)) {
      if (sDarkModeColorFilter == null) {
        int i = getDarkModeColorInCurrentContrast(0.84F);
        sDarkModeColorFilter = new LightingColorFilter(i, 0);
      } 
      paramDrawable.setColorFilter((ColorFilter)sDarkModeColorFilter);
    } else {
      paramDrawable.setColorFilter(null);
    } 
  }
  
  private static void setDarkFilterToPaint(Resources paramResources) {
    if (needSetDarkFilter(paramResources)) {
      if (sDarkModeColorFilter == null) {
        int i = getDarkModeColorInCurrentContrast(0.84F);
        sDarkModeColorFilter = new LightingColorFilter(i, 0);
      } 
      sPaint.setColorFilter((ColorFilter)sDarkModeColorFilter);
    } else {
      sPaint.setColorFilter(null);
    } 
  }
  
  private static int getDarkModeColorInCurrentContrast(float paramFloat) {
    int i;
    if (paramFloat == -1.0F) {
      i = 214;
    } else {
      i = (int)(255.0F * paramFloat);
    } 
    String str = Integer.toHexString(i);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str);
    stringBuilder.append(str);
    stringBuilder.append(str);
    return Integer.parseInt(stringBuilder.toString(), 16);
  }
}
