package com.oplus.theme;

public class OplusThemeStyle implements IOplusThemeStyle {
  public static final int DEFAULT_DIALOG_BOOTMSG_THEME = 201523272;
  
  public static final int DEFAULT_DIALOG_SHARE_THEME = 201523273;
  
  public static final int DEFAULT_DIALOG_THEME = 201523224;
  
  public static final int DEFAULT_SYSTEM_THEME = 201523222;
  
  public int getSystemThemeStyle(int paramInt) {
    return 201523222;
  }
  
  public int getDialogThemeStyle(int paramInt) {
    return 201523224;
  }
  
  public int getDialogBootMessageThemeStyle(int paramInt) {
    return 201523272;
  }
  
  public int getDialogAlertShareThemeStyle(int paramInt) {
    return 201523273;
  }
}
