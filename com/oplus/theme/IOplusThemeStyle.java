package com.oplus.theme;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;

public interface IOplusThemeStyle extends IOplusCommonFeature {
  public static final IOplusThemeStyle DEFAULT = (IOplusThemeStyle)new Object();
  
  public static final String NAME = "IOplusThemeStyle";
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusThemeStyle;
  }
  
  default IOplusCommonFeature getDefault() {
    return DEFAULT;
  }
  
  default int getSystemThemeStyle(int paramInt) {
    return paramInt;
  }
  
  default int getDialogThemeStyle(int paramInt) {
    return paramInt;
  }
  
  default int getDialogBootMessageThemeStyle(int paramInt) {
    return paramInt;
  }
  
  default int getDialogAlertShareThemeStyle(int paramInt) {
    return paramInt;
  }
}
