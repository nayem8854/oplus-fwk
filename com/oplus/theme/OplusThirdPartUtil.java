package com.oplus.theme;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.SystemProperties;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class OplusThirdPartUtil {
  private static final long CUSTOM_FLAG = 256L;
  
  public static final String CUSTOM_THEME_PATH = OplusThemeUtil.CUSTOM_THEME_PATH;
  
  public static final String DATA_SYSTEM_THEME = "/data/theme/";
  
  private static final String[] DIRS_DRAWABLE;
  
  private static final String[] DIRS_DRAWABLE_XH;
  
  private static final String[] DIRS_DRAWABLE_XXH;
  
  private static final int NUM = 3;
  
  private static final String TAG = "OplusThirdPartUtil";
  
  private static final long THIRD_FLAG = 1L;
  
  public static final String ZIPICONS = "icons";
  
  public static final String ZIPLAUNCHER = "com.oppo.launcher";
  
  public static boolean mIsDefaultTheme;
  
  public static String sThemePath = "/data/theme/";
  
  static {
    mIsDefaultTheme = true;
    DIRS_DRAWABLE = new String[] { "res/drawable-hdpi/", "res/drawable-xhdpi/", "res/drawable-xxhdpi/" };
    DIRS_DRAWABLE_XH = new String[] { "res/drawable-xhdpi/", "res/drawable-hdpi/", "res/drawable-xxhdpi/" };
    DIRS_DRAWABLE_XXH = new String[] { "res/drawable-xxhdpi/", "res/drawable-xhdpi/", "res/drawable-hdpi/" };
  }
  
  public static boolean moveFile(String paramString1, String paramString2, String paramString3) throws Exception {
    ZipFile zipFile = new ZipFile(paramString1);
    ZipEntry zipEntry = zipFile.getEntry(paramString2);
    if (zipEntry == null)
      return false; 
    InputStream inputStream = zipFile.getInputStream(zipEntry);
    FileOutputStream fileOutputStream = new FileOutputStream(paramString3);
    byte[] arrayOfByte = new byte[1024000];
    while (true) {
      int i = inputStream.read(arrayOfByte);
      if (i > 0) {
        fileOutputStream.write(arrayOfByte, 0, i);
        continue;
      } 
      break;
    } 
    inputStream.close();
    fileOutputStream.close();
    return true;
  }
  
  public static boolean clearDir(String paramString) {
    try {
      File file = new File();
      this(paramString);
      for (String str : file.list()) {
        File file1 = new File();
        this(paramString, str);
        if (file1.exists())
          file1.delete(); 
      } 
      return true;
    } catch (Exception exception) {
      return false;
    } 
  }
  
  public static Drawable getDrawableForUser(int paramInt1, Resources paramResources, int paramInt2) {
    String str1 = paramResources.getResourceEntryName(paramInt1);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str1);
    stringBuilder.append(".png");
    String str2 = stringBuilder.toString();
    return getLauncherDrawableByNameForUser(paramResources, str2, paramInt2);
  }
  
  public static Drawable getLauncherDrawableByNameForUser(Resources paramResources, String paramString, int paramInt) {
    return getDrawableByNameForUser(paramResources, paramString, "com.oppo.launcher", paramInt);
  }
  
  public static Drawable getIconDrawableByNameForUser(Resources paramResources, String paramString, int paramInt) {
    return getDrawableByNameForUser(paramResources, paramString, "icons", paramInt);
  }
  
  public static Drawable getDrawableByNameForUser(Resources paramResources, String paramString, int paramInt) {
    return getDrawableByNameForUser(paramResources, paramString, "com.oppo.launcher", paramInt);
  }
  
  public static Drawable getDrawableByNameForUser(Resources paramResources, String paramString1, String paramString2, int paramInt) {
    String str1 = null;
    Exception exception1 = null, exception2 = null;
    String str2 = null;
    String str3 = getThemePathForUser(paramInt);
    if (mIsDefaultTheme)
      str3 = OplusThemeUtil.getDefaultThemePath(); 
    InputStream inputStream1 = null, inputStream2 = null;
    ZipFile zipFile1 = null, zipFile2 = null;
    InputStream inputStream3 = inputStream2;
    ZipFile zipFile3 = zipFile2;
    InputStream inputStream4 = inputStream1;
    ZipFile zipFile4 = zipFile1;
    try {
      BitmapDrawable bitmapDrawable2;
      ZipFile zipFile6 = new ZipFile();
      inputStream3 = inputStream2;
      zipFile3 = zipFile2;
      inputStream4 = inputStream1;
      zipFile4 = zipFile1;
      StringBuilder stringBuilder = new StringBuilder();
      inputStream3 = inputStream2;
      zipFile3 = zipFile2;
      inputStream4 = inputStream1;
      zipFile4 = zipFile1;
      this();
      inputStream3 = inputStream2;
      zipFile3 = zipFile2;
      inputStream4 = inputStream1;
      zipFile4 = zipFile1;
      stringBuilder.append(str3);
      inputStream3 = inputStream2;
      zipFile3 = zipFile2;
      inputStream4 = inputStream1;
      zipFile4 = zipFile1;
      stringBuilder.append(paramString2);
      inputStream3 = inputStream2;
      zipFile3 = zipFile2;
      inputStream4 = inputStream1;
      zipFile4 = zipFile1;
      this(stringBuilder.toString());
      ZipFile zipFile5 = zipFile6;
      inputStream3 = inputStream2;
      zipFile3 = zipFile5;
      inputStream4 = inputStream1;
      zipFile4 = zipFile5;
      InputStream inputStream = getDrawableStream(paramResources, zipFile5, paramString1);
      paramString1 = str2;
      if (inputStream != null) {
        inputStream3 = inputStream;
        zipFile3 = zipFile5;
        inputStream4 = inputStream;
        zipFile4 = zipFile5;
        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
        inputStream3 = inputStream;
        zipFile3 = zipFile5;
        inputStream4 = inputStream;
        zipFile4 = zipFile5;
        bitmapDrawable2 = new BitmapDrawable();
        inputStream3 = inputStream;
        zipFile3 = zipFile5;
        inputStream4 = inputStream;
        zipFile4 = zipFile5;
        this(paramResources, bitmap);
      } 
      try {
        zipFile5.close();
      } catch (IOException iOException) {}
    } catch (Exception exception) {
      if (zipFile4 != null)
        try {
          zipFile4.close();
        } catch (IOException iOException) {} 
      exception = exception2;
    } finally {
      if (zipFile3 != null)
        try {
          zipFile3.close();
        } catch (IOException iOException) {} 
      if (inputStream3 != null)
        try {
          inputStream3.close();
        } catch (IOException iOException) {} 
    } 
    return (Drawable)paramResources;
  }
  
  public static Drawable getDrawable(int paramInt, Resources paramResources) {
    String str = paramResources.getResourceEntryName(paramInt);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str);
    stringBuilder.append(".png");
    str = stringBuilder.toString();
    return getLauncherDrawableByNameForUser(paramResources, str, 0);
  }
  
  public static Drawable getLauncherDrawableByName(Resources paramResources, String paramString) {
    return getDrawableByNameForUser(paramResources, paramString, "com.oppo.launcher", 0);
  }
  
  public static Drawable getIconDrawableByName(Resources paramResources, String paramString) {
    return getDrawableByNameForUser(paramResources, paramString, "icons", 0);
  }
  
  public static Drawable getDrawableByName(Resources paramResources, String paramString) {
    return getDrawableByNameForUser(paramResources, paramString, "com.oppo.launcher", 0);
  }
  
  public static Drawable getDrawableByName(Resources paramResources, String paramString1, String paramString2) {
    String str1 = null;
    Exception exception1 = null, exception2 = null;
    String str2 = null;
    String str3 = sThemePath;
    if (mIsDefaultTheme)
      str3 = OplusThemeUtil.getDefaultThemePath(); 
    InputStream inputStream1 = null, inputStream2 = null;
    ZipFile zipFile1 = null, zipFile2 = null;
    InputStream inputStream3 = inputStream2;
    ZipFile zipFile3 = zipFile2;
    InputStream inputStream4 = inputStream1;
    ZipFile zipFile4 = zipFile1;
    try {
      BitmapDrawable bitmapDrawable2;
      ZipFile zipFile6 = new ZipFile();
      inputStream3 = inputStream2;
      zipFile3 = zipFile2;
      inputStream4 = inputStream1;
      zipFile4 = zipFile1;
      StringBuilder stringBuilder = new StringBuilder();
      inputStream3 = inputStream2;
      zipFile3 = zipFile2;
      inputStream4 = inputStream1;
      zipFile4 = zipFile1;
      this();
      inputStream3 = inputStream2;
      zipFile3 = zipFile2;
      inputStream4 = inputStream1;
      zipFile4 = zipFile1;
      stringBuilder.append(str3);
      inputStream3 = inputStream2;
      zipFile3 = zipFile2;
      inputStream4 = inputStream1;
      zipFile4 = zipFile1;
      stringBuilder.append(paramString2);
      inputStream3 = inputStream2;
      zipFile3 = zipFile2;
      inputStream4 = inputStream1;
      zipFile4 = zipFile1;
      this(stringBuilder.toString());
      ZipFile zipFile5 = zipFile6;
      inputStream3 = inputStream2;
      zipFile3 = zipFile5;
      inputStream4 = inputStream1;
      zipFile4 = zipFile5;
      InputStream inputStream = getDrawableStream(paramResources, zipFile5, paramString1);
      paramString1 = str2;
      if (inputStream != null) {
        inputStream3 = inputStream;
        zipFile3 = zipFile5;
        inputStream4 = inputStream;
        zipFile4 = zipFile5;
        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
        inputStream3 = inputStream;
        zipFile3 = zipFile5;
        inputStream4 = inputStream;
        zipFile4 = zipFile5;
        bitmapDrawable2 = new BitmapDrawable();
        inputStream3 = inputStream;
        zipFile3 = zipFile5;
        inputStream4 = inputStream;
        zipFile4 = zipFile5;
        this(paramResources, bitmap);
      } 
      try {
        zipFile5.close();
      } catch (IOException iOException) {}
    } catch (Exception exception) {
      if (zipFile4 != null)
        try {
          zipFile4.close();
        } catch (IOException iOException) {} 
      exception = exception2;
    } finally {
      if (zipFile3 != null)
        try {
          zipFile3.close();
        } catch (IOException iOException) {} 
      if (inputStream3 != null)
        try {
          inputStream3.close();
        } catch (IOException iOException) {} 
    } 
    return (Drawable)paramResources;
  }
  
  private static InputStream getDrawableStream(Resources paramResources, ZipFile paramZipFile, String paramString) throws Exception {
    String[] arrayOfString1, arrayOfString2 = DIRS_DRAWABLE;
    float f = (paramResources.getDisplayMetrics()).density;
    if (f >= 3.0F) {
      arrayOfString1 = DIRS_DRAWABLE_XXH;
    } else {
      arrayOfString1 = arrayOfString2;
      if (f >= 2.0F)
        arrayOfString1 = DIRS_DRAWABLE_XH; 
    } 
    for (byte b = 0; b <= 2; b++) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(arrayOfString1[b]);
      stringBuilder.append(paramString);
      ZipEntry zipEntry = paramZipFile.getEntry(stringBuilder.toString());
      if (zipEntry != null)
        return paramZipFile.getInputStream(zipEntry); 
    } 
    return null;
  }
  
  protected static String getThemePathForUser(int paramInt) {
    if (paramInt <= 0 || CUSTOM_THEME_PATH.equals(sThemePath))
      return sThemePath; 
    StringBuilder stringBuilder1 = new StringBuilder();
    String str = sThemePath;
    stringBuilder1.append(str);
    stringBuilder1.append(paramInt);
    StringBuilder stringBuilder2 = stringBuilder1.append("/");
    return stringBuilder2.toString();
  }
  
  public static void setDefaultTheme() {
    setDefaultTheme(0);
  }
  
  public static boolean getDefaultTheme() {
    return getDefaultTheme(0);
  }
  
  public static void setDefaultTheme(int paramInt) {
    long l = SystemProperties.getLong(getThemeKeyForUser(paramInt), 0L);
    if ((0x1L & l) == 0L) {
      mIsDefaultTheme = true;
    } else {
      mIsDefaultTheme = false;
    } 
    if ((l & 0x100L) == 256L) {
      sThemePath = CUSTOM_THEME_PATH;
    } else {
      sThemePath = "/data/theme/";
    } 
  }
  
  public static boolean getDefaultTheme(int paramInt) {
    long l = SystemProperties.getLong(getThemeKeyForUser(paramInt), 0L);
    if ((0x1L & l) == 0L)
      return true; 
    return false;
  }
  
  public static String getThemeKeyForUser(int paramInt) {
    if (paramInt <= 0)
      return "persist.sys.themeflag"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("persist.sys.themeflag");
    stringBuilder.append(".");
    stringBuilder = stringBuilder.append(paramInt);
    return stringBuilder.toString();
  }
}
