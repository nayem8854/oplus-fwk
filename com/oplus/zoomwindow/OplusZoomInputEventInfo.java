package com.oplus.zoomwindow;

import android.graphics.Point;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

public class OplusZoomInputEventInfo implements Parcelable {
  public IBinder who;
  
  public Point point = new Point();
  
  public Bundle extension = new Bundle();
  
  public int evActionFlag;
  
  public String callPkg;
  
  public OplusZoomInputEventInfo(Parcel paramParcel) {
    this.point = (Point)paramParcel.readParcelable(Point.class.getClassLoader());
    this.evActionFlag = paramParcel.readInt();
    this.callPkg = paramParcel.readString();
    this.extension = paramParcel.readBundle();
    this.who = paramParcel.readStrongBinder();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.point, 0);
    paramParcel.writeInt(this.evActionFlag);
    paramParcel.writeString(this.callPkg);
    paramParcel.writeBundle(this.extension);
    paramParcel.writeStrongBinder(this.who);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<OplusZoomInputEventInfo> CREATOR = new Parcelable.Creator<OplusZoomInputEventInfo>() {
      public OplusZoomInputEventInfo createFromParcel(Parcel param1Parcel) {
        return new OplusZoomInputEventInfo(param1Parcel);
      }
      
      public OplusZoomInputEventInfo[] newArray(int param1Int) {
        return new OplusZoomInputEventInfo[param1Int];
      }
    };
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("OplusZoomInputEventInfo = { ");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" point = ");
    stringBuilder2.append(this.point);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" evActionFlag = ");
    stringBuilder2.append(this.evActionFlag);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" callPkg = ");
    stringBuilder2.append(this.callPkg);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" extension = ");
    stringBuilder2.append(this.extension);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append("}");
    return stringBuilder1.toString();
  }
  
  public OplusZoomInputEventInfo() {}
}
