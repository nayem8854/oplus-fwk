package com.oplus.zoomwindow;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusZoomWindowObserver extends IInterface {
  void onInputMethodChanged(boolean paramBoolean) throws RemoteException;
  
  void onZoomWindowDied(String paramString) throws RemoteException;
  
  void onZoomWindowHide(OplusZoomWindowInfo paramOplusZoomWindowInfo) throws RemoteException;
  
  void onZoomWindowShow(OplusZoomWindowInfo paramOplusZoomWindowInfo) throws RemoteException;
  
  class Default implements IOplusZoomWindowObserver {
    public void onZoomWindowShow(OplusZoomWindowInfo param1OplusZoomWindowInfo) throws RemoteException {}
    
    public void onZoomWindowHide(OplusZoomWindowInfo param1OplusZoomWindowInfo) throws RemoteException {}
    
    public void onZoomWindowDied(String param1String) throws RemoteException {}
    
    public void onInputMethodChanged(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusZoomWindowObserver {
    private static final String DESCRIPTOR = "com.oplus.zoomwindow.IOplusZoomWindowObserver";
    
    static final int TRANSACTION_onInputMethodChanged = 4;
    
    static final int TRANSACTION_onZoomWindowDied = 3;
    
    static final int TRANSACTION_onZoomWindowHide = 2;
    
    static final int TRANSACTION_onZoomWindowShow = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.zoomwindow.IOplusZoomWindowObserver");
    }
    
    public static IOplusZoomWindowObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.zoomwindow.IOplusZoomWindowObserver");
      if (iInterface != null && iInterface instanceof IOplusZoomWindowObserver)
        return (IOplusZoomWindowObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onInputMethodChanged";
          } 
          return "onZoomWindowDied";
        } 
        return "onZoomWindowHide";
      } 
      return "onZoomWindowShow";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            boolean bool;
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("com.oplus.zoomwindow.IOplusZoomWindowObserver");
              return true;
            } 
            param1Parcel1.enforceInterface("com.oplus.zoomwindow.IOplusZoomWindowObserver");
            if (param1Parcel1.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            onInputMethodChanged(bool);
            return true;
          } 
          param1Parcel1.enforceInterface("com.oplus.zoomwindow.IOplusZoomWindowObserver");
          str = param1Parcel1.readString();
          onZoomWindowDied(str);
          return true;
        } 
        str.enforceInterface("com.oplus.zoomwindow.IOplusZoomWindowObserver");
        if (str.readInt() != 0) {
          OplusZoomWindowInfo oplusZoomWindowInfo = (OplusZoomWindowInfo)OplusZoomWindowInfo.CREATOR.createFromParcel((Parcel)str);
        } else {
          str = null;
        } 
        onZoomWindowHide((OplusZoomWindowInfo)str);
        return true;
      } 
      str.enforceInterface("com.oplus.zoomwindow.IOplusZoomWindowObserver");
      if (str.readInt() != 0) {
        OplusZoomWindowInfo oplusZoomWindowInfo = (OplusZoomWindowInfo)OplusZoomWindowInfo.CREATOR.createFromParcel((Parcel)str);
      } else {
        str = null;
      } 
      onZoomWindowShow((OplusZoomWindowInfo)str);
      return true;
    }
    
    private static class Proxy implements IOplusZoomWindowObserver {
      public static IOplusZoomWindowObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.zoomwindow.IOplusZoomWindowObserver";
      }
      
      public void onZoomWindowShow(OplusZoomWindowInfo param2OplusZoomWindowInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.zoomwindow.IOplusZoomWindowObserver");
          if (param2OplusZoomWindowInfo != null) {
            parcel.writeInt(1);
            param2OplusZoomWindowInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusZoomWindowObserver.Stub.getDefaultImpl() != null) {
            IOplusZoomWindowObserver.Stub.getDefaultImpl().onZoomWindowShow(param2OplusZoomWindowInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onZoomWindowHide(OplusZoomWindowInfo param2OplusZoomWindowInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.zoomwindow.IOplusZoomWindowObserver");
          if (param2OplusZoomWindowInfo != null) {
            parcel.writeInt(1);
            param2OplusZoomWindowInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IOplusZoomWindowObserver.Stub.getDefaultImpl() != null) {
            IOplusZoomWindowObserver.Stub.getDefaultImpl().onZoomWindowHide(param2OplusZoomWindowInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onZoomWindowDied(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.zoomwindow.IOplusZoomWindowObserver");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IOplusZoomWindowObserver.Stub.getDefaultImpl() != null) {
            IOplusZoomWindowObserver.Stub.getDefaultImpl().onZoomWindowDied(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onInputMethodChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.oplus.zoomwindow.IOplusZoomWindowObserver");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(4, parcel, null, 1);
          if (!bool1 && IOplusZoomWindowObserver.Stub.getDefaultImpl() != null) {
            IOplusZoomWindowObserver.Stub.getDefaultImpl().onInputMethodChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusZoomWindowObserver param1IOplusZoomWindowObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusZoomWindowObserver != null) {
          Proxy.sDefaultImpl = param1IOplusZoomWindowObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusZoomWindowObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
