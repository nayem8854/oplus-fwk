package com.oplus.zoomwindow;

import android.graphics.Rect;
import android.graphics.Region;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public class OplusZoomWindowRegion implements Parcelable {
  private List<Rect> mRectList = new ArrayList<>();
  
  public Region getRegion() {
    Region region = new Region();
    for (byte b = 0; b < this.mRectList.size(); b++) {
      Rect rect = this.mRectList.get(b);
      region.op(rect, Region.Op.UNION);
    } 
    return region;
  }
  
  public List<Rect> getRectList() {
    return this.mRectList;
  }
  
  public OplusZoomWindowRegion(Parcel paramParcel) {
    this.mRectList = paramParcel.createTypedArrayList(Rect.CREATOR);
  }
  
  public static final Parcelable.Creator<OplusZoomWindowRegion> CREATOR = new Parcelable.Creator<OplusZoomWindowRegion>() {
      public OplusZoomWindowRegion createFromParcel(Parcel param1Parcel) {
        return new OplusZoomWindowRegion(param1Parcel);
      }
      
      public OplusZoomWindowRegion[] newArray(int param1Int) {
        return new OplusZoomWindowRegion[param1Int];
      }
    };
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeTypedList(this.mRectList);
  }
  
  public String toString() {
    Region region = getRegion();
    return region.toString();
  }
  
  public OplusZoomWindowRegion() {}
}
