package com.oplus.zoomwindow;

import android.os.Parcel;
import android.os.Parcelable;

public class OplusZoomWindowSize implements Parcelable {
  public OplusZoomWindowSize() {}
  
  protected OplusZoomWindowSize(Parcel paramParcel) {
    this.mPortraitWidth = paramParcel.readInt();
    this.mPortraitHeight = paramParcel.readInt();
    this.mLandScapeWidth = paramParcel.readInt();
    this.mLandScapeHeight = paramParcel.readInt();
  }
  
  public static final Parcelable.Creator<OplusZoomWindowSize> CREATOR = new Parcelable.Creator<OplusZoomWindowSize>() {
      public OplusZoomWindowSize createFromParcel(Parcel param1Parcel) {
        return new OplusZoomWindowSize(param1Parcel);
      }
      
      public OplusZoomWindowSize[] newArray(int param1Int) {
        return new OplusZoomWindowSize[param1Int];
      }
    };
  
  private int mLandScapeHeight;
  
  private int mLandScapeWidth;
  
  private int mPortraitHeight;
  
  private int mPortraitWidth;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mPortraitWidth);
    paramParcel.writeInt(this.mPortraitHeight);
    paramParcel.writeInt(this.mLandScapeWidth);
    paramParcel.writeInt(this.mLandScapeHeight);
  }
  
  public int getPortraitWidth() {
    return this.mPortraitWidth;
  }
  
  public int getPortraitHeight() {
    return this.mPortraitHeight;
  }
  
  public int getLandScapeWidth() {
    return this.mLandScapeWidth;
  }
  
  public int getLandScapeHeight() {
    return this.mLandScapeHeight;
  }
  
  public void setZoomWindowPortraitWidth(int paramInt) {
    this.mPortraitWidth = paramInt;
  }
  
  public void setZoomWindowPortraitHeight(int paramInt) {
    this.mPortraitHeight = paramInt;
  }
  
  public void setZoomWindowLandScapeWidth(int paramInt) {
    this.mLandScapeWidth = paramInt;
  }
  
  public void setZoomWindowlandScapeHeight(int paramInt) {
    this.mLandScapeHeight = paramInt;
  }
  
  public void setZoomWindowSize(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mPortraitWidth = paramInt1;
    this.mPortraitHeight = paramInt2;
    this.mLandScapeWidth = paramInt3;
    this.mLandScapeHeight = paramInt4;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("PortraitWidth = ");
    stringBuilder.append(this.mPortraitWidth);
    stringBuilder.append(",PortraitHeight = ");
    stringBuilder.append(this.mPortraitHeight);
    stringBuilder.append(",LandScapeWidth = ");
    stringBuilder.append(this.mLandScapeWidth);
    stringBuilder.append(",LandScapeHeight = ");
    stringBuilder.append(this.mLandScapeHeight);
    return stringBuilder.toString();
  }
}
