package com.oplus.zoomwindow;

import android.os.Parcel;
import android.os.Parcelable;

public class OplusZoomWindowSupportAppInfo implements Parcelable {
  public OplusZoomWindowSupportAppInfo() {}
  
  public String getPkg() {
    return this.mPkg;
  }
  
  public int getAppVersionCode() {
    return this.mAppVersionCode;
  }
  
  public int getFeatureVersionCode() {
    return this.mFeatureVersionCode;
  }
  
  public void setPkg(String paramString) {
    this.mPkg = paramString;
  }
  
  public void setAppVersionCode(int paramInt) {
    this.mAppVersionCode = paramInt;
  }
  
  public void setFeatureVersionCode(int paramInt) {
    this.mFeatureVersionCode = paramInt;
  }
  
  public OplusZoomWindowSupportAppInfo(Parcel paramParcel) {
    this.mPkg = paramParcel.readString();
    this.mAppVersionCode = paramParcel.readInt();
    this.mFeatureVersionCode = paramParcel.readInt();
  }
  
  public static final Parcelable.Creator<OplusZoomWindowSupportAppInfo> CREATOR = new Parcelable.Creator<OplusZoomWindowSupportAppInfo>() {
      public OplusZoomWindowSupportAppInfo createFromParcel(Parcel param1Parcel) {
        return new OplusZoomWindowSupportAppInfo(param1Parcel);
      }
      
      public OplusZoomWindowSupportAppInfo[] newArray(int param1Int) {
        return new OplusZoomWindowSupportAppInfo[param1Int];
      }
    };
  
  private int mAppVersionCode;
  
  private int mFeatureVersionCode;
  
  private String mPkg;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mPkg);
    paramParcel.writeInt(this.mAppVersionCode);
    paramParcel.writeInt(this.mFeatureVersionCode);
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("OplusZoomWindowSupportAppInfo{");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("pkgName=");
    stringBuilder2.append(this.mPkg);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", AppVersionCode=");
    stringBuilder2.append(this.mAppVersionCode);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", FeatureVersionCode=");
    stringBuilder2.append(this.mFeatureVersionCode);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append("}");
    return stringBuilder1.toString();
  }
}
