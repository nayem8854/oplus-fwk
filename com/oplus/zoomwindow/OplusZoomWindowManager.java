package com.oplus.zoomwindow;

import android.app.OplusActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.ArrayMap;
import android.util.Log;
import android.util.Slog;
import com.oplus.app.IOplusZoomWindowConfigChangedListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OplusZoomWindowManager {
  public static final int ACTION_MASK_ON_SHOWING_OF_MINI_ZOOM_MODE = -8193;
  
  public static final int ACTION_MASK_ON_SHOWING_OF_ZOOM_MODE = -2;
  
  public static final int DEFAULT_TOP_OFFSET_OF_ZOOM_PORTRAIT = 317;
  
  public static final String EXTRA_WINDOW_MODE = "extra_window_mode";
  
  public static final int FLAG_BUBBLE_ZOOM_WINDOW = 2;
  
  public static final int FLAG_CLICKED_FULL_SCREEN_BUTTON = 5;
  
  public static final int FLAG_CLICK_OUTSIDE_ZOOM = 3;
  
  public static final int FLAG_DELETE_MINI_ZOOM_WINDOW = 8;
  
  public static final int FLAG_DELETE_ON_ACTION_USER_SWITCHED = 15;
  
  public static final int FLAG_DELETE_ON_CLICKED_CLOSED_ZOOM = 14;
  
  public static final int FLAG_DELETE_ON_ENTER_DRAG_WINDOW = 10;
  
  public static final int FLAG_DELETE_ON_ENTER_SPEC_MODE = 13;
  
  public static final int FLAG_DELETE_ZOOM_BY_RECENTS = 12;
  
  public static final int FLAG_DELETE_ZOOM_BY_SCREEN_OFF = 11;
  
  public static final int FLAG_DELETE_ZOOM_WINDOW = 1;
  
  public static final int FLAG_DELETE_ZOOM_WINDOW_BY_GLOBAL_DRAG = 9;
  
  public static final int FLAG_EXIT_ZOOM_BY_OTHERS = 6;
  
  public static final int FLAG_HIDE_ZOOM_WINDOW = 2;
  
  public static final int FLAG_MOVE_TO_HOT_EDGE = 7;
  
  public static final int FLAG_ON_DEFAULT_ACTION = 0;
  
  public static final int FLAG_ON_DELETE_CONTROL_VIEW = 2;
  
  public static final int FLAG_ON_DELETE_HANDLE = 8;
  
  public static final int FLAG_ON_DELETE_MINI_ZOOM_BUTTON_RESTORE = 65536;
  
  public static final int FLAG_ON_DELETE_MINI_ZOOM_BUTTON_SELECTED = 32768;
  
  public static final int FLAG_ON_DELETE_MINI_ZOOM_CONTROL_VIEW = 16384;
  
  public static final int FLAG_ON_DELETE_ZOOM_BUTTON_RESTORE = 2048;
  
  public static final int FLAG_ON_DELETE_ZOOM_BUTTON_SELECTED = 256;
  
  public static final int FLAG_ON_DISABLE_CONTROL_VIEW = 64;
  
  public static final int FLAG_ON_FLING = 2;
  
  public static final int FLAG_ON_FULL_SCREEN_BUTTON_RESTORE = 1024;
  
  public static final int FLAG_ON_FULL_SCREEN_BUTTON_SELECTED = 128;
  
  public static final int FLAG_ON_HIDE_HANDLE = 16;
  
  public static final int FLAG_ON_HIDE_TIPS_VIEW = 1048576;
  
  public static final int FLAG_ON_LONG_PRESS = 1;
  
  public static final int FLAG_ON_MINI_ZOOM_BUTTON_RESTORE = 4096;
  
  public static final int FLAG_ON_MINI_ZOOM_BUTTON_SELECTED = 512;
  
  public static final int FLAG_ON_MINI_ZOOM_SHOW_TIPS_VIEW = 262144;
  
  public static final int FLAG_ON_SHOW_CONTROL_VIEW = 1;
  
  public static final int FLAG_ON_SHOW_GUIDE = 2097152;
  
  public static final int FLAG_ON_SHOW_HANDLE = 4;
  
  public static final int FLAG_ON_SHOW_MINI_ZOOM_CONTROL_VIEW = 8192;
  
  public static final int FLAG_ON_SINGLE_TAP_TO_CLOSED_ZOOM = 16;
  
  public static final int FLAG_ON_SINGLE_TAP_TO_SHOW_TIP = 8;
  
  public static final int FLAG_ON_SING_TAP_MINI_ZOOM = 524288;
  
  public static final int FLAG_ON_SING_TAP_SHOW_TIPS_VIEW = 131072;
  
  public static final int FLAG_ON_START_BY_LAUNCHER_GESTURE_FROM_FULL_SCREEN = 1;
  
  public static final int FLAG_ON_START_BY_LAUNCHER_SWIPE_UP_FROM_ZOOM = 3;
  
  public static final int FLAG_ON_START_BY_MINI_ZOOM_BUTTON_FROM_ZOOM = 2;
  
  public static final int FLAG_ON_START_FULL_SCREEN_FROM_RECENTS = 3;
  
  public static final int FLAG_ON_START_MINI_ZOOM_BY_LAUNCHER_CARD = 4;
  
  public static final int FLAG_ON_START_MINI_ZOOM_FROM_FULL_SCREEN = 1;
  
  public static final int FLAG_ON_START_MINI_ZOOM_FROM_RECENTS = 2;
  
  public static final int FLAG_ON_START_MINI_ZOOM_FROM_ZOOM = 2;
  
  public static final int FLAG_ON_START_ZOOM_FROM_RECENTS = 1;
  
  public static final int FLAG_ON_UP = 4;
  
  public static final int FLAG_SHOW_ZOOM_WINDOW = 1;
  
  public static final int FLAG_UNSUPPORT_ZOOM = 4;
  
  public static final String KEY_OF_DELETE_MINI_ZOOM_BUTTON = "delete_mini_zoom_button";
  
  public static final String KEY_OF_DELETE_ZOOM_BUTTON = "delete_zoom_button";
  
  public static final String KEY_OF_FULL_SCREEN_BUTTON = "full_screen_button";
  
  public static final String KEY_OF_MINI_ZOOM_BUTTON = "mini_zoom_button";
  
  public static final String KEY_ZOOM_TASK_ID_FROM_RECENTS = "zoom_task_id";
  
  public static final String KEY_ZOOM_TYPE_FROM_RECENTS = "android:activity.mZoomLaunchFlags";
  
  public static final int LEFT_RIGHT_LIMIT_EDGE_OF_MOVE_MINI_ZOOM = 16;
  
  public static final float MINI_ZOOM_SCALE_FOR_LANDSCAPE_DEFAULT = 0.288F;
  
  public static final float MINI_ZOOM_SCALE_FOR_PORTRAIT_DEFAULT = 0.264F;
  
  private static final String TAG = "OplusZoomWindowManager";
  
  public static final int TOP_BOTTOM_LIMIT_EDGE_OF_MOVE_MINI_ZOOM_LANDSCAPE = 16;
  
  public static final int TOP_BOTTOM_LIMIT_EDGE_OF_MOVE_MINI_ZOOM_PORTRAIT = 40;
  
  public static final int TYPE_FORCED_RELAUNCH_ZOOM_CPN_LIST = 9;
  
  public static final int TYPE_FORCES_SHOW_TOAST_LIST = 4;
  
  public static final int TYPE_NOT_SHOW_TOAST_LIST = 5;
  
  public static final int TYPE_SHOW_COMPATIBILITY_TOAST = 1;
  
  public static final int TYPE_SHOW_UNSUPPORT_TOAST = 2;
  
  public static final int TYPE_UNRELAUNCH_ZOOM_CPN_LIST = 8;
  
  public static final int TYPE_UNREUSED_ZOOM_CPN_LIST = 7;
  
  public static final int TYPE_UNSUPPORT_ZOOM_CPN_LIST = 6;
  
  public static final int TYPE_ZOOM_APP_BLACK_LIST = 2;
  
  public static final int TYPE_ZOOM_APP_REPLY_LIST = 3;
  
  public static final int TYPE_ZOOM_APP_SUPPORT_LIST = 1;
  
  public static final int WINDOWING_MODE_FULLSCREEN = 1;
  
  public static final int WINDOWING_MODE_ZOOM = 100;
  
  public static final int WINDOWING_MODE_ZOOM_LEGACY = 6;
  
  public static final int WINDOWING_MODE_ZOOM_TO_FULLSCREEN = 101;
  
  public static final int WINDOW_TYPE_MINIZOOM = 2;
  
  public static final int WINDOW_TYPE_UNDEFINE = 0;
  
  public static final int WINDOW_TYPE_ZOOM = 1;
  
  public static final int ZOOM_CORNER_RADIUS_LANDSCAPE = 15;
  
  public static final int ZOOM_CORNER_RADIUS_PORTRAIT = 22;
  
  public static final float ZOOM_LANDSCAPE_APP_RATIO = 1.6666666F;
  
  public static final float ZOOM_LANDSCAPE_RATIO = 1.45F;
  
  public static final float ZOOM_RATIO = 1.6666666F;
  
  public static final float ZOOM_SCALE_FOR_LANDSCAPE_APP_DEFAULT = 0.556F;
  
  public static final float ZOOM_SCALE_FOR_LANDSCAPE_DEFAULT = 0.6F;
  
  public static final float ZOOM_SCALE_FOR_PORTRAIT_DEFAULT = 0.667F;
  
  private static volatile OplusZoomWindowManager sInstance;
  
  private final Map<OnConfigChangedListener, IOplusZoomWindowConfigChangedListener> mConfigListeners = (Map<OnConfigChangedListener, IOplusZoomWindowConfigChangedListener>)new ArrayMap();
  
  private OplusActivityManager mOAms;
  
  public static OplusZoomWindowManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/zoomwindow/OplusZoomWindowManager.sInstance : Lcom/oplus/zoomwindow/OplusZoomWindowManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/zoomwindow/OplusZoomWindowManager
    //   8: monitorenter
    //   9: getstatic com/oplus/zoomwindow/OplusZoomWindowManager.sInstance : Lcom/oplus/zoomwindow/OplusZoomWindowManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/zoomwindow/OplusZoomWindowManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/zoomwindow/OplusZoomWindowManager.sInstance : Lcom/oplus/zoomwindow/OplusZoomWindowManager;
    //   27: ldc com/oplus/zoomwindow/OplusZoomWindowManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/zoomwindow/OplusZoomWindowManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/zoomwindow/OplusZoomWindowManager.sInstance : Lcom/oplus/zoomwindow/OplusZoomWindowManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #309	-> 0
    //   #310	-> 6
    //   #311	-> 9
    //   #312	-> 15
    //   #314	-> 27
    //   #316	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  private OplusZoomWindowManager() {
    this.mOAms = new OplusActivityManager();
  }
  
  public int startZoomWindow(Intent paramIntent, Bundle paramBundle, int paramInt, String paramString) {
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("startZoomWindow: ");
      stringBuilder.append(paramIntent);
      stringBuilder.append(" callPkg: ");
      stringBuilder.append(paramString);
      Log.v("OplusZoomWindowManager", stringBuilder.toString());
      return this.mOAms.startZoomWindow(paramIntent, paramBundle, paramInt, paramString);
    } catch (RemoteException remoteException) {
      Log.e("OplusZoomWindowManager", "startZoomWindow remoteException ");
      remoteException.printStackTrace();
      return -1;
    } 
  }
  
  public boolean registerZoomWindowObserver(IOplusZoomWindowObserver paramIOplusZoomWindowObserver) {
    try {
      return this.mOAms.registerZoomWindowObserver(paramIOplusZoomWindowObserver);
    } catch (RemoteException remoteException) {
      Log.e("OplusZoomWindowManager", "registerZoomWindowObserver remoteException ");
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public boolean unregisterZoomWindowObserver(IOplusZoomWindowObserver paramIOplusZoomWindowObserver) {
    try {
      return this.mOAms.unregisterZoomWindowObserver(paramIOplusZoomWindowObserver);
    } catch (RemoteException remoteException) {
      Log.e("OplusZoomWindowManager", "unregisterZoomWindowObserver remoteException ");
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public boolean registerZoomAppObserver(IOplusZoomAppObserver paramIOplusZoomAppObserver) {
    Slog.i("OplusZoomWindowManager", "registerZoomAppObserver start");
    try {
      return this.mOAms.registerZoomAppObserver(paramIOplusZoomAppObserver);
    } catch (RemoteException remoteException) {
      Log.e("OplusZoomWindowManager", "registerZoomAppObserver remoteException ");
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public boolean unregisterZoomAppObserver(IOplusZoomAppObserver paramIOplusZoomAppObserver) {
    Slog.i("OplusZoomWindowManager", "unregisterZoomAppObserver start");
    try {
      return this.mOAms.unregisterZoomAppObserver(paramIOplusZoomAppObserver);
    } catch (RemoteException remoteException) {
      Log.e("OplusZoomWindowManager", "unregisterZoomAppObserver remoteException ");
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public boolean isSupportZoomWindowMode() {
    try {
      return this.mOAms.isSupportZoomWindowMode();
    } catch (RemoteException remoteException) {
      Log.e("OplusZoomWindowManager", "isSupportZoomWindowMode remoteException ");
      remoteException.printStackTrace();
      return true;
    } 
  }
  
  public boolean isSupportZoomMode(String paramString1, int paramInt, String paramString2, Bundle paramBundle) {
    try {
      return this.mOAms.isSupportZoomMode(paramString1, paramInt, paramString2, paramBundle);
    } catch (RemoteException remoteException) {
      Log.e("OplusZoomWindowManager", "isSupportZoomMode remoteException ");
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public boolean handleShowCompatibilityToast(String paramString1, int paramInt1, String paramString2, Bundle paramBundle, int paramInt2) {
    try {
      return this.mOAms.handleShowCompatibilityToast(paramString1, paramInt1, paramString2, paramBundle, paramInt2);
    } catch (RemoteException remoteException) {
      Log.e("OplusZoomWindowManager", "handleShowCompatibilityToast remoteException ");
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public OplusZoomWindowInfo getCurrentZoomWindowState() {
    try {
      return this.mOAms.getCurrentZoomWindowState();
    } catch (RemoteException remoteException) {
      Log.e("OplusZoomWindowManager", "getCurrentZoomWindowState remoteException ");
      remoteException.printStackTrace();
      return null;
    } 
  }
  
  public void hideZoomWindow(int paramInt) {
    try {
      this.mOAms.hideZoomWindow(paramInt);
    } catch (RemoteException remoteException) {
      Log.e("OplusZoomWindowManager", "hideZoomWindow remoteException ");
      remoteException.printStackTrace();
    } 
  }
  
  public List<String> getZoomAppConfigList(int paramInt) {
    try {
      return this.mOAms.getZoomAppConfigList(paramInt);
    } catch (RemoteException remoteException) {
      Log.e("OplusZoomWindowManager", "hideZoomWindow remoteException ");
      remoteException.printStackTrace();
      return new ArrayList<>(0);
    } 
  }
  
  public OplusZoomWindowRUSConfig getZoomWindowConfig() {
    Log.i("OplusZoomWindowManager", "getZoomWindowConfig start");
    try {
      return this.mOAms.getZoomWindowConfig();
    } catch (RemoteException remoteException) {
      Log.e("OplusZoomWindowManager", "getZoomWindowConfig remoteException ");
      remoteException.printStackTrace();
      return new OplusZoomWindowRUSConfig();
    } 
  }
  
  public void setZoomWindowConfig(OplusZoomWindowRUSConfig paramOplusZoomWindowRUSConfig) {
    Log.i("OplusZoomWindowManager", "setZoomWindowConfig start");
    try {
      this.mOAms.setZoomWindowConfig(paramOplusZoomWindowRUSConfig);
    } catch (RemoteException remoteException) {
      Log.e("OplusZoomWindowManager", "setZoomWindowConfig remoteException ");
      remoteException.printStackTrace();
    } 
  }
  
  public boolean addOnConfigChangedListener(OnConfigChangedListener paramOnConfigChangedListener) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("addOnConfigChangedListener listener = ");
    stringBuilder.append(paramOnConfigChangedListener);
    Log.i("OplusZoomWindowManager", stringBuilder.toString());
    synchronized (this.mConfigListeners) {
      if (this.mConfigListeners.get(paramOnConfigChangedListener) != null) {
        Log.i("OplusZoomWindowManager", "addOnConfigChangedListener already added before");
        return false;
      } 
      OnConfigChangeListenerDelegate onConfigChangeListenerDelegate = new OnConfigChangeListenerDelegate();
      this(this, paramOnConfigChangedListener, Looper.getMainLooper());
      try {
        if (this.mOAms != null) {
          this.mConfigListeners.put(paramOnConfigChangedListener, onConfigChangeListenerDelegate);
          return this.mOAms.addZoomWindowConfigChangedListener(onConfigChangeListenerDelegate);
        } 
      } catch (RemoteException remoteException) {
        Log.e("OplusZoomWindowManager", "addOnConfigChangedListener remoteException ");
        remoteException.printStackTrace();
      } 
      return false;
    } 
  }
  
  public boolean removeOnConfigChangedListener(OnConfigChangedListener paramOnConfigChangedListener) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("removeOnConfigChangedListener listener = ");
    stringBuilder.append(paramOnConfigChangedListener);
    Log.i("OplusZoomWindowManager", stringBuilder.toString());
    synchronized (this.mConfigListeners) {
      IOplusZoomWindowConfigChangedListener iOplusZoomWindowConfigChangedListener = this.mConfigListeners.get(paramOnConfigChangedListener);
      if (iOplusZoomWindowConfigChangedListener != null)
        try {
          if (this.mOAms != null) {
            this.mConfigListeners.remove(paramOnConfigChangedListener);
            return this.mOAms.removeZoomWindowConfigChangedListener(iOplusZoomWindowConfigChangedListener);
          } 
        } catch (RemoteException remoteException) {
          Log.e("OplusZoomWindowManager", "removeOnConfigChangedListener remoteException ");
          remoteException.printStackTrace();
        }  
      return false;
    } 
  }
  
  class OnConfigChangeListenerDelegate extends IOplusZoomWindowConfigChangedListener.Stub implements Handler.Callback {
    private static final int MSG_CONFIG_SWITCH_CHANGED = 2;
    
    private static final int MSG_CONFIG_TYPE_CHANGED = 1;
    
    private final Handler mHandler;
    
    private final OplusZoomWindowManager.OnConfigChangedListener mListener;
    
    final OplusZoomWindowManager this$0;
    
    public OnConfigChangeListenerDelegate(OplusZoomWindowManager.OnConfigChangedListener param1OnConfigChangedListener, Looper param1Looper) {
      this.mListener = param1OnConfigChangedListener;
      this.mHandler = new Handler(param1Looper, this);
    }
    
    public void onConfigTypeChanged(int param1Int) {
      this.mHandler.obtainMessage(1, param1Int, 0).sendToTarget();
    }
    
    public void onConfigSwitchChanged(boolean param1Boolean) {
      Handler handler = this.mHandler;
      Message message = handler.obtainMessage(2, param1Boolean, 0);
      message.sendToTarget();
    }
    
    public boolean handleMessage(Message param1Message) {
      int i = param1Message.what;
      if (i != 1) {
        boolean bool = false;
        if (i != 2)
          return false; 
        if (param1Message.arg1 != 0)
          bool = true; 
        this.mListener.onConfigSwitchChanged(bool);
        return true;
      } 
      i = param1Message.arg1;
      this.mListener.onConfigTypeChanged(i);
      return true;
    }
  }
  
  public void onInputEvent(OplusZoomInputEventInfo paramOplusZoomInputEventInfo) {
    try {
      this.mOAms.onInputEvent(paramOplusZoomInputEventInfo);
    } catch (RemoteException remoteException) {
      Log.e("OplusZoomWindowManager", "onInputEvent remoteException ");
      remoteException.printStackTrace();
    } 
  }
  
  public void onControlViewChanged(OplusZoomControlViewInfo paramOplusZoomControlViewInfo) {
    try {
      this.mOAms.onControlViewChanged(paramOplusZoomControlViewInfo);
    } catch (RemoteException remoteException) {
      Log.e("OplusZoomWindowManager", "onControlViewChanged remoteException ");
      remoteException.printStackTrace();
    } 
  }
  
  public void setBubbleMode(boolean paramBoolean) {}
  
  public static interface OnConfigChangedListener {
    void onConfigSwitchChanged(boolean param1Boolean);
    
    void onConfigTypeChanged(int param1Int);
  }
}
