package com.oplus.zoomwindow;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusZoomAppObserver extends IInterface {
  void notifyControlViewChange(OplusZoomWindowInfo paramOplusZoomWindowInfo) throws RemoteException;
  
  void notifyShowCompatibilityToast(int paramInt1, int paramInt2, String paramString1, String paramString2, Bundle paramBundle) throws RemoteException;
  
  class Default implements IOplusZoomAppObserver {
    public void notifyControlViewChange(OplusZoomWindowInfo param1OplusZoomWindowInfo) throws RemoteException {}
    
    public void notifyShowCompatibilityToast(int param1Int1, int param1Int2, String param1String1, String param1String2, Bundle param1Bundle) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusZoomAppObserver {
    private static final String DESCRIPTOR = "com.oplus.zoomwindow.IOplusZoomAppObserver";
    
    static final int TRANSACTION_notifyControlViewChange = 1;
    
    static final int TRANSACTION_notifyShowCompatibilityToast = 2;
    
    public Stub() {
      attachInterface(this, "com.oplus.zoomwindow.IOplusZoomAppObserver");
    }
    
    public static IOplusZoomAppObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.zoomwindow.IOplusZoomAppObserver");
      if (iInterface != null && iInterface instanceof IOplusZoomAppObserver)
        return (IOplusZoomAppObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "notifyShowCompatibilityToast";
      } 
      return "notifyControlViewChange";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("com.oplus.zoomwindow.IOplusZoomAppObserver");
          return true;
        } 
        param1Parcel1.enforceInterface("com.oplus.zoomwindow.IOplusZoomAppObserver");
        param1Int1 = param1Parcel1.readInt();
        param1Int2 = param1Parcel1.readInt();
        String str2 = param1Parcel1.readString();
        String str1 = param1Parcel1.readString();
        if (param1Parcel1.readInt() != 0) {
          Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        notifyShowCompatibilityToast(param1Int1, param1Int2, str2, str1, (Bundle)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.zoomwindow.IOplusZoomAppObserver");
      if (param1Parcel1.readInt() != 0) {
        OplusZoomWindowInfo oplusZoomWindowInfo = (OplusZoomWindowInfo)OplusZoomWindowInfo.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      notifyControlViewChange((OplusZoomWindowInfo)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IOplusZoomAppObserver {
      public static IOplusZoomAppObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.zoomwindow.IOplusZoomAppObserver";
      }
      
      public void notifyControlViewChange(OplusZoomWindowInfo param2OplusZoomWindowInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.zoomwindow.IOplusZoomAppObserver");
          if (param2OplusZoomWindowInfo != null) {
            parcel.writeInt(1);
            param2OplusZoomWindowInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusZoomAppObserver.Stub.getDefaultImpl() != null) {
            IOplusZoomAppObserver.Stub.getDefaultImpl().notifyControlViewChange(param2OplusZoomWindowInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyShowCompatibilityToast(int param2Int1, int param2Int2, String param2String1, String param2String2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.zoomwindow.IOplusZoomAppObserver");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IOplusZoomAppObserver.Stub.getDefaultImpl() != null) {
            IOplusZoomAppObserver.Stub.getDefaultImpl().notifyShowCompatibilityToast(param2Int1, param2Int2, param2String1, param2String2, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusZoomAppObserver param1IOplusZoomAppObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusZoomAppObserver != null) {
          Proxy.sDefaultImpl = param1IOplusZoomAppObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusZoomAppObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
