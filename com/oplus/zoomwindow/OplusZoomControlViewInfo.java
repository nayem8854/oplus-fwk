package com.oplus.zoomwindow;

import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashMap;
import java.util.Map;

public class OplusZoomControlViewInfo implements Parcelable {
  public Map<String, Rect> cvRectMap = new HashMap<>();
  
  public Bundle extension = new Bundle();
  
  public OplusZoomControlViewInfo(Parcel paramParcel) {
    this.cvActionFlag = paramParcel.readInt();
    paramParcel.readMap(this.cvRectMap, HashMap.class.getClassLoader());
    this.extension = paramParcel.readBundle();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.cvActionFlag);
    paramParcel.writeMap(this.cvRectMap);
    paramParcel.writeBundle(this.extension);
  }
  
  public static final Parcelable.Creator<OplusZoomControlViewInfo> CREATOR = new Parcelable.Creator<OplusZoomControlViewInfo>() {
      public OplusZoomControlViewInfo createFromParcel(Parcel param1Parcel) {
        return new OplusZoomControlViewInfo(param1Parcel);
      }
      
      public OplusZoomControlViewInfo[] newArray(int param1Int) {
        return new OplusZoomControlViewInfo[param1Int];
      }
    };
  
  public int cvActionFlag;
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("OplusZoomControlViewInfo = { ");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" cvActionFlag = ");
    stringBuilder2.append(this.cvActionFlag);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" cvRectMap = ");
    stringBuilder2.append(this.cvRectMap);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" extension = ");
    stringBuilder2.append(this.extension);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append("}");
    return stringBuilder1.toString();
  }
  
  public void copyFrom(OplusZoomControlViewInfo paramOplusZoomControlViewInfo) {
    this.cvActionFlag = paramOplusZoomControlViewInfo.cvActionFlag;
    this.cvRectMap.putAll(paramOplusZoomControlViewInfo.cvRectMap);
    this.extension.putAll(paramOplusZoomControlViewInfo.extension);
  }
  
  public void clear() {
    this.cvActionFlag = 0;
    this.cvRectMap.clear();
    this.extension.clear();
  }
  
  public boolean isEmpty() {
    Map<String, Rect> map = this.cvRectMap;
    return (map == null || map.isEmpty());
  }
  
  public OplusZoomControlViewInfo() {}
}
