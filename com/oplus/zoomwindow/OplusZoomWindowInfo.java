package com.oplus.zoomwindow;

import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public final class OplusZoomWindowInfo implements Parcelable {
  public int zoomUserId;
  
  public Rect zoomRect = new Rect();
  
  public String zoomPkg;
  
  public int windowType;
  
  public boolean windowShown;
  
  public int rotation;
  
  public int lockUserId;
  
  public String lockPkg;
  
  public int lastExitMethod;
  
  public boolean inputShow;
  
  public int inputMethodType;
  
  public Bundle extension = new Bundle();
  
  public int cvActionFlag;
  
  public String cpnName;
  
  public OplusZoomWindowInfo() {
    this.cvActionFlag = 0;
    this.windowType = 0;
    this.zoomRect = new Rect();
  }
  
  public OplusZoomWindowInfo(Parcel paramParcel) {
    boolean bool1 = false;
    this.cvActionFlag = 0;
    this.windowType = 0;
    this.rotation = paramParcel.readInt();
    if (paramParcel.readByte() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.windowShown = bool2;
    this.lockPkg = paramParcel.readString();
    this.zoomRect = (Rect)paramParcel.readParcelable(null);
    this.zoomPkg = paramParcel.readString();
    this.lockUserId = paramParcel.readInt();
    this.zoomUserId = paramParcel.readInt();
    boolean bool2 = bool1;
    if (paramParcel.readByte() != 0)
      bool2 = true; 
    this.inputShow = bool2;
    this.cpnName = paramParcel.readString();
    this.lastExitMethod = paramParcel.readInt();
    this.inputMethodType = paramParcel.readInt();
    this.extension = paramParcel.readBundle();
    this.cvActionFlag = paramParcel.readInt();
    this.windowType = paramParcel.readInt();
  }
  
  public OplusZoomWindowInfo(OplusZoomWindowInfo paramOplusZoomWindowInfo) {
    this.cvActionFlag = 0;
    this.windowType = 0;
    if (paramOplusZoomWindowInfo != null) {
      this.rotation = paramOplusZoomWindowInfo.rotation;
      this.windowShown = paramOplusZoomWindowInfo.windowShown;
      this.lockPkg = paramOplusZoomWindowInfo.lockPkg;
      this.zoomRect = paramOplusZoomWindowInfo.zoomRect;
      this.zoomPkg = paramOplusZoomWindowInfo.zoomPkg;
      this.lockUserId = paramOplusZoomWindowInfo.lockUserId;
      this.zoomUserId = paramOplusZoomWindowInfo.zoomUserId;
      this.inputShow = paramOplusZoomWindowInfo.inputShow;
      this.cpnName = paramOplusZoomWindowInfo.cpnName;
      this.lastExitMethod = paramOplusZoomWindowInfo.lastExitMethod;
      this.inputMethodType = paramOplusZoomWindowInfo.inputMethodType;
      this.extension = paramOplusZoomWindowInfo.extension;
      this.cvActionFlag = paramOplusZoomWindowInfo.cvActionFlag;
    } 
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.rotation);
    paramParcel.writeByte((byte)this.windowShown);
    paramParcel.writeString(this.lockPkg);
    paramParcel.writeParcelable((Parcelable)this.zoomRect, 0);
    paramParcel.writeString(this.zoomPkg);
    paramParcel.writeInt(this.lockUserId);
    paramParcel.writeInt(this.zoomUserId);
    paramParcel.writeByte((byte)this.inputShow);
    paramParcel.writeString(this.cpnName);
    paramParcel.writeInt(this.lastExitMethod);
    paramParcel.writeInt(this.inputMethodType);
    paramParcel.writeBundle(this.extension);
    paramParcel.writeInt(this.cvActionFlag);
    paramParcel.writeInt(this.windowType);
  }
  
  public static final Parcelable.Creator<OplusZoomWindowInfo> CREATOR = new Parcelable.Creator<OplusZoomWindowInfo>() {
      public OplusZoomWindowInfo createFromParcel(Parcel param1Parcel) {
        return new OplusZoomWindowInfo(param1Parcel);
      }
      
      public OplusZoomWindowInfo[] newArray(int param1Int) {
        return new OplusZoomWindowInfo[param1Int];
      }
    };
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("OplusZoomWindowInfo = { ");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" windowType = ");
    stringBuilder2.append(this.windowType);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" cvActionFlag = ");
    stringBuilder2.append(this.cvActionFlag);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" pName = ");
    stringBuilder2.append(this.zoomPkg);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" rotation = ");
    stringBuilder2.append(this.rotation);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" shown = ");
    stringBuilder2.append(this.windowShown);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" lockPkg = ");
    stringBuilder2.append(this.lockPkg);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" zoomRect = ");
    stringBuilder2.append(this.zoomRect);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" lockUserId = ");
    stringBuilder2.append(this.lockUserId);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" zoomUserId = ");
    stringBuilder2.append(this.zoomUserId);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" inputShow = ");
    stringBuilder2.append(this.inputShow);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" cpnName = ");
    stringBuilder2.append(this.cpnName);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" lastExitMethod = ");
    stringBuilder2.append(this.lastExitMethod);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" inputMethodType = ");
    stringBuilder2.append(this.inputMethodType);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" extension = ");
    stringBuilder2.append(this.extension);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append("}");
    return stringBuilder1.toString();
  }
}
