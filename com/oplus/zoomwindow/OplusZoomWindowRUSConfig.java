package com.oplus.zoomwindow;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public class OplusZoomWindowRUSConfig implements Parcelable {
  public int getVersion() {
    return this.mVersion;
  }
  
  public void setVersion(int paramInt) {
    this.mVersion = paramInt;
  }
  
  public boolean getZoomWindowSwitch() {
    return this.mZoomWindowSwitch;
  }
  
  public void setZoomWindowSwitch(boolean paramBoolean) {
    this.mZoomWindowSwitch = paramBoolean;
  }
  
  public boolean getShowToastSwitch() {
    return this.mShowToastSwitch;
  }
  
  public void setShowToastSwitch(boolean paramBoolean) {
    this.mShowToastSwitch = paramBoolean;
  }
  
  public boolean getOnlySupportPkgListSwitch() {
    return this.mOnlySupportPkgListSwitch;
  }
  
  public void setOnlySupportPkgListSwitch(boolean paramBoolean) {
    this.mOnlySupportPkgListSwitch = paramBoolean;
  }
  
  public List<String> getPkgList() {
    return this.mPkgList;
  }
  
  public void setPkgList(List<String> paramList) {
    this.mPkgList = paramList;
  }
  
  public List<String> getReplyPkgList() {
    return this.mReplyPkgList;
  }
  
  public void setReplyPkgList(List<String> paramList) {
    this.mReplyPkgList = paramList;
  }
  
  public List<String> getUnSupportPkgList() {
    return this.mUnSupportPkgList;
  }
  
  public void setUnSupportPkgList(List<String> paramList) {
    this.mUnSupportPkgList = paramList;
  }
  
  public List<String> getForcedShowToastPkgList() {
    return this.mForcedShowToastPkgList;
  }
  
  public void setForcedShowToastPkgList(List<String> paramList) {
    this.mForcedShowToastPkgList = paramList;
  }
  
  public List<String> getNotShowToastPkgList() {
    return this.mNotShowToastPkgList;
  }
  
  public void setNotShowToastPkgList(List<String> paramList) {
    this.mNotShowToastPkgList = paramList;
  }
  
  public List<String> getUnusedZoomDisplayInfoPkgList() {
    return this.mUnusedZoomDisplayInfoPkgList;
  }
  
  public void setUnusedZoomDisplayInfoPkgList(List<String> paramList) {
    this.mUnusedZoomDisplayInfoPkgList = paramList;
  }
  
  public List<String> getCpnList() {
    return this.mCpnList;
  }
  
  public void setCpnList(List<String> paramList) {
    this.mCpnList = paramList;
  }
  
  public List<String> getUnSupportCpnList() {
    return this.mUnSupportCpnList;
  }
  
  public void setUnSupportCpnList(List<String> paramList) {
    this.mUnSupportCpnList = paramList;
  }
  
  public List<String> getUnReusedCpnList() {
    return this.mUnReusedCpnList;
  }
  
  public void setUnReusedCpnList(List<String> paramList) {
    this.mUnReusedCpnList = paramList;
  }
  
  public List<String> getUnRelaunchCpnList() {
    return this.mUnRelaunchCpnList;
  }
  
  public void setUnRelaunchCpnList(List<String> paramList) {
    this.mUnRelaunchCpnList = paramList;
  }
  
  public List<String> getForcedRelaunchCpnList() {
    return this.mForcedRelaunchCpnList;
  }
  
  public void setForcedRelaunchCpnList(List<String> paramList) {
    this.mForcedRelaunchCpnList = paramList;
  }
  
  public float getCornerRadius() {
    return this.mCornerRadius;
  }
  
  public void setCornerRadius(float paramFloat) {
    this.mCornerRadius = paramFloat;
  }
  
  public OplusZoomWindowSize getOplusZoomWindowSize() {
    return this.mOplusZoomWindowSize;
  }
  
  public void setOplusZoomWindowSize(OplusZoomWindowSize paramOplusZoomWindowSize) {
    this.mOplusZoomWindowSize = paramOplusZoomWindowSize;
  }
  
  public OplusZoomWindowRegion getOplusZoomWindowRegion() {
    return this.mOplusZoomWindowRegion;
  }
  
  public void setOplusZoomWindowRegion(OplusZoomWindowRegion paramOplusZoomWindowRegion) {
    this.mOplusZoomWindowRegion = paramOplusZoomWindowRegion;
  }
  
  public List<OplusZoomWindowSupportAppInfo> getVersionLimitPkgList() {
    return this.mVersionLimitPkgList;
  }
  
  public void setVersionLimitPkgList(List<OplusZoomWindowSupportAppInfo> paramList) {
    this.mVersionLimitPkgList = paramList;
  }
  
  public List<OplusZoomWindowSupportAppInfo> getVersionLimitReplyPkgList() {
    return this.mVersionLimitReplyPkgList;
  }
  
  public void setVersionLimitReplyPkgList(List<OplusZoomWindowSupportAppInfo> paramList) {
    this.mVersionLimitReplyPkgList = paramList;
  }
  
  public OplusZoomWindowRUSConfig() {
    this.mPkgList = new ArrayList<>();
    this.mReplyPkgList = new ArrayList<>();
    this.mUnSupportPkgList = new ArrayList<>();
    this.mForcedShowToastPkgList = new ArrayList<>();
    this.mNotShowToastPkgList = new ArrayList<>();
    this.mUnusedZoomDisplayInfoPkgList = new ArrayList<>();
    this.mCpnList = new ArrayList<>();
    this.mUnSupportCpnList = new ArrayList<>();
    this.mUnReusedCpnList = new ArrayList<>();
    this.mUnRelaunchCpnList = new ArrayList<>();
    this.mForcedRelaunchCpnList = new ArrayList<>();
    this.mOplusZoomWindowSize = new OplusZoomWindowSize();
    this.mOplusZoomWindowRegion = new OplusZoomWindowRegion();
    this.mVersionLimitPkgList = new ArrayList<>();
    this.mVersionLimitReplyPkgList = new ArrayList<>();
  }
  
  public OplusZoomWindowRUSConfig(Parcel paramParcel) {
    boolean bool2;
    this.mPkgList = new ArrayList<>();
    this.mReplyPkgList = new ArrayList<>();
    this.mUnSupportPkgList = new ArrayList<>();
    this.mForcedShowToastPkgList = new ArrayList<>();
    this.mNotShowToastPkgList = new ArrayList<>();
    this.mUnusedZoomDisplayInfoPkgList = new ArrayList<>();
    this.mCpnList = new ArrayList<>();
    this.mUnSupportCpnList = new ArrayList<>();
    this.mUnReusedCpnList = new ArrayList<>();
    this.mUnRelaunchCpnList = new ArrayList<>();
    this.mForcedRelaunchCpnList = new ArrayList<>();
    this.mOplusZoomWindowSize = new OplusZoomWindowSize();
    this.mOplusZoomWindowRegion = new OplusZoomWindowRegion();
    this.mVersionLimitPkgList = new ArrayList<>();
    this.mVersionLimitReplyPkgList = new ArrayList<>();
    this.mVersion = paramParcel.readInt();
    byte b = paramParcel.readByte();
    boolean bool1 = true;
    if (b != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mZoomWindowSwitch = bool2;
    if (paramParcel.readByte() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mShowToastSwitch = bool2;
    if (paramParcel.readByte() != 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.mOnlySupportPkgListSwitch = bool2;
    this.mPkgList = paramParcel.createStringArrayList();
    this.mReplyPkgList = paramParcel.createStringArrayList();
    this.mUnSupportPkgList = paramParcel.createStringArrayList();
    this.mForcedShowToastPkgList = paramParcel.createStringArrayList();
    this.mNotShowToastPkgList = paramParcel.createStringArrayList();
    this.mUnusedZoomDisplayInfoPkgList = paramParcel.createStringArrayList();
    this.mCpnList = paramParcel.createStringArrayList();
    this.mUnSupportCpnList = paramParcel.createStringArrayList();
    this.mUnReusedCpnList = paramParcel.createStringArrayList();
    this.mUnRelaunchCpnList = paramParcel.createStringArrayList();
    this.mForcedRelaunchCpnList = paramParcel.createStringArrayList();
    this.mCornerRadius = paramParcel.readFloat();
    this.mOplusZoomWindowSize = (OplusZoomWindowSize)paramParcel.readParcelable(OplusZoomWindowSize.class.getClassLoader());
    this.mOplusZoomWindowRegion = (OplusZoomWindowRegion)paramParcel.readParcelable(OplusZoomWindowRegion.class.getClassLoader());
    List<OplusZoomWindowSupportAppInfo> list1 = this.mVersionLimitPkgList;
    ClassLoader classLoader2 = OplusZoomWindowSupportAppInfo.class.getClassLoader();
    this.mVersionLimitPkgList = paramParcel.readParcelableList(list1, classLoader2);
    List<OplusZoomWindowSupportAppInfo> list2 = this.mVersionLimitReplyPkgList;
    ClassLoader classLoader1 = OplusZoomWindowSupportAppInfo.class.getClassLoader();
    this.mVersionLimitReplyPkgList = paramParcel.readParcelableList(list2, classLoader1);
  }
  
  public static final Parcelable.Creator<OplusZoomWindowRUSConfig> CREATOR = new Parcelable.Creator<OplusZoomWindowRUSConfig>() {
      public OplusZoomWindowRUSConfig createFromParcel(Parcel param1Parcel) {
        return new OplusZoomWindowRUSConfig(param1Parcel);
      }
      
      public OplusZoomWindowRUSConfig[] newArray(int param1Int) {
        return new OplusZoomWindowRUSConfig[param1Int];
      }
    };
  
  private float mCornerRadius;
  
  private List<String> mCpnList;
  
  private List<String> mForcedRelaunchCpnList;
  
  private List<String> mForcedShowToastPkgList;
  
  private List<String> mNotShowToastPkgList;
  
  private boolean mOnlySupportPkgListSwitch;
  
  private OplusZoomWindowRegion mOplusZoomWindowRegion;
  
  private OplusZoomWindowSize mOplusZoomWindowSize;
  
  private List<String> mPkgList;
  
  private List<String> mReplyPkgList;
  
  private boolean mShowToastSwitch;
  
  private List<String> mUnRelaunchCpnList;
  
  private List<String> mUnReusedCpnList;
  
  private List<String> mUnSupportCpnList;
  
  private List<String> mUnSupportPkgList;
  
  private List<String> mUnusedZoomDisplayInfoPkgList;
  
  private int mVersion;
  
  private List<OplusZoomWindowSupportAppInfo> mVersionLimitPkgList;
  
  private List<OplusZoomWindowSupportAppInfo> mVersionLimitReplyPkgList;
  
  private boolean mZoomWindowSwitch;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mVersion);
    paramParcel.writeByte((byte)this.mZoomWindowSwitch);
    paramParcel.writeByte((byte)this.mShowToastSwitch);
    paramParcel.writeByte((byte)this.mOnlySupportPkgListSwitch);
    paramParcel.writeStringList(this.mPkgList);
    paramParcel.writeStringList(this.mReplyPkgList);
    paramParcel.writeStringList(this.mUnSupportPkgList);
    paramParcel.writeStringList(this.mForcedShowToastPkgList);
    paramParcel.writeStringList(this.mNotShowToastPkgList);
    paramParcel.writeStringList(this.mUnusedZoomDisplayInfoPkgList);
    paramParcel.writeStringList(this.mCpnList);
    paramParcel.writeStringList(this.mUnSupportCpnList);
    paramParcel.writeStringList(this.mUnReusedCpnList);
    paramParcel.writeStringList(this.mUnRelaunchCpnList);
    paramParcel.writeStringList(this.mForcedRelaunchCpnList);
    paramParcel.writeFloat(this.mCornerRadius);
    paramParcel.writeParcelable(this.mOplusZoomWindowSize, paramInt);
    paramParcel.writeParcelable(this.mOplusZoomWindowRegion, paramInt);
    paramParcel.writeParcelableList(this.mVersionLimitPkgList, paramInt);
    paramParcel.writeParcelableList(this.mVersionLimitReplyPkgList, paramInt);
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("OplusZoomWindowRUSConfig{");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("version = ");
    stringBuilder2.append(this.mVersion);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nZoomWindowSwitch = ");
    stringBuilder2.append(this.mZoomWindowSwitch);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nShowToastSwitch = ");
    stringBuilder2.append(this.mShowToastSwitch);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nOnlySupportPkgListSwitch = ");
    stringBuilder2.append(this.mOnlySupportPkgListSwitch);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nPkgList = ");
    stringBuilder2.append(this.mPkgList);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nReplyPkgList = ");
    stringBuilder2.append(this.mReplyPkgList);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nUnSupportPkgList = ");
    stringBuilder2.append(this.mUnSupportPkgList);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nForcedShowToastPkgList = ");
    stringBuilder2.append(this.mForcedShowToastPkgList);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nNotShowToastPkgList = ");
    stringBuilder2.append(this.mNotShowToastPkgList);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nUnusedZoomDisplayInfoPkgList = ");
    stringBuilder2.append(this.mUnusedZoomDisplayInfoPkgList);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nCpnList = ");
    stringBuilder2.append(this.mCpnList);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nUnSupportCpnList = ");
    stringBuilder2.append(this.mUnSupportCpnList);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nUnReusedCpnList = ");
    stringBuilder2.append(this.mUnReusedCpnList);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nUnRelaunchCpnList = ");
    stringBuilder2.append(this.mUnRelaunchCpnList);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nForcedRelaunchCpnList = ");
    stringBuilder2.append(this.mForcedRelaunchCpnList);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nCornerRadius=");
    stringBuilder2.append(this.mCornerRadius);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nZoom Window size =  = ");
    stringBuilder2.append(this.mOplusZoomWindowSize.toString());
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nRegion = ");
    stringBuilder2.append(this.mOplusZoomWindowRegion.toString());
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nVersionLimitPkgList = ");
    stringBuilder2.append(this.mVersionLimitPkgList);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nVersionLimitReplyPkgList = ");
    stringBuilder2.append(this.mVersionLimitReplyPkgList);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append("}");
    return stringBuilder1.toString();
  }
}
