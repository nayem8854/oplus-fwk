package com.oplus.splitscreen;

import android.app.AppGlobals;
import android.app.OplusActivityTaskManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.provider.Settings;
import android.util.Log;
import com.oplus.app.IOplusSplitScreenObserver;
import java.util.HashMap;
import java.util.Map;
import oplus.util.OplusStatistics;

public class OplusSplitScreenManager {
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  public static final String EVENT_DISMISS_SPLIT_SCREEN = "dismissSplitScreen";
  
  private static final String EVENT_ID_SPLIT_SCREEN_LAUNCH = "split_screen_launch";
  
  public static final String EVENT_SPLIT_SCREEN_MINIMIZED_CHANGED = "splitScreenMinimizedChange";
  
  public static final String EVENT_SPLIT_SCREEN_MODE_CHANGED = "splitScreenModeChange";
  
  public static final String KEY_DISMISS_SPLIT_SCREEN_TYPE = "dismissSplitScreenType";
  
  public static final String KEY_IS_IN_SPLIT_SCREEN_MODE = "isInSplitScreenMode";
  
  public static final String KEY_IS_MINIMIZED = "isMinimized";
  
  private static final String KEY_LAUNCH_STYLE = "start_style";
  
  private static final String SETTINGS_FORBID_SPLITSCREEN = "forbid_splitscreen_by_ep";
  
  private static final String SPLIT_SCREEN_APPID = "20232";
  
  public static final int SPLIT_SCREEN_FROM_BREENO = 5;
  
  public static final int SPLIT_SCREEN_FROM_FLOAT_ASSISTANT = 4;
  
  public static final int SPLIT_SCREEN_FROM_MENU = 2;
  
  public static final int SPLIT_SCREEN_FROM_NONE = -1;
  
  public static final int SPLIT_SCREEN_FROM_RECENT = 3;
  
  public static final int SPLIT_SCREEN_FROM_SERVICE = 1;
  
  private static final String SPLIT_SCREEN_STATISTIC_ID = "20232001";
  
  public static final int STATE_APP_NOT_SUPPORT = 1006;
  
  public static final int STATE_BLACK_LIST = 1004;
  
  public static final int STATE_CHILDREN_MODE = 1005;
  
  public static final int STATE_FORBID_SPECIAL_APP = 1008;
  
  public static final int STATE_FORCE_FULLSCREEN = 1007;
  
  public static final int STATE_INVALID = 1000;
  
  public static final int STATE_SINGLE_HAND = 1003;
  
  public static final int STATE_SNAPSHOT = 1002;
  
  public static final int STATE_SUPPORT = 1001;
  
  private static final String TAG = "OplusSplitScreenManager";
  
  private static OplusSplitScreenManager sInstance;
  
  private OplusActivityTaskManager mOAms = new OplusActivityTaskManager();
  
  public static OplusSplitScreenManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/splitscreen/OplusSplitScreenManager.sInstance : Lcom/oplus/splitscreen/OplusSplitScreenManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/splitscreen/OplusSplitScreenManager
    //   8: monitorenter
    //   9: getstatic com/oplus/splitscreen/OplusSplitScreenManager.sInstance : Lcom/oplus/splitscreen/OplusSplitScreenManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/splitscreen/OplusSplitScreenManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/splitscreen/OplusSplitScreenManager.sInstance : Lcom/oplus/splitscreen/OplusSplitScreenManager;
    //   27: ldc com/oplus/splitscreen/OplusSplitScreenManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/splitscreen/OplusSplitScreenManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/splitscreen/OplusSplitScreenManager.sInstance : Lcom/oplus/splitscreen/OplusSplitScreenManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #83	-> 0
    //   #84	-> 6
    //   #85	-> 9
    //   #86	-> 15
    //   #88	-> 27
    //   #90	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public int getSplitScreenState(Intent paramIntent) {
    if (paramIntent != null) {
      if (DEBUG)
        Log.i("OplusSplitScreenManager", "getSplitScreenState"); 
      try {
        return this.mOAms.getSplitScreenState(paramIntent);
      } catch (RemoteException remoteException) {
        Log.e("OplusSplitScreenManager", "getSplitScreenState remoteException ");
        remoteException.printStackTrace();
        return 1000;
      } 
    } 
    throw new IllegalArgumentException("getSplitScreenState intent=null");
  }
  
  public int getTopAppSplitScreenState() {
    try {
      return this.mOAms.getSplitScreenState(null);
    } catch (RemoteException remoteException) {
      Log.e("OplusSplitScreenManager", "getTopAppSplitScreenState remoteException ");
      remoteException.printStackTrace();
      return 1000;
    } 
  }
  
  @Deprecated
  public void swapDockedFullscreenStack() {}
  
  public boolean splitScreenForTopApp(int paramInt) {
    if (isEnterpriseDisableSplitScreen()) {
      Log.i("OplusSplitScreenManager", "splitScreenForTopApp isEnterpriseDisableSplitScreen");
      return false;
    } 
    if (paramInt != 3) {
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("splitWindowForTopApp type:");
        stringBuilder.append(paramInt);
        Log.i("OplusSplitScreenManager", stringBuilder.toString());
      } 
      try {
        return this.mOAms.splitScreenForTopApp(paramInt);
      } catch (RemoteException remoteException) {
        Log.e("OplusSplitScreenManager", "splitScreenForTopApp RemoteException");
        return false;
      } 
    } 
    throw new IllegalArgumentException("splitWindowForTopApp type is abnormal");
  }
  
  public boolean splitScreenForRecentTasks(int paramInt) {
    if (isEnterpriseDisableSplitScreen()) {
      Log.i("OplusSplitScreenManager", "splitScreenForRecentTasks isEnterpriseDisableSplitScreen");
      return false;
    } 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("splitWindowForRecentTasks taskId:");
      stringBuilder.append(paramInt);
      Log.i("OplusSplitScreenManager", stringBuilder.toString());
    } 
    try {
      return this.mOAms.splitScreenForRecentTasks(paramInt);
    } catch (RemoteException remoteException) {
      Log.e("OplusSplitScreenManager", "splitScreenForRecentTasks RemoteException");
      return false;
    } 
  }
  
  public void onSplitScreenLaunched(int paramInt) {
    HashMap<Object, Object> hashMap = new HashMap<>();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramInt);
    stringBuilder.append("");
    hashMap.put("start_style", stringBuilder.toString());
    OplusStatistics.onCommon((Context)AppGlobals.getInitialApplication(), "20232", "20232001", "split_screen_launch", (Map)hashMap, false);
    if (DEBUG) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("onSplitScreenLaunched logMap:");
      stringBuilder.append(hashMap);
      Log.i("OplusSplitScreenManager", stringBuilder.toString());
    } 
  }
  
  public int splitScreenForEdgePanel(Intent paramIntent, int paramInt) {
    if (isEnterpriseDisableSplitScreen()) {
      Log.i("OplusSplitScreenManager", "splitScreenForEdgePanel isEnterpriseDisableSplitScreen");
      return 0;
    } 
    if (paramIntent != null) {
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("splitWindowForTopApp intent:");
        stringBuilder.append(paramIntent);
        Log.i("OplusSplitScreenManager", stringBuilder.toString());
      } 
      try {
        return this.mOAms.splitScreenForEdgePanel(paramIntent, paramInt);
      } catch (RemoteException remoteException) {
        Log.e("OplusSplitScreenManager", "splitScreenForEdgePanel failed");
        return 0;
      } 
    } 
    throw new IllegalArgumentException("getSplitScreenState intent=null");
  }
  
  public int getVersion() {
    return 1;
  }
  
  public boolean setSplitScreenObserver(IOplusSplitScreenObserver paramIOplusSplitScreenObserver) {
    if (paramIOplusSplitScreenObserver == null) {
      Log.e("OplusSplitScreenManager", "SystemUi setSplitScreenObserver error, observer is null");
      return false;
    } 
    if (DEBUG)
      Log.d("OplusSplitScreenManager", "SystemUi setSplitScreenObserver"); 
    try {
      return this.mOAms.setSplitScreenObserver(paramIOplusSplitScreenObserver);
    } catch (RemoteException remoteException) {
      Log.e("OplusSplitScreenManager", "SystemUi setSplitScreenObserver RemoteException");
      return false;
    } 
  }
  
  public void notifySplitScreenStateChanged(String paramString, Bundle paramBundle, boolean paramBoolean) {
    if (DEBUG)
      Log.d("OplusSplitScreenManager", "SystemUi notifyStateChanged"); 
    try {
      this.mOAms.notifySplitScreenStateChanged(paramString, paramBundle, paramBoolean);
    } catch (RemoteException remoteException) {
      Log.e("OplusSplitScreenManager", "notifyStateChanged RemoteException");
    } 
  }
  
  public boolean isInSplitScreenMode() {
    try {
      return this.mOAms.isInSplitScreenMode();
    } catch (RemoteException remoteException) {
      Log.e("OplusSplitScreenManager", "isInSplitScreenMode RemoteException");
      return false;
    } 
  }
  
  public boolean dismissSplitScreenMode(int paramInt) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("dismissSplitScreenMode type = ");
      stringBuilder.append(paramInt);
      Log.d("OplusSplitScreenManager", stringBuilder.toString());
    } 
    try {
      return this.mOAms.dismissSplitScreenMode(paramInt);
    } catch (RemoteException remoteException) {
      Log.e("OplusSplitScreenManager", "dismissSplitScreenMode RemoteException");
      return false;
    } 
  }
  
  public boolean registerSplitScreenObserver(IOplusSplitScreenObserver paramIOplusSplitScreenObserver) {
    if (paramIOplusSplitScreenObserver == null) {
      Log.e("OplusSplitScreenManager", "registerSplitScreenObserver error, observer is null");
      return false;
    } 
    try {
      return this.mOAms.registerSplitScreenObserver(paramIOplusSplitScreenObserver.hashCode(), paramIOplusSplitScreenObserver);
    } catch (RemoteException remoteException) {
      Log.e("OplusSplitScreenManager", "registerSplitScreenObserver failed");
      return false;
    } 
  }
  
  public boolean unregisterSplitScreenObserver(IOplusSplitScreenObserver paramIOplusSplitScreenObserver) {
    if (paramIOplusSplitScreenObserver == null) {
      Log.e("OplusSplitScreenManager", "unregisterSplitScreenObserver error, observer is null");
      return false;
    } 
    try {
      return this.mOAms.unregisterSplitScreenObserver(paramIOplusSplitScreenObserver.hashCode(), paramIOplusSplitScreenObserver);
    } catch (RemoteException remoteException) {
      Log.e("OplusSplitScreenManager", "unregisterSplitScreenObserver failed");
      return false;
    } 
  }
  
  public Bundle getSplitScreenStatus(String paramString) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getSplitScreenStatus event = ");
      stringBuilder.append(paramString);
      Log.d("OplusSplitScreenManager", stringBuilder.toString());
    } 
    try {
      return this.mOAms.getSplitScreenStatus(paramString);
    } catch (RemoteException remoteException) {
      Log.e("OplusSplitScreenManager", "getSplitScreenCurrentState failed");
      return null;
    } 
  }
  
  private boolean isEnterpriseDisableSplitScreen() {
    boolean bool = false;
    try {
      int i = Settings.Secure.getInt(AppGlobals.getInitialApplication().getContentResolver(), "forbid_splitscreen_by_ep", 0);
      if (i == 1)
        bool = true; 
      return bool;
    } catch (Exception exception) {
      Log.e("OplusSplitScreenManager", "isEnterpriseDisableSplitScreen error");
      return false;
    } 
  }
}
