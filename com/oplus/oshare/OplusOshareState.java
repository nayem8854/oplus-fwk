package com.oplus.oshare;

import android.os.Parcel;
import android.os.Parcelable;

public enum OplusOshareState implements Parcelable {
  BUSUY, BUSY, CANCEL, CANCEL_WAIT, IDLE, READY, SPACE_NOT_ENOUGH, TRANSITING, TRANSIT_FAILED, TRANSIT_REJECT, TRANSIT_SUCCESS, TRANSIT_TIMEOUT, TRANSIT_WAIT;
  
  private static final OplusOshareState[] $VALUES;
  
  public static final Parcelable.Creator<OplusOshareState> CREATOR;
  
  static {
    TRANSITING = new OplusOshareState("TRANSITING", 3);
    CANCEL = new OplusOshareState("CANCEL", 4);
    TRANSIT_SUCCESS = new OplusOshareState("TRANSIT_SUCCESS", 5);
    TRANSIT_FAILED = new OplusOshareState("TRANSIT_FAILED", 6);
    TRANSIT_REJECT = new OplusOshareState("TRANSIT_REJECT", 7);
    TRANSIT_TIMEOUT = new OplusOshareState("TRANSIT_TIMEOUT", 8);
    BUSUY = new OplusOshareState("BUSUY", 9);
    BUSY = new OplusOshareState("BUSY", 10);
    CANCEL_WAIT = new OplusOshareState("CANCEL_WAIT", 11);
    OplusOshareState oplusOshareState = new OplusOshareState("SPACE_NOT_ENOUGH", 12);
    $VALUES = new OplusOshareState[] { 
        IDLE, READY, TRANSIT_WAIT, TRANSITING, CANCEL, TRANSIT_SUCCESS, TRANSIT_FAILED, TRANSIT_REJECT, TRANSIT_TIMEOUT, BUSUY, 
        BUSY, CANCEL_WAIT, oplusOshareState };
    CREATOR = new Parcelable.Creator<OplusOshareState>() {
        public OplusOshareState createFromParcel(Parcel param1Parcel) {
          return (OplusOshareState)param1Parcel.readSerializable();
        }
        
        public OplusOshareState[] newArray(int param1Int) {
          return new OplusOshareState[param1Int];
        }
      };
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeSerializable(this);
  }
}
