package com.oplus.oshare;

import android.bluetooth.BluetoothDevice;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class OplusOshareDevice implements Parcelable {
  private String mDisplayName = "";
  
  private String mName = "";
  
  private String mBleMac = "";
  
  private String mWifiMac = null;
  
  private OplusOshareState mState = OplusOshareState.IDLE;
  
  private int mVirtual = 8;
  
  public long getLastFoundTime() {
    return this.mLastFoundTime;
  }
  
  public void setLastFoundTime(long paramLong) {
    this.mLastFoundTime = paramLong;
  }
  
  public String getDisplayName() {
    return this.mDisplayName;
  }
  
  public void setDisplayName(String paramString) {
    this.mDisplayName = paramString;
  }
  
  public int getSucceedNum() {
    return this.mSucceedNum;
  }
  
  public void setSucceedNum(int paramInt) {
    this.mSucceedNum = paramInt;
  }
  
  public int getTotalNum() {
    return this.mTotalNum;
  }
  
  public void setTotalNum(int paramInt) {
    this.mTotalNum = paramInt;
  }
  
  public int getVender() {
    return this.mVender;
  }
  
  public void setVender(int paramInt) {
    this.mVender = paramInt;
  }
  
  public String getHeadIconUrl() {
    return this.mHeadIconUrl;
  }
  
  public void setHeadIconUrl(String paramString) {
    this.mHeadIconUrl = paramString;
  }
  
  public Bitmap getHeadIcon() {
    return this.mHeadIcon;
  }
  
  public void setHeadIcon(Bitmap paramBitmap) {
    this.mHeadIcon = paramBitmap;
  }
  
  public String getRemainTime() {
    return this.mRemainTime;
  }
  
  public void setRemainTime(String paramString) {
    this.mRemainTime = paramString;
  }
  
  public void setProgress(int paramInt) {
    this.mProgress = paramInt;
  }
  
  public int getProgress() {
    return this.mProgress;
  }
  
  public BluetoothDevice getBluetoothDevice() {
    return this.mBluetootchDevice;
  }
  
  public boolean isVirtual() {
    boolean bool;
    if (this.mVirtual > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setVirtual(int paramInt) {
    this.mVirtual = paramInt;
  }
  
  public int getVirtual() {
    return this.mVirtual;
  }
  
  public void setBluetootchDevice(BluetoothDevice paramBluetoothDevice) {
    this.mBluetootchDevice = paramBluetoothDevice;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public void setName(String paramString) {
    this.mName = paramString;
  }
  
  public String getBleMac() {
    return this.mBleMac;
  }
  
  public void setBleMac(String paramString) {
    this.mBleMac = paramString;
  }
  
  public String getWifiMac() {
    return this.mWifiMac;
  }
  
  public void setWifiMac(String paramString) {
    this.mWifiMac = paramString;
  }
  
  public OplusOshareState getState() {
    return this.mState;
  }
  
  public void setState(OplusOshareState paramOplusOshareState) {
    if (this.mState.equals(OplusOshareState.TRANSIT_SUCCESS) && paramOplusOshareState.equals(OplusOshareState.TRANSIT_FAILED))
      return; 
    this.mState = paramOplusOshareState;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Name:");
    stringBuilder.append(this.mName);
    stringBuilder.append(" Virtual:");
    stringBuilder.append(this.mVirtual);
    stringBuilder.append(" State:");
    stringBuilder.append(this.mState);
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    if (paramObject == null || !(paramObject instanceof OplusOshareDevice))
      return false; 
    paramObject = paramObject;
    if (compare(this.mName, paramObject.getName()) && compare(this.mBleMac, paramObject.getBleMac())) {
      String str = this.mWifiMac;
      if (compare(str, paramObject.getWifiMac()))
        bool = true; 
    } 
    return bool;
  }
  
  private boolean compare(String paramString1, String paramString2) {
    if (TextUtils.isEmpty(paramString1) && TextUtils.isEmpty(paramString2))
      return true; 
    if (!TextUtils.isEmpty(paramString1))
      return paramString1.equals(paramString2); 
    return false;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mName);
    paramParcel.writeString(this.mBleMac);
    paramParcel.writeString(this.mWifiMac);
    paramParcel.writeSerializable(this.mState);
    paramParcel.writeInt(this.mVirtual);
    paramParcel.writeParcelable((Parcelable)this.mBluetootchDevice, paramInt);
    paramParcel.writeInt(this.mProgress);
    paramParcel.writeString(this.mRemainTime);
    paramParcel.writeParcelable((Parcelable)this.mHeadIcon, paramInt);
    paramParcel.writeInt(this.mSucceedNum);
    paramParcel.writeInt(this.mTotalNum);
    paramParcel.writeLong(this.mLastFoundTime);
    paramParcel.writeInt(this.mVender);
    paramParcel.writeString(this.mHeadIconUrl);
  }
  
  public static final Parcelable.Creator<OplusOshareDevice> CREATOR = new Parcelable.Creator<OplusOshareDevice>() {
      public OplusOshareDevice createFromParcel(Parcel param1Parcel) {
        String str1 = param1Parcel.readString();
        String str2 = param1Parcel.readString();
        String str3 = param1Parcel.readString();
        OplusOshareState oplusOshareState = (OplusOshareState)param1Parcel.readSerializable();
        int i = param1Parcel.readInt();
        BluetoothDevice bluetoothDevice = (BluetoothDevice)param1Parcel.readParcelable(null);
        int j = param1Parcel.readInt();
        String str4 = param1Parcel.readString();
        Bitmap bitmap = (Bitmap)param1Parcel.readParcelable(null);
        int k = param1Parcel.readInt();
        int m = param1Parcel.readInt();
        long l = param1Parcel.readLong();
        int n = param1Parcel.readInt();
        String str5 = param1Parcel.readString();
        OplusOshareDevice oplusOshareDevice = new OplusOshareDevice();
        OplusOshareDevice.access$002(oplusOshareDevice, str1);
        OplusOshareDevice.access$102(oplusOshareDevice, str2);
        OplusOshareDevice.access$202(oplusOshareDevice, str3);
        OplusOshareDevice.access$302(oplusOshareDevice, oplusOshareState);
        OplusOshareDevice.access$402(oplusOshareDevice, i);
        OplusOshareDevice.access$502(oplusOshareDevice, bluetoothDevice);
        OplusOshareDevice.access$602(oplusOshareDevice, j);
        OplusOshareDevice.access$702(oplusOshareDevice, str4);
        OplusOshareDevice.access$802(oplusOshareDevice, bitmap);
        OplusOshareDevice.access$902(oplusOshareDevice, k);
        OplusOshareDevice.access$1002(oplusOshareDevice, m);
        OplusOshareDevice.access$1102(oplusOshareDevice, l);
        OplusOshareDevice.access$1202(oplusOshareDevice, n);
        OplusOshareDevice.access$1302(oplusOshareDevice, str5);
        return oplusOshareDevice;
      }
      
      public OplusOshareDevice[] newArray(int param1Int) {
        return new OplusOshareDevice[param1Int];
      }
    };
  
  public static final int DEFAULT_VIRTUAL = 8;
  
  private BluetoothDevice mBluetootchDevice;
  
  private Bitmap mHeadIcon;
  
  private String mHeadIconUrl;
  
  private long mLastFoundTime;
  
  private int mProgress;
  
  private String mRemainTime;
  
  private int mSucceedNum;
  
  private int mTotalNum;
  
  private int mVender;
}
