package com.oplus.oshare;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusOshareInitListener extends IInterface {
  void onShareInit() throws RemoteException;
  
  void onShareUninit() throws RemoteException;
  
  class Default implements IOplusOshareInitListener {
    public void onShareInit() throws RemoteException {}
    
    public void onShareUninit() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusOshareInitListener {
    private static final String DESCRIPTOR = "com.oplus.oshare.IOplusOshareInitListener";
    
    static final int TRANSACTION_onShareInit = 1;
    
    static final int TRANSACTION_onShareUninit = 2;
    
    public Stub() {
      attachInterface(this, "com.oplus.oshare.IOplusOshareInitListener");
    }
    
    public static IOplusOshareInitListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.oshare.IOplusOshareInitListener");
      if (iInterface != null && iInterface instanceof IOplusOshareInitListener)
        return (IOplusOshareInitListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onShareUninit";
      } 
      return "onShareInit";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("com.oplus.oshare.IOplusOshareInitListener");
          return true;
        } 
        param1Parcel1.enforceInterface("com.oplus.oshare.IOplusOshareInitListener");
        onShareUninit();
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.oshare.IOplusOshareInitListener");
      onShareInit();
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IOplusOshareInitListener {
      public static IOplusOshareInitListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.oshare.IOplusOshareInitListener";
      }
      
      public void onShareInit() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oshare.IOplusOshareInitListener");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusOshareInitListener.Stub.getDefaultImpl() != null) {
            IOplusOshareInitListener.Stub.getDefaultImpl().onShareInit();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onShareUninit() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oshare.IOplusOshareInitListener");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusOshareInitListener.Stub.getDefaultImpl() != null) {
            IOplusOshareInitListener.Stub.getDefaultImpl().onShareUninit();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusOshareInitListener param1IOplusOshareInitListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusOshareInitListener != null) {
          Proxy.sDefaultImpl = param1IOplusOshareInitListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusOshareInitListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
