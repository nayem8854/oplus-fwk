package com.oplus.oshare;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusOshareService extends IInterface {
  void cancelTask(OplusOshareDevice paramOplusOshareDevice) throws RemoteException;
  
  boolean isSendOn() throws RemoteException;
  
  void pause() throws RemoteException;
  
  void registerCallback(IOplusOshareCallback paramIOplusOshareCallback) throws RemoteException;
  
  void resume() throws RemoteException;
  
  void scan() throws RemoteException;
  
  void sendData(Intent paramIntent, OplusOshareDevice paramOplusOshareDevice) throws RemoteException;
  
  void stop() throws RemoteException;
  
  void switchSend(boolean paramBoolean) throws RemoteException;
  
  void unregisterCallback(IOplusOshareCallback paramIOplusOshareCallback) throws RemoteException;
  
  class Default implements IOplusOshareService {
    public boolean isSendOn() throws RemoteException {
      return false;
    }
    
    public void switchSend(boolean param1Boolean) throws RemoteException {}
    
    public void scan() throws RemoteException {}
    
    public void registerCallback(IOplusOshareCallback param1IOplusOshareCallback) throws RemoteException {}
    
    public void unregisterCallback(IOplusOshareCallback param1IOplusOshareCallback) throws RemoteException {}
    
    public void sendData(Intent param1Intent, OplusOshareDevice param1OplusOshareDevice) throws RemoteException {}
    
    public void cancelTask(OplusOshareDevice param1OplusOshareDevice) throws RemoteException {}
    
    public void pause() throws RemoteException {}
    
    public void resume() throws RemoteException {}
    
    public void stop() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusOshareService {
    private static final String DESCRIPTOR = "com.oplus.oshare.IOplusOshareService";
    
    static final int TRANSACTION_cancelTask = 7;
    
    static final int TRANSACTION_isSendOn = 1;
    
    static final int TRANSACTION_pause = 8;
    
    static final int TRANSACTION_registerCallback = 4;
    
    static final int TRANSACTION_resume = 9;
    
    static final int TRANSACTION_scan = 3;
    
    static final int TRANSACTION_sendData = 6;
    
    static final int TRANSACTION_stop = 10;
    
    static final int TRANSACTION_switchSend = 2;
    
    static final int TRANSACTION_unregisterCallback = 5;
    
    public Stub() {
      attachInterface(this, "com.oplus.oshare.IOplusOshareService");
    }
    
    public static IOplusOshareService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.oshare.IOplusOshareService");
      if (iInterface != null && iInterface instanceof IOplusOshareService)
        return (IOplusOshareService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 10:
          return "stop";
        case 9:
          return "resume";
        case 8:
          return "pause";
        case 7:
          return "cancelTask";
        case 6:
          return "sendData";
        case 5:
          return "unregisterCallback";
        case 4:
          return "registerCallback";
        case 3:
          return "scan";
        case 2:
          return "switchSend";
        case 1:
          break;
      } 
      return "isSendOn";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        IOplusOshareCallback iOplusOshareCallback;
        Intent intent;
        boolean bool1;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 10:
            param1Parcel1.enforceInterface("com.oplus.oshare.IOplusOshareService");
            stop();
            param1Parcel2.writeNoException();
            return true;
          case 9:
            param1Parcel1.enforceInterface("com.oplus.oshare.IOplusOshareService");
            resume();
            param1Parcel2.writeNoException();
            return true;
          case 8:
            param1Parcel1.enforceInterface("com.oplus.oshare.IOplusOshareService");
            pause();
            param1Parcel2.writeNoException();
            return true;
          case 7:
            param1Parcel1.enforceInterface("com.oplus.oshare.IOplusOshareService");
            if (param1Parcel1.readInt() != 0) {
              OplusOshareDevice oplusOshareDevice = (OplusOshareDevice)OplusOshareDevice.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            cancelTask((OplusOshareDevice)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            param1Parcel1.enforceInterface("com.oplus.oshare.IOplusOshareService");
            if (param1Parcel1.readInt() != 0) {
              intent = (Intent)Intent.CREATOR.createFromParcel(param1Parcel1);
            } else {
              intent = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              OplusOshareDevice oplusOshareDevice = (OplusOshareDevice)OplusOshareDevice.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            sendData(intent, (OplusOshareDevice)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            param1Parcel1.enforceInterface("com.oplus.oshare.IOplusOshareService");
            iOplusOshareCallback = IOplusOshareCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            unregisterCallback(iOplusOshareCallback);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            iOplusOshareCallback.enforceInterface("com.oplus.oshare.IOplusOshareService");
            iOplusOshareCallback = IOplusOshareCallback.Stub.asInterface(iOplusOshareCallback.readStrongBinder());
            registerCallback(iOplusOshareCallback);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            iOplusOshareCallback.enforceInterface("com.oplus.oshare.IOplusOshareService");
            scan();
            param1Parcel2.writeNoException();
            return true;
          case 2:
            iOplusOshareCallback.enforceInterface("com.oplus.oshare.IOplusOshareService");
            if (iOplusOshareCallback.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            switchSend(bool1);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        iOplusOshareCallback.enforceInterface("com.oplus.oshare.IOplusOshareService");
        boolean bool = isSendOn();
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool);
        return true;
      } 
      param1Parcel2.writeString("com.oplus.oshare.IOplusOshareService");
      return true;
    }
    
    private static class Proxy implements IOplusOshareService {
      public static IOplusOshareService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.oshare.IOplusOshareService";
      }
      
      public boolean isSendOn() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oshare.IOplusOshareService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IOplusOshareService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusOshareService.Stub.getDefaultImpl().isSendOn();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void switchSend(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.oplus.oshare.IOplusOshareService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool1 && IOplusOshareService.Stub.getDefaultImpl() != null) {
            IOplusOshareService.Stub.getDefaultImpl().switchSend(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void scan() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oshare.IOplusOshareService");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOplusOshareService.Stub.getDefaultImpl() != null) {
            IOplusOshareService.Stub.getDefaultImpl().scan();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerCallback(IOplusOshareCallback param2IOplusOshareCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oplus.oshare.IOplusOshareService");
          if (param2IOplusOshareCallback != null) {
            iBinder = param2IOplusOshareCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOplusOshareService.Stub.getDefaultImpl() != null) {
            IOplusOshareService.Stub.getDefaultImpl().registerCallback(param2IOplusOshareCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterCallback(IOplusOshareCallback param2IOplusOshareCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oplus.oshare.IOplusOshareService");
          if (param2IOplusOshareCallback != null) {
            iBinder = param2IOplusOshareCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOplusOshareService.Stub.getDefaultImpl() != null) {
            IOplusOshareService.Stub.getDefaultImpl().unregisterCallback(param2IOplusOshareCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendData(Intent param2Intent, OplusOshareDevice param2OplusOshareDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oshare.IOplusOshareService");
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2OplusOshareDevice != null) {
            parcel1.writeInt(1);
            param2OplusOshareDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IOplusOshareService.Stub.getDefaultImpl() != null) {
            IOplusOshareService.Stub.getDefaultImpl().sendData(param2Intent, param2OplusOshareDevice);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelTask(OplusOshareDevice param2OplusOshareDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oshare.IOplusOshareService");
          if (param2OplusOshareDevice != null) {
            parcel1.writeInt(1);
            param2OplusOshareDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IOplusOshareService.Stub.getDefaultImpl() != null) {
            IOplusOshareService.Stub.getDefaultImpl().cancelTask(param2OplusOshareDevice);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void pause() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oshare.IOplusOshareService");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IOplusOshareService.Stub.getDefaultImpl() != null) {
            IOplusOshareService.Stub.getDefaultImpl().pause();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resume() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oshare.IOplusOshareService");
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IOplusOshareService.Stub.getDefaultImpl() != null) {
            IOplusOshareService.Stub.getDefaultImpl().resume();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stop() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oshare.IOplusOshareService");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IOplusOshareService.Stub.getDefaultImpl() != null) {
            IOplusOshareService.Stub.getDefaultImpl().stop();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusOshareService param1IOplusOshareService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusOshareService != null) {
          Proxy.sDefaultImpl = param1IOplusOshareService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusOshareService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
