package com.oplus.oshare;

import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.os.RemoteException;
import android.provider.Settings;
import android.util.Log;

public class OplusOshareServiceUtil extends IOplusOshareService.Stub {
  private volatile boolean mServiceConnected = false;
  
  private IBinder.DeathRecipient mDeathRecipient = (IBinder.DeathRecipient)new Object(this);
  
  private ServiceConnection mServiceConnection = (ServiceConnection)new Object(this);
  
  public static final String ACTION_OSHARE_STATE = "coloros.intent.action.OSHARE_STATE";
  
  private static final String KEY_SECURITY_CHECK_AGAIN = "key_security_check_again";
  
  public static final int OSHARE_OFF = 0;
  
  public static final int OSHARE_ON = 1;
  
  public static final String OSHARE_STATE = "oshare_state";
  
  private static final String PREFERENCE_PACKAGE = "com.coloros.oshare";
  
  private static final String SHARED_PREFERENCES_NAME = "oshare_preferences";
  
  protected static final String TAG = "OShareServiceUtil";
  
  private Context mContext;
  
  private IOplusOshareInitListener mInitListener;
  
  private IOplusOshareCallback mOShareCallback;
  
  private IOplusOshareService mService;
  
  public OplusOshareServiceUtil(Context paramContext, IOplusOshareInitListener paramIOplusOshareInitListener) {
    this.mContext = paramContext;
    this.mInitListener = paramIOplusOshareInitListener;
  }
  
  public void initShareEngine() {
    Log.d("OShareServiceUtil", "initShareEngine");
    Intent intent = new Intent("com.coloros.oshare.OShareClient");
    if (this.mServiceConnection != null && !this.mServiceConnected) {
      intent.setPackage("com.coloros.oshare");
      this.mContext.bindService(intent, this.mServiceConnection, 1);
    } 
  }
  
  public void scan() {
    Log.d("OShareServiceUtil", "scan");
    IOplusOshareService iOplusOshareService = this.mService;
    if (iOplusOshareService != null)
      try {
        iOplusOshareService.scan();
      } catch (RemoteException remoteException) {
        remoteException.printStackTrace();
      }  
  }
  
  public void registerCallback(IOplusOshareCallback paramIOplusOshareCallback) {
    Log.d("OShareServiceUtil", "registerCallback");
    IOplusOshareService iOplusOshareService = this.mService;
    if (iOplusOshareService != null)
      try {
        this.mOShareCallback = paramIOplusOshareCallback;
        iOplusOshareService.registerCallback(paramIOplusOshareCallback);
      } catch (RemoteException remoteException) {
        remoteException.printStackTrace();
      }  
  }
  
  public void unregisterCallback(IOplusOshareCallback paramIOplusOshareCallback) {
    Log.d("OShareServiceUtil", "unregisterCallback");
    IOplusOshareService iOplusOshareService = this.mService;
    if (iOplusOshareService != null)
      try {
        iOplusOshareService.unregisterCallback(paramIOplusOshareCallback);
      } catch (RemoteException remoteException) {
        remoteException.printStackTrace();
      }  
  }
  
  public void sendData(Intent paramIntent, OplusOshareDevice paramOplusOshareDevice) {
    Log.d("OShareServiceUtil", "sendData");
    IOplusOshareService iOplusOshareService = this.mService;
    if (iOplusOshareService != null)
      try {
        iOplusOshareService.sendData(paramIntent, paramOplusOshareDevice);
      } catch (RemoteException remoteException) {
        remoteException.printStackTrace();
      }  
  }
  
  public void cancelTask(OplusOshareDevice paramOplusOshareDevice) {
    Log.d("OShareServiceUtil", "cancelTask");
    IOplusOshareService iOplusOshareService = this.mService;
    if (iOplusOshareService != null)
      try {
        iOplusOshareService.cancelTask(paramOplusOshareDevice);
      } catch (RemoteException remoteException) {
        remoteException.printStackTrace();
      }  
  }
  
  public void stop() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("stop : mServiceConnected = ");
    stringBuilder.append(this.mServiceConnected);
    Log.d("OShareServiceUtil", stringBuilder.toString());
    IOplusOshareService iOplusOshareService = this.mService;
    if (iOplusOshareService != null)
      try {
        iOplusOshareService.stop();
      } catch (RemoteException remoteException) {
        remoteException.printStackTrace();
      }  
    if (this.mServiceConnection != null && this.mServiceConnected)
      try {
        this.mContext.unbindService(this.mServiceConnection);
      } catch (IllegalArgumentException illegalArgumentException) {
        illegalArgumentException.printStackTrace();
      }  
    this.mInitListener = null;
  }
  
  public static boolean isOshareOn(Context paramContext) {
    ContentResolver contentResolver = paramContext.getContentResolver();
    boolean bool = checkRuntimePermission(paramContext);
    boolean bool1 = true;
    if (!bool || Settings.System.getInt(contentResolver, "oshare_state", 0) != 1)
      bool1 = false; 
    return bool1;
  }
  
  public static void switchOshare(Context paramContext, boolean paramBoolean) {
    Intent intent;
    ContentResolver contentResolver = paramContext.getContentResolver();
    if (paramBoolean) {
      intent = new Intent("coloros.intent.action.SECURITY_CHECK");
      intent.setPackage("com.coloros.oshare");
      intent.addFlags(268435456);
      try {
        paramContext.startActivity(intent);
      } catch (ActivityNotFoundException activityNotFoundException) {
        activityNotFoundException.printStackTrace();
      } 
    } else {
      sendOshareStateBroadcast((Context)activityNotFoundException, paramBoolean, (ContentResolver)intent);
    } 
  }
  
  private static void sendOshareStateBroadcast(Context paramContext, boolean paramBoolean, ContentResolver paramContentResolver) {
    Intent intent = new Intent("coloros.intent.action.OSHARE_STATE");
    intent.putExtra("oshare_state", paramBoolean);
    paramContext.sendBroadcast(intent);
    Settings.System.putInt(paramContentResolver, "oshare_state", paramBoolean);
  }
  
  public static SharedPreferences getSharedPreferences(Context paramContext) {
    return paramContext.getSharedPreferences("oshare_preferences", 7);
  }
  
  public static void setSecurityCheckAgain(Context paramContext, boolean paramBoolean) {
    SharedPreferences.Editor editor = getSharedPreferences(paramContext).edit();
    editor.putBoolean("key_security_check_again", paramBoolean);
    editor.commit();
  }
  
  public static boolean isSecurityCheckAgain(Context paramContext) {
    PackageManager.NameNotFoundException nameNotFoundException2 = null;
    try {
      paramContext = paramContext.createPackageContext("com.coloros.oshare", 2);
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException1) {
      nameNotFoundException1.printStackTrace();
      nameNotFoundException1 = nameNotFoundException2;
    } 
    if (nameNotFoundException1 != null)
      return getSharedPreferences((Context)nameNotFoundException1).getBoolean("key_security_check_again", true); 
    return true;
  }
  
  public boolean isSendOn() {
    IOplusOshareService iOplusOshareService = this.mService;
    if (iOplusOshareService != null)
      try {
        return iOplusOshareService.isSendOn();
      } catch (RemoteException remoteException) {
        remoteException.printStackTrace();
      }  
    return false;
  }
  
  public void switchSend(boolean paramBoolean) {
    IOplusOshareService iOplusOshareService = this.mService;
    if (iOplusOshareService != null)
      try {
        iOplusOshareService.switchSend(paramBoolean);
      } catch (RemoteException remoteException) {
        remoteException.printStackTrace();
      }  
  }
  
  public void pause() throws RemoteException {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("pause : mServiceConnected = ");
    stringBuilder.append(this.mServiceConnected);
    Log.d("OShareServiceUtil", stringBuilder.toString());
    IOplusOshareService iOplusOshareService = this.mService;
    if (iOplusOshareService != null)
      try {
        iOplusOshareService.pause();
      } catch (RemoteException remoteException) {
        remoteException.printStackTrace();
      }  
  }
  
  public void resume() throws RemoteException {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("resume : mServiceConnected = ");
    stringBuilder.append(this.mServiceConnected);
    Log.d("OShareServiceUtil", stringBuilder.toString());
    if (!this.mServiceConnected)
      initShareEngine(); 
    IOplusOshareService iOplusOshareService = this.mService;
    if (iOplusOshareService != null)
      try {
        iOplusOshareService.resume();
      } catch (RemoteException remoteException) {
        remoteException.printStackTrace();
      }  
  }
  
  public static boolean checkRuntimePermission(Context paramContext) {
    boolean bool = true;
    if (paramContext.checkSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") != 0)
      bool = false; 
    if (paramContext.checkSelfPermission("android.permission.READ_EXTERNAL_STORAGE") != 0)
      bool = false; 
    if (paramContext.checkSelfPermission("android.permission.ACCESS_FINE_LOCATION") != 0)
      bool = false; 
    if (paramContext.checkSelfPermission("android.permission.ACCESS_COARSE_LOCATION") != 0)
      bool = false; 
    return bool;
  }
}
