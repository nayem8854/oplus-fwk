package com.oplus.oshare;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IOplusOshareCallback extends IInterface {
  void onDeviceChanged(List<OplusOshareDevice> paramList) throws RemoteException;
  
  void onSendSwitchChanged(boolean paramBoolean) throws RemoteException;
  
  class Default implements IOplusOshareCallback {
    public void onDeviceChanged(List<OplusOshareDevice> param1List) throws RemoteException {}
    
    public void onSendSwitchChanged(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusOshareCallback {
    private static final String DESCRIPTOR = "com.oplus.oshare.IOplusOshareCallback";
    
    static final int TRANSACTION_onDeviceChanged = 1;
    
    static final int TRANSACTION_onSendSwitchChanged = 2;
    
    public Stub() {
      attachInterface(this, "com.oplus.oshare.IOplusOshareCallback");
    }
    
    public static IOplusOshareCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.oshare.IOplusOshareCallback");
      if (iInterface != null && iInterface instanceof IOplusOshareCallback)
        return (IOplusOshareCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onSendSwitchChanged";
      } 
      return "onDeviceChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        boolean bool;
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("com.oplus.oshare.IOplusOshareCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("com.oplus.oshare.IOplusOshareCallback");
        if (param1Parcel1.readInt() != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        onSendSwitchChanged(bool);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.oshare.IOplusOshareCallback");
      ArrayList<OplusOshareDevice> arrayList = param1Parcel1.createTypedArrayList(OplusOshareDevice.CREATOR);
      onDeviceChanged(arrayList);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IOplusOshareCallback {
      public static IOplusOshareCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.oshare.IOplusOshareCallback";
      }
      
      public void onDeviceChanged(List<OplusOshareDevice> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.oshare.IOplusOshareCallback");
          parcel1.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusOshareCallback.Stub.getDefaultImpl() != null) {
            IOplusOshareCallback.Stub.getDefaultImpl().onDeviceChanged(param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onSendSwitchChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.oplus.oshare.IOplusOshareCallback");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool1 && IOplusOshareCallback.Stub.getDefaultImpl() != null) {
            IOplusOshareCallback.Stub.getDefaultImpl().onSendSwitchChanged(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusOshareCallback param1IOplusOshareCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusOshareCallback != null) {
          Proxy.sDefaultImpl = param1IOplusOshareCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusOshareCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
