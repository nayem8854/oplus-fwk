package com.oplus.lockscreen;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusLockScreenCallback extends IInterface {
  void showDialogForIntercpet(String paramString) throws RemoteException;
  
  class Default implements IOplusLockScreenCallback {
    public void showDialogForIntercpet(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusLockScreenCallback {
    private static final String DESCRIPTOR = "com.oplus.lockscreen.IOplusLockScreenCallback";
    
    static final int TRANSACTION_showDialogForIntercpet = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.lockscreen.IOplusLockScreenCallback");
    }
    
    public static IOplusLockScreenCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.lockscreen.IOplusLockScreenCallback");
      if (iInterface != null && iInterface instanceof IOplusLockScreenCallback)
        return (IOplusLockScreenCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "showDialogForIntercpet";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.lockscreen.IOplusLockScreenCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.lockscreen.IOplusLockScreenCallback");
      String str = param1Parcel1.readString();
      showDialogForIntercpet(str);
      return true;
    }
    
    private static class Proxy implements IOplusLockScreenCallback {
      public static IOplusLockScreenCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.lockscreen.IOplusLockScreenCallback";
      }
      
      public void showDialogForIntercpet(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.lockscreen.IOplusLockScreenCallback");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusLockScreenCallback.Stub.getDefaultImpl() != null) {
            IOplusLockScreenCallback.Stub.getDefaultImpl().showDialogForIntercpet(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusLockScreenCallback param1IOplusLockScreenCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusLockScreenCallback != null) {
          Proxy.sDefaultImpl = param1IOplusLockScreenCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusLockScreenCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
