package com.oplus.lockscreen;

import android.app.OplusActivityManager;
import android.os.RemoteException;
import android.util.Log;
import java.util.Map;

public class OplusLockScreenManager {
  private static final String TAG = "OplusLockScreenManager";
  
  private static volatile OplusLockScreenManager sInstance;
  
  public static OplusLockScreenManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/lockscreen/OplusLockScreenManager.sInstance : Lcom/oplus/lockscreen/OplusLockScreenManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/lockscreen/OplusLockScreenManager
    //   8: monitorenter
    //   9: getstatic com/oplus/lockscreen/OplusLockScreenManager.sInstance : Lcom/oplus/lockscreen/OplusLockScreenManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/lockscreen/OplusLockScreenManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/lockscreen/OplusLockScreenManager.sInstance : Lcom/oplus/lockscreen/OplusLockScreenManager;
    //   27: ldc com/oplus/lockscreen/OplusLockScreenManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/lockscreen/OplusLockScreenManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/lockscreen/OplusLockScreenManager.sInstance : Lcom/oplus/lockscreen/OplusLockScreenManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #33	-> 0
    //   #34	-> 6
    //   #35	-> 9
    //   #36	-> 15
    //   #38	-> 27
    //   #40	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  private OplusActivityManager mOAms = new OplusActivityManager();
  
  public void setPackagesState(Map<String, Integer> paramMap) {
    try {
      this.mOAms.setPackagesState(paramMap);
    } catch (RemoteException remoteException) {
      Log.e("OplusLockScreenManager", "setPackagesState remoteException ");
      remoteException.printStackTrace();
    } 
  }
  
  public boolean registerLockScreenCallback(IOplusLockScreenCallback paramIOplusLockScreenCallback) {
    try {
      return this.mOAms.registerLockScreenCallback(paramIOplusLockScreenCallback);
    } catch (RemoteException remoteException) {
      Log.e("OplusLockScreenManager", "registerLockScreenCallback remoteException ");
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public boolean unregisterLockScreenCallback(IOplusLockScreenCallback paramIOplusLockScreenCallback) {
    try {
      return this.mOAms.unregisterLockScreenCallback(paramIOplusLockScreenCallback);
    } catch (RemoteException remoteException) {
      Log.e("OplusLockScreenManager", "unRegisterLockScreenCallback remoteException ");
      remoteException.printStackTrace();
      return false;
    } 
  }
}
