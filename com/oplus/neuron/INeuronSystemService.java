package com.oplus.neuron;

import android.content.ContentValues;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface INeuronSystemService extends IInterface {
  void enableRecommendedApps(boolean paramBoolean, List<String> paramList) throws RemoteException;
  
  List<String> getRecommendedApps(int paramInt) throws RemoteException;
  
  void publishEvent(int paramInt, ContentValues paramContentValues) throws RemoteException;
  
  void registerEventListener(INeuronSystemEventListener paramINeuronSystemEventListener) throws RemoteException;
  
  class Default implements INeuronSystemService {
    public void publishEvent(int param1Int, ContentValues param1ContentValues) throws RemoteException {}
    
    public void registerEventListener(INeuronSystemEventListener param1INeuronSystemEventListener) throws RemoteException {}
    
    public void enableRecommendedApps(boolean param1Boolean, List<String> param1List) throws RemoteException {}
    
    public List<String> getRecommendedApps(int param1Int) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INeuronSystemService {
    private static final String DESCRIPTOR = "com.oplus.neuron.INeuronSystemService";
    
    static final int TRANSACTION_enableRecommendedApps = 3;
    
    static final int TRANSACTION_getRecommendedApps = 4;
    
    static final int TRANSACTION_publishEvent = 1;
    
    static final int TRANSACTION_registerEventListener = 2;
    
    public Stub() {
      attachInterface(this, "com.oplus.neuron.INeuronSystemService");
    }
    
    public static INeuronSystemService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.neuron.INeuronSystemService");
      if (iInterface != null && iInterface instanceof INeuronSystemService)
        return (INeuronSystemService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "getRecommendedApps";
          } 
          return "enableRecommendedApps";
        } 
        return "registerEventListener";
      } 
      return "publishEvent";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      INeuronSystemEventListener iNeuronSystemEventListener;
      if (param1Int1 != 1) {
        List<String> list;
        if (param1Int1 != 2) {
          boolean bool;
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("com.oplus.neuron.INeuronSystemService");
              return true;
            } 
            param1Parcel1.enforceInterface("com.oplus.neuron.INeuronSystemService");
            param1Int1 = param1Parcel1.readInt();
            list = getRecommendedApps(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list);
            return true;
          } 
          list.enforceInterface("com.oplus.neuron.INeuronSystemService");
          if (list.readInt() != 0) {
            bool = true;
          } else {
            bool = false;
          } 
          list = list.createStringArrayList();
          enableRecommendedApps(bool, list);
          param1Parcel2.writeNoException();
          return true;
        } 
        list.enforceInterface("com.oplus.neuron.INeuronSystemService");
        iNeuronSystemEventListener = INeuronSystemEventListener.Stub.asInterface(list.readStrongBinder());
        registerEventListener(iNeuronSystemEventListener);
        param1Parcel2.writeNoException();
        return true;
      } 
      iNeuronSystemEventListener.enforceInterface("com.oplus.neuron.INeuronSystemService");
      param1Int1 = iNeuronSystemEventListener.readInt();
      if (iNeuronSystemEventListener.readInt() != 0) {
        ContentValues contentValues = (ContentValues)ContentValues.CREATOR.createFromParcel((Parcel)iNeuronSystemEventListener);
      } else {
        iNeuronSystemEventListener = null;
      } 
      publishEvent(param1Int1, (ContentValues)iNeuronSystemEventListener);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements INeuronSystemService {
      public static INeuronSystemService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.neuron.INeuronSystemService";
      }
      
      public void publishEvent(int param2Int, ContentValues param2ContentValues) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.neuron.INeuronSystemService");
          parcel1.writeInt(param2Int);
          if (param2ContentValues != null) {
            parcel1.writeInt(1);
            param2ContentValues.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && INeuronSystemService.Stub.getDefaultImpl() != null) {
            INeuronSystemService.Stub.getDefaultImpl().publishEvent(param2Int, param2ContentValues);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerEventListener(INeuronSystemEventListener param2INeuronSystemEventListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oplus.neuron.INeuronSystemService");
          if (param2INeuronSystemEventListener != null) {
            iBinder = param2INeuronSystemEventListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && INeuronSystemService.Stub.getDefaultImpl() != null) {
            INeuronSystemService.Stub.getDefaultImpl().registerEventListener(param2INeuronSystemEventListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enableRecommendedApps(boolean param2Boolean, List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.oplus.neuron.INeuronSystemService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeStringList(param2List);
          boolean bool1 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool1 && INeuronSystemService.Stub.getDefaultImpl() != null) {
            INeuronSystemService.Stub.getDefaultImpl().enableRecommendedApps(param2Boolean, param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getRecommendedApps(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.neuron.INeuronSystemService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && INeuronSystemService.Stub.getDefaultImpl() != null)
            return INeuronSystemService.Stub.getDefaultImpl().getRecommendedApps(param2Int); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INeuronSystemService param1INeuronSystemService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INeuronSystemService != null) {
          Proxy.sDefaultImpl = param1INeuronSystemService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INeuronSystemService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
