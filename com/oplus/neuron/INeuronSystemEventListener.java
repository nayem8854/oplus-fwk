package com.oplus.neuron;

import android.content.ContentValues;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INeuronSystemEventListener extends IInterface {
  void onEvent(int paramInt, ContentValues paramContentValues) throws RemoteException;
  
  class Default implements INeuronSystemEventListener {
    public void onEvent(int param1Int, ContentValues param1ContentValues) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INeuronSystemEventListener {
    private static final String DESCRIPTOR = "com.oplus.neuron.INeuronSystemEventListener";
    
    static final int TRANSACTION_onEvent = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.neuron.INeuronSystemEventListener");
    }
    
    public static INeuronSystemEventListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.neuron.INeuronSystemEventListener");
      if (iInterface != null && iInterface instanceof INeuronSystemEventListener)
        return (INeuronSystemEventListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onEvent";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.neuron.INeuronSystemEventListener");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.neuron.INeuronSystemEventListener");
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        ContentValues contentValues = (ContentValues)ContentValues.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onEvent(param1Int1, (ContentValues)param1Parcel1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements INeuronSystemEventListener {
      public static INeuronSystemEventListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.neuron.INeuronSystemEventListener";
      }
      
      public void onEvent(int param2Int, ContentValues param2ContentValues) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.neuron.INeuronSystemEventListener");
          parcel1.writeInt(param2Int);
          if (param2ContentValues != null) {
            parcel1.writeInt(1);
            param2ContentValues.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && INeuronSystemEventListener.Stub.getDefaultImpl() != null) {
            INeuronSystemEventListener.Stub.getDefaultImpl().onEvent(param2Int, param2ContentValues);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INeuronSystemEventListener param1INeuronSystemEventListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INeuronSystemEventListener != null) {
          Proxy.sDefaultImpl = param1INeuronSystemEventListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INeuronSystemEventListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
