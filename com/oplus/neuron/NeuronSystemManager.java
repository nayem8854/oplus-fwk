package com.oplus.neuron;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.location.Location;
import android.os.IBinder;
import android.os.OplusSystemProperties;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityNr;
import android.telephony.CellInfo;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoNr;
import android.util.Log;
import android.util.Slog;
import java.util.Iterator;
import java.util.List;

public final class NeuronSystemManager {
  public static final int DEFAULT_PROP = 1;
  
  public static final boolean LOG_ON = false;
  
  public static final int NS_LOG_ON = 2;
  
  public static final int NS_ON = 1;
  
  private static final String TAG = "NeuronSystem";
  
  private static NeuronSystemManager sNeuronSystemManager;
  
  public static int sNsProp = OplusSystemProperties.getInt("persist.sys.oplus.neuron_system", 1);
  
  private INeuronSystemService mService = null;
  
  static {
    sNeuronSystemManager = null;
  }
  
  private NeuronSystemManager() {
    if (isEnable()) {
      IBinder iBinder = ServiceManager.getService("neuronsystem");
      INeuronSystemService iNeuronSystemService = INeuronSystemService.Stub.asInterface(iBinder);
      if (iNeuronSystemService == null)
        Slog.d("NeuronSystem", "can not get service neuronsystem"); 
    } 
  }
  
  public static NeuronSystemManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/neuron/NeuronSystemManager.sNeuronSystemManager : Lcom/oplus/neuron/NeuronSystemManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/neuron/NeuronSystemManager
    //   8: monitorenter
    //   9: getstatic com/oplus/neuron/NeuronSystemManager.sNeuronSystemManager : Lcom/oplus/neuron/NeuronSystemManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/neuron/NeuronSystemManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/neuron/NeuronSystemManager.sNeuronSystemManager : Lcom/oplus/neuron/NeuronSystemManager;
    //   27: ldc com/oplus/neuron/NeuronSystemManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/neuron/NeuronSystemManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/neuron/NeuronSystemManager.sNeuronSystemManager : Lcom/oplus/neuron/NeuronSystemManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #69	-> 0
    //   #70	-> 6
    //   #71	-> 9
    //   #72	-> 15
    //   #74	-> 27
    //   #77	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public static boolean isEnable() {
    int i = sNsProp;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public static boolean isLogEnable() {
    boolean bool;
    if ((sNsProp & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void publishEvent(int paramInt, ContentValues paramContentValues) {
    if (isEnable()) {
      INeuronSystemService iNeuronSystemService = this.mService;
      if (iNeuronSystemService != null) {
        try {
          iNeuronSystemService.publishEvent(paramInt, paramContentValues);
        } catch (Exception exception) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("NeuronSystemManager publishEvent err: ");
          stringBuilder.append(exception);
          Slog.d("NeuronSystem", stringBuilder.toString());
        } 
        return;
      } 
    } 
  }
  
  public void enableRecommendedApps(boolean paramBoolean, List<String> paramList) {
    if (isEnable()) {
      INeuronSystemService iNeuronSystemService = this.mService;
      if (iNeuronSystemService != null)
        try {
          iNeuronSystemService.enableRecommendedApps(paramBoolean, paramList);
          return;
        } catch (RemoteException remoteException) {
          Slog.e("NeuronSystem", "Exception happend while enableRecommendedApps", (Throwable)remoteException);
          return;
        }  
    } 
    Slog.d("NeuronSystem", "NeuronSystemManager enableRecommendedApps can not get service neuronsystem");
  }
  
  public List<String> getRecommendedApps(int paramInt) {
    if (isEnable()) {
      INeuronSystemService iNeuronSystemService = this.mService;
      if (iNeuronSystemService != null)
        try {
          return iNeuronSystemService.getRecommendedApps(paramInt);
        } catch (RemoteException remoteException) {
          Slog.e("NeuronSystem", "Exception happend while getRecommendedApps", (Throwable)remoteException);
          return null;
        }  
    } 
    Slog.d("NeuronSystem", "NeuronSystemManager getRecommendedApps can not get service neuronsystem");
    return null;
  }
  
  public static void notifyCellToNeuronSystem(int paramInt, List<CellInfo> paramList, boolean paramBoolean) {
    if (isEnable()) {
      ContentValues contentValues = new ContentValues();
      contentValues.put("date", Long.valueOf(System.currentTimeMillis()));
      for (Iterator<CellInfo> iterator = paramList.iterator(); iterator.hasNext(); ) {
        CellIdentityLte cellIdentityLte;
        CellInfo cellInfo = iterator.next();
        if (cellInfo.isRegistered() && cellInfo instanceof CellInfoLte) {
          cellIdentityLte = ((CellInfoLte)cellInfo).getCellIdentity();
          String str1 = cellIdentityLte.getMccString();
          String str2 = cellIdentityLte.getMncString();
          int i = cellIdentityLte.getCi();
          int j = cellIdentityLte.getPci();
          int k = cellIdentityLte.getTac();
          str1 = String.format("subid-%d-mcc-%s-mnc-%s-ci-%d-pci-%d-tac-%d", new Object[] { Integer.valueOf(paramInt), str1, str2, Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
          if (paramBoolean) {
            contentValues.put("type", "5GNSA");
          } else {
            contentValues.put("type", "4G");
          } 
          contentValues.put("data", str1);
          getInstance().publishEvent(107, contentValues);
          break;
        } 
        if (cellIdentityLte.isRegistered() && cellIdentityLte instanceof CellInfoNr) {
          CellInfoNr cellInfoNr = (CellInfoNr)cellIdentityLte;
          CellIdentityNr cellIdentityNr = (CellIdentityNr)cellInfoNr.getCellIdentity();
          String str2 = cellIdentityNr.getMccString();
          String str3 = cellIdentityNr.getMncString();
          long l = cellIdentityNr.getNci();
          int i = cellIdentityNr.getPci();
          int j = cellIdentityNr.getTac();
          String str1 = String.format("subid-%d-mcc-%s-mnc-%s-ci-%d-pci-%d-tac-%d", new Object[] { Integer.valueOf(paramInt), str2, str3, Long.valueOf(l), Integer.valueOf(i), Integer.valueOf(j) });
          contentValues.put("type", "5GSA");
          contentValues.put("data", str1);
          getInstance().publishEvent(107, contentValues);
          break;
        } 
      } 
    } 
  }
  
  public static void notifyTextViewContent(Context paramContext, int paramInt, String paramString) {
    if (!isEnable())
      return; 
    if (!OplusSystemProperties.getBoolean("persist.sys.oplus.feature_oca_on", false))
      return; 
    if ("com.app.shanghai.metro".equals(paramContext.getPackageName())) {
      Resources resources = paramContext.getResources();
      if (paramInt > 0 && Resources.resourceHasPackage(paramInt) && resources != null) {
        String str = resources.getResourceEntryName(paramInt);
        if ("tvInStationName".equals(str)) {
          bringUpContextAwareService(paramContext, "type_metro_app", paramString);
        } else if ("tvOutName".equals(str)) {
          ContentValues contentValues = new ContentValues();
          contentValues.put("SubwayOutStationName", paramString);
          NeuronSystemManager neuronSystemManager = getInstance();
          neuronSystemManager.publishEvent(108, contentValues);
        } 
      } 
    } 
  }
  
  private static void bringUpContextAwareService(Context paramContext, String paramString1, String paramString2) {
    Intent intent = new Intent();
    intent.setClassName("com.oplus.oca", "com.oppo.oca.MobileContextAwareService");
    intent.putExtra("SubwayInStationName", paramString2);
    intent.putExtra("type", paramString1);
    paramContext.startService(intent);
  }
  
  public static void notifyBrightness(float paramFloat) {
    if (!isEnable())
      return; 
    ContentValues contentValues = new ContentValues();
    contentValues.put("screen_brightness_event", Integer.valueOf((int)paramFloat));
    NeuronSystemManager neuronSystemManager = getInstance();
    neuronSystemManager.publishEvent(105, contentValues);
  }
  
  public static void notifyLocationChange(String paramString, Location paramLocation) {
    if (!isEnable())
      return; 
    ContentValues contentValues = new ContentValues();
    if ("gps".equals(paramString)) {
      contentValues.put("location_provider_type", Integer.valueOf(0));
    } else if ("network".equals(paramString)) {
      contentValues.put("location_provider_type", Integer.valueOf(1));
    } else if ("passive".equals(paramString)) {
      contentValues.put("location_provider_type", Integer.valueOf(2));
    } else if ("fused".equals(paramString)) {
      contentValues.put("location_provider_type", Integer.valueOf(3));
    } else {
      contentValues.put("location_provider_type", Integer.valueOf(-1));
    } 
    contentValues.put("provider", paramString);
    contentValues.put("gps_location_accuracy", Float.valueOf(paramLocation.getAccuracy()));
    contentValues.put("gps_location_attitude", Double.valueOf(paramLocation.getAltitude()));
    contentValues.put("gps_location_latitude", Double.valueOf(paramLocation.getLatitude()));
    contentValues.put("gps_location_longitude", Double.valueOf(paramLocation.getLongitude()));
    contentValues.put("gps_event", Integer.valueOf(3));
    getInstance().publishEvent(103, contentValues);
  }
  
  public static void notifyActivityChange(String paramString1, String paramString2, int paramInt1, int paramInt2) {
    if (!isEnable())
      return; 
    ContentValues contentValues = new ContentValues();
    contentValues.put("pkgname", paramString1);
    contentValues.put("activity", paramString2);
    contentValues.put("date", Long.valueOf(System.currentTimeMillis()));
    contentValues.put("uid", Integer.valueOf(paramInt1));
    contentValues.put("pid", Integer.valueOf(paramInt2));
    getInstance().publishEvent(101, contentValues);
  }
  
  public static void notifyNotificationContent(Context paramContext, String paramString1, String paramString2, String paramString3) {
    if (!isEnable())
      return; 
    if (!OplusSystemProperties.getBoolean("persist.sys.oplus.feature_oca_on", false))
      return; 
    if (isLogEnable()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("NeuronSystemManager notifyNotificationContent packageName=");
      stringBuilder.append(paramString1);
      stringBuilder.append(" title=");
      stringBuilder.append(paramString2);
      stringBuilder.append(" content=");
      stringBuilder.append(paramString3);
      Log.d("NeuronSystem", stringBuilder.toString());
    } 
    if (paramString1.equals("com.tencent.mm") && 
      "腾讯乘车码#服务通知".contains(paramString2) && 
      paramString3.contains("乘车通知"))
      bringUpContextAwareService(paramContext, "type_notification", null); 
  }
}
