package com.oplus;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.SystemProperties;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class OplusRomUpdateHelper {
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  static {
    CONTENT_URI_WHITE_LIST = Uri.parse("content://com.nearme.romupdate.provider.db/update_list");
  }
  
  private boolean which2use = true;
  
  private String mFilterName = "";
  
  private String mSystemFilePath = "";
  
  private String mDataFilePath = "";
  
  public Context mContext = null;
  
  public static final String BROADCAST_ACTION_ROM_UPDATE_CONFIG_SUCCES = "oppo.intent.action.ROM_UPDATE_CONFIG_SUCCESS";
  
  private static final String COLUMN_NAME_1 = "version";
  
  private static final String COLUMN_NAME_2 = "xml";
  
  private static final Uri CONTENT_URI_WHITE_LIST;
  
  private static final String OPPO_COMPONENT_SAFE_PERMISSION = "oppo.permission.OPPO_COMPONENT_SAFE";
  
  public static final String ROM_UPDATE_CONFIG_LIST = "ROM_UPDATE_CONFIG_LIST";
  
  private static final String TAG = "OplusRomUpdateHelper";
  
  private UpdateInfo mUpdateInfo1;
  
  private UpdateInfo mUpdateInfo2;
  
  protected class UpdateInfo {
    protected long mVersion = -1L;
    
    final OplusRomUpdateHelper this$0;
    
    public void parseContentFromXML(String param1String) {}
    
    public boolean clone(UpdateInfo param1UpdateInfo) {
      return false;
    }
    
    public boolean insert(int param1Int, String param1String) {
      return false;
    }
    
    public void clear() {}
    
    public void dump() {}
    
    public long getVersion() {
      return this.mVersion;
    }
    
    public boolean updateToLowerVersion(String param1String) {
      return false;
    }
  }
  
  public OplusRomUpdateHelper(Context paramContext, String paramString1, String paramString2, String paramString3) {
    this.mContext = paramContext;
    this.mFilterName = paramString1;
    this.mSystemFilePath = paramString2;
    this.mDataFilePath = paramString3;
  }
  
  public void init() {
    if (this.mDataFilePath == null || this.mSystemFilePath == null)
      return; 
    File file1 = new File(this.mDataFilePath);
    File file2 = file1;
    if (!file1.exists()) {
      file1 = new File(this.mSystemFilePath);
      file2 = file1;
      if (!file1.exists())
        return; 
    } 
    parseContentFromXML(readFromFile(file2));
  }
  
  protected void setUpdateInfo(UpdateInfo paramUpdateInfo1, UpdateInfo paramUpdateInfo2) {
    this.mUpdateInfo1 = paramUpdateInfo1;
    this.mUpdateInfo2 = paramUpdateInfo2;
  }
  
  public void initUpdateBroadcastReceiver() {
    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction("oppo.intent.action.ROM_UPDATE_CONFIG_SUCCESS");
    this.mContext.registerReceiver((BroadcastReceiver)new Object(this), intentFilter, "oppo.permission.OPPO_COMPONENT_SAFE", null);
  }
  
  protected UpdateInfo getUpdateInfo(boolean paramBoolean) {
    UpdateInfo updateInfo;
    if (paramBoolean) {
      if (this.which2use) {
        updateInfo = this.mUpdateInfo1;
      } else {
        updateInfo = this.mUpdateInfo2;
      } 
      return updateInfo;
    } 
    if (this.which2use) {
      updateInfo = this.mUpdateInfo2;
    } else {
      updateInfo = this.mUpdateInfo1;
    } 
    return updateInfo;
  }
  
  private void setFlip() {
    this.which2use ^= 0x1;
  }
  
  private boolean saveToFile(String paramString1, String paramString2) {
    try {
      File file2 = new File();
      this(paramString2);
      File file1 = new File();
      this(file2.getParent());
      if (!file1.isDirectory())
        file1.mkdirs(); 
      FileOutputStream fileOutputStream = new FileOutputStream();
      this(file2);
      fileOutputStream.write(paramString1.getBytes());
      fileOutputStream.close();
      return true;
    } catch (Exception exception) {
      exception.printStackTrace();
      return false;
    } 
  }
  
  public String getFilterName() {
    return this.mFilterName;
  }
  
  private String getDataFromProvider() {
    Cursor cursor1 = null, cursor2 = null;
    String str1 = null;
    Cursor cursor3 = null;
    Cursor cursor4 = cursor2, cursor5 = cursor1;
    String str2 = str1;
    try {
      String str3;
      Context context = this.mContext;
      if (context == null) {
        if (false)
          throw new NullPointerException(); 
        return null;
      } 
      cursor4 = cursor2;
      cursor5 = cursor1;
      str2 = str1;
      ContentResolver contentResolver = this.mContext.getContentResolver();
      cursor4 = cursor2;
      cursor5 = cursor1;
      str2 = str1;
      Uri uri = CONTENT_URI_WHITE_LIST;
      cursor4 = cursor2;
      cursor5 = cursor1;
      str2 = str1;
      StringBuilder stringBuilder = new StringBuilder();
      cursor4 = cursor2;
      cursor5 = cursor1;
      str2 = str1;
      this();
      cursor4 = cursor2;
      cursor5 = cursor1;
      str2 = str1;
      stringBuilder.append("filtername=\"");
      cursor4 = cursor2;
      cursor5 = cursor1;
      str2 = str1;
      stringBuilder.append(this.mFilterName);
      cursor4 = cursor2;
      cursor5 = cursor1;
      str2 = str1;
      stringBuilder.append("\"");
      cursor4 = cursor2;
      cursor5 = cursor1;
      str2 = str1;
      String str5 = stringBuilder.toString();
      cursor4 = cursor2;
      cursor5 = cursor1;
      str2 = str1;
      cursor1 = contentResolver.query(uri, new String[] { "version", "xml" }, str5, null, null);
      cursor2 = cursor3;
      if (cursor1 != null) {
        cursor2 = cursor3;
        cursor4 = cursor1;
        cursor5 = cursor1;
        str2 = str1;
        if (cursor1.getCount() > 0) {
          cursor4 = cursor1;
          cursor5 = cursor1;
          str2 = str1;
          int i = cursor1.getColumnIndex("version");
          cursor4 = cursor1;
          cursor5 = cursor1;
          str2 = str1;
          int j = cursor1.getColumnIndex("xml");
          cursor4 = cursor1;
          cursor5 = cursor1;
          str2 = str1;
          cursor1.moveToNext();
          cursor4 = cursor1;
          cursor5 = cursor1;
          str2 = str1;
          i = cursor1.getInt(i);
          cursor4 = cursor1;
          cursor5 = cursor1;
          str2 = str1;
          str3 = cursor1.getString(j);
          cursor4 = cursor1;
          cursor5 = cursor1;
          str2 = str3;
          StringBuilder stringBuilder1 = new StringBuilder();
          cursor4 = cursor1;
          cursor5 = cursor1;
          str2 = str3;
          this();
          cursor4 = cursor1;
          cursor5 = cursor1;
          str2 = str3;
          stringBuilder1.append("White List updated, version = ");
          cursor4 = cursor1;
          cursor5 = cursor1;
          str2 = str3;
          stringBuilder1.append(i);
          cursor4 = cursor1;
          cursor5 = cursor1;
          str2 = str3;
          Log.d("OplusRomUpdateHelper", stringBuilder1.toString());
        } 
      } 
      String str4 = str3;
      if (cursor1 != null) {
        cursor5 = cursor1;
        str2 = str3;
      } else {
        return str4;
      } 
    } catch (Exception exception) {
      cursor4 = cursor5;
      StringBuilder stringBuilder = new StringBuilder();
      cursor4 = cursor5;
      this();
      cursor4 = cursor5;
      stringBuilder.append("We can not get white list data from provider, because of ");
      cursor4 = cursor5;
      stringBuilder.append(exception);
      cursor4 = cursor5;
      Log.w("OplusRomUpdateHelper", stringBuilder.toString());
      String str = str2;
      if (cursor5 != null) {
        cursor5.close();
        str = str2;
        return str;
      } 
    } finally {}
    cursor5.close();
    return str2;
  }
  
  public String readFromFile(File paramFile) {
    if (paramFile == null)
      return ""; 
    FileInputStream fileInputStream1 = null, fileInputStream2 = null, fileInputStream3 = null;
    FileInputStream fileInputStream4 = fileInputStream3, fileInputStream5 = fileInputStream1, fileInputStream6 = fileInputStream2;
    try {
      FileInputStream fileInputStream8 = new FileInputStream();
      fileInputStream4 = fileInputStream3;
      fileInputStream5 = fileInputStream1;
      fileInputStream6 = fileInputStream2;
      this(paramFile);
      FileInputStream fileInputStream7 = fileInputStream8;
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      BufferedReader bufferedReader = new BufferedReader();
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      InputStreamReader inputStreamReader = new InputStreamReader();
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      this(fileInputStream7);
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      this(inputStreamReader);
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      StringBuffer stringBuffer = new StringBuffer();
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      this();
      while (true) {
        fileInputStream4 = fileInputStream7;
        fileInputStream5 = fileInputStream7;
        fileInputStream6 = fileInputStream7;
        String str1 = bufferedReader.readLine();
        if (str1 != null) {
          fileInputStream4 = fileInputStream7;
          fileInputStream5 = fileInputStream7;
          fileInputStream6 = fileInputStream7;
          stringBuffer.append(str1);
          continue;
        } 
        break;
      } 
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      String str = stringBuffer.toString();
      try {
        fileInputStream7.close();
      } catch (IOException iOException) {
        iOException.printStackTrace();
      } 
      return str;
    } catch (FileNotFoundException fileNotFoundException) {
      fileInputStream4 = fileInputStream6;
      fileNotFoundException.printStackTrace();
      if (fileInputStream6 != null)
        fileInputStream6.close(); 
    } catch (IOException iOException) {
      fileInputStream4 = fileInputStream5;
      iOException.printStackTrace();
      if (fileInputStream5 != null)
        try {
          fileInputStream5.close();
        } catch (IOException iOException1) {
          iOException1.printStackTrace();
        }  
    } finally {}
    return null;
  }
  
  public void parseContentFromXML(String paramString) {
    if (getUpdateInfo(true) != null)
      getUpdateInfo(true).parseContentFromXML(paramString); 
  }
  
  public void getUpdateFromProvider() {
    try {
      String str = getDataFromProvider();
      if (str == null)
        return; 
      if (updateToLowerVersion(str))
        return; 
      saveToFile(str, this.mDataFilePath);
      if (getUpdateInfo(false) == null)
        return; 
      getUpdateInfo(false).parseContentFromXML(str);
      setFlip();
      getUpdateInfo(false).clear();
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  private boolean updateToLowerVersion(String paramString) {
    UpdateInfo updateInfo = getUpdateInfo(true);
    if (updateInfo != null && updateInfo.updateToLowerVersion(paramString)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("updateToLowerVersion true, ");
      stringBuilder.append(updateInfo.hashCode());
      Log.d("OplusRomUpdateHelper", stringBuilder.toString());
      return true;
    } 
    return false;
  }
  
  protected boolean insertValueInList(int paramInt, String paramString) {
    if (getUpdateInfo(false).clone(getUpdateInfo(true)) && 
      getUpdateInfo(false).insert(paramInt, paramString)) {
      setFlip();
      getUpdateInfo(false).clear();
      return true;
    } 
    log("Failed to insert!");
    return false;
  }
  
  public void dump() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("which2use = ");
    stringBuilder.append(this.which2use);
    log(stringBuilder.toString());
    this.mUpdateInfo1.dump();
    this.mUpdateInfo2.dump();
  }
  
  public void log(String paramString) {
    if (!DEBUG)
      return; 
    Log.d("OplusRomUpdateHelper", paramString);
  }
  
  public void log(String paramString, Exception paramException) {
    if (!DEBUG)
      return; 
    Log.d("OplusRomUpdateHelper", paramString);
  }
}
