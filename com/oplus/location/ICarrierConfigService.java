package com.oplus.location;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ResultReceiver;

public interface ICarrierConfigService extends IInterface {
  void getCarrierConfig(String paramString, ResultReceiver paramResultReceiver) throws RemoteException;
  
  class Default implements ICarrierConfigService {
    public void getCarrierConfig(String param1String, ResultReceiver param1ResultReceiver) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICarrierConfigService {
    private static final String DESCRIPTOR = "com.oplus.location.ICarrierConfigService";
    
    static final int TRANSACTION_getCarrierConfig = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.location.ICarrierConfigService");
    }
    
    public static ICarrierConfigService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.location.ICarrierConfigService");
      if (iInterface != null && iInterface instanceof ICarrierConfigService)
        return (ICarrierConfigService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "getCarrierConfig";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.location.ICarrierConfigService");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.location.ICarrierConfigService");
      String str = param1Parcel1.readString();
      if (param1Parcel1.readInt() != 0) {
        ResultReceiver resultReceiver = (ResultReceiver)ResultReceiver.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      getCarrierConfig(str, (ResultReceiver)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements ICarrierConfigService {
      public static ICarrierConfigService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.location.ICarrierConfigService";
      }
      
      public void getCarrierConfig(String param2String, ResultReceiver param2ResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.location.ICarrierConfigService");
          parcel.writeString(param2String);
          if (param2ResultReceiver != null) {
            parcel.writeInt(1);
            param2ResultReceiver.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ICarrierConfigService.Stub.getDefaultImpl() != null) {
            ICarrierConfigService.Stub.getDefaultImpl().getCarrierConfig(param2String, param2ResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICarrierConfigService param1ICarrierConfigService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICarrierConfigService != null) {
          Proxy.sDefaultImpl = param1ICarrierConfigService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICarrierConfigService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
