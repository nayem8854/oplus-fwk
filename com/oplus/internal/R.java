package com.oplus.internal;

public final class R {
  public static final class anim {
    public static final int android_push_down_in = 201916416;
    
    public static final int android_push_down_out = 201916417;
    
    public static final int android_push_up_in = 201916418;
    
    public static final int android_push_up_out = 201916419;
    
    public static final int oplus_dialog_enter = 201916421;
    
    public static final int oplus_dialog_exit = 201916422;
    
    public static final int oplus_light_screen_rotate_0_enter = 201916423;
    
    public static final int oplus_light_screen_rotate_0_exit = 201916424;
    
    public static final int oplus_light_screen_rotate_180_enter = 201916425;
    
    public static final int oplus_light_screen_rotate_180_exit = 201916426;
    
    public static final int oplus_light_screen_rotate_minus_90_enter = 201916427;
    
    public static final int oplus_light_screen_rotate_minus_90_exit = 201916428;
    
    public static final int oplus_light_screen_rotate_plus_90_enter = 201916429;
    
    public static final int oplus_light_screen_rotate_plus_90_exit = 201916430;
    
    public static final int oplus_open_slide_enter = 201916431;
    
    public static final int oplus_open_slide_exit = 201916432;
    
    public static final int oplus_resolver_enter = 201916433;
    
    public static final int oplus_resolver_exit = 201916434;
    
    public static final int oplus_wallpaper_close_enter = 201916435;
    
    public static final int oplus_wallpaper_close_exit = 201916436;
    
    public static final int oppo_close_slide_enter = 201916442;
    
    public static final int oppo_close_slide_exit = 201916443;
    
    public static final int oppo_push_down_exit = 201916477;
    
    public static final int oppo_push_down_exit_splitscreen = 201916479;
    
    public static final int oppo_push_up_enter = 201916481;
    
    public static final int oppo_push_up_exit_splitscreen = 201916483;
    
    public static final int oppo_screen_rotate_0_enter = 201916485;
    
    public static final int oppo_screen_rotate_0_exit = 201916486;
    
    public static final int oppo_screen_rotate_180_enter = 201916487;
    
    public static final int oppo_screen_rotate_180_exit = 201916488;
    
    public static final int oppo_screen_rotate_minus_90_enter = 201916489;
    
    public static final int oppo_screen_rotate_minus_90_exit = 201916490;
    
    public static final int oppo_screen_rotate_plus_90_enter = 201916491;
    
    public static final int oppo_screen_rotate_plus_90_exit = 201916492;
    
    public static final int oppo_task_close_slide_enter = 201916495;
    
    public static final int oppo_task_close_slide_exit = 201916496;
    
    public static final int oppo_task_open_slide_enter = 201916498;
    
    public static final int oppo_task_open_slide_exit = 201916499;
    
    public static final int oppo_zoom_fade_enter = 201916506;
    
    public static final int oppo_zoom_fade_exit = 201916507;
  }
  
  public static final class array {
    public static final int android_common_nicknames = 201785348;
    
    public static final int android_config_mobile_hotspot_provision_app = 201785346;
    
    public static final int android_config_virtualKeyVibePattern = 201785347;
    
    public static final int brand_select_ringtone_files = 201785350;
    
    public static final int brand_select_ringtone_names = 201785351;
    
    public static final int color_uxicon_common_style_config_range = 201785352;
    
    public static final int color_uxicon_common_style_name = 201785353;
    
    public static final int color_uxicon_common_style_path = 201785354;
    
    public static final int color_uxicon_common_style_path_exp = 201785349;
    
    public static final int color_uxicon_common_style_prefix = 201785355;
    
    public static final int color_uxicon_special_style_config_range = 201785356;
    
    public static final int color_uxicon_special_style_name = 201785357;
    
    public static final int color_uxicon_special_style_path = 201785358;
    
    public static final int color_uxicon_special_style_prefix = 201785359;
    
    public static final int config_autoBrightnessLuxMaxLimit = 201785360;
    
    public static final int config_autoBrightnessLuxMinLimit = 201785361;
    
    public static final int config_elevenbit_autoBrightnessLcdBacklightValues = 201785362;
    
    public static final int config_elevenbit_autoBrightnessLevels = 201785363;
    
    public static final int config_elevenbit_autoBrightnessLuxMaxLimit = 201785364;
    
    public static final int config_elevenbit_autoBrightnessLuxMinLimit = 201785365;
    
    public static final int config_networkLocationProviderPackageNames = 201785366;
    
    public static final int config_tenbit_autoBrightnessLcdBacklightValues = 201785367;
    
    public static final int config_tenbit_autoBrightnessLevels = 201785368;
    
    public static final int config_tenbit_autoBrightnessLuxMaxLimit = 201785369;
    
    public static final int config_tenbit_autoBrightnessLuxMinLimit = 201785370;
    
    public static final int oplus_resolver_target_pin = 201785371;
    
    public static final int oplus_resolver_target_unpin = 201785372;
    
    public static final int oppo_cmcc_operator_list = 201785373;
    
    public static final int oppo_cn_operator_list = 201785374;
    
    public static final int oppo_config_statusBarIcons = 201785375;
    
    public static final int oppo_data_full_array = 201785376;
    
    public static final int oppo_data_not_enough_array = 201785377;
    
    public static final int oppo_permissions_prompt = 201785378;
    
    public static final int oppo_preloaded_color_state_lists = 201785379;
    
    public static final int oppo_preloaded_drawables = 201785380;
    
    public static final int oppo_select_ringtone_new = 201785344;
    
    public static final int oppo_select_ringtone_old = 201785345;
    
    public static final int oppo_sms_filter_package_names = 201785381;
  }
  
  public static final class attr {
    public static final int C05 = 201981956;
    
    public static final int C18 = 201981969;
    
    public static final int C20 = 201981971;
    
    public static final int C24 = 201981975;
    
    public static final int OplusCircleProgressBarStyle = 201981987;
    
    public static final int OplusLoadingViewStyle = 201981990;
    
    public static final int alertBackgroundDrawable = 201982002;
    
    public static final int alertCornerRadius = 201982003;
    
    public static final int alertShadowDrawable = 201982004;
    
    public static final int colorCancelNormalDrawable = 201982010;
    
    public static final int colorCancelPressedDrawable = 201982011;
    
    public static final int colorCancelText = 201982012;
    
    public static final int colorEmergencyNormalDrawable = 201982014;
    
    public static final int colorEmergencyPressedDrawable = 201982015;
    
    public static final int colorEmergencyText = 201982016;
    
    public static final int colorErrorColor = 201982017;
    
    public static final int colorLockText = 201392128;
    
    public static final int colorManuallyNormalDrawable = 201392129;
    
    public static final int colorOShareButtonBackground = 201392131;
    
    public static final int colorOShareButtonTextColor = 201392130;
    
    public static final int colorOuterCircleMaxAlpha = 201982019;
    
    public static final int colorPathColor = 201982020;
    
    public static final int colorRebootHint = 201982021;
    
    public static final int colorRegularColor = 201982022;
    
    public static final int colorShutdownHint = 201982023;
    
    public static final int colorShutdownViewStyle = 201982024;
    
    public static final int colorSuccessColor = 201982025;
    
    public static final int colorTintControlNormal = 201982027;
    
    public static final int colorTintLightNormal = 201982029;
    
    public static final int containerCornerRadius = 201982035;
    
    public static final int containerDeltaLength = 201982036;
    
    public static final int containerShadowColor = 201982037;
    
    public static final int containerShadowRadius = 201982038;
    
    public static final int deltaX = 201982039;
    
    public static final int deltaY = 201982040;
    
    public static final int dotClickable = 201982043;
    
    public static final int dotColor = 201982044;
    
    public static final int dotCornerRadius = 201982045;
    
    public static final int dotIsStrokeStyle = 201982046;
    
    public static final int dotSize = 201982047;
    
    public static final int dotSpacing = 201982048;
    
    public static final int dotStrokeWidth = 201982049;
    
    public static final int enable = 201982051;
    
    public static final int isOplusTheme = 201982057;
    
    public static final int landscapeMaxHeight = 201982058;
    
    public static final int landscapeMaxWidth = 201982059;
    
    public static final int max = 201982066;
    
    public static final int oplusToastLayout = 201982069;
    
    public static final int portraitMaxHeight = 201982077;
    
    public static final int portraitMaxWidth = 201982078;
    
    public static final int progress = 201982079;
    
    public static final int progressBarBgCircleColor = 201982080;
    
    public static final int progressBarColor = 201982081;
    
    public static final int progressBarHeight = 201982082;
    
    public static final int progressBarType = 201982083;
    
    public static final int progressBarWidth = 201982084;
    
    public static final int rfRadius = 201982087;
    
    public static final int traceDotColor = 201982099;
  }
  
  public static final class bool {
    public static final int android_config_alwaysUseCdmaRssi = 202113030;
    
    public static final int android_config_automatic_brightness_available = 202113026;
    
    public static final int android_config_closeDialogWhenTouchOutside = 202113035;
    
    public static final int android_config_dreamsActivatedOnDockByDefault = 202113032;
    
    public static final int android_config_dreamsActivatedOnSleepByDefault = 202113033;
    
    public static final int android_config_dreamsEnabledByDefault = 202113031;
    
    public static final int android_config_enableDreams = 202113027;
    
    public static final int android_config_enable_emergency_call_while_sim_locked = 202113036;
    
    public static final int android_config_enable_puk_unlock_screen = 202113037;
    
    public static final int android_config_intrusiveNotificationLed = 202113029;
    
    public static final int android_config_safe_media_volume_enabled = 202113038;
    
    public static final int android_config_useMasterVolume = 202113039;
    
    public static final int android_config_useVolumeKeySounds = 202113040;
    
    public static final int android_config_use_strict_phone_number_comparation = 202113024;
    
    public static final int android_config_voice_capable = 202113025;
    
    public static final int android_config_wimaxEnabled = 202113028;
    
    public static final int config_bluetooth_hfp_inband_ringing_support_overlay = 202113042;
    
    public static final int config_enableNetworkLocationProviderOverlay = 202113043;
    
    public static final int oppo_scene_preview_tap_scroll_fast_mode = 202113045;
    
    public static final int oppo_texture_resize = 202113046;
  }
  
  public static final class color {
    public static final int android_accent_device_default_dark = 201719826;
    
    public static final int android_accent_device_default_light = 201719827;
    
    public static final int android_error_color_material_dark = 201719817;
    
    public static final int android_error_color_material_light = 201719818;
    
    public static final int android_notification_default_color = 201719821;
    
    public static final int android_notification_material_background_color = 201719822;
    
    public static final int android_notification_primary_text_color_dark = 201719820;
    
    public static final int android_notification_primary_text_color_light = 201719819;
    
    public static final int android_primary_text_material_dark = 201719813;
    
    public static final int android_primary_text_material_light = 201719814;
    
    public static final int android_quaternary_device_default_settings = 201719823;
    
    public static final int android_secondary_text_material_dark = 201719815;
    
    public static final int android_secondary_text_material_light = 201719816;
    
    public static final int android_tertiary_device_default_settings = 201719824;
    
    public static final int color_background_color = 201719877;
    
    public static final int color_bar_color = 201719878;
    
    public static final int color_cancel_text_color = 201719899;
    
    public static final int color_force_reboot_text_color = 201719911;
    
    public static final int color_handler_color = 201719920;
    
    public static final int color_input_method_navigation_guard = 201719829;
    
    public static final int color_input_method_navigation_guard_exp = 201719830;
    
    public static final int color_manually_lock_button_pressed = 201719837;
    
    public static final int color_manually_lock_text_color = 201719836;
    
    public static final int color_notification_content_color = 201719812;
    
    public static final int color_notification_summary_color = 201719810;
    
    public static final int color_notification_time_color = 201719811;
    
    public static final int color_notification_title_color = 201719809;
    
    public static final int color_permission_controller_green = 201719828;
    
    public static final int color_reboot_color = 201719934;
    
    public static final int color_road_color = 201719941;
    
    public static final int color_save_dialog_background_color = 201719834;
    
    public static final int color_save_dialog_divider_color = 201719833;
    
    public static final int color_save_dialog_ripple_color = 201719835;
    
    public static final int color_save_dialog_text_color = 201719831;
    
    public static final int color_save_dialog_title_color = 201719832;
    
    public static final int color_shutdown_color = 201719942;
    
    public static final int color_shutdown_text_color = 201719943;
    
    public static final int color_toast_text_color = 201719951;
    
    public static final int oplus_chooser_preview_more_cover = 201720047;
    
    public static final int oplus_inputmethod_switcher_item_color = 201720050;
    
    public static final int oplus_panel_drag_color = 201720052;
    
    public static final int oplus_popup_list_selector_color_disable = 201720053;
    
    public static final int oplus_popup_list_selector_color_normal = 201720054;
    
    public static final int oplus_popup_list_selector_color_pressed = 201720055;
    
    public static final int oplus_popup_list_window_text_color_disabled = 201720056;
    
    public static final int oplus_popup_list_window_text_color_normal = 201720057;
    
    public static final int oplus_popup_list_window_text_color_selector = 201720058;
    
    public static final int oplus_popup_list_window_text_pressed_color = 201720059;
    
    public static final int oplus_resolve_dialog_oshare_user_status_text_color = 201720061;
    
    public static final int oplus_resolver_dot = 201720062;
    
    public static final int oplus_resolver_dot_circle = 201720063;
    
    public static final int oplus_resolver_empty_color = 201720064;
    
    public static final int oplus_resolver_navigation_bar = 201720065;
    
    public static final int oppo_alert_dialog_content_text_color = 201720071;
    
    public static final int oppo_alert_dialog_title_text_color = 201720072;
    
    public static final int oppo_dialog_button_divider_color = 201720079;
    
    public static final int oppo_dialog_button_text_color_bottom = 201720081;
    
    public static final int oppo_dialog_button_text_color_disable = 201720083;
    
    public static final int oppo_dialog_content_text_color_large = 201720086;
    
    public static final int oppo_dialog_content_text_color_small = 201720087;
    
    public static final int oppo_dialog_title_text_color = 201720088;
    
    public static final int oppo_list_text_color_large = 201720091;
    
    public static final int oppo_list_text_color_small = 201720092;
    
    public static final int oppo_primary_text_dark = 201720098;
    
    public static final int oppo_transparence = 201720103;
    
    public static final int resolve_dialog_button_textcolor_normal = 201720160;
    
    public static final int resolve_dialog_button_textcolor_select = 201720161;
    
    public static final int resolve_dialog_dot_focus = 201720162;
    
    public static final int resolve_dialog_dot_no_focus = 201720163;
    
    public static final int resolve_dialog_item_textcolor = 201720164;
    
    public static final int resolve_dialog_select = 201720165;
  }
  
  public static final class dimen {
    public static final int TD05 = 201654305;
    
    public static final int TD07 = 201654307;
    
    public static final int TF04 = 201654338;
    
    public static final int alert_dialog_bottom_corner_radius = 201654369;
    
    public static final int alert_dialog_central_max_height = 201654371;
    
    public static final int alert_dialog_item_padding_offset = 201654372;
    
    public static final int alert_dialog_list_item_min_height = 201654373;
    
    public static final int alert_dialog_list_item_padding_bottom = 201654374;
    
    public static final int alert_dialog_list_item_padding_left = 201654375;
    
    public static final int alert_dialog_list_item_padding_right = 201654376;
    
    public static final int alert_dialog_list_item_padding_top = 201654377;
    
    public static final int alert_dialog_padding_bottom = 201654378;
    
    public static final int alert_dialog_single_list_padding_left = 201654379;
    
    public static final int alert_dialog_single_list_padding_right = 201654380;
    
    public static final int alert_dialog_single_list_padding_vertical = 201654381;
    
    public static final int alert_dialog_single_list_title_margin_end = 201654382;
    
    public static final int android_navigation_bar_height = 201654273;
    
    public static final int android_notification_content_margin_end = 201654278;
    
    public static final int android_notification_content_margin_start = 201654277;
    
    public static final int android_notification_content_margin_top = 201654276;
    
    public static final int android_notification_header_icon_size_ambient = 201654280;
    
    public static final int android_notification_header_padding_top = 201654279;
    
    public static final int android_notification_text_size = 201654275;
    
    public static final int android_status_bar_height = 201654272;
    
    public static final int color_alert_dialog_bg_padding_left = 201654392;
    
    public static final int color_alert_dialog_bg_radius = 201654393;
    
    public static final int color_alert_dialog_button_horizontal_padding = 201654395;
    
    public static final int color_alert_dialog_button_padding = 201654396;
    
    public static final int color_alert_dialog_button_padding_top = 201654397;
    
    public static final int color_alert_dialog_divider_margin_top = 201654403;
    
    public static final int color_alert_dialog_message_padding_left = 201654408;
    
    public static final int color_bar_height = 201654415;
    
    public static final int color_bar_offset = 201654416;
    
    public static final int color_bar_radius = 201654417;
    
    public static final int color_bar_width = 201654418;
    
    public static final int color_cancel_drawable_margin = 201654419;
    
    public static final int color_cancel_margin_bottom = 201654420;
    
    public static final int color_cancel_margin_right = 201654421;
    
    public static final int color_cancel_offset = 201654422;
    
    public static final int color_cancel_text_size = 201654423;
    
    public static final int color_cancel_width = 201654424;
    
    public static final int color_default_minimal_size_resizable_task = 201654425;
    
    public static final int color_delete_alert_dialog_bottom_button_height = 201654426;
    
    public static final int color_delete_alert_dialog_button_height = 201654427;
    
    public static final int color_delete_alert_dialog_divider_height = 201654429;
    
    public static final int color_dimen_gesture_area_height = 201654286;
    
    public static final int color_emergency_text_max_width = 201654431;
    
    public static final int color_force_reboot_margin_bottom = 201654455;
    
    public static final int color_force_reboot_text_max_height = 201654456;
    
    public static final int color_force_reboot_text_size = 201654457;
    
    public static final int color_global_action_shut_down_text_padding = 201654458;
    
    public static final int color_handler_width = 201654460;
    
    public static final int color_inputmethod_padding_bottom_20 = 201654461;
    
    public static final int color_inputmethod_padding_bottom_25 = 201654462;
    
    public static final int color_inputmethod_padding_bottom_30 = 201654463;
    
    public static final int color_inputmethod_radius_20_threshold = 201654464;
    
    public static final int color_inputmethod_radius_25_threshold = 201654465;
    
    public static final int color_inputmethod_radius_30_threshold = 201654466;
    
    public static final int color_landscape_force_reboot_margin_bottom = 201654467;
    
    public static final int color_longshot_cover_height = 201654489;
    
    public static final int color_longshot_min_list_height = 201654490;
    
    public static final int color_longshot_min_scroll_distance = 201654491;
    
    public static final int color_longshot_min_scroll_height = 201654492;
    
    public static final int color_manually_lock_drawable_margin = 201654283;
    
    public static final int color_manually_lock_text_size = 201654285;
    
    public static final int color_manually_lock_width = 201654284;
    
    public static final int color_reboot_line_height = 201654513;
    
    public static final int color_reboot_line_width = 201654514;
    
    public static final int color_reboot_offset = 201654515;
    
    public static final int color_reboot_width = 201654516;
    
    public static final int color_road_line_width = 201654517;
    
    public static final int color_road_offset = 201654518;
    
    public static final int color_road_phase = 201654519;
    
    public static final int color_road_width = 201654520;
    
    public static final int color_scrollbar_min_height_normal = 201654521;
    
    public static final int color_scrollbar_min_height_overscroll = 201654522;
    
    public static final int color_scrollbar_padding = 201654523;
    
    public static final int color_shutdown_line_width = 201654524;
    
    public static final int color_shutdown_text_offset = 201654525;
    
    public static final int color_shutdown_text_size = 201654526;
    
    public static final int color_systemui_gesture_bar_height = 201654274;
    
    public static final int color_systemui_inputmethod_padding_bottom = 201654534;
    
    public static final int color_toast_padding_bottom = 201654537;
    
    public static final int color_toast_padding_left = 201654538;
    
    public static final int color_toast_padding_right = 201654539;
    
    public static final int color_toast_padding_top = 201654540;
    
    public static final int color_toast_text_size = 201654541;
    
    public static final int floating_toolbar_corner_radius = 201654542;
    
    public static final int floating_toolbar_overflow_min_width = 201654543;
    
    public static final int floating_toolbar_overflow_padding_bottom_extra = 201654544;
    
    public static final int floating_toolbar_overflow_padding_top_extra = 201654545;
    
    public static final int floating_toolbar_overflow_scroll_bar_size = 201654546;
    
    public static final int floating_toolbar_padding_end_extra = 201654547;
    
    public static final int floating_toolbar_padding_start_extra = 201654548;
    
    public static final int magnifier_corner_radius = 201654551;
    
    public static final int magnifier_height = 201654552;
    
    public static final int magnifier_offset = 201654553;
    
    public static final int magnifier_width = 201654554;
    
    public static final int notification_progress_bar_radius_material = 201654282;
    
    public static final int oplus_chooser_preview_corner_radius = 201654555;
    
    public static final int oplus_chooser_preview_image_border = 201654558;
    
    public static final int oplus_chooser_preview_more_textSize = 201654562;
    
    public static final int oplus_circle_loading_large_strokewidth = 201654565;
    
    public static final int oplus_circle_loading_medium_strokewidth = 201654566;
    
    public static final int oplus_circle_loading_strokewidth = 201654567;
    
    public static final int oplus_inputmethod_switcher_max_height = 201654568;
    
    public static final int oplus_inputmethod_switcher_max_width = 201654569;
    
    public static final int oplus_loading_view_default_length = 201654570;
    
    public static final int oplus_popup_list_padding_vertical = 201654571;
    
    public static final int oplus_popup_list_window_content_radius = 201654572;
    
    public static final int oplus_popup_list_window_fading_edge_length = 201654573;
    
    public static final int oplus_popup_list_window_item_icon_margin_left = 201654574;
    
    public static final int oplus_popup_list_window_item_icon_margin_right = 201654575;
    
    public static final int oplus_popup_list_window_item_min_height = 201654576;
    
    public static final int oplus_popup_list_window_item_padding_top_and_bottom = 201654577;
    
    public static final int oplus_popup_list_window_item_title_margin_left = 201654578;
    
    public static final int oplus_popup_list_window_item_title_margin_right = 201654579;
    
    public static final int oplus_popup_list_window_item_title_margin_with_no_icon = 201654580;
    
    public static final int oplus_popup_list_window_item_title_text_size = 201654581;
    
    public static final int oplus_popup_list_window_max_width = 201654582;
    
    public static final int oplus_popup_list_window_min_width = 201654583;
    
    public static final int oplus_popup_list_window_selection_scroll_offset = 201654584;
    
    public static final int oplus_resolve_dialog_app_name_font_size = 201654586;
    
    public static final int oplus_resolve_dialog_chinese_text_length = 201654587;
    
    public static final int oplus_resolve_dialog_english_text_length = 201654588;
    
    public static final int oplus_resolve_dialog_itembg_paddingright = 201654590;
    
    public static final int oplus_resolve_dialog_text_line_distance = 201654602;
    
    public static final int oplus_resolve_dialog_text_paddingbottom = 201654603;
    
    public static final int oplus_resolve_empty_item_width = 201654604;
    
    public static final int oplus_resolver_tabhost_height = 201654632;
    
    public static final int oppo_dialog_buttonPanel_height = 201654646;
    
    public static final int oppo_dialog_button_text_size = 201654648;
    
    public static final int oppo_dialog_max_height = 201654658;
    
    public static final int oppo_dialog_max_height_landscape = 201654659;
    
    public static final int oppo_dialog_width = 201654662;
    
    public static final int oppo_floating_toolbar_height = 201654667;
    
    public static final int oppo_list_text_size_large = 201654670;
    
    public static final int oppo_list_text_size_small = 201654671;
    
    public static final int resolve_dialog_app_icon_top_margin = 201654684;
    
    public static final int resolve_dialog_button_height = 201654685;
    
    public static final int resolve_dialog_button_width = 201654686;
    
    public static final int resolve_dialog_dot_margin_bottom = 201654687;
    
    public static final int resolve_dialog_dot_radius = 201654688;
    
    public static final int resolve_dialog_dot_view_height = 201654689;
    
    public static final int resolve_dialog_icon_height = 201654690;
    
    public static final int resolve_dialog_icon_width = 201654691;
    
    public static final int resolve_dialog_item_height = 201654692;
    
    public static final int resolve_dialog_item_textsize = 201654693;
    
    public static final int resolve_dialog_item_width = 201654694;
    
    public static final int resolve_dialog_left_padding = 201654695;
    
    public static final int resolve_dialog_menu_icon_text_distance = 201654696;
    
    public static final int resolve_dialog_menu_item_height = 201654697;
    
    public static final int resolve_dialog_menu_item_width = 201654698;
    
    public static final int resolve_dialog_menu_padding_left = 201654699;
    
    public static final int resolve_dialog_menu_padding_top = 201654700;
    
    public static final int resolve_dialog_menu_view_height = 201654701;
    
    public static final int resolve_dialog_oshare_icon_top_margin = 201654702;
    
    public static final int resolve_dialog_select_height = 201654703;
    
    public static final int resolve_dialog_select_width = 201654704;
    
    public static final int resolve_dialog_top_padding = 201654705;
    
    public static final int resolver_panel_landscape_max_width = 201654707;
    
    public static final int resolver_panel_portrait_max_height = 201654708;
    
    public static final int textsize_on_shutdown_bar = 201654709;
    
    public static final int ux_icon_default_material_size = 201654710;
    
    public static final int ux_icon_default_radius = 201654711;
    
    public static final int ux_icon_default_size = 201654712;
    
    public static final int ux_icon_radius_end = 201654713;
  }
  
  public static final class drawable {
    public static final int android_btn_keyboard_key_fulltrans = 201850888;
    
    public static final int android_code_lock_bottom = 201850891;
    
    public static final int android_code_lock_left = 201850889;
    
    public static final int android_code_lock_top = 201850890;
    
    public static final int android_ic_audio_vol = 201850921;
    
    public static final int android_ic_audio_vol_mute = 201850920;
    
    public static final int android_ic_contact_picture = 201850922;
    
    public static final int android_ic_emergency = 201850917;
    
    public static final int android_ic_media_route_disabled_holo_dark = 201850919;
    
    public static final int android_ic_media_route_on_holo_dark = 201850918;
    
    public static final int android_ic_menu_cc = 201851219;
    
    public static final int android_quickcontact_badge_overlay_normal_light = 201850887;
    
    public static final int android_quickcontact_badge_overlay_pressed_light = 201850886;
    
    public static final int android_scrubber_control_disabled_holo = 201850897;
    
    public static final int android_scrubber_control_selector_holo = 201850898;
    
    public static final int android_scrubber_progress_horizontal_holo_dark = 201850899;
    
    public static final int android_stat_sys_gps_on = 201850892;
    
    public static final int android_sym_app_on_sd_unavailable_icon = 201850893;
    
    public static final int android_usb_android = 201850900;
    
    public static final int android_usb_android_connected = 201850901;
    
    public static final int color_alert_bottom_dialog_corner_button_background = 201851221;
    
    public static final int color_alert_button_left = 201851222;
    
    public static final int color_alert_button_right = 201851225;
    
    public static final int color_alert_dialog_item_background = 201851229;
    
    public static final int color_autofill_save_dialog_background = 201850907;
    
    public static final int color_autofill_save_text_ripple = 201850908;
    
    public static final int color_bottom_alert_dialog_bg_landscape = 201851230;
    
    public static final int color_bottom_alert_dialog_bg_portrait = 201851231;
    
    public static final int color_bottom_alert_dialog_bg_with_shadow = 201851232;
    
    public static final int color_data_space_monitor_curve = 201851249;
    
    public static final int color_global_action_dynamic_bg = 201851257;
    
    public static final int color_global_drag_text_shape = 201851263;
    
    public static final int color_ic_account_circle = 201850910;
    
    public static final int color_ic_corp_badge_case_multiapp = 201850911;
    
    public static final int color_ic_corp_badge_no_background_multiapp = 201850912;
    
    public static final int color_ic_corp_icon_badge_multiapp = 201850903;
    
    public static final int color_launcher_ic_warning = 201851267;
    
    public static final int color_notification_bg = 201850896;
    
    public static final int color_permission_controller_center_alert_dialog_bg = 201850904;
    
    public static final int color_permission_controller_dialog_button_middle_ripple = 201850905;
    
    public static final int color_permission_controller_list_divider = 201850906;
    
    public static final int color_stat_notify_disk_full = 201850931;
    
    public static final int color_toast_frame = 201851285;
    
    public static final int color_upgrade_logo = 201850885;
    
    public static final int decor_zoom_closed_button = 201851288;
    
    public static final int decor_zoom_control_button = 201851289;
    
    public static final int file_apk_icon = 201851290;
    
    public static final int file_audio_icon = 201851291;
    
    public static final int file_compress_rar_icon = 201851292;
    
    public static final int file_compress_zip_icon = 201851293;
    
    public static final int file_doc_icon = 201851294;
    
    public static final int file_ebook_icon = 201851295;
    
    public static final int file_excel_icon = 201851296;
    
    public static final int file_html_icon = 201851297;
    
    public static final int file_lrc_icon = 201851298;
    
    public static final int file_multiple_files = 201851299;
    
    public static final int file_other_icon = 201851300;
    
    public static final int file_pdf_icon = 201851301;
    
    public static final int file_ppt_icon = 201851302;
    
    public static final int file_txt_icon = 201851303;
    
    public static final int oplus_chooser_grid_preview_bg = 201851307;
    
    public static final int oplus_chooser_text_copy = 201851308;
    
    public static final int oplus_ic_o_share_off_normal = 201851309;
    
    public static final int oplus_ic_o_share_on = 201851310;
    
    public static final int oplus_inputmethod_switcher_item_icon = 201851312;
    
    public static final int oplus_notification_progress_horizontal_material = 201850909;
    
    public static final int oplus_oshare_icon_default = 201851313;
    
    public static final int oplus_oshare_icon_mi = 201851314;
    
    public static final int oplus_oshare_icon_oppo = 201851315;
    
    public static final int oplus_oshare_icon_realme = 201851316;
    
    public static final int oplus_oshare_icon_vivo = 201851317;
    
    public static final int oplus_page_indicator_dot = 201851319;
    
    public static final int oplus_page_indicator_dot_stroke = 201851320;
    
    public static final int oplus_popup_list_selector = 201851322;
    
    public static final int oplus_popup_list_window_bg = 201851323;
    
    public static final int oplus_resolver_jump_to_market_next = 201851334;
    
    public static final int oplus_resolver_more_icon = 201851335;
    
    public static final int oplus_resolver_no_access = 201851338;
    
    public static final int oplus_resolver_no_content_empty = 201851339;
    
    public static final int oplus_resolver_tabhost_bg = 201851340;
    
    public static final int oplus_verification_code_bg = 201851341;
    
    public static final int oppo_default_wallpaper = 201850902;
    
    public static final int oppo_ft_avd_toarrow = 201851365;
    
    public static final int oppo_ft_avd_toarrow_animation = 201851366;
    
    public static final int oppo_ft_avd_tooverflow = 201851367;
    
    public static final int oppo_ft_avd_tooverflow_animation = 201851368;
    
    public static final int oppo_ic_audio_alarm = 201850895;
    
    public static final int oppo_ic_audio_alarm_mute = 201850894;
    
    public static final int oppo_magnifier = 201851371;
    
    public static final int oppo_reboot = 201851448;
    
    public static final int oppo_root_large_icon = 201850930;
    
    public static final int oppo_root_small_icon = 201850929;
    
    public static final int oppo_shutdown = 201851454;
    
    public static final int oppo_stat_notify_android_system = 201851455;
    
    public static final int oppo_stat_notify_wifi_in_range = 201850916;
    
    public static final int oppo_stat_notify_wifi_in_range_large = 201850924;
    
    public static final int oppo_stat_sys_tether_bluetooth = 201850923;
    
    public static final int oppo_stat_sys_tether_bluetooth_large = 201850926;
    
    public static final int oppo_stat_sys_tether_general = 201850913;
    
    public static final int oppo_stat_sys_tether_general_large = 201850928;
    
    public static final int oppo_stat_sys_tether_usb = 201850915;
    
    public static final int oppo_stat_sys_tether_usb_large = 201850927;
    
    public static final int oppo_stat_sys_tether_wifi = 201850914;
    
    public static final int oppo_stat_sys_tether_wifi_large = 201850925;
    
    public static final int oppo_stat_sys_tether_wifi_sharing_large = 201851458;
    
    public static final int sim_settings_dualsim_ic = 201851479;
    
    public static final int splash_window_com_tencent_mm = 201851480;
    
    public static final int splash_window_com_tencent_mtt = 201851481;
    
    public static final int splash_window_com_ucmobile = 201851482;
  }
  
  public static final class id {
    public static final int action0 = 201457664;
    
    public static final int action_divider = 201457666;
    
    public static final int actions = 201457667;
    
    public static final int alertTitle = 201457668;
    
    public static final int alert_dialog_bottom_space = 201457669;
    
    public static final int anchor = 201457671;
    
    public static final int android_progress_percent = 201457672;
    
    public static final int android_status = 201457665;
    
    public static final int android_up = 201457673;
    
    public static final int big_picture = 201457675;
    
    public static final int big_text = 201457676;
    
    public static final int bottom_panel = 201457678;
    
    public static final int btn_more_settings = 201457679;
    
    public static final int button_view_stub = 201457682;
    
    public static final int chronometer = 201457687;
    
    public static final int closed_zoom = 201457688;
    
    public static final int color_alert_dialog_divider1 = 201457689;
    
    public static final int color_alert_dialog_divider2 = 201457690;
    
    public static final int color_app_fullscreen_textview = 201457691;
    
    public static final int color_back_arrow = 201457693;
    
    public static final int color_keyboard_view_close = 201457696;
    
    public static final int color_keyboard_view_detail = 201457697;
    
    public static final int color_keyboard_view_text = 201457698;
    
    public static final int color_keyboard_view_top = 201457699;
    
    public static final int content_panel = 201457710;
    
    public static final int control_zoom = 201457711;
    
    public static final int custom_panel = 201457712;
    
    public static final int floating_toolbar_menu_item_image = 201457716;
    
    public static final int floating_toolbar_menu_item_text = 201457717;
    
    public static final int hard_keyboard_section = 201457721;
    
    public static final int hard_keyboard_switch = 201457722;
    
    public static final int icon = 201457724;
    
    public static final int ime_name = 201457725;
    
    public static final int ime_status = 201457726;
    
    public static final int inbox_more = 201457727;
    
    public static final int inbox_text0 = 201457728;
    
    public static final int inbox_text1 = 201457729;
    
    public static final int inbox_text2 = 201457730;
    
    public static final int inbox_text3 = 201457731;
    
    public static final int inbox_text4 = 201457732;
    
    public static final int inbox_text5 = 201457733;
    
    public static final int inbox_text6 = 201457734;
    
    public static final int info = 201457735;
    
    public static final int iv_button_content_divider = 201457736;
    
    public static final int iv_button_divider_1 = 201457737;
    
    public static final int iv_button_divider_2 = 201457738;
    
    public static final int iv_security_button_content_divider = 201457739;
    
    public static final int keyboardview = 201457740;
    
    public static final int line1 = 201457744;
    
    public static final int line3 = 201457745;
    
    public static final int listPanel = 201457746;
    
    public static final int list_divider = 201457747;
    
    public static final int list_view = 201457749;
    
    public static final int message = 201457751;
    
    public static final int message_view = 201457752;
    
    public static final int oplus_buttonSelect = 201457759;
    
    public static final int oplus_chooser_action_nearby_tx = 201457760;
    
    public static final int oplus_page_indicator_dot = 201457761;
    
    public static final int oplus_popup_list_container = 201457762;
    
    public static final int oplus_popup_list_view = 201457763;
    
    public static final int oplus_resolver_checkbox_container = 201457764;
    
    public static final int oplus_resolver_dots = 201457765;
    
    public static final int oplus_resolver_market_jump = 201457766;
    
    public static final int oplus_resolver_oshare_devider_line = 201457767;
    
    public static final int oplus_resolver_pager = 201457768;
    
    public static final int oplus_toast_layout = 201457771;
    
    public static final int oplus_verification_code_container = 201457772;
    
    public static final int oplus_verification_code_mms_icon = 201457773;
    
    public static final int oplus_verification_code_textview = 201457774;
    
    public static final int oppo_data_low_full_message_1_1_id = 201457779;
    
    public static final int oppo_data_low_full_message_2_id = 201457780;
    
    public static final int oppo_data_low_full_message_3_id = 201457781;
    
    public static final int oppo_data_low_full_message_4_id = 201457782;
    
    public static final int oppo_data_low_message_5_id = 201457783;
    
    public static final int oppo_resolver_grid = 201457791;
    
    public static final int oppo_security_content = 201457796;
    
    public static final int oppo_security_remember = 201457797;
    
    public static final int oppo_stream_icon = 201457798;
    
    public static final int oppo_stream_title = 201457799;
    
    public static final int oppo_text1 = 201457802;
    
    public static final int oppo_text2 = 201457803;
    
    public static final int oppo_up_motor_button = 201457805;
    
    public static final int oppo_up_motor_button_container = 201457806;
    
    public static final int oshare_progress = 201457808;
    
    public static final int oshare_user_item = 201457809;
    
    public static final int oshare_user_name = 201457810;
    
    public static final int oshare_user_pic = 201457811;
    
    public static final int oshare_user_status = 201457812;
    
    public static final int overflow_divider = 201457813;
    
    public static final int parentPanel = 201457814;
    
    public static final int permission_prompt = 201457816;
    
    public static final int popup_list_window_item_icon = 201457821;
    
    public static final int popup_list_window_item_title = 201457822;
    
    public static final int remember_cb = 201457829;
    
    public static final int resolver_bottom = 201457830;
    
    public static final int resolver_dialog_dots = 201457831;
    
    public static final int resolver_dialog_menu_view = 201457832;
    
    public static final int resolver_dialog_pager = 201457833;
    
    public static final int resolver_header_transparent = 201457834;
    
    public static final int resolver_item_icon = 201457835;
    
    public static final int resolver_item_name = 201457836;
    
    public static final int resolver_nfc = 201457837;
    
    public static final int resolver_notice_open_oshare = 201457838;
    
    public static final int resolver_open_oshare_Panel = 201457839;
    
    public static final int resolver_open_oshare_summary = 201457840;
    
    public static final int resolver_open_oshare_title = 201457841;
    
    public static final int resolver_open_wifi_bluetooth = 201457842;
    
    public static final int resolver_open_wifi_bluetooth_title = 201457843;
    
    public static final int resolver_oshare_Panel = 201457844;
    
    public static final int resolver_oshare_icon = 201457845;
    
    public static final int resolver_oshare_open = 201457846;
    
    public static final int resolver_oshareing_Panel = 201457847;
    
    public static final int resolver_oshareing_RecyclerView = 201457848;
    
    public static final int resolver_oshareing_text = 201457849;
    
    public static final int resolver_scroll_panel = 201457850;
    
    public static final int resolver_tabhost = 201457851;
    
    public static final int right_icon = 201457853;
    
    public static final int save_panel = 201457855;
    
    public static final int status = 201457864;
    
    public static final int status_bar_latest_event_content = 201457865;
    
    public static final int summary_text2 = 201457869;
    
    public static final int text2 = 201457871;
    
    public static final int text_shadow = 201457872;
    
    public static final int time = 201457873;
    
    public static final int titleDivider = 201457875;
  }
  
  public static final class integer {
    public static final int android_config_maxResolverActivityColumns = 202178560;
    
    public static final int android_config_safe_media_volume_index = 202178563;
    
    public static final int color_config_bottom_gesture_area_height = 202178571;
    
    public static final int color_config_gesture_area_height = 202178561;
    
    public static final int color_config_gesture_area_height_screenassistant = 202178562;
    
    public static final int color_config_side_gesture_area_width = 202178573;
    
    public static final int color_config_top_gesture_area_height = 202178572;
    
    public static final int config_elevenbit_screenBrightnessSettingMaximum = 202178569;
    
    public static final int config_elevenbit_screenBrightnessSettingMinimum = 202178570;
    
    public static final int config_faceMaxTemplatesPerUser = 202178574;
    
    public static final int config_longPressOnHomeBehavior_exp = 202178575;
    
    public static final int config_tenbit_screenBrightnessSettingMaximum = 202178576;
    
    public static final int config_tenbit_screenBrightnessSettingMinimum = 202178577;
    
    public static final int oplus_animation_time_flashing = 202178578;
    
    public static final int oplus_animation_time_loop = 202178579;
    
    public static final int oplus_animation_time_moment = 202178580;
    
    public static final int oplus_animation_time_move = 202178581;
    
    public static final int oplus_animation_time_move_fast = 202178582;
    
    public static final int oplus_animation_time_move_slow = 202178583;
    
    public static final int oplus_animation_time_move_veryfast = 202178584;
    
    public static final int oplus_animation_time_move_veryslow = 202178585;
    
    public static final int oppo_config_gestureInvalidateDuration = 202178587;
    
    public static final int oppo_config_gestureInvalidateFisrtFrameDelay = 202178588;
    
    public static final int oppo_config_highBatteryVoltageWarningLevel = 202178589;
    
    public static final int oppo_config_highChargerVoltageWarningLevel = 202178590;
    
    public static final int oppo_config_lowBatteryVoltageWarningLevel = 202178591;
    
    public static final int oppo_config_lowChargerVoltageWarningLevel = 202178592;
    
    public static final int oppo_config_tomorrowLowBatteryWarningLevel = 202178593;
    
    public static final int oppo_roll_cover_render_delay = 202178598;
    
    public static final int oppo_scene_preview_render_delay = 202178599;
    
    public static final int oppo_scene_preview_track_scroll_fast_slop = 202178600;
    
    public static final int oppo_screen_rotation_alpha_duration_short = 202178602;
    
    public static final int oppo_screen_rotation_total_duration = 202178603;
    
    public static final int oppo_smartwake_drawable_col = 202178604;
    
    public static final int oppo_smartwake_drawable_row = 202178605;
  }
  
  public static final class interpolator {
    public static final int oplus_curve_opacity_inout = 202375185;
    
    public static final int oplus_decelerate_cubic = 202375186;
    
    public static final int oplus_decelerate_quint = 202375187;
    
    public static final int oppo_screen_rotation_alpha_interpolator = 202375210;
    
    public static final int oppo_screen_rotation_rotate_interpolator = 202375211;
  }
  
  public static final class layout {
    public static final int alert_dialog_horizontal_button_panel = 202440704;
    
    public static final int alert_dialog_vertical_button_panel = 202440705;
    
    public static final int android_global_actions_item = 202440706;
    
    public static final int application_full_screen = 202440708;
    
    public static final int application_full_screen_land_left = 202440709;
    
    public static final int application_full_screen_land_right = 202440710;
    
    public static final int color_application_full_screen_dialog_layout = 202440711;
    
    public static final int color_bottom_alert_dialog = 202440712;
    
    public static final int color_center_alert_dialog = 202440713;
    
    public static final int color_global_drag_text_shadow = 202440714;
    
    public static final int color_list_dialog = 202440715;
    
    public static final int color_list_dialog_item = 202440716;
    
    public static final int color_select_dialog_delete_item = 202440717;
    
    public static final int color_starting_window_title = 202440718;
    
    public static final int decor_zoom = 202440719;
    
    public static final int oplus_chooser_action_nearby = 202440721;
    
    public static final int oplus_chooser_grid_preview_file = 202440722;
    
    public static final int oplus_chooser_grid_preview_image = 202440723;
    
    public static final int oplus_chooser_grid_preview_text = 202440724;
    
    public static final int oplus_chooser_list_per_profile = 202440725;
    
    public static final int oplus_inputmethod_switcher_anchor = 202440726;
    
    public static final int oplus_inputmethod_switcher_bottom_buttons = 202440727;
    
    public static final int oplus_inputmethod_switcher_item = 202440728;
    
    public static final int oplus_page_indicator_dot_layout = 202440729;
    
    public static final int oplus_popup_list_window_item = 202440730;
    
    public static final int oplus_popup_list_window_layout = 202440731;
    
    public static final int oplus_resolver_grid = 202440736;
    
    public static final int oplus_resolver_grid_item = 202440737;
    
    public static final int oplus_resolver_list_per_profile = 202440738;
    
    public static final int oplus_resolver_oshare_item = 202440742;
    
    public static final int oplus_verification_code_window_content = 202440745;
    
    public static final int oppo_floating_popup_container = 202440748;
    
    public static final int oppo_floating_popup_menu_button = 202440749;
    
    public static final int oppo_floating_popup_overflow_button = 202440750;
    
    public static final int oppo_input_method_switch_dialog_title = 202440751;
    
    public static final int oppo_permission_choice = 202440752;
    
    public static final int oppo_select_dialog = 202440753;
    
    public static final int oppo_select_dialog_item = 202440754;
    
    public static final int oppo_select_dialog_multichoice = 202440755;
    
    public static final int oppo_status_bar_latest_event_ticker = 202440757;
    
    public static final int oppo_status_bar_latest_event_ticker_large_icon = 202440758;
    
    public static final int oppo_storage_data_monitor_dialog_view = 202440759;
    
    public static final int oppo_storage_data_monitor_dialog_view_no_curve = 202440760;
    
    public static final int oppo_up_motor_view = 202440761;
  }
  
  public static final class plurals {
    public static final int oppo_tethered_notification_title2_plurals = 202506240;
    
    public static final int oppo_tethered_notification_title2_sharing_plurals = 202506241;
    
    public static final int tethered_cellular_connected_sta_message = 202506242;
    
    public static final int tethered_default_connected_sta_message = 202506243;
    
    public static final int tethered_wifi_connected_sta_message = 202506244;
  }
  
  public static final class raw {
    public static final int android_fallbackring = 202244096;
  }
  
  public static final class string {
    public static final int aerr_ok = 201588748;
    
    public static final int android_action_bar_up_description = 201588830;
    
    public static final int android_chooseUsbActivity = 201588754;
    
    public static final int android_common_last_name_prefixes = 201588751;
    
    public static final int android_common_name_conjunctions = 201588753;
    
    public static final int android_common_name_prefixes = 201588750;
    
    public static final int android_common_name_suffixes = 201588752;
    
    public static final int android_config_bodyFontFamily = 201588835;
    
    public static final int android_config_bodyFontFamilyMedium = 201588836;
    
    public static final int android_config_defaultDreamComponent = 201588755;
    
    public static final int android_config_dreamsDefaultComponent = 201588777;
    
    public static final int android_config_headlineFontFamily = 201588834;
    
    public static final int android_config_headlineFontFamilyMedium = 201588825;
    
    public static final int android_config_headlineFontFeatureSettings = 201588831;
    
    public static final int android_ext_media_badremoval_notification_message = 201588756;
    
    public static final int android_ext_media_badremoval_notification_title = 201588757;
    
    public static final int android_ext_media_checking_notification_message = 201588758;
    
    public static final int android_ext_media_checking_notification_title = 201588759;
    
    public static final int android_ext_media_nofs_notification_message = 201588760;
    
    public static final int android_ext_media_nofs_notification_title = 201588761;
    
    public static final int android_ext_media_nomedia_notification_message = 201588762;
    
    public static final int android_ext_media_nomedia_notification_title = 201588763;
    
    public static final int android_ext_media_safe_unmount_notification_message = 201588764;
    
    public static final int android_ext_media_safe_unmount_notification_title = 201588765;
    
    public static final int android_ext_media_unmountable_notification_message = 201588766;
    
    public static final int android_ext_media_unmountable_notification_title = 201588767;
    
    public static final int android_fast_scroll_alphabet = 201588747;
    
    public static final int android_global_action_logout = 201588826;
    
    public static final int android_ime_action_send = 201588829;
    
    public static final int android_issue_crash = 201588783;
    
    public static final int android_lockscreen_access_pattern_cell_added = 201588784;
    
    public static final int android_lockscreen_access_pattern_cell_added_verbose = 201588785;
    
    public static final int android_lockscreen_access_pattern_cleared = 201588786;
    
    public static final int android_lockscreen_access_pattern_detected = 201588787;
    
    public static final int android_lockscreen_access_pattern_start = 201588788;
    
    public static final int android_lockscreen_emergency_call = 201588789;
    
    public static final int android_lockscreen_return_to_call = 201588790;
    
    public static final int android_media_route_controller_disconnect = 201588828;
    
    public static final int android_notification_header_divider_symbol = 201588827;
    
    public static final int android_power_off = 201588744;
    
    public static final int android_report = 201588745;
    
    public static final int android_ringtone_silent = 201588743;
    
    public static final int android_ringtone_unknown = 201588742;
    
    public static final int android_safe_media_volume_warning = 201588791;
    
    public static final int android_share = 201588768;
    
    public static final int android_ssl_certificate = 201588746;
    
    public static final int android_start_title_string = 201588837;
    
    public static final int android_system_ui_date_pattern = 201588824;
    
    public static final int android_usb_storage_message = 201588769;
    
    public static final int android_usb_storage_notification_message = 201588770;
    
    public static final int android_usb_storage_notification_title = 201588771;
    
    public static final int android_usb_storage_stop_message = 201588772;
    
    public static final int android_usb_storage_stop_notification_message = 201588773;
    
    public static final int android_usb_storage_stop_notification_title = 201588774;
    
    public static final int android_usb_storage_stop_title = 201588775;
    
    public static final int android_usb_storage_title = 201588776;
    
    public static final int android_volume_alarm = 201588792;
    
    public static final int android_volume_icon_description_bluetooth = 201588793;
    
    public static final int android_volume_icon_description_incall = 201588794;
    
    public static final int android_volume_icon_description_media = 201588795;
    
    public static final int android_volume_icon_description_notification = 201588796;
    
    public static final int android_volume_icon_description_ringer = 201588797;
    
    public static final int android_volume_unknown = 201588832;
    
    public static final int available_good_ssid = 201588798;
    
    public static final int available_good_ssid_should_selected = 201588799;
    
    public static final int available_ssid = 201588800;
    
    public static final int available_ssid_selected = 201588801;
    
    public static final int available_ssid_switch_data = 201588802;
    
    public static final int color_app_split = 201588804;
    
    public static final int color_application_full_screen_dialog_button_negative_text = 201588805;
    
    public static final int color_application_full_screen_dialog_button_positive_text = 201588806;
    
    public static final int color_application_full_screen_dialog_title = 201588809;
    
    public static final int color_application_full_screen_text = 201588810;
    
    public static final int color_cloneapp_choose_title = 201588812;
    
    public static final int color_global_action_force_reboot_hint = 201588813;
    
    public static final int color_global_action_landscape_reboot_text = 201588814;
    
    public static final int color_global_action_landscape_shutdown_text = 201588815;
    
    public static final int color_global_action_reboot_text_on_bar = 201588816;
    
    public static final int color_global_action_shut_down_access = 201588817;
    
    public static final int color_global_action_shutdown_text_on_bar = 201588818;
    
    public static final int color_inputmethod_switcher_changing = 201588819;
    
    public static final int color_inputmethod_switcher_more_settings = 201588820;
    
    public static final int color_loading_view_access_string = 201588822;
    
    public static final int color_oppo_set_default_sms_mms_toast = 201588778;
    
    public static final int color_oppo_set_default_sms_mms_toast_ok = 201588779;
    
    public static final int color_oppo_set_default_sms_mms_toast_title = 201588780;
    
    public static final int color_oshare_state_busy = 201588843;
    
    public static final int color_oshare_state_cancel = 201588844;
    
    public static final int color_oshare_state_cancel_wait = 201588845;
    
    public static final int color_oshare_state_ready = 201588846;
    
    public static final int color_oshare_state_reject = 201588847;
    
    public static final int color_oshare_state_succes = 201588848;
    
    public static final int color_oshare_state_transit_failed = 201588849;
    
    public static final int color_oshare_state_transit_timeout = 201588850;
    
    public static final int color_oshare_state_transiting = 201588851;
    
    public static final int color_oshare_state_wait = 201588852;
    
    public static final int color_oshare_state_wait_new = 201588853;
    
    public static final int color_resolver_cant_access_personal_apps = 201588854;
    
    public static final int color_resolver_cant_access_work_apps = 201588855;
    
    public static final int color_resolver_cant_share_open = 201588856;
    
    public static final int color_resolver_cant_share_open_dialog_message = 201588857;
    
    public static final int color_resolver_cant_share_with_personal_apps = 201588858;
    
    public static final int color_resolver_cant_share_with_work_apps = 201588859;
    
    public static final int color_resolver_drag2share_no_app = 201588860;
    
    public static final int color_resolver_more = 201588861;
    
    public static final int color_resolver_no_apps_available_share = 201588862;
    
    public static final int color_resolver_no_apps_available_view = 201588863;
    
    public static final int color_resolver_turn_on_work_apps = 201588868;
    
    public static final int color_talkback_watermark_guide = 201588925;
    
    public static final int color_talkback_watermark_status = 201588926;
    
    public static final int color_user_switching_message = 201588927;
    
    public static final int config_nlp_packageName_amap = 201588928;
    
    public static final int config_nlp_packageName_baidu = 201588929;
    
    public static final int config_nlp_packageName_tencent = 201588930;
    
    public static final int config_osNetworkLocationProviderPackageName = 201588931;
    
    public static final int config_regionNetworkLocationProviderPackageName = 201588932;
    
    public static final int configure_input_methods = 201588933;
    
    public static final int confirm_remove_wifi = 201588934;
    
    public static final int daily_data_limit_disconnect_body = 201588935;
    
    public static final int daily_data_limit_remind_only_body = 201588936;
    
    public static final int daily_data_limit_title = 201588937;
    
    public static final int daily_data_warning_body = 201588938;
    
    public static final int daily_data_warning_title = 201588939;
    
    public static final int developer_preview_hint = 201588942;
    
    public static final int developer_preview_title = 201588943;
    
    public static final int face_name_template = 201588966;
    
    public static final int gr_abandon = 201588967;
    
    public static final int gr_cancel = 201588968;
    
    public static final int gr_do_down = 201588969;
    
    public static final int gr_do_down_down = 201588970;
    
    public static final int gr_down_tip_content = 201588971;
    
    public static final int gr_down_tip_content_down = 201588972;
    
    public static final int gr_exception = 201588973;
    
    public static final int gr_exception_down = 201588974;
    
    public static final int gr_exception_talkback_install = 201588975;
    
    public static final int gr_network = 201588976;
    
    public static final int gr_never_remind = 201588977;
    
    public static final int gr_no_oppo_roam_tip = 201588978;
    
    public static final int gr_not_access_tip = 201588979;
    
    public static final int gr_ok = 201588980;
    
    public static final int gr_oppo_roam = 201588981;
    
    public static final int gr_oppo_roam_tip = 201588982;
    
    public static final int gr_reinstall_progress_tip_content = 201588983;
    
    public static final int gr_space = 201588984;
    
    public static final int gr_succ = 201588985;
    
    public static final int gr_succ_down = 201588986;
    
    public static final int gr_talkback_install_progress_tip_content = 201588987;
    
    public static final int gr_tip_content = 201588988;
    
    public static final int gr_tip_content_down = 201588989;
    
    public static final int gr_tip_installing_gr = 201588990;
    
    public static final int gr_tip_title = 201588991;
    
    public static final int hotspot_consumed_traffic_message = 201588992;
    
    public static final int icon_mask_shape_circle = 201588993;
    
    public static final int icon_mask_shape_leaf = 201588994;
    
    public static final int icon_mask_shape_octagon = 201588995;
    
    public static final int icon_mask_shape_sticker = 201588996;
    
    public static final int icon_mask_theme_pebble = 201588997;
    
    public static final int icon_mask_theme_rectangle = 201588998;
    
    public static final int know_unavailable_ssid = 201589010;
    
    public static final int mobile_data_title = 201589017;
    
    public static final int more_time_download = 201589018;
    
    public static final int most_time_download = 201589019;
    
    public static final int not_allow_running_process = 201589020;
    
    public static final int oplus_selecttext = 201589021;
    
    public static final int oplus_server_url_eu1 = 201589022;
    
    public static final int oplus_server_url_eu2 = 201589023;
    
    public static final int oplus_text_copied = 201589024;
    
    public static final int oplus_text_cut = 201589025;
    
    public static final int oppo_allow_text = 201589026;
    
    public static final int oppo_cancel_button_text = 201589028;
    
    public static final int oppo_clear_data_cancel = 201589029;
    
    public static final int oppo_clear_data_message = 201589030;
    
    public static final int oppo_clear_data_ok = 201589031;
    
    public static final int oppo_clear_data_title = 201589032;
    
    public static final int oppo_daily_data_limit_disconnect_body = 201589033;
    
    public static final int oppo_daily_data_limit_remind_only_body = 201589034;
    
    public static final int oppo_daily_data_limit_title = 201589035;
    
    public static final int oppo_daily_data_warning_body = 201589036;
    
    public static final int oppo_daily_data_warning_title = 201589037;
    
    public static final int oppo_data_critical_low_info = 201589038;
    
    public static final int oppo_data_critical_low_title = 201589039;
    
    public static final int oppo_data_full_message_no_sd = 201589042;
    
    public static final int oppo_data_full_message_with_sd = 201589043;
    
    public static final int oppo_data_full_title = 201589044;
    
    public static final int oppo_data_low_full_message_1 = 201589045;
    
    public static final int oppo_data_low_full_message_1_1 = 201589046;
    
    public static final int oppo_data_low_full_message_1_2 = 201589047;
    
    public static final int oppo_data_low_full_message_2 = 201589048;
    
    public static final int oppo_data_low_full_message_3 = 201589049;
    
    public static final int oppo_data_low_full_message_4 = 201589050;
    
    public static final int oppo_data_low_message_5 = 201589051;
    
    public static final int oppo_data_low_title = 201589052;
    
    public static final int oppo_data_manager_button_text = 201589053;
    
    public static final int oppo_data_not_enough_info = 201589056;
    
    public static final int oppo_data_not_enough_sec_info = 201589057;
    
    public static final int oppo_data_not_enough_title = 201589058;
    
    public static final int oppo_data_notification_content = 201589059;
    
    public static final int oppo_data_notification_full_title = 201589060;
    
    public static final int oppo_data_notification_info = 201589061;
    
    public static final int oppo_data_notification_low_title = 201589062;
    
    public static final int oppo_data_space_monitor_curve = 201589063;
    
    public static final int oppo_data_used_space = 201589064;
    
    public static final int oppo_disable_bottom_key_mode_off = 201589065;
    
    public static final int oppo_disable_bottom_key_mode_toast = 201589066;
    
    public static final int oppo_dual_sta_first_take_effect_reminder = 201589067;
    
    public static final int oppo_dual_sta_first_take_effect_title = 201589068;
    
    public static final int oppo_dual_sta_sign_in_reminder = 201589069;
    
    public static final int oppo_dual_sta_sign_in_title = 201589070;
    
    public static final int oppo_dual_sta_taking_effect = 201589071;
    
    public static final int oppo_flip_frame_step = 201589072;
    
    public static final int oppo_lockscreen_carrier_default = 201589075;
    
    public static final int oppo_modify_pkg_perms = 201589076;
    
    public static final int oppo_modify_pkg_perms_detail = 201589077;
    
    public static final int oppo_motorcalib_dialog_enable = 201589079;
    
    public static final int oppo_motorcalib_dialog_info = 201589080;
    
    public static final int oppo_motorcontrol_autoflash_toast_info = 201589081;
    
    public static final int oppo_motorcontrol_dialog_cancel = 201589082;
    
    public static final int oppo_motorcontrol_dialog_default = 201589083;
    
    public static final int oppo_motorcontrol_dialog_enable = 201589084;
    
    public static final int oppo_motorcontrol_dialog_info = 201589085;
    
    public static final int oppo_motorcontrol_uplocked_camera_info = 201589086;
    
    public static final int oppo_motorcontrol_uplocked_enable = 201589087;
    
    public static final int oppo_motorcontrol_uplocked_flash_info = 201589088;
    
    public static final int oppo_move_button_text = 201589089;
    
    public static final int oppo_permission_alert_prompt = 201589090;
    
    public static final int oppo_permission_charge_sms = 201589091;
    
    public static final int oppo_permission_charge_sms_prompt = 201589092;
    
    public static final int oppo_permission_security_label = 201589094;
    
    public static final int oppo_permission_security_title = 201589095;
    
    public static final int oppo_permission_tencent = 201589097;
    
    public static final int oppo_reject_text = 201589117;
    
    public static final int oppo_scene_preview_track_scroll_fast_step = 201589122;
    
    public static final int oppo_sd_clean_button_text = 201589123;
    
    public static final int oppo_sd_not_enough_info = 201589124;
    
    public static final int oppo_sd_not_enough_title = 201589125;
    
    public static final int oppo_sd_notification_info = 201589126;
    
    public static final int oppo_sla_cancel = 201589127;
    
    public static final int oppo_sla_go_and_set = 201589128;
    
    public static final int oppo_sla_optimize = 201589129;
    
    public static final int oppo_sla_reminder = 201589130;
    
    public static final int oppo_sla_reminder_title = 201589131;
    
    public static final int oppo_task_finish_phone_low_info = 201589132;
    
    public static final int oppo_task_finish_sd_low_info = 201589133;
    
    public static final int oppo_temp_fullscreen_message = 201589134;
    
    public static final int oppo_tethered_notification_title1 = 201589135;
    
    public static final int oppo_tethered_notification_title1_sharing = 201589136;
    
    public static final int oppo_tethered_notification_title2 = 201589137;
    
    public static final int oppo_tethered_notification_title2_sharing = 201589138;
    
    public static final int oppo_tethered_notification_title3 = 201589139;
    
    public static final int oppo_text_toolbar_overflow_title = 201589140;
    
    public static final int oppo_usb_charge = 201589142;
    
    public static final int oppo_usb_mtp = 201589143;
    
    public static final int oppo_usb_selection = 201589144;
    
    public static final int oppo_usb_storage = 201589145;
    
    public static final int oppo_volume_system_description = 201589147;
    
    public static final int oppo_whichApplication = 201589149;
    
    public static final int oppo_wifi_p2p_frequency_conflict_message = 201589150;
    
    public static final int oppo_wifi_tether_configure_ssid_default = 201588739;
    
    public static final int oppo_wlan_share_occupy = 201589151;
    
    public static final int optimizing_apps_progress = 201589152;
    
    public static final int private_dns_cancel = 201589153;
    
    public static final int private_dns_close = 201589154;
    
    public static final int private_dns_error_dialog_title = 201589155;
    
    public static final int private_dns_notification_subtitle = 201589156;
    
    public static final int private_dns_notification_title = 201589157;
    
    public static final int private_dns_turn_off = 201589158;
    
    public static final int redtea_virtul_card = 201589162;
    
    public static final int remove_wifi = 201589163;
    
    public static final int super_hotspot_negative_text = 201589167;
    
    public static final int super_hotspot_network_switch_message = 201589168;
    
    public static final int super_hotspot_network_switch_toast = 201589169;
    
    public static final int super_hotspot_positive_text = 201589170;
    
    public static final int tethered_cellular_started_message = 201589172;
    
    public static final int tethered_notification_setup_message = 201589173;
    
    public static final int tethered_wifi_started_message = 201589174;
    
    public static final int third_app_dexopt = 201589179;
    
    public static final int third_app_uninstall = 201589180;
    
    public static final int torch_protect_alert_neutral_close = 201589181;
    
    public static final int torch_protect_alert_neutral_understood = 201589182;
    
    public static final int torch_protect_high_temp_alert_flashlight_closed_now = 201589183;
    
    public static final int torch_protect_high_temp_alert_flashlight_closed_title = 201589184;
    
    public static final int torch_protect_high_temp_alert_flashlight_title = 201589185;
    
    public static final int torch_protect_high_temp_alert_title = 201589186;
    
    public static final int torch_protect_high_temp_notification = 201589187;
    
    public static final int torch_protect_notification_channel_name = 201589188;
    
    public static final int torch_protect_turned_off_alert_title = 201589189;
    
    public static final int type_issue_Kernelpanic = 201589190;
    
    public static final int type_issue_adsp_crash = 201589191;
    
    public static final int type_issue_display_exception = 201589192;
    
    public static final int type_issue_inputmethod_fail = 201589193;
    
    public static final int type_issue_install_fail = 201589194;
    
    public static final int type_issue_launch_activity = 201589195;
    
    public static final int type_issue_modernreboot = 201589196;
    
    public static final int type_issue_reboot = 201589197;
    
    public static final int type_issue_reboot_blocked = 201589198;
    
    public static final int type_issue_record_fail = 201589199;
    
    public static final int type_issue_shutdown = 201589200;
    
    public static final int type_issue_skip_frames = 201589201;
    
    public static final int type_issue_sound_exception = 201589202;
    
    public static final int type_issue_spmi = 201589203;
    
    public static final int type_issue_symbol_version_disagree = 201589204;
    
    public static final int type_issue_system_server_blocked = 201589205;
    
    public static final int type_issue_unknown = 201589206;
    
    public static final int type_issue_unknown_reboot = 201589207;
    
    public static final int type_issue_venus_crash = 201589208;
    
    public static final int type_issue_wcn_crash = 201589209;
    
    public static final int type_wdi_exception = 201589210;
    
    public static final int type_wifi_connect_failed = 201589211;
    
    public static final int unavailable_ssid = 201589212;
    
    public static final int unavailable_ssid_selected = 201589213;
    
    public static final int unavailable_ssid_switch_data = 201589214;
    
    public static final int virtual_display_added_limit_refresh_rate = 201589215;
    
    public static final int weixin_game_title = 201589216;
    
    public static final int wifi_assistant_allow = 201589217;
    
    public static final int wifi_assistant_deny = 201589218;
    
    public static final int wifi_enhance_know = 201589219;
    
    public static final int wifi_enhance_unvalid = 201589220;
    
    public static final int wifi_hotspot_closed = 201589221;
    
    public static final int wifi_hotspot_notificaiton_channel = 201589222;
    
    public static final int wifi_hotspot_traffic_consumption = 201589223;
    
    public static final int wifi_hotspot_traffic_limitation = 201589224;
    
    public static final int wifi_network_private_dns_error = 201589225;
    
    public static final int wifi_p2p_connect = 201589226;
    
    public static final int wifi_p2p_dialog_default_title = 201589227;
    
    public static final int wifi_p2p_failed_dialog_title = 201589228;
    
    public static final int wifi_p2p_message = 201589229;
    
    public static final int wifi_p2p_ok = 201589230;
    
    public static final int wlan_assistant_select = 201589231;
    
    public static final int wlan_switch_data = 201589232;
    
    public static final int wlan_switch_data_checkbox = 201589233;
    
    public static final int wlan_switch_data_message = 201589234;
    
    public static final int zz_critical_log_plugin_625 = 201589235;
    
    public static final int zz_critical_log_plugout_626 = 201589236;
    
    public static final int zz_oppo_critical_log_10 = 201589237;
    
    public static final int zz_oppo_critical_log_11 = 201589247;
    
    public static final int zz_oppo_critical_log_110 = 201589248;
    
    public static final int zz_oppo_critical_log_111 = 201589249;
    
    public static final int zz_oppo_critical_log_112 = 201589250;
    
    public static final int zz_oppo_critical_log_113 = 201589251;
    
    public static final int zz_oppo_critical_log_12 = 201589258;
    
    public static final int zz_oppo_critical_log_13 = 201589268;
    
    public static final int zz_oppo_critical_log_14 = 201589277;
    
    public static final int zz_oppo_critical_log_15 = 201589286;
    
    public static final int zz_oppo_critical_log_16 = 201589293;
    
    public static final int zz_oppo_critical_log_160 = 201589294;
    
    public static final int zz_oppo_critical_log_161 = 201589295;
    
    public static final int zz_oppo_critical_log_17 = 201589300;
    
    public static final int zz_oppo_critical_log_18 = 201589301;
    
    public static final int zz_oppo_critical_log_19 = 201589304;
    
    public static final int zz_oppo_critical_log_20 = 201589305;
    
    public static final int zz_oppo_critical_log_21 = 201589306;
    
    public static final int zz_oppo_critical_log_210 = 201589307;
    
    public static final int zz_oppo_critical_log_211 = 201589308;
    
    public static final int zz_oppo_critical_log_212 = 201589309;
    
    public static final int zz_oppo_critical_log_260 = 201589325;
    
    public static final int zz_oppo_critical_log_261 = 201589326;
    
    public static final int zz_oppo_critical_log_262 = 201589327;
    
    public static final int zz_oppo_critical_log_60 = 201589381;
    
    public static final int zz_oppo_critical_log_61 = 201589382;
    
    public static final int zz_oppo_critical_log_62 = 201589383;
    
    public static final int zz_oppo_critical_log_63 = 201589384;
    
    public static final int zz_oppo_critical_log_64 = 201589385;
    
    public static final int zz_oppo_critical_log_65 = 201589386;
    
    public static final int zz_oppo_critical_log_66 = 201589387;
    
    public static final int zz_oppo_critical_log_67 = 201589388;
    
    public static final int zz_oppo_critical_log_68 = 201589389;
    
    public static final int zz_oppo_critical_log_70 = 201589390;
    
    public static final int zz_oppo_critical_log_71 = 201589391;
    
    public static final int zz_oppo_critical_log_72 = 201589392;
    
    public static final int zz_oppo_critical_log_73 = 201589393;
    
    public static final int zz_oppo_critical_log_74 = 201589394;
    
    public static final int zz_oppo_critical_log_75 = 201589395;
    
    public static final int zz_oppo_critical_log_76 = 201589396;
    
    public static final int zz_oppo_critical_log_77 = 201589397;
    
    public static final int zz_oppo_critical_log_78 = 201589398;
    
    public static final int zzz_engineeringmode_warning_message = 201589420;
    
    public static final int zzz_engineeringmode_warning_title = 201589421;
  }
  
  public static final class style {
    public static final int AlertDialog = 201523225;
    
    public static final int Animation_Dialog = 201523230;
    
    public static final int Animation_Dialog_Alpha = 201523231;
    
    public static final int Animation_Dialog_Resolver = 201523232;
    
    public static final int Animation_OPPO_Dialog = 201523235;
    
    public static final int Animation_OPPO_Dialog_Alpha = 201523236;
    
    public static final int Animation_VolumePanel = 201523239;
    
    public static final int DefaultDialogItemTextStyle = 201523242;
    
    public static final int DialogTextAppearance_Content = 201523244;
    
    public static final int DialogTextAppearance_Title = 201523245;
    
    public static final int PermissionControllerCategoryTitle = 201523218;
    
    public static final int PermissionControllerSettings = 201523217;
    
    public static final int TextAppearance_Android_DeviceDefault_Body2 = 201523204;
    
    public static final int TextAppearance_Android_DeviceDefault_Notification_Info = 201523221;
    
    public static final int TextAppearance_Android_DeviceDefault_Notification_Title = 201523220;
    
    public static final int TextAppearance_Android_DeviceDefault_Subhead = 201523205;
    
    public static final int TextAppearance_Android_StatusBar = 201523203;
    
    public static final int TextAppearance_Small = 201523266;
    
    public static final int Theme = 201523202;
    
    public static final int ThemeOverlay_Android_DeviceDefault_Accent = 201523211;
    
    public static final int ThemeOverlay_Android_DeviceDefault_Accent_Light = 201523212;
    
    public static final int Theme_Android_DeviceDefault_QuickSettings = 201523200;
    
    public static final int Theme_Android_DeviceDefault_QuickSettings_Dialog = 201523201;
    
    public static final int Theme_Android_DeviceDefault_Settings_Dialog = 201523219;
    
    public static final int Theme_Dialog = 201523206;
    
    public static final int Theme_Dialog_Alert = 201523207;
    
    public static final int Theme_Dialog_Alert_BootMessage = 201523270;
    
    public static final int Theme_Dialog_Alert_Share = 201523271;
    
    public static final int Theme_OPPO = 201523222;
    
    public static final int Theme_OPPO_Dialog = 201523223;
    
    public static final int Theme_OPPO_Dialog_Alert = 201523224;
    
    public static final int Theme_OPPO_Dialog_Alert_BootMessage = 201523272;
    
    public static final int Theme_OPPO_Dialog_Alert_Share = 201523273;
    
    public static final int Theme_OPPO_Panel_Volume = 201523275;
    
    public static final int Theme_Panel_Volume = 201523277;
    
    public static final int Theme_Realme = 201523213;
    
    public static final int Theme_Realme_Dialog = 201523214;
    
    public static final int Theme_Realme_Dialog_Alert = 201523215;
    
    public static final int Theme_Realme_Dialog_Alert_Share = 201523216;
    
    public static final int Widget_Android_DeviceDefault_Notification_Text = 201523210;
    
    public static final int Widget_Android_DeviceDefault_Toolbar = 201523209;
    
    public static final int Widget_Android_Material_Notification_Text = 201523208;
    
    public static final int Widget_ProgressBar_Horizontal = 201523291;
  }
  
  public static final class styleable {
    public static final int[] AndroidManifestActivityAlias = new int[] { 16842752 };
    
    public static final int AndroidManifestActivityAlias_android_theme = 0;
    
    public static final int[] ApduPatternFilter = new int[] { 201982005, 201982041, 201982085 };
    
    public static final int ApduPatternFilter_apdupattern_mask = 0;
    
    public static final int ApduPatternFilter_description = 1;
    
    public static final int ApduPatternFilter_reference_data = 2;
    
    public static final int[] ApduPatternGroup = new int[] { 201982041 };
    
    public static final int ApduPatternGroup_description = 0;
    
    public static final int[] ApkLock = new int[] { 201982061, 201982062, 201982063, 201982064, 201982065 };
    
    public static final int ApkLock_lockAuthor = 0;
    
    public static final int ApkLock_lockDescription = 1;
    
    public static final int ApkLock_lockThumbleft = 2;
    
    public static final int ApkLock_lockThumbnail = 3;
    
    public static final int ApkLock_lockThumbright = 4;
    
    public static final int[] ColorDialog = new int[] { 201982013 };
    
    public static final int ColorDialog_colorCustomLayoutWidth = 0;
    
    public static final int[] ColorGlobalDragTextShadowContainer = new int[] { 201982035, 201982036, 201982037, 201982038, 201982039, 201982040, 201982051 };
    
    public static final int ColorGlobalDragTextShadowContainer_containerCornerRadius = 0;
    
    public static final int ColorGlobalDragTextShadowContainer_containerDeltaLength = 1;
    
    public static final int ColorGlobalDragTextShadowContainer_containerShadowColor = 2;
    
    public static final int ColorGlobalDragTextShadowContainer_containerShadowRadius = 3;
    
    public static final int ColorGlobalDragTextShadowContainer_deltaX = 4;
    
    public static final int ColorGlobalDragTextShadowContainer_deltaY = 5;
    
    public static final int ColorGlobalDragTextShadowContainer_enable = 6;
    
    public static final int[] ColorLockPatternView = new int[] { 201982017, 201982019, 201982020, 201982022, 201982025 };
    
    public static final int ColorLockPatternView_colorErrorColor = 0;
    
    public static final int ColorLockPatternView_colorOuterCircleMaxAlpha = 1;
    
    public static final int ColorLockPatternView_colorPathColor = 2;
    
    public static final int ColorLockPatternView_colorRegularColor = 3;
    
    public static final int ColorLockPatternView_colorSuccessColor = 4;
    
    public static final int[] ColorMenuItem = new int[] { 201982033, 201982034 };
    
    public static final int ColorMenuItem_colorWarningPointMode = 0;
    
    public static final int ColorMenuItem_colorWarningPointNum = 1;
    
    public static final int[] ColorToast = new int[] { 16842927, 16842994, 201982031 };
    
    public static final int[] ColorToastTheme = new int[] { 201982032 };
    
    public static final int ColorToastTheme_colorToastStyle = 0;
    
    public static final int ColorToast_android_gravity = 0;
    
    public static final int ColorToast_android_layout = 1;
    
    public static final int ColorToast_colorToastOffsetY = 2;
    
    public static final int[] EdgeTriggerView = new int[] { 201982050, 201982060 };
    
    public static final int EdgeTriggerView_edgeWidth = 0;
    
    public static final int EdgeTriggerView_listenEdges = 1;
    
    public static final int[] Mapping = new int[] { 201982042, 201982056, 201982067, 201982086, 201982091 };
    
    public static final int Mapping_detailColumn = 0;
    
    public static final int Mapping_icon = 1;
    
    public static final int Mapping_mimeType = 2;
    
    public static final int Mapping_remoteViews = 3;
    
    public static final int Mapping_summaryColumn = 4;
    
    public static final int[] OplusAlertLinearLayout = new int[] { 201982002, 201982003, 201982004 };
    
    public static final int OplusAlertLinearLayout_alertBackgroundDrawable = 0;
    
    public static final int OplusAlertLinearLayout_alertCornerRadius = 1;
    
    public static final int OplusAlertLinearLayout_alertShadowDrawable = 2;
    
    public static final int[] OplusCircleProgressBar = new int[] { 201982066, 201982079, 201982080, 201982081, 201982082, 201982083, 201982084 };
    
    public static final int OplusCircleProgressBar_max = 0;
    
    public static final int OplusCircleProgressBar_progress = 1;
    
    public static final int OplusCircleProgressBar_progressBarBgCircleColor = 2;
    
    public static final int OplusCircleProgressBar_progressBarColor = 3;
    
    public static final int OplusCircleProgressBar_progressBarHeight = 4;
    
    public static final int OplusCircleProgressBar_progressBarType = 5;
    
    public static final int OplusCircleProgressBar_progressBarWidth = 6;
    
    public static final int[] OplusLoadingView = new int[] { 201981988, 201981989, 201981991, 201981992 };
    
    public static final int OplusLoadingView_OplusLoadingViewColor = 0;
    
    public static final int OplusLoadingView_OplusLoadingViewHeight = 1;
    
    public static final int OplusLoadingView_OplusLoadingViewType = 2;
    
    public static final int OplusLoadingView_OplusLoadingViewWidth = 3;
    
    public static final int[] OplusMaxLinearLayout = new int[] { 201982058, 201982059, 201982077, 201982078 };
    
    public static final int OplusMaxLinearLayout_landscapeMaxHeight = 0;
    
    public static final int OplusMaxLinearLayout_landscapeMaxWidth = 1;
    
    public static final int OplusMaxLinearLayout_portraitMaxHeight = 2;
    
    public static final int OplusMaxLinearLayout_portraitMaxWidth = 3;
    
    public static final int[] OplusPageIndicator = new int[] { 201982043, 201982044, 201982045, 201982046, 201982047, 201982048, 201982049, 201982099 };
    
    public static final int OplusPageIndicator_dotClickable = 0;
    
    public static final int OplusPageIndicator_dotColor = 1;
    
    public static final int OplusPageIndicator_dotCornerRadius = 2;
    
    public static final int OplusPageIndicator_dotIsStrokeStyle = 3;
    
    public static final int OplusPageIndicator_dotSize = 4;
    
    public static final int OplusPageIndicator_dotSpacing = 5;
    
    public static final int OplusPageIndicator_dotStrokeWidth = 6;
    
    public static final int OplusPageIndicator_traceDotColor = 7;
    
    public static final int[] OplusResolverDrawerLayout = new int[] { 201982059, 201982078 };
    
    public static final int OplusResolverDrawerLayout_landscapeMaxWidth = 0;
    
    public static final int OplusResolverDrawerLayout_portraitMaxWidth = 1;
    
    public static final int[] OplusTheme = new int[] { 201982057, 201982096 };
    
    public static final int OplusTheme_isOplusTheme = 0;
    
    public static final int OplusTheme_themeType = 1;
    
    public static final int[] Oppo3DMultiCallView = new int[] { 201982092, 201982093, 201982094, 201982095 };
    
    public static final int Oppo3DMultiCallView_textPositionY1 = 0;
    
    public static final int Oppo3DMultiCallView_textPositionY2 = 1;
    
    public static final int Oppo3DMultiCallView_textPositionY3 = 2;
    
    public static final int Oppo3DMultiCallView_textSize = 3;
    
    public static final int[] Oppo3DRollCoverView = new int[] { 201982100 };
    
    public static final int Oppo3DRollCoverView_translucentMode = 0;
    
    public static final int[] Oppo3DScenePreView = new int[] { 201982006, 201982052, 201982053, 201982088, 201982089, 201982103 };
    
    public static final int Oppo3DScenePreView_centerOffset = 0;
    
    public static final int Oppo3DScenePreView_frameMode = 1;
    
    public static final int Oppo3DScenePreView_frameRadio = 2;
    
    public static final int Oppo3DScenePreView_sceneModelType = 3;
    
    public static final int Oppo3DScenePreView_shaowHeight = 4;
    
    public static final int Oppo3DScenePreView_zoomEnable = 5;
    
    public static final int[] OppoWindow = new int[] { 201982070, 201982075 };
    
    public static final int OppoWindow_oppoAddMaskView = 0;
    
    public static final int OppoWindow_oppoWindowActionBarAndStatusBarOverlay = 1;
    
    public static final int[] RoundFrameLayout = new int[] { 201982087 };
    
    public static final int RoundFrameLayout_rfRadius = 0;
    
    public static final int[] ScenePreView = new int[] { 201982068, 201982090 };
    
    public static final int ScenePreView_operable = 0;
    
    public static final int ScenePreView_singleFrame = 1;
    
    public static final int[] ShutdownView = new int[] { 201392128, 201392129, 201982010, 201982011, 201982012, 201982014, 201982015, 201982016, 201982021, 201982023 };
    
    public static final int ShutdownView_colorCancelNormalDrawable = 2;
    
    public static final int ShutdownView_colorCancelPressedDrawable = 3;
    
    public static final int ShutdownView_colorCancelText = 4;
    
    public static final int ShutdownView_colorEmergencyNormalDrawable = 5;
    
    public static final int ShutdownView_colorEmergencyPressedDrawable = 6;
    
    public static final int ShutdownView_colorEmergencyText = 7;
    
    public static final int ShutdownView_colorLockText = 0;
    
    public static final int ShutdownView_colorManuallyNormalDrawable = 1;
    
    public static final int ShutdownView_colorRebootHint = 8;
    
    public static final int ShutdownView_colorShutdownHint = 9;
    
    public static final int[] SlidePiece = new int[] { 
        201981980, 201981981, 201981982, 201981983, 201981984, 201981993, 201981994, 201981995, 201981996, 201981997, 
        201981998, 201981999, 201982000, 201982001 };
    
    public static final int SlidePiece_ImgViewBottomPadding = 0;
    
    public static final int SlidePiece_ImgViewDrawableRes = 1;
    
    public static final int SlidePiece_ImgViewLeftPadding = 2;
    
    public static final int SlidePiece_ImgViewRightPadding = 3;
    
    public static final int SlidePiece_ImgViewTopPadding = 4;
    
    public static final int SlidePiece_SlidePieceBkgRes = 5;
    
    public static final int SlidePiece_TextOfTextView = 6;
    
    public static final int SlidePiece_TextSizeOfTextView = 7;
    
    public static final int SlidePiece_TextViewBottomPadding = 8;
    
    public static final int SlidePiece_TextViewColor = 9;
    
    public static final int SlidePiece_TextViewLeftPadding = 10;
    
    public static final int SlidePiece_TextViewRightPadding = 11;
    
    public static final int SlidePiece_TextViewShine = 12;
    
    public static final int SlidePiece_TextViewTopPadding = 13;
    
    public static final int[] TextAppearance = new int[] { 16842901, 16842902, 16842903, 16842904 };
    
    public static final int TextAppearance_android_textColor = 3;
    
    public static final int TextAppearance_android_textSize = 0;
    
    public static final int TextAppearance_android_textStyle = 2;
    
    public static final int TextAppearance_android_typeface = 1;
    
    public static final int[] VerticalRollAnimation = new int[] { 201982054, 201982055, 201982097, 201982098 };
    
    public static final int VerticalRollAnimation_fromRollDegrees = 0;
    
    public static final int VerticalRollAnimation_fromYRoll = 1;
    
    public static final int VerticalRollAnimation_toRollDegrees = 2;
    
    public static final int VerticalRollAnimation_toYRoll = 3;
    
    public static final int[] WindowAnimation = new int[] { 201982101, 201982102 };
    
    public static final int WindowAnimation_windowEnterAnimation = 0;
    
    public static final int WindowAnimation_windowExitAnimation = 1;
  }
  
  public static final class xml {
    public static final int android_apns = 202047488;
    
    public static final int color_default_eyeprotect_data = 202047489;
    
    public static final int power_profile_13077 = 202047490;
    
    public static final int power_profile_13097 = 202047491;
    
    public static final int power_profile_14001 = 202047492;
    
    public static final int storage_all_file_access_list = 202047493;
  }
}
