package com.oplus.internal.widget;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

public class OplusToastLayout extends LinearLayout {
  private static final boolean DBG = false;
  
  private static final int HORIZONTAL_MARGIN_DELTA = 8;
  
  private static final int LAYOUT_MIN_HEIGHT_DP = 34;
  
  private static final float PIXEL_OFFSET = 0.5F;
  
  private static final String TAG = "OplusToastLayout";
  
  private static final float TEXT_ADD_SPACING = 0.0F;
  
  private static final float TEXT_MULTI_SPACING = 1.2F;
  
  private static final int TOAST_MARGIN_LEFT = 24;
  
  private final int mTextColor;
  
  private final Rect mTextPadding = new Rect();
  
  private final float mTextSize;
  
  private final Typeface mTypeface;
  
  private final WindowManager mWm;
  
  public OplusToastLayout(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public OplusToastLayout(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    setBackgroundResource(201851285);
    this.mTextColor = getResources().getColor(201719951);
    this.mTextSize = getResources().getDimension(201654541);
    this.mTextPadding.left = getResources().getDimensionPixelSize(201654538);
    this.mTextPadding.top = getResources().getDimensionPixelSize(201654540);
    this.mTextPadding.right = getResources().getDimensionPixelSize(201654539);
    this.mTextPadding.bottom = getResources().getDimensionPixelSize(201654537);
    this.mTypeface = Typeface.create("sans-serif-medium", 0);
    setId(201457771);
    this.mWm = (WindowManager)paramContext.getSystemService("window");
  }
  
  protected void onFinishInflate() {
    super.onFinishInflate();
    TextView textView = (TextView)findViewById(16908299);
    textView.setShadowLayer(0.0F, 0.0F, 0.0F, 0);
    textView.setGravity(17);
    textView.setPadding(this.mTextPadding.left, this.mTextPadding.top, this.mTextPadding.right, this.mTextPadding.bottom);
    textView.setTextColor(this.mTextColor);
    textView.setTextSize(0, this.mTextSize);
    Typeface typeface = this.mTypeface;
    if (typeface != null)
      textView.setTypeface(typeface, 0); 
    textView.setMinHeight(dp2px(34));
    textView.setLineSpacing(0.0F, 1.2F);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    int i = View.MeasureSpec.getSize(paramInt1);
    int j = View.MeasureSpec.getMode(paramInt1);
    Point point = new Point();
    this.mWm.getDefaultDisplay().getRealSize(point);
    int k = point.x - dp2px(24) * 2;
    if (i > k) {
      super.onMeasure(View.MeasureSpec.makeSafeMeasureSpec(k, j), paramInt2);
    } else {
      super.onMeasure(paramInt1, paramInt2);
    } 
  }
  
  private int dp2px(int paramInt) {
    return (int)((getContext().getResources().getDisplayMetrics()).density * paramInt + 0.5F);
  }
}
