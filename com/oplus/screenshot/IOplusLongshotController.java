package com.oplus.screenshot;

public interface IOplusLongshotController {
  default boolean overScrollBy(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, boolean paramBoolean1, int paramInt9, boolean paramBoolean2) {
    return false;
  }
  
  default boolean isLongshotConnected() {
    return false;
  }
  
  default int getOverScrollMode(int paramInt) {
    return paramInt;
  }
  
  boolean findInfo(OplusLongshotViewInfo paramOplusLongshotViewInfo);
}
