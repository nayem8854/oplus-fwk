package com.oplus.screenshot;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusScreenshotManager extends IInterface {
  boolean isLongshotDisabled() throws RemoteException;
  
  boolean isLongshotEnabled() throws RemoteException;
  
  boolean isLongshotMode() throws RemoteException;
  
  boolean isScreenshotEdit() throws RemoteException;
  
  boolean isScreenshotEnabled() throws RemoteException;
  
  boolean isScreenshotMode() throws RemoteException;
  
  boolean isScreenshotSupported() throws RemoteException;
  
  void notifyOverScroll(OplusLongshotEvent paramOplusLongshotEvent) throws RemoteException;
  
  void reportLongshotDumpResult(OplusLongshotDump paramOplusLongshotDump) throws RemoteException;
  
  void setLongshotEnabled(boolean paramBoolean) throws RemoteException;
  
  void setScreenshotEnabled(boolean paramBoolean) throws RemoteException;
  
  void stopLongshot() throws RemoteException;
  
  void takeLongshot(boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void takeScreenshot(Bundle paramBundle) throws RemoteException;
  
  class Default implements IOplusScreenshotManager {
    public void takeScreenshot(Bundle param1Bundle) throws RemoteException {}
    
    public boolean isScreenshotMode() throws RemoteException {
      return false;
    }
    
    public boolean isScreenshotEdit() throws RemoteException {
      return false;
    }
    
    public void takeLongshot(boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public void stopLongshot() throws RemoteException {}
    
    public boolean isLongshotMode() throws RemoteException {
      return false;
    }
    
    public boolean isLongshotDisabled() throws RemoteException {
      return false;
    }
    
    public void reportLongshotDumpResult(OplusLongshotDump param1OplusLongshotDump) throws RemoteException {}
    
    public boolean isScreenshotSupported() throws RemoteException {
      return false;
    }
    
    public void setScreenshotEnabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isScreenshotEnabled() throws RemoteException {
      return false;
    }
    
    public void setLongshotEnabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isLongshotEnabled() throws RemoteException {
      return false;
    }
    
    public void notifyOverScroll(OplusLongshotEvent param1OplusLongshotEvent) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusScreenshotManager {
    private static final String DESCRIPTOR = "com.oplus.screenshot.IOplusScreenshotManager";
    
    static final int TRANSACTION_isLongshotDisabled = 7;
    
    static final int TRANSACTION_isLongshotEnabled = 13;
    
    static final int TRANSACTION_isLongshotMode = 6;
    
    static final int TRANSACTION_isScreenshotEdit = 3;
    
    static final int TRANSACTION_isScreenshotEnabled = 11;
    
    static final int TRANSACTION_isScreenshotMode = 2;
    
    static final int TRANSACTION_isScreenshotSupported = 9;
    
    static final int TRANSACTION_notifyOverScroll = 14;
    
    static final int TRANSACTION_reportLongshotDumpResult = 8;
    
    static final int TRANSACTION_setLongshotEnabled = 12;
    
    static final int TRANSACTION_setScreenshotEnabled = 10;
    
    static final int TRANSACTION_stopLongshot = 5;
    
    static final int TRANSACTION_takeLongshot = 4;
    
    static final int TRANSACTION_takeScreenshot = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.screenshot.IOplusScreenshotManager");
    }
    
    public static IOplusScreenshotManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.screenshot.IOplusScreenshotManager");
      if (iInterface != null && iInterface instanceof IOplusScreenshotManager)
        return (IOplusScreenshotManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 14:
          return "notifyOverScroll";
        case 13:
          return "isLongshotEnabled";
        case 12:
          return "setLongshotEnabled";
        case 11:
          return "isScreenshotEnabled";
        case 10:
          return "setScreenshotEnabled";
        case 9:
          return "isScreenshotSupported";
        case 8:
          return "reportLongshotDumpResult";
        case 7:
          return "isLongshotDisabled";
        case 6:
          return "isLongshotMode";
        case 5:
          return "stopLongshot";
        case 4:
          return "takeLongshot";
        case 3:
          return "isScreenshotEdit";
        case 2:
          return "isScreenshotMode";
        case 1:
          break;
      } 
      return "takeScreenshot";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        boolean bool1 = false, bool2 = false, bool3 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 14:
            param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusScreenshotManager");
            if (param1Parcel1.readInt() != 0) {
              OplusLongshotEvent oplusLongshotEvent = (OplusLongshotEvent)OplusLongshotEvent.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifyOverScroll((OplusLongshotEvent)param1Parcel1);
            return true;
          case 13:
            param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusScreenshotManager");
            bool = isLongshotEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 12:
            param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusScreenshotManager");
            if (param1Parcel1.readInt() != 0)
              bool3 = true; 
            setLongshotEnabled(bool3);
            return true;
          case 11:
            param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusScreenshotManager");
            bool = isScreenshotEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 10:
            param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusScreenshotManager");
            bool3 = bool1;
            if (param1Parcel1.readInt() != 0)
              bool3 = true; 
            setScreenshotEnabled(bool3);
            return true;
          case 9:
            param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusScreenshotManager");
            bool = isScreenshotSupported();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 8:
            param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusScreenshotManager");
            if (param1Parcel1.readInt() != 0) {
              OplusLongshotDump oplusLongshotDump = (OplusLongshotDump)OplusLongshotDump.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            reportLongshotDumpResult((OplusLongshotDump)param1Parcel1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusScreenshotManager");
            bool = isLongshotDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 6:
            param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusScreenshotManager");
            bool = isLongshotMode();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 5:
            param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusScreenshotManager");
            stopLongshot();
            return true;
          case 4:
            param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusScreenshotManager");
            if (param1Parcel1.readInt() != 0) {
              bool3 = true;
            } else {
              bool3 = false;
            } 
            if (param1Parcel1.readInt() != 0)
              bool2 = true; 
            takeLongshot(bool3, bool2);
            return true;
          case 3:
            param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusScreenshotManager");
            bool = isScreenshotEdit();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 2:
            param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusScreenshotManager");
            bool = isScreenshotMode();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusScreenshotManager");
        if (param1Parcel1.readInt() != 0) {
          Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        takeScreenshot((Bundle)param1Parcel1);
        return true;
      } 
      param1Parcel2.writeString("com.oplus.screenshot.IOplusScreenshotManager");
      return true;
    }
    
    private static class Proxy implements IOplusScreenshotManager {
      public static IOplusScreenshotManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.screenshot.IOplusScreenshotManager";
      }
      
      public void takeScreenshot(Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.screenshot.IOplusScreenshotManager");
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusScreenshotManager.Stub.getDefaultImpl() != null) {
            IOplusScreenshotManager.Stub.getDefaultImpl().takeScreenshot(param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean isScreenshotMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.screenshot.IOplusScreenshotManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IOplusScreenshotManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusScreenshotManager.Stub.getDefaultImpl().isScreenshotMode();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isScreenshotEdit() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.screenshot.IOplusScreenshotManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IOplusScreenshotManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusScreenshotManager.Stub.getDefaultImpl().isScreenshotEdit();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void takeLongshot(boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.screenshot.IOplusScreenshotManager");
          boolean bool1 = false;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel.writeInt(bool2);
          boolean bool2 = bool1;
          if (param2Boolean2)
            bool2 = true; 
          parcel.writeInt(bool2);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IOplusScreenshotManager.Stub.getDefaultImpl() != null) {
            IOplusScreenshotManager.Stub.getDefaultImpl().takeLongshot(param2Boolean1, param2Boolean2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void stopLongshot() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.screenshot.IOplusScreenshotManager");
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IOplusScreenshotManager.Stub.getDefaultImpl() != null) {
            IOplusScreenshotManager.Stub.getDefaultImpl().stopLongshot();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean isLongshotMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.screenshot.IOplusScreenshotManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IOplusScreenshotManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusScreenshotManager.Stub.getDefaultImpl().isLongshotMode();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isLongshotDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.screenshot.IOplusScreenshotManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IOplusScreenshotManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusScreenshotManager.Stub.getDefaultImpl().isLongshotDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportLongshotDumpResult(OplusLongshotDump param2OplusLongshotDump) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.screenshot.IOplusScreenshotManager");
          if (param2OplusLongshotDump != null) {
            parcel.writeInt(1);
            param2OplusLongshotDump.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IOplusScreenshotManager.Stub.getDefaultImpl() != null) {
            IOplusScreenshotManager.Stub.getDefaultImpl().reportLongshotDumpResult(param2OplusLongshotDump);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean isScreenshotSupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.screenshot.IOplusScreenshotManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IOplusScreenshotManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusScreenshotManager.Stub.getDefaultImpl().isScreenshotSupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setScreenshotEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.oplus.screenshot.IOplusScreenshotManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(10, parcel, null, 1);
          if (!bool1 && IOplusScreenshotManager.Stub.getDefaultImpl() != null) {
            IOplusScreenshotManager.Stub.getDefaultImpl().setScreenshotEnabled(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean isScreenshotEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.screenshot.IOplusScreenshotManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(11, parcel1, parcel2, 0);
          if (!bool2 && IOplusScreenshotManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusScreenshotManager.Stub.getDefaultImpl().isScreenshotEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setLongshotEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.oplus.screenshot.IOplusScreenshotManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(12, parcel, null, 1);
          if (!bool1 && IOplusScreenshotManager.Stub.getDefaultImpl() != null) {
            IOplusScreenshotManager.Stub.getDefaultImpl().setLongshotEnabled(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean isLongshotEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.screenshot.IOplusScreenshotManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(13, parcel1, parcel2, 0);
          if (!bool2 && IOplusScreenshotManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusScreenshotManager.Stub.getDefaultImpl().isLongshotEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyOverScroll(OplusLongshotEvent param2OplusLongshotEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.screenshot.IOplusScreenshotManager");
          if (param2OplusLongshotEvent != null) {
            parcel.writeInt(1);
            param2OplusLongshotEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(14, parcel, null, 1);
          if (!bool && IOplusScreenshotManager.Stub.getDefaultImpl() != null) {
            IOplusScreenshotManager.Stub.getDefaultImpl().notifyOverScroll(param2OplusLongshotEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusScreenshotManager param1IOplusScreenshotManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusScreenshotManager != null) {
          Proxy.sDefaultImpl = param1IOplusScreenshotManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusScreenshotManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
