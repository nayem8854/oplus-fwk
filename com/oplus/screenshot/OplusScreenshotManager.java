package com.oplus.screenshot;

import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import com.oplus.util.OplusLog;

public final class OplusScreenshotManager {
  private static final boolean DBG = OplusLongshotDump.DBG;
  
  public static final String GLOBAL_ACTION_VISIBLE = "global_action_visible";
  
  public static final String NAVIGATIONBAR_VISIBLE = "navigationbar_visible";
  
  public static final String SCREENSHOT_DIRECTION = "screenshot_direction";
  
  public static final String SCREENSHOT_ORIENTATION = "screenshot_orientation";
  
  public static final String SCREENSHOT_SOURCE = "screenshot_source";
  
  public static final String STATUSBAR_VISIBLE = "statusbar_visible";
  
  private static final String TAG = "LongshotDump";
  
  private static volatile OplusScreenshotManager sInstance = null;
  
  private boolean mIsLaunching = false;
  
  private int mRotation;
  
  private final IOplusScreenshotManager mService;
  
  private OplusScreenshotManager() {
    IBinder iBinder = ServiceManager.getService("color_screenshot");
    this.mService = IOplusScreenshotManager.Stub.asInterface(iBinder);
  }
  
  public static OplusScreenshotManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/screenshot/OplusScreenshotManager.sInstance : Lcom/oplus/screenshot/OplusScreenshotManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/screenshot/OplusScreenshotManager
    //   8: monitorenter
    //   9: getstatic com/oplus/screenshot/OplusScreenshotManager.sInstance : Lcom/oplus/screenshot/OplusScreenshotManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/screenshot/OplusScreenshotManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/screenshot/OplusScreenshotManager.sInstance : Lcom/oplus/screenshot/OplusScreenshotManager;
    //   27: ldc com/oplus/screenshot/OplusScreenshotManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/screenshot/OplusScreenshotManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/screenshot/OplusScreenshotManager.sInstance : Lcom/oplus/screenshot/OplusScreenshotManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #127	-> 0
    //   #128	-> 6
    //   #129	-> 9
    //   #130	-> 15
    //   #132	-> 27
    //   #134	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public static OplusScreenshotManager peekInstance() {
    return sInstance;
  }
  
  public void takeScreenshot(Bundle paramBundle) {
    IOplusScreenshotManager iOplusScreenshotManager = this.mService;
    if (iOplusScreenshotManager != null)
      try {
        iOplusScreenshotManager.takeScreenshot(paramBundle);
      } catch (RemoteException remoteException) {
        boolean bool = DBG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("takeScreenshot : ");
        stringBuilder.append(remoteException.toString());
        OplusLog.e(bool, "LongshotDump", stringBuilder.toString());
      } catch (Exception exception) {
        OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
      }  
  }
  
  public boolean isScreenshotMode() {
    boolean bool1 = false;
    boolean bool = false;
    IOplusScreenshotManager iOplusScreenshotManager = this.mService;
    boolean bool2 = bool1;
    if (iOplusScreenshotManager != null)
      try {
        bool2 = iOplusScreenshotManager.isScreenshotMode();
      } catch (RemoteException remoteException) {
        bool2 = DBG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("isScreenshotMode : ");
        stringBuilder.append(remoteException.toString());
        OplusLog.e(bool2, "LongshotDump", stringBuilder.toString());
        bool2 = bool;
      } catch (Exception exception) {
        OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
        bool2 = bool1;
      }  
    return bool2;
  }
  
  public boolean isScreenshotEdit() {
    boolean bool1 = false;
    boolean bool = false;
    IOplusScreenshotManager iOplusScreenshotManager = this.mService;
    boolean bool2 = bool1;
    if (iOplusScreenshotManager != null)
      try {
        bool2 = iOplusScreenshotManager.isScreenshotEdit();
      } catch (RemoteException remoteException) {
        bool2 = DBG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("isScreenshotEdit : ");
        stringBuilder.append(remoteException.toString());
        OplusLog.e(bool2, "LongshotDump", stringBuilder.toString());
        bool2 = bool;
      } catch (Exception exception) {
        OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
        bool2 = bool1;
      }  
    return bool2;
  }
  
  public void takeLongshot(boolean paramBoolean1, boolean paramBoolean2) {
    IOplusScreenshotManager iOplusScreenshotManager = this.mService;
    if (iOplusScreenshotManager != null)
      try {
        iOplusScreenshotManager.takeLongshot(paramBoolean1, paramBoolean2);
      } catch (RemoteException remoteException) {
        paramBoolean1 = DBG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("takeLongshot : ");
        stringBuilder.append(remoteException.toString());
        OplusLog.e(paramBoolean1, "LongshotDump", stringBuilder.toString());
      } catch (Exception exception) {
        OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
      }  
  }
  
  public void stopLongshot() {
    IOplusScreenshotManager iOplusScreenshotManager = this.mService;
    if (iOplusScreenshotManager != null)
      try {
        iOplusScreenshotManager.stopLongshot();
      } catch (RemoteException remoteException) {
        boolean bool = DBG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("stopLongshot : ");
        stringBuilder.append(remoteException.toString());
        OplusLog.e(bool, "LongshotDump", stringBuilder.toString());
      } catch (Exception exception) {
        OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
      }  
  }
  
  public boolean isLongshotMode() {
    boolean bool1 = false;
    boolean bool = false;
    IOplusScreenshotManager iOplusScreenshotManager = this.mService;
    boolean bool2 = bool1;
    if (iOplusScreenshotManager != null)
      try {
        bool2 = iOplusScreenshotManager.isLongshotMode();
      } catch (RemoteException remoteException) {
        bool2 = DBG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("isLongshotMode : ");
        stringBuilder.append(remoteException.toString());
        OplusLog.e(bool2, "LongshotDump", stringBuilder.toString());
        bool2 = bool;
      } catch (Exception exception) {
        OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
        bool2 = bool1;
      }  
    return bool2;
  }
  
  public boolean isLongshotDisabled() {
    boolean bool1 = false;
    boolean bool = false;
    IOplusScreenshotManager iOplusScreenshotManager = this.mService;
    boolean bool2 = bool1;
    if (iOplusScreenshotManager != null)
      try {
        bool2 = iOplusScreenshotManager.isLongshotDisabled();
      } catch (RemoteException remoteException) {
        bool2 = DBG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("isLongshotDisabled : ");
        stringBuilder.append(remoteException.toString());
        OplusLog.e(bool2, "LongshotDump", stringBuilder.toString());
        bool2 = bool;
      } catch (Exception exception) {
        OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
        bool2 = bool1;
      }  
    return bool2;
  }
  
  public void reportLongshotDumpResult(OplusLongshotDump paramOplusLongshotDump) {
    IOplusScreenshotManager iOplusScreenshotManager = this.mService;
    if (iOplusScreenshotManager != null)
      try {
        iOplusScreenshotManager.reportLongshotDumpResult(paramOplusLongshotDump);
      } catch (RemoteException remoteException) {
        boolean bool = DBG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("reportLongshotDumpResult : ");
        stringBuilder.append(remoteException.toString());
        OplusLog.e(bool, "LongshotDump", stringBuilder.toString());
      } catch (Exception exception) {
        OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
      }  
  }
  
  public boolean isScreenshotSupported() {
    boolean bool1 = true;
    boolean bool = true;
    IOplusScreenshotManager iOplusScreenshotManager = this.mService;
    boolean bool2 = bool1;
    if (iOplusScreenshotManager != null)
      try {
        bool2 = iOplusScreenshotManager.isScreenshotSupported();
      } catch (RemoteException remoteException) {
        bool2 = DBG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("isScreenshotSupported : ");
        stringBuilder.append(remoteException.toString());
        OplusLog.e(bool2, "LongshotDump", stringBuilder.toString());
        bool2 = bool;
      } catch (Exception exception) {
        OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
        bool2 = bool1;
      }  
    return bool2;
  }
  
  public void setScreenshotEnabled(boolean paramBoolean) {
    IOplusScreenshotManager iOplusScreenshotManager = this.mService;
    if (iOplusScreenshotManager != null)
      try {
        iOplusScreenshotManager.setScreenshotEnabled(paramBoolean);
      } catch (RemoteException remoteException) {
        paramBoolean = DBG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("setScreenshotEnabled : ");
        stringBuilder.append(remoteException.toString());
        OplusLog.e(paramBoolean, "LongshotDump", stringBuilder.toString());
      } catch (Exception exception) {
        OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
      }  
  }
  
  public boolean isScreenshotEnabled() {
    boolean bool1 = true;
    boolean bool = true;
    IOplusScreenshotManager iOplusScreenshotManager = this.mService;
    boolean bool2 = bool1;
    if (iOplusScreenshotManager != null)
      try {
        bool2 = iOplusScreenshotManager.isScreenshotEnabled();
      } catch (RemoteException remoteException) {
        bool2 = DBG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("isScreenshotEnabled : ");
        stringBuilder.append(remoteException.toString());
        OplusLog.e(bool2, "LongshotDump", stringBuilder.toString());
        bool2 = bool;
      } catch (Exception exception) {
        OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
        bool2 = bool1;
      }  
    return bool2;
  }
  
  public void setLongshotEnabled(boolean paramBoolean) {
    IOplusScreenshotManager iOplusScreenshotManager = this.mService;
    if (iOplusScreenshotManager != null)
      try {
        iOplusScreenshotManager.setLongshotEnabled(paramBoolean);
      } catch (RemoteException remoteException) {
        paramBoolean = DBG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("setLongshotEnabled : ");
        stringBuilder.append(remoteException.toString());
        OplusLog.e(paramBoolean, "LongshotDump", stringBuilder.toString());
      } catch (Exception exception) {
        OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
      }  
  }
  
  public boolean isLongshotEnabled() {
    boolean bool1 = true;
    boolean bool = true;
    IOplusScreenshotManager iOplusScreenshotManager = this.mService;
    boolean bool2 = bool1;
    if (iOplusScreenshotManager != null)
      try {
        bool2 = iOplusScreenshotManager.isLongshotEnabled();
      } catch (RemoteException remoteException) {
        bool2 = DBG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("isLongshotEnabled : ");
        stringBuilder.append(remoteException.toString());
        OplusLog.e(bool2, "LongshotDump", stringBuilder.toString());
        bool2 = bool;
      } catch (Exception exception) {
        OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
        bool2 = bool1;
      }  
    return bool2;
  }
  
  @Deprecated
  public void notifyOverScroll(OplusLongshotEvent paramOplusLongshotEvent) {
    IOplusScreenshotManager iOplusScreenshotManager = this.mService;
    if (iOplusScreenshotManager != null)
      try {
        iOplusScreenshotManager.notifyOverScroll(paramOplusLongshotEvent);
      } catch (RemoteException remoteException) {
        boolean bool = DBG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("notifyOverScroll : ");
        stringBuilder.append(remoteException.toString());
        OplusLog.e(bool, "LongshotDump", stringBuilder.toString());
      } catch (Exception exception) {
        OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
      }  
  }
  
  public void setFixedRotationLaunchingAppUnchecked(boolean paramBoolean, int paramInt) {
    this.mIsLaunching = paramBoolean;
    this.mRotation = paramInt;
  }
  
  public boolean isLaunching() {
    return this.mIsLaunching;
  }
  
  public int getRatation() {
    return this.mRotation;
  }
}
