package com.oplus.screenshot;

public interface OplusLongshotView extends OplusLongshotViewBase {
  boolean pointInView(float paramFloat1, float paramFloat2, float paramFloat3);
}
