package com.oplus.screenshot;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.OplusActivityManager;
import android.app.OplusStatusBarManager;
import android.app.OplusWhiteListManager;
import android.app.StatusBarManager;
import android.content.ComponentName;
import android.content.Context;
import android.graphics.Rect;
import android.os.RemoteException;
import android.util.Log;
import android.view.IWindowManager;
import android.view.InputEvent;
import android.view.OplusWindowManager;
import android.view.WindowManagerGlobal;
import com.android.internal.notification.SystemNotificationChannels;
import com.oplus.app.OplusAppInfo;
import com.oplus.util.OplusLog;
import java.lang.reflect.Method;
import java.util.List;

public final class OplusScreenshotCompatible {
  private static final String CHANNEL_ID;
  
  private static final boolean DBG = OplusLongshotDump.DBG;
  
  private static final String TAG = "LongshotDump";
  
  private final ActivityManager mActivityManager;
  
  private final Context mContext;
  
  private final NotificationManager mNotificationManager;
  
  static {
    CHANNEL_ID = SystemNotificationChannels.FOREGROUND_SERVICE;
  }
  
  private final OplusStatusBarManager mOplusStatusBarManager = new OplusStatusBarManager();
  
  private final OplusWindowManager mOplusWindowManager = new OplusWindowManager();
  
  private final OplusActivityManager mOppoAms;
  
  private final StatusBarManager mStatusBarManager;
  
  private final OplusWhiteListManager mWhiteListManager;
  
  public OplusScreenshotCompatible(Context paramContext) {
    this.mContext = paramContext;
    this.mActivityManager = (ActivityManager)paramContext.getSystemService("activity");
    this.mNotificationManager = (NotificationManager)paramContext.getSystemService("notification");
    this.mStatusBarManager = (StatusBarManager)paramContext.getSystemService("statusbar");
    this.mWhiteListManager = new OplusWhiteListManager(paramContext);
    this.mOppoAms = new OplusActivityManager();
  }
  
  public boolean isInMultiWindowMode() {
    boolean bool;
    int i = -1;
    try {
      IWindowManager iWindowManager = WindowManagerGlobal.getWindowManagerService();
      int j = iWindowManager.getDockedStackSide();
    } catch (RemoteException remoteException) {
      bool = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isInMultiWindowMode : ");
      stringBuilder.append(remoteException.toString());
      OplusLog.e(bool, "LongshotDump", stringBuilder.toString());
    } catch (Exception exception) {
      OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
    } 
    if (-1 != i) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void longshotNotifyConnected(boolean paramBoolean) {
    try {
      this.mOplusWindowManager.longshotNotifyConnected(paramBoolean);
    } catch (RemoteException remoteException) {
      paramBoolean = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("longshotNotifyConnected : ");
      stringBuilder.append(remoteException.toString());
      OplusLog.e(paramBoolean, "LongshotDump", stringBuilder.toString());
    } catch (Exception exception) {
      OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
    } 
  }
  
  public int getLongshotSurfaceLayer() {
    boolean bool = false;
    int i = 0;
    try {
      int j = this.mOplusWindowManager.getLongshotSurfaceLayer();
    } catch (RemoteException remoteException) {
      boolean bool1 = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getLongshotSurfaceLayer : ");
      stringBuilder.append(remoteException.toString());
      OplusLog.e(bool1, "LongshotDump", stringBuilder.toString());
    } catch (Exception exception) {
      OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
      i = bool;
    } 
    return i;
  }
  
  public int getLongshotSurfaceLayerByType(int paramInt) {
    boolean bool1 = false, bool2 = false;
    try {
      paramInt = this.mOplusWindowManager.getLongshotSurfaceLayerByType(paramInt);
    } catch (RemoteException remoteException) {
      boolean bool = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getLongshotSurfaceLayerByType : ");
      stringBuilder.append(remoteException.toString());
      OplusLog.e(bool, "LongshotDump", stringBuilder.toString());
      paramInt = bool2;
    } catch (Exception exception) {
      OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
      paramInt = bool1;
    } 
    return paramInt;
  }
  
  public boolean isInputShow() {
    boolean bool1 = false, bool2 = false;
    try {
      boolean bool = this.mOplusWindowManager.isInputShow();
    } catch (RemoteException remoteException) {
      bool1 = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isInputShow : ");
      stringBuilder.append(remoteException.toString());
      OplusLog.e(bool1, "LongshotDump", stringBuilder.toString());
    } catch (Exception exception) {
      OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
      bool2 = bool1;
    } 
    return bool2;
  }
  
  public boolean isVolumeShow() {
    boolean bool1 = false, bool2 = false;
    try {
      boolean bool = this.mOplusWindowManager.isVolumeShow();
    } catch (RemoteException remoteException) {
      bool1 = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isVolumeShow : ");
      stringBuilder.append(remoteException.toString());
      OplusLog.e(bool1, "LongshotDump", stringBuilder.toString());
    } catch (Exception exception) {
      OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
      bool2 = bool1;
    } 
    return bool2;
  }
  
  public boolean isShortcutsPanelShow() {
    boolean bool = false;
    boolean bool1 = false;
    try {
      boolean bool2 = this.mOplusWindowManager.isShortcutsPanelShow();
    } catch (RemoteException remoteException) {
      boolean bool2 = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isShortcutsPanelShow : ");
      stringBuilder.append(remoteException.toString());
      OplusLog.e(bool2, "LongshotDump", stringBuilder.toString());
    } catch (Exception exception) {
      OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
      bool1 = bool;
    } 
    return bool1;
  }
  
  public boolean isStatusBarVisible() {
    boolean bool1 = false, bool2 = false;
    try {
      boolean bool = this.mOplusWindowManager.isStatusBarVisible();
    } catch (RemoteException remoteException) {
      bool1 = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isStatusBarVisible : ");
      stringBuilder.append(remoteException.toString());
      OplusLog.e(bool1, "LongshotDump", stringBuilder.toString());
    } catch (Exception exception) {
      OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
      bool2 = bool1;
    } 
    return bool2;
  }
  
  public boolean isNavigationBarVisible() {
    boolean bool1 = false, bool2 = false;
    try {
      boolean bool = this.mOplusWindowManager.isNavigationBarVisible();
    } catch (RemoteException remoteException) {
      bool1 = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isNavigationBarVisible : ");
      stringBuilder.append(remoteException.toString());
      OplusLog.e(bool1, "LongshotDump", stringBuilder.toString());
    } catch (Exception exception) {
      OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
      bool2 = bool1;
    } 
    return bool2;
  }
  
  public boolean isKeyguardShowingAndNotOccluded() {
    boolean bool = false;
    boolean bool1 = false;
    try {
      boolean bool2 = this.mOplusWindowManager.isKeyguardShowingAndNotOccluded();
    } catch (RemoteException remoteException) {
      boolean bool2 = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isKeyguardShowingAndNotOccluded : ");
      stringBuilder.append(remoteException.toString());
      OplusLog.e(bool2, "LongshotDump", stringBuilder.toString());
    } catch (Exception exception) {
      OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
      bool1 = bool;
    } 
    return bool1;
  }
  
  public void getFocusedWindowFrame(Rect paramRect) {
    try {
      this.mOplusWindowManager.getFocusedWindowFrame(paramRect);
    } catch (RemoteException remoteException) {
      boolean bool = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getFocusedWindowFrame : ");
      stringBuilder.append(remoteException.toString());
      OplusLog.e(bool, "LongshotDump", stringBuilder.toString());
    } catch (Exception exception) {
      OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
    } 
  }
  
  public boolean injectInputEvent(InputEvent paramInputEvent, int paramInt) {
    try {
      this.mOplusWindowManager.longshotInjectInput(paramInputEvent, paramInt);
    } catch (RemoteException remoteException) {
      boolean bool = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("injectInputEvent : ");
      stringBuilder.append(remoteException.toString());
      OplusLog.e(bool, "LongshotDump", stringBuilder.toString());
    } catch (Exception exception) {
      OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
    } 
    return true;
  }
  
  public void injectInputBegin() {
    try {
      this.mOplusWindowManager.longshotInjectInputBegin();
    } catch (RemoteException remoteException) {
      boolean bool = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("injectInputBegin : ");
      stringBuilder.append(remoteException.toString());
      OplusLog.e(bool, "LongshotDump", stringBuilder.toString());
    } catch (Exception exception) {
      OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
    } 
  }
  
  public void injectInputEnd() {
    try {
      this.mOplusWindowManager.longshotInjectInputEnd();
    } catch (RemoteException remoteException) {
      boolean bool = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("injectInputEnd : ");
      stringBuilder.append(remoteException.toString());
      OplusLog.e(bool, "LongshotDump", stringBuilder.toString());
    } catch (Exception exception) {
      OplusLog.e(DBG, "LongshotDump", Log.getStackTraceString(exception));
    } 
  }
  
  @Deprecated
  public Notification.Builder createNotificationBuilder() {
    String str = CHANNEL_ID;
    Context context = this.mContext;
    NotificationChannel notificationChannel = new NotificationChannel(str, context.getString(17040716), 0);
    this.mNotificationManager.createNotificationChannel(notificationChannel);
    return new Notification.Builder(this.mContext, CHANNEL_ID);
  }
  
  @Deprecated
  public void cancelNotification(int paramInt) {
    this.mNotificationManager.deleteNotificationChannel(CHANNEL_ID);
    this.mNotificationManager.cancel(paramInt);
  }
  
  public void createNotificationChannel(String paramString1, String paramString2, int paramInt) {
    NotificationChannel notificationChannel = new NotificationChannel(paramString1, paramString2, paramInt);
    this.mNotificationManager.createNotificationChannel(notificationChannel);
  }
  
  public void createNotificationChannel(String paramString1, String paramString2) {
    createNotificationChannel(paramString1, paramString2, 0);
  }
  
  public void deleteNotificationChannel(String paramString) {
    this.mNotificationManager.deleteNotificationChannel(paramString);
  }
  
  public Notification.Builder createNotificationBuilder(String paramString) {
    return new Notification.Builder(this.mContext, paramString);
  }
  
  public void showNavigationBar(boolean paramBoolean) {}
  
  public void setShortcutsPanelState(boolean paramBoolean) {}
  
  public void addStageProtectInfo(String paramString, long paramLong) {
    this.mWhiteListManager.addStageProtectInfo(paramString, paramLong);
  }
  
  public void removeStageProtectInfo(String paramString) {
    this.mWhiteListManager.removeStageProtectInfo(paramString);
  }
  
  public ComponentName getTopActivity() {
    Exception exception1 = null;
    OplusAppInfo oplusAppInfo = null;
    try {
      List<OplusAppInfo> list = this.mOppoAms.getAllTopAppInfos();
      OplusAppInfo oplusAppInfo1 = oplusAppInfo;
      if (list != null) {
        oplusAppInfo1 = oplusAppInfo;
        if (!list.isEmpty()) {
          byte b = 0;
          while (true) {
            oplusAppInfo1 = oplusAppInfo;
            if (b < list.size()) {
              oplusAppInfo1 = list.get(b);
              ComponentName componentName = oplusAppInfo1.topActivity;
              if (componentName != null)
                break; 
              b++;
              continue;
            } 
            break;
          } 
        } 
      } 
    } catch (Exception exception2) {
      exception2 = exception1;
    } 
    return (ComponentName)exception2;
  }
  
  public String getTopPackage() {
    List<String> list = null;
    try {
      Method method = this.mActivityManager.getClass().getMethod("getAllTopPkgName", new Class[0]);
      method.setAccessible(true);
      List<String> list1 = (List)method.invoke(this.mActivityManager, new Object[0]);
    } catch (Exception exception) {}
    if (list != null) {
      int i = list.size();
      if (i > 0)
        return list.get(0); 
    } 
    return null;
  }
}
