package com.oplus.screenshot;

import android.content.Context;
import android.os.Parcel;
import com.oplus.util.OplusContextUtil;

public final class OplusLongshotComponentName {
  private String mPackageName = null;
  
  private String mContextName = null;
  
  private String mClassName = null;
  
  private String mAccessibilityName = null;
  
  private OplusLongshotComponentName(String paramString1, String paramString2, String paramString3, String paramString4) {
    this.mPackageName = paramString1;
    this.mContextName = paramString2;
    this.mClassName = paramString3;
    this.mAccessibilityName = paramString4;
  }
  
  public static OplusLongshotComponentName create(String paramString1, String paramString2, String paramString3, String paramString4) {
    return new OplusLongshotComponentName(paramString1, paramString2, paramString3, paramString4);
  }
  
  public static OplusLongshotComponentName create(OplusLongshotViewBase paramOplusLongshotViewBase, String paramString) {
    Context context = paramOplusLongshotViewBase.getContext();
    String str3 = context.getPackageName(), str2 = OplusContextUtil.getActivityContextName(context);
    String str1 = paramOplusLongshotViewBase.getClass().getName();
    return create(str3, str2, str1, paramString);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Component{");
    stringBuilder.append(this.mPackageName);
    stringBuilder.append("/");
    stringBuilder.append(this.mContextName);
    stringBuilder.append("/");
    stringBuilder.append(this.mClassName);
    stringBuilder.append("/");
    stringBuilder.append(this.mAccessibilityName);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeString(paramParcel, this.mPackageName);
    writeString(paramParcel, this.mContextName);
    writeString(paramParcel, this.mAccessibilityName);
    writeString(paramParcel, this.mClassName);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mPackageName = readString(paramParcel);
    this.mContextName = readString(paramParcel);
    this.mClassName = readString(paramParcel);
    this.mAccessibilityName = readString(paramParcel);
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
  
  public String getContextName() {
    return this.mContextName;
  }
  
  public String getClassName() {
    return this.mClassName;
  }
  
  public String getAccessibilityName() {
    return this.mAccessibilityName;
  }
  
  private void writeString(Parcel paramParcel, String paramString) {
    if (paramString != null) {
      paramParcel.writeInt(1);
      paramParcel.writeString(paramString);
    } else {
      paramParcel.writeInt(0);
    } 
  }
  
  private String readString(Parcel paramParcel) {
    if (1 == paramParcel.readInt())
      return paramParcel.readString(); 
    return null;
  }
}
