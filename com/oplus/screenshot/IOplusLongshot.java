package com.oplus.screenshot;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusLongshot extends IInterface {
  void notifyOverScroll(OplusLongshotEvent paramOplusLongshotEvent) throws RemoteException;
  
  void start(IOplusLongshotCallback paramIOplusLongshotCallback) throws RemoteException;
  
  void stop() throws RemoteException;
  
  class Default implements IOplusLongshot {
    public void start(IOplusLongshotCallback param1IOplusLongshotCallback) throws RemoteException {}
    
    public void stop() throws RemoteException {}
    
    public void notifyOverScroll(OplusLongshotEvent param1OplusLongshotEvent) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusLongshot {
    private static final String DESCRIPTOR = "com.oplus.screenshot.IOplusLongshot";
    
    static final int TRANSACTION_notifyOverScroll = 3;
    
    static final int TRANSACTION_start = 1;
    
    static final int TRANSACTION_stop = 2;
    
    public Stub() {
      attachInterface(this, "com.oplus.screenshot.IOplusLongshot");
    }
    
    public static IOplusLongshot asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.screenshot.IOplusLongshot");
      if (iInterface != null && iInterface instanceof IOplusLongshot)
        return (IOplusLongshot)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "notifyOverScroll";
        } 
        return "stop";
      } 
      return "start";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.oplus.screenshot.IOplusLongshot");
            return true;
          } 
          param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusLongshot");
          if (param1Parcel1.readInt() != 0) {
            OplusLongshotEvent oplusLongshotEvent = (OplusLongshotEvent)OplusLongshotEvent.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          notifyOverScroll((OplusLongshotEvent)param1Parcel1);
          return true;
        } 
        param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusLongshot");
        stop();
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusLongshot");
      IOplusLongshotCallback iOplusLongshotCallback = IOplusLongshotCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
      start(iOplusLongshotCallback);
      return true;
    }
    
    private static class Proxy implements IOplusLongshot {
      public static IOplusLongshot sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.screenshot.IOplusLongshot";
      }
      
      public void start(IOplusLongshotCallback param2IOplusLongshotCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.oplus.screenshot.IOplusLongshot");
          if (param2IOplusLongshotCallback != null) {
            iBinder = param2IOplusLongshotCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusLongshot.Stub.getDefaultImpl() != null) {
            IOplusLongshot.Stub.getDefaultImpl().start(param2IOplusLongshotCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void stop() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.screenshot.IOplusLongshot");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IOplusLongshot.Stub.getDefaultImpl() != null) {
            IOplusLongshot.Stub.getDefaultImpl().stop();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyOverScroll(OplusLongshotEvent param2OplusLongshotEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.screenshot.IOplusLongshot");
          if (param2OplusLongshotEvent != null) {
            parcel.writeInt(1);
            param2OplusLongshotEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IOplusLongshot.Stub.getDefaultImpl() != null) {
            IOplusLongshot.Stub.getDefaultImpl().notifyOverScroll(param2OplusLongshotEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusLongshot param1IOplusLongshot) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusLongshot != null) {
          Proxy.sDefaultImpl = param1IOplusLongshot;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusLongshot getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
