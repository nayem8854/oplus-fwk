package com.oplus.screenshot;

public abstract class OplusLongshotController implements IOplusLongshotController {
  final String mSource;
  
  final OplusLongshotViewBase mViewBase;
  
  public OplusLongshotController(OplusLongshotViewBase paramOplusLongshotViewBase, String paramString) {
    this.mViewBase = paramOplusLongshotViewBase;
    this.mSource = paramString;
  }
  
  public boolean findInfo(OplusLongshotViewInfo paramOplusLongshotViewInfo) {
    findUnsupported(paramOplusLongshotViewInfo);
    return true;
  }
  
  private void findUnsupported(OplusLongshotViewInfo paramOplusLongshotViewInfo) {
    OplusLongshotViewBase oplusLongshotViewBase = this.mViewBase;
    if (oplusLongshotViewBase instanceof OplusLongshotUnsupported) {
      OplusLongshotUnsupported oplusLongshotUnsupported = (OplusLongshotUnsupported)oplusLongshotViewBase;
      if (oplusLongshotUnsupported.isLongshotUnsupported())
        paramOplusLongshotViewInfo.setUnsupported(); 
    } 
  }
}
