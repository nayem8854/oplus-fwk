package com.oplus.screenshot;

public class OplusLongshotCustomController extends OplusLongshotController {
  private final OplusScreenshotManager mScreenshotManager;
  
  public OplusLongshotCustomController(OplusLongshotViewBase paramOplusLongshotViewBase, String paramString) {
    super(paramOplusLongshotViewBase, paramString);
    this.mScreenshotManager = OplusLongshotUtils.getScreenshotManager(paramOplusLongshotViewBase.getContext());
  }
  
  public void onLongScroll() {
    if (!canLongshot()) {
      OplusLongshotEvent oplusLongshotEvent = new OplusLongshotEvent(this.mViewBase.getClass().getName(), 0, true);
      this.mScreenshotManager.notifyOverScroll(oplusLongshotEvent);
    } 
  }
  
  private boolean canLongshot() {
    OplusLongshotViewBase oplusLongshotViewBase = this.mViewBase;
    int i = oplusLongshotViewBase.computeLongScrollOffset();
    int j = oplusLongshotViewBase.computeLongScrollRange(), k = oplusLongshotViewBase.computeLongScrollExtent();
    boolean bool = false;
    if (j - k == 0)
      return false; 
    if (i > 0)
      bool = true; 
    return bool;
  }
}
