package com.oplus.screenshot;

import android.view.IOplusBaseViewRoot;
import android.view.OplusBaseView;

public class OplusLongshotViewController extends OplusLongshotController {
  private static final boolean DBG = OplusLongshotDump.DBG;
  
  private static final String TAG = "LongshotDump";
  
  private final OplusBaseView mView;
  
  public OplusLongshotViewController(OplusBaseView paramOplusBaseView) {
    super((OplusLongshotViewBase)paramOplusBaseView, "View");
    this.mView = paramOplusBaseView;
  }
  
  public boolean isLongshotConnected() {
    IOplusBaseViewRoot iOplusBaseViewRoot = this.mView.getViewRootImpl();
    if (iOplusBaseViewRoot != null) {
      OplusLongshotViewRoot oplusLongshotViewRoot = iOplusBaseViewRoot.getLongshotViewRoot();
      return oplusLongshotViewRoot.isConnected();
    } 
    return false;
  }
  
  public int getOverScrollMode(int paramInt) {
    return paramInt;
  }
  
  public boolean overScrollBy(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, boolean paramBoolean1, int paramInt9, boolean paramBoolean2) {
    int i = -paramInt8;
    paramInt6 = paramInt8 + paramInt6;
    paramBoolean1 = false;
    paramInt4 += paramInt2;
    if (paramInt4 > paramInt6) {
      paramInt2 = paramInt6;
      paramBoolean1 = true;
    } else {
      paramInt2 = paramInt4;
      if (paramInt4 < i) {
        paramInt2 = i;
        paramBoolean1 = true;
      } 
    } 
    if (paramInt2 != paramInt9) {
      paramInt4 = -paramInt7;
      paramInt5 = paramInt7 + paramInt5;
      paramBoolean2 = false;
      paramInt3 += paramInt1;
      if (paramInt3 > paramInt5) {
        paramInt1 = paramInt5;
        paramBoolean2 = true;
      } else {
        paramInt1 = paramInt3;
        if (paramInt3 < paramInt4) {
          paramInt1 = paramInt4;
          paramBoolean2 = true;
        } 
      } 
      this.mView.onLongshotOverScrolled(paramInt1, paramInt2, paramBoolean2, paramBoolean1);
      if (paramBoolean2 || paramBoolean1) {
        paramBoolean1 = true;
        return paramBoolean1;
      } 
      paramBoolean1 = false;
    } else {
      paramBoolean1 = paramBoolean2;
    } 
    return paramBoolean1;
  }
}
