package com.oplus.screenshot;

import com.oplus.util.OplusLog;

public class OplusLongshotViewRoot {
  private static final boolean DBG = OplusLongshotDump.DBG;
  
  private static final String TAG = "LongshotDump";
  
  private boolean mIsConnected = false;
  
  public void setConnected(boolean paramBoolean) {
    boolean bool = DBG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("setConnected : ");
    stringBuilder.append(paramBoolean);
    OplusLog.d(bool, "LongshotDump", stringBuilder.toString());
    this.mIsConnected = paramBoolean;
  }
  
  public boolean isConnected() {
    return this.mIsConnected;
  }
}
