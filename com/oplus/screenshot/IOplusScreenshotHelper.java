package com.oplus.screenshot;

public interface IOplusScreenshotHelper {
  String getSource();
  
  boolean isGlobalAction();
}
