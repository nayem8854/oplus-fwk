package com.oplus.screenshot;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusScreenshot extends IInterface {
  boolean isEdit() throws RemoteException;
  
  void start(IOplusScreenshotCallback paramIOplusScreenshotCallback) throws RemoteException;
  
  void stop() throws RemoteException;
  
  class Default implements IOplusScreenshot {
    public void start(IOplusScreenshotCallback param1IOplusScreenshotCallback) throws RemoteException {}
    
    public void stop() throws RemoteException {}
    
    public boolean isEdit() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusScreenshot {
    private static final String DESCRIPTOR = "com.oplus.screenshot.IOplusScreenshot";
    
    static final int TRANSACTION_isEdit = 3;
    
    static final int TRANSACTION_start = 1;
    
    static final int TRANSACTION_stop = 2;
    
    public Stub() {
      attachInterface(this, "com.oplus.screenshot.IOplusScreenshot");
    }
    
    public static IOplusScreenshot asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.screenshot.IOplusScreenshot");
      if (iInterface != null && iInterface instanceof IOplusScreenshot)
        return (IOplusScreenshot)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "isEdit";
        } 
        return "stop";
      } 
      return "start";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.oplus.screenshot.IOplusScreenshot");
            return true;
          } 
          param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusScreenshot");
          boolean bool = isEdit();
          param1Parcel2.writeNoException();
          param1Parcel2.writeInt(bool);
          return true;
        } 
        param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusScreenshot");
        stop();
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusScreenshot");
      IOplusScreenshotCallback iOplusScreenshotCallback = IOplusScreenshotCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
      start(iOplusScreenshotCallback);
      return true;
    }
    
    private static class Proxy implements IOplusScreenshot {
      public static IOplusScreenshot sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.screenshot.IOplusScreenshot";
      }
      
      public void start(IOplusScreenshotCallback param2IOplusScreenshotCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.oplus.screenshot.IOplusScreenshot");
          if (param2IOplusScreenshotCallback != null) {
            iBinder = param2IOplusScreenshotCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusScreenshot.Stub.getDefaultImpl() != null) {
            IOplusScreenshot.Stub.getDefaultImpl().start(param2IOplusScreenshotCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void stop() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.screenshot.IOplusScreenshot");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IOplusScreenshot.Stub.getDefaultImpl() != null) {
            IOplusScreenshot.Stub.getDefaultImpl().stop();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean isEdit() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.screenshot.IOplusScreenshot");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IOplusScreenshot.Stub.getDefaultImpl() != null) {
            bool1 = IOplusScreenshot.Stub.getDefaultImpl().isEdit();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusScreenshot param1IOplusScreenshot) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusScreenshot != null) {
          Proxy.sDefaultImpl = param1IOplusScreenshot;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusScreenshot getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
