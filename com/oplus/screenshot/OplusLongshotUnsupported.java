package com.oplus.screenshot;

public interface OplusLongshotUnsupported {
  boolean isLongshotUnsupported();
}
