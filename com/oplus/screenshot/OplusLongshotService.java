package com.oplus.screenshot;

import android.content.Context;

public abstract class OplusLongshotService extends IOplusLongshot.Stub {
  protected final Context mContext;
  
  protected final boolean mNavBarVisible;
  
  protected final boolean mStatusBarVisible;
  
  public OplusLongshotService(Context paramContext, boolean paramBoolean1, boolean paramBoolean2) {
    this.mContext = paramContext;
    this.mStatusBarVisible = paramBoolean1;
    this.mNavBarVisible = paramBoolean2;
  }
  
  public void start(IOplusLongshotCallback paramIOplusLongshotCallback) {}
  
  public void stop() {}
  
  public void notifyOverScroll(OplusLongshotEvent paramOplusLongshotEvent) {}
}
