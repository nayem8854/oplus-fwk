package com.oplus.screenshot;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusLongshotCallback extends IInterface {
  void stop() throws RemoteException;
  
  class Default implements IOplusLongshotCallback {
    public void stop() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusLongshotCallback {
    private static final String DESCRIPTOR = "com.oplus.screenshot.IOplusLongshotCallback";
    
    static final int TRANSACTION_stop = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.screenshot.IOplusLongshotCallback");
    }
    
    public static IOplusLongshotCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.screenshot.IOplusLongshotCallback");
      if (iInterface != null && iInterface instanceof IOplusLongshotCallback)
        return (IOplusLongshotCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "stop";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.screenshot.IOplusLongshotCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusLongshotCallback");
      stop();
      return true;
    }
    
    private static class Proxy implements IOplusLongshotCallback {
      public static IOplusLongshotCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.screenshot.IOplusLongshotCallback";
      }
      
      public void stop() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.screenshot.IOplusLongshotCallback");
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusLongshotCallback.Stub.getDefaultImpl() != null) {
            IOplusLongshotCallback.Stub.getDefaultImpl().stop();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusLongshotCallback param1IOplusLongshotCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusLongshotCallback != null) {
          Proxy.sDefaultImpl = param1IOplusLongshotCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusLongshotCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
