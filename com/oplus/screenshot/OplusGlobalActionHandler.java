package com.oplus.screenshot;

import android.os.Handler;
import android.os.Looper;

public class OplusGlobalActionHandler extends Handler implements IOplusScreenshotHelper {
  public OplusGlobalActionHandler(Looper paramLooper) {
    super(paramLooper);
  }
  
  public String getSource() {
    return "GlobalAction";
  }
  
  public boolean isGlobalAction() {
    return false;
  }
}
