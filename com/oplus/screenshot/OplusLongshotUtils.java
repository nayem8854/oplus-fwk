package com.oplus.screenshot;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;
import com.oplus.feature.OplusDisableFeatures;
import com.oplus.util.OplusLog;
import com.oplus.view.OplusWindowUtils;
import java.util.List;

public final class OplusLongshotUtils {
  public static final Intent INTENT_HOME = (new Intent("android.intent.action.MAIN")).addCategory("android.intent.category.HOME");
  
  public static final String PACKAGE_EXSERVICEUI = "com.coloros.exserviceui";
  
  public static final String PACKAGE_FLOATASSISTANT = "com.coloros.floatassistant";
  
  public static final String PACKAGE_GALLERY = "com.coloros.gallery3d";
  
  public static final String PACKAGE_SCREENSHOT = "com.coloros.screenshot";
  
  public static final String PACKAGE_SYSTEMUI = "com.android.systemui";
  
  private static final String TAG = "LongshotDump";
  
  public static final String TAG_LONGSHOT = "Screenshot";
  
  public static final int VALUE_FIVE = 5;
  
  public static final int VALUE_FOUR = 4;
  
  public static final int VALUE_THREE = 3;
  
  public static final int VALUE_TWO = 2;
  
  public static final String VIEW_DECOR = "com.android.internal.policy.impl.PhoneWindow$DecorView";
  
  public static final String VIEW_NAVIGATIONBAR = "com.android.systemui.statusbar.phone.NavigationBarView";
  
  public static final String VIEW_STATUSBAR = "com.android.systemui.statusbar.phone.StatusBarWindowView";
  
  public static OplusScreenshotManager getScreenshotManager(Context paramContext) {
    return (OplusScreenshotManager)paramContext.getSystemService("color_screenshot");
  }
  
  public static boolean isDisabled(Context paramContext) {
    return paramContext.getPackageManager().hasSystemFeature(OplusDisableFeatures.SystemCenter.LONGSHOT);
  }
  
  public static boolean isInstalled(Context paramContext) {
    boolean bool = false;
    try {
    
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
    
    } finally {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isInstalled : ");
      stringBuilder.append(false);
      OplusLog.w("LongshotDump", stringBuilder.toString());
    } 
    paramContext.append("isInstalled : ");
    paramContext.append(bool);
    OplusLog.w("LongshotDump", paramContext.toString());
    return bool;
  }
  
  public static boolean isHomeApp(Context paramContext) {
    String str = paramContext.getPackageName();
    List list = paramContext.getPackageManager().queryIntentActivities(INTENT_HOME, 65536);
    for (ResolveInfo resolveInfo : list) {
      if (str.equals(resolveInfo.activityInfo.packageName))
        return true; 
    } 
    return false;
  }
  
  public static boolean isExServiceUiApp(String paramString) {
    return OplusWindowUtils.isExServiceUiApp(paramString);
  }
  
  public static boolean isExServiceUiApp(Context paramContext) {
    return isExServiceUiApp(paramContext.getPackageName());
  }
  
  public static boolean isScreenshotApp(String paramString) {
    return OplusWindowUtils.isScreenshotApp(paramString);
  }
  
  public static boolean isScreenshotApp(Context paramContext) {
    return isScreenshotApp(paramContext.getPackageName());
  }
  
  public static boolean isSystemUiApp(String paramString) {
    return OplusWindowUtils.isSystemUiApp(paramString);
  }
  
  public static boolean isSystemUiApp(Context paramContext) {
    return isSystemUiApp(paramContext.getPackageName());
  }
  
  public static boolean isDecorView(Object paramObject) {
    return paramObject.getClass().getName().equals("com.android.internal.policy.impl.PhoneWindow$DecorView");
  }
  
  public static boolean isStatusBarView(Object paramObject) {
    return paramObject.getClass().getName().equals("com.android.systemui.statusbar.phone.StatusBarWindowView");
  }
  
  public static boolean isNavigationBarView(Object paramObject) {
    return paramObject.getClass().getName().equals("com.android.systemui.statusbar.phone.NavigationBarView");
  }
  
  public static boolean isStatusBar(int paramInt) {
    return OplusWindowUtils.isStatusBar(paramInt);
  }
  
  public static boolean isNavigationBar(int paramInt) {
    return OplusWindowUtils.isNavigationBar(paramInt);
  }
  
  public static boolean isSystemUiBar(int paramInt, CharSequence paramCharSequence) {
    return OplusWindowUtils.isSystemUiBar(paramInt, paramCharSequence);
  }
  
  public static boolean isTickerPanel(String paramString, CharSequence paramCharSequence) {
    if (isSystemUiApp(paramString))
      return "TickerPanel".equals(paramCharSequence); 
    return false;
  }
  
  public static boolean isSystemWindow(String paramString, CharSequence paramCharSequence, int paramInt) {
    if (isExServiceUiApp(paramString))
      return true; 
    if (isSystemUiApp(paramString))
      return isSystemUiBar(paramInt, paramCharSequence); 
    return false;
  }
  
  public static boolean isAllSystemWindow(String paramString, CharSequence paramCharSequence, int paramInt) {
    if (isScreenshotApp(paramString))
      return true; 
    return isSystemWindow(paramString, paramCharSequence, paramInt);
  }
  
  public static boolean isInputMethodWindow(int paramInt, CharSequence paramCharSequence) {
    return OplusWindowUtils.isInputMethodWindow(paramInt, paramCharSequence);
  }
  
  public static String getBaseClassNameOf(View paramView) {
    Exception exception2 = null;
    try {
      AccessibilityNodeInfo accessibilityNodeInfo = paramView.createAccessibilityNodeInfo();
    } catch (Exception exception1) {
      exception1 = exception2;
    } 
    if (exception1 == null)
      return null; 
    CharSequence charSequence = exception1.getClassName();
    try {
      exception1.recycle();
    } catch (Exception exception) {}
    if (charSequence == null)
      return null; 
    return charSequence.toString();
  }
  
  public static boolean isWebFromBaseName(String paramString) {
    if (paramString == null)
      return false; 
    if (paramString.contains("WebView"))
      return true; 
    if (paramString.contains("webkit"))
      return true; 
    return false;
  }
  
  public static boolean isWebContent(String paramString) {
    if (paramString == null)
      return false; 
    if (!paramString.startsWith("org.chromium.content.browser."))
      return false; 
    if (!paramString.endsWith("ContentView"))
      return false; 
    return true;
  }
  
  public static boolean canScrollVertically(View paramView) {
    boolean bool;
    try {
      bool = testScrollVertically(paramView);
    } catch (Exception exception) {
      bool = true;
    } 
    return bool;
  }
  
  public static boolean canScrollVerticallyForward(OplusLongshotViewBase paramOplusLongshotViewBase) {
    return paramOplusLongshotViewBase.canLongScroll();
  }
  
  public static boolean canScrollVerticallyWithPadding(OplusLongshotViewBase paramOplusLongshotViewBase, int paramInt) {
    int i = paramOplusLongshotViewBase.computeLongScrollOffset();
    int j = paramOplusLongshotViewBase.computeLongScrollRange() - paramOplusLongshotViewBase.computeLongScrollExtent();
    boolean bool = false;
    if (j == 0)
      return false; 
    if (i < j - 1 + paramInt)
      bool = true; 
    return bool;
  }
  
  public static boolean isFloatAssistant(String paramString) {
    return OplusWindowUtils.isFloatAssistant(paramString);
  }
  
  public static boolean isGallery(String paramString) {
    return OplusWindowUtils.isGallery(paramString);
  }
  
  private static boolean testScrollVertically(View paramView) {
    if (paramView.canScrollVertically(1))
      return true; 
    if (paramView.canScrollVertically(-1))
      return true; 
    return false;
  }
}
