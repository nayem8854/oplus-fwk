package com.oplus.screenshot;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusScreenshotCallback extends IInterface {
  void stop() throws RemoteException;
  
  class Default implements IOplusScreenshotCallback {
    public void stop() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusScreenshotCallback {
    private static final String DESCRIPTOR = "com.oplus.screenshot.IOplusScreenshotCallback";
    
    static final int TRANSACTION_stop = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.screenshot.IOplusScreenshotCallback");
    }
    
    public static IOplusScreenshotCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.screenshot.IOplusScreenshotCallback");
      if (iInterface != null && iInterface instanceof IOplusScreenshotCallback)
        return (IOplusScreenshotCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "stop";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.screenshot.IOplusScreenshotCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.screenshot.IOplusScreenshotCallback");
      stop();
      return true;
    }
    
    private static class Proxy implements IOplusScreenshotCallback {
      public static IOplusScreenshotCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.screenshot.IOplusScreenshotCallback";
      }
      
      public void stop() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.screenshot.IOplusScreenshotCallback");
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusScreenshotCallback.Stub.getDefaultImpl() != null) {
            IOplusScreenshotCallback.Stub.getDefaultImpl().stop();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusScreenshotCallback param1IOplusScreenshotCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusScreenshotCallback != null) {
          Proxy.sDefaultImpl = param1IOplusScreenshotCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusScreenshotCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
