package com.oplus.screenshot;

import android.os.Parcel;
import android.os.Parcelable;

public class OplusLongshotEvent implements Parcelable {
  public static final Parcelable.Creator<OplusLongshotEvent> CREATOR = new Parcelable.Creator<OplusLongshotEvent>() {
      public OplusLongshotEvent createFromParcel(Parcel param1Parcel) {
        return new OplusLongshotEvent(param1Parcel);
      }
      
      public OplusLongshotEvent[] newArray(int param1Int) {
        return new OplusLongshotEvent[param1Int];
      }
    };
  
  private String mSource = null;
  
  private int mOffset = 0;
  
  private boolean mIsOverScroll = false;
  
  private static final String TAG = "OplusLongshotEvent";
  
  public OplusLongshotEvent(String paramString, int paramInt, boolean paramBoolean) {
    this.mSource = paramString;
    this.mOffset = paramInt;
    this.mIsOverScroll = paramBoolean;
  }
  
  public OplusLongshotEvent(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mSource);
    paramParcel.writeInt(this.mOffset);
    paramParcel.writeInt(this.mIsOverScroll);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mSource = paramParcel.readString();
    this.mOffset = paramParcel.readInt();
    int i = paramParcel.readInt();
    boolean bool = true;
    if (i != 1)
      bool = false; 
    this.mIsOverScroll = bool;
  }
  
  public String getSource() {
    return this.mSource;
  }
  
  public int getOffset() {
    return this.mOffset;
  }
  
  public boolean isOverScroll() {
    return this.mIsOverScroll;
  }
}
