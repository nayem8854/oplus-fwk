package com.oplus.screenshot;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.IOplusLongshotWindowManager;
import android.view.WindowManager;

public interface IOplusScreenShotEuclidManager extends IOplusCommonFeature {
  public static final IOplusScreenShotEuclidManager DEFAULT = (IOplusScreenShotEuclidManager)new Object();
  
  default IOplusScreenShotEuclidManager getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusScreenShotEuclidManager;
  }
  
  default boolean updateSpecialSystemBar(WindowManager.LayoutParams paramLayoutParams) {
    return false;
  }
  
  default boolean skipSystemUiVisibility(WindowManager.LayoutParams paramLayoutParams) {
    return false;
  }
  
  default boolean isSpecialAppWindow(boolean paramBoolean, WindowManager.LayoutParams paramLayoutParams) {
    return paramBoolean;
  }
  
  default boolean takeScreenshot(Context paramContext, int paramInt, boolean paramBoolean1, boolean paramBoolean2, Handler paramHandler) {
    return false;
  }
  
  default Handler getScreenShotHandler(Looper paramLooper) {
    return new Handler(Looper.getMainLooper());
  }
  
  default IOplusLongshotWindowManager getIOplusLongshotWindowManager() {
    return null;
  }
}
