package com.oplus.screenshot;

import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemProperties;

public class OplusLongshotDump implements Parcelable {
  public static final Parcelable.Creator<OplusLongshotDump> CREATOR;
  
  public static final boolean DBG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  public static final String JSON_POSTFIX_LIST = "_list";
  
  public static final String JSON_POSTFIX_RECT = "_rect";
  
  public static final String JSON_PREFIX_CHILD = "child_";
  
  public static final String JSON_PREFIX_PARENT = "parent_";
  
  public static final String JSON_PREFIX_SCROLL = "scroll_";
  
  public static final String JSON_PREFIX_WINDOW = "window_";
  
  public static final String TAG = "LongshotDump";
  
  private OplusLongshotComponentName mComponent;
  
  private int mDumpCount;
  
  private int mScrollCount;
  
  private Rect mScrollRect;
  
  private long mSpendCalc;
  
  private long mSpendDump;
  
  private long mSpendPack;
  
  static {
    CREATOR = new Parcelable.Creator<OplusLongshotDump>() {
        public OplusLongshotDump createFromParcel(Parcel param1Parcel) {
          return new OplusLongshotDump(param1Parcel);
        }
        
        public OplusLongshotDump[] newArray(int param1Int) {
          return new OplusLongshotDump[param1Int];
        }
      };
  }
  
  public OplusLongshotDump() {
    this.mScrollRect = new Rect();
    this.mComponent = null;
    this.mSpendDump = 0L;
    this.mSpendCalc = 0L;
    this.mSpendPack = 0L;
    this.mDumpCount = 0;
    this.mScrollCount = 0;
  }
  
  public OplusLongshotDump(Parcel paramParcel) {
    this.mScrollRect = new Rect();
    this.mComponent = null;
    this.mSpendDump = 0L;
    this.mSpendCalc = 0L;
    this.mSpendPack = 0L;
    this.mDumpCount = 0;
    this.mScrollCount = 0;
    readFromParcel(paramParcel);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[rect=");
    stringBuilder.append(this.mScrollRect);
    stringBuilder.append(":");
    stringBuilder.append(this.mComponent);
    stringBuilder.append(":dump=");
    stringBuilder.append(this.mSpendDump);
    stringBuilder.append(":calc=");
    stringBuilder.append(this.mSpendCalc);
    stringBuilder.append(":pack=");
    stringBuilder.append(this.mSpendPack);
    stringBuilder.append(":dumpCount=");
    stringBuilder.append(this.mDumpCount);
    stringBuilder.append(":scrollCount=");
    stringBuilder.append(this.mScrollCount);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.mScrollRect.writeToParcel(paramParcel, paramInt);
    writeComponent(paramParcel, paramInt);
    paramParcel.writeLong(this.mSpendDump);
    paramParcel.writeLong(this.mSpendCalc);
    paramParcel.writeLong(this.mSpendPack);
    paramParcel.writeInt(this.mDumpCount);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mScrollRect.readFromParcel(paramParcel);
    readComponent(paramParcel);
    this.mSpendDump = paramParcel.readLong();
    this.mSpendCalc = paramParcel.readLong();
    this.mSpendPack = paramParcel.readLong();
    this.mDumpCount = paramParcel.readInt();
  }
  
  public void setScrollRect(Rect paramRect) {
    this.mScrollRect.set(paramRect);
  }
  
  public void setScrollComponent(OplusLongshotComponentName paramOplusLongshotComponentName) {
    this.mComponent = paramOplusLongshotComponentName;
  }
  
  public void setSpendDump(long paramLong) {
    this.mSpendDump = paramLong;
  }
  
  public void setSpendCalc(long paramLong) {
    this.mSpendCalc = paramLong;
  }
  
  public void setSpendPack(long paramLong) {
    this.mSpendPack = paramLong;
  }
  
  public void setDumpCount(int paramInt) {
    this.mDumpCount = paramInt;
  }
  
  public void setScrollCount(int paramInt) {
    this.mScrollCount = paramInt;
  }
  
  public long getTotalSpend() {
    return this.mSpendDump + this.mSpendCalc + this.mSpendPack;
  }
  
  public int getDumpCount() {
    return this.mDumpCount;
  }
  
  private void writeComponent(Parcel paramParcel, int paramInt) {
    if (this.mComponent != null) {
      paramParcel.writeInt(1);
      this.mComponent.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeInt(0);
    } 
  }
  
  private void readComponent(Parcel paramParcel) {
    if (1 == paramParcel.readInt())
      this.mComponent.readFromParcel(paramParcel); 
  }
}
