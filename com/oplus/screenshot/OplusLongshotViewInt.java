package com.oplus.screenshot;

public interface OplusLongshotViewInt extends OplusLongshotViewBase {
  void onLongshotOverScrolled(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2);
}
