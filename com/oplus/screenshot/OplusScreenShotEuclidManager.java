package com.oplus.screenshot;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.IOplusLongshotWindowManager;
import android.view.OplusBaseLayoutParams;
import android.view.OplusLongshotWindowManager;
import android.view.WindowManager;
import com.oplus.util.OplusLog;
import com.oplus.util.OplusTypeCastingHelper;
import com.oplus.view.OplusWindowManager;

public class OplusScreenShotEuclidManager implements IOplusScreenShotEuclidManager {
  private static volatile OplusScreenShotEuclidManager sInstance = null;
  
  public static OplusScreenShotEuclidManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/screenshot/OplusScreenShotEuclidManager.sInstance : Lcom/oplus/screenshot/OplusScreenShotEuclidManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/screenshot/OplusScreenShotEuclidManager
    //   8: monitorenter
    //   9: getstatic com/oplus/screenshot/OplusScreenShotEuclidManager.sInstance : Lcom/oplus/screenshot/OplusScreenShotEuclidManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/screenshot/OplusScreenShotEuclidManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/screenshot/OplusScreenShotEuclidManager.sInstance : Lcom/oplus/screenshot/OplusScreenShotEuclidManager;
    //   27: ldc com/oplus/screenshot/OplusScreenShotEuclidManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/screenshot/OplusScreenShotEuclidManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/screenshot/OplusScreenShotEuclidManager.sInstance : Lcom/oplus/screenshot/OplusScreenShotEuclidManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #41	-> 0
    //   #42	-> 6
    //   #43	-> 9
    //   #44	-> 15
    //   #46	-> 27
    //   #48	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public boolean updateSpecialSystemBar(WindowManager.LayoutParams paramLayoutParams) {
    if (OplusWindowManager.isUseLastStatusBarTint(paramLayoutParams))
      return true; 
    if (OplusWindowManager.updateDarkNavigationBar(paramLayoutParams))
      return true; 
    return false;
  }
  
  public boolean skipSystemUiVisibility(WindowManager.LayoutParams paramLayoutParams) {
    OplusBaseLayoutParams oplusBaseLayoutParams = (OplusBaseLayoutParams)OplusTypeCastingHelper.typeCasting(OplusBaseLayoutParams.class, paramLayoutParams);
    boolean bool = false;
    if (oplusBaseLayoutParams != null)
      bool = oplusBaseLayoutParams.mOplusLayoutParams.getSkipSystemUiVisibility(); 
    if (bool && OplusLongshotDump.DBG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("updateSystemUiVisibilityLw : skip ");
      stringBuilder.append(bool);
      Log.d("LongshotDump", stringBuilder.toString());
    } 
    return bool;
  }
  
  public boolean isSpecialAppWindow(boolean paramBoolean, WindowManager.LayoutParams paramLayoutParams) {
    if (OplusWindowManager.LayoutParams.isFullscreen(paramLayoutParams) && 
      OplusWindowManager.LayoutParams.isLongshotWindow(paramLayoutParams.type))
      return true; 
    return paramBoolean;
  }
  
  public boolean takeScreenshot(Context paramContext, int paramInt, boolean paramBoolean1, boolean paramBoolean2, Handler paramHandler) {
    OplusScreenshotManager oplusScreenshotManager = OplusLongshotUtils.getScreenshotManager(paramContext);
    if (oplusScreenshotManager != null && oplusScreenshotManager.isScreenshotSupported()) {
      Bundle bundle = new Bundle();
      if (paramHandler instanceof IOplusScreenshotHelper) {
        IOplusScreenshotHelper iOplusScreenshotHelper = (IOplusScreenshotHelper)paramHandler;
        bundle.putString("screenshot_source", iOplusScreenshotHelper.getSource());
        bundle.putBoolean("global_action_visible", iOplusScreenshotHelper.isGlobalAction());
      } 
      bundle.putBoolean("statusbar_visible", paramBoolean1);
      bundle.putBoolean("navigationbar_visible", paramBoolean2);
      bundle.putBoolean("screenshot_orientation", isLandscape(paramContext));
      oplusScreenshotManager.takeScreenshot(bundle);
      OplusLog.d("LongshotDump", "takeScreenshot : PASS");
      return true;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("takeScreenshot : FAIL sm = ");
    stringBuilder.append(oplusScreenshotManager);
    stringBuilder.append(" , ");
    if (oplusScreenshotManager == null) {
      paramBoolean1 = false;
    } else {
      paramBoolean1 = oplusScreenshotManager.isScreenshotSupported();
    } 
    stringBuilder.append(paramBoolean1);
    OplusLog.e("LongshotDump", stringBuilder.toString());
    return false;
  }
  
  public Handler getScreenShotHandler(Looper paramLooper) {
    return new OplusGlobalActionHandler(Looper.getMainLooper());
  }
  
  public IOplusLongshotWindowManager getIOplusLongshotWindowManager() {
    return (IOplusLongshotWindowManager)new OplusLongshotWindowManager();
  }
  
  private boolean isLandscape(Context paramContext) {
    OplusScreenshotManager oplusScreenshotManager = OplusLongshotUtils.getScreenshotManager(paramContext);
    boolean bool = oplusScreenshotManager.isLaunching();
    boolean bool1 = false, bool2 = false;
    if (bool) {
      if (oplusScreenshotManager.getRatation() == 1 || oplusScreenshotManager.getRatation() == 3)
        bool2 = true; 
      return bool2;
    } 
    Configuration configuration = paramContext.getResources().getConfiguration();
    bool2 = bool1;
    if (configuration.orientation == 2)
      bool2 = true; 
    return bool2;
  }
}
