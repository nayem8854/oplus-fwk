package com.oplus.screenshot;

import android.os.Parcel;
import android.os.Parcelable;

public final class OplusLongshotViewInfo implements Parcelable {
  public static final Parcelable.Creator<OplusLongshotViewInfo> CREATOR = new Parcelable.Creator<OplusLongshotViewInfo>() {
      public OplusLongshotViewInfo createFromParcel(Parcel param1Parcel) {
        return new OplusLongshotViewInfo(param1Parcel);
      }
      
      public OplusLongshotViewInfo[] newArray(int param1Int) {
        return new OplusLongshotViewInfo[param1Int];
      }
    };
  
  private boolean mIsUnsupported = false;
  
  public OplusLongshotViewInfo(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mIsUnsupported);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.readInt();
    boolean bool = true;
    if (1 != i)
      bool = false; 
    this.mIsUnsupported = bool;
  }
  
  public void reset() {
    this.mIsUnsupported = false;
  }
  
  public void setUnsupported() {
    this.mIsUnsupported = true;
  }
  
  public boolean isUnsupported() {
    return this.mIsUnsupported;
  }
  
  public OplusLongshotViewInfo() {}
}
