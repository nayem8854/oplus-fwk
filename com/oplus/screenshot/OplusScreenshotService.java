package com.oplus.screenshot;

import android.content.Context;
import android.os.Bundle;

public abstract class OplusScreenshotService extends IOplusScreenshot.Stub {
  protected final Context mContext;
  
  protected final Bundle mExtras;
  
  public OplusScreenshotService(Context paramContext, Bundle paramBundle) {
    this.mContext = paramContext;
    this.mExtras = paramBundle;
  }
  
  public void start(IOplusScreenshotCallback paramIOplusScreenshotCallback) {}
  
  public void stop() {}
  
  public boolean isEdit() {
    return false;
  }
}
