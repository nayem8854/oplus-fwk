package com.oplus.widget;

class OplusPagerMenuDelegate {
  private float mLastMenuOffset = -1.0F;
  
  private boolean mLastDirection = true;
  
  private boolean mNextDirection = true;
  
  private boolean mIsBeingSettled = false;
  
  private int mLastItem = -1;
  
  private int mNextItem = -1;
  
  private int mMenuScrollState = 0;
  
  private OplusViewPager.OnPageMenuChangeListener mOnPageMenuChangeListener = null;
  
  private OplusBottomMenuCallback mCallback = null;
  
  private OplusViewPager mPager = null;
  
  private static final float MENU_SCROLL_OFFSET = 0.3F;
  
  private static final float MENU_SCROLL_OFFSET_HIGH = 0.7F;
  
  private static final float MENU_SCROLL_OFFSET_LOW = 0.3F;
  
  private static final int MENU_SCROLL_STATE_DOWN = 1;
  
  private static final int MENU_SCROLL_STATE_IDLE = 0;
  
  private static final int MENU_SCROLL_STATE_OUT = 3;
  
  private static final int MENU_SCROLL_STATE_UP = 2;
  
  public OplusPagerMenuDelegate(OplusViewPager paramOplusViewPager) {
    this.mPager = paramOplusViewPager;
  }
  
  void setOnPageMenuChangeListener(OplusViewPager.OnPageMenuChangeListener paramOnPageMenuChangeListener) {
    this.mOnPageMenuChangeListener = paramOnPageMenuChangeListener;
  }
  
  void bindSplitMenuCallback(OplusBottomMenuCallback paramOplusBottomMenuCallback) {
    this.mCallback = paramOplusBottomMenuCallback;
  }
  
  void setSettleState() {
    this.mIsBeingSettled = true;
  }
  
  void onPageMenuSelected(int paramInt) {
    this.mLastItem = this.mPager.getCurrentItem();
    this.mNextItem = paramInt;
    if (this.mPager.getDragState() || this.mIsBeingSettled)
      setMenuUpdateMode(2); 
    OplusViewPager.OnPageMenuChangeListener onPageMenuChangeListener = this.mOnPageMenuChangeListener;
    if (onPageMenuChangeListener != null)
      onPageMenuChangeListener.onPageMenuSelected(paramInt); 
  }
  
  void onPageMenuScrollStateChanged(int paramInt) {
    if (this.mPager.getScrollState() == 0) {
      this.mIsBeingSettled = false;
      setMenuUpdateMode(1);
    } 
    OplusViewPager.OnPageMenuChangeListener onPageMenuChangeListener = this.mOnPageMenuChangeListener;
    if (onPageMenuChangeListener != null)
      onPageMenuChangeListener.onPageMenuScrollStateChanged(paramInt); 
  }
  
  void pageMenuScrolled(int paramInt, float paramFloat) {
    float f = getMenuOffset(paramInt, paramFloat);
    paramFloat = this.mLastMenuOffset;
    if (paramFloat != f) {
      if (f == 1.0F || f < paramFloat)
        onPageMenuScrollDataChanged(); 
      this.mLastMenuOffset = f;
    } 
    onPageMenuScrolled(-1, f);
  }
  
  void updateNextItem(float paramFloat) {
    OplusViewPager.ItemInfo itemInfo = this.mPager.infoForCurrentScrollPosition();
    if (itemInfo == null)
      return; 
    int i = itemInfo.position;
    boolean bool = this.mPager.isLayoutRtl();
    boolean bool1 = false;
    if (bool ? (paramFloat < 0.0F) : (paramFloat > 0.0F))
      bool1 = true; 
    updateDirection(bool1);
    if (this.mNextDirection) {
      this.mLastItem = i;
      this.mNextItem = Math.min(i + 1, this.mPager.getAdapter().getCount() - 1);
    } else {
      this.mLastItem = i;
      this.mNextItem = i;
    } 
  }
  
  void updateDirection(boolean paramBoolean) {
    this.mLastDirection = this.mNextDirection;
    this.mNextDirection = paramBoolean;
  }
  
  private void setMenuUpdateMode(int paramInt) {
    OplusBottomMenuCallback oplusBottomMenuCallback = this.mCallback;
    if (oplusBottomMenuCallback != null)
      oplusBottomMenuCallback.setMenuUpdateMode(paramInt); 
  }
  
  private float getMenuOffset(int paramInt, float paramFloat) {
    int i = this.mNextItem, j = this.mLastItem;
    if (i != j) {
      i = Math.min(i, j);
      j = Math.abs(this.mNextItem - this.mLastItem);
      paramFloat = (paramInt + paramFloat - i) / j;
    } 
    if (paramFloat > 0.0F && paramFloat <= 0.3F) {
      paramFloat /= 0.3F;
    } else if (paramFloat > 0.3F && paramFloat < 0.7F) {
      paramFloat = 1.0F;
    } else if (paramFloat >= 0.7F) {
      paramFloat = (1.0F - paramFloat) / 0.3F;
    } else {
      paramFloat = 0.0F;
    } 
    return paramFloat;
  }
  
  private void onPageMenuScrolled(int paramInt, float paramFloat) {
    OplusViewPager.OnPageMenuChangeListener onPageMenuChangeListener = this.mOnPageMenuChangeListener;
    if (onPageMenuChangeListener != null)
      onPageMenuChangeListener.onPageMenuScrolled(paramInt, paramFloat); 
  }
  
  private void onPageMenuScrollDataChanged() {
    OplusViewPager.OnPageMenuChangeListener onPageMenuChangeListener = this.mOnPageMenuChangeListener;
    if (onPageMenuChangeListener != null)
      onPageMenuChangeListener.onPageMenuScrollDataChanged(); 
  }
}
