package com.oplus.widget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.util.MathUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityRecord;
import android.view.animation.Interpolator;
import android.widget.EdgeEffect;
import android.widget.Scroller;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class OplusViewPager extends ViewGroup {
  private static final int[] LAYOUT_ATTRS = new int[] { 16842931 };
  
  class ItemInfo {
    Object object;
    
    float offset;
    
    int position;
    
    boolean scrolling;
    
    float widthFactor;
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("{position=");
      stringBuilder.append(this.position);
      stringBuilder.append(",widthFactor=");
      stringBuilder.append(this.widthFactor);
      stringBuilder.append(",offset=");
      stringBuilder.append(this.offset);
      stringBuilder.append(",scrolling=");
      stringBuilder.append(this.scrolling);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  static {
    COMPARATOR = (Comparator<ItemInfo>)new Object();
    sInterpolator = OplusBottomMenuCallback.ANIMATOR_INTERPOLATOR;
    sPositionComparator = new ViewPositionComparator();
    DURATION_SCALE = ValueAnimator.getDurationScale();
  }
  
  private final ArrayList<ItemInfo> mItems = new ArrayList<>();
  
  private final ItemInfo mTempItem = new ItemInfo();
  
  private final Rect mTempRect = new Rect();
  
  private int mRestoredCurItem = -1;
  
  private Parcelable mRestoredAdapterState = null;
  
  private ClassLoader mRestoredClassLoader = null;
  
  private float mFirstOffset = -3.4028235E38F;
  
  private float mLastOffset = Float.MAX_VALUE;
  
  private int mOffscreenPageLimit = 1;
  
  private int mActivePointerId = -1;
  
  private boolean mFirstLayout = true;
  
  private boolean mNeedCalculatePageOffsets = false;
  
  private final Runnable mEndScrollRunnable = (Runnable)new Object(this);
  
  private int mScrollState = 0;
  
  private static final int CLOSE_ENOUGH = 2;
  
  private static final Comparator<ItemInfo> COMPARATOR;
  
  private static final boolean DEBUG = false;
  
  private static final int DEFAULT_GUTTER_SIZE = 16;
  
  private static final int DEFAULT_OFFSCREEN_PAGES = 1;
  
  private static final int DRAW_ORDER_DEFAULT = 0;
  
  private static final int DRAW_ORDER_FORWARD = 1;
  
  private static final int DRAW_ORDER_REVERSE = 2;
  
  private static final float DURATION_SCALE;
  
  private static final int INVALID_POINTER = -1;
  
  private static final int MAX_SCROLL_X = 16777216;
  
  private static final int MAX_SETTLE_DURATION = 600;
  
  private static final int MIN_DISTANCE_FOR_FLING = 25;
  
  private static final int MIN_FLING_VELOCITY = 400;
  
  public static final int SCROLL_STATE_DRAGGING = 1;
  
  public static final int SCROLL_STATE_IDLE = 0;
  
  public static final int SCROLL_STATE_SETTLING = 2;
  
  private static final String TAG = "OplusViewPager";
  
  private static final boolean USE_CACHE = false;
  
  private static final Interpolator sInterpolator;
  
  private static final ViewPositionComparator sPositionComparator;
  
  private OplusPagerAdapter mAdapter;
  
  private OnAdapterChangeListener mAdapterChangeListener;
  
  private List<OnAdapterChangeListener> mAdapterChangeListeners;
  
  private int mBottomPageBounds;
  
  private boolean mCalledSuper;
  
  private int mChildHeightMeasureSpec;
  
  private int mChildWidthMeasureSpec;
  
  private int mCloseEnough;
  
  private int mCurItem;
  
  private int mDecorChildCount;
  
  private int mDefaultGutterSize;
  
  private boolean mDisableTouch;
  
  private int mDrawingOrder;
  
  private ArrayList<View> mDrawingOrderedChildren;
  
  private int mExpectedAdapterCount;
  
  private long mFakeDragBeginTime;
  
  private boolean mFakeDragging;
  
  private int mFlingDistance;
  
  private int mGutterSize;
  
  private boolean mIgnoreGutter;
  
  private boolean mInLayout;
  
  private float mInitialMotionX;
  
  private float mInitialMotionY;
  
  private OnPageChangeListener mInternalPageChangeListener;
  
  private boolean mIsBeingDragged;
  
  private boolean mIsUnableToDrag;
  
  private float mLastMotionX;
  
  private float mLastMotionY;
  
  private EdgeEffect mLeftEdge;
  
  private Drawable mMarginDrawable;
  
  private int mMaximumVelocity;
  
  private OplusPagerMenuDelegate mMenuDelegate;
  
  private int mMinimumVelocity;
  
  private PagerObserver mObserver;
  
  private OnPageChangeListener mOnPageChangeListener;
  
  private List<OnPageChangeListener> mOnPageChangeListeners;
  
  private int mPageMargin;
  
  private PageTransformer mPageTransformer;
  
  private boolean mPopulatePending;
  
  private EdgeEffect mRightEdge;
  
  private Scroller mScroller;
  
  private boolean mScrollingCacheEnabled;
  
  private Method mSetChildrenDrawingOrderEnabled;
  
  private int mTopPageBounds;
  
  private int mTouchSlop;
  
  private VelocityTracker mVelocityTracker;
  
  class SimpleOnPageChangeListener implements OnPageChangeListener {
    public void onPageScrolled(int param1Int1, float param1Float, int param1Int2) {}
    
    public void onPageSelected(int param1Int) {}
    
    public void onPageScrollStateChanged(int param1Int) {}
  }
  
  public OplusViewPager(Context paramContext) {
    super(paramContext);
    this.mMenuDelegate = new OplusPagerMenuDelegate(this);
    this.mDisableTouch = false;
    initViewPager();
  }
  
  public OplusViewPager(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    this.mMenuDelegate = new OplusPagerMenuDelegate(this);
    this.mDisableTouch = false;
    initViewPager();
  }
  
  void initViewPager() {
    setWillNotDraw(false);
    setDescendantFocusability(262144);
    setFocusable(true);
    Context context = getContext();
    this.mScroller = new Scroller(context, sInterpolator);
    ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
    float f = (context.getResources().getDisplayMetrics()).density;
    this.mTouchSlop = viewConfiguration.getScaledPagingTouchSlop();
    this.mMinimumVelocity = (int)(400.0F * f);
    this.mMaximumVelocity = viewConfiguration.getScaledMaximumFlingVelocity();
    this.mLeftEdge = new EdgeEffect(context);
    this.mRightEdge = new EdgeEffect(context);
    this.mFlingDistance = (int)(25.0F * f);
    this.mCloseEnough = (int)(2.0F * f);
    this.mDefaultGutterSize = (int)(16.0F * f);
    setAccessibilityDelegate(new MyAccessibilityDelegate());
    if (getImportantForAccessibility() == 0)
      setImportantForAccessibility(1); 
  }
  
  protected void onDetachedFromWindow() {
    removeCallbacks(this.mEndScrollRunnable);
    super.onDetachedFromWindow();
  }
  
  private void setScrollState(int paramInt) {
    if (this.mScrollState == paramInt)
      return; 
    this.mScrollState = paramInt;
    if (this.mPageTransformer != null) {
      boolean bool;
      if (paramInt != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      enableLayers(bool);
    } 
    OnPageChangeListener onPageChangeListener = this.mOnPageChangeListener;
    if (onPageChangeListener != null)
      onPageChangeListener.onPageScrollStateChanged(paramInt); 
    List<OnPageChangeListener> list = this.mOnPageChangeListeners;
    if (list != null) {
      byte b;
      int i;
      for (b = 0, i = list.size(); b < i; b++) {
        OnPageChangeListener onPageChangeListener1 = this.mOnPageChangeListeners.get(b);
        if (onPageChangeListener1 != null)
          onPageChangeListener1.onPageScrollStateChanged(paramInt); 
      } 
    } 
    this.mMenuDelegate.onPageMenuScrollStateChanged(this.mScrollState);
  }
  
  public void setAdapter(OplusPagerAdapter paramOplusPagerAdapter) {
    OplusPagerAdapter oplusPagerAdapter = this.mAdapter;
    if (oplusPagerAdapter != null) {
      oplusPagerAdapter.unregisterDataSetObserver(this.mObserver);
      this.mAdapter.startUpdate(this);
      for (byte b = 0; b < this.mItems.size(); b++) {
        ItemInfo itemInfo = this.mItems.get(b);
        this.mAdapter.destroyItem(this, itemInfo.position, itemInfo.object);
      } 
      this.mAdapter.finishUpdate(this);
      this.mItems.clear();
      removeNonDecorViews();
      this.mCurItem = 0;
      scrollTo(0, 0);
    } 
    oplusPagerAdapter = this.mAdapter;
    this.mAdapter = paramOplusPagerAdapter;
    this.mExpectedAdapterCount = 0;
    if (paramOplusPagerAdapter != null) {
      if (this.mObserver == null)
        this.mObserver = new PagerObserver(); 
      this.mAdapter.registerDataSetObserver(this.mObserver);
      this.mPopulatePending = false;
      boolean bool = this.mFirstLayout;
      this.mFirstLayout = true;
      this.mExpectedAdapterCount = this.mAdapter.getCount();
      if (this.mRestoredCurItem >= 0) {
        this.mAdapter.restoreState(this.mRestoredAdapterState, this.mRestoredClassLoader);
        setCurrentItemInternal(this.mRestoredCurItem, false, true);
        this.mRestoredCurItem = -1;
        this.mRestoredAdapterState = null;
        this.mRestoredClassLoader = null;
      } else if (!bool) {
        populate();
      } else {
        requestLayout();
      } 
    } 
    OnAdapterChangeListener onAdapterChangeListener = this.mAdapterChangeListener;
    if (onAdapterChangeListener != null && oplusPagerAdapter != paramOplusPagerAdapter)
      onAdapterChangeListener.onAdapterChanged(oplusPagerAdapter, paramOplusPagerAdapter); 
    if (oplusPagerAdapter != paramOplusPagerAdapter) {
      List<OnAdapterChangeListener> list = this.mAdapterChangeListeners;
      if (list != null) {
        byte b;
        int i;
        for (b = 0, i = list.size(); b < i; b++) {
          OnAdapterChangeListener onAdapterChangeListener1 = this.mAdapterChangeListeners.get(b);
          if (onAdapterChangeListener1 != null)
            onAdapterChangeListener1.onAdapterChanged(oplusPagerAdapter, paramOplusPagerAdapter); 
        } 
      } 
    } 
  }
  
  private void removeNonDecorViews() {
    for (int i = 0; i < getChildCount(); i = j + 1) {
      View view = getChildAt(i);
      LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
      int j = i;
      if (!layoutParams.isDecor) {
        removeViewAt(i);
        j = i - 1;
      } 
    } 
  }
  
  public OplusPagerAdapter getAdapter() {
    return this.mAdapter;
  }
  
  public void setOnAdapterChangeListener(OnAdapterChangeListener paramOnAdapterChangeListener) {
    this.mAdapterChangeListener = paramOnAdapterChangeListener;
  }
  
  private int getClientWidth() {
    return getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
  }
  
  public void setCurrentItem(int paramInt) {
    this.mPopulatePending = false;
    setCurrentItemInternal(paramInt, this.mFirstLayout ^ true, false);
  }
  
  public void setCurrentItem(int paramInt, boolean paramBoolean) {
    this.mPopulatePending = false;
    setCurrentItemInternal(paramInt, paramBoolean, false);
  }
  
  public int getCurrentItem() {
    return this.mCurItem;
  }
  
  boolean setCurrentItemInternal(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    return setCurrentItemInternal(paramInt, paramBoolean1, paramBoolean2, 0);
  }
  
  boolean setCurrentItemInternal(int paramInt1, boolean paramBoolean1, boolean paramBoolean2, int paramInt2) {
    OplusPagerAdapter oplusPagerAdapter = this.mAdapter;
    boolean bool = false;
    if (oplusPagerAdapter == null || oplusPagerAdapter.getCount() <= 0) {
      setScrollingCacheEnabled(false);
      return false;
    } 
    int i = MathUtils.constrain(paramInt1, 0, this.mAdapter.getCount() - 1);
    if (!paramBoolean2 && this.mCurItem == i && this.mItems.size() != 0) {
      setScrollingCacheEnabled(false);
      return false;
    } 
    this.mMenuDelegate.setSettleState();
    paramInt1 = this.mOffscreenPageLimit;
    int j = this.mCurItem;
    if (i > j + paramInt1 || i < j - paramInt1)
      for (paramInt1 = 0; paramInt1 < this.mItems.size(); paramInt1++)
        ((ItemInfo)this.mItems.get(paramInt1)).scrolling = true;  
    paramBoolean2 = bool;
    if (this.mCurItem != i)
      paramBoolean2 = true; 
    if (this.mFirstLayout) {
      this.mCurItem = i;
      this.mMenuDelegate.onPageMenuSelected(i);
      if (paramBoolean2) {
        OnPageChangeListener onPageChangeListener = this.mOnPageChangeListener;
        if (onPageChangeListener != null)
          onPageChangeListener.onPageSelected(i); 
      } 
      if (paramBoolean2) {
        List<OnPageChangeListener> list = this.mOnPageChangeListeners;
        if (list != null)
          for (paramInt1 = 0, paramInt2 = list.size(); paramInt1 < paramInt2; paramInt1++) {
            OnPageChangeListener onPageChangeListener = this.mOnPageChangeListeners.get(paramInt1);
            if (onPageChangeListener != null)
              onPageChangeListener.onPageSelected(i); 
          }  
      } 
      if (paramBoolean2) {
        OnPageChangeListener onPageChangeListener = this.mInternalPageChangeListener;
        if (onPageChangeListener != null)
          onPageChangeListener.onPageSelected(i); 
      } 
      requestLayout();
    } else {
      populate(i);
      scrollToItem(i, paramBoolean1, paramInt2, paramBoolean2);
    } 
    return true;
  }
  
  private void scrollToItem(int paramInt1, boolean paramBoolean1, int paramInt2, boolean paramBoolean2) {
    int i = getLeftEdgeForItem(paramInt1);
    if (paramBoolean1) {
      smoothScrollTo(i, 0, paramInt2);
      if (paramBoolean2) {
        OnPageChangeListener onPageChangeListener = this.mOnPageChangeListener;
        if (onPageChangeListener != null)
          onPageChangeListener.onPageSelected(paramInt1); 
      } 
      if (paramBoolean2) {
        List<OnPageChangeListener> list = this.mOnPageChangeListeners;
        if (list != null)
          for (paramInt2 = 0, i = list.size(); paramInt2 < i; paramInt2++) {
            OnPageChangeListener onPageChangeListener = this.mOnPageChangeListeners.get(paramInt2);
            if (onPageChangeListener != null)
              onPageChangeListener.onPageSelected(paramInt1); 
          }  
      } 
      if (paramBoolean2) {
        OnPageChangeListener onPageChangeListener = this.mInternalPageChangeListener;
        if (onPageChangeListener != null)
          onPageChangeListener.onPageSelected(paramInt1); 
      } 
    } else {
      if (paramBoolean2) {
        OnPageChangeListener onPageChangeListener = this.mOnPageChangeListener;
        if (onPageChangeListener != null)
          onPageChangeListener.onPageSelected(paramInt1); 
      } 
      if (paramBoolean2) {
        List<OnPageChangeListener> list = this.mOnPageChangeListeners;
        if (list != null) {
          int j;
          for (paramInt2 = 0, j = list.size(); paramInt2 < j; paramInt2++) {
            OnPageChangeListener onPageChangeListener = this.mOnPageChangeListeners.get(paramInt2);
            if (onPageChangeListener != null)
              onPageChangeListener.onPageSelected(paramInt1); 
          } 
        } 
      } 
      if (paramBoolean2) {
        OnPageChangeListener onPageChangeListener = this.mInternalPageChangeListener;
        if (onPageChangeListener != null)
          onPageChangeListener.onPageSelected(paramInt1); 
      } 
      completeScroll(false);
      scrollTo(i, 0);
      pageScrolled(i);
    } 
  }
  
  private int getLeftEdgeForItem(int paramInt) {
    ItemInfo itemInfo = infoForPosition(paramInt);
    if (itemInfo == null)
      return 0; 
    int i = getClientWidth();
    paramInt = (int)(i * MathUtils.constrain(itemInfo.offset, this.mFirstOffset, this.mLastOffset));
    if (isLayoutRtl()) {
      i = (int)(i * itemInfo.widthFactor + 0.5F);
      return 16777216 - i - paramInt;
    } 
    return paramInt;
  }
  
  public void setOnPageChangeListener(OnPageChangeListener paramOnPageChangeListener) {
    this.mOnPageChangeListener = paramOnPageChangeListener;
  }
  
  public void addOnPageChangeListener(OnPageChangeListener paramOnPageChangeListener) {
    if (this.mOnPageChangeListeners == null)
      this.mOnPageChangeListeners = new ArrayList<>(); 
    this.mOnPageChangeListeners.add(paramOnPageChangeListener);
  }
  
  public void removeOnPageChangeListener(OnPageChangeListener paramOnPageChangeListener) {
    List<OnPageChangeListener> list = this.mOnPageChangeListeners;
    if (list != null)
      list.remove(paramOnPageChangeListener); 
  }
  
  public void addOnAdapterChangeListener(OnAdapterChangeListener paramOnAdapterChangeListener) {
    if (this.mAdapterChangeListeners == null)
      this.mAdapterChangeListeners = new ArrayList<>(); 
    this.mAdapterChangeListeners.add(paramOnAdapterChangeListener);
  }
  
  public void removeOnAdapterChangeListener(OnAdapterChangeListener paramOnAdapterChangeListener) {
    List<OnAdapterChangeListener> list = this.mAdapterChangeListeners;
    if (list != null)
      list.remove(paramOnAdapterChangeListener); 
  }
  
  public void setPageTransformer(boolean paramBoolean, PageTransformer paramPageTransformer) {
    if (Build.VERSION.SDK_INT >= 11) {
      boolean bool1, bool2, bool3;
      byte b = 1;
      if (paramPageTransformer != null) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      if (this.mPageTransformer != null) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (bool1 != bool2) {
        bool3 = true;
      } else {
        bool3 = false;
      } 
      this.mPageTransformer = paramPageTransformer;
      setChildrenDrawingOrderEnabled(bool1);
      if (bool1) {
        if (paramBoolean)
          b = 2; 
        this.mDrawingOrder = b;
      } else {
        this.mDrawingOrder = 0;
      } 
      if (bool3)
        populate(); 
    } 
  }
  
  protected void setChildrenDrawingOrderEnabled(boolean paramBoolean) {
    if (Build.VERSION.SDK_INT >= 7) {
      if (this.mSetChildrenDrawingOrderEnabled == null)
        try {
          this.mSetChildrenDrawingOrderEnabled = ViewGroup.class.getDeclaredMethod("setChildrenDrawingOrderEnabled", new Class[] { boolean.class });
        } catch (NoSuchMethodException noSuchMethodException) {
          Log.e("OplusViewPager", "Can't find setChildrenDrawingOrderEnabled", noSuchMethodException);
        }  
      try {
        this.mSetChildrenDrawingOrderEnabled.invoke(this, new Object[] { Boolean.valueOf(paramBoolean) });
      } catch (Exception exception) {
        Log.e("OplusViewPager", "Error changing children drawing order", exception);
      } 
    } 
  }
  
  protected int getChildDrawingOrder(int paramInt1, int paramInt2) {
    if (this.mDrawingOrder == 2) {
      paramInt1 = paramInt1 - 1 - paramInt2;
    } else {
      paramInt1 = paramInt2;
    } 
    paramInt1 = ((LayoutParams)((View)this.mDrawingOrderedChildren.get(paramInt1)).getLayoutParams()).childIndex;
    return paramInt1;
  }
  
  OnPageChangeListener setInternalPageChangeListener(OnPageChangeListener paramOnPageChangeListener) {
    OnPageChangeListener onPageChangeListener = this.mInternalPageChangeListener;
    this.mInternalPageChangeListener = paramOnPageChangeListener;
    return onPageChangeListener;
  }
  
  public int getOffscreenPageLimit() {
    return this.mOffscreenPageLimit;
  }
  
  public void setOffscreenPageLimit(int paramInt) {
    int i = paramInt;
    if (paramInt < 1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Requested offscreen page limit ");
      stringBuilder.append(paramInt);
      stringBuilder.append(" too small; defaulting to ");
      stringBuilder.append(1);
      Log.w("OplusViewPager", stringBuilder.toString());
      i = 1;
    } 
    if (i != this.mOffscreenPageLimit) {
      this.mOffscreenPageLimit = i;
      populate();
    } 
  }
  
  public void setPageMargin(int paramInt) {
    int i = this.mPageMargin;
    this.mPageMargin = paramInt;
    int j = getWidth();
    recomputeScrollPosition(j, j, paramInt, i);
    requestLayout();
  }
  
  public int getPageMargin() {
    return this.mPageMargin;
  }
  
  public void setPageMarginDrawable(Drawable paramDrawable) {
    boolean bool;
    this.mMarginDrawable = paramDrawable;
    if (paramDrawable != null)
      refreshDrawableState(); 
    if (paramDrawable == null) {
      bool = true;
    } else {
      bool = false;
    } 
    setWillNotDraw(bool);
    invalidate();
  }
  
  public void setPageMarginDrawable(int paramInt) {
    setPageMarginDrawable(getContext().getResources().getDrawable(paramInt));
  }
  
  protected boolean verifyDrawable(Drawable paramDrawable) {
    return (super.verifyDrawable(paramDrawable) || paramDrawable == this.mMarginDrawable);
  }
  
  protected void drawableStateChanged() {
    super.drawableStateChanged();
    Drawable drawable = this.mMarginDrawable;
    if (drawable != null && drawable.isStateful())
      drawable.setState(getDrawableState()); 
  }
  
  float distanceInfluenceForSnapDuration(float paramFloat) {
    paramFloat = (float)((paramFloat - 0.5F) * 0.4712389167638204D);
    return (float)Math.sin(paramFloat);
  }
  
  void smoothScrollTo(int paramInt1, int paramInt2) {
    smoothScrollTo(paramInt1, paramInt2, 0);
  }
  
  void smoothScrollTo(int paramInt1, int paramInt2, int paramInt3) {
    if (getChildCount() == 0) {
      setScrollingCacheEnabled(false);
      return;
    } 
    int i = getScrollX();
    int j = getScrollY();
    int k = paramInt1 - i;
    paramInt2 -= j;
    if (k == 0 && paramInt2 == 0) {
      completeScroll(false);
      populate();
      setScrollState(0);
      return;
    } 
    setScrollingCacheEnabled(true);
    setScrollState(2);
    paramInt1 = getClientWidth();
    int m = paramInt1 / 2;
    float f1 = Math.min(1.0F, Math.abs(k) * 1.0F / paramInt1);
    float f2 = m, f3 = m;
    f1 = distanceInfluenceForSnapDuration(f1);
    paramInt3 = Math.abs(paramInt3);
    if (paramInt3 > 0) {
      paramInt1 = Math.round(Math.abs((f2 + f3 * f1) / paramInt3) * 1000.0F) * 4;
    } else {
      f2 = paramInt1;
      f3 = this.mAdapter.getPageWidth(this.mCurItem);
      f2 = Math.abs(k) / (this.mPageMargin + f2 * f3);
      paramInt1 = (int)((1.0F + f2) * 100.0F);
    } 
    paramInt1 = (int)(Math.min(paramInt1, 600) * DURATION_SCALE);
    this.mScroller.startScroll(i, j, k, paramInt2, paramInt1);
    postInvalidateOnAnimation();
  }
  
  ItemInfo addNewItem(int paramInt1, int paramInt2) {
    ItemInfo itemInfo = new ItemInfo();
    itemInfo.position = paramInt1;
    itemInfo.object = this.mAdapter.instantiateItem(this, paramInt1);
    itemInfo.widthFactor = this.mAdapter.getPageWidth(paramInt1);
    if (paramInt2 < 0 || paramInt2 >= this.mItems.size()) {
      this.mItems.add(itemInfo);
      return itemInfo;
    } 
    this.mItems.add(paramInt2, itemInfo);
    return itemInfo;
  }
  
  void dataSetChanged() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mAdapter : Lcom/oplus/widget/OplusPagerAdapter;
    //   4: invokevirtual getCount : ()I
    //   7: istore_1
    //   8: aload_0
    //   9: iload_1
    //   10: putfield mExpectedAdapterCount : I
    //   13: aload_0
    //   14: getfield mItems : Ljava/util/ArrayList;
    //   17: invokevirtual size : ()I
    //   20: aload_0
    //   21: getfield mOffscreenPageLimit : I
    //   24: iconst_2
    //   25: imul
    //   26: iconst_1
    //   27: iadd
    //   28: if_icmpge -> 49
    //   31: aload_0
    //   32: getfield mItems : Ljava/util/ArrayList;
    //   35: astore_2
    //   36: aload_2
    //   37: invokevirtual size : ()I
    //   40: iload_1
    //   41: if_icmpge -> 49
    //   44: iconst_1
    //   45: istore_3
    //   46: goto -> 51
    //   49: iconst_0
    //   50: istore_3
    //   51: aload_0
    //   52: getfield mCurItem : I
    //   55: istore #4
    //   57: iconst_0
    //   58: istore #5
    //   60: iconst_0
    //   61: istore #6
    //   63: iload #6
    //   65: aload_0
    //   66: getfield mItems : Ljava/util/ArrayList;
    //   69: invokevirtual size : ()I
    //   72: if_icmpge -> 308
    //   75: aload_0
    //   76: getfield mItems : Ljava/util/ArrayList;
    //   79: iload #6
    //   81: invokevirtual get : (I)Ljava/lang/Object;
    //   84: checkcast com/oplus/widget/OplusViewPager$ItemInfo
    //   87: astore_2
    //   88: aload_0
    //   89: getfield mAdapter : Lcom/oplus/widget/OplusPagerAdapter;
    //   92: aload_2
    //   93: getfield object : Ljava/lang/Object;
    //   96: invokevirtual getItemPosition : (Ljava/lang/Object;)I
    //   99: istore #7
    //   101: iload #7
    //   103: iconst_m1
    //   104: if_icmpne -> 122
    //   107: iload #4
    //   109: istore #8
    //   111: iload #5
    //   113: istore #9
    //   115: iload #6
    //   117: istore #10
    //   119: goto -> 291
    //   122: iload #7
    //   124: bipush #-2
    //   126: if_icmpne -> 235
    //   129: aload_0
    //   130: getfield mItems : Ljava/util/ArrayList;
    //   133: iload #6
    //   135: invokevirtual remove : (I)Ljava/lang/Object;
    //   138: pop
    //   139: iload #6
    //   141: iconst_1
    //   142: isub
    //   143: istore #7
    //   145: iload #5
    //   147: istore #6
    //   149: iload #5
    //   151: ifne -> 165
    //   154: aload_0
    //   155: getfield mAdapter : Lcom/oplus/widget/OplusPagerAdapter;
    //   158: aload_0
    //   159: invokevirtual startUpdate : (Landroid/view/ViewGroup;)V
    //   162: iconst_1
    //   163: istore #6
    //   165: aload_0
    //   166: getfield mAdapter : Lcom/oplus/widget/OplusPagerAdapter;
    //   169: aload_0
    //   170: aload_2
    //   171: getfield position : I
    //   174: aload_2
    //   175: getfield object : Ljava/lang/Object;
    //   178: invokevirtual destroyItem : (Landroid/view/ViewGroup;ILjava/lang/Object;)V
    //   181: iconst_1
    //   182: istore_3
    //   183: iload #4
    //   185: istore #8
    //   187: iload #6
    //   189: istore #9
    //   191: iload #7
    //   193: istore #10
    //   195: aload_0
    //   196: getfield mCurItem : I
    //   199: aload_2
    //   200: getfield position : I
    //   203: if_icmpne -> 291
    //   206: iconst_0
    //   207: aload_0
    //   208: getfield mCurItem : I
    //   211: iload_1
    //   212: iconst_1
    //   213: isub
    //   214: invokestatic min : (II)I
    //   217: invokestatic max : (II)I
    //   220: istore #8
    //   222: iconst_1
    //   223: istore_3
    //   224: iload #6
    //   226: istore #9
    //   228: iload #7
    //   230: istore #10
    //   232: goto -> 291
    //   235: iload #4
    //   237: istore #8
    //   239: iload #5
    //   241: istore #9
    //   243: iload #6
    //   245: istore #10
    //   247: aload_2
    //   248: getfield position : I
    //   251: iload #7
    //   253: if_icmpeq -> 291
    //   256: aload_2
    //   257: getfield position : I
    //   260: aload_0
    //   261: getfield mCurItem : I
    //   264: if_icmpne -> 271
    //   267: iload #7
    //   269: istore #4
    //   271: aload_2
    //   272: iload #7
    //   274: putfield position : I
    //   277: iconst_1
    //   278: istore_3
    //   279: iload #6
    //   281: istore #10
    //   283: iload #5
    //   285: istore #9
    //   287: iload #4
    //   289: istore #8
    //   291: iload #10
    //   293: iconst_1
    //   294: iadd
    //   295: istore #6
    //   297: iload #8
    //   299: istore #4
    //   301: iload #9
    //   303: istore #5
    //   305: goto -> 63
    //   308: iload #5
    //   310: ifeq -> 321
    //   313: aload_0
    //   314: getfield mAdapter : Lcom/oplus/widget/OplusPagerAdapter;
    //   317: aload_0
    //   318: invokevirtual finishUpdate : (Landroid/view/ViewGroup;)V
    //   321: aload_0
    //   322: getfield mItems : Ljava/util/ArrayList;
    //   325: getstatic com/oplus/widget/OplusViewPager.COMPARATOR : Ljava/util/Comparator;
    //   328: invokestatic sort : (Ljava/util/List;Ljava/util/Comparator;)V
    //   331: iload_3
    //   332: ifeq -> 394
    //   335: aload_0
    //   336: invokevirtual getChildCount : ()I
    //   339: istore #5
    //   341: iconst_0
    //   342: istore_3
    //   343: iload_3
    //   344: iload #5
    //   346: if_icmpge -> 381
    //   349: aload_0
    //   350: iload_3
    //   351: invokevirtual getChildAt : (I)Landroid/view/View;
    //   354: astore_2
    //   355: aload_2
    //   356: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   359: checkcast com/oplus/widget/OplusViewPager$LayoutParams
    //   362: astore_2
    //   363: aload_2
    //   364: getfield isDecor : Z
    //   367: ifne -> 375
    //   370: aload_2
    //   371: fconst_0
    //   372: putfield widthFactor : F
    //   375: iinc #3, 1
    //   378: goto -> 343
    //   381: aload_0
    //   382: iload #4
    //   384: iconst_0
    //   385: iconst_1
    //   386: invokevirtual setCurrentItemInternal : (IZZ)Z
    //   389: pop
    //   390: aload_0
    //   391: invokevirtual requestLayout : ()V
    //   394: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #983	-> 0
    //   #984	-> 8
    //   #985	-> 13
    //   #986	-> 36
    //   #987	-> 51
    //   #989	-> 57
    //   #990	-> 60
    //   #991	-> 75
    //   #992	-> 88
    //   #994	-> 101
    //   #995	-> 107
    //   #998	-> 122
    //   #999	-> 129
    //   #1000	-> 139
    //   #1002	-> 145
    //   #1003	-> 154
    //   #1004	-> 162
    //   #1007	-> 165
    //   #1008	-> 181
    //   #1010	-> 183
    //   #1012	-> 206
    //   #1013	-> 222
    //   #1018	-> 235
    //   #1019	-> 256
    //   #1021	-> 267
    //   #1024	-> 271
    //   #1025	-> 277
    //   #990	-> 291
    //   #1029	-> 308
    //   #1030	-> 313
    //   #1033	-> 321
    //   #1035	-> 331
    //   #1037	-> 335
    //   #1038	-> 341
    //   #1039	-> 349
    //   #1040	-> 355
    //   #1041	-> 363
    //   #1042	-> 370
    //   #1038	-> 375
    //   #1046	-> 381
    //   #1047	-> 390
    //   #1049	-> 394
  }
  
  void populate() {
    populate(this.mCurItem);
  }
  
  void populate(int paramInt) {
    ItemInfo itemInfo;
    byte b;
    int i = this.mCurItem;
    if (i != paramInt) {
      boolean bool;
      if (i < paramInt) {
        i = 66;
      } else {
        i = 17;
      } 
      itemInfo = infoForPosition(this.mCurItem);
      OplusPagerMenuDelegate oplusPagerMenuDelegate = this.mMenuDelegate;
      if (paramInt > this.mCurItem) {
        bool = true;
      } else {
        bool = false;
      } 
      oplusPagerMenuDelegate.updateDirection(bool);
      this.mMenuDelegate.onPageMenuSelected(paramInt);
      this.mCurItem = paramInt;
      b = i;
    } else {
      b = 2;
      itemInfo = null;
    } 
    if (this.mAdapter == null) {
      sortChildDrawingOrder();
      return;
    } 
    if (this.mPopulatePending) {
      sortChildDrawingOrder();
      return;
    } 
    if (getWindowToken() == null)
      return; 
    this.mAdapter.startUpdate(this);
    int j = this.mOffscreenPageLimit;
    int k = Math.max(0, this.mCurItem - j);
    int m = this.mAdapter.getCount();
    int n = Math.min(m - 1, this.mCurItem + j);
    if (m == this.mExpectedAdapterCount) {
      ItemInfo itemInfo1, itemInfo2 = null;
      paramInt = 0;
      while (true) {
        itemInfo1 = itemInfo2;
        if (paramInt < this.mItems.size()) {
          ItemInfo itemInfo4 = this.mItems.get(paramInt);
          if (itemInfo4.position >= this.mCurItem) {
            itemInfo1 = itemInfo2;
            if (itemInfo4.position == this.mCurItem)
              itemInfo1 = itemInfo4; 
            break;
          } 
          paramInt++;
          continue;
        } 
        break;
      } 
      ItemInfo itemInfo3 = itemInfo1;
      if (itemInfo1 == null) {
        itemInfo3 = itemInfo1;
        if (m > 0)
          itemInfo3 = addNewItem(this.mCurItem, paramInt); 
      } 
      if (itemInfo3 != null) {
        float f1 = 0.0F;
        int i1 = paramInt - 1;
        if (i1 >= 0) {
          itemInfo1 = this.mItems.get(i1);
        } else {
          itemInfo1 = null;
        } 
        int i2 = getClientWidth();
        if (i2 <= 0) {
          f2 = 0.0F;
        } else {
          f2 = 2.0F - itemInfo3.widthFactor + getPaddingLeft() / i2;
        } 
        int i3, i4;
        float f3;
        for (i3 = this.mCurItem - 1, itemInfo2 = itemInfo1, i4 = paramInt, f3 = f2; i3 >= 0; i3--, i4 = paramInt, f1 = f2, i1 = i, itemInfo2 = itemInfo1) {
          if (f1 >= f3 && i3 < k) {
            if (itemInfo2 == null)
              break; 
            paramInt = i4;
            f2 = f1;
            i = i1;
            itemInfo1 = itemInfo2;
            if (i3 == itemInfo2.position) {
              paramInt = i4;
              f2 = f1;
              i = i1;
              itemInfo1 = itemInfo2;
              if (!itemInfo2.scrolling) {
                this.mItems.remove(i1);
                this.mAdapter.destroyItem(this, i3, itemInfo2.object);
                i = i1 - 1;
                paramInt = i4 - 1;
                if (i >= 0) {
                  itemInfo1 = this.mItems.get(i);
                } else {
                  itemInfo1 = null;
                } 
                f2 = f1;
              } 
            } 
          } else if (itemInfo2 != null && i3 == itemInfo2.position) {
            f2 = f1 + itemInfo2.widthFactor;
            i = i1 - 1;
            if (i >= 0) {
              itemInfo1 = this.mItems.get(i);
            } else {
              itemInfo1 = null;
            } 
            paramInt = i4;
          } else {
            itemInfo1 = addNewItem(i3, i1 + 1);
            f2 = f1 + itemInfo1.widthFactor;
            paramInt = i4 + 1;
            if (i1 >= 0) {
              itemInfo1 = this.mItems.get(i1);
            } else {
              itemInfo1 = null;
            } 
            i = i1;
          } 
        } 
        float f2 = itemInfo3.widthFactor;
        paramInt = i4 + 1;
        if (f2 < 2.0F) {
          if (paramInt < this.mItems.size()) {
            itemInfo1 = this.mItems.get(paramInt);
          } else {
            itemInfo1 = null;
          } 
          if (i2 <= 0) {
            f3 = 0.0F;
          } else {
            f3 = getPaddingRight() / i2 + 2.0F;
          } 
          for (i1 = this.mCurItem + 1, i = j, i3 = k; i1 < m; i1++) {
            if (f2 >= f3 && i1 > n) {
              if (itemInfo1 == null)
                break; 
              if (i1 == itemInfo1.position && !itemInfo1.scrolling) {
                this.mItems.remove(paramInt);
                this.mAdapter.destroyItem(this, i1, itemInfo1.object);
                if (paramInt < this.mItems.size()) {
                  itemInfo1 = this.mItems.get(paramInt);
                } else {
                  itemInfo1 = null;
                } 
              } 
            } else if (itemInfo1 != null && i1 == itemInfo1.position) {
              f2 += itemInfo1.widthFactor;
              paramInt++;
              if (paramInt < this.mItems.size()) {
                itemInfo1 = this.mItems.get(paramInt);
              } else {
                itemInfo1 = null;
              } 
            } else {
              itemInfo1 = addNewItem(i1, paramInt);
              paramInt++;
              f2 += itemInfo1.widthFactor;
              if (paramInt < this.mItems.size()) {
                itemInfo1 = this.mItems.get(paramInt);
              } else {
                itemInfo1 = null;
              } 
            } 
          } 
        } 
        calculatePageOffsets(itemInfo3, i4, itemInfo);
      } 
      OplusPagerAdapter oplusPagerAdapter = this.mAdapter;
      paramInt = this.mCurItem;
      if (itemInfo3 != null) {
        object = itemInfo3.object;
      } else {
        itemInfo1 = null;
      } 
      oplusPagerAdapter.setPrimaryItem(this, paramInt, itemInfo1);
      this.mAdapter.finishUpdate(this);
      i = getChildCount();
      for (paramInt = 0; paramInt < i; paramInt++) {
        View view = getChildAt(paramInt);
        object = view.getLayoutParams();
        ((LayoutParams)object).childIndex = paramInt;
        if (!((LayoutParams)object).isDecor && ((LayoutParams)object).widthFactor == 0.0F) {
          ItemInfo itemInfo4 = infoForChild(view);
          if (itemInfo4 != null) {
            ((LayoutParams)object).widthFactor = itemInfo4.widthFactor;
            ((LayoutParams)object).position = itemInfo4.position;
          } 
        } 
      } 
      sortChildDrawingOrder();
      if (hasFocus()) {
        object = findFocus();
        if (object != null) {
          ItemInfo itemInfo4 = infoForAnyChild((View)object);
        } else {
          object = null;
        } 
        if (object == null || ((ItemInfo)object).position != this.mCurItem)
          for (paramInt = 0; paramInt < getChildCount(); paramInt++) {
            View view = getChildAt(paramInt);
            ItemInfo itemInfo4 = infoForChild(view);
            if (itemInfo4 != null && itemInfo4.position == this.mCurItem && view.requestFocus(b))
              break; 
          }  
      } 
      return;
    } 
    try {
      object = getResources().getResourceName(getId());
    } catch (android.content.res.Resources.NotFoundException notFoundException) {
      object = Integer.toHexString(getId());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("The application's ColorPagerAdapter changed the adapter's contents without calling ColorPagerAdapter#notifyDataSetChanged! Expected adapter item count: ");
    stringBuilder.append(this.mExpectedAdapterCount);
    stringBuilder.append(", found: ");
    stringBuilder.append(m);
    stringBuilder.append(" Pager id: ");
    stringBuilder.append((String)object);
    stringBuilder.append(" Pager class: ");
    stringBuilder.append(getClass());
    stringBuilder.append(" Problematic adapter: ");
    Object object = this.mAdapter;
    stringBuilder.append(object.getClass());
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  private void sortChildDrawingOrder() {
    if (this.mDrawingOrder != 0) {
      ArrayList<View> arrayList = this.mDrawingOrderedChildren;
      if (arrayList == null) {
        this.mDrawingOrderedChildren = new ArrayList<>();
      } else {
        arrayList.clear();
      } 
      int i = getChildCount();
      for (byte b = 0; b < i; b++) {
        View view = getChildAt(b);
        this.mDrawingOrderedChildren.add(view);
      } 
      Collections.sort(this.mDrawingOrderedChildren, sPositionComparator);
    } 
  }
  
  private void calculatePageOffsets(ItemInfo paramItemInfo1, int paramInt, ItemInfo paramItemInfo2) {
    float f1;
    int i = this.mAdapter.getCount();
    int j = getClientWidth();
    if (j > 0) {
      f1 = this.mPageMargin / j;
    } else {
      f1 = 0.0F;
    } 
    if (paramItemInfo2 != null) {
      j = paramItemInfo2.position;
      if (j < paramItemInfo1.position) {
        byte b = 0;
        f2 = paramItemInfo2.offset + paramItemInfo2.widthFactor + f1;
        j++;
        for (; j <= paramItemInfo1.position && b < this.mItems.size(); j = n + 1) {
          float f;
          int n;
          paramItemInfo2 = this.mItems.get(b);
          while (true) {
            f = f2;
            n = j;
            if (j > paramItemInfo2.position) {
              f = f2;
              n = j;
              if (b < this.mItems.size() - 1) {
                b++;
                paramItemInfo2 = this.mItems.get(b);
                continue;
              } 
            } 
            break;
          } 
          while (n < paramItemInfo2.position) {
            f += this.mAdapter.getPageWidth(n) + f1;
            n++;
          } 
          paramItemInfo2.offset = f;
          f2 = f + paramItemInfo2.widthFactor + f1;
        } 
      } else if (j > paramItemInfo1.position) {
        int n = this.mItems.size() - 1;
        f2 = paramItemInfo2.offset;
        j--;
        for (; j >= paramItemInfo1.position && n >= 0; j = i1 - 1) {
          float f;
          int i1;
          paramItemInfo2 = this.mItems.get(n);
          while (true) {
            f = f2;
            i1 = j;
            if (j < paramItemInfo2.position) {
              f = f2;
              i1 = j;
              if (n > 0) {
                n--;
                paramItemInfo2 = this.mItems.get(n);
                continue;
              } 
            } 
            break;
          } 
          while (i1 > paramItemInfo2.position) {
            f -= this.mAdapter.getPageWidth(i1) + f1;
            i1--;
          } 
          f2 = f - paramItemInfo2.widthFactor + f1;
          paramItemInfo2.offset = f2;
        } 
      } 
    } 
    int m = this.mItems.size();
    float f3 = paramItemInfo1.offset;
    j = paramItemInfo1.position - 1;
    if (paramItemInfo1.position == 0) {
      f2 = paramItemInfo1.offset;
    } else {
      f2 = -3.4028235E38F;
    } 
    this.mFirstOffset = f2;
    if (paramItemInfo1.position == i - 1) {
      f2 = paramItemInfo1.offset + paramItemInfo1.widthFactor - 1.0F;
    } else {
      f2 = Float.MAX_VALUE;
    } 
    this.mLastOffset = f2;
    int k;
    float f2;
    for (k = paramInt - 1, f2 = f3; k >= 0; k--, j--) {
      paramItemInfo2 = this.mItems.get(k);
      while (j > paramItemInfo2.position) {
        f2 -= this.mAdapter.getPageWidth(j) + f1;
        j--;
      } 
      f2 -= paramItemInfo2.widthFactor + f1;
      paramItemInfo2.offset = f2;
      if (paramItemInfo2.position == 0)
        this.mFirstOffset = f2; 
    } 
    f2 = paramItemInfo1.offset + paramItemInfo1.widthFactor + f1;
    k = paramItemInfo1.position + 1;
    for (j = paramInt + 1, paramInt = k; j < m; j++, paramInt++) {
      paramItemInfo1 = this.mItems.get(j);
      while (paramInt < paramItemInfo1.position) {
        f2 += this.mAdapter.getPageWidth(paramInt) + f1;
        paramInt++;
      } 
      if (paramItemInfo1.position == i - 1)
        this.mLastOffset = paramItemInfo1.widthFactor + f2 - 1.0F; 
      paramItemInfo1.offset = f2;
      f2 += paramItemInfo1.widthFactor + f1;
    } 
    this.mNeedCalculatePageOffsets = false;
  }
  
  public static class SavedState extends View.BaseSavedState {
    public SavedState(Parcelable param1Parcelable) {
      super(param1Parcelable);
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeInt(this.position);
      param1Parcel.writeParcelable(this.adapterState, param1Int);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("FragmentPager.SavedState{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(" position=");
      stringBuilder.append(this.position);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public static final Parcelable.ClassLoaderCreator<SavedState> CREATOR = (Parcelable.ClassLoaderCreator<SavedState>)new Object();
    
    Parcelable adapterState;
    
    ClassLoader loader;
    
    int position;
    
    SavedState(Parcel param1Parcel, ClassLoader param1ClassLoader) {
      super(param1Parcel);
      ClassLoader classLoader = param1ClassLoader;
      if (param1ClassLoader == null)
        classLoader = getClass().getClassLoader(); 
      this.position = param1Parcel.readInt();
      this.adapterState = param1Parcel.readParcelable(classLoader);
      this.loader = classLoader;
    }
  }
  
  public Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    SavedState savedState = new SavedState(parcelable);
    savedState.position = this.mCurItem;
    OplusPagerAdapter oplusPagerAdapter = this.mAdapter;
    if (oplusPagerAdapter != null)
      savedState.adapterState = oplusPagerAdapter.saveState(); 
    return (Parcelable)savedState;
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    if (!(paramParcelable instanceof SavedState)) {
      super.onRestoreInstanceState(paramParcelable);
      return;
    } 
    SavedState savedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(savedState.getSuperState());
    OplusPagerAdapter oplusPagerAdapter = this.mAdapter;
    if (oplusPagerAdapter != null) {
      oplusPagerAdapter.restoreState(savedState.adapterState, savedState.loader);
      setCurrentItemInternal(savedState.position, false, true);
    } else {
      this.mRestoredCurItem = savedState.position;
      this.mRestoredAdapterState = savedState.adapterState;
      this.mRestoredClassLoader = savedState.loader;
    } 
  }
  
  public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams) {
    ViewGroup.LayoutParams layoutParams = paramLayoutParams;
    if (!checkLayoutParams(paramLayoutParams))
      layoutParams = generateLayoutParams(paramLayoutParams); 
    paramLayoutParams = layoutParams;
    ((LayoutParams)paramLayoutParams).isDecor |= paramView instanceof Decor;
    if (this.mInLayout) {
      if (paramLayoutParams == null || !((LayoutParams)paramLayoutParams).isDecor) {
        ((LayoutParams)paramLayoutParams).needsMeasure = true;
        addViewInLayout(paramView, paramInt, layoutParams);
        return;
      } 
      throw new IllegalStateException("Cannot add pager decor view during layout");
    } 
    super.addView(paramView, paramInt, layoutParams);
  }
  
  public Object getCurrent() {
    Object object = infoForPosition(getCurrentItem());
    if (object == null) {
      object = null;
    } else {
      object = ((ItemInfo)object).object;
    } 
    return object;
  }
  
  public void removeView(View paramView) {
    if (this.mInLayout) {
      removeViewInLayout(paramView);
    } else {
      super.removeView(paramView);
    } 
  }
  
  ItemInfo infoForChild(View paramView) {
    for (byte b = 0; b < this.mItems.size(); b++) {
      ItemInfo itemInfo = this.mItems.get(b);
      if (this.mAdapter.isViewFromObject(paramView, itemInfo.object))
        return itemInfo; 
    } 
    return null;
  }
  
  ItemInfo infoForAnyChild(View paramView) {
    while (true) {
      ViewParent viewParent = paramView.getParent();
      if (viewParent != this) {
        if (viewParent == null || !(viewParent instanceof View))
          return null; 
        paramView = (View)viewParent;
        continue;
      } 
      break;
    } 
    return infoForChild(paramView);
  }
  
  ItemInfo infoForPosition(int paramInt) {
    for (byte b = 0; b < this.mItems.size(); b++) {
      ItemInfo itemInfo = this.mItems.get(b);
      if (itemInfo.position == paramInt)
        return itemInfo; 
    } 
    return null;
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    this.mFirstLayout = true;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    paramInt1 = getDefaultSize(0, paramInt1);
    paramInt2 = getDefaultSize(0, paramInt2);
    setMeasuredDimension(paramInt1, paramInt2);
    int i = getMeasuredWidth();
    int j = i / 10;
    this.mGutterSize = Math.min(j, this.mDefaultGutterSize);
    paramInt1 = i - getPaddingLeft() - getPaddingRight();
    paramInt2 = getMeasuredHeight() - getPaddingTop() - getPaddingBottom();
    int k = getChildCount();
    for (byte b = 0; b < k; b++, paramInt1 = n, paramInt2 = i1) {
      int n, i1;
      View view = getChildAt(b);
      if (view.getVisibility() != 8) {
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (layoutParams != null && layoutParams.isDecor) {
          boolean bool;
          int i4;
          n = layoutParams.gravity & 0x7;
          int i2 = layoutParams.gravity & 0x70;
          int i3 = Integer.MIN_VALUE;
          i1 = Integer.MIN_VALUE;
          if (i2 == 48 || i2 == 80) {
            i2 = 1;
          } else {
            i2 = 0;
          } 
          if (n == 3 || n == 5) {
            bool = true;
          } else {
            bool = false;
          } 
          if (i2 != 0) {
            n = 1073741824;
          } else {
            n = i3;
            if (bool) {
              i1 = 1073741824;
              n = i3;
            } 
          } 
          if (layoutParams.width != -2) {
            i4 = 1073741824;
            if (layoutParams.width != -1) {
              n = layoutParams.width;
            } else {
              n = paramInt1;
            } 
          } else {
            i3 = paramInt1;
            i4 = n;
            n = i3;
          } 
          if (layoutParams.height != -2) {
            if (layoutParams.height != -1) {
              i3 = layoutParams.height;
              i1 = 1073741824;
            } else {
              i1 = 1073741824;
              i3 = paramInt2;
            } 
          } else {
            i3 = paramInt2;
          } 
          n = View.MeasureSpec.makeMeasureSpec(n, i4);
          i1 = View.MeasureSpec.makeMeasureSpec(i3, i1);
          view.measure(n, i1);
          if (i2 != 0) {
            i1 = paramInt2 - view.getMeasuredHeight();
            n = paramInt1;
          } else {
            n = paramInt1;
            i1 = paramInt2;
            if (bool) {
              n = paramInt1 - view.getMeasuredWidth();
              i1 = paramInt2;
            } 
          } 
        } else {
          n = paramInt1;
          i1 = paramInt2;
        } 
      } else {
        i1 = paramInt2;
        n = paramInt1;
      } 
    } 
    this.mChildWidthMeasureSpec = View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824);
    this.mChildHeightMeasureSpec = View.MeasureSpec.makeMeasureSpec(paramInt2, 1073741824);
    this.mInLayout = true;
    populate();
    this.mInLayout = false;
    int m = getChildCount();
    for (paramInt2 = 0; paramInt2 < m; paramInt2++) {
      View view = getChildAt(paramInt2);
      if (view.getVisibility() != 8) {
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (layoutParams == null || !layoutParams.isDecor) {
          int n = View.MeasureSpec.makeMeasureSpec((int)(paramInt1 * layoutParams.widthFactor), 1073741824);
          view.measure(n, this.mChildHeightMeasureSpec);
        } 
      } 
    } 
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    if (paramInt1 != paramInt3) {
      paramInt2 = this.mPageMargin;
      recomputeScrollPosition(paramInt1, paramInt3, paramInt2, paramInt2);
    } 
  }
  
  private void recomputeScrollPosition(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (paramInt2 > 0 && !this.mItems.isEmpty()) {
      int i = getPaddingLeft(), j = getPaddingRight();
      int k = getPaddingLeft(), m = getPaddingRight();
      int n = getScrollX();
      float f = n / (paramInt2 - k - m + paramInt4);
      paramInt2 = (int)((paramInt1 - i - j + paramInt3) * f);
      if (isLayoutRtl()) {
        scrollToItem(this.mCurItem, false, 0, false);
      } else {
        scrollTo(paramInt2, getScrollY());
      } 
      if (!this.mScroller.isFinished()) {
        paramInt3 = (int)((this.mScroller.getDuration() - this.mScroller.timePassed()) * DURATION_SCALE);
        ItemInfo itemInfo = infoForPosition(this.mCurItem);
        this.mScroller.startScroll(paramInt2, 0, (int)(itemInfo.offset * paramInt1), 0, paramInt3);
      } 
    } else {
      float f;
      ItemInfo itemInfo = infoForPosition(this.mCurItem);
      if (itemInfo != null) {
        f = Math.min(itemInfo.offset, this.mLastOffset);
      } else {
        f = 0.0F;
      } 
      paramInt1 = (int)((paramInt1 - getPaddingLeft() - getPaddingRight()) * f);
      if (paramInt1 != getScrollX()) {
        completeScroll(false);
        scrollTo(paramInt1, getScrollY());
      } 
    } 
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    int i = getChildCount();
    int j = paramInt3 - paramInt1;
    int k = paramInt4 - paramInt2;
    paramInt1 = getPaddingLeft();
    paramInt2 = getPaddingTop();
    int m = getPaddingRight();
    paramInt4 = getPaddingBottom();
    int n = getScrollX();
    int i1 = 0;
    byte b;
    for (b = 0; b < i; b++, paramInt1 = paramInt3, paramInt2 = i5, m = i6, paramInt4 = i7, i1 = i8) {
      View view = getChildAt(b);
      paramInt3 = paramInt1;
      int i5 = paramInt2, i6 = m, i7 = paramInt4, i8 = i1;
      if (view.getVisibility() != 8) {
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (layoutParams.isDecor) {
          paramInt3 = layoutParams.gravity & 0x7;
          i6 = layoutParams.gravity & 0x70;
          if (paramInt3 != 1) {
            if (paramInt3 != 3) {
              if (paramInt3 != 5) {
                paramInt3 = paramInt1;
                i5 = paramInt1;
              } else {
                paramInt3 = j - m - view.getMeasuredWidth();
                m += view.getMeasuredWidth();
                i5 = paramInt1;
              } 
            } else {
              paramInt3 = paramInt1;
              i5 = paramInt1 + view.getMeasuredWidth();
            } 
          } else {
            paramInt3 = Math.max((j - view.getMeasuredWidth()) / 2, paramInt1);
            i5 = paramInt1;
          } 
          if (i6 != 16) {
            if (i6 != 48) {
              if (i6 != 80) {
                paramInt1 = paramInt2;
              } else {
                paramInt1 = k - paramInt4 - view.getMeasuredHeight();
                paramInt4 += view.getMeasuredHeight();
              } 
            } else {
              paramInt1 = paramInt2;
              paramInt2 += view.getMeasuredHeight();
            } 
          } else {
            paramInt1 = Math.max((k - view.getMeasuredHeight()) / 2, paramInt2);
          } 
          paramInt3 += n;
          i7 = view.getMeasuredWidth();
          i6 = view.getMeasuredHeight();
          view.layout(paramInt3, paramInt1, i7 + paramInt3, paramInt1 + i6);
          i8 = i1 + 1;
          paramInt3 = i5;
          i5 = paramInt2;
          i6 = m;
          i7 = paramInt4;
        } else {
          i8 = i1;
          i7 = paramInt4;
          i6 = m;
          i5 = paramInt2;
          paramInt3 = paramInt1;
        } 
      } 
    } 
    int i4 = j - paramInt1 - m;
    int i2, i3;
    for (b = 0, paramInt3 = n, i2 = j, i3 = i; b < i3; b++) {
      View view = getChildAt(b);
      if (view.getVisibility() != 8) {
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (!layoutParams.isDecor) {
          ItemInfo itemInfo = infoForChild(view);
          if (itemInfo != null) {
            i = (int)(i4 * itemInfo.offset);
            int i5 = paramInt1 + i;
            if (layoutParams.needsMeasure) {
              layoutParams.needsMeasure = false;
              n = View.MeasureSpec.makeMeasureSpec((int)(i4 * layoutParams.widthFactor), 1073741824);
              j = View.MeasureSpec.makeMeasureSpec(k - paramInt2 - paramInt4, 1073741824);
              view.measure(n, j);
            } 
            j = view.getMeasuredWidth();
            if (isLayoutRtl())
              i5 = 16777216 - m - i - j; 
            j = view.getMeasuredWidth();
            i = view.getMeasuredHeight();
            view.layout(i5, paramInt2, j + i5, i + paramInt2);
          } 
        } 
      } 
    } 
    this.mTopPageBounds = paramInt2;
    this.mBottomPageBounds = k - paramInt4;
    this.mDecorChildCount = i1;
    if (this.mFirstLayout)
      scrollToItem(this.mCurItem, false, 0, false); 
    this.mFirstLayout = false;
  }
  
  public void computeScroll() {
    if (!this.mScroller.isFinished() && this.mScroller.computeScrollOffset()) {
      int i = getScrollX();
      int j = getScrollY();
      int k = this.mScroller.getCurrX();
      int m = this.mScroller.getCurrY();
      if (i != k || j != m) {
        scrollTo(k, m);
        if (!pageScrolled(k)) {
          this.mScroller.abortAnimation();
          scrollTo(0, m);
        } 
      } 
      postInvalidateOnAnimation();
      return;
    } 
    completeScroll(true);
  }
  
  private boolean pageScrolled(int paramInt) {
    if (this.mItems.size() == 0) {
      this.mCalledSuper = false;
      onPageScrolled(0, 0.0F, 0);
      if (this.mCalledSuper)
        return false; 
      throw new IllegalStateException("onPageScrolled did not call superclass implementation");
    } 
    int i = paramInt;
    if (isLayoutRtl())
      i = 16777216 - paramInt - getClientWidth(); 
    ItemInfo itemInfo = infoForCurrentScrollPosition();
    int j = getClientWidth();
    int k = this.mPageMargin;
    float f = k / j;
    paramInt = itemInfo.position;
    f = (i / j - itemInfo.offset) / (itemInfo.widthFactor + f);
    i = (int)((j + k) * f);
    this.mCalledSuper = false;
    onPageScrolled(paramInt, f, i);
    this.mMenuDelegate.pageMenuScrolled(paramInt, f);
    if (this.mCalledSuper)
      return true; 
    throw new IllegalStateException("onPageScrolled did not call superclass implementation");
  }
  
  protected void onPageScrolled(int paramInt1, float paramFloat, int paramInt2) {
    if (this.mDecorChildCount > 0) {
      int i = getScrollX();
      int j = getPaddingLeft();
      int k = getPaddingRight();
      int m = getWidth();
      int n = getChildCount();
      for (byte b = 0; b < n; b++, j = i1, k = i2) {
        int i1, i2;
        View view = getChildAt(b);
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (!layoutParams.isDecor) {
          i1 = j;
          i2 = k;
        } else {
          i2 = layoutParams.gravity & 0x7;
          if (i2 != 1) {
            if (i2 != 3) {
              if (i2 != 5) {
                i2 = j;
              } else {
                i2 = m - k - view.getMeasuredWidth();
                k += view.getMeasuredWidth();
              } 
            } else {
              i2 = j;
              j += view.getWidth();
            } 
          } else {
            i2 = Math.max((m - view.getMeasuredWidth()) / 2, j);
          } 
          int i3 = i2 + i - view.getLeft();
          i1 = j;
          i2 = k;
          if (i3 != 0) {
            view.offsetLeftAndRight(i3);
            i2 = k;
            i1 = j;
          } 
        } 
      } 
    } 
    OnPageChangeListener onPageChangeListener2 = this.mOnPageChangeListener;
    if (onPageChangeListener2 != null)
      onPageChangeListener2.onPageScrolled(paramInt1, paramFloat, paramInt2); 
    List<OnPageChangeListener> list = this.mOnPageChangeListeners;
    if (list != null) {
      byte b;
      int i;
      for (b = 0, i = list.size(); b < i; b++) {
        OnPageChangeListener onPageChangeListener = this.mOnPageChangeListeners.get(b);
        if (onPageChangeListener != null)
          onPageChangeListener.onPageScrolled(paramInt1, paramFloat, paramInt2); 
      } 
    } 
    OnPageChangeListener onPageChangeListener1 = this.mInternalPageChangeListener;
    if (onPageChangeListener1 != null)
      onPageChangeListener1.onPageScrolled(paramInt1, paramFloat, paramInt2); 
    if (this.mPageTransformer != null) {
      paramInt2 = getScrollX();
      int i = getChildCount();
      for (paramInt1 = 0; paramInt1 < i; paramInt1++) {
        View view = getChildAt(paramInt1);
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (!layoutParams.isDecor) {
          paramFloat = (view.getLeft() - paramInt2) / getClientWidth();
          this.mPageTransformer.transformPage(view, paramFloat);
        } 
      } 
    } 
    this.mCalledSuper = true;
  }
  
  private void completeScroll(boolean paramBoolean) {
    boolean bool;
    if (this.mScrollState == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool) {
      setScrollingCacheEnabled(false);
      this.mScroller.abortAnimation();
      int i = getScrollX();
      int j = getScrollY();
      int k = this.mScroller.getCurrX();
      int m = this.mScroller.getCurrY();
      if (i != k || j != m)
        scrollTo(k, m); 
    } 
    this.mPopulatePending = false;
    for (byte b = 0; b < this.mItems.size(); b++) {
      ItemInfo itemInfo = this.mItems.get(b);
      if (itemInfo.scrolling) {
        bool = true;
        itemInfo.scrolling = false;
      } 
    } 
    if (bool)
      if (paramBoolean) {
        postOnAnimation(this.mEndScrollRunnable);
      } else {
        this.mEndScrollRunnable.run();
      }  
  }
  
  private boolean isGutterDrag(float paramFloat1, float paramFloat2) {
    boolean bool;
    if ((paramFloat1 < this.mGutterSize && paramFloat2 > 0.0F) || (paramFloat1 > (getWidth() - this.mGutterSize) && paramFloat2 < 0.0F)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void enableLayers(boolean paramBoolean) {
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      boolean bool;
      if (paramBoolean) {
        bool = true;
      } else {
        bool = false;
      } 
      getChildAt(b).setLayerType(bool, null);
    } 
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mDisableTouch : Z
    //   4: ifeq -> 9
    //   7: iconst_0
    //   8: ireturn
    //   9: aload_1
    //   10: invokevirtual getAction : ()I
    //   13: sipush #255
    //   16: iand
    //   17: istore_2
    //   18: iload_2
    //   19: iconst_3
    //   20: if_icmpeq -> 499
    //   23: iload_2
    //   24: iconst_1
    //   25: if_icmpne -> 31
    //   28: goto -> 499
    //   31: iload_2
    //   32: ifeq -> 53
    //   35: aload_0
    //   36: getfield mIsBeingDragged : Z
    //   39: ifeq -> 44
    //   42: iconst_1
    //   43: ireturn
    //   44: aload_0
    //   45: getfield mIsUnableToDrag : Z
    //   48: ifeq -> 53
    //   51: iconst_0
    //   52: ireturn
    //   53: iload_2
    //   54: ifeq -> 333
    //   57: iload_2
    //   58: iconst_2
    //   59: if_icmpeq -> 79
    //   62: iload_2
    //   63: bipush #6
    //   65: if_icmpeq -> 71
    //   68: goto -> 472
    //   71: aload_0
    //   72: aload_1
    //   73: invokespecial onSecondaryPointerUp : (Landroid/view/MotionEvent;)V
    //   76: goto -> 472
    //   79: aload_0
    //   80: getfield mActivePointerId : I
    //   83: istore_2
    //   84: iload_2
    //   85: iconst_m1
    //   86: if_icmpne -> 92
    //   89: goto -> 472
    //   92: aload_1
    //   93: iload_2
    //   94: invokevirtual findPointerIndex : (I)I
    //   97: istore_2
    //   98: aload_1
    //   99: iload_2
    //   100: invokevirtual getX : (I)F
    //   103: fstore_3
    //   104: fload_3
    //   105: aload_0
    //   106: getfield mLastMotionX : F
    //   109: fsub
    //   110: fstore #4
    //   112: fload #4
    //   114: invokestatic abs : (F)F
    //   117: fstore #5
    //   119: aload_1
    //   120: iload_2
    //   121: invokevirtual getY : (I)F
    //   124: fstore #6
    //   126: fload #6
    //   128: aload_0
    //   129: getfield mInitialMotionY : F
    //   132: fsub
    //   133: invokestatic abs : (F)F
    //   136: fstore #7
    //   138: fload #4
    //   140: fconst_0
    //   141: fcmpl
    //   142: ifeq -> 203
    //   145: aload_0
    //   146: aload_0
    //   147: getfield mLastMotionX : F
    //   150: fload #4
    //   152: invokespecial isGutterDrag : (FF)Z
    //   155: ifne -> 203
    //   158: fload #4
    //   160: f2i
    //   161: istore_2
    //   162: fload_3
    //   163: f2i
    //   164: istore #8
    //   166: fload #6
    //   168: f2i
    //   169: istore #9
    //   171: aload_0
    //   172: aload_0
    //   173: iconst_0
    //   174: iload_2
    //   175: iload #8
    //   177: iload #9
    //   179: invokevirtual canScroll : (Landroid/view/View;ZIII)Z
    //   182: ifeq -> 203
    //   185: aload_0
    //   186: fload_3
    //   187: putfield mLastMotionX : F
    //   190: aload_0
    //   191: fload #6
    //   193: putfield mLastMotionY : F
    //   196: aload_0
    //   197: iconst_1
    //   198: putfield mIsUnableToDrag : Z
    //   201: iconst_0
    //   202: ireturn
    //   203: fload #5
    //   205: aload_0
    //   206: getfield mTouchSlop : I
    //   209: i2f
    //   210: fcmpl
    //   211: ifle -> 295
    //   214: ldc_w 0.5
    //   217: fload #5
    //   219: fmul
    //   220: fload #7
    //   222: fcmpl
    //   223: ifle -> 295
    //   226: aload_0
    //   227: iconst_1
    //   228: putfield mIsBeingDragged : Z
    //   231: aload_0
    //   232: iconst_1
    //   233: invokespecial requestParentDisallowInterceptTouchEvent : (Z)V
    //   236: aload_0
    //   237: iconst_1
    //   238: invokespecial setScrollState : (I)V
    //   241: fload #4
    //   243: fconst_0
    //   244: fcmpl
    //   245: ifle -> 263
    //   248: aload_0
    //   249: getfield mInitialMotionX : F
    //   252: aload_0
    //   253: getfield mTouchSlop : I
    //   256: i2f
    //   257: fadd
    //   258: fstore #7
    //   260: goto -> 275
    //   263: aload_0
    //   264: getfield mInitialMotionX : F
    //   267: aload_0
    //   268: getfield mTouchSlop : I
    //   271: i2f
    //   272: fsub
    //   273: fstore #7
    //   275: aload_0
    //   276: fload #7
    //   278: putfield mLastMotionX : F
    //   281: aload_0
    //   282: fload #6
    //   284: putfield mLastMotionY : F
    //   287: aload_0
    //   288: iconst_1
    //   289: invokespecial setScrollingCacheEnabled : (Z)V
    //   292: goto -> 311
    //   295: fload #7
    //   297: aload_0
    //   298: getfield mTouchSlop : I
    //   301: i2f
    //   302: fcmpl
    //   303: ifle -> 311
    //   306: aload_0
    //   307: iconst_1
    //   308: putfield mIsUnableToDrag : Z
    //   311: aload_0
    //   312: getfield mIsBeingDragged : Z
    //   315: ifeq -> 472
    //   318: aload_0
    //   319: fload_3
    //   320: invokespecial performDrag : (F)Z
    //   323: ifeq -> 472
    //   326: aload_0
    //   327: invokevirtual postInvalidateOnAnimation : ()V
    //   330: goto -> 472
    //   333: aload_1
    //   334: invokevirtual getX : ()F
    //   337: fstore #7
    //   339: aload_0
    //   340: fload #7
    //   342: putfield mInitialMotionX : F
    //   345: aload_0
    //   346: fload #7
    //   348: putfield mLastMotionX : F
    //   351: aload_1
    //   352: invokevirtual getY : ()F
    //   355: fstore #7
    //   357: aload_0
    //   358: fload #7
    //   360: putfield mInitialMotionY : F
    //   363: aload_0
    //   364: fload #7
    //   366: putfield mLastMotionY : F
    //   369: aload_0
    //   370: aload_1
    //   371: iconst_0
    //   372: invokevirtual getPointerId : (I)I
    //   375: putfield mActivePointerId : I
    //   378: aload_0
    //   379: iconst_0
    //   380: putfield mIsUnableToDrag : Z
    //   383: aload_0
    //   384: getfield mScroller : Landroid/widget/Scroller;
    //   387: invokevirtual computeScrollOffset : ()Z
    //   390: pop
    //   391: aload_0
    //   392: getfield mScrollState : I
    //   395: iconst_2
    //   396: if_icmpne -> 462
    //   399: aload_0
    //   400: getfield mScroller : Landroid/widget/Scroller;
    //   403: astore #10
    //   405: aload #10
    //   407: invokevirtual getFinalX : ()I
    //   410: aload_0
    //   411: getfield mScroller : Landroid/widget/Scroller;
    //   414: invokevirtual getCurrX : ()I
    //   417: isub
    //   418: invokestatic abs : (I)I
    //   421: aload_0
    //   422: getfield mCloseEnough : I
    //   425: if_icmple -> 462
    //   428: aload_0
    //   429: getfield mScroller : Landroid/widget/Scroller;
    //   432: invokevirtual abortAnimation : ()V
    //   435: aload_0
    //   436: iconst_0
    //   437: putfield mPopulatePending : Z
    //   440: aload_0
    //   441: invokevirtual populate : ()V
    //   444: aload_0
    //   445: iconst_1
    //   446: putfield mIsBeingDragged : Z
    //   449: aload_0
    //   450: iconst_1
    //   451: invokespecial requestParentDisallowInterceptTouchEvent : (Z)V
    //   454: aload_0
    //   455: iconst_1
    //   456: invokespecial setScrollState : (I)V
    //   459: goto -> 472
    //   462: aload_0
    //   463: iconst_0
    //   464: invokespecial completeScroll : (Z)V
    //   467: aload_0
    //   468: iconst_0
    //   469: putfield mIsBeingDragged : Z
    //   472: aload_0
    //   473: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   476: ifnonnull -> 486
    //   479: aload_0
    //   480: invokestatic obtain : ()Landroid/view/VelocityTracker;
    //   483: putfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   486: aload_0
    //   487: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   490: aload_1
    //   491: invokevirtual addMovement : (Landroid/view/MotionEvent;)V
    //   494: aload_0
    //   495: getfield mIsBeingDragged : Z
    //   498: ireturn
    //   499: aload_0
    //   500: iconst_0
    //   501: putfield mIsBeingDragged : Z
    //   504: aload_0
    //   505: iconst_0
    //   506: putfield mIsUnableToDrag : Z
    //   509: aload_0
    //   510: iconst_m1
    //   511: putfield mActivePointerId : I
    //   514: aload_0
    //   515: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   518: astore_1
    //   519: aload_1
    //   520: ifnull -> 532
    //   523: aload_1
    //   524: invokevirtual recycle : ()V
    //   527: aload_0
    //   528: aconst_null
    //   529: putfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   532: iconst_0
    //   533: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1962	-> 0
    //   #1963	-> 7
    //   #1972	-> 9
    //   #1975	-> 18
    //   #1990	-> 31
    //   #1991	-> 35
    //   #1993	-> 42
    //   #1995	-> 44
    //   #1997	-> 51
    //   #2001	-> 53
    //   #2092	-> 71
    //   #2012	-> 79
    //   #2013	-> 84
    //   #2015	-> 89
    //   #2018	-> 92
    //   #2019	-> 98
    //   #2020	-> 104
    //   #2021	-> 112
    //   #2022	-> 119
    //   #2023	-> 126
    //   #2026	-> 138
    //   #2027	-> 171
    //   #2029	-> 185
    //   #2030	-> 190
    //   #2031	-> 196
    //   #2032	-> 201
    //   #2026	-> 203
    //   #2034	-> 203
    //   #2036	-> 226
    //   #2037	-> 231
    //   #2038	-> 236
    //   #2039	-> 241
    //   #2040	-> 263
    //   #2041	-> 281
    //   #2042	-> 287
    //   #2043	-> 295
    //   #2049	-> 306
    //   #2051	-> 311
    //   #2053	-> 318
    //   #2054	-> 326
    //   #2065	-> 333
    //   #2066	-> 351
    //   #2067	-> 369
    //   #2068	-> 378
    //   #2070	-> 383
    //   #2071	-> 391
    //   #2072	-> 405
    //   #2074	-> 428
    //   #2075	-> 435
    //   #2076	-> 440
    //   #2077	-> 444
    //   #2078	-> 449
    //   #2079	-> 454
    //   #2081	-> 462
    //   #2082	-> 467
    //   #2088	-> 472
    //   #2096	-> 472
    //   #2097	-> 479
    //   #2099	-> 486
    //   #2105	-> 494
    //   #1978	-> 499
    //   #1979	-> 504
    //   #1980	-> 509
    //   #1981	-> 514
    //   #1982	-> 523
    //   #1983	-> 527
    //   #1985	-> 532
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mDisableTouch : Z
    //   4: istore_2
    //   5: iconst_0
    //   6: istore_3
    //   7: iload_2
    //   8: ifeq -> 13
    //   11: iconst_0
    //   12: ireturn
    //   13: aload_0
    //   14: getfield mFakeDragging : Z
    //   17: ifeq -> 22
    //   20: iconst_1
    //   21: ireturn
    //   22: aload_1
    //   23: invokevirtual getAction : ()I
    //   26: ifne -> 38
    //   29: aload_1
    //   30: invokevirtual getEdgeFlags : ()I
    //   33: ifeq -> 38
    //   36: iconst_0
    //   37: ireturn
    //   38: aload_0
    //   39: getfield mAdapter : Lcom/oplus/widget/OplusPagerAdapter;
    //   42: astore #4
    //   44: aload #4
    //   46: ifnull -> 731
    //   49: aload #4
    //   51: invokevirtual getCount : ()I
    //   54: ifne -> 60
    //   57: goto -> 731
    //   60: aload_0
    //   61: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   64: ifnonnull -> 74
    //   67: aload_0
    //   68: invokestatic obtain : ()Landroid/view/VelocityTracker;
    //   71: putfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   74: aload_0
    //   75: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   78: aload_1
    //   79: invokevirtual addMovement : (Landroid/view/MotionEvent;)V
    //   82: aload_1
    //   83: invokevirtual getAction : ()I
    //   86: istore #5
    //   88: iconst_0
    //   89: istore #6
    //   91: iload #5
    //   93: sipush #255
    //   96: iand
    //   97: istore #5
    //   99: iload #5
    //   101: ifeq -> 659
    //   104: iload #5
    //   106: iconst_1
    //   107: if_icmpeq -> 463
    //   110: iload #5
    //   112: iconst_2
    //   113: if_icmpeq -> 262
    //   116: iload #5
    //   118: iconst_3
    //   119: if_icmpeq -> 192
    //   122: iload #5
    //   124: iconst_5
    //   125: if_icmpeq -> 162
    //   128: iload #5
    //   130: bipush #6
    //   132: if_icmpeq -> 138
    //   135: goto -> 720
    //   138: aload_0
    //   139: aload_1
    //   140: invokespecial onSecondaryPointerUp : (Landroid/view/MotionEvent;)V
    //   143: aload_0
    //   144: aload_1
    //   145: aload_1
    //   146: aload_0
    //   147: getfield mActivePointerId : I
    //   150: invokevirtual findPointerIndex : (I)I
    //   153: invokevirtual getX : (I)F
    //   156: putfield mLastMotionX : F
    //   159: goto -> 720
    //   162: aload_1
    //   163: invokevirtual getActionIndex : ()I
    //   166: istore_3
    //   167: aload_1
    //   168: iload_3
    //   169: invokevirtual getX : (I)F
    //   172: fstore #7
    //   174: aload_0
    //   175: fload #7
    //   177: putfield mLastMotionX : F
    //   180: aload_0
    //   181: aload_1
    //   182: iload_3
    //   183: invokevirtual getPointerId : (I)I
    //   186: putfield mActivePointerId : I
    //   189: goto -> 720
    //   192: aload_0
    //   193: getfield mIsBeingDragged : Z
    //   196: ifeq -> 720
    //   199: aload_0
    //   200: aload_0
    //   201: getfield mCurItem : I
    //   204: iconst_1
    //   205: iconst_0
    //   206: iconst_0
    //   207: invokespecial scrollToItem : (IZIZ)V
    //   210: aload_0
    //   211: iconst_m1
    //   212: putfield mActivePointerId : I
    //   215: aload_0
    //   216: invokespecial endDrag : ()V
    //   219: aload_0
    //   220: getfield mLeftEdge : Landroid/widget/EdgeEffect;
    //   223: invokevirtual onRelease : ()V
    //   226: aload_0
    //   227: getfield mRightEdge : Landroid/widget/EdgeEffect;
    //   230: invokevirtual onRelease : ()V
    //   233: aload_0
    //   234: getfield mLeftEdge : Landroid/widget/EdgeEffect;
    //   237: invokevirtual isFinished : ()Z
    //   240: ifeq -> 256
    //   243: iload_3
    //   244: istore #6
    //   246: aload_0
    //   247: getfield mRightEdge : Landroid/widget/EdgeEffect;
    //   250: invokevirtual isFinished : ()Z
    //   253: ifne -> 259
    //   256: iconst_1
    //   257: istore #6
    //   259: goto -> 720
    //   262: aload_0
    //   263: getfield mIsBeingDragged : Z
    //   266: ifne -> 425
    //   269: aload_1
    //   270: aload_0
    //   271: getfield mActivePointerId : I
    //   274: invokevirtual findPointerIndex : (I)I
    //   277: istore_3
    //   278: aload_1
    //   279: iload_3
    //   280: invokevirtual getX : (I)F
    //   283: fstore #7
    //   285: fload #7
    //   287: aload_0
    //   288: getfield mLastMotionX : F
    //   291: fsub
    //   292: invokestatic abs : (F)F
    //   295: fstore #8
    //   297: aload_1
    //   298: iload_3
    //   299: invokevirtual getY : (I)F
    //   302: fstore #9
    //   304: fload #9
    //   306: aload_0
    //   307: getfield mLastMotionY : F
    //   310: fsub
    //   311: invokestatic abs : (F)F
    //   314: fstore #10
    //   316: fload #8
    //   318: aload_0
    //   319: getfield mTouchSlop : I
    //   322: i2f
    //   323: fcmpl
    //   324: ifle -> 425
    //   327: fload #8
    //   329: fload #10
    //   331: fcmpl
    //   332: ifle -> 425
    //   335: aload_0
    //   336: iconst_1
    //   337: putfield mIsBeingDragged : Z
    //   340: aload_0
    //   341: iconst_1
    //   342: invokespecial requestParentDisallowInterceptTouchEvent : (Z)V
    //   345: aload_0
    //   346: getfield mInitialMotionX : F
    //   349: fstore #8
    //   351: fload #7
    //   353: fload #8
    //   355: fsub
    //   356: fconst_0
    //   357: fcmpl
    //   358: ifle -> 374
    //   361: fload #8
    //   363: aload_0
    //   364: getfield mTouchSlop : I
    //   367: i2f
    //   368: fadd
    //   369: fstore #7
    //   371: goto -> 384
    //   374: fload #8
    //   376: aload_0
    //   377: getfield mTouchSlop : I
    //   380: i2f
    //   381: fsub
    //   382: fstore #7
    //   384: aload_0
    //   385: fload #7
    //   387: putfield mLastMotionX : F
    //   390: aload_0
    //   391: fload #9
    //   393: putfield mLastMotionY : F
    //   396: aload_0
    //   397: iconst_1
    //   398: invokespecial setScrollState : (I)V
    //   401: aload_0
    //   402: iconst_1
    //   403: invokespecial setScrollingCacheEnabled : (Z)V
    //   406: aload_0
    //   407: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   410: astore #4
    //   412: aload #4
    //   414: ifnull -> 425
    //   417: aload #4
    //   419: iconst_1
    //   420: invokeinterface requestDisallowInterceptTouchEvent : (Z)V
    //   425: aload_0
    //   426: getfield mIsBeingDragged : Z
    //   429: ifeq -> 720
    //   432: aload_1
    //   433: aload_0
    //   434: getfield mActivePointerId : I
    //   437: invokevirtual findPointerIndex : (I)I
    //   440: istore #6
    //   442: aload_1
    //   443: iload #6
    //   445: invokevirtual getX : (I)F
    //   448: fstore #7
    //   450: iconst_0
    //   451: aload_0
    //   452: fload #7
    //   454: invokespecial performDrag : (F)Z
    //   457: ior
    //   458: istore #6
    //   460: goto -> 720
    //   463: aload_0
    //   464: getfield mIsBeingDragged : Z
    //   467: ifeq -> 720
    //   470: aload_0
    //   471: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   474: astore #4
    //   476: aload #4
    //   478: sipush #1000
    //   481: aload_0
    //   482: getfield mMaximumVelocity : I
    //   485: i2f
    //   486: invokevirtual computeCurrentVelocity : (IF)V
    //   489: aload #4
    //   491: aload_0
    //   492: getfield mActivePointerId : I
    //   495: invokevirtual getXVelocity : (I)F
    //   498: f2i
    //   499: istore #6
    //   501: aload_0
    //   502: iconst_1
    //   503: putfield mPopulatePending : Z
    //   506: aload_0
    //   507: invokespecial getClientWidth : ()I
    //   510: istore #5
    //   512: aload_0
    //   513: invokespecial getScrollStart : ()I
    //   516: istore #11
    //   518: aload_0
    //   519: invokevirtual infoForCurrentScrollPosition : ()Lcom/oplus/widget/OplusViewPager$ItemInfo;
    //   522: astore #4
    //   524: aload #4
    //   526: getfield position : I
    //   529: istore_3
    //   530: iload #11
    //   532: i2f
    //   533: iload #5
    //   535: i2f
    //   536: fdiv
    //   537: aload #4
    //   539: getfield offset : F
    //   542: fsub
    //   543: aload #4
    //   545: getfield widthFactor : F
    //   548: fdiv
    //   549: fstore #7
    //   551: aload_1
    //   552: aload_0
    //   553: getfield mActivePointerId : I
    //   556: invokevirtual findPointerIndex : (I)I
    //   559: istore #5
    //   561: aload_1
    //   562: iload #5
    //   564: invokevirtual getX : (I)F
    //   567: fstore #9
    //   569: fload #9
    //   571: aload_0
    //   572: getfield mInitialMotionX : F
    //   575: fsub
    //   576: f2i
    //   577: istore #5
    //   579: aload_0
    //   580: iload_3
    //   581: fload #7
    //   583: iload #6
    //   585: iload #5
    //   587: invokespecial determineTargetPage : (IFII)I
    //   590: istore_3
    //   591: aload_0
    //   592: iload_3
    //   593: iconst_1
    //   594: iconst_1
    //   595: iload #6
    //   597: invokevirtual setCurrentItemInternal : (IZZI)Z
    //   600: pop
    //   601: aload_0
    //   602: iconst_m1
    //   603: putfield mActivePointerId : I
    //   606: aload_0
    //   607: invokespecial endDrag : ()V
    //   610: aload_0
    //   611: getfield mLeftEdge : Landroid/widget/EdgeEffect;
    //   614: invokevirtual onRelease : ()V
    //   617: aload_0
    //   618: getfield mRightEdge : Landroid/widget/EdgeEffect;
    //   621: invokevirtual onRelease : ()V
    //   624: aload_0
    //   625: getfield mLeftEdge : Landroid/widget/EdgeEffect;
    //   628: invokevirtual isFinished : ()Z
    //   631: ifeq -> 653
    //   634: aload_0
    //   635: getfield mRightEdge : Landroid/widget/EdgeEffect;
    //   638: invokevirtual isFinished : ()Z
    //   641: ifne -> 647
    //   644: goto -> 653
    //   647: iconst_0
    //   648: istore #6
    //   650: goto -> 656
    //   653: iconst_1
    //   654: istore #6
    //   656: goto -> 720
    //   659: aload_0
    //   660: getfield mScroller : Landroid/widget/Scroller;
    //   663: invokevirtual abortAnimation : ()V
    //   666: aload_0
    //   667: iconst_0
    //   668: putfield mPopulatePending : Z
    //   671: aload_0
    //   672: invokevirtual populate : ()V
    //   675: aload_1
    //   676: invokevirtual getX : ()F
    //   679: fstore #7
    //   681: aload_0
    //   682: fload #7
    //   684: putfield mInitialMotionX : F
    //   687: aload_0
    //   688: fload #7
    //   690: putfield mLastMotionX : F
    //   693: aload_1
    //   694: invokevirtual getY : ()F
    //   697: fstore #7
    //   699: aload_0
    //   700: fload #7
    //   702: putfield mInitialMotionY : F
    //   705: aload_0
    //   706: fload #7
    //   708: putfield mLastMotionY : F
    //   711: aload_0
    //   712: aload_1
    //   713: iconst_0
    //   714: invokevirtual getPointerId : (I)I
    //   717: putfield mActivePointerId : I
    //   720: iload #6
    //   722: ifeq -> 729
    //   725: aload_0
    //   726: invokevirtual postInvalidateOnAnimation : ()V
    //   729: iconst_1
    //   730: ireturn
    //   731: iconst_0
    //   732: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2110	-> 0
    //   #2111	-> 11
    //   #2114	-> 13
    //   #2118	-> 20
    //   #2121	-> 22
    //   #2124	-> 36
    //   #2127	-> 38
    //   #2132	-> 60
    //   #2133	-> 67
    //   #2135	-> 74
    //   #2137	-> 82
    //   #2138	-> 88
    //   #2140	-> 91
    //   #2231	-> 138
    //   #2232	-> 143
    //   #2224	-> 162
    //   #2225	-> 167
    //   #2226	-> 174
    //   #2227	-> 180
    //   #2228	-> 189
    //   #2214	-> 192
    //   #2215	-> 199
    //   #2216	-> 210
    //   #2217	-> 215
    //   #2218	-> 219
    //   #2219	-> 226
    //   #2220	-> 233
    //   #2153	-> 262
    //   #2154	-> 269
    //   #2155	-> 278
    //   #2156	-> 285
    //   #2157	-> 297
    //   #2158	-> 304
    //   #2160	-> 316
    //   #2162	-> 335
    //   #2163	-> 340
    //   #2164	-> 345
    //   #2165	-> 374
    //   #2166	-> 390
    //   #2167	-> 396
    //   #2168	-> 401
    //   #2171	-> 406
    //   #2172	-> 412
    //   #2173	-> 417
    //   #2178	-> 425
    //   #2180	-> 432
    //   #2181	-> 442
    //   #2182	-> 450
    //   #2183	-> 460
    //   #2186	-> 463
    //   #2187	-> 470
    //   #2188	-> 476
    //   #2189	-> 489
    //   #2190	-> 501
    //   #2191	-> 506
    //   #2194	-> 512
    //   #2196	-> 518
    //   #2197	-> 524
    //   #2198	-> 530
    //   #2199	-> 551
    //   #2200	-> 561
    //   #2201	-> 569
    //   #2202	-> 579
    //   #2204	-> 591
    //   #2206	-> 601
    //   #2207	-> 606
    //   #2208	-> 610
    //   #2209	-> 617
    //   #2210	-> 624
    //   #2211	-> 656
    //   #2142	-> 659
    //   #2143	-> 666
    //   #2144	-> 671
    //   #2147	-> 675
    //   #2148	-> 693
    //   #2149	-> 711
    //   #2150	-> 720
    //   #2235	-> 720
    //   #2236	-> 725
    //   #2238	-> 729
    //   #2129	-> 731
  }
  
  private void requestParentDisallowInterceptTouchEvent(boolean paramBoolean) {
    ViewParent viewParent = getParent();
    if (viewParent != null)
      viewParent.requestDisallowInterceptTouchEvent(paramBoolean); 
  }
  
  private boolean performDrag(float paramFloat) {
    EdgeEffect edgeEffect1, edgeEffect2;
    boolean bool1 = false, bool2 = false, bool3 = false;
    int i = getClientWidth();
    float f1 = this.mLastMotionX - paramFloat;
    this.mLastMotionX = paramFloat;
    if (isLayoutRtl()) {
      edgeEffect1 = this.mRightEdge;
      edgeEffect2 = this.mLeftEdge;
    } else {
      edgeEffect1 = this.mLeftEdge;
      edgeEffect2 = this.mRightEdge;
    } 
    float f2 = getScrollStart();
    if (isLayoutRtl()) {
      paramFloat = -f1;
    } else {
      paramFloat = f1;
    } 
    float f3 = f2 + paramFloat;
    f2 = 0.0F;
    paramFloat = 0.0F;
    int j = 1;
    int k = 0;
    if (this.mItems.size() > 0) {
      ItemInfo itemInfo2 = this.mItems.get(0);
      k = itemInfo2.position;
      boolean bool = true;
      if (k == 0) {
        k = 1;
      } else {
        k = 0;
      } 
      j = k;
      if (j != 0) {
        f2 = itemInfo2.offset * i;
      } else {
        f2 = i * this.mFirstOffset;
      } 
      ArrayList<ItemInfo> arrayList = this.mItems;
      ItemInfo itemInfo1 = arrayList.get(arrayList.size() - 1);
      if (itemInfo1.position == this.mAdapter.getCount() - 1) {
        k = bool;
      } else {
        k = 0;
      } 
      if (k != 0) {
        paramFloat = itemInfo1.offset * i;
      } else {
        paramFloat = i * this.mLastOffset;
      } 
    } 
    if (f3 < f2) {
      bool2 = bool3;
      if (j != 0) {
        edgeEffect1.onPull(Math.abs(f1) / i);
        bool2 = true;
      } 
      paramFloat = f2;
    } else if (f3 > paramFloat) {
      bool2 = bool1;
      if (k != 0) {
        edgeEffect2.onPull(Math.abs(f1) / i);
        bool2 = true;
      } 
    } else {
      paramFloat = f3;
    } 
    if (isLayoutRtl())
      paramFloat = 1.6777216E7F - paramFloat - i; 
    this.mLastMotionX += paramFloat - (int)paramFloat;
    scrollTo((int)paramFloat, getScrollY());
    this.mMenuDelegate.updateNextItem(f1);
    pageScrolled((int)paramFloat);
    return bool2;
  }
  
  ItemInfo infoForCurrentScrollPosition() {
    float f2;
    int i = getScrollStart();
    int j = getClientWidth();
    float f1 = 0.0F;
    if (j > 0) {
      f2 = i / j;
    } else {
      f2 = 0.0F;
    } 
    if (j > 0)
      f1 = this.mPageMargin / j; 
    int k = -1;
    float f3 = 0.0F;
    float f4 = 0.0F;
    i = 1;
    ItemInfo itemInfo = null;
    for (j = 0; j < this.mItems.size(); ) {
      ItemInfo itemInfo1 = this.mItems.get(j);
      int m = j;
      ItemInfo itemInfo2 = itemInfo1;
      if (i == 0) {
        m = j;
        itemInfo2 = itemInfo1;
        if (itemInfo1.position != k + 1) {
          itemInfo2 = this.mTempItem;
          itemInfo2.offset = f3 + f4 + f1;
          itemInfo2.position = k + 1;
          itemInfo2.widthFactor = this.mAdapter.getPageWidth(itemInfo2.position);
          m = j - 1;
        } 
      } 
      f3 = itemInfo2.offset;
      f4 = itemInfo2.offset;
      if (i != 0 || f2 >= f4) {
        f4 = itemInfo2.widthFactor;
        if (f2 < f4 + f3 + f1 || m == this.mItems.size() - 1)
          return itemInfo2; 
        i = 0;
        k = itemInfo2.position;
        f4 = itemInfo2.widthFactor;
        j = m + 1;
        itemInfo = itemInfo2;
      } 
      return itemInfo;
    } 
    return itemInfo;
  }
  
  private int getScrollStart() {
    if (isLayoutRtl())
      return 16777216 - getScrollX() - getClientWidth(); 
    return getScrollX();
  }
  
  private int determineTargetPage(int paramInt1, float paramFloat, int paramInt2, int paramInt3) {
    int i;
    if (Math.abs(paramInt3) > this.mFlingDistance && Math.abs(paramInt2) > this.mMinimumVelocity) {
      if (paramInt2 > 0) {
        i = paramInt1;
      } else {
        i = paramInt1 + 1;
      } 
      if (isLayoutRtl())
        if (paramInt2 > 0) {
          i = paramInt1 + 1;
        } else {
          i = paramInt1;
        }  
    } else {
      i = this.mCurItem;
      i = (int)(paramInt1 + paramFloat + 0.5F);
    } 
    int j = i;
    if (this.mItems.size() > 0) {
      ItemInfo itemInfo1 = this.mItems.get(0);
      ArrayList<ItemInfo> arrayList = this.mItems;
      ItemInfo itemInfo2 = arrayList.get(arrayList.size() - 1);
      j = Math.max(itemInfo1.position, Math.min(i, itemInfo2.position));
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("determineTargetPage --> targetPage=");
    stringBuilder.append(j);
    stringBuilder.append(" --> currentPage=");
    stringBuilder.append(paramInt1);
    stringBuilder.append(" --> velocity=");
    stringBuilder.append(paramInt2);
    stringBuilder.append(" -->deltaX=");
    stringBuilder.append(paramInt3);
    stringBuilder.append(" -->pageOffset=");
    stringBuilder.append(paramFloat);
    stringBuilder.append(" --> mCurItem=");
    stringBuilder.append(this.mCurItem);
    Log.d("OplusViewPager", stringBuilder.toString());
    return j;
  }
  
  public void draw(Canvas paramCanvas) {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokespecial draw : (Landroid/graphics/Canvas;)V
    //   5: iconst_0
    //   6: istore_2
    //   7: iconst_0
    //   8: istore_3
    //   9: aload_0
    //   10: invokevirtual getOverScrollMode : ()I
    //   13: istore #4
    //   15: iload #4
    //   17: ifeq -> 66
    //   20: iload #4
    //   22: iconst_1
    //   23: if_icmpne -> 49
    //   26: aload_0
    //   27: getfield mAdapter : Lcom/oplus/widget/OplusPagerAdapter;
    //   30: astore #5
    //   32: aload #5
    //   34: ifnull -> 49
    //   37: aload #5
    //   39: invokevirtual getCount : ()I
    //   42: iconst_1
    //   43: if_icmple -> 49
    //   46: goto -> 66
    //   49: aload_0
    //   50: getfield mLeftEdge : Landroid/widget/EdgeEffect;
    //   53: invokevirtual finish : ()V
    //   56: aload_0
    //   57: getfield mRightEdge : Landroid/widget/EdgeEffect;
    //   60: invokevirtual finish : ()V
    //   63: goto -> 368
    //   66: aload_0
    //   67: getfield mLeftEdge : Landroid/widget/EdgeEffect;
    //   70: invokevirtual isFinished : ()Z
    //   73: ifne -> 209
    //   76: aload_1
    //   77: invokevirtual save : ()I
    //   80: istore_2
    //   81: aload_0
    //   82: invokevirtual getHeight : ()I
    //   85: aload_0
    //   86: invokevirtual getPaddingTop : ()I
    //   89: isub
    //   90: aload_0
    //   91: invokevirtual getPaddingBottom : ()I
    //   94: isub
    //   95: istore #4
    //   97: aload_0
    //   98: invokevirtual getWidth : ()I
    //   101: istore_3
    //   102: aload_0
    //   103: invokevirtual isLayoutRtl : ()Z
    //   106: ifeq -> 123
    //   109: aload_1
    //   110: aload_0
    //   111: invokevirtual getScrollX : ()I
    //   114: i2f
    //   115: aload_0
    //   116: invokevirtual getScrollY : ()I
    //   119: i2f
    //   120: invokevirtual translate : (FF)V
    //   123: aload_1
    //   124: ldc_w 270.0
    //   127: invokevirtual rotate : (F)V
    //   130: iload #4
    //   132: ineg
    //   133: aload_0
    //   134: invokevirtual getPaddingTop : ()I
    //   137: iadd
    //   138: i2f
    //   139: fstore #6
    //   141: aload_0
    //   142: invokevirtual isLayoutRtl : ()Z
    //   145: ifeq -> 166
    //   148: aload_0
    //   149: invokespecial getScrollStart : ()I
    //   152: i2f
    //   153: aload_0
    //   154: getfield mLastOffset : F
    //   157: iload_3
    //   158: i2f
    //   159: fmul
    //   160: fsub
    //   161: fstore #7
    //   163: goto -> 175
    //   166: aload_0
    //   167: getfield mFirstOffset : F
    //   170: iload_3
    //   171: i2f
    //   172: fmul
    //   173: fstore #7
    //   175: aload_1
    //   176: fload #6
    //   178: fload #7
    //   180: invokevirtual translate : (FF)V
    //   183: aload_0
    //   184: getfield mLeftEdge : Landroid/widget/EdgeEffect;
    //   187: iload #4
    //   189: iload_3
    //   190: invokevirtual setSize : (II)V
    //   193: iconst_0
    //   194: aload_0
    //   195: getfield mLeftEdge : Landroid/widget/EdgeEffect;
    //   198: aload_1
    //   199: invokevirtual draw : (Landroid/graphics/Canvas;)Z
    //   202: ior
    //   203: istore_3
    //   204: aload_1
    //   205: iload_2
    //   206: invokevirtual restoreToCount : (I)V
    //   209: iload_3
    //   210: istore_2
    //   211: aload_0
    //   212: getfield mRightEdge : Landroid/widget/EdgeEffect;
    //   215: invokevirtual isFinished : ()Z
    //   218: ifne -> 368
    //   221: aload_1
    //   222: invokevirtual save : ()I
    //   225: istore #4
    //   227: aload_0
    //   228: invokevirtual getWidth : ()I
    //   231: istore #8
    //   233: aload_0
    //   234: invokevirtual getHeight : ()I
    //   237: istore #9
    //   239: aload_0
    //   240: invokevirtual getPaddingTop : ()I
    //   243: istore #10
    //   245: aload_0
    //   246: invokevirtual getPaddingBottom : ()I
    //   249: istore_2
    //   250: aload_0
    //   251: invokevirtual isLayoutRtl : ()Z
    //   254: ifeq -> 271
    //   257: aload_1
    //   258: aload_0
    //   259: invokevirtual getScrollX : ()I
    //   262: i2f
    //   263: aload_0
    //   264: invokevirtual getScrollY : ()I
    //   267: i2f
    //   268: invokevirtual translate : (FF)V
    //   271: aload_1
    //   272: ldc_w 90.0
    //   275: invokevirtual rotate : (F)V
    //   278: aload_0
    //   279: invokevirtual getPaddingTop : ()I
    //   282: ineg
    //   283: i2f
    //   284: fstore #6
    //   286: aload_0
    //   287: invokevirtual isLayoutRtl : ()Z
    //   290: ifeq -> 314
    //   293: aload_0
    //   294: invokespecial getScrollStart : ()I
    //   297: i2f
    //   298: aload_0
    //   299: getfield mFirstOffset : F
    //   302: fconst_1
    //   303: fadd
    //   304: iload #8
    //   306: i2f
    //   307: fmul
    //   308: fsub
    //   309: fstore #7
    //   311: goto -> 327
    //   314: aload_0
    //   315: getfield mLastOffset : F
    //   318: fconst_1
    //   319: fadd
    //   320: fneg
    //   321: iload #8
    //   323: i2f
    //   324: fmul
    //   325: fstore #7
    //   327: aload_1
    //   328: fload #6
    //   330: fload #7
    //   332: invokevirtual translate : (FF)V
    //   335: aload_0
    //   336: getfield mRightEdge : Landroid/widget/EdgeEffect;
    //   339: iload #9
    //   341: iload #10
    //   343: isub
    //   344: iload_2
    //   345: isub
    //   346: iload #8
    //   348: invokevirtual setSize : (II)V
    //   351: iload_3
    //   352: aload_0
    //   353: getfield mRightEdge : Landroid/widget/EdgeEffect;
    //   356: aload_1
    //   357: invokevirtual draw : (Landroid/graphics/Canvas;)Z
    //   360: ior
    //   361: istore_2
    //   362: aload_1
    //   363: iload #4
    //   365: invokevirtual restoreToCount : (I)V
    //   368: iload_2
    //   369: ifeq -> 376
    //   372: aload_0
    //   373: invokevirtual postInvalidateOnAnimation : ()V
    //   376: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2437	-> 0
    //   #2438	-> 5
    //   #2440	-> 9
    //   #2441	-> 15
    //   #2443	-> 37
    //   #2477	-> 49
    //   #2478	-> 56
    //   #2444	-> 66
    //   #2445	-> 76
    //   #2446	-> 81
    //   #2447	-> 97
    //   #2450	-> 102
    //   #2451	-> 109
    //   #2453	-> 123
    //   #2454	-> 130
    //   #2456	-> 183
    //   #2457	-> 193
    //   #2458	-> 204
    //   #2460	-> 209
    //   #2461	-> 221
    //   #2462	-> 227
    //   #2463	-> 233
    //   #2466	-> 250
    //   #2467	-> 257
    //   #2469	-> 271
    //   #2470	-> 278
    //   #2472	-> 335
    //   #2473	-> 351
    //   #2474	-> 362
    //   #2481	-> 368
    //   #2483	-> 372
    //   #2485	-> 376
  }
  
  protected void onDraw(Canvas paramCanvas) {
    super.onDraw(paramCanvas);
    if (this.mPageMargin > 0 && this.mMarginDrawable != null && this.mItems.size() > 0 && this.mAdapter != null) {
      int i = getScrollX();
      int j = getWidth();
      float f1 = this.mPageMargin / j;
      byte b = 0;
      ItemInfo itemInfo = this.mItems.get(0);
      float f2 = itemInfo.offset;
      int k = this.mItems.size();
      int m = itemInfo.position;
      int n = ((ItemInfo)this.mItems.get(k - 1)).position;
      for (; m < n; m++) {
        ItemInfo itemInfo1;
        float f3;
        while (m > itemInfo.position && b < k) {
          ArrayList<ItemInfo> arrayList = this.mItems;
          itemInfo1 = arrayList.get(++b);
        } 
        if (m == itemInfo1.position) {
          f2 = itemInfo1.offset;
          f3 = itemInfo1.widthFactor;
        } else {
          f3 = this.mAdapter.getPageWidth(m);
        } 
        float f4 = j * f2;
        if (isLayoutRtl()) {
          f4 = 1.6777216E7F - f4 - j;
        } else {
          f4 = j * f3 + f4;
        } 
        f2 = f2 + f3 + f1;
        int i1 = this.mPageMargin;
        if (i1 + f4 > i) {
          this.mMarginDrawable.setBounds((int)f4, this.mTopPageBounds, (int)(i1 + f4 + 0.5F), this.mBottomPageBounds);
          this.mMarginDrawable.draw(paramCanvas);
        } 
        if (f4 > (i + j))
          break; 
      } 
    } 
  }
  
  public boolean beginFakeDrag() {
    if (this.mIsBeingDragged)
      return false; 
    this.mFakeDragging = true;
    setScrollState(1);
    this.mLastMotionX = 0.0F;
    this.mInitialMotionX = 0.0F;
    VelocityTracker velocityTracker = this.mVelocityTracker;
    if (velocityTracker == null) {
      this.mVelocityTracker = VelocityTracker.obtain();
    } else {
      velocityTracker.clear();
    } 
    long l = SystemClock.uptimeMillis();
    MotionEvent motionEvent = MotionEvent.obtain(l, l, 0, 0.0F, 0.0F, 0);
    this.mVelocityTracker.addMovement(motionEvent);
    motionEvent.recycle();
    this.mFakeDragBeginTime = l;
    return true;
  }
  
  public void endFakeDrag() {
    if (this.mFakeDragging) {
      VelocityTracker velocityTracker = this.mVelocityTracker;
      velocityTracker.computeCurrentVelocity(1000, this.mMaximumVelocity);
      int i = (int)velocityTracker.getXVelocity(this.mActivePointerId);
      this.mPopulatePending = true;
      int j = getClientWidth();
      int k = getScrollStart();
      ItemInfo itemInfo = infoForCurrentScrollPosition();
      int m = itemInfo.position;
      float f = (k / j - itemInfo.offset) / itemInfo.widthFactor;
      k = (int)(this.mLastMotionX - this.mInitialMotionX);
      m = determineTargetPage(m, f, i, k);
      setCurrentItemInternal(m, true, true, i);
      endDrag();
      this.mFakeDragging = false;
      return;
    } 
    throw new IllegalStateException("No fake drag in progress. Call beginFakeDrag first.");
  }
  
  public void fakeDragBy(float paramFloat) {
    if (this.mFakeDragging) {
      this.mLastMotionX += paramFloat;
      float f1 = getScrollStart();
      float f2 = f1 - paramFloat;
      int i = getClientWidth();
      f1 = i * this.mFirstOffset;
      float f3 = i * this.mLastOffset;
      float f4 = f1;
      paramFloat = f3;
      if (this.mItems.size() > 0) {
        ItemInfo itemInfo1 = this.mItems.get(0);
        ArrayList<ItemInfo> arrayList = this.mItems;
        ItemInfo itemInfo2 = arrayList.get(arrayList.size() - 1);
        if (itemInfo1.position != 0)
          f1 = itemInfo1.offset * i; 
        f4 = f1;
        paramFloat = f3;
        if (itemInfo2.position != this.mAdapter.getCount() - 1) {
          paramFloat = itemInfo2.offset * i;
          f4 = f1;
        } 
      } 
      if (f2 < f4) {
        f1 = f4;
      } else {
        f1 = f2;
        if (f2 > paramFloat)
          f1 = paramFloat; 
      } 
      this.mLastMotionX += f1 - (int)f1;
      scrollTo((int)f1, getScrollY());
      pageScrolled((int)f1);
      long l = SystemClock.uptimeMillis();
      MotionEvent motionEvent = MotionEvent.obtain(this.mFakeDragBeginTime, l, 2, this.mLastMotionX, 0.0F, 0);
      this.mVelocityTracker.addMovement(motionEvent);
      motionEvent.recycle();
      return;
    } 
    throw new IllegalStateException("No fake drag in progress. Call beginFakeDrag first.");
  }
  
  public boolean isFakeDragging() {
    return this.mFakeDragging;
  }
  
  private void onSecondaryPointerUp(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getActionIndex();
    int j = paramMotionEvent.getPointerId(i);
    if (j == this.mActivePointerId) {
      if (i == 0) {
        j = 1;
      } else {
        j = 0;
      } 
      this.mLastMotionX = paramMotionEvent.getX(j);
      this.mActivePointerId = paramMotionEvent.getPointerId(j);
      VelocityTracker velocityTracker = this.mVelocityTracker;
      if (velocityTracker != null)
        velocityTracker.clear(); 
    } 
  }
  
  private void endDrag() {
    this.mIsBeingDragged = false;
    this.mIsUnableToDrag = false;
    VelocityTracker velocityTracker = this.mVelocityTracker;
    if (velocityTracker != null) {
      velocityTracker.recycle();
      this.mVelocityTracker = null;
    } 
  }
  
  private void setScrollingCacheEnabled(boolean paramBoolean) {
    if (this.mScrollingCacheEnabled != paramBoolean)
      this.mScrollingCacheEnabled = paramBoolean; 
  }
  
  public boolean canScrollHorizontally(int paramInt) {
    OplusPagerAdapter oplusPagerAdapter = this.mAdapter;
    boolean bool1 = false, bool2 = false;
    if (oplusPagerAdapter == null)
      return false; 
    int i = getClientWidth();
    int j = getScrollX();
    if (paramInt < 0) {
      bool1 = bool2;
      if (j > (int)(i * this.mFirstOffset))
        bool1 = true; 
      return bool1;
    } 
    if (paramInt > 0) {
      if (j < (int)(i * this.mLastOffset))
        bool1 = true; 
      return bool1;
    } 
    return false;
  }
  
  protected boolean canScroll(View paramView, boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3) {
    boolean bool = paramView instanceof ViewGroup;
    boolean bool1 = true;
    if (bool) {
      ViewGroup viewGroup = (ViewGroup)paramView;
      int i = getScrollStart();
      int j = paramView.getScrollY();
      int k = viewGroup.getChildCount();
      for (; --k >= 0; k--) {
        View view = viewGroup.getChildAt(k);
        if (!isLayoutRtl() && paramInt2 + i >= view.getLeft() && paramInt2 + i < view.getRight() && paramInt3 + j >= view.getTop() && paramInt3 + j < view.getBottom()) {
          int m = view.getLeft();
          int n = view.getTop();
          if (canScroll(view, true, paramInt1, paramInt2 + i - m, paramInt3 + j - n))
            return true; 
        } 
      } 
    } 
    if (paramBoolean && paramView.canScrollHorizontally(-paramInt1)) {
      paramBoolean = bool1;
    } else {
      paramBoolean = false;
    } 
    return paramBoolean;
  }
  
  public boolean isLayoutRtl() {
    int i = Build.VERSION.SDK_INT;
    boolean bool = false;
    if (i > 16) {
      if (getLayoutDirection() == 1)
        bool = true; 
      return bool;
    } 
    return false;
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent) {
    return (super.dispatchKeyEvent(paramKeyEvent) || executeKeyEvent(paramKeyEvent));
  }
  
  public boolean executeKeyEvent(KeyEvent paramKeyEvent) {
    boolean bool1 = false;
    boolean bool2 = bool1;
    if (paramKeyEvent.getAction() == 0) {
      int i = paramKeyEvent.getKeyCode();
      if (i != 21) {
        if (i != 22) {
          if (i != 61) {
            bool2 = bool1;
          } else {
            bool2 = bool1;
            if (Build.VERSION.SDK_INT >= 11)
              if (paramKeyEvent.hasNoModifiers()) {
                bool2 = arrowScroll(2);
              } else {
                bool2 = bool1;
                if (paramKeyEvent.hasModifiers(1))
                  bool2 = arrowScroll(1); 
              }  
          } 
        } else {
          bool2 = arrowScroll(66);
        } 
      } else {
        bool2 = arrowScroll(17);
      } 
    } 
    return bool2;
  }
  
  public boolean arrowScroll(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual findFocus : ()Landroid/view/View;
    //   4: astore_2
    //   5: aload_2
    //   6: aload_0
    //   7: if_acmpne -> 15
    //   10: aconst_null
    //   11: astore_3
    //   12: goto -> 174
    //   15: aload_2
    //   16: astore_3
    //   17: aload_2
    //   18: ifnull -> 174
    //   21: iconst_0
    //   22: istore #4
    //   24: aload_2
    //   25: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   28: astore_3
    //   29: iload #4
    //   31: istore #5
    //   33: aload_3
    //   34: instanceof android/view/ViewGroup
    //   37: ifeq -> 61
    //   40: aload_3
    //   41: aload_0
    //   42: if_acmpne -> 51
    //   45: iconst_1
    //   46: istore #5
    //   48: goto -> 61
    //   51: aload_3
    //   52: invokeinterface getParent : ()Landroid/view/ViewParent;
    //   57: astore_3
    //   58: goto -> 29
    //   61: aload_2
    //   62: astore_3
    //   63: iload #5
    //   65: ifne -> 174
    //   68: new java/lang/StringBuilder
    //   71: dup
    //   72: invokespecial <init> : ()V
    //   75: astore #6
    //   77: aload #6
    //   79: aload_2
    //   80: invokevirtual getClass : ()Ljava/lang/Class;
    //   83: invokevirtual getSimpleName : ()Ljava/lang/String;
    //   86: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   89: pop
    //   90: aload_2
    //   91: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   94: astore_3
    //   95: aload_3
    //   96: instanceof android/view/ViewGroup
    //   99: ifeq -> 134
    //   102: aload #6
    //   104: ldc_w ' => '
    //   107: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   110: pop
    //   111: aload #6
    //   113: aload_3
    //   114: invokevirtual getClass : ()Ljava/lang/Class;
    //   117: invokevirtual getSimpleName : ()Ljava/lang/String;
    //   120: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   123: pop
    //   124: aload_3
    //   125: invokeinterface getParent : ()Landroid/view/ViewParent;
    //   130: astore_3
    //   131: goto -> 95
    //   134: new java/lang/StringBuilder
    //   137: dup
    //   138: invokespecial <init> : ()V
    //   141: astore_3
    //   142: aload_3
    //   143: ldc_w 'arrowScroll tried to find focus based on non-child current focused view '
    //   146: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   149: pop
    //   150: aload_3
    //   151: aload #6
    //   153: invokevirtual toString : ()Ljava/lang/String;
    //   156: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   159: pop
    //   160: aload_3
    //   161: invokevirtual toString : ()Ljava/lang/String;
    //   164: astore_3
    //   165: ldc 'OplusViewPager'
    //   167: aload_3
    //   168: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   171: pop
    //   172: aconst_null
    //   173: astore_3
    //   174: iconst_0
    //   175: istore #7
    //   177: iconst_0
    //   178: istore #8
    //   180: invokestatic getInstance : ()Landroid/view/FocusFinder;
    //   183: aload_0
    //   184: aload_3
    //   185: iload_1
    //   186: invokevirtual findNextFocus : (Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;
    //   189: astore_2
    //   190: aload_2
    //   191: ifnull -> 325
    //   194: aload_2
    //   195: aload_3
    //   196: if_acmpeq -> 325
    //   199: iload_1
    //   200: bipush #17
    //   202: if_icmpne -> 262
    //   205: aload_0
    //   206: aload_0
    //   207: getfield mTempRect : Landroid/graphics/Rect;
    //   210: aload_2
    //   211: invokespecial getChildRectInPagerCoordinates : (Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;
    //   214: getfield left : I
    //   217: istore #5
    //   219: aload_0
    //   220: aload_0
    //   221: getfield mTempRect : Landroid/graphics/Rect;
    //   224: aload_3
    //   225: invokespecial getChildRectInPagerCoordinates : (Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;
    //   228: getfield left : I
    //   231: istore #4
    //   233: aload_3
    //   234: ifnull -> 253
    //   237: iload #5
    //   239: iload #4
    //   241: if_icmplt -> 253
    //   244: aload_0
    //   245: invokevirtual pageLeft : ()Z
    //   248: istore #8
    //   250: goto -> 259
    //   253: aload_2
    //   254: invokevirtual requestFocus : ()Z
    //   257: istore #8
    //   259: goto -> 369
    //   262: iload_1
    //   263: bipush #66
    //   265: if_icmpne -> 259
    //   268: aload_0
    //   269: aload_0
    //   270: getfield mTempRect : Landroid/graphics/Rect;
    //   273: aload_2
    //   274: invokespecial getChildRectInPagerCoordinates : (Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;
    //   277: getfield left : I
    //   280: istore #4
    //   282: aload_0
    //   283: aload_0
    //   284: getfield mTempRect : Landroid/graphics/Rect;
    //   287: aload_3
    //   288: invokespecial getChildRectInPagerCoordinates : (Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;
    //   291: getfield left : I
    //   294: istore #5
    //   296: aload_3
    //   297: ifnull -> 316
    //   300: iload #4
    //   302: iload #5
    //   304: if_icmpgt -> 316
    //   307: aload_0
    //   308: invokevirtual pageRight : ()Z
    //   311: istore #8
    //   313: goto -> 322
    //   316: aload_2
    //   317: invokevirtual requestFocus : ()Z
    //   320: istore #8
    //   322: goto -> 369
    //   325: iload_1
    //   326: bipush #17
    //   328: if_icmpeq -> 363
    //   331: iload_1
    //   332: iconst_1
    //   333: if_icmpne -> 339
    //   336: goto -> 363
    //   339: iload_1
    //   340: bipush #66
    //   342: if_icmpeq -> 354
    //   345: iload #7
    //   347: istore #8
    //   349: iload_1
    //   350: iconst_2
    //   351: if_icmpne -> 369
    //   354: aload_0
    //   355: invokevirtual pageRight : ()Z
    //   358: istore #8
    //   360: goto -> 369
    //   363: aload_0
    //   364: invokevirtual pageLeft : ()Z
    //   367: istore #8
    //   369: iload #8
    //   371: ifeq -> 382
    //   374: aload_0
    //   375: iload_1
    //   376: invokestatic getContantForFocusDirection : (I)I
    //   379: invokevirtual playSoundEffect : (I)V
    //   382: iload #8
    //   384: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2820	-> 0
    //   #2821	-> 5
    //   #2822	-> 10
    //   #2823	-> 15
    //   #2824	-> 21
    //   #2825	-> 24
    //   #2827	-> 40
    //   #2828	-> 45
    //   #2829	-> 48
    //   #2826	-> 51
    //   #2832	-> 61
    //   #2834	-> 68
    //   #2835	-> 77
    //   #2836	-> 90
    //   #2838	-> 102
    //   #2837	-> 124
    //   #2840	-> 134
    //   #2841	-> 150
    //   #2840	-> 165
    //   #2842	-> 172
    //   #2846	-> 174
    //   #2848	-> 180
    //   #2850	-> 190
    //   #2851	-> 199
    //   #2854	-> 205
    //   #2855	-> 219
    //   #2856	-> 233
    //   #2857	-> 244
    //   #2859	-> 253
    //   #2861	-> 259
    //   #2864	-> 268
    //   #2865	-> 282
    //   #2866	-> 296
    //   #2867	-> 307
    //   #2869	-> 316
    //   #2871	-> 322
    //   #2872	-> 325
    //   #2875	-> 339
    //   #2877	-> 354
    //   #2874	-> 363
    //   #2879	-> 369
    //   #2880	-> 374
    //   #2882	-> 382
  }
  
  private Rect getChildRectInPagerCoordinates(Rect paramRect, View paramView) {
    Rect rect = paramRect;
    if (paramRect == null)
      rect = new Rect(); 
    if (paramView == null) {
      rect.set(0, 0, 0, 0);
      return rect;
    } 
    rect.left = paramView.getLeft();
    rect.right = paramView.getRight();
    rect.top = paramView.getTop();
    rect.bottom = paramView.getBottom();
    ViewParent viewParent = paramView.getParent();
    while (viewParent instanceof ViewGroup && viewParent != this) {
      ViewGroup viewGroup = (ViewGroup)viewParent;
      rect.left += viewGroup.getLeft();
      rect.right += viewGroup.getRight();
      rect.top += viewGroup.getTop();
      rect.bottom += viewGroup.getBottom();
      ViewParent viewParent1 = viewGroup.getParent();
    } 
    return rect;
  }
  
  boolean pageLeft() {
    byte b = -1;
    if (isLayoutRtl())
      b = 1; 
    return setCurrentItemInternal(this.mCurItem + b, true, false);
  }
  
  boolean pageRight() {
    byte b = -1;
    if (isLayoutRtl())
      b = 1; 
    return setCurrentItemInternal(this.mCurItem - b, true, false);
  }
  
  public void addFocusables(ArrayList<View> paramArrayList, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual size : ()I
    //   4: istore #4
    //   6: aload_0
    //   7: invokevirtual getDescendantFocusability : ()I
    //   10: istore #5
    //   12: iload #5
    //   14: ldc_w 393216
    //   17: if_icmpeq -> 87
    //   20: iconst_0
    //   21: istore #6
    //   23: iload #6
    //   25: aload_0
    //   26: invokevirtual getChildCount : ()I
    //   29: if_icmpge -> 87
    //   32: aload_0
    //   33: iload #6
    //   35: invokevirtual getChildAt : (I)Landroid/view/View;
    //   38: astore #7
    //   40: aload #7
    //   42: invokevirtual getVisibility : ()I
    //   45: ifne -> 81
    //   48: aload_0
    //   49: aload #7
    //   51: invokevirtual infoForChild : (Landroid/view/View;)Lcom/oplus/widget/OplusViewPager$ItemInfo;
    //   54: astore #8
    //   56: aload #8
    //   58: ifnull -> 81
    //   61: aload #8
    //   63: getfield position : I
    //   66: aload_0
    //   67: getfield mCurItem : I
    //   70: if_icmpne -> 81
    //   73: aload #7
    //   75: aload_1
    //   76: iload_2
    //   77: iload_3
    //   78: invokevirtual addFocusables : (Ljava/util/ArrayList;II)V
    //   81: iinc #6, 1
    //   84: goto -> 23
    //   87: iload #5
    //   89: ldc_w 262144
    //   92: if_icmpne -> 104
    //   95: iload #4
    //   97: aload_1
    //   98: invokevirtual size : ()I
    //   101: if_icmpne -> 144
    //   104: aload_0
    //   105: invokevirtual isFocusable : ()Z
    //   108: ifne -> 112
    //   111: return
    //   112: iload_3
    //   113: iconst_1
    //   114: iand
    //   115: iconst_1
    //   116: if_icmpne -> 134
    //   119: aload_0
    //   120: invokevirtual isInTouchMode : ()Z
    //   123: ifeq -> 134
    //   126: aload_0
    //   127: invokevirtual isFocusableInTouchMode : ()Z
    //   130: ifne -> 134
    //   133: return
    //   134: aload_1
    //   135: ifnull -> 144
    //   138: aload_1
    //   139: aload_0
    //   140: invokevirtual add : (Ljava/lang/Object;)Z
    //   143: pop
    //   144: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2937	-> 0
    //   #2939	-> 6
    //   #2941	-> 12
    //   #2942	-> 20
    //   #2943	-> 32
    //   #2944	-> 40
    //   #2945	-> 48
    //   #2946	-> 56
    //   #2947	-> 73
    //   #2942	-> 81
    //   #2957	-> 87
    //   #2960	-> 95
    //   #2963	-> 104
    //   #2964	-> 111
    //   #2966	-> 112
    //   #2967	-> 119
    //   #2968	-> 133
    //   #2970	-> 134
    //   #2971	-> 138
    //   #2974	-> 144
  }
  
  public void addTouchables(ArrayList<View> paramArrayList) {
    for (byte b = 0; b < getChildCount(); b++) {
      View view = getChildAt(b);
      if (view.getVisibility() == 0) {
        ItemInfo itemInfo = infoForChild(view);
        if (itemInfo != null && itemInfo.position == this.mCurItem)
          view.addTouchables(paramArrayList); 
      } 
    } 
  }
  
  protected boolean onRequestFocusInDescendants(int paramInt, Rect paramRect) {
    int j;
    byte b;
    int i = getChildCount();
    if ((paramInt & 0x2) != 0) {
      j = 0;
      b = 1;
    } else {
      j = i - 1;
      b = -1;
      i = -1;
    } 
    for (; j != i; j += b) {
      View view = getChildAt(j);
      if (view.getVisibility() == 0) {
        ItemInfo itemInfo = infoForChild(view);
        if (itemInfo != null && itemInfo.position == this.mCurItem && view.requestFocus(paramInt, paramRect))
          return true; 
      } 
    } 
    return false;
  }
  
  public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    if (paramAccessibilityEvent.getEventType() == 4096)
      return super.dispatchPopulateAccessibilityEvent(paramAccessibilityEvent); 
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = getChildAt(b);
      if (view.getVisibility() == 0) {
        ItemInfo itemInfo = infoForChild(view);
        if (itemInfo != null && itemInfo.position == this.mCurItem && view.dispatchPopulateAccessibilityEvent(paramAccessibilityEvent))
          return true; 
      } 
    } 
    return false;
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
    return new LayoutParams();
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return generateDefaultLayoutParams();
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    boolean bool;
    if (paramLayoutParams instanceof LayoutParams && super.checkLayoutParams(paramLayoutParams)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  class MyAccessibilityDelegate extends View.AccessibilityDelegate {
    final OplusViewPager this$0;
    
    public void onInitializeAccessibilityEvent(View param1View, AccessibilityEvent param1AccessibilityEvent) {
      super.onInitializeAccessibilityEvent(param1View, param1AccessibilityEvent);
      param1AccessibilityEvent.setClassName(OplusViewPager.class.getName());
      AccessibilityRecord accessibilityRecord = AccessibilityRecord.obtain();
      accessibilityRecord.setScrollable(canScroll());
      if (param1AccessibilityEvent.getEventType() == 4096) {
        OplusViewPager oplusViewPager = OplusViewPager.this;
        if (oplusViewPager.mAdapter != null) {
          accessibilityRecord.setItemCount(OplusViewPager.this.mAdapter.getCount());
          accessibilityRecord.setFromIndex(OplusViewPager.this.mCurItem);
          accessibilityRecord.setToIndex(OplusViewPager.this.mCurItem);
        } 
      } 
    }
    
    public void onInitializeAccessibilityNodeInfo(View param1View, AccessibilityNodeInfo param1AccessibilityNodeInfo) {
      super.onInitializeAccessibilityNodeInfo(param1View, param1AccessibilityNodeInfo);
      param1AccessibilityNodeInfo.setClassName(OplusViewPager.class.getName());
      param1AccessibilityNodeInfo.setScrollable(canScroll());
      if (OplusViewPager.this.canScrollHorizontally(1))
        param1AccessibilityNodeInfo.addAction(4096); 
      if (OplusViewPager.this.canScrollHorizontally(-1))
        param1AccessibilityNodeInfo.addAction(8192); 
    }
    
    public boolean performAccessibilityAction(View param1View, int param1Int, Bundle param1Bundle) {
      if (super.performAccessibilityAction(param1View, param1Int, param1Bundle))
        return true; 
      if (param1Int != 4096) {
        if (param1Int != 8192)
          return false; 
        if (OplusViewPager.this.canScrollHorizontally(-1)) {
          OplusViewPager oplusViewPager = OplusViewPager.this;
          oplusViewPager.setCurrentItem(oplusViewPager.mCurItem - 1);
          return true;
        } 
        return false;
      } 
      if (OplusViewPager.this.canScrollHorizontally(1)) {
        OplusViewPager oplusViewPager = OplusViewPager.this;
        oplusViewPager.setCurrentItem(oplusViewPager.mCurItem + 1);
        return true;
      } 
      return false;
    }
    
    private boolean canScroll() {
      OplusPagerAdapter oplusPagerAdapter = OplusViewPager.this.mAdapter;
      boolean bool = true;
      if (oplusPagerAdapter == null || OplusViewPager.this.mAdapter.getCount() <= 1)
        bool = false; 
      return bool;
    }
  }
  
  class PagerObserver extends DataSetObserver {
    final OplusViewPager this$0;
    
    private PagerObserver() {}
    
    public void onChanged() {
      OplusViewPager.this.dataSetChanged();
    }
    
    public void onInvalidated() {
      OplusViewPager.this.dataSetChanged();
    }
  }
  
  class LayoutParams extends ViewGroup.LayoutParams {
    int childIndex;
    
    public int gravity;
    
    public boolean isDecor;
    
    boolean needsMeasure;
    
    int position;
    
    float widthFactor = 0.0F;
    
    public LayoutParams() {
      super(-1, -1);
    }
    
    public LayoutParams(OplusViewPager this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
      TypedArray typedArray = this$0.obtainStyledAttributes(param1AttributeSet, OplusViewPager.LAYOUT_ATTRS);
      this.gravity = typedArray.getInteger(0, 48);
      typedArray.recycle();
    }
  }
  
  class ViewPositionComparator implements Comparator<View> {
    public int compare(View param1View1, View param1View2) {
      OplusViewPager.LayoutParams layoutParams1 = (OplusViewPager.LayoutParams)param1View1.getLayoutParams();
      OplusViewPager.LayoutParams layoutParams2 = (OplusViewPager.LayoutParams)param1View2.getLayoutParams();
      if (layoutParams1.isDecor != layoutParams2.isDecor) {
        byte b;
        if (layoutParams1.isDecor) {
          b = 1;
        } else {
          b = -1;
        } 
        return b;
      } 
      return layoutParams1.position - layoutParams2.position;
    }
  }
  
  public void setOnPageMenuChangeListener(OnPageMenuChangeListener paramOnPageMenuChangeListener) {
    this.mMenuDelegate.setOnPageMenuChangeListener(paramOnPageMenuChangeListener);
  }
  
  public void setDisableTouchEvent(boolean paramBoolean) {
    this.mDisableTouch = paramBoolean;
  }
  
  public void bindSplitMenuCallback(OplusBottomMenuCallback paramOplusBottomMenuCallback) {
    this.mMenuDelegate.bindSplitMenuCallback(paramOplusBottomMenuCallback);
  }
  
  int getScrollState() {
    return this.mScrollState;
  }
  
  boolean getDragState() {
    return this.mIsBeingDragged;
  }
  
  class Decor {}
  
  class OnAdapterChangeListener {
    public abstract void onAdapterChanged(OplusPagerAdapter param1OplusPagerAdapter1, OplusPagerAdapter param1OplusPagerAdapter2);
  }
  
  class OnPageChangeListener {
    public abstract void onPageScrollStateChanged(int param1Int);
    
    public abstract void onPageScrolled(int param1Int1, float param1Float, int param1Int2);
    
    public abstract void onPageSelected(int param1Int);
  }
  
  class OnPageMenuChangeListener {
    public abstract void onPageMenuScrollDataChanged();
    
    public abstract void onPageMenuScrollStateChanged(int param1Int);
    
    public abstract void onPageMenuScrolled(int param1Int, float param1Float);
    
    public abstract void onPageMenuSelected(int param1Int);
  }
  
  class PageTransformer {
    public abstract void transformPage(View param1View, float param1Float);
  }
}
