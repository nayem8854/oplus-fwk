package com.oplus.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class OplusAutofillSaveTitleTextView extends TextView {
  public OplusAutofillSaveTitleTextView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public OplusAutofillSaveTitleTextView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public OplusAutofillSaveTitleTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    initAttrs();
  }
  
  private void initAttrs() {
    setTextColor(getResources().getColor(201719832));
  }
}
