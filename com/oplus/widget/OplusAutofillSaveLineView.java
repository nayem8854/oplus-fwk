package com.oplus.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public class OplusAutofillSaveLineView extends View {
  public OplusAutofillSaveLineView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public OplusAutofillSaveLineView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public OplusAutofillSaveLineView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    initBackground();
  }
  
  private void initBackground() {
    setBackgroundColor(getResources().getColor(201719833));
  }
}
