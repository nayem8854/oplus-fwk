package com.oplus.widget.popupwindow;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import com.oplus.widget.OplusMaxLinearLayout;
import java.util.ArrayList;
import java.util.List;

public class OplusPopupListWindow extends PopupWindow implements View.OnLayoutChangeListener {
  private Point mCoordinate = new Point();
  
  private int[] mTempLocation = new int[2];
  
  private int[] mWindowLocationOnScreen = new int[2];
  
  private int[] mPopupWindowOffset = new int[4];
  
  private Animation.AnimationListener mAnimationListener = new Animation.AnimationListener() {
      final OplusPopupListWindow this$0;
      
      public void onAnimationStart(Animation param1Animation) {
        OplusPopupListWindow.access$002(OplusPopupListWindow.this, true);
      }
      
      public void onAnimationEnd(Animation param1Animation) {
        OplusPopupListWindow.this.dismissPopupWindow();
        OplusPopupListWindow.access$002(OplusPopupListWindow.this, false);
      }
      
      public void onAnimationRepeat(Animation param1Animation) {}
    };
  
  private BaseAdapter mAdapter;
  
  private int mAlphaAnimationDuration;
  
  private Interpolator mAlphaAnimationInterpolator;
  
  private View mAnchor;
  
  private Rect mAnchorRect;
  
  private Rect mBackgroundPaddingRect;
  
  private FrameLayout mBottomPanel;
  
  private LinearLayout mContentPanel;
  
  private ViewGroup mContentView;
  
  private Context mContext;
  
  private BaseAdapter mCustomAdapter;
  
  private Rect mDecorViewRect;
  
  private BaseAdapter mDefaultAdapter;
  
  private boolean mHasHorizontalSpace;
  
  private boolean mHasVerticalSpace;
  
  private boolean mIsDismissing;
  
  private List<PopupListItem> mItemList;
  
  private OplusMaxLinearLayout mListContainer;
  
  private ListView mListView;
  
  private ListView mListViewUsedToMeasure;
  
  private AdapterView.OnItemClickListener mOnItemClickListener;
  
  private Rect mParentRectOnScreen;
  
  private float mPivotX;
  
  private float mPivotY;
  
  private int mPopupListMaxHeight;
  
  private int mPopupListMaxWidth;
  
  private int mPopupListSelection;
  
  private int mPopupListSelectionScrollOffset;
  
  private int mPopupListWindowMinWidth;
  
  private int mScaleAnimationDuration;
  
  private Interpolator mScaleAnimationInterpolator;
  
  private boolean mShowAboveFirst;
  
  public OplusPopupListWindow(Context paramContext) {
    super(paramContext);
    this.mContext = paramContext;
    this.mItemList = new ArrayList<>();
    this.mScaleAnimationDuration = paramContext.getResources().getInteger(202178584);
    this.mAlphaAnimationDuration = paramContext.getResources().getInteger(202178584);
    Interpolator interpolator = AnimationUtils.loadInterpolator(paramContext, 202375185);
    this.mScaleAnimationInterpolator = interpolator;
    this.mPopupListWindowMinWidth = paramContext.getResources().getDimensionPixelSize(201654583);
    this.mPopupListSelectionScrollOffset = paramContext.getResources().getDimensionPixelSize(201654584);
    ListView listView = new ListView(paramContext);
    listView.setDivider(null);
    this.mListViewUsedToMeasure.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
    this.mContentView = createContentView(paramContext);
    setBackgroundDrawable((Drawable)new ColorDrawable(0));
    setClippingEnabled(false);
    if (Build.VERSION.SDK_INT > 23) {
      setExitTransition(null);
      setEnterTransition(null);
    } 
  }
  
  private ViewGroup createContentView(Context paramContext) {
    FrameLayout frameLayout = (FrameLayout)LayoutInflater.from(paramContext).inflate(202440731, null);
    this.mListView = (ListView)frameLayout.findViewById(201457763);
    this.mListContainer = (OplusMaxLinearLayout)frameLayout.findViewById(201457762);
    this.mBottomPanel = (FrameLayout)frameLayout.findViewById(201457678);
    this.mContentPanel = (LinearLayout)frameLayout.findViewById(201457710);
    Drawable drawable = paramContext.getResources().getDrawable(201851323);
    Rect rect = new Rect();
    drawable.getPadding(rect);
    frameLayout.setBackground(drawable);
    return (ViewGroup)frameLayout;
  }
  
  public void show(View paramView) {
    if (paramView == null || (this.mDefaultAdapter == null && this.mCustomAdapter == null) || isShowing())
      return; 
    this.mAnchor = paramView;
    paramView.getRootView().removeOnLayoutChangeListener(this);
    this.mAnchor.getRootView().addOnLayoutChangeListener(this);
    BaseAdapter baseAdapter = this.mCustomAdapter;
    if (baseAdapter == null) {
      this.mAdapter = this.mDefaultAdapter;
    } else {
      this.mAdapter = baseAdapter;
    } 
    this.mListView.setAdapter((ListAdapter)this.mAdapter);
    int i = this.mPopupListSelection;
    if (i > 0)
      this.mListView.setSelectionFromTop(i, this.mPopupListSelectionScrollOffset); 
    AdapterView.OnItemClickListener onItemClickListener = this.mOnItemClickListener;
    if (onItemClickListener != null)
      this.mListView.setOnItemClickListener(onItemClickListener); 
    this.mDecorViewRect = new Rect();
    this.mAnchorRect = new Rect();
    this.mParentRectOnScreen = new Rect();
    this.mAnchor.getWindowVisibleDisplayFrame(this.mDecorViewRect);
    this.mAnchor.getGlobalVisibleRect(this.mAnchorRect);
    this.mAnchor.getRootView().getGlobalVisibleRect(this.mParentRectOnScreen);
    Rect rect = this.mAnchorRect;
    rect.left -= this.mPopupWindowOffset[0];
    rect = this.mAnchorRect;
    rect.top -= this.mPopupWindowOffset[1];
    rect = this.mAnchorRect;
    rect.right += this.mPopupWindowOffset[2];
    rect = this.mAnchorRect;
    rect.bottom += this.mPopupWindowOffset[3];
    this.mAnchor.getRootView().getLocationOnScreen(this.mTempLocation);
    rect = this.mAnchorRect;
    int[] arrayOfInt = this.mTempLocation;
    rect.offset(arrayOfInt[0], arrayOfInt[1]);
    rect = this.mParentRectOnScreen;
    arrayOfInt = this.mTempLocation;
    rect.offset(arrayOfInt[0], arrayOfInt[1]);
    rect = this.mDecorViewRect;
    rect.left = Math.max(rect.left, this.mParentRectOnScreen.left);
    rect = this.mDecorViewRect;
    rect.top = Math.max(rect.top, this.mParentRectOnScreen.top);
    rect = this.mDecorViewRect;
    rect.right = Math.min(rect.right, this.mParentRectOnScreen.right);
    rect = this.mDecorViewRect;
    rect.bottom = Math.min(rect.bottom, this.mParentRectOnScreen.bottom);
    this.mPopupListMaxWidth = this.mListContainer.getMaxWidth();
    this.mPopupListMaxHeight = this.mListContainer.getMaxHeight();
    calculateWindowLocation();
    measurePopupWindow();
    calculateCoordinate();
    if (!this.mHasVerticalSpace || !this.mHasHorizontalSpace)
      return; 
    setContentView((View)this.mContentView);
    calculatePivot();
    animateEnter();
    showAtLocation(this.mAnchor, 0, this.mCoordinate.x, this.mCoordinate.y);
  }
  
  private void calculateWindowLocation() {
    this.mAnchor.getRootView().getLocationOnScreen(this.mTempLocation);
    int arrayOfInt[] = this.mTempLocation, i = arrayOfInt[0];
    int j = arrayOfInt[1];
    this.mAnchor.getRootView().getLocationInWindow(this.mTempLocation);
    arrayOfInt = this.mTempLocation;
    int k = arrayOfInt[0];
    int m = arrayOfInt[1];
    arrayOfInt = this.mWindowLocationOnScreen;
    arrayOfInt[0] = i - k;
    arrayOfInt[1] = j - m;
  }
  
  public void measurePopupWindow() {
    BaseAdapter baseAdapter = this.mAdapter;
    int i = 0;
    int j = 0;
    View view = null;
    int k = 0;
    int m = View.MeasureSpec.makeMeasureSpec(getPopupWindowMaxWidth(), -2147483648);
    int n = View.MeasureSpec.makeMeasureSpec(0, 0);
    int i1 = baseAdapter.getCount();
    int i2, i3;
    for (i2 = 0, i3 = n; i2 < i1; i2++, j = k, k = i5) {
      int i4 = baseAdapter.getItemViewType(i2);
      int i5 = k;
      if (i4 != k) {
        i5 = i4;
        view = null;
      } 
      view = baseAdapter.getView(i2, view, (ViewGroup)this.mListViewUsedToMeasure);
      AbsListView.LayoutParams layoutParams1 = (AbsListView.LayoutParams)view.getLayoutParams();
      if (layoutParams1.height != -2)
        i3 = View.MeasureSpec.makeMeasureSpec(layoutParams1.height, 1073741824); 
      view.measure(m, i3);
      i4 = view.getMeasuredWidth();
      int i6 = view.getMeasuredHeight();
      k = i;
      if (i4 > i)
        k = i4; 
      i = k;
      if (k > this.mPopupListMaxWidth)
        i = this.mPopupListMaxWidth; 
      j += i6;
      k = j;
      if (j > this.mPopupListMaxHeight)
        k = this.mPopupListMaxHeight; 
    } 
    this.mBottomPanel.measure(m, n);
    i2 = this.mBottomPanel.getMeasuredHeight();
    i3 = this.mContentPanel.getPaddingTop();
    k = this.mContentPanel.getPaddingBottom();
    ViewGroup.LayoutParams layoutParams = this.mListContainer.getLayoutParams();
    layoutParams.height = j;
    this.mListContainer.setLayoutParams(layoutParams);
    setWidth(Math.max(i, this.mPopupListWindowMinWidth) + this.mBackgroundPaddingRect.left + this.mBackgroundPaddingRect.right);
    setHeight(this.mBackgroundPaddingRect.top + 0 + j + i2 + i3 + k + this.mBackgroundPaddingRect.bottom);
  }
  
  private int getPopupWindowMaxWidth() {
    return this.mDecorViewRect.right - this.mDecorViewRect.left - this.mBackgroundPaddingRect.left - this.mBackgroundPaddingRect.right;
  }
  
  private void calculateCoordinate() {
    boolean bool;
    this.mHasHorizontalSpace = true;
    this.mHasVerticalSpace = true;
    if (this.mDecorViewRect.right - this.mDecorViewRect.left < getWidth()) {
      this.mHasHorizontalSpace = false;
      return;
    } 
    int i = Math.min(this.mAnchorRect.centerX() - getWidth() / 2, this.mDecorViewRect.right - getWidth());
    int j = Math.max(this.mDecorViewRect.left, i), k = this.mWindowLocationOnScreen[0];
    int m = this.mAnchorRect.top - this.mDecorViewRect.top;
    int n = this.mDecorViewRect.bottom - this.mAnchorRect.bottom;
    int i1 = getHeight();
    if (m >= i1) {
      i = 1;
    } else {
      i = 0;
    } 
    if (n >= i1) {
      bool = true;
    } else {
      bool = false;
    } 
    int i2 = this.mAnchorRect.top - i1;
    i1 = this.mAnchorRect.bottom;
    if (n <= 0 && m <= 0) {
      this.mHasVerticalSpace = false;
      return;
    } 
    if (this.mShowAboveFirst ? (i != 0) : bool) {
      if (this.mShowAboveFirst) {
        i = i2;
      } else {
        i = i1;
      } 
    } else if (this.mShowAboveFirst ? bool : (i != 0)) {
      if (this.mShowAboveFirst) {
        i = i1;
      } else {
        i = i2;
      } 
    } else if (m > n) {
      i = this.mDecorViewRect.top;
      setHeight(m);
    } else {
      i = this.mAnchorRect.bottom;
      setHeight(n);
    } 
    this.mCoordinate.set(j - k, i - this.mWindowLocationOnScreen[1]);
  }
  
  private void calculatePivot() {
    if (this.mAnchorRect.centerX() - this.mWindowLocationOnScreen[0] - this.mCoordinate.x >= getWidth()) {
      this.mPivotX = 1.0F;
    } else {
      this.mPivotX = (this.mAnchorRect.centerX() - this.mWindowLocationOnScreen[0] - this.mCoordinate.x) / getWidth();
    } 
    if (this.mCoordinate.y >= this.mAnchorRect.top - this.mWindowLocationOnScreen[1]) {
      this.mPivotY = 0.0F;
    } else {
      this.mPivotY = 1.0F;
    } 
  }
  
  private void animateEnter() {
    ScaleAnimation scaleAnimation = new ScaleAnimation(0.5F, 1.0F, 0.5F, 1.0F, 1, this.mPivotX, 1, this.mPivotY);
    AlphaAnimation alphaAnimation = new AlphaAnimation(0.0F, 1.0F);
    scaleAnimation.setDuration(this.mScaleAnimationDuration);
    scaleAnimation.setInterpolator(this.mScaleAnimationInterpolator);
    alphaAnimation.setDuration(this.mAlphaAnimationDuration);
    alphaAnimation.setInterpolator(this.mAlphaAnimationInterpolator);
    AnimationSet animationSet = new AnimationSet(false);
    animationSet.addAnimation((Animation)scaleAnimation);
    animationSet.addAnimation((Animation)alphaAnimation);
    this.mContentView.startAnimation((Animation)animationSet);
  }
  
  private void animateExit() {
    ScaleAnimation scaleAnimation = new ScaleAnimation(1.0F, 0.5F, 1.0F, 0.5F, 1, this.mPivotX, 1, this.mPivotY);
    AlphaAnimation alphaAnimation = new AlphaAnimation(1.0F, 0.0F);
    scaleAnimation.setDuration(this.mScaleAnimationDuration);
    scaleAnimation.setInterpolator(this.mScaleAnimationInterpolator);
    alphaAnimation.setDuration(this.mAlphaAnimationDuration);
    alphaAnimation.setInterpolator(this.mAlphaAnimationInterpolator);
    AnimationSet animationSet = new AnimationSet(false);
    animationSet.addAnimation((Animation)scaleAnimation);
    animationSet.addAnimation((Animation)alphaAnimation);
    animationSet.setAnimationListener(this.mAnimationListener);
    this.mContentView.startAnimation((Animation)animationSet);
  }
  
  public void onLayoutChange(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8) {
    Rect rect1 = new Rect(paramInt1, paramInt2, paramInt3, paramInt4);
    Rect rect2 = new Rect(paramInt5, paramInt6, paramInt7, paramInt8);
    if (isShowing() && !rect1.equals(rect2))
      dismissPopupWindow(); 
  }
  
  private void dismissPopupWindow() {
    super.dismiss();
    this.mIsDismissing = false;
    this.mAnchor.getRootView().removeOnLayoutChangeListener(this);
    setContentView(null);
  }
  
  public void dismiss() {
    dismiss(true);
  }
  
  public void dismiss(boolean paramBoolean) {
    if (this.mIsDismissing)
      return; 
    if (!isShowing())
      return; 
    if (paramBoolean) {
      animateExit();
    } else {
      dismissPopupWindow();
    } 
  }
  
  public List<PopupListItem> getItemList() {
    return this.mItemList;
  }
  
  public void setItemList(List<PopupListItem> paramList) {
    if (paramList != null) {
      this.mItemList = paramList;
      this.mDefaultAdapter = new DefaultAdapter(this.mContext, paramList);
    } 
  }
  
  public void setOnItemClickListener(AdapterView.OnItemClickListener paramOnItemClickListener) {
    this.mOnItemClickListener = paramOnItemClickListener;
  }
  
  public void setAdapter(BaseAdapter paramBaseAdapter) {
    this.mCustomAdapter = paramBaseAdapter;
  }
  
  public void setSelection(int paramInt) {
    this.mPopupListSelection = paramInt;
  }
  
  public ListView getListView() {
    return this.mListView;
  }
  
  public void setListPortraitMaxWidth(int paramInt) {
    this.mListContainer.setPortraitMaxWidth(paramInt);
  }
  
  public void setListPortraitMaxHeight(int paramInt) {
    this.mListContainer.setPortraitMaxHeight(paramInt);
  }
  
  public void setListLandscapeMaxWidth(int paramInt) {
    this.mListContainer.setLandscapeMaxWidth(paramInt);
  }
  
  public void setListLandscapeMaxHeight(int paramInt) {
    this.mListContainer.setLandscapeMaxHeight(paramInt);
  }
  
  public void addBottomPanel(View paramView) {
    this.mBottomPanel.addView(paramView);
  }
  
  public void setDismissTouchOutside(boolean paramBoolean) {
    if (paramBoolean) {
      setTouchable(true);
      setFocusable(true);
      setOutsideTouchable(true);
    } else {
      setFocusable(false);
      setOutsideTouchable(false);
    } 
    update();
  }
  
  public void setOffset(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    int[] arrayOfInt = this.mPopupWindowOffset;
    arrayOfInt[0] = paramInt1;
    arrayOfInt[1] = paramInt2;
    arrayOfInt[2] = paramInt3;
    arrayOfInt[3] = paramInt4;
  }
  
  public void showAboveFirst(boolean paramBoolean) {
    this.mShowAboveFirst = paramBoolean;
  }
}
