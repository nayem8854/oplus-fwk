package com.oplus.widget.popupwindow;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Xfermode;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.oplus.internal.R;

public class RoundFrameLayout extends FrameLayout {
  private Paint mPaint;
  
  private Path mPath;
  
  private float mRadius;
  
  private RectF mRectF;
  
  public RoundFrameLayout(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public RoundFrameLayout(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public RoundFrameLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.RoundFrameLayout);
    this.mRadius = typedArray.getDimension(0, 0.0F);
    typedArray.recycle();
    this.mPath = new Path();
    this.mPaint = new Paint(1);
    this.mRectF = new RectF();
    this.mPaint.setXfermode((Xfermode)new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
  }
  
  public void setRadius(float paramFloat) {
    this.mRadius = paramFloat;
    postInvalidate();
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    this.mRectF.set(getPaddingLeft(), getPaddingTop(), (paramInt1 - getPaddingRight()), (paramInt2 - getPaddingBottom()));
  }
  
  protected void dispatchDraw(Canvas paramCanvas) {
    if (Build.VERSION.SDK_INT >= 28) {
      dispatchDraw28(paramCanvas);
    } else {
      dispatchDraw27(paramCanvas);
    } 
  }
  
  private void dispatchDraw27(Canvas paramCanvas) {
    paramCanvas.saveLayer(this.mRectF, null, 31);
    super.dispatchDraw(paramCanvas);
    paramCanvas.drawPath(genPath(), this.mPaint);
    paramCanvas.restore();
  }
  
  private void dispatchDraw28(Canvas paramCanvas) {
    paramCanvas.save();
    paramCanvas.clipPath(genPath());
    super.dispatchDraw(paramCanvas);
    paramCanvas.restore();
  }
  
  private Path genPath() {
    this.mPath.reset();
    Path path = this.mPath;
    RectF rectF = this.mRectF;
    float f = this.mRadius;
    path.addRoundRect(rectF, f, f, Path.Direction.CW);
    return this.mPath;
  }
}
