package com.oplus.widget.popupwindow;

import android.graphics.drawable.Drawable;

public class PopupListItem {
  private Drawable mIcon;
  
  private int mIconId;
  
  private boolean mIsEnable;
  
  private String mTitle;
  
  public PopupListItem(int paramInt, String paramString, boolean paramBoolean) {
    this.mIconId = paramInt;
    this.mTitle = paramString;
    this.mIsEnable = paramBoolean;
  }
  
  public PopupListItem(String paramString, boolean paramBoolean) {
    this((Drawable)null, paramString, paramBoolean);
  }
  
  public PopupListItem(Drawable paramDrawable, String paramString, boolean paramBoolean) {
    this.mIcon = paramDrawable;
    this.mTitle = paramString;
    this.mIsEnable = paramBoolean;
  }
  
  public int getIconId() {
    return this.mIconId;
  }
  
  public void setIconId(int paramInt) {
    this.mIconId = paramInt;
  }
  
  public Drawable getIcon() {
    return this.mIcon;
  }
  
  public void setIcon(Drawable paramDrawable) {
    this.mIcon = paramDrawable;
  }
  
  public String getTitle() {
    return this.mTitle;
  }
  
  public void setTitle(String paramString) {
    this.mTitle = paramString;
  }
  
  public boolean isEnable() {
    return this.mIsEnable;
  }
  
  public void setEnable(boolean paramBoolean) {
    this.mIsEnable = paramBoolean;
  }
}
