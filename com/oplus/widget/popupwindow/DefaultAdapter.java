package com.oplus.widget.popupwindow;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.oplus.util.OplusChangeTextUtil;
import java.util.List;

public class DefaultAdapter extends BaseAdapter {
  private Context mContext;
  
  private List<PopupListItem> mItemList;
  
  private int mPopupListItemMinHeight;
  
  private int mPopupListItemPaddingVertical;
  
  private int mPopupListPaddingVertical;
  
  public DefaultAdapter(Context paramContext, List<PopupListItem> paramList) {
    this.mContext = paramContext;
    this.mItemList = paramList;
    Resources resources = paramContext.getResources();
    this.mPopupListPaddingVertical = resources.getDimensionPixelSize(201654571);
    this.mPopupListItemPaddingVertical = resources.getDimensionPixelSize(201654577);
    this.mPopupListItemMinHeight = resources.getDimensionPixelSize(201654576);
  }
  
  public int getCount() {
    return this.mItemList.size();
  }
  
  public Object getItem(int paramInt) {
    return this.mItemList.get(paramInt);
  }
  
  public long getItemId(int paramInt) {
    return paramInt;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    View view;
    ViewHolder viewHolder;
    if (paramView == null) {
      ViewHolder viewHolder1 = new ViewHolder();
      View view1 = LayoutInflater.from(this.mContext).inflate(202440730, paramViewGroup, false);
      viewHolder1.mIcon = (ImageView)view1.findViewById(201457821);
      viewHolder1.mTitle = (TextView)view1.findViewById(201457822);
      view1.setTag(viewHolder1);
      viewHolder = viewHolder1;
      view = view1;
    } else {
      viewHolder = (ViewHolder)view.getTag();
    } 
    if (getCount() == 1) {
      view.setMinimumHeight(this.mPopupListItemMinHeight + this.mPopupListPaddingVertical * 2);
      int i = this.mPopupListItemPaddingVertical, j = this.mPopupListPaddingVertical;
      view.setPadding(0, i + j, 0, i + j);
    } else if (paramInt == 0) {
      view.setMinimumHeight(this.mPopupListItemMinHeight + this.mPopupListPaddingVertical);
      int i = this.mPopupListItemPaddingVertical;
      view.setPadding(0, this.mPopupListPaddingVertical + i, 0, i);
    } else if (paramInt == getCount() - 1) {
      view.setMinimumHeight(this.mPopupListItemMinHeight + this.mPopupListPaddingVertical);
      int i = this.mPopupListItemPaddingVertical;
      view.setPadding(0, i, 0, this.mPopupListPaddingVertical + i);
    } else {
      view.setMinimumHeight(this.mPopupListItemMinHeight);
      int i = this.mPopupListItemPaddingVertical;
      view.setPadding(0, i, 0, i);
    } 
    boolean bool = ((PopupListItem)this.mItemList.get(paramInt)).isEnable();
    view.setEnabled(bool);
    setIcon(viewHolder.mIcon, viewHolder.mTitle, this.mItemList.get(paramInt), bool);
    setTitle(viewHolder.mTitle, this.mItemList.get(paramInt), bool);
    return view;
  }
  
  private void setIcon(ImageView paramImageView, TextView paramTextView, PopupListItem paramPopupListItem, boolean paramBoolean) {
    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)paramTextView.getLayoutParams();
    if (paramPopupListItem.getIconId() == 0 && paramPopupListItem.getIcon() == null) {
      paramImageView.setVisibility(8);
      layoutParams.leftMargin = this.mContext.getResources().getDimensionPixelSize(201654580);
      layoutParams.rightMargin = this.mContext.getResources().getDimensionPixelSize(201654580);
    } else {
      Drawable drawable;
      paramImageView.setVisibility(0);
      layoutParams.leftMargin = this.mContext.getResources().getDimensionPixelSize(201654578);
      layoutParams.rightMargin = this.mContext.getResources().getDimensionPixelSize(201654579);
      paramImageView.setEnabled(paramBoolean);
      if (paramPopupListItem.getIcon() == null) {
        drawable = this.mContext.getResources().getDrawable(paramPopupListItem.getIconId());
      } else {
        drawable = paramPopupListItem.getIcon();
      } 
      paramImageView.setImageDrawable(drawable);
    } 
  }
  
  private void setTitle(TextView paramTextView, PopupListItem paramPopupListItem, boolean paramBoolean) {
    paramTextView.setEnabled(paramBoolean);
    paramTextView.setText(paramPopupListItem.getTitle());
    paramTextView.setTextColor(this.mContext.getResources().getColorStateList(201720058));
    Context context = this.mContext;
    float f1 = context.getResources().getDimensionPixelSize(201654581);
    context = this.mContext;
    float f2 = (context.getResources().getConfiguration()).fontScale;
    f1 = OplusChangeTextUtil.getSuitableFontSize(f1, f2, 5);
    paramTextView.setTextSize(0, f1);
  }
  
  class ViewHolder {
    ImageView mIcon;
    
    TextView mTitle;
  }
}
