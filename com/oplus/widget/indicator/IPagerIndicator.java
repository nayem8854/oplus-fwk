package com.oplus.widget.indicator;

public interface IPagerIndicator {
  void onPageScrollStateChanged(int paramInt);
  
  void onPageScrolled(int paramInt1, float paramFloat, int paramInt2);
  
  void onPageSelected(int paramInt);
}
