package com.oplus.widget.indicator;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.PathInterpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.oplus.internal.R;
import com.oplus.util.OplusContextUtil;
import com.oplus.util.OplusDarkModeUtil;
import java.util.ArrayList;
import java.util.List;

public class OplusPageIndicator extends FrameLayout implements IPagerIndicator {
  private int mDotStepDistance = 0;
  
  private float mTraceLeft = 0.0F;
  
  private float mTraceRight = 0.0F;
  
  private float mFinalLeft = 0.0F;
  
  private float mFinalRight = 0.0F;
  
  private boolean mTranceCutTailRight = false;
  
  private boolean mIsAnimated = false;
  
  private boolean mIsAnimating = false;
  
  private boolean mIsAnimatorCanceled = false;
  
  private boolean mIsPaused = false;
  
  private boolean mNeedSettlePositionTemp = false;
  
  private RectF mTraceRect = new RectF();
  
  private static final boolean DEBUG = false;
  
  private static final int MSG_START_TRACE_ANIMATION = 17;
  
  public static final int SCROLL_STATE_DRAGGING = 1;
  
  public static final int SCROLL_STATE_IDLE = 0;
  
  public static final int SCROLL_STATE_SETTLING = 2;
  
  private static final String TAG = "OplusPageIndicator2";
  
  private int mCurrentPosition;
  
  private int mDotColor;
  
  private int mDotCornerRadius;
  
  private int mDotSize;
  
  private int mDotSpacing;
  
  private int mDotStrokeWidth;
  
  private int mDotsCount;
  
  private Handler mHandler;
  
  private List<View> mIndicatorDots;
  
  private LinearLayout mIndicatorDotsParent;
  
  private boolean mIsClickable;
  
  private boolean mIsStrokeStyle;
  
  private int mLastPosition;
  
  private OnIndicatorDotClickListener mOnDotClickListener;
  
  private ValueAnimator mTraceAnimator;
  
  private int mTraceDotColor;
  
  private Paint mTracePaint;
  
  private int mWidth;
  
  public OplusPageIndicator(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public OplusPageIndicator(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public OplusPageIndicator(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    OplusDarkModeUtil.setForceDarkAllow((View)this, false);
    this.mIndicatorDots = new ArrayList<>();
    this.mDotSize = dpToPx(10);
    this.mDotSpacing = dpToPx(5);
    this.mDotStrokeWidth = dpToPx(2);
    this.mDotCornerRadius = this.mDotSize / 2;
    this.mDotColor = -1;
    this.mTraceDotColor = OplusContextUtil.getAttrColor(paramContext, 201982027);
    this.mIsClickable = true;
    this.mIsStrokeStyle = true;
    if (paramAttributeSet != null) {
      TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.OplusPageIndicator);
      this.mTraceDotColor = typedArray.getColor(7, this.mTraceDotColor);
      if (!OplusDarkModeUtil.isNightMode(paramContext))
        this.mDotColor = typedArray.getColor(1, this.mDotColor); 
      this.mDotSize = (int)typedArray.getDimension(4, this.mDotSize);
      this.mDotSpacing = (int)typedArray.getDimension(5, this.mDotSpacing);
      this.mDotCornerRadius = (int)typedArray.getDimension(2, (this.mDotSize / 2));
      this.mIsClickable = typedArray.getBoolean(0, this.mIsClickable);
      this.mDotStrokeWidth = (int)typedArray.getDimension(6, this.mDotStrokeWidth);
      typedArray.recycle();
    } 
    this.mTraceRect.top = 0.0F;
    this.mTraceRect.bottom = this.mDotSize;
    ValueAnimator valueAnimator = ValueAnimator.ofFloat(new float[] { 0.0F, 1.0F });
    valueAnimator.setDuration(240L);
    if (Build.VERSION.SDK_INT >= 21)
      this.mTraceAnimator.setInterpolator((TimeInterpolator)new PathInterpolator(0.25F, 0.1F, 0.25F, 1.0F)); 
    this.mTraceAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new Object(this));
    this.mTraceAnimator.addListener((Animator.AnimatorListener)new Object(this));
    Paint paint = new Paint(1);
    paint.setStyle(Paint.Style.FILL);
    this.mTracePaint.setColor(this.mTraceDotColor);
    this.mDotStepDistance = this.mDotSize + this.mDotSpacing * 2;
    this.mHandler = (Handler)new Object(this);
    this.mIndicatorDotsParent = new LinearLayout(paramContext);
    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
    this.mIndicatorDotsParent.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
    this.mIndicatorDotsParent.setOrientation(0);
    addView((View)this.mIndicatorDotsParent);
    getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)new Object(this));
  }
  
  private void snapToPoition(int paramInt) {
    verifyFinalPosition(this.mCurrentPosition);
    this.mTraceRect.left = this.mFinalLeft;
    this.mTraceRect.right = this.mFinalRight;
    invalidate();
  }
  
  public void addDot() {
    this.mDotsCount++;
    verifyLayoutWidth();
    addIndicatorDots(1);
  }
  
  public void removeDot() {
    this.mDotsCount--;
    verifyLayoutWidth();
    removeIndicatorDots(1);
  }
  
  protected void dispatchDraw(Canvas paramCanvas) {
    super.dispatchDraw(paramCanvas);
    RectF rectF = this.mTraceRect;
    int i = this.mDotCornerRadius;
    paramCanvas.drawRoundRect(rectF, i, i, this.mTracePaint);
  }
  
  private void setupDotView(boolean paramBoolean, View paramView, int paramInt) {
    GradientDrawable gradientDrawable = (GradientDrawable)paramView.getBackground();
    if (paramBoolean) {
      gradientDrawable.setStroke(this.mDotStrokeWidth, paramInt);
    } else {
      gradientDrawable.setColor(paramInt);
    } 
    gradientDrawable.setCornerRadius(this.mDotCornerRadius);
  }
  
  private View buildDot(boolean paramBoolean, int paramInt) {
    View view1 = LayoutInflater.from(getContext()).inflate(202440729, (ViewGroup)this, false);
    View view2 = view1.findViewById(201457761);
    int i = Build.VERSION.SDK_INT, j = 201851320;
    if (i > 16) {
      Resources resources = getContext().getResources();
      if (!paramBoolean)
        j = 201851319; 
      view2.setBackground(resources.getDrawable(j));
    } else {
      Resources resources = getContext().getResources();
      if (!paramBoolean)
        j = 201851319; 
      view2.setBackgroundDrawable(resources.getDrawable(j));
    } 
    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)view2.getLayoutParams();
    layoutParams.height = j = this.mDotSize;
    layoutParams.width = j;
    view2.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
    j = this.mDotSpacing;
    layoutParams.setMargins(j, 0, j, 0);
    setupDotView(paramBoolean, view2, paramInt);
    return view1;
  }
  
  private void removeIndicatorDots(int paramInt) {
    for (byte b = 0; b < paramInt; b++) {
      LinearLayout linearLayout = this.mIndicatorDotsParent;
      linearLayout.removeViewAt(linearLayout.getChildCount() - 1);
      List<View> list = this.mIndicatorDots;
      list.remove(list.size() - 1);
    } 
  }
  
  private void addIndicatorDots(int paramInt) {
    for (byte b = 0; b < paramInt; b++) {
      View view = buildDot(this.mIsStrokeStyle, this.mDotColor);
      if (this.mIsClickable)
        view.setOnClickListener((View.OnClickListener)new Object(this, b)); 
      this.mIndicatorDots.add(view.findViewById(201457761));
      this.mIndicatorDotsParent.addView(view);
    } 
  }
  
  private void verifyLayoutWidth() {
    int i = this.mDotsCount;
    if (i < 1)
      return; 
    this.mWidth = this.mDotStepDistance * i;
    requestLayout();
  }
  
  private void startTraceAnimator() {
    if (this.mTraceAnimator == null)
      return; 
    stopTraceAnimator();
    this.mTraceAnimator.start();
  }
  
  public void stopTraceAnimator() {
    if (!this.mIsAnimatorCanceled)
      this.mIsAnimatorCanceled = true; 
    ValueAnimator valueAnimator = this.mTraceAnimator;
    if (valueAnimator != null && 
      valueAnimator.isRunning())
      this.mTraceAnimator.cancel(); 
  }
  
  private void pauseTrace() {
    this.mIsPaused = true;
  }
  
  private void resumeTrace() {
    this.mIsPaused = false;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    super.onMeasure(this.mWidth, this.mDotSize);
  }
  
  public void setDotsCount(int paramInt) {
    boolean bool;
    if (paramInt <= 1) {
      bool = true;
    } else {
      bool = false;
    } 
    setVisibility(bool);
    this.mDotsCount = paramInt;
    verifyLayoutWidth();
    this.mIndicatorDots.clear();
    this.mIndicatorDotsParent.removeAllViews();
    addIndicatorDots(paramInt);
  }
  
  public void setCurrentPosition(int paramInt) {
    this.mCurrentPosition = paramInt;
    this.mLastPosition = paramInt;
    snapToPoition(paramInt);
  }
  
  public void setTraceDotColor(int paramInt) {
    this.mTraceDotColor = paramInt;
    this.mTracePaint.setColor(paramInt);
  }
  
  public void setPageIndicatorDotsColor(int paramInt) {
    this.mDotColor = paramInt;
    List<View> list = this.mIndicatorDots;
    if (list != null && !list.isEmpty())
      for (View view : this.mIndicatorDots)
        setupDotView(this.mIsStrokeStyle, view, paramInt);  
  }
  
  public void setOnDotClickListener(OnIndicatorDotClickListener paramOnIndicatorDotClickListener) {
    this.mOnDotClickListener = paramOnIndicatorDotClickListener;
  }
  
  public void onPageScrolled(int paramInt1, float paramFloat, int paramInt2) {
    boolean bool = isLayoutRtl();
    paramInt2 = 1;
    int i = this.mCurrentPosition;
    if (bool ? (i > paramInt1) : (i <= paramInt1))
      paramInt2 = 0; 
    if (paramInt2 != 0) {
      if (bool) {
        float f1 = this.mWidth;
        paramInt2 = this.mDotSpacing;
        i = this.mDotStepDistance;
        float f2 = (paramInt2 + paramInt1 * i), f3 = i;
        this.mTraceRect.right = f1 - f2 + f3 * paramFloat;
      } else {
        i = this.mDotSpacing;
        paramInt2 = this.mDotSize;
        int j = this.mDotStepDistance;
        float f2 = (i + paramInt2 + paramInt1 * j), f1 = j;
        this.mTraceRect.right = f2 + f1 * paramFloat;
      } 
      if (this.mIsPaused) {
        if (!this.mIsAnimating && this.mIsAnimated) {
          RectF rectF = this.mTraceRect;
          rectF.left = rectF.right - this.mDotSize;
        } else if (this.mTraceRect.right - this.mTraceRect.left < this.mDotSize) {
          RectF rectF = this.mTraceRect;
          rectF.left = rectF.right - this.mDotSize;
        } 
      } else if (this.mIsAnimated) {
        RectF rectF = this.mTraceRect;
        rectF.left = rectF.right - this.mDotSize;
      } else if (this.mTraceRect.right - this.mTraceRect.left < this.mDotSize) {
        RectF rectF = this.mTraceRect;
        rectF.left = rectF.right - this.mDotSize;
      } 
    } else {
      if (bool) {
        float f1 = this.mWidth, f4 = this.mDotStepDistance, f3 = paramInt1, f5 = this.mDotSpacing, f2 = this.mDotSize;
        this.mTraceRect.left = f1 - f4 * (f3 + paramFloat) - f5 - f2;
      } else {
        float f2 = this.mDotSpacing, f3 = this.mDotStepDistance, f1 = paramInt1;
        this.mTraceRect.left = f2 + f3 * (f1 + paramFloat);
      } 
      if (this.mIsPaused) {
        if (!this.mIsAnimating && this.mIsAnimated) {
          RectF rectF = this.mTraceRect;
          rectF.right = rectF.left + this.mDotSize;
        } else if (this.mTraceRect.right - this.mTraceRect.left < this.mDotSize) {
          RectF rectF = this.mTraceRect;
          rectF.right = rectF.left + this.mDotSize;
        } 
      } else if (this.mIsAnimated) {
        RectF rectF = this.mTraceRect;
        rectF.right = rectF.left + this.mDotSize;
      } else if (this.mTraceRect.right - this.mTraceRect.left < this.mDotSize) {
        RectF rectF = this.mTraceRect;
        rectF.right = rectF.left + this.mDotSize;
      } 
    } 
    this.mTraceLeft = this.mTraceRect.left;
    this.mTraceRight = this.mTraceRect.right;
    if (paramFloat == 0.0F)
      this.mCurrentPosition = paramInt1; 
    invalidate();
  }
  
  private void verifyFinalPosition(int paramInt) {
    if (isLayoutRtl()) {
      float f = (this.mWidth - this.mDotSpacing + this.mDotStepDistance * paramInt);
      this.mFinalLeft = f - this.mDotSize;
    } else {
      int i = this.mDotSpacing, j = this.mDotSize;
      float f = (i + j + this.mDotStepDistance * paramInt);
      this.mFinalLeft = f - j;
    } 
  }
  
  public void onPageSelected(int paramInt) {
    int i = this.mLastPosition;
    boolean bool = false;
    if (i != paramInt && 
      this.mIsAnimated)
      this.mIsAnimated = false; 
    if (isLayoutRtl() ? (this.mLastPosition <= paramInt) : (this.mLastPosition > paramInt))
      bool = true; 
    this.mTranceCutTailRight = bool;
    verifyFinalPosition(paramInt);
    if (this.mLastPosition != paramInt) {
      if (this.mHandler.hasMessages(17))
        this.mHandler.removeMessages(17); 
      stopTraceAnimator();
      this.mHandler.sendEmptyMessageDelayed(17, 100L);
    } else if (this.mHandler.hasMessages(17)) {
      this.mHandler.removeMessages(17);
    } 
    this.mLastPosition = paramInt;
  }
  
  public void onPageScrollStateChanged(int paramInt) {
    if (paramInt == 1) {
      pauseTrace();
      if (this.mIsAnimated)
        this.mIsAnimated = false; 
    } else if (paramInt == 2) {
      resumeTrace();
    } 
  }
  
  public boolean isLayoutRtl() {
    int i = Build.VERSION.SDK_INT;
    boolean bool = false;
    if (i > 16) {
      if (getLayoutDirection() == 1)
        bool = true; 
      return bool;
    } 
    return false;
  }
  
  private int dpToPx(int paramInt) {
    return (int)((getContext().getResources().getDisplayMetrics()).density * paramInt);
  }
  
  class OnIndicatorDotClickListener {
    public abstract void onClick(int param1Int);
  }
}
