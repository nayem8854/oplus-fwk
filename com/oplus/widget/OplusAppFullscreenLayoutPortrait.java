package com.oplus.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.OplusBaseConfiguration;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.oplus.content.OplusFeatureConfigManager;
import com.oplus.util.OplusTypeCastingHelper;
import java.util.Locale;

public class OplusAppFullscreenLayoutPortrait extends LinearLayout {
  private Context mContext = null;
  
  private int mDefaultHeight = 96;
  
  private Locale mLocale = null;
  
  private int mLastFontFlipFlag = -1;
  
  public OplusAppFullscreenLayoutPortrait(Context paramContext) {
    this(paramContext, null);
  }
  
  public OplusAppFullscreenLayoutPortrait(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public OplusAppFullscreenLayoutPortrait(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext);
  }
  
  private void init(Context paramContext) {
    byte b;
    this.mContext = paramContext;
    this.mLocale = (paramContext.getResources().getConfiguration()).locale;
    boolean bool = OplusFeatureConfigManager.getInstacne().hasFeature("oplus.software.display.screen_heteromorphism");
    if (bool) {
      b = 136;
    } else {
      b = 96;
    } 
    this.mDefaultHeight = b;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    paramInt2 = View.MeasureSpec.makeMeasureSpec(this.mDefaultHeight, 1073741824);
    super.onMeasure(paramInt1, paramInt2);
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration) {
    super.onConfigurationChanged(paramConfiguration);
    if (paramConfiguration != null) {
      TextView textView = (TextView)findViewById(201457691);
      if (paramConfiguration.locale != this.mLocale) {
        this.mLocale = paramConfiguration.locale;
        if (textView != null)
          textView.setText(201588810); 
      } else {
        OplusBaseConfiguration oplusBaseConfiguration = OplusTypeCastingHelper.<OplusBaseConfiguration>typeCasting(OplusBaseConfiguration.class, paramConfiguration);
        if (oplusBaseConfiguration != null && oplusBaseConfiguration.mOplusExtraConfiguration.mFlipFont != this.mLastFontFlipFlag) {
          if (textView != null)
            textView.setTypeface(Typeface.DEFAULT); 
          this.mLastFontFlipFlag = oplusBaseConfiguration.mOplusExtraConfiguration.mFlipFont;
        } 
      } 
    } 
  }
}
