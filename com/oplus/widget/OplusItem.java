package com.oplus.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;

public class OplusItem {
  public static final int ITEM_FIFTH = 4;
  
  public static final int ITEM_FIRST = 0;
  
  public static final int ITEM_FOURTH = 3;
  
  public static final int ITEM_SECOND = 1;
  
  public static final int ITEM_THIRD = 2;
  
  private Drawable mBackgroud;
  
  private Context mContext;
  
  private Drawable mIcon;
  
  private String mLabel;
  
  private OnItemClickListener mOnItemClickListener;
  
  private String mText;
  
  public static class Builder {
    private OplusItem ci;
    
    public Builder(Context param1Context) {
      OplusItem oplusItem = new OplusItem();
      OplusItem.access$002(oplusItem, param1Context);
    }
    
    public Builder setText(String param1String) {
      OplusItem.access$102(this.ci, param1String);
      return this;
    }
    
    public Builder setText(int param1Int) {
      OplusItem oplusItem = this.ci;
      OplusItem.access$102(oplusItem, oplusItem.getContext().getString(param1Int));
      return this;
    }
    
    public Builder setLabel(int param1Int) {
      OplusItem oplusItem = this.ci;
      OplusItem.access$202(oplusItem, oplusItem.getContext().getString(param1Int));
      return this;
    }
    
    public Builder setLabel(String param1String) {
      OplusItem.access$202(this.ci, param1String);
      return this;
    }
    
    public Builder setIcon(Drawable param1Drawable) {
      OplusItem.access$302(this.ci, param1Drawable);
      return this;
    }
    
    public Builder setIcon(int param1Int) {
      OplusItem oplusItem = this.ci;
      OplusItem.access$302(oplusItem, oplusItem.getContext().getResources().getDrawable(param1Int));
      return this;
    }
    
    public Builder setBackgroud(Drawable param1Drawable) {
      OplusItem.access$402(this.ci, param1Drawable);
      return this;
    }
    
    public Builder setBackgroud(int param1Int) {
      OplusItem oplusItem = this.ci;
      OplusItem.access$402(oplusItem, oplusItem.getContext().getResources().getDrawable(param1Int));
      return this;
    }
    
    public Builder setOnItemClickListener(OplusItem.OnItemClickListener param1OnItemClickListener) {
      OplusItem.access$502(this.ci, param1OnItemClickListener);
      return this;
    }
    
    public OplusItem create() {
      return this.ci;
    }
  }
  
  public String getText() {
    return this.mText;
  }
  
  public void setText(String paramString) {
    this.mText = paramString;
  }
  
  public String getLabel() {
    return this.mLabel;
  }
  
  public void setLabel(String paramString) {
    this.mLabel = paramString;
  }
  
  public Drawable getIcon() {
    return this.mIcon;
  }
  
  public void setIcon(Drawable paramDrawable) {
    this.mIcon = paramDrawable;
  }
  
  public Drawable getBackgroud() {
    return this.mBackgroud;
  }
  
  public void setBackgroud(Drawable paramDrawable) {
    this.mBackgroud = paramDrawable;
  }
  
  public Context getContext() {
    return this.mContext;
  }
  
  public void setContext(Context paramContext) {
    this.mContext = paramContext;
  }
  
  public OnItemClickListener getOnItemClickListener() {
    return this.mOnItemClickListener;
  }
  
  public void setOnItemClickListener(OnItemClickListener paramOnItemClickListener) {
    this.mOnItemClickListener = paramOnItemClickListener;
  }
  
  public static interface OnItemClickListener {
    void OnMenuItemClick(int param1Int);
  }
}
