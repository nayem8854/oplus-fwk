package com.oplus.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.android.internal.widget.OplusViewExplorerByTouchHelper;
import java.util.ArrayList;
import java.util.List;

public class OplusMenuView extends View {
  static final int[] STATE_ENABLED = new int[] { 16842910 };
  
  static final int[] STATE_UNENABLED = new int[] { -16842910 };
  
  static {
    STATE_PRESSED = new int[] { 16842919, 16842910 };
    STATE_NORMAL = new int[] { -16842919, 16842910 };
    IETMNUMBERS = 5;
  }
  
  private List<OplusItem> mOplusItemList = new ArrayList<>();
  
  private int mItemCounts = 0;
  
  private Rect mSelectRect = new Rect();
  
  private boolean mIsSelected = false;
  
  private int mSelectedPosition = -1;
  
  private float mTextSize = 30.0F;
  
  private float mScale = 0.0F;
  
  private static final int DISPLAYDSEN = 160;
  
  private static int IETMNUMBERS = 0;
  
  static final int MAX_MENU_ITEM_COUNTS = 5;
  
  static final int[] STATE_NORMAL;
  
  static final int[] STATE_PRESSED;
  
  private static final String TAG = "OplusMenuView";
  
  static final int VIEW_STATE_ENABLED = 16842910;
  
  static final int VIEW_STATE_PRESSED = 16842919;
  
  private int[] mBottom;
  
  private int mIconTextDis;
  
  private int mItemHeight;
  
  private int mItemWidth;
  
  private int[] mLeft;
  
  private int mNormalColor;
  
  private Runnable mOnclickRunnable;
  
  private OplusViewExplorerByTouchHelper.OplusViewTalkBalkInteraction mOplusViewTalkBalkInteraction;
  
  private int mPaddingLeft;
  
  private int mPaddingTop;
  
  private Paint mPaint;
  
  private int[] mRight;
  
  private int mSelectedColor;
  
  private int[] mTop;
  
  private final OplusViewExplorerByTouchHelper mTouchHelper;
  
  public OplusMenuView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public OplusMenuView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public OplusMenuView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    this.mOnclickRunnable = (Runnable)new Object(this);
    this.mOplusViewTalkBalkInteraction = (OplusViewExplorerByTouchHelper.OplusViewTalkBalkInteraction)new Object(this);
    Paint paint = new Paint();
    paint.setTextAlign(Paint.Align.CENTER);
    this.mPaint.setAntiAlias(true);
    this.mSelectedColor = getResources().getColor(201720161);
    this.mNormalColor = getResources().getColor(201720160);
    this.mTextSize = (int)getResources().getDimension(201654693);
    this.mPaddingLeft = (int)getResources().getDimension(201654699);
    this.mPaddingTop = (int)getResources().getDimension(201654700);
    this.mItemHeight = (int)getResources().getDimension(201654697);
    this.mItemWidth = (int)getResources().getDimension(201654698);
    this.mIconTextDis = (int)getResources().getDimension(201654696);
    this.mPaint.setTextSize(this.mTextSize);
    this.mScale = (paramContext.getResources().getDisplayMetrics()).densityDpi;
    OplusViewExplorerByTouchHelper oplusViewExplorerByTouchHelper = new OplusViewExplorerByTouchHelper(this);
    oplusViewExplorerByTouchHelper.setOplusViewTalkBalkInteraction(this.mOplusViewTalkBalkInteraction);
    setAccessibilityDelegate(this.mTouchHelper);
    setImportantForAccessibility(1);
  }
  
  public void setItem(List<OplusItem> paramList) {
    int j;
    this.mOplusItemList = paramList;
    int i = paramList.size();
    if (i > 5) {
      this.mItemCounts = 5;
      this.mOplusItemList = this.mOplusItemList.subList(0, 5);
    } else {
      this.mItemCounts = i;
    } 
    i = 0;
    while (true) {
      j = this.mItemCounts;
      if (i < j) {
        initStateListDrawable(i);
        i++;
        continue;
      } 
      break;
    } 
    IETMNUMBERS = j;
    this.mLeft = new int[j];
    this.mTop = new int[j];
    this.mRight = new int[j];
    this.mBottom = new int[j];
    this.mTouchHelper.invalidateRoot();
  }
  
  private void initStateListDrawable(int paramInt) {
    Drawable drawable = ((OplusItem)this.mOplusItemList.get(paramInt)).getIcon();
    StateListDrawable stateListDrawable = new StateListDrawable();
    drawable.setState(STATE_PRESSED);
    stateListDrawable.addState(STATE_PRESSED, drawable.getCurrent());
    drawable.setState(STATE_ENABLED);
    stateListDrawable.addState(STATE_ENABLED, drawable.getCurrent());
    drawable.setState(STATE_UNENABLED);
    stateListDrawable.addState(STATE_UNENABLED, drawable.getCurrent());
    drawable.setState(STATE_NORMAL);
    stateListDrawable.addState(STATE_NORMAL, drawable.getCurrent());
    ((OplusItem)this.mOplusItemList.get(paramInt)).setIcon((Drawable)stateListDrawable);
    ((OplusItem)this.mOplusItemList.get(paramInt)).getIcon().setCallback((Drawable.Callback)this);
    clearState();
  }
  
  protected void drawableStateChanged() {
    int i = this.mSelectedPosition;
    if (i >= 0 && i < this.mItemCounts) {
      Drawable drawable = ((OplusItem)this.mOplusItemList.get(i)).getIcon();
      if (drawable != null && drawable.isStateful())
        drawable.setState(getDrawableState()); 
    } 
    super.drawableStateChanged();
  }
  
  protected boolean verifyDrawable(Drawable paramDrawable) {
    super.verifyDrawable(paramDrawable);
    return true;
  }
  
  public void onDraw(Canvas paramCanvas) {
    super.onDraw(paramCanvas);
    this.mPaddingLeft = (int)getResources().getDimension(201654699);
    if (IETMNUMBERS < 5)
      this.mPaddingLeft = (getWidth() / IETMNUMBERS - this.mItemWidth) / 2; 
    for (byte b = 0; b < this.mItemCounts; b++) {
      getRect(b, this.mSelectRect);
      OplusItem oplusItem = this.mOplusItemList.get(b);
      oplusItem.getIcon().setBounds(this.mSelectRect);
      oplusItem.getIcon().draw(paramCanvas);
      if (this.mSelectedPosition == b && this.mIsSelected) {
        this.mPaint.setColor(this.mSelectedColor);
      } else {
        this.mPaint.setColor(this.mNormalColor);
      } 
      paramCanvas.drawText(oplusItem.getText(), (this.mPaddingLeft + this.mItemWidth / 2 + getWidth() / IETMNUMBERS * b), (this.mPaddingTop + this.mItemHeight + this.mIconTextDis) + this.mTextSize / 2.0F, this.mPaint);
    } 
  }
  
  private void getRect(int paramInt, Rect paramRect) {
    this.mLeft[paramInt] = this.mPaddingLeft + getWidth() / IETMNUMBERS * paramInt;
    int arrayOfInt1[] = this.mTop, i = this.mPaddingTop;
    arrayOfInt1[paramInt] = i;
    int arrayOfInt2[] = this.mRight, j = this.mItemWidth, arrayOfInt3[] = this.mLeft;
    arrayOfInt2[paramInt] = j + arrayOfInt3[paramInt];
    int[] arrayOfInt4 = this.mBottom;
    arrayOfInt4[paramInt] = i + this.mItemHeight;
    paramRect.set(arrayOfInt3[paramInt], arrayOfInt1[paramInt], arrayOfInt2[paramInt], arrayOfInt4[paramInt]);
  }
  
  private int selectedIndex(float paramFloat1, float paramFloat2) {
    int i = (int)(paramFloat1 / (getWidth() / IETMNUMBERS));
    if (i >= this.mItemCounts)
      i = -2; 
    return i;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    super.onTouchEvent(paramMotionEvent);
    paramMotionEvent.getPointerCount();
    int i = (int)paramMotionEvent.getY();
    int j = (int)paramMotionEvent.getX();
    int k = 0;
    int m = k;
    if (i < this.mItemHeight * this.mScale / 160.0F) {
      m = k;
      if (i > 0)
        m = 1; 
    } 
    k = paramMotionEvent.getAction();
    if (k != 0) {
      if (k != 1)
        return true; 
      k = this.mSelectedPosition;
      if (k >= 0 && m != 0) {
        ((OplusItem)this.mOplusItemList.get(k)).getOnItemClickListener().OnMenuItemClick(this.mSelectedPosition);
        this.mTouchHelper.sendEventForVirtualView(this.mSelectedPosition, 1);
      } 
      clearState();
      return false;
    } 
    m = this.mSelectedPosition;
    if (m >= 0 && j > this.mLeft[m] && j < this.mRight[m])
      this.mIsSelected = true; 
    return true;
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent) {
    float f1 = paramMotionEvent.getX();
    float f2 = paramMotionEvent.getY();
    int i = paramMotionEvent.getPointerCount();
    if (i == 1 && f2 >= 0.0F) {
      if (paramMotionEvent.getAction() == 0) {
        this.mSelectedPosition = i = selectedIndex(paramMotionEvent.getX(), paramMotionEvent.getY());
        if (i < 0 || f1 <= this.mLeft[i] || f1 >= this.mRight[i])
          this.mSelectedPosition = -1; 
      } 
    } else {
      clearState();
    } 
    return super.dispatchTouchEvent(paramMotionEvent);
  }
  
  private void clearState() {
    for (OplusItem oplusItem : this.mOplusItemList) {
      Drawable drawable = oplusItem.getIcon();
      if (drawable != null && drawable.isStateful())
        drawable.setState(STATE_NORMAL); 
    } 
    this.mIsSelected = false;
    this.mSelectedPosition = -1;
    invalidate();
  }
  
  public void clearAccessibilityFocus() {
    OplusViewExplorerByTouchHelper oplusViewExplorerByTouchHelper = this.mTouchHelper;
    if (oplusViewExplorerByTouchHelper != null)
      oplusViewExplorerByTouchHelper.clearFocusedVirtualView(); 
  }
  
  boolean restoreAccessibilityFocus(int paramInt) {
    if (paramInt < 0 || paramInt >= this.mItemCounts)
      return false; 
    OplusViewExplorerByTouchHelper oplusViewExplorerByTouchHelper = this.mTouchHelper;
    if (oplusViewExplorerByTouchHelper != null)
      oplusViewExplorerByTouchHelper.setFocusedVirtualView(paramInt); 
    return true;
  }
  
  protected boolean dispatchHoverEvent(MotionEvent paramMotionEvent) {
    if (this.mTouchHelper.dispatchHoverEvent(paramMotionEvent))
      return true; 
    return super.dispatchHoverEvent(paramMotionEvent);
  }
}
