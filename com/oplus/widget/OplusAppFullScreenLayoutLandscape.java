package com.oplus.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.OplusBaseConfiguration;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.oplus.content.OplusFeatureConfigManager;
import com.oplus.util.OplusTypeCastingHelper;
import java.util.Locale;

public class OplusAppFullScreenLayoutLandscape extends LinearLayout {
  private Context mContext = null;
  
  private int mDefaultWidth = 96;
  
  private Locale mLocale = null;
  
  private int mLastFontFlipFlag = -1;
  
  public OplusAppFullScreenLayoutLandscape(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public OplusAppFullScreenLayoutLandscape(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public OplusAppFullScreenLayoutLandscape(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext);
  }
  
  private void init(Context paramContext) {
    byte b;
    this.mContext = paramContext;
    this.mLocale = (paramContext.getResources().getConfiguration()).locale;
    boolean bool = OplusFeatureConfigManager.getInstacne().hasFeature("oplus.software.display.screen_heteromorphism");
    if (bool) {
      b = 136;
    } else {
      b = 96;
    } 
    this.mDefaultWidth = b;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    View view = getChildAt(0);
    if (view != null) {
      paramInt2 = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(paramInt2), -2147483648);
      paramInt1 = View.MeasureSpec.makeMeasureSpec(this.mDefaultWidth, 1073741824);
      view.measure(paramInt2, paramInt1);
      setMeasuredDimension(this.mDefaultWidth, view.getMeasuredWidth());
    } else {
      super.onMeasure(paramInt1, paramInt2);
    } 
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    View view = getChildAt(0);
    if (view != null) {
      int i = view.getMeasuredWidth();
      int j = view.getMeasuredHeight();
      paramInt2 = (paramInt4 - paramInt2 - j) / 2;
      paramInt1 = (paramInt3 - paramInt1 - i) / 2;
      view.layout(paramInt1, paramInt2, paramInt1 + i, paramInt2 + j);
    } else {
      super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    } 
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration) {
    super.onConfigurationChanged(paramConfiguration);
    if (paramConfiguration != null) {
      TextView textView = (TextView)findViewById(201457691);
      if (paramConfiguration.locale != this.mLocale) {
        this.mLocale = paramConfiguration.locale;
        if (textView != null)
          textView.setText(201588810); 
      } else {
        OplusBaseConfiguration oplusBaseConfiguration = OplusTypeCastingHelper.<OplusBaseConfiguration>typeCasting(OplusBaseConfiguration.class, paramConfiguration);
        if (oplusBaseConfiguration != null && oplusBaseConfiguration.mOplusExtraConfiguration.mFlipFont != this.mLastFontFlipFlag) {
          if (textView != null)
            textView.setTypeface(Typeface.DEFAULT); 
          this.mLastFontFlipFlag = oplusBaseConfiguration.mOplusExtraConfiguration.mFlipFont;
        } 
      } 
    } 
  }
}
