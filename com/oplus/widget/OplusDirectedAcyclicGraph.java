package com.oplus.widget;

import android.util.ArrayMap;
import android.util.Pools;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public final class OplusDirectedAcyclicGraph<T> {
  private final Pools.Pool<ArrayList<T>> mListPool = (Pools.Pool<ArrayList<T>>)new Pools.SimplePool(10);
  
  private final ArrayMap<T, ArrayList<T>> mGraph = new ArrayMap();
  
  private final ArrayList<T> mSortResult = new ArrayList<>();
  
  private final HashSet<T> mSortTmpMarked = new HashSet<>();
  
  public void addNode(T paramT) {
    if (!this.mGraph.containsKey(paramT))
      this.mGraph.put(paramT, null); 
  }
  
  public boolean contains(T paramT) {
    return this.mGraph.containsKey(paramT);
  }
  
  public void addEdge(T paramT1, T paramT2) {
    if (this.mGraph.containsKey(paramT1) && this.mGraph.containsKey(paramT2)) {
      ArrayList<T> arrayList1 = (ArrayList)this.mGraph.get(paramT1);
      ArrayList<T> arrayList2 = arrayList1;
      if (arrayList1 == null) {
        arrayList2 = getEmptyList();
        this.mGraph.put(paramT1, arrayList2);
      } 
      arrayList2.add(paramT2);
      return;
    } 
    throw new IllegalArgumentException("All nodes must be present in the graph before being added as an edge");
  }
  
  public List getIncomingEdges(T paramT) {
    return (List)this.mGraph.get(paramT);
  }
  
  public List<T> getOutgoingEdges(T paramT) {
    ArrayList<Object> arrayList = null;
    byte b;
    int i;
    for (b = 0, i = this.mGraph.size(); b < i; b++, arrayList = arrayList2) {
      ArrayList arrayList1 = (ArrayList)this.mGraph.valueAt(b);
      ArrayList<Object> arrayList2 = arrayList;
      if (arrayList1 != null) {
        arrayList2 = arrayList;
        if (arrayList1.contains(paramT)) {
          arrayList2 = arrayList;
          if (arrayList == null)
            arrayList2 = new ArrayList(); 
          arrayList2.add(this.mGraph.keyAt(b));
        } 
      } 
    } 
    return arrayList;
  }
  
  public boolean hasOutgoingEdges(T paramT) {
    byte b;
    int i;
    for (b = 0, i = this.mGraph.size(); b < i; b++) {
      ArrayList arrayList = (ArrayList)this.mGraph.valueAt(b);
      if (arrayList != null && arrayList.contains(paramT))
        return true; 
    } 
    return false;
  }
  
  public void clear() {
    byte b;
    int i;
    for (b = 0, i = this.mGraph.size(); b < i; b++) {
      ArrayList<T> arrayList = (ArrayList)this.mGraph.valueAt(b);
      if (arrayList != null)
        poolList(arrayList); 
    } 
    this.mGraph.clear();
  }
  
  public ArrayList<T> getSortedList() {
    this.mSortResult.clear();
    this.mSortTmpMarked.clear();
    byte b;
    int i;
    for (b = 0, i = this.mGraph.size(); b < i; b++)
      dfs((T)this.mGraph.keyAt(b), this.mSortResult, this.mSortTmpMarked); 
    return this.mSortResult;
  }
  
  private void dfs(T paramT, ArrayList<T> paramArrayList, HashSet<T> paramHashSet) {
    if (paramArrayList.contains(paramT))
      return; 
    if (!paramHashSet.contains(paramT)) {
      paramHashSet.add(paramT);
      ArrayList<T> arrayList = (ArrayList)this.mGraph.get(paramT);
      if (arrayList != null) {
        byte b;
        int i;
        for (b = 0, i = arrayList.size(); b < i; b++)
          dfs(arrayList.get(b), paramArrayList, paramHashSet); 
      } 
      paramHashSet.remove(paramT);
      paramArrayList.add(paramT);
      return;
    } 
    throw new RuntimeException("This graph contains cyclic dependencies");
  }
  
  int size() {
    return this.mGraph.size();
  }
  
  private ArrayList<T> getEmptyList() {
    ArrayList<T> arrayList1 = (ArrayList)this.mListPool.acquire();
    ArrayList<T> arrayList2 = arrayList1;
    if (arrayList1 == null)
      arrayList2 = new ArrayList(); 
    return arrayList2;
  }
  
  private void poolList(ArrayList<T> paramArrayList) {
    paramArrayList.clear();
    this.mListPool.release(paramArrayList);
  }
}
