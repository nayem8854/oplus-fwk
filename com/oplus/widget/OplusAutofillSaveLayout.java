package com.oplus.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class OplusAutofillSaveLayout extends LinearLayout {
  public OplusAutofillSaveLayout(Context paramContext) {
    this(paramContext, null);
  }
  
  public OplusAutofillSaveLayout(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public OplusAutofillSaveLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    initBackground();
  }
  
  private void initBackground() {
    setBackgroundResource(201850907);
  }
}
