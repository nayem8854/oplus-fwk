package com.oplus.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class OplusAutofillSaveTextView extends TextView {
  public OplusAutofillSaveTextView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public OplusAutofillSaveTextView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public OplusAutofillSaveTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    initAttrs();
  }
  
  private void initAttrs() {
    setTextColor(getResources().getColor(201719831));
    setBackgroundResource(201850908);
  }
}
