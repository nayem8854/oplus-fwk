package com.oplus.widget;

import android.animation.Animator;

public interface OplusBottomMenuCallback extends OplusPagerCallback {
  public static final int INVALID_POSITION = -1;
  
  public static final int UPDATE_MODE_ANIMATE = 1;
  
  public static final int UPDATE_MODE_DIRECT = 0;
  
  public static final int UPDATE_MODE_OUTER = 2;
  
  void lockMenuUpdate();
  
  void setMenuUpdateMode(int paramInt);
  
  void unlockMenuUpdate();
  
  void updateMenuScrollData();
  
  void updateMenuScrollPosition(int paramInt, float paramFloat);
  
  void updateMenuScrollState(int paramInt);
  
  class Updater {
    public abstract Animator getUpdater(int param1Int1, int param1Int2);
    
    public abstract boolean visibleFirst();
  }
}
