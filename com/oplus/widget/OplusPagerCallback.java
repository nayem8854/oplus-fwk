package com.oplus.widget;

import android.view.animation.Interpolator;

public interface OplusPagerCallback {
  public static final Interpolator ANIMATOR_INTERPOLATOR = (Interpolator)new Object();
  
  public static final int SCROLL_STATE_DRAGGING = 1;
  
  public static final int SCROLL_STATE_IDLE = 0;
  
  public static final int SCROLL_STATE_SETTLING = 2;
}
