package com.oplus.widget;

import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;
import com.android.internal.widget.OplusViewExplorerByTouchHelper;
import com.oplus.internal.R;
import com.oplus.util.OplusContextUtil;

public class OplusLoadingView extends View {
  private float[] mPointsAlpha = new float[12];
  
  private int mWidth = 0;
  
  private int mHeight = 0;
  
  private int mLoadingType = 1;
  
  private int mStrokeWidth = 0;
  
  private int mPointRadius = 0;
  
  private float mStepDegree = 30.0F;
  
  private String mAccessDescription = null;
  
  private float mStartAlpha = 0.1F;
  
  private float mEndAlpha = 0.4F;
  
  private static final boolean DEBUG = false;
  
  public static final int DEFAULT_TYPE = 1;
  
  private static final float LARGE_POINT_END_ALPHA = 1.0F;
  
  private static final float LARGE_POINT_START_ALPHA = 0.215F;
  
  public static final int LARGE_TYPE = 2;
  
  private static final float MEDIUM_POINT_END_ALPHA = 0.4F;
  
  private static final float MEDIUM_POINT_START_ALPHA = 0.1F;
  
  public static final int MEDIUM_TYPE = 1;
  
  private static final int ONE_CIRCLE_DEGREE = 360;
  
  private static final int ONE_CYCLE_DURATION = 960;
  
  private static final int PROGRESS_POINT_COUNT = 12;
  
  private static final String TAG = "OplusLoadingView";
  
  private float mCenterX;
  
  private float mCenterY;
  
  private Context mContext;
  
  private float mCurrentDegree;
  
  private int mLoadingViewColor;
  
  private OplusViewExplorerByTouchHelper.OplusViewTalkBalkInteraction mOplusViewTalkBalkInteraction;
  
  private ValueAnimator mProgressAnimator;
  
  private Paint mProgressPaint;
  
  private int mStrokeDefaultWidth;
  
  private int mStrokeLargeWidth;
  
  private int mStrokeMediumWidth;
  
  private OplusViewExplorerByTouchHelper mTouchHelper;
  
  public OplusLoadingView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public OplusLoadingView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 201981990);
  }
  
  public OplusLoadingView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, 201981990, 0);
  }
  
  public OplusLoadingView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1);
    this.mOplusViewTalkBalkInteraction = (OplusViewExplorerByTouchHelper.OplusViewTalkBalkInteraction)new Object(this);
    this.mContext = paramContext;
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.OplusLoadingView, paramInt1, 0);
    paramInt1 = getResources().getDimensionPixelSize(201654570);
    this.mWidth = typedArray.getDimensionPixelSize(3, paramInt1);
    this.mHeight = typedArray.getDimensionPixelSize(1, paramInt1);
    this.mLoadingType = typedArray.getInteger(2, 1);
    paramInt1 = OplusContextUtil.getAttrColor(paramContext, 201982027);
    this.mLoadingViewColor = typedArray.getColor(0, paramInt1);
    typedArray.recycle();
    this.mStrokeDefaultWidth = paramContext.getResources().getDimensionPixelSize(201654567);
    this.mStrokeMediumWidth = paramContext.getResources().getDimensionPixelSize(201654566);
    this.mStrokeLargeWidth = paramInt2 = paramContext.getResources().getDimensionPixelSize(201654565);
    this.mStrokeWidth = this.mStrokeDefaultWidth;
    paramInt1 = this.mLoadingType;
    if (1 == paramInt1) {
      this.mStrokeWidth = this.mStrokeMediumWidth;
      this.mStartAlpha = 0.1F;
      this.mEndAlpha = 0.4F;
    } else if (2 == paramInt1) {
      this.mStrokeWidth = paramInt2;
      this.mStartAlpha = 0.215F;
      this.mEndAlpha = 1.0F;
    } 
    this.mPointRadius = this.mStrokeWidth >> 1;
    this.mCenterX = (this.mWidth >> 1);
    this.mCenterY = (this.mHeight >> 1);
    OplusViewExplorerByTouchHelper oplusViewExplorerByTouchHelper = new OplusViewExplorerByTouchHelper(this);
    oplusViewExplorerByTouchHelper.setOplusViewTalkBalkInteraction(this.mOplusViewTalkBalkInteraction);
    setAccessibilityDelegate(this.mTouchHelper);
    setImportantForAccessibility(1);
    this.mAccessDescription = this.mContext.getString(201588822);
    Paint paint = new Paint(1);
    paint.setStyle(Paint.Style.FILL_AND_STROKE);
    this.mProgressPaint.setColor(this.mLoadingViewColor);
  }
  
  private void createAnimator() {
    ValueAnimator valueAnimator = ValueAnimator.ofFloat(new float[] { 0.0F, 1.0F });
    valueAnimator.setDuration(960L);
    this.mProgressAnimator.setInterpolator((TimeInterpolator)new LinearInterpolator());
    this.mProgressAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new Object(this));
    this.mProgressAnimator.setRepeatMode(1);
    this.mProgressAnimator.setRepeatCount(-1);
    this.mProgressAnimator.setInterpolator((TimeInterpolator)new LinearInterpolator());
  }
  
  private void setPointsAlpha(float paramFloat) {
    float f = this.mEndAlpha - this.mStartAlpha;
    int i = (int)((1.0F - 0.083333336F + 0.083333336F * 4.0F) / 0.083333336F) + 1;
    for (byte b = 0; b < 12; b++) {
      if (b > i + paramFloat / 0.083333336F) {
        this.mPointsAlpha[b] = -f / 0.083333336F * 4.0F * (paramFloat - (b - i - 1) * 0.083333336F) + f / 4.0F + this.mStartAlpha;
      } else if (b * 0.083333336F <= paramFloat && paramFloat < (b + 1) * 0.083333336F) {
        this.mPointsAlpha[b] = f / 0.083333336F * (paramFloat - b * 0.083333336F) + this.mStartAlpha;
      } else if ((b + 1) * 0.083333336F <= paramFloat && paramFloat <= (b + 5) * 0.083333336F) {
        this.mPointsAlpha[b] = -f / 0.083333336F * 4.0F * (paramFloat - b * 0.083333336F) + f / 4.0F + this.mEndAlpha;
      } 
    } 
  }
  
  private void destroyAnimator() {
    ValueAnimator valueAnimator = this.mProgressAnimator;
    if (valueAnimator != null) {
      valueAnimator.cancel();
      this.mProgressAnimator.removeAllListeners();
      this.mProgressAnimator.removeAllUpdateListeners();
      this.mProgressAnimator = null;
    } 
  }
  
  public void onAttachedToWindow() {
    super.onAttachedToWindow();
    createAnimator();
  }
  
  public void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    destroyAnimator();
  }
  
  private void startAnimations() {
    ValueAnimator valueAnimator = this.mProgressAnimator;
    if (valueAnimator != null) {
      if (valueAnimator.isRunning())
        this.mProgressAnimator.cancel(); 
      this.mProgressAnimator.start();
    } 
  }
  
  private void cancelAnimations() {
    ValueAnimator valueAnimator = this.mProgressAnimator;
    if (valueAnimator != null)
      valueAnimator.cancel(); 
  }
  
  public void onVisibilityChanged(View paramView, int paramInt) {
    super.onVisibilityChanged(paramView, paramInt);
    if (paramInt == 0) {
      startAnimations();
    } else {
      cancelAnimations();
    } 
  }
  
  public void onMeasure(int paramInt1, int paramInt2) {
    setMeasuredDimension(this.mWidth, this.mHeight);
  }
  
  public void onDraw(Canvas paramCanvas) {
    paramCanvas.save();
    for (byte b = 0; b < 12; b++) {
      float f1 = this.mStepDegree, f2 = this.mCenterX;
      paramCanvas.rotate(f1, f2, f2);
      int i = (int)(this.mPointsAlpha[b] * 255.0F);
      this.mProgressPaint.setAlpha(i);
      f2 = this.mCenterX;
      i = this.mPointRadius;
      paramCanvas.drawCircle(f2, i, i, this.mProgressPaint);
    } 
    paramCanvas.restore();
  }
}
