package com.oplus.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.PropertyValuesHolder;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Xfermode;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.IntArray;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OplusBezierInterpolator;
import android.view.animation.PathInterpolator;
import com.android.internal.widget.ExploreByTouchHelper;
import com.oplus.util.OplusChangeTextUtil;
import java.util.ArrayList;
import java.util.List;

public class OplusGridView extends View {
  private static final Interpolator POLATOR = (Interpolator)new PathInterpolator(0.32F, 1.22F, 0.32F, 1.0F);
  
  private int mPagerSize = 4;
  
  private int mColumnCounts = 4;
  
  private int mRowCounts = 2;
  
  private int mItemCounts = 0;
  
  public int mPageNumber = 1;
  
  private boolean isSelected = false;
  
  private Rect selRect = new Rect();
  
  private Rect selRect2 = new Rect();
  
  private ColorMatrix mSelectColorMatrix = new ColorMatrix();
  
  private List<ResolveInfo> mResolveInfoList = new ArrayList<>();
  
  private OplusItem[][] mAppInfos = null;
  
  private static final float APPNAME_TEXT_FIRST_SCALEMULTIPLIER = 0.88F;
  
  private static final float APPNAME_TEXT_SECOND_SCALE_MULTIPLIER = 0.7744F;
  
  public static final int COLUMN_SIZE = 4;
  
  private static final int PFLAG_PREPRESSED = 33554432;
  
  private static final int PFLAG_PRESSED = 16384;
  
  public static final String TAG = "OplusGridView";
  
  private static final int TOUCH_END_DURATION = 300;
  
  private static final float TOUCH_MODE_BRIGHTNESS = 1.09F;
  
  private static final int TOUCH_START_DURATION = 66;
  
  private int dotViewHeight;
  
  private int mAppIconMarginBottom;
  
  private int mAppIconMarginTop;
  
  private Integer[][] mAppIcons;
  
  private int mAppNameSize;
  
  private String[][] mAppNames;
  
  private boolean[][] mCanDraw;
  
  private Runnable mCancelclickRunnable;
  
  private int mChineseLength;
  
  private Context mContext;
  
  private float mCurrentBrightness;
  
  private int mCurrentIconWidth;
  
  private int mCurrentPosition;
  
  private float mDownX;
  
  private float mDownY;
  
  private int mEnglishLength;
  
  private GestureDetector mGestureDetector;
  
  private int mIconDistance;
  
  private int mIconHeight;
  
  private int mIconWidth;
  
  private boolean mIsLandscape;
  
  private int mItemBgPaddingRight;
  
  private int mItemHeight;
  
  private int mItemWidth;
  
  private Drawable mMoreIcon;
  
  private int mMoreIconAlpha;
  
  private int mMoreIconIndex;
  
  private int mNavBarHeight;
  
  private boolean mNeedExpandAnim;
  
  private int mOShareIconMarginBottom;
  
  private int mOShareIconMarginTop;
  
  private OnItemClickListener mOnItemClickListener;
  
  private Runnable mOnclickRunnable;
  
  private int[][] mOpacity;
  
  private int mPaddingLeft;
  
  private int mPaddingTop;
  
  private TextPaint mPaint1;
  
  private Paint mPaint2;
  
  private int mPrivateFlags;
  
  private Float[][] mScale;
  
  private int mSelectColor;
  
  private ColorMatrixColorFilter mSelectColorFilter;
  
  private int mSelectHeight;
  
  private int mSelectWidth;
  
  private int mTextColor;
  
  private int mTextPaddingBottom;
  
  private int mTextPaddingLeft;
  
  private int mTotalHeight;
  
  private OplusBezierInterpolator mTouchEndInterpolator;
  
  private final ColorViewTouchHelper mTouchHelper;
  
  private int mTouchModeWidth;
  
  private OplusBezierInterpolator mTouchStartInterpolator;
  
  private int mTwoLineDistance;
  
  private int selectX;
  
  private int selectY;
  
  public OplusGridView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public OplusGridView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public OplusGridView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    Float float_ = Float.valueOf(1.0F);
    this.mScale = new Float[][] { { float_, float_, float_, float_ }, { float_, float_, float_, float_ } };
    this.mOpacity = new int[][] { { 255, 255, 255, 255 }, { 255, 255, 255, 255 } };
    this.mMoreIconAlpha = 255;
    this.mCanDraw = new boolean[2][4];
    this.mMoreIconIndex = 1;
    this.mTouchStartInterpolator = new OplusBezierInterpolator(0.25D, 0.1D, 0.1D, 1.0D, true);
    this.mTouchEndInterpolator = new OplusBezierInterpolator(0.25D, 0.1D, 0.25D, 1.0D, true);
    this.mCurrentBrightness = 1.0F;
    this.mOnclickRunnable = (Runnable)new Object(this);
    this.mCancelclickRunnable = (Runnable)new Object(this);
    this.mCurrentPosition = -1;
    setLayerType(1, null);
    setFocusable(true);
    setFocusableInTouchMode(true);
    this.mContext = paramContext;
    paramContext.getResources().getConfiguration();
    initGetureDetecor();
    this.mSelectColor = getResources().getColor(201720165);
    this.mTextColor = getResources().getColor(201720164);
    paramInt = getResources().getDimensionPixelSize(201654586);
    float f = (this.mContext.getResources().getConfiguration()).fontScale;
    this.mAppNameSize = (int)OplusChangeTextUtil.getSuitableFontSize(paramInt, f, 2);
    this.dotViewHeight = (int)getResources().getDimension(201654689);
    this.mItemHeight = (int)getResources().getDimension(201654692);
    Resources resources = getResources();
    this.mSelectHeight = (int)resources.getDimension(201654703);
    this.mSelectWidth = (int)getResources().getDimension(201654704);
    this.mPaddingLeft = (int)getResources().getDimension(201654695);
    this.mPaddingTop = (int)getResources().getDimension(201654705);
    this.mOShareIconMarginTop = (int)getResources().getDimension(201654702);
    this.mAppIconMarginTop = (int)getResources().getDimension(201654684);
    this.mIconHeight = getResources().getDimensionPixelSize(201654690);
    this.mIconWidth = paramInt = getResources().getDimensionPixelSize(201654691);
    this.mCurrentIconWidth = paramInt;
    this.mTouchModeWidth = paramInt + dip2px(getContext(), 3.0F);
    this.mItemBgPaddingRight = getResources().getDimensionPixelSize(201654590);
    this.mTextPaddingBottom = getResources().getDimensionPixelSize(201654603);
    this.mTwoLineDistance = getResources().getDimensionPixelSize(201654602);
    this.mChineseLength = getResources().getDimensionPixelSize(201654587);
    this.mEnglishLength = getResources().getDimensionPixelSize(201654588);
    this.mMoreIcon = this.mContext.getDrawable(201851335);
    this.mSelectColorFilter = new ColorMatrixColorFilter(this.mSelectColorMatrix);
    ColorViewTouchHelper colorViewTouchHelper = new ColorViewTouchHelper(this);
    setAccessibilityDelegate(colorViewTouchHelper);
    setImportantForAccessibility(1);
  }
  
  protected void onDraw(Canvas paramCanvas) {
    super.onDraw(paramCanvas);
    Configuration configuration = this.mContext.getResources().getConfiguration();
    if (this.mAppInfos == null)
      return; 
    if (configuration.orientation == 2) {
      this.mRowCounts = 1;
      this.mIsLandscape = true;
    } else {
      this.mRowCounts = Math.min(2, this.mAppInfos.length);
      this.mIsLandscape = false;
    } 
    Context context = this.mContext;
    if (context instanceof Activity && ((Activity)context).isInMultiWindowMode()) {
      this.mRowCounts = 1;
      this.mIsLandscape = true;
    } 
    this.mTextPaddingLeft = 0;
    this.mItemWidth = getWidth() / this.mColumnCounts;
    TextPaint textPaint = new TextPaint();
    textPaint.setColor(this.mTextColor);
    this.mPaint1.setTextSize(this.mAppNameSize);
    this.mPaint1.setTextAlign(Paint.Align.CENTER);
    this.mPaint1.setAntiAlias(true);
    this.mPaint1.setXfermode((Xfermode)new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
    Paint paint = new Paint();
    paint.setColor(this.mSelectColor);
    for (byte b = 0; b < this.mRowCounts; b++) {
      for (byte b1 = 0; b1 < (this.mAppInfos[b]).length; b1++) {
        int i = this.mColumnCounts * b + b1;
        getRect2(b1, b, this.selRect2, i);
        OplusItem[][] arrayOfOplusItem = this.mAppInfos;
        if (arrayOfOplusItem[b][b1] != null) {
          arrayOfOplusItem[b][b1].getIcon().setBounds(this.selRect2);
          if (this.mNeedExpandAnim) {
            if (i == this.mMoreIconIndex) {
              if (i == this.mCurrentPosition) {
                ColorMatrix colorMatrix = this.mSelectColorMatrix;
                float f = this.mCurrentBrightness;
                colorMatrix.setScale(f, f, f, 1.0F);
                this.mSelectColorFilter.setColorMatrix(this.mSelectColorMatrix);
                this.mMoreIcon.setColorFilter((ColorFilter)this.mSelectColorFilter);
              } else {
                this.mSelectColorMatrix.setScale(1.0F, 1.0F, 1.0F, 1.0F);
                this.mSelectColorFilter.setColorMatrix(this.mSelectColorMatrix);
                this.mMoreIcon.setColorFilter((ColorFilter)this.mSelectColorFilter);
              } 
              this.mMoreIcon.setBounds(this.selRect2);
              this.mMoreIcon.setAlpha(this.mMoreIconAlpha);
              this.mMoreIcon.draw(paramCanvas);
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("moreIcon = ");
              stringBuilder.append(i);
              stringBuilder.append(", alpha = ");
              stringBuilder.append(this.mMoreIconAlpha);
              Log.d("OplusGridView", stringBuilder.toString());
            } 
            if (this.mCanDraw[b][b1]) {
              paramCanvas.save();
              float f2 = this.mScale[b][b1].floatValue();
              float f1 = this.selRect2.exactCenterX();
              Rect rect = this.selRect2;
              float f3 = rect.exactCenterY();
              paramCanvas.scale(f2, f2, f1, f3);
              if (i == this.mCurrentPosition) {
                ColorMatrix colorMatrix = this.mSelectColorMatrix;
                f1 = this.mCurrentBrightness;
                colorMatrix.setScale(f1, f1, f1, 1.0F);
                this.mSelectColorFilter.setColorMatrix(this.mSelectColorMatrix);
                this.mAppInfos[b][b1].getIcon().setColorFilter((ColorFilter)this.mSelectColorFilter);
              } else {
                this.mSelectColorMatrix.setScale(1.0F, 1.0F, 1.0F, 1.0F);
                this.mSelectColorFilter.setColorMatrix(this.mSelectColorMatrix);
                this.mAppInfos[b][b1].getIcon().setColorFilter((ColorFilter)this.mSelectColorFilter);
              } 
              this.mAppInfos[b][b1].getIcon().setAlpha(this.mOpacity[b][b1]);
              this.mAppInfos[b][b1].getIcon().draw(paramCanvas);
              paramCanvas.restore();
              this.mPaint1.setAlpha((int)(this.mOpacity[b][b1] * 0.7F));
              drawText(paramCanvas, this.mAppInfos[b][b1].getText(), b, b1);
            } 
          } else {
            if (i == this.mCurrentPosition) {
              ColorMatrix colorMatrix = this.mSelectColorMatrix;
              float f = this.mCurrentBrightness;
              colorMatrix.setScale(f, f, f, 1.0F);
              this.mSelectColorFilter.setColorMatrix(this.mSelectColorMatrix);
              this.mAppInfos[b][b1].getIcon().setColorFilter((ColorFilter)this.mSelectColorFilter);
            } else {
              this.mSelectColorMatrix.setScale(1.0F, 1.0F, 1.0F, 1.0F);
              this.mSelectColorFilter.setColorMatrix(this.mSelectColorMatrix);
              this.mAppInfos[b][b1].getIcon().setColorFilter((ColorFilter)this.mSelectColorFilter);
            } 
            this.mAppInfos[b][b1].getIcon().setAlpha(255);
            this.mAppInfos[b][b1].getIcon().draw(paramCanvas);
            drawText(paramCanvas, this.mAppInfos[b][b1].getText(), b, b1);
          } 
        } 
      } 
    } 
  }
  
  public void setMoreIconIndex(int paramInt) {
    this.mMoreIconIndex = paramInt;
  }
  
  public void startExpandAnimation() {
    this.mNeedExpandAnim = true;
    for (byte b = 0; b < this.mRowCounts; ) {
      byte b1 = 0;
      for (;; b++) {
        OplusItem[][] arrayOfOplusItem = this.mAppInfos;
        if (b1 < (arrayOfOplusItem[b]).length) {
          int i = this.mColumnCounts * b + b1;
          this.mCanDraw[b][b1] = true;
          if (arrayOfOplusItem[b][b1] != null) {
            int j = this.mMoreIconIndex;
            if (i >= j) {
              ValueAnimator valueAnimator1 = getAlphaAnim(b, b1, i - j);
              ValueAnimator valueAnimator2 = getScaleAnim(b, b1, i - this.mMoreIconIndex);
              ValueAnimator valueAnimator3 = getMoreIconAnim();
              AnimatorSet animatorSet = new AnimatorSet();
              animatorSet.playTogether(new Animator[] { (Animator)valueAnimator1, (Animator)valueAnimator2, (Animator)valueAnimator3 });
              animatorSet.start();
            } 
          } 
          b1++;
          continue;
        } 
      } 
    } 
  }
  
  private ValueAnimator getAlphaAnim(int paramInt1, int paramInt2, int paramInt3) {
    ValueAnimator valueAnimator = ValueAnimator.ofInt(new int[] { 0, 255 });
    this.mCanDraw[paramInt1][paramInt2] = false;
    valueAnimator.addListener((Animator.AnimatorListener)new Object(this, paramInt1, paramInt2));
    valueAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new Object(this, paramInt1, paramInt2));
    valueAnimator.setDuration(150L);
    valueAnimator.setInterpolator((TimeInterpolator)new LinearInterpolator());
    paramInt1 = (int)((paramInt3 - Math.floor((paramInt3 + 1)) * 2.0D / 4.0D) * 30.0D + 100.0D);
    valueAnimator.setStartDelay(paramInt1);
    return valueAnimator;
  }
  
  private ValueAnimator getScaleAnim(int paramInt1, int paramInt2, int paramInt3) {
    ValueAnimator valueAnimator = ValueAnimator.ofFloat(new float[] { 0.0F, 1.0F });
    valueAnimator.addListener((Animator.AnimatorListener)new Object(this, paramInt1, paramInt2));
    valueAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new Object(this, paramInt1, paramInt2));
    valueAnimator.setDuration(300L);
    valueAnimator.setInterpolator((TimeInterpolator)POLATOR);
    int i = (int)((paramInt3 - Math.floor((paramInt3 + 1)) * 2.0D / 4.0D) * 30.0D + 100.0D);
    valueAnimator.setStartDelay(i);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("getScaleAnim : ");
    stringBuilder.append(paramInt1);
    stringBuilder.append(", ");
    stringBuilder.append(paramInt2);
    stringBuilder.append(", position : ");
    stringBuilder.append(paramInt3);
    stringBuilder.append(", delay : ");
    stringBuilder.append(i);
    Log.d("OplusGridView", stringBuilder.toString());
    return valueAnimator;
  }
  
  private ValueAnimator getMoreIconAnim() {
    ValueAnimator valueAnimator = ValueAnimator.ofInt(new int[] { 255, 0 });
    valueAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new Object(this));
    valueAnimator.setDuration(150L);
    valueAnimator.setInterpolator((TimeInterpolator)new LinearInterpolator());
    return valueAnimator;
  }
  
  private void drawText(Canvas paramCanvas, String paramString, int paramInt1, int paramInt2) {
    if (isChinese(paramString)) {
      int i = this.mChineseLength;
      if (needFullSpaceForChinese(paramString)) {
        i = this.mEnglishLength;
      } else {
        handleTooLongAppNameStr(paramString, true, this.mAppNameSize);
      } 
      String str = this.mAppInfos[paramInt1][paramInt2].getText();
      int j = this.mPaint1.breakText(str, true, i, null);
      if (j == str.length()) {
        if (getLayoutDirection() == 1) {
          paramCanvas.drawText(paramString, getWidth() - (float)(paramInt2 + 0.5D) * this.mItemWidth - this.mTextPaddingLeft, ((paramInt1 + 1) * this.mItemHeight - this.mTextPaddingBottom), (Paint)this.mPaint1);
        } else {
          paramCanvas.drawText(paramString, (float)(paramInt2 + 0.5D) * this.mItemWidth + this.mTextPaddingLeft, ((paramInt1 + 1) * this.mItemHeight - this.mTextPaddingBottom), (Paint)this.mPaint1);
        } 
      } else {
        if (getLayoutDirection() == 1) {
          paramCanvas.drawText(paramString.substring(0, j), getWidth() - (float)(paramInt2 + 0.5D) * this.mItemWidth - this.mTextPaddingLeft, ((paramInt1 + 1) * this.mItemHeight - this.mTextPaddingBottom), (Paint)this.mPaint1);
        } else {
          paramCanvas.drawText(paramString.substring(0, j), (float)(paramInt2 + 0.5D) * this.mItemWidth + this.mTextPaddingLeft, ((paramInt1 + 1) * this.mItemHeight - this.mTextPaddingBottom), (Paint)this.mPaint1);
        } 
        paramString = paramString.substring(j);
        i = this.mPaint1.breakText(paramString, true, i, null);
        Paint.FontMetricsInt fontMetricsInt = this.mPaint1.getFontMetricsInt();
        j = fontMetricsInt.descent - fontMetricsInt.ascent;
        if (i != paramString.length()) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(paramString.substring(0, i));
          stringBuilder.append("...");
          paramString = stringBuilder.toString();
        } 
        if (getLayoutDirection() == 1) {
          paramCanvas.drawText(paramString, getWidth() - (float)(paramInt2 + 0.5D) * this.mItemWidth - this.mTextPaddingLeft, ((paramInt1 + 1) * this.mItemHeight - this.mTextPaddingBottom + j), (Paint)this.mPaint1);
        } else {
          paramCanvas.drawText(paramString, (float)(paramInt2 + 0.5D) * this.mItemWidth + this.mTextPaddingLeft, ((paramInt1 + 1) * this.mItemHeight - this.mTextPaddingBottom + j), (Paint)this.mPaint1);
        } 
      } 
    } else {
      handleTooLongAppNameStr(paramString, false, this.mAppNameSize);
      drawTextExp(paramCanvas, paramString, paramInt1, paramInt2);
    } 
  }
  
  private void drawTextExp(Canvas paramCanvas, String paramString, int paramInt1, int paramInt2) {
    int i = this.mPaint1.breakText(paramString, true, this.mEnglishLength, null);
    if (i == paramString.length()) {
      if (getLayoutDirection() == 1) {
        paramCanvas.drawText(paramString, getWidth() - (float)(paramInt2 + 0.5D) * this.mItemWidth - this.mTextPaddingLeft, ((paramInt1 + 1) * this.mItemHeight - this.mTextPaddingBottom), (Paint)this.mPaint1);
      } else {
        paramCanvas.drawText(paramString, (float)(paramInt2 + 0.5D) * this.mItemWidth + this.mTextPaddingLeft, ((paramInt1 + 1) * this.mItemHeight - this.mTextPaddingBottom), (Paint)this.mPaint1);
      } 
    } else {
      String str = paramString.substring(0, i);
      int j = str.lastIndexOf(' ');
      Paint.FontMetricsInt fontMetricsInt = this.mPaint1.getFontMetricsInt();
      int k = fontMetricsInt.descent - fontMetricsInt.ascent;
      if (j > 0) {
        str = paramString.substring(0, j);
        paramString = paramString.substring(j);
        i = this.mPaint1.breakText(paramString, true, this.mEnglishLength, null);
        if (i != paramString.length()) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(paramString.substring(0, i));
          stringBuilder.append("...");
          paramString = stringBuilder.toString();
        } 
      } else {
        paramString = paramString.substring(i);
        i = this.mPaint1.breakText(paramString, true, this.mEnglishLength, null);
        if (i != paramString.length()) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(paramString.substring(0, i));
          stringBuilder.append("...");
          paramString = stringBuilder.toString();
        } 
      } 
      if (getLayoutDirection() == 1) {
        paramCanvas.drawText(str, getWidth() - (float)(paramInt2 + 0.5D) * this.mItemWidth - this.mTextPaddingLeft, ((paramInt1 + 1) * this.mItemHeight - this.mTextPaddingBottom), (Paint)this.mPaint1);
        paramCanvas.drawText(paramString, getWidth() - (float)(paramInt2 + 0.5D) * this.mItemWidth - this.mTextPaddingLeft, ((paramInt1 + 1) * this.mItemHeight - this.mTextPaddingBottom + k), (Paint)this.mPaint1);
      } else {
        paramCanvas.drawText(str, (float)(paramInt2 + 0.5D) * this.mItemWidth + this.mTextPaddingLeft, ((paramInt1 + 1) * this.mItemHeight - this.mTextPaddingBottom), (Paint)this.mPaint1);
        paramCanvas.drawText(paramString, (float)(paramInt2 + 0.5D) * this.mItemWidth + this.mTextPaddingLeft, ((paramInt1 + 1) * this.mItemHeight - this.mTextPaddingBottom + k), (Paint)this.mPaint1);
      } 
    } 
  }
  
  private boolean needFullSpaceForChinese(String paramString) {
    this.mPaint1.setTextSize(this.mAppNameSize);
    int i = this.mPaint1.breakText(paramString, true, this.mChineseLength, null);
    if (i < paramString.length()) {
      String str = paramString.substring(i);
      TextPaint textPaint = this.mPaint1;
      float f = this.mChineseLength;
      i = textPaint.breakText(str, true, f, null);
      if (i < str.length()) {
        i = this.mPaint1.breakText(paramString, true, this.mEnglishLength, null);
        paramString = paramString.substring(i);
        TextPaint textPaint1 = this.mPaint1;
        f = this.mEnglishLength;
        i = textPaint1.breakText(paramString, true, f, null);
        if (i == paramString.length())
          return true; 
      } 
    } 
    return false;
  }
  
  private void handleTooLongAppNameStr(String paramString, boolean paramBoolean, float paramFloat) {
    this.mPaint1.setTextSize(paramFloat);
    if (Math.abs(paramFloat - this.mAppNameSize * 0.7744F) < 0.01D)
      return; 
    TextPaint textPaint = this.mPaint1;
    if (paramBoolean) {
      i = this.mChineseLength;
    } else {
      i = this.mEnglishLength;
    } 
    int i = textPaint.breakText(paramString, true, i, null);
    if (i < paramString.length()) {
      String str;
      if (!paramBoolean) {
        str = paramString.substring(0, i);
        int j = str.lastIndexOf(' ');
        if (j > 0) {
          str = paramString.substring(j);
          i = this.mPaint1.breakText(str, true, this.mEnglishLength, null);
        } else {
          str = paramString.substring(i);
          i = this.mPaint1.breakText(str, true, this.mEnglishLength, null);
        } 
      } else {
        str = paramString.substring(i);
        i = this.mPaint1.breakText(str, true, this.mChineseLength, null);
      } 
      if (i < str.length())
        handleTooLongAppNameStr(paramString, paramBoolean, 0.88F * paramFloat); 
    } 
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    if (!isEnabled())
      return true; 
    paramMotionEvent.getPointerCount();
    this.mGestureDetector.onTouchEvent(paramMotionEvent);
    int i = paramMotionEvent.getAction();
    if (i != 0) {
      if (i != 1) {
        if (i != 3)
          return true; 
        this.isSelected = false;
        invalidate(this.selRect);
        this.mPrivateFlags |= 0xFDFFFFFF;
        removeCallbacks(this.mOnclickRunnable);
        this.selRect = new Rect();
        performTouchEndAnim();
        return true;
      } 
      if ((this.mPrivateFlags & 0x2000000) == 0) {
        i = 1;
      } else {
        i = 0;
      } 
      if (i != 0) {
        this.isSelected = true;
        invalidate(this.selRect);
      } else {
        this.isSelected = false;
        invalidate(this.selRect);
      } 
      postDelayed(this.mCancelclickRunnable, ViewConfiguration.getTapTimeout());
      this.selRect = new Rect();
      performTouchEndAnim();
      return true;
    } 
    this.mPrivateFlags |= 0x2000000;
    performTouchStartAnim();
    return true;
  }
  
  public void initGetureDetecor() {
    this.mGestureDetector = new GestureDetector(this.mContext, (GestureDetector.OnGestureListener)new Object(this));
  }
  
  private void click(int paramInt, boolean paramBoolean) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Item click :position = ");
    stringBuilder.append(paramInt);
    stringBuilder.append("; isLongClick = ");
    stringBuilder.append(paramBoolean);
    Log.i("OplusGridView", stringBuilder.toString());
    if (paramInt < this.mItemCounts && paramInt >= 0) {
      int i = this.mPagerSize, j = this.mPageNumber;
      if ((j - 1) * i + paramInt >= 0)
        if (!paramBoolean) {
          this.mOnItemClickListener.OnItemClick(i * (j - 1) + paramInt);
          this.mTouchHelper.sendEventForVirtualView(paramInt, 1);
        } else {
          this.mOnItemClickListener.OnItemLongClick(i * (j - 1) + paramInt);
        }  
    } 
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    setMeasuredDimension(measureWidth(paramInt1), measureHeight(paramInt2));
  }
  
  private int measureHeight(int paramInt) {
    int i = this.mTotalHeight;
    int j = View.MeasureSpec.getMode(paramInt);
    View.MeasureSpec.getSize(paramInt);
    if (j == 1073741824) {
      paramInt = this.mTotalHeight;
    } else {
      paramInt = i;
      if (j == Integer.MIN_VALUE)
        paramInt = this.mTotalHeight; 
    } 
    return paramInt;
  }
  
  private int measureWidth(int paramInt) {
    boolean bool = false;
    int i = View.MeasureSpec.getMode(paramInt);
    int j = View.MeasureSpec.getSize(paramInt);
    paramInt = bool;
    if (i == 1073741824)
      paramInt = j; 
    return paramInt;
  }
  
  private void getRect(int paramInt1, int paramInt2, Rect paramRect) {
    if (isLayoutRtl()) {
      paramInt1 = getWidth() - Math.min(paramInt1 + 1, this.mColumnCounts) * this.mItemWidth;
    } else {
      paramInt1 = this.mItemWidth * paramInt1;
    } 
    int i = this.mItemHeight * paramInt2 + this.mPaddingTop;
    int j = this.mItemWidth;
    paramInt2 = this.mSelectHeight;
    paramRect.set(paramInt1, i, j + paramInt1, paramInt2 + i);
  }
  
  private void getRect2(int paramInt1, int paramInt2, Rect paramRect, int paramInt3) {
    int i;
    if (paramInt3 == this.mCurrentPosition) {
      paramInt3 = this.mItemWidth;
      paramInt3 = paramInt1 * paramInt3 + (int)((paramInt3 - this.mCurrentIconWidth) * 1.0F / 2.0F);
      if (isLayoutRtl())
        if (this.mIsLandscape) {
          paramInt3 = getWidth();
          paramInt1 = Math.min(paramInt1 + 1, this.mColumnCounts);
          int k = this.mItemWidth;
          paramInt3 = paramInt3 - paramInt1 * k + k - this.mPaddingLeft - this.mCurrentIconWidth;
        } else {
          paramInt3 = getWidth() - Math.min(paramInt1 + 1, this.mColumnCounts) * this.mItemWidth + this.mPaddingLeft;
        }  
      int j = this.mItemHeight;
      paramInt1 = this.mAppIconMarginTop;
      i = this.mCurrentIconWidth;
      paramInt1 = j * paramInt2 + paramInt1 - (int)((i - this.mIconWidth) * 1.0F / 2.0F);
      paramInt2 = paramInt3 + i;
      i += paramInt1;
    } else {
      paramInt3 = this.mItemWidth;
      paramInt3 = paramInt1 * paramInt3 + (int)((paramInt3 - this.mIconWidth) * 1.0F / 2.0F);
      if (isLayoutRtl())
        if (this.mIsLandscape) {
          paramInt3 = getWidth();
          int j = Math.min(paramInt1 + 1, this.mColumnCounts);
          paramInt1 = this.mItemWidth;
          paramInt3 = paramInt3 - j * paramInt1 + paramInt1 - this.mPaddingLeft - this.mIconWidth;
        } else {
          paramInt3 = getWidth() - Math.min(paramInt1 + 1, this.mColumnCounts) * this.mItemWidth + this.mPaddingLeft;
        }  
      paramInt1 = this.mItemHeight * paramInt2 + this.mAppIconMarginTop;
      paramInt2 = this.mIconWidth;
      i = paramInt1 + paramInt2;
      paramInt2 = paramInt3 + paramInt2;
    } 
    paramRect.set(paramInt3, paramInt1, paramInt2, i);
  }
  
  private void select(int paramInt1, int paramInt2, Rect paramRect) {
    this.selectX = Math.min(paramInt1, this.mColumnCounts - 1);
    this.selectY = paramInt2 = Math.min(paramInt2, this.mRowCounts - 1);
    paramInt1 = this.mColumnCounts;
    int i = this.selectX;
    if (paramInt1 * paramInt2 + i < this.mItemCounts)
      getRect(i, paramInt2, paramRect); 
  }
  
  public void setPageCount(int paramInt) {
    this.mPageNumber = paramInt;
  }
  
  public void setPagerSize(int paramInt) {
    this.mPagerSize = paramInt;
  }
  
  public void setOnItemClickListener(OnItemClickListener paramOnItemClickListener) {
    this.mOnItemClickListener = paramOnItemClickListener;
  }
  
  public void setAppInfo(OplusItem[][] paramArrayOfOplusItem) {
    this.mAppInfos = paramArrayOfOplusItem;
    this.mRowCounts = paramArrayOfOplusItem.length;
    this.mItemCounts = get2DimenArrayCounts(paramArrayOfOplusItem);
    this.mTotalHeight = this.mItemHeight * this.mRowCounts + this.mPaddingTop;
    this.mMoreIcon.setAlpha(255);
    this.mNeedExpandAnim = false;
    this.mTouchHelper.invalidateRoot();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("mTotalHeight = ");
    stringBuilder.append(this.mTotalHeight);
    Log.d("OplusGridView", stringBuilder.toString());
  }
  
  public OplusItem[][] getAppInfo() {
    return this.mAppInfos;
  }
  
  public static String trancateText(String paramString, int paramInt) {
    String[] arrayOfString = paramString.split(" ");
    StringBuilder stringBuilder = new StringBuilder();
    for (byte b = 0; b < arrayOfString.length; b++) {
      String str = arrayOfString[b];
      if (str.length() > paramInt) {
        stringBuilder.append(str.subSequence(0, paramInt - 1));
        stringBuilder.append("...\n");
      } else {
        stringBuilder.append(str);
        stringBuilder.append("\n");
      } 
    } 
    return stringBuilder.toString();
  }
  
  public static int get2DimenArrayCounts(OplusItem[][] paramArrayOfOplusItem) {
    int i = 0;
    for (byte b = 0; b < paramArrayOfOplusItem.length; b++) {
      for (byte b1 = 0; b1 < (paramArrayOfOplusItem[b]).length; b1++, i = j) {
        int j = i;
        if (paramArrayOfOplusItem[b][b1] != null)
          j = i + 1; 
      } 
    } 
    return i;
  }
  
  public static int getNum(String paramString) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_1
    //   2: iconst_0
    //   3: istore_2
    //   4: iconst_0
    //   5: istore_3
    //   6: iload_3
    //   7: aload_0
    //   8: invokevirtual length : ()I
    //   11: if_icmpge -> 96
    //   14: aload_0
    //   15: iload_3
    //   16: invokevirtual charAt : (I)C
    //   19: invokestatic toString : (C)Ljava/lang/String;
    //   22: astore #4
    //   24: iload_1
    //   25: istore #5
    //   27: aload #4
    //   29: ldc_w '^[一-龥]{1}$'
    //   32: invokevirtual matches : (Ljava/lang/String;)Z
    //   35: ifeq -> 43
    //   38: iload_1
    //   39: iconst_1
    //   40: iadd
    //   41: istore #5
    //   43: aload_0
    //   44: iload_3
    //   45: invokevirtual charAt : (I)C
    //   48: istore_1
    //   49: iload_1
    //   50: bipush #97
    //   52: if_icmplt -> 61
    //   55: iload_1
    //   56: bipush #122
    //   58: if_icmple -> 79
    //   61: iload_2
    //   62: istore #6
    //   64: iload_1
    //   65: bipush #65
    //   67: if_icmplt -> 84
    //   70: iload_2
    //   71: istore #6
    //   73: iload_1
    //   74: bipush #90
    //   76: if_icmpgt -> 84
    //   79: iload_2
    //   80: iconst_1
    //   81: iadd
    //   82: istore #6
    //   84: iinc #3, 1
    //   87: iload #5
    //   89: istore_1
    //   90: iload #6
    //   92: istore_2
    //   93: goto -> 6
    //   96: iload_1
    //   97: i2d
    //   98: iload_2
    //   99: iconst_2
    //   100: idiv
    //   101: i2d
    //   102: invokestatic ceil : (D)D
    //   105: dadd
    //   106: d2i
    //   107: istore_3
    //   108: iload_3
    //   109: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #951	-> 0
    //   #952	-> 0
    //   #953	-> 2
    //   #954	-> 4
    //   #955	-> 14
    //   #956	-> 24
    //   #957	-> 38
    //   #959	-> 43
    //   #960	-> 49
    //   #961	-> 79
    //   #954	-> 84
    //   #964	-> 96
    //   #965	-> 108
  }
  
  public static boolean isChinese(String paramString) {
    int i = 0;
    for (byte b = 0; b < paramString.length(); b++, i = j) {
      String str = Character.toString(paramString.charAt(b));
      int j = i;
      if (str.matches("^[一-龥]{1}$"))
        j = i + 1; 
    } 
    if (i > 0)
      return true; 
    return false;
  }
  
  protected boolean dispatchHoverEvent(MotionEvent paramMotionEvent) {
    if (this.mTouchHelper.dispatchHoverEvent(paramMotionEvent))
      return true; 
    return super.dispatchHoverEvent(paramMotionEvent);
  }
  
  OplusItem getAppinfo(int paramInt) {
    OplusItem oplusItem, arrayOfOplusItem1[][] = null;
    OplusItem[][] arrayOfOplusItem2 = this.mAppInfos, arrayOfOplusItem3 = arrayOfOplusItem1;
    if (arrayOfOplusItem2 != null) {
      arrayOfOplusItem3 = arrayOfOplusItem1;
      if (paramInt > -1) {
        arrayOfOplusItem3 = arrayOfOplusItem1;
        if (paramInt < get2DimenArrayCounts(arrayOfOplusItem2)) {
          arrayOfOplusItem3 = this.mAppInfos;
          oplusItem = arrayOfOplusItem3[paramInt / (arrayOfOplusItem3[0]).length][paramInt % (arrayOfOplusItem3[0]).length];
        } 
      } 
    } 
    return oplusItem;
  }
  
  OplusItem getAccessibilityFocus() {
    int i = this.mTouchHelper.getFocusedVirtualView();
    OplusItem oplusItem = null;
    if (i >= 0)
      oplusItem = getAppinfo(i); 
    return oplusItem;
  }
  
  public void clearAccessibilityFocus() {
    ColorViewTouchHelper colorViewTouchHelper = this.mTouchHelper;
    if (colorViewTouchHelper != null)
      colorViewTouchHelper.clearFocusedVirtualView(); 
  }
  
  boolean restoreAccessibilityFocus(int paramInt) {
    if (paramInt < 0 || paramInt >= this.mItemCounts)
      return false; 
    ColorViewTouchHelper colorViewTouchHelper = this.mTouchHelper;
    if (colorViewTouchHelper != null)
      colorViewTouchHelper.setFocusedVirtualView(paramInt); 
    return true;
  }
  
  private CharSequence getItemDescription(int paramInt) {
    if (paramInt < this.mItemCounts) {
      OplusItem oplusItem = getAppinfo(paramInt);
      if (oplusItem != null && oplusItem != null && oplusItem.getText() != null)
        return oplusItem.getText(); 
    } 
    return getClass().getSimpleName();
  }
  
  class ColorViewTouchHelper extends ExploreByTouchHelper {
    private final Rect mTempRect = new Rect();
    
    final OplusGridView this$0;
    
    public ColorViewTouchHelper(View param1View) {
      super(param1View);
    }
    
    public void setFocusedVirtualView(int param1Int) {
      getAccessibilityNodeProvider(OplusGridView.this).performAction(param1Int, 64, null);
    }
    
    public void clearFocusedVirtualView() {
      int i = getFocusedVirtualView();
      if (i != Integer.MIN_VALUE)
        getAccessibilityNodeProvider(OplusGridView.this).performAction(i, 128, null); 
    }
    
    protected int getVirtualViewAt(float param1Float1, float param1Float2) {
      int i;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getVirtualViewAt --> ondown getwidth = ");
      stringBuilder.append(OplusGridView.this.getWidth());
      stringBuilder.append(" --> downX = ");
      stringBuilder.append(param1Float1);
      Log.d("View", stringBuilder.toString());
      if (OplusGridView.this.isLayoutRtl()) {
        int j = (int)(param1Float2 / OplusGridView.this.mItemHeight);
        i = OplusGridView.this.mColumnCounts;
        OplusGridView oplusGridView = OplusGridView.this;
        i = j * i + (int)((oplusGridView.getWidth() - param1Float1) / OplusGridView.this.mItemWidth);
      } else {
        i = (int)(param1Float2 / OplusGridView.this.mItemHeight) * OplusGridView.this.mColumnCounts + (int)(param1Float1 / OplusGridView.this.mItemWidth);
      } 
      if (i >= 0)
        return i; 
      return Integer.MIN_VALUE;
    }
    
    protected void getVisibleVirtualViews(IntArray param1IntArray) {
      for (byte b = 0; b < OplusGridView.this.mItemCounts; b++)
        param1IntArray.add(b); 
    }
    
    protected void onPopulateEventForVirtualView(int param1Int, AccessibilityEvent param1AccessibilityEvent) {
      param1AccessibilityEvent.setContentDescription(OplusGridView.this.getItemDescription(param1Int));
    }
    
    protected void onPopulateNodeForVirtualView(int param1Int, AccessibilityNodeInfo param1AccessibilityNodeInfo) {
      getItemBounds(param1Int, this.mTempRect);
      param1AccessibilityNodeInfo.setContentDescription(OplusGridView.this.getItemDescription(param1Int));
      param1AccessibilityNodeInfo.setBoundsInParent(this.mTempRect);
      param1AccessibilityNodeInfo.addAction(16);
      if (param1Int == OplusGridView.this.mCurrentPosition)
        param1AccessibilityNodeInfo.setSelected(true); 
    }
    
    protected boolean onPerformActionForVirtualView(int param1Int1, int param1Int2, Bundle param1Bundle) {
      if (param1Int2 != 16)
        return false; 
      OplusGridView.this.click(param1Int1, false);
      return true;
    }
    
    private void getItemBounds(int param1Int, Rect param1Rect) {
      if (param1Int >= 0 && param1Int < OplusGridView.this.mItemCounts) {
        OplusGridView oplusGridView = OplusGridView.this;
        oplusGridView.getRect(param1Int % (oplusGridView.mAppInfos[0]).length, param1Int / (OplusGridView.this.mAppInfos[0]).length, param1Rect);
      } 
    }
  }
  
  private int dip2px(Context paramContext, float paramFloat) {
    float f = (paramContext.getResources().getDisplayMetrics()).density;
    return (int)(paramFloat * f + 0.5F);
  }
  
  private void performTouchStartAnim() {
    PropertyValuesHolder propertyValuesHolder1 = PropertyValuesHolder.ofInt("widthHolder", new int[] { this.mCurrentIconWidth, this.mTouchModeWidth });
    PropertyValuesHolder propertyValuesHolder2 = PropertyValuesHolder.ofFloat("brightnessHolder", new float[] { this.mCurrentBrightness, 1.09F });
    ValueAnimator valueAnimator = ValueAnimator.ofPropertyValuesHolder(new PropertyValuesHolder[] { propertyValuesHolder1, propertyValuesHolder2 });
    valueAnimator.setInterpolator((TimeInterpolator)this.mTouchStartInterpolator);
    valueAnimator.setDuration(66L);
    valueAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new Object(this));
    valueAnimator.start();
  }
  
  private void performTouchEndAnim() {
    Log.i("OplusGridView", "Item touched end,performTouchEndAnim.");
    PropertyValuesHolder propertyValuesHolder1 = PropertyValuesHolder.ofInt("widthHolder", new int[] { this.mCurrentIconWidth, this.mIconWidth });
    PropertyValuesHolder propertyValuesHolder2 = PropertyValuesHolder.ofFloat("brightnessHolder", new float[] { this.mCurrentBrightness, 1.0F });
    ValueAnimator valueAnimator = ValueAnimator.ofPropertyValuesHolder(new PropertyValuesHolder[] { propertyValuesHolder1, propertyValuesHolder2 });
    valueAnimator.setInterpolator((TimeInterpolator)this.mTouchEndInterpolator);
    valueAnimator.setDuration(300L);
    valueAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new Object(this));
    valueAnimator.start();
  }
  
  class OnItemClickListener {
    public abstract void OnItemClick(int param1Int);
    
    public abstract void OnItemLongClick(int param1Int);
  }
}
