package com.oplus.widget;

import android.animation.PropertyValuesHolder;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.OplusBezierInterpolator;
import android.widget.Button;

public class OplusFadeBackButton extends Button {
  private OplusBezierInterpolator mTouchStartInterpolator = new OplusBezierInterpolator(0.25D, 0.1D, 0.1D, 1.0D, true);
  
  private OplusBezierInterpolator mTouchEndInterpolator = new OplusBezierInterpolator(0.25D, 0.1D, 0.25D, 1.0D, true);
  
  private float mCurrentScale = 1.0F;
  
  private ValueAnimator mAnimator;
  
  private static final int TOUCH_START_DURATION = 200;
  
  private static final int TOUCH_END_DURATION = 300;
  
  public OplusFadeBackButton(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public OplusFadeBackButton(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public OplusFadeBackButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getAction();
    if (i != 0) {
      if (i == 1 || i == 3)
        performTouchEndAnim(); 
    } else {
      performTouchStartAnim();
    } 
    return super.onTouchEvent(paramMotionEvent);
  }
  
  private void performTouchStartAnim() {
    ValueAnimator valueAnimator2 = this.mAnimator;
    if (valueAnimator2 != null && valueAnimator2.isRunning())
      this.mAnimator.cancel(); 
    PropertyValuesHolder propertyValuesHolder = PropertyValuesHolder.ofFloat("scaleHolder", new float[] { this.mCurrentScale, 0.9F });
    ValueAnimator valueAnimator1 = ValueAnimator.ofPropertyValuesHolder(new PropertyValuesHolder[] { propertyValuesHolder });
    valueAnimator1.setInterpolator((TimeInterpolator)this.mTouchStartInterpolator);
    this.mAnimator.setDuration(200L);
    this.mAnimator.addUpdateListener(new _$$Lambda$OplusFadeBackButton$il_u1Xlafc3VPwosLrqH0tIDkLE(this));
    this.mAnimator.start();
  }
  
  private void performTouchEndAnim() {
    ValueAnimator valueAnimator2 = this.mAnimator;
    if (valueAnimator2 != null && valueAnimator2.isRunning())
      this.mAnimator.cancel(); 
    PropertyValuesHolder propertyValuesHolder = PropertyValuesHolder.ofFloat("scaleHolder", new float[] { this.mCurrentScale, 1.0F });
    ValueAnimator valueAnimator1 = ValueAnimator.ofPropertyValuesHolder(new PropertyValuesHolder[] { propertyValuesHolder });
    valueAnimator1.setInterpolator((TimeInterpolator)this.mTouchEndInterpolator);
    this.mAnimator.setDuration(300L);
    this.mAnimator.addUpdateListener(new _$$Lambda$OplusFadeBackButton$QNFx2xEHCJAxtiUmMLwqS6XDIvU(this));
    this.mAnimator.start();
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    ValueAnimator valueAnimator = this.mAnimator;
    if (valueAnimator != null && valueAnimator.isRunning()) {
      this.mAnimator.cancel();
      setScaleX(1.0F);
      setScaleY(1.0F);
    } 
  }
}
