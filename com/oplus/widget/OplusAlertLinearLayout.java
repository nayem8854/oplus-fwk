package com.oplus.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ViewOutlineProvider;
import android.widget.LinearLayout;
import com.oplus.internal.R;

public final class OplusAlertLinearLayout extends LinearLayout {
  private int mShadowLeft = 0;
  
  private int mShadowTop = 0;
  
  private int mShadowRight = 0;
  
  private int mShadowBottom = 0;
  
  public static final int TYPE_SHADOW_WITH_CORNER = 0;
  
  private Drawable mBackgroundDrawable;
  
  private int mBackgroundRadius;
  
  private int mFixedBottom;
  
  private int mFixedLeft;
  
  private int mFixedRight;
  
  private int mFixedTop;
  
  private boolean mHasShadow;
  
  private boolean mNeedClip;
  
  private Drawable mShadowDrawable;
  
  public OplusAlertLinearLayout(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public OplusAlertLinearLayout(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public OplusAlertLinearLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    int i = paramContext.getResources().getDimensionPixelSize(201654393);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.OplusAlertLinearLayout, paramInt, 0);
    this.mBackgroundRadius = typedArray.getDimensionPixelSize(1, i);
    this.mShadowDrawable = paramContext.getResources().getDrawable(201851232);
    if (typedArray.hasValue(2))
      this.mShadowDrawable = typedArray.getDrawable(2); 
    this.mBackgroundDrawable = paramContext.getDrawable(201851230);
    if (typedArray.hasValue(0))
      this.mBackgroundDrawable = typedArray.getDrawable(0); 
    typedArray.recycle();
  }
  
  public void setHasShadow(boolean paramBoolean) {
    this.mHasShadow = paramBoolean;
    if (paramBoolean) {
      setBackground(this.mShadowDrawable);
      this.mShadowLeft = getPaddingLeft();
      this.mShadowRight = getPaddingRight();
      this.mShadowTop = getPaddingTop();
      this.mShadowBottom = getPaddingBottom();
    } else {
      setBackground(null);
      setPadding(0, 0, 0, 0);
      this.mShadowLeft = 0;
      this.mShadowTop = 0;
      this.mShadowRight = 0;
      this.mShadowBottom = 0;
    } 
    requestLayout();
  }
  
  public void setType(int paramInt) {
    boolean bool;
    if (paramInt == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    setHasShadow(bool);
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    this.mFixedLeft = this.mShadowLeft;
    this.mFixedTop = this.mShadowTop;
    this.mFixedRight = paramInt1 - this.mShadowRight;
    this.mFixedBottom = paramInt2 - this.mShadowBottom;
    if (this.mNeedClip) {
      clipBackground();
    } else {
      setClipToOutline(false);
    } 
  }
  
  public void setNeedClip(boolean paramBoolean) {
    this.mNeedClip = paramBoolean;
  }
  
  private void clipBackground() {
    Object object = new Object(this);
    setClipToOutline(true);
    setOutlineProvider((ViewOutlineProvider)object);
  }
  
  protected void dispatchDraw(Canvas paramCanvas) {
    Drawable drawable;
    paramCanvas.save();
    if (this.mHasShadow) {
      drawable = this.mBackgroundDrawable;
    } else {
      drawable = getNoShadowDrawable();
    } 
    this.mBackgroundDrawable = drawable;
    if (drawable != null) {
      drawable.setBounds(this.mFixedLeft, this.mFixedTop, this.mFixedRight, this.mFixedBottom);
      this.mBackgroundDrawable.draw(paramCanvas);
    } 
    paramCanvas.restore();
    super.dispatchDraw(paramCanvas);
  }
  
  public Drawable getNoShadowDrawable() {
    return getContext().getDrawable(201851231);
  }
}
