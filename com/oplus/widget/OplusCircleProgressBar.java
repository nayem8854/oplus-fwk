package com.oplus.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import com.oplus.internal.R;
import com.oplus.util.OplusContextUtil;
import java.util.ArrayList;

public class OplusCircleProgressBar extends View {
  private final boolean DEBUG = false;
  
  private int mWidth = 0;
  
  private int mHeight = 0;
  
  private int mProgressBarType = 0;
  
  private int mStrokeWidth = 0;
  
  private int mPointRadius = 0;
  
  private int mMax = 100;
  
  private int mProgress = 0;
  
  private int mCurrentStepProgress = 0;
  
  private int mPreStepProgress = -1;
  
  private float mStepDegree = 1.0F;
  
  private ArrayList<ProgressPoint> mPointList = new ArrayList<>();
  
  public static final int ACCURACY = 2;
  
  private static final int ALPHA_SHOW_DURATION = 360;
  
  private static final float BASE_PROGRESS_POINT_ALPHA = 0.215F;
  
  public static final int DEFAULT_TYPE = 0;
  
  public static final int LARGE_TYPE = 2;
  
  public static final int MEDIUM_TYPE = 1;
  
  private static final int ONE_CIRCLE_DEGREE = 360;
  
  public static final int ORIGINAL_ANGLE = -90;
  
  private static final int PROGRESS_POINT_COUNT = 360;
  
  private static final String TAG = "OplusCircleProgressBar";
  
  private static final int TIMEOUT_SEND_ACCESSIBILITY_EVENT = 10;
  
  private float arcRadius;
  
  private AccessibilityEventSender mAccessibilityEventSender;
  
  private RectF mArcRect;
  
  private Paint mBackGroundPaint;
  
  private float mCenterX;
  
  private float mCenterY;
  
  private Context mContext;
  
  private int mHalfStrokeWidth;
  
  private int mHalfWidth;
  
  private AccessibilityManager mManager;
  
  private int mProgressBarBgCircleColor;
  
  private int mProgressBarColor;
  
  private Paint mProgressPaint;
  
  private int mStrokeDefaultWidth;
  
  private int mStrokeLargeWidth;
  
  private int mStrokeMediumWidth;
  
  public OplusCircleProgressBar(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public OplusCircleProgressBar(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 201981987);
  }
  
  public OplusCircleProgressBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    this.mContext = paramContext;
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.OplusCircleProgressBar, paramInt, 0);
    paramInt = getResources().getDimensionPixelSize(201654570);
    this.mWidth = typedArray.getDimensionPixelSize(6, paramInt);
    this.mHeight = typedArray.getDimensionPixelSize(4, paramInt);
    this.mProgressBarType = typedArray.getInteger(5, 0);
    paramInt = OplusContextUtil.getAttrColor(paramContext, 201982027);
    int i = OplusContextUtil.getAttrColor(paramContext, 201982029);
    this.mProgressBarBgCircleColor = typedArray.getColor(2, i);
    this.mProgressBarColor = typedArray.getColor(3, paramInt);
    this.mProgress = typedArray.getInteger(1, this.mProgress);
    this.mMax = typedArray.getInteger(0, this.mMax);
    typedArray.recycle();
    this.mStrokeDefaultWidth = paramContext.getResources().getDimensionPixelSize(201654567);
    this.mStrokeMediumWidth = paramContext.getResources().getDimensionPixelSize(201654566);
    this.mStrokeLargeWidth = paramInt = paramContext.getResources().getDimensionPixelSize(201654565);
    this.mStrokeWidth = this.mStrokeDefaultWidth;
    i = this.mProgressBarType;
    if (1 == i) {
      this.mStrokeWidth = this.mStrokeMediumWidth;
    } else if (2 == i) {
      this.mStrokeWidth = paramInt;
    } 
    this.mPointRadius = this.mStrokeWidth >> 1;
    this.mCenterX = (this.mWidth >> 1);
    this.mCenterY = (this.mHeight >> 1);
    init();
  }
  
  private void init() {
    if (Build.VERSION.SDK_INT > 16 && 
      getImportantForAccessibility() == 0)
      setImportantForAccessibility(1); 
    initBackgroundPaint();
    initProgressPaint();
    setProgress(this.mProgress);
    setMax(this.mMax);
    this.mManager = (AccessibilityManager)this.mContext.getSystemService("accessibility");
  }
  
  private void initProgressPaint() {
    Paint paint = new Paint(1);
    paint.setStyle(Paint.Style.FILL_AND_STROKE);
    this.mProgressPaint.setColor(this.mProgressBarColor);
    this.mProgressPaint.setStyle(Paint.Style.STROKE);
    this.mProgressPaint.setStrokeWidth(this.mStrokeWidth);
    this.mProgressPaint.setStrokeCap(Paint.Cap.ROUND);
  }
  
  private void initBackgroundPaint() {
    Paint paint = new Paint(1);
    paint.setColor(this.mProgressBarBgCircleColor);
    this.mBackGroundPaint.setStyle(Paint.Style.STROKE);
  }
  
  private void verifyProgress() {
    int i = this.mMax;
    if (i > 0) {
      float f = i / 360.0F;
      this.mCurrentStepProgress = i = (int)(this.mProgress / f);
      if (360 - i < 2)
        this.mCurrentStepProgress = 360; 
      this.mPreStepProgress = this.mCurrentStepProgress;
    } else {
      this.mCurrentStepProgress = 0;
      this.mPreStepProgress = 0;
    } 
    invalidate();
  }
  
  public void onDraw(Canvas paramCanvas) {
    drawBackgroudCicle(paramCanvas);
    paramCanvas.save();
    int i = this.mHalfWidth;
    paramCanvas.rotate(-90.0F, i, i);
    paramCanvas.drawArc(this.mArcRect, 0.0F, this.mCurrentStepProgress, false, this.mProgressPaint);
    paramCanvas.restore();
  }
  
  private void drawBackgroudCicle(Canvas paramCanvas) {
    this.mBackGroundPaint.setStrokeWidth(this.mStrokeWidth);
    int i = this.mHalfWidth;
    paramCanvas.drawCircle(i, i, this.arcRadius, this.mBackGroundPaint);
  }
  
  public void setProgress(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("setProgress: ");
    stringBuilder.append(paramInt);
    Log.i("OplusCircleProgressBar", stringBuilder.toString());
    int i = paramInt;
    if (paramInt < 0)
      i = 0; 
    paramInt = i;
    if (i > this.mMax)
      paramInt = this.mMax; 
    if (paramInt != this.mProgress)
      this.mProgress = paramInt; 
    verifyProgress();
    onProgressRefresh();
  }
  
  public int getProgress() {
    return this.mProgress;
  }
  
  public void setMax(int paramInt) {
    int i = paramInt;
    if (paramInt < 0)
      i = 0; 
    if (i != this.mMax) {
      this.mMax = i;
      if (this.mProgress > i)
        this.mProgress = i; 
    } 
    verifyProgress();
  }
  
  public int getMax() {
    return this.mMax;
  }
  
  void onProgressRefresh() {
    AccessibilityManager accessibilityManager = this.mManager;
    if (accessibilityManager != null && accessibilityManager.isEnabled()) {
      accessibilityManager = this.mManager;
      if (accessibilityManager.isTouchExplorationEnabled())
        scheduleAccessibilityEventSender(); 
    } 
  }
  
  private void scheduleAccessibilityEventSender() {
    AccessibilityEventSender accessibilityEventSender = this.mAccessibilityEventSender;
    if (accessibilityEventSender == null) {
      this.mAccessibilityEventSender = new AccessibilityEventSender();
    } else {
      removeCallbacks(accessibilityEventSender);
    } 
    postDelayed(this.mAccessibilityEventSender, 10L);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    setMeasuredDimension(this.mWidth, this.mHeight);
  }
  
  protected void onDetachedFromWindow() {
    AccessibilityEventSender accessibilityEventSender = this.mAccessibilityEventSender;
    if (accessibilityEventSender != null)
      removeCallbacks(accessibilityEventSender); 
    super.onDetachedFromWindow();
  }
  
  protected Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    SavedState savedState = new SavedState(parcelable);
    savedState.mProgress = this.mProgress;
    return (Parcelable)savedState;
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    this.mHalfStrokeWidth = this.mStrokeWidth / 2;
    this.mHalfWidth = paramInt1 = getWidth() / 2;
    this.arcRadius = (paramInt1 - this.mHalfStrokeWidth);
    paramInt1 = this.mHalfWidth;
    float f1 = paramInt1, f2 = this.arcRadius;
    this.mArcRect = new RectF(f1 - f2, paramInt1 - f2, paramInt1 + f2, paramInt1 + f2);
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    SavedState savedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(savedState.getSuperState());
    setProgress(savedState.mProgress);
    requestLayout();
  }
  
  class AccessibilityEventSender implements Runnable {
    final OplusCircleProgressBar this$0;
    
    private AccessibilityEventSender() {}
    
    public void run() {
      OplusCircleProgressBar.this.sendAccessibilityEvent(4);
    }
  }
  
  static class SavedState extends View.BaseSavedState {
    SavedState(Parcelable param1Parcelable) {
      super(param1Parcelable);
    }
    
    private SavedState(Parcel param1Parcel) {
      super(param1Parcel);
      this.mProgress = ((Integer)param1Parcel.readValue(null)).intValue();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeValue(Integer.valueOf(this.mProgress));
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("OplusCircleProgressBar.SavedState { ");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(" mProgress = ");
      stringBuilder.append(this.mProgress);
      stringBuilder.append(" }");
      return stringBuilder.toString();
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    int mProgress;
  }
  
  class ProgressPoint {
    float currentAlpha;
    
    final OplusCircleProgressBar this$0;
    
    public float getCurrentAlpha() {
      return this.currentAlpha;
    }
    
    public void setCurrentAlpha(float param1Float) {
      this.currentAlpha = param1Float;
    }
  }
}
