package com.oplus.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import com.oplus.internal.R;

public class OplusMaxLinearLayout extends LinearLayout {
  public static final int INVALID_MAX_VALUE = 2147483647;
  
  private int mLandscapeMaxHeight;
  
  private int mLandscapeMaxWidth;
  
  private int mPortraitMaxHeight;
  
  private int mPortraitMaxWidth;
  
  public OplusMaxLinearLayout(Context paramContext) {
    super(paramContext);
  }
  
  public OplusMaxLinearLayout(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.OplusMaxLinearLayout);
    this.mPortraitMaxHeight = typedArray.getDimensionPixelSize(2, 2147483647);
    this.mLandscapeMaxHeight = typedArray.getDimensionPixelSize(0, 2147483647);
    this.mPortraitMaxWidth = typedArray.getDimensionPixelSize(3, 2147483647);
    this.mLandscapeMaxWidth = typedArray.getDimensionPixelSize(1, 2147483647);
    typedArray.recycle();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    super.onMeasure(paramInt1, paramInt2);
    boolean bool = false;
    int i = getMeasuredWidth();
    int j = getMeasuredHeight();
    int k = getMaxWidth();
    int m = getMaxHeight();
    if (i > k) {
      paramInt1 = View.MeasureSpec.makeMeasureSpec(k, 1073741824);
      bool = true;
    } 
    if (j > m) {
      paramInt2 = View.MeasureSpec.makeMeasureSpec(m, 1073741824);
      bool = true;
    } 
    if (bool)
      super.onMeasure(paramInt1, paramInt2); 
  }
  
  public void setPortraitMaxWidth(int paramInt) {
    this.mPortraitMaxWidth = paramInt;
  }
  
  public void setPortraitMaxHeight(int paramInt) {
    this.mPortraitMaxHeight = paramInt;
  }
  
  public void setLandscapeMaxWidth(int paramInt) {
    this.mLandscapeMaxWidth = paramInt;
  }
  
  public void setLandscapeMaxHeight(int paramInt) {
    this.mLandscapeMaxHeight = paramInt;
  }
  
  public int getMaxWidth() {
    int i;
    if (isPortrait()) {
      i = this.mPortraitMaxWidth;
    } else {
      i = this.mLandscapeMaxWidth;
    } 
    return i;
  }
  
  public int getMaxHeight() {
    int i;
    if (isPortrait()) {
      i = this.mPortraitMaxHeight;
    } else {
      i = this.mLandscapeMaxHeight;
    } 
    return i;
  }
  
  private boolean isPortrait() {
    boolean bool;
    DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
    if (displayMetrics.widthPixels < displayMetrics.heightPixels) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
