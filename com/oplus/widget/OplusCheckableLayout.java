package com.oplus.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;
import android.widget.LinearLayout;

public class OplusCheckableLayout extends LinearLayout implements Checkable {
  private Checkable mCheckable;
  
  public OplusCheckableLayout(Context paramContext) {
    super(paramContext);
  }
  
  public OplusCheckableLayout(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public OplusCheckableLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  protected void onFinishInflate() {
    super.onFinishInflate();
    int i = getChildCount();
    if (i > 0)
      for (byte b = 0; b < i; b++) {
        View view = getChildAt(b);
        if (view instanceof Checkable) {
          this.mCheckable = (Checkable)view;
          return;
        } 
      }  
  }
  
  public void setChecked(boolean paramBoolean) {
    Checkable checkable = this.mCheckable;
    if (checkable != null)
      checkable.setChecked(paramBoolean); 
  }
  
  public boolean isChecked() {
    boolean bool;
    Checkable checkable = this.mCheckable;
    if (checkable != null && checkable.isChecked()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void toggle() {
    Checkable checkable = this.mCheckable;
    if (checkable != null)
      checkable.toggle(); 
  }
}
