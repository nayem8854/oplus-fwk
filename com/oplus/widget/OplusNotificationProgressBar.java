package com.oplus.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ProgressBar;
import android.widget.RemoteViews.RemoteView;

@RemoteView
public class OplusNotificationProgressBar extends ProgressBar {
  public OplusNotificationProgressBar(Context paramContext) {
    super(paramContext);
    initProgressDrawable();
  }
  
  public OplusNotificationProgressBar(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    initProgressDrawable();
  }
  
  public OplusNotificationProgressBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    initProgressDrawable();
  }
  
  public OplusNotificationProgressBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    initProgressDrawable();
  }
  
  private void initProgressDrawable() {
    setProgressDrawable(getResources().getDrawable(201850909));
  }
}
