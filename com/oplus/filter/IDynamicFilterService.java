package com.oplus.filter;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IDynamicFilterService extends IInterface {
  void addToFilter(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  String getFilterTagValue(String paramString1, String paramString2) throws RemoteException;
  
  boolean hasFilter(String paramString) throws RemoteException;
  
  boolean inFilter(String paramString1, String paramString2) throws RemoteException;
  
  void removeFromFilter(String paramString1, String paramString2) throws RemoteException;
  
  class Default implements IDynamicFilterService {
    public boolean hasFilter(String param1String) throws RemoteException {
      return false;
    }
    
    public boolean inFilter(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public void addToFilter(String param1String1, String param1String2, String param1String3) throws RemoteException {}
    
    public void removeFromFilter(String param1String1, String param1String2) throws RemoteException {}
    
    public String getFilterTagValue(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDynamicFilterService {
    private static final String DESCRIPTOR = "com.oplus.filter.IDynamicFilterService";
    
    static final int TRANSACTION_addToFilter = 3;
    
    static final int TRANSACTION_getFilterTagValue = 5;
    
    static final int TRANSACTION_hasFilter = 1;
    
    static final int TRANSACTION_inFilter = 2;
    
    static final int TRANSACTION_removeFromFilter = 4;
    
    public Stub() {
      attachInterface(this, "com.oplus.filter.IDynamicFilterService");
    }
    
    public static IDynamicFilterService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.filter.IDynamicFilterService");
      if (iInterface != null && iInterface instanceof IDynamicFilterService)
        return (IDynamicFilterService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "getFilterTagValue";
            } 
            return "removeFromFilter";
          } 
          return "addToFilter";
        } 
        return "inFilter";
      } 
      return "hasFilter";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str2;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("com.oplus.filter.IDynamicFilterService");
                return true;
              } 
              param1Parcel1.enforceInterface("com.oplus.filter.IDynamicFilterService");
              String str5 = param1Parcel1.readString();
              str1 = param1Parcel1.readString();
              str1 = getFilterTagValue(str5, str1);
              param1Parcel2.writeNoException();
              param1Parcel2.writeString(str1);
              return true;
            } 
            str1.enforceInterface("com.oplus.filter.IDynamicFilterService");
            String str4 = str1.readString();
            str1 = str1.readString();
            removeFromFilter(str4, str1);
            return true;
          } 
          str1.enforceInterface("com.oplus.filter.IDynamicFilterService");
          str2 = str1.readString();
          String str3 = str1.readString();
          str1 = str1.readString();
          addToFilter(str2, str3, str1);
          return true;
        } 
        str1.enforceInterface("com.oplus.filter.IDynamicFilterService");
        String str = str1.readString();
        str1 = str1.readString();
        boolean bool1 = inFilter(str, str1);
        str2.writeNoException();
        str2.writeInt(bool1);
        return true;
      } 
      str1.enforceInterface("com.oplus.filter.IDynamicFilterService");
      String str1 = str1.readString();
      boolean bool = hasFilter(str1);
      str2.writeNoException();
      str2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements IDynamicFilterService {
      public static IDynamicFilterService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.filter.IDynamicFilterService";
      }
      
      public boolean hasFilter(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.filter.IDynamicFilterService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IDynamicFilterService.Stub.getDefaultImpl() != null) {
            bool1 = IDynamicFilterService.Stub.getDefaultImpl().hasFilter(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean inFilter(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.filter.IDynamicFilterService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IDynamicFilterService.Stub.getDefaultImpl() != null) {
            bool1 = IDynamicFilterService.Stub.getDefaultImpl().inFilter(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addToFilter(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.filter.IDynamicFilterService");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeString(param2String3);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IDynamicFilterService.Stub.getDefaultImpl() != null) {
            IDynamicFilterService.Stub.getDefaultImpl().addToFilter(param2String1, param2String2, param2String3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeFromFilter(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.filter.IDynamicFilterService");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IDynamicFilterService.Stub.getDefaultImpl() != null) {
            IDynamicFilterService.Stub.getDefaultImpl().removeFromFilter(param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public String getFilterTagValue(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.filter.IDynamicFilterService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IDynamicFilterService.Stub.getDefaultImpl() != null) {
            param2String1 = IDynamicFilterService.Stub.getDefaultImpl().getFilterTagValue(param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDynamicFilterService param1IDynamicFilterService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDynamicFilterService != null) {
          Proxy.sDefaultImpl = param1IDynamicFilterService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDynamicFilterService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
