package com.oplus.filter;

import android.content.Context;
import android.os.RemoteException;
import android.util.Slog;

public class DynamicFilterManager {
  public static final String FILTER_BRK_SEARCH_2_WAY = "brk_s2w";
  
  public static final String FILTER_GL_OOM = "gl_oom";
  
  public static final String FILTER_UI_FIRST_BLACK = "ui_first_black";
  
  public static final String SERVICE_NAME = "dynamic_filter";
  
  private static final String TAG = "DynamicFilterManager";
  
  private Context mContext;
  
  private IDynamicFilterService mService;
  
  public DynamicFilterManager(Context paramContext, IDynamicFilterService paramIDynamicFilterService) {
    this.mContext = paramContext;
    this.mService = paramIDynamicFilterService;
    if (paramIDynamicFilterService == null) {
      Slog.e("DynamicFilterManager", "DynamicFilterService was null!");
      return;
    } 
  }
  
  public boolean hasFilter(String paramString) {
    try {
      return this.mService.hasFilter(paramString);
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean inFilter(String paramString1, String paramString2) {
    try {
      return this.mService.inFilter(paramString1, paramString2);
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public void addToFilter(String paramString1, String paramString2, String paramString3) {
    try {
      this.mService.addToFilter(paramString1, paramString2, paramString3);
    } catch (RemoteException remoteException) {}
  }
  
  public void removeFromFilter(String paramString1, String paramString2) {
    try {
      this.mService.removeFromFilter(paramString1, paramString2);
    } catch (RemoteException remoteException) {}
  }
  
  public String getFilterTagValue(String paramString1, String paramString2) {
    try {
      return this.mService.getFilterTagValue(paramString1, paramString2);
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
}
