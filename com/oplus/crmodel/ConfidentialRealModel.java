package com.oplus.crmodel;

import android.os.Build;
import android.os.SystemProperties;
import java.lang.reflect.Field;

public class ConfidentialRealModel {
  private static final String TAG = "CRmodel";
  
  private String mFactoryProductName = SystemProperties.get("ro.build.display.full_id");
  
  private String mMtkFactoryProductName = SystemProperties.get("ro.mediatek.version.release");
  
  public boolean ConfidentialRealModelOk(String paramString) {
    boolean bool1 = false;
    boolean bool = "true".equals(SystemProperties.get("persist.version.confidential"));
    boolean bool2 = bool1;
    if (bool) {
      bool2 = bool1;
      if (false)
        bool2 = true; 
    } 
    return bool2;
  }
  
  public void changeToRealModel() {
    String str1 = null;
    String str2 = this.mFactoryProductName;
    if (str2 != null && str2.length() != 0) {
      str2 = this.mFactoryProductName;
      str2 = str2.substring(0, str2.indexOf("_"));
    } else {
      String str = this.mMtkFactoryProductName;
      str2 = str1;
      if (str != null) {
        str2 = str1;
        if (str.length() != 0) {
          str2 = this.mMtkFactoryProductName;
          str2 = str2.substring(0, str2.indexOf("_"));
        } 
      } 
    } 
    if (str2 != null && str2.length() != 0)
      try {
        Field field = Build.class.getField("MODEL");
        field.setAccessible(true);
        field.set(Build.class, str2);
      } catch (NoSuchFieldException|IllegalAccessException noSuchFieldException) {
        noSuchFieldException.printStackTrace();
      }  
  }
}
