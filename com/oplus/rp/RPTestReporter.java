package com.oplus.rp;

import android.content.Context;
import android.util.Log;
import com.oplus.rp.test.ITestRunner;
import dalvik.system.PathClassLoader;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class RPTestReporter {
  private static final String sTag = "RPTestReporter";
  
  private final AtomicBoolean kLock = new AtomicBoolean(false);
  
  class IdentifierWrapperImpl implements IdentifierWrapper {
    private int mId;
    
    final RPTestReporter this$0;
    
    public IdentifierWrapperImpl(int param1Int) {
      this.mId = param1Int;
    }
    
    public boolean identify(Object param1Object) {
      return RPManager.get().identify(this.mId, param1Object);
    }
  }
  
  public void createTestReport(Context paramContext, boolean paramBoolean) {
    Object object = new Object(this, paramContext, paramBoolean);
    object.execute((Object[])new Void[0]);
  }
  
  private void createTestReportBrid(Context paramContext, boolean paramBoolean) {
    if (paramContext == null) {
      if (paramBoolean)
        Log.e("RPTestReporter", "Fatal error: no context supply"); 
      return;
    } 
    PathClassLoader pathClassLoader = new PathClassLoader("/sdcard/tmp-rp-test/Test.jar", paramContext.getClassLoader());
    int[] arrayOfInt = new int[3];
    arrayOfInt[0] = 2;
    arrayOfInt[1] = 3;
    arrayOfInt[2] = 4;
    ArrayList<IdentifierWrapperImpl> arrayList = new ArrayList();
    int i;
    byte b;
    for (i = arrayOfInt.length, b = 0; b < i; ) {
      int j = arrayOfInt[b];
      if (paramBoolean) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Creating detecotr(id: ");
        stringBuilder.append(j);
        stringBuilder.append(")...");
        Log.d("RPTestReporter", stringBuilder.toString());
      } 
      arrayList.add(new IdentifierWrapperImpl(j));
      b++;
    } 
    if (paramBoolean)
      Log.d("RPTestReporter", "Pass identifiers down to impl..."); 
    try {
      Class<ITestRunner> clazz = pathClassLoader.loadClass("com.oppo.rp.test.TestRunner");
      ITestRunner iTestRunner = clazz.newInstance();
      iTestRunner.runWithIdentifiers((ArrayList)arrayList);
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
}
