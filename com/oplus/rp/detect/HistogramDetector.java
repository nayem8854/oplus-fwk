package com.oplus.rp.detect;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.os.SystemProperties;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.oplus.rp.RPManager;
import java.util.ArrayList;

public final class HistogramDetector extends Detector {
  public static final double LMRATIO_DOWN = 0.28D;
  
  public static final double LMRATIO_UP = 0.45D;
  
  private static final String TAG = "HistogramDetector";
  
  public static final double WRATIO_DOWN = 0.5D;
  
  public static final int mId = 4;
  
  protected static final boolean mIsAsync = true;
  
  private static boolean mLibraryLoaded = false;
  
  public static final String mName = "Histogram Detector";
  
  private static String mSuccessPrompt = defaultSuccessPrompt("Histogram Detector");
  
  private final ArrayList<String> mRPLayoutList;
  
  private final ArrayList<Integer> mRPNodeNumberList;
  
  public HistogramDetector(ArrayList<Integer> paramArrayList, ArrayList<String> paramArrayList1) {
    checkOrLoadLibrary();
    this.mRPNodeNumberList = paramArrayList;
    this.mRPLayoutList = paramArrayList1;
  }
  
  public boolean isAsync() {
    return true;
  }
  
  public int getId() {
    return 4;
  }
  
  public String getName() {
    return "Histogram Detector";
  }
  
  public String getSuccessPrompt() {
    return mSuccessPrompt;
  }
  
  public void setSuccessPrompt(String paramString) {
    mSuccessPrompt = paramString;
  }
  
  private static boolean checkOrLoadLibrary() {
    if (mLibraryLoaded)
      return true; 
    RPManager.getRPManager().getHandler().post((Runnable)new Object());
    return mLibraryLoaded;
  }
  
  public void identifyAsyn(Object paramObject, DetectorCallback paramDetectorCallback) {
    if (paramObject instanceof View) {
      identifyAsyn((View)paramObject, paramDetectorCallback);
    } else {
      Log.w("HistogramDetector", "parameter o is not instance of View");
    } 
  }
  
  public void identifyAsyn(View paramView, DetectorCallback paramDetectorCallback) {
    if (SystemProperties.get("persist.oppo.debug.cnndisable", "0").equals("1")) {
      Log.d("HistogramDetector", "cnndisable is true");
    } else {
      Log.d("HistogramDetector", " preProcessLuckMoney start");
      paramView = preProcessLuckMoney(paramView, this.mRPNodeNumberList, this.mRPLayoutList);
      if (paramView != null) {
        Log.d("HistogramDetector", " loadBitmapFromView start");
        Bitmap bitmap = loadBitmapFromView(paramView);
        Log.d("HistogramDetector", " compareBitmapAsync start");
        RPManager.getRPManager().getHandler().post(new BitmapComparator(bitmap, this, paramDetectorCallback));
      } 
    } 
  }
  
  private static View preProcessLuckMoney(View paramView, ArrayList<Integer> paramArrayList, ArrayList<String> paramArrayList1) {
    if (paramArrayList.size() != paramArrayList1.size()) {
      Log.e("HistogramDetector", "Encounter bad configuration");
      return null;
    } 
    View view = preProcessLuckMoneyInternal(paramView, paramArrayList, paramArrayList1, 0);
    if (view == null || view.getHeight() == 0 || view.getWidth() == 0)
      return null; 
    float f1 = view.getWidth() / paramView.getWidth();
    float f2 = view.getHeight() / view.getWidth();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("preProcessLuckMoney wRatio:");
    stringBuilder.append(f1);
    stringBuilder.append(" lmRatio:");
    stringBuilder.append(f2);
    Log.d("HistogramDetector", stringBuilder.toString());
    if (0.28D < f2 && f2 < 0.45D && f1 > 0.5D)
      return view; 
    return null;
  }
  
  private static View preProcessLuckMoneyInternal(View paramView, ArrayList<Integer> paramArrayList, ArrayList<String> paramArrayList1, int paramInt) {
    int i = paramArrayList.size();
    int j = ((Integer)paramArrayList.get(paramInt)).intValue();
    String str = paramArrayList1.get(paramInt);
    paramInt++;
    if (paramView instanceof ViewGroup) {
      ViewGroup viewGroup = (ViewGroup)paramView;
      if (viewGroup.getChildCount() > j) {
        View view = viewGroup.getChildAt(j);
        if (view != null && view.getClass().getName().contains(str)) {
          if (paramInt < i)
            return preProcessLuckMoneyInternal(view, paramArrayList, paramArrayList1, paramInt); 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("LinearLayout width: ");
          stringBuilder.append(view.getWidth());
          stringBuilder.append(" height:");
          stringBuilder.append(view.getHeight());
          Log.w("HistogramDetector", stringBuilder.toString());
          return view;
        } 
      } 
    } else {
      Log.w("HistogramDetector", "preProcessLuckMoneyInternal not view group");
    } 
    return null;
  }
  
  private static Bitmap loadBitmapFromView(View paramView) {
    int i = paramView.getWidth();
    int j = paramView.getHeight();
    if (i == 0 || j == 0)
      return null; 
    Bitmap bitmap = Bitmap.createBitmap(i, j, Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);
    canvas.drawColor(-1);
    paramView.layout(0, 0, i, j);
    paramView.draw(canvas);
    return bitmap;
  }
  
  public static native float nativeDetect(Bitmap paramBitmap);
  
  private static native void nativeInit();
  
  class BitmapComparator implements Runnable {
    DetectorCallback mCallback;
    
    Detector mDetector;
    
    Bitmap mSrcBitmap;
    
    public BitmapComparator(HistogramDetector this$0, Detector param1Detector, DetectorCallback param1DetectorCallback) {
      this.mSrcBitmap = (Bitmap)this$0;
      this.mDetector = param1Detector;
      this.mCallback = param1DetectorCallback;
    }
    
    private Bitmap preProcessBitmap(Bitmap param1Bitmap) {
      Log.i("HistogramDetector", " start preProcessBitmap bitmap");
      Bitmap bitmap = scaleBitmap(param1Bitmap, 128, 128);
      if (!param1Bitmap.isRecycled())
        param1Bitmap.recycle(); 
      return bitmap;
    }
    
    private Bitmap scaleBitmap(Bitmap param1Bitmap, int param1Int1, int param1Int2) {
      int i = param1Bitmap.getWidth();
      int j = param1Bitmap.getHeight();
      Matrix matrix = new Matrix();
      float f1 = param1Int2 / j;
      float f2 = param1Int1 / i;
      matrix.preScale(f2, f1);
      Bitmap bitmap = Bitmap.createBitmap(param1Bitmap, 0, 0, i, j, matrix, false);
      if (!param1Bitmap.isRecycled())
        param1Bitmap.recycle(); 
      return bitmap;
    }
    
    public void run() {
      // Byte code:
      //   0: aload_0
      //   1: aload_0
      //   2: getfield mSrcBitmap : Landroid/graphics/Bitmap;
      //   5: invokespecial preProcessBitmap : (Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
      //   8: astore_1
      //   9: iconst_0
      //   10: istore_2
      //   11: ldc 'HistogramDetector'
      //   13: ldc 'start detect using CNN'
      //   15: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   18: pop
      //   19: invokestatic access$200 : ()Z
      //   22: ifeq -> 101
      //   25: ldc com/oplus/rp/detect/HistogramDetector
      //   27: monitorenter
      //   28: aload_1
      //   29: invokestatic nativeDetect : (Landroid/graphics/Bitmap;)F
      //   32: fstore_3
      //   33: ldc com/oplus/rp/detect/HistogramDetector
      //   35: monitorexit
      //   36: ldc 'HistogramDetector'
      //   38: ldc 'find LM using cnn'
      //   40: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   43: pop
      //   44: new java/lang/StringBuilder
      //   47: dup
      //   48: invokespecial <init> : ()V
      //   51: astore #4
      //   53: aload #4
      //   55: ldc 'compare CNN:'
      //   57: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   60: pop
      //   61: aload #4
      //   63: fload_3
      //   64: invokevirtual append : (F)Ljava/lang/StringBuilder;
      //   67: pop
      //   68: ldc 'HistogramDetector'
      //   70: aload #4
      //   72: invokevirtual toString : ()Ljava/lang/String;
      //   75: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   78: pop
      //   79: fload_3
      //   80: f2d
      //   81: ldc2_w 0.9
      //   84: dcmpl
      //   85: ifle -> 109
      //   88: iconst_1
      //   89: istore_2
      //   90: goto -> 109
      //   93: astore #4
      //   95: ldc com/oplus/rp/detect/HistogramDetector
      //   97: monitorexit
      //   98: aload #4
      //   100: athrow
      //   101: ldc 'HistogramDetector'
      //   103: ldc 'error whlie loading library'
      //   105: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   108: pop
      //   109: aload_1
      //   110: invokevirtual isRecycled : ()Z
      //   113: ifne -> 120
      //   116: aload_1
      //   117: invokevirtual recycle : ()V
      //   120: aload_0
      //   121: getfield mCallback : Lcom/oplus/rp/detect/DetectorCallback;
      //   124: aload_0
      //   125: getfield mDetector : Lcom/oplus/rp/detect/Detector;
      //   128: iload_2
      //   129: invokeinterface handle : (Lcom/oplus/rp/detect/Detector;Z)V
      //   134: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #198	-> 0
      //   #199	-> 9
      //   #200	-> 11
      //   #201	-> 19
      //   #202	-> 19
      //   #203	-> 25
      //   #204	-> 28
      //   #205	-> 33
      //   #206	-> 36
      //   #207	-> 44
      //   #208	-> 79
      //   #209	-> 88
      //   #205	-> 93
      //   #212	-> 101
      //   #214	-> 109
      //   #215	-> 116
      //   #217	-> 120
      //   #218	-> 134
      // Exception table:
      //   from	to	target	type
      //   28	33	93	finally
      //   33	36	93	finally
      //   95	98	93	finally
    }
  }
}
