package com.oplus.rp.detect;

public interface DetectorCallback {
  void handle(Detector paramDetector, boolean paramBoolean);
}
