package com.oplus.rp.detect;

import android.graphics.Bitmap;
import android.graphics.OplusBaseBitmap;
import android.util.Log;
import com.oplus.util.OplusTypeCastingHelper;
import java.util.ArrayList;

public final class HashDetector extends Detector {
  private static final String TAG = "HashDetector";
  
  private static final boolean mGetHash = false;
  
  public static final int mId = 2;
  
  protected static final boolean mIsAsync = false;
  
  public static final String mName = "Hash Detector";
  
  private static String mSuccessPrompt = defaultSuccessPrompt("Hash Detector");
  
  private final ArrayList<String> mRPHashList;
  
  private final ArrayList<Integer> mRPHeightList;
  
  private final ArrayList<Integer> mRPWidthList;
  
  public HashDetector(ArrayList<String> paramArrayList, ArrayList<Integer> paramArrayList1, ArrayList<Integer> paramArrayList2) {
    this.mRPHashList = paramArrayList;
    this.mRPWidthList = paramArrayList1;
    this.mRPHeightList = paramArrayList2;
  }
  
  public boolean isAsync() {
    return false;
  }
  
  public int getId() {
    return 2;
  }
  
  public String getName() {
    return "Hash Detector";
  }
  
  public String getSuccessPrompt() {
    return mSuccessPrompt;
  }
  
  public void setSuccessPrompt(String paramString) {
    mSuccessPrompt = paramString;
  }
  
  public boolean identify(Object paramObject) {
    if (paramObject instanceof Bitmap)
      return identify((Bitmap)paramObject); 
    Log.w("HashDetector", "parameter o is not instance of Bitmap");
    return false;
  }
  
  public boolean identify(Bitmap paramBitmap) {
    int i = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    int k = this.mRPWidthList.indexOf(Integer.valueOf(i));
    int m = this.mRPHeightList.indexOf(Integer.valueOf(j));
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("try to identify bimap");
    stringBuilder.append(paramBitmap);
    Log.i("HashDetector", stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("bitmap width ");
    stringBuilder.append(i);
    stringBuilder.append(" height ");
    stringBuilder.append(j);
    Log.i("HashDetector", stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("width, height index: (");
    stringBuilder.append(k);
    stringBuilder.append(", ");
    stringBuilder.append(m);
    stringBuilder.append(")");
    Log.i("HashDetector", stringBuilder.toString());
    if (k == m && k >= 0 && this.mRPHashList.size() > k) {
      byte[] arrayOfByte = getRPHash(this.mRPHashList.get(k));
      OplusBaseBitmap oplusBaseBitmap = OplusTypeCastingHelper.<OplusBaseBitmap>typeCasting(OplusBaseBitmap.class, paramBitmap);
      if (oplusBaseBitmap != null && arrayOfByte != null)
        return oplusBaseBitmap.checkLM(arrayOfByte, false, i, j); 
    } 
    return false;
  }
  
  private static byte[] getRPHash(String paramString) {
    if (paramString != null && paramString.length() == 32) {
      byte[] arrayOfByte = new byte[paramString.length() / 2];
      byte b = 0;
      try {
        for (; b < arrayOfByte.length; b++)
          arrayOfByte[b] = (byte)(Integer.parseInt(paramString.substring(b * 2, b * 2 + 2), 16) & 0xFF); 
        return arrayOfByte;
      } catch (NumberFormatException numberFormatException) {
        numberFormatException.printStackTrace();
      } 
    } 
    return null;
  }
}
