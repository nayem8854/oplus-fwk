package com.oplus.rp.detect;

import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;

public class DetectorManager {
  public static final int HASH_DETECTOR_ID = 2;
  
  public static final int HISTOGRAM_DETECTOR_ID = 4;
  
  public static final int LAYOUT_DETECTOR_ID = 3;
  
  private static final String TAG = "DetectorManager";
  
  private static DetectorCallback mAsynCallback;
  
  private static ArrayList<Class<Detector>> mDetectorTypes;
  
  private static HashMap<Integer, Detector> mDetectors;
  
  private static boolean mOwnable = true;
  
  static {
    mDetectorTypes = new ArrayList<>();
    mDetectors = new HashMap<>();
    mAsynCallback = null;
  }
  
  public static DetectorManager seizeDetectorManager() {
    // Byte code:
    //   0: ldc com/oplus/rp/detect/DetectorManager
    //   2: monitorenter
    //   3: getstatic com/oplus/rp/detect/DetectorManager.mOwnable : Z
    //   6: ifeq -> 26
    //   9: iconst_0
    //   10: putstatic com/oplus/rp/detect/DetectorManager.mOwnable : Z
    //   13: new com/oplus/rp/detect/DetectorManager
    //   16: dup
    //   17: invokespecial <init> : ()V
    //   20: astore_0
    //   21: ldc com/oplus/rp/detect/DetectorManager
    //   23: monitorexit
    //   24: aload_0
    //   25: areturn
    //   26: ldc com/oplus/rp/detect/DetectorManager
    //   28: monitorexit
    //   29: aconst_null
    //   30: areturn
    //   31: astore_0
    //   32: ldc com/oplus/rp/detect/DetectorManager
    //   34: monitorexit
    //   35: aload_0
    //   36: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #37	-> 3
    //   #38	-> 9
    //   #39	-> 13
    //   #41	-> 26
    //   #36	-> 31
    // Exception table:
    //   from	to	target	type
    //   3	9	31	finally
    //   9	13	31	finally
    //   13	21	31	finally
  }
  
  public void createHashDetector(ArrayList<String> paramArrayList, ArrayList<Integer> paramArrayList1, ArrayList<Integer> paramArrayList2) {
    HashDetector hashDetector = new HashDetector(paramArrayList, paramArrayList1, paramArrayList2);
    hashDetector.setSuccessPrompt("1 - Identified LM successful");
    mDetectors.put(Integer.valueOf(2), hashDetector);
  }
  
  public void createLayoutDetector(String paramString, ArrayList<Integer> paramArrayList, ArrayList<String> paramArrayList1) {
    LayoutDetector layoutDetector = new LayoutDetector(paramString, paramArrayList, paramArrayList1);
    layoutDetector.setSuccessPrompt("2 - Identified LM successful");
    mDetectors.put(Integer.valueOf(3), layoutDetector);
  }
  
  public void createHistogramDetector(DetectorCallback paramDetectorCallback, ArrayList<Integer> paramArrayList, ArrayList<String> paramArrayList1) {
    HistogramDetector histogramDetector = new HistogramDetector(paramArrayList, paramArrayList1);
    histogramDetector.setSuccessPrompt("3 - Identified LM successful");
    setAsynCallback(paramDetectorCallback);
    mDetectors.put(Integer.valueOf(4), histogramDetector);
  }
  
  private void setAsynCallback(DetectorCallback paramDetectorCallback) {
    mAsynCallback = new C(paramDetectorCallback);
  }
  
  public boolean identify(int paramInt, Object paramObject) {
    boolean bool1 = false, bool2 = false;
    Detector detector = mDetectors.get(Integer.valueOf(paramInt));
    if (detector != null) {
      System.currentTimeMillis();
      if (!detector.isAsync() && detector.identify(paramObject)) {
        bool1 = true;
        Log.i("DetectorManager", detector.getSuccessPrompt());
      } else {
        if (mAsynCallback == null)
          setAsynCallback(null); 
        detector.identifyAsyn(paramObject, mAsynCallback);
        bool1 = bool2;
      } 
      System.currentTimeMillis();
    } else {
      paramObject = new StringBuilder();
      paramObject.append("No corresponding detector for id ");
      paramObject.append(paramInt);
      Log.w("DetectorManager", paramObject.toString());
    } 
    return bool1;
  }
  
  class C implements DetectorCallback {
    DetectorCallback mNext;
    
    final DetectorManager this$0;
    
    public C(DetectorCallback param1DetectorCallback) {
      this.mNext = param1DetectorCallback;
    }
    
    public void handle(Detector param1Detector, boolean param1Boolean) {
      if (param1Boolean)
        Log.i("DetectorManager", param1Detector.getSuccessPrompt()); 
      DetectorCallback detectorCallback = this.mNext;
      if (detectorCallback != null)
        detectorCallback.handle(param1Detector, param1Boolean); 
    }
  }
}
