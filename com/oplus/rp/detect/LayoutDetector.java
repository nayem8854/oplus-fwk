package com.oplus.rp.detect;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;

public final class LayoutDetector extends Detector {
  private static final String TAG = "LayoutDetector";
  
  public static final int mId = 3;
  
  protected static final boolean mIsAsync = false;
  
  public static final String mName = "Layout Detector";
  
  private static String mSuccessPrompt = defaultSuccessPrompt("Layout Detector");
  
  private final ArrayList<String> mRPLayoutList;
  
  private final ArrayList<Integer> mRPNodeNumberList;
  
  private final String mRPText;
  
  public LayoutDetector(String paramString, ArrayList<Integer> paramArrayList, ArrayList<String> paramArrayList1) {
    this.mRPText = paramString;
    this.mRPNodeNumberList = paramArrayList;
    this.mRPLayoutList = paramArrayList1;
  }
  
  public boolean isAsync() {
    return false;
  }
  
  public int getId() {
    return 3;
  }
  
  public String getName() {
    return "Layout Detector";
  }
  
  public String getSuccessPrompt() {
    return mSuccessPrompt;
  }
  
  public void setSuccessPrompt(String paramString) {
    mSuccessPrompt = paramString;
  }
  
  public boolean identify(Object paramObject) {
    if (paramObject instanceof View)
      return identify((View)paramObject); 
    Log.w("LayoutDetector", "parameter o is not instance of Bitmap");
    return false;
  }
  
  public boolean identify(View paramView) {
    return findLuckyMoneyByText(paramView, this.mRPText, this.mRPNodeNumberList, this.mRPLayoutList);
  }
  
  private static boolean findLuckyMoneyByText(View paramView, String paramString, ArrayList<Integer> paramArrayList, ArrayList<String> paramArrayList1) {
    if (paramView == null || paramArrayList.isEmpty() || paramArrayList1.isEmpty())
      return false; 
    int i = ((Integer)paramArrayList.get(0)).intValue();
    String str = paramArrayList1.get(0);
    paramView = findLuckMoneyInternal(paramView, i, str, 0, paramArrayList, paramArrayList1);
    if (paramView != null && paramView instanceof TextView) {
      TextView textView = (TextView)paramView;
      if (paramString.equals(textView.getText().toString()))
        return true; 
    } 
    return false;
  }
  
  private static View findLuckMoneyInternal(View paramView, int paramInt1, String paramString, int paramInt2, ArrayList<Integer> paramArrayList, ArrayList<String> paramArrayList1) {
    int i = paramArrayList.size();
    paramInt2++;
    if (paramView instanceof ViewGroup) {
      ViewGroup viewGroup = (ViewGroup)paramView;
      if (viewGroup.getChildCount() > paramInt1) {
        View view = viewGroup.getChildAt(paramInt1);
        if (view != null && view.getClass().getName().contains(paramString)) {
          if (paramInt2 < i)
            return findLuckMoneyInternal(view, ((Integer)paramArrayList.get(paramInt2)).intValue(), paramArrayList1.get(paramInt2), paramInt2, paramArrayList, paramArrayList1); 
          return view;
        } 
      } 
    } else {
      Log.w("LayoutDetector", "findLMInternal not view group");
    } 
    return null;
  }
}
