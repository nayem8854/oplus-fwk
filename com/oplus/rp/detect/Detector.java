package com.oplus.rp.detect;

public abstract class Detector {
  public boolean identify(Object paramObject) {
    return false;
  }
  
  public void identifyAsyn(Object paramObject, DetectorCallback paramDetectorCallback) {
    paramDetectorCallback.handle(this, false);
  }
  
  protected static String defaultSuccessPrompt(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(" identified succeed");
    return stringBuilder.toString();
  }
  
  public abstract int getId();
  
  public abstract String getName();
  
  public abstract String getSuccessPrompt();
  
  public abstract boolean isAsync();
  
  public abstract void setSuccessPrompt(String paramString);
}
