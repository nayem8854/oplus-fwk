package com.oplus.rp;

import android.app.ActivityThread;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.Log;
import android.view.View;
import com.android.internal.app.ILMServiceManager;
import com.oplus.rp.detect.Detector;
import com.oplus.rp.detect.DetectorCallback;
import com.oplus.rp.detect.DetectorManager;
import java.util.ArrayList;
import java.util.HashMap;

public class RPManager {
  public static final String BROADCAST_ACTION_TEST_REPORT = "com.oppo.intent.action.rp.TEST_REPORT";
  
  public static final String BROADCAST_ACTION_UPDATE_lUCKYMONEYINFO = "com.oppo.intent.action.UPDATE_LUCKYMONEYINFO";
  
  private static final boolean DEBUG_RP;
  
  private static final boolean IS_RELEASE_VERSION;
  
  public static final String KEY_CURRENT_TIME = "CURRENT_TIME";
  
  public static final String KEY_DETECT_RETURN = "DETECT_RETURN";
  
  public static final String KEY_SPEND_TIME = "SPEND_TIME";
  
  public static final String KEY_TYPE = "TYPE";
  
  private static final String LUCKY_MONEY_SERVICE = "luckymoney";
  
  private static Handler LastNewMsgTimeoutHandler;
  
  private static final String MM_PACKAGENAME = "com.tencent.mm";
  
  public static final String MODE_2_HG_HASH = "hg_hash";
  
  private static final String QQ_PACKAGENAME = "com.tencent.mobileqq";
  
  private static final String TAG = "RPManager";
  
  public static final int TYPE_MODE_MM = 0;
  
  public static final int TYPE_MODE_NONE = -1;
  
  public static final int TYPE_MODE_QQ = 1;
  
  private static int mAppInfoVersionCode;
  
  private static float mCNNReturn;
  
  private static String mChatView;
  
  private static Context mContext;
  
  private static boolean mDetectorAllFailed;
  
  private static DetectorManager mDetectorManager;
  
  private static long mEndTime;
  
  private static Handler mHandler;
  
  private static HandlerThread mHandlerThread;
  
  private static boolean mInitialized;
  
  public static boolean mIsEnable;
  
  private static boolean mLastViewItem;
  
  public static boolean mModeEnable;
  
  public static HashMap<Integer, Boolean> mModeEnableInfo;
  
  private static boolean mNewMsgDetected;
  
  public static ArrayList<String> mOpenHbActivity = null;
  
  private static RPManager mRPManager;
  
  public static String mReceiverClassName;
  
  public static boolean mSMEnable;
  
  private static long mStartTime;
  
  private static boolean mTestReportReceiverRegistered;
  
  private static boolean mUpdateReceiverRegistered;
  
  public static int sBoostMode;
  
  private static boolean sDebugMode;
  
  static {
    mModeEnableInfo = null;
    mReceiverClassName = "";
    mIsEnable = true;
    mModeEnable = true;
    mSMEnable = true;
    sBoostMode = 0;
    IS_RELEASE_VERSION = SystemProperties.getBoolean("ro.build.release_type", false);
    DEBUG_RP = SystemProperties.getBoolean("persist.sys.assert.panic", false);
    mContext = null;
    mHandler = null;
    LastNewMsgTimeoutHandler = null;
    mHandlerThread = new HandlerThread("luckmoney");
    mRPManager = null;
    mDetectorManager = null;
    mChatView = "";
    mUpdateReceiverRegistered = false;
    mTestReportReceiverRegistered = false;
    mDetectorAllFailed = false;
    mInitialized = false;
    mLastViewItem = false;
    mNewMsgDetected = false;
    sDebugMode = false;
    mCNNReturn = 0.0F;
    mAppInfoVersionCode = 0;
    mEndTime = 0L;
    mStartTime = 0L;
  }
  
  public static RPManager get() {
    return getRPManager();
  }
  
  public static RPManager getRPManager() {
    // Byte code:
    //   0: ldc com/oplus/rp/RPManager
    //   2: monitorenter
    //   3: getstatic com/oplus/rp/RPManager.mRPManager : Lcom/oplus/rp/RPManager;
    //   6: ifnonnull -> 27
    //   9: new com/oplus/rp/RPManager
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic com/oplus/rp/RPManager.mRPManager : Lcom/oplus/rp/RPManager;
    //   21: getstatic com/oplus/rp/RPManager.mHandlerThread : Landroid/os/HandlerThread;
    //   24: invokevirtual start : ()V
    //   27: getstatic com/oplus/rp/RPManager.mRPManager : Lcom/oplus/rp/RPManager;
    //   30: astore_0
    //   31: ldc com/oplus/rp/RPManager
    //   33: monitorexit
    //   34: aload_0
    //   35: areturn
    //   36: astore_0
    //   37: ldc com/oplus/rp/RPManager
    //   39: monitorexit
    //   40: aload_0
    //   41: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #96	-> 3
    //   #97	-> 9
    //   #98	-> 21
    //   #100	-> 27
    //   #95	-> 36
    // Exception table:
    //   from	to	target	type
    //   3	9	36	finally
    //   9	21	36	finally
    //   21	27	36	finally
    //   27	31	36	finally
  }
  
  public static boolean isInjectingTarget(String paramString) {
    return "com.tencent.mm".equals(paramString);
  }
  
  public static boolean inject(Context paramContext) {
    // Byte code:
    //   0: ldc com/oplus/rp/RPManager
    //   2: monitorenter
    //   3: aload_0
    //   4: ifnull -> 42
    //   7: new java/lang/StringBuilder
    //   10: astore_1
    //   11: aload_1
    //   12: invokespecial <init> : ()V
    //   15: aload_1
    //   16: ldc_w 'Injecting package name '
    //   19: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   22: pop
    //   23: aload_1
    //   24: aload_0
    //   25: invokevirtual getPackageName : ()Ljava/lang/String;
    //   28: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   31: pop
    //   32: ldc 'RPManager'
    //   34: aload_1
    //   35: invokevirtual toString : ()Ljava/lang/String;
    //   38: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   41: pop
    //   42: getstatic com/oplus/rp/RPManager.mInitialized : Z
    //   45: ifne -> 56
    //   48: invokestatic getRPManager : ()Lcom/oplus/rp/RPManager;
    //   51: aload_0
    //   52: invokespecial init : (Landroid/content/Context;)Z
    //   55: pop
    //   56: getstatic com/oplus/rp/RPManager.mInitialized : Z
    //   59: istore_2
    //   60: ldc com/oplus/rp/RPManager
    //   62: monitorexit
    //   63: iload_2
    //   64: ireturn
    //   65: astore_0
    //   66: ldc com/oplus/rp/RPManager
    //   68: monitorexit
    //   69: aload_0
    //   70: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #108	-> 3
    //   #109	-> 7
    //   #111	-> 42
    //   #112	-> 48
    //   #114	-> 56
    //   #107	-> 65
    // Exception table:
    //   from	to	target	type
    //   7	42	65	finally
    //   42	48	65	finally
    //   48	56	65	finally
    //   56	60	65	finally
  }
  
  private boolean init(Context paramContext) {
    Log.i("RPManager", "try to init RPManager");
    if (!Client.checkServiceFullyOnline())
      return false; 
    Client.requestDebugMode();
    Client.requestSwitchInfo();
    if (!mIsEnable)
      return false; 
    if ("com.tencent.mm".equals(ActivityThread.currentPackageName())) {
      ApplicationInfo applicationInfo = ActivityThread.currentApplication().getApplicationInfo();
      if (applicationInfo != null) {
        mAppInfoVersionCode = applicationInfo.versionCode;
        if (mSMEnable) {
          SharedPreferences sharedPreferences = ActivityThread.currentApplication().getSharedPreferences("__RP_SP", 0);
          int i = sharedPreferences.getInt("VERSION_CODE", 0);
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("SP version ");
          stringBuilder.append(i);
          stringBuilder.append(", app versionCode ");
          stringBuilder.append(mAppInfoVersionCode);
          Log.d("RPManager", stringBuilder.toString());
          if (i != 0 && i != mAppInfoVersionCode)
            SP.removePreference(); 
        } else {
          SP.removePreference();
        } 
        mDetectorManager = DetectorManager.seizeDetectorManager();
        registerBroadcastReceiver(paramContext, 0, mAppInfoVersionCode);
        Client.requestModeData(0, mAppInfoVersionCode);
        Client.requestModeEnableInfo(0, mAppInfoVersionCode);
        mInitialized = true;
        return true;
      } 
      Log.e("RPManager", "Fatal error due to null app info");
    } 
    return false;
  }
  
  private void registerBroadcastReceiver(Context paramContext, int paramInt1, int paramInt2) {
    if (paramContext != null) {
      if (!mUpdateReceiverRegistered) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.oppo.intent.action.UPDATE_LUCKYMONEYINFO");
        paramContext.registerReceiver((BroadcastReceiver)new Object(this, paramInt1, paramInt2), intentFilter);
        mUpdateReceiverRegistered = true;
      } 
      if (!IS_RELEASE_VERSION && sDebugMode && !mTestReportReceiverRegistered) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.oppo.intent.action.rp.TEST_REPORT");
        paramContext.registerReceiver((BroadcastReceiver)new Object(this), intentFilter);
        mTestReportReceiverRegistered = true;
      } 
    } 
  }
  
  private void unpackModeData(Bundle paramBundle) {
    if (paramBundle != null) {
      int i = paramBundle.getInt("xmlVersion", 0);
      mIsEnable = paramBundle.getBoolean("isEnable", mIsEnable);
      mSMEnable = paramBundle.getBoolean("smEnable", mSMEnable);
      if (i == 1) {
        Bundle bundle1 = (Bundle)paramBundle.getParcelable(String.valueOf(2));
        if (bundle1 != null) {
          mModeEnable = bundle1.getBoolean("isModeEnable", true);
          ArrayList<String> arrayList = bundle1.getStringArrayList("hbHashs");
          ArrayList<Integer> arrayList2 = bundle1.getIntegerArrayList("hbWidths");
          ArrayList<Integer> arrayList1 = bundle1.getIntegerArrayList("hbHeights");
          mDetectorManager.createHashDetector(arrayList, arrayList2, arrayList1);
        } 
        bundle1 = (Bundle)paramBundle.getParcelable(String.valueOf(3));
        if (bundle1 != null) {
          mModeEnable = bundle1.getBoolean("isModeEnable", true);
          String str = bundle1.getString("hbText", "");
          ArrayList<String> arrayList1 = bundle1.getStringArrayList("hbLayout");
          ArrayList<Integer> arrayList = bundle1.getIntegerArrayList("hbLayoutNodes");
          mDetectorManager.createLayoutDetector(str, arrayList, arrayList1);
        } 
        Bundle bundle2 = (Bundle)paramBundle.getParcelable(String.valueOf(4));
        if (bundle2 != null) {
          mModeEnable = bundle2.getBoolean("isModeEnable", true);
          ArrayList<String> arrayList1 = bundle2.getStringArrayList("hbLayout");
          ArrayList<Integer> arrayList = bundle2.getIntegerArrayList("hbLayoutNodes");
          mDetectorManager.createHistogramDetector(new RPAsynCallback(), arrayList, arrayList1);
        } 
        sBoostMode = paramBundle.getInt("defaultMode", 0);
        SharedPreferences sharedPreferences = ActivityThread.currentApplication().getSharedPreferences("__RP_SP", 0);
        i = DetectorSwitcher.getFirstAvailableDetectorId(sharedPreferences);
        if (!DetectorSwitcher.isDetectorAvailable(sharedPreferences, sBoostMode)) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("change sboostmode to ");
          stringBuilder.append(i);
          Log.e("RPManager", stringBuilder.toString());
          sBoostMode = i;
        } 
        mChatView = paramBundle.getString("chatView", "");
        mReceiverClassName = paramBundle.getString("receiver_class", "");
        mOpenHbActivity = paramBundle.getStringArrayList("openHbActivity");
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unable to handle xml version ");
        stringBuilder.append(i);
        Log.e("RPManager", stringBuilder.toString());
      } 
    } else {
      Log.e("RPManager", "Got null mode data from service");
    } 
  }
  
  private void updateRedpacketInfo(int paramInt1, int paramInt2) {
    Client.requestModeData(paramInt1, paramInt2);
  }
  
  private static void setLastViewItem(boolean paramBoolean) {
    mLastViewItem = paramBoolean;
  }
  
  public Handler getHandler() {
    if (mHandler == null)
      mHandler = new Handler(mHandlerThread.getLooper()); 
    return mHandler;
  }
  
  public static boolean notifyBeforeEnter(String paramString) {
    boolean bool2, bool1 = false;
    if (mIsEnable && mModeEnable) {
      bool2 = bool1;
      if ("com.tencent.mm".equals(paramString)) {
        bool1 = true;
        bool2 = bool1;
        if (getDetectorId() == 2) {
          bool2 = bool1;
          if (!mLastViewItem) {
            setLastViewItem(true);
            bool2 = bool1;
          } 
        } 
      } 
    } else {
      bool2 = bool1;
      if (DEBUG_RP) {
        Log.w("RPManager", "disable luckmoney detect");
        bool2 = bool1;
      } 
    } 
    return bool2;
  }
  
  public static boolean verifyBeforeEnter() {
    boolean bool;
    if (mNewMsgDetected && mLastViewItem) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isRPActivities(String paramString) {
    if (paramString != null) {
      ArrayList<String> arrayList = mOpenHbActivity;
      if (arrayList != null && arrayList.contains(paramString))
        return true; 
    } 
    return false;
  }
  
  private static int getDetectorId() {
    return SystemProperties.getInt("persist.oppo.debug.luckymoney", sBoostMode);
  }
  
  private static void setNewMsgDetected(Handler paramHandler, boolean paramBoolean) {
    Runnable runnable = new Runnable() {
        public void run() {
          RPManager.access$302(false);
        }
      };
    mNewMsgDetected = paramBoolean;
    if (paramHandler != null) {
      paramHandler.removeCallbacks(runnable);
      LastNewMsgTimeoutHandler = paramHandler;
      if (paramBoolean)
        paramHandler.postDelayed(runnable, 1000L); 
    } else {
      paramHandler = LastNewMsgTimeoutHandler;
      if (paramHandler != null)
        paramHandler.removeCallbacks(runnable); 
    } 
  }
  
  public void notify(Object paramObject) {
    notify(paramObject, true);
  }
  
  public void notify(Object paramObject, boolean paramBoolean) {
    if (!mIsEnable || !mModeEnable) {
      Log.w("RPManager", "disable luckmoney detect");
      return;
    } 
    if (paramObject == null || !mNewMsgDetected)
      return; 
    if (paramBoolean && mLastViewItem)
      setLastViewItem(false); 
    if (paramBoolean) {
      View view = (View)paramObject;
      if (mChatView.isEmpty() || !view.getClass().getName().contains(mChatView))
        return; 
    } else {
      Bitmap bitmap = (Bitmap)paramObject;
    } 
    paramBoolean = mDetectorManager.identify(getDetectorId(), paramObject);
    if (paramBoolean) {
      int i = getDetectorId();
      if (i != 2) {
        if (i == 3) {
          paramObject = new StringBuilder();
          paramObject.append("Identify using detector ");
          paramObject.append(getDetectorId());
          paramObject.append(" return ");
          paramObject.append(paramBoolean);
          paramObject.append(", find LM successful_3");
          Log.d("RPManager", paramObject.toString());
          Client.requestBoost(0, 2018);
          setNewMsgDetected(null, false);
        } 
      } else {
        paramObject = new StringBuilder();
        paramObject.append("Identify using detector ");
        paramObject.append(getDetectorId());
        paramObject.append(" return ");
        paramObject.append(paramBoolean);
        paramObject.append(", find LM successful_2");
        Log.d("RPManager", paramObject.toString());
        Client.requestBoost(0, 2015);
        setNewMsgDetected(null, false);
      } 
    } else {
      paramObject = new StringBuilder();
      paramObject.append("Identify using detector ");
      paramObject.append(getDetectorId());
      paramObject.append(" return ");
      paramObject.append(paramBoolean);
      Log.i("RPManager", paramObject.toString());
    } 
  }
  
  protected boolean identify(int paramInt, Object paramObject) {
    return mDetectorManager.identify(paramInt, paramObject);
  }
  
  public static void checkReceiver(String paramString) {
    if (mReceiverClassName.equals(paramString))
      setNewMsgDetected(getRPManager().getHandler(), true); 
  }
  
  public void handleRPOpened() {
    if (!mIsEnable || !mSMEnable) {
      Log.w("RPManager", "disable luckmoney change mode");
      return;
    } 
    Client.requestWriteDCS(2020);
    if (!mDetectorAllFailed && ActivityThread.currentApplication() != null && mNewMsgDetected)
      getHandler().post(new DetectorSwitcher()); 
  }
  
  public void enableBoost(int paramInt1, int paramInt2) {
    Client.requestBoost(paramInt1, paramInt2);
  }
  
  private static final class SP {
    public static final String AVAILABLE_DETECTOR_IDS = "AVAILABLE_DETECTOR_IDS";
    
    public static final String BOOSTED = "BOOSTED";
    
    public static final String DETECTOR_ALL_FAILED = "DETECTOR_ALL_FAILED";
    
    public static final String FILE_NAME = "__RP_SP";
    
    public static final String INVALID_COUNT = "INVALID_COUNT";
    
    public static final String VERSION_CODE = "VERSION_CODE";
    
    public static void recordBoost() {
      Object object = new Object();
      object.execute((Object[])new Void[0]);
    }
    
    public static void removePreference() {
      Object object = new Object();
      object.execute((Object[])new Void[0]);
    }
  }
  
  private static final class DetectorSwitcher implements Runnable {
    private DetectorSwitcher() {}
    
    private static boolean mDetectorSwitchNeeded = false;
    
    private static int getFirstAvailableDetectorId(SharedPreferences param1SharedPreferences) {
      int i = param1SharedPreferences.getInt("AVAILABLE_DETECTOR_IDS", 0);
      if (i >= 7) {
        Log.e("RPManager", "getFirstAvailableDetectorId, All detectors failed");
        return 0;
      } 
      byte b = 2;
      while ((i & 0x1) != 0) {
        i >>= 1;
        b++;
      } 
      return b;
    }
    
    private static boolean isDetectorAvailable(SharedPreferences param1SharedPreferences, int param1Int) {
      int i = param1SharedPreferences.getInt("AVAILABLE_DETECTOR_IDS", 0);
      if (param1Int > 1) {
        int j = i >> param1Int - 2;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("isDetectorAvailable, availIds = ");
        stringBuilder.append(i);
        stringBuilder.append(" id = ");
        stringBuilder.append(param1Int);
        stringBuilder.append("tempAvailIds = ");
        stringBuilder.append(j);
        Log.d("RPManager", stringBuilder.toString());
        if ((j & 0x1) == 1)
          return false; 
        return true;
      } 
      return false;
    }
    
    private static void setDetectorToOffline(SharedPreferences param1SharedPreferences, int param1Int) {
      int i = param1SharedPreferences.getInt("AVAILABLE_DETECTOR_IDS", 0);
      if (param1Int > 0) {
        i |= 1 << param1Int - 2;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("setDetectorToOffline, availIds = ");
        stringBuilder.append(i);
        stringBuilder.append(" id = ");
        stringBuilder.append(param1Int);
        Log.d("RPManager", stringBuilder.toString());
        param1SharedPreferences.edit().putInt("AVAILABLE_DETECTOR_IDS", i).apply();
      } 
    }
    
    public void run() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: invokestatic currentApplication : ()Landroid/app/Application;
      //   5: ldc '__RP_SP'
      //   7: iconst_0
      //   8: invokevirtual getSharedPreferences : (Ljava/lang/String;I)Landroid/content/SharedPreferences;
      //   11: astore_1
      //   12: aload_1
      //   13: invokeinterface edit : ()Landroid/content/SharedPreferences$Editor;
      //   18: astore_2
      //   19: aload_1
      //   20: ldc 'DETECTOR_ALL_FAILED'
      //   22: iconst_0
      //   23: invokeinterface getBoolean : (Ljava/lang/String;Z)Z
      //   28: ifeq -> 47
      //   31: iconst_1
      //   32: invokestatic access$602 : (Z)Z
      //   35: pop
      //   36: ldc 'RPManager'
      //   38: ldc 'All detectors failed'
      //   40: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   43: pop
      //   44: aload_0
      //   45: monitorexit
      //   46: return
      //   47: getstatic com/oplus/rp/RPManager$DetectorSwitcher.mDetectorSwitchNeeded : Z
      //   50: ifne -> 104
      //   53: aload_1
      //   54: ldc 'INVALID_COUNT'
      //   56: iconst_0
      //   57: invokeinterface getInt : (Ljava/lang/String;I)I
      //   62: istore_3
      //   63: getstatic com/oplus/rp/RPManager.mModeEnable : Z
      //   66: ifeq -> 89
      //   69: iload_3
      //   70: iconst_3
      //   71: if_icmpge -> 89
      //   74: aload_2
      //   75: ldc 'INVALID_COUNT'
      //   77: iload_3
      //   78: iconst_1
      //   79: iadd
      //   80: invokeinterface putInt : (Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;
      //   85: pop
      //   86: goto -> 93
      //   89: iconst_1
      //   90: putstatic com/oplus/rp/RPManager$DetectorSwitcher.mDetectorSwitchNeeded : Z
      //   93: ldc 'RPManager'
      //   95: ldc 'Current detector is failed, switch to next detector'
      //   97: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   100: pop
      //   101: goto -> 331
      //   104: aload_1
      //   105: invokestatic access$700 : ()I
      //   108: invokestatic setDetectorToOffline : (Landroid/content/SharedPreferences;I)V
      //   111: aload_1
      //   112: invokestatic getFirstAvailableDetectorId : (Landroid/content/SharedPreferences;)I
      //   115: istore_3
      //   116: getstatic com/oplus/rp/RPManager.mModeEnableInfo : Ljava/util/HashMap;
      //   119: ifnull -> 203
      //   122: iload_3
      //   123: istore #4
      //   125: iload_3
      //   126: ifeq -> 214
      //   129: getstatic com/oplus/rp/RPManager.mModeEnableInfo : Ljava/util/HashMap;
      //   132: iload_3
      //   133: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   136: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   139: checkcast java/lang/Boolean
      //   142: invokevirtual booleanValue : ()Z
      //   145: ifeq -> 154
      //   148: iload_3
      //   149: istore #4
      //   151: goto -> 214
      //   154: new java/lang/StringBuilder
      //   157: astore #5
      //   159: aload #5
      //   161: invokespecial <init> : ()V
      //   164: aload #5
      //   166: ldc 'Skip over detector '
      //   168: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   171: pop
      //   172: aload #5
      //   174: iload_3
      //   175: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   178: pop
      //   179: ldc 'RPManager'
      //   181: aload #5
      //   183: invokevirtual toString : ()Ljava/lang/String;
      //   186: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   189: pop
      //   190: aload_1
      //   191: iload_3
      //   192: invokestatic setDetectorToOffline : (Landroid/content/SharedPreferences;I)V
      //   195: aload_1
      //   196: invokestatic getFirstAvailableDetectorId : (Landroid/content/SharedPreferences;)I
      //   199: istore_3
      //   200: goto -> 122
      //   203: ldc 'RPManager'
      //   205: ldc 'Unable to skip disabled modes, got null mode enabled info'
      //   207: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   210: pop
      //   211: iload_3
      //   212: istore #4
      //   214: new java/lang/StringBuilder
      //   217: astore #5
      //   219: aload #5
      //   221: invokespecial <init> : ()V
      //   224: aload #5
      //   226: ldc 'First availiable detector id '
      //   228: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   231: pop
      //   232: aload #5
      //   234: iload #4
      //   236: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   239: pop
      //   240: ldc 'RPManager'
      //   242: aload #5
      //   244: invokevirtual toString : ()Ljava/lang/String;
      //   247: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   250: pop
      //   251: iload #4
      //   253: ifne -> 269
      //   256: aload_2
      //   257: ldc 'DETECTOR_ALL_FAILED'
      //   259: iconst_1
      //   260: invokeinterface putBoolean : (Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;
      //   265: pop
      //   266: goto -> 317
      //   269: new java/lang/StringBuilder
      //   272: astore #5
      //   274: aload #5
      //   276: invokespecial <init> : ()V
      //   279: aload #5
      //   281: ldc 'Switch to detector '
      //   283: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   286: pop
      //   287: aload #5
      //   289: iload #4
      //   291: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   294: pop
      //   295: ldc 'RPManager'
      //   297: aload #5
      //   299: invokevirtual toString : ()Ljava/lang/String;
      //   302: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   305: pop
      //   306: iload #4
      //   308: putstatic com/oplus/rp/RPManager.sBoostMode : I
      //   311: sipush #2021
      //   314: invokestatic requestWriteDCS : (I)V
      //   317: iconst_0
      //   318: putstatic com/oplus/rp/RPManager$DetectorSwitcher.mDetectorSwitchNeeded : Z
      //   321: aload_2
      //   322: ldc 'INVALID_COUNT'
      //   324: iconst_0
      //   325: invokeinterface putInt : (Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;
      //   330: pop
      //   331: aload_2
      //   332: ldc 'VERSION_CODE'
      //   334: invokestatic access$500 : ()I
      //   337: invokeinterface putInt : (Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;
      //   342: pop
      //   343: aload_2
      //   344: invokeinterface commit : ()Z
      //   349: pop
      //   350: aload_0
      //   351: monitorexit
      //   352: return
      //   353: astore_2
      //   354: aload_0
      //   355: monitorexit
      //   356: aload_2
      //   357: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #450	-> 2
      //   #451	-> 12
      //   #452	-> 19
      //   #453	-> 31
      //   #454	-> 36
      //   #455	-> 44
      //   #457	-> 47
      //   #458	-> 53
      //   #459	-> 63
      //   #460	-> 74
      //   #461	-> 74
      //   #463	-> 89
      //   #465	-> 93
      //   #466	-> 101
      //   #467	-> 104
      //   #468	-> 111
      //   #469	-> 116
      //   #470	-> 122
      //   #471	-> 129
      //   #472	-> 148
      //   #474	-> 154
      //   #475	-> 190
      //   #476	-> 195
      //   #479	-> 203
      //   #481	-> 214
      //   #482	-> 251
      //   #483	-> 256
      //   #485	-> 269
      //   #486	-> 306
      //   #487	-> 311
      //   #489	-> 317
      //   #490	-> 321
      //   #492	-> 331
      //   #493	-> 343
      //   #494	-> 350
      //   #449	-> 353
      // Exception table:
      //   from	to	target	type
      //   2	12	353	finally
      //   12	19	353	finally
      //   19	31	353	finally
      //   31	36	353	finally
      //   36	44	353	finally
      //   47	53	353	finally
      //   53	63	353	finally
      //   63	69	353	finally
      //   74	86	353	finally
      //   89	93	353	finally
      //   93	101	353	finally
      //   104	111	353	finally
      //   111	116	353	finally
      //   116	122	353	finally
      //   129	148	353	finally
      //   154	190	353	finally
      //   190	195	353	finally
      //   195	200	353	finally
      //   203	211	353	finally
      //   214	251	353	finally
      //   256	266	353	finally
      //   269	306	353	finally
      //   306	311	353	finally
      //   311	317	353	finally
      //   317	321	353	finally
      //   321	331	353	finally
      //   331	343	353	finally
      //   343	350	353	finally
    }
  }
  
  class RPAsynCallback implements DetectorCallback {
    final RPManager this$0;
    
    public void handle(Detector param1Detector, boolean param1Boolean) {
      if (param1Boolean) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Identify using detector ");
        stringBuilder.append(RPManager.getDetectorId());
        stringBuilder.append(" return ");
        stringBuilder.append(param1Boolean);
        stringBuilder.append(", find LM successful_4");
        Log.d("RPManager", stringBuilder.toString());
        RPManager.Client.requestBoost(0, 2019);
        RPManager.setNewMsgDetected(null, false);
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Identify using RPAsynCallback detector ");
        stringBuilder.append(RPManager.getDetectorId());
        stringBuilder.append(" return ");
        stringBuilder.append(param1Boolean);
        Log.i("RPManager", stringBuilder.toString());
      } 
    }
  }
  
  private static final class Client {
    public static final String DETECT_CNN = "DETECT_CNN";
    
    public static final String DETECT_IMAGEVIEW = "DETECT_IMAGEVIEW";
    
    public static final String DETECT_NETWORK = "DETECT_NETWORK";
    
    public static final String DETECT_TEXT = "DETECT_TEXT";
    
    public static final String OPEN_LUCKYMONEY = "OPEN_LUCKYMONEY";
    
    public static final String SWITCH_MODE = "SWITCH_MODE";
    
    private static final Object mMobileBoostLock;
    
    private static boolean mServiceInitialized = false;
    
    private static volatile boolean mShouldMobileBoost = true;
    
    private static ILMServiceManager sService = null;
    
    static {
      mMobileBoostLock = new Object();
    }
    
    private static boolean init() {
      if (sService == null) {
        IBinder iBinder = ServiceManager.getService("luckymoney");
        ILMServiceManager iLMServiceManager = ILMServiceManager.Stub.asInterface(iBinder);
        if (iLMServiceManager == null) {
          Log.e("RPManager", "Service not exist for name luckymoney");
          return false;
        } 
      } 
      return true;
    }
    
    public static boolean checkServiceFullyOnline() {
      if (!init())
        return false; 
      if (!mServiceInitialized)
        try {
          boolean bool = sService.isInitialized();
          return bool;
        } catch (RemoteException remoteException) {
          remoteException.printStackTrace();
        }  
      return true;
    }
    
    private static boolean checkServiceStatus(String param1String) {
      boolean bool = checkServiceFullyOnline();
      if (!bool) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Service not ready when try to request ");
        stringBuilder.append(param1String);
        Log.e("RPManager", stringBuilder.toString());
      } 
      return bool;
    }
    
    public static void requestDebugMode() {
      if (checkServiceStatus("DebugMode"))
        try {
          RPManager.access$902(sService.inDebugMode());
        } catch (RemoteException remoteException) {
          remoteException.printStackTrace();
        }  
    }
    
    public static void requestSwitchInfo() {
      if (checkServiceStatus("SwitchInfo"))
        try {
          Bundle bundle = sService.getSwitchInfo();
          RPManager.mIsEnable = bundle.getBoolean("isEnable", true);
          RPManager.mSMEnable = bundle.getBoolean("smEnable", true);
        } catch (RemoteException remoteException) {
          remoteException.printStackTrace();
        }  
    }
    
    public static HashMap<Integer, Boolean> requestModeEnableInfo(int param1Int1, int param1Int2) {
      if (checkServiceStatus("ModeEnableInfo"))
        try {
          RPManager.mModeEnableInfo = (HashMap<Integer, Boolean>)sService.getModeEnableInfo(param1Int1, param1Int2).getSerializable("modeEnableInfo");
          if (RPManager.mModeEnableInfo == null)
            Log.d("RPManager", "got null modeEnableInfo"); 
        } catch (RemoteException remoteException) {
          remoteException.printStackTrace();
        }  
      return RPManager.mModeEnableInfo;
    }
    
    public static void requestBoost(int param1Int1, int param1Int2) {
      if (!RPManager.mIsEnable) {
        Log.d("RPManager", "enableBoost is disable");
        return;
      } 
      if (checkServiceStatus("Boost"))
        try {
          if (RPManager.mSMEnable)
            RPManager.SP.recordBoost(); 
          ILMServiceManager iLMServiceManager = sService;
          int i = Process.myPid();
          int j = Process.myUid();
          boolean bool = iLMServiceManager.enableBoost(i, j, param1Int1, param1Int2);
          requestWriteDCS(param1Int2);
          if (bool && mShouldMobileBoost)
            synchronized (mMobileBoostLock) {
              mShouldMobileBoost = false;
              sService.enableMobileBoost();
              null = RPManager.getRPManager().getHandler();
              Runnable runnable = new Runnable() {
                  public void run() {
                    synchronized (RPManager.Client.mMobileBoostLock) {
                      RPManager.Client.access$1102(true);
                      Log.e("RPManager", "Reset ShouldMobileBoost to true");
                      return;
                    } 
                  }
                };
              super();
              null.postDelayed(runnable, 500L);
            }  
        } catch (RemoteException remoteException) {
          remoteException.printStackTrace();
        }  
    }
    
    public static void requestModeData(int param1Int1, int param1Int2) {
      if (checkServiceStatus("ModeData"))
        try {
          Bundle bundle = sService.getModeData(param1Int1, param1Int2, -1);
          RPManager.getRPManager().unpackModeData(bundle);
        } catch (RemoteException remoteException) {
          remoteException.printStackTrace();
        }  
    }
    
    public static String requestLuckyMoneyInfo(int param1Int) {
      if (checkServiceStatus("LuckyMoneyInfo"))
        try {
          return sService.getLuckyMoneyInfo(param1Int);
        } catch (RemoteException remoteException) {
          remoteException.printStackTrace();
        }  
      return null;
    }
    
    public static void requestWriteDCS(int param1Int) {
      if (checkServiceStatus("WriteDCS")) {
        Bundle bundle = new Bundle();
        bundle.putString("TYPE", getenableBoostType(param1Int));
        bundle.putString("DETECT_RETURN", String.valueOf(RPManager.mCNNReturn));
        if (param1Int == 2020 || param1Int == 2021) {
          bundle.putString("SPEND_TIME", "0");
        } else {
          bundle.putString("SPEND_TIME", String.valueOf(RPManager.mEndTime - RPManager.mStartTime));
        } 
        bundle.putString("CURRENT_TIME", String.valueOf(System.currentTimeMillis()));
        try {
          sService.writeDCS(bundle);
        } catch (RemoteException remoteException) {
          remoteException.printStackTrace();
        } 
      } 
    }
    
    private static String getenableBoostType(int param1Int) {
      String str = "";
      switch (param1Int) {
        default:
          return str;
        case 2021:
          str = "SWITCH_MODE";
        case 2020:
          str = "OPEN_LUCKYMONEY";
        case 2019:
          str = "DETECT_CNN";
        case 2018:
          str = "DETECT_TEXT";
        case 2017:
          str = "DETECT_IMAGEVIEW";
        case 2015:
          break;
      } 
      str = "DETECT_NETWORK";
    }
  }
  
  class null implements Runnable {
    public void run() {
      synchronized (RPManager.Client.mMobileBoostLock) {
        RPManager.Client.access$1102(true);
        Log.e("RPManager", "Reset ShouldMobileBoost to true");
        return;
      } 
    }
  }
}
