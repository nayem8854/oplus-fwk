package com.oplus.rp.bridge;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.content.Context;

public interface IOplusRedPacketManager extends IOplusCommonFeature {
  public static final IOplusRedPacketManager DEFAULT = (IOplusRedPacketManager)new Object();
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusRedPacketManager;
  }
  
  default IOplusCommonFeature getDefault() {
    return DEFAULT;
  }
  
  default boolean isInjectingTarget(String paramString) {
    return false;
  }
  
  default boolean inject(Context paramContext) {
    return false;
  }
  
  default boolean notifyBeforeEnter(String paramString) {
    return false;
  }
  
  default boolean verifyBeforeEnter() {
    return false;
  }
  
  default boolean isRPActivities(String paramString) {
    return false;
  }
  
  default void notify(Object paramObject) {}
  
  default void notify(Object paramObject, boolean paramBoolean) {}
  
  default void checkReceiver(String paramString) {}
  
  default void handleRPOpened() {}
  
  default void enableBoost(int paramInt1, int paramInt2) {}
}
