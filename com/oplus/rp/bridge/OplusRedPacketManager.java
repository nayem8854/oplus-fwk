package com.oplus.rp.bridge;

import android.content.Context;
import com.oplus.rp.RPManager;

public class OplusRedPacketManager implements IOplusRedPacketManager {
  public static IOplusRedPacketManager getInstance() {
    return new OplusRedPacketManager();
  }
  
  public boolean isInjectingTarget(String paramString) {
    return RPManager.isInjectingTarget(paramString);
  }
  
  public boolean inject(Context paramContext) {
    return RPManager.inject(paramContext);
  }
  
  public boolean notifyBeforeEnter(String paramString) {
    return RPManager.notifyBeforeEnter(paramString);
  }
  
  public boolean verifyBeforeEnter() {
    return RPManager.verifyBeforeEnter();
  }
  
  public boolean isRPActivities(String paramString) {
    return RPManager.isRPActivities(paramString);
  }
  
  public void checkReceiver(String paramString) {
    RPManager.checkReceiver(paramString);
  }
  
  public void notify(Object paramObject) {
    RPManager.get().notify(paramObject);
  }
  
  public void notify(Object paramObject, boolean paramBoolean) {
    RPManager.get().notify(paramObject, paramBoolean);
  }
  
  public void handleRPOpened() {
    RPManager.get().handleRPOpened();
  }
  
  public void enableBoost(int paramInt1, int paramInt2) {
    RPManager.get().enableBoost(paramInt1, paramInt2);
  }
}
