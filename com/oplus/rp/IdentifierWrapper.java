package com.oplus.rp;

public interface IdentifierWrapper {
  boolean identify(Object paramObject);
}
