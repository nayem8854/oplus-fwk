package com.oplus.rp.test;

import com.oplus.rp.IdentifierWrapper;
import java.util.ArrayList;

public interface ITestRunner {
  void runWithIdentifiers(ArrayList<? extends IdentifierWrapper> paramArrayList);
}
