package com.oplus.felica;

import android.content.Context;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import android.util.Slog;

public class FelicadeviceManager {
  private IFelicadeviceService mService = null;
  
  private IBinder mToken = (IBinder)new Binder();
  
  private static FelicadeviceManager sFelicadeviceManager = null;
  
  public static final String OPLUS_DEFAULT_MODEL = "OPPO-Default";
  
  private static final String SERVICE_NAME = "securestore";
  
  private static final String TAG = "FelicadeviceManager";
  
  private Context mContext;
  
  private FelicadeviceManager() {
    Slog.v("FelicadeviceManager", "FelicadeviceManager new default");
    IFelicadeviceService iFelicadeviceService = IFelicadeviceService.Stub.asInterface(ServiceManager.getService("felicaser"));
    if (iFelicadeviceService == null)
      Slog.d("FelicadeviceManager", "can not get service securestore"); 
  }
  
  public static FelicadeviceManager getInstance() {
    // Byte code:
    //   0: ldc com/oplus/felica/FelicadeviceManager
    //   2: monitorenter
    //   3: ldc 'FelicadeviceManager'
    //   5: ldc 'FelicadeviceManager getInstance enter'
    //   7: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   10: pop
    //   11: getstatic com/oplus/felica/FelicadeviceManager.sFelicadeviceManager : Lcom/oplus/felica/FelicadeviceManager;
    //   14: ifnonnull -> 50
    //   17: ldc com/oplus/felica/FelicadeviceManager
    //   19: monitorenter
    //   20: getstatic com/oplus/felica/FelicadeviceManager.sFelicadeviceManager : Lcom/oplus/felica/FelicadeviceManager;
    //   23: ifnonnull -> 38
    //   26: new com/oplus/felica/FelicadeviceManager
    //   29: astore_0
    //   30: aload_0
    //   31: invokespecial <init> : ()V
    //   34: aload_0
    //   35: putstatic com/oplus/felica/FelicadeviceManager.sFelicadeviceManager : Lcom/oplus/felica/FelicadeviceManager;
    //   38: ldc com/oplus/felica/FelicadeviceManager
    //   40: monitorexit
    //   41: goto -> 50
    //   44: astore_0
    //   45: ldc com/oplus/felica/FelicadeviceManager
    //   47: monitorexit
    //   48: aload_0
    //   49: athrow
    //   50: getstatic com/oplus/felica/FelicadeviceManager.sFelicadeviceManager : Lcom/oplus/felica/FelicadeviceManager;
    //   53: astore_0
    //   54: ldc com/oplus/felica/FelicadeviceManager
    //   56: monitorexit
    //   57: aload_0
    //   58: areturn
    //   59: astore_0
    //   60: ldc com/oplus/felica/FelicadeviceManager
    //   62: monitorexit
    //   63: aload_0
    //   64: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #58	-> 3
    //   #59	-> 11
    //   #60	-> 17
    //   #61	-> 20
    //   #62	-> 26
    //   #64	-> 38
    //   #67	-> 50
    //   #57	-> 59
    // Exception table:
    //   from	to	target	type
    //   3	11	59	finally
    //   11	17	59	finally
    //   17	20	59	finally
    //   20	26	44	finally
    //   26	38	44	finally
    //   38	41	44	finally
    //   45	48	44	finally
    //   48	50	59	finally
    //   50	54	59	finally
  }
  
  public FelicadeviceManager(Context paramContext, IFelicadeviceService paramIFelicadeviceService) {
    Slog.v("FelicadeviceManager", "FelicadeviceManager(Context context, IFelicadeviceService service) new 1216");
    this.mContext = paramContext;
    this.mService = paramIFelicadeviceService;
    if (paramIFelicadeviceService == null)
      Slog.v("FelicadeviceManager", "mService is null"); 
  }
  
  public boolean oplusGetFelicaLockStatus() {
    boolean bool1 = false, bool2 = false;
    IFelicadeviceService iFelicadeviceService = this.mService;
    if (iFelicadeviceService != null) {
      try {
        bool2 = bool1 = iFelicadeviceService.oplusGetFelicaLockStatus();
      } catch (RemoteException remoteException) {
        Log.v("FelicadeviceManager", "Remote exception in oplusGetFelicaLockStatus(): ", (Throwable)remoteException);
      } 
    } else {
      Log.w("FelicadeviceManager", "oplusGetFelicaLockStatus(): Service not connected!");
      bool2 = bool1;
    } 
    return bool2;
  }
  
  public boolean oplusSetFelicaLockStatus(boolean paramBoolean) {
    boolean bool1 = false, bool2 = false;
    IFelicadeviceService iFelicadeviceService = this.mService;
    if (iFelicadeviceService != null) {
      try {
        paramBoolean = iFelicadeviceService.oplusSetFelicaLockStatus(paramBoolean);
      } catch (RemoteException remoteException) {
        Log.v("FelicadeviceManager", "Remote exception in oplusSetFelicaLockStatus(): ", (Throwable)remoteException);
        paramBoolean = bool2;
      } 
    } else {
      Log.w("FelicadeviceManager", "oplusSetFelicaLockStatus(): Service not connected!");
      paramBoolean = bool1;
    } 
    return paramBoolean;
  }
  
  public byte[] oplusGetFelicaLockKey() {
    RemoteException remoteException2;
    byte[] arrayOfByte1 = null, arrayOfByte2 = null;
    IFelicadeviceService iFelicadeviceService = this.mService;
    if (iFelicadeviceService != null) {
      try {
        arrayOfByte2 = arrayOfByte1 = iFelicadeviceService.oplusGetFelicaLockKey();
      } catch (RemoteException remoteException1) {
        Log.v("FelicadeviceManager", "Remote exception in oplusGetFelicaLockKey(): ", (Throwable)remoteException1);
      } 
    } else {
      Log.w("FelicadeviceManager", "oplusGetFelicaLockKey(): Service not connected!");
      remoteException2 = remoteException1;
    } 
    return (byte[])remoteException2;
  }
  
  public boolean oplusSetFelicaLockKey(byte[] paramArrayOfbyte) {
    boolean bool1 = false, bool2 = false;
    IFelicadeviceService iFelicadeviceService = this.mService;
    if (iFelicadeviceService != null) {
      try {
        bool2 = bool1 = iFelicadeviceService.oplusSetFelicaLockKey(paramArrayOfbyte);
      } catch (RemoteException remoteException) {
        Log.v("FelicadeviceManager", "Remote exception in opooSetFelicaLockKey(): ", (Throwable)remoteException);
      } 
    } else {
      Log.w("FelicadeviceManager", "opooSetFelicaLockKey(): Service not connected!");
      bool2 = bool1;
    } 
    return bool2;
  }
  
  public boolean oplusEraseFelicaLockData() {
    boolean bool1 = false, bool2 = false;
    IFelicadeviceService iFelicadeviceService = this.mService;
    if (iFelicadeviceService != null) {
      try {
        bool2 = bool1 = iFelicadeviceService.oplusEraseFelicaLockData();
      } catch (RemoteException remoteException) {
        Log.e("FelicadeviceManager", "Remote exception in oplusEraseFelicaLockData(): ", (Throwable)remoteException);
      } 
    } else {
      Log.w("FelicadeviceManager", "oplusEraseFelicaLockData(): Service not connected!");
      bool2 = bool1;
    } 
    return bool2;
  }
  
  public int oplusPnscrTestGpFelicaSpc() {
    int i = -1;
    IFelicadeviceService iFelicadeviceService = this.mService;
    if (iFelicadeviceService != null) {
      try {
        int j = iFelicadeviceService.oplusPnscrTestGpFelicaSpc();
      } catch (RemoteException remoteException) {
        Log.e("FelicadeviceManager", "Remote exception in oplusPnscrTestGpFelicaSpc(): ", (Throwable)remoteException);
      } 
    } else {
      Log.w("FelicadeviceManager", "oplusPnscrTestGpFelicaSpc(): Service not connected!");
    } 
    return i;
  }
  
  public boolean oplusPnscrTestCardEmulation() {
    boolean bool1 = false, bool2 = false;
    IFelicadeviceService iFelicadeviceService = this.mService;
    if (iFelicadeviceService != null) {
      try {
        bool2 = bool1 = iFelicadeviceService.oplusPnscrTestCardEmulation();
      } catch (RemoteException remoteException) {
        Log.e("FelicadeviceManager", "Remote exception in oplusPnscrTestCardEmulation(): ", (Throwable)remoteException);
      } 
    } else {
      Log.w("FelicadeviceManager", "oplusPnscrTestCardEmulation(): Service not connected!");
      bool2 = bool1;
    } 
    return bool2;
  }
}
