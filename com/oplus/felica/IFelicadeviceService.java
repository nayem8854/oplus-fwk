package com.oplus.felica;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IFelicadeviceService extends IInterface {
  boolean oplusEraseFelicaLockData() throws RemoteException;
  
  byte[] oplusGetFelicaLockKey() throws RemoteException;
  
  boolean oplusGetFelicaLockStatus() throws RemoteException;
  
  boolean oplusPnscrTestCardEmulation() throws RemoteException;
  
  int oplusPnscrTestGpFelicaSpc() throws RemoteException;
  
  boolean oplusSetFelicaLockKey(byte[] paramArrayOfbyte) throws RemoteException;
  
  boolean oplusSetFelicaLockStatus(boolean paramBoolean) throws RemoteException;
  
  class Default implements IFelicadeviceService {
    public boolean oplusGetFelicaLockStatus() throws RemoteException {
      return false;
    }
    
    public boolean oplusSetFelicaLockStatus(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public byte[] oplusGetFelicaLockKey() throws RemoteException {
      return null;
    }
    
    public boolean oplusSetFelicaLockKey(byte[] param1ArrayOfbyte) throws RemoteException {
      return false;
    }
    
    public boolean oplusEraseFelicaLockData() throws RemoteException {
      return false;
    }
    
    public int oplusPnscrTestGpFelicaSpc() throws RemoteException {
      return 0;
    }
    
    public boolean oplusPnscrTestCardEmulation() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IFelicadeviceService {
    private static final String DESCRIPTOR = "com.oplus.felica.IFelicadeviceService";
    
    static final int TRANSACTION_oplusEraseFelicaLockData = 5;
    
    static final int TRANSACTION_oplusGetFelicaLockKey = 3;
    
    static final int TRANSACTION_oplusGetFelicaLockStatus = 1;
    
    static final int TRANSACTION_oplusPnscrTestCardEmulation = 7;
    
    static final int TRANSACTION_oplusPnscrTestGpFelicaSpc = 6;
    
    static final int TRANSACTION_oplusSetFelicaLockKey = 4;
    
    static final int TRANSACTION_oplusSetFelicaLockStatus = 2;
    
    public Stub() {
      attachInterface(this, "com.oplus.felica.IFelicadeviceService");
    }
    
    public static IFelicadeviceService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.felica.IFelicadeviceService");
      if (iInterface != null && iInterface instanceof IFelicadeviceService)
        return (IFelicadeviceService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "oplusPnscrTestCardEmulation";
        case 6:
          return "oplusPnscrTestGpFelicaSpc";
        case 5:
          return "oplusEraseFelicaLockData";
        case 4:
          return "oplusSetFelicaLockKey";
        case 3:
          return "oplusGetFelicaLockKey";
        case 2:
          return "oplusSetFelicaLockStatus";
        case 1:
          break;
      } 
      return "oplusGetFelicaLockStatus";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int i;
        byte[] arrayOfByte;
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("com.oplus.felica.IFelicadeviceService");
            bool2 = oplusPnscrTestCardEmulation();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 6:
            param1Parcel1.enforceInterface("com.oplus.felica.IFelicadeviceService");
            i = oplusPnscrTestGpFelicaSpc();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 5:
            param1Parcel1.enforceInterface("com.oplus.felica.IFelicadeviceService");
            bool1 = oplusEraseFelicaLockData();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("com.oplus.felica.IFelicadeviceService");
            arrayOfByte = param1Parcel1.createByteArray();
            bool1 = oplusSetFelicaLockKey(arrayOfByte);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 3:
            arrayOfByte.enforceInterface("com.oplus.felica.IFelicadeviceService");
            arrayOfByte = oplusGetFelicaLockKey();
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte);
            return true;
          case 2:
            arrayOfByte.enforceInterface("com.oplus.felica.IFelicadeviceService");
            if (arrayOfByte.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            bool1 = oplusSetFelicaLockStatus(bool);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        arrayOfByte.enforceInterface("com.oplus.felica.IFelicadeviceService");
        boolean bool1 = oplusGetFelicaLockStatus();
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("com.oplus.felica.IFelicadeviceService");
      return true;
    }
    
    private static class Proxy implements IFelicadeviceService {
      public static IFelicadeviceService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.felica.IFelicadeviceService";
      }
      
      public boolean oplusGetFelicaLockStatus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.felica.IFelicadeviceService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IFelicadeviceService.Stub.getDefaultImpl() != null) {
            bool1 = IFelicadeviceService.Stub.getDefaultImpl().oplusGetFelicaLockStatus();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean oplusSetFelicaLockStatus(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.felica.IFelicadeviceService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool1 && IFelicadeviceService.Stub.getDefaultImpl() != null) {
            param2Boolean = IFelicadeviceService.Stub.getDefaultImpl().oplusSetFelicaLockStatus(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] oplusGetFelicaLockKey() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.felica.IFelicadeviceService");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IFelicadeviceService.Stub.getDefaultImpl() != null)
            return IFelicadeviceService.Stub.getDefaultImpl().oplusGetFelicaLockKey(); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean oplusSetFelicaLockKey(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.felica.IFelicadeviceService");
          parcel1.writeByteArray(param2ArrayOfbyte);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IFelicadeviceService.Stub.getDefaultImpl() != null) {
            bool1 = IFelicadeviceService.Stub.getDefaultImpl().oplusSetFelicaLockKey(param2ArrayOfbyte);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean oplusEraseFelicaLockData() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.felica.IFelicadeviceService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IFelicadeviceService.Stub.getDefaultImpl() != null) {
            bool1 = IFelicadeviceService.Stub.getDefaultImpl().oplusEraseFelicaLockData();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int oplusPnscrTestGpFelicaSpc() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.felica.IFelicadeviceService");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IFelicadeviceService.Stub.getDefaultImpl() != null)
            return IFelicadeviceService.Stub.getDefaultImpl().oplusPnscrTestGpFelicaSpc(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean oplusPnscrTestCardEmulation() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.felica.IFelicadeviceService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IFelicadeviceService.Stub.getDefaultImpl() != null) {
            bool1 = IFelicadeviceService.Stub.getDefaultImpl().oplusPnscrTestCardEmulation();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IFelicadeviceService param1IFelicadeviceService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IFelicadeviceService != null) {
          Proxy.sDefaultImpl = param1IFelicadeviceService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IFelicadeviceService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
