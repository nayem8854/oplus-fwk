package com.oplus.drmDecoder;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.FileDescriptor;
import java.io.InputStream;

public class OplusDrmDecoderFeature implements IOplusDrmDecoderFeature {
  private static final String TAG = "OplusDrmDecoderFeature";
  
  private OplusDrmDecoderManager manager = OplusDrmDecoderFactory.getInstance().makeOplusDrmDecoderManager();
  
  private static class InstanceHolder {
    static final OplusDrmDecoderFeature INSTANCE = new OplusDrmDecoderFeature();
  }
  
  public static OplusDrmDecoderFeature getInstance() {
    return InstanceHolder.INSTANCE;
  }
  
  public boolean isDrmLoop() {
    return this.manager.isDrmLoop();
  }
  
  public Bitmap decodeDrmImageIfNeededImpl(byte[] paramArrayOfbyte, InputStream paramInputStream, BitmapFactory.Options paramOptions) {
    return this.manager.decodeDrmImageIfNeededImpl(paramArrayOfbyte, paramInputStream, paramOptions);
  }
  
  public Bitmap decodeDrmImageIfNeededImpl(FileDescriptor paramFileDescriptor, BitmapFactory.Options paramOptions) {
    return this.manager.decodeDrmImageIfNeededImpl(paramFileDescriptor, paramOptions);
  }
  
  public Bitmap decodeDrmImageIfNeededImpl(byte[] paramArrayOfbyte, BitmapFactory.Options paramOptions) {
    return this.manager.decodeDrmImageIfNeededImpl(paramArrayOfbyte, paramOptions);
  }
  
  private OplusDrmDecoderFeature() {}
}
