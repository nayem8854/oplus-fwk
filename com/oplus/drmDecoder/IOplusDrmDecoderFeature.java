package com.oplus.drmDecoder;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.FileDescriptor;
import java.io.InputStream;

public interface IOplusDrmDecoderFeature extends IOplusCommonFeature {
  public static final IOplusDrmDecoderFeature DEFAULT = (IOplusDrmDecoderFeature)new Object();
  
  public static final String NAME = "IOplusDrmDecoder";
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusDrmDecoderFeature;
  }
  
  default IOplusCommonFeature getDefault() {
    return DEFAULT;
  }
  
  default boolean isDrmLoop() {
    return false;
  }
  
  default Bitmap decodeDrmImageIfNeededImpl(byte[] paramArrayOfbyte, InputStream paramInputStream, BitmapFactory.Options paramOptions) {
    return null;
  }
  
  default Bitmap decodeDrmImageIfNeededImpl(FileDescriptor paramFileDescriptor, BitmapFactory.Options paramOptions) {
    return null;
  }
  
  default Bitmap decodeDrmImageIfNeededImpl(byte[] paramArrayOfbyte, BitmapFactory.Options paramOptions) {
    return null;
  }
}
