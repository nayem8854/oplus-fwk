package com.oplus.drmDecoder;

public class OplusDrmDecoderFactory {
  private static final String TAG = "OplusDrmDecoderFactory";
  
  private static OplusDrmDecoderFactory sInstance = null;
  
  public static OplusDrmDecoderFactory getInstance() {
    if (sInstance == null)
      sInstance = new OplusDrmDecoderFactory(); 
    return sInstance;
  }
  
  public OplusDrmDecoderManager makeOplusDrmDecoderManager() {
    return new OplusDrmDecoderManager();
  }
}
