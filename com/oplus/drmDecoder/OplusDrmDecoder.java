package com.oplus.drmDecoder;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.SystemProperties;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import libcore.io.IoBridge;

public class OplusDrmDecoder {
  private static final int HEADER_BUFFER_SIZE = 128;
  
  private static final String TAG = "OplusDrmDecoder";
  
  private static boolean sIsOmaDrmEnabled = false;
  
  static {
    boolean bool = SystemProperties.getBoolean("persist.sys.oplus.oma.support", false);
    if (bool)
      System.loadLibrary("oplusdrmdecoderjni"); 
  }
  
  public static Bitmap decodeDrmImageIfNeeded(byte[] paramArrayOfbyte, InputStream paramInputStream, BitmapFactory.Options paramOptions) {
    // Byte code:
    //   0: getstatic com/oplus/drmDecoder/OplusDrmDecoder.sIsOmaDrmEnabled : Z
    //   3: ifne -> 8
    //   6: aconst_null
    //   7: areturn
    //   8: aload_2
    //   9: ifnull -> 35
    //   12: aload_2
    //   13: getfield inJustDecodeBounds : Z
    //   16: ifeq -> 35
    //   19: aload_2
    //   20: getfield outWidth : I
    //   23: ifle -> 35
    //   26: aload_2
    //   27: getfield outHeight : I
    //   30: ifle -> 35
    //   33: aconst_null
    //   34: areturn
    //   35: ldc 'OplusDrmDecoder'
    //   37: ldc 'decodeDrmImageIfNeeded with stream'
    //   39: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   42: pop
    //   43: aload_0
    //   44: ifnonnull -> 49
    //   47: aconst_null
    //   48: areturn
    //   49: aload_0
    //   50: invokestatic isDrmFile : ([B)Z
    //   53: ifne -> 58
    //   56: aconst_null
    //   57: areturn
    //   58: aconst_null
    //   59: astore_3
    //   60: aconst_null
    //   61: astore #4
    //   63: aconst_null
    //   64: astore #5
    //   66: aconst_null
    //   67: astore #6
    //   69: aload #6
    //   71: astore #7
    //   73: aload #5
    //   75: astore_0
    //   76: new com/oplus/drmDecoder/OmaDrmInputStream
    //   79: astore #8
    //   81: aload #6
    //   83: astore #7
    //   85: aload #5
    //   87: astore_0
    //   88: aload #8
    //   90: aload_1
    //   91: iconst_1
    //   92: invokespecial <init> : (Ljava/io/InputStream;Z)V
    //   95: aload #8
    //   97: astore_1
    //   98: aload_1
    //   99: astore #7
    //   101: aload_1
    //   102: astore_0
    //   103: aload_1
    //   104: aconst_null
    //   105: aload_2
    //   106: invokestatic decodeStream : (Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   109: astore_2
    //   110: aload_2
    //   111: astore_0
    //   112: aload_1
    //   113: invokevirtual close : ()V
    //   116: aload_0
    //   117: astore_1
    //   118: aload_1
    //   119: astore_0
    //   120: goto -> 240
    //   123: astore_1
    //   124: new java/lang/StringBuilder
    //   127: dup
    //   128: invokespecial <init> : ()V
    //   131: astore_2
    //   132: aload_2
    //   133: ldc 'Unable to close drm file: '
    //   135: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   138: pop
    //   139: aload_2
    //   140: aload_1
    //   141: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   144: pop
    //   145: ldc 'OplusDrmDecoder'
    //   147: aload_2
    //   148: invokevirtual toString : ()Ljava/lang/String;
    //   151: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   154: pop
    //   155: goto -> 240
    //   158: astore_0
    //   159: goto -> 242
    //   162: astore_1
    //   163: aload_0
    //   164: astore #7
    //   166: new java/lang/StringBuilder
    //   169: astore_2
    //   170: aload_0
    //   171: astore #7
    //   173: aload_2
    //   174: invokespecial <init> : ()V
    //   177: aload_0
    //   178: astore #7
    //   180: aload_2
    //   181: ldc 'Error while getBitmap! '
    //   183: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   186: pop
    //   187: aload_0
    //   188: astore #7
    //   190: aload_2
    //   191: aload_1
    //   192: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   195: pop
    //   196: aload_0
    //   197: astore #7
    //   199: ldc 'OplusDrmDecoder'
    //   201: aload_2
    //   202: invokevirtual toString : ()Ljava/lang/String;
    //   205: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   208: pop
    //   209: aload #4
    //   211: astore_1
    //   212: aload_0
    //   213: ifnull -> 118
    //   216: aload_0
    //   217: invokevirtual close : ()V
    //   220: aload #4
    //   222: astore_1
    //   223: goto -> 118
    //   226: astore_1
    //   227: new java/lang/StringBuilder
    //   230: dup
    //   231: invokespecial <init> : ()V
    //   234: astore_2
    //   235: aload_3
    //   236: astore_0
    //   237: goto -> 132
    //   240: aload_0
    //   241: areturn
    //   242: aload #7
    //   244: ifnull -> 290
    //   247: aload #7
    //   249: invokevirtual close : ()V
    //   252: goto -> 290
    //   255: astore_2
    //   256: new java/lang/StringBuilder
    //   259: dup
    //   260: invokespecial <init> : ()V
    //   263: astore_1
    //   264: aload_1
    //   265: ldc 'Unable to close drm file: '
    //   267: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   270: pop
    //   271: aload_1
    //   272: aload_2
    //   273: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   276: pop
    //   277: ldc 'OplusDrmDecoder'
    //   279: aload_1
    //   280: invokevirtual toString : ()Ljava/lang/String;
    //   283: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   286: pop
    //   287: goto -> 290
    //   290: aload_0
    //   291: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #58	-> 0
    //   #59	-> 6
    //   #62	-> 8
    //   #63	-> 33
    //   #66	-> 35
    //   #67	-> 43
    //   #68	-> 47
    //   #71	-> 49
    //   #72	-> 56
    //   #75	-> 58
    //   #76	-> 63
    //   #78	-> 69
    //   #79	-> 98
    //   #85	-> 112
    //   #86	-> 112
    //   #90	-> 118
    //   #88	-> 123
    //   #89	-> 124
    //   #91	-> 155
    //   #84	-> 158
    //   #80	-> 162
    //   #81	-> 163
    //   #85	-> 209
    //   #86	-> 216
    //   #88	-> 226
    //   #89	-> 227
    //   #93	-> 240
    //   #85	-> 242
    //   #86	-> 247
    //   #88	-> 255
    //   #89	-> 256
    //   #90	-> 290
    //   #91	-> 290
    // Exception table:
    //   from	to	target	type
    //   76	81	162	java/lang/Exception
    //   76	81	158	finally
    //   88	95	162	java/lang/Exception
    //   88	95	158	finally
    //   103	110	162	java/lang/Exception
    //   103	110	158	finally
    //   112	116	123	java/io/IOException
    //   166	170	158	finally
    //   173	177	158	finally
    //   180	187	158	finally
    //   190	196	158	finally
    //   199	209	158	finally
    //   216	220	226	java/io/IOException
    //   247	252	255	java/io/IOException
  }
  
  public static Bitmap decodeDrmImageIfNeeded(FileDescriptor paramFileDescriptor, BitmapFactory.Options paramOptions) {
    if (!sIsOmaDrmEnabled)
      return null; 
    if (paramOptions != null && paramOptions.inJustDecodeBounds && paramOptions.outWidth > 0 && paramOptions.outHeight > 0)
      return null; 
    Log.d("OplusDrmDecoder", "decodeDrmImageIfNeeded with fd");
    long l1 = -1L;
    long l2 = l1, l3 = l1;
    try {
      long l = Os.lseek(paramFileDescriptor, 0L, OsConstants.SEEK_CUR);
      l2 = l;
      l3 = l;
      l1 = l;
      Os.lseek(paramFileDescriptor, 0L, OsConstants.SEEK_SET);
      l2 = l;
      l3 = l;
      l1 = l;
      byte[] arrayOfByte = new byte[128];
      l2 = l;
      l3 = l;
      l1 = l;
      int i = IoBridge.read(paramFileDescriptor, arrayOfByte, 0, 128);
      if (i == 128) {
        l2 = l;
        l3 = l;
        l1 = l;
        boolean bool = isDrmFile(arrayOfByte);
        if (bool) {
          StringBuilder stringBuilder;
          if (l != -1L)
            try {
              Os.lseek(paramFileDescriptor, l, OsConstants.SEEK_SET);
            } catch (ErrnoException null) {
              Log.e("OplusDrmDecoder", "decodeDrmImageIfNeeded seek fd to initial offset with ", (Throwable)errnoException);
              return null;
            }  
          byte[] arrayOfByte1 = null;
          OmaDrmInputStream omaDrmInputStream1 = null, omaDrmInputStream2 = null;
          OmaDrmInputStream omaDrmInputStream3 = omaDrmInputStream2;
          arrayOfByte = arrayOfByte1;
          OmaDrmInputStream omaDrmInputStream4 = omaDrmInputStream1;
          try {
            OmaDrmInputStream omaDrmInputStream = new OmaDrmInputStream();
            omaDrmInputStream3 = omaDrmInputStream2;
            arrayOfByte = arrayOfByte1;
            omaDrmInputStream4 = omaDrmInputStream1;
            this((FileDescriptor)errnoException, true);
            omaDrmInputStream1 = omaDrmInputStream;
            omaDrmInputStream3 = omaDrmInputStream1;
            arrayOfByte = arrayOfByte1;
            omaDrmInputStream4 = omaDrmInputStream1;
            StringBuilder stringBuilder1 = new StringBuilder();
            omaDrmInputStream3 = omaDrmInputStream1;
            arrayOfByte = arrayOfByte1;
            omaDrmInputStream4 = omaDrmInputStream1;
            this();
            omaDrmInputStream3 = omaDrmInputStream1;
            arrayOfByte = arrayOfByte1;
            omaDrmInputStream4 = omaDrmInputStream1;
            stringBuilder1.append("pass OmaDrmInputStream + ");
            omaDrmInputStream3 = omaDrmInputStream1;
            arrayOfByte = arrayOfByte1;
            omaDrmInputStream4 = omaDrmInputStream1;
            stringBuilder1.append(omaDrmInputStream1);
            omaDrmInputStream3 = omaDrmInputStream1;
            arrayOfByte = arrayOfByte1;
            omaDrmInputStream4 = omaDrmInputStream1;
            Log.d("OplusDrmDecoder", stringBuilder1.toString());
            omaDrmInputStream3 = omaDrmInputStream1;
            arrayOfByte = arrayOfByte1;
            omaDrmInputStream4 = omaDrmInputStream1;
            Bitmap bitmap1 = BitmapFactory.decodeStream(omaDrmInputStream1, null, paramOptions);
            omaDrmInputStream3 = omaDrmInputStream1;
            Bitmap bitmap2 = bitmap1;
            omaDrmInputStream4 = omaDrmInputStream1;
            StringBuilder stringBuilder2 = new StringBuilder();
            omaDrmInputStream3 = omaDrmInputStream1;
            bitmap2 = bitmap1;
            omaDrmInputStream4 = omaDrmInputStream1;
            this();
            omaDrmInputStream3 = omaDrmInputStream1;
            bitmap2 = bitmap1;
            omaDrmInputStream4 = omaDrmInputStream1;
            stringBuilder2.append("OmaDrmInputStream bm is + ");
            omaDrmInputStream3 = omaDrmInputStream1;
            bitmap2 = bitmap1;
            omaDrmInputStream4 = omaDrmInputStream1;
            stringBuilder2.append(bitmap1);
            omaDrmInputStream3 = omaDrmInputStream1;
            bitmap2 = bitmap1;
            omaDrmInputStream4 = omaDrmInputStream1;
            Log.d("OplusDrmDecoder", stringBuilder2.toString());
            try {
              omaDrmInputStream1.close();
            } catch (IOException iOException) {
              stringBuilder = new StringBuilder();
              stringBuilder.append("Unable to close drm file: ");
              stringBuilder.append(iOException);
              Log.e("OplusDrmDecoder", stringBuilder.toString());
            } 
          } catch (Exception exception) {
            omaDrmInputStream3 = omaDrmInputStream4;
            StringBuilder stringBuilder1 = new StringBuilder();
            omaDrmInputStream3 = omaDrmInputStream4;
            this();
            omaDrmInputStream3 = omaDrmInputStream4;
            stringBuilder1.append("Error while getBitmap! ");
            omaDrmInputStream3 = omaDrmInputStream4;
            stringBuilder1.append(exception);
            omaDrmInputStream3 = omaDrmInputStream4;
            Log.e("OplusDrmDecoder", stringBuilder1.toString());
            stringBuilder1 = stringBuilder;
            if (omaDrmInputStream4 != null)
              try {
                omaDrmInputStream4.close();
                stringBuilder1 = stringBuilder;
              } catch (IOException iOException) {
                StringBuilder stringBuilder2 = new StringBuilder();
                stringBuilder1 = stringBuilder;
                stringBuilder = stringBuilder2;
                stringBuilder.append("Unable to close drm file: ");
                stringBuilder.append(iOException);
                Log.e("OplusDrmDecoder", stringBuilder.toString());
              }  
          } finally {}
          return (Bitmap)errnoException;
        } 
      } 
      if (l != -1L)
        try {
          Os.lseek((FileDescriptor)errnoException, l, OsConstants.SEEK_SET);
        } catch (ErrnoException null) {
          Log.e("OplusDrmDecoder", "decodeDrmImageIfNeeded seek fd to initial offset with ", (Throwable)errnoException);
          return null;
        }  
      return null;
    } catch (ErrnoException errnoException1) {
      l2 = l1;
      Log.e("OplusDrmDecoder", "decodeDrmImageIfNeeded seek fd to beginning with ", (Throwable)errnoException1);
      if (l1 != -1L)
        try {
          Os.lseek((FileDescriptor)errnoException, l1, OsConstants.SEEK_SET);
        } catch (ErrnoException null) {
          Log.e("OplusDrmDecoder", "decodeDrmImageIfNeeded seek fd to initial offset with ", (Throwable)errnoException);
          return null;
        }  
      return null;
    } catch (IOException iOException) {
      l2 = l3;
      Log.e("OplusDrmDecoder", "decodeDrmImageIfNeeded get header with ", iOException);
      if (l3 != -1L)
        try {
          Os.lseek((FileDescriptor)errnoException, l3, OsConstants.SEEK_SET);
        } catch (ErrnoException errnoException) {
          Log.e("OplusDrmDecoder", "decodeDrmImageIfNeeded seek fd to initial offset with ", (Throwable)errnoException);
          return null;
        }  
      return null;
    } finally {}
    if (l2 != -1L)
      try {
        Os.lseek((FileDescriptor)errnoException, l2, OsConstants.SEEK_SET);
      } catch (ErrnoException errnoException1) {
        Log.e("OplusDrmDecoder", "decodeDrmImageIfNeeded seek fd to initial offset with ", (Throwable)errnoException1);
        return null;
      }  
    throw paramOptions;
  }
  
  public static Bitmap decodeDrmImageIfNeeded(byte[] paramArrayOfbyte, BitmapFactory.Options paramOptions) {
    if (!sIsOmaDrmEnabled)
      return null; 
    if (paramOptions != null && paramOptions.inJustDecodeBounds && paramOptions.outWidth > 0 && paramOptions.outHeight > 0)
      return null; 
    Log.d("OplusDrmDecoder", "decodeDrmImageIfNeeded with data");
    if (!isDrmFile(paramArrayOfbyte))
      return null; 
    return null;
  }
  
  private static boolean isDrmFile(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte == null || paramArrayOfbyte.length < 128)
      return false; 
    String str = new String(paramArrayOfbyte, 0, 8);
    if (str.startsWith("FWLK")) {
      String str1 = new String(paramArrayOfbyte, 8, 6);
      if (str1.startsWith("image/")) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("isDrmFile: this is a fwlk file: ");
        stringBuilder.append(str);
        Log.v("OplusDrmDecoder", stringBuilder.toString());
        return true;
      } 
    } 
    return false;
  }
}
