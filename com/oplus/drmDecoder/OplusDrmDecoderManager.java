package com.oplus.drmDecoder;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.FileDescriptor;
import java.io.InputStream;

public class OplusDrmDecoderManager {
  public boolean isDrmLoop() {
    Throwable throwable = new Throwable();
    StackTraceElement[] arrayOfStackTraceElement = throwable.getStackTrace();
    if (arrayOfStackTraceElement != null)
      for (byte b = 0; b < arrayOfStackTraceElement.length; b++) {
        String str = arrayOfStackTraceElement[b].getMethodName();
        if (str != null && str.equals("decodeDrmImageIfNeeded"))
          return true; 
      }  
    return false;
  }
  
  public Bitmap decodeDrmImageIfNeededImpl(byte[] paramArrayOfbyte, InputStream paramInputStream, BitmapFactory.Options paramOptions) {
    return OplusDrmDecoder.decodeDrmImageIfNeeded(paramArrayOfbyte, paramInputStream, paramOptions);
  }
  
  public Bitmap decodeDrmImageIfNeededImpl(FileDescriptor paramFileDescriptor, BitmapFactory.Options paramOptions) {
    return OplusDrmDecoder.decodeDrmImageIfNeeded(paramFileDescriptor, paramOptions);
  }
  
  public Bitmap decodeDrmImageIfNeededImpl(byte[] paramArrayOfbyte, BitmapFactory.Options paramOptions) {
    return OplusDrmDecoder.decodeDrmImageIfNeeded(paramArrayOfbyte, paramOptions);
  }
}
