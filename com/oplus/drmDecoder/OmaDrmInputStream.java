package com.oplus.drmDecoder;

import android.media.MediaDrmException;
import android.util.Log;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class OmaDrmInputStream extends InputStream {
  private static int sSeqUniqueId = 0;
  
  private int mUniqueId = -1;
  
  private FileDescriptor mFD = null;
  
  FileInputStream mRedirectedInputStream = null;
  
  private static final int DRM_DUMP_BYTE_SIZE = 4096;
  
  private static final boolean IS_DRM_USES_REDIRECTED_INPUT_STREAM = true;
  
  private static final String TAG = "OmaDrmInputStream";
  
  private static final int UNIQUE_ID_NOT_INITIALIZED = -1;
  
  private boolean mConsumeRights;
  
  private String mDumpFilePath;
  
  private long mFileSize;
  
  private File tmpFile;
  
  public OmaDrmInputStream(InputStream paramInputStream, boolean paramBoolean) throws FileNotFoundException, MediaDrmException {
    this.mConsumeRights = paramBoolean;
    try {
      if (paramInputStream instanceof FileInputStream)
        this.mFD = ((FileInputStream)paramInputStream).getFD(); 
    } catch (IOException iOException) {
      Log.e("OmaDrmInputStream", "OmaDrmInputStream getFd fail ", iOException);
    } 
    if (this.mFD != null) {
      int i = generateUniqueId();
      i = initializeInputStream(i, this.mFD, this.mConsumeRights);
      if (i < 0) {
        try {
          close();
        } catch (IOException iOException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Eception while closing OmaDrmInputStream. ");
          stringBuilder.append(iOException);
          Log.e("OmaDrmInputStream", stringBuilder.toString());
        } 
        throw new MediaDrmException("MediaDrmException! May Rights invalid!");
      } 
    } 
    this.mFileSize = getFileSizeFromOmaDrmInputStream(this.mUniqueId);
    prepareRedirectedInputStream();
  }
  
  public OmaDrmInputStream(FileDescriptor paramFileDescriptor, boolean paramBoolean) throws FileNotFoundException, MediaDrmException {
    this.mConsumeRights = paramBoolean;
    this.mFD = paramFileDescriptor;
    if (paramFileDescriptor != null) {
      int i = generateUniqueId();
      i = initializeInputStream(i, this.mFD, this.mConsumeRights);
      if (i < 0) {
        try {
          close();
        } catch (IOException iOException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Eception while closing OmaDrmInputStream. ");
          stringBuilder.append(iOException);
          Log.e("OmaDrmInputStream", stringBuilder.toString());
        } 
        throw new MediaDrmException("MediaDrmException! May Rights invalid!");
      } 
    } 
    this.mFileSize = getFileSizeFromOmaDrmInputStream(this.mUniqueId);
    prepareRedirectedInputStream();
  }
  
  private void prepareRedirectedInputStream() {
    try {
      FileInputStream fileInputStream = new FileInputStream();
      File file = new File();
      this(creadeDrmDumpFile());
      this(file);
      this.mRedirectedInputStream = fileInputStream;
    } catch (FileNotFoundException fileNotFoundException) {
      Log.e("OmaDrmInputStream", "OmaDrmInputStream mRedirectedInputStream failed");
      fileNotFoundException.printStackTrace();
    } 
  }
  
  private void deleteDrmDumpFile() {
    if (this.mDumpFilePath == null)
      return; 
    File file = new File(this.mDumpFilePath);
    if (file.exists())
      file.delete(); 
  }
  
  public File getTmpFile() {
    return this.tmpFile;
  }
  
  private String creadeDrmDumpFile() {
    byte[] arrayOfByte = new byte[4096];
    int i = 0;
    FileOutputStream fileOutputStream1 = null, fileOutputStream2 = null;
    FileOutputStream fileOutputStream3 = fileOutputStream2, fileOutputStream4 = fileOutputStream1;
    try {
      File file = new File();
      fileOutputStream3 = fileOutputStream2;
      fileOutputStream4 = fileOutputStream1;
      StringBuilder stringBuilder2 = new StringBuilder();
      fileOutputStream3 = fileOutputStream2;
      fileOutputStream4 = fileOutputStream1;
      this();
      fileOutputStream3 = fileOutputStream2;
      fileOutputStream4 = fileOutputStream1;
      stringBuilder2.append("/storage/emulated/0/.drm");
      fileOutputStream3 = fileOutputStream2;
      fileOutputStream4 = fileOutputStream1;
      stringBuilder2.append(System.currentTimeMillis());
      fileOutputStream3 = fileOutputStream2;
      fileOutputStream4 = fileOutputStream1;
      stringBuilder2.append(".jpg");
      fileOutputStream3 = fileOutputStream2;
      fileOutputStream4 = fileOutputStream1;
      this(stringBuilder2.toString());
      fileOutputStream3 = fileOutputStream2;
      fileOutputStream4 = fileOutputStream1;
      if (file.exists()) {
        fileOutputStream3 = fileOutputStream2;
        fileOutputStream4 = fileOutputStream1;
        file.delete();
      } 
      fileOutputStream3 = fileOutputStream2;
      fileOutputStream4 = fileOutputStream1;
      Log.d("OmaDrmInputStream", file.getAbsolutePath());
      fileOutputStream3 = fileOutputStream2;
      fileOutputStream4 = fileOutputStream1;
      file.createNewFile();
      fileOutputStream3 = fileOutputStream2;
      fileOutputStream4 = fileOutputStream1;
      this.tmpFile = file;
      fileOutputStream3 = fileOutputStream2;
      fileOutputStream4 = fileOutputStream1;
      this.mDumpFilePath = file.getAbsolutePath();
      fileOutputStream3 = fileOutputStream2;
      fileOutputStream4 = fileOutputStream1;
      FileOutputStream fileOutputStream = new FileOutputStream();
      fileOutputStream3 = fileOutputStream2;
      fileOutputStream4 = fileOutputStream1;
      this(file);
      fileOutputStream2 = fileOutputStream;
      fileOutputStream3 = fileOutputStream2;
      fileOutputStream4 = fileOutputStream2;
      StringBuilder stringBuilder1 = new StringBuilder();
      fileOutputStream3 = fileOutputStream2;
      fileOutputStream4 = fileOutputStream2;
      this();
      fileOutputStream3 = fileOutputStream2;
      fileOutputStream4 = fileOutputStream2;
      stringBuilder1.append("createDrmDumpFIle mDumpFIlePath = ");
      fileOutputStream3 = fileOutputStream2;
      fileOutputStream4 = fileOutputStream2;
      stringBuilder1.append(this.mDumpFilePath);
      fileOutputStream3 = fileOutputStream2;
      fileOutputStream4 = fileOutputStream2;
      Log.d("OmaDrmInputStream", stringBuilder1.toString());
      while (true) {
        fileOutputStream3 = fileOutputStream2;
        fileOutputStream4 = fileOutputStream2;
        int j = readFromOmaDrmInputStream(this.mUniqueId, arrayOfByte, 4096, i);
        if (j > 0) {
          fileOutputStream3 = fileOutputStream2;
          fileOutputStream4 = fileOutputStream2;
          fileOutputStream2.write(arrayOfByte, 0, j);
          i += j;
          continue;
        } 
        break;
      } 
      try {
        fileOutputStream2.close();
      } catch (IOException iOException) {
        iOException.printStackTrace();
      } 
    } catch (Exception exception) {
      fileOutputStream3 = fileOutputStream4;
      exception.printStackTrace();
      fileOutputStream3 = fileOutputStream4;
      Log.w("OmaDrmInputStream", "createDrmDumpFIle fail");
      if (fileOutputStream4 != null)
        try {
          fileOutputStream4.close();
        } catch (IOException iOException) {
          iOException.printStackTrace();
        }  
    } finally {}
    return this.mDumpFilePath;
  }
  
  private int generateUniqueId() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic com/oplus/drmDecoder/OmaDrmInputStream.sSeqUniqueId : I
    //   5: istore_1
    //   6: iload_1
    //   7: iconst_1
    //   8: iadd
    //   9: istore_2
    //   10: iload_2
    //   11: putstatic com/oplus/drmDecoder/OmaDrmInputStream.sSeqUniqueId : I
    //   14: iload_2
    //   15: ldc 2147483647
    //   17: if_icmplt -> 24
    //   20: iconst_0
    //   21: putstatic com/oplus/drmDecoder/OmaDrmInputStream.sSeqUniqueId : I
    //   24: aload_0
    //   25: monitorexit
    //   26: iload_1
    //   27: ireturn
    //   28: astore_3
    //   29: aload_0
    //   30: monitorexit
    //   31: aload_3
    //   32: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #178	-> 2
    //   #179	-> 14
    //   #180	-> 20
    //   #182	-> 24
    //   #177	-> 28
    // Exception table:
    //   from	to	target	type
    //   2	6	28	finally
    //   10	14	28	finally
    //   20	24	28	finally
  }
  
  public int read() throws IOException {
    if (this.mUniqueId != -1) {
      byte[] arrayOfByte = new byte[1];
      return this.mRedirectedInputStream.read();
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("DrmInputStream has not been initialized! mUniqueId = ");
    stringBuilder.append(this.mUniqueId);
    throw new IOException(stringBuilder.toString());
  }
  
  public int read(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws IOException {
    if (this.mUniqueId != -1) {
      paramInt1 = this.mRedirectedInputStream.read(paramArrayOfbyte, paramInt1, paramInt2);
      return paramInt1;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("DrmInputStream has not been initialized! mUniqueId = ");
    stringBuilder.append(this.mUniqueId);
    throw new IOException(stringBuilder.toString());
  }
  
  public void close() throws IOException {
    FileInputStream fileInputStream = this.mRedirectedInputStream;
    if (fileInputStream != null)
      try {
        fileInputStream.close();
      } catch (IOException iOException) {
        Log.w("OmaDrmInputStream", "Close InputStream failed.");
      }  
    releaseInputStream(this.mUniqueId);
    deleteDrmDumpFile();
  }
  
  public long size() {
    return this.mFileSize;
  }
  
  public static int initializeInputStream(int paramInt, FileDescriptor paramFileDescriptor, boolean paramBoolean) {
    return _initializeOmaDrmInputStream(paramInt, paramFileDescriptor, paramBoolean);
  }
  
  public static int releaseInputStream(int paramInt) {
    return _closeOmaDrmInputStream(paramInt);
  }
  
  public static int readFromOmaDrmInputStream(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3) {
    return _readOmaDrmInputStream(paramInt1, paramArrayOfbyte, paramInt2, paramInt3);
  }
  
  public static long getFileSizeFromOmaDrmInputStream(int paramInt) {
    return _fileSizeOmaDrmInputStream(paramInt);
  }
  
  public static Object getOmaDrmInfoOmaDrmInputStream(int paramInt) {
    return _getOmaDrmInfoOmaDrmInputStream(paramInt);
  }
  
  private static native int _closeOmaDrmInputStream(int paramInt);
  
  private static native long _fileSizeOmaDrmInputStream(int paramInt);
  
  private static native Object _getOmaDrmInfoOmaDrmInputStream(int paramInt);
  
  private static native int _initializeOmaDrmInputStream(int paramInt, FileDescriptor paramFileDescriptor, boolean paramBoolean);
  
  private static native int _readOmaDrmInputStream(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3);
}
