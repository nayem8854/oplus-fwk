package com.oplus.phoenix;

import android.os.SystemProperties;
import android.text.format.DateFormat;
import android.util.Log;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Phoenix {
  public static final String ANDROID_AMS_ENABLE_SCREEN = "ANDROID_AMS_ENABLE_SCREEN";
  
  public static final String ANDROID_AMS_READY = "ANDROID_AMS_READY";
  
  public static final String ANDROID_BOOT_COMPLETED = "ANDROID_BOOT_COMPLETED";
  
  public static final String ANDROID_PMS_DEXOPT_END = "ANDROID_PMS_DEXOPT_END";
  
  public static final String ANDROID_PMS_DEXOPT_PERSISTPKGS_END = "ANDROID_PMS_DEXOPT_PERSISTPKGS_END";
  
  public static final String ANDROID_PMS_DEXOPT_PERSISTPKGS_START = "ANDROID_PMS_DEXOPT_PERSISTPKGS_START";
  
  public static final String ANDROID_PMS_DEXOPT_START = "ANDROID_PMS_DEXOPT_START";
  
  public static final String ANDROID_PMS_INIT_START = "ANDROID_PMS_INIT_START";
  
  public static final String ANDROID_PMS_READY = "ANDROID_PMS_READY";
  
  public static final String ANDROID_PMS_SCAN_END = "ANDROID_PMS_SCAN_END";
  
  public static final String ANDROID_PMS_SCAN_START = "ANDROID_PMS_SCAN_START";
  
  public static final String ANDROID_SYSTEMSERVER_INIT_START = "ANDROID_SYSTEMSERVER_INIT_START";
  
  public static final String ANDROID_SYSTEMSERVER_READY = "ANDROID_SYSTEMSERVER_READY";
  
  public static final String ANDROID_ZYGOTE_GC_INIT_END = "ANDROID_ZYGOTE_GC_INIT_END";
  
  public static final String ANDROID_ZYGOTE_GC_INIT_START = "ANDROID_ZYGOTE_GC_INIT_START";
  
  public static final String ANDROID_ZYGOTE_INIT_END = "ANDROID_ZYGOTE_INIT_END";
  
  public static final String ANDROID_ZYGOTE_INIT_START = "ANDROID_ZYGOTE_INIT_START";
  
  public static final String ANDROID_ZYGOTE_PRELOAD_END = "ANDROID_ZYGOTE_PRELOAD_END";
  
  public static final String ANDROID_ZYGOTE_PRELOAD_START = "ANDROID_ZYGOTE_PRELOAD_START";
  
  private static final String BOOT_COMPLETED = "1";
  
  public static final String ERROR_SYSTEM_SERVER_WATCHDOG = "ERROR_SYSTEM_SERVER_WATCHDOG";
  
  private static final String PHOENIX_TAG = "[PHOENIX]";
  
  private static final String PROC_PHOENIX = "/proc/phoenix";
  
  private static final String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
  
  static {
    System.loadLibrary("phoenix_jni");
  }
  
  public static boolean isSwtHappened = false;
  
  private static String getFormatLocaltime(String paramString) {
    return DateFormat.format(paramString, System.currentTimeMillis()).toString();
  }
  
  public static boolean isBootCompleted() {
    return "1".equals(SystemProperties.get("sys.oppo.boot_completed"));
  }
  
  public static void setBooterror(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(" errno: ");
    stringBuilder.append(paramString);
    stringBuilder.append(", time: ");
    stringBuilder.append(getFormatLocaltime("yyyy-MM-dd HH:mm:ss"));
    Log.i("[PHOENIX]", stringBuilder.toString());
    native_set_booterror(paramString);
  }
  
  public static void setBootstage(String paramString) {
    if (isBootCompleted())
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(" stage: ");
    stringBuilder.append(paramString);
    Log.i("[PHOENIX]", stringBuilder.toString());
    native_set_bootstage(paramString);
  }
  
  private static String writeBootFromProc(String paramString1, String paramString2) {
    FileWriter fileWriter1 = null;
    String str = null;
    File file = new File(paramString1);
    paramString1 = str;
    FileWriter fileWriter2 = fileWriter1;
    try {
      FileWriter fileWriter5 = new FileWriter();
      paramString1 = str;
      fileWriter2 = fileWriter1;
      this(file);
      FileWriter fileWriter4 = fileWriter5;
      FileWriter fileWriter3 = fileWriter4;
      fileWriter2 = fileWriter4;
      fileWriter4.write(paramString2);
      fileWriter3 = fileWriter4;
      fileWriter2 = fileWriter4;
      fileWriter4.flush();
      fileWriter3 = fileWriter4;
      fileWriter2 = fileWriter4;
      fileWriter4.close();
      try {
        fileWriter4.close();
      } catch (IOException iOException) {
        Log.e("[PHOENIX]", "writeBootFromProc close the writer failed!!!", iOException);
      } 
    } catch (IOException iOException) {
      FileWriter fileWriter = fileWriter2;
      Log.e("[PHOENIX]", "writeBootFromProc write failed!!!", iOException);
      if (fileWriter2 != null)
        fileWriter2.close(); 
    } finally {}
    return null;
  }
  
  public static void updateProcOpbootfrom(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    if (paramBoolean1) {
      writeBootFromProc("/proc/opbootfrom", "ota");
    } else if (paramBoolean2) {
      writeBootFromProc("/proc/opbootfrom", "upgrade");
    } else if (paramBoolean3) {
      writeBootFromProc("/proc/opbootfrom", "first_boot");
    } else {
      writeBootFromProc("/proc/opbootfrom", "normal");
    } 
  }
  
  public static native void native_set_booterror(String paramString);
  
  public static native void native_set_bootstage(String paramString);
}
