package com.oplus.launcher.graphic;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.SystemProperties;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.WindowManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GaussianBlur {
  private static final boolean DEBUG;
  
  private static final float DEFAULT_BRIGHTNESS = 0.8F;
  
  private static final int DEFAULT_RADIUS = 25;
  
  private static final ExecutorService EXECUTOR;
  
  private static final int EXECUTOR_THREADS;
  
  public static final float MAX_BRIGHTNESS = 1.0F;
  
  public static final float MINIM_BRIGHTNESS = 0.0F;
  
  private static final String TAG = "GaussianBlur";
  
  private static GaussianBlur sGaussianBlur;
  
  public static int sScreenHeight;
  
  public static int sScreenWidth = -1;
  
  private Bitmap mBitmap;
  
  static {
    sScreenHeight = -1;
    DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
    int i = Runtime.getRuntime().availableProcessors();
    EXECUTOR = Executors.newFixedThreadPool(i);
    sGaussianBlur = null;
    System.loadLibrary("gaussgraphic");
  }
  
  private SparseArray<Bitmap> mBitmapMapMemoryCache = new SparseArray();
  
  private SparseArray<int[]> mInoutPixelMapMemoryCache = new SparseArray();
  
  private int[] mInoutPixels;
  
  public static GaussianBlur getInstance() {
    if (sGaussianBlur == null)
      sGaussianBlur = new GaussianBlur(); 
    return sGaussianBlur;
  }
  
  public static void setScreenWidth(Context paramContext) {
    if (sScreenWidth == -1 || sScreenHeight == -1) {
      int k;
      WindowManager windowManager = (WindowManager)paramContext.getSystemService("window");
      Display display = windowManager.getDefaultDisplay();
      DisplayMetrics displayMetrics = new DisplayMetrics();
      display.getRealMetrics(displayMetrics);
      int i = displayMetrics.widthPixels;
      int j = displayMetrics.heightPixels;
      if (i < j) {
        k = i;
      } else {
        k = j;
      } 
      sScreenWidth = k;
      if (i >= j)
        j = i; 
      sScreenHeight = j;
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GaussianBlur:setScreenWidth   getRotation() ");
        stringBuilder.append(display.getRotation());
        stringBuilder.append(", sScreenWidth = ");
        stringBuilder.append(sScreenWidth);
        Log.d("GaussianBlur", stringBuilder.toString());
      } 
    } 
  }
  
  public static Bitmap scaleBitmap(Bitmap paramBitmap) {
    return scaleBitmap(paramBitmap, 0.25F);
  }
  
  public static Bitmap scaleBitmap(Bitmap paramBitmap, float paramFloat) {
    Bitmap bitmap;
    Matrix matrix = null;
    if (paramBitmap != null) {
      matrix = new Matrix();
      matrix.postScale(paramFloat, paramFloat);
      int i = paramBitmap.getWidth() - sScreenWidth;
      int j = sScreenWidth;
      if (i < 0) {
        j = paramBitmap.getWidth();
        i = 0;
      } 
      bitmap = Bitmap.createBitmap(paramBitmap, i, 0, j, paramBitmap.getHeight(), matrix, true);
    } 
    if (bitmap != null && DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Gaussian:captureWallpaper bm.getWidth = ");
      stringBuilder.append(paramBitmap.getWidth());
      stringBuilder.append(", bmp.getWidth = ");
      stringBuilder.append(bitmap.getWidth());
      String str = stringBuilder.toString();
      Log.d("GaussianBlur", str);
    } 
    return bitmap;
  }
  
  public void setParameter(int paramInt1, int paramInt2, int paramInt3) {}
  
  public Bitmap captureScreen(Context paramContext, int paramInt1, int paramInt2, float paramFloat, int paramInt3, int paramInt4) {
    if (paramInt1 == -1 || paramInt2 == -1) {
      setScreenWidth(paramContext);
      paramInt1 = sScreenWidth;
      paramInt1 = sScreenHeight;
    } 
    return null;
  }
  
  public Bitmap generateGaussianWallpaper(Context paramContext, float paramFloat1, int paramInt, float paramFloat2) {
    int i = Settings.System.getInt(paramContext.getContentResolver(), "LAYER_WALLPAPER", -1);
    Bitmap bitmap = captureScreen(paramContext, -1, -1, paramFloat1, i, i);
    return generateGaussianBitmap(bitmap, paramInt, paramFloat2, true);
  }
  
  public Bitmap generateGaussianScreenshot(Context paramContext, float paramFloat1, int paramInt, float paramFloat2) {
    Bitmap bitmap = captureScreen(paramContext, -1, -1, paramFloat1, -1, -1);
    return generateGaussianBitmap(bitmap, paramInt, paramFloat2, true);
  }
  
  public Bitmap generateGaussianBitmap(Bitmap paramBitmap, boolean paramBoolean) {
    return generateGaussianBitmap(paramBitmap, 25, 0.8F, paramBoolean);
  }
  
  public Bitmap generateGaussianBitmap(Bitmap paramBitmap, float paramFloat, boolean paramBoolean) {
    return generateGaussianBitmap(paramBitmap, 25, paramFloat, paramBoolean);
  }
  
  public Bitmap generateGaussianBitmap(Bitmap paramBitmap, int paramInt, float paramFloat, boolean paramBoolean) {
    return generateGaussianBitmap(paramBitmap, paramInt, paramFloat, false, paramBoolean);
  }
  
  public Bitmap generateGaussianBitmapResuse(Bitmap paramBitmap, int paramInt, float paramFloat, boolean paramBoolean) {
    if (paramBitmap == null || paramBitmap.isRecycled()) {
      Log.w("GaussianBlur", "GaussianBlur:generateGaussianBitmapResuse  bmp is null or isRecycled!");
      return null;
    } 
    if (DEBUG)
      Log.v("GaussianBlur", "GaussianBlur:generateGaussianBitmapResuse  Enter"); 
    boolean bool = paramBitmap.hasAlpha();
    int i = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    if (this.mInoutPixelMapMemoryCache == null)
      this.mInoutPixelMapMemoryCache = new SparseArray(); 
    int[] arrayOfInt = (int[])this.mInoutPixelMapMemoryCache.get(j * i);
    if (arrayOfInt != null) {
      this.mInoutPixels = arrayOfInt;
    } else {
      this.mInoutPixels = arrayOfInt = new int[i * j];
      this.mInoutPixelMapMemoryCache.put(j * i, arrayOfInt);
    } 
    if (this.mBitmapMapMemoryCache == null)
      this.mBitmapMapMemoryCache = new SparseArray(); 
    Bitmap bitmap = (Bitmap)this.mBitmapMapMemoryCache.get(j);
    if (bitmap != null && bitmap.getWidth() == i && bitmap.getHeight() == j) {
      this.mBitmap = bitmap;
    } else {
      this.mBitmap = bitmap = Bitmap.createBitmap(i, j, Bitmap.Config.ARGB_8888);
      this.mBitmapMapMemoryCache.put(j, bitmap);
    } 
    paramBitmap.getPixels(this.mInoutPixels, 0, i, 0, 0, i, j);
    blurIteration_native(this.mInoutPixels, i, j, paramInt, bool);
    if (paramFloat > 0.0F && paramFloat < 1.0F)
      blurBrightness_native(this.mInoutPixels, j, i, paramFloat); 
    this.mBitmap.setPixels(this.mInoutPixels, 0, i, 0, 0, i, j);
    if (paramBoolean)
      paramBitmap.recycle(); 
    if (DEBUG)
      Log.d("GaussianBlur", "GaussianBlur:generateGaussianBitmap  generate Complete"); 
    return this.mBitmap;
  }
  
  public Bitmap generateGaussianBitmap(Bitmap paramBitmap, int paramInt, float paramFloat, boolean paramBoolean1, boolean paramBoolean2) {
    Bitmap bitmap1 = paramBitmap;
    if (bitmap1 == null || paramBitmap.isRecycled()) {
      Log.w("GaussianBlur", "GaussianBlur:generateGaussianBitmap  bmp is null or isRecycled!");
      return null;
    } 
    if (DEBUG)
      Log.v("GaussianBlur", "GaussianBlur:generateGaussianBitmap  Enter"); 
    if (!paramBitmap.hasAlpha())
      paramBoolean1 = false; 
    int i = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    int[] arrayOfInt = new int[i * j];
    Bitmap bitmap2 = Bitmap.createBitmap(i, j, Bitmap.Config.ARGB_8888);
    Bitmap bitmap3 = bitmap1;
    if (bitmap1 != null) {
      bitmap3 = bitmap1;
      if (paramBitmap.getConfig() == Bitmap.Config.HARDWARE)
        bitmap3 = bitmap1.copy(Bitmap.Config.ARGB_8888, true); 
    } 
    bitmap3.getPixels(arrayOfInt, 0, i, 0, 0, i, j);
    blurIteration_native(arrayOfInt, i, j, paramInt, paramBoolean1);
    if (DEBUG)
      Log.d("GaussianBlur", "GaussianBlur:generateGaussianBitmap  generate brightness"); 
    if (paramFloat > 0.0F && paramFloat < 1.0F)
      blurBrightness_native(arrayOfInt, j, i, paramFloat); 
    bitmap2.setPixels(arrayOfInt, 0, i, 0, 0, i, j);
    if (paramBoolean2)
      bitmap3.recycle(); 
    if (DEBUG)
      Log.d("GaussianBlur", "GaussianBlur:generateGaussianBitmap  generate Complete"); 
    return bitmap2;
  }
  
  public Bitmap generateGaussianBitmapTask(Bitmap paramBitmap, int paramInt, float paramFloat, boolean paramBoolean1, boolean paramBoolean2) {
    if (paramBitmap == null || paramBitmap.isRecycled()) {
      Log.w("GaussianBlur", "GaussianBlur:generateGaussianBitmapTask  bmp is null or isRecycled!");
      return null;
    } 
    if (DEBUG)
      Log.v("GaussianBlur", "GaussianBlur:generateGaussianBitmapTask  Enter"); 
    if (!paramBitmap.hasAlpha())
      paramBoolean1 = false; 
    int i = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    int[] arrayOfInt = new int[i * j];
    Bitmap bitmap = Bitmap.createBitmap(i, j, Bitmap.Config.ARGB_8888);
    paramBitmap.getPixels(arrayOfInt, 0, i, 0, 0, i, j);
    blurIteration_nativeTask(arrayOfInt, i, j, paramInt, paramBoolean1);
    if (DEBUG)
      Log.d("GaussianBlur", "GaussianBlur:generateGaussianBitmap  generate brightness"); 
    if (paramFloat > 0.0F && paramFloat < 1.0F)
      blurBrightness_native(arrayOfInt, j, i, paramFloat); 
    bitmap.setPixels(arrayOfInt, 0, i, 0, 0, i, j);
    if (paramBoolean2)
      paramBitmap.recycle(); 
    if (DEBUG)
      Log.d("GaussianBlur", "GaussianBlur:generateGaussianBitmapTask  generate Complete"); 
    return bitmap;
  }
  
  public void releaseResource() {
    Bitmap bitmap = this.mBitmap;
    if (bitmap != null && bitmap.isRecycled())
      this.mBitmap.recycle(); 
    if (this.mInoutPixels != null)
      this.mInoutPixels = null; 
    SparseArray<Bitmap> sparseArray1 = this.mBitmapMapMemoryCache;
    if (sparseArray1 != null) {
      int i = sparseArray1.size();
      for (byte b = 0; b < i; b++) {
        Bitmap bitmap1 = (Bitmap)this.mBitmapMapMemoryCache.valueAt(b);
        if (bitmap1 != null && !bitmap1.isRecycled())
          bitmap1.recycle(); 
      } 
      this.mBitmapMapMemoryCache.clear();
      this.mBitmapMapMemoryCache = null;
    } 
    SparseArray<int[]> sparseArray = this.mInoutPixelMapMemoryCache;
    if (sparseArray != null) {
      sparseArray.clear();
      this.mInoutPixelMapMemoryCache = null;
    } 
  }
  
  private class BlurTask implements Callable<Void> {
    private final int mCore;
    
    private final int mCores;
    
    private final boolean mHasAlpha;
    
    private final int mHeight;
    
    private final int[] mInout;
    
    private final int mRadius;
    
    private final int mStep;
    
    private final int mWidth;
    
    final GaussianBlur this$0;
    
    public BlurTask(int[] param1ArrayOfint, int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6, boolean param1Boolean) {
      this.mInout = param1ArrayOfint;
      this.mWidth = param1Int1;
      this.mHeight = param1Int2;
      this.mRadius = param1Int3;
      this.mCores = param1Int4;
      this.mCore = param1Int5;
      this.mStep = param1Int6;
      this.mHasAlpha = param1Boolean;
    }
    
    public Void call() throws Exception {
      if (this.mHasAlpha) {
        GaussianBlur.this.blurIterationAlpha_native(this.mInout, this.mWidth, this.mHeight, this.mRadius, this.mCores, this.mCore, this.mStep);
      } else {
        GaussianBlur.this.blurIteration_native(this.mInout, this.mWidth, this.mHeight, this.mRadius, this.mCores, this.mCore, this.mStep);
      } 
      return null;
    }
  }
  
  public void blurIteration_native(int[] paramArrayOfint, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) {
    if (paramBoolean) {
      blurIterationAlpha_native(paramArrayOfint, paramInt1, paramInt2, paramInt3, 1, 0, 1);
      blurIterationAlpha_native(paramArrayOfint, paramInt1, paramInt2, paramInt3, 1, 0, 2);
    } else {
      blurIteration_native(paramArrayOfint, paramInt1, paramInt2, paramInt3, 1, 0, 1);
      blurIteration_native(paramArrayOfint, paramInt1, paramInt2, paramInt3, 1, 0, 2);
    } 
  }
  
  public void blurIteration_nativeTask(int[] paramArrayOfint, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) {
    int i = EXECUTOR_THREADS;
    ArrayList<BlurTask> arrayList1 = new ArrayList(i);
    ArrayList<BlurTask> arrayList2 = new ArrayList(i);
    for (byte b = 0; b < i; b++) {
      arrayList1.add(new BlurTask(paramArrayOfint, paramInt1, paramInt2, paramInt3, i, b, 1, paramBoolean));
      arrayList2.add(new BlurTask(paramArrayOfint, paramInt1, paramInt2, paramInt3, i, b, 2, paramBoolean));
    } 
    try {
      EXECUTOR.invokeAll((Collection)arrayList1);
      try {
        EXECUTOR.invokeAll((Collection)arrayList2);
        return;
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GaussianBlur:blurIteration_nativeTask  e2 = ");
        stringBuilder.append(exception);
        Log.e("GaussianBlur", stringBuilder.toString());
        return;
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("GaussianBlur:blurIteration_nativeTask  e1 = ");
      stringBuilder.append(exception);
      Log.e("GaussianBlur", stringBuilder.toString());
      return;
    } 
  }
  
  public native void blurBrightness_native(int[] paramArrayOfint, int paramInt1, int paramInt2, float paramFloat);
  
  public native void blurIterationAlpha_native(int[] paramArrayOfint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6);
  
  public native void blurIteration_native(int[] paramArrayOfint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6);
}
