package com.oplus.reflect;

import android.util.Log;
import java.lang.reflect.Field;

public class RefBoolean {
  private static final String TAG = "RefBoolean";
  
  private Field field;
  
  public RefBoolean(Class<?> paramClass, Field paramField) throws NoSuchFieldException {
    Field field = paramClass.getDeclaredField(paramField.getName());
    field.setAccessible(true);
  }
  
  public boolean get(Object paramObject) {
    try {
      return this.field.getBoolean(paramObject);
    } catch (Exception exception) {
      Log.e("RefBoolean", exception.toString());
      return false;
    } 
  }
  
  public void set(Object paramObject, boolean paramBoolean) {
    try {
      this.field.setBoolean(paramObject, paramBoolean);
    } catch (Exception exception) {
      Log.e("RefBoolean", exception.toString());
    } 
  }
}
