package com.oplus.reflect;

import android.util.Log;
import java.lang.reflect.Field;

public class RefInt {
  private static final String TAG = "RefInt";
  
  private Field field;
  
  public RefInt(Class paramClass, Field paramField) throws NoSuchFieldException {
    Field field = paramClass.getDeclaredField(paramField.getName());
    field.setAccessible(true);
  }
  
  public int get(Object paramObject) {
    try {
      return this.field.getInt(paramObject);
    } catch (Exception exception) {
      Log.e("RefInt", exception.toString());
      return 0;
    } 
  }
  
  public void set(Object paramObject, int paramInt) {
    try {
      this.field.setInt(paramObject, paramInt);
    } catch (Exception exception) {
      Log.e("RefInt", exception.toString());
    } 
  }
}
