package com.oplus.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class RefStaticMethod<T> {
  private Method method;
  
  public RefStaticMethod(Class<?> paramClass, Field paramField) throws NoSuchMethodException {
    Method method;
    if (paramField.isAnnotationPresent((Class)MethodParams.class)) {
      Class[] arrayOfClass = ((MethodParams)paramField.<MethodParams>getAnnotation(MethodParams.class)).value();
      for (byte b = 0; b < arrayOfClass.length; b++)
        Class clazz = arrayOfClass[b]; 
      this.method = method = paramClass.getDeclaredMethod(paramField.getName(), arrayOfClass);
      method.setAccessible(true);
    } else if (paramField.isAnnotationPresent((Class)MethodReflectParams.class)) {
      boolean bool = false;
      String[] arrayOfString = ((MethodReflectParams)paramField.<MethodReflectParams>getAnnotation(MethodReflectParams.class)).value();
      Class[] arrayOfClass1 = new Class[arrayOfString.length];
      Class[] arrayOfClass2 = new Class[arrayOfString.length];
      for (byte b = 0; b < arrayOfString.length; b++) {
        Class<?> clazz1 = getProtoType(arrayOfString[b]);
        Class<?> clazz2 = clazz1;
        if (clazz1 == null)
          try {
            clazz2 = Class.forName(arrayOfString[b]);
          } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
            clazz2 = clazz1;
          }  
        arrayOfClass1[b] = clazz2;
        if ("java.util.HashSet".equals(arrayOfString[b])) {
          bool = true;
          clazz1 = clazz2;
          try {
            Class<?> clazz = Class.forName("android.util.ArraySet");
          } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
          } 
          if (clazz1 != null) {
            arrayOfClass2[b] = clazz1;
          } else {
            arrayOfClass2[b] = clazz2;
          } 
        } else {
          arrayOfClass2[b] = clazz2;
        } 
      } 
      try {
        this.method = method.getDeclaredMethod(paramField.getName(), arrayOfClass1);
      } catch (Exception exception) {
        exception.printStackTrace();
        if (bool)
          this.method = method.getDeclaredMethod(paramField.getName(), arrayOfClass2); 
      } 
      this.method.setAccessible(true);
    } else {
      for (Method method1 : method.getDeclaredMethods()) {
        if (method1.getName().equals(paramField.getName())) {
          this.method = method1;
          method1.setAccessible(true);
          break;
        } 
      } 
    } 
    if (this.method != null)
      return; 
    throw new NoSuchMethodException(paramField.getName());
  }
  
  static Class<?> getProtoType(String paramString) {
    if (paramString.equals("int"))
      return int.class; 
    if (paramString.equals("long"))
      return long.class; 
    if (paramString.equals("boolean"))
      return boolean.class; 
    if (paramString.equals("byte"))
      return byte.class; 
    if (paramString.equals("short"))
      return short.class; 
    if (paramString.equals("char"))
      return char.class; 
    if (paramString.equals("float"))
      return float.class; 
    if (paramString.equals("double"))
      return double.class; 
    if (paramString.equals("void"))
      return void.class; 
    return null;
  }
  
  public T call(Object... paramVarArgs) {
    Exception exception = null;
    try {
      object = this.method.invoke(null, paramVarArgs);
    } catch (Exception object) {
      object.printStackTrace();
      object = exception;
    } 
    return (T)object;
  }
  
  public T callWithException(Object... paramVarArgs) throws Throwable {
    try {
      return (T)this.method.invoke(null, paramVarArgs);
    } catch (InvocationTargetException invocationTargetException) {
      if (invocationTargetException.getCause() != null)
        throw invocationTargetException.getCause(); 
      throw invocationTargetException;
    } 
  }
}
