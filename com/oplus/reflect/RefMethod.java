package com.oplus.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class RefMethod<T> {
  private Method method;
  
  public RefMethod(Class<?> paramClass, Field paramField) throws NoSuchMethodException {
    Method method;
    if (paramField.isAnnotationPresent((Class)MethodParams.class)) {
      Class[] arrayOfClass = ((MethodParams)paramField.<MethodParams>getAnnotation(MethodParams.class)).value();
      this.method = method = paramClass.getDeclaredMethod(paramField.getName(), arrayOfClass);
      method.setAccessible(true);
    } else if (paramField.isAnnotationPresent((Class)MethodReflectParams.class)) {
      String[] arrayOfString = ((MethodReflectParams)paramField.<MethodReflectParams>getAnnotation(MethodReflectParams.class)).value();
      Class[] arrayOfClass = new Class[arrayOfString.length];
      for (byte b = 0; b < arrayOfString.length; b++) {
        Class<?> clazz2 = RefStaticMethod.getProtoType(arrayOfString[b]);
        Class<?> clazz1 = clazz2;
        if (clazz2 == null)
          try {
            clazz1 = Class.forName(arrayOfString[b]);
          } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
            clazz1 = clazz2;
          }  
        arrayOfClass[b] = clazz1;
      } 
      this.method = method = method.getDeclaredMethod(paramField.getName(), arrayOfClass);
      method.setAccessible(true);
    } else {
      for (Method method1 : method.getDeclaredMethods()) {
        if (method1.getName().equals(paramField.getName())) {
          this.method = method1;
          method1.setAccessible(true);
          break;
        } 
      } 
    } 
    if (this.method != null)
      return; 
    throw new NoSuchMethodException(paramField.getName());
  }
  
  public T call(Object paramObject, Object... paramVarArgs) {
    try {
      return (T)this.method.invoke(paramObject, paramVarArgs);
    } catch (InvocationTargetException invocationTargetException) {
    
    } finally {
      paramObject = null;
    } 
    return null;
  }
  
  public T callWithException(Object paramObject, Object... paramVarArgs) throws Throwable {
    try {
      return (T)this.method.invoke(paramObject, paramVarArgs);
    } catch (InvocationTargetException invocationTargetException) {
      if (invocationTargetException.getCause() != null)
        throw invocationTargetException.getCause(); 
      throw invocationTargetException;
    } 
  }
  
  public Class<?>[] paramList() {
    return this.method.getParameterTypes();
  }
}
