package com.oplus.reflect;

import android.util.Log;
import java.lang.reflect.Field;

public class RefFloat {
  private static final String TAG = "RefFloat";
  
  private Field field;
  
  public RefFloat(Class paramClass, Field paramField) throws NoSuchFieldException {
    Field field = paramClass.getDeclaredField(paramField.getName());
    field.setAccessible(true);
  }
  
  public float get(Object paramObject) {
    try {
      return this.field.getFloat(paramObject);
    } catch (Exception exception) {
      Log.e("RefFloat", exception.toString());
      return 0.0F;
    } 
  }
  
  public void set(Object paramObject, float paramFloat) {
    try {
      this.field.setFloat(paramObject, paramFloat);
    } catch (Exception exception) {
      Log.e("RefFloat", exception.toString());
    } 
  }
}
