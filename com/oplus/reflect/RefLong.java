package com.oplus.reflect;

import android.util.Log;
import java.lang.reflect.Field;

public class RefLong {
  private static final String TAG = "RefLong";
  
  private Field field;
  
  public RefLong(Class paramClass, Field paramField) throws NoSuchFieldException {
    Field field = paramClass.getDeclaredField(paramField.getName());
    field.setAccessible(true);
  }
  
  public long get(Object paramObject) {
    try {
      return this.field.getLong(paramObject);
    } catch (Exception exception) {
      Log.e("RefLong", exception.toString());
      return 0L;
    } 
  }
  
  public void set(Object paramObject, long paramLong) {
    try {
      this.field.setLong(paramObject, paramLong);
    } catch (Exception exception) {
      Log.e("RefLong", exception.toString());
    } 
  }
}
