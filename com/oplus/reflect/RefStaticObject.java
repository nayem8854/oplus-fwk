package com.oplus.reflect;

import android.util.Log;
import java.lang.reflect.Field;

public class RefStaticObject<T> {
  private static final String TAG = "RefStaticObject";
  
  private Field field;
  
  public RefStaticObject(Class<?> paramClass, Field paramField) throws NoSuchFieldException {
    Field field = paramClass.getDeclaredField(paramField.getName());
    field.setAccessible(true);
  }
  
  public Class<?> type() {
    return this.field.getType();
  }
  
  public T get() {
    Object object = null;
    try {
      Object object1 = this.field.get(null);
    } catch (Exception exception) {
      Log.e("RefStaticObject", exception.toString());
    } 
    return (T)object;
  }
  
  public void set(T paramT) {
    try {
      this.field.set(null, paramT);
    } catch (Exception exception) {
      Log.e("RefStaticObject", exception.toString());
    } 
  }
}
