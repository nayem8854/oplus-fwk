package com.oplus.reflect;

import android.util.Log;
import java.lang.reflect.Field;

public class RefDouble {
  private static final String TAG = "RefDouble";
  
  private Field field;
  
  public RefDouble(Class paramClass, Field paramField) throws NoSuchFieldException {
    Field field = paramClass.getDeclaredField(paramField.getName());
    field.setAccessible(true);
  }
  
  public double get(Object paramObject) {
    try {
      return this.field.getDouble(paramObject);
    } catch (Exception exception) {
      Log.e("RefDouble", exception.toString());
      return 0.0D;
    } 
  }
  
  public void set(Object paramObject, double paramDouble) {
    try {
      this.field.setDouble(paramObject, paramDouble);
    } catch (Exception exception) {
      Log.e("RefDouble", exception.toString());
    } 
  }
}
