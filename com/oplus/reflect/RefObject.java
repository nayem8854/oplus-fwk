package com.oplus.reflect;

import android.util.Log;
import java.lang.reflect.Field;

public class RefObject<T> {
  private static final String TAG = "RefObject";
  
  private Field field;
  
  public RefObject(Class<?> paramClass, Field paramField) throws NoSuchFieldException {
    Field field = paramClass.getDeclaredField(paramField.getName());
    field.setAccessible(true);
  }
  
  public T get(Object paramObject) {
    try {
      return (T)this.field.get(paramObject);
    } catch (Exception exception) {
      Log.e("RefObject", exception.toString());
      return null;
    } 
  }
  
  public void set(Object paramObject, T paramT) {
    try {
      this.field.set(paramObject, paramT);
    } catch (Exception exception) {
      Log.e("RefObject", exception.toString());
    } 
  }
}
