package com.oplus.reflect;

import android.util.Log;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

public class RefConstructor<T> {
  private static final String TAG = "RefConstructor";
  
  private Constructor<?> ctor;
  
  public RefConstructor(Class<?> paramClass, Field paramField) throws NoSuchMethodException {
    Class[] arrayOfClass;
    if (paramField.isAnnotationPresent((Class)MethodParams.class)) {
      arrayOfClass = ((MethodParams)paramField.<MethodParams>getAnnotation(MethodParams.class)).value();
      this.ctor = paramClass.getDeclaredConstructor(arrayOfClass);
    } else if (arrayOfClass.isAnnotationPresent((Class)MethodReflectParams.class)) {
      String[] arrayOfString = ((MethodReflectParams)arrayOfClass.<MethodReflectParams>getAnnotation(MethodReflectParams.class)).value();
      Class[] arrayOfClass1 = new Class[arrayOfString.length];
      byte b = 0;
      while (b < arrayOfString.length) {
        try {
          arrayOfClass1[b] = Class.forName(arrayOfString[b]);
          b++;
        } catch (Exception exception) {
          exception.printStackTrace();
        } 
      } 
      this.ctor = paramClass.getDeclaredConstructor(arrayOfClass1);
    } else {
      this.ctor = paramClass.getDeclaredConstructor(new Class[0]);
    } 
    Constructor<?> constructor = this.ctor;
    if (constructor != null && !constructor.isAccessible())
      this.ctor.setAccessible(true); 
  }
  
  public T newInstance() {
    try {
      return (T)this.ctor.newInstance(new Object[0]);
    } catch (Exception exception) {
      Log.e("RefConstructor", exception.toString());
      return null;
    } 
  }
  
  public T newInstance(Object... paramVarArgs) {
    try {
      return (T)this.ctor.newInstance(paramVarArgs);
    } catch (Exception exception) {
      Log.e("RefConstructor", exception.toString());
      return null;
    } 
  }
}
