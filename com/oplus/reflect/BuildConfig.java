package com.oplus.reflect;

public final class BuildConfig {
  @Deprecated
  public static final String APPLICATION_ID = "com.oplus.reflect";
  
  public static final String BUILD_TYPE = "debug";
  
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  
  public static final String FLAVOR = "";
  
  public static final String LIBRARY_PACKAGE_NAME = "com.oplus.reflect";
  
  public static final int VERSION_CODE = 1;
  
  public static final String VERSION_NAME = "1.0";
}
