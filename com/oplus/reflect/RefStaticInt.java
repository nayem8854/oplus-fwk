package com.oplus.reflect;

import android.util.Log;
import java.lang.reflect.Field;

public class RefStaticInt {
  private static final String TAG = "RefStaticInt";
  
  private Field field;
  
  public RefStaticInt(Class<?> paramClass, Field paramField) throws NoSuchFieldException {
    Field field = paramClass.getDeclaredField(paramField.getName());
    field.setAccessible(true);
  }
  
  public int get() {
    try {
      return this.field.getInt(null);
    } catch (Exception exception) {
      Log.e("RefStaticInt", exception.toString());
      return 0;
    } 
  }
  
  public void set(int paramInt) {
    try {
      this.field.setInt(null, paramInt);
    } catch (Exception exception) {
      Log.e("RefStaticInt", exception.toString());
    } 
  }
}
