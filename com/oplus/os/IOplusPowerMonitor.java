package com.oplus.os;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.oplus.app.OplusAlarmInfo;
import com.oplus.app.OplusWakeLockInfo;
import java.util.List;

public interface IOplusPowerMonitor extends IInterface {
  List<OplusAlarmInfo> getAlarmWakeUpInfo(long paramLong1, long paramLong2) throws RemoteException;
  
  String getOppoRpmMasterStatsFilePath() throws RemoteException;
  
  String getOppoRpmStatsFilePath() throws RemoteException;
  
  List<OplusWakeLockInfo> getWakeLockInfo(long paramLong1, long paramLong2) throws RemoteException;
  
  void recordAlarmWakeupEvent() throws RemoteException;
  
  void recordAppWakeupEvent(int paramInt, String paramString) throws RemoteException;
  
  void recordAppWakeupInfoEvent(OplusAlarmInfo paramOplusAlarmInfo) throws RemoteException;
  
  void recordWakeLockAcquireEvent(OplusWakeLockInfo paramOplusWakeLockInfo) throws RemoteException;
  
  void recordWakeLockReleaseEvent(OplusWakeLockInfo paramOplusWakeLockInfo) throws RemoteException;
  
  void resetWakeupEventRecords() throws RemoteException;
  
  void scheduleOppoRpmUpdate(boolean paramBoolean) throws RemoteException;
  
  class Default implements IOplusPowerMonitor {
    public void recordAlarmWakeupEvent() throws RemoteException {}
    
    public void recordAppWakeupEvent(int param1Int, String param1String) throws RemoteException {}
    
    public void resetWakeupEventRecords() throws RemoteException {}
    
    public String getOppoRpmStatsFilePath() throws RemoteException {
      return null;
    }
    
    public String getOppoRpmMasterStatsFilePath() throws RemoteException {
      return null;
    }
    
    public void scheduleOppoRpmUpdate(boolean param1Boolean) throws RemoteException {}
    
    public void recordAppWakeupInfoEvent(OplusAlarmInfo param1OplusAlarmInfo) throws RemoteException {}
    
    public List<OplusAlarmInfo> getAlarmWakeUpInfo(long param1Long1, long param1Long2) throws RemoteException {
      return null;
    }
    
    public void recordWakeLockAcquireEvent(OplusWakeLockInfo param1OplusWakeLockInfo) throws RemoteException {}
    
    public void recordWakeLockReleaseEvent(OplusWakeLockInfo param1OplusWakeLockInfo) throws RemoteException {}
    
    public List<OplusWakeLockInfo> getWakeLockInfo(long param1Long1, long param1Long2) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusPowerMonitor {
    private static final String DESCRIPTOR = "com.oplus.os.IOplusPowerMonitor";
    
    static final int TRANSACTION_getAlarmWakeUpInfo = 8;
    
    static final int TRANSACTION_getOppoRpmMasterStatsFilePath = 5;
    
    static final int TRANSACTION_getOppoRpmStatsFilePath = 4;
    
    static final int TRANSACTION_getWakeLockInfo = 11;
    
    static final int TRANSACTION_recordAlarmWakeupEvent = 1;
    
    static final int TRANSACTION_recordAppWakeupEvent = 2;
    
    static final int TRANSACTION_recordAppWakeupInfoEvent = 7;
    
    static final int TRANSACTION_recordWakeLockAcquireEvent = 9;
    
    static final int TRANSACTION_recordWakeLockReleaseEvent = 10;
    
    static final int TRANSACTION_resetWakeupEventRecords = 3;
    
    static final int TRANSACTION_scheduleOppoRpmUpdate = 6;
    
    public Stub() {
      attachInterface(this, "com.oplus.os.IOplusPowerMonitor");
    }
    
    public static IOplusPowerMonitor asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.os.IOplusPowerMonitor");
      if (iInterface != null && iInterface instanceof IOplusPowerMonitor)
        return (IOplusPowerMonitor)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 11:
          return "getWakeLockInfo";
        case 10:
          return "recordWakeLockReleaseEvent";
        case 9:
          return "recordWakeLockAcquireEvent";
        case 8:
          return "getAlarmWakeUpInfo";
        case 7:
          return "recordAppWakeupInfoEvent";
        case 6:
          return "scheduleOppoRpmUpdate";
        case 5:
          return "getOppoRpmMasterStatsFilePath";
        case 4:
          return "getOppoRpmStatsFilePath";
        case 3:
          return "resetWakeupEventRecords";
        case 2:
          return "recordAppWakeupEvent";
        case 1:
          break;
      } 
      return "recordAlarmWakeupEvent";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        List<OplusWakeLockInfo> list;
        String str;
        long l1, l2;
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 11:
            param1Parcel1.enforceInterface("com.oplus.os.IOplusPowerMonitor");
            l1 = param1Parcel1.readLong();
            l2 = param1Parcel1.readLong();
            list = getWakeLockInfo(l1, l2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 10:
            list.enforceInterface("com.oplus.os.IOplusPowerMonitor");
            if (list.readInt() != 0) {
              OplusWakeLockInfo oplusWakeLockInfo = (OplusWakeLockInfo)OplusWakeLockInfo.CREATOR.createFromParcel((Parcel)list);
            } else {
              list = null;
            } 
            recordWakeLockReleaseEvent((OplusWakeLockInfo)list);
            return true;
          case 9:
            list.enforceInterface("com.oplus.os.IOplusPowerMonitor");
            if (list.readInt() != 0) {
              OplusWakeLockInfo oplusWakeLockInfo = (OplusWakeLockInfo)OplusWakeLockInfo.CREATOR.createFromParcel((Parcel)list);
            } else {
              list = null;
            } 
            recordWakeLockAcquireEvent((OplusWakeLockInfo)list);
            return true;
          case 8:
            list.enforceInterface("com.oplus.os.IOplusPowerMonitor");
            l1 = list.readLong();
            l2 = list.readLong();
            list = (List)getAlarmWakeUpInfo(l1, l2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 7:
            list.enforceInterface("com.oplus.os.IOplusPowerMonitor");
            if (list.readInt() != 0) {
              OplusAlarmInfo oplusAlarmInfo = (OplusAlarmInfo)OplusAlarmInfo.CREATOR.createFromParcel((Parcel)list);
            } else {
              list = null;
            } 
            recordAppWakeupInfoEvent((OplusAlarmInfo)list);
            return true;
          case 6:
            list.enforceInterface("com.oplus.os.IOplusPowerMonitor");
            if (list.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            scheduleOppoRpmUpdate(bool);
            return true;
          case 5:
            list.enforceInterface("com.oplus.os.IOplusPowerMonitor");
            str = getOppoRpmMasterStatsFilePath();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str);
            return true;
          case 4:
            str.enforceInterface("com.oplus.os.IOplusPowerMonitor");
            str = getOppoRpmStatsFilePath();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str);
            return true;
          case 3:
            str.enforceInterface("com.oplus.os.IOplusPowerMonitor");
            resetWakeupEventRecords();
            return true;
          case 2:
            str.enforceInterface("com.oplus.os.IOplusPowerMonitor");
            param1Int1 = str.readInt();
            str = str.readString();
            recordAppWakeupEvent(param1Int1, str);
            return true;
          case 1:
            break;
        } 
        str.enforceInterface("com.oplus.os.IOplusPowerMonitor");
        recordAlarmWakeupEvent();
        return true;
      } 
      param1Parcel2.writeString("com.oplus.os.IOplusPowerMonitor");
      return true;
    }
    
    private static class Proxy implements IOplusPowerMonitor {
      public static IOplusPowerMonitor sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.os.IOplusPowerMonitor";
      }
      
      public void recordAlarmWakeupEvent() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.os.IOplusPowerMonitor");
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusPowerMonitor.Stub.getDefaultImpl() != null) {
            IOplusPowerMonitor.Stub.getDefaultImpl().recordAlarmWakeupEvent();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void recordAppWakeupEvent(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.os.IOplusPowerMonitor");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IOplusPowerMonitor.Stub.getDefaultImpl() != null) {
            IOplusPowerMonitor.Stub.getDefaultImpl().recordAppWakeupEvent(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void resetWakeupEventRecords() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.os.IOplusPowerMonitor");
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IOplusPowerMonitor.Stub.getDefaultImpl() != null) {
            IOplusPowerMonitor.Stub.getDefaultImpl().resetWakeupEventRecords();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public String getOppoRpmStatsFilePath() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.os.IOplusPowerMonitor");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOplusPowerMonitor.Stub.getDefaultImpl() != null)
            return IOplusPowerMonitor.Stub.getDefaultImpl().getOppoRpmStatsFilePath(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getOppoRpmMasterStatsFilePath() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.os.IOplusPowerMonitor");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOplusPowerMonitor.Stub.getDefaultImpl() != null)
            return IOplusPowerMonitor.Stub.getDefaultImpl().getOppoRpmMasterStatsFilePath(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void scheduleOppoRpmUpdate(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.oplus.os.IOplusPowerMonitor");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(6, parcel, null, 1);
          if (!bool1 && IOplusPowerMonitor.Stub.getDefaultImpl() != null) {
            IOplusPowerMonitor.Stub.getDefaultImpl().scheduleOppoRpmUpdate(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void recordAppWakeupInfoEvent(OplusAlarmInfo param2OplusAlarmInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.os.IOplusPowerMonitor");
          if (param2OplusAlarmInfo != null) {
            parcel.writeInt(1);
            param2OplusAlarmInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IOplusPowerMonitor.Stub.getDefaultImpl() != null) {
            IOplusPowerMonitor.Stub.getDefaultImpl().recordAppWakeupInfoEvent(param2OplusAlarmInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public List<OplusAlarmInfo> getAlarmWakeUpInfo(long param2Long1, long param2Long2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.os.IOplusPowerMonitor");
          parcel1.writeLong(param2Long1);
          parcel1.writeLong(param2Long2);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IOplusPowerMonitor.Stub.getDefaultImpl() != null)
            return IOplusPowerMonitor.Stub.getDefaultImpl().getAlarmWakeUpInfo(param2Long1, param2Long2); 
          parcel2.readException();
          return parcel2.createTypedArrayList(OplusAlarmInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void recordWakeLockAcquireEvent(OplusWakeLockInfo param2OplusWakeLockInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.os.IOplusPowerMonitor");
          if (param2OplusWakeLockInfo != null) {
            parcel.writeInt(1);
            param2OplusWakeLockInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IOplusPowerMonitor.Stub.getDefaultImpl() != null) {
            IOplusPowerMonitor.Stub.getDefaultImpl().recordWakeLockAcquireEvent(param2OplusWakeLockInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void recordWakeLockReleaseEvent(OplusWakeLockInfo param2OplusWakeLockInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.os.IOplusPowerMonitor");
          if (param2OplusWakeLockInfo != null) {
            parcel.writeInt(1);
            param2OplusWakeLockInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IOplusPowerMonitor.Stub.getDefaultImpl() != null) {
            IOplusPowerMonitor.Stub.getDefaultImpl().recordWakeLockReleaseEvent(param2OplusWakeLockInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public List<OplusWakeLockInfo> getWakeLockInfo(long param2Long1, long param2Long2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.os.IOplusPowerMonitor");
          parcel1.writeLong(param2Long1);
          parcel1.writeLong(param2Long2);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IOplusPowerMonitor.Stub.getDefaultImpl() != null)
            return IOplusPowerMonitor.Stub.getDefaultImpl().getWakeLockInfo(param2Long1, param2Long2); 
          parcel2.readException();
          return parcel2.createTypedArrayList(OplusWakeLockInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusPowerMonitor param1IOplusPowerMonitor) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusPowerMonitor != null) {
          Proxy.sDefaultImpl = param1IOplusPowerMonitor;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusPowerMonitor getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
