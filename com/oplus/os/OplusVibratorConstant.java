package com.oplus.os;

public class OplusVibratorConstant {
  public static final long LONG_MIDDLE_ONESHOT_TIME = 150L;
  
  public static final long LONG_STRONG_ONESHOT_TIME = 400L;
  
  public static final int MIDDLE_AMPLITUDE = 175;
  
  public static final long RAPID_MIDDLE_ONESHOT_TIME = 50L;
  
  public static final int[] RAPID_MIDDLE_WAVEFORM_AMPLITUDE;
  
  public static final long[] RAPID_MIDDLE_WAVEFORM_TIME = new long[] { 0L, 25L, 70L, 25L };
  
  public static final int[] RAPID_STRONG_WAVEFORM_AMPLITUDE;
  
  public static final long[] RAPID_STRONG_WAVEFORM_TIME;
  
  public static final long RAPID_WEAK_ONESHOT_TIME = 25L;
  
  public static final int STRONG_AMPLITUDE = 250;
  
  public static final int WEAK_AMPLITUDE = 100;
  
  static {
    RAPID_MIDDLE_WAVEFORM_AMPLITUDE = new int[] { 0, 195, 0, 195 };
    RAPID_STRONG_WAVEFORM_TIME = new long[] { 0L, 350L, 200L, 350L };
    RAPID_STRONG_WAVEFORM_AMPLITUDE = new int[] { 0, 250, 0, 250 };
  }
}
