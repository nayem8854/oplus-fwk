package com.oplus.os;

import android.content.Context;
import android.os.IBinder;
import android.os.ServiceManager;
import android.util.Log;
import com.oplus.app.OplusAlarmInfo;
import com.oplus.app.OplusWakeLockInfo;
import java.util.List;

public class OplusPowerMonitor {
  private IOplusPowerMonitor mOppoPowerMonitorService = null;
  
  private static OplusPowerMonitor mInstance = null;
  
  private Context mContext = null;
  
  public static final String POWER_MONITOR_SERVICE_NAME = "power_monitor";
  
  private static final String TAG = "OplusPowerMonitor";
  
  private OplusPowerMonitor(Context paramContext) {
    IBinder iBinder = ServiceManager.getService("power_monitor");
    this.mOppoPowerMonitorService = IOplusPowerMonitor.Stub.asInterface(iBinder);
    this.mContext = paramContext;
  }
  
  public static OplusPowerMonitor getInstance(Context paramContext) {
    if (mInstance == null)
      mInstance = new OplusPowerMonitor(paramContext); 
    return mInstance;
  }
  
  public void recordAlarmWakeupEvent() {
    IOplusPowerMonitor iOplusPowerMonitor = this.mOppoPowerMonitorService;
    if (iOplusPowerMonitor != null) {
      try {
        iOplusPowerMonitor.recordAlarmWakeupEvent();
      } catch (Exception exception) {
        Log.e("OplusPowerMonitor", "recordAlarmWakeupEvent failed.", exception);
      } 
    } else {
      Log.e("OplusPowerMonitor", "recordAlarmWakeupEvent failed: service unavailable");
    } 
  }
  
  public void recordAppWakeupEvent(int paramInt, String paramString) {
    IOplusPowerMonitor iOplusPowerMonitor = this.mOppoPowerMonitorService;
    if (iOplusPowerMonitor != null) {
      try {
        iOplusPowerMonitor.recordAppWakeupEvent(paramInt, paramString);
      } catch (Exception exception) {
        Log.e("OplusPowerMonitor", "recordAppWakeupEvent failed.", exception);
      } 
    } else {
      Log.e("OplusPowerMonitor", "recordAppWakeupEvent failed: service unavailable");
    } 
  }
  
  public void recordAppWakeupInfoEvent(OplusAlarmInfo paramOplusAlarmInfo) {
    IOplusPowerMonitor iOplusPowerMonitor = this.mOppoPowerMonitorService;
    if (iOplusPowerMonitor != null) {
      try {
        iOplusPowerMonitor.recordAppWakeupInfoEvent(paramOplusAlarmInfo);
      } catch (Exception exception) {
        Log.e("OplusPowerMonitor", "recordAppWakeupInfoEvent failed.", exception);
      } 
    } else {
      Log.e("OplusPowerMonitor", "recordAppWakeupInfoEvent failed: service unavailable");
    } 
  }
  
  public void resetWakeupEventRecords() {
    IOplusPowerMonitor iOplusPowerMonitor = this.mOppoPowerMonitorService;
    if (iOplusPowerMonitor != null) {
      try {
        iOplusPowerMonitor.resetWakeupEventRecords();
      } catch (Exception exception) {
        Log.e("OplusPowerMonitor", "resetWakeupEventRecords failed.", exception);
      } 
    } else {
      Log.e("OplusPowerMonitor", "resetWakeupEventRecords failed: service unavailable");
    } 
  }
  
  public List<OplusAlarmInfo> getAlarmWakeUpInfo(long paramLong1, long paramLong2) {
    IOplusPowerMonitor iOplusPowerMonitor = this.mOppoPowerMonitorService;
    if (iOplusPowerMonitor != null) {
      try {
        return iOplusPowerMonitor.getAlarmWakeUpInfo(paramLong1, paramLong2);
      } catch (Exception exception) {
        Log.e("OplusPowerMonitor", "getAlarmWakeUpInfo failed.", exception);
      } 
    } else {
      Log.e("OplusPowerMonitor", "getAlarmWakeUpInfo failed: service unavailable");
    } 
    return null;
  }
  
  public void recordWakeLockAcquireEvent(OplusWakeLockInfo paramOplusWakeLockInfo) {
    IOplusPowerMonitor iOplusPowerMonitor = this.mOppoPowerMonitorService;
    if (iOplusPowerMonitor != null) {
      try {
        iOplusPowerMonitor.recordWakeLockAcquireEvent(paramOplusWakeLockInfo);
      } catch (Exception exception) {
        Log.e("OplusPowerMonitor", "recordWakeLockAcquireEvent failed.", exception);
      } 
    } else {
      Log.e("OplusPowerMonitor", "recordWakeLockAcquireEvent failed: service unavailable");
    } 
  }
  
  public void recordWakeLockReleaseEvent(OplusWakeLockInfo paramOplusWakeLockInfo) {
    IOplusPowerMonitor iOplusPowerMonitor = this.mOppoPowerMonitorService;
    if (iOplusPowerMonitor != null) {
      try {
        iOplusPowerMonitor.recordWakeLockReleaseEvent(paramOplusWakeLockInfo);
      } catch (Exception exception) {
        Log.e("OplusPowerMonitor", "recordWakeLockEvent failed.", exception);
      } 
    } else {
      Log.e("OplusPowerMonitor", "recordWakeLockEvent failed: service unavailable");
    } 
  }
  
  public List<OplusWakeLockInfo> getWakeLockInfo(long paramLong1, long paramLong2) {
    IOplusPowerMonitor iOplusPowerMonitor = this.mOppoPowerMonitorService;
    if (iOplusPowerMonitor != null) {
      try {
        return iOplusPowerMonitor.getWakeLockInfo(paramLong1, paramLong2);
      } catch (Exception exception) {
        Log.e("OplusPowerMonitor", "getWakeLockInfo failed.", exception);
      } 
    } else {
      Log.e("OplusPowerMonitor", "getWakeLockInfo failed: service unavailable");
    } 
    return null;
  }
}
