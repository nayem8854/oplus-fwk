package com.oplus.os;

import android.content.Context;
import android.os.Environment;
import android.util.Slog;
import android.util.Xml;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class OplusApkInstallHelper {
  private static final String TAG = "OplusApkInstallHelper";
  
  private static boolean external;
  
  private static boolean internal = false;
  
  static {
    external = false;
    parsexml();
  }
  
  public static boolean InstallUIDisplay(Context paramContext) {
    boolean bool2, bool1 = false;
    if (internal) {
      bool2 = true;
    } else {
      bool2 = bool1;
      if (external) {
        bool2 = bool1;
        if (!OplusUsbEnvironment.isExternalSDRemoved(paramContext))
          bool2 = true; 
      } 
    } 
    return bool2;
  }
  
  public static boolean IsInstallSdMounted(Context paramContext) {
    boolean bool = false;
    if (internal) {
      bool = "mounted".equals(OplusUsbEnvironment.getInternalSdState(paramContext));
    } else if (external) {
      bool = "mounted".equals(OplusUsbEnvironment.getExternalSdState(paramContext));
    } 
    return bool;
  }
  
  private static void parsexml() {
    File file = new File(Environment.getRootDirectory(), "etc/apk_install.xml");
    try {
      FileReader fileReader = new FileReader(file);
      try {
        XmlPullParser xmlPullParser = Xml.newPullParser();
        xmlPullParser.setInput(fileReader);
        int i = xmlPullParser.getEventType();
        while (true) {
          boolean bool = true;
          if (i != 1) {
            if (i == 2)
              if (xmlPullParser.getName().equals("path")) {
                boolean bool1;
                xmlPullParser.next();
                if (xmlPullParser.getText().equals("internal")) {
                  bool1 = true;
                } else {
                  bool1 = false;
                } 
                internal = bool1;
                if (xmlPullParser.getText().equals("external")) {
                  bool1 = bool;
                } else {
                  bool1 = false;
                } 
                external = bool1;
              }  
            i = xmlPullParser.next();
            continue;
          } 
          break;
        } 
      } catch (XmlPullParserException xmlPullParserException) {
        Slog.w("OplusApkInstallHelper", "Got execption parsing permissions.", (Throwable)xmlPullParserException);
      } catch (IOException iOException) {
        Slog.w("OplusApkInstallHelper", "Got execption parsing permissions.", iOException);
      } 
      try {
        fileReader.close();
      } catch (IOException iOException1) {
        iOException1.printStackTrace();
      } 
      return;
    } catch (FileNotFoundException fileNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Couldn't find or open apk_install file ");
      stringBuilder.append(iOException);
      Slog.w("OplusApkInstallHelper", stringBuilder.toString());
      return;
    } 
  }
}
