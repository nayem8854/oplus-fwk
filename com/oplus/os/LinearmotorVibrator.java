package com.oplus.os;

import android.content.Context;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.RemoteException;
import android.util.Slog;

public class LinearmotorVibrator {
  private final Object mLock = new Object();
  
  private final Binder mToken = new Binder();
  
  private HandlerThread mHandlerThread = null;
  
  private VibratorHandler mHandler = null;
  
  public static final String FEATURE_WAVEFORM_VIBRATOR = "oplus.software.vibrator_lmvibrator";
  
  public static final String LINEARMOTORVIBRATOR_SERVICE = "linearmotor";
  
  private static final int MSG_LINEARMOTOR_VIBRATOR_BEGIN = 10000;
  
  private static final int MSG_LINEARMOTOR_VIBRATOR_VIBRATE = 10001;
  
  public static final String TAG = "LinearmotorVibrator";
  
  private Context mContext;
  
  private final String mPackageName;
  
  private final ILinearmotorVibratorService mService;
  
  private boolean mVibrating;
  
  class VibratorHandler extends Handler {
    final LinearmotorVibrator this$0;
    
    public VibratorHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.arg1;
      if (i == 10001)
        synchronized (LinearmotorVibrator.this.mLock) {
          if (!LinearmotorVibrator.this.mVibrating) {
            LinearmotorVibrator.access$102(LinearmotorVibrator.this, true);
            try {
              WaveformEffect waveformEffect = (WaveformEffect)param1Message.obj;
              LinearmotorVibrator.this.mService.vibrate(Process.myUid(), LinearmotorVibrator.this.mPackageName, waveformEffect, (IBinder)LinearmotorVibrator.this.mToken);
            } catch (RemoteException remoteException) {
              Slog.w("LinearmotorVibrator", "Remote exception in LinearmotorVibrator: ", (Throwable)remoteException);
            } 
            LinearmotorVibrator.access$102(LinearmotorVibrator.this, false);
          } 
          return;
        }  
    }
  }
  
  public LinearmotorVibrator(Context paramContext, ILinearmotorVibratorService paramILinearmotorVibratorService) {
    this.mContext = paramContext;
    this.mService = paramILinearmotorVibratorService;
    this.mPackageName = paramContext.getOpPackageName();
    if (this.mService == null)
      Slog.v("LinearmotorVibrator", "ILinearmotorVibratorService was null"); 
    this.mVibrating = false;
  }
  
  public void vibrate(WaveformEffect paramWaveformEffect) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("vibrate WaveformEffect:");
    stringBuilder.append(paramWaveformEffect.toString());
    Slog.d("LinearmotorVibrator", stringBuilder.toString());
    if (this.mService != null && paramWaveformEffect != null)
      if (paramWaveformEffect.getAsynchronous()) {
        if (this.mHandlerThread == null) {
          HandlerThread handlerThread = new HandlerThread("LinearmotorVibrator-Thread");
          handlerThread.start();
        } 
        if (this.mHandler == null)
          this.mHandler = new VibratorHandler(this.mHandlerThread.getLooper()); 
        if (!this.mVibrating) {
          Message message = this.mHandler.obtainMessage();
          message.arg1 = 10001;
          message.obj = paramWaveformEffect;
          this.mHandler.sendMessage(message);
          Slog.d("LinearmotorVibrator", "vibrate WaveformEffect async.");
        } 
      } else {
        try {
          Slog.d("LinearmotorVibrator", "call service vibrate");
          this.mService.vibrate(Process.myUid(), this.mPackageName, paramWaveformEffect, (IBinder)this.mToken);
        } catch (RemoteException remoteException) {
          Slog.w("LinearmotorVibrator", "Remote exception in LinearmotorVibrator: ", (Throwable)remoteException);
        } 
      }  
  }
  
  public void cancelVibrate(WaveformEffect paramWaveformEffect) {
    if (this.mService != null)
      try {
        Slog.d("LinearmotorVibrator", "call linearmotor vibrator service cancelVibrate");
        this.mService.cancelVibrate(paramWaveformEffect, (IBinder)this.mToken);
        if (this.mHandlerThread != null) {
          this.mHandlerThread.quitSafely();
          this.mHandlerThread = null;
        } 
        if (this.mHandler != null)
          this.mHandler = null; 
      } catch (RemoteException remoteException) {
        Slog.w("LinearmotorVibrator", "Remote exception in LinearmotorVibrator: ", (Throwable)remoteException);
      }  
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mHandlerThread != null) {
        this.mHandlerThread.quitSafely();
        this.mHandlerThread = null;
      } 
      if (this.mHandler != null)
        this.mHandler = null; 
      return;
    } finally {
      super.finalize();
    } 
  }
}
