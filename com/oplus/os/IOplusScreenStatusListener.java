package com.oplus.os;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusScreenStatusListener extends IInterface {
  void onScreenOff() throws RemoteException;
  
  void onScreenOn() throws RemoteException;
  
  class Default implements IOplusScreenStatusListener {
    public void onScreenOff() throws RemoteException {}
    
    public void onScreenOn() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusScreenStatusListener {
    private static final String DESCRIPTOR = "com.oplus.os.IOplusScreenStatusListener";
    
    static final int TRANSACTION_onScreenOff = 1;
    
    static final int TRANSACTION_onScreenOn = 2;
    
    public Stub() {
      attachInterface(this, "com.oplus.os.IOplusScreenStatusListener");
    }
    
    public static IOplusScreenStatusListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.os.IOplusScreenStatusListener");
      if (iInterface != null && iInterface instanceof IOplusScreenStatusListener)
        return (IOplusScreenStatusListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onScreenOn";
      } 
      return "onScreenOff";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("com.oplus.os.IOplusScreenStatusListener");
          return true;
        } 
        param1Parcel1.enforceInterface("com.oplus.os.IOplusScreenStatusListener");
        onScreenOn();
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.os.IOplusScreenStatusListener");
      onScreenOff();
      return true;
    }
    
    private static class Proxy implements IOplusScreenStatusListener {
      public static IOplusScreenStatusListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.os.IOplusScreenStatusListener";
      }
      
      public void onScreenOff() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.os.IOplusScreenStatusListener");
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusScreenStatusListener.Stub.getDefaultImpl() != null) {
            IOplusScreenStatusListener.Stub.getDefaultImpl().onScreenOff();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onScreenOn() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.os.IOplusScreenStatusListener");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IOplusScreenStatusListener.Stub.getDefaultImpl() != null) {
            IOplusScreenStatusListener.Stub.getDefaultImpl().onScreenOn();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusScreenStatusListener param1IOplusScreenStatusListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusScreenStatusListener != null) {
          Proxy.sDefaultImpl = param1IOplusScreenStatusListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusScreenStatusListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
