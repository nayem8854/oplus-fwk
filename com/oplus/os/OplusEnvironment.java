package com.oplus.os;

import android.content.Context;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import java.io.File;

public class OplusEnvironment {
  private static void update(Context paramContext) {
    StorageManager storageManager = (StorageManager)paramContext.getSystemService("storage");
    if (storageManager == null)
      return; 
    StorageVolume[] arrayOfStorageVolume = storageManager.getVolumeList();
    if (arrayOfStorageVolume == null)
      return; 
    for (byte b = 0; b < arrayOfStorageVolume.length; b++) {
      if (arrayOfStorageVolume[b].isRemovable()) {
        externalSdDir = arrayOfStorageVolume[b].getPath();
      } else {
        internalSdDir = arrayOfStorageVolume[b].getPath();
      } 
    } 
  }
  
  public static File getInternalSdDirectory(Context paramContext) {
    File file;
    update(paramContext);
    if (internalSdDir == null) {
      paramContext = null;
    } else {
      file = new File(internalSdDir);
    } 
    return file;
  }
  
  public static File getExternalSdDirectory(Context paramContext) {
    File file;
    update(paramContext);
    if (externalSdDir == null) {
      paramContext = null;
    } else {
      file = new File(externalSdDir);
    } 
    return file;
  }
  
  public static String getInternalSdState(Context paramContext) {
    update(paramContext);
    String str = internalSdDir;
    if (str == null) {
      str = null;
    } else {
      str = mStorageManager.getVolumeState(str);
    } 
    return str;
  }
  
  public static String getExternalSdState(Context paramContext) {
    update(paramContext);
    String str = externalSdDir;
    if (str == null) {
      str = null;
    } else {
      str = mStorageManager.getVolumeState(str);
    } 
    return str;
  }
  
  public static boolean isExternalSDRemoved(Context paramContext) {
    update(paramContext);
    String str = externalSdDir;
    if (str == null)
      return true; 
    StorageManager storageManager = mStorageManager;
    return 
      "removed".equals(storageManager.getVolumeState(str));
  }
  
  public static final File PARENT_STORAGE_DIRECTORY = getDirectory("EXTERNAL_STORAGE", "/storage/sdcard0");
  
  public static final File SUB_STORAGE_DIRECTORY = getDirectory("INTERNAL_STORAGE", "/storage/sdcard0/external_sd");
  
  private static final String TAG = "OplusEnvironment";
  
  private static String externalSdDir;
  
  private static String internalSdDir;
  
  private static StorageManager mStorageManager;
  
  static File getDirectory(String paramString1, String paramString2) {
    String str = System.getenv(paramString1);
    File file = new File();
    if (str == null) {
      this(paramString2);
    } else {
      this(str);
    } 
    return file;
  }
}
