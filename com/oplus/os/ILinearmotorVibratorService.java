package com.oplus.os;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ILinearmotorVibratorService extends IInterface {
  void cancelVibrate(WaveformEffect paramWaveformEffect, IBinder paramIBinder) throws RemoteException;
  
  void vibrate(int paramInt, String paramString, WaveformEffect paramWaveformEffect, IBinder paramIBinder) throws RemoteException;
  
  class Default implements ILinearmotorVibratorService {
    public void vibrate(int param1Int, String param1String, WaveformEffect param1WaveformEffect, IBinder param1IBinder) throws RemoteException {}
    
    public void cancelVibrate(WaveformEffect param1WaveformEffect, IBinder param1IBinder) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ILinearmotorVibratorService {
    private static final String DESCRIPTOR = "com.oplus.os.ILinearmotorVibratorService";
    
    static final int TRANSACTION_cancelVibrate = 2;
    
    static final int TRANSACTION_vibrate = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.os.ILinearmotorVibratorService");
    }
    
    public static ILinearmotorVibratorService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.os.ILinearmotorVibratorService");
      if (iInterface != null && iInterface instanceof ILinearmotorVibratorService)
        return (ILinearmotorVibratorService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "cancelVibrate";
      } 
      return "vibrate";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      WaveformEffect waveformEffect;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("com.oplus.os.ILinearmotorVibratorService");
          return true;
        } 
        param1Parcel1.enforceInterface("com.oplus.os.ILinearmotorVibratorService");
        if (param1Parcel1.readInt() != 0) {
          waveformEffect = (WaveformEffect)WaveformEffect.CREATOR.createFromParcel(param1Parcel1);
        } else {
          waveformEffect = null;
        } 
        iBinder = param1Parcel1.readStrongBinder();
        cancelVibrate(waveformEffect, iBinder);
        param1Parcel2.writeNoException();
        return true;
      } 
      iBinder.enforceInterface("com.oplus.os.ILinearmotorVibratorService");
      param1Int1 = iBinder.readInt();
      String str = iBinder.readString();
      if (iBinder.readInt() != 0) {
        waveformEffect = (WaveformEffect)WaveformEffect.CREATOR.createFromParcel((Parcel)iBinder);
      } else {
        waveformEffect = null;
      } 
      IBinder iBinder = iBinder.readStrongBinder();
      vibrate(param1Int1, str, waveformEffect, iBinder);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements ILinearmotorVibratorService {
      public static ILinearmotorVibratorService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.os.ILinearmotorVibratorService";
      }
      
      public void vibrate(int param2Int, String param2String, WaveformEffect param2WaveformEffect, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.os.ILinearmotorVibratorService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          if (param2WaveformEffect != null) {
            parcel1.writeInt(1);
            param2WaveformEffect.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ILinearmotorVibratorService.Stub.getDefaultImpl() != null) {
            ILinearmotorVibratorService.Stub.getDefaultImpl().vibrate(param2Int, param2String, param2WaveformEffect, param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelVibrate(WaveformEffect param2WaveformEffect, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.os.ILinearmotorVibratorService");
          if (param2WaveformEffect != null) {
            parcel1.writeInt(1);
            param2WaveformEffect.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ILinearmotorVibratorService.Stub.getDefaultImpl() != null) {
            ILinearmotorVibratorService.Stub.getDefaultImpl().cancelVibrate(param2WaveformEffect, param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ILinearmotorVibratorService param1ILinearmotorVibratorService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ILinearmotorVibratorService != null) {
          Proxy.sDefaultImpl = param1ILinearmotorVibratorService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ILinearmotorVibratorService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
