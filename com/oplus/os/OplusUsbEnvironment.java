package com.oplus.os;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Environment;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.os.storage.DiskInfo;
import android.os.storage.IStorageManager;
import android.os.storage.StorageEventListener;
import android.os.storage.StorageManager;
import android.os.storage.VolumeInfo;
import android.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class OplusUsbEnvironment extends Environment {
  private static final String DEFAULT_INTERNAL_PATH = "/storage/emulated/0";
  
  public static final int EXTERNAL = 2;
  
  public static final int INTERNAL = 1;
  
  private static final String MULTIAPP_INTERNAL_PATH = "/storage/emulated/999";
  
  public static final int NONE = -1;
  
  public static final int OTG = 3;
  
  private static final String TAG = "OppoUsbEnvironmentSys";
  
  private static String sExternalSdDir;
  
  private static boolean sInited = false;
  
  private static String sInternalSdDir = "/storage/emulated/0";
  
  private static Object sLock;
  
  private static IStorageManager sMountService;
  
  private static ArrayList<String> sOtgPathes;
  
  private static StorageEventListener sStorageListener;
  
  private static BroadcastReceiver sVolumeStateReceiver;
  
  static {
    sExternalSdDir = null;
    sOtgPathes = new ArrayList<>();
    sMountService = null;
    sLock = new Object();
    sVolumeStateReceiver = (BroadcastReceiver)new Object();
    sStorageListener = (StorageEventListener)new Object();
  }
  
  private static void update(Context paramContext) {
    if (sMountService == null)
      sMountService = IStorageManager.Stub.asInterface(ServiceManager.getService("mount")); 
    if (!sInited) {
      IntentFilter intentFilter;
      boolean bool = true;
      sInited = true;
      getVolumes();
      Context context = paramContext.getApplicationContext();
      if (paramContext.checkSelfPermission("android.permission.WRITE_MEDIA_STORAGE") != 0)
        bool = false; 
      if (context != null && bool) {
        intentFilter = new IntentFilter();
        intentFilter.addAction("android.os.storage.action.VOLUME_STATE_CHANGED");
        context.registerReceiver(sVolumeStateReceiver, intentFilter);
        Log.d("OppoUsbEnvironmentSys", "update: registerReceiver sVolumeStateReceiver");
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("update: hasPerm WRITE_MEDIA_STORAGE=");
        stringBuilder.append(bool);
        stringBuilder.append(", contextApp=");
        stringBuilder.append(context);
        Log.d("OppoUsbEnvironmentSys", stringBuilder.toString());
        StorageManager storageManager = (StorageManager)intentFilter.getSystemService("storage");
        if (storageManager != null) {
          storageManager.registerListener(sStorageListener);
          Log.d("OppoUsbEnvironmentSys", "update: registerListener sStorageListener");
        } 
      } 
    } 
  }
  
  public static File getInternalSdDirectory(Context paramContext) {
    synchronized (sLock) {
      File file;
      update(paramContext);
      String str = sInternalSdDir;
      if (str == null) {
        str = null;
      } else {
        file = new File(str);
      } 
      return file;
    } 
  }
  
  public static File getExternalSdDirectory(Context paramContext) {
    synchronized (sLock) {
      File file;
      update(paramContext);
      String str = sExternalSdDir;
      if (str == null) {
        str = null;
      } else {
        file = new File(str);
      } 
      return file;
    } 
  }
  
  public static String getInternalSdState(Context paramContext) {
    synchronized (sLock) {
      update(paramContext);
      String str = sInternalSdDir;
      if (str == null)
        return "unknown"; 
      StorageManager storageManager = (StorageManager)paramContext.getSystemService("storage");
      if (storageManager != null)
        return storageManager.getVolumeState(str); 
      return "unknown";
    } 
  }
  
  public static String getExternalSdState(Context paramContext) {
    synchronized (sLock) {
      update(paramContext);
      String str = sExternalSdDir;
      if (str == null)
        return "unknown"; 
      StorageManager storageManager = (StorageManager)paramContext.getSystemService("storage");
      if (storageManager != null)
        return storageManager.getVolumeState(str); 
      return "unknown";
    } 
  }
  
  public static boolean isExternalSDRemoved(Context paramContext) {
    synchronized (sLock) {
      update(paramContext);
      String str = sExternalSdDir;
      if (str == null)
        return true; 
      null = "unknown";
      StorageManager storageManager = (StorageManager)paramContext.getSystemService("storage");
      Object object = null;
      if (storageManager != null)
        object = storageManager.getVolumeState(str); 
      null = new StringBuilder();
      null.append("isExternalSDRemoved: the state of volume is: ");
      null.append((String)object);
      Log.i("OppoUsbEnvironmentSys", null.toString());
      return "removed".equals(object);
    } 
  }
  
  public static boolean isNestMounted() {
    // Byte code:
    //   0: getstatic com/oplus/os/OplusUsbEnvironment.sLock : Ljava/lang/Object;
    //   3: astore_0
    //   4: aload_0
    //   5: monitorenter
    //   6: iconst_0
    //   7: istore_1
    //   8: iload_1
    //   9: istore_2
    //   10: getstatic com/oplus/os/OplusUsbEnvironment.sInternalSdDir : Ljava/lang/String;
    //   13: ifnull -> 52
    //   16: iload_1
    //   17: istore_2
    //   18: getstatic com/oplus/os/OplusUsbEnvironment.sExternalSdDir : Ljava/lang/String;
    //   21: ifnull -> 52
    //   24: getstatic com/oplus/os/OplusUsbEnvironment.sInternalSdDir : Ljava/lang/String;
    //   27: getstatic com/oplus/os/OplusUsbEnvironment.sExternalSdDir : Ljava/lang/String;
    //   30: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   33: ifne -> 50
    //   36: iload_1
    //   37: istore_2
    //   38: getstatic com/oplus/os/OplusUsbEnvironment.sExternalSdDir : Ljava/lang/String;
    //   41: getstatic com/oplus/os/OplusUsbEnvironment.sInternalSdDir : Ljava/lang/String;
    //   44: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   47: ifeq -> 52
    //   50: iconst_1
    //   51: istore_2
    //   52: aload_0
    //   53: monitorexit
    //   54: iload_2
    //   55: ireturn
    //   56: astore_3
    //   57: aload_0
    //   58: monitorexit
    //   59: aload_3
    //   60: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #177	-> 0
    //   #178	-> 6
    //   #179	-> 8
    //   #180	-> 24
    //   #181	-> 50
    //   #184	-> 52
    //   #185	-> 56
    // Exception table:
    //   from	to	target	type
    //   10	16	56	finally
    //   18	24	56	finally
    //   24	36	56	finally
    //   38	50	56	finally
    //   52	54	56	finally
    //   57	59	56	finally
  }
  
  public static List<String> getOtgPath(Context paramContext) {
    synchronized (sLock) {
      update(paramContext);
      if (sOtgPathes == null)
        return null; 
      return (ArrayList)sOtgPathes.clone();
    } 
  }
  
  public static boolean isVolumeMounted(Context paramContext, String paramString) {
    synchronized (sLock) {
      update(paramContext);
      StorageManager storageManager = (StorageManager)paramContext.getSystemService("storage");
      if (paramString != null && storageManager != null)
        return "mounted".equals(storageManager.getVolumeState(paramString)); 
      return false;
    } 
  }
  
  public static String getInternalPath(Context paramContext) {
    synchronized (sLock) {
      update(paramContext);
      return sInternalSdDir;
    } 
  }
  
  public static String getExternalPath(Context paramContext) {
    synchronized (sLock) {
      update(paramContext);
      return sExternalSdDir;
    } 
  }
  
  public static int getPathType(Context paramContext, String paramString) {
    synchronized (sLock) {
      update(paramContext);
      if (paramString == null)
        return -1; 
      if (paramString.equals(sInternalSdDir))
        return 1; 
      if (paramString.equals(sExternalSdDir))
        return 2; 
      if (sOtgPathes != null && sOtgPathes.contains(paramString))
        return 3; 
      return -1;
    } 
  }
  
  private static void getVolumes() {
    IStorageManager iStorageManager = sMountService;
    if (iStorageManager == null) {
      Log.e("OppoUsbEnvironmentSys", "getVolumes: sMountService is null!!!");
      return;
    } 
    byte b = 0;
    try {
      VolumeInfo[] arrayOfVolumeInfo = iStorageManager.getVolumes(0);
      sExternalSdDir = null;
      sOtgPathes.clear();
      for (int i = arrayOfVolumeInfo.length; b < i; ) {
        StringBuilder stringBuilder;
        VolumeInfo volumeInfo = arrayOfVolumeInfo[b];
        String str = volumeInfo.path;
        if (volumeInfo.type == 2) {
          int j = UserHandle.myUserId();
          if (str != null) {
            sInternalSdDir = str.concat("/").concat(Integer.toString(j));
            stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("getVolumes: sInternalSdDir=");
            stringBuilder.append(sInternalSdDir);
            Log.d("OppoUsbEnvironmentSys", stringBuilder.toString());
          } 
        } else {
          DiskInfo diskInfo = volumeInfo.getDisk();
          if (diskInfo != null) {
            if (diskInfo.isSd() && 
              stringBuilder != null) {
              sExternalSdDir = (String)stringBuilder;
              StringBuilder stringBuilder1 = new StringBuilder();
              this();
              stringBuilder1.append("getVolumes: sExternalSdDir=");
              stringBuilder1.append(sExternalSdDir);
              Log.d("OppoUsbEnvironmentSys", stringBuilder1.toString());
            } 
            if (diskInfo.isUsb() && 
              stringBuilder != null && !sOtgPathes.contains(stringBuilder)) {
              sOtgPathes.add(stringBuilder);
              StringBuilder stringBuilder1 = new StringBuilder();
              this();
              stringBuilder1.append("getVolumes: sOtgPathes.add=");
              stringBuilder1.append((String)stringBuilder);
              Log.d("OppoUsbEnvironmentSys", stringBuilder1.toString());
            } 
          } 
        } 
        b++;
      } 
    } catch (RemoteException remoteException) {}
  }
  
  public static String getMultiappSdDirectory() {
    return "/storage/emulated/999";
  }
}
