package com.oplus.os;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Build;
import android.os.SystemProperties;
import android.provider.Settings;
import android.text.TextUtils;

public class OplusBuild {
  public static final String MARKET = getString("ro.oppo.market.name");
  
  public static final int OplusOS_11_0 = 19;
  
  public static final int OplusOS_11_1 = 20;
  
  public static final int OplusOS_11_2 = 21;
  
  public static final int OplusOS_1_0 = 1;
  
  public static final int OplusOS_1_2 = 2;
  
  public static final int OplusOS_1_4 = 3;
  
  public static final int OplusOS_2_0 = 4;
  
  public static final int OplusOS_2_1 = 5;
  
  public static final int OplusOS_3_0 = 6;
  
  public static final int OplusOS_3_1 = 7;
  
  public static final int OplusOS_3_2 = 8;
  
  public static final int OplusOS_5_0 = 9;
  
  public static final int OplusOS_5_1 = 10;
  
  public static final int OplusOS_5_2 = 11;
  
  public static final int OplusOS_6_0 = 12;
  
  public static final int OplusOS_6_1 = 13;
  
  public static final int OplusOS_6_2 = 14;
  
  public static final int OplusOS_6_7 = 15;
  
  public static final int OplusOS_7_0 = 16;
  
  public static final int OplusOS_7_1 = 17;
  
  public static final int OplusOS_7_2 = 18;
  
  private static final String SECURE_SETTINGS_BLUETOOTH_NAME = "bluetooth_name";
  
  private static final String SECURE_SETTINGS_DEVICE_NAME = "oppo_device_name";
  
  public static final int UNKNOWN = 0;
  
  private static final String[] VERSIONS = new String[] { 
      "V1.0", "V1.2", "V1.4", "V2.0", "V2.1", "V3.0", "V3.1", "V3.2", "V5.0", "V5.1", 
      "V5.2", "V6.0", "V6.1", "V6.2", "V6.7", "V7", "V7.1", "V7.2", "V11", "V11.1", 
      "V11.2", null };
  
  public static int getOplusOSVERSION() {
    for (int i = VERSIONS.length - 2; i >= 0; i--) {
      if (!TextUtils.isEmpty(VERSION.RELEASE) && VERSION.RELEASE.startsWith(VERSIONS[i]))
        return i + 1; 
    } 
    return 19;
  }
  
  public static boolean setDeviceName(String paramString) {
    return true;
  }
  
  public static String getDeviceName() {
    return null;
  }
  
  public static String getDeviceName(Context paramContext) {
    String str = Settings.Secure.getString(paramContext.getContentResolver(), "oppo_device_name");
    if (str != null && str.length() != 0 && !str.trim().isEmpty())
      return str; 
    str = SystemProperties.get("ro.oppo.market.name", "");
    if (!TextUtils.isEmpty(str)) {
      putDeviceName(paramContext, str);
      return str;
    } 
    return Build.MODEL;
  }
  
  public static void putDeviceName(Context paramContext, String paramString) {
    if (paramString != null)
      Settings.Secure.putString(paramContext.getContentResolver(), "oppo_device_name", paramString); 
  }
  
  public static void setDeviceName(Context paramContext, String paramString) {
    if (paramString != null && !paramString.trim().isEmpty()) {
      putDeviceName(paramContext, paramString);
      WifiManager wifiManager = (WifiManager)paramContext.getSystemService("wifi");
      if (wifiManager.isWifiEnabled()) {
        WifiP2pManager wifiP2pManager = (WifiP2pManager)paramContext.getSystemService("wifip2p");
        if (wifiP2pManager != null) {
          WifiP2pManager.Channel channel = wifiP2pManager.initialize(paramContext, paramContext.getMainLooper(), null);
          if (channel != null)
            wifiP2pManager.setDeviceName(channel, paramString, null); 
        } 
      } 
      BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
      if (bluetoothAdapter != null && bluetoothAdapter.isEnabled())
        bluetoothAdapter.setName(paramString); 
    } 
  }
  
  private static String getString(String paramString) {
    return SystemProperties.get(paramString, "unknown");
  }
  
  public static class VERSION {
    public static final String RELEASE = OplusBuild.getString("ro.build.version.opporom");
  }
}
