package com.oplus.settings;

import android.content.Context;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class OplusSettings extends OplusBaseSettings {
  private static final String TAG = "OplusSettings";
  
  public static final int TYPE_OPLUS = 0;
  
  public static final int TYPE_PSW = 1;
  
  public static InputStream readConfig(Context paramContext, String paramString, int paramInt) throws IOException {
    return readConfigAsUser(paramContext, paramString, -2, paramInt);
  }
  
  public static OutputStream writeConfig(Context paramContext, String paramString, int paramInt) throws IOException {
    return writeConfigAsUser(paramContext, paramString, -2, paramInt);
  }
  
  public static String readConfigString(Context paramContext, String paramString, int paramInt) throws IOException {
    return readConfigStringAsUser(paramContext, paramString, -2, paramInt);
  }
  
  public static int writeConfigString(Context paramContext, String paramString1, int paramInt, String paramString2) throws IOException {
    return writeConfigStringAsUser(paramContext, paramString1, -2, paramInt, paramString2);
  }
  
  public static void registerChangeListener(Context paramContext, String paramString, int paramInt, OplusSettingsChangeListener paramOplusSettingsChangeListener) {
    registerChangeListenerAsUser(paramContext, paramString, -2, paramInt, paramOplusSettingsChangeListener);
  }
  
  public static void unRegisterChangeListener(Context paramContext, OplusSettingsChangeListener paramOplusSettingsChangeListener) {
    paramContext.getContentResolver().unregisterContentObserver(paramOplusSettingsChangeListener);
  }
}
