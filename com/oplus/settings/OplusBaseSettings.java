package com.oplus.settings;

import android.app.ActivityThread;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.os.SystemProperties;
import android.util.Log;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class OplusBaseSettings {
  private static final String BASE_URI = "content://OplusSettings";
  
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static final String TAG = "OplusBaseSettings";
  
  public static InputStream readConfigAsUser(Context paramContext, String paramString, int paramInt1, int paramInt2) throws IOException {
    StringBuilder stringBuilder;
    if (isSystemProcess()) {
      String str = OplusSettingsConfig.getFilePath(paramInt2, paramInt1, paramString);
      if (DEBUG) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("readConfigAsUser systemUser path=");
        stringBuilder.append(str);
        stringBuilder.append(" type=");
        stringBuilder.append(paramInt2);
        stringBuilder.append(" userId=");
        stringBuilder.append(paramInt1);
        stringBuilder.append(" customPath=");
        stringBuilder.append(paramString);
        Log.d("OplusBaseSettings", stringBuilder.toString());
      } 
      return new FileInputStream(str);
    } 
    Uri uri = OplusSettingsConfig.getUri("content://OplusSettings", paramString, paramInt1, paramInt2);
    ContentResolver contentResolver = stringBuilder.getContentResolver();
    return contentResolver.openInputStream(uri);
  }
  
  public static OutputStream writeConfigAsUser(Context paramContext, String paramString, int paramInt1, int paramInt2) throws IOException {
    Uri uri = OplusSettingsConfig.getUri("content://OplusSettings", paramString, paramInt1, paramInt2);
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("writeConfigAsUser customPath=");
      stringBuilder.append(paramString);
      stringBuilder.append(" userId=");
      stringBuilder.append(paramInt1);
      stringBuilder.append(" type=");
      stringBuilder.append(paramInt2);
      stringBuilder.append(" uri=");
      stringBuilder.append(uri.toString());
      Log.d("OplusBaseSettings", stringBuilder.toString());
    } 
    if (isSystemProcess()) {
      paramString = OplusSettingsConfig.getFilePath(paramInt2, paramInt1, paramString);
      File file = new File(paramString);
      if (file.getParentFile() != null)
        file.getParentFile().mkdirs(); 
      if (!file.exists())
        file.createNewFile(); 
      return new OplusFileOutputStream(paramString, paramContext.getContentResolver(), uri);
    } 
    ContentResolver contentResolver = paramContext.getContentResolver();
    AssetFileDescriptor assetFileDescriptor = contentResolver.openAssetFileDescriptor(uri, "w", null);
    if (assetFileDescriptor == null)
      return null; 
    try {
      long l = assetFileDescriptor.getDeclaredLength();
      if (l < 0L)
        return (OutputStream)new ParcelFileAutoCloseOutputStream(assetFileDescriptor.getParcelFileDescriptor(), paramContext.getContentResolver(), uri); 
      return (OutputStream)new AssertFileAutoCloseOutputStream(assetFileDescriptor, paramContext.getContentResolver(), uri);
    } catch (IOException iOException) {
      throw new FileNotFoundException("Unable to create stream");
    } 
  }
  
  public static String readConfigStringAsUser(Context paramContext, String paramString, int paramInt1, int paramInt2) throws IOException {
    InputStream inputStream = null;
    BufferedReader bufferedReader1 = null;
    BufferedReader bufferedReader2 = bufferedReader1;
    try {
      InputStream inputStream1 = readConfigAsUser(paramContext, paramString, paramInt1, paramInt2);
      if (inputStream1 == null) {
        inputStream = inputStream1;
        bufferedReader2 = bufferedReader1;
        Log.e("OplusBaseSettings", "readConfig error is is null");
        return null;
      } 
      inputStream = inputStream1;
      bufferedReader2 = bufferedReader1;
      BufferedReader bufferedReader = new BufferedReader();
      inputStream = inputStream1;
      bufferedReader2 = bufferedReader1;
      InputStreamReader inputStreamReader = new InputStreamReader();
      inputStream = inputStream1;
      bufferedReader2 = bufferedReader1;
      this(inputStream1);
      inputStream = inputStream1;
      bufferedReader2 = bufferedReader1;
      this(inputStreamReader);
      inputStream = inputStream1;
      bufferedReader2 = bufferedReader;
      StringBuilder stringBuilder = new StringBuilder();
      inputStream = inputStream1;
      bufferedReader2 = bufferedReader;
      this();
      while (true) {
        inputStream = inputStream1;
        bufferedReader2 = bufferedReader;
        String str = bufferedReader.readLine();
        if (str != null) {
          inputStream = inputStream1;
          bufferedReader2 = bufferedReader;
          stringBuilder.append(str);
          continue;
        } 
        break;
      } 
      inputStream = inputStream1;
      bufferedReader2 = bufferedReader;
      return stringBuilder.toString();
    } finally {
      if (inputStream != null)
        try {
          inputStream.close();
        } catch (IOException iOException) {
          Log.e("OplusBaseSettings", "readConfig close is error", iOException);
        }  
      if (bufferedReader2 != null)
        try {
          bufferedReader2.close();
        } catch (IOException iOException) {
          Log.e("OplusBaseSettings", "readConfig close br error", iOException);
        }  
    } 
  }
  
  public static int writeConfigStringAsUser(Context paramContext, String paramString1, int paramInt1, int paramInt2, String paramString2) throws IOException {
    OutputStream outputStream = null;
    BufferedWriter bufferedWriter1 = null;
    BufferedWriter bufferedWriter2 = bufferedWriter1;
    try {
      OutputStream outputStream1 = writeConfigAsUser(paramContext, paramString1, paramInt1, paramInt2);
      if (outputStream1 == null) {
        outputStream = outputStream1;
        bufferedWriter2 = bufferedWriter1;
        Log.e("OplusBaseSettings", "writeConfig error os is null");
        return -2;
      } 
      outputStream = outputStream1;
      bufferedWriter2 = bufferedWriter1;
      BufferedWriter bufferedWriter = new BufferedWriter();
      outputStream = outputStream1;
      bufferedWriter2 = bufferedWriter1;
      OutputStreamWriter outputStreamWriter = new OutputStreamWriter();
      outputStream = outputStream1;
      bufferedWriter2 = bufferedWriter1;
      this(outputStream1);
      outputStream = outputStream1;
      bufferedWriter2 = bufferedWriter1;
      this(outputStreamWriter);
      outputStream = outputStream1;
      bufferedWriter2 = bufferedWriter;
      bufferedWriter.write(paramString2);
      outputStream = outputStream1;
      bufferedWriter2 = bufferedWriter;
      bufferedWriter.flush();
      return 0;
    } finally {
      if (outputStream != null)
        try {
          outputStream.close();
        } catch (IOException iOException) {
          Log.e("OplusBaseSettings", "writeConfig close os error", iOException);
        }  
      if (bufferedWriter2 != null)
        try {
          bufferedWriter2.close();
        } catch (IOException iOException) {
          Log.e("OplusBaseSettings", "writeConfig close bw error", iOException);
        }  
    } 
  }
  
  public static void registerChangeListenerAsUser(Context paramContext, String paramString, int paramInt1, int paramInt2, OplusSettingsChangeListener paramOplusSettingsChangeListener) {
    Uri uri = OplusSettingsConfig.getUri("content://OplusSettings", paramString, paramInt1, paramInt2);
    paramContext.getContentResolver().registerContentObserver(uri, true, paramOplusSettingsChangeListener);
  }
  
  public static void registerChangeListenerForAll(Context paramContext, String paramString, int paramInt, OplusSettingsChangeListener paramOplusSettingsChangeListener) {
    Uri uri = OplusSettingsConfig.getUri("content://OplusSettings", paramString, -2, paramInt);
    paramContext.getContentResolver().registerContentObserver(uri, true, paramOplusSettingsChangeListener, -1);
  }
  
  public static boolean isSystemProcess() {
    return ActivityThread.isSystem();
  }
  
  private static class OplusFileOutputStream extends FileOutputStream {
    private int callCount = 1;
    
    private final ContentResolver contentResolver;
    
    private final Uri uri;
    
    public OplusFileOutputStream(String param1String, ContentResolver param1ContentResolver, Uri param1Uri) throws FileNotFoundException {
      super(param1String);
      this.contentResolver = param1ContentResolver;
      this.uri = param1Uri;
    }
    
    public void close() throws IOException {
      super.close();
      ContentResolver contentResolver = this.contentResolver;
      if (contentResolver != null) {
        int i = this.callCount;
        if (i >= 1) {
          this.callCount = i - 1;
          contentResolver.update(this.uri, new ContentValues(), null, null);
        } 
      } 
    }
  }
  
  class ParcelFileAutoCloseOutputStream extends ParcelFileDescriptor.AutoCloseOutputStream {
    private int callCount = 1;
    
    private final ContentResolver contentResolver;
    
    private final Uri uri;
    
    ParcelFileAutoCloseOutputStream(OplusBaseSettings this$0, ContentResolver param1ContentResolver, Uri param1Uri) {
      super((ParcelFileDescriptor)this$0);
      this.contentResolver = param1ContentResolver;
      this.uri = param1Uri;
    }
    
    public void close() throws IOException {
      super.close();
      ContentResolver contentResolver = this.contentResolver;
      if (contentResolver != null) {
        int i = this.callCount;
        if (i >= 1) {
          this.callCount = i - 1;
          contentResolver.update(this.uri, new ContentValues(), null, null);
        } 
      } 
    }
  }
  
  class AssertFileAutoCloseOutputStream extends AssetFileDescriptor.AutoCloseOutputStream {
    private int callCount = 1;
    
    private final ContentResolver contentResolver;
    
    private final Uri uri;
    
    AssertFileAutoCloseOutputStream(OplusBaseSettings this$0, ContentResolver param1ContentResolver, Uri param1Uri) throws IOException {
      super((AssetFileDescriptor)this$0);
      this.contentResolver = param1ContentResolver;
      this.uri = param1Uri;
    }
    
    public void close() throws IOException {
      super.close();
      ContentResolver contentResolver = this.contentResolver;
      if (contentResolver != null) {
        int i = this.callCount;
        if (i >= 1) {
          this.callCount = i - 1;
          contentResolver.update(this.uri, new ContentValues(), null, null);
        } 
      } 
    }
  }
}
