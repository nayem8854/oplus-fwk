package com.oplus.settings;

import android.net.Uri;
import android.os.UserHandle;
import java.io.File;

public final class OplusSettingsConfig {
  private static final String CONFIG_PATH = "/data/oppo";
  
  public static final String PARAMS_TYPE = "ParamsType";
  
  public static final String PARAMS_USER_ID = "ParamsUserId";
  
  private static final String TAG = "CSConfig";
  
  protected static final int TYPE_OPLUS = 0;
  
  protected static final int TYPE_PSW = 1;
  
  private static String transferTypeToStr(int paramInt) {
    if (paramInt != 0) {
      if (paramInt == 1)
        return "psw"; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error type=");
      stringBuilder.append(paramInt);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    return "coloros";
  }
  
  public static String getFilePath(int paramInt1, int paramInt2, String paramString) {
    StringBuilder stringBuilder;
    String str = transferTypeToStr(paramInt1);
    if (paramInt2 == 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("/data/oppo");
      stringBuilder1.append(File.separator);
      stringBuilder1.append(str);
      stringBuilder1.append(File.separator);
      stringBuilder1.append(paramString);
      paramString = stringBuilder1.toString();
    } else if (paramInt2 == -2) {
      paramInt1 = UserHandle.myUserId();
      if (paramInt1 == 0) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("/data/oppo");
        stringBuilder1.append(File.separator);
        stringBuilder1.append(str);
        stringBuilder1.append(File.separator);
        stringBuilder1.append(paramString);
        paramString = stringBuilder1.toString();
      } else {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("/data/oppo");
        stringBuilder1.append(File.separator);
        stringBuilder1.append(str);
        stringBuilder1.append(File.separator);
        stringBuilder1.append(paramInt1);
        stringBuilder1.append(File.separator);
        stringBuilder1.append(paramString);
        paramString = stringBuilder1.toString();
      } 
    } else {
      if (paramInt2 >= 0) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("/data/oppo");
        stringBuilder1.append(File.separator);
        stringBuilder1.append(str);
        stringBuilder1.append(File.separator);
        stringBuilder1.append(paramInt2);
        stringBuilder1.append(File.separator);
        stringBuilder1.append(paramString);
        paramString = stringBuilder1.toString();
        return paramString;
      } 
      stringBuilder = new StringBuilder();
      stringBuilder.append("Error userId=");
      stringBuilder.append(paramInt2);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    return (String)stringBuilder;
  }
  
  public static Uri getUri(String paramString1, String paramString2, int paramInt1, int paramInt2) {
    Uri uri = Uri.parse(paramString1);
    Uri.Builder builder = uri.buildUpon();
    builder.appendQueryParameter("ParamsType", String.valueOf(paramInt2));
    paramInt2 = paramInt1;
    if (paramInt1 == -2)
      paramInt2 = UserHandle.myUserId(); 
    builder.appendQueryParameter("ParamsUserId", String.valueOf(paramInt2));
    builder.encodedPath(paramString2);
    return builder.build();
  }
}
