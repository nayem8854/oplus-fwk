package com.oplus.settings;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.UserHandle;
import android.util.Log;

public abstract class OplusSettingsChangeListener extends ContentObserver {
  public OplusSettingsChangeListener(Handler paramHandler) {
    super(paramHandler);
  }
  
  public final void onChange(boolean paramBoolean) {}
  
  public final void onChange(boolean paramBoolean, Uri paramUri) {
    filterUserId(paramBoolean, paramUri);
  }
  
  public final void onChange(boolean paramBoolean, Uri paramUri, int paramInt) {
    filterUserId(paramBoolean, paramUri);
  }
  
  private void filterUserId(boolean paramBoolean, Uri paramUri) {
    try {
      int i = Integer.valueOf(paramUri.getQueryParameter("ParamsUserId")).intValue();
      if (OplusSettings.isSystemProcess()) {
        onSettingsChange(paramBoolean, paramUri.getPath(), i);
      } else if (i == UserHandle.myUserId()) {
        onSettingsChange(paramBoolean, paramUri.getPath(), i);
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("filterUserId else selfChange=");
        stringBuilder.append(paramBoolean);
        stringBuilder.append(" uri=");
        stringBuilder.append(paramUri.toString());
        Log.w("CSListener", stringBuilder.toString());
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("filterUserId ERROR selfChange=");
      stringBuilder.append(paramBoolean);
      stringBuilder.append(" uri=");
      stringBuilder.append(paramUri.toString());
      Log.e("CSListener", stringBuilder.toString(), exception);
    } 
  }
  
  public abstract void onSettingsChange(boolean paramBoolean, String paramString, int paramInt);
}
