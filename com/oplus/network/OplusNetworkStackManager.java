package com.oplus.network;

import android.content.Context;
import android.net.Network;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.Log;
import java.util.ArrayList;

public class OplusNetworkStackManager {
  private boolean showDebug = SystemProperties.getBoolean("persist.oplus.network.stack.show", false);
  
  private IOplusNetworkStack mNetworkStackService;
  
  public Context mContext;
  
  private static OplusNetworkStackManager sInstance;
  
  private static ArrayList<INetworkScoreCallback> allCallbacks = new ArrayList<>();
  
  public static final String SRV_NAME = "oplusnetworkstack";
  
  public static final String LOG_TAG = "OplusNetworkStackManager";
  
  public static final boolean DBG = true;
  
  public static void registerTcpCallback(INetworkScoreCallback paramINetworkScoreCallback) {
    synchronized (allCallbacks) {
      if (!allCallbacks.contains(paramINetworkScoreCallback))
        allCallbacks.add(paramINetworkScoreCallback); 
      return;
    } 
  }
  
  public static void unregisterTcpCallback(INetworkScoreCallback paramINetworkScoreCallback) {
    synchronized (allCallbacks) {
      if (allCallbacks.contains(paramINetworkScoreCallback))
        allCallbacks.remove(paramINetworkScoreCallback); 
      return;
    } 
  }
  
  public void startTest() {
    IOplusNetworkStack iOplusNetworkStack = IOplusNetworkStack.Stub.asInterface(ServiceManager.getService("oplusnetworkstack"));
    if (iOplusNetworkStack != null) {
      try {
        Log.e("OplusNetworkStackManager", "registerTcpScoreChange start!");
        iOplusNetworkStack = this.mNetworkStackService;
        MyCallBack myCallBack = new MyCallBack();
        this(this);
        iOplusNetworkStack.registerTcpScoreChange(myCallBack);
      } catch (RemoteException remoteException) {
        Log.e("OplusNetworkStackManager", "registerTcpScoreChange fail!");
      } 
    } else {
      Log.e("OplusNetworkStackManager", "mNetworkStackService is null!");
    } 
  }
  
  public String getOplusNetworkStackInfo() {
    IOplusNetworkStack iOplusNetworkStack = IOplusNetworkStack.Stub.asInterface(ServiceManager.getService("oplusnetworkstack"));
    String str = "";
    if (iOplusNetworkStack != null) {
      try {
        Log.e("OplusNetworkStackManager", "getOplusNetworkStackInfo start!");
        String str1 = this.mNetworkStackService.getOplusNetworkStackInfo();
      } catch (RemoteException remoteException) {
        Log.e("OplusNetworkStackManager", "getOplusNetworkStackInfo fail!");
      } 
    } else {
      Log.e("OplusNetworkStackManager", "getOplusNetworkStackInfo is null!");
    } 
    return str;
  }
  
  public void setOplusNetworkStackConfig(String paramString) {
    IOplusNetworkStack iOplusNetworkStack = IOplusNetworkStack.Stub.asInterface(ServiceManager.getService("oplusnetworkstack"));
    if (iOplusNetworkStack != null) {
      try {
        Log.e("OplusNetworkStackManager", "setOplusNetworkStackConfig start!");
        this.mNetworkStackService.setOplusNetworkStackConfig(paramString);
      } catch (RemoteException remoteException) {
        Log.e("OplusNetworkStackManager", "setOplusNetworkStackConfig fail!");
      } 
    } else {
      Log.e("OplusNetworkStackManager", "setOplusNetworkStackConfig is null!");
    } 
  }
  
  public int getFailRate(Network paramNetwork) {
    IOplusNetworkStack iOplusNetworkStack = IOplusNetworkStack.Stub.asInterface(ServiceManager.getService("oplusnetworkstack"));
    if (iOplusNetworkStack != null) {
      try {
        Log.e("OplusNetworkStackManager", "getFailRate start!");
        int i = this.mNetworkStackService.getPortalResult(paramNetwork, 3);
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("getFailRate rate = ");
        stringBuilder.append(i);
        Log.e("OplusNetworkStackManager", stringBuilder.toString());
        return i;
      } catch (RemoteException remoteException) {
        Log.e("OplusNetworkStackManager", "getFailRate fail!");
      } 
    } else {
      Log.e("OplusNetworkStackManager", "getFailRate is null!");
    } 
    Log.e("OplusNetworkStackManager", "getFailRate is error rerurn 0!");
    return 0;
  }
  
  public int getNetworkScore(Network paramNetwork) {
    IOplusNetworkStack iOplusNetworkStack = IOplusNetworkStack.Stub.asInterface(ServiceManager.getService("oplusnetworkstack"));
    if (iOplusNetworkStack != null) {
      try {
        logFunc("getNetworkScore start!");
        int i = this.mNetworkStackService.getNetworkScore(paramNetwork);
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("getNetworkScore score = ");
        stringBuilder.append(i);
        logFunc(stringBuilder.toString());
        return i;
      } catch (RemoteException remoteException) {
        logFunc("getNetworkScore fail!");
      } 
    } else {
      logFunc("getNetworkScore is null!");
    } 
    logFunc("getNetworkScore is error rerurn 0!");
    return 0;
  }
  
  public static interface INetworkScoreCallback {
    void onNetworkQualityChange(boolean param1Boolean, int param1Int);
  }
  
  class MyCallBack extends IOplusNetScoreChange.Stub {
    final OplusNetworkStackManager this$0;
    
    private MyCallBack() {}
    
    public void networkScoreChange(boolean param1Boolean, int param1Int) throws RemoteException {
      synchronized (OplusNetworkStackManager.allCallbacks) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("callback len = ");
        stringBuilder.append(OplusNetworkStackManager.allCallbacks.size());
        Log.e("OplusNetworkStackManager", stringBuilder.toString());
        for (OplusNetworkStackManager.INetworkScoreCallback iNetworkScoreCallback : OplusNetworkStackManager.allCallbacks)
          iNetworkScoreCallback.onNetworkQualityChange(param1Boolean, param1Int); 
        return;
      } 
    }
  }
  
  public static OplusNetworkStackManager getInstance(Context paramContext) {
    // Byte code:
    //   0: ldc com/oplus/network/OplusNetworkStackManager
    //   2: monitorenter
    //   3: getstatic com/oplus/network/OplusNetworkStackManager.sInstance : Lcom/oplus/network/OplusNetworkStackManager;
    //   6: ifnonnull -> 30
    //   9: new com/oplus/network/OplusNetworkStackManager
    //   12: astore_1
    //   13: aload_1
    //   14: aload_0
    //   15: invokespecial <init> : (Landroid/content/Context;)V
    //   18: aload_1
    //   19: putstatic com/oplus/network/OplusNetworkStackManager.sInstance : Lcom/oplus/network/OplusNetworkStackManager;
    //   22: ldc 'OplusNetworkStackManager'
    //   24: ldc 'OplusNetworkStackManager first new!'
    //   26: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   29: pop
    //   30: getstatic com/oplus/network/OplusNetworkStackManager.sInstance : Lcom/oplus/network/OplusNetworkStackManager;
    //   33: astore_0
    //   34: ldc com/oplus/network/OplusNetworkStackManager
    //   36: monitorexit
    //   37: aload_0
    //   38: areturn
    //   39: astore_0
    //   40: ldc com/oplus/network/OplusNetworkStackManager
    //   42: monitorexit
    //   43: aload_0
    //   44: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #141	-> 0
    //   #142	-> 3
    //   #143	-> 9
    //   #144	-> 22
    //   #146	-> 30
    //   #147	-> 39
    // Exception table:
    //   from	to	target	type
    //   3	9	39	finally
    //   9	22	39	finally
    //   22	30	39	finally
    //   30	37	39	finally
    //   40	43	39	finally
  }
  
  protected OplusNetworkStackManager(Context paramContext) {
    this.mContext = paramContext;
    IOplusNetworkStack iOplusNetworkStack = IOplusNetworkStack.Stub.asInterface(ServiceManager.getService("oplusnetworkstack"));
    if (iOplusNetworkStack != null) {
      try {
        Log.e("OplusNetworkStackManager", "registerTcpScoreChange start!");
        iOplusNetworkStack = this.mNetworkStackService;
        MyCallBack myCallBack = new MyCallBack();
        this(this);
        iOplusNetworkStack.registerTcpScoreChange(myCallBack);
      } catch (RemoteException remoteException) {
        Log.e("OplusNetworkStackManager", "registerTcpScoreChange fail!");
      } 
    } else {
      Log.e("OplusNetworkStackManager", "mNetworkStackService is null!");
    } 
  }
  
  private void logFunc(String paramString) {
    if (this.showDebug)
      Log.e("OplusNetworkStackManager", paramString); 
  }
}
