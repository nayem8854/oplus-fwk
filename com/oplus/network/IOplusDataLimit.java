package com.oplus.network;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusDataLimit extends IInterface {
  boolean disableLimitBgData() throws RemoteException;
  
  boolean enableLimitBgData(int paramInt) throws RemoteException;
  
  int getBackgroundDLRate() throws RemoteException;
  
  int getBackgroundULRate() throws RemoteException;
  
  int getBgLimitStatus() throws RemoteException;
  
  int getForegroundDLRate() throws RemoteException;
  
  int getForegroundULRate() throws RemoteException;
  
  class Default implements IOplusDataLimit {
    public int getBgLimitStatus() throws RemoteException {
      return 0;
    }
    
    public boolean enableLimitBgData(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean disableLimitBgData() throws RemoteException {
      return false;
    }
    
    public int getBackgroundDLRate() throws RemoteException {
      return 0;
    }
    
    public int getBackgroundULRate() throws RemoteException {
      return 0;
    }
    
    public int getForegroundDLRate() throws RemoteException {
      return 0;
    }
    
    public int getForegroundULRate() throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusDataLimit {
    private static final String DESCRIPTOR = "com.oplus.network.IOplusDataLimit";
    
    static final int TRANSACTION_disableLimitBgData = 3;
    
    static final int TRANSACTION_enableLimitBgData = 2;
    
    static final int TRANSACTION_getBackgroundDLRate = 4;
    
    static final int TRANSACTION_getBackgroundULRate = 5;
    
    static final int TRANSACTION_getBgLimitStatus = 1;
    
    static final int TRANSACTION_getForegroundDLRate = 6;
    
    static final int TRANSACTION_getForegroundULRate = 7;
    
    public Stub() {
      attachInterface(this, "com.oplus.network.IOplusDataLimit");
    }
    
    public static IOplusDataLimit asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.network.IOplusDataLimit");
      if (iInterface != null && iInterface instanceof IOplusDataLimit)
        return (IOplusDataLimit)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "getForegroundULRate";
        case 6:
          return "getForegroundDLRate";
        case 5:
          return "getBackgroundULRate";
        case 4:
          return "getBackgroundDLRate";
        case 3:
          return "disableLimitBgData";
        case 2:
          return "enableLimitBgData";
        case 1:
          break;
      } 
      return "getBgLimitStatus";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int j;
        boolean bool1;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("com.oplus.network.IOplusDataLimit");
            param1Int1 = getForegroundULRate();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("com.oplus.network.IOplusDataLimit");
            param1Int1 = getForegroundDLRate();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("com.oplus.network.IOplusDataLimit");
            param1Int1 = getBackgroundULRate();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("com.oplus.network.IOplusDataLimit");
            param1Int1 = getBackgroundDLRate();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 3:
            param1Parcel1.enforceInterface("com.oplus.network.IOplusDataLimit");
            bool2 = disableLimitBgData();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 2:
            param1Parcel1.enforceInterface("com.oplus.network.IOplusDataLimit");
            j = param1Parcel1.readInt();
            bool1 = enableLimitBgData(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("com.oplus.network.IOplusDataLimit");
        int i = getBgLimitStatus();
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(i);
        return true;
      } 
      param1Parcel2.writeString("com.oplus.network.IOplusDataLimit");
      return true;
    }
    
    private static class Proxy implements IOplusDataLimit {
      public static IOplusDataLimit sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.network.IOplusDataLimit";
      }
      
      public int getBgLimitStatus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.network.IOplusDataLimit");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusDataLimit.Stub.getDefaultImpl() != null)
            return IOplusDataLimit.Stub.getDefaultImpl().getBgLimitStatus(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean enableLimitBgData(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.network.IOplusDataLimit");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IOplusDataLimit.Stub.getDefaultImpl() != null) {
            bool1 = IOplusDataLimit.Stub.getDefaultImpl().enableLimitBgData(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean disableLimitBgData() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.network.IOplusDataLimit");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IOplusDataLimit.Stub.getDefaultImpl() != null) {
            bool1 = IOplusDataLimit.Stub.getDefaultImpl().disableLimitBgData();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getBackgroundDLRate() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.network.IOplusDataLimit");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOplusDataLimit.Stub.getDefaultImpl() != null)
            return IOplusDataLimit.Stub.getDefaultImpl().getBackgroundDLRate(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getBackgroundULRate() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.network.IOplusDataLimit");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOplusDataLimit.Stub.getDefaultImpl() != null)
            return IOplusDataLimit.Stub.getDefaultImpl().getBackgroundULRate(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getForegroundDLRate() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.network.IOplusDataLimit");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IOplusDataLimit.Stub.getDefaultImpl() != null)
            return IOplusDataLimit.Stub.getDefaultImpl().getForegroundDLRate(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getForegroundULRate() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.network.IOplusDataLimit");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IOplusDataLimit.Stub.getDefaultImpl() != null)
            return IOplusDataLimit.Stub.getDefaultImpl().getForegroundULRate(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusDataLimit param1IOplusDataLimit) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusDataLimit != null) {
          Proxy.sDefaultImpl = param1IOplusDataLimit;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusDataLimit getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
