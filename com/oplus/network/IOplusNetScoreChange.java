package com.oplus.network;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusNetScoreChange extends IInterface {
  void networkScoreChange(boolean paramBoolean, int paramInt) throws RemoteException;
  
  class Default implements IOplusNetScoreChange {
    public void networkScoreChange(boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusNetScoreChange {
    private static final String DESCRIPTOR = "com.oplus.network.IOplusNetScoreChange";
    
    static final int TRANSACTION_networkScoreChange = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.network.IOplusNetScoreChange");
    }
    
    public static IOplusNetScoreChange asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.network.IOplusNetScoreChange");
      if (iInterface != null && iInterface instanceof IOplusNetScoreChange)
        return (IOplusNetScoreChange)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "networkScoreChange";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.network.IOplusNetScoreChange");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.network.IOplusNetScoreChange");
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      param1Int1 = param1Parcel1.readInt();
      networkScoreChange(bool, param1Int1);
      return true;
    }
    
    private static class Proxy implements IOplusNetScoreChange {
      public static IOplusNetScoreChange sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.network.IOplusNetScoreChange";
      }
      
      public void networkScoreChange(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.oplus.network.IOplusNetScoreChange");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && IOplusNetScoreChange.Stub.getDefaultImpl() != null) {
            IOplusNetScoreChange.Stub.getDefaultImpl().networkScoreChange(param2Boolean, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusNetScoreChange param1IOplusNetScoreChange) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusNetScoreChange != null) {
          Proxy.sDefaultImpl = param1IOplusNetScoreChange;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusNetScoreChange getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
