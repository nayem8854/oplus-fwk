package com.oplus.network;

import android.content.Context;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;

public class OplusDataLimitManager {
  public static final boolean DBG = true;
  
  public static final String LOG_TAG = "OplusDataLimitManager";
  
  public static final String SRV_NAME = "oplusdatalimit";
  
  private static OplusDataLimitManager sInstance;
  
  private Context mContext;
  
  private IOplusDataLimit mOplusDataLimitService;
  
  public boolean enableLimitBgData(int paramInt) {
    IOplusDataLimit iOplusDataLimit = this.mOplusDataLimitService;
    if (iOplusDataLimit != null) {
      try {
        boolean bool = iOplusDataLimit.enableLimitBgData(paramInt);
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("enableLimitBgData result = ");
        stringBuilder.append(bool);
        Log.e("OplusDataLimitManager", stringBuilder.toString());
        return bool;
      } catch (RemoteException remoteException) {
        Log.e("OplusDataLimitManager", "enableLimitBgData fail!");
      } 
    } else {
      Log.e("OplusDataLimitManager", "enableLimitBgData fail, mOplusDataLimitService is null!");
    } 
    Log.e("OplusDataLimitManager", "enableLimitBgData is error rerurn 0!");
    return false;
  }
  
  public boolean disableLimitBgData() {
    IOplusDataLimit iOplusDataLimit = this.mOplusDataLimitService;
    if (iOplusDataLimit != null) {
      try {
        boolean bool = iOplusDataLimit.disableLimitBgData();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("disableLimitBgData result = ");
        stringBuilder.append(bool);
        Log.e("OplusDataLimitManager", stringBuilder.toString());
        return bool;
      } catch (RemoteException remoteException) {
        Log.e("OplusDataLimitManager", "disableLimitBgData fail!");
      } 
    } else {
      Log.e("OplusDataLimitManager", "disableLimitBgData fail, mOplusDataLimitService is null!");
    } 
    Log.e("OplusDataLimitManager", "disableLimitBgData is error rerurn 0!");
    return false;
  }
  
  public int getBgLimitStatus() {
    IOplusDataLimit iOplusDataLimit = this.mOplusDataLimitService;
    if (iOplusDataLimit != null) {
      try {
        int i = iOplusDataLimit.getBgLimitStatus();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("getBgLimitStatus result = ");
        stringBuilder.append(i);
        Log.e("OplusDataLimitManager", stringBuilder.toString());
        return i;
      } catch (RemoteException remoteException) {
        Log.e("OplusDataLimitManager", "getBgLimitStatus fail!");
      } 
    } else {
      Log.e("OplusDataLimitManager", "getBgLimitStatus fail, mOplusDataLimitService is null!");
    } 
    Log.e("OplusDataLimitManager", "getBgLimitStatus is error rerurn 0!");
    return 0;
  }
  
  public static OplusDataLimitManager getInstance(Context paramContext) {
    // Byte code:
    //   0: ldc com/oplus/network/OplusDataLimitManager
    //   2: monitorenter
    //   3: getstatic com/oplus/network/OplusDataLimitManager.sInstance : Lcom/oplus/network/OplusDataLimitManager;
    //   6: ifnonnull -> 30
    //   9: new com/oplus/network/OplusDataLimitManager
    //   12: astore_1
    //   13: aload_1
    //   14: aload_0
    //   15: invokespecial <init> : (Landroid/content/Context;)V
    //   18: aload_1
    //   19: putstatic com/oplus/network/OplusDataLimitManager.sInstance : Lcom/oplus/network/OplusDataLimitManager;
    //   22: ldc 'OplusDataLimitManager'
    //   24: ldc 'OplusDataLimitManager first new!'
    //   26: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   29: pop
    //   30: getstatic com/oplus/network/OplusDataLimitManager.sInstance : Lcom/oplus/network/OplusDataLimitManager;
    //   33: astore_0
    //   34: ldc com/oplus/network/OplusDataLimitManager
    //   36: monitorexit
    //   37: aload_0
    //   38: areturn
    //   39: astore_0
    //   40: ldc com/oplus/network/OplusDataLimitManager
    //   42: monitorexit
    //   43: aload_0
    //   44: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #72	-> 0
    //   #73	-> 3
    //   #74	-> 9
    //   #75	-> 22
    //   #77	-> 30
    //   #78	-> 39
    // Exception table:
    //   from	to	target	type
    //   3	9	39	finally
    //   9	22	39	finally
    //   22	30	39	finally
    //   30	37	39	finally
    //   40	43	39	finally
  }
  
  protected OplusDataLimitManager(Context paramContext) {
    this.mContext = paramContext;
    IOplusDataLimit iOplusDataLimit = IOplusDataLimit.Stub.asInterface(ServiceManager.getService("oplusdatalimit"));
    if (iOplusDataLimit != null) {
      try {
        Log.e("OplusDataLimitManager", "OplusDataLimitManager init.");
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Init. getBackgroundDLRate = ");
        stringBuilder.append(this.mOplusDataLimitService.getBackgroundDLRate());
        Log.e("OplusDataLimitManager", stringBuilder.toString());
      } catch (RemoteException remoteException) {
        Log.e("OplusDataLimitManager", "OplusDataLimitManager init fail!");
      } 
    } else {
      Log.e("OplusDataLimitManager", "mOplusDataLimitService is null!");
    } 
  }
}
