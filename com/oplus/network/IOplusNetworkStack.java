package com.oplus.network;

import android.net.Network;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusNetworkStack extends IInterface {
  boolean disableDUALWIFIUid(int paramInt) throws RemoteException;
  
  boolean disableMPTCPUid(int paramInt) throws RemoteException;
  
  boolean disableSLAPUid(int paramInt) throws RemoteException;
  
  boolean enableDUALWIFIUid(int paramInt) throws RemoteException;
  
  boolean enableMPTCPUid(int paramInt) throws RemoteException;
  
  boolean enableSLAUid(int paramInt) throws RemoteException;
  
  void enableScreenshotDetect() throws RemoteException;
  
  int getNetworkScore(Network paramNetwork) throws RemoteException;
  
  String getOplusNetworkStackInfo() throws RemoteException;
  
  int getPortalResult(Network paramNetwork, int paramInt) throws RemoteException;
  
  boolean isGatewayConflict(Network paramNetwork) throws RemoteException;
  
  void registerTcpScoreChange(IOplusNetScoreChange paramIOplusNetScoreChange) throws RemoteException;
  
  void setOplusNetworkStackConfig(String paramString) throws RemoteException;
  
  void unregisterTcpScoreChange(IOplusNetScoreChange paramIOplusNetScoreChange) throws RemoteException;
  
  class Default implements IOplusNetworkStack {
    public void registerTcpScoreChange(IOplusNetScoreChange param1IOplusNetScoreChange) throws RemoteException {}
    
    public void unregisterTcpScoreChange(IOplusNetScoreChange param1IOplusNetScoreChange) throws RemoteException {}
    
    public int getPortalResult(Network param1Network, int param1Int) throws RemoteException {
      return 0;
    }
    
    public boolean isGatewayConflict(Network param1Network) throws RemoteException {
      return false;
    }
    
    public void enableScreenshotDetect() throws RemoteException {}
    
    public boolean enableMPTCPUid(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean disableMPTCPUid(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean enableSLAUid(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean disableSLAPUid(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean enableDUALWIFIUid(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean disableDUALWIFIUid(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getNetworkScore(Network param1Network) throws RemoteException {
      return 0;
    }
    
    public String getOplusNetworkStackInfo() throws RemoteException {
      return null;
    }
    
    public void setOplusNetworkStackConfig(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusNetworkStack {
    private static final String DESCRIPTOR = "com.oplus.network.IOplusNetworkStack";
    
    static final int TRANSACTION_disableDUALWIFIUid = 11;
    
    static final int TRANSACTION_disableMPTCPUid = 7;
    
    static final int TRANSACTION_disableSLAPUid = 9;
    
    static final int TRANSACTION_enableDUALWIFIUid = 10;
    
    static final int TRANSACTION_enableMPTCPUid = 6;
    
    static final int TRANSACTION_enableSLAUid = 8;
    
    static final int TRANSACTION_enableScreenshotDetect = 5;
    
    static final int TRANSACTION_getNetworkScore = 12;
    
    static final int TRANSACTION_getOplusNetworkStackInfo = 13;
    
    static final int TRANSACTION_getPortalResult = 3;
    
    static final int TRANSACTION_isGatewayConflict = 4;
    
    static final int TRANSACTION_registerTcpScoreChange = 1;
    
    static final int TRANSACTION_setOplusNetworkStackConfig = 14;
    
    static final int TRANSACTION_unregisterTcpScoreChange = 2;
    
    public Stub() {
      attachInterface(this, "com.oplus.network.IOplusNetworkStack");
    }
    
    public static IOplusNetworkStack asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.network.IOplusNetworkStack");
      if (iInterface != null && iInterface instanceof IOplusNetworkStack)
        return (IOplusNetworkStack)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 14:
          return "setOplusNetworkStackConfig";
        case 13:
          return "getOplusNetworkStackInfo";
        case 12:
          return "getNetworkScore";
        case 11:
          return "disableDUALWIFIUid";
        case 10:
          return "enableDUALWIFIUid";
        case 9:
          return "disableSLAPUid";
        case 8:
          return "enableSLAUid";
        case 7:
          return "disableMPTCPUid";
        case 6:
          return "enableMPTCPUid";
        case 5:
          return "enableScreenshotDetect";
        case 4:
          return "isGatewayConflict";
        case 3:
          return "getPortalResult";
        case 2:
          return "unregisterTcpScoreChange";
        case 1:
          break;
      } 
      return "registerTcpScoreChange";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool6;
        int i1;
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        int i;
        String str;
        Network network;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 14:
            param1Parcel1.enforceInterface("com.oplus.network.IOplusNetworkStack");
            str = param1Parcel1.readString();
            setOplusNetworkStackConfig(str);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            str.enforceInterface("com.oplus.network.IOplusNetworkStack");
            str = getOplusNetworkStackInfo();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str);
            return true;
          case 12:
            str.enforceInterface("com.oplus.network.IOplusNetworkStack");
            if (str.readInt() != 0) {
              Network network1 = (Network)Network.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            param1Int1 = getNetworkScore((Network)str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 11:
            str.enforceInterface("com.oplus.network.IOplusNetworkStack");
            param1Int1 = str.readInt();
            bool6 = disableDUALWIFIUid(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 10:
            str.enforceInterface("com.oplus.network.IOplusNetworkStack");
            i1 = str.readInt();
            bool5 = enableDUALWIFIUid(i1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 9:
            str.enforceInterface("com.oplus.network.IOplusNetworkStack");
            n = str.readInt();
            bool4 = disableSLAPUid(n);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 8:
            str.enforceInterface("com.oplus.network.IOplusNetworkStack");
            m = str.readInt();
            bool3 = enableSLAUid(m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 7:
            str.enforceInterface("com.oplus.network.IOplusNetworkStack");
            k = str.readInt();
            bool2 = disableMPTCPUid(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 6:
            str.enforceInterface("com.oplus.network.IOplusNetworkStack");
            j = str.readInt();
            bool1 = enableMPTCPUid(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 5:
            str.enforceInterface("com.oplus.network.IOplusNetworkStack");
            enableScreenshotDetect();
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str.enforceInterface("com.oplus.network.IOplusNetworkStack");
            if (str.readInt() != 0) {
              Network network1 = (Network)Network.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            bool1 = isGatewayConflict((Network)str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 3:
            str.enforceInterface("com.oplus.network.IOplusNetworkStack");
            if (str.readInt() != 0) {
              network = (Network)Network.CREATOR.createFromParcel((Parcel)str);
            } else {
              network = null;
            } 
            i = str.readInt();
            i = getPortalResult(network, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 2:
            str.enforceInterface("com.oplus.network.IOplusNetworkStack");
            iOplusNetScoreChange = IOplusNetScoreChange.Stub.asInterface(str.readStrongBinder());
            unregisterTcpScoreChange(iOplusNetScoreChange);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        iOplusNetScoreChange.enforceInterface("com.oplus.network.IOplusNetworkStack");
        IOplusNetScoreChange iOplusNetScoreChange = IOplusNetScoreChange.Stub.asInterface(iOplusNetScoreChange.readStrongBinder());
        registerTcpScoreChange(iOplusNetScoreChange);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("com.oplus.network.IOplusNetworkStack");
      return true;
    }
    
    private static class Proxy implements IOplusNetworkStack {
      public static IOplusNetworkStack sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.network.IOplusNetworkStack";
      }
      
      public void registerTcpScoreChange(IOplusNetScoreChange param2IOplusNetScoreChange) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oplus.network.IOplusNetworkStack");
          if (param2IOplusNetScoreChange != null) {
            iBinder = param2IOplusNetScoreChange.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusNetworkStack.Stub.getDefaultImpl() != null) {
            IOplusNetworkStack.Stub.getDefaultImpl().registerTcpScoreChange(param2IOplusNetScoreChange);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterTcpScoreChange(IOplusNetScoreChange param2IOplusNetScoreChange) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oplus.network.IOplusNetworkStack");
          if (param2IOplusNetScoreChange != null) {
            iBinder = param2IOplusNetScoreChange.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusNetworkStack.Stub.getDefaultImpl() != null) {
            IOplusNetworkStack.Stub.getDefaultImpl().unregisterTcpScoreChange(param2IOplusNetScoreChange);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPortalResult(Network param2Network, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.network.IOplusNetworkStack");
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOplusNetworkStack.Stub.getDefaultImpl() != null) {
            param2Int = IOplusNetworkStack.Stub.getDefaultImpl().getPortalResult(param2Network, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isGatewayConflict(Network param2Network) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.network.IOplusNetworkStack");
          boolean bool1 = true;
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IOplusNetworkStack.Stub.getDefaultImpl() != null) {
            bool1 = IOplusNetworkStack.Stub.getDefaultImpl().isGatewayConflict(param2Network);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enableScreenshotDetect() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.network.IOplusNetworkStack");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOplusNetworkStack.Stub.getDefaultImpl() != null) {
            IOplusNetworkStack.Stub.getDefaultImpl().enableScreenshotDetect();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean enableMPTCPUid(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.network.IOplusNetworkStack");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IOplusNetworkStack.Stub.getDefaultImpl() != null) {
            bool1 = IOplusNetworkStack.Stub.getDefaultImpl().enableMPTCPUid(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean disableMPTCPUid(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.network.IOplusNetworkStack");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IOplusNetworkStack.Stub.getDefaultImpl() != null) {
            bool1 = IOplusNetworkStack.Stub.getDefaultImpl().disableMPTCPUid(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean enableSLAUid(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.network.IOplusNetworkStack");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(8, parcel1, parcel2, 0);
          if (!bool2 && IOplusNetworkStack.Stub.getDefaultImpl() != null) {
            bool1 = IOplusNetworkStack.Stub.getDefaultImpl().enableSLAUid(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean disableSLAPUid(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.network.IOplusNetworkStack");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IOplusNetworkStack.Stub.getDefaultImpl() != null) {
            bool1 = IOplusNetworkStack.Stub.getDefaultImpl().disableSLAPUid(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean enableDUALWIFIUid(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.network.IOplusNetworkStack");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
          if (!bool2 && IOplusNetworkStack.Stub.getDefaultImpl() != null) {
            bool1 = IOplusNetworkStack.Stub.getDefaultImpl().enableDUALWIFIUid(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean disableDUALWIFIUid(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.network.IOplusNetworkStack");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(11, parcel1, parcel2, 0);
          if (!bool2 && IOplusNetworkStack.Stub.getDefaultImpl() != null) {
            bool1 = IOplusNetworkStack.Stub.getDefaultImpl().disableDUALWIFIUid(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getNetworkScore(Network param2Network) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.network.IOplusNetworkStack");
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IOplusNetworkStack.Stub.getDefaultImpl() != null)
            return IOplusNetworkStack.Stub.getDefaultImpl().getNetworkScore(param2Network); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getOplusNetworkStackInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.network.IOplusNetworkStack");
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IOplusNetworkStack.Stub.getDefaultImpl() != null)
            return IOplusNetworkStack.Stub.getDefaultImpl().getOplusNetworkStackInfo(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setOplusNetworkStackConfig(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.network.IOplusNetworkStack");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IOplusNetworkStack.Stub.getDefaultImpl() != null) {
            IOplusNetworkStack.Stub.getDefaultImpl().setOplusNetworkStackConfig(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusNetworkStack param1IOplusNetworkStack) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusNetworkStack != null) {
          Proxy.sDefaultImpl = param1IOplusNetworkStack;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusNetworkStack getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
