package com.oplus.kinect;

import android.content.ComponentName;

public abstract class GestureStateCallback {
  public static final int MSG_NOTIFYRESULT = 0;
  
  public static final int MSG_SERVICEDISCONNECTED = 1;
  
  public IRemoteServiceCallback mCallback = (IRemoteServiceCallback)new Object(this);
  
  public void notifyResult(int[] paramArrayOfint) {}
  
  public void onServiceDisconnected(ComponentName paramComponentName) {}
}
