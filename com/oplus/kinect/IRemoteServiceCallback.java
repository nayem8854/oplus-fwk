package com.oplus.kinect;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IRemoteServiceCallback extends IInterface {
  void notifyResult(int[] paramArrayOfint) throws RemoteException;
  
  class Default implements IRemoteServiceCallback {
    public void notifyResult(int[] param1ArrayOfint) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRemoteServiceCallback {
    private static final String DESCRIPTOR = "com.oplus.kinect.IRemoteServiceCallback";
    
    static final int TRANSACTION_notifyResult = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.kinect.IRemoteServiceCallback");
    }
    
    public static IRemoteServiceCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.kinect.IRemoteServiceCallback");
      if (iInterface != null && iInterface instanceof IRemoteServiceCallback)
        return (IRemoteServiceCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "notifyResult";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.kinect.IRemoteServiceCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.kinect.IRemoteServiceCallback");
      int[] arrayOfInt = param1Parcel1.createIntArray();
      notifyResult(arrayOfInt);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IRemoteServiceCallback {
      public static IRemoteServiceCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.kinect.IRemoteServiceCallback";
      }
      
      public void notifyResult(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.kinect.IRemoteServiceCallback");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IRemoteServiceCallback.Stub.getDefaultImpl() != null) {
            IRemoteServiceCallback.Stub.getDefaultImpl().notifyResult(param2ArrayOfint);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRemoteServiceCallback param1IRemoteServiceCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRemoteServiceCallback != null) {
          Proxy.sDefaultImpl = param1IRemoteServiceCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRemoteServiceCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
