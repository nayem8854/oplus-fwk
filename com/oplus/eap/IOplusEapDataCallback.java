package com.oplus.eap;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.SharedMemory;

public interface IOplusEapDataCallback extends IInterface {
  void onAppCrashed(SharedMemory paramSharedMemory) throws RemoteException;
  
  class Default implements IOplusEapDataCallback {
    public void onAppCrashed(SharedMemory param1SharedMemory) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusEapDataCallback {
    private static final String DESCRIPTOR = "com.oplus.eap.IOplusEapDataCallback";
    
    static final int TRANSACTION_onAppCrashed = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.eap.IOplusEapDataCallback");
    }
    
    public static IOplusEapDataCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.eap.IOplusEapDataCallback");
      if (iInterface != null && iInterface instanceof IOplusEapDataCallback)
        return (IOplusEapDataCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onAppCrashed";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.eap.IOplusEapDataCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.eap.IOplusEapDataCallback");
      if (param1Parcel1.readInt() != 0) {
        SharedMemory sharedMemory = (SharedMemory)SharedMemory.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onAppCrashed((SharedMemory)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IOplusEapDataCallback {
      public static IOplusEapDataCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.eap.IOplusEapDataCallback";
      }
      
      public void onAppCrashed(SharedMemory param2SharedMemory) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.eap.IOplusEapDataCallback");
          if (param2SharedMemory != null) {
            parcel.writeInt(1);
            param2SharedMemory.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusEapDataCallback.Stub.getDefaultImpl() != null) {
            IOplusEapDataCallback.Stub.getDefaultImpl().onAppCrashed(param2SharedMemory);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusEapDataCallback param1IOplusEapDataCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusEapDataCallback != null) {
          Proxy.sDefaultImpl = param1IOplusEapDataCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusEapDataCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
