package com.oplus.miragewindow;

import android.app.OplusActivityManager;
import android.content.ComponentName;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.util.Slog;
import android.view.Surface;
import java.util.List;

public class OplusMirageWindowManager {
  public static final int DISPLAY_ID = 2020;
  
  public static final String DISPLAY_NAME = "Mirage_display";
  
  public static final int FLAG_MASK = 65535;
  
  public static final int FLAG_POWER_SAVE = 1;
  
  public static final int FLAG_PRIVACY_PROTECTION = 16;
  
  public static final String IS_DEFAULT = "is_default";
  
  public static final int MODE_MASK = -65536;
  
  public static final int MODE_MIRROR_CAST = 16777216;
  
  public static final int MODE_SINGLE_APP_CAST = 33554432;
  
  public static final float Mirage_CORNER_RADIUS = 30.0F;
  
  private static final String TAG = "OplusMirageWindowManager";
  
  private static volatile OplusMirageWindowManager sInstance;
  
  private OplusActivityManager mAms;
  
  private IOplusMirageWindowSession mSession;
  
  public static OplusMirageWindowManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/miragewindow/OplusMirageWindowManager.sInstance : Lcom/oplus/miragewindow/OplusMirageWindowManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/miragewindow/OplusMirageWindowManager
    //   8: monitorenter
    //   9: getstatic com/oplus/miragewindow/OplusMirageWindowManager.sInstance : Lcom/oplus/miragewindow/OplusMirageWindowManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/miragewindow/OplusMirageWindowManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/miragewindow/OplusMirageWindowManager.sInstance : Lcom/oplus/miragewindow/OplusMirageWindowManager;
    //   27: ldc com/oplus/miragewindow/OplusMirageWindowManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/miragewindow/OplusMirageWindowManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/miragewindow/OplusMirageWindowManager.sInstance : Lcom/oplus/miragewindow/OplusMirageWindowManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #53	-> 0
    //   #54	-> 6
    //   #55	-> 9
    //   #56	-> 15
    //   #58	-> 27
    //   #60	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  private OplusMirageWindowManager() {
    OplusActivityManager oplusActivityManager = new OplusActivityManager();
    try {
      Object object = new Object();
      super(this);
      this.mSession = oplusActivityManager.createMirageWindowSession((IOplusMirageSessionCallback)object);
    } catch (RemoteException remoteException) {
      Log.e("OplusMirageWindowManager", "create session remoteException ");
    } 
  }
  
  public void startMirageWindowMode(ComponentName paramComponentName, int paramInt1, int paramInt2, Bundle paramBundle) {
    try {
      this.mAms.startMirageWindowMode(paramComponentName, paramInt1, paramInt2, paramBundle);
    } catch (RemoteException remoteException) {
      Log.e("OplusMirageWindowManager", "startMirageWindowMode remoteException ");
      remoteException.printStackTrace();
    } 
  }
  
  public boolean isMirageWindowShow() {
    try {
      return this.mAms.isMirageWindowShow();
    } catch (RemoteException remoteException) {
      Log.e("OplusMirageWindowManager", "isMirageWindowShow remoteException ");
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public void stopMirageWindowMode() {
    try {
      this.mAms.stopMirageWindowMode();
    } catch (RemoteException remoteException) {
      Log.e("OplusMirageWindowManager", "stopMirageWindowMode remoteException ");
      remoteException.printStackTrace();
    } 
  }
  
  public void expandToFullScreen() {
    try {
      this.mAms.expandToFullScreen();
    } catch (RemoteException remoteException) {
      Log.e("OplusMirageWindowManager", "expandTofullscreen remoteException ");
      remoteException.printStackTrace();
    } 
  }
  
  public void setMirageWindowSilent(String paramString) {
    try {
      this.mAms.setMirageWindowSilent(paramString);
    } catch (RemoteException remoteException) {
      Log.e("OplusMirageWindowManager", "setMirageWindowSilent remoteException ");
      remoteException.printStackTrace();
    } 
  }
  
  public int createMirageDisplay(Surface paramSurface) {
    try {
      return this.mAms.createMirageDisplay(paramSurface);
    } catch (RemoteException remoteException) {
      Slog.e("OplusMirageWindowManager", "Failed to createMirageDisplay: remoteException");
      remoteException.printStackTrace();
      return -1;
    } 
  }
  
  public boolean isSupportMirageWindowMode() {
    try {
      return this.mAms.isSupportMirageWindowMode();
    } catch (RemoteException remoteException) {
      Log.e("OplusMirageWindowManager", "isSupportMirageWindowMode remoteException ");
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public OplusMirageWindowInfo getMirageWindowInfo() {
    try {
      return this.mAms.getMirageWindowInfo();
    } catch (RemoteException remoteException) {
      Log.e("OplusMirageWindowManager", "getMirageWindowInfo remoteException ");
      remoteException.printStackTrace();
      return new OplusMirageWindowInfo();
    } 
  }
  
  public boolean registerMirageWindowObserver(IOplusMirageWindowObserver paramIOplusMirageWindowObserver) {
    try {
      return this.mAms.registerMirageWindowObserver(paramIOplusMirageWindowObserver);
    } catch (RemoteException remoteException) {
      Slog.e("OplusMirageWindowManager", "Failed to registerMirageWindowObserver: remoteException");
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public boolean unregisterMirageWindowObserver(IOplusMirageWindowObserver paramIOplusMirageWindowObserver) {
    try {
      return this.mAms.unregisterMirageWindowObserver(paramIOplusMirageWindowObserver);
    } catch (RemoteException remoteException) {
      Slog.e("OplusMirageWindowManager", "Failed to unregisterMirageWindowObserver: remoteException");
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public boolean updateMirageWindowCastFlag(int paramInt) {
    return updateMirageWindowCastFlag(paramInt, null);
  }
  
  public boolean updateMirageWindowCastFlag(int paramInt, Bundle paramBundle) {
    try {
      return this.mAms.updateMirageWindowCastFlag(paramInt, paramBundle);
    } catch (RemoteException remoteException) {
      Log.e("OplusMirageWindowManager", "updateMirageWindowCastFlag remoteException ");
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public boolean updatePrivacyProtectionList(List<String> paramList, boolean paramBoolean) {
    try {
      return this.mAms.updatePrivacyProtectionList(paramList, paramBoolean);
    } catch (RemoteException remoteException) {
      Log.e("OplusMirageWindowManager", "updatePrivacyProtectionList remoteException ");
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public boolean updatePrivacyProtectionList(List<String> paramList, boolean paramBoolean1, boolean paramBoolean2, Bundle paramBundle) {
    try {
      return this.mAms.updatePrivacyProtectionList(paramList, paramBoolean1, paramBoolean2, paramBundle);
    } catch (RemoteException remoteException) {
      Log.e("OplusMirageWindowManager", "updatePrivacyProtectionList default remoteException ");
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public void addCastScreenState(OplusCastScreenState paramOplusCastScreenState) {
    /* monitor enter TypeReferenceDotClassExpression{ObjectType{com/oplus/miragewindow/OplusMirageWindowManager}} */
    try {
      if (this.mSession == null || paramOplusCastScreenState == null) {
        /* monitor exit TypeReferenceDotClassExpression{ObjectType{com/oplus/miragewindow/OplusMirageWindowManager}} */
        return;
      } 
      this.mSession.addCastScreenState(paramOplusCastScreenState);
    } catch (RemoteException remoteException) {
      Log.e("OplusMirageWindowManager", "addCastScreenState remoteException ");
    } finally {}
    /* monitor exit TypeReferenceDotClassExpression{ObjectType{com/oplus/miragewindow/OplusMirageWindowManager}} */
  }
  
  public void removeCastScreenState() {
    /* monitor enter TypeReferenceDotClassExpression{ObjectType{com/oplus/miragewindow/OplusMirageWindowManager}} */
    try {
      IOplusMirageWindowSession iOplusMirageWindowSession = this.mSession;
      if (iOplusMirageWindowSession == null) {
        /* monitor exit TypeReferenceDotClassExpression{ObjectType{com/oplus/miragewindow/OplusMirageWindowManager}} */
        return;
      } 
      this.mSession.removeCastScreenState();
    } catch (RemoteException remoteException) {
      Log.e("OplusMirageWindowManager", "removeCastScreenState remoteException ");
    } finally {
      Exception exception;
    } 
    /* monitor exit TypeReferenceDotClassExpression{ObjectType{com/oplus/miragewindow/OplusMirageWindowManager}} */
  }
  
  public List<OplusCastScreenState> getCastScreenStateList() {
    Exception exception;
    /* monitor enter TypeReferenceDotClassExpression{ObjectType{com/oplus/miragewindow/OplusMirageWindowManager}} */
    try {
      IOplusMirageWindowSession iOplusMirageWindowSession = this.mSession;
      if (iOplusMirageWindowSession == null) {
        /* monitor exit TypeReferenceDotClassExpression{ObjectType{com/oplus/miragewindow/OplusMirageWindowManager}} */
        return null;
      } 
      List<OplusCastScreenState> list = this.mSession.getCastScreenStateList();
      /* monitor exit TypeReferenceDotClassExpression{ObjectType{com/oplus/miragewindow/OplusMirageWindowManager}} */
      return list;
    } catch (RemoteException remoteException) {
      Log.e("OplusMirageWindowManager", "getCastScreenStateList remoteException ");
      /* monitor exit TypeReferenceDotClassExpression{ObjectType{com/oplus/miragewindow/OplusMirageWindowManager}} */
      return null;
    } finally {}
    /* monitor exit TypeReferenceDotClassExpression{ObjectType{com/oplus/miragewindow/OplusMirageWindowManager}} */
    throw exception;
  }
  
  public boolean registerCastScreenStateObserver(IOplusCastScreenStateObserver paramIOplusCastScreenStateObserver) {
    try {
      if (this.mSession == null || paramIOplusCastScreenStateObserver == null)
        return false; 
      return this.mSession.registerCastScreenStateObserver(paramIOplusCastScreenStateObserver);
    } catch (RemoteException remoteException) {
      Slog.e("OplusMirageWindowManager", "Failed to registerCastScreenStateObserver: remoteException");
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public boolean unregisterCastScreenStateObserver(IOplusCastScreenStateObserver paramIOplusCastScreenStateObserver) {
    try {
      if (this.mSession == null || paramIOplusCastScreenStateObserver == null)
        return false; 
      return this.mSession.unregisterCastScreenStateObserver(paramIOplusCastScreenStateObserver);
    } catch (RemoteException remoteException) {
      Slog.e("OplusMirageWindowManager", "Failed to unregisterCastScreenStateObserver: remoteException");
      remoteException.printStackTrace();
      return false;
    } 
  }
}
