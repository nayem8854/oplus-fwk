package com.oplus.miragewindow;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IOplusMirageWindowSession extends IInterface {
  void addCastScreenState(OplusCastScreenState paramOplusCastScreenState) throws RemoteException;
  
  List<OplusCastScreenState> getCastScreenStateList() throws RemoteException;
  
  boolean registerCastScreenStateObserver(IOplusCastScreenStateObserver paramIOplusCastScreenStateObserver) throws RemoteException;
  
  void removeCastScreenState() throws RemoteException;
  
  boolean unregisterCastScreenStateObserver(IOplusCastScreenStateObserver paramIOplusCastScreenStateObserver) throws RemoteException;
  
  class Default implements IOplusMirageWindowSession {
    public void addCastScreenState(OplusCastScreenState param1OplusCastScreenState) throws RemoteException {}
    
    public List<OplusCastScreenState> getCastScreenStateList() throws RemoteException {
      return null;
    }
    
    public void removeCastScreenState() throws RemoteException {}
    
    public boolean registerCastScreenStateObserver(IOplusCastScreenStateObserver param1IOplusCastScreenStateObserver) throws RemoteException {
      return false;
    }
    
    public boolean unregisterCastScreenStateObserver(IOplusCastScreenStateObserver param1IOplusCastScreenStateObserver) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusMirageWindowSession {
    private static final String DESCRIPTOR = "com.oplus.miragewindow.IOplusMirageWindowSession";
    
    static final int TRANSACTION_addCastScreenState = 1;
    
    static final int TRANSACTION_getCastScreenStateList = 2;
    
    static final int TRANSACTION_registerCastScreenStateObserver = 4;
    
    static final int TRANSACTION_removeCastScreenState = 3;
    
    static final int TRANSACTION_unregisterCastScreenStateObserver = 5;
    
    public Stub() {
      attachInterface(this, "com.oplus.miragewindow.IOplusMirageWindowSession");
    }
    
    public static IOplusMirageWindowSession asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.miragewindow.IOplusMirageWindowSession");
      if (iInterface != null && iInterface instanceof IOplusMirageWindowSession)
        return (IOplusMirageWindowSession)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "unregisterCastScreenStateObserver";
            } 
            return "registerCastScreenStateObserver";
          } 
          return "removeCastScreenState";
        } 
        return "getCastScreenStateList";
      } 
      return "addCastScreenState";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      List<OplusCastScreenState> list;
      if (param1Int1 != 1) {
        IOplusCastScreenStateObserver iOplusCastScreenStateObserver;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("com.oplus.miragewindow.IOplusMirageWindowSession");
                return true;
              } 
              param1Parcel1.enforceInterface("com.oplus.miragewindow.IOplusMirageWindowSession");
              iOplusCastScreenStateObserver = IOplusCastScreenStateObserver.Stub.asInterface(param1Parcel1.readStrongBinder());
              boolean bool1 = unregisterCastScreenStateObserver(iOplusCastScreenStateObserver);
              param1Parcel2.writeNoException();
              param1Parcel2.writeInt(bool1);
              return true;
            } 
            iOplusCastScreenStateObserver.enforceInterface("com.oplus.miragewindow.IOplusMirageWindowSession");
            iOplusCastScreenStateObserver = IOplusCastScreenStateObserver.Stub.asInterface(iOplusCastScreenStateObserver.readStrongBinder());
            boolean bool = registerCastScreenStateObserver(iOplusCastScreenStateObserver);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          } 
          iOplusCastScreenStateObserver.enforceInterface("com.oplus.miragewindow.IOplusMirageWindowSession");
          removeCastScreenState();
          param1Parcel2.writeNoException();
          return true;
        } 
        iOplusCastScreenStateObserver.enforceInterface("com.oplus.miragewindow.IOplusMirageWindowSession");
        list = getCastScreenStateList();
        param1Parcel2.writeNoException();
        param1Parcel2.writeTypedList(list);
        return true;
      } 
      list.enforceInterface("com.oplus.miragewindow.IOplusMirageWindowSession");
      if (list.readInt() != 0) {
        OplusCastScreenState oplusCastScreenState = (OplusCastScreenState)OplusCastScreenState.CREATOR.createFromParcel((Parcel)list);
      } else {
        list = null;
      } 
      addCastScreenState((OplusCastScreenState)list);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IOplusMirageWindowSession {
      public static IOplusMirageWindowSession sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.miragewindow.IOplusMirageWindowSession";
      }
      
      public void addCastScreenState(OplusCastScreenState param2OplusCastScreenState) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.miragewindow.IOplusMirageWindowSession");
          if (param2OplusCastScreenState != null) {
            parcel1.writeInt(1);
            param2OplusCastScreenState.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusMirageWindowSession.Stub.getDefaultImpl() != null) {
            IOplusMirageWindowSession.Stub.getDefaultImpl().addCastScreenState(param2OplusCastScreenState);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<OplusCastScreenState> getCastScreenStateList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.miragewindow.IOplusMirageWindowSession");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusMirageWindowSession.Stub.getDefaultImpl() != null)
            return IOplusMirageWindowSession.Stub.getDefaultImpl().getCastScreenStateList(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(OplusCastScreenState.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeCastScreenState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.miragewindow.IOplusMirageWindowSession");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOplusMirageWindowSession.Stub.getDefaultImpl() != null) {
            IOplusMirageWindowSession.Stub.getDefaultImpl().removeCastScreenState();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean registerCastScreenStateObserver(IOplusCastScreenStateObserver param2IOplusCastScreenStateObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.miragewindow.IOplusMirageWindowSession");
          if (param2IOplusCastScreenStateObserver != null) {
            iBinder = param2IOplusCastScreenStateObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IOplusMirageWindowSession.Stub.getDefaultImpl() != null) {
            bool1 = IOplusMirageWindowSession.Stub.getDefaultImpl().registerCastScreenStateObserver(param2IOplusCastScreenStateObserver);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean unregisterCastScreenStateObserver(IOplusCastScreenStateObserver param2IOplusCastScreenStateObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.miragewindow.IOplusMirageWindowSession");
          if (param2IOplusCastScreenStateObserver != null) {
            iBinder = param2IOplusCastScreenStateObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IOplusMirageWindowSession.Stub.getDefaultImpl() != null) {
            bool1 = IOplusMirageWindowSession.Stub.getDefaultImpl().unregisterCastScreenStateObserver(param2IOplusCastScreenStateObserver);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusMirageWindowSession param1IOplusMirageWindowSession) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusMirageWindowSession != null) {
          Proxy.sDefaultImpl = param1IOplusMirageWindowSession;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusMirageWindowSession getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
