package com.oplus.miragewindow;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusMirageSessionCallback extends IInterface {
  class Default implements IOplusMirageSessionCallback {
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusMirageSessionCallback {
    private static final String DESCRIPTOR = "com.oplus.miragewindow.IOplusMirageSessionCallback";
    
    public Stub() {
      attachInterface(this, "com.oplus.miragewindow.IOplusMirageSessionCallback");
    }
    
    public static IOplusMirageSessionCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.miragewindow.IOplusMirageSessionCallback");
      if (iInterface != null && iInterface instanceof IOplusMirageSessionCallback)
        return (IOplusMirageSessionCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      return null;
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902)
        return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
      param1Parcel2.writeString("com.oplus.miragewindow.IOplusMirageSessionCallback");
      return true;
    }
    
    private static class Proxy implements IOplusMirageSessionCallback {
      public static IOplusMirageSessionCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.miragewindow.IOplusMirageSessionCallback";
      }
    }
    
    public static boolean setDefaultImpl(IOplusMirageSessionCallback param1IOplusMirageSessionCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusMirageSessionCallback != null) {
          Proxy.sDefaultImpl = param1IOplusMirageSessionCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusMirageSessionCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
