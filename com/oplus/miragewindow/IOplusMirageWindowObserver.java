package com.oplus.miragewindow;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusMirageWindowObserver extends IInterface {
  void onMirageWindConfigChanged(OplusMirageWindowInfo paramOplusMirageWindowInfo) throws RemoteException;
  
  void onMirageWindowDied(String paramString) throws RemoteException;
  
  void onMirageWindowExit(OplusMirageWindowInfo paramOplusMirageWindowInfo) throws RemoteException;
  
  void onMirageWindowShow(OplusMirageWindowInfo paramOplusMirageWindowInfo) throws RemoteException;
  
  class Default implements IOplusMirageWindowObserver {
    public void onMirageWindowShow(OplusMirageWindowInfo param1OplusMirageWindowInfo) throws RemoteException {}
    
    public void onMirageWindowExit(OplusMirageWindowInfo param1OplusMirageWindowInfo) throws RemoteException {}
    
    public void onMirageWindowDied(String param1String) throws RemoteException {}
    
    public void onMirageWindConfigChanged(OplusMirageWindowInfo param1OplusMirageWindowInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusMirageWindowObserver {
    private static final String DESCRIPTOR = "com.oplus.miragewindow.IOplusMirageWindowObserver";
    
    static final int TRANSACTION_onMirageWindConfigChanged = 4;
    
    static final int TRANSACTION_onMirageWindowDied = 3;
    
    static final int TRANSACTION_onMirageWindowExit = 2;
    
    static final int TRANSACTION_onMirageWindowShow = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.miragewindow.IOplusMirageWindowObserver");
    }
    
    public static IOplusMirageWindowObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.miragewindow.IOplusMirageWindowObserver");
      if (iInterface != null && iInterface instanceof IOplusMirageWindowObserver)
        return (IOplusMirageWindowObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onMirageWindConfigChanged";
          } 
          return "onMirageWindowDied";
        } 
        return "onMirageWindowExit";
      } 
      return "onMirageWindowShow";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("com.oplus.miragewindow.IOplusMirageWindowObserver");
              return true;
            } 
            param1Parcel1.enforceInterface("com.oplus.miragewindow.IOplusMirageWindowObserver");
            if (param1Parcel1.readInt() != 0) {
              OplusMirageWindowInfo oplusMirageWindowInfo = (OplusMirageWindowInfo)OplusMirageWindowInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onMirageWindConfigChanged((OplusMirageWindowInfo)param1Parcel1);
            return true;
          } 
          param1Parcel1.enforceInterface("com.oplus.miragewindow.IOplusMirageWindowObserver");
          str = param1Parcel1.readString();
          onMirageWindowDied(str);
          return true;
        } 
        str.enforceInterface("com.oplus.miragewindow.IOplusMirageWindowObserver");
        if (str.readInt() != 0) {
          OplusMirageWindowInfo oplusMirageWindowInfo = (OplusMirageWindowInfo)OplusMirageWindowInfo.CREATOR.createFromParcel((Parcel)str);
        } else {
          str = null;
        } 
        onMirageWindowExit((OplusMirageWindowInfo)str);
        return true;
      } 
      str.enforceInterface("com.oplus.miragewindow.IOplusMirageWindowObserver");
      if (str.readInt() != 0) {
        OplusMirageWindowInfo oplusMirageWindowInfo = (OplusMirageWindowInfo)OplusMirageWindowInfo.CREATOR.createFromParcel((Parcel)str);
      } else {
        str = null;
      } 
      onMirageWindowShow((OplusMirageWindowInfo)str);
      return true;
    }
    
    private static class Proxy implements IOplusMirageWindowObserver {
      public static IOplusMirageWindowObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.miragewindow.IOplusMirageWindowObserver";
      }
      
      public void onMirageWindowShow(OplusMirageWindowInfo param2OplusMirageWindowInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.miragewindow.IOplusMirageWindowObserver");
          if (param2OplusMirageWindowInfo != null) {
            parcel.writeInt(1);
            param2OplusMirageWindowInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusMirageWindowObserver.Stub.getDefaultImpl() != null) {
            IOplusMirageWindowObserver.Stub.getDefaultImpl().onMirageWindowShow(param2OplusMirageWindowInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onMirageWindowExit(OplusMirageWindowInfo param2OplusMirageWindowInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.miragewindow.IOplusMirageWindowObserver");
          if (param2OplusMirageWindowInfo != null) {
            parcel.writeInt(1);
            param2OplusMirageWindowInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IOplusMirageWindowObserver.Stub.getDefaultImpl() != null) {
            IOplusMirageWindowObserver.Stub.getDefaultImpl().onMirageWindowExit(param2OplusMirageWindowInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onMirageWindowDied(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.miragewindow.IOplusMirageWindowObserver");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IOplusMirageWindowObserver.Stub.getDefaultImpl() != null) {
            IOplusMirageWindowObserver.Stub.getDefaultImpl().onMirageWindowDied(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onMirageWindConfigChanged(OplusMirageWindowInfo param2OplusMirageWindowInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.miragewindow.IOplusMirageWindowObserver");
          if (param2OplusMirageWindowInfo != null) {
            parcel.writeInt(1);
            param2OplusMirageWindowInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IOplusMirageWindowObserver.Stub.getDefaultImpl() != null) {
            IOplusMirageWindowObserver.Stub.getDefaultImpl().onMirageWindConfigChanged(param2OplusMirageWindowInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusMirageWindowObserver param1IOplusMirageWindowObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusMirageWindowObserver != null) {
          Proxy.sDefaultImpl = param1IOplusMirageWindowObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusMirageWindowObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
