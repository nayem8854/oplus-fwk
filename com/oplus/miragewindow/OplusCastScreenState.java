package com.oplus.miragewindow;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public class OplusCastScreenState implements Parcelable {
  public Bundle extension = new Bundle();
  
  public int castState;
  
  public String castName;
  
  public static final int RUNNING = 2;
  
  public static final int INITIALIZING = 1;
  
  public static final int FINISHED = 3;
  
  public OplusCastScreenState(Parcel paramParcel) {
    this.castName = paramParcel.readString();
    this.castState = paramParcel.readInt();
    this.extension = paramParcel.readBundle();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.castName);
    paramParcel.writeInt(this.castState);
    paramParcel.writeBundle(this.extension);
  }
  
  public static final Parcelable.Creator<OplusCastScreenState> CREATOR = new Parcelable.Creator<OplusCastScreenState>() {
      public OplusCastScreenState createFromParcel(Parcel param1Parcel) {
        return new OplusCastScreenState(param1Parcel);
      }
      
      public OplusCastScreenState[] newArray(int param1Int) {
        return new OplusCastScreenState[param1Int];
      }
    };
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("OplusCastScreenState = { ");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" castName = ");
    stringBuilder2.append(this.castName);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" castState = ");
    stringBuilder2.append(this.castState);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" extension = ");
    stringBuilder2.append(this.extension);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append("}");
    return stringBuilder1.toString();
  }
  
  public OplusCastScreenState() {}
}
