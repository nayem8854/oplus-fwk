package com.oplus.miragewindow;

import android.content.ComponentName;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public class OplusMirageWindowInfo implements Parcelable {
  public OplusMirageWindowInfo() {
    this.extension = new Bundle();
  }
  
  public OplusMirageWindowInfo(Parcel paramParcel) {
    boolean bool;
    this.extension = new Bundle();
    this.pkgName = paramParcel.readString();
    if (paramParcel.readByte() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.windowShown = bool;
    this.width = paramParcel.readInt();
    this.height = paramParcel.readInt();
    this.taskId = paramParcel.readInt();
    this.stackId = paramParcel.readInt();
    this.flag = paramParcel.readInt();
    this.castMode = paramParcel.readInt();
    this.castFlag = paramParcel.readInt();
    this.extension = paramParcel.readBundle();
    if (paramParcel.readInt() != 0)
      this.cpnName = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel); 
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.pkgName);
    paramParcel.writeByte((byte)this.windowShown);
    paramParcel.writeInt(this.width);
    paramParcel.writeInt(this.height);
    paramParcel.writeInt(this.taskId);
    paramParcel.writeInt(this.stackId);
    paramParcel.writeInt(this.flag);
    paramParcel.writeInt(this.castMode);
    paramParcel.writeInt(this.castFlag);
    paramParcel.writeBundle(this.extension);
    if (this.cpnName != null) {
      paramParcel.writeInt(1);
      this.cpnName.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeInt(0);
    } 
  }
  
  public static final Parcelable.Creator<OplusMirageWindowInfo> CREATOR = new Parcelable.Creator<OplusMirageWindowInfo>() {
      public OplusMirageWindowInfo createFromParcel(Parcel param1Parcel) {
        return new OplusMirageWindowInfo(param1Parcel);
      }
      
      public OplusMirageWindowInfo[] newArray(int param1Int) {
        return new OplusMirageWindowInfo[param1Int];
      }
    };
  
  public int castFlag;
  
  public int castMode;
  
  public ComponentName cpnName;
  
  public Bundle extension;
  
  public int flag;
  
  public int height;
  
  public String pkgName;
  
  public int stackId;
  
  public int taskId;
  
  public int width;
  
  public boolean windowShown;
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("OplusMirageWindowInfo = { ");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" pkgName = ");
    stringBuilder2.append(this.pkgName);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" windowShown = ");
    stringBuilder2.append(this.windowShown);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" cpnName = ");
    stringBuilder2.append(this.cpnName);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" width = ");
    stringBuilder2.append(this.width);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" height = ");
    stringBuilder2.append(this.height);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" taskId = ");
    stringBuilder2.append(this.taskId);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" stackId = ");
    stringBuilder2.append(this.stackId);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" flag = ");
    stringBuilder2.append(this.flag);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" castMode = ");
    stringBuilder2.append(this.castMode);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" castFlag = ");
    stringBuilder2.append(this.castFlag);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" extension = ");
    stringBuilder2.append(this.extension);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append("}");
    return stringBuilder1.toString();
  }
}
