package com.oplus.miragewindow;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusCastScreenStateObserver extends IInterface {
  void onCastScreenStateChanged(OplusCastScreenState paramOplusCastScreenState) throws RemoteException;
  
  class Default implements IOplusCastScreenStateObserver {
    public void onCastScreenStateChanged(OplusCastScreenState param1OplusCastScreenState) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusCastScreenStateObserver {
    private static final String DESCRIPTOR = "com.oplus.miragewindow.IOplusCastScreenStateObserver";
    
    static final int TRANSACTION_onCastScreenStateChanged = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.miragewindow.IOplusCastScreenStateObserver");
    }
    
    public static IOplusCastScreenStateObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.miragewindow.IOplusCastScreenStateObserver");
      if (iInterface != null && iInterface instanceof IOplusCastScreenStateObserver)
        return (IOplusCastScreenStateObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onCastScreenStateChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.miragewindow.IOplusCastScreenStateObserver");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.miragewindow.IOplusCastScreenStateObserver");
      if (param1Parcel1.readInt() != 0) {
        OplusCastScreenState oplusCastScreenState = (OplusCastScreenState)OplusCastScreenState.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onCastScreenStateChanged((OplusCastScreenState)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IOplusCastScreenStateObserver {
      public static IOplusCastScreenStateObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.miragewindow.IOplusCastScreenStateObserver";
      }
      
      public void onCastScreenStateChanged(OplusCastScreenState param2OplusCastScreenState) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.miragewindow.IOplusCastScreenStateObserver");
          if (param2OplusCastScreenState != null) {
            parcel.writeInt(1);
            param2OplusCastScreenState.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusCastScreenStateObserver.Stub.getDefaultImpl() != null) {
            IOplusCastScreenStateObserver.Stub.getDefaultImpl().onCastScreenStateChanged(param2OplusCastScreenState);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusCastScreenStateObserver param1IOplusCastScreenStateObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusCastScreenStateObserver != null) {
          Proxy.sDefaultImpl = param1IOplusCastScreenStateObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusCastScreenStateObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
