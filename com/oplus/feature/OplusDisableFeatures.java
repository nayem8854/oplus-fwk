package com.oplus.feature;

public class OplusDisableFeatures {
  private static String getDeptFeature(String paramString1, String paramString2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("oppo.");
    stringBuilder.append(paramString1);
    stringBuilder.append(".");
    stringBuilder.append(paramString2);
    stringBuilder.append(".disable");
    return stringBuilder.toString();
  }
  
  public static class SystemCenter {
    public static final String LONGSHOT = getFeature("longshot");
    
    private static String getFeature(String param1String) {
      return OplusDisableFeatures.getDeptFeature("system_center", param1String);
    }
    
    public static final String TRANSLATE = getFeature("translate");
    
    static {
    
    }
  }
}
