package com.oplus.resolver;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.RemoteException;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.internal.widget.LinearLayoutManager;
import com.android.internal.widget.RecyclerView;
import com.oplus.oshare.IOplusOshareCallback;
import com.oplus.oshare.IOplusOshareInitListener;
import com.oplus.oshare.OplusOshareDevice;
import com.oplus.oshare.OplusOshareServiceUtil;
import com.oplus.oshare.OplusOshareState;
import com.oplus.util.OplusChangeTextUtil;
import com.oplus.util.OplusContextUtil;
import com.oplus.widget.OplusCircleProgressBar;
import java.util.List;

public class OplusResolverOshare {
  private static final String O_SHARE_CLASS = "com.coloros.oshare.ui.GrantUriActivity";
  
  private static final String O_SHARE_PACKAGE = "com.coloros.oshare";
  
  private static final int TOUCH_END_DURATION = 300;
  
  private boolean isPause;
  
  private Context mContext;
  
  private IOplusOshareCallback mOShareCallback;
  
  private IOplusOshareInitListener mOShareInitListener;
  
  private boolean mOShareServiceInited = false;
  
  private OplusOshareServiceUtil mOShareServiceUtil;
  
  private Intent mOriginIntent;
  
  private View mOshareClosedView;
  
  private View mOshareDividerLine;
  
  private View mOshareOpenedView;
  
  private View mOshareSharingView;
  
  RecyclerView mRecyclerView;
  
  private ResolverOshareingAdapter mResolverOshareingAdapter;
  
  public OplusResolverOshare(Context paramContext, Intent paramIntent) {
    this.mOShareInitListener = (IOplusOshareInitListener)new IOplusOshareInitListener.Stub() {
        final OplusResolverOshare this$0;
        
        public void onShareUninit() throws RemoteException {
          OplusResolverOshare.access$002(OplusResolverOshare.this, false);
          if (OplusResolverOshare.this.mOShareServiceUtil != null)
            OplusResolverOshare.this.mOShareServiceUtil.unregisterCallback(OplusResolverOshare.this.mOShareCallback); 
        }
        
        public void onShareInit() throws RemoteException {
          OplusResolverOshare.access$002(OplusResolverOshare.this, true);
          if (OplusResolverOshare.this.mContext != null && OplusResolverOshare.this.mContext instanceof Activity)
            ((Activity)OplusResolverOshare.this.mContext).runOnUiThread(new _$$Lambda$OplusResolverOshare$1$UvdCXsC3xdyvowJ2HYcyEekYXUE(this)); 
          if (OplusResolverOshare.this.mOShareServiceUtil != null)
            OplusResolverOshare.this.mOShareServiceUtil.registerCallback(OplusResolverOshare.this.mOShareCallback); 
        }
      };
    this.mOShareCallback = (IOplusOshareCallback)new IOplusOshareCallback.Stub() {
        final OplusResolverOshare this$0;
        
        public void onDeviceChanged(List<OplusOshareDevice> param1List) throws RemoteException {
          if (OplusResolverOshare.this.mContext != null && OplusResolverOshare.this.mContext instanceof Activity)
            ((Activity)OplusResolverOshare.this.mContext).runOnUiThread(new _$$Lambda$OplusResolverOshare$2$ExY2IcnZHep1xWFmsicEccymjzM(this, param1List)); 
        }
        
        public void onSendSwitchChanged(boolean param1Boolean) {
          if (OplusResolverOshare.this.mContext != null && OplusResolverOshare.this.mContext instanceof Activity)
            ((Activity)OplusResolverOshare.this.mContext).runOnUiThread(new _$$Lambda$OplusResolverOshare$2$Ezd7rXx_cLxENB3POAAThdGlaMM(this)); 
        }
      };
    this.mContext = paramContext;
    this.mOriginIntent = paramIntent;
  }
  
  void initOShareService() {
    if (this.mOShareServiceUtil == null) {
      OplusOshareServiceUtil oplusOshareServiceUtil = new OplusOshareServiceUtil(this.mContext, this.mOShareInitListener);
      oplusOshareServiceUtil.initShareEngine();
    } 
  }
  
  void initOShareView(View paramView) {
    List<OplusOshareDevice> list;
    this.isPause = false;
    if (this.mOshareClosedView != null) {
      this.mOshareDividerLine.setVisibility(0);
      onResume();
      ResolverOshareingAdapter resolverOshareingAdapter = this.mResolverOshareingAdapter;
      if (resolverOshareingAdapter == null) {
        resolverOshareingAdapter = null;
      } else {
        list = resolverOshareingAdapter.mDeviceList;
      } 
      updateOShareUI(list);
      return;
    } 
    View view = list.findViewById(201457839);
    if (view instanceof ViewStub)
      this.mOshareClosedView = ((ViewStub)view).inflate(); 
    this.mOshareDividerLine = view = list.findViewById(201457767);
    view.setVisibility(0);
    this.mOshareOpenedView = list.findViewById(201457838);
    this.mOshareSharingView = list.findViewById(201457847);
    float f = (this.mContext.getResources().getConfiguration()).fontScale;
    TextView textView = (TextView)this.mOshareClosedView.findViewById(201457843);
    if (textView != null) {
      int i = this.mContext.getResources().getDimensionPixelSize(201654307);
      float f1 = i;
      i = (int)OplusChangeTextUtil.getSuitableFontSize(f1, f, 4);
      textView.setTextSize(0, i);
    } 
    Button button = (Button)this.mOshareClosedView.findViewById(201457846);
    if (button != null) {
      int i = this.mContext.getResources().getDimensionPixelSize(201654307);
      float f1 = i;
      i = (int)OplusChangeTextUtil.getSuitableFontSize(f1, f, 4);
      button.setTextSize(0, i);
      button.setOnClickListener(new _$$Lambda$OplusResolverOshare$iM2SGSUIcegC5uQwSksL6XpFDBY(this));
    } 
  }
  
  void dismissOShareView() {
    if (this.mOshareClosedView != null) {
      View view = this.mOshareDividerLine;
      if (view != null)
        view.setVisibility(8); 
      resetOshareView();
      this.isPause = true;
    } 
  }
  
  void onResume() {
    OplusOshareServiceUtil oplusOshareServiceUtil = this.mOShareServiceUtil;
    if (oplusOshareServiceUtil != null)
      try {
        oplusOshareServiceUtil.resume();
      } catch (RemoteException remoteException) {
        remoteException.printStackTrace();
      }  
  }
  
  void onPause() {
    OplusOshareServiceUtil oplusOshareServiceUtil = this.mOShareServiceUtil;
    if (oplusOshareServiceUtil != null)
      try {
        oplusOshareServiceUtil.pause();
      } catch (RemoteException remoteException) {
        remoteException.printStackTrace();
      }  
  }
  
  void onDestroy() {
    OplusOshareServiceUtil oplusOshareServiceUtil = this.mOShareServiceUtil;
    if (oplusOshareServiceUtil != null) {
      oplusOshareServiceUtil.stop();
      this.mOShareServiceUtil.unregisterCallback(this.mOShareCallback);
      this.mOShareServiceUtil = null;
    } 
  }
  
  private void updateOShareUI(List<OplusOshareDevice> paramList) {
    if (this.isPause)
      return; 
    resetOshareView();
    OplusOshareServiceUtil oplusOshareServiceUtil = this.mOShareServiceUtil;
    if (oplusOshareServiceUtil != null && oplusOshareServiceUtil.isSendOn()) {
      if (paramList == null || paramList.size() < 1) {
        initOshareOpenedView();
        View view1 = this.mOshareOpenedView;
        if (view1 != null)
          view1.setVisibility(0); 
        return;
      } 
      initOsharingView();
      View view = this.mOshareSharingView;
      if (view != null)
        view.setVisibility(0); 
    } else {
      View view = this.mOshareClosedView;
      if (view != null)
        view.setVisibility(0); 
    } 
  }
  
  private void initOshareOpenedView() {
    View view = this.mOshareOpenedView;
    if (view instanceof ViewStub)
      this.mOshareOpenedView = ((ViewStub)view).inflate(); 
    if (this.mOshareSharingView == null)
      return; 
    float f = (this.mContext.getResources().getConfiguration()).fontScale;
    TextView textView = (TextView)this.mOshareOpenedView.findViewById(201457841);
    if (textView != null) {
      int i = this.mContext.getResources().getDimensionPixelSize(201654307);
      float f1 = i;
      i = (int)OplusChangeTextUtil.getSuitableFontSize(f1, f, 4);
      textView.setTextSize(0, i);
    } 
    textView = (TextView)this.mOshareOpenedView.findViewById(201457840);
    if (textView != null) {
      int i = this.mContext.getResources().getDimensionPixelSize(201654305);
      i = (int)OplusChangeTextUtil.getSuitableFontSize(i, f, 4);
      textView.setTextSize(0, i);
    } 
  }
  
  private void initOsharingView() {
    View view = this.mOshareSharingView;
    if (view instanceof ViewStub)
      this.mOshareSharingView = ((ViewStub)view).inflate(); 
    view = this.mOshareSharingView;
    if (view == null || this.mResolverOshareingAdapter != null)
      return; 
    this.mRecyclerView = (RecyclerView)view.findViewById(201457848);
    this.mResolverOshareingAdapter = new ResolverOshareingAdapter(this.mContext);
    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.mContext, 0, false);
    this.mRecyclerView.setAdapter(this.mResolverOshareingAdapter);
    this.mRecyclerView.setLayoutManager((RecyclerView.LayoutManager)linearLayoutManager);
  }
  
  private void resetOshareView() {
    View view = this.mOshareClosedView;
    if (view != null)
      view.setVisibility(8); 
    view = this.mOshareOpenedView;
    if (view != null)
      view.setVisibility(8); 
    view = this.mOshareSharingView;
    if (view != null)
      view.setVisibility(8); 
  }
  
  private static class EmptyViewHolder extends RecyclerView.ViewHolder {
    public static final int EMPTY_TYPE = -1;
    
    public EmptyViewHolder(View param1View) {
      super(param1View);
    }
  }
  
  private class MyViewHolder extends RecyclerView.ViewHolder {
    final OplusResolverOshare this$0;
    
    public final TextView userName;
    
    public final View userPanel;
    
    public final ImageView userPic;
    
    public final OplusCircleProgressBar userProgress;
    
    public final TextView userStatus;
    
    public MyViewHolder(View param1View) {
      super(param1View);
      this.userName = (TextView)param1View.findViewById(201457810);
      this.userStatus = (TextView)param1View.findViewById(201457812);
      this.userPic = (ImageView)param1View.findViewById(201457811);
      this.userProgress = (OplusCircleProgressBar)param1View.findViewById(201457808);
      this.userPanel = param1View.findViewById(201457809);
    }
  }
  
  private class ResolverOshareingAdapter extends RecyclerView.Adapter {
    private static final int MI = 30;
    
    private static final int OPPO = 10;
    
    private static final int REALME = 11;
    
    private static final int VIVO = 20;
    
    String BUSUY_STR;
    
    String CANCEL_STR;
    
    String CANCEL_WAIT_STR;
    
    private final int ICON_COVER_COLOR = Color.parseColor("#7F000000");
    
    String READY_STR;
    
    String TRANSITING_STR;
    
    String TRANSIT_FAILED_STR;
    
    String TRANSIT_REJECT_STR;
    
    String TRANSIT_SUCCESS_STR;
    
    String TRANSIT_TIMEOUT_STR;
    
    String TRANSIT_WAIT_STR;
    
    private int mAction = Integer.MIN_VALUE;
    
    private Context mContext;
    
    private List<OplusOshareDevice> mDeviceList;
    
    int mStateTextColorCancel;
    
    int mStateTextColorNomarl;
    
    int mStateTextColorSucces;
    
    final OplusResolverOshare this$0;
    
    public void setDeviceList(List<OplusOshareDevice> param1List) {
      this.mDeviceList = param1List;
    }
    
    public ResolverOshareingAdapter(Context param1Context) {
      this.mContext = param1Context;
      this.mStateTextColorNomarl = param1Context.getColor(201720061);
      this.mStateTextColorSucces = OplusContextUtil.getAttrColor(this.mContext, 201981956);
      this.mStateTextColorCancel = param1Context.getColor(201720052);
      this.READY_STR = param1Context.getString(201588853);
      this.TRANSIT_WAIT_STR = param1Context.getString(201588853);
      this.TRANSITING_STR = param1Context.getString(201588851);
      this.TRANSIT_FAILED_STR = param1Context.getString(201588849);
      this.TRANSIT_REJECT_STR = param1Context.getString(201588847);
      this.TRANSIT_SUCCESS_STR = param1Context.getString(201588848);
      this.BUSUY_STR = param1Context.getString(201588843);
      this.CANCEL_STR = param1Context.getString(201588844);
      this.CANCEL_WAIT_STR = param1Context.getString(201588845);
      this.TRANSIT_TIMEOUT_STR = param1Context.getString(201588850);
    }
    
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup param1ViewGroup, int param1Int) {
      LinearLayout.LayoutParams layoutParams;
      if (param1Int == -1) {
        View view = new View(param1ViewGroup.getContext());
        layoutParams = new LinearLayout.LayoutParams(param1ViewGroup.getResources().getDimensionPixelSize(201654604), 1);
        view.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
        return new OplusResolverOshare.EmptyViewHolder(view);
      } 
      OplusResolverOshare oplusResolverOshare = OplusResolverOshare.this;
      LayoutInflater layoutInflater = LayoutInflater.from(layoutParams.getContext());
      return new OplusResolverOshare.MyViewHolder(layoutInflater.inflate(202440742, (ViewGroup)layoutParams, false));
    }
    
    public void onBindViewHolder(RecyclerView.ViewHolder param1ViewHolder, int param1Int) {
      List<OplusOshareDevice> list = this.mDeviceList;
      if (list == null || list.isEmpty() || param1Int > this.mDeviceList.size() || !(param1ViewHolder instanceof OplusResolverOshare.MyViewHolder) || param1Int == 0)
        return; 
      param1ViewHolder = param1ViewHolder;
      OplusOshareDevice oplusOshareDevice = this.mDeviceList.get(param1Int - 1);
      if (oplusOshareDevice != null) {
        bindCompanyIcon(((OplusResolverOshare.MyViewHolder)param1ViewHolder).userPic, oplusOshareDevice);
        if (oplusOshareDevice.getState() == OplusOshareState.TRANSITING) {
          ((OplusResolverOshare.MyViewHolder)param1ViewHolder).userPic.setColorFilter(this.ICON_COVER_COLOR);
          ((OplusResolverOshare.MyViewHolder)param1ViewHolder).userProgress.setVisibility(0);
          ((OplusResolverOshare.MyViewHolder)param1ViewHolder).userProgress.setProgress(oplusOshareDevice.getProgress());
        } else {
          ((OplusResolverOshare.MyViewHolder)param1ViewHolder).userPic.clearColorFilter();
          ((OplusResolverOshare.MyViewHolder)param1ViewHolder).userProgress.setVisibility(8);
        } 
        ((OplusResolverOshare.MyViewHolder)param1ViewHolder).userStatus.setText(getStateString(oplusOshareDevice.getState()));
        ((OplusResolverOshare.MyViewHolder)param1ViewHolder).userStatus.setTextColor(getStateColor(oplusOshareDevice.getState()));
        ((OplusResolverOshare.MyViewHolder)param1ViewHolder).userName.setText(oplusOshareDevice.getName());
        ((OplusResolverOshare.MyViewHolder)param1ViewHolder).userPic.setOnTouchListener(new _$$Lambda$OplusResolverOshare$ResolverOshareingAdapter$K1tzSiPkZqwfegYzAyaMxVJEf_k(this, oplusOshareDevice));
      } 
    }
    
    public int getItemViewType(int param1Int) {
      if (param1Int == 0)
        return -1; 
      return super.getItemViewType(param1Int);
    }
    
    public int getItemCount() {
      List<OplusOshareDevice> list = this.mDeviceList;
      if (list == null || list.isEmpty())
        return 0; 
      return this.mDeviceList.size() + 1;
    }
    
    private String getStateString(OplusOshareState param1OplusOshareState) {
      String str2 = "";
      switch (param1OplusOshareState) {
        default:
          str1 = str2;
          return str1;
        case TRANSIT_TIMEOUT:
          str1 = this.TRANSIT_TIMEOUT_STR;
          return str1;
        case CANCEL_WAIT:
          str1 = this.CANCEL_WAIT_STR;
          return str1;
        case CANCEL:
          str1 = this.CANCEL_STR;
          return str1;
        case BUSUY:
          str1 = this.BUSUY_STR;
          return str1;
        case TRANSIT_SUCCESS:
          str1 = this.TRANSIT_SUCCESS_STR;
          return str1;
        case TRANSIT_REJECT:
          str1 = this.TRANSIT_REJECT_STR;
          return str1;
        case TRANSIT_FAILED:
          str1 = this.TRANSIT_FAILED_STR;
          return str1;
        case TRANSITING:
          str1 = this.TRANSITING_STR;
          return str1;
        case TRANSIT_WAIT:
          str1 = this.TRANSIT_WAIT_STR;
          return str1;
        case READY:
          break;
      } 
      String str1 = this.READY_STR;
      return str1;
    }
    
    private void bindCompanyIcon(ImageView param1ImageView, OplusOshareDevice param1OplusOshareDevice) {
      if (!TextUtils.isEmpty(param1OplusOshareDevice.getHeadIconUrl())) {
        param1ImageView.setImageURI(Uri.parse(param1OplusOshareDevice.getHeadIconUrl()));
      } else {
        int i = param1OplusOshareDevice.getVender();
        if (i != 10) {
          if (i != 11) {
            if (i != 20) {
              if (i != 30) {
                i = 201851313;
              } else {
                i = 201851314;
              } 
            } else {
              i = 201851317;
            } 
          } else {
            i = 201851316;
          } 
        } else {
          i = 201851315;
        } 
        param1ImageView.setImageResource(i);
      } 
      param1ImageView.getDrawable().mutate();
    }
    
    private int getStateColor(OplusOshareState param1OplusOshareState) {
      int i = this.mStateTextColorNomarl;
      int j = OplusResolverOshare.null.$SwitchMap$com$oplus$oshare$OplusOshareState[param1OplusOshareState.ordinal()];
      if (j != 6) {
        if (j == 9)
          i = this.mStateTextColorCancel; 
      } else {
        i = this.mStateTextColorSucces;
      } 
      return i;
    }
    
    private void oShareClick(OplusOshareDevice param1OplusOshareDevice) {
      if (OplusResolverOshare.this.mOShareServiceUtil != null && OplusResolverOshare.this.mOShareServiceInited) {
        Intent intent = new Intent(OplusResolverOshare.this.mOriginIntent);
        ComponentName componentName = new ComponentName("com.coloros.oshare", "com.coloros.oshare.ui.GrantUriActivity");
        intent.setComponent(componentName);
        intent.addFlags(65536);
        try {
          ((Activity)this.mContext).startActivityAsCaller(intent, null, null, false, -10000);
        } catch (Exception exception) {
          exception.printStackTrace();
        } 
        OplusResolverOshare.this.mOShareServiceUtil.sendData(OplusResolverOshare.this.mOriginIntent, param1OplusOshareDevice);
      } 
    }
    
    private boolean fadeBackTouchEvent(MotionEvent param1MotionEvent, OplusOshareDevice param1OplusOshareDevice) {
      this.mAction = param1MotionEvent.getAction();
      if (isUpOrCancelAction())
        if (OplusResolverOshare.this.mRecyclerView != null) {
          OplusResolverOshare.this.mRecyclerView.postDelayed(new _$$Lambda$OplusResolverOshare$ResolverOshareingAdapter$YMOCREMN3KUxGUADVuz2e9os0J0(this, param1OplusOshareDevice), 300L);
        } else if (this.mAction == 1) {
          oShareClick(param1OplusOshareDevice);
        }  
      return true;
    }
    
    boolean isUpOrCancelAction() {
      int i = this.mAction;
      boolean bool1 = true, bool2 = bool1;
      if (i != 3)
        if (i == 1) {
          bool2 = bool1;
        } else {
          bool2 = false;
        }  
      return bool2;
    }
    
    boolean isDefaultAction() {
      boolean bool;
      if (this.mAction == Integer.MIN_VALUE) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
}
