package com.oplus.resolver;

import android.app.OplusActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Environment;
import android.os.RemoteException;
import android.os.storage.StorageManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import com.oplus.content.OplusFeatureConfigManager;
import com.oplus.util.OplusResolveData;
import com.oplus.util.OplusResolverIntentUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import oplus.util.OplusStatistics;

public class OplusResolverInfoHelper {
  private HashMap<String, List<String>> mCloudBlackResolveMap = new HashMap<>();
  
  private HashMap<String, List<String>> mCloudResolveMap = new HashMap<>();
  
  private HashMap<String, List<String>> mCloudChooserMap = new HashMap<>();
  
  private HashMap<String, List<String>> mCloudBlackChooseMap = new HashMap<>();
  
  private HashMap<String, List<String>> mCloudBlackChooseActivityMap = new HashMap<>();
  
  private HashMap<String, String> mIconNameMap = new HashMap<>();
  
  private List<ResolveInfo> mPriorityList = new ArrayList<>();
  
  private static final String APP_EVENT_ID = "resolver_app";
  
  private static final String CODE = "20120";
  
  private static final String GALLERY_PIN_LIST = "gallery_pin_list";
  
  private static final String KEY_ACTION = "action";
  
  private static final String KEY_CATEGORIES = "categories";
  
  private static final String KEY_CHOOSE = "isChooser";
  
  private static final String KEY_COMPONENT_LABEL = "componentLabel";
  
  private static final String KEY_DATA = "data";
  
  private static final String KEY_INTENT = "intent";
  
  private static final String KEY_MIME_TYPE = "mimeType";
  
  private static final String KEY_NAME = "name";
  
  private static final String KEY_POSITION = "position";
  
  private static final String KEY_REFERRER_PACKAGE = "referrerPackage";
  
  private static final String KEY_SCHEME = "scheme";
  
  private static final String KEY_TARGET_PACKAGE = "targetPackage";
  
  private static final String KEY_TYPE = "type";
  
  private static final String PINNED_SHARED_PREFS_NAME = "chooser_pin_settings";
  
  private static final String SHOW_EVENT_ID = "resolver_show";
  
  private static final String TAG = "OplusResolveInfoHelper";
  
  private static OplusResolverInfoHelper sResolveInfoHelper;
  
  private Context mContext;
  
  private SharedPreferences mPinnedSharedPrefs;
  
  public static OplusResolverInfoHelper getInstance(Context paramContext) {
    if (sResolveInfoHelper == null)
      sResolveInfoHelper = new OplusResolverInfoHelper(paramContext.getApplicationContext()); 
    return sResolveInfoHelper;
  }
  
  public void initData() {
    try {
      OplusActivityManager oplusActivityManager = new OplusActivityManager();
      this();
      OplusResolveData oplusResolveData = oplusActivityManager.getResolveData();
      this.mCloudBlackResolveMap = oplusResolveData.getBlackResolveMap();
      this.mCloudBlackChooseMap = oplusResolveData.getBlackChoosePackageMap();
      this.mCloudBlackChooseActivityMap = oplusResolveData.getBlackChooseActivityMap();
      this.mCloudResolveMap = oplusResolveData.getResolveMap();
      this.mCloudChooserMap = oplusResolveData.getChooseMap();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("mCloudBlackResolveMap = ");
      stringBuilder.append(this.mCloudBlackResolveMap);
      Log.d("OplusResolveInfoHelper", stringBuilder.toString());
      stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("mCloudBlackChooseMap = ");
      stringBuilder.append(this.mCloudBlackChooseMap);
      Log.d("OplusResolveInfoHelper", stringBuilder.toString());
      stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("mCloudBlackChooseActivityMap = ");
      stringBuilder.append(this.mCloudBlackChooseActivityMap);
      Log.d("OplusResolveInfoHelper", stringBuilder.toString());
      stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("mCloudResolveMap = ");
      stringBuilder.append(this.mCloudResolveMap);
      Log.d("OplusResolveInfoHelper", stringBuilder.toString());
      stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("mCloudChooserMap = ");
      stringBuilder.append(this.mCloudChooserMap);
      Log.d("OplusResolveInfoHelper", stringBuilder.toString());
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("init data RemoteException , ");
      stringBuilder.append(remoteException);
      Log.e("OplusResolveInfoHelper", stringBuilder.toString());
      remoteException.printStackTrace();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("init data Exception , ");
      stringBuilder.append(exception);
      Log.e("OplusResolveInfoHelper", stringBuilder.toString());
      exception.printStackTrace();
    } 
  }
  
  public int getResolveTopSize(Intent paramIntent) {
    String str = OplusResolverIntentUtil.getIntentType(paramIntent);
    if (this.mPriorityList != null && (!"email".equals(str) || !isSupportTopApp()))
      return this.mPriorityList.size(); 
    return 0;
  }
  
  private OplusResolverInfoHelper(Context paramContext) {
    this.mContext = paramContext;
  }
  
  private SharedPreferences getPinnedSharedPrefs(Context paramContext) {
    String str1 = StorageManager.UUID_PRIVATE_INTERNAL;
    int i = paramContext.getUserId();
    String str2 = paramContext.getPackageName();
    File file = new File(new File(Environment.getDataUserCePackageDirectory(str1, i, str2), "shared_prefs"), "chooser_pin_settings.xml");
    return paramContext.getSharedPreferences(file, 0);
  }
  
  private List<String> getPriorityListFromXml(boolean paramBoolean, String paramString) {
    List<String> list;
    if (paramBoolean) {
      list = getChooserListWithType(paramString);
    } else {
      list = getResolveListWithType((String)list);
    } 
    return list;
  }
  
  public void resort(List<ResolveInfo> paramList, Intent paramIntent) {
    initData();
    String str = OplusResolverIntentUtil.getIntentType(paramIntent);
    paramList = filterBlackApps(paramIntent, str, paramList);
    boolean bool = OplusResolverIntentUtil.isChooserAction(paramIntent);
    startToResort(paramList, str, getPriorityListFromXml(bool, str), bool);
  }
  
  private List<ResolveInfo> filterBlackApps(Intent paramIntent, String paramString, List<ResolveInfo> paramList) {
    List<String> list;
    if (paramList == null || paramList.isEmpty())
      return paramList; 
    boolean bool = OplusResolverIntentUtil.isChooserAction(paramIntent);
    if (bool) {
      List<String> list1 = getBlackChooseListWithType(paramString);
      if (list1 != null && !list1.isEmpty()) {
        Iterator<ResolveInfo> iterator = paramList.iterator();
        while (iterator.hasNext()) {
          ActivityInfo activityInfo = ((ResolveInfo)iterator.next()).activityInfo;
          if (activityInfo != null && OplusResolverIntentUtil.isInDataSet(list1, activityInfo.packageName)) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("remove black choose package : ");
            stringBuilder.append(activityInfo.packageName);
            Log.d("OplusResolveInfoHelper", stringBuilder.toString());
            iterator.remove();
          } 
        } 
      } 
      list = getBlackChooseActivityListWithType(paramString);
      if (list != null && !list.isEmpty()) {
        Iterator<ResolveInfo> iterator = paramList.iterator();
        while (iterator.hasNext()) {
          ComponentName componentName = ((ResolveInfo)iterator.next()).getComponentInfo().getComponentName();
          String str = componentName.flattenToShortString();
          if (OplusResolverIntentUtil.isInDataSet(list, str)) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("remove black choose componentName : ");
            stringBuilder.append(str);
            Log.d("OplusResolveInfoHelper", stringBuilder.toString());
            iterator.remove();
          } 
        } 
      } 
    } else {
      List<String> list1 = getBlackResolveListWithType((String)list);
      if (list1 != null && !list1.isEmpty()) {
        Iterator<ResolveInfo> iterator = paramList.iterator();
        while (iterator.hasNext()) {
          ActivityInfo activityInfo = ((ResolveInfo)iterator.next()).activityInfo;
          if (activityInfo != null && OplusResolverIntentUtil.isInDataSet(list1, activityInfo.packageName)) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("remove black resolve package : ");
            stringBuilder.append(activityInfo.packageName);
            Log.d("OplusResolveInfoHelper", stringBuilder.toString());
            iterator.remove();
          } 
        } 
      } 
    } 
    return paramList;
  }
  
  private void startToResort(List<ResolveInfo> paramList, String paramString, List<String> paramList1, boolean paramBoolean) {
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("start to resort : ");
    stringBuilder2.append(paramList);
    Log.d("OplusResolveInfoHelper", stringBuilder2.toString());
    if (!paramBoolean) {
      HashMap<Object, Object> hashMap = new HashMap<>();
      hashMap.put("type", paramString);
      OplusStatistics.onCommon(this.mContext, "20120", "resolver_show", hashMap, false);
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append("statistics data [resolver_show]: ");
      stringBuilder2.append(hashMap);
      Log.d("OplusResolveInfoHelper", stringBuilder2.toString());
    } 
    if (this.mPinnedSharedPrefs == null)
      this.mPinnedSharedPrefs = getPinnedSharedPrefs(this.mContext); 
    ArrayList<ResolveInfo> arrayList1 = new ArrayList();
    ArrayList<ResolveInfo> arrayList2 = new ArrayList();
    this.mPriorityList.clear();
    HashSet hashSet = new HashSet();
    Set set = hashSet;
    if (paramBoolean) {
      set = hashSet;
      if (paramString != null) {
        if ("gallery".equals(paramString)) {
          String str = Settings.Secure.getString(this.mContext.getContentResolver(), "gallery_pin_list");
          StringBuilder stringBuilder3 = new StringBuilder();
          stringBuilder3.append("galleryPinList = ");
          stringBuilder3.append(str);
          Log.d("OplusResolveInfoHelper", stringBuilder3.toString());
          set = hashSet;
          if (!TextUtils.isEmpty(str))
            set = new HashSet(Arrays.asList((Object[])str.split(";"))); 
        } else {
          set = this.mPinnedSharedPrefs.getStringSet(paramString, null);
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("mPinnedSharedPrefs pinPrefList : ");
        stringBuilder.append(set);
        stringBuilder.append(", type : ");
        stringBuilder.append(paramString);
        Log.d("OplusResolveInfoHelper", stringBuilder.toString());
      } 
    } 
    for (byte b = 0; b < paramList.size(); b++) {
      StringBuilder stringBuilder;
      ComponentName componentName = ((ResolveInfo)paramList.get(b)).getComponentInfo().getComponentName();
      String str2 = componentName.flattenToShortString();
      String str1 = ((ResolveInfo)paramList.get(b)).activityInfo.packageName;
      boolean bool1 = false;
      boolean bool2 = bool1;
      if (paramBoolean) {
        bool2 = bool1;
        if (set != null) {
          bool2 = bool1;
          if (set.size() > 0)
            bool2 = set.contains(str2); 
        } 
      } 
      if (bool2) {
        arrayList1.add(paramList.get(b));
        stringBuilder = new StringBuilder();
        stringBuilder.append("pinnedList add : ");
        stringBuilder.append(paramList.get(b));
        Log.d("OplusResolveInfoHelper", stringBuilder.toString());
      } else if (paramList1 != null && (paramList1.contains(str2) || paramList1.contains(stringBuilder))) {
        this.mPriorityList.add(paramList.get(b));
        stringBuilder = new StringBuilder();
        stringBuilder.append("priorityList add : ");
        stringBuilder.append(paramList.get(b));
        Log.d("OplusResolveInfoHelper", stringBuilder.toString());
      } else {
        arrayList2.add(paramList.get(b));
      } 
    } 
    if (!this.mPriorityList.isEmpty())
      sortPriorityList(paramList1); 
    paramList.clear();
    paramList.addAll(arrayList1);
    paramList.addAll(this.mPriorityList);
    paramList.addAll(arrayList2);
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("finish to resort : ");
    stringBuilder1.append(paramList);
    Log.d("OplusResolveInfoHelper", stringBuilder1.toString());
  }
  
  public int getExpandSizeWithoutMoreIcon(List<ResolveInfo> paramList, Intent paramIntent) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual initData : ()V
    //   4: aload_2
    //   5: invokestatic getIntentType : (Landroid/content/Intent;)Ljava/lang/String;
    //   8: astore_3
    //   9: aload_0
    //   10: aload_2
    //   11: aload_3
    //   12: aload_1
    //   13: invokespecial filterBlackApps : (Landroid/content/Intent;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    //   16: astore #4
    //   18: aload_2
    //   19: invokestatic isChooserAction : (Landroid/content/Intent;)Z
    //   22: istore #5
    //   24: aload_0
    //   25: iload #5
    //   27: aload_3
    //   28: invokespecial getPriorityListFromXml : (ZLjava/lang/String;)Ljava/util/List;
    //   31: astore_1
    //   32: iconst_0
    //   33: istore #6
    //   35: aload #4
    //   37: invokeinterface iterator : ()Ljava/util/Iterator;
    //   42: astore_2
    //   43: aload_2
    //   44: invokeinterface hasNext : ()Z
    //   49: ifeq -> 133
    //   52: aload_2
    //   53: invokeinterface next : ()Ljava/lang/Object;
    //   58: checkcast android/content/pm/ResolveInfo
    //   61: astore #4
    //   63: aload #4
    //   65: invokevirtual getComponentInfo : ()Landroid/content/pm/ComponentInfo;
    //   68: invokevirtual getComponentName : ()Landroid/content/ComponentName;
    //   71: astore_3
    //   72: aload_3
    //   73: invokevirtual flattenToShortString : ()Ljava/lang/String;
    //   76: astore_3
    //   77: aload #4
    //   79: getfield activityInfo : Landroid/content/pm/ActivityInfo;
    //   82: getfield packageName : Ljava/lang/String;
    //   85: astore #4
    //   87: iload #6
    //   89: istore #7
    //   91: aload_1
    //   92: ifnull -> 126
    //   95: aload_1
    //   96: aload_3
    //   97: invokeinterface contains : (Ljava/lang/Object;)Z
    //   102: ifne -> 120
    //   105: iload #6
    //   107: istore #7
    //   109: aload_1
    //   110: aload #4
    //   112: invokeinterface contains : (Ljava/lang/Object;)Z
    //   117: ifeq -> 126
    //   120: iload #6
    //   122: iconst_1
    //   123: iadd
    //   124: istore #7
    //   126: iload #7
    //   128: istore #6
    //   130: goto -> 43
    //   133: iload #6
    //   135: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #266	-> 0
    //   #267	-> 4
    //   #268	-> 4
    //   #269	-> 9
    //   #270	-> 18
    //   #271	-> 24
    //   #273	-> 32
    //   #274	-> 35
    //   #275	-> 63
    //   #276	-> 72
    //   #277	-> 77
    //   #278	-> 87
    //   #279	-> 120
    //   #281	-> 126
    //   #283	-> 133
  }
  
  private void sortPriorityList(List<String> paramList) {
    ArrayList<ResolveInfo> arrayList = new ArrayList();
    for (byte b = 0; b < paramList.size(); b++) {
      for (byte b1 = 0; b1 < this.mPriorityList.size(); b1++) {
        String str1 = ((ResolveInfo)this.mPriorityList.get(b1)).activityInfo.packageName;
        ComponentName componentName = ((ResolveInfo)this.mPriorityList.get(b1)).getComponentInfo().getComponentName();
        String str2 = componentName.flattenToShortString();
        if (((String)paramList.get(b)).equals(str2) || ((String)paramList.get(b)).equals(str1))
          arrayList.add(this.mPriorityList.get(b1)); 
      } 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("sort priorityList : ");
    stringBuilder.append(arrayList);
    Log.d("OplusResolveInfoHelper", stringBuilder.toString());
    this.mPriorityList.clear();
    this.mPriorityList.addAll(arrayList);
  }
  
  private List<String> getBlackResolveListWithType(String paramString) {
    if (paramString != null)
      return this.mCloudBlackResolveMap.get(paramString); 
    return null;
  }
  
  private List<String> getBlackChooseListWithType(String paramString) {
    if (paramString != null)
      return this.mCloudBlackChooseMap.get(paramString); 
    return null;
  }
  
  private List<String> getBlackChooseActivityListWithType(String paramString) {
    if (paramString != null)
      return this.mCloudBlackChooseActivityMap.get(paramString); 
    return null;
  }
  
  private List<String> getResolveListWithType(String paramString) {
    if (paramString != null)
      return this.mCloudResolveMap.get(paramString); 
    return null;
  }
  
  private List<String> getChooserListWithType(String paramString) {
    if (paramString != null)
      return this.mCloudChooserMap.get(paramString); 
    return null;
  }
  
  public void statisticsData(ResolveInfo paramResolveInfo, Intent paramIntent, int paramInt, String paramString) {
    HashMap<Object, Object> hashMap = new HashMap<>();
    String str1 = paramResolveInfo.getComponentInfo().getComponentName().flattenToShortString();
    PackageManager packageManager = this.mContext.getPackageManager();
    CharSequence charSequence = paramResolveInfo.loadLabel(packageManager);
    String str2 = OplusResolverIntentUtil.getIntentType(paramIntent);
    boolean bool = OplusResolverIntentUtil.isChooserAction(paramIntent);
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append(paramIntent);
    stringBuilder3.append("");
    hashMap.put("intent", stringBuilder3.toString());
    stringBuilder3 = new StringBuilder();
    stringBuilder3.append(paramIntent.getAction());
    stringBuilder3.append("");
    hashMap.put("action", stringBuilder3.toString());
    stringBuilder3 = new StringBuilder();
    stringBuilder3.append(paramIntent.getCategories());
    stringBuilder3.append("");
    hashMap.put("categories", stringBuilder3.toString());
    stringBuilder3 = new StringBuilder();
    stringBuilder3.append(paramIntent.getData());
    stringBuilder3.append("");
    hashMap.put("data", stringBuilder3.toString());
    stringBuilder3 = new StringBuilder();
    stringBuilder3.append(paramIntent.getType());
    stringBuilder3.append("");
    hashMap.put("mimeType", stringBuilder3.toString());
    stringBuilder3 = new StringBuilder();
    stringBuilder3.append(paramIntent.getScheme());
    stringBuilder3.append("");
    hashMap.put("scheme", stringBuilder3.toString());
    hashMap.put("type", str2);
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(bool);
    stringBuilder2.append("");
    hashMap.put("isChooser", stringBuilder2.toString());
    hashMap.put("referrerPackage", paramString);
    hashMap.put("targetPackage", paramResolveInfo.activityInfo.packageName);
    hashMap.put("name", str1);
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(charSequence);
    stringBuilder1.append("");
    hashMap.put("componentLabel", stringBuilder1.toString());
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append(paramInt);
    stringBuilder1.append("");
    hashMap.put("position", stringBuilder1.toString());
    OplusStatistics.onCommon(this.mContext, "20120", "resolver_app", hashMap, false);
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append("statistics data [resolver_app] :");
    stringBuilder1.append(hashMap);
    Log.d("OplusResolveInfoHelper", stringBuilder1.toString());
  }
  
  public void adjustPosition(List<ResolveInfo> paramList, List<String> paramList1) {
    ArrayList<ResolveInfo> arrayList1 = new ArrayList();
    ArrayList<ResolveInfo> arrayList2 = new ArrayList();
    ArrayList<ResolveInfo> arrayList3 = new ArrayList();
    byte b;
    for (b = 0; b < paramList.size(); b++) {
      String str = ((ResolveInfo)paramList.get(b)).activityInfo.packageName;
      if (paramList1.contains(str)) {
        ResolveInfo resolveInfo = paramList.get(b);
        arrayList1.add(resolveInfo);
      } else {
        arrayList3.add(paramList.get(b));
      } 
    } 
    for (b = 0; b < paramList1.size(); b++) {
      for (byte b1 = 0; b1 < arrayList1.size(); b1++) {
        if (((String)paramList1.get(b)).equals(((ResolveInfo)arrayList1.get(b1)).activityInfo.packageName))
          arrayList2.add(arrayList1.get(b1)); 
      } 
    } 
    paramList.clear();
    paramList.addAll(arrayList2);
    paramList.addAll(arrayList3);
  }
  
  public boolean isMarketRecommendType(String paramString) {
    if ("email".equals(paramString) || "video".equals(paramString) || 
      "txt".equals(paramString) || "pdf".equals(paramString) || 
      "word".equals(paramString) || "excel".equals(paramString) || 
      "ppt".equals(paramString))
      return true; 
    return false;
  }
  
  private boolean isSupportTopApp() {
    return OplusFeatureConfigManager.getInstacne().hasFeature("oplus.software.resolver_share_email");
  }
  
  public Map<String, String> getIconsMap() {
    try {
      OplusActivityManager oplusActivityManager = new OplusActivityManager();
      this();
      OplusResolveData oplusResolveData = oplusActivityManager.getResolveData();
      this.mIconNameMap = oplusResolveData.getIconName();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("iconMapsSize:");
      stringBuilder.append(this.mIconNameMap.size());
      Log.d("OplusResolveInfoHelper", stringBuilder.toString());
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getIconsMap RemoteException , ");
      stringBuilder.append(remoteException.getMessage());
      Log.e("OplusResolveInfoHelper", stringBuilder.toString());
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getIconsMap Exception , ");
      stringBuilder.append(exception.getMessage());
      Log.e("OplusResolveInfoHelper", stringBuilder.toString());
      exception.printStackTrace();
    } 
    return this.mIconNameMap;
  }
}
