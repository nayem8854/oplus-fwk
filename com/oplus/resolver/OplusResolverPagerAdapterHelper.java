package com.oplus.resolver;

import android.app.Activity;
import android.app.Dialog;
import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.IOplusThemeManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.UserHandle;
import android.util.Log;
import android.util.LruCache;
import android.view.View;
import com.android.internal.widget.DefaultItemAnimator;
import com.android.internal.widget.GridLayoutManager;
import com.android.internal.widget.RecyclerView;
import com.oplus.multiapp.OplusMultiAppManager;
import com.oplus.util.OplusResolverIntentUtil;
import com.oplus.util.OplusResolverUtil;
import com.oplus.widget.OplusItem;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OplusResolverPagerAdapterHelper implements IOplusResolverGridItemClickListener, IOplusGalleryPagerAdapter {
  private OplusResolverInfoHelper mResolveInfoHelper;
  
  private boolean mNeedMoreIcon = false;
  
  private boolean mNeedAnimation = false;
  
  private OplusItem mMoreItem = new OplusItem();
  
  private int mMoreIconTotalPosition;
  
  private LruCache<String, OplusItem> mMemoryCache = new LruCache(100);
  
  private IOplusResolverGridItemClickListener mItemClickListener;
  
  private OplusResolverCustomIconHelper mCustomIconHelper;
  
  private Context mContext;
  
  private Dialog mAlertDialog;
  
  private ExecutorService executor = Executors.newCachedThreadPool();
  
  private static final String TAG = "OplusResolverPagerAdapterHelper";
  
  OplusResolverPagerAdapterHelper(Context paramContext, Dialog paramDialog) {
    this.mContext = paramContext;
    this.mAlertDialog = paramDialog;
    this.mResolveInfoHelper = OplusResolverInfoHelper.getInstance(paramContext);
    this.mCustomIconHelper = new OplusResolverCustomIconHelper(paramContext);
    this.mMoreItem.setText(this.mContext.getString(201588861));
    this.mMoreItem.setIcon(this.mCustomIconHelper.getAdaptiveIcon(201851335));
  }
  
  public View createPagerView(List<OplusItem> paramList, int paramInt1, int paramInt2) {
    RecyclerView recyclerView = new RecyclerView(this.mContext);
    recyclerView.setOverScrollMode(2);
    recyclerView.setItemAnimator((RecyclerView.ItemAnimator)new DefaultItemAnimator());
    recyclerView.setLayoutManager((RecyclerView.LayoutManager)new GridLayoutManager(this.mContext, OplusResolverPagerAdapter.COLUMN_SIZE));
    OplusResolverGridAdapter oplusResolverGridAdapter = new OplusResolverGridAdapter(this.mContext);
    if (paramList != null) {
      int i = this.mMoreIconTotalPosition, j = i % paramInt2;
      paramInt2 = (int)Math.ceil((i + 1) / paramInt2);
      if (this.mNeedMoreIcon && paramInt2 == paramInt1 + 1) {
        oplusResolverGridAdapter.setOplusItems(initAppInfoTop(paramList, j));
      } else {
        oplusResolverGridAdapter.setOplusItems(paramList);
        if (this.mNeedAnimation && paramInt2 == paramInt1 + 1) {
          this.mNeedAnimation = false;
          oplusResolverGridAdapter.startMoreAnimation(j);
        } 
      } 
      oplusResolverGridAdapter.setPagerNumber(paramInt1);
      oplusResolverGridAdapter.setOnItemClickListener(this);
    } 
    recyclerView.setAdapter(oplusResolverGridAdapter);
    return (View)recyclerView;
  }
  
  public OplusItem[][] listToArray(List<OplusItem> paramList) {
    int i = (int)Math.min(Math.ceil(paramList.size() / OplusResolverPagerAdapter.COLUMN_SIZE), 2.0D);
    OplusItem[][] arrayOfOplusItem = new OplusItem[i][OplusResolverPagerAdapter.COLUMN_SIZE];
    int j = 0;
    int k = OplusResolverPagerAdapter.COLUMN_SIZE + 0;
    byte b = 0;
    while (j < paramList.size() && b < i) {
      if (k < paramList.size()) {
        m = k;
      } else {
        m = paramList.size();
      } 
      List<OplusItem> list = paramList.subList(j, m);
      System.arraycopy(list.toArray(), 0, arrayOfOplusItem[b], 0, list.size());
      int m = k + OplusResolverPagerAdapter.COLUMN_SIZE;
      b++;
      j = k;
      k = m;
    } 
    return arrayOfOplusItem;
  }
  
  public void setOplusResolverItemEventListener(IOplusResolverGridItemClickListener paramIOplusResolverGridItemClickListener) {
    this.mItemClickListener = paramIOplusResolverGridItemClickListener;
  }
  
  public void clickMoreIcon() {
    this.mNeedAnimation = true;
    this.mNeedMoreIcon = false;
  }
  
  public boolean isNeedMoreIcon() {
    return this.mNeedMoreIcon;
  }
  
  public int getMoreIconTotalPosition() {
    return this.mMoreIconTotalPosition;
  }
  
  public boolean isMoreIconPosition(int paramInt) {
    boolean bool;
    if (this.mNeedMoreIcon && this.mMoreIconTotalPosition == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void updateNeedMoreIcon(Intent paramIntent, int paramInt) {
    boolean bool = OplusResolverIntentUtil.isChooserAction(paramIntent);
    int i = this.mResolveInfoHelper.getResolveTopSize(paramIntent);
    if (!bool && i > 0 && i < paramInt) {
      this.mNeedMoreIcon = true;
      this.mMoreIconTotalPosition = i;
    } else {
      this.mNeedMoreIcon = false;
      this.mMoreIconTotalPosition = 0;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("init:resolveTopSize=");
    stringBuilder.append(i);
    stringBuilder.append(",mRiList=");
    stringBuilder.append(paramInt);
    stringBuilder.append(",mNeedMoreIcon=");
    stringBuilder.append(this.mNeedMoreIcon);
    Log.d("OplusResolverPagerAdapterHelper", stringBuilder.toString());
  }
  
  public void dismiss() {
    Dialog dialog = this.mAlertDialog;
    if (dialog != null && dialog.isShowing()) {
      this.mAlertDialog.dismiss();
    } else {
      Context context = this.mContext;
      if (context != null && context instanceof Activity && !((Activity)context).isFinishing())
        ((Activity)this.mContext).finish(); 
    } 
  }
  
  public List<OplusItem> loadBitmap(Intent paramIntent, List<ResolveInfo> paramList, int paramInt1, int paramInt2, int paramInt3) {
    List<OplusItem> list2, list1;
    ArrayList<OplusItem> arrayList = new ArrayList();
    if (paramList == null || paramList.isEmpty()) {
      OplusResolverCustomIconHelper oplusResolverCustomIconHelper = this.mCustomIconHelper;
      if ((paramInt1 + 1) * paramInt2 > paramInt3) {
        paramInt1 = paramInt3 - paramInt1 * paramInt2;
      } else {
        paramInt1 = paramInt2;
      } 
      list2 = oplusResolverCustomIconHelper.getDefaultAppInfo(paramInt1);
      return list2;
    } 
    BitmapTask bitmapTask = new BitmapTask((Intent)list2, paramList, paramInt1, paramInt2);
    try {
      list1 = this.executor.<List>submit(bitmapTask).get();
    } catch (CancellationException cancellationException) {
      cancellationException.printStackTrace();
      list1 = arrayList;
    } catch (ExecutionException executionException) {
      executionException.printStackTrace();
      list1 = arrayList;
    } catch (InterruptedException interruptedException) {
      interruptedException.printStackTrace();
      list1 = arrayList;
    } 
    return list1;
  }
  
  private List<OplusItem> initAppInfoTop(List<OplusItem> paramList, int paramInt) {
    ArrayList<OplusItem> arrayList = new ArrayList();
    byte b;
    int i;
    for (b = 0, i = paramList.size(); b < i; b++) {
      if (b == paramInt) {
        arrayList.add(this.mMoreItem);
        break;
      } 
      arrayList.add(paramList.get(b));
    } 
    return arrayList;
  }
  
  private OplusItem getBitmapFromMemCache(String paramString) {
    return (OplusItem)this.mMemoryCache.get(paramString);
  }
  
  private void addBitmapToMemoryCache(String paramString, OplusItem paramOplusItem) {
    if (getBitmapFromMemCache(paramString) == null)
      this.mMemoryCache.put(paramString, paramOplusItem); 
  }
  
  public void onItemClick(int paramInt1, int paramInt2) {
    IOplusResolverGridItemClickListener iOplusResolverGridItemClickListener = this.mItemClickListener;
    if (iOplusResolverGridItemClickListener != null)
      iOplusResolverGridItemClickListener.onItemClick(paramInt1, paramInt2); 
  }
  
  public void onItemLongClick(int paramInt1, int paramInt2) {
    IOplusResolverGridItemClickListener iOplusResolverGridItemClickListener = this.mItemClickListener;
    if (iOplusResolverGridItemClickListener != null)
      iOplusResolverGridItemClickListener.onItemLongClick(paramInt1, paramInt2); 
  }
  
  private class BitmapTask implements Callable<List<OplusItem>> {
    private Intent mOriginIntent;
    
    private int mPagerNumber;
    
    private int mPagerSize;
    
    private List<ResolveInfo> mRiList;
    
    final OplusResolverPagerAdapterHelper this$0;
    
    BitmapTask(Intent param1Intent, List<ResolveInfo> param1List, int param1Int1, int param1Int2) {
      this.mOriginIntent = param1Intent;
      this.mRiList = param1List;
      this.mPagerNumber = param1Int1;
      this.mPagerSize = param1Int2;
    }
    
    public List<OplusItem> call() {
      int k;
      ArrayList<OplusItem> arrayList = new ArrayList();
      List<ResolveInfo> list = this.mRiList;
      int i = this.mPagerNumber, j = this.mPagerSize;
      if ((i + 1) * j > list.size()) {
        k = this.mRiList.size();
      } else {
        k = (this.mPagerNumber + 1) * this.mPagerSize;
      } 
      list = list.subList(i * j, k);
      for (ResolveInfo resolveInfo : list) {
        OplusItem oplusItem1;
        boolean bool = OplusResolverPagerAdapterHelper.this.isMultiAppResolveInfo(resolveInfo);
        ActivityInfo activityInfo = resolveInfo.activityInfo;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(activityInfo.applicationInfo.packageName);
        stringBuilder.append(activityInfo.name);
        stringBuilder.append(bool);
        String str = stringBuilder.toString();
        OplusItem oplusItem2 = OplusResolverPagerAdapterHelper.this.getBitmapFromMemCache(str);
        if (oplusItem2 == null || OplusResolverUtil.isChooserCtsTest(OplusResolverPagerAdapterHelper.this.mContext, this.mOriginIntent)) {
          PackageManager packageManager = OplusResolverPagerAdapterHelper.this.mContext.getPackageManager();
          oplusItem2 = OplusResolverPagerAdapterHelper.this.mCustomIconHelper.getAppInfo(this.mOriginIntent, resolveInfo, packageManager);
          if (bool) {
            ComponentInfo componentInfo = resolveInfo.getComponentInfo();
            if (componentInfo != null) {
              String str1 = OplusMultiAppManager.getInstance().getMultiAppAlias(componentInfo.packageName);
              if (str1 != null)
                oplusItem2.setText(str1); 
            } else {
              StringBuilder stringBuilder1 = new StringBuilder();
              stringBuilder1.append(oplusItem2.getText());
              stringBuilder1.append(Resources.getSystem().getString(201588804));
              oplusItem2.setText(stringBuilder1.toString());
            } 
            UserHandle userHandle = new UserHandle(999);
            try {
              Context context = OplusResolverPagerAdapterHelper.this.mContext.createPackageContextAsUser(OplusResolverPagerAdapterHelper.this.mContext.getPackageName(), 0, userHandle);
              PackageManager packageManager1 = context.getPackageManager();
            } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
              nameNotFoundException.printStackTrace();
            } 
            oplusItem2.setIcon(packageManager.getUserBadgedIcon(oplusItem2.getIcon(), userHandle));
          } 
          oplusItem1 = oplusItem2;
        } else {
          Drawable drawable = oplusItem2.getIcon();
          oplusItem1 = oplusItem2;
          if (drawable != null) {
            Drawable.ConstantState constantState = drawable.getConstantState();
            Drawable drawable1 = constantState.newDrawable();
            ((IOplusThemeManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusThemeManager.DEFAULT, new Object[0])).setDarkFilterToDrawable(drawable1);
            oplusItem2.setIcon(drawable1);
            oplusItem1 = oplusItem2;
          } 
        } 
        arrayList.add(oplusItem1);
        OplusResolverPagerAdapterHelper.this.addBitmapToMemoryCache(str, oplusItem1);
      } 
      return arrayList;
    }
  }
  
  private boolean isMultiAppResolveInfo(ResolveInfo paramResolveInfo) {
    if (paramResolveInfo != null && paramResolveInfo.getComponentInfo() != null) {
      OplusMultiAppManager oplusMultiAppManager = OplusMultiAppManager.getInstance();
      return 
        oplusMultiAppManager.isMultiAppUserId(UserHandle.getUserId((paramResolveInfo.getComponentInfo()).applicationInfo.uid));
    } 
    return false;
  }
}
