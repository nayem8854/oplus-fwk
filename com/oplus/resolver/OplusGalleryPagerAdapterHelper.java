package com.oplus.resolver;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.util.Log;
import android.view.View;
import com.oplus.widget.OplusItem;
import java.lang.reflect.Constructor;
import java.util.List;

public class OplusGalleryPagerAdapterHelper {
  private static final String FEATURE_PACKAGE = "com.oplus.resolver.OplusResolverPagerAdapterHelper";
  
  private static final String TAG = "OplusResolverPagerAdapterHelper";
  
  private IOplusGalleryPagerAdapter mAdapterHelper = IOplusGalleryPagerAdapter.DEFAULT;
  
  private Context mContext;
  
  public OplusGalleryPagerAdapterHelper(Context paramContext, Dialog paramDialog) {
    this.mContext = paramContext;
    try {
      Class<?> clazz = Class.forName("com.oplus.resolver.OplusResolverPagerAdapterHelper");
      Constructor<?> constructor = clazz.getDeclaredConstructor(new Class[] { Context.class, Dialog.class });
      this.mAdapterHelper = (IOplusGalleryPagerAdapter)constructor.newInstance(new Object[] { paramContext, paramDialog });
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("cannot constructor error:");
      stringBuilder.append(exception.getMessage());
      Log.d("OplusResolverPagerAdapterHelper", stringBuilder.toString());
    } 
  }
  
  public View createPagerView(List<OplusItem> paramList, int paramInt1, int paramInt2) {
    View view2 = this.mAdapterHelper.createPagerView(paramList, paramInt1, paramInt2);
    View view1 = view2;
    if (view2 == null)
      view1 = new View(this.mContext); 
    return view1;
  }
  
  public OplusItem[][] listToArray(List<OplusItem> paramList) {
    return this.mAdapterHelper.listToArray(paramList);
  }
  
  public void dismiss() {
    this.mAdapterHelper.dismiss();
  }
  
  public List<OplusItem> loadBitmap(Intent paramIntent, List<ResolveInfo> paramList, int paramInt1, int paramInt2, int paramInt3) {
    return this.mAdapterHelper.loadBitmap(paramIntent, paramList, paramInt1, paramInt2, paramInt3);
  }
  
  public void setOplusResolverItemEventListener(IOplusResolverGridItemClickListener paramIOplusResolverGridItemClickListener) {
    this.mAdapterHelper.setOplusResolverItemEventListener(paramIOplusResolverGridItemClickListener);
  }
}
