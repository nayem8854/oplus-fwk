package com.oplus.resolver;

public interface IOplusResolverGridItemClickListener {
  void onItemClick(int paramInt1, int paramInt2);
  
  void onItemLongClick(int paramInt1, int paramInt2);
}
