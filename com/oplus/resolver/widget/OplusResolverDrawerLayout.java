package com.oplus.resolver.widget;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import com.android.internal.widget.ResolverDrawerLayout;
import com.oplus.internal.R;
import com.oplus.resolver.OplusResolverDialogHelper;

public class OplusResolverDrawerLayout extends ResolverDrawerLayout implements OplusResolverDialogHelper.OnProfileSelectedListener {
  private int mCurrentPager;
  
  private boolean mIsUserMaxHeight;
  
  private int mLandscapeMaxHeight;
  
  private int mLandscapeMaxWidth;
  
  private SparseIntArray mPagerHeight = new SparseIntArray();
  
  private int mPortraitMaxHeight;
  
  private int mPortraitMaxWidth;
  
  private View mSpaceView;
  
  private View mViewBottom;
  
  public OplusResolverDrawerLayout(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public OplusResolverDrawerLayout(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public OplusResolverDrawerLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    TypedArray typedArray2 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.OplusResolverDrawerLayout);
    this.mPortraitMaxWidth = typedArray2.getDimensionPixelSize(1, -1);
    this.mLandscapeMaxWidth = typedArray2.getDimensionPixelSize(0, -1);
    typedArray2.recycle();
    TypedArray typedArray1 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.OplusMaxLinearLayout);
    this.mPortraitMaxHeight = typedArray1.getDimensionPixelSize(2, 0);
    this.mLandscapeMaxHeight = typedArray1.getDimensionPixelSize(0, 0);
    typedArray1.recycle();
    View view = new View(getContext());
    view.setLayoutParams((ViewGroup.LayoutParams)new ResolverDrawerLayout.LayoutParams(-2, 0));
    this.mSpaceView.setBackgroundResource(201720065);
  }
  
  public void setIsUserMaxHeight(boolean paramBoolean, View paramView) {
    this.mIsUserMaxHeight = paramBoolean;
    this.mViewBottom = paramView;
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    updateNavigationBarColor();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    int j, i = View.MeasureSpec.getSize(paramInt1);
    Point point = getScreenSize();
    if (point.x < point.y) {
      j = 1;
    } else {
      j = 0;
    } 
    if ((j && this.mPortraitMaxWidth == -1) || (!j && this.mLandscapeMaxWidth == -1)) {
      super.onMeasure(paramInt1, paramInt2);
      j = paramInt1;
    } else {
      if (j != 0) {
        paramInt1 = this.mPortraitMaxWidth;
      } else {
        paramInt1 = this.mLandscapeMaxWidth;
      } 
      paramInt1 = Math.min(i, paramInt1);
      j = View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824);
      super.onMeasure(j, paramInt2);
    } 
    if (this.mIsUserMaxHeight) {
      paramInt1 = 0;
      int k;
      for (k = 0; k < getChildCount(); k++)
        paramInt1 += getChildAt(k).getMeasuredHeight(); 
      k = paramInt1;
      if (this.mSpaceView.getParent() != null)
        k = paramInt1 - this.mSpaceView.getMeasuredHeight(); 
      this.mPagerHeight.put(this.mCurrentPager, k);
      if (this.mPagerHeight.size() > 1) {
        int m = 0;
        for (paramInt1 = 0; paramInt1 < this.mPagerHeight.size(); paramInt1++, m = i1) {
          int n = this.mPagerHeight.valueAt(paramInt1);
          int i1 = m;
          if (m < n)
            i1 = n; 
        } 
        if (k < m) {
          (this.mSpaceView.getLayoutParams()).height = m - k;
          if (this.mSpaceView.getParent() == null) {
            View view = this.mViewBottom;
            if (view != null && view.getParent() instanceof ViewGroup) {
              paramInt1 = ((ViewGroup)this.mViewBottom.getParent()).indexOfChild(this.mViewBottom);
              ((ViewGroup)this.mViewBottom.getParent()).addView(this.mSpaceView, paramInt1 + 1);
            } else {
              addView(this.mSpaceView);
            } 
          } 
        } else {
          View view = this.mViewBottom;
          if (view != null && view.getParent() instanceof ViewGroup) {
            ((ViewGroup)this.mViewBottom.getParent()).removeView(this.mSpaceView);
          } else {
            removeView(this.mSpaceView);
          } 
        } 
        super.onMeasure(j, paramInt2);
      } 
    } 
    setMeasuredDimension(i, getMeasuredHeight());
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration) {
    super.onConfigurationChanged(paramConfiguration);
    updateNavigationBarColor();
  }
  
  public void onNestedScrollAccepted(View paramView1, View paramView2, int paramInt) {
    super.onNestedScrollAccepted(paramView1, paramView2, 0);
  }
  
  private void updateNavigationBarColor() {
    int i, j;
    if (!(getContext() instanceof Activity))
      return; 
    Point point = getScreenSize();
    if (point.x < point.y) {
      i = 1;
    } else {
      i = 0;
    } 
    if (i) {
      j = this.mPortraitMaxHeight;
    } else {
      j = this.mLandscapeMaxHeight;
    } 
    setMaxCollapsedHeight(j);
    Window window = ((Activity)getContext()).getWindow();
    Context context = getContext();
    if (i) {
      i = 201720065;
    } else {
      i = 17170445;
    } 
    window.setNavigationBarColor(context.getColor(i));
    requestLayout();
  }
  
  private Point getScreenSize() {
    Point point = new Point();
    WindowManager windowManager = (WindowManager)getContext().getSystemService("window");
    windowManager.getDefaultDisplay().getRealSize(point);
    return point;
  }
  
  public void onProfileSelected(int paramInt) {
    this.mCurrentPager = paramInt;
  }
}
