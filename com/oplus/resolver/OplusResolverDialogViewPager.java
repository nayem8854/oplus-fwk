package com.oplus.resolver;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import com.oplus.widget.OplusGridView;
import com.oplus.widget.OplusPagerAdapter;
import com.oplus.widget.OplusViewPager;
import java.util.List;

public class OplusResolverDialogViewPager extends OplusViewPager {
  public OplusResolverDialogViewPager(Context paramContext) {
    super(paramContext);
    setOverScrollMode(2);
  }
  
  public OplusResolverDialogViewPager(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    setOverScrollMode(2);
  }
  
  public int getCurrentItem() {
    return super.getCurrentItem();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    super.onMeasure(paramInt1, paramInt2);
    paramInt2 = 0;
    int i;
    for (i = 0; i < getChildCount(); i++, paramInt2 = k) {
      View view = getChildAt(i);
      int k = View.MeasureSpec.makeMeasureSpec(536870911, 0);
      view.measure(paramInt1, k);
      int m = view.getMeasuredHeight();
      k = paramInt2;
      if (m > paramInt2)
        k = m; 
    } 
    int j = getPaddingTop();
    i = getPaddingBottom();
    setMeasuredDimension(View.MeasureSpec.getSize(paramInt1), j + paramInt2 + i);
  }
  
  @Deprecated
  public void setGridViewList(List<OplusGridView> paramList, List<ResolveInfo> paramList1, Intent paramIntent, CheckBox paramCheckBox, Dialog paramDialog) {}
  
  public void setResolverItemEventListener(OplusPagerAdapter.OplusResolverItemEventListener paramOplusResolverItemEventListener) {
    OplusPagerAdapter oplusPagerAdapter = getAdapter();
    if (oplusPagerAdapter != null)
      oplusPagerAdapter.setOplusResolverItemEventListener(paramOplusResolverItemEventListener); 
  }
}
