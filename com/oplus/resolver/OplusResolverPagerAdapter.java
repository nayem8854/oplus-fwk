package com.oplus.resolver;

import android.app.Activity;
import android.app.ActivityManagerNative;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Parcelable;
import android.os.PatternMatcher;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import com.oplus.util.OplusResolverUtil;
import com.oplus.widget.OplusGridView;
import com.oplus.widget.OplusItem;
import com.oplus.widget.OplusPagerAdapter;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class OplusResolverPagerAdapter extends OplusPagerAdapter implements IOplusResolverGridItemClickListener {
  public static int COLUMN_SIZE = 4;
  
  private int mPagerSize = COLUMN_SIZE;
  
  private boolean mIsChecked = false;
  
  public static final String TAG = "OplusResolverPagerAdapter";
  
  private Activity mActivity;
  
  private IntentSender mChosenComponentSender;
  
  private Context mContext;
  
  private Intent mOriginIntent;
  
  private OplusResolverPagerAdapterHelper mPagerAdapterHelper;
  
  private int mPlaceholderCount;
  
  private List<ResolveInfo> mRiList;
  
  private boolean mSafeForwardingMode;
  
  @Deprecated
  public OplusResolverPagerAdapter(Context paramContext, List<OplusGridView> paramList, List<ResolveInfo> paramList1, int paramInt, Intent paramIntent, CheckBox paramCheckBox, Dialog paramDialog, boolean paramBoolean) {
    this.mActivity = (Activity)paramContext;
    this.mContext = paramContext;
    this.mRiList = paramList1;
    this.mOriginIntent = paramIntent;
    this.mSafeForwardingMode = paramBoolean;
    OplusResolverPagerAdapterHelper oplusResolverPagerAdapterHelper = new OplusResolverPagerAdapterHelper(paramContext, paramDialog);
    oplusResolverPagerAdapterHelper.setOplusResolverItemEventListener(this);
    updatePageSize(false);
    if (paramCheckBox != null)
      paramCheckBox.setOnCheckedChangeListener(new _$$Lambda$OplusResolverPagerAdapter$Xatdo9KoBxNVkqcP0XA_73_Jl2c(this)); 
  }
  
  public OplusResolverPagerAdapter(Context paramContext, List<ResolveInfo> paramList, Intent paramIntent, CheckBox paramCheckBox, boolean paramBoolean) {
    this(false, paramContext, paramList, paramIntent, paramCheckBox, paramBoolean);
  }
  
  public OplusResolverPagerAdapter(boolean paramBoolean1, Context paramContext, List<ResolveInfo> paramList, Intent paramIntent, CheckBox paramCheckBox, boolean paramBoolean2) {
    this.mActivity = (Activity)paramContext;
    this.mContext = paramContext;
    this.mRiList = paramList;
    this.mOriginIntent = paramIntent;
    this.mSafeForwardingMode = paramBoolean2;
    OplusResolverPagerAdapterHelper oplusResolverPagerAdapterHelper = new OplusResolverPagerAdapterHelper(paramContext, null);
    oplusResolverPagerAdapterHelper.setOplusResolverItemEventListener(this);
    updatePageSize(paramBoolean1);
    if (paramCheckBox != null)
      paramCheckBox.setOnCheckedChangeListener(new _$$Lambda$OplusResolverPagerAdapter$r6hLud8Y0_1Muk3L2y6G4bONPeo(this)); 
    if (OplusResolverUtil.isChooserCtsTest(paramContext, paramIntent)) {
      COLUMN_SIZE = 8;
    } else {
      COLUMN_SIZE = 4;
    } 
  }
  
  public int getCount() {
    List<ResolveInfo> list = this.mRiList;
    if (list == null || list.isEmpty())
      return (int)Math.ceil(this.mPlaceholderCount / this.mPagerSize); 
    if (this.mPagerAdapterHelper.isNeedMoreIcon())
      return (int)Math.ceil((this.mPagerAdapterHelper.getMoreIconTotalPosition() + 1) / this.mPagerSize); 
    return (int)Math.ceil(this.mRiList.size() / this.mPagerSize);
  }
  
  public boolean isViewFromObject(View paramView, Object paramObject) {
    boolean bool;
    if (paramView == paramObject) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public Object instantiateItem(ViewGroup paramViewGroup, int paramInt) {
    List<OplusItem> list = this.mPagerAdapterHelper.loadBitmap(this.mOriginIntent, this.mRiList, paramInt, this.mPagerSize, this.mPlaceholderCount);
    onInstantiateDataFinished(paramInt, list);
    View view = this.mPagerAdapterHelper.createPagerView(list, paramInt, this.mPagerSize);
    paramViewGroup.addView(view);
    return view;
  }
  
  public void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject) {
    paramViewGroup.removeView((View)paramObject);
  }
  
  public int getItemPosition(Object paramObject) {
    return -2;
  }
  
  public void onInstantiateDataFinished(int paramInt, List<OplusItem> paramList) {
    OplusGridView oplusGridView = new OplusGridView(this.mContext);
    OplusItem[][] arrayOfOplusItem = this.mPagerAdapterHelper.listToArray(paramList);
    oplusGridView.setAppInfo(arrayOfOplusItem);
    loadBitmap(paramInt, oplusGridView);
  }
  
  public void updatePageSize() {
    updatePageSize(false);
  }
  
  public void updatePageSize(boolean paramBoolean) {
    Configuration configuration = this.mContext.getResources().getConfiguration();
    if (configuration.orientation == 2) {
      this.mPagerSize = COLUMN_SIZE;
    } else {
      this.mPagerSize = COLUMN_SIZE * 2;
    } 
    Activity activity = this.mActivity;
    if (activity instanceof Activity && activity.isInMultiWindowMode())
      this.mPagerSize = COLUMN_SIZE; 
    if (paramBoolean)
      this.mPagerSize += COLUMN_SIZE; 
  }
  
  public void updateNeedMoreIcon(Intent paramIntent) {
    this.mPagerAdapterHelper.updateNeedMoreIcon(paramIntent, this.mRiList.size());
  }
  
  public boolean isMoreIconPositionAndClick(int paramInt) {
    boolean bool = this.mPagerAdapterHelper.isMoreIconPosition(paramInt);
    if (bool) {
      this.mPagerAdapterHelper.clickMoreIcon();
      notifyDataSetChanged();
    } 
    return bool;
  }
  
  public int getMoreIconTotalPosition() {
    return this.mPagerAdapterHelper.getMoreIconTotalPosition();
  }
  
  public void setPlaceholderCount(int paramInt) {
    this.mPlaceholderCount = paramInt;
  }
  
  public void onItemClick(int paramInt1, int paramInt2) {
    OnItemClick(this.mPagerSize * paramInt1 + paramInt2);
  }
  
  public void OnItemClick(int paramInt) {
    if (this.mOplusResolverItemEventListener != null) {
      this.mOplusResolverItemEventListener.OnItemClick(paramInt);
    } else {
      IntentFilter intentFilter = new IntentFilter();
      Intent intent = new Intent(this.mOriginIntent);
      intent.addFlags(50331648);
      if (paramInt >= this.mRiList.size() || paramInt < 0)
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onItemClick : ");
      stringBuilder.append(paramInt);
      stringBuilder.append(", ");
      stringBuilder.append(this.mRiList);
      Log.d("OplusResolverPagerAdapter", stringBuilder.toString());
      ActivityInfo activityInfo = ((ResolveInfo)this.mRiList.get(paramInt)).activityInfo;
      intent.setComponent(new ComponentName(activityInfo.applicationInfo.packageName, activityInfo.name));
      if (!isInLockTaskMode()) {
        safelyStartActivity(intent, this.mActivity, this.mSafeForwardingMode);
        this.mActivity.overridePendingTransition(201916431, 201916432);
      } 
      ResolveInfo resolveInfo = this.mRiList.get(paramInt);
      if (this.mIsChecked) {
        if (intent.getAction() != null)
          intentFilter.addAction(intent.getAction()); 
        Set set = intent.getCategories();
        if (set != null)
          for (String str : set)
            intentFilter.addCategory(str);  
        intentFilter.addCategory("android.intent.category.DEFAULT");
        paramInt = 0xFFF0000 & resolveInfo.match;
        Uri uri = intent.getData();
        IntentFilter intentFilter1 = intentFilter;
        if (paramInt == 6291456) {
          String str = intent.resolveType(this.mContext);
          intentFilter1 = intentFilter;
          if (str != null)
            try {
              intentFilter.addDataType(str);
              intentFilter1 = intentFilter;
            } catch (android.content.IntentFilter.MalformedMimeTypeException malformedMimeTypeException) {
              malformedMimeTypeException = null;
            }  
        } 
        if (malformedMimeTypeException != null && uri != null && uri.getScheme() != null)
          if (paramInt != 6291456 || (
            !"file".equals(uri.getScheme()) && !"content".equals(uri.getScheme()))) {
            malformedMimeTypeException.addDataScheme(uri.getScheme());
            if (resolveInfo.filter != null) {
              Iterator<IntentFilter.AuthorityEntry> iterator = resolveInfo.filter.authoritiesIterator();
              if (iterator != null)
                while (iterator.hasNext()) {
                  IntentFilter.AuthorityEntry authorityEntry = iterator.next();
                  if (authorityEntry.match(uri) >= 0) {
                    paramInt = authorityEntry.getPort();
                    String str = authorityEntry.getHost();
                    if (paramInt >= 0) {
                      String str1 = Integer.toString(paramInt);
                    } else {
                      iterator = null;
                    } 
                    malformedMimeTypeException.addDataAuthority(str, (String)iterator);
                    break;
                  } 
                }  
              iterator = resolveInfo.filter.pathsIterator();
              if (iterator != null) {
                String str = uri.getPath();
                while (str != null && iterator.hasNext()) {
                  PatternMatcher patternMatcher = (PatternMatcher)iterator.next();
                  if (patternMatcher.match(str)) {
                    malformedMimeTypeException.addDataPath(patternMatcher.getPath(), patternMatcher.getType());
                    break;
                  } 
                } 
              } 
            } 
          }  
        int i = this.mRiList.size();
        ComponentName[] arrayOfComponentName = new ComponentName[i];
        int j = 0;
        for (paramInt = 0; paramInt < i; paramInt++, j = k) {
          ResolveInfo resolveInfo1 = this.mRiList.get(paramInt);
          arrayOfComponentName[paramInt] = new ComponentName(resolveInfo1.activityInfo.packageName, resolveInfo1.activityInfo.name);
          int k = j;
          if (resolveInfo1.match > j)
            k = resolveInfo1.match; 
        } 
        this.mContext.getPackageManager().addPreferredActivity((IntentFilter)malformedMimeTypeException, j, arrayOfComponentName, intent.getComponent());
      } 
      this.mPagerAdapterHelper.dismiss();
    } 
  }
  
  public void onItemLongClick(int paramInt1, int paramInt2) {
    OnItemLongClick(this.mPagerSize * paramInt1 + paramInt2);
  }
  
  public void OnItemLongClick(int paramInt) {
    if (this.mOplusResolverItemEventListener != null)
      this.mOplusResolverItemEventListener.OnItemLongClick(paramInt); 
  }
  
  @Deprecated
  public void unRegister() {}
  
  private static boolean isInLockTaskMode() {
    try {
      return ActivityManagerNative.getDefault().isInLockTaskMode();
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  private void safelyStartActivity(Intent paramIntent, Activity paramActivity, boolean paramBoolean) {
    if (!paramBoolean) {
      try {
        paramActivity.startActivity(paramIntent);
        onActivityStarted(paramIntent);
      } catch (RuntimeException runtimeException) {
        runtimeException.printStackTrace();
      } 
      return;
    } 
    try {
      paramActivity.startActivityAsCaller((Intent)runtimeException, null, null, false, -10000);
      onActivityStarted((Intent)runtimeException);
    } catch (RuntimeException runtimeException1) {
      String str;
      try {
        str = ActivityManagerNative.getDefault().getLaunchedFromPackage(paramActivity.getActivityToken());
      } catch (RemoteException remoteException) {
        str = "??";
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" safelyStartActivity : ");
      stringBuilder.append(str);
      Log.d("OplusResolverPagerAdapter", stringBuilder.toString());
    } 
  }
  
  private void onActivityStarted(Intent paramIntent) {
    if (this.mChosenComponentSender != null) {
      ComponentName componentName = paramIntent.getComponent();
      if (componentName != null) {
        Intent intent = (new Intent()).putExtra("android.intent.extra.CHOSEN_COMPONENT", (Parcelable)componentName);
        try {
          this.mChosenComponentSender.sendIntent(this.mContext, -1, intent, null, null);
        } catch (android.content.IntentSender.SendIntentException sendIntentException) {}
      } 
    } 
  }
  
  @Deprecated
  public void loadBitmap(int paramInt, OplusGridView paramOplusGridView) {}
  
  public void setChosenComponentSender(IntentSender paramIntentSender) {
    this.mChosenComponentSender = paramIntentSender;
  }
  
  public void updateIntent(Intent paramIntent) {
    this.mOriginIntent = paramIntent;
  }
}
