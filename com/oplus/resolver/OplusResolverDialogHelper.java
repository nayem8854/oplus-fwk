package com.oplus.resolver;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import com.android.internal.app.ResolverListAdapter;
import com.oplus.util.OplusContextUtil;
import com.oplus.util.OplusResolverIntentUtil;
import com.oplus.widget.OplusPagerAdapter;
import com.oplus.widget.OplusViewPager;
import com.oplus.widget.indicator.OplusPageIndicator;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OplusResolverDialogHelper {
  private List<ResolveInfo> mRiList = new ArrayList<>();
  
  private List<ResolveInfo> mList = new ArrayList<>();
  
  private Set<OnProfileSelectedListener> mProfileSelectedListener = new HashSet<>();
  
  private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
      final OplusResolverDialogHelper this$0;
      
      public void onReceive(Context param1Context, Intent param1Intent) {
        String str = param1Intent.getAction();
        if ("oppo.intent.action.STOP_CHOOSER".equals(str) && 
          OplusResolverDialogHelper.this.mActivity != null && !OplusResolverDialogHelper.this.mActivity.isFinishing())
          OplusResolverDialogHelper.this.mActivity.finish(); 
      }
    };
  
  private BroadcastReceiver mAPKChangedReceiver = new BroadcastReceiver() {
      final OplusResolverDialogHelper this$0;
      
      public void onReceive(Context param1Context, Intent param1Intent) {
        if (OplusResolverDialogHelper.this.mActivity != null && !OplusResolverDialogHelper.this.mActivity.isFinishing())
          OplusResolverDialogHelper.this.mActivity.finish(); 
      }
    };
  
  private static final String ACTION_CHOOSER_STOP = "oppo.intent.action.STOP_CHOOSER";
  
  private static final String CODE = "20120";
  
  private static final String GALLERY_PIN_LIST = "gallery_pin_list";
  
  private static final String KEY = "49";
  
  private static final String KEY_TYPE = "type";
  
  private static final String NEW_APP_HEYTAP_MARKET = "com.heytap.market";
  
  private static final String PERSON_PROFILE = "person_profile";
  
  private static final String RECOMMEND_EVENT_ID = "resolver_recommend";
  
  private static final String SECRET = "be7a52eaeb67a660ecfdcff7c742c8a2";
  
  private static final String TAG = "OplusResolverDialogHelper";
  
  private static final String TYPE_GALLERY = "gallery";
  
  private static final String WORK_PROFILE = "work_profile";
  
  private Activity mActivity;
  
  private Context mContext;
  
  private AdapterView.OnItemLongClickListener mLongClickListener;
  
  private AdapterView.OnItemClickListener mOnItemClickListener;
  
  private OplusResolverOshare mOplusResolverOshare;
  
  private Intent mOriginIntent;
  
  private OplusResolverPagerAdapter mPagerAdapter;
  
  private OplusResolverInfoHelper mResolveInfoHelper;
  
  private OplusResolverDialogViewPager mViewPager;
  
  public void initOShareService() {
    this.mOplusResolverOshare.initOShareService();
  }
  
  public void initOShareView(View paramView) {
    this.mOplusResolverOshare.initOShareView(paramView);
  }
  
  public void tearDown(View paramView) {
    this.mOplusResolverOshare.dismissOShareView();
    paramView = paramView.findViewById(201457837);
    if (!(paramView instanceof ViewStub))
      paramView.setVisibility(8); 
  }
  
  public List<ResolveInfo> getResolveInforList() {
    return this.mList;
  }
  
  private void addInitiaIntents(Intent[] paramArrayOfIntent) {
    if (paramArrayOfIntent != null)
      for (byte b = 0; b < paramArrayOfIntent.length; b++) {
        Intent intent = paramArrayOfIntent[b];
        if (intent != null) {
          ActivityInfo activityInfo = intent.resolveActivityInfo(this.mContext.getPackageManager(), 0);
          if (activityInfo == null) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("No activity found for ");
            stringBuilder.append(intent);
            Log.w("OplusResolverDialogHelper", stringBuilder.toString());
          } else {
            ResolveInfo resolveInfo = new ResolveInfo();
            resolveInfo.activityInfo = activityInfo;
            if (intent instanceof LabeledIntent) {
              LabeledIntent labeledIntent = (LabeledIntent)intent;
              resolveInfo.resolvePackageName = labeledIntent.getSourcePackage();
              resolveInfo.labelRes = labeledIntent.getLabelResource();
              resolveInfo.nonLocalizedLabel = labeledIntent.getNonLocalizedLabel();
              resolveInfo.icon = labeledIntent.getIconResource();
            } 
            this.mList.add(resolveInfo);
          } 
        } 
      }  
  }
  
  private void setResolveView(boolean paramBoolean1, final OplusResolverDialogViewPager viewPager, final OplusPageIndicator dotView, CheckBox paramCheckBox, boolean paramBoolean2, int paramInt) {
    OplusResolverPagerAdapter oplusResolverPagerAdapter = new OplusResolverPagerAdapter(paramBoolean1, this.mContext, this.mList, this.mOriginIntent, paramCheckBox, paramBoolean2);
    this.mViewPager = viewPager;
    oplusResolverPagerAdapter.setPlaceholderCount(paramInt);
    viewPager.setAdapter(this.mPagerAdapter);
    viewPager.setResolverItemEventListener(new OplusPagerAdapter.OplusResolverItemEventListener() {
          final OplusResolverDialogHelper this$0;
          
          final OplusResolverDialogViewPager val$viewPager;
          
          public void OnItemLongClick(int param1Int) {
            if (OplusResolverDialogHelper.this.mLongClickListener != null) {
              OplusResolverDialogHelper.this.mLongClickListener.onItemLongClick(null, null, param1Int, -1L);
              viewPager.performHapticFeedback(0);
            } 
          }
          
          public void OnItemClick(int param1Int) {
            OplusResolverDialogHelper oplusResolverDialogHelper = OplusResolverDialogHelper.this;
            if (oplusResolverDialogHelper.clickMoreIcon(oplusResolverDialogHelper.mActivity, param1Int))
              return; 
            if (OplusResolverDialogHelper.this.mOnItemClickListener != null)
              OplusResolverDialogHelper.this.mOnItemClickListener.onItemClick(null, null, param1Int, -1L); 
          }
        });
    viewPager.setOnPageChangeListener(new OplusViewPager.OnPageChangeListener() {
          final OplusResolverDialogHelper this$0;
          
          final OplusPageIndicator val$dotView;
          
          public void onPageSelected(int param1Int) {
            dotView.onPageSelected(param1Int);
          }
          
          public void onPageScrolled(int param1Int1, float param1Float, int param1Int2) {
            dotView.onPageScrolled(param1Int1, param1Float, param1Int2);
          }
          
          public void onPageScrollStateChanged(int param1Int) {
            dotView.onPageScrollStateChanged(param1Int);
          }
        });
  }
  
  public OplusResolverDialogHelper(Activity paramActivity, Intent paramIntent, Intent[] paramArrayOfIntent, boolean paramBoolean, List<ResolveInfo> paramList) {
    this.mLongClickListener = null;
    this.mOnItemClickListener = null;
    this.mContext = (Context)paramActivity;
    this.mActivity = paramActivity;
    this.mOriginIntent = paramIntent;
    this.mOplusResolverOshare = new OplusResolverOshare((Context)paramActivity, paramIntent);
    if (paramList != null) {
      this.mRiList = paramList;
    } else {
      if (paramIntent == null && paramArrayOfIntent == null)
        return; 
      PackageManager packageManager = paramActivity.getPackageManager();
      byte b = 0;
      List<ResolveInfo> list = packageManager.queryIntentActivities(paramIntent, 0);
      if (list.size() == 0) {
        Intent intent = new Intent();
        if (paramIntent.getAction() != null)
          intent.setAction(paramIntent.getAction()); 
        if (paramIntent.getType() != null)
          intent.setType(paramIntent.getType()); 
        if (paramIntent.getExtras() != null)
          intent.putExtras(paramIntent.getExtras()); 
        PackageManager packageManager1 = paramActivity.getPackageManager();
        if (paramBoolean)
          b = 64; 
        this.mRiList = packageManager1.queryIntentActivities(intent, b | 0x10000);
      } 
      addInitiaIntents(paramArrayOfIntent);
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("init ");
    stringBuilder2.append(this.mRiList);
    stringBuilder2.append(", ");
    stringBuilder2.append(this.mOriginIntent);
    Log.d("OplusResolverDialogHelper", stringBuilder2.toString());
    OplusResolverInfoHelper oplusResolverInfoHelper = OplusResolverInfoHelper.getInstance((Context)paramActivity);
    oplusResolverInfoHelper.resort(this.mRiList, paramIntent);
    this.mList.addAll(this.mRiList);
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("resort ");
    stringBuilder1.append(this.mRiList);
    Log.d("OplusResolverDialogHelper", stringBuilder1.toString());
  }
  
  public void setOnItemLongClickListener(AdapterView.OnItemLongClickListener paramOnItemLongClickListener) {
    this.mLongClickListener = paramOnItemLongClickListener;
  }
  
  public void setOnItemClickListener(AdapterView.OnItemClickListener paramOnItemClickListener) {
    this.mOnItemClickListener = paramOnItemClickListener;
  }
  
  public OplusResolverPagerAdapter getPagerAdapter() {
    return this.mPagerAdapter;
  }
  
  private boolean clickMoreIcon(Activity paramActivity, int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("clickMoreIcon : ");
    stringBuilder.append(paramInt);
    Log.d("OplusResolverDialogHelper", stringBuilder.toString());
    if (this.mPagerAdapter.isMoreIconPositionAndClick(paramInt)) {
      OplusPageIndicator oplusPageIndicator = (OplusPageIndicator)paramActivity.findViewById(201457765);
      paramInt = this.mPagerAdapter.getCount();
      oplusPageIndicator.setDotsCount(paramInt);
      return true;
    } 
    return false;
  }
  
  public void showTargetDetails(ResolveInfo paramResolveInfo, SharedPreferences paramSharedPreferences, String paramString, ResolverListAdapter paramResolverListAdapter) {
    boolean bool;
    int i;
    String str = paramResolveInfo.activityInfo.getComponentName().flattenToShortString();
    Set set = null;
    if ("gallery".equals(paramString)) {
      String str1 = Settings.Secure.getString(this.mContext.getContentResolver(), "gallery_pin_list");
      if (!TextUtils.isEmpty(str1))
        set = new HashSet(Arrays.asList((Object[])str1.split(";"))); 
    } else {
      set = paramSharedPreferences.getStringSet(paramString, null);
    } 
    if (set != null) {
      bool = set.contains(str);
    } else {
      bool = false;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("showTargetDetails : ");
    stringBuilder.append(set);
    stringBuilder.append(", type : ");
    stringBuilder.append(paramString);
    stringBuilder.append(", componentName : ");
    stringBuilder.append(str);
    stringBuilder.append(", isPinned : ");
    stringBuilder.append(bool);
    Log.d("OplusResolverDialogHelper", stringBuilder.toString());
    AlertDialog.Builder builder2 = new AlertDialog.Builder(this.mContext);
    try {
      Method method = builder2.getClass().getSuperclass().getDeclaredMethod("setDialogType", new Class[] { int.class });
      method.setAccessible(true);
      method.invoke(builder2, new Object[] { Integer.valueOf(1) });
    } catch (Exception exception) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Failed to reflect setDialogType: ");
      stringBuilder1.append(exception.getMessage());
      Log.w("OplusResolverDialogHelper", stringBuilder1.toString());
    } 
    if (bool) {
      i = 201785372;
    } else {
      i = 201785371;
    } 
    AlertDialog.Builder builder1 = builder2.setItems(i, new _$$Lambda$OplusResolverDialogHelper$vxSh5HSgJkR5oQCXMCd5AImLNig(this, paramSharedPreferences, str, paramString, paramResolverListAdapter, paramResolveInfo));
    -$.Lambda.OplusResolverDialogHelper.szgrw_IvmhjanVi7xgDY16DCtK8 szgrw_IvmhjanVi7xgDY16DCtK8 = _$$Lambda$OplusResolverDialogHelper$szgrw_IvmhjanVi7xgDY16DCtK8.INSTANCE;
    builder1 = builder1.setNegativeButton(17039360, (DialogInterface.OnClickListener)szgrw_IvmhjanVi7xgDY16DCtK8);
    AlertDialog alertDialog = builder1.create();
    alertDialog.show();
  }
  
  private void updatePinnedData(SharedPreferences paramSharedPreferences, String paramString1, String paramString2) {
    boolean bool1 = false, bool2 = false;
    if (!"gallery".equals(paramString2)) {
      StringBuilder stringBuilder1;
      Set<?> set = paramSharedPreferences.getStringSet(paramString2, null);
      HashSet<String> hashSet = new HashSet();
      bool1 = bool2;
      if (set != null) {
        bool1 = set.contains(paramString1);
        hashSet = new HashSet(set);
      } 
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("newList = ");
      stringBuilder2.append(hashSet);
      Log.d("OplusResolverDialogHelper", stringBuilder2.toString());
      if (bool1) {
        paramSharedPreferences.edit().remove(paramString2).apply();
        hashSet.remove(paramString1);
        paramSharedPreferences.edit().putStringSet(paramString2, hashSet).apply();
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("remove : ");
        stringBuilder1.append(paramString1);
        Log.d("OplusResolverDialogHelper", stringBuilder1.toString());
      } else {
        stringBuilder1.edit().remove(paramString2).apply();
        hashSet.add(paramString1);
        stringBuilder1.edit().putStringSet(paramString2, hashSet).apply();
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("add : ");
        stringBuilder1.append(paramString1);
        Log.d("OplusResolverDialogHelper", stringBuilder1.toString());
      } 
    } else {
      paramString2 = Settings.Secure.getString(this.mContext.getContentResolver(), "gallery_pin_list");
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("galleryPinList = ");
      stringBuilder2.append(paramString2);
      Log.d("OplusResolverDialogHelper", stringBuilder2.toString());
      List<String> list = new ArrayList();
      if (!TextUtils.isEmpty(paramString2)) {
        list = Arrays.asList(paramString2.split(";"));
        bool1 = list.contains(paramString1);
        list = new ArrayList<>(list);
      } 
      StringBuilder stringBuilder3 = new StringBuilder();
      stringBuilder3.append("newList = ");
      stringBuilder3.append(list);
      Log.d("OplusResolverDialogHelper", stringBuilder3.toString());
      if (bool1) {
        list.remove(paramString1);
        stringBuilder3 = new StringBuilder();
        stringBuilder3.append("remove : ");
        stringBuilder3.append(paramString1);
        Log.d("OplusResolverDialogHelper", stringBuilder3.toString());
      } else {
        list.add(paramString1);
        stringBuilder3 = new StringBuilder();
        stringBuilder3.append("add : ");
        stringBuilder3.append(paramString1);
        Log.d("OplusResolverDialogHelper", stringBuilder3.toString());
      } 
      paramString1 = listToString(list, ';');
      Settings.Secure.putString(this.mContext.getContentResolver(), "gallery_pin_list", paramString1);
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("putStringForUser : ");
      stringBuilder1.append(paramString1);
      Log.d("OplusResolverDialogHelper", stringBuilder1.toString());
    } 
  }
  
  private String listToString(List<String> paramList, char paramChar) {
    StringBuilder stringBuilder = new StringBuilder();
    for (byte b = 0; b < paramList.size(); b++) {
      if (b == paramList.size() - 1) {
        stringBuilder.append(paramList.get(b));
      } else {
        stringBuilder.append(paramList.get(b));
        stringBuilder.append(paramChar);
      } 
    } 
    return stringBuilder.toString();
  }
  
  public void showRecommend(Activity paramActivity) {
    final View marketJump = paramActivity.findViewById(201457766);
    if (view == null)
      return; 
    if (!isMarketEnable()) {
      view.setVisibility(8);
      Log.d("OplusResolverDialogHelper", "Market is disable");
      return;
    } 
    final String intentType = OplusResolverIntentUtil.getIntentType(this.mOriginIntent);
    if (!this.mResolveInfoHelper.isMarketRecommendType(str)) {
      view.setVisibility(8);
      Log.d("OplusResolverDialogHelper", "not is MarketRecommend Type");
      return;
    } 
    AsyncTask<Void, Void, Boolean> asyncTask = new AsyncTask<Void, Void, Boolean>() {
        final OplusResolverDialogHelper this$0;
        
        final String val$intentType;
        
        final View val$marketJump;
        
        protected Boolean doInBackground(Void... param1VarArgs) {
          OplusResolverDialogHelper oplusResolverDialogHelper = OplusResolverDialogHelper.this;
          return Boolean.valueOf(oplusResolverDialogHelper.support(oplusResolverDialogHelper.mContext));
        }
        
        protected void onPostExecute(Boolean param1Boolean) {
          OplusResolverDialogHelper.this.showMarket(param1Boolean.booleanValue(), marketJump, intentType);
        }
      };
    asyncTask.execute((Object[])new Void[0]);
  }
  
  private void showMarket(boolean paramBoolean, View paramView, String paramString) {
    if (!paramBoolean) {
      paramView.setVisibility(8);
      return;
    } 
    paramView.setVisibility(0);
    String str = paramString;
    if ("txt".equals(paramString))
      str = "text"; 
    paramView.setOnClickListener(new _$$Lambda$OplusResolverDialogHelper$M2JY20CDfqP2LO_cJ4MUiGF5W6I(this, str));
  }
  
  private boolean startRecommend(Context paramContext, String paramString) {
    int j;
    byte b1 = 0, b2 = 0;
    boolean bool = false;
    int i = b1;
    try {
      Uri uri = Uri.parse("content://oaps_mk");
      i = b1;
      Bundle bundle2 = new Bundle();
      i = b1;
      this();
      i = b1;
      bundle2.putString("rtp", paramString);
      i = b1;
      bundle2.putString("goback", "1");
      i = b1;
      bundle2.putString("secret", "be7a52eaeb67a660ecfdcff7c742c8a2");
      i = b1;
      bundle2.putString("enterId", "49");
      i = b1;
      bundle2.putString("sgtp", "1");
      i = b1;
      Bundle bundle1 = call(paramContext, uri, "/recapp", bundle2);
      j = b2;
      if (bundle1 != null) {
        j = b2;
        i = b1;
        if (bundle1.containsKey("code")) {
          i = b1;
          j = bundle1.getInt("code");
        } 
      } 
      i = j;
      StringBuilder stringBuilder = new StringBuilder();
      i = j;
      this();
      i = j;
      stringBuilder.append("startRecommend:");
      i = j;
      stringBuilder.append(paramString);
      i = j;
      stringBuilder.append(",response:");
      i = j;
      stringBuilder.append(j);
      i = j;
    } finally {
      paramContext = null;
      paramContext.printStackTrace();
    } 
    if (j == 1)
      bool = true; 
    return bool;
  }
  
  private boolean support(Context paramContext) {
    boolean bool2;
    Uri uri = Uri.parse("content://oaps_mk");
    Bundle bundle = new Bundle();
    bundle.putString("tp", "/recapp");
    bundle.putString("secret", "be7a52eaeb67a660ecfdcff7c742c8a2");
    bundle.putString("enterId", "49");
    bundle.putString("sgtp", "1");
    boolean bool1 = false;
    byte b = 0;
    try {
      Bundle bundle1 = call(paramContext, uri, "/support", bundle);
    } finally {
      paramContext = null;
      paramContext.printStackTrace();
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("oaps support:");
    stringBuilder.append(bool2);
    Log.d("OplusResolverDialogHelper", stringBuilder.toString());
    boolean bool3 = true;
    if (bool2 != true)
      bool3 = false; 
    return bool3;
  }
  
  private Bundle call(Context paramContext, Uri paramUri, String paramString, Bundle paramBundle) {
    try {
      return contentResolver.call(paramUri, paramString, "", paramBundle);
    } finally {
      paramContext = null;
      paramContext.printStackTrace();
    } 
  }
  
  private boolean isMarketEnable() {
    PackageInfo packageInfo = null;
    try {
      PackageInfo packageInfo1 = this.mContext.getPackageManager().getPackageInfo("com.heytap.market", 8192);
    } catch (Exception exception) {}
    boolean bool1 = false, bool2 = bool1;
    if (packageInfo != null) {
      bool2 = bool1;
      if (packageInfo.applicationInfo != null) {
        bool2 = bool1;
        if (packageInfo.applicationInfo.enabled)
          bool2 = true; 
      } 
    } 
    return bool2;
  }
  
  public void dialogHelperDestroy() {
    this.mOplusResolverOshare.onDestroy();
    try {
      this.mContext.unregisterReceiver(this.mBroadcastReceiver);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("fail to unregister receiver, ");
      stringBuilder.append(exception);
      Log.d("OplusResolverDialogHelper", stringBuilder.toString());
    } 
    try {
      this.mContext.unregisterReceiver(this.mAPKChangedReceiver);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("fail to unregister receiver, ");
      stringBuilder.append(exception);
      Log.d("OplusResolverDialogHelper", stringBuilder.toString());
    } 
  }
  
  public void oSharePause() {
    this.mOplusResolverOshare.onPause();
  }
  
  public void oShareResume() {
    this.mOplusResolverOshare.onResume();
  }
  
  public void initNfcView(View paramView) {
    Intent intent = this.mOriginIntent;
    if (intent == null && intent.getAction() == null)
      return; 
    String str = this.mOriginIntent.getAction();
    if (!"android.nfc.action.NDEF_DISCOVERED".equals(str) && !"android.nfc.action.TECH_DISCOVERED".equals(str) && 
      !"android.nfc.action.TAG_DISCOVERED".equals(str))
      return; 
    paramView = paramView.findViewById(201457837);
    if (paramView instanceof ViewStub) {
      paramView = ((ViewStub)paramView).inflate();
      paramView.setId(201457837);
    } else {
      paramView.setVisibility(0);
    } 
  }
  
  public void initTabView(View paramView, OnProfileSelectedListener paramOnProfileSelectedListener, int paramInt) {
    this.mProfileSelectedListener.add(paramOnProfileSelectedListener);
    TabHost tabHost = (TabHost)paramView.findViewById(201457851);
    tabHost.setup();
    TabHost.TabSpec tabSpec2 = tabHost.newTabSpec("person_profile").setContent(201457850);
    Context context2 = this.mContext;
    tabSpec2 = tabSpec2.setIndicator(context2.getString(17041184));
    tabHost.addTab(tabSpec2);
    TabHost.TabSpec tabSpec3 = tabHost.newTabSpec("work_profile").setContent(201457850);
    Context context1 = this.mContext;
    TabHost.TabSpec tabSpec1 = tabSpec3.setIndicator(context1.getString(17041188));
    tabHost.addTab(tabSpec1);
    initTabProfile(tabHost.getTabWidget());
    resetTabsHeaderStyle(tabHost.getTabWidget());
    tabHost.setCurrentTab(1 - paramInt);
    tabHost.setCurrentTab(paramInt);
    updateActiveTabStyle(tabHost);
    tabHost.setOnTabChangedListener(new _$$Lambda$OplusResolverDialogHelper$mYVEbkY4QWzUjfreQ4plxtctrXE(this, tabHost));
  }
  
  public void initChooserTopBroadcast() {
    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction("oppo.intent.action.STOP_CHOOSER");
    this.mContext.registerReceiver(this.mBroadcastReceiver, intentFilter);
    intentFilter = new IntentFilter();
    intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
    intentFilter.addDataScheme("package");
    this.mContext.registerReceiver(this.mAPKChangedReceiver, intentFilter);
  }
  
  public View createView(boolean paramBoolean1, boolean paramBoolean2, int paramInt) {
    View view = this.mActivity.getLayoutInflater().inflate(202440736, null);
    OplusResolverDialogViewPager oplusResolverDialogViewPager = (OplusResolverDialogViewPager)view.findViewById(201457768);
    OplusPageIndicator oplusPageIndicator = (OplusPageIndicator)view.findViewById(201457765);
    setResolveView(paramBoolean1, oplusResolverDialogViewPager, oplusPageIndicator, null, paramBoolean2, paramInt);
    updateDotView((View)oplusPageIndicator);
    return view;
  }
  
  public void reLoadTabPlaceholderCount(boolean paramBoolean, int paramInt) {
    this.mPagerAdapter.setPlaceholderCount(paramInt);
    this.mRiList.clear();
    this.mList.clear();
    updatePageSize(paramBoolean);
    this.mPagerAdapter.notifyDataSetChanged();
    this.mViewPager.setCurrentItem(0);
  }
  
  private void resetTabsHeaderStyle(TabWidget paramTabWidget) {
    byte b;
    int i;
    for (b = 0, i = paramTabWidget.getChildCount(); b < i; b++) {
      paramTabWidget.getChildAt(b).setBackground(null);
      TextView textView = (TextView)paramTabWidget.getChildAt(b).findViewById(16908310);
      textView.setTextColor(this.mContext.getColor(17170444));
    } 
  }
  
  private void updateActiveTabStyle(TabHost paramTabHost) {
    paramTabHost.getTabWidget().getChildAt(paramTabHost.getCurrentTab()).setBackgroundResource(201851340);
    TextView textView = (TextView)paramTabHost.getTabWidget().getChildAt(paramTabHost.getCurrentTab()).findViewById(16908310);
    textView.setTextColor(OplusContextUtil.getAttrColor(this.mContext, 201982027));
  }
  
  private void initTabProfile(TabWidget paramTabWidget) {
    byte b;
    int i;
    for (b = 0, i = paramTabWidget.getChildCount(); b < i; b++) {
      ViewGroup.LayoutParams layoutParams = paramTabWidget.getChildAt(b).getLayoutParams();
      layoutParams.height = this.mContext.getResources().getDimensionPixelSize(201654632);
      paramTabWidget.getChildAt(b).setLayoutParams(layoutParams);
      TextView textView = (TextView)paramTabWidget.getChildAt(b).findViewById(16908310);
      textView.setTextSize(0, this.mContext.getResources().getDimensionPixelSize(201654307));
    } 
  }
  
  private void updateDotView(View paramView) {
    OplusResolverPagerAdapter oplusResolverPagerAdapter = this.mPagerAdapter;
    if (oplusResolverPagerAdapter == null || paramView == null || !(paramView instanceof OplusPageIndicator))
      return; 
    int i = oplusResolverPagerAdapter.getCount();
    ((OplusPageIndicator)paramView).setDotsCount(i);
  }
  
  public boolean initCheckBox(Intent paramIntent, View paramView, boolean paramBoolean) {
    View view = null;
    if (paramBoolean) {
      paramView = paramView.findViewById(201457764);
      view = paramView;
      if (paramView instanceof ViewStub)
        view = ((ViewStub)paramView).inflate(); 
    } 
    boolean bool = false;
    try {
      paramBoolean = paramIntent.getBooleanExtra("oppo_filemanager_openflag", false);
    } catch (Exception exception) {
      exception.printStackTrace();
      paramBoolean = bool;
    } 
    if (paramBoolean && view != null)
      view.setVisibility(8); 
    return paramBoolean;
  }
  
  public void resortListAndNotifyChange(boolean paramBoolean, List<ResolveInfo> paramList) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getPagerAdapter : ()Lcom/oplus/resolver/OplusResolverPagerAdapter;
    //   4: invokevirtual getMoreIconTotalPosition : ()I
    //   7: istore_3
    //   8: aload_0
    //   9: getfield mList : Ljava/util/List;
    //   12: invokeinterface size : ()I
    //   17: istore #4
    //   19: aload_0
    //   20: getfield mPagerAdapter : Lcom/oplus/resolver/OplusResolverPagerAdapter;
    //   23: invokevirtual getCount : ()I
    //   26: istore #5
    //   28: aload_2
    //   29: ifnull -> 37
    //   32: aload_0
    //   33: aload_2
    //   34: putfield mRiList : Ljava/util/List;
    //   37: aload_0
    //   38: getfield mResolveInfoHelper : Lcom/oplus/resolver/OplusResolverInfoHelper;
    //   41: aload_0
    //   42: getfield mRiList : Ljava/util/List;
    //   45: aload_0
    //   46: getfield mOriginIntent : Landroid/content/Intent;
    //   49: invokevirtual resort : (Ljava/util/List;Landroid/content/Intent;)V
    //   52: aload_0
    //   53: getfield mList : Ljava/util/List;
    //   56: invokeinterface clear : ()V
    //   61: aload_0
    //   62: getfield mList : Ljava/util/List;
    //   65: aload_0
    //   66: getfield mRiList : Ljava/util/List;
    //   69: invokeinterface addAll : (Ljava/util/Collection;)Z
    //   74: pop
    //   75: new java/lang/StringBuilder
    //   78: dup
    //   79: invokespecial <init> : ()V
    //   82: astore_2
    //   83: aload_2
    //   84: ldc_w 'sort '
    //   87: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   90: pop
    //   91: aload_2
    //   92: aload_0
    //   93: getfield mRiList : Ljava/util/List;
    //   96: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   99: pop
    //   100: aload_2
    //   101: ldc ', '
    //   103: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   106: pop
    //   107: aload_2
    //   108: aload_0
    //   109: getfield mOriginIntent : Landroid/content/Intent;
    //   112: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   115: pop
    //   116: ldc 'OplusResolverDialogHelper'
    //   118: aload_2
    //   119: invokevirtual toString : ()Ljava/lang/String;
    //   122: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   125: pop
    //   126: aload_0
    //   127: iload_1
    //   128: invokespecial updatePageSize : (Z)V
    //   131: aload_0
    //   132: getfield mOriginIntent : Landroid/content/Intent;
    //   135: invokestatic isChooserAction : (Landroid/content/Intent;)Z
    //   138: ifne -> 162
    //   141: aload_0
    //   142: getfield mResolveInfoHelper : Lcom/oplus/resolver/OplusResolverInfoHelper;
    //   145: astore_2
    //   146: aload_0
    //   147: getfield mOriginIntent : Landroid/content/Intent;
    //   150: astore #6
    //   152: iload_3
    //   153: aload_2
    //   154: aload #6
    //   156: invokevirtual getResolveTopSize : (Landroid/content/Intent;)I
    //   159: if_icmpne -> 199
    //   162: iload #4
    //   164: aload_0
    //   165: getfield mList : Ljava/util/List;
    //   168: invokeinterface size : ()I
    //   173: if_icmpne -> 199
    //   176: aload_0
    //   177: getfield mPagerAdapter : Lcom/oplus/resolver/OplusResolverPagerAdapter;
    //   180: astore_2
    //   181: iload #5
    //   183: aload_2
    //   184: invokevirtual getCount : ()I
    //   187: if_icmpeq -> 193
    //   190: goto -> 199
    //   193: iconst_0
    //   194: istore #4
    //   196: goto -> 202
    //   199: iconst_1
    //   200: istore #4
    //   202: aload_0
    //   203: getfield mPagerAdapter : Lcom/oplus/resolver/OplusResolverPagerAdapter;
    //   206: astore_2
    //   207: aload_2
    //   208: ifnull -> 245
    //   211: iload #4
    //   213: ifeq -> 238
    //   216: aload_2
    //   217: aload_0
    //   218: getfield mOriginIntent : Landroid/content/Intent;
    //   221: invokevirtual updateNeedMoreIcon : (Landroid/content/Intent;)V
    //   224: aload_0
    //   225: aload_0
    //   226: getfield mActivity : Landroid/app/Activity;
    //   229: ldc_w 201457765
    //   232: invokevirtual findViewById : (I)Landroid/view/View;
    //   235: invokespecial updateDotView : (Landroid/view/View;)V
    //   238: aload_0
    //   239: getfield mPagerAdapter : Lcom/oplus/resolver/OplusResolverPagerAdapter;
    //   242: invokevirtual notifyDataSetChanged : ()V
    //   245: aload_0
    //   246: getfield mViewPager : Lcom/oplus/resolver/OplusResolverDialogViewPager;
    //   249: astore_2
    //   250: aload_2
    //   251: ifnull -> 264
    //   254: iload #4
    //   256: ifeq -> 264
    //   259: aload_2
    //   260: iconst_0
    //   261: invokevirtual setCurrentItem : (I)V
    //   264: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #676	-> 0
    //   #677	-> 8
    //   #678	-> 19
    //   #680	-> 28
    //   #681	-> 32
    //   #683	-> 37
    //   #684	-> 52
    //   #685	-> 61
    //   #686	-> 75
    //   #688	-> 126
    //   #690	-> 131
    //   #691	-> 152
    //   #692	-> 181
    //   #693	-> 202
    //   #694	-> 211
    //   #695	-> 216
    //   #696	-> 224
    //   #698	-> 238
    //   #701	-> 245
    //   #702	-> 259
    //   #704	-> 264
  }
  
  private void updatePageSize(boolean paramBoolean) {
    OplusResolverPagerAdapter oplusResolverPagerAdapter = this.mPagerAdapter;
    if (oplusResolverPagerAdapter != null)
      oplusResolverPagerAdapter.updatePageSize(paramBoolean); 
  }
  
  public void addProfileSelectedListener(OnProfileSelectedListener paramOnProfileSelectedListener) {
    if (paramOnProfileSelectedListener != null)
      this.mProfileSelectedListener.add(paramOnProfileSelectedListener); 
  }
  
  public void removeProfileSelectedListener(OnProfileSelectedListener paramOnProfileSelectedListener) {
    if (paramOnProfileSelectedListener != null)
      this.mProfileSelectedListener.remove(paramOnProfileSelectedListener); 
  }
  
  public void restoreProfilePager(View paramView, int paramInt) {
    TabHost tabHost = (TabHost)paramView.findViewById(201457851);
    if (tabHost != null)
      tabHost.setCurrentTab(paramInt); 
  }
  
  public static interface OnProfileSelectedListener {
    void onProfileSelected(int param1Int);
  }
}
