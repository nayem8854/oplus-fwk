package com.oplus.resolver;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.res.IOplusThemeManager;
import android.content.res.XmlResourceParser;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.util.Pair;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.File;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

public class OplusResolverApkPreView {
  private static final String ATTR_NAME = "name";
  
  private static final String ATTR_PATH = "path";
  
  private static final File DEVICE_ROOT = new File("/");
  
  private static final String META_DATA_FILE_PROVIDER_PATHS = "android.support.FILE_PROVIDER_PATHS";
  
  private static final String TAG = "OplusResolverApkPreView";
  
  private static final String TAG_CACHE_PATH = "cache-path";
  
  private static final String TAG_EXTERNAL = "external-path";
  
  private static final String TAG_EXTERNAL_CACHE = "external-cache-path";
  
  private static final String TAG_EXTERNAL_FILES = "external-files-path";
  
  private static final String TAG_EXTERNAL_MEDIA = "external-media-path";
  
  private static final String TAG_FILES_PATH = "files-path";
  
  private static final String TAG_ROOT_PATH = "root-path";
  
  private Context mContext;
  
  private StagingAsyncTask mStagingAsyncTask;
  
  public OplusResolverApkPreView(Context paramContext) {
    this.mContext = paramContext;
  }
  
  public void execute(Uri paramUri, ImageView paramImageView, TextView paramTextView) {
    paramTextView.setVisibility(4);
    paramImageView.setVisibility(4);
    StagingAsyncTask stagingAsyncTask = new StagingAsyncTask(paramImageView, paramTextView);
    stagingAsyncTask.execute((Object[])new Uri[] { paramUri });
  }
  
  public void onDestroy() {
    StagingAsyncTask stagingAsyncTask = this.mStagingAsyncTask;
    if (stagingAsyncTask != null)
      stagingAsyncTask.cancel(true); 
  }
  
  private final class StagingAsyncTask extends AsyncTask<Uri, Void, String> {
    private ImageView mImageView;
    
    private TextView mTextView;
    
    final OplusResolverApkPreView this$0;
    
    StagingAsyncTask(ImageView param1ImageView, TextView param1TextView) {
      this.mImageView = param1ImageView;
      this.mTextView = param1TextView;
    }
    
    protected String doInBackground(Uri... param1VarArgs) {
      if (param1VarArgs == null || param1VarArgs.length <= 0)
        return null; 
      Uri uri = param1VarArgs[0];
      return getRealFilePath(OplusResolverApkPreView.this.mContext, uri);
    }
    
    protected void onPostExecute(String param1String) {
      if (param1String == null) {
        this.mImageView.setImageResource(201851290);
      } else {
        PackageManager packageManager = OplusResolverApkPreView.this.mContext.getPackageManager();
        PackageInfo packageInfo = packageManager.getPackageArchiveInfo(param1String, 1);
        if (packageInfo != null) {
          CharSequence charSequence = packageManager.getApplicationLabel(packageInfo.applicationInfo);
          Drawable drawable = ((IOplusThemeManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusThemeManager.DEFAULT, new Object[0])).loadOverlayResolverDrawable(OplusResolverApkPreView.this.mContext.getPackageManager(), packageInfo.packageName, packageInfo.applicationInfo.icon, packageInfo.applicationInfo, null);
          this.mImageView.setImageDrawable(drawable);
          this.mTextView.setText(charSequence);
        } else {
          this.mImageView.setImageResource(201851290);
        } 
      } 
      this.mImageView.setVisibility(0);
      this.mTextView.setVisibility(0);
    }
    
    public String getRealFilePath(Context param1Context, Uri param1Uri) {
      if (param1Uri == null)
        return null; 
      String str1 = param1Uri.getScheme();
      String str2 = null, str3 = null, str4 = null;
      if (str1 == null) {
        str3 = param1Uri.getPath();
      } else if ("file".equals(str1)) {
        str3 = param1Uri.getPath();
      } else if ("content".equals(str1)) {
        try {
          Cursor cursor = param1Context.getContentResolver().query(param1Uri, null, null, null, null);
          str3 = str2;
          if (cursor != null) {
            str3 = str4;
            if (cursor.moveToFirst()) {
              int i = cursor.getColumnIndex("_data");
              str3 = str4;
              if (i > -1)
                str3 = cursor.getString(i); 
            } 
            cursor.close();
          } 
        } catch (Exception exception) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("getRealFilePath error:");
          stringBuilder1.append(exception.getMessage());
          Log.d("OplusResolverApkPreView", stringBuilder1.toString());
          str3 = OplusResolverApkPreView.this.getFPUriToPath(param1Context, param1Uri);
        } 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("get path:");
      stringBuilder.append(str3);
      Log.d("OplusResolverApkPreView", stringBuilder.toString());
      return str3;
    }
  }
  
  private String getFPUriToPath(Context paramContext, Uri paramUri) {
    String str = paramUri.getAuthority();
    if (str == null)
      return null; 
    try {
      Pair<String, File> pair = parsePathStrategy(paramContext, str);
      return getFileForUri(paramUri, pair).getAbsolutePath();
    } catch (Exception exception) {
      return null;
    } 
  }
  
  private Pair<String, File> parsePathStrategy(Context paramContext, String paramString) throws IOException, XmlPullParserException {
    File file;
    PackageManager packageManager = paramContext.getPackageManager();
    ProviderInfo providerInfo = packageManager.resolveContentProvider(paramString, 128);
    if (providerInfo != null) {
      PackageManager packageManager1 = paramContext.getPackageManager();
      XmlResourceParser xmlResourceParser = providerInfo.loadXmlMetaData(packageManager1, "android.support.FILE_PROVIDER_PATHS");
      if (xmlResourceParser != null) {
        while (true) {
          int i = xmlResourceParser.next();
          if (i != 1) {
            if (i == 2) {
              String str1 = xmlResourceParser.getName();
              String str2 = xmlResourceParser.getAttributeValue(null, "name");
              String str3 = xmlResourceParser.getAttributeValue(null, "path");
              packageManager1 = null;
              providerInfo = null;
              PackageManager packageManager2 = null;
              if ("root-path".equals(str1)) {
                file = DEVICE_ROOT;
              } else if ("files-path".equals(str1)) {
                file = paramContext.getFilesDir();
              } else if ("cache-path".equals(str1)) {
                file = paramContext.getCacheDir();
              } else if ("external-path".equals(str1)) {
                file = Environment.getExternalStorageDirectory();
              } else if ("external-files-path".equals(str1)) {
                File[] arrayOfFile = paramContext.getExternalFilesDirs(null);
                packageManager1 = packageManager2;
                if (arrayOfFile.length > 0)
                  file = arrayOfFile[0]; 
              } else {
                File[] arrayOfFile;
                if ("external-cache-path".equals(str1)) {
                  arrayOfFile = paramContext.getExternalCacheDirs();
                  if (arrayOfFile.length > 0)
                    file = arrayOfFile[0]; 
                } else if (Build.VERSION.SDK_INT >= 21) {
                  File[] arrayOfFile1 = arrayOfFile;
                  if ("external-media-path".equals(str1)) {
                    File[] arrayOfFile2 = paramContext.getExternalMediaDirs();
                    arrayOfFile1 = arrayOfFile;
                    if (arrayOfFile2.length > 0)
                      file = arrayOfFile2[0]; 
                  } 
                } 
              } 
              if (file != null)
                return new Pair(str2, new File(file, str3)); 
            } 
            continue;
          } 
          break;
        } 
        return null;
      } 
      throw new IllegalArgumentException("Missing android.support.FILE_PROVIDER_PATHS meta-data");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Couldn't find meta-data for provider with authority ");
    stringBuilder.append((String)file);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public File getFileForUri(Uri paramUri, Pair<String, File> paramPair) {
    File file;
    String str1 = paramUri.getEncodedPath();
    if (paramPair == null || str1 == null)
      return null; 
    int i = str1.indexOf('/', 1);
    String str2 = Uri.decode(str1.substring(1, i));
    str1 = Uri.decode(str1.substring(i + 1));
    if (str2.equals(paramPair.first)) {
      File file1 = (File)paramPair.second;
      file = new File(file1, str1);
      try {
        File file2 = file.getCanonicalFile();
        if (file2.getPath().startsWith(file1.getPath()))
          return file2; 
        throw new SecurityException("Resolved path jumped beyond configured root");
      } catch (IOException iOException) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Failed to resolve canonical path for ");
        stringBuilder1.append(file);
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unable to find configured root for ");
    stringBuilder.append(file);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
}
