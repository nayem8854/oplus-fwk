package com.oplus.resolver;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.IOplusThemeManager;
import android.graphics.drawable.Drawable;
import com.oplus.util.OplusResolverIntentUtil;
import com.oplus.util.OplusResolverUtil;
import com.oplus.widget.OplusItem;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OplusResolverCustomIconHelper {
  private static final Map<String, String> DEFAULT_RESOLVER_ICON;
  
  private static final String TAG = "OplusResolverCustomIconHelper";
  
  private static final int mColumnCounts = OplusResolverPagerAdapter.COLUMN_SIZE;
  
  private Context mContext;
  
  private Map<String, String> mIconNameMap;
  
  static {
    HashMap<Object, Object> hashMap = new HashMap<>();
    hashMap.put("cn.wps.moffice_eng/cn.wps.moffice.documentmanager.PreStartActivity2/excel", "share_excel");
    DEFAULT_RESOLVER_ICON.put("cn.wps.moffice_eng/cn.wps.moffice.documentmanager.PreStartActivity2/pdf", "share_pdf");
    DEFAULT_RESOLVER_ICON.put("cn.wps.moffice_eng/cn.wps.moffice.documentmanager.PreStartActivity2/ppt", "share_ppt");
    DEFAULT_RESOLVER_ICON.put("cn.wps.moffice_eng/cn.wps.moffice.documentmanager.PreStartActivity2/word", "share_word");
    DEFAULT_RESOLVER_ICON.put("com.android.bluetooth/.opp.BluetoothOppLauncherActivity", "share_blue");
    DEFAULT_RESOLVER_ICON.put("com.baidu.netdisk/.p2pshare.ui.ReceiverP2PShareFileActivity", "share_lightnin");
    DEFAULT_RESOLVER_ICON.put("com.eg.android.AlipayGphone/com.alipay.mobile.quinox.splash.ShareScanQRDispenseActivity", "share_scan");
    DEFAULT_RESOLVER_ICON.put("com.mt.mtxx.mtxx/com.meitu.mtxx.img.IMGMainActivity", "share_edit");
    DEFAULT_RESOLVER_ICON.put("com.mt.mtxx.mtxx/.beauty.BeautyMainActivity", "share_retouch");
    DEFAULT_RESOLVER_ICON.put("com.sina.weibo/.weiyou.share.WeiyouShareDispatcher", "share_chat");
    DEFAULT_RESOLVER_ICON.put("com.sina.weibo/.story.publisher.StoryDispatcher", "share_story");
    DEFAULT_RESOLVER_ICON.put("com.tencent.eim/com.tencent.mobileqq.activity.qfileJumpActivity", "share_pc");
    DEFAULT_RESOLVER_ICON.put("com.tencent.mm/.ui.tools.AddFavoriteUI", "share_fav");
    DEFAULT_RESOLVER_ICON.put("com.tencent.mm/.ui.tools.ShareToTimeLineUI", "share_moment");
    DEFAULT_RESOLVER_ICON.put("com.tencent.mobileqq/cooperation.qqfav.widget.QfavJumpActivity", "share_fav");
    DEFAULT_RESOLVER_ICON.put("com.tencent.mobileqq/.activity.qfileJumpActivity", "share_pc");
    DEFAULT_RESOLVER_ICON.put("com.tencent.mobileqq/cooperation.qlink.QlinkShareJumpActivity", "share_quick");
    DEFAULT_RESOLVER_ICON.put("com.tencent.qqlite/cooperation.qqfav.widget.QfavJumpActivity", "share_fav");
    DEFAULT_RESOLVER_ICON.put("com.tencent.qqlite/com.tencent.mobileqq.activity.qfileJumpActivity", "share_pc");
    DEFAULT_RESOLVER_ICON.put("com.tencent.tim/cooperation.qqfav.widget.QfavJumpActivity", "share_fav");
    DEFAULT_RESOLVER_ICON.put("com.UCMobile/.share", "share_chat");
  }
  
  OplusResolverCustomIconHelper(Context paramContext) {
    this.mContext = paramContext;
    Map<String, String> map = OplusResolverInfoHelper.getInstance(paramContext).getIconsMap();
    if (map == null || map.isEmpty())
      this.mIconNameMap = DEFAULT_RESOLVER_ICON; 
  }
  
  public OplusItem getAppInfo(Intent paramIntent, ResolveInfo paramResolveInfo, PackageManager paramPackageManager) {
    if (paramResolveInfo == null)
      return null; 
    OplusItem oplusItem = new OplusItem();
    oplusItem.setText(paramResolveInfo.loadLabel(paramPackageManager).toString());
    if (OplusResolverUtil.isChooserCtsTest(this.mContext, paramIntent)) {
      ApplicationInfo applicationInfo = paramResolveInfo.activityInfo.applicationInfo;
      if (applicationInfo != null) {
        String str = applicationInfo.loadLabel(paramPackageManager).toString();
        if (!oplusItem.getText().equals(str))
          oplusItem.setLabel(str); 
      } 
    } 
    oplusItem.setIcon(oplusLoadIconForResolveInfo(paramIntent, paramResolveInfo, paramPackageManager));
    return oplusItem;
  }
  
  public List<OplusItem> getDefaultAppInfo(int paramInt) {
    Drawable drawable;
    try {
      ApplicationInfo applicationInfo = this.mContext.getPackageManager().getApplicationInfo(this.mContext.getPackageName(), 128);
      drawable = ((IOplusThemeManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusThemeManager.DEFAULT, new Object[0])).loadOverlayResolverDrawable(this.mContext.getPackageManager(), applicationInfo.packageName, 17303426, applicationInfo, null);
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      drawable = this.mContext.getDrawable(17303426);
    } 
    ArrayList<OplusItem> arrayList = new ArrayList();
    int i = (int)Math.min(Math.ceil(paramInt / mColumnCounts), 2.0D);
    for (byte b = 0; b < i; b++) {
      byte b1 = 0;
      while (true) {
        int j = mColumnCounts;
        if (b1 < paramInt - b * j && b1 < j) {
          OplusItem oplusItem = new OplusItem();
          oplusItem.setIcon(drawable.getConstantState().newDrawable());
          oplusItem.setText("");
          arrayList.add(oplusItem);
          b1++;
          continue;
        } 
        break;
      } 
    } 
    return arrayList;
  }
  
  public Drawable getAdaptiveIcon(int paramInt) {
    try {
      ApplicationInfo applicationInfo = this.mContext.getPackageManager().getApplicationInfo(this.mContext.getPackageName(), 128);
      return ((IOplusThemeManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusThemeManager.DEFAULT, new Object[0])).loadOverlayResolverDrawable(this.mContext.getPackageManager(), applicationInfo.packageName, paramInt, applicationInfo, null);
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      return this.mContext.getDrawable(paramInt);
    } 
  }
  
  private Drawable oplusLoadIconForResolveInfo(Intent paramIntent, ResolveInfo paramResolveInfo, PackageManager paramPackageManager) {
    int i = paramResolveInfo.getIconResource();
    String str1 = paramResolveInfo.activityInfo.packageName;
    ApplicationInfo applicationInfo1 = paramResolveInfo.activityInfo.applicationInfo;
    int j = i;
    String str2 = str1;
    ApplicationInfo applicationInfo2 = applicationInfo1;
    if (paramResolveInfo.resolvePackageName != null) {
      j = i;
      str2 = str1;
      applicationInfo2 = applicationInfo1;
      if (paramResolveInfo.icon != 0) {
        i = paramResolveInfo.icon;
        j = i;
        str2 = str1;
        applicationInfo2 = applicationInfo1;
        if (paramResolveInfo.activityInfo.packageName != null) {
          String str3 = paramResolveInfo.resolvePackageName, str4 = paramResolveInfo.activityInfo.packageName;
          j = i;
          str2 = str1;
          applicationInfo2 = applicationInfo1;
          if (!str3.contains(str4)) {
            str2 = paramResolveInfo.resolvePackageName;
            applicationInfo2 = null;
            j = i;
          } 
        } 
      } 
    } 
    if (j != 0) {
      IOplusThemeManager iOplusThemeManager = (IOplusThemeManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusThemeManager.DEFAULT, new Object[0]);
      String str4 = paramResolveInfo.activityInfo.name;
      String str3 = getResolveDrawableName(str2, str4, OplusResolverIntentUtil.getIntentType(paramIntent));
      Drawable drawable = iOplusThemeManager.loadOverlayResolverDrawable(paramPackageManager, str2, j, applicationInfo2, str3);
      if (drawable != null)
        return drawable; 
    } 
    return paramResolveInfo.loadIcon(paramPackageManager);
  }
  
  private String getResolveDrawableName(String paramString1, String paramString2, String paramString3) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString1);
    stringBuilder.append("/");
    stringBuilder.append(paramString2);
    stringBuilder.append("/");
    stringBuilder.append(paramString3);
    String str1 = this.mIconNameMap.get(stringBuilder.toString());
    String str2 = str1;
    if (str1 == null) {
      str2 = str1;
      if (paramString2.startsWith(paramString1)) {
        stringBuilder.delete(0, stringBuilder.length());
        stringBuilder.append(paramString1);
        stringBuilder.append("/");
        stringBuilder.append(paramString2.substring(paramString1.length()));
        stringBuilder.append("/");
        stringBuilder.append(paramString3);
        str2 = this.mIconNameMap.get(stringBuilder.toString());
      } 
    } 
    paramString3 = str2;
    if (str2 == null) {
      stringBuilder.delete(0, stringBuilder.length());
      stringBuilder.append(paramString1);
      stringBuilder.append("/");
      stringBuilder.append(paramString2);
      paramString3 = this.mIconNameMap.get(stringBuilder.toString());
    } 
    str2 = paramString3;
    if (paramString3 == null) {
      str2 = paramString3;
      if (paramString2.startsWith(paramString1)) {
        stringBuilder.delete(0, stringBuilder.length());
        stringBuilder.append(paramString1);
        stringBuilder.append("/");
        stringBuilder.append(paramString2.substring(paramString1.length()));
        str2 = this.mIconNameMap.get(stringBuilder.toString());
      } 
    } 
    return str2;
  }
}
