package com.oplus.resolver;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.view.View;
import com.oplus.widget.OplusItem;
import java.util.ArrayList;
import java.util.List;

public interface IOplusGalleryPagerAdapter {
  public static final IOplusGalleryPagerAdapter DEFAULT = (IOplusGalleryPagerAdapter)new Object();
  
  default List<OplusItem> loadBitmap(Intent paramIntent, List<ResolveInfo> paramList, int paramInt1, int paramInt2, int paramInt3) {
    return new ArrayList<>();
  }
  
  default OplusItem[][] listToArray(List<OplusItem> paramList) {
    return new OplusItem[0][0];
  }
  
  default View createPagerView(List<OplusItem> paramList, int paramInt1, int paramInt2) {
    return null;
  }
  
  default void dismiss() {}
  
  default void setOplusResolverItemEventListener(IOplusResolverGridItemClickListener paramIOplusResolverGridItemClickListener) {}
}
