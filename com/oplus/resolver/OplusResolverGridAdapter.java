package com.oplus.resolver;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.internal.widget.RecyclerView;
import com.oplus.util.OplusChangeTextUtil;
import com.oplus.widget.OplusItem;
import java.util.List;

public class OplusResolverGridAdapter extends RecyclerView.Adapter<OplusResolverGridAdapter.GridViewHolder> {
  private int mAppNameSize;
  
  private int mMoreIconIndex = -1;
  
  private IOplusResolverGridItemClickListener mOnItemClickListener;
  
  private int mPagerNumber;
  
  private List<OplusItem> oplusItems;
  
  public OplusResolverGridAdapter(Context paramContext) {
    Resources resources = paramContext.getResources();
    int i = resources.getDimensionPixelSize(201654586);
    float f = (paramContext.getResources().getConfiguration()).fontScale;
    this.mAppNameSize = (int)OplusChangeTextUtil.getSuitableFontSize(i, f, 2);
  }
  
  public void setOnItemClickListener(IOplusResolverGridItemClickListener paramIOplusResolverGridItemClickListener) {
    this.mOnItemClickListener = paramIOplusResolverGridItemClickListener;
  }
  
  public GridViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt) {
    View view = LayoutInflater.from(paramViewGroup.getContext()).inflate(202440737, paramViewGroup, false);
    return new GridViewHolder(view);
  }
  
  public void onBindViewHolder(GridViewHolder paramGridViewHolder, int paramInt) {
    List<OplusItem> list = this.oplusItems;
    if (list == null || list.size() <= paramInt)
      return; 
    paramGridViewHolder.mIcon.setImageDrawable(((OplusItem)this.oplusItems.get(paramInt)).getIcon());
    if (paramGridViewHolder.mLabel != null) {
      String str = ((OplusItem)this.oplusItems.get(paramInt)).getLabel();
      if (!TextUtils.isEmpty(str)) {
        paramGridViewHolder.mLabel.setVisibility(0);
        paramGridViewHolder.mLabel.setText(str);
      } else if (paramGridViewHolder.mLabel.getVisibility() != 8) {
        paramGridViewHolder.mLabel.setVisibility(8);
      } 
    } 
    paramGridViewHolder.mName.setText(((OplusItem)this.oplusItems.get(paramInt)).getText());
    paramGridViewHolder.mName.setTextSize(0, this.mAppNameSize);
    paramGridViewHolder.itemView.setOnClickListener(new _$$Lambda$OplusResolverGridAdapter$KA3mCLIl0oe4k5Pqf4QiVY1lm_I(this, paramInt));
    paramGridViewHolder.mIcon.setOnClickListener(new _$$Lambda$OplusResolverGridAdapter$AXGhkU8pjKzpXllJi8Cb_JDTZsw(this, paramInt));
    paramGridViewHolder.itemView.setOnLongClickListener(new _$$Lambda$OplusResolverGridAdapter$HcSuZuuryK0ZwtWVxohbEHXtjj0(this, paramInt));
    paramGridViewHolder.mIcon.setOnLongClickListener(new _$$Lambda$OplusResolverGridAdapter$gM_6Y5ZxFC_sCtzmIKApmBH4BGA(this, paramInt));
  }
  
  public int getItemCount() {
    int i;
    List<OplusItem> list = this.oplusItems;
    if (list == null) {
      i = 0;
    } else {
      i = this.mMoreIconIndex;
      if (i != -1) {
        i++;
      } else {
        i = list.size();
      } 
    } 
    return i;
  }
  
  public void setOplusItems(List<OplusItem> paramList) {
    this.oplusItems = paramList;
  }
  
  public void setPagerNumber(int paramInt) {
    this.mPagerNumber = paramInt;
  }
  
  public void startMoreAnimation(int paramInt) {
    this.mMoreIconIndex = paramInt;
    (new Handler(Looper.getMainLooper())).post(new _$$Lambda$OplusResolverGridAdapter$p3IO74m7Yd5diSLsDcveWRxqtyM(this));
  }
  
  static class GridViewHolder extends RecyclerView.ViewHolder {
    ImageView mIcon;
    
    TextView mLabel;
    
    TextView mName;
    
    public GridViewHolder(View param1View) {
      super(param1View);
      this.mIcon = (ImageView)param1View.findViewById(201457835);
      this.mLabel = (TextView)param1View.findViewById(16908308);
      this.mName = (TextView)param1View.findViewById(201457836);
    }
  }
}
