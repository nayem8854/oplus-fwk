package com.oplus.font;

import android.common.IOplusCommonFeature;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.widget.TextView;

public interface IOplusBaseFontManager extends IOplusCommonFeature {
  default void createFontLink(String paramString) {}
  
  default void deleteFontLink(String paramString) {}
  
  default void handleFactoryReset() {}
  
  default Typeface flipTypeface(Typeface paramTypeface) {
    return paramTypeface;
  }
  
  default String getSystemFontConfig() {
    return "/system_ext/etc/fonts_base.xml";
  }
  
  default boolean isFlipFontUsed() {
    return false;
  }
  
  default void setCurrentAppName(String paramString) {}
  
  default void setFlipFont(Configuration paramConfiguration, int paramInt) {}
  
  default void setFlipFontWhenUserChange(Configuration paramConfiguration, int paramInt) {}
  
  default void replaceFakeBoldToMedium(TextView paramTextView, Typeface paramTypeface, int paramInt) {}
  
  default void updateTypefaceInCurrProcess(Configuration paramConfiguration) {}
  
  default void onCleanupUserForFont(int paramInt) {}
  
  default Typeface replaceTypefaceWithVariation(Typeface paramTypeface, Paint paramPaint) {
    return paramTypeface;
  }
  
  default void updateFontVariationConfiguration(Configuration paramConfiguration, int paramInt) {}
  
  default void initVariationFontVariable(Context paramContext) {}
  
  default void updateLanguageLocale(Configuration paramConfiguration) {}
  
  default void updateConfigurationInUIMode(Context paramContext, Configuration paramConfiguration, int paramInt) {}
  
  default int getTypefaceIndex(int paramInt1, int paramInt2) {
    return paramInt1;
  }
}
