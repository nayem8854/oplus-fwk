package com.oplus.font;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.FileUtils;
import android.os.SystemProperties;
import android.util.Log;
import android.util.Slog;
import android.widget.TextView;
import com.oplus.util.OplusFontUtils;
import java.io.File;

public class OplusFontManager implements IOplusFontManager {
  public static boolean sDebugfDetail = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private boolean mDynamicDebug = false;
  
  private boolean DEBUG_SWITCH = false | sDebugfDetail;
  
  private static volatile OplusFontManager sInstance = null;
  
  private static final String TAG = "OplusFontManager";
  
  public static OplusFontManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/font/OplusFontManager.sInstance : Lcom/oplus/font/OplusFontManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/font/OplusFontManager
    //   8: monitorenter
    //   9: getstatic com/oplus/font/OplusFontManager.sInstance : Lcom/oplus/font/OplusFontManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/font/OplusFontManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/font/OplusFontManager.sInstance : Lcom/oplus/font/OplusFontManager;
    //   27: ldc com/oplus/font/OplusFontManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/font/OplusFontManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/font/OplusFontManager.sInstance : Lcom/oplus/font/OplusFontManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #58	-> 0
    //   #59	-> 6
    //   #60	-> 9
    //   #61	-> 15
    //   #63	-> 27
    //   #65	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public void createFontLink(String paramString) {
    printLog("createFontLink");
    OplusFontUtils.createFontLink(paramString);
  }
  
  public void deleteFontLink(String paramString) {
    printLog("deleteFontLink");
    OplusFontUtils.deleteFontLink(paramString);
  }
  
  public void handleFactoryReset() {
    printLog("handleFactoryReset");
    OplusFontUtils.handleFactoryReset();
  }
  
  public Typeface flipTypeface(Typeface paramTypeface) {
    return OplusFontUtils.flipTypeface(paramTypeface);
  }
  
  public boolean isFlipFontUsed() {
    return OplusFontUtils.isFlipFontUsed;
  }
  
  public void setCurrentAppName(String paramString) {
    OplusFontUtils.setAppTypeFace(paramString);
  }
  
  public void setFlipFont(Configuration paramConfiguration, int paramInt) {
    if ((0x2000000 & paramInt) != 0)
      OplusFontUtils.setFlipFont(paramConfiguration); 
  }
  
  public void setFlipFontWhenUserChange(Configuration paramConfiguration, int paramInt) {
    OplusFontUtils.setFlipFontWhenUserChange(paramConfiguration);
  }
  
  public void replaceFakeBoldToMedium(TextView paramTextView, Typeface paramTypeface, int paramInt) {
    OplusFontUtils.replaceFakeBoldToColorMedium(paramTextView, paramTypeface, paramInt);
  }
  
  public void updateTypefaceInCurrProcess(Configuration paramConfiguration) {
    OplusFontUtils.updateTypefaceInCurrProcess(paramConfiguration);
  }
  
  private void printLog(String paramString) {
    if (this.DEBUG_SWITCH) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("[impl] ");
      stringBuilder.append(paramString);
      Slog.d("OplusFontManager", stringBuilder.toString());
    } 
  }
  
  public void onCleanupUserForFont(int paramInt) {
    if (paramInt != 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(OplusFontUtils.DATA_FONT_DIRECTORY);
      stringBuilder.append(paramInt);
      File file = new File(stringBuilder.toString());
      if (file.exists()) {
        boolean bool = FileUtils.deleteContentsAndDir(file);
        if (this.DEBUG_SWITCH) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("onCleanupUserForFont result :");
          stringBuilder1.append(bool);
          Log.v("OplusFontManager", stringBuilder1.toString());
        } 
      } 
    } 
  }
  
  public Typeface replaceTypefaceWithVariation(Typeface paramTypeface, Paint paramPaint) {
    return OplusFontUtils.replaceTypefaceWithVariation(paramTypeface, paramPaint);
  }
  
  public void updateFontVariationConfiguration(Configuration paramConfiguration, int paramInt) {
    OplusFontUtils.updateFontVariationConfiguration(paramConfiguration, paramInt);
  }
  
  public void initVariationFontVariable(Context paramContext) {
    OplusFontUtils.initVariationFontVariable(paramContext);
  }
  
  public void updateLanguageLocale(Configuration paramConfiguration) {
    OplusFontUtils.updateLanguageLocale(paramConfiguration);
  }
  
  public void updateConfigurationInUIMode(Context paramContext, Configuration paramConfiguration, int paramInt) {
    OplusFontUtils.updateConfigurationInUIMode(paramContext, paramConfiguration, paramInt);
  }
  
  public int getTypefaceIndex(int paramInt1, int paramInt2) {
    return paramInt2;
  }
}
