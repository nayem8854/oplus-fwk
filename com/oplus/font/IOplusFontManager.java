package com.oplus.font;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;

public interface IOplusFontManager extends IOplusBaseFontManager {
  public static final IOplusFontManager DEFAULT = (IOplusFontManager)new Object();
  
  public static final String NAME = "OplusFontManager";
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusFontManager;
  }
  
  default IOplusFontManager getDefault() {
    return DEFAULT;
  }
}
