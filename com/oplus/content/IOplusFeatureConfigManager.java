package com.oplus.content;

import android.os.RemoteException;
import java.util.List;

public interface IOplusFeatureConfigManager {
  public static final String DESCRIPTOR = "android.app.IPackageManager";
  
  public static final int DISABLE_FEATURE_CONFIG = 20004;
  
  public static final int ENABLE_FEATURE_CONFIG = 20003;
  
  public static final int HAS_FEATURE_CONFIG = 20002;
  
  public static final int NOTIFY_FEATURE_CONFIG_UPDATE = 20005;
  
  public static final int REGISTER_FEATURE_OBSERVER = 20006;
  
  public static final int UNREGISTER_FEATURE_OBSERVER = 20007;
  
  default boolean hasFeature(String paramString) throws RemoteException {
    return false;
  }
  
  default boolean enableFeature(String paramString) throws RemoteException {
    return false;
  }
  
  default boolean disableFeature(String paramString) throws RemoteException {
    return false;
  }
  
  default void notifyFeaturesUpdate(String paramString1, String paramString2) throws RemoteException {}
  
  default boolean registerFeatureObserver(List<String> paramList, IOplusFeatureObserver paramIOplusFeatureObserver) {
    return false;
  }
  
  default boolean unregisterFeatureObserver(IOplusFeatureObserver paramIOplusFeatureObserver) {
    return false;
  }
}
