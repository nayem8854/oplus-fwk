package com.oplus.content;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemProperties;
import android.util.PrintWriterPrinter;
import android.util.Printer;
import android.util.Slog;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public final class OplusRuleInfo implements Parcelable {
  private static final String BLACK_STR = "black";
  
  private static final int CHOICE_BLACK = 1;
  
  private static final int CHOICE_INVALID = -1;
  
  private static final int CHOICE_WHITE = 0;
  
  public static final Parcelable.Creator<OplusRuleInfo> CREATOR;
  
  private static boolean DEBUG = false;
  
  private static boolean DEBUG_PANIC = false;
  
  private static final String INVALID_STR = "invalid";
  
  private static final String NAME_STR = "name";
  
  private static final String TAG = "CII_ColorRuleInfo";
  
  private static final String TAG_INTENT_FILETER = "intent-filter";
  
  private static final String TAG_SOURCE_PKG_BLACK = "source-pkg-black";
  
  private static final String TAG_SOURCE_PKG_CHOICE = "source-pkg-choice";
  
  private static final String TAG_SOURCE_PKG_WHITE = "source-pkg-white";
  
  private static final String TAG_TARGET_CPN = "target-cpn";
  
  private static final String WHITE_STR = "white";
  
  public IntentFilter mIntentFilter;
  
  public List<String> mSourcePkgBlackList;
  
  public int mSourcePkgChoice = -1;
  
  public List<String> mSourcePkgWhiteList;
  
  public List<String> mTargetComponentList;
  
  static {
    boolean bool = SystemProperties.getBoolean("persist.sys.assert.panic", false);
    DEBUG = bool;
    CREATOR = new Parcelable.Creator<OplusRuleInfo>() {
        public OplusRuleInfo createFromParcel(Parcel param1Parcel) {
          return new OplusRuleInfo(param1Parcel);
        }
        
        public OplusRuleInfo[] newArray(int param1Int) {
          return new OplusRuleInfo[param1Int];
        }
      };
  }
  
  public static void setDebugEnable(boolean paramBoolean) {
    DEBUG = paramBoolean;
  }
  
  private static String getStringByChoice(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1)
        return "invalid"; 
      return "black";
    } 
    return "white";
  }
  
  private static int getIntByChoice(String paramString) {
    if ("white".equals(paramString))
      return 0; 
    if ("black".equals(paramString))
      return 1; 
    if ("invalid".equals(paramString))
      return -1; 
    return -1;
  }
  
  public OplusRuleInfo() {
    this.mTargetComponentList = new ArrayList<>();
    this.mSourcePkgWhiteList = new ArrayList<>();
    this.mSourcePkgBlackList = new ArrayList<>();
  }
  
  public OplusRuleInfo(OplusRuleInfo paramOplusRuleInfo) {
    if (paramOplusRuleInfo != null) {
      this.mTargetComponentList = new ArrayList<>(paramOplusRuleInfo.mTargetComponentList);
      this.mSourcePkgChoice = paramOplusRuleInfo.mSourcePkgChoice;
      this.mSourcePkgWhiteList = new ArrayList<>(paramOplusRuleInfo.mSourcePkgWhiteList);
      this.mSourcePkgBlackList = new ArrayList<>(paramOplusRuleInfo.mSourcePkgBlackList);
      this.mIntentFilter = new IntentFilter(paramOplusRuleInfo.mIntentFilter);
    } 
  }
  
  public OplusRuleInfo(List<String> paramList1, int paramInt, List<String> paramList2, List<String> paramList3, IntentFilter paramIntentFilter) {
    this.mTargetComponentList = paramList1;
    this.mSourcePkgChoice = paramInt;
    this.mSourcePkgWhiteList = paramList2;
    this.mSourcePkgBlackList = paramList3;
    this.mIntentFilter = paramIntentFilter;
  }
  
  public boolean needIntercept(String paramString1, String paramString2, Intent paramIntent) {
    boolean bool;
    if (matchTargetCpn(paramString1) && matchSourcePkg(paramString2) && matchIntent(paramIntent)) {
      bool = true;
    } else {
      bool = false;
    } 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("needIntercept intercept = ");
      stringBuilder.append(bool);
      Slog.i("CII_ColorRuleInfo", stringBuilder.toString());
    } 
    return bool;
  }
  
  private boolean matchTargetCpn(String paramString) {
    boolean bool1 = false;
    List<String> list = this.mTargetComponentList;
    boolean bool2 = bool1;
    if (list != null) {
      bool2 = bool1;
      if (list.contains(paramString))
        bool2 = true; 
    } 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("matchTargetCpn cpn = ");
      stringBuilder.append(paramString);
      stringBuilder.append("match = ");
      stringBuilder.append(bool2);
      Slog.i("CII_ColorRuleInfo", stringBuilder.toString());
    } 
    return bool2;
  }
  
  private boolean matchSourcePkg(String paramString) {
    boolean bool2, bool1 = false;
    int i = this.mSourcePkgChoice;
    if (i == 0) {
      List<String> list = this.mSourcePkgWhiteList;
      bool2 = bool1;
      if (list != null) {
        bool2 = bool1;
        if (list.contains(paramString))
          bool2 = true; 
      } 
    } else if (i == 1) {
      List<String> list = this.mSourcePkgBlackList;
      bool2 = bool1;
      if (list != null) {
        bool2 = bool1;
        if (!list.contains(paramString))
          bool2 = true; 
      } 
    } else {
      bool2 = false;
    } 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("matchSourcePkg callingPkg = ");
      stringBuilder.append(paramString);
      stringBuilder.append(" choice = ");
      i = this.mSourcePkgChoice;
      stringBuilder.append(getStringByChoice(i));
      stringBuilder.append(" match = ");
      stringBuilder.append(bool2);
      paramString = stringBuilder.toString();
      Slog.i("CII_ColorRuleInfo", paramString);
    } 
    return bool2;
  }
  
  private boolean matchIntent(Intent paramIntent) {
    boolean bool1 = true, bool2 = true;
    boolean bool3 = true;
    IntentFilter intentFilter = this.mIntentFilter;
    boolean bool4 = bool1, bool5 = bool3;
    if (intentFilter != null) {
      bool4 = bool1;
      bool5 = bool3;
      if (paramIntent != null) {
        if (intentFilter.countActions() > 0)
          bool2 = this.mIntentFilter.matchAction(paramIntent.getAction()); 
        bool4 = bool2;
        bool5 = bool3;
        if (this.mIntentFilter.countDataSchemes() > 0)
          if (paramIntent.getData() != null) {
            bool5 = this.mIntentFilter.hasDataScheme(paramIntent.getData().getScheme());
            bool4 = bool2;
          } else {
            bool5 = false;
            bool4 = bool2;
          }  
      } 
    } 
    if (bool4 && bool5) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("matchIntent matchAction = ");
      stringBuilder.append(bool4);
      stringBuilder.append(" matchScheme = ");
      stringBuilder.append(bool5);
      Slog.i("CII_ColorRuleInfo", stringBuilder.toString());
    } 
    return bool2;
  }
  
  public OplusRuleInfo(Parcel paramParcel) {
    int i = paramParcel.readInt();
    this.mTargetComponentList = new ArrayList<>();
    if (i > 0)
      for (byte b = 0; b < i; b++)
        this.mTargetComponentList.add(paramParcel.readString());  
    this.mSourcePkgChoice = paramParcel.readInt();
    i = paramParcel.readInt();
    this.mSourcePkgWhiteList = new ArrayList<>();
    if (i > 0)
      for (byte b = 0; b < i; b++)
        this.mSourcePkgWhiteList.add(paramParcel.readString());  
    i = paramParcel.readInt();
    this.mSourcePkgBlackList = new ArrayList<>();
    if (i > 0)
      for (byte b = 0; b < i; b++)
        this.mSourcePkgBlackList.add(paramParcel.readString());  
    if (paramParcel.readInt() != 0)
      this.mIntentFilter = (IntentFilter)IntentFilter.CREATOR.createFromParcel(paramParcel); 
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    List<String> list = this.mTargetComponentList;
    if (list != null) {
      int i = list.size();
      paramParcel.writeInt(i);
      for (byte b = 0; b < i; b++)
        paramParcel.writeString(this.mTargetComponentList.get(b)); 
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeInt(this.mSourcePkgChoice);
    list = this.mSourcePkgWhiteList;
    if (list != null) {
      int i = list.size();
      paramParcel.writeInt(i);
      for (byte b = 0; b < i; b++)
        paramParcel.writeString(this.mSourcePkgWhiteList.get(b)); 
    } else {
      paramParcel.writeInt(0);
    } 
    list = this.mSourcePkgBlackList;
    if (list != null) {
      int i = list.size();
      paramParcel.writeInt(i);
      for (byte b = 0; b < i; b++)
        paramParcel.writeString(this.mSourcePkgBlackList.get(b)); 
    } else {
      paramParcel.writeInt(0);
    } 
    if (this.mIntentFilter != null) {
      paramParcel.writeInt(1);
      this.mIntentFilter.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeInt(0);
    } 
  }
  
  public void writeToXml(XmlSerializer paramXmlSerializer) throws IOException {
    List<String> list = this.mTargetComponentList;
    if (list != null && list.size() > 0)
      for (String str : this.mTargetComponentList) {
        paramXmlSerializer.startTag(null, "target-cpn");
        paramXmlSerializer.attribute(null, "name", str);
        paramXmlSerializer.endTag(null, "target-cpn");
      }  
    paramXmlSerializer.startTag(null, "source-pkg-choice");
    paramXmlSerializer.attribute(null, "name", getStringByChoice(this.mSourcePkgChoice));
    paramXmlSerializer.endTag(null, "source-pkg-choice");
    list = this.mSourcePkgWhiteList;
    if (list != null && list.size() > 0)
      for (String str : this.mSourcePkgWhiteList) {
        paramXmlSerializer.startTag(null, "source-pkg-white");
        paramXmlSerializer.attribute(null, "name", str);
        paramXmlSerializer.endTag(null, "source-pkg-white");
      }  
    list = this.mSourcePkgBlackList;
    if (list != null && list.size() > 0)
      for (String str : this.mSourcePkgBlackList) {
        paramXmlSerializer.startTag(null, "source-pkg-black");
        paramXmlSerializer.attribute(null, "name", str);
        paramXmlSerializer.endTag(null, "source-pkg-black");
      }  
    if (this.mIntentFilter != null) {
      paramXmlSerializer.startTag(null, "intent-filter");
      this.mIntentFilter.writeToXml(paramXmlSerializer);
      paramXmlSerializer.endTag(null, "intent-filter");
    } 
  }
  
  public void readFromXml(XmlPullParser paramXmlPullParser) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: aload_1
    //   1: invokeinterface getDepth : ()I
    //   6: istore_2
    //   7: aload_1
    //   8: invokeinterface next : ()I
    //   13: istore_3
    //   14: iload_3
    //   15: iconst_1
    //   16: if_icmpeq -> 296
    //   19: iload_3
    //   20: iconst_3
    //   21: if_icmpne -> 34
    //   24: aload_1
    //   25: invokeinterface getDepth : ()I
    //   30: iload_2
    //   31: if_icmple -> 296
    //   34: iload_3
    //   35: iconst_3
    //   36: if_icmpeq -> 7
    //   39: iload_3
    //   40: iconst_4
    //   41: if_icmpne -> 47
    //   44: goto -> 7
    //   47: aload_1
    //   48: invokeinterface getName : ()Ljava/lang/String;
    //   53: astore #4
    //   55: getstatic com/oplus/content/OplusRuleInfo.DEBUG : Z
    //   58: ifeq -> 98
    //   61: new java/lang/StringBuilder
    //   64: dup
    //   65: invokespecial <init> : ()V
    //   68: astore #5
    //   70: aload #5
    //   72: ldc_w 'readFromXml tagName = '
    //   75: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   78: pop
    //   79: aload #5
    //   81: aload #4
    //   83: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   86: pop
    //   87: ldc 'CII_ColorRuleInfo'
    //   89: aload #5
    //   91: invokevirtual toString : ()Ljava/lang/String;
    //   94: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   97: pop
    //   98: aload #4
    //   100: ldc 'target-cpn'
    //   102: invokevirtual equals : (Ljava/lang/Object;)Z
    //   105: ifeq -> 128
    //   108: aload_1
    //   109: aconst_null
    //   110: ldc 'name'
    //   112: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   117: astore #4
    //   119: aload_0
    //   120: aload #4
    //   122: invokespecial addTargetCpn : (Ljava/lang/String;)V
    //   125: goto -> 293
    //   128: aload #4
    //   130: ldc 'source-pkg-choice'
    //   132: invokevirtual equals : (Ljava/lang/Object;)Z
    //   135: ifeq -> 158
    //   138: aload_1
    //   139: aconst_null
    //   140: ldc 'name'
    //   142: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   147: astore #4
    //   149: aload_0
    //   150: aload #4
    //   152: invokespecial setSourcePkgChoice : (Ljava/lang/String;)V
    //   155: goto -> 293
    //   158: aload #4
    //   160: ldc 'source-pkg-white'
    //   162: invokevirtual equals : (Ljava/lang/Object;)Z
    //   165: ifeq -> 188
    //   168: aload_1
    //   169: aconst_null
    //   170: ldc 'name'
    //   172: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   177: astore #4
    //   179: aload_0
    //   180: aload #4
    //   182: invokespecial addSourcePkgWhite : (Ljava/lang/String;)V
    //   185: goto -> 293
    //   188: aload #4
    //   190: ldc 'source-pkg-black'
    //   192: invokevirtual equals : (Ljava/lang/Object;)Z
    //   195: ifeq -> 218
    //   198: aload_1
    //   199: aconst_null
    //   200: ldc 'name'
    //   202: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   207: astore #4
    //   209: aload_0
    //   210: aload #4
    //   212: invokespecial addSourcePkgBlack : (Ljava/lang/String;)V
    //   215: goto -> 293
    //   218: aload #4
    //   220: ldc 'intent-filter'
    //   222: invokevirtual equals : (Ljava/lang/Object;)Z
    //   225: ifeq -> 252
    //   228: new android/content/IntentFilter
    //   231: dup
    //   232: invokespecial <init> : ()V
    //   235: astore #4
    //   237: aload #4
    //   239: aload_1
    //   240: invokevirtual readFromXml : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   243: aload_0
    //   244: aload #4
    //   246: invokespecial setIntentFilter : (Landroid/content/IntentFilter;)V
    //   249: goto -> 293
    //   252: new java/lang/StringBuilder
    //   255: dup
    //   256: invokespecial <init> : ()V
    //   259: astore #5
    //   261: aload #5
    //   263: ldc_w 'Unknown tag parsing ColorRuleInfo : '
    //   266: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   269: pop
    //   270: aload #5
    //   272: aload #4
    //   274: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   277: pop
    //   278: ldc 'CII_ColorRuleInfo'
    //   280: aload #5
    //   282: invokevirtual toString : ()Ljava/lang/String;
    //   285: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   288: pop
    //   289: aload_1
    //   290: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   293: goto -> 7
    //   296: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #319	-> 0
    //   #320	-> 7
    //   #321	-> 7
    //   #323	-> 24
    //   #324	-> 34
    //   #326	-> 44
    //   #328	-> 47
    //   #329	-> 55
    //   #330	-> 61
    //   #332	-> 98
    //   #333	-> 108
    //   #334	-> 119
    //   #335	-> 125
    //   #336	-> 138
    //   #337	-> 149
    //   #338	-> 155
    //   #339	-> 168
    //   #340	-> 179
    //   #341	-> 185
    //   #342	-> 198
    //   #343	-> 209
    //   #344	-> 215
    //   #345	-> 228
    //   #346	-> 237
    //   #347	-> 243
    //   #348	-> 249
    //   #349	-> 252
    //   #350	-> 289
    //   #352	-> 293
    //   #353	-> 296
  }
  
  private void addTargetCpn(String paramString) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("addTargetCpn cpn = ");
      stringBuilder.append(paramString);
      Slog.i("CII_ColorRuleInfo", stringBuilder.toString());
    } 
    if (this.mTargetComponentList == null)
      this.mTargetComponentList = new ArrayList<>(); 
    if (!this.mTargetComponentList.contains(paramString))
      this.mTargetComponentList.add(paramString); 
  }
  
  private void setSourcePkgChoice(String paramString) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setSourcePkgChoice choice = ");
      stringBuilder.append(paramString);
      Slog.i("CII_ColorRuleInfo", stringBuilder.toString());
    } 
    this.mSourcePkgChoice = getIntByChoice(paramString);
  }
  
  private void addSourcePkgWhite(String paramString) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("addSourcePkgWhite name = ");
      stringBuilder.append(paramString);
      Slog.i("CII_ColorRuleInfo", stringBuilder.toString());
    } 
    if (this.mSourcePkgWhiteList == null)
      this.mSourcePkgWhiteList = new ArrayList<>(); 
    if (!this.mSourcePkgWhiteList.contains(paramString))
      this.mSourcePkgWhiteList.add(paramString); 
  }
  
  private void addSourcePkgBlack(String paramString) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("addSourcePkgBlack name = ");
      stringBuilder.append(paramString);
      Slog.i("CII_ColorRuleInfo", stringBuilder.toString());
    } 
    if (this.mSourcePkgBlackList == null)
      this.mSourcePkgBlackList = new ArrayList<>(); 
    if (!this.mSourcePkgBlackList.contains(paramString))
      this.mSourcePkgBlackList.add(paramString); 
  }
  
  private void setIntentFilter(IntentFilter paramIntentFilter) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setIntentFilter filter = ");
      stringBuilder.append(paramIntentFilter);
      Slog.i("CII_ColorRuleInfo", stringBuilder.toString());
    } 
    this.mIntentFilter = paramIntentFilter;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(128);
    toShortString(stringBuilder);
    return stringBuilder.toString();
  }
  
  private void toShortString(StringBuilder paramStringBuilder) {
    if (this.mTargetComponentList != null) {
      paramStringBuilder.append(" targetcpn = [");
      for (String str : this.mTargetComponentList) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(" ");
        stringBuilder1.append(str);
        stringBuilder1.append(" ");
        paramStringBuilder.append(stringBuilder1.toString());
      } 
      paramStringBuilder.append("] ");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(" choice = ");
    stringBuilder.append(getStringByChoice(this.mSourcePkgChoice));
    paramStringBuilder.append(stringBuilder.toString());
    if (this.mSourcePkgWhiteList != null) {
      paramStringBuilder.append(" sourcewhite = [");
      for (String str : this.mSourcePkgWhiteList) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(" ");
        stringBuilder1.append(str);
        stringBuilder1.append(" ");
        paramStringBuilder.append(stringBuilder1.toString());
      } 
      paramStringBuilder.append("] ");
    } 
    if (this.mSourcePkgBlackList != null) {
      paramStringBuilder.append(" sourceblack = [");
      for (String str : this.mSourcePkgBlackList) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(" ");
        stringBuilder1.append(str);
        stringBuilder1.append(" ");
        paramStringBuilder.append(stringBuilder1.toString());
      } 
      paramStringBuilder.append("] ");
    } 
    IntentFilter intentFilter = this.mIntentFilter;
    if (intentFilter != null)
      paramStringBuilder.append(intentFilter.toString()); 
  }
  
  public void dump(PrintWriter paramPrintWriter, String paramString) {
    paramPrintWriter.print(paramString);
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("DEBUG = ");
    stringBuilder2.append(DEBUG);
    paramPrintWriter.println(stringBuilder2.toString());
    List<String> list2 = this.mTargetComponentList;
    if (list2 != null)
      for (String str : list2) {
        paramPrintWriter.print(paramString);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("targetCpn = ");
        stringBuilder.append(str);
        paramPrintWriter.println(stringBuilder.toString());
      }  
    paramPrintWriter.print(paramString);
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("Choice = ");
    stringBuilder1.append(getStringByChoice(this.mSourcePkgChoice));
    paramPrintWriter.println(stringBuilder1.toString());
    List<String> list1 = this.mSourcePkgWhiteList;
    if (list1 != null)
      for (String str : list1) {
        paramPrintWriter.print(paramString);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("sourcePkgWhite = ");
        stringBuilder.append(str);
        paramPrintWriter.println(stringBuilder.toString());
      }  
    list1 = this.mSourcePkgBlackList;
    if (list1 != null)
      for (String str : list1) {
        paramPrintWriter.print(paramString);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("sourcePkgBlack = ");
        stringBuilder.append(str);
        paramPrintWriter.println(stringBuilder.toString());
      }  
    paramPrintWriter.print(paramString);
    paramPrintWriter.println("Intent-Filter:");
    IntentFilter intentFilter = this.mIntentFilter;
    if (intentFilter != null) {
      PrintWriterPrinter printWriterPrinter = new PrintWriterPrinter(paramPrintWriter);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append("  ");
      intentFilter.dump((Printer)printWriterPrinter, stringBuilder.toString());
    } 
  }
}
