package com.oplus.content;

public class OplusFeatureConfigInfo {
  public String name;
  
  public int priority;
  
  public OplusFeatureConfigInfo() {}
  
  public OplusFeatureConfigInfo(String paramString, int paramInt) {
    this.name = paramString;
    this.priority = paramInt;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("OplusFeatureConfigInfo{name='");
    stringBuilder.append(this.name);
    stringBuilder.append('\'');
    stringBuilder.append(", priority=");
    stringBuilder.append(this.priority);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
}
