package com.oplus.content;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IOplusFeatureObserver extends IInterface {
  void onFeatureUpdate(List<String> paramList) throws RemoteException;
  
  class Default implements IOplusFeatureObserver {
    public void onFeatureUpdate(List<String> param1List) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusFeatureObserver {
    private static final String DESCRIPTOR = "com.oplus.content.IOplusFeatureObserver";
    
    static final int TRANSACTION_onFeatureUpdate = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.content.IOplusFeatureObserver");
    }
    
    public static IOplusFeatureObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.content.IOplusFeatureObserver");
      if (iInterface != null && iInterface instanceof IOplusFeatureObserver)
        return (IOplusFeatureObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onFeatureUpdate";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.content.IOplusFeatureObserver");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.content.IOplusFeatureObserver");
      ArrayList<String> arrayList = param1Parcel1.createStringArrayList();
      onFeatureUpdate(arrayList);
      return true;
    }
    
    private static class Proxy implements IOplusFeatureObserver {
      public static IOplusFeatureObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.content.IOplusFeatureObserver";
      }
      
      public void onFeatureUpdate(List<String> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.content.IOplusFeatureObserver");
          parcel.writeStringList(param2List);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusFeatureObserver.Stub.getDefaultImpl() != null) {
            IOplusFeatureObserver.Stub.getDefaultImpl().onFeatureUpdate(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusFeatureObserver param1IOplusFeatureObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusFeatureObserver != null) {
          Proxy.sDefaultImpl = param1IOplusFeatureObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusFeatureObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
