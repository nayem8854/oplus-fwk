package com.oplus.content;

public abstract class OplusContext {
  public static final String ACCESS_CONTROL_SERVICE = "color_accesscontrol";
  
  public static final String SCREENSHOT_SERVICE = "color_screenshot";
}
