package com.oplus.content;

import android.app.PropertyInvalidatedCache;
import android.os.SystemProperties;
import android.util.Log;
import com.oplus.annotation.OplusFeature;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Objects;

public class OplusFeatureCache {
  public static final int ALL_TYPE = 6;
  
  private static final ArrayList<String> MEMORYFEATURES;
  
  private static final ArrayList<String> MEMORYNATIVEFEATURES;
  
  private static final ArrayList<String> PERSISTFEATURES;
  
  private static final ArrayList<String> PERSISTNATIVEFEATURES;
  
  private static final ArrayList<String> READONLYFEATURES = new ArrayList<>();
  
  private static final ArrayList<String> READONLYNATIVEFEATURES;
  
  private static final String TAG = "OplusFeatureCache";
  
  private static final PropertyInvalidatedCache<HasOplusFeatureQuery, Boolean> sMemoryFeatureCache;
  
  private static final PropertyInvalidatedCache<HasOplusFeatureQuery, Boolean> sPersistFeatureCache;
  
  private static final PropertyInvalidatedCache<HasOplusFeatureQuery, Boolean> sReadonlyFeatureCache;
  
  static {
    MEMORYFEATURES = new ArrayList<>();
    PERSISTFEATURES = new ArrayList<>();
    READONLYNATIVEFEATURES = new ArrayList<>();
    MEMORYNATIVEFEATURES = new ArrayList<>();
    PERSISTNATIVEFEATURES = new ArrayList<>();
    sReadonlyFeatureCache = (PropertyInvalidatedCache<HasOplusFeatureQuery, Boolean>)new Object(256, "sys.cache.has_feature_readonly");
    sPersistFeatureCache = (PropertyInvalidatedCache<HasOplusFeatureQuery, Boolean>)new Object(256, "sys.cache.has_feature_persist");
    sMemoryFeatureCache = (PropertyInvalidatedCache<HasOplusFeatureQuery, Boolean>)new Object(256, "sys.cache.has_feature_memory");
  }
  
  public OplusFeatureCache() {
    init();
  }
  
  private void init() {
    StringBuilder stringBuilder;
    long l2, l1 = System.currentTimeMillis();
    try {
      Field[] arrayOfField = IOplusFeatureConfigList.class.getDeclaredFields();
      int i = arrayOfField.length;
      byte b = 0;
      while (true) {
        if (b < i) {
          Field field = arrayOfField[b];
          boolean bool = field.isAnnotationPresent((Class)OplusFeature.class);
          if (bool) {
            String str = (String)field.get((Object)null);
            OplusFeature oplusFeature = field.<OplusFeature>getDeclaredAnnotation(OplusFeature.class);
            if (oplusFeature != null) {
              StringBuilder stringBuilder1;
              switch (oplusFeature.value()) {
                default:
                  stringBuilder1 = new StringBuilder();
                  this();
                  stringBuilder1.append("Unknow type = ");
                  stringBuilder1.append(oplusFeature.value());
                  Log.i("OplusFeatureCache", stringBuilder1.toString());
                  b++;
                  continue;
                case PERSIST_NATIVE_FEATURE:
                  PERSISTNATIVEFEATURES.add(stringBuilder1);
                  break;
                case MEMORY_NATIVE_FEATURE:
                  MEMORYNATIVEFEATURES.add(stringBuilder1);
                  break;
                case READONLY_NATIVE_FEATURE:
                  READONLYNATIVEFEATURES.add(stringBuilder1);
                  break;
                case PERSIST_FEATURE:
                  PERSISTFEATURES.add(stringBuilder1);
                  break;
                case MEMORY_FEATURE:
                  MEMORYFEATURES.add(stringBuilder1);
                  break;
                case READONLY_FEATURE:
                  READONLYFEATURES.add(stringBuilder1);
                  break;
              } 
            } 
          } 
        } else {
          break;
        } 
        b++;
      } 
      l2 = System.currentTimeMillis();
      stringBuilder = new StringBuilder();
    } catch (IllegalAccessException illegalAccessException) {
      illegalAccessException.printStackTrace();
      l2 = System.currentTimeMillis();
      stringBuilder = new StringBuilder();
    } finally {
      Exception exception;
    } 
    stringBuilder.append("Milliseconds spent on init(): ");
    stringBuilder.append(l2 - l1);
    Log.i("OplusFeatureCache", stringBuilder.toString());
  }
  
  private static final class HasOplusFeatureQuery {
    public final String name;
    
    public final int type;
    
    public HasOplusFeatureQuery(String param1String, int param1Int) {
      this.name = param1String;
      this.type = param1Int;
    }
    
    public String toString() {
      String str = this.name;
      int i = this.type;
      return String.format("HasOplusFeatureQuery(name=\"%s\", type=%d)", new Object[] { str, Integer.valueOf(i) });
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof HasOplusFeatureQuery;
      boolean bool1 = false;
      if (bool) {
        param1Object = param1Object;
        bool = bool1;
        if (Objects.equals(this.name, ((HasOplusFeatureQuery)param1Object).name)) {
          bool = bool1;
          if (this.type == ((HasOplusFeatureQuery)param1Object).type)
            bool = true; 
        } 
        return bool;
      } 
      return false;
    }
    
    public int hashCode() {
      return Objects.hashCode(this.name);
    }
  }
  
  public boolean query(String paramString) {
    if (MEMORYFEATURES.contains(paramString))
      return ((Boolean)sMemoryFeatureCache.query(new HasOplusFeatureQuery(paramString, OplusFeature.OplusFeatureType.MEMORY_FEATURE.ordinal()))).booleanValue(); 
    if (PERSISTFEATURES.contains(paramString))
      return ((Boolean)sPersistFeatureCache.query(new HasOplusFeatureQuery(paramString, OplusFeature.OplusFeatureType.PERSIST_FEATURE.ordinal()))).booleanValue(); 
    if (READONLYFEATURES.contains(paramString))
      return ((Boolean)sReadonlyFeatureCache.query(new HasOplusFeatureQuery(paramString, OplusFeature.OplusFeatureType.READONLY_FEATURE.ordinal()))).booleanValue(); 
    if (isNativeFeature(paramString))
      return SystemProperties.getBoolean(paramString, false); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("invalid oplus feature ");
    stringBuilder.append(paramString);
    Log.e("OplusFeatureCache", stringBuilder.toString());
    return false;
  }
  
  private boolean isNativeFeature(String paramString) {
    if (!MEMORYNATIVEFEATURES.contains(paramString)) {
      ArrayList<String> arrayList = READONLYNATIVEFEATURES;
      if (!arrayList.contains(paramString)) {
        arrayList = PERSISTNATIVEFEATURES;
        if (!arrayList.contains(paramString))
          return false; 
      } 
    } 
    return true;
  }
  
  public static void disableOplusFeatureCache(int paramInt) {
    if (paramInt == OplusFeature.OplusFeatureType.MEMORY_FEATURE.ordinal()) {
      sMemoryFeatureCache.disableLocal();
    } else if (paramInt == OplusFeature.OplusFeatureType.PERSIST_FEATURE.ordinal()) {
      sPersistFeatureCache.disableLocal();
    } else if (paramInt == OplusFeature.OplusFeatureType.READONLY_FEATURE.ordinal()) {
      sReadonlyFeatureCache.disableLocal();
    } else if (paramInt == 6) {
      sMemoryFeatureCache.disableLocal();
      sPersistFeatureCache.disableLocal();
      sReadonlyFeatureCache.disableLocal();
    } 
  }
  
  public static void invalidateOplusFeatureCache(int paramInt) {
    if (paramInt == OplusFeature.OplusFeatureType.MEMORY_FEATURE.ordinal()) {
      sMemoryFeatureCache.invalidateCache();
    } else if (paramInt == OplusFeature.OplusFeatureType.PERSIST_FEATURE.ordinal()) {
      sPersistFeatureCache.invalidateCache();
    } else if (paramInt == OplusFeature.OplusFeatureType.READONLY_FEATURE.ordinal()) {
      sReadonlyFeatureCache.invalidateCache();
    } else if (paramInt == 6) {
      sMemoryFeatureCache.invalidateCache();
      sPersistFeatureCache.invalidateCache();
      sReadonlyFeatureCache.invalidateCache();
    } 
  }
}
