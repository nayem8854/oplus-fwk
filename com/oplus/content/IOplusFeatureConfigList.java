package com.oplus.content;

import com.oplus.annotation.OplusFeature;
import com.oplus.annotation.OplusFeaturePermission;

public interface IOplusFeatureConfigList {
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String DISABLE_FEATURE_TORCH_HELPER = "oplus.software.powerkey_disbale_turnoff_torch";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_5G_ICON_JP_REQUIREMENT = "oplus.software.5g_icon_jp_requirement";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_5G_SUPPORT = "oplus.software.radio.support_5g";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_ACCELERATE_THREAD = "oplus.software.radio.accelerate";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_AIR_INTERFACE_DETECT_SUPPORT = "oplus.software.radio.air_interface_detect_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_ALIGN_ALARM_SUPPORT = "oplus.software.power.align_alarm";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_AOD_PREVENT_BURN_SUPPORT = "oplus.software.display.aod_prevent_burn_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_AOD_SUPPORT = "oplus.software.display.aod_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_AON_ANSWERPHONE_SUPPORT = "oplus.software.aon_phone_answerphone";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_AON_GESTUREUI_SUPPORT = "oplus.software.aon_gestureui_enable";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_AON_HARDWARE_SUPPORT = "oplus.hardware.aon_enable";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_AON_PHONE_CAMERA_GESTURE_RECOGNITION_SUPPORT = "oplus.software.aon_phone_camera_gesture_recognition";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_AON_PHONE_MUTE_SUPPORT = "oplus.software.aon_phone_mute";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_AON_PHONE_SUPPORT = "oplus.software.aon_phone_enable";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_AON_SOFTWARE_SUPPORT = "oplus.software.aon_enable";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_AON_SUB_CAM_SUPPORT = "oplus.software.aon_sub_cam_enable";
  
  @OplusFeature(OplusFeature.OplusFeatureType.PERSIST_FEATURE)
  public static final String FEATURE_APP_ACCESS_NFC = "oplus.software.nfc.app_access_nfc";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_APP_RESOLUTION_DEFAULT = "oplus.software.app_resolution_auto";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_APP_RESOLUTION_SWITCH = "oplus.software.app_resolution_switch";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_APTG_FORCE_ENABLE_ROAMING = "oplus.software.radio.aptg_force_enable_roaming";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_AUDIOEFFECT_SUPPORT = "oplus.software.audio.audioeffect_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_AUTOBRIGHTCTL_ANIMATION_SUPPORT = "oplus.software.display.autobrightctl_animation_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_AUTOLIGHT_PSENSOR_NOTSUPPORT = "oplus.software.display.autolight_psensor_notsupport";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_AUTOTIMEUPDATE_FORCE = "oplus.software.country.autotimeupdate_force";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_BACK_TOUCH_FINGERPRINT = "oplus.software.fingeprint_back_touch";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_BENCHMARK_SUPPORT = "oplus.software.benchmark.support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_BRIGHTNESS_MODE_AUTOMATIC = "oplus.software.display.brightness_mode_automatic";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_BROWSER_HARMONYNET = "oplus.software.browser.harmonynet";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_BT_ABSVOLUME_SUPPORT = "oplus.software.bt.absvolume_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_BT_BLE_SCAN_STRATEGYMODE = "oplus.software.bt.ble_scan_strategymode";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_BT_IGNORE_LOCATIONMODE = "oplus.software.bt.ignore_locationmode";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_BT_IOT_ENABLELOGGING = "oplus.software.bt.iot_enablelogging";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_BT_PROFILE_TRAKCER = "oplus.software.bt.profile_tracker";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_BT_RMTDEVICE_REPORT = "oplus.software.bt.rmtdevice_report";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_BT_VENDOR_AT_COMMAND = "oplus.software.bt.vendor_at_command";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_CALL_ASSISTANT_SUPPORT = "oplus.software.audio.call_assistant_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_CALL_TRANSLATOR_SUPPORT = "oplus.software.audio.call_translator_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_CAMERA_HEIC_SUPPORT = "oplus.software.camera.heic_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_CINEMA_MODE_SUPPORT = "oplus.software.display.cinema_mode_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_CLOSEEYE_FACE = "oplus.software.face_closeeye_detect";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_CMCC_DM = "oplus.software.radio.cmcc_dm";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_COLORMODE_OLED_SUPPORT = "oplus.software.display.colormode_oled_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_CONNECTIVITY_REGION_CN = "oplus.software.connectivity.region_CN";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_COOLEFECT_COOLEX_SUPPORT = "oplus.software.coolex.support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_CUSTOM_ECCLIST_WRITABLE = "oplus.software.radio.custom_ecclist_writable";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DCBACKLIGHT_SUPPORT = "oplus.software.display.dcbacklight_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DDS_SWITCH = "oplus.software.radio.dds_switch";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DEFAULTAPP_OPLUS_POLICY_ENABLED = "oplus.software.defaultapp.color_policy_enabled";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DEFAULTAPP_REMOVE_FORCE_LAUNCHER = "oplus.software.defaultapp.remove_force_launcher";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DEFAULTAPP_SET_CHROME_BROWSER = "oplus.software.defaultapp.set_chrome_browser";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DEVICEIDLE_NETWORK_OPT = "oplus.software.deviceidle_network_optimization";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DIABLE_DIRAC_FOR_ENGINEERING = "oplus.software.audio.disable_dirac_for_engineering";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DIABLE_DOLBY_FOR_ENGINEERING = "oplus.software.audio.disable_dolby_for_engineering";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DIALING_SHOW86_ENABLED = "oplus.software.radio.dialingshow86_enabled";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DIRAC_A2DP_SUPPORT = "oplus.software.audio.dirac.a2dp.support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DIRAC_SUPPORT = "oplus.software.audio.dirac_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DIRAC_V2_SUPPORT = "oplus.software.audio.dirac_v2_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DISABLE_AUTO_NETWORK_SELECT = "oplus.software.radio.disable_auto_network_select";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DISABLE_CLEAN_EU = "oplus.software.phonemanager_disable_clean_eu";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DISABLE_CLEAN_JP = "oplus.software.phonemanager_disable_clean_jp";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DISABLE_NR5G_CAUSE_5G_DUMP = "oplus.software.radio.disable_5g_nr_5gdump";
  
  public static final String FEATURE_DISABLE_POWERSAVE = "oplus.software.power.disable_power_save";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DISPLAY_PLMN_SPN = "oplus.software.radio.display_plmn_spn";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DOLBY_SUPPORT = "oplus.software.audio.dolby_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DRM_SUPPORT = "oplus.software.video.drm_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DUALHEADPHONE_SUPPORT = "oplus.software.audio.dualheadphone";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DUALHEADPHONE_SUPPORT_LITE = "oplus.software.audio.dualheadphone.lite";
  
  @OplusFeature(OplusFeature.OplusFeatureType.PERSIST_FEATURE)
  public static final String FEATURE_DUAL_NR_ENABLED = "oplus.software.radio.dual_nr_enabled";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DUAL_SPEAKER_SUPPORT = "oplus.software.audio.dualspk_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_DYNAMIC_FPS_SWITCH = "oplus.software.display.dynamic_fps_switch";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_ECC_CSFB_UPDATE_RAT = "oplus.software.radio.ecc_csfb_update_rat";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_ENVELOPE_ASSISTANT_ENABLE = "oplus.software.notification.envelope_assistant_enable";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_FAST_DORMANCY = "oplus.software.radio.fast_dormancy";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_FINGERPRINT_DISABLE_DIMLAYER = "oplus.software.display.fingerprint_disable_dimlayer";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_FINGERPRINT_SHUTTER = "oplus.software.fingerprint.shutter";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_FORWARDLY_FREEZE = "oplus.software.forwardly_freeze";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_FRONT_PRESS_FINGERPRINT = "oplus.software.fingeprint_front_press";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_FRONT_TOUCH_FINGERPRINT = "oplus.software.fingeprint_front_touch";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_FRONT_TPPROTECT_FINGERPRINT = "oplus.software.fingeprint_front_tpprotect";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_GAME_HAPTIC_VIBRATE_V1_SUPPORT = "oplus.software.haptic_vibrator_v1.support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_GAME_HAPTIC_VIBRATE_V2_SUPPORT = "oplus.software.haptic_vibrator_v2.support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_GAME_JOYSTICK_SUPPORT = "oplus.software.joystick.game_joystick_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_GAME_OPLUS_PLUS_V2_SUPPORT = "oplus.software.display.game_color_plus_v2_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_GAME_TOUCH_ADJUSTER_SUPPORT = "oplus.software.game.touch_adjuster_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_GEME_VOLUMEMUTE_SUPPORT = "oplus.software.audio.game_volumemute_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_GESTURE_BAR_FIEXD_SUPPORT = "oplus.software.gesture_bar_fixed.support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_GESTURE_PROXIMITYSENSOR = "oplus.software.gesture_proximitysensor";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_GUARDELF_SHOW_DELUSIVE_MEMORY = "oplus.software.oppoguardelf.show_confused_hardwareinfo";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_GUARDELF_SUPPORT = "oplus.software.power.guardelf";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_HANS_RESTRICTION = "oplus.software.hans_restriction";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_HDR_ENHANCE_BRIGHTNESS = "oplus.software.display.hdr_enhance_brightness_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_HIDE_ALERT_WINDOW_NOTIFICATION = "oplus.software.floatwindow_hide_alertwindow_notification";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_HQV_FRC_SUPPORT = "oplus.software.video.hqv_frc_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_HVOLTE_DISABLED = "oplus.software.radio.hvolte_disabled";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_IGNORE_RETRICTED_ENABLED = "oplus.software.radio.ignore_dcnr_retricted_enable";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_INEXACT_ALARM_SUPPORT = "oplus.software.power.inexact_alarm";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_INPUTMETHOD_CN = "oplus.software.inputmethod.cn";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_INTELLIGENT_OPLUS_TEMPERATURE_SUPPORT = "oplus.software.display.intelligent_color_temperature_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_INTERNAL_RECORD_SUPPORT = "oplus.software.audio.internal_record_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_JOB_CUSTOMIZEFUNC_SUPPORT = "oplus.software.power.jobscheduler";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_JP_SIGNAL_STRENGTH = "oplus.software.radio.jp_signal_strength";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_KARAOKE_V2_PITCHCHANGE = "oplus.software.audio.karaoke_v2.pitchchange";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_KARAOKE_V2_SUPPORT = "oplus.software.audio.karaoke_v2.support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_KEEPALIVE = "oplus.software.keep_alive";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_KEEPALIVE_EXT = "oplus.software.keep_alive_ext";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_KEYBOARD_RAISE = "oplus.software.country.keyboard_raise";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_LIGHT_SMART5G_ENABLED = "oplus.software.radio.light_smart5g_enabled";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_LIST_OPTIMIZE = "oplus.software.list_optimize";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_LMVIBRATOR_SUPPORT = "oplus.software.vibrator_lmvibrator";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_LOC_CONTROL_SWITCH = "oplus.software.radio.loc_control_switch";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_LOGKIT_HIDE_DESKTOP_ICON = "oplus.software.logkit.hide_desktop_icon";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_LOG_GAME_ERR = "oplus.software.radio.game_err";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_LOG_GAME_PAGING = "oplus.software.radio.game_paging";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_LTE_INSTEND_OF_CA = "oplus.software.radio.lte_instend_of_ca";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_MAGICVOICE_LOOPBACK_SUPPORT = "oplus.software.audio.magicvoice_loopback_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_MAGICVOICE_SUPPORT = "oplus.software.audio.magicvoice_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_MAGICVOICE_V2_BASIC_SUPPORT = "oplus.software.audio.magicvoice_v2_basic_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_MAX_DEFAULT_VOLUME_SUPPORT = "oplus.software.audio.max_default_volume";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_MAX_THREE_PDNS = "oplus.software.radio.max_three_pdns";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_MMS_NOT_ALLOW_INTERNET = "oplus.software.radio.mms_not_allow_internet";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_MOTOR_BACKCAMERA = "oplus.software.motor.backcamera";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_MOTOR_BACKFLASH = "oplus.software.motor.backflash";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_MOTOR_BREATHLED = "oplus.software.motor.breathled";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_MOTOR_DAT_ENABLED = "oplus.software.radio.motor_dat_enabled";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_MOTOR_SUPPORT = "oplus.software.motor_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_MTKOPLUSMODE_SUPPORT = "oplus.software.display.mtkcolormode_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_MULTIBITS_DIMMING_SUPPORT = "oplus.software.display.multibits_dimming_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_MULTI_APP_DISABLED = "oplus.software.multi_app_disabled";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_MULTI_APP_SUPPORT_LESS = "oplus.software.multiapp_support_less";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_MULTI_DEVICE_DECREASE_REFRESH_RATE_SUPPORT = "oplus.software.display.multi_device_decrease_refresh_rate";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_MULTI_USER_ENTRY_DISABLED = "oplus.software.multiuser_entry_disabled";
  
  @OplusFeature(OplusFeature.OplusFeatureType.PERSIST_FEATURE)
  public static final String FEATURE_NFC_AID_OVERFLOW = "oplus.software.nfc.over_flow";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_NFC_FELICA_SUPPORT = "oplus.software.nfc.felica_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_NOPROWERBLOCK_FACE = "oplus.software.face_nopowerblock";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_NOTIFICATION_CUSTOM_VERSION = "oplus.software.notification_custom_version";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_NOTIFY_DATA_CONNECTION = "oplus.software.radio.notify_data_connection";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_NOT_GRANT_CONTACTS = "oplus.software.disable_grant_calendar_contacts_permissions";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_NR_ALWAYS_SA_PRE = "oplus.software.radio.nr_always_sa_pre";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_NR_ICON_OPTIMIZED_ENABLED = "oplus.software.radio.nr_icon_optimized_enabled";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_NTP_PREFERRED = "oplus.software.radio.ntp_preferred";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_NWPOWER_STATIC_NETCONTROLLER_ENABLED = "oplus.software.radio.nwpower_static_netcontroller_enabled";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_NW_DATA_EVAL = "oplus.software.radio.data_eval";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_NW_DATA_FAST_RECOVERY = "oplus.software.radio.data_fast_recovery";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_NW_DATA_HONGBAO = "oplus.software.radio.data_hongbao";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_NW_DATA_LIMIT = "oplus.software.radio.data_limit";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_OPERATOR_SERVICE_COTA_COTA = "oplus.software.operator_service.cota_cota";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_OPERATOR_SERVICE_SFR_FR = "oplus.software.operator_service.sfr_fr";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_OPERATOR_SERVICE_TM_DE = "oplus.software.operator_service.tmobile_de";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_OPERATOR_SERVICE_TM_NL = "oplus.software.operator_service.tmobile_nl";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_OPERATOR_SERVICE_TM_PL = "oplus.software.operator_service.tmobile_pl";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_OPERATOR_SERVICE_TM_RO = "oplus.software.operator_service.tmobile_ro";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_OPERATOR_SERVICE_VF_DE = "oplus.software.operator_service.vodafone_de";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_OPERATOR_SERVICE_VF_ES = "oplus.software.operator_service.vodafone_es";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_OPERATOR_SERVICE_VF_GB = "oplus.software.operator_service.vodafone_gb";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_OPERATOR_SERVICE_VF_IT = "oplus.software.operator_service.vodafone_it";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_OPERATOR_SERVICE_VF_PT = "oplus.software.operator_service.vodafone_pt";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_OPLUSMODE_POWERSAVING_SUPPORT = "oplus.software.display.colormode_powersaving_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_OPLUSMODE_SUPPORT = "oplus.software.display.colormode_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_OPTICAL_FINGERPRINT = "oplus.software.fingeprint_optical_enabled";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_ORIGIN_ROUNDCORNER_SUPPORT = "oplus.software.display.origin_roundcorner_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_PA_AGING_FOR_N41_CHANNEL = "oplus.software.radio.pa_aging_for_n41_channel";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_PCCONNECT_SUPPORT = "oplus.software.pcconnect.support";
  
  public static final String FEATURE_PERMISSION_ACCOUNT_DIALOG_DISABLED = "oplus.software.permission_account_dialog_disabled";
  
  public static final String FEATURE_PERMISSION_BACKGROUND_REJECT_DISABLED = "oplus.software.permission_background_reject_disabled";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_PERMISSION_INTERCEPT = "oplus.software.permission_intercept_enabled";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_PERMISSION_INTERCEPT_LOCKSCREEN = "oplus.software.intercept_lockscreen_service";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_PERMISSION_PARSE = "oplus.software.permission_product_xml_parse";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_PERSONAL_ASSIST_ENABLE = "oplus.software.personal_assist_enable";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_PIXELWORKS_SUPPORT = "oplus.software.display.pixelworks_enable";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_PMS_ADB_SESSION_INSTALLER_INTERCEPT = "oplus.software.pms_adb_session_installer_intercept";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_PMS_APP_FROZEN = "oplus.software.pms_app_frozen";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_PMS_APP_LIST_INTERCEPT = "oplus.software.pms_app_list_intercept";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_PMS_DEVICE_OWNER_UNINSTALL_POLICY = "oplus.software.pms_device_owner_uninstall_policy";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_PMS_GP_VERIFICATION_DISABLED = "oplus.software.pms_gp_verification_disabled";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_PMS_HIDE_LAUNCHER_ENABLE = "oplus.software.pms_hide_launcher_enable";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_PMS_INSTALL_PERMISSION_AUTOALLOWED = "oplus.software.install_permission_autoallowed";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_PMS_INSTALL_STATISTICS_NO_SILENT = "oplus.software.pms_install_statistics_no_silent";
  
  @OplusFeature(OplusFeature.OplusFeatureType.PERSIST_FEATURE)
  public static final String FEATURE_PMS_SELLMODE = "oplus.software.pms_sellmode";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_PREPAID_POSTPAID_DYNAMIC_APN = "oplus.software.radio.prepaid_postpaid_dynamic_apn";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_QCOM_LMVIBRATOR_SUPPORT = "oplus.software.vibrator_qcom_lmvibrator";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_QC_GSM_FBRX_NOT_CAL = "oplus.software.radio.gsm_fbrx_not_cal";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_QC_PA_AGING_FOR_LTE_B28B = "oplus.software.radio.pa_aging_for_lte_b28b";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_QC_PA_AGING_FOR_NR_B28B = "oplus.software.radio.pa_aging_for_nr_b28b";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_RADIO_HEALTH_SERVICE_SUPPORT = "oplus.software.radio.health_service_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_RECORD_CHANNEL_SUPPORT = "oplus.software.audio.record_channel_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_REFRESHRATE_DEFAULT_SMART = "oplus.software.display.refreshrate_default_smart";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_RESOLVER_SHARE_EMAIL = "oplus.software.resolver_share_email";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_RINGER_TOGGLE_CHORD_DISABLE = "oplus.software.ringer_toggle_chord_disable";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_ROAMING_DYNAMIC_DUN_APN = "oplus.software.radio.roaming_dynamic_dun_apn";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SAR_AIRINTERFACE_DETECT_NOTSUPPORT = "oplus.software.radio.sar.airinterface.detect.notsupport";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SAR_EARPIECE_DETECT_SUPPORT = "oplus.software.radio.sar.earpiece.detect.support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SAR_FCC_DETECT_SUPPORT = "oplus.software.radio.sar.fcc.detect.support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SA_RAT_CONTROL = "oplus.software.radio.sa_rat_control";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SCREEN_DEFAULT_SMART_MODE_SUPPORT = "oplus.software.display.screen_defaultsmartmode";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SCREEN_GLOABLEHBM_SUPPORT = "oplus.software.display.screen_gloablehbm_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SCREEN_HETEROMORPHISM = "oplus.software.display.screen_heteromorphism";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SCREEN_PARTHBM_SUPPORT = "oplus.software.display.screen_parthbm_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SECURITYANALYSIS_SENDBROADCAST = "oplus.software.securityanalysis.sendbroadcast";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SENSOR_FUSIONLIGHT_SUPPORT = "oplus.sensor.fusionlight.support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SET_WIFI_SAR_ENABLED = "oplus.software.radio.set_wifi_sar_enabled";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SHOW_CONFUSED_HARDWARE_INFO = "oplus.software.show_confused_hardwareinfo";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SHOW_ESIM_ENGINEERMODE_ENABLED = "oplus.software.radio.esim_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SHOW_HD_PLUS = "oplus.software.radio.show_hd_plus";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SHOW_NOTIFICATION_TOAST = "oplus.software.notification.show_toast";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SHOW_NR5G_MODE = "oplus.software.radio.show_5g_nr_mode";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SIDE_FINGERPRINT = "oplus.software.side_fingerprint";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SIGNAL_SMOOTH_DISABLED = "oplus.software.radio.signal_smooth_disabled";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SIM_DETECT_STATUS_CHECK_SUPPORT = "oplus.software.radio.sim_detect_check_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SMART5G_EXP_CFG = "oplus.software.radio.smart5g_exp_cfg";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SMART5G_SA_ENABLED = "oplus.software.radio.smart5g_sa_enabled";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SMART5G_SP_REGION_DISABLE = "oplus.software.radio.smart5g_sp_region_disable";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SMS_FIND_PHONE = "oplus.software.radio.find_phone";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SMS_SET_DEFAULT_APP = "oplus.software.radio.sms_set_default_app";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SOS_SPECIAL_FUNCTION = "oplus.software.sos_special_function";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SPEECH_ASSIST_FOR_BREENO = "oplus.software.speech_assist_for_breeno";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SPEECH_ASSIST_FOR_GOOGLE = "oplus.software.speech_assist_for_google";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_STARTUP_STATISTICS_UPLOAD = "oplus.software.startup_statistics_upload";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_STARTUP_STRATEGY_RESTRICT = "oplus.software.startup_strategy_restrict";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SUPER_SLEEP = "oplus.software.super_sleep";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SUPPORT_DUAL_NR = "oplus.software.radio.support_dual_nr";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SUPPORT_SIM_LOCK_STATE = "oplus.software.radio.support_sim_lock_state";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_SURROUND_RECORD_SUPPORT = "oplus.software.video.surround_record_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.MEMORY_FEATURE)
  @OplusFeaturePermission("com.oppo.permission.OPLUS_FEATURE")
  public static final String FEATURE_TEST_MEMORY = "oplus.software.memory_test";
  
  @OplusFeature(OplusFeature.OplusFeatureType.MEMORY_FEATURE)
  @OplusFeaturePermission("com.oppo.permission.safe.OPLUS_FEATURE")
  public static final String FEATURE_TEST_MEMORY_1 = "oplus.software.memory_test1";
  
  @OplusFeature(OplusFeature.OplusFeatureType.MEMORY_NATIVE_FEATURE)
  public static final String FEATURE_TEST_NATIVE_MEMORY = "vendor.feature.test_native";
  
  @OplusFeature(OplusFeature.OplusFeatureType.PERSIST_NATIVE_FEATURE)
  public static final String FEATURE_TEST_NATIVE_PERSIST = "persist.vendor.feature.test_native";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_NATIVE_FEATURE)
  public static final String FEATURE_TEST_NATIVE_READONLY = "ro.vendor.feature.test_native";
  
  @OplusFeature(OplusFeature.OplusFeatureType.PERSIST_FEATURE)
  @OplusFeaturePermission("com.oppo.permission.OPLUS_FEATURE")
  public static final String FEATURE_TEST_PERSIST = "oplus.software.persist_test";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_TEST_READONLY_A = "oplus.software.readonly_test_a";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_TEST_READONLY_B = "oplus.software.readonly_test_b";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_TEST_READONLY_C = "oplus.software.readonly_test_c";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_TVCONNECT_SUPPORT = "oplus.software.tvconnect.support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_TV_VIDEOCALL_SUPPORT = "oplus.software.tv_videocall.support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_UNKNOWN_SOURCE_APP_INSTALL = "oplus.software.unknown_source_app_install";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_UXICON_EXP = "oplus.software.uxicon_exp";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_VERIFY_CODE_ENABLE = "oplus.software.inputmethod.verify_code_enable";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_VOICE_WAKEUP_3WORDS_SUPPORT = "oplus.software.audio.voice_wakeup_3words_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_VOICE_WAKEUP_SUPPORT = "oplus.software.audio.voice_wakeup_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WALLET_QUICK_LAUNCH = "oplus.software.wallet_quick_launch";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WIFI_AUTORECONNECT = "oplus.software.wlan.auto_reconnect";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WIFI_DBS = "oplus.software.wlan.dbs";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WIFI_DUALSTA = "oplus.software.wlan.dual_sta";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WIFI_DUALSTA_SWITCH_DEFAULT_OFF = "oplus.software.wlan.dual_sta.switch_default_off";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WIFI_LOG_EUEX = "oplus.software.wlan.log_euex";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WIFI_PRESET_BOUYGUE = "oplus.software.wlan.operator_preset_bouygue";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WIFI_PRESET_O2 = "oplus.software.wlan.o2_preset";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WIFI_PRESET_RAKUTEN = "oplus.software.wlan.operator_preset_rakuten";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WIFI_PRESET_SINGTEL = "oplus.software.wlan.operator_preset_singtel";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WIFI_PRESET_SWISSCOM = "oplus.software.wlan.operator_preset_swisscom";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WIFI_PRESET_THAILAND = "oplus.software.wlan.operator_preset_thailand";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WIFI_PRESET_TMOBILE = "oplus.software.wlan.operator_preset_tmobile";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WIFI_PRESET_TURKCELL = "oplus.software.wlan.operator_preset_turkcell";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WIFI_ROUTERBOOST = "oplus.software.wlan.routerboost";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WIFI_SAP_RANDOM_SSID = "oplus.software.wlan.softap_random_ssid";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WIFI_WIFI6 = "oplus.software.wlan.wifi6";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WLAN_ASSISTANT = "oplus.software.wlan.assistant";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WLAN_SLA = "oplus.software.wlan.sla";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WLAN_TRAFFIC_LIMIT = "oplus.software.wlan.traffic_limit";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String FEATURE_WNR_SUPPORT = "oplus.software.video.wnr_support";
  
  @OplusFeature(OplusFeature.OplusFeatureType.READONLY_FEATURE)
  public static final String OPLUS_FEATURE_INCREASE_TCP_SYNC_RETRIES = "oplus.software.radio.increase_tcp_sync_retries";
}
