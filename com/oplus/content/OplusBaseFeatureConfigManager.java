package com.oplus.content;

import android.os.IBinder;
import android.os.ServiceManager;
import android.util.Log;

public abstract class OplusBaseFeatureConfigManager implements IOplusFeatureConfigManager {
  protected String mName;
  
  protected IBinder mRemote;
  
  public OplusBaseFeatureConfigManager(String paramString) {
    this(ServiceManager.getService(paramString), paramString);
  }
  
  private OplusBaseFeatureConfigManager(IBinder paramIBinder, String paramString) {
    this.mRemote = paramIBinder;
    this.mName = paramString;
    if (paramIBinder == null)
      Log.e(OplusBaseFeatureConfigManager.class.getSimpleName(), "remote not published yet!!!", new Throwable()); 
  }
}
