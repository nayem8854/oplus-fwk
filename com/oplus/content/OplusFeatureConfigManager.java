package com.oplus.content;

import android.os.Debug;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.ArrayMap;
import android.util.Log;
import java.util.List;

public class OplusFeatureConfigManager extends OplusBaseFeatureConfigManager implements IOplusFeatureConfigList {
  private static final int STACK_SIZE = 6;
  
  private static final String TAG = "OplusFeatureConfigManager";
  
  private static volatile OplusFeatureConfigManager sInstance = null;
  
  private OplusFeatureCache mCache;
  
  private final ArrayMap<OnFeatureObserver, IOplusFeatureObserver> mFeatureObservers = new ArrayMap();
  
  private OplusFeatureConfigManager() {
    super("package");
    this.mCache = new OplusFeatureCache();
  }
  
  @Deprecated
  public static OplusFeatureConfigManager getInstacne() {
    // Byte code:
    //   0: getstatic com/oplus/content/OplusFeatureConfigManager.sInstance : Lcom/oplus/content/OplusFeatureConfigManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/content/OplusFeatureConfigManager
    //   8: monitorenter
    //   9: getstatic com/oplus/content/OplusFeatureConfigManager.sInstance : Lcom/oplus/content/OplusFeatureConfigManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/content/OplusFeatureConfigManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/content/OplusFeatureConfigManager.sInstance : Lcom/oplus/content/OplusFeatureConfigManager;
    //   27: ldc com/oplus/content/OplusFeatureConfigManager
    //   29: monitorexit
    //   30: goto -> 88
    //   33: astore_0
    //   34: ldc com/oplus/content/OplusFeatureConfigManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/content/OplusFeatureConfigManager.sInstance : Lcom/oplus/content/OplusFeatureConfigManager;
    //   42: getfield mRemote : Landroid/os/IBinder;
    //   45: ifnonnull -> 88
    //   48: getstatic com/oplus/content/OplusFeatureConfigManager.sInstance : Lcom/oplus/content/OplusFeatureConfigManager;
    //   51: getfield mName : Ljava/lang/String;
    //   54: invokestatic getService : (Ljava/lang/String;)Landroid/os/IBinder;
    //   57: astore_0
    //   58: aload_0
    //   59: ifnonnull -> 73
    //   62: ldc 'OplusFeatureConfigManager'
    //   64: ldc 'remote is still null'
    //   66: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   69: pop
    //   70: goto -> 88
    //   73: ldc 'OplusFeatureConfigManager'
    //   75: ldc 'remote is not null, update remote'
    //   77: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   80: pop
    //   81: getstatic com/oplus/content/OplusFeatureConfigManager.sInstance : Lcom/oplus/content/OplusFeatureConfigManager;
    //   84: aload_0
    //   85: putfield mRemote : Landroid/os/IBinder;
    //   88: getstatic com/oplus/content/OplusFeatureConfigManager.sInstance : Lcom/oplus/content/OplusFeatureConfigManager;
    //   91: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #48	-> 0
    //   #49	-> 6
    //   #50	-> 9
    //   #51	-> 15
    //   #53	-> 27
    //   #54	-> 39
    //   #55	-> 48
    //   #56	-> 58
    //   #57	-> 62
    //   #59	-> 73
    //   #60	-> 81
    //   #63	-> 88
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public boolean isRemoteReady() {
    boolean bool;
    if (this.mRemote != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static OplusFeatureConfigManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/content/OplusFeatureConfigManager.sInstance : Lcom/oplus/content/OplusFeatureConfigManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/content/OplusFeatureConfigManager
    //   8: monitorenter
    //   9: getstatic com/oplus/content/OplusFeatureConfigManager.sInstance : Lcom/oplus/content/OplusFeatureConfigManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/content/OplusFeatureConfigManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/content/OplusFeatureConfigManager.sInstance : Lcom/oplus/content/OplusFeatureConfigManager;
    //   27: ldc com/oplus/content/OplusFeatureConfigManager
    //   29: monitorexit
    //   30: goto -> 88
    //   33: astore_0
    //   34: ldc com/oplus/content/OplusFeatureConfigManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/content/OplusFeatureConfigManager.sInstance : Lcom/oplus/content/OplusFeatureConfigManager;
    //   42: getfield mRemote : Landroid/os/IBinder;
    //   45: ifnonnull -> 88
    //   48: getstatic com/oplus/content/OplusFeatureConfigManager.sInstance : Lcom/oplus/content/OplusFeatureConfigManager;
    //   51: getfield mName : Ljava/lang/String;
    //   54: invokestatic getService : (Ljava/lang/String;)Landroid/os/IBinder;
    //   57: astore_0
    //   58: aload_0
    //   59: ifnonnull -> 73
    //   62: ldc 'OplusFeatureConfigManager'
    //   64: ldc 'remote is still null'
    //   66: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   69: pop
    //   70: goto -> 88
    //   73: ldc 'OplusFeatureConfigManager'
    //   75: ldc 'remote is not null, update remote'
    //   77: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   80: pop
    //   81: getstatic com/oplus/content/OplusFeatureConfigManager.sInstance : Lcom/oplus/content/OplusFeatureConfigManager;
    //   84: aload_0
    //   85: putfield mRemote : Landroid/os/IBinder;
    //   88: getstatic com/oplus/content/OplusFeatureConfigManager.sInstance : Lcom/oplus/content/OplusFeatureConfigManager;
    //   91: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #71	-> 0
    //   #72	-> 6
    //   #73	-> 9
    //   #74	-> 15
    //   #76	-> 27
    //   #77	-> 39
    //   #78	-> 48
    //   #79	-> 58
    //   #80	-> 62
    //   #82	-> 73
    //   #83	-> 81
    //   #86	-> 88
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public boolean hasFeatureIPC(String paramString) throws RemoteException {
    if (this.mRemote == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("hasFeatureIPC ");
      stringBuilder.append(paramString);
      stringBuilder.append(" failed , pms not initialized.");
      stringBuilder.append(Debug.getCallers(6));
      paramString = stringBuilder.toString();
      Log.e("OplusFeatureConfigManager", paramString);
      return false;
    } 
    Parcel parcel2 = Parcel.obtain();
    Parcel parcel1 = Parcel.obtain();
    try {
      parcel2.writeInterfaceToken("android.app.IPackageManager");
      parcel2.writeString(paramString);
      this.mRemote.transact(20002, parcel2, parcel1, 0);
      parcel1.readException();
      return Boolean.valueOf(parcel1.readString()).booleanValue();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean hasFeature(String paramString) {
    return this.mCache.query(paramString);
  }
  
  public boolean enableFeature(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeString(paramString);
      this.mRemote.transact(20003, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean disableFeature(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeString(paramString);
      this.mRemote.transact(20004, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void notifyFeaturesUpdate(String paramString1, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      this.mRemote.transact(20005, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean registerFeatureObserver(List<String> paramList, OnFeatureObserver paramOnFeatureObserver) {
    if (paramOnFeatureObserver == null) {
      Log.e("OplusFeatureConfigManager", " registerFeatureObserver null observer");
      return false;
    } 
    synchronized (this.mFeatureObservers) {
      if (this.mFeatureObservers.get(paramOnFeatureObserver) != null) {
        Log.e("OplusFeatureConfigManager", "already regiter before");
        return false;
      } 
      OnFeatureObserverDelegate onFeatureObserverDelegate = new OnFeatureObserverDelegate();
      this(this, paramOnFeatureObserver);
      try {
        boolean bool = registerFeatureObserverInner(paramList, onFeatureObserverDelegate);
        if (bool)
          this.mFeatureObservers.put(paramOnFeatureObserver, onFeatureObserverDelegate); 
        return bool;
      } catch (RemoteException remoteException) {
        Log.e("OplusFeatureConfigManager", "registerFeatureObserver failed!!");
        remoteException.printStackTrace();
        return false;
      } 
    } 
  }
  
  public boolean unregisterFeatureObserver(OnFeatureObserver paramOnFeatureObserver) {
    if (paramOnFeatureObserver == null) {
      Log.i("OplusFeatureConfigManager", "unregisterFeatureObserver null observer");
      return false;
    } 
    synchronized (this.mFeatureObservers) {
      IOplusFeatureObserver iOplusFeatureObserver = (IOplusFeatureObserver)this.mFeatureObservers.get(paramOnFeatureObserver);
      if (iOplusFeatureObserver != null)
        try {
          this.mFeatureObservers.remove(paramOnFeatureObserver);
          return unregisterFeatureObserverInner(iOplusFeatureObserver);
        } catch (RemoteException remoteException) {
          remoteException.printStackTrace();
        }  
      return false;
    } 
  }
  
  private boolean registerFeatureObserverInner(List<String> paramList, IOplusFeatureObserver paramIOplusFeatureObserver) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeStringList(paramList);
      if (paramIOplusFeatureObserver != null) {
        IBinder iBinder = paramIOplusFeatureObserver.asBinder();
      } else {
        paramList = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramList);
      this.mRemote.transact(20006, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  private boolean unregisterFeatureObserverInner(IOplusFeatureObserver paramIOplusFeatureObserver) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      if (paramIOplusFeatureObserver != null) {
        IBinder iBinder = paramIOplusFeatureObserver.asBinder();
      } else {
        paramIOplusFeatureObserver = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusFeatureObserver);
      this.mRemote.transact(20007, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  class OnFeatureObserver {
    public abstract void onFeatureUpdate(List<String> param1List);
  }
  
  class OnFeatureObserverDelegate extends IOplusFeatureObserver.Stub {
    private final OplusFeatureConfigManager.OnFeatureObserver mObserver;
    
    final OplusFeatureConfigManager this$0;
    
    public OnFeatureObserverDelegate(OplusFeatureConfigManager.OnFeatureObserver param1OnFeatureObserver) {
      this.mObserver = param1OnFeatureObserver;
    }
    
    public void onFeatureUpdate(List<String> param1List) {
      this.mObserver.onFeatureUpdate(param1List);
    }
  }
}
