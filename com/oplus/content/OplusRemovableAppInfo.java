package com.oplus.content;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.io.PrintWriter;

public class OplusRemovableAppInfo implements Parcelable {
  private static final String TAG = OplusRemovableAppInfo.class.getSimpleName();
  
  private String packageName = "";
  
  private long versionCode = -1L;
  
  private String versionName = "";
  
  private String codePath = "";
  
  private String baseCodePath = "";
  
  private boolean uninstalled = false;
  
  private long fileSize = 0L;
  
  public static final Parcelable.Creator<OplusRemovableAppInfo> CREATOR;
  
  public OplusRemovableAppInfo(String paramString) {
    this.packageName = paramString;
  }
  
  public OplusRemovableAppInfo(Parcel paramParcel) {
    this.packageName = paramParcel.readString();
    this.versionCode = paramParcel.readLong();
    this.versionName = paramParcel.readString();
    this.codePath = paramParcel.readString();
    this.baseCodePath = paramParcel.readString();
    this.uninstalled = paramParcel.readBoolean();
    this.fileSize = paramParcel.readLong();
  }
  
  static {
    CREATOR = new Parcelable.Creator<OplusRemovableAppInfo>() {
        public OplusRemovableAppInfo createFromParcel(Parcel param1Parcel) {
          return new OplusRemovableAppInfo(param1Parcel);
        }
        
        public OplusRemovableAppInfo[] newArray(int param1Int) {
          return new OplusRemovableAppInfo[param1Int];
        }
      };
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (TextUtils.isEmpty(this.packageName)) {
      paramParcel.writeString("");
    } else {
      paramParcel.writeString(this.packageName);
    } 
    paramParcel.writeLong(this.versionCode);
    if (TextUtils.isEmpty(this.versionName)) {
      paramParcel.writeString("");
    } else {
      paramParcel.writeString(this.versionName);
    } 
    if (TextUtils.isEmpty(this.codePath)) {
      paramParcel.writeString("");
    } else {
      paramParcel.writeString(this.codePath);
    } 
    if (TextUtils.isEmpty(this.baseCodePath)) {
      paramParcel.writeString("");
    } else {
      paramParcel.writeString(this.baseCodePath);
    } 
    paramParcel.writeBoolean(this.uninstalled);
    paramParcel.writeLong(this.fileSize);
  }
  
  public String getPackageName() {
    return this.packageName;
  }
  
  public void setPackageName(String paramString) {
    this.packageName = paramString;
  }
  
  public long getVersionCode() {
    return this.versionCode;
  }
  
  public void setVersionCode(long paramLong) {
    this.versionCode = paramLong;
  }
  
  public String getVersionName() {
    return this.versionName;
  }
  
  public void setVersionName(String paramString) {
    this.versionName = paramString;
  }
  
  public String getCodePath() {
    return this.codePath;
  }
  
  public void setCodePath(String paramString) {
    this.codePath = paramString;
  }
  
  public String getBaseCodePath() {
    return this.baseCodePath;
  }
  
  public void setBaseCodePath(String paramString) {
    this.baseCodePath = paramString;
  }
  
  public boolean isUninstalled() {
    return this.uninstalled;
  }
  
  public void setUninstalled(boolean paramBoolean) {
    this.uninstalled = paramBoolean;
  }
  
  public long getFileSize() {
    return this.fileSize;
  }
  
  public void setFileSize(long paramLong) {
    this.fileSize = paramLong;
  }
  
  public void dump(PrintWriter paramPrintWriter) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("packageName = ");
    stringBuilder.append(this.packageName);
    stringBuilder.append(", versionCode = ");
    stringBuilder.append(this.versionCode);
    stringBuilder.append(", versionName = ");
    stringBuilder.append(this.versionName);
    stringBuilder.append(", codePath = ");
    stringBuilder.append(this.codePath);
    stringBuilder.append(", baseCodePath = ");
    stringBuilder.append(this.baseCodePath);
    stringBuilder.append(", uninstalled = ");
    stringBuilder.append(this.uninstalled);
    stringBuilder.append(", fileSize = ");
    stringBuilder.append(this.fileSize);
    paramPrintWriter.append(stringBuilder.toString());
  }
}
