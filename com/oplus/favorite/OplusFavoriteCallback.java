package com.oplus.favorite;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import java.util.ArrayList;

public abstract class OplusFavoriteCallback implements IOplusFavoriteCallback {
  protected static final boolean DBG = false;
  
  protected final String TAG = getClass().getSimpleName();
  
  public Handler createWorkHandler(Context paramContext, String paramString, int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Favorite.");
    stringBuilder.append(paramString);
    HandlerThread handlerThread = new HandlerThread(stringBuilder.toString(), paramInt);
    handlerThread.start();
    return new OplusFavoriteHandler(handlerThread, "Client");
  }
  
  public void onLoadFinished(Context paramContext, boolean paramBoolean1, boolean paramBoolean2, ArrayList<String> paramArrayList) {}
  
  public void onFavoriteStart(Context paramContext, OplusFavoriteResult paramOplusFavoriteResult) {}
  
  public void onFavoriteFinished(Context paramContext, OplusFavoriteResult paramOplusFavoriteResult) {}
}
