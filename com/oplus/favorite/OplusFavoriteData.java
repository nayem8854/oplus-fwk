package com.oplus.favorite;

public class OplusFavoriteData {
  public static final String DATA_TITLE = "data_title";
  
  public static final String DATA_URL = "data_url";
  
  private String mTitle = null;
  
  private String mUrl = null;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("FavoriteData{");
    stringBuilder.append("mTitle:");
    stringBuilder.append(this.mTitle);
    stringBuilder.append(", mUrl=");
    stringBuilder.append(this.mUrl);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void setTitle(String paramString) {
    this.mTitle = paramString;
  }
  
  public void setUrl(String paramString) {
    this.mUrl = paramString;
  }
  
  public String getTitle() {
    return this.mTitle;
  }
  
  public String getUrl() {
    return this.mUrl;
  }
}
