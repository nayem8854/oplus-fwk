package com.oplus.favorite;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.View;

public class OplusFavoriteFactory {
  public OplusFavoriteEngine setEngine(OplusFavoriteEngines paramOplusFavoriteEngines) {
    if (null.$SwitchMap$com$oplus$favorite$OplusFavoriteEngines[paramOplusFavoriteEngines.ordinal()] != 1)
      return new OplusEngineNone(); 
    return new OplusEngineTeddy();
  }
  
  class OplusEngineNone extends OplusFavoriteEngine {
    private OplusEngineNone() {}
    
    protected void onInit() {}
    
    protected void onRelease() {}
    
    protected void onLoadRule(Context param1Context, String param1String, OplusFavoriteCallback param1OplusFavoriteCallback) {}
    
    protected void onProcessClick(View param1View, OplusFavoriteCallback param1OplusFavoriteCallback) {}
    
    protected void onProcessCrawl(View param1View, OplusFavoriteCallback param1OplusFavoriteCallback) {}
    
    protected void onProcessSave(View param1View, OplusFavoriteCallback param1OplusFavoriteCallback) {}
    
    protected void onLogActivityInfo(Activity param1Activity) {}
    
    protected void onLogViewInfo(View param1View) {}
    
    public Handler getWorkHandler() {
      return null;
    }
  }
}
