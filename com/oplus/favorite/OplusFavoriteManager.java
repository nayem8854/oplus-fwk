package com.oplus.favorite;

import android.app.Activity;
import android.app.OplusActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.view.View;
import com.oplus.util.OplusContextUtil;
import com.oplus.util.OplusLog;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class OplusFavoriteManager implements IOplusFavoriteManager {
  private static final String ACTION_FAVORITE_NOTIFY_FAILED = "com.coloros.favorite.FavoriteBroadcast.NotifyFailed";
  
  private static final String ACTION_FAVORITE_NOTIFY_START = "com.coloros.favorite.FavoriteService.NotifyStart";
  
  private static final String EXTRA_PACKAGE_NAME = "package_name";
  
  private static final String EXTRA_SCENE_LIST = "scene_list";
  
  private static final boolean NOTIFY_DEBUG = SystemProperties.getBoolean("feature.favorite.notify", false);
  
  private static final String PACKAGE_FAVORITE = "com.coloros.favorite";
  
  private static final String RESULT_AUTHORITY = "com.coloros.favorite.result.provider";
  
  private static final String RESULT_PATH = "result";
  
  private static final Uri RESULT_URI;
  
  private static final String SETTINGS_FAVORITE = "coloros_favorite";
  
  private static final String SETTING_FIRST_START = "first_start";
  
  public static final String TAG = "OplusFavoriteManager";
  
  static {
    Uri.Builder builder = new Uri.Builder();
    builder = builder.scheme("content");
    builder = builder.authority("com.coloros.favorite.result.provider");
    builder = builder.path("result");
    RESULT_URI = builder.build();
  }
  
  private static volatile OplusFavoriteManager sInstance = null;
  
  private IOplusFavoriteEngine mEngine = null;
  
  private final OplusFavoriteFactory mFactory;
  
  private OplusFavoriteManager() {
    this.mFactory = new OplusFavoriteFactory();
    setEngine(OplusFavoriteEngines.TEDDY);
  }
  
  public static OplusFavoriteManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/favorite/OplusFavoriteManager.sInstance : Lcom/oplus/favorite/OplusFavoriteManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/favorite/OplusFavoriteManager
    //   8: monitorenter
    //   9: getstatic com/oplus/favorite/OplusFavoriteManager.sInstance : Lcom/oplus/favorite/OplusFavoriteManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/favorite/OplusFavoriteManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/favorite/OplusFavoriteManager.sInstance : Lcom/oplus/favorite/OplusFavoriteManager;
    //   27: ldc com/oplus/favorite/OplusFavoriteManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/favorite/OplusFavoriteManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/favorite/OplusFavoriteManager.sInstance : Lcom/oplus/favorite/OplusFavoriteManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #94	-> 0
    //   #95	-> 6
    //   #96	-> 9
    //   #97	-> 15
    //   #99	-> 27
    //   #101	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public void init(Context paramContext) {}
  
  public void release() {
    IOplusFavoriteEngine iOplusFavoriteEngine = this.mEngine;
    if (iOplusFavoriteEngine != null)
      iOplusFavoriteEngine.release(); 
  }
  
  public void processClick(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback) {
    IOplusFavoriteEngine iOplusFavoriteEngine = this.mEngine;
    if (iOplusFavoriteEngine == null || !iOplusFavoriteEngine.isTestOn())
      return; 
    this.mEngine.processClick(paramView, paramOplusFavoriteCallback);
  }
  
  public void processCrawl(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback) {
    if (this.mEngine != null) {
      FavoriteProcessCallback favoriteProcessCallback = new FavoriteProcessCallback(paramOplusFavoriteCallback);
      FavoriteStart favoriteStart = new FavoriteStart(favoriteProcessCallback, paramView);
      favoriteStart.run();
      this.mEngine.processCrawl(paramView, favoriteProcessCallback);
    } 
  }
  
  public void processSave(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback) {
    if (this.mEngine != null) {
      paramOplusFavoriteCallback = new FavoriteSaveCallback(paramOplusFavoriteCallback);
      this.mEngine.processSave(paramView, paramOplusFavoriteCallback);
    } 
  }
  
  public void logActivityInfo(Activity paramActivity) {
    IOplusFavoriteEngine iOplusFavoriteEngine = this.mEngine;
    if (iOplusFavoriteEngine == null || !iOplusFavoriteEngine.isLogOn())
      return; 
    this.mEngine.logActivityInfo(paramActivity);
  }
  
  public void logViewInfo(View paramView) {
    IOplusFavoriteEngine iOplusFavoriteEngine = this.mEngine;
    if (iOplusFavoriteEngine == null || !iOplusFavoriteEngine.isLogOn())
      return; 
    this.mEngine.logViewInfo(paramView);
  }
  
  public Handler getWorkHandler() {
    return this.mEngine.getWorkHandler();
  }
  
  public void setEngine(OplusFavoriteEngines paramOplusFavoriteEngines) {
    IOplusFavoriteEngine iOplusFavoriteEngine = this.mEngine;
    if (iOplusFavoriteEngine != null)
      iOplusFavoriteEngine.release(); 
    this.mEngine = this.mFactory.setEngine(paramOplusFavoriteEngines);
  }
  
  public boolean isSaved(Context paramContext, String paramString, List<Bundle> paramList) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual getContentResolver : ()Landroid/content/ContentResolver;
    //   4: astore #4
    //   6: aload_3
    //   7: invokeinterface iterator : ()Ljava/util/Iterator;
    //   12: astore #5
    //   14: iconst_0
    //   15: istore #6
    //   17: aload #5
    //   19: invokeinterface hasNext : ()Z
    //   24: ifeq -> 552
    //   27: aload #5
    //   29: invokeinterface next : ()Ljava/lang/Object;
    //   34: checkcast android/os/Bundle
    //   37: astore #7
    //   39: aconst_null
    //   40: astore #8
    //   42: aconst_null
    //   43: astore_3
    //   44: aconst_null
    //   45: astore #9
    //   47: aconst_null
    //   48: astore_1
    //   49: aconst_null
    //   50: astore #10
    //   52: new java/lang/StringBuilder
    //   55: astore #11
    //   57: aload #11
    //   59: invokespecial <init> : ()V
    //   62: aload #11
    //   64: ldc 'packageName = ?'
    //   66: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   69: pop
    //   70: new java/util/ArrayList
    //   73: astore #12
    //   75: aload #12
    //   77: invokespecial <init> : ()V
    //   80: aload #8
    //   82: astore_3
    //   83: aload #9
    //   85: astore_1
    //   86: aload #12
    //   88: aload_2
    //   89: invokeinterface add : (Ljava/lang/Object;)Z
    //   94: pop
    //   95: aload #8
    //   97: astore_3
    //   98: aload #9
    //   100: astore_1
    //   101: aload #7
    //   103: ldc 'data_title'
    //   105: invokevirtual getString : (Ljava/lang/String;)Ljava/lang/String;
    //   108: astore #13
    //   110: aload #8
    //   112: astore_3
    //   113: aload #9
    //   115: astore_1
    //   116: aload #7
    //   118: ldc 'data_url'
    //   120: invokevirtual getString : (Ljava/lang/String;)Ljava/lang/String;
    //   123: astore #7
    //   125: aload #8
    //   127: astore_3
    //   128: aload #9
    //   130: astore_1
    //   131: aload #13
    //   133: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   136: istore #14
    //   138: iconst_0
    //   139: istore #15
    //   141: iload #14
    //   143: ifne -> 152
    //   146: iconst_1
    //   147: istore #16
    //   149: goto -> 155
    //   152: iconst_0
    //   153: istore #16
    //   155: aload #8
    //   157: astore_3
    //   158: aload #9
    //   160: astore_1
    //   161: aload #7
    //   163: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   166: iconst_1
    //   167: ixor
    //   168: istore #17
    //   170: iload #16
    //   172: ifeq -> 229
    //   175: iload #17
    //   177: ifeq -> 229
    //   180: aload #8
    //   182: astore_3
    //   183: aload #9
    //   185: astore_1
    //   186: aload #11
    //   188: ldc ' AND title = ? AND url = ?'
    //   190: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   193: pop
    //   194: aload #8
    //   196: astore_3
    //   197: aload #9
    //   199: astore_1
    //   200: aload #12
    //   202: aload #13
    //   204: invokeinterface add : (Ljava/lang/Object;)Z
    //   209: pop
    //   210: aload #8
    //   212: astore_3
    //   213: aload #9
    //   215: astore_1
    //   216: aload #12
    //   218: aload #7
    //   220: invokeinterface add : (Ljava/lang/Object;)Z
    //   225: pop
    //   226: goto -> 318
    //   229: iload #16
    //   231: ifeq -> 267
    //   234: aload #8
    //   236: astore_3
    //   237: aload #9
    //   239: astore_1
    //   240: aload #11
    //   242: ldc ' AND title = ?'
    //   244: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   247: pop
    //   248: aload #8
    //   250: astore_3
    //   251: aload #9
    //   253: astore_1
    //   254: aload #12
    //   256: aload #13
    //   258: invokeinterface add : (Ljava/lang/Object;)Z
    //   263: pop
    //   264: goto -> 318
    //   267: iload #17
    //   269: ifeq -> 305
    //   272: aload #8
    //   274: astore_3
    //   275: aload #9
    //   277: astore_1
    //   278: aload #11
    //   280: ldc ' AND url = ?'
    //   282: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   285: pop
    //   286: aload #8
    //   288: astore_3
    //   289: aload #9
    //   291: astore_1
    //   292: aload #12
    //   294: aload #7
    //   296: invokeinterface add : (Ljava/lang/Object;)Z
    //   301: pop
    //   302: goto -> 318
    //   305: aload #8
    //   307: astore_3
    //   308: aload #9
    //   310: astore_1
    //   311: aload #12
    //   313: invokeinterface clear : ()V
    //   318: iload #6
    //   320: istore #14
    //   322: aload #8
    //   324: astore_3
    //   325: aload #9
    //   327: astore_1
    //   328: aload #12
    //   330: invokeinterface isEmpty : ()Z
    //   335: ifne -> 449
    //   338: aload #8
    //   340: astore_3
    //   341: aload #9
    //   343: astore_1
    //   344: aload #11
    //   346: invokevirtual toString : ()Ljava/lang/String;
    //   349: astore #10
    //   351: aload #8
    //   353: astore_3
    //   354: aload #9
    //   356: astore_1
    //   357: aload #12
    //   359: aload #12
    //   361: invokeinterface size : ()I
    //   366: anewarray java/lang/String
    //   369: invokeinterface toArray : ([Ljava/lang/Object;)[Ljava/lang/Object;
    //   374: checkcast [Ljava/lang/String;
    //   377: astore #12
    //   379: aload #8
    //   381: astore_3
    //   382: aload #9
    //   384: astore_1
    //   385: aload #4
    //   387: getstatic com/oplus/favorite/OplusFavoriteManager.RESULT_URI : Landroid/net/Uri;
    //   390: aconst_null
    //   391: aload #10
    //   393: aload #12
    //   395: aconst_null
    //   396: invokevirtual query : (Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   399: astore #8
    //   401: iload #6
    //   403: istore #14
    //   405: aload #8
    //   407: astore #10
    //   409: aload #8
    //   411: ifnull -> 449
    //   414: aload #8
    //   416: astore_3
    //   417: aload #8
    //   419: astore_1
    //   420: aload #8
    //   422: invokeinterface getCount : ()I
    //   427: istore #16
    //   429: iload #15
    //   431: istore #6
    //   433: iload #16
    //   435: ifle -> 441
    //   438: iconst_1
    //   439: istore #6
    //   441: aload #8
    //   443: astore #10
    //   445: iload #6
    //   447: istore #14
    //   449: aload #10
    //   451: ifnull -> 461
    //   454: aload #10
    //   456: invokeinterface close : ()V
    //   461: iload #14
    //   463: istore #15
    //   465: iload #14
    //   467: ifeq -> 524
    //   470: iload #14
    //   472: istore #6
    //   474: goto -> 552
    //   477: astore #8
    //   479: goto -> 488
    //   482: astore_2
    //   483: goto -> 532
    //   486: astore #8
    //   488: aload_3
    //   489: astore_1
    //   490: ldc 'OplusFavoriteManager'
    //   492: aload #8
    //   494: invokevirtual getMessage : ()Ljava/lang/String;
    //   497: aload #8
    //   499: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   502: aload_3
    //   503: ifnull -> 512
    //   506: aload_3
    //   507: invokeinterface close : ()V
    //   512: iload #6
    //   514: istore #15
    //   516: iload #6
    //   518: ifeq -> 524
    //   521: goto -> 474
    //   524: iload #15
    //   526: istore #6
    //   528: goto -> 17
    //   531: astore_2
    //   532: aload_1
    //   533: ifnull -> 542
    //   536: aload_1
    //   537: invokeinterface close : ()V
    //   542: iload #6
    //   544: ifeq -> 550
    //   547: goto -> 474
    //   550: aload_2
    //   551: athrow
    //   552: iload #6
    //   554: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #174	-> 0
    //   #175	-> 0
    //   #176	-> 6
    //   #177	-> 39
    //   #179	-> 52
    //   #180	-> 62
    //   #181	-> 70
    //   #182	-> 80
    //   #183	-> 95
    //   #184	-> 110
    //   #185	-> 125
    //   #186	-> 155
    //   #187	-> 170
    //   #188	-> 180
    //   #189	-> 194
    //   #190	-> 210
    //   #191	-> 229
    //   #192	-> 234
    //   #193	-> 248
    //   #194	-> 267
    //   #195	-> 272
    //   #196	-> 286
    //   #198	-> 305
    //   #200	-> 318
    //   #201	-> 338
    //   #202	-> 351
    //   #203	-> 379
    //   #209	-> 401
    //   #210	-> 414
    //   #216	-> 449
    //   #217	-> 454
    //   #219	-> 461
    //   #220	-> 474
    //   #213	-> 477
    //   #216	-> 482
    //   #213	-> 486
    //   #214	-> 488
    //   #216	-> 502
    //   #217	-> 506
    //   #219	-> 512
    //   #220	-> 521
    //   #223	-> 524
    //   #216	-> 531
    //   #217	-> 536
    //   #219	-> 542
    //   #220	-> 547
    //   #222	-> 550
    //   #176	-> 552
    //   #224	-> 552
    // Exception table:
    //   from	to	target	type
    //   52	62	486	java/lang/Exception
    //   52	62	482	finally
    //   62	70	486	java/lang/Exception
    //   62	70	482	finally
    //   70	80	486	java/lang/Exception
    //   70	80	482	finally
    //   86	95	477	java/lang/Exception
    //   86	95	531	finally
    //   101	110	477	java/lang/Exception
    //   101	110	531	finally
    //   116	125	477	java/lang/Exception
    //   116	125	531	finally
    //   131	138	477	java/lang/Exception
    //   131	138	531	finally
    //   161	170	477	java/lang/Exception
    //   161	170	531	finally
    //   186	194	477	java/lang/Exception
    //   186	194	531	finally
    //   200	210	477	java/lang/Exception
    //   200	210	531	finally
    //   216	226	477	java/lang/Exception
    //   216	226	531	finally
    //   240	248	477	java/lang/Exception
    //   240	248	531	finally
    //   254	264	477	java/lang/Exception
    //   254	264	531	finally
    //   278	286	477	java/lang/Exception
    //   278	286	531	finally
    //   292	302	477	java/lang/Exception
    //   292	302	531	finally
    //   311	318	477	java/lang/Exception
    //   311	318	531	finally
    //   328	338	477	java/lang/Exception
    //   328	338	531	finally
    //   344	351	477	java/lang/Exception
    //   344	351	531	finally
    //   357	379	477	java/lang/Exception
    //   357	379	531	finally
    //   385	401	477	java/lang/Exception
    //   385	401	531	finally
    //   420	429	477	java/lang/Exception
    //   420	429	531	finally
    //   490	502	531	finally
  }
  
  private void queryRule(Context paramContext) {
    long l2;
    boolean bool;
    long l1 = SystemClock.uptimeMillis();
    try {
      OplusActivityManager oplusActivityManager = new OplusActivityManager();
      this();
      String str = paramContext.getPackageName();
      FavoriteQueryCallback favoriteQueryCallback = new FavoriteQueryCallback();
      this(paramContext, this.mEngine);
      oplusActivityManager.favoriteQueryRule(str, (IOplusFavoriteQueryCallback)favoriteQueryCallback);
      l2 = SystemClock.uptimeMillis() - l1;
      bool = DBG;
      StringBuilder stringBuilder = new StringBuilder();
    } catch (RemoteException remoteException) {
      OplusLog.e("OplusFavoriteManager", remoteException.getMessage(), (Throwable)remoteException);
      l2 = SystemClock.uptimeMillis() - l1;
      bool = DBG;
      StringBuilder stringBuilder = new StringBuilder();
    } catch (Exception exception) {
      OplusLog.e("OplusFavoriteManager", exception.getMessage(), exception);
      l2 = SystemClock.uptimeMillis() - l1;
      bool = DBG;
      StringBuilder stringBuilder = new StringBuilder();
    } finally {}
    paramContext.append("queryRule : spend=");
    paramContext.append(l2);
    paramContext.append("ms");
    OplusLog.d(bool, "OplusFavoriteManager", paramContext.toString());
  }
  
  private void initRule(Context paramContext) {
    IOplusFavoriteEngine iOplusFavoriteEngine = this.mEngine;
    if (iOplusFavoriteEngine != null) {
      iOplusFavoriteEngine.init();
      queryRule(paramContext);
    } 
  }
  
  private static class FavoriteStart implements Runnable {
    private final WeakReference<OplusFavoriteCallback> mCallback;
    
    private final WeakReference<View> mView;
    
    public FavoriteStart(OplusFavoriteCallback param1OplusFavoriteCallback, View param1View) {
      this.mCallback = new WeakReference<>(param1OplusFavoriteCallback);
      this.mView = new WeakReference<>(param1View);
    }
    
    public void run() {
      OplusFavoriteCallback oplusFavoriteCallback = this.mCallback.get();
      View view = this.mView.get();
      if (oplusFavoriteCallback != null && view != null) {
        Context context = view.getContext();
        OplusFavoriteResult oplusFavoriteResult = new OplusFavoriteResult();
        oplusFavoriteResult.setPackageName(context.getPackageName());
        oplusFavoriteCallback.onFavoriteStart(context, oplusFavoriteResult);
      } 
    }
  }
  
  private static class FavoriteQueryCallback extends OplusFavoriteQueryCallback {
    private final WeakReference<Context> mContext;
    
    private final WeakReference<IOplusFavoriteEngine> mEngine;
    
    public FavoriteQueryCallback(Context param1Context, IOplusFavoriteEngine param1IOplusFavoriteEngine) {
      this.mContext = new WeakReference<>(param1Context);
      this.mEngine = new WeakReference<>(param1IOplusFavoriteEngine);
    }
    
    public void onQueryResult(OplusFavoriteQueryResult param1OplusFavoriteQueryResult) {
      Context context = this.mContext.get();
      IOplusFavoriteEngine iOplusFavoriteEngine = this.mEngine.get();
      if (context != null && iOplusFavoriteEngine != null) {
        Bundle bundle = param1OplusFavoriteQueryResult.getBundle();
        String str = bundle.getString("data", null);
        iOplusFavoriteEngine.loadRule(context, str, new OplusFavoriteManager.FavoriteLoadCallback(null));
      } 
    }
  }
  
  private static class FavoriteCallback extends OplusFavoriteCallback {
    private final WeakReference<OplusFavoriteCallback> mCallback;
    
    public boolean isSettingOn(Context param1Context) {
      return OplusFavoriteHelper.isSettingOn(param1Context);
    }
    
    public FavoriteCallback(OplusFavoriteCallback param1OplusFavoriteCallback) {
      this.mCallback = new WeakReference<>(param1OplusFavoriteCallback);
    }
    
    public void onFavoriteStart(Context param1Context, OplusFavoriteResult param1OplusFavoriteResult) {
      OplusFavoriteCallback oplusFavoriteCallback = this.mCallback.get();
      if (oplusFavoriteCallback != null)
        oplusFavoriteCallback.onFavoriteStart(param1Context, param1OplusFavoriteResult); 
    }
    
    public void onFavoriteFinished(Context param1Context, OplusFavoriteResult param1OplusFavoriteResult) {
      OplusFavoriteCallback oplusFavoriteCallback = this.mCallback.get();
      if (oplusFavoriteCallback != null)
        oplusFavoriteCallback.onFavoriteFinished(param1Context, param1OplusFavoriteResult); 
    }
  }
  
  class FavoriteLoadCallback extends FavoriteCallback {
    public FavoriteLoadCallback(OplusFavoriteManager this$0) {
      super((OplusFavoriteCallback)this$0);
    }
    
    public boolean isSettingOn(Context param1Context) {
      return OplusFavoriteHelper.isSettingOn(param1Context);
    }
    
    public void onLoadFinished(Context param1Context, boolean param1Boolean1, boolean param1Boolean2, ArrayList<String> param1ArrayList) {
      super.onLoadFinished(param1Context, param1Boolean1, param1Boolean2, param1ArrayList);
      Activity activity = OplusContextUtil.getActivityContext(param1Context);
      if (activity == null) {
        logI("onLoadFinished", activity, "Not Activity");
      } else if (param1Boolean1) {
        logI("onLoadFinished", activity, "No Rule List");
      } else if (param1Boolean2) {
        logE("onLoadFinished", activity, "Empty Rule List");
      } else if (!isFirstStart(param1Context)) {
        logI("onLoadFinished", activity, "Load Again");
      } else if (!isSettingOn(param1Context)) {
        logI("onLoadFinished", activity, "Setting Off");
      } else {
        logI("onLoadFinished", activity, "Load First");
        clearFirstStart(param1Context);
        notifyStart(param1Context, param1Context.getPackageName(), param1ArrayList);
      } 
    }
    
    private String buildMessage(String param1String1, Activity param1Activity, String param1String2) {
      try {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("[");
        stringBuilder.append(this.TAG);
        stringBuilder.append("][");
        stringBuilder.append(param1String1);
        stringBuilder.append("] ");
        stringBuilder.append(param1Activity);
        stringBuilder.append(" : ");
        stringBuilder.append(param1String2);
        return stringBuilder.toString();
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("buildMessage exception! tag = ");
        stringBuilder.append(param1String1);
        stringBuilder.append(", msg ");
        stringBuilder.append(param1String2);
        stringBuilder.append(", e = ");
        stringBuilder.append(exception);
        OplusLog.w(false, "AnteaterFavorite", stringBuilder.toString());
        return exception.getMessage();
      } 
    }
    
    private void logI(String param1String1, Activity param1Activity, String param1String2) {
      OplusLog.i(false, "AnteaterFavorite", buildMessage(param1String1, param1Activity, param1String2));
    }
    
    private void logE(String param1String1, Activity param1Activity, String param1String2) {
      OplusLog.e(false, "AnteaterFavorite", buildMessage(param1String1, param1Activity, param1String2));
    }
    
    private SharedPreferences getSharedPreferences(Context param1Context) {
      return param1Context.getSharedPreferences("coloros_favorite", 0);
    }
    
    private void clearFirstStart(Context param1Context) {
      SharedPreferences sharedPreferences = getSharedPreferences(param1Context);
      SharedPreferences.Editor editor = sharedPreferences.edit();
      editor.putBoolean("first_start", false);
      editor.commit();
    }
    
    private boolean isFirstStart(Context param1Context) {
      if (OplusFavoriteManager.NOTIFY_DEBUG)
        return true; 
      SharedPreferences sharedPreferences = getSharedPreferences(param1Context);
      return sharedPreferences.getBoolean("first_start", true);
    }
    
    private void notifyStart(Context param1Context, String param1String, ArrayList<String> param1ArrayList) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("[");
      stringBuilder.append(this.TAG);
      stringBuilder.append("] notifyStart : ");
      stringBuilder.append(param1String);
      OplusLog.i(false, "AnteaterFavorite", stringBuilder.toString());
      try {
        Intent intent = new Intent();
        this();
        intent.setAction("com.coloros.favorite.FavoriteService.NotifyStart");
        intent.setPackage("com.coloros.favorite");
        intent.putExtra("package_name", param1String);
        intent.putExtra("scene_list", param1ArrayList);
        param1Context.startForegroundService(intent);
      } catch (SecurityException securityException) {
        OplusLog.w(this.TAG, securityException.getMessage(), securityException);
      } catch (Exception exception) {
        OplusLog.e(this.TAG, exception.getMessage(), exception);
      } 
    }
  }
  
  class FavoriteProcessCallback extends FavoriteCallback {
    public FavoriteProcessCallback(OplusFavoriteManager this$0) {
      super((OplusFavoriteCallback)this$0);
    }
  }
  
  class FavoriteSaveCallback extends FavoriteCallback {
    public FavoriteSaveCallback(OplusFavoriteManager this$0) {
      super((OplusFavoriteCallback)this$0);
    }
    
    public void onFavoriteFinished(Context param1Context, OplusFavoriteResult param1OplusFavoriteResult) {
      super.onFavoriteFinished(param1Context, param1OplusFavoriteResult);
      if (TextUtils.isEmpty(param1OplusFavoriteResult.getError())) {
        onSaveSuccess(param1Context);
      } else {
        onSaveFailed(param1Context);
      } 
    }
    
    private void onSaveSuccess(Context param1Context) {
      String str = param1Context.getPackageName();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("[");
      stringBuilder.append(this.TAG);
      stringBuilder.append("] onSaveSuccess : ");
      stringBuilder.append(str);
      OplusLog.i(false, "AnteaterFavorite", stringBuilder.toString());
    }
    
    private void onSaveFailed(Context param1Context) {
      String str = param1Context.getPackageName();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("[");
      stringBuilder.append(this.TAG);
      stringBuilder.append("] onSaveFailed : ");
      stringBuilder.append(str);
      OplusLog.i(false, "AnteaterFavorite", stringBuilder.toString());
      try {
        Intent intent = new Intent();
        this();
        intent.setAction("com.coloros.favorite.FavoriteBroadcast.NotifyFailed");
        intent.setPackage("com.coloros.favorite");
        intent.putExtra("package_name", str);
        param1Context.sendBroadcast(intent);
      } catch (SecurityException securityException) {
        OplusLog.w(this.TAG, securityException.getMessage(), securityException);
      } catch (Exception exception) {
        OplusLog.e(this.TAG, exception.getMessage(), exception);
      } 
    }
  }
}
