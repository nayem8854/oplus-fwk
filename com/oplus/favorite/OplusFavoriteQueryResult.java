package com.oplus.favorite;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public class OplusFavoriteQueryResult implements Parcelable {
  public static final Parcelable.Creator<OplusFavoriteQueryResult> CREATOR = new Parcelable.Creator<OplusFavoriteQueryResult>() {
      public OplusFavoriteQueryResult createFromParcel(Parcel param1Parcel) {
        return new OplusFavoriteQueryResult(param1Parcel);
      }
      
      public OplusFavoriteQueryResult[] newArray(int param1Int) {
        return new OplusFavoriteQueryResult[param1Int];
      }
    };
  
  public static final String EXTRA_DATA = "data";
  
  public static final String EXTRA_ERROR = "error";
  
  private static final String TAG = "OplusFavoriteQueryResult";
  
  private final Bundle mBundle = new Bundle();
  
  public OplusFavoriteQueryResult(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Result=");
    stringBuilder.append(this.mBundle);
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.mBundle.writeToParcel(paramParcel, paramInt);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mBundle.readFromParcel(paramParcel);
  }
  
  public Bundle getBundle() {
    return this.mBundle;
  }
  
  public OplusFavoriteQueryResult() {}
}
