package com.oplus.favorite;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusFavoriteQueryCallback extends IInterface {
  void onQueryResult(OplusFavoriteQueryResult paramOplusFavoriteQueryResult) throws RemoteException;
  
  class Default implements IOplusFavoriteQueryCallback {
    public void onQueryResult(OplusFavoriteQueryResult param1OplusFavoriteQueryResult) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusFavoriteQueryCallback {
    private static final String DESCRIPTOR = "com.oplus.favorite.IOplusFavoriteQueryCallback";
    
    static final int TRANSACTION_onQueryResult = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.favorite.IOplusFavoriteQueryCallback");
    }
    
    public static IOplusFavoriteQueryCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.favorite.IOplusFavoriteQueryCallback");
      if (iInterface != null && iInterface instanceof IOplusFavoriteQueryCallback)
        return (IOplusFavoriteQueryCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onQueryResult";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.favorite.IOplusFavoriteQueryCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.favorite.IOplusFavoriteQueryCallback");
      if (param1Parcel1.readInt() != 0) {
        OplusFavoriteQueryResult oplusFavoriteQueryResult = (OplusFavoriteQueryResult)OplusFavoriteQueryResult.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onQueryResult((OplusFavoriteQueryResult)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IOplusFavoriteQueryCallback {
      public static IOplusFavoriteQueryCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.favorite.IOplusFavoriteQueryCallback";
      }
      
      public void onQueryResult(OplusFavoriteQueryResult param2OplusFavoriteQueryResult) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.favorite.IOplusFavoriteQueryCallback");
          if (param2OplusFavoriteQueryResult != null) {
            parcel.writeInt(1);
            param2OplusFavoriteQueryResult.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusFavoriteQueryCallback.Stub.getDefaultImpl() != null) {
            IOplusFavoriteQueryCallback.Stub.getDefaultImpl().onQueryResult(param2OplusFavoriteQueryResult);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusFavoriteQueryCallback param1IOplusFavoriteQueryCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusFavoriteQueryCallback != null) {
          Proxy.sDefaultImpl = param1IOplusFavoriteQueryCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusFavoriteQueryCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
