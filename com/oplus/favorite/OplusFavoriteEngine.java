package com.oplus.favorite;

import android.app.Activity;
import android.content.Context;
import android.os.SystemProperties;
import android.view.View;
import com.oplus.util.OplusLog;

public abstract class OplusFavoriteEngine implements IOplusFavoriteEngine {
  protected final String TAG = getClass().getSimpleName();
  
  protected static final boolean DBG = IOplusFavoriteManager.DBG;
  
  private static final boolean ENGINE_DEBUG = SystemProperties.getBoolean("feature.favorite.debug", false);
  
  private static final boolean ENGINE_TEST = SystemProperties.getBoolean("feature.favorite.test", false);
  
  private static final boolean ENGINE_LOG;
  
  static {
    ENGINE_LOG = SystemProperties.getBoolean("feature.favorite.log", false);
  }
  
  public final void init() {
    boolean bool = DBG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[");
    stringBuilder.append(this.TAG);
    stringBuilder.append("] init()");
    OplusLog.i(bool, "AnteaterFavorite", stringBuilder.toString());
    onInit();
  }
  
  public final void release() {
    boolean bool = DBG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[");
    stringBuilder.append(this.TAG);
    stringBuilder.append("] release()");
    OplusLog.i(bool, "AnteaterFavorite", stringBuilder.toString());
    onRelease();
  }
  
  public final void loadRule(Context paramContext, String paramString, OplusFavoriteCallback paramOplusFavoriteCallback) {
    boolean bool = DBG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[");
    stringBuilder.append(this.TAG);
    stringBuilder.append("] loadRule() : ");
    stringBuilder.append(paramContext.getPackageName());
    stringBuilder.append("/");
    stringBuilder.append(paramContext);
    OplusLog.i(bool, "AnteaterFavorite", stringBuilder.toString());
    onLoadRule(paramContext, paramString, paramOplusFavoriteCallback);
  }
  
  public final void processClick(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback) {
    boolean bool = DBG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[");
    stringBuilder.append(this.TAG);
    stringBuilder.append("] processClick() : ");
    stringBuilder.append(paramView);
    OplusLog.i(bool, "AnteaterFavorite", stringBuilder.toString());
    onProcessClick(paramView, paramOplusFavoriteCallback);
  }
  
  public final void processCrawl(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback) {
    boolean bool = DBG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[");
    stringBuilder.append(this.TAG);
    stringBuilder.append("] processCrawl() : ");
    stringBuilder.append(paramView);
    OplusLog.i(bool, "AnteaterFavorite", stringBuilder.toString());
    onProcessCrawl(paramView, paramOplusFavoriteCallback);
  }
  
  public final void processSave(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback) {
    boolean bool = DBG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[");
    stringBuilder.append(this.TAG);
    stringBuilder.append("] processSave() : ");
    stringBuilder.append(paramView);
    OplusLog.i(bool, "AnteaterFavorite", stringBuilder.toString());
    onProcessSave(paramView, paramOplusFavoriteCallback);
  }
  
  public final void logActivityInfo(Activity paramActivity) {
    try {
      boolean bool = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("[");
      stringBuilder.append(this.TAG);
      stringBuilder.append("] logActivityInfo() : ");
      stringBuilder.append(paramActivity);
      OplusLog.i(bool, "AnteaterFavorite", stringBuilder.toString());
    } catch (Exception exception) {
      boolean bool = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("[");
      stringBuilder.append(this.TAG);
      stringBuilder.append("] logActivityInfo() exception e : ");
      stringBuilder.append(exception);
      OplusLog.w(bool, "AnteaterFavorite", stringBuilder.toString());
    } 
    onLogActivityInfo(paramActivity);
  }
  
  public final void logViewInfo(View paramView) {
    try {
      boolean bool = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("[");
      stringBuilder.append(this.TAG);
      stringBuilder.append("] logViewInfo() : ");
      stringBuilder.append(paramView);
      OplusLog.i(bool, "AnteaterFavorite", stringBuilder.toString());
    } catch (Exception exception) {
      boolean bool = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("[");
      stringBuilder.append(this.TAG);
      stringBuilder.append("] logViewInfo() exception e : ");
      stringBuilder.append(exception);
      OplusLog.w(bool, "AnteaterFavorite", stringBuilder.toString());
    } 
    onLogViewInfo(paramView);
  }
  
  public final boolean isDebugOn() {
    return ENGINE_DEBUG;
  }
  
  public final boolean isTestOn() {
    return ENGINE_TEST;
  }
  
  public final boolean isLogOn() {
    return ENGINE_LOG;
  }
  
  protected abstract void onInit();
  
  protected abstract void onLoadRule(Context paramContext, String paramString, OplusFavoriteCallback paramOplusFavoriteCallback);
  
  protected abstract void onLogActivityInfo(Activity paramActivity);
  
  protected abstract void onLogViewInfo(View paramView);
  
  protected abstract void onProcessClick(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback);
  
  protected abstract void onProcessCrawl(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback);
  
  protected abstract void onProcessSave(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback);
  
  protected abstract void onRelease();
}
