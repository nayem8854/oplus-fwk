package com.oplus.favorite;

import android.content.Context;
import android.os.Handler;
import java.util.ArrayList;

public interface IOplusFavoriteCallback {
  Handler createWorkHandler(Context paramContext, String paramString, int paramInt);
  
  boolean isSettingOn(Context paramContext);
  
  void onFavoriteFinished(Context paramContext, OplusFavoriteResult paramOplusFavoriteResult);
  
  void onFavoriteStart(Context paramContext, OplusFavoriteResult paramOplusFavoriteResult);
  
  void onLoadFinished(Context paramContext, boolean paramBoolean1, boolean paramBoolean2, ArrayList<String> paramArrayList);
}
