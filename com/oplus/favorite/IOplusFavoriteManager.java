package com.oplus.favorite;

import android.app.Activity;
import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemProperties;
import android.view.View;
import java.util.List;

public interface IOplusFavoriteManager extends IOplusBaseFavoriteManager {
  public static final boolean DBG;
  
  public static final IOplusFavoriteManager DEFAULT;
  
  public static final boolean LOG_DEBUG;
  
  public static final boolean LOG_PANIC = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  static {
    boolean bool = false;
  }
  
  static {
    boolean bool1 = SystemProperties.getBoolean("log.favorite.debug", false);
    if (LOG_PANIC || bool1)
      bool = true; 
    DBG = bool;
    DEFAULT = (IOplusFavoriteManager)new Object();
  }
  
  default IOplusFavoriteManager getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusFavoriteManager;
  }
  
  default void init(Context paramContext) {}
  
  default void release() {}
  
  default void processClick(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback) {}
  
  default void processCrawl(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback) {}
  
  default void processSave(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback) {}
  
  default void logActivityInfo(Activity paramActivity) {}
  
  default void logViewInfo(View paramView) {}
  
  default Handler getWorkHandler() {
    return null;
  }
  
  default void setEngine(OplusFavoriteEngines paramOplusFavoriteEngines) {}
  
  default boolean isSaved(Context paramContext, String paramString, List<Bundle> paramList) {
    return false;
  }
}
