package com.oplus.favorite;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import cn.teddymobile.free.anteater.AnteaterClient;
import cn.teddymobile.free.anteater.helper.AnteaterHelper;
import cn.teddymobile.free.anteater.resources.RuleResourcesClient;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class OplusEngineTeddy extends OplusFavoriteEngine {
  protected void onInit() {
    AnteaterClient anteaterClient = AnteaterClient.getInstance();
    anteaterClient.init();
  }
  
  protected void onRelease() {
    AnteaterClient anteaterClient = AnteaterClient.getInstance();
    anteaterClient.release();
  }
  
  protected void onProcessClick(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback) {
    startSave(paramView, paramOplusFavoriteCallback, "click");
  }
  
  protected void onProcessCrawl(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback) {
    View view = paramView;
    if (paramView != null)
      view = paramView.findViewById(16908290); 
    startCrawl(view, paramOplusFavoriteCallback, "display");
  }
  
  protected void onProcessSave(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback) {
    View view = paramView;
    if (paramView != null)
      view = paramView.findViewById(16908290); 
    startSave(view, paramOplusFavoriteCallback, "display");
  }
  
  protected void onLogActivityInfo(Activity paramActivity) {
    AnteaterHelper anteaterHelper = AnteaterHelper.getInstance();
    anteaterHelper.logActivityInfo(paramActivity);
  }
  
  protected void onLogViewInfo(View paramView) {
    AnteaterHelper anteaterHelper = AnteaterHelper.getInstance();
    anteaterHelper.logViewInfo(paramView);
  }
  
  protected void onLoadRule(Context paramContext, String paramString, OplusFavoriteCallback paramOplusFavoriteCallback) {
    AnteaterClient anteaterClient = AnteaterClient.getInstance();
    anteaterClient.loadRule(paramContext, paramString, new AntaterLoadCallback(paramOplusFavoriteCallback));
  }
  
  public Handler getWorkHandler() {
    AnteaterClient anteaterClient = AnteaterClient.getInstance();
    return anteaterClient.getWorkHandler();
  }
  
  private void startSave(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback, String paramString) {
    AnteaterSaveCallback anteaterSaveCallback = new AnteaterSaveCallback(paramOplusFavoriteCallback);
    AnteaterClient anteaterClient = AnteaterClient.getInstance();
    anteaterClient.save(paramString, paramView, anteaterSaveCallback);
  }
  
  private void startCrawl(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback, String paramString) {
    AnteaterProcessCallback anteaterProcessCallback = new AnteaterProcessCallback(paramOplusFavoriteCallback);
    AnteaterClient anteaterClient = AnteaterClient.getInstance();
    anteaterClient.process(paramString, paramView, anteaterProcessCallback);
  }
  
  class AnteaterCallback {
    final WeakReference<OplusFavoriteCallback> mCallback;
    
    public AnteaterCallback(OplusEngineTeddy this$0) {
      this.mCallback = new WeakReference(this$0);
    }
    
    protected boolean isSettingOn(Context param1Context) {
      OplusFavoriteCallback oplusFavoriteCallback = this.mCallback.get();
      if (oplusFavoriteCallback != null)
        return oplusFavoriteCallback.isSettingOn(param1Context); 
      return false;
    }
    
    protected Handler onCreateWorkHandler(Context param1Context, String param1String, int param1Int) {
      OplusFavoriteCallback oplusFavoriteCallback = this.mCallback.get();
      if (oplusFavoriteCallback != null)
        return oplusFavoriteCallback.createWorkHandler(param1Context, param1String, param1Int); 
      return null;
    }
    
    protected void onHandleFinished(Context param1Context, AnteaterClient.ResultData param1ResultData) {
      OplusFavoriteCallback oplusFavoriteCallback = this.mCallback.get();
      if (oplusFavoriteCallback != null) {
        OplusFavoriteResult oplusFavoriteResult = new OplusFavoriteResult();
        oplusFavoriteResult.setData(getQueryList(param1ResultData));
        oplusFavoriteResult.setError(getErrorMessage(param1ResultData.getError()));
        oplusFavoriteCallback.onFavoriteFinished(param1Context, oplusFavoriteResult);
      } 
    }
    
    private ArrayList<OplusFavoriteData> getQueryList(AnteaterClient.ResultData param1ResultData) {
      null = new ArrayList();
      synchronized (param1ResultData.getQueryList()) {
        for (AnteaterClient.QueryData queryData : null) {
          OplusFavoriteData oplusFavoriteData = new OplusFavoriteData();
          this();
          oplusFavoriteData.setTitle(queryData.getTitle());
          oplusFavoriteData.setUrl(queryData.getUrl());
          null.add(oplusFavoriteData);
        } 
        return null;
      } 
    }
    
    private String getErrorMessage(AnteaterClient.ErrorCode param1ErrorCode) {
      switch (OplusEngineTeddy.null.$SwitchMap$cn$teddymobile$free$anteater$AnteaterClient$ErrorCode[param1ErrorCode.ordinal()]) {
        default:
          return null;
        case 6:
          return "Save failed";
        case 5:
          return "Setting off";
        case 4:
          return "Unsupported";
        case 3:
          return "Not found";
        case 2:
          return "Not init";
        case 1:
          break;
      } 
      return "No view";
    }
  }
  
  private static class AntaterLoadCallback extends AnteaterCallback implements RuleResourcesClient.LoadCallback {
    public AntaterLoadCallback(OplusFavoriteCallback param1OplusFavoriteCallback) {
      super(param1OplusFavoriteCallback);
    }
    
    public void onLoadResult(Context param1Context, boolean param1Boolean1, boolean param1Boolean2, ArrayList<String> param1ArrayList) {
      OplusFavoriteCallback oplusFavoriteCallback = this.mCallback.get();
      if (oplusFavoriteCallback != null)
        oplusFavoriteCallback.onLoadFinished(param1Context, param1Boolean1, param1Boolean2, param1ArrayList); 
    }
  }
  
  private static class AnteaterSaveCallback extends AnteaterCallback implements AnteaterClient.SaveCallback {
    public AnteaterSaveCallback(OplusFavoriteCallback param1OplusFavoriteCallback) {
      super(param1OplusFavoriteCallback);
    }
    
    public Handler createWorkHandler(Context param1Context, String param1String, int param1Int) {
      return onCreateWorkHandler(param1Context, param1String, param1Int);
    }
    
    public boolean isSettingOff(Context param1Context) {
      return isSettingOn(param1Context) ^ true;
    }
    
    public void onSaveResult(Context param1Context, AnteaterClient.ResultData param1ResultData) {
      onHandleFinished(param1Context, param1ResultData);
    }
  }
  
  private static class AnteaterProcessCallback extends AnteaterCallback implements AnteaterClient.ProcessCallback {
    public AnteaterProcessCallback(OplusFavoriteCallback param1OplusFavoriteCallback) {
      super(param1OplusFavoriteCallback);
    }
    
    public Handler createWorkHandler(Context param1Context, String param1String, int param1Int) {
      return onCreateWorkHandler(param1Context, param1String, param1Int);
    }
    
    public boolean isSettingOff(Context param1Context) {
      return isSettingOn(param1Context) ^ true;
    }
    
    public void onProcessResult(Context param1Context, AnteaterClient.ResultData param1ResultData) {
      onHandleFinished(param1Context, param1ResultData);
    }
  }
}
