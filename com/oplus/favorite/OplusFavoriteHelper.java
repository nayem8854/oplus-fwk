package com.oplus.favorite;

import android.content.Context;
import android.net.Uri;
import android.os.RemoteException;
import android.view.OplusWindowManager;
import com.oplus.direct.IOplusDirectFindCallback;
import com.oplus.direct.OplusDirectFindCmd;
import com.oplus.direct.OplusDirectFindCmds;
import com.oplus.util.OplusLog;

public class OplusFavoriteHelper implements IOplusFavoriteConstans {
  private static final String[] SELECTION_ARGS = new String[] { "favorite_all" };
  
  private static final String SETTINGS_AUTHORITY = "com.coloros.favorite.settings.provider";
  
  private static final String SETTINGS_PATH = "settings";
  
  private static final Uri SETTINGS_URI;
  
  private static final String SETTING_KEY_ALL = "favorite_all";
  
  public static final String TAG = "OplusFavoriteHelper";
  
  static {
    Uri.Builder builder = new Uri.Builder();
    builder = builder.scheme("content");
    builder = builder.authority("com.coloros.favorite.settings.provider");
    builder = builder.path("settings");
    SETTINGS_URI = builder.build();
  }
  
  public static void startCrawl(IOplusDirectFindCallback paramIOplusDirectFindCallback) {
    try {
      OplusLog.d("OplusFavoriteHelper", "startCrawl");
      OplusWindowManager oplusWindowManager = new OplusWindowManager();
      this();
      OplusDirectFindCmd oplusDirectFindCmd = new OplusDirectFindCmd();
      this();
      oplusDirectFindCmd.putCommand(OplusDirectFindCmds.FIND_FAVORITE.name());
      oplusDirectFindCmd.setCallback(paramIOplusDirectFindCallback);
      oplusWindowManager.directFindCmd(oplusDirectFindCmd);
    } catch (RemoteException remoteException) {
      OplusLog.wtf("OplusFavoriteHelper", (Throwable)remoteException);
    } catch (Exception exception) {
      OplusLog.wtf("OplusFavoriteHelper", exception);
    } 
  }
  
  public static void startSave(IOplusDirectFindCallback paramIOplusDirectFindCallback) {
    try {
      OplusLog.d("OplusFavoriteHelper", "startSave");
      OplusWindowManager oplusWindowManager = new OplusWindowManager();
      this();
      OplusDirectFindCmd oplusDirectFindCmd = new OplusDirectFindCmd();
      this();
      oplusDirectFindCmd.putCommand(OplusDirectFindCmds.SAVE_FAVORITE.name());
      oplusDirectFindCmd.setCallback(paramIOplusDirectFindCallback);
      oplusWindowManager.directFindCmd(oplusDirectFindCmd);
    } catch (RemoteException remoteException) {
      OplusLog.wtf("OplusFavoriteHelper", (Throwable)remoteException);
    } catch (Exception exception) {
      OplusLog.wtf("OplusFavoriteHelper", exception);
    } 
  }
  
  public static boolean isSettingOn(Context paramContext) {
    return true;
  }
}
