package com.oplus.favorite;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.oplus.util.OplusLog;

public class OplusFavoriteHandler extends Handler {
  private static final boolean DBG = false;
  
  private final HandlerThread mThread;
  
  public OplusFavoriteHandler(HandlerThread paramHandlerThread, String paramString) {
    super(getLooper(paramHandlerThread));
    this.mThread = paramHandlerThread;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("<init>() : ");
    stringBuilder.append(this.mThread.getName());
    stringBuilder.append(" @ ");
    stringBuilder.append(paramString);
    OplusLog.i(false, "AnteaterFavorite", stringBuilder.toString());
  }
  
  public boolean quit() {
    return this.mThread.quit();
  }
  
  private static Looper getLooper(HandlerThread paramHandlerThread) {
    Looper looper = paramHandlerThread.getLooper();
    if (looper != null)
      return looper; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Looper is null of ");
    stringBuilder.append(paramHandlerThread.getName());
    stringBuilder.append("[");
    stringBuilder.append(paramHandlerThread.getThreadId());
    stringBuilder.append("]");
    throw new IllegalThreadStateException(stringBuilder.toString());
  }
}
