package com.oplus.favorite;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.View;

public interface IOplusFavoriteEngine extends IOplusFavoriteConstans {
  Handler getWorkHandler();
  
  void init();
  
  boolean isDebugOn();
  
  boolean isLogOn();
  
  boolean isTestOn();
  
  void loadRule(Context paramContext, String paramString, OplusFavoriteCallback paramOplusFavoriteCallback);
  
  void logActivityInfo(Activity paramActivity);
  
  void logViewInfo(View paramView);
  
  void processClick(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback);
  
  void processCrawl(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback);
  
  void processSave(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback);
  
  void release();
}
