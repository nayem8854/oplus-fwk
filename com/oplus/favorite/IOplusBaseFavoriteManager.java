package com.oplus.favorite;

import android.app.Activity;
import android.common.IOplusCommonFeature;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import java.util.List;

public interface IOplusBaseFavoriteManager extends IOplusFavoriteConstans, IOplusCommonFeature {
  public static final String NAME = "OplusFavoriteManager";
  
  Handler getWorkHandler();
  
  void init(Context paramContext);
  
  boolean isSaved(Context paramContext, String paramString, List<Bundle> paramList);
  
  void logActivityInfo(Activity paramActivity);
  
  void logViewInfo(View paramView);
  
  void processClick(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback);
  
  void processCrawl(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback);
  
  void processSave(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback);
  
  void release();
  
  void setEngine(OplusFavoriteEngines paramOplusFavoriteEngines);
}
