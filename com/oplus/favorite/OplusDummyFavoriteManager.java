package com.oplus.favorite;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import java.util.List;

public class OplusDummyFavoriteManager implements IOplusFavoriteManager {
  private static final boolean DEBUG = true;
  
  private static final String TAG = "OplusDummyFavoriteManager";
  
  private static volatile OplusDummyFavoriteManager sInstance = null;
  
  public static OplusDummyFavoriteManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/favorite/OplusDummyFavoriteManager.sInstance : Lcom/oplus/favorite/OplusDummyFavoriteManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/favorite/OplusDummyFavoriteManager
    //   8: monitorenter
    //   9: getstatic com/oplus/favorite/OplusDummyFavoriteManager.sInstance : Lcom/oplus/favorite/OplusDummyFavoriteManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/favorite/OplusDummyFavoriteManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/favorite/OplusDummyFavoriteManager.sInstance : Lcom/oplus/favorite/OplusDummyFavoriteManager;
    //   27: ldc com/oplus/favorite/OplusDummyFavoriteManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/favorite/OplusDummyFavoriteManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/favorite/OplusDummyFavoriteManager.sInstance : Lcom/oplus/favorite/OplusDummyFavoriteManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #35	-> 0
    //   #36	-> 6
    //   #37	-> 9
    //   #38	-> 15
    //   #40	-> 27
    //   #42	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public void init(Context paramContext) {}
  
  public void release() {}
  
  public void processClick(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback) {}
  
  public void processCrawl(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback) {}
  
  public void processSave(View paramView, OplusFavoriteCallback paramOplusFavoriteCallback) {}
  
  public void logActivityInfo(Activity paramActivity) {}
  
  public void logViewInfo(View paramView) {}
  
  public Handler getWorkHandler() {
    return null;
  }
  
  public void setEngine(OplusFavoriteEngines paramOplusFavoriteEngines) {}
  
  public boolean isSaved(Context paramContext, String paramString, List<Bundle> paramList) {
    return false;
  }
}
