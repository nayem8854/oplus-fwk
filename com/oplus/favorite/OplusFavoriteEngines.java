package com.oplus.favorite;

public enum OplusFavoriteEngines {
  TEDDY, UNKNOWN;
  
  private static final OplusFavoriteEngines[] $VALUES;
  
  static {
    OplusFavoriteEngines oplusFavoriteEngines = new OplusFavoriteEngines("UNKNOWN", 1);
    $VALUES = new OplusFavoriteEngines[] { TEDDY, oplusFavoriteEngines };
  }
}
