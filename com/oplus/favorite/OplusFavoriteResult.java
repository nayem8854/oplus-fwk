package com.oplus.favorite;

import java.util.ArrayList;

public class OplusFavoriteResult {
  public static final String ERROR_NOT_FOUND = "Not found";
  
  public static final String ERROR_NOT_INIT = "Not init";
  
  public static final String ERROR_NO_VIEW = "No view";
  
  public static final String ERROR_SAVE_FAILED = "Save failed";
  
  public static final String ERROR_SETTING_OFF = "Setting off";
  
  public static final String ERROR_UNSUPPORT = "Unsupported";
  
  private final ArrayList<OplusFavoriteData> mData = new ArrayList<>();
  
  private String mError = null;
  
  private String mPackageName = null;
  
  public void setData(ArrayList<OplusFavoriteData> paramArrayList) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mData : Ljava/util/ArrayList;
    //   4: astore_2
    //   5: aload_2
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield mData : Ljava/util/ArrayList;
    //   11: invokevirtual clear : ()V
    //   14: aload_1
    //   15: ifnull -> 39
    //   18: aload_1
    //   19: monitorenter
    //   20: aload_0
    //   21: getfield mData : Ljava/util/ArrayList;
    //   24: aload_1
    //   25: invokevirtual addAll : (Ljava/util/Collection;)Z
    //   28: pop
    //   29: aload_1
    //   30: monitorexit
    //   31: goto -> 39
    //   34: astore_3
    //   35: aload_1
    //   36: monitorexit
    //   37: aload_3
    //   38: athrow
    //   39: aload_2
    //   40: monitorexit
    //   41: return
    //   42: astore_1
    //   43: aload_2
    //   44: monitorexit
    //   45: aload_1
    //   46: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #48	-> 0
    //   #49	-> 7
    //   #50	-> 14
    //   #51	-> 18
    //   #52	-> 20
    //   #53	-> 29
    //   #55	-> 39
    //   #56	-> 41
    //   #55	-> 42
    // Exception table:
    //   from	to	target	type
    //   7	14	42	finally
    //   18	20	42	finally
    //   20	29	34	finally
    //   29	31	34	finally
    //   35	37	34	finally
    //   37	39	42	finally
    //   39	41	42	finally
    //   43	45	42	finally
  }
  
  public void setError(String paramString) {
    this.mError = paramString;
  }
  
  public void setPackageName(String paramString) {
    this.mPackageName = paramString;
  }
  
  public ArrayList<OplusFavoriteData> getData() {
    synchronized (this.mData) {
      return this.mData;
    } 
  }
  
  public String getError() {
    return this.mError;
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
}
