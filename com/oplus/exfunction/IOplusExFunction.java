package com.oplus.exfunction;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusExFunction extends IInterface {
  int endRepairMode() throws RemoteException;
  
  int startRepairMode() throws RemoteException;
  
  class Default implements IOplusExFunction {
    public int startRepairMode() throws RemoteException {
      return 0;
    }
    
    public int endRepairMode() throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusExFunction {
    private static final String DESCRIPTOR = "com.oplus.exfunction.IOplusExFunction";
    
    static final int TRANSACTION_endRepairMode = 2;
    
    static final int TRANSACTION_startRepairMode = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.exfunction.IOplusExFunction");
    }
    
    public static IOplusExFunction asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.exfunction.IOplusExFunction");
      if (iInterface != null && iInterface instanceof IOplusExFunction)
        return (IOplusExFunction)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "endRepairMode";
      } 
      return "startRepairMode";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("com.oplus.exfunction.IOplusExFunction");
          return true;
        } 
        param1Parcel1.enforceInterface("com.oplus.exfunction.IOplusExFunction");
        param1Int1 = endRepairMode();
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.exfunction.IOplusExFunction");
      param1Int1 = startRepairMode();
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(param1Int1);
      return true;
    }
    
    private static class Proxy implements IOplusExFunction {
      public static IOplusExFunction sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.exfunction.IOplusExFunction";
      }
      
      public int startRepairMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.exfunction.IOplusExFunction");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusExFunction.Stub.getDefaultImpl() != null)
            return IOplusExFunction.Stub.getDefaultImpl().startRepairMode(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int endRepairMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.exfunction.IOplusExFunction");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusExFunction.Stub.getDefaultImpl() != null)
            return IOplusExFunction.Stub.getDefaultImpl().endRepairMode(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusExFunction param1IOplusExFunction) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusExFunction != null) {
          Proxy.sDefaultImpl = param1IOplusExFunction;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusExFunction getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
