package com.oplus.exfunction;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class ExFunctionManager {
  public static final int REPAIR_MODE_BIND_SERVICE_ERROR = -4;
  
  public static final int REPAIR_MODE_CREATE_USER_ERROR = -3;
  
  public static final int REPAIR_MODE_END_ERROR = -6;
  
  public static final int REPAIR_MODE_END_IOPLUS_ERROR = -8;
  
  public static final int REPAIR_MODE_FLAG_USERID_OTHER = 3;
  
  public static final int REPAIR_MODE_FLAG_USERID_OWNER = 1;
  
  public static final int REPAIR_MODE_FLAG_USERID_REPAIR = 2;
  
  public static final int REPAIR_MODE_REBOOT_ERROR = -2;
  
  public static final int REPAIR_MODE_START_ERROR = -5;
  
  public static final int REPAIR_MODE_START_IOPLUS_ERROR = -7;
  
  public static final int REPAIR_MODE_SUCCESS = 0;
  
  public static final int REPAIR_MODE_WRITE_ERROR = -1;
  
  private static final String TAG = "ExFunctionManager";
  
  public static final int USER_FLAG_REPAIR_MODE = 134217728;
  
  public static final int USER_ID_REPAIR_MODE = 888;
  
  public int startRepairMode(Context paramContext) {
    ServiceConnectionWrapper serviceConnectionWrapper = bindRepairService(paramContext);
    if (serviceConnectionWrapper == null)
      return -4; 
    try {
      int i = serviceConnectionWrapper.startRepairMode();
      if (i == -1)
        return -1; 
      if (i == -2)
        return -2; 
      if (i == -3)
        return -3; 
      if (i == -7)
        return -7; 
    } catch (RemoteException remoteException) {
      Log.e("ExFunctionManager", "startRepairMode: ", (Throwable)remoteException);
    } 
    paramContext.unbindService(serviceConnectionWrapper);
    return -5;
  }
  
  public int endRepairMode(Context paramContext) {
    ServiceConnectionWrapper serviceConnectionWrapper = bindRepairService(paramContext);
    if (serviceConnectionWrapper == null)
      return -4; 
    try {
      int i = serviceConnectionWrapper.endRepairMode();
      if (i == -1)
        return -1; 
      if (i == -2)
        return -2; 
      if (i == -3)
        return -3; 
      if (i == -8)
        return -8; 
      paramContext.unbindService(serviceConnectionWrapper);
      return -6;
    } catch (RemoteException remoteException) {
      Log.e("ExFunctionManager", "endRepairMode: ", (Throwable)remoteException);
      return -6;
    } 
  }
  
  public boolean isEnableRepairMode() {
    return true;
  }
  
  public int getRepairModeFlag(Context paramContext) {
    int i = paramContext.getUserId();
    if (i == 0)
      return 1; 
    if (i == 888)
      return 2; 
    return 3;
  }
  
  class ServiceConnectionWrapper implements ServiceConnection {
    CountDownLatch countDownLatch = new CountDownLatch(1);
    
    IOplusExFunction iOplusExFunction;
    
    final ExFunctionManager this$0;
    
    boolean waitForFinish(long param1Long) {
      try {
        return this.countDownLatch.await(param1Long, TimeUnit.MILLISECONDS);
      } catch (InterruptedException interruptedException) {
        Log.e("ExFunctionManager", "waitForFinish: ", interruptedException);
        return false;
      } 
    }
    
    public void onServiceConnected(ComponentName param1ComponentName, IBinder param1IBinder) {
      this.iOplusExFunction = IOplusExFunction.Stub.asInterface(param1IBinder);
      this.countDownLatch.countDown();
    }
    
    public void onServiceDisconnected(ComponentName param1ComponentName) {
      this.iOplusExFunction = null;
    }
    
    int startRepairMode() throws RemoteException {
      IOplusExFunction iOplusExFunction = this.iOplusExFunction;
      if (iOplusExFunction != null)
        return iOplusExFunction.startRepairMode(); 
      return -7;
    }
    
    int endRepairMode() throws RemoteException {
      IOplusExFunction iOplusExFunction = this.iOplusExFunction;
      if (iOplusExFunction != null)
        return iOplusExFunction.endRepairMode(); 
      return -8;
    }
  }
  
  private ServiceConnectionWrapper bindRepairService(Context paramContext) {
    Intent intent = new Intent();
    intent.setClassName("com.coloros.exsystemservice", "com.color.exfunction.ExFunctionService");
    ServiceConnectionWrapper serviceConnectionWrapper = new ServiceConnectionWrapper();
    boolean bool = paramContext.bindService(intent, serviceConnectionWrapper, 1);
    if (!bool)
      return null; 
    if (!serviceConnectionWrapper.waitForFinish(10000L))
      return null; 
    return serviceConnectionWrapper;
  }
}
