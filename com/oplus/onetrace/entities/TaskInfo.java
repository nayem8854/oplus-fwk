package com.oplus.onetrace.entities;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TaskInfo implements Parcelable {
  public static final Parcelable.Creator<TaskInfo> CREATOR = new Parcelable.Creator<TaskInfo>() {
      public TaskInfo createFromParcel(Parcel param1Parcel) {
        return new TaskInfo(param1Parcel);
      }
      
      public TaskInfo[] newArray(int param1Int) {
        return new TaskInfo[param1Int];
      }
    };
  
  private final String mTaskName;
  
  private List<TaskInfo> mThreadTasks;
  
  private final int mTid;
  
  public TaskInfo(int paramInt, String paramString) {
    this.mTid = paramInt;
    this.mTaskName = paramString;
  }
  
  public TaskInfo(Parcel paramParcel) {
    ArrayList<TaskInfo> arrayList1;
    this.mTid = paramParcel.readInt();
    this.mTaskName = paramParcel.readString();
    ArrayList<TaskInfo> arrayList2 = new ArrayList();
    paramParcel.readParcelableList(arrayList2, TaskInfo.class.getClassLoader());
    if (arrayList2.isEmpty()) {
      paramParcel = null;
    } else {
      arrayList1 = arrayList2;
    } 
    this.mThreadTasks = arrayList1;
  }
  
  public int getTaskId() {
    return this.mTid;
  }
  
  public String getTaskName() {
    return this.mTaskName;
  }
  
  public List<TaskInfo> getThreadTasks() {
    if (this.mThreadTasks == null)
      return null; 
    return new ArrayList<>(this.mThreadTasks);
  }
  
  public int getThreadCount() {
    int i;
    List<TaskInfo> list = this.mThreadTasks;
    if (list == null) {
      i = 0;
    } else {
      i = list.size();
    } 
    return i;
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder("TaskInfo{ taskId=");
    int i = this.mTid;
    stringBuilder1.append(i);
    stringBuilder1.append(", cmdline=");
    String str = this.mTaskName;
    StringBuilder stringBuilder2 = stringBuilder1.append(str);
    if (this.mThreadTasks != null) {
      stringBuilder2.append(", threadTasks=");
      stringBuilder2.append(this.mThreadTasks);
    } 
    stringBuilder2.append(" }");
    return stringBuilder2.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mTid);
    paramParcel.writeString(this.mTaskName);
    paramParcel.writeParcelableList(this.mThreadTasks, paramInt);
  }
  
  public void addAllThreadTasks(Map<Integer, String> paramMap) {
    if (paramMap == null)
      return; 
    Stream stream2 = paramMap.entrySet().stream();
    -$.Lambda.TaskInfo._sqfhFPBViVYbewn9Ht2LRqiYmY _sqfhFPBViVYbewn9Ht2LRqiYmY = _$$Lambda$TaskInfo$_sqfhFPBViVYbewn9Ht2LRqiYmY.INSTANCE;
    Stream stream1 = stream2.map((Function)_sqfhFPBViVYbewn9Ht2LRqiYmY);
    this.mThreadTasks = (List<TaskInfo>)stream1.collect(Collectors.toList());
  }
}
