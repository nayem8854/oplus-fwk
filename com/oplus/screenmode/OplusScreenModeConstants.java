package com.oplus.screenmode;

public class OplusScreenModeConstants {
  public static final int DELAY_ONE_SECOND = 1000;
  
  public static final int FINGERPRINT_TYPE = 2315;
  
  public static final int NO_INITIALIZED_DENSITY = 100;
  
  public static final int RATE_VAL_120 = 120;
  
  public static final int RATE_VAL_144 = 144;
  
  public static final int RATE_VAL_60 = 60;
  
  public static final int RATE_VAL_90 = 90;
  
  public static final float RATE_VAL_PRECISION = 0.1F;
  
  public static final int REFRESH_RATE_ID_120 = 3;
  
  public static final int REFRESH_RATE_ID_144 = 4;
  
  public static final int REFRESH_RATE_ID_60 = 2;
  
  public static final int REFRESH_RATE_ID_90 = 1;
  
  public static final int REFRESH_RATE_ID_AUTO = 0;
  
  public static final int REFRESH_RATE_ID_HIGH = 7;
  
  public static final int REFRESH_RATE_ID_INVALID = -1;
  
  public static final int RESOLUTION_AUTO = 1;
  
  public static final int RESOLUTION_BIT_SHIFT = 4;
  
  public static final int RESOLUTION_HIGH = 3;
  
  public static final int RESOLUTION_INVALID = -1;
  
  public static final int RESOLUTION_LOW = 2;
  
  public static final int VERSION_LEN = 8;
  
  public static final int WIDTH_2K = 1440;
  
  public static final int WIDTH_FHD = 1080;
}
