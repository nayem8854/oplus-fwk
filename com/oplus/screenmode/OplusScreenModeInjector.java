package com.oplus.screenmode;

import android.common.OplusFrameworkFactory;
import android.content.Context;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import com.oplus.content.OplusFeatureConfigManager;

@Deprecated
public class OplusScreenModeInjector {
  public static final int DEFAULT_RATE = 0;
  
  public static final int RATE_120 = 3;
  
  public static final int RATE_60 = 2;
  
  public static final int RATE_90 = 1;
  
  public static final int RATE_AUTO = 0;
  
  private static final String TAG = "ScreenMode";
  
  private static IOplusScreenModeFeature mIScreenModeFeature;
  
  private static boolean sInitialized;
  
  private static boolean sScreenRateSupport = false;
  
  static {
    sInitialized = false;
    mIScreenModeFeature = null;
  }
  
  public static void init(Context paramContext) {
    if (sInitialized)
      return; 
    if (paramContext == null) {
      Log.e("ScreenMode", "failed to init for context null!");
      return;
    } 
    if (OplusFeatureConfigManager.getInstacne().hasFeature("oplus.software.display.dynamic_fps_switch")) {
      sScreenRateSupport = true;
    } else {
      sScreenRateSupport = false;
    } 
    OplusFrameworkFactory oplusFrameworkFactory = OplusFrameworkFactory.getInstance();
    IOplusScreenModeFeature iOplusScreenModeFeature = IOplusScreenModeFeature.DEFAULT;
    mIScreenModeFeature = (IOplusScreenModeFeature)oplusFrameworkFactory.getFeature(iOplusScreenModeFeature, new Object[] { paramContext });
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("init OplusScreenModeInjector, sScreenRateSupport:");
    stringBuilder.append(sScreenRateSupport);
    Log.d("ScreenMode", stringBuilder.toString());
    if (mIScreenModeFeature != null) {
      sInitialized = true;
    } else {
      sInitialized = false;
    } 
  }
  
  public static void setRefreshRate(IBinder paramIBinder, int paramInt) {
    if (!sInitialized) {
      Log.e("ScreenMode", "setRefreshRate: failed for not init! Must call init(Context context) before.");
      return;
    } 
    if (!sScreenRateSupport)
      return; 
    IOplusScreenModeFeature iOplusScreenModeFeature = mIScreenModeFeature;
    if (iOplusScreenModeFeature != null)
      iOplusScreenModeFeature.setRefreshRate(paramIBinder, paramInt); 
  }
  
  public static void setRefreshRate(View paramView, int paramInt) {
    if (!sInitialized) {
      Log.e("ScreenMode", "setRefreshRate: failed for not init! Must call init(Context context) before.");
      return;
    } 
    if (!sScreenRateSupport)
      return; 
    IOplusScreenModeFeature iOplusScreenModeFeature = mIScreenModeFeature;
    if (iOplusScreenModeFeature != null)
      iOplusScreenModeFeature.setRefreshRate(paramView, paramInt); 
  }
  
  public static boolean requestRefreshRate(boolean paramBoolean, int paramInt) {
    boolean bool1 = sInitialized, bool2 = false;
    if (!bool1) {
      Log.e("ScreenMode", "setRefreshRate: failed for not init! Must call init(Context context) before.");
      return false;
    } 
    if (!sScreenRateSupport)
      return false; 
    IOplusScreenModeFeature iOplusScreenModeFeature = mIScreenModeFeature;
    if (iOplusScreenModeFeature != null)
      bool2 = iOplusScreenModeFeature.requestRefreshRate(paramBoolean, paramInt); 
    return bool2;
  }
}
