package com.oplus.screenmode;

public class OplusAppModeInfo {
  private static final String TAG = "ScreenMode";
  
  private String mActivityName;
  
  private boolean mAppRequestFirst;
  
  private boolean mDisableAnimHighRate;
  
  private boolean mIsGame;
  
  private boolean mOnTpList;
  
  private String mPkgName;
  
  private int[] mRateIDS;
  
  private boolean mSTViewDisable;
  
  private int mSTViewRate;
  
  private int mSeqNum;
  
  private String mViewList;
  
  public OplusAppModeInfo(String paramString1, String paramString2) {
    this.mPkgName = "";
    this.mActivityName = "";
    this.mRateIDS = new int[0];
    this.mSTViewRate = 0;
    this.mSTViewDisable = false;
    this.mIsGame = false;
    this.mAppRequestFirst = false;
    this.mOnTpList = false;
    this.mDisableAnimHighRate = false;
    this.mViewList = "";
    this.mSeqNum = -1;
    this.mPkgName = paramString1;
    this.mActivityName = paramString2;
  }
  
  public OplusAppModeInfo(OplusAppModeInfo paramOplusAppModeInfo) {
    this.mPkgName = "";
    this.mActivityName = "";
    this.mRateIDS = new int[0];
    this.mSTViewRate = 0;
    this.mSTViewDisable = false;
    this.mIsGame = false;
    this.mAppRequestFirst = false;
    this.mOnTpList = false;
    this.mDisableAnimHighRate = false;
    this.mViewList = "";
    this.mSeqNum = -1;
    this.mPkgName = paramOplusAppModeInfo.mPkgName;
    this.mActivityName = paramOplusAppModeInfo.mActivityName;
    this.mRateIDS = new int[paramOplusAppModeInfo.mRateIDS.length];
    byte b = 0;
    while (true) {
      int[] arrayOfInt = this.mRateIDS;
      if (b < arrayOfInt.length) {
        arrayOfInt[b] = paramOplusAppModeInfo.mRateIDS[b];
        b++;
        continue;
      } 
      break;
    } 
    this.mSTViewDisable = paramOplusAppModeInfo.mSTViewDisable;
    this.mIsGame = paramOplusAppModeInfo.mIsGame;
    this.mAppRequestFirst = paramOplusAppModeInfo.mAppRequestFirst;
    this.mOnTpList = paramOplusAppModeInfo.mOnTpList;
    this.mViewList = paramOplusAppModeInfo.mViewList;
    this.mDisableAnimHighRate = paramOplusAppModeInfo.mDisableAnimHighRate;
  }
  
  public OplusAppModeInfo(String paramString1, String paramString2, String paramString3, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, String paramString4, boolean paramBoolean5) {
    String str = "";
    this.mPkgName = "";
    this.mActivityName = "";
    this.mRateIDS = new int[0];
    this.mSTViewRate = 0;
    this.mSTViewDisable = false;
    this.mIsGame = false;
    this.mAppRequestFirst = false;
    this.mOnTpList = false;
    this.mDisableAnimHighRate = false;
    this.mViewList = "";
    this.mSeqNum = -1;
    if (paramString1 == null)
      paramString1 = ""; 
    this.mPkgName = paramString1;
    paramString1 = str;
    if (paramString2 != null)
      paramString1 = paramString2; 
    this.mActivityName = paramString1;
    if (paramString3 != null && paramString3.length() > 0) {
      String[] arrayOfString = paramString3.split("-");
      this.mRateIDS = new int[arrayOfString.length];
      for (byte b = 0; b < arrayOfString.length; b++)
        this.mRateIDS[b] = Integer.parseInt(arrayOfString[b]); 
    } 
    this.mSTViewDisable = paramBoolean1;
    this.mIsGame = paramBoolean2;
    this.mAppRequestFirst = paramBoolean3;
    this.mOnTpList = paramBoolean4;
    this.mDisableAnimHighRate = paramBoolean5;
    if (paramString4 != null)
      this.mViewList = paramString4; 
  }
  
  public boolean getDisableAnimHighRate() {
    return this.mDisableAnimHighRate;
  }
  
  public void updateFrom(OplusAppModeInfo paramOplusAppModeInfo, int paramInt) {
    int i = paramOplusAppModeInfo.mRateIDS.length;
    this.mRateIDS = new int[i];
    for (byte b = 0; b < i; b++)
      this.mRateIDS[b] = paramOplusAppModeInfo.mRateIDS[b]; 
    this.mSTViewDisable = paramOplusAppModeInfo.mSTViewDisable;
    this.mIsGame = paramOplusAppModeInfo.mIsGame;
    this.mAppRequestFirst = paramOplusAppModeInfo.mAppRequestFirst;
    this.mOnTpList = paramOplusAppModeInfo.mOnTpList;
    this.mViewList = paramOplusAppModeInfo.mViewList;
    this.mDisableAnimHighRate = paramOplusAppModeInfo.mDisableAnimHighRate;
    this.mSeqNum = paramInt;
  }
  
  public boolean isWhiteListChange(int paramInt) {
    boolean bool;
    if (this.mSeqNum != paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void updateSequenece(int paramInt) {
    this.mSeqNum = paramInt;
  }
  
  public String getPkgName() {
    return this.mPkgName;
  }
  
  public String getActName() {
    return this.mActivityName;
  }
  
  public void setSTViewRate(int paramInt) {
    this.mSTViewRate = paramInt;
  }
  
  public int getSTViewRate() {
    return this.mSTViewRate;
  }
  
  public boolean stViewDisable() {
    return this.mSTViewDisable;
  }
  
  public boolean getIsGame() {
    return this.mIsGame;
  }
  
  public boolean appRequestFirst() {
    return this.mAppRequestFirst;
  }
  
  public boolean onTpList() {
    return this.mOnTpList;
  }
  
  public String viewList() {
    return this.mViewList;
  }
  
  public int getRate(int paramInt1, int paramInt2) {
    if (!this.mSTViewDisable) {
      int i = this.mSTViewRate;
      if (i != 0)
        return i; 
    } 
    int[] arrayOfInt = this.mRateIDS;
    if (paramInt1 < arrayOfInt.length && paramInt1 >= 0)
      return arrayOfInt[paramInt1]; 
    if (paramInt1 == 3)
      return 1; 
    return paramInt2;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("pkg=");
    stringBuilder.append(this.mPkgName);
    stringBuilder.append(",window=");
    stringBuilder.append(this.mActivityName);
    stringBuilder.append(",rate [");
    int[] arrayOfInt = this.mRateIDS;
    if (arrayOfInt.length > 0)
      stringBuilder.append(arrayOfInt[0]); 
    for (byte b = 1; b < this.mRateIDS.length; b++) {
      stringBuilder.append("-");
      stringBuilder.append(this.mRateIDS[b]);
    } 
    stringBuilder.append("]");
    stringBuilder.append(",view disable=");
    stringBuilder.append(this.mSTViewDisable);
    stringBuilder.append(",view rate=");
    stringBuilder.append(this.mSTViewRate);
    stringBuilder.append(",isGame=");
    stringBuilder.append(this.mIsGame);
    if (this.mAppRequestFirst) {
      stringBuilder.append(",appRequestFirst=");
      stringBuilder.append(this.mAppRequestFirst);
    } 
    if (this.mOnTpList) {
      stringBuilder.append(",onTpList=");
      stringBuilder.append(this.mOnTpList);
    } 
    stringBuilder.append(",seq");
    stringBuilder.append(this.mSeqNum);
    if (this.mDisableAnimHighRate) {
      stringBuilder.append(",disAnimHighRate=");
      stringBuilder.append(this.mDisableAnimHighRate);
    } 
    stringBuilder.append(",");
    stringBuilder.append(this.mViewList);
    return stringBuilder.toString();
  }
}
