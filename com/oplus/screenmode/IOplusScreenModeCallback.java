package com.oplus.screenmode;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusScreenModeCallback extends IInterface {
  void requestRefreshRate(String paramString, int paramInt) throws RemoteException;
  
  class Default implements IOplusScreenModeCallback {
    public void requestRefreshRate(String param1String, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusScreenModeCallback {
    private static final String DESCRIPTOR = "com.oplus.screenmode.IOplusScreenModeCallback";
    
    static final int TRANSACTION_requestRefreshRate = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.screenmode.IOplusScreenModeCallback");
    }
    
    public static IOplusScreenModeCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.screenmode.IOplusScreenModeCallback");
      if (iInterface != null && iInterface instanceof IOplusScreenModeCallback)
        return (IOplusScreenModeCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "requestRefreshRate";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.screenmode.IOplusScreenModeCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.screenmode.IOplusScreenModeCallback");
      String str = param1Parcel1.readString();
      param1Int1 = param1Parcel1.readInt();
      requestRefreshRate(str, param1Int1);
      return true;
    }
    
    private static class Proxy implements IOplusScreenModeCallback {
      public static IOplusScreenModeCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.screenmode.IOplusScreenModeCallback";
      }
      
      public void requestRefreshRate(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.screenmode.IOplusScreenModeCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusScreenModeCallback.Stub.getDefaultImpl() != null) {
            IOplusScreenModeCallback.Stub.getDefaultImpl().requestRefreshRate(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusScreenModeCallback param1IOplusScreenModeCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusScreenModeCallback != null) {
          Proxy.sDefaultImpl = param1IOplusScreenModeCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusScreenModeCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
