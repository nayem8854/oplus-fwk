package com.oplus.screenmode;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.OplusApplicationInfoEx;
import android.content.res.CompatibilityInfo;
import android.graphics.Point;
import android.hardware.display.DisplayManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Debug;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.DisplayInfo;
import android.view.View;
import android.view.ViewGroup;
import com.android.internal.view.RootViewSurfaceTaker;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class OplusScreenModeFeature implements IOplusScreenModeFeature {
  private static final boolean mOplusDebug = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  static {
    DEBUG = SystemProperties.getBoolean("persist.sys.compat.debug", false);
    sScreenRateSupport = false;
    sResolutionSupport = false;
    sIsDisplayCompatApp = false;
    sCompatDensity = 420;
  }
  
  private float mAppScale = 1.0F;
  
  private float mAppInvScale = 1.0F;
  
  private IOplusScreenMode mScreenModeService = null;
  
  private Binder mToken = null;
  
  private HashMap<IBinder, ArrayList<View>> mSTViewTokenMap = null;
  
  private int mInitializeStatus = 1;
  
  private boolean mIsSystem = false;
  
  private String mPackageName = "";
  
  private final View.OnAttachStateChangeListener mAttachStateChangeListener = new View.OnAttachStateChangeListener() {
      final OplusScreenModeFeature this$0;
      
      public void onViewAttachedToWindow(View param1View) {}
      
      public void onViewDetachedFromWindow(View param1View) {
        try {
          OplusScreenModeFeature.this.clearInvalidView();
          param1View.removeOnAttachStateChangeListener(OplusScreenModeFeature.this.mAttachStateChangeListener);
        } catch (Exception exception) {}
      }
    };
  
  public static final boolean DEBUG;
  
  public static final int DEBUG_CALLER_DEPTH = 8;
  
  private static final int INITIALIZE_END = 4;
  
  private static final int INIT_DELAY_TIMEOUT = 100;
  
  private static final int INIT_POST_MESSAGE = 3;
  
  private static final int INIT_RATE_SUPPORT = 2;
  
  private static final int INIT_UNKNOW = 1;
  
  private static final int MAX_STVIEW_LIST_SIZE = 3;
  
  private static final int MIN_SWITCH_SIZE = 2;
  
  private static final String OPLUS_SCREENMODE_SERVICE_NAME = "opposcreenmode";
  
  private static final int SEARCH_DEPTH = 20;
  
  private static final String SYSTEMUI_PKG = "com.android.systemui";
  
  private static final String TAG = "ScreenmodeClient";
  
  private static int sCompatDensity;
  
  private static boolean sIsDisplayCompatApp;
  
  private static boolean sResolutionSupport;
  
  private static boolean sScreenRateSupport;
  
  private OplusScreenModeFeature() {
    if (DEBUG)
      Log.d("ScreenmodeClient", "OplusScreenModeFeature create"); 
  }
  
  private static class InstanceHolder {
    static final OplusScreenModeFeature INSTANCE = new OplusScreenModeFeature();
  }
  
  public static OplusScreenModeFeature getInstance() {
    return InstanceHolder.INSTANCE;
  }
  
  public void init(Context paramContext) {
    // Byte code:
    //   0: getstatic com/oplus/screenmode/OplusScreenModeFeature.DEBUG : Z
    //   3: ifeq -> 15
    //   6: ldc 'ScreenmodeClient'
    //   8: ldc_w 'OplusScreenModeFeature init.'
    //   11: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   14: pop
    //   15: aload_1
    //   16: ifnonnull -> 29
    //   19: ldc 'ScreenmodeClient'
    //   21: ldc_w 'OplusScreenModeFeature init failed for context empty!'
    //   24: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   27: pop
    //   28: return
    //   29: aload_0
    //   30: monitorenter
    //   31: aload_0
    //   32: getfield mInitializeStatus : I
    //   35: iconst_4
    //   36: if_icmpne -> 42
    //   39: aload_0
    //   40: monitorexit
    //   41: return
    //   42: aload_0
    //   43: getfield mInitializeStatus : I
    //   46: iconst_2
    //   47: if_icmpge -> 55
    //   50: aload_0
    //   51: aload_1
    //   52: invokespecial initSupportMode : (Landroid/content/Context;)V
    //   55: aload_0
    //   56: iconst_4
    //   57: putfield mInitializeStatus : I
    //   60: getstatic com/oplus/screenmode/OplusScreenModeFeature.sResolutionSupport : Z
    //   63: ifne -> 78
    //   66: invokestatic getInstacne : ()Lcom/oplus/content/OplusFeatureConfigManager;
    //   69: ldc_w 'oplus.software.app_resolution_switch'
    //   72: invokevirtual hasFeature : (Ljava/lang/String;)Z
    //   75: putstatic com/oplus/screenmode/OplusScreenModeFeature.sResolutionSupport : Z
    //   78: getstatic com/oplus/screenmode/OplusScreenModeFeature.sResolutionSupport : Z
    //   81: ifne -> 107
    //   84: getstatic com/oplus/screenmode/OplusScreenModeFeature.sScreenRateSupport : Z
    //   87: ifne -> 107
    //   90: ldc 'ScreenmodeClient'
    //   92: ldc_w ' display mode not support '
    //   95: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   98: pop
    //   99: aload_0
    //   100: iconst_4
    //   101: putfield mInitializeStatus : I
    //   104: aload_0
    //   105: monitorexit
    //   106: return
    //   107: ldc2_w 8
    //   110: ldc_w 'initoppposcreenmode'
    //   113: invokestatic traceBegin : (JLjava/lang/String;)V
    //   116: aload_0
    //   117: getfield mScreenModeService : Lcom/oplus/screenmode/IOplusScreenMode;
    //   120: ifnonnull -> 153
    //   123: ldc 'opposcreenmode'
    //   125: invokestatic getService : (Ljava/lang/String;)Landroid/os/IBinder;
    //   128: astore_2
    //   129: aload_2
    //   130: ifnonnull -> 145
    //   133: ldc 'ScreenmodeClient'
    //   135: ldc_w 'failed to get oppposcreenmode service:binder null'
    //   138: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   141: pop
    //   142: goto -> 153
    //   145: aload_0
    //   146: aload_2
    //   147: invokestatic asInterface : (Landroid/os/IBinder;)Lcom/oplus/screenmode/IOplusScreenMode;
    //   150: putfield mScreenModeService : Lcom/oplus/screenmode/IOplusScreenMode;
    //   153: aload_0
    //   154: getfield mScreenModeService : Lcom/oplus/screenmode/IOplusScreenMode;
    //   157: ifnull -> 173
    //   160: new android/os/Binder
    //   163: astore_2
    //   164: aload_2
    //   165: invokespecial <init> : ()V
    //   168: aload_0
    //   169: aload_2
    //   170: putfield mToken : Landroid/os/Binder;
    //   173: invokestatic myUid : ()I
    //   176: invokestatic isCore : (I)Z
    //   179: istore_3
    //   180: aload_0
    //   181: iload_3
    //   182: putfield mIsSystem : Z
    //   185: iconst_1
    //   186: istore #4
    //   188: iload_3
    //   189: ifne -> 209
    //   192: ldc 'com.android.systemui'
    //   194: aload_1
    //   195: invokevirtual getPackageName : ()Ljava/lang/String;
    //   198: invokevirtual equals : (Ljava/lang/Object;)Z
    //   201: ifeq -> 209
    //   204: aload_0
    //   205: iconst_1
    //   206: putfield mIsSystem : Z
    //   209: aload_1
    //   210: invokevirtual getPackageName : ()Ljava/lang/String;
    //   213: astore_1
    //   214: aload_0
    //   215: aload_1
    //   216: putfield mPackageName : Ljava/lang/String;
    //   219: aload_1
    //   220: ifnonnull -> 229
    //   223: aload_0
    //   224: ldc ''
    //   226: putfield mPackageName : Ljava/lang/String;
    //   229: ldc2_w 8
    //   232: invokestatic traceEnd : (J)V
    //   235: aload_0
    //   236: iconst_4
    //   237: putfield mInitializeStatus : I
    //   240: aload_0
    //   241: monitorexit
    //   242: aload_0
    //   243: invokespecial forceUpdateSTViewRate : ()V
    //   246: getstatic com/oplus/screenmode/OplusScreenModeFeature.mOplusDebug : Z
    //   249: ifeq -> 331
    //   252: new java/lang/StringBuilder
    //   255: dup
    //   256: invokespecial <init> : ()V
    //   259: astore_1
    //   260: aload_1
    //   261: ldc_w ' rate switch enable '
    //   264: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   267: pop
    //   268: aload_1
    //   269: getstatic com/oplus/screenmode/OplusScreenModeFeature.sScreenRateSupport : Z
    //   272: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   275: pop
    //   276: aload_1
    //   277: ldc_w ',core app:'
    //   280: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   283: pop
    //   284: aload_1
    //   285: aload_0
    //   286: getfield mIsSystem : Z
    //   289: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   292: pop
    //   293: aload_1
    //   294: ldc_w ',service == null ? '
    //   297: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   300: pop
    //   301: aload_0
    //   302: getfield mScreenModeService : Lcom/oplus/screenmode/IOplusScreenMode;
    //   305: ifnonnull -> 311
    //   308: goto -> 314
    //   311: iconst_0
    //   312: istore #4
    //   314: aload_1
    //   315: iload #4
    //   317: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   320: pop
    //   321: ldc 'ScreenmodeClient'
    //   323: aload_1
    //   324: invokevirtual toString : ()Ljava/lang/String;
    //   327: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   330: pop
    //   331: return
    //   332: astore_1
    //   333: aload_0
    //   334: monitorexit
    //   335: aload_1
    //   336: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #132	-> 0
    //   #133	-> 6
    //   #136	-> 15
    //   #137	-> 19
    //   #138	-> 28
    //   #141	-> 29
    //   #142	-> 31
    //   #143	-> 39
    //   #146	-> 42
    //   #147	-> 50
    //   #150	-> 55
    //   #151	-> 60
    //   #152	-> 66
    //   #157	-> 78
    //   #158	-> 90
    //   #159	-> 99
    //   #160	-> 104
    //   #162	-> 107
    //   #163	-> 116
    //   #164	-> 123
    //   #165	-> 129
    //   #166	-> 133
    //   #168	-> 145
    //   #172	-> 153
    //   #173	-> 160
    //   #176	-> 173
    //   #177	-> 185
    //   #178	-> 204
    //   #180	-> 209
    //   #181	-> 219
    //   #182	-> 223
    //   #184	-> 229
    //   #185	-> 235
    //   #186	-> 240
    //   #188	-> 242
    //   #189	-> 246
    //   #190	-> 252
    //   #193	-> 331
    //   #186	-> 332
    // Exception table:
    //   from	to	target	type
    //   31	39	332	finally
    //   39	41	332	finally
    //   42	50	332	finally
    //   50	55	332	finally
    //   55	60	332	finally
    //   60	66	332	finally
    //   66	78	332	finally
    //   78	90	332	finally
    //   90	99	332	finally
    //   99	104	332	finally
    //   104	106	332	finally
    //   107	116	332	finally
    //   116	123	332	finally
    //   123	129	332	finally
    //   133	142	332	finally
    //   145	153	332	finally
    //   153	160	332	finally
    //   160	173	332	finally
    //   173	185	332	finally
    //   192	204	332	finally
    //   204	209	332	finally
    //   209	219	332	finally
    //   223	229	332	finally
    //   229	235	332	finally
    //   235	240	332	finally
    //   240	242	332	finally
    //   333	335	332	finally
  }
  
  private void initSupportMode(Context paramContext) {
    DisplayManager displayManager = (DisplayManager)paramContext.getSystemService(DisplayManager.class);
    Display display = displayManager.getDisplay(0);
    if (display == null) {
      Log.e("ScreenmodeClient", "supportRefreshSwitch can get DEFAULT_DISPLAY ");
      return;
    } 
    Display.Mode[] arrayOfMode = display.getSupportedModes();
    ArrayList<Integer> arrayList1 = new ArrayList();
    ArrayList<Integer> arrayList2 = new ArrayList();
    for (byte b = 0; b < arrayOfMode.length; b++) {
      if (arrayList1.indexOf(Integer.valueOf(arrayOfMode[b].getPhysicalWidth())) == -1)
        arrayList1.add(Integer.valueOf(arrayOfMode[b].getPhysicalWidth())); 
      int i = (int)(arrayOfMode[b].getRefreshRate() + 0.1F);
      if (arrayList2.indexOf(Integer.valueOf(i)) == -1)
        arrayList2.add(Integer.valueOf(i)); 
    } 
    if (arrayList1.size() >= 2)
      sResolutionSupport = true; 
    if (arrayList2.size() >= 2)
      sScreenRateSupport = true; 
  }
  
  private void initDelay(Context paramContext, ViewGroup paramViewGroup) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mInitializeStatus : I
    //   6: iconst_2
    //   7: if_icmpge -> 20
    //   10: aload_0
    //   11: aload_1
    //   12: invokespecial initSupportMode : (Landroid/content/Context;)V
    //   15: aload_0
    //   16: iconst_2
    //   17: putfield mInitializeStatus : I
    //   20: aload_0
    //   21: getfield mInitializeStatus : I
    //   24: iconst_3
    //   25: if_icmpge -> 61
    //   28: aload_2
    //   29: invokevirtual getHandler : ()Landroid/os/Handler;
    //   32: astore_3
    //   33: aload_3
    //   34: ifnull -> 61
    //   37: aload_0
    //   38: iconst_3
    //   39: putfield mInitializeStatus : I
    //   42: new com/oplus/screenmode/OplusScreenModeFeature$2
    //   45: astore_2
    //   46: aload_2
    //   47: aload_0
    //   48: aload_1
    //   49: invokespecial <init> : (Lcom/oplus/screenmode/OplusScreenModeFeature;Landroid/content/Context;)V
    //   52: aload_3
    //   53: aload_2
    //   54: ldc2_w 100
    //   57: invokevirtual postDelayed : (Ljava/lang/Runnable;J)Z
    //   60: pop
    //   61: aload_0
    //   62: monitorexit
    //   63: return
    //   64: astore_1
    //   65: aload_0
    //   66: monitorexit
    //   67: aload_1
    //   68: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #226	-> 0
    //   #227	-> 2
    //   #228	-> 10
    //   #229	-> 15
    //   #232	-> 20
    //   #233	-> 28
    //   #234	-> 33
    //   #235	-> 37
    //   #236	-> 42
    //   #237	-> 42
    //   #244	-> 61
    //   #245	-> 63
    //   #244	-> 64
    // Exception table:
    //   from	to	target	type
    //   2	10	64	finally
    //   10	15	64	finally
    //   15	20	64	finally
    //   20	28	64	finally
    //   28	33	64	finally
    //   37	42	64	finally
    //   42	61	64	finally
    //   61	63	64	finally
    //   65	67	64	finally
  }
  
  private void forceUpdateSTViewRate() {
    try {
      if (this.mSTViewTokenMap != null) {
        HashMap<IBinder, ArrayList<View>> hashMap = this.mSTViewTokenMap;
        if (hashMap.size() > 0 && this.mScreenModeService != null) {
          Iterator<IBinder> iterator = this.mSTViewTokenMap.keySet().iterator();
          while (iterator.hasNext()) {
            IBinder iBinder = iterator.next();
            ArrayList arrayList = this.mSTViewTokenMap.get(iBinder);
            if (arrayList != null && arrayList.size() > 0) {
              this.mScreenModeService.setClientRefreshRate(iBinder, 2);
              if (mOplusDebug) {
                StringBuilder stringBuilder = new StringBuilder();
                this();
                stringBuilder.append("forceUpdateSTViewRate token ");
                stringBuilder.append(iBinder);
                stringBuilder.append(" rateId ");
                stringBuilder.append(2);
                Log.d("ScreenmodeClient", stringBuilder.toString());
              } 
            } 
          } 
        } 
      } 
    } catch (Exception exception) {}
  }
  
  private void clearInvalidView() {
    HashMap<IBinder, ArrayList<View>> hashMap = this.mSTViewTokenMap;
    if (hashMap != null && hashMap.size() > 0) {
      Iterator<IBinder> iterator = this.mSTViewTokenMap.keySet().iterator();
      while (iterator.hasNext()) {
        IBinder iBinder = iterator.next();
        ArrayList<View> arrayList = this.mSTViewTokenMap.get(iBinder);
        removeInvalidView(arrayList);
        if (arrayList.size() == 0) {
          this.mSTViewTokenMap.remove(iBinder);
          if (DEBUG) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("clearInvalidView remove token =");
            stringBuilder.append(iBinder);
            Log.d("ScreenmodeClient", stringBuilder.toString());
          } 
        } 
      } 
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("clearInvalidView size=");
        stringBuilder.append(this.mSTViewTokenMap.size());
        Log.d("ScreenmodeClient", stringBuilder.toString());
      } 
    } 
  }
  
  public void setRefreshRateIfNeed(Context paramContext, ViewGroup paramViewGroup, IBinder paramIBinder) {
    if (this.mInitializeStatus != 4)
      initDelay(paramContext, paramViewGroup); 
    if (!sScreenRateSupport || this.mIsSystem)
      return; 
    if (this.mScreenModeService == null && this.mInitializeStatus == 4)
      return; 
    if (this.mSTViewTokenMap == null)
      this.mSTViewTokenMap = new HashMap<>(3); 
    int i = 0;
    try {
      if (paramViewGroup instanceof RootViewSurfaceTaker)
        if (((RootViewSurfaceTaker)paramViewGroup).willYouTakeTheSurface() != null) {
          i = 2;
        } else {
          i = 0;
        }  
      Point point = new Point();
      this();
      Display display = paramViewGroup.getDisplay();
      if (display == null) {
        Log.d("ScreenmodeClient", " display null ");
        return;
      } 
      display.getRealSize(point);
      int j = i;
      if (i == 0) {
        i = getAreaThreshold(point.x, point.y);
        j = getRefreshRateId(paramViewGroup, i, 0 + 1);
      } 
      if (this.mScreenModeService != null) {
        if (updateSTViewRate(paramViewGroup, paramIBinder, j)) {
          if (mOplusDebug) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("setRefreshRateIfNeed token ");
            stringBuilder.append(paramIBinder);
            stringBuilder.append(" rateId ");
            stringBuilder.append(j);
            Log.d("ScreenmodeClient", stringBuilder.toString());
          } 
          this.mScreenModeService.setClientRefreshRate(paramIBinder, j);
        } 
      } else if (mOplusDebug) {
        Log.d("ScreenmodeClient", "setRefreshRateIfNeed service unavailable!!");
      } 
    } catch (Exception exception) {}
  }
  
  private boolean updateSTViewRate(ViewGroup paramViewGroup, IBinder paramIBinder, int paramInt) {
    ArrayList<View> arrayList2;
    int i = 0;
    ArrayList<View> arrayList1 = this.mSTViewTokenMap.get(paramIBinder);
    boolean bool = false;
    if (arrayList1 != null) {
      i = arrayList1.size();
      removeInvalidView(arrayList1);
    } else if (paramInt == 0) {
      return false;
    } 
    if (paramInt != 0) {
      ArrayList<View> arrayList = arrayList1;
      if (arrayList1 == null) {
        arrayList1 = new ArrayList<>(3);
        if (this.mSTViewTokenMap.size() >= 3)
          this.mSTViewTokenMap.clear(); 
        this.mSTViewTokenMap.put(paramIBinder, arrayList1);
        arrayList = arrayList1;
        if (DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("updateSTViewRate add token =");
          stringBuilder.append(paramIBinder);
          Log.d("ScreenmodeClient", stringBuilder.toString());
          arrayList = arrayList1;
        } 
      } 
      arrayList2 = arrayList;
      if (!arrayList.contains(paramViewGroup)) {
        if (arrayList.size() >= 3)
          arrayList.remove(0); 
        arrayList.add(paramViewGroup);
        paramViewGroup.addOnAttachStateChangeListener(this.mAttachStateChangeListener);
        arrayList2 = arrayList;
      } 
    } else {
      arrayList2 = arrayList1;
      if (arrayList1 != null) {
        arrayList1.remove(paramViewGroup);
        arrayList2 = arrayList1;
      } 
    } 
    if (arrayList2.size() == 0)
      this.mSTViewTokenMap.remove(paramIBinder); 
    null = bool;
    if (arrayList2.size() != i) {
      if (arrayList2.size() != 0) {
        null = bool;
        return (i == 0) ? true : null;
      } 
    } else {
      return null;
    } 
    return true;
  }
  
  private void removeInvalidView(ArrayList<View> paramArrayList) {
    for (int i = paramArrayList.size() - 1; i >= 0; i--) {
      View view = paramArrayList.get(i);
      if (view.getParent() == null || view.getVisibility() != 0) {
        paramArrayList.remove(i);
        view.removeOnAttachStateChangeListener(this.mAttachStateChangeListener);
      } 
    } 
  }
  
  private int getAreaThreshold(int paramInt1, int paramInt2) {
    paramInt1 = Math.min(paramInt1, paramInt2);
    return paramInt1 * paramInt1;
  }
  
  private int getRefreshRateId(ViewGroup paramViewGroup, int paramInt1, int paramInt2) {
    StringBuilder stringBuilder;
    byte b;
    if (paramInt2 > 20) {
      if (mOplusDebug) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(",depth=");
        stringBuilder.append(paramInt2);
        Log.d("ScreenmodeClient", stringBuilder.toString());
      } 
      return 0;
    } 
    if (stringBuilder.getVisibility() == 0 && 
      stringBuilder.getWidth() * stringBuilder.getHeight() >= paramInt1) {
      b = 1;
    } else {
      b = 0;
    } 
    if (b) {
      int i = stringBuilder.getChildCount();
      for (b = 0; b < i; b++) {
        int j;
        View view = stringBuilder.getChildAt(b);
        if (view instanceof ViewGroup) {
          j = getRefreshRateId((ViewGroup)view, paramInt1, paramInt2 + 1);
        } else {
          int k = getRefreshRateIdFromView(view, paramInt1);
          j = k;
          if (DEBUG) {
            j = k;
            if (k != 0) {
              StringBuilder stringBuilder1 = new StringBuilder();
              stringBuilder1.append(" getRefreshRateId   w= ");
              stringBuilder1.append(view.getWidth());
              stringBuilder1.append(",h=");
              stringBuilder1.append(view.getHeight());
              stringBuilder1.append(",depth=");
              stringBuilder1.append(paramInt2);
              stringBuilder1.append(",view=");
              stringBuilder1.append(view);
              String str = stringBuilder1.toString();
              Log.d("ScreenmodeClient", str);
              j = k;
            } 
          } 
        } 
        if (j != 0)
          return j; 
      } 
    } 
    return 0;
  }
  
  private int getRefreshRateIdFromView(View paramView, int paramInt) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual getVisibility : ()I
    //   4: istore_3
    //   5: iconst_0
    //   6: istore #4
    //   8: iload_3
    //   9: ifne -> 44
    //   12: aload_1
    //   13: instanceof android/view/SurfaceView
    //   16: ifne -> 26
    //   19: aload_1
    //   20: instanceof android/view/TextureView
    //   23: ifeq -> 44
    //   26: aload_1
    //   27: invokevirtual getWidth : ()I
    //   30: aload_1
    //   31: invokevirtual getHeight : ()I
    //   34: imul
    //   35: iload_2
    //   36: if_icmplt -> 44
    //   39: iconst_1
    //   40: istore_2
    //   41: goto -> 46
    //   44: iconst_0
    //   45: istore_2
    //   46: iload_2
    //   47: ifeq -> 53
    //   50: iconst_2
    //   51: istore #4
    //   53: iload #4
    //   55: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #441	-> 0
    //   #443	-> 26
    //   #444	-> 46
  }
  
  public void setRefreshRate(IBinder paramIBinder, int paramInt) {}
  
  public void setRefreshRate(View paramView, int paramInt) {}
  
  public boolean requestRefreshRate(boolean paramBoolean, int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("request Refresh Rate, ");
    stringBuilder.append(paramBoolean);
    stringBuilder.append(",");
    stringBuilder.append(paramInt);
    stringBuilder.append(",");
    stringBuilder.append(sScreenRateSupport);
    Log.d("ScreenmodeClient", stringBuilder.toString());
    if (!sScreenRateSupport)
      return false; 
    if (this.mScreenModeService != null)
      try {
        if (mOplusDebug) {
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("requestRefreshRate open ");
          stringBuilder.append(paramBoolean);
          stringBuilder.append(" rate ");
          stringBuilder.append(paramInt);
          Log.d("ScreenmodeClient", stringBuilder.toString());
        } 
        return this.mScreenModeService.requestRefreshRateWithToken(paramBoolean, paramInt, (IBinder)this.mToken);
      } catch (RemoteException remoteException) {
        Log.e("ScreenmodeClient", "requestRefreshRate failed!", (Throwable)remoteException);
        return false;
      }  
    Log.w("ScreenmodeClient", "requestRefreshRate service unavailable!!");
    return false;
  }
  
  public boolean setHighTemperatureStatus(int paramInt1, int paramInt2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("High Temperature Status ");
    stringBuilder.append(paramInt1);
    stringBuilder.append(",");
    stringBuilder.append(paramInt2);
    stringBuilder.append(",");
    stringBuilder.append(sScreenRateSupport);
    Log.d("ScreenmodeClient", stringBuilder.toString());
    if (!sScreenRateSupport)
      return false; 
    if (this.mScreenModeService != null)
      try {
        if (mOplusDebug) {
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("setHighTemperatureStatus status ");
          stringBuilder.append(paramInt1);
          stringBuilder.append(" rate ");
          stringBuilder.append(paramInt2);
          Log.d("ScreenmodeClient", stringBuilder.toString());
        } 
        return this.mScreenModeService.setHighTemperatureStatus(paramInt1, paramInt2);
      } catch (RemoteException remoteException) {
        Log.e("ScreenmodeClient", "requestRefreshRate failed!", (Throwable)remoteException);
        return false;
      }  
    Log.w("ScreenmodeClient", "setHighTemperatureStatus service unavailable!!");
    return false;
  }
  
  public void enterDCAndLowBrightnessMode(boolean paramBoolean) {
    if (mOplusDebug) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" enterDCAndLowBrightnessMode, enter:");
      stringBuilder.append(paramBoolean);
      Log.d("ScreenmodeClient", stringBuilder.toString());
    } 
    if (!sScreenRateSupport)
      return; 
    if (this.mScreenModeService != null)
      try {
        if (mOplusDebug) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("enterDCAndLowBrightnessMode enter ");
          stringBuilder.append(paramBoolean);
          Log.d("ScreenmodeClient", stringBuilder.toString());
        } 
        this.mScreenModeService.enterDCAndLowBrightnessMode(paramBoolean);
        return;
      } catch (RemoteException remoteException) {
        Log.e("ScreenmodeClient", "enterDCAndLowBrightnessMode failed!", (Throwable)remoteException);
        return;
      }  
    Log.w("ScreenmodeClient", "enterDCAndLowBrightnessMode service unavailable!!");
  }
  
  public boolean isDisplayCompat(String paramString, int paramInt) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" isDisplayCompat, pkg:");
      stringBuilder.append(paramString);
      stringBuilder.append(",");
      stringBuilder.append(sResolutionSupport);
      Log.d("ScreenmodeClient", stringBuilder.toString());
    } 
    if (!sResolutionSupport)
      return false; 
    if (this.mPackageName.equals(paramString))
      return sIsDisplayCompatApp; 
    IOplusScreenMode iOplusScreenMode = this.mScreenModeService;
    if (iOplusScreenMode != null)
      try {
        return iOplusScreenMode.isDisplayCompat(paramString, paramInt);
      } catch (RemoteException remoteException) {
        Log.e("ScreenmodeClient", "isDisplayCompat failed!", (Throwable)remoteException);
        return false;
      }  
    Log.w("ScreenmodeClient", "isDisplayCompat service unavailable!!");
    return false;
  }
  
  public void enterPSMode(boolean paramBoolean) {
    if (!sScreenRateSupport)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(" performance spec mode,");
    stringBuilder.append(paramBoolean);
    Log.d("ScreenmodeClient", stringBuilder.toString());
    IOplusScreenMode iOplusScreenMode = this.mScreenModeService;
    if (iOplusScreenMode != null)
      try {
        iOplusScreenMode.enterPSModeOnRateWithToken(paramBoolean, 7, (IBinder)this.mToken);
        return;
      } catch (RemoteException remoteException) {
        Log.e("ScreenmodeClient", "performance spec mode failed!", (Throwable)remoteException);
        return;
      }  
    Log.w("ScreenmodeClient", "performance spec mode service unavailable!!");
  }
  
  public void enterPSModeOnRate(boolean paramBoolean, int paramInt) {
    if (!sScreenRateSupport)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(" performance spec mode, ");
    stringBuilder.append(paramBoolean);
    stringBuilder.append(",");
    stringBuilder.append(paramInt);
    Log.d("ScreenmodeClient", stringBuilder.toString());
    IOplusScreenMode iOplusScreenMode = this.mScreenModeService;
    if (iOplusScreenMode != null)
      try {
        iOplusScreenMode.enterPSModeOnRateWithToken(paramBoolean, paramInt, (IBinder)this.mToken);
        return;
      } catch (RemoteException remoteException) {
        Log.e("ScreenmodeClient", "performance spec mode failed!", (Throwable)remoteException);
        return;
      }  
    Log.w("ScreenmodeClient", "performance spec mode service unavailable!!");
  }
  
  public boolean getGameList(Bundle paramBundle) {
    if (!sScreenRateSupport)
      return false; 
    IOplusScreenMode iOplusScreenMode = this.mScreenModeService;
    if (iOplusScreenMode != null)
      try {
        return iOplusScreenMode.getGameList(paramBundle);
      } catch (RemoteException remoteException) {
        Log.e("ScreenmodeClient", "game list failed!", (Throwable)remoteException);
        return false;
      }  
    Log.w("ScreenmodeClient", "game list mode service unavailable!!");
    return false;
  }
  
  public void overrideDisplayMetricsIfNeed(DisplayMetrics paramDisplayMetrics) {
    if (paramDisplayMetrics == null) {
      Log.e("ScreenmodeClient", "overrideDisplayMetricsIfNeed failed for inoutDm null.");
      return;
    } 
    int i = paramDisplayMetrics.densityDpi, j = sCompatDensity;
    if (i != j) {
      float f = this.mAppInvScale;
      paramDisplayMetrics.density = j * 0.00625F;
      paramDisplayMetrics.scaledDensity = paramDisplayMetrics.density;
      paramDisplayMetrics.densityDpi = sCompatDensity;
      paramDisplayMetrics.xdpi = paramDisplayMetrics.noncompatXdpi * f;
      paramDisplayMetrics.ydpi = paramDisplayMetrics.noncompatYdpi * f;
      paramDisplayMetrics.widthPixels = (int)(paramDisplayMetrics.noncompatWidthPixels * f);
      paramDisplayMetrics.heightPixels = (int)(paramDisplayMetrics.noncompatHeightPixels * f);
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("DisplayCompat: applyToDisplayMetrics0, inoutDm=");
        stringBuilder.append(paramDisplayMetrics);
        stringBuilder.append(" noncompatDensityDpi=");
        stringBuilder.append(paramDisplayMetrics.noncompatDensityDpi);
        stringBuilder.append(" caller:");
        stringBuilder.append(Debug.getCallers(8));
        String str = stringBuilder.toString();
        Log.d("ScreenmodeClient", str);
      } 
    } 
  }
  
  public void applyCompatInfo(CompatibilityInfo paramCompatibilityInfo, DisplayMetrics paramDisplayMetrics) {
    if (paramCompatibilityInfo == null || paramDisplayMetrics == null) {
      Log.e("ScreenmodeClient", "applyCompatInfo failed for param null.");
      return;
    } 
    if (sIsDisplayCompatApp && paramDisplayMetrics.densityDpi != sCompatDensity) {
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("DisplayCompat: applyCompatInfo, change out=");
        stringBuilder.append(paramDisplayMetrics);
        stringBuilder.append(" to ");
        stringBuilder.append(sCompatDensity);
        stringBuilder.append(" caller:");
        stringBuilder.append(Debug.getCallers(8));
        String str = stringBuilder.toString();
        Log.d("ScreenmodeClient", str);
      } 
      paramCompatibilityInfo.applyToDisplayMetrics(paramDisplayMetrics);
    } 
  }
  
  public void updateCompatDensityIfNeed(int paramInt) {
    if (!sIsDisplayCompatApp)
      return; 
    int i = (int)(paramInt * this.mAppInvScale);
    if (sCompatDensity != i) {
      sCompatDensity = i;
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("DisplayCompat: updateCompatDensityIfNeed from ");
        stringBuilder.append(paramInt);
        stringBuilder.append(" to density=");
        stringBuilder.append(i);
        stringBuilder.append(", ");
        stringBuilder.append(Debug.getCallers(8));
        String str = stringBuilder.toString();
        Log.i("ScreenmodeClient", str);
      } 
    } 
  }
  
  public boolean supportDisplayCompat(String paramString, int paramInt) {
    boolean bool;
    if (this.mScreenModeService != null && sResolutionSupport) {
      try {
        if (this.mPackageName.equals(paramString))
          return sIsDisplayCompatApp; 
        boolean bool1 = this.mScreenModeService.supportDisplayCompat(paramString, paramInt);
        bool = bool1;
        if (DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("supportDisplayCompat pkg ");
          stringBuilder.append(paramString);
          stringBuilder.append(", ");
          stringBuilder.append(sIsDisplayCompatApp);
          Log.d("ScreenmodeClient", stringBuilder.toString());
          bool = bool1;
        } 
      } catch (RemoteException remoteException) {
        Log.e("ScreenmodeClient", "supportDisplayCompat failed!", (Throwable)remoteException);
        bool = false;
      } 
    } else {
      Log.w("ScreenmodeClient", "supportDisplayCompat service unavailable!!");
      bool = false;
    } 
    return bool;
  }
  
  public boolean supportDisplayCompat() {
    if (DEBUG)
      Log.d("IOplusScreenModeFeature", "supportDisplayCompat  "); 
    return sIsDisplayCompatApp;
  }
  
  public int displayCompatDensity(int paramInt) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("displayCompatDensity sCompatDensity=");
      stringBuilder.append(sCompatDensity);
      Log.d("IOplusScreenModeFeature", stringBuilder.toString());
    } 
    return sCompatDensity;
  }
  
  public void setSupportDisplayCompat(boolean paramBoolean) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setSupportDisplayCompat support=");
      stringBuilder.append(paramBoolean);
      stringBuilder.append(",");
      stringBuilder.append(sResolutionSupport);
      Log.d("IOplusScreenModeFeature", stringBuilder.toString());
    } 
    sIsDisplayCompatApp = paramBoolean;
  }
  
  public void initDisplayCompat(ApplicationInfo paramApplicationInfo) {
    OplusApplicationInfoEx oplusApplicationInfoEx = OplusApplicationInfoEx.getOplusAppInfoExFromAppInfoRef(paramApplicationInfo);
    if (oplusApplicationInfoEx != null) {
      this.mAppScale = oplusApplicationInfoEx.getAppScale();
      this.mAppInvScale = oplusApplicationInfoEx.getAppInvScale();
      sCompatDensity = oplusApplicationInfoEx.getCompatDensity();
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("initDisplayCompat ");
    stringBuilder.append(this.mAppScale);
    stringBuilder.append(",");
    stringBuilder.append(this.mAppInvScale);
    stringBuilder.append(",");
    stringBuilder.append(sCompatDensity);
    Log.d("IOplusScreenModeFeature", stringBuilder.toString());
  }
  
  public void updateCompatRealSize(DisplayInfo paramDisplayInfo, Point paramPoint) {
    if (!sIsDisplayCompatApp)
      return; 
    paramPoint.x = (int)(paramDisplayInfo.logicalWidth * this.mAppInvScale);
    paramPoint.y = (int)(paramDisplayInfo.logicalHeight * this.mAppInvScale);
  }
}
