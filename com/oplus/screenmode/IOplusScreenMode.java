package com.oplus.screenmode;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusScreenMode extends IInterface {
  void addCallback(IOplusScreenModeCallback paramIOplusScreenModeCallback) throws RemoteException;
  
  void enterDCAndLowBrightnessMode(boolean paramBoolean) throws RemoteException;
  
  void enterPSMode(boolean paramBoolean) throws RemoteException;
  
  void enterPSModeOnRate(boolean paramBoolean, int paramInt) throws RemoteException;
  
  void enterPSModeOnRateWithToken(boolean paramBoolean, int paramInt, IBinder paramIBinder) throws RemoteException;
  
  String getDisableOverrideViewList(String paramString) throws RemoteException;
  
  boolean getGameList(Bundle paramBundle) throws RemoteException;
  
  boolean isDisplayCompat(String paramString, int paramInt) throws RemoteException;
  
  void remove(IOplusScreenModeCallback paramIOplusScreenModeCallback) throws RemoteException;
  
  boolean requestGameRefreshRate(String paramString, int paramInt) throws RemoteException;
  
  boolean requestRefreshRate(boolean paramBoolean, int paramInt) throws RemoteException;
  
  boolean requestRefreshRateWithToken(boolean paramBoolean, int paramInt, IBinder paramIBinder) throws RemoteException;
  
  void setClientRefreshRate(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  boolean setHighTemperatureStatus(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean supportDisplayCompat(String paramString, int paramInt) throws RemoteException;
  
  class Default implements IOplusScreenMode {
    public void addCallback(IOplusScreenModeCallback param1IOplusScreenModeCallback) throws RemoteException {}
    
    public void remove(IOplusScreenModeCallback param1IOplusScreenModeCallback) throws RemoteException {}
    
    public void setClientRefreshRate(IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public boolean requestRefreshRate(boolean param1Boolean, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean supportDisplayCompat(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean setHighTemperatureStatus(int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public void enterDCAndLowBrightnessMode(boolean param1Boolean) throws RemoteException {}
    
    public boolean isDisplayCompat(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public void enterPSMode(boolean param1Boolean) throws RemoteException {}
    
    public void enterPSModeOnRate(boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public boolean requestGameRefreshRate(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean requestRefreshRateWithToken(boolean param1Boolean, int param1Int, IBinder param1IBinder) throws RemoteException {
      return false;
    }
    
    public boolean getGameList(Bundle param1Bundle) throws RemoteException {
      return false;
    }
    
    public String getDisableOverrideViewList(String param1String) throws RemoteException {
      return null;
    }
    
    public void enterPSModeOnRateWithToken(boolean param1Boolean, int param1Int, IBinder param1IBinder) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusScreenMode {
    private static final String DESCRIPTOR = "com.oplus.screenmode.IOplusScreenMode";
    
    static final int TRANSACTION_addCallback = 1;
    
    static final int TRANSACTION_enterDCAndLowBrightnessMode = 7;
    
    static final int TRANSACTION_enterPSMode = 9;
    
    static final int TRANSACTION_enterPSModeOnRate = 10;
    
    static final int TRANSACTION_enterPSModeOnRateWithToken = 15;
    
    static final int TRANSACTION_getDisableOverrideViewList = 14;
    
    static final int TRANSACTION_getGameList = 13;
    
    static final int TRANSACTION_isDisplayCompat = 8;
    
    static final int TRANSACTION_remove = 2;
    
    static final int TRANSACTION_requestGameRefreshRate = 11;
    
    static final int TRANSACTION_requestRefreshRate = 4;
    
    static final int TRANSACTION_requestRefreshRateWithToken = 12;
    
    static final int TRANSACTION_setClientRefreshRate = 3;
    
    static final int TRANSACTION_setHighTemperatureStatus = 6;
    
    static final int TRANSACTION_supportDisplayCompat = 5;
    
    public Stub() {
      attachInterface(this, "com.oplus.screenmode.IOplusScreenMode");
    }
    
    public static IOplusScreenMode asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.screenmode.IOplusScreenMode");
      if (iInterface != null && iInterface instanceof IOplusScreenMode)
        return (IOplusScreenMode)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 15:
          return "enterPSModeOnRateWithToken";
        case 14:
          return "getDisableOverrideViewList";
        case 13:
          return "getGameList";
        case 12:
          return "requestRefreshRateWithToken";
        case 11:
          return "requestGameRefreshRate";
        case 10:
          return "enterPSModeOnRate";
        case 9:
          return "enterPSMode";
        case 8:
          return "isDisplayCompat";
        case 7:
          return "enterDCAndLowBrightnessMode";
        case 6:
          return "setHighTemperatureStatus";
        case 5:
          return "supportDisplayCompat";
        case 4:
          return "requestRefreshRate";
        case 3:
          return "setClientRefreshRate";
        case 2:
          return "remove";
        case 1:
          break;
      } 
      return "addCallback";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IBinder iBinder;
      if (param1Int1 != 1598968902) {
        boolean bool7;
        int i2;
        boolean bool6;
        int i1;
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        int i;
        IBinder iBinder2;
        String str1;
        Bundle bundle;
        IBinder iBinder1;
        String str2;
        boolean bool8 = false, bool9 = false, bool10 = false, bool11 = false, bool12 = false, bool13 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 15:
            param1Parcel1.enforceInterface("com.oplus.screenmode.IOplusScreenMode");
            bool8 = bool13;
            if (param1Parcel1.readInt() != 0)
              bool8 = true; 
            param1Int1 = param1Parcel1.readInt();
            iBinder2 = param1Parcel1.readStrongBinder();
            enterPSModeOnRateWithToken(bool8, param1Int1, iBinder2);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            iBinder2.enforceInterface("com.oplus.screenmode.IOplusScreenMode");
            str1 = iBinder2.readString();
            str1 = getDisableOverrideViewList(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 13:
            str1.enforceInterface("com.oplus.screenmode.IOplusScreenMode");
            bundle = new Bundle();
            bool7 = getGameList(bundle);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            param1Parcel2.writeInt(1);
            bundle.writeToParcel(param1Parcel2, 1);
            return true;
          case 12:
            bundle.enforceInterface("com.oplus.screenmode.IOplusScreenMode");
            if (bundle.readInt() != 0)
              bool8 = true; 
            i2 = bundle.readInt();
            iBinder1 = bundle.readStrongBinder();
            bool6 = requestRefreshRateWithToken(bool8, i2, iBinder1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 11:
            iBinder1.enforceInterface("com.oplus.screenmode.IOplusScreenMode");
            str2 = iBinder1.readString();
            i1 = iBinder1.readInt();
            bool5 = requestGameRefreshRate(str2, i1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 10:
            iBinder1.enforceInterface("com.oplus.screenmode.IOplusScreenMode");
            bool8 = bool9;
            if (iBinder1.readInt() != 0)
              bool8 = true; 
            n = iBinder1.readInt();
            enterPSModeOnRate(bool8, n);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            iBinder1.enforceInterface("com.oplus.screenmode.IOplusScreenMode");
            bool8 = bool10;
            if (iBinder1.readInt() != 0)
              bool8 = true; 
            enterPSMode(bool8);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            iBinder1.enforceInterface("com.oplus.screenmode.IOplusScreenMode");
            str2 = iBinder1.readString();
            n = iBinder1.readInt();
            bool4 = isDisplayCompat(str2, n);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 7:
            iBinder1.enforceInterface("com.oplus.screenmode.IOplusScreenMode");
            bool8 = bool11;
            if (iBinder1.readInt() != 0)
              bool8 = true; 
            enterDCAndLowBrightnessMode(bool8);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            iBinder1.enforceInterface("com.oplus.screenmode.IOplusScreenMode");
            param1Int2 = iBinder1.readInt();
            m = iBinder1.readInt();
            bool3 = setHighTemperatureStatus(param1Int2, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 5:
            iBinder1.enforceInterface("com.oplus.screenmode.IOplusScreenMode");
            str2 = iBinder1.readString();
            k = iBinder1.readInt();
            bool2 = supportDisplayCompat(str2, k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 4:
            iBinder1.enforceInterface("com.oplus.screenmode.IOplusScreenMode");
            bool8 = bool12;
            if (iBinder1.readInt() != 0)
              bool8 = true; 
            j = iBinder1.readInt();
            bool1 = requestRefreshRate(bool8, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 3:
            iBinder1.enforceInterface("com.oplus.screenmode.IOplusScreenMode");
            iBinder = iBinder1.readStrongBinder();
            i = iBinder1.readInt();
            setClientRefreshRate(iBinder, i);
            return true;
          case 2:
            iBinder1.enforceInterface("com.oplus.screenmode.IOplusScreenMode");
            iOplusScreenModeCallback = IOplusScreenModeCallback.Stub.asInterface(iBinder1.readStrongBinder());
            remove(iOplusScreenModeCallback);
            iBinder.writeNoException();
            return true;
          case 1:
            break;
        } 
        iOplusScreenModeCallback.enforceInterface("com.oplus.screenmode.IOplusScreenMode");
        IOplusScreenModeCallback iOplusScreenModeCallback = IOplusScreenModeCallback.Stub.asInterface(iOplusScreenModeCallback.readStrongBinder());
        addCallback(iOplusScreenModeCallback);
        iBinder.writeNoException();
        return true;
      } 
      iBinder.writeString("com.oplus.screenmode.IOplusScreenMode");
      return true;
    }
    
    private static class Proxy implements IOplusScreenMode {
      public static IOplusScreenMode sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.screenmode.IOplusScreenMode";
      }
      
      public void addCallback(IOplusScreenModeCallback param2IOplusScreenModeCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oplus.screenmode.IOplusScreenMode");
          if (param2IOplusScreenModeCallback != null) {
            iBinder = param2IOplusScreenModeCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusScreenMode.Stub.getDefaultImpl() != null) {
            IOplusScreenMode.Stub.getDefaultImpl().addCallback(param2IOplusScreenModeCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void remove(IOplusScreenModeCallback param2IOplusScreenModeCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oplus.screenmode.IOplusScreenMode");
          if (param2IOplusScreenModeCallback != null) {
            iBinder = param2IOplusScreenModeCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusScreenMode.Stub.getDefaultImpl() != null) {
            IOplusScreenMode.Stub.getDefaultImpl().remove(param2IOplusScreenModeCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setClientRefreshRate(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.screenmode.IOplusScreenMode");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IOplusScreenMode.Stub.getDefaultImpl() != null) {
            IOplusScreenMode.Stub.getDefaultImpl().setClientRefreshRate(param2IBinder, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean requestRefreshRate(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("com.oplus.screenmode.IOplusScreenMode");
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOplusScreenMode.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusScreenMode.Stub.getDefaultImpl().requestRefreshRate(param2Boolean, param2Int);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean supportDisplayCompat(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.screenmode.IOplusScreenMode");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IOplusScreenMode.Stub.getDefaultImpl() != null) {
            bool1 = IOplusScreenMode.Stub.getDefaultImpl().supportDisplayCompat(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setHighTemperatureStatus(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.screenmode.IOplusScreenMode");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IOplusScreenMode.Stub.getDefaultImpl() != null) {
            bool1 = IOplusScreenMode.Stub.getDefaultImpl().setHighTemperatureStatus(param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enterDCAndLowBrightnessMode(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.oplus.screenmode.IOplusScreenMode");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool1 && IOplusScreenMode.Stub.getDefaultImpl() != null) {
            IOplusScreenMode.Stub.getDefaultImpl().enterDCAndLowBrightnessMode(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDisplayCompat(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.screenmode.IOplusScreenMode");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(8, parcel1, parcel2, 0);
          if (!bool2 && IOplusScreenMode.Stub.getDefaultImpl() != null) {
            bool1 = IOplusScreenMode.Stub.getDefaultImpl().isDisplayCompat(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enterPSMode(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.oplus.screenmode.IOplusScreenMode");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool1 && IOplusScreenMode.Stub.getDefaultImpl() != null) {
            IOplusScreenMode.Stub.getDefaultImpl().enterPSMode(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enterPSModeOnRate(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.oplus.screenmode.IOplusScreenMode");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool1 && IOplusScreenMode.Stub.getDefaultImpl() != null) {
            IOplusScreenMode.Stub.getDefaultImpl().enterPSModeOnRate(param2Boolean, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean requestGameRefreshRate(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.screenmode.IOplusScreenMode");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(11, parcel1, parcel2, 0);
          if (!bool2 && IOplusScreenMode.Stub.getDefaultImpl() != null) {
            bool1 = IOplusScreenMode.Stub.getDefaultImpl().requestGameRefreshRate(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean requestRefreshRateWithToken(boolean param2Boolean, int param2Int, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("com.oplus.screenmode.IOplusScreenMode");
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IOplusScreenMode.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusScreenMode.Stub.getDefaultImpl().requestRefreshRateWithToken(param2Boolean, param2Int, param2IBinder);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getGameList(Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.screenmode.IOplusScreenMode");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(13, parcel1, parcel2, 0);
          if (!bool2 && IOplusScreenMode.Stub.getDefaultImpl() != null) {
            bool1 = IOplusScreenMode.Stub.getDefaultImpl().getGameList(param2Bundle);
            return bool1;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0)
            bool1 = true; 
          if (parcel2.readInt() != 0)
            param2Bundle.readFromParcel(parcel2); 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDisableOverrideViewList(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.screenmode.IOplusScreenMode");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IOplusScreenMode.Stub.getDefaultImpl() != null) {
            param2String = IOplusScreenMode.Stub.getDefaultImpl().getDisableOverrideViewList(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enterPSModeOnRateWithToken(boolean param2Boolean, int param2Int, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.oplus.screenmode.IOplusScreenMode");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool1 = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool1 && IOplusScreenMode.Stub.getDefaultImpl() != null) {
            IOplusScreenMode.Stub.getDefaultImpl().enterPSModeOnRateWithToken(param2Boolean, param2Int, param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusScreenMode param1IOplusScreenMode) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusScreenMode != null) {
          Proxy.sDefaultImpl = param1IOplusScreenMode;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusScreenMode getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
