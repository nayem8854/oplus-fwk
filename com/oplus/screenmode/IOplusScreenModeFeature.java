package com.oplus.screenmode;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.CompatibilityInfo;
import android.graphics.Point;
import android.os.Bundle;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.DisplayInfo;
import android.view.View;
import android.view.ViewGroup;

public interface IOplusScreenModeFeature extends IOplusCommonFeature {
  public static final IOplusScreenModeFeature DEFAULT = (IOplusScreenModeFeature)new Object();
  
  public static final String NAME = "IOplusScreenModeFeature";
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusScreenModeFeature;
  }
  
  default IOplusCommonFeature getDefault() {
    return DEFAULT;
  }
  
  default void init(Context paramContext) {
    Log.d("IOplusScreenModeFeature", "default init");
  }
  
  default void setRefreshRate(IBinder paramIBinder, int paramInt) {}
  
  default void setRefreshRate(View paramView, int paramInt) {}
  
  default boolean requestRefreshRate(boolean paramBoolean, int paramInt) {
    return false;
  }
  
  default boolean setHighTemperatureStatus(int paramInt1, int paramInt2) {
    return false;
  }
  
  default void enterDCAndLowBrightnessMode(boolean paramBoolean) {}
  
  default boolean isDisplayCompat(String paramString, int paramInt) {
    return false;
  }
  
  default void enterPSMode(boolean paramBoolean) {}
  
  default void enterPSModeOnRate(boolean paramBoolean, int paramInt) {}
  
  default boolean getGameList(Bundle paramBundle) {
    return false;
  }
  
  default void setRefreshRateIfNeed(Context paramContext, ViewGroup paramViewGroup, IBinder paramIBinder) {}
  
  default void overrideDisplayMetricsIfNeed(DisplayMetrics paramDisplayMetrics) {}
  
  default void applyCompatInfo(CompatibilityInfo paramCompatibilityInfo, DisplayMetrics paramDisplayMetrics) {}
  
  default void updateCompatDensityIfNeed(int paramInt) {}
  
  default boolean supportDisplayCompat(String paramString, int paramInt) {
    return false;
  }
  
  default boolean supportDisplayCompat() {
    return false;
  }
  
  default int displayCompatDensity(int paramInt) {
    return paramInt;
  }
  
  default void setSupportDisplayCompat(boolean paramBoolean) {}
  
  default void initDisplayCompat(ApplicationInfo paramApplicationInfo) {}
  
  default void updateCompatRealSize(DisplayInfo paramDisplayInfo, Point paramPoint) {}
}
