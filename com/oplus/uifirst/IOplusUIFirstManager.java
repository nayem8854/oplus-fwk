package com.oplus.uifirst;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.os.Bundle;
import android.util.IntArray;

public interface IOplusUIFirstManager extends IOplusCommonFeature {
  public static final int APP_STATUS_MOVE_TO_BG = 2;
  
  public static final int APP_STATUS_MOVE_TO_FG = 1;
  
  public static final int APP_STATUS_PROC_DIE = 3;
  
  public static final int APP_STATUS_START_ACTIVITY = 0;
  
  public static final IOplusUIFirstManager DEFAULT = (IOplusUIFirstManager)new Object();
  
  public static final String NAME = "IOplusUIFirstManager";
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusUIFirstManager;
  }
  
  default IOplusCommonFeature getDefault() {
    return DEFAULT;
  }
  
  default void acquireLaunchBoost() {}
  
  default void setfpsgoparamforperformance() {}
  
  default void getfpsgoparamforreserve() {}
  
  default void backfpsgoparam() {}
  
  default void onAppStatusChanged(int paramInt1, int paramInt2, Bundle paramBundle) {}
  
  default void onAppStatusChanged(int paramInt1, int paramInt2, int paramInt3) {}
  
  default void onAppStatusChanged(int paramInt1, int paramInt2, String paramString, int paramInt3) {}
  
  default void onAppStatusChanged(int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle) {}
  
  default void adjustUxProcess(int paramInt1, int paramInt2, int paramInt3, IntArray paramIntArray, String paramString, boolean paramBoolean) {}
  
  default void setUxThread(int paramInt1, int paramInt2, boolean paramBoolean) {}
  
  default void setUxThread(int paramInt1, int paramInt2, String paramString) {}
  
  default void setUxThreadValue(int paramInt1, int paramInt2, String paramString) {}
  
  default void setUxThreadValue(String paramString1, int paramInt1, int paramInt2, String paramString2) {}
  
  default void writeProcNode(String paramString1, String paramString2) {}
  
  default String readProcNode(String paramString) {
    return null;
  }
}
