package com.oplus.uifirst;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.Trace;
import android.util.IntArray;
import android.util.Log;
import android.util.SparseArray;
import com.oplus.filter.IDynamicFilterService;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.IntFunction;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OplusUIFirstManager implements IOplusUIFirstManager {
  private static OplusUIFirstManager sInstance = null;
  
  private ArrayList<String> mTargetUxThreads = new ArrayList<>();
  
  private SparseArray<IntArray> mBoostingProcs = new SparseArray();
  
  private static final long APP_MOVE_TO_BG_DELAY = 1000L;
  
  private static final boolean DEBUG = false;
  
  private static final String INKERNEL_FBT_CPU_PARAM_BHR = "sys/module/fbt_cpu/parameters/bhr";
  
  private static final String INKERNEL_FBT_CPU_PARAM_BHR_FLOOR_BOUND = "sys/module/fbt_cpu/parameters/floor_bound";
  
  private static final String INKERNEL_FBT_CPU_PARAM_BHR_KMIN = "sys/module/fbt_cpu/parameters/kmin";
  
  private static final String INKERNEL_FBT_CPU_PARAM_BHR_MIN_RESCUE_PERCENT = "sys/module/fbt_cpu/parameters/min_rescue_percent";
  
  private static final String INKERNEL_FBT_CPU_PARAM_BHR_OPP = "sys/module/fbt_cpu/parameters/bhr_opp";
  
  private static final String INKERNEL_FBT_CPU_PARAM_BHR_RESCUE_ENHANCE_F = "sys/module/fbt_cpu/parameters/rescue_enhance_f";
  
  private static final String INKERNEL_FBT_CPU_PARAM_BHR_RESCUE_PERCENT_120 = "sys/module/fbt_cpu/parameters/rescue_percent_120";
  
  private static final String INKERNEL_FBT_CPU_PARAM_BHR_RESCUE_PERCENT_90 = "sys/module/fbt_cpu/parameters/rescue_percent_90";
  
  private static final String INKERNEL_FBT_CPU_PARAM_RESCUE_PERCENT = "sys/module/fbt_cpu/parameters/rescue_percent";
  
  private static final String INKERNEL_XGF_PARAM_XGF_DEP_FRAMES = "sys/module/xgf/parameters/xgf_dep_frames";
  
  private static final long LAUNCH_BOOST_DURATION = 3000L;
  
  private static final int MSG_APP_MOVE_TO_BG = 2;
  
  private static final int MSG_APP_MOVE_TO_BG_DELAY = 3;
  
  private static final int MSG_APP_STATUS_CHANGED = 0;
  
  private static final int MSG_DISABLE_LAUNCH_BOOST = 1;
  
  private static final String SLIDE_BOOST_PATH = "/proc/sys/kernel/slide_boost_enabled";
  
  private static final String TAG = "OplusUIFirst";
  
  private static final String UIFIRST_LAUNCH_BOOST_PATH = "/proc/sys/kernel/launcher_boost_enabled";
  
  private IDynamicFilterService mFilterService;
  
  private Handler mHandler;
  
  private HandlerThread mThread;
  
  private String mbhr;
  
  private String mbhrkmin;
  
  private String mbhropp;
  
  private String mfloorbount;
  
  private String mminrescuepercent;
  
  private String mrescueenhancef;
  
  private String mrescuepercent;
  
  private String mrescuepercent120;
  
  private String mrescuepercent90;
  
  private String mxgfdepframes;
  
  public static OplusUIFirstManager getInstance() {
    if (sInstance == null)
      sInstance = new OplusUIFirstManager(); 
    return sInstance;
  }
  
  private class AppStatusParam {
    Bundle mData;
    
    int mPid;
    
    int mRenderTid;
    
    int mStatus;
    
    final OplusUIFirstManager this$0;
    
    private AppStatusParam() {}
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("AppStatusParam @pid:");
      stringBuilder.append(this.mPid);
      stringBuilder.append(" renderTid:");
      stringBuilder.append(this.mRenderTid);
      return stringBuilder.toString();
    }
    
    public boolean equals(Object param1Object) {
      boolean bool;
      param1Object = param1Object;
      if (this.mPid == ((AppStatusParam)param1Object).mPid && this.mRenderTid == ((AppStatusParam)param1Object).mRenderTid) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
  
  private class UIFirstHandler extends Handler {
    final OplusUIFirstManager this$0;
    
    public UIFirstHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      try {
        int arrayOfInt[], i = param1Message.what;
        if (i != 0) {
          if (i != 1) {
            if (i != 2) {
              if (i == 3) {
                int j = param1Message.arg1;
                int k = param1Message.arg2;
                OplusUIFirstManager.this.setUxThread(j, j, false);
                OplusUIFirstManager.this.setUxThread(j, k, false);
                arrayOfInt = (int[])param1Message.obj;
                for (i = 0; i < arrayOfInt.length; i++)
                  OplusUIFirstManager.this.setUxThread(j, arrayOfInt[i], false); 
                OplusUIFirstManager.this.uxTrace(j, k, arrayOfInt, false);
              } 
            } else {
              OplusUIFirstManager.this.handleAppStatusChanged((OplusUIFirstManager.AppStatusParam)((Message)arrayOfInt).obj);
            } 
          } else {
            OplusUIFirstManager.this.writeProcNode("/proc/sys/kernel/launcher_boost_enabled", "0");
          } 
        } else {
          OplusUIFirstManager.this.handleAppStatusChanged((OplusUIFirstManager.AppStatusParam)((Message)arrayOfInt).obj);
        } 
      } catch (Exception exception) {
        Log.w("OplusUIFirst", "exception in handle msg :");
        exception.printStackTrace();
      } 
    }
  }
  
  public void acquireLaunchBoost() {
    if (this.mHandler.hasMessages(1)) {
      this.mHandler.removeMessages(1);
    } else {
      writeProcNode("/proc/sys/kernel/launcher_boost_enabled", "1");
    } 
    this.mHandler.sendEmptyMessageDelayed(1, 3000L);
  }
  
  IntArray getUxThreadForPid(int paramInt) {
    IntArray intArray = new IntArray(4);
    if (this.mTargetUxThreads.size() != 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getUxThreadForPid:");
      stringBuilder.append(paramInt);
      Trace.traceBegin(64L, stringBuilder.toString());
      try {
        file = new File();
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("/proc/");
        stringBuilder.append(paramInt);
        stringBuilder.append("/task");
        this(stringBuilder.toString());
        for (File file : file.listFiles()) {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append(file.getAbsolutePath());
          stringBuilder1.append("/comm");
          String str = stringBuilder1.toString();
          str = readProcNode(str);
          if (this.mTargetUxThreads.contains(str.trim()))
            intArray.add(Integer.valueOf(file.getName()).intValue()); 
        } 
      } catch (Exception exception) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("exception in getUxThreadForPid:");
        stringBuilder1.append(paramInt);
        Log.d("OplusUIFirst", stringBuilder1.toString());
        exception.printStackTrace();
      } 
      Trace.traceEnd(64L);
    } 
    return intArray;
  }
  
  private void boostThreadsToUx(int paramInt, IntArray paramIntArray, boolean paramBoolean) {
    StringBuilder stringBuilder1;
    if (paramIntArray == null || paramIntArray.size() == 0) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("skip boostThreadsToUx for ");
      stringBuilder1.append(paramInt);
      stringBuilder1.append("due to null thread list");
      Log.w("OplusUIFirst", stringBuilder1.toString());
      return;
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("");
    stringBuilder2.append(paramInt);
    String str = stringBuilder2.toString();
    for (byte b = 0; b < stringBuilder1.size(); b++) {
      int i = stringBuilder1.get(b);
      setUxThread(paramInt, i, paramBoolean);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append(",");
      stringBuilder.append(i);
      str = stringBuilder.toString();
    } 
    if (paramBoolean) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("UX Boost: ");
      stringBuilder1.append(str);
      Trace.asyncTraceBegin(64L, stringBuilder1.toString(), paramInt);
    } else {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("UX Boost: ");
      stringBuilder1.append(str);
      Trace.asyncTraceEnd(64L, stringBuilder1.toString(), paramInt);
    } 
  }
  
  private void handleAppStatusChanged(AppStatusParam paramAppStatusParam) {
    // Byte code:
    //   0: aload_1
    //   1: ifnull -> 399
    //   4: aload_1
    //   5: getfield mStatus : I
    //   8: istore_2
    //   9: iload_2
    //   10: ifeq -> 389
    //   13: iload_2
    //   14: iconst_1
    //   15: if_icmpeq -> 165
    //   18: iload_2
    //   19: iconst_2
    //   20: if_icmpeq -> 59
    //   23: iload_2
    //   24: iconst_3
    //   25: if_icmpeq -> 31
    //   28: goto -> 399
    //   31: aload_0
    //   32: getfield mBoostingProcs : Landroid/util/SparseArray;
    //   35: astore_3
    //   36: aload_3
    //   37: monitorenter
    //   38: aload_0
    //   39: getfield mBoostingProcs : Landroid/util/SparseArray;
    //   42: aload_1
    //   43: getfield mPid : I
    //   46: invokevirtual remove : (I)V
    //   49: aload_3
    //   50: monitorexit
    //   51: goto -> 399
    //   54: astore_1
    //   55: aload_3
    //   56: monitorexit
    //   57: aload_1
    //   58: athrow
    //   59: aload_1
    //   60: getfield mData : Landroid/os/Bundle;
    //   63: ifnull -> 116
    //   66: ldc 'com.oppo.launcher'
    //   68: aload_1
    //   69: getfield mData : Landroid/os/Bundle;
    //   72: ldc_w 'packageName'
    //   75: invokevirtual getString : (Ljava/lang/String;)Ljava/lang/String;
    //   78: invokevirtual equals : (Ljava/lang/Object;)Z
    //   81: ifeq -> 116
    //   84: invokestatic obtain : ()Landroid/os/Message;
    //   87: astore_3
    //   88: aload_3
    //   89: iconst_2
    //   90: putfield what : I
    //   93: aload_1
    //   94: aconst_null
    //   95: putfield mData : Landroid/os/Bundle;
    //   98: aload_3
    //   99: aload_1
    //   100: putfield obj : Ljava/lang/Object;
    //   103: aload_0
    //   104: getfield mHandler : Landroid/os/Handler;
    //   107: aload_3
    //   108: ldc2_w 1000
    //   111: invokevirtual sendMessageDelayed : (Landroid/os/Message;J)Z
    //   114: pop
    //   115: return
    //   116: aload_0
    //   117: getfield mBoostingProcs : Landroid/util/SparseArray;
    //   120: astore_3
    //   121: aload_3
    //   122: monitorenter
    //   123: aload_0
    //   124: getfield mBoostingProcs : Landroid/util/SparseArray;
    //   127: aload_1
    //   128: getfield mPid : I
    //   131: invokevirtual get : (I)Ljava/lang/Object;
    //   134: checkcast android/util/IntArray
    //   137: astore #4
    //   139: aload_3
    //   140: monitorexit
    //   141: aload #4
    //   143: ifnull -> 399
    //   146: aload_0
    //   147: aload_1
    //   148: getfield mPid : I
    //   151: aload #4
    //   153: iconst_0
    //   154: invokespecial boostThreadsToUx : (ILandroid/util/IntArray;Z)V
    //   157: goto -> 399
    //   160: astore_1
    //   161: aload_3
    //   162: monitorexit
    //   163: aload_1
    //   164: athrow
    //   165: iconst_0
    //   166: istore #5
    //   168: iload #5
    //   170: istore_2
    //   171: aload_1
    //   172: getfield mData : Landroid/os/Bundle;
    //   175: ifnull -> 209
    //   178: iload #5
    //   180: istore_2
    //   181: ldc 'com.oppo.launcher'
    //   183: aload_1
    //   184: getfield mData : Landroid/os/Bundle;
    //   187: ldc_w 'packageName'
    //   190: invokevirtual getString : (Ljava/lang/String;)Ljava/lang/String;
    //   193: invokevirtual equals : (Ljava/lang/Object;)Z
    //   196: ifeq -> 209
    //   199: iconst_1
    //   200: istore_2
    //   201: aload_0
    //   202: getfield mHandler : Landroid/os/Handler;
    //   205: iconst_2
    //   206: invokevirtual removeMessages : (I)V
    //   209: aload_0
    //   210: getfield mBoostingProcs : Landroid/util/SparseArray;
    //   213: astore_3
    //   214: aload_3
    //   215: monitorenter
    //   216: aload_0
    //   217: getfield mBoostingProcs : Landroid/util/SparseArray;
    //   220: aload_1
    //   221: getfield mPid : I
    //   224: invokevirtual get : (I)Ljava/lang/Object;
    //   227: checkcast android/util/IntArray
    //   230: astore #4
    //   232: aload_3
    //   233: monitorexit
    //   234: aload #4
    //   236: ifnull -> 271
    //   239: aload #4
    //   241: aload_1
    //   242: getfield mRenderTid : I
    //   245: invokevirtual indexOf : (I)I
    //   248: iconst_m1
    //   249: if_icmpeq -> 271
    //   252: aload #4
    //   254: astore_3
    //   255: iload_2
    //   256: ifeq -> 341
    //   259: aload #4
    //   261: astore_3
    //   262: aload #4
    //   264: invokevirtual size : ()I
    //   267: iconst_2
    //   268: if_icmpgt -> 341
    //   271: aload_0
    //   272: aload_1
    //   273: getfield mPid : I
    //   276: invokevirtual getUxThreadForPid : (I)Landroid/util/IntArray;
    //   279: astore #4
    //   281: aload #4
    //   283: aload_1
    //   284: getfield mPid : I
    //   287: invokevirtual indexOf : (I)I
    //   290: iconst_m1
    //   291: if_icmpne -> 303
    //   294: aload #4
    //   296: aload_1
    //   297: getfield mPid : I
    //   300: invokevirtual add : (I)V
    //   303: aload #4
    //   305: astore_3
    //   306: aload_1
    //   307: getfield mRenderTid : I
    //   310: ifeq -> 341
    //   313: aload #4
    //   315: astore_3
    //   316: aload #4
    //   318: aload_1
    //   319: getfield mRenderTid : I
    //   322: invokevirtual indexOf : (I)I
    //   325: iconst_m1
    //   326: if_icmpne -> 341
    //   329: aload #4
    //   331: aload_1
    //   332: getfield mRenderTid : I
    //   335: invokevirtual add : (I)V
    //   338: aload #4
    //   340: astore_3
    //   341: aload_0
    //   342: aload_1
    //   343: getfield mPid : I
    //   346: aload_3
    //   347: iconst_1
    //   348: invokespecial boostThreadsToUx : (ILandroid/util/IntArray;Z)V
    //   351: aload_0
    //   352: getfield mBoostingProcs : Landroid/util/SparseArray;
    //   355: astore #4
    //   357: aload #4
    //   359: monitorenter
    //   360: aload_0
    //   361: getfield mBoostingProcs : Landroid/util/SparseArray;
    //   364: aload_1
    //   365: getfield mPid : I
    //   368: aload_3
    //   369: invokevirtual put : (ILjava/lang/Object;)V
    //   372: aload #4
    //   374: monitorexit
    //   375: goto -> 399
    //   378: astore_1
    //   379: aload #4
    //   381: monitorexit
    //   382: aload_1
    //   383: athrow
    //   384: astore_1
    //   385: aload_3
    //   386: monitorexit
    //   387: aload_1
    //   388: athrow
    //   389: aload_0
    //   390: aload_1
    //   391: getfield mPid : I
    //   394: iconst_0
    //   395: iconst_1
    //   396: invokevirtual setUxThread : (IIZ)V
    //   399: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #212	-> 0
    //   #216	-> 4
    //   #217	-> 4
    //   #274	-> 31
    //   #275	-> 38
    //   #276	-> 49
    //   #277	-> 51
    //   #276	-> 54
    //   #254	-> 59
    //   #255	-> 84
    //   #256	-> 88
    //   #257	-> 93
    //   #258	-> 98
    //   #259	-> 103
    //   #260	-> 115
    //   #263	-> 116
    //   #264	-> 123
    //   #265	-> 139
    //   #266	-> 141
    //   #267	-> 146
    //   #265	-> 160
    //   #223	-> 165
    //   #224	-> 168
    //   #225	-> 199
    //   #226	-> 201
    //   #229	-> 209
    //   #230	-> 216
    //   #231	-> 232
    //   #234	-> 234
    //   #235	-> 271
    //   #237	-> 281
    //   #238	-> 294
    //   #240	-> 303
    //   #241	-> 329
    //   #244	-> 341
    //   #245	-> 351
    //   #249	-> 360
    //   #250	-> 372
    //   #251	-> 375
    //   #250	-> 378
    //   #231	-> 384
    //   #220	-> 389
    //   #282	-> 399
    // Exception table:
    //   from	to	target	type
    //   38	49	54	finally
    //   49	51	54	finally
    //   55	57	54	finally
    //   123	139	160	finally
    //   139	141	160	finally
    //   161	163	160	finally
    //   216	232	384	finally
    //   232	234	384	finally
    //   360	372	378	finally
    //   372	375	378	finally
    //   379	382	378	finally
    //   385	387	384	finally
  }
  
  public void setfpsgoparamforperformance() {
    Log.d("OplusUIFirst", "set fps go para fo performance");
    writeProcNode("sys/module/fbt_cpu/parameters/bhr_opp", "15");
  }
  
  public void getfpsgoparamforreserve() {
    Log.d("OplusUIFirst", "get now origin fpsgo param");
    this.mbhropp = readProcNode("sys/module/fbt_cpu/parameters/bhr_opp");
  }
  
  public void backfpsgoparam() {
    Log.d("OplusUIFirst", "back to origin fpsgo param");
    writeProcNode("sys/module/fbt_cpu/parameters/bhr_opp", this.mbhropp);
  }
  
  public void onAppStatusChanged(int paramInt1, int paramInt2, Bundle paramBundle) {
    onAppStatusChanged(paramInt1, paramInt2, 0, paramBundle);
  }
  
  public void onAppStatusChanged(int paramInt1, int paramInt2, int paramInt3) {
    onAppStatusChanged(paramInt1, paramInt2, paramInt3, (Bundle)null);
  }
  
  public void onAppStatusChanged(int paramInt1, int paramInt2, String paramString, int paramInt3) {
    Bundle bundle = new Bundle();
    bundle.putString("packageName", paramString);
    onAppStatusChanged(paramInt1, paramInt2, paramInt3, bundle);
  }
  
  public void onAppStatusChanged(int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle) {
    AppStatusParam appStatusParam = new AppStatusParam();
    appStatusParam.mStatus = paramInt1;
    appStatusParam.mPid = paramInt2;
    appStatusParam.mRenderTid = paramInt3;
    appStatusParam.mData = paramBundle;
    Message message = Message.obtain();
    message.what = 0;
    message.obj = appStatusParam;
    this.mHandler.sendMessage(message);
  }
  
  private OplusUIFirstManager() {
    this.mTargetUxThreads.add("hwuiTask0");
    this.mTargetUxThreads.add("hwuiTask1");
    this.mFilterService = IDynamicFilterService.Stub.asInterface(ServiceManager.getService("dynamic_filter"));
    HandlerThread handlerThread = new HandlerThread("OplusUIFirst", -2);
    handlerThread.start();
    this.mHandler = new UIFirstHandler(this.mThread.getLooper());
    setUxThread(Process.myPid(), this.mThread.getThreadId(), "1");
  }
  
  private boolean inFilter(String paramString) {
    try {
      if (this.mFilterService != null) {
        boolean bool = this.mFilterService.inFilter("ui_first_black", paramString);
        if (bool)
          return true; 
      } 
    } catch (RemoteException remoteException) {}
    return false;
  }
  
  public void setUxThread(int paramInt1, int paramInt2, boolean paramBoolean) {
    String str1;
    if (paramInt1 == 0 || paramInt2 == 0)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("proc/");
    stringBuilder.append(paramInt1);
    stringBuilder.append("/task/");
    stringBuilder.append(paramInt2);
    stringBuilder.append("/static_ux");
    String str2 = stringBuilder.toString();
    if (paramBoolean) {
      str1 = "1";
    } else {
      str1 = "0";
    } 
    writeProcNode(str2, str1);
  }
  
  public void setUxThreadValue(String paramString1, int paramInt1, int paramInt2, String paramString2) {
    if (inFilter(paramString1))
      return; 
    setUxThread(paramInt1, paramInt2, paramString2);
  }
  
  public void setUxThreadValue(int paramInt1, int paramInt2, String paramString) {
    if (paramInt1 == 0 || paramInt2 == 0)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("proc/");
    stringBuilder.append(paramInt1);
    stringBuilder.append("/task/");
    stringBuilder.append(paramInt2);
    stringBuilder.append("/static_ux");
    String str = stringBuilder.toString();
    writeProcNode(str, paramString);
  }
  
  public void setUxThread(int paramInt1, int paramInt2, String paramString) {
    if (paramInt1 == 0)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("proc/");
    stringBuilder.append(paramInt1);
    stringBuilder.append("/task/");
    stringBuilder.append(paramInt1);
    stringBuilder.append("/static_ux");
    String str = stringBuilder.toString();
    writeProcNode(str, paramString);
    if (paramInt2 != 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("proc/");
      stringBuilder1.append(paramInt1);
      stringBuilder1.append("/task/");
      stringBuilder1.append(paramInt2);
      stringBuilder1.append("/static_ux");
      String str1 = stringBuilder1.toString();
      writeProcNode(str1, paramString);
    } 
    if ("1".equals(paramString)) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("UX Boost: ");
      stringBuilder1.append(paramInt1);
      Trace.asyncTraceBegin(64L, stringBuilder1.toString(), paramInt1);
    } else {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("UX Boost: ");
      stringBuilder1.append(paramInt1);
      Trace.asyncTraceEnd(64L, stringBuilder1.toString(), paramInt1);
    } 
  }
  
  public void writeProcNode(String paramString1, String paramString2) {
    File file = new File(paramString1);
    if (!file.exists())
      return; 
    FileOutputStream fileOutputStream1 = null;
    String str = null;
    paramString1 = str;
    FileOutputStream fileOutputStream2 = fileOutputStream1;
    try {
      FileOutputStream fileOutputStream4 = new FileOutputStream();
      paramString1 = str;
      fileOutputStream2 = fileOutputStream1;
      this(file);
      fileOutputStream1 = fileOutputStream4;
      FileOutputStream fileOutputStream3 = fileOutputStream1;
      fileOutputStream2 = fileOutputStream1;
      byte[] arrayOfByte = paramString2.getBytes(StandardCharsets.UTF_8);
      fileOutputStream3 = fileOutputStream1;
      fileOutputStream2 = fileOutputStream1;
      fileOutputStream1.write(arrayOfByte);
      try {
        fileOutputStream1.close();
      } catch (IOException iOException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("writeProcNode: ");
        stringBuilder.append(iOException.getMessage());
        Log.d("OplusUIFirst", stringBuilder.toString());
      } 
    } catch (IOException iOException) {
      FileOutputStream fileOutputStream = fileOutputStream2;
      StringBuilder stringBuilder = new StringBuilder();
      fileOutputStream = fileOutputStream2;
      this();
      fileOutputStream = fileOutputStream2;
      stringBuilder.append("writeProcNode: ");
      fileOutputStream = fileOutputStream2;
      stringBuilder.append(iOException.getMessage());
      fileOutputStream = fileOutputStream2;
      Log.d("OplusUIFirst", stringBuilder.toString());
      if (fileOutputStream2 != null)
        try {
          fileOutputStream2.close();
        } catch (IOException iOException1) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("writeProcNode: ");
          stringBuilder1.append(iOException1.getMessage());
          Log.d("OplusUIFirst", stringBuilder1.toString());
        }  
    } finally {}
  }
  
  public String readProcNode(String paramString) {
    StringBuilder stringBuilder1, stringBuilder5, stringBuilder2 = null, stringBuilder3 = null, stringBuilder4 = null;
    BufferedReader bufferedReader1 = null;
    File file = new File(paramString);
    if (!file.exists())
      return null; 
    BufferedReader bufferedReader2 = null;
    String str = null;
    paramString = str;
    BufferedReader bufferedReader3 = bufferedReader2;
    try {
      BufferedReader bufferedReader6 = new BufferedReader();
      paramString = str;
      bufferedReader3 = bufferedReader2;
      FileReader fileReader = new FileReader();
      paramString = str;
      bufferedReader3 = bufferedReader2;
      this(file);
      paramString = str;
      bufferedReader3 = bufferedReader2;
      this(fileReader);
      BufferedReader bufferedReader5 = bufferedReader6;
      BufferedReader bufferedReader4 = bufferedReader5;
      bufferedReader3 = bufferedReader5;
      String str1 = bufferedReader5.readLine();
      bufferedReader4 = bufferedReader1;
      if (str1 != null)
        String str2 = str1; 
      try {
        bufferedReader5.close();
      } catch (IOException iOException) {
        stringBuilder5 = new StringBuilder();
        stringBuilder5.append("readProcNode: ");
        stringBuilder5.append(iOException.getMessage());
        Log.d("OplusUIFirst", stringBuilder5.toString());
      } 
    } catch (IOException iOException) {
      stringBuilder1 = stringBuilder5;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder1 = stringBuilder5;
      this();
      stringBuilder1 = stringBuilder5;
      stringBuilder.append("readProcNode: ");
      stringBuilder1 = stringBuilder5;
      stringBuilder.append(iOException.getMessage());
      stringBuilder1 = stringBuilder5;
      Log.d("OplusUIFirst", stringBuilder.toString());
      stringBuilder1 = stringBuilder4;
      if (stringBuilder5 != null)
        try {
          stringBuilder5.close();
          stringBuilder1 = stringBuilder2;
        } catch (IOException iOException1) {
          stringBuilder5 = new StringBuilder();
          stringBuilder1 = stringBuilder3;
          stringBuilder5.append("readProcNode: ");
          stringBuilder5.append(iOException1.getMessage());
          Log.d("OplusUIFirst", stringBuilder5.toString());
        }  
    } finally {}
    return (String)stringBuilder1;
  }
  
  void uxTrace(int paramInt1, int paramInt2, int[] paramArrayOfint, boolean paramBoolean) {
    if (!Trace.isTagEnabled(64L))
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramInt1);
    if (paramInt2 > 0) {
      stringBuilder.append(",");
      stringBuilder.append(paramInt2);
    } 
    if (paramArrayOfint.length > 0) {
      Stream<?> stream = Arrays.stream(paramArrayOfint).filter((IntPredicate)_$$Lambda$OplusUIFirstManager$5LZasoX36fG7BMlh2fqMJnGBSbs.INSTANCE).mapToObj((IntFunction<?>)_$$Lambda$OplusUIFirstManager$KoQEdklWR3b1YR5EHqMsmvq8u3U.INSTANCE);
      String str1 = stream.collect(Collectors.joining(","));
      stringBuilder.append(",");
      stringBuilder.append(str1);
    } 
    String str = stringBuilder.toString();
    if (paramBoolean) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("UX Boost: ");
      stringBuilder.append(str);
      Trace.asyncTraceBegin(64L, stringBuilder.toString(), paramInt1);
    } else {
      stringBuilder = new StringBuilder();
      stringBuilder.append("UX Boost: ");
      stringBuilder.append(str);
      Trace.asyncTraceEnd(64L, stringBuilder.toString(), paramInt1);
    } 
  }
  
  public void adjustUxProcess(int paramInt1, int paramInt2, int paramInt3, IntArray paramIntArray, String paramString, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: aload #5
    //   3: invokespecial inFilter : (Ljava/lang/String;)Z
    //   6: ifeq -> 10
    //   9: return
    //   10: iload_1
    //   11: iconst_1
    //   12: if_icmpeq -> 169
    //   15: iload_1
    //   16: iconst_2
    //   17: if_icmpeq -> 23
    //   20: goto -> 372
    //   23: aload #5
    //   25: ifnull -> 69
    //   28: ldc 'com.oppo.launcher'
    //   30: aload #5
    //   32: invokevirtual equals : (Ljava/lang/Object;)Z
    //   35: ifeq -> 69
    //   38: aload_0
    //   39: getfield mHandler : Landroid/os/Handler;
    //   42: iconst_3
    //   43: iload_2
    //   44: iload_3
    //   45: aload #4
    //   47: invokevirtual toArray : ()[I
    //   50: invokestatic obtain : (Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;
    //   53: astore #4
    //   55: aload_0
    //   56: getfield mHandler : Landroid/os/Handler;
    //   59: aload #4
    //   61: ldc2_w 1000
    //   64: invokevirtual sendMessageDelayed : (Landroid/os/Message;J)Z
    //   67: pop
    //   68: return
    //   69: aload #5
    //   71: ifnull -> 107
    //   74: ldc_w 'com.android.systemui'
    //   77: aload #5
    //   79: invokevirtual equals : (Ljava/lang/Object;)Z
    //   82: ifeq -> 107
    //   85: ldc 'OplusUIFirst'
    //   87: ldc_w 'unset slide boost in system ui'
    //   90: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   93: pop
    //   94: aload_0
    //   95: ldc '/proc/sys/kernel/slide_boost_enabled'
    //   97: ldc_w '0'
    //   100: invokevirtual writeProcNode : (Ljava/lang/String;Ljava/lang/String;)V
    //   103: aload_0
    //   104: invokevirtual backfpsgoparam : ()V
    //   107: aload_0
    //   108: iload_2
    //   109: iload_2
    //   110: iconst_0
    //   111: invokevirtual setUxThread : (IIZ)V
    //   114: aload_0
    //   115: iload_2
    //   116: iload_3
    //   117: iconst_0
    //   118: invokevirtual setUxThread : (IIZ)V
    //   121: iconst_0
    //   122: istore_1
    //   123: iload_1
    //   124: aload #4
    //   126: invokevirtual size : ()I
    //   129: if_icmpge -> 154
    //   132: aload #4
    //   134: iload_1
    //   135: invokevirtual get : (I)I
    //   138: istore #7
    //   140: aload_0
    //   141: iload_2
    //   142: iload #7
    //   144: iconst_0
    //   145: invokevirtual setUxThread : (IIZ)V
    //   148: iinc #1, 1
    //   151: goto -> 123
    //   154: aload_0
    //   155: iload_2
    //   156: iload_3
    //   157: aload #4
    //   159: invokevirtual toArray : ()[I
    //   162: iconst_0
    //   163: invokevirtual uxTrace : (II[IZ)V
    //   166: goto -> 372
    //   169: aload #5
    //   171: ifnull -> 192
    //   174: ldc 'com.oppo.launcher'
    //   176: aload #5
    //   178: invokevirtual equals : (Ljava/lang/Object;)Z
    //   181: ifeq -> 192
    //   184: aload_0
    //   185: getfield mHandler : Landroid/os/Handler;
    //   188: iconst_3
    //   189: invokevirtual removeMessages : (I)V
    //   192: iload #6
    //   194: ifeq -> 204
    //   197: ldc '1'
    //   199: astore #8
    //   201: goto -> 209
    //   204: ldc_w '2'
    //   207: astore #8
    //   209: aload_0
    //   210: ldc '/proc/sys/kernel/launcher_boost_enabled'
    //   212: invokevirtual readProcNode : (Ljava/lang/String;)Ljava/lang/String;
    //   215: astore #9
    //   217: aload #8
    //   219: astore #10
    //   221: aload #5
    //   223: ifnull -> 269
    //   226: ldc 'com.oppo.launcher'
    //   228: aload #5
    //   230: invokevirtual equals : (Ljava/lang/Object;)Z
    //   233: ifne -> 265
    //   236: aload #8
    //   238: astore #10
    //   240: ldc_w 'com.android.systemui'
    //   243: aload #5
    //   245: invokevirtual equals : (Ljava/lang/Object;)Z
    //   248: ifeq -> 269
    //   251: aload #8
    //   253: astore #10
    //   255: ldc '1'
    //   257: aload #9
    //   259: invokevirtual equals : (Ljava/lang/Object;)Z
    //   262: ifeq -> 269
    //   265: ldc '1'
    //   267: astore #10
    //   269: aload_0
    //   270: iload_2
    //   271: iload_2
    //   272: aload #10
    //   274: invokevirtual setUxThreadValue : (IILjava/lang/String;)V
    //   277: aload_0
    //   278: iload_2
    //   279: iload_3
    //   280: aload #10
    //   282: invokevirtual setUxThreadValue : (IILjava/lang/String;)V
    //   285: aload #5
    //   287: ifnull -> 327
    //   290: ldc_w 'com.android.systemui'
    //   293: aload #5
    //   295: invokevirtual equals : (Ljava/lang/Object;)Z
    //   298: ifeq -> 327
    //   301: ldc 'OplusUIFirst'
    //   303: ldc_w 'set slide boost in system ui'
    //   306: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   309: pop
    //   310: aload_0
    //   311: invokevirtual getfpsgoparamforreserve : ()V
    //   314: aload_0
    //   315: invokevirtual setfpsgoparamforperformance : ()V
    //   318: aload_0
    //   319: ldc '/proc/sys/kernel/slide_boost_enabled'
    //   321: ldc_w '2'
    //   324: invokevirtual writeProcNode : (Ljava/lang/String;Ljava/lang/String;)V
    //   327: iconst_0
    //   328: istore_1
    //   329: iload_1
    //   330: aload #4
    //   332: invokevirtual size : ()I
    //   335: if_icmpge -> 360
    //   338: aload #4
    //   340: iload_1
    //   341: invokevirtual get : (I)I
    //   344: istore #7
    //   346: aload_0
    //   347: iload_2
    //   348: iload #7
    //   350: iconst_1
    //   351: invokevirtual setUxThread : (IIZ)V
    //   354: iinc #1, 1
    //   357: goto -> 329
    //   360: aload_0
    //   361: iload_2
    //   362: iload_3
    //   363: aload #4
    //   365: invokevirtual toArray : ()[I
    //   368: iconst_1
    //   369: invokevirtual uxTrace : (II[IZ)V
    //   372: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #494	-> 0
    //   #495	-> 9
    //   #497	-> 10
    //   #525	-> 23
    //   #526	-> 38
    //   #527	-> 55
    //   #528	-> 68
    //   #530	-> 69
    //   #531	-> 85
    //   #532	-> 94
    //   #533	-> 103
    //   #535	-> 107
    //   #536	-> 114
    //   #537	-> 121
    //   #538	-> 132
    //   #539	-> 140
    //   #537	-> 148
    //   #541	-> 154
    //   #499	-> 169
    //   #500	-> 184
    //   #503	-> 192
    //   #504	-> 209
    //   #505	-> 217
    //   #506	-> 236
    //   #507	-> 265
    //   #509	-> 269
    //   #510	-> 277
    //   #511	-> 285
    //   #512	-> 301
    //   #513	-> 310
    //   #514	-> 314
    //   #515	-> 318
    //   #517	-> 327
    //   #518	-> 338
    //   #519	-> 346
    //   #517	-> 354
    //   #521	-> 360
    //   #522	-> 372
    //   #544	-> 372
  }
}
