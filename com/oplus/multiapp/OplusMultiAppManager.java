package com.oplus.multiapp;

import android.os.UserHandle;
import android.util.Singleton;
import java.util.Arrays;
import java.util.List;

public class OplusMultiAppManager extends BaseOplusMultiAppManager {
  public static final String ACTION_MULTI_APP_ALIAS_CHANGED = "oppo.intent.action.MULTI_APP_RENAME";
  
  public static final String ACTION_MULTI_APP_CONFIG_CHANGED = "oppo.intent.action.MULTI_APP_CONFIG_CHANGED";
  
  public static final String ACTION_MULTI_APP_PACKAGE_ADDED = "oppo.intent.action.MULTI_APP_PACKAGE_ADDED";
  
  public static final String ACTION_MULTI_APP_PACKAGE_REMOVED = "oppo.intent.action.MULTI_APP_PACKAGE_REMOVED";
  
  public static final String EXTRA_ALIAS = "name";
  
  public static final String EXTRA_PACKAGE_NAME = "pkg";
  
  public static final String EXTRA_STATUS = "extra_status";
  
  public static final List<String> KEEP_CROSS_VOLUME_PKG = Arrays.asList(new String[] { "com.google.android.providers.media.module", "android" });
  
  public static final int LIST_TYPE_ACROSS_AUTHORITY = 4;
  
  public static final int LIST_TYPE_ALLOWED = 1;
  
  public static final int LIST_TYPE_CREATED = 0;
  
  public static final int LIST_TYPE_INSTALLED = 3;
  
  public static final int LIST_TYPE_RELATED = 2;
  
  public static final int MULTI_APP_STATUS_ADD = 1;
  
  public static final int MULTI_APP_STATUS_REMOVE = -1;
  
  public static final int RESULT_ERROR_NOT_SUPPORT = -2;
  
  public static final int RESULT_ERROR_NO_SPACE = -3;
  
  public static final int RESULT_FAILED = -1;
  
  public static final int RESULT_SUCCESS = 1;
  
  public static final String VOLUME_MAIN = "ace-0";
  
  public static final String VOLUME_MAIN_PATH = "/storage/ace-0";
  
  public static final String VOLUME_MULTI_APP = "ace-999";
  
  public static final String VOLUME_MULTI_APP_PATH = "/storage/ace-999";
  
  private static final Singleton<OplusMultiAppManager> sColorMultiAppManagerSingleton = new Singleton<OplusMultiAppManager>() {
      protected OplusMultiAppManager create() {
        return new OplusMultiAppManager();
      }
    };
  
  private OplusMultiAppManager() {}
  
  public static OplusMultiAppManager getInstance() {
    return (OplusMultiAppManager)sColorMultiAppManagerSingleton.get();
  }
  
  public int getMaxCreateNum() {
    return this.oplusMultiApp.getMaxCreateNum();
  }
  
  public List<String> getMultiAppList(int paramInt) {
    return this.oplusMultiApp.getMultiAppList(paramInt);
  }
  
  public boolean setMultiAppConfig(OplusMultiAppConfig paramOplusMultiAppConfig) {
    return this.oplusMultiApp.setMultiAppConfig(paramOplusMultiAppConfig);
  }
  
  public OplusMultiAppConfig getMultiAppConfig() {
    return this.oplusMultiApp.getMultiAppConfig();
  }
  
  public int setMultiAppStatus(String paramString, int paramInt) {
    return this.oplusMultiApp.setMultiAppPackageStatus(paramString, paramInt);
  }
  
  public boolean setMultiAppAlias(String paramString1, String paramString2) {
    return this.oplusMultiApp.setMultiAppAlias(paramString1, paramString2);
  }
  
  public String getMultiAppAlias(String paramString) {
    return this.oplusMultiApp.getMultiAppAlias(paramString);
  }
  
  public boolean isMultiAppSupport() {
    return this.oplusMultiApp.isMultiAppSupport();
  }
  
  public boolean isMultiAppUserHandle(UserHandle paramUserHandle) {
    return this.oplusMultiApp.isMultiAppUserHandle(paramUserHandle);
  }
  
  public UserHandle getMultiAppUserHandle() {
    return this.oplusMultiApp.getMultiAppUserHandle();
  }
}
