package com.oplus.multiapp;

import android.content.ComponentName;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.ArrayMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OplusMultiAppConfig implements Parcelable {
  private String mVersionName = "error";
  
  private int mVersionNum = -1;
  
  private volatile int mMaxCreatedNum = 2;
  
  private List<String> mRelatedPkgList = new ArrayList<>();
  
  private List<String> mAllowedPkgList = new ArrayList<>();
  
  private List<String> mCrossAuthorityList = new ArrayList<>();
  
  private List<ComponentName> mChooseRecentList = new ArrayList<>();
  
  private ArrayMap<String, List<ComponentName>> mChooseSkipMap = new ArrayMap();
  
  public String getVersionName() {
    return this.mVersionName;
  }
  
  public void setVersionName(String paramString) {
    this.mVersionName = paramString;
  }
  
  public int getVersionNum() {
    return this.mVersionNum;
  }
  
  public void setVersionNum(int paramInt) {
    this.mVersionNum = paramInt;
  }
  
  public int getMaxCreatedNum() {
    return this.mMaxCreatedNum;
  }
  
  public void setMaxCreatedNum(int paramInt) {
    this.mMaxCreatedNum = paramInt;
  }
  
  public List<String> getRelatedPkgList() {
    return this.mRelatedPkgList;
  }
  
  public void setRelatedPkgList(List<String> paramList) {
    this.mRelatedPkgList = paramList;
  }
  
  public List<String> getAllowedPkgList() {
    return this.mAllowedPkgList;
  }
  
  public void setAllowedPkgList(List<String> paramList) {
    this.mAllowedPkgList = paramList;
  }
  
  public List<ComponentName> getChooseRecentList() {
    return this.mChooseRecentList;
  }
  
  public void setChooseRecentList(List<ComponentName> paramList) {
    this.mChooseRecentList = paramList;
  }
  
  public ArrayMap<String, List<ComponentName>> getChooseSkipMap() {
    return this.mChooseSkipMap;
  }
  
  public void setChooseSkipMap(ArrayMap<String, List<ComponentName>> paramArrayMap) {
    this.mChooseSkipMap = paramArrayMap;
  }
  
  public List<String> getCrossAuthorityList() {
    return this.mCrossAuthorityList;
  }
  
  public void setCrossAuthorityList(List<String> paramList) {
    this.mCrossAuthorityList = paramList;
  }
  
  public OplusMultiAppConfig(Parcel paramParcel) {
    this.mVersionName = paramParcel.readString();
    this.mVersionNum = paramParcel.readInt();
    this.mMaxCreatedNum = paramParcel.readInt();
    paramParcel.readStringList(this.mAllowedPkgList);
    paramParcel.readStringList(this.mRelatedPkgList);
    this.mChooseRecentList = paramParcel.readArrayList(ComponentName.class.getClassLoader());
    paramParcel.readMap((Map)this.mChooseSkipMap, ArrayList.class.getClassLoader());
    paramParcel.readStringList(this.mCrossAuthorityList);
  }
  
  public static final Parcelable.Creator<OplusMultiAppConfig> CREATOR = new Parcelable.Creator<OplusMultiAppConfig>() {
      public OplusMultiAppConfig createFromParcel(Parcel param1Parcel) {
        return new OplusMultiAppConfig(param1Parcel);
      }
      
      public OplusMultiAppConfig[] newArray(int param1Int) {
        return new OplusMultiAppConfig[param1Int];
      }
    };
  
  public static final int CHOOSE_TYPE_NONE = -1;
  
  public static final int CHOOSE_TYPE_RECENT_TASK = 0;
  
  public static final int CHOOSE_TYPE_SKIP = 1;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mVersionName);
    paramParcel.writeInt(this.mVersionNum);
    paramParcel.writeInt(this.mMaxCreatedNum);
    paramParcel.writeStringList(this.mAllowedPkgList);
    paramParcel.writeStringList(this.mRelatedPkgList);
    paramParcel.writeList(this.mChooseRecentList);
    paramParcel.writeMap((Map)this.mChooseSkipMap);
    paramParcel.writeStringList(this.mCrossAuthorityList);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("MultiAppConfig[versionName: ");
    str = this.mVersionName;
    stringBuilder.append(str);
    stringBuilder.append(" num: ");
    int i = this.mVersionNum;
    stringBuilder.append(i);
    stringBuilder.append("]\n");
    stringBuilder.append("maxCreateNum[");
    stringBuilder.append(this.mMaxCreatedNum);
    stringBuilder.append(")]\n");
    stringBuilder.append("Allowed[\n");
    for (String str : this.mAllowedPkgList) {
      stringBuilder.append(str);
      stringBuilder.append("\n");
    } 
    stringBuilder.append("]\n");
    stringBuilder.append("Related[\n");
    for (String str1 : this.mRelatedPkgList) {
      stringBuilder.append(str1);
      stringBuilder.append("\n");
    } 
    stringBuilder.append("]\n");
    stringBuilder.append("Filter[\n");
    stringBuilder.append(" RecentTask(\n");
    for (ComponentName componentName : this.mChooseRecentList)
      stringBuilder.append(componentName); 
    stringBuilder.append(" )");
    stringBuilder.append("]\n");
    stringBuilder.append(" ChooseSkip[\n");
    for (String str1 : this.mChooseSkipMap.keySet()) {
      stringBuilder.append(str1);
      stringBuilder.append("->[\n");
      List list = (List)this.mChooseSkipMap.get(str1);
      if (list != null) {
        for (ComponentName componentName : list) {
          stringBuilder.append(componentName.toShortString());
          stringBuilder.append("\n");
        } 
      } else {
        stringBuilder.append("null");
        stringBuilder.append("\n");
      } 
      stringBuilder.append("]");
    } 
    stringBuilder.append("]\n");
    stringBuilder.append("CrossAuthority[\n");
    for (String str : this.mCrossAuthorityList) {
      stringBuilder.append(str);
      stringBuilder.append("\n");
    } 
    stringBuilder.append("]\n");
    return stringBuilder.toString();
  }
  
  public OplusMultiAppConfig() {}
}
