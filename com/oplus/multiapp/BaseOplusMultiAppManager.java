package com.oplus.multiapp;

import android.app.AppGlobals;
import android.app.Application;
import android.common.ColorFrameworkFactory;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.os.storage.VolumeInfo;
import android.text.TextUtils;
import android.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class BaseOplusMultiAppManager {
  public static final String ACTION_MULTI_APP_USER_UNLOCKED = "oppo.intent.action.MULTI_APP_USER_UNLOCKED";
  
  public static final boolean DEBUG_ALL = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  public static final String EXTERNAL_PATH = "/storage/emulated/";
  
  public static final String EXTERNAL_PRIMARY_MAIN_PATH = "/storage/emulated/0";
  
  public static final String EXTERNAL_PRIMARY_MULTIAPP_PATH = "/storage/emulated/999";
  
  public static final String MEDIA_PROVIDER_PACKAGE_NAME = "com.android.providers.media";
  
  public static final int MULTI_APP_VERSION = 2;
  
  private static final String TAG = "OplusMultiAppManager";
  
  public static final int USER_FLAG_MULTI_APP = 67108864;
  
  public static final int USER_ID_MULTI_APP = 999;
  
  public static final int USER_ID_ORIGINAL = 0;
  
  private static Map<Integer, StorageVolume> sStorageVolumes = new ConcurrentHashMap<>();
  
  IOplusMultiApp oplusMultiApp = ColorFrameworkFactory.getInstance().getOplusMultiApp();
  
  public static File getMultiAppVolumePath(String paramString) throws FileNotFoundException {
    if ("external_primary".equals(paramString)) {
      Application application = AppGlobals.getInitialApplication();
      StorageManager storageManager = (StorageManager)application.getSystemService(StorageManager.class);
      for (VolumeInfo volumeInfo : storageManager.getVolumes()) {
        File file = volumeInfo.getPathForUser(999);
        if (file != null && file.toString().startsWith("/storage/emulated/999"))
          return file; 
      } 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Failed to find path for ");
    stringBuilder.append(paramString);
    throw new FileNotFoundException(stringBuilder.toString());
  }
  
  public boolean isMultiAppUserId(int paramInt) {
    return this.oplusMultiApp.isMultiAppUserId(paramInt);
  }
  
  public boolean isCrossUserAuthority(String paramString, int paramInt) {
    return this.oplusMultiApp.isCrossUserAuthority(paramString, paramInt);
  }
  
  public boolean isProfileFilterPackage(String paramString) {
    return this.oplusMultiApp.isProfileFilterPackage(paramString);
  }
  
  public void scanFileIfNeed(int paramInt, String paramString) {
    if (TextUtils.isEmpty(paramString))
      return; 
    if (999 == paramInt || paramInt == 0)
      this.oplusMultiApp.scanFileIfNeed(paramInt, paramString); 
  }
  
  public static String redirectPath(int paramInt, String paramString) {
    String str2;
    if (TextUtils.isEmpty(paramString))
      return null; 
    String str1 = null;
    if (paramInt == 999) {
      if (paramString.startsWith("/storage/emulated/999")) {
        str2 = paramString.replace("/storage/emulated/999", "/storage/ace-999");
      } else {
        str2 = str1;
        if (paramString.startsWith("/storage/ace-0"))
          str2 = paramString.replace("/storage/ace-0", "/storage/emulated/0"); 
      } 
    } else {
      str2 = str1;
      if (paramInt == 0)
        if (paramString.startsWith("/storage/emulated/0")) {
          str2 = paramString.replace("/storage/emulated/0", "/storage/ace-0");
        } else {
          str2 = str1;
          if (paramString.startsWith("/storage/ace-999"))
            str2 = paramString.replace("/storage/ace-999", "/storage/emulated/999"); 
        }  
    } 
    return str2;
  }
  
  public static String getSharedParalledPathIfNeeded(String paramString1, String paramString2) {
    int i = UserHandle.myUserId();
    return getSharedParalledPathIfNeeded(paramString1, paramString2, i);
  }
  
  private static String getSharedParalledPathIfNeeded(String paramString1, String paramString2, int paramInt) {
    String str;
    boolean bool2, bool1 = false;
    if (paramString2 != null && paramString2.startsWith("/storage/emulated/")) {
      if (paramString2.startsWith("/storage/emulated/0") && paramInt == 999) {
        str = paramString2.replace("/storage/emulated/0", "/storage/ace-0");
        bool2 = true;
      } else {
        bool2 = bool1;
        str = paramString2;
        if (paramString2.startsWith("/storage/emulated/999")) {
          bool2 = bool1;
          str = paramString2;
          if (paramInt == 0) {
            str = paramString2.replace("/storage/emulated/999", "/storage/ace-999");
            bool2 = true;
          } 
        } 
      } 
    } else {
      bool2 = bool1;
      str = paramString2;
      if (paramString2 != null) {
        bool2 = bool1;
        str = paramString2;
        if (paramString2.startsWith("/storage/ace-999")) {
          bool2 = bool1;
          str = paramString2;
          if (paramInt == 999) {
            bool2 = true;
            str = paramString2.replace("/storage/ace-999", "/storage/emulated/999");
          } 
        } 
      } 
    } 
    if (bool2 && DEBUG_ALL) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getSharedParalledPathIfNeeded: path updated [");
      stringBuilder.append(paramString1);
      stringBuilder.append("] - ");
      stringBuilder.append(str);
      stringBuilder.append(" user = ");
      stringBuilder.append(paramInt);
      Log.d("OplusMultiAppManager", stringBuilder.toString(), new Throwable());
    } 
    return str;
  }
  
  public static boolean shouldRedirectToMainUser(Uri paramUri, Context paramContext) {
    if (paramUri == null || paramContext == null)
      return false; 
    int i = paramContext.getUserId();
    String str3 = paramUri.getScheme();
    String str2 = paramUri.getPath();
    String str1 = ContentProvider.getAuthorityWithoutUserId(paramUri.getAuthority());
    if (999 == i && "content".equals(str3) && 
      "media".equals(str1)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(File.separator);
      stringBuilder.append("internal");
      String str = stringBuilder.toString();
      if (str2.startsWith(str))
        return true; 
    } 
    return false;
  }
  
  public static StorageVolume getCoressUserStorageVolume(int paramInt) {
    char c;
    if (sStorageVolumes.containsKey(Integer.valueOf(paramInt)))
      return sStorageVolumes.get(Integer.valueOf(paramInt)); 
    if (paramInt == 999) {
      c = Character.MIN_VALUE;
    } else {
      if (paramInt == 0) {
        c = 'ϧ';
        StringBuilder stringBuilder3 = new StringBuilder();
        stringBuilder3.append("ace-");
        stringBuilder3.append(c);
        String str3 = stringBuilder3.toString();
        StringBuilder stringBuilder4 = new StringBuilder();
        stringBuilder4.append("/storage/");
        stringBuilder4.append(str3);
        File file1 = new File(stringBuilder4.toString());
        UserHandle userHandle1 = new UserHandle(paramInt);
        String str4 = Resources.getSystem().getString(17039374);
        StorageVolume storageVolume1 = new StorageVolume(str3, file1, file1, str4, false, false, true, false, 0L, userHandle1, str3, "mounted_ro");
        sStorageVolumes.put(Integer.valueOf(paramInt), storageVolume1);
        return storageVolume1;
      } 
      return null;
    } 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("ace-");
    stringBuilder1.append(c);
    String str1 = stringBuilder1.toString();
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("/storage/");
    stringBuilder2.append(str1);
    File file = new File(stringBuilder2.toString());
    UserHandle userHandle = new UserHandle(paramInt);
    String str2 = Resources.getSystem().getString(17039374);
    StorageVolume storageVolume = new StorageVolume(str1, file, file, str2, false, false, true, false, 0L, userHandle, str1, "mounted_ro");
    sStorageVolumes.put(Integer.valueOf(paramInt), storageVolume);
    return storageVolume;
  }
  
  public static Uri changeCrossUserVolume(Uri paramUri, ContentValues paramContentValues) {
    int i = UserHandle.myUserId();
    if (paramUri == null || i != 999)
      return paramUri; 
    String str = ContentProvider.getAuthorityWithoutUserId(paramUri.getAuthority());
    List<String> list = paramUri.getPathSegments();
    if (!"media".equals(str) || list == null || list.isEmpty())
      return paramUri; 
    str = list.get(0);
    if (!"external".equals(str) && !"external_primary".equals(str))
      return paramUri; 
    if (paramContentValues != null && paramContentValues.containsKey("_data"))
      paramContentValues.put("_data", getSharedParalledPathIfNeeded("insert", paramContentValues.getAsString("_data"), 0)); 
    return Uri.parse(paramUri.toString().replace(str, "ace-999"));
  }
}
