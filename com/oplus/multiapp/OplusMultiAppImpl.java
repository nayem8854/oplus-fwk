package com.oplus.multiapp;

import android.app.AppGlobals;
import android.app.OplusActivityManager;
import android.content.pm.IPackageManager;
import android.content.pm.PackageInfo;
import android.os.IUserManager;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Singleton;
import java.util.ArrayList;
import java.util.List;

public class OplusMultiAppImpl implements IOplusMultiApp {
  private static boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  static {
    ArrayList<String> arrayList = new ArrayList();
    arrayList.add("com.android.settings");
    FILTER_PROFILE_PACKAGE.add("com.android.managedprovisioning");
  }
  
  private Boolean mIsSupportCache = null;
  
  public OplusMultiAppImpl() {
    Log.e("MultiApp.Impl", "OplusMultiAppImpl");
  }
  
  private boolean enforceActivityManager() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield colorActivityManager : Landroid/app/OplusActivityManager;
    //   6: astore_1
    //   7: aload_1
    //   8: ifnonnull -> 61
    //   11: aconst_null
    //   12: astore_1
    //   13: invokestatic getService : ()Landroid/app/IActivityManager;
    //   16: astore_2
    //   17: aload_2
    //   18: astore_1
    //   19: goto -> 32
    //   22: astore_2
    //   23: ldc 'MultiApp.Impl'
    //   25: ldc 'enforceActivityManager error, ams not ready yet!'
    //   27: aload_2
    //   28: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   31: pop
    //   32: aload_1
    //   33: ifnonnull -> 48
    //   36: ldc 'MultiApp.Impl'
    //   38: ldc 'enforceActivityManager error, ams not ready yet!'
    //   40: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   43: pop
    //   44: aload_0
    //   45: monitorexit
    //   46: iconst_0
    //   47: ireturn
    //   48: new android/app/OplusActivityManager
    //   51: astore_1
    //   52: aload_1
    //   53: invokespecial <init> : ()V
    //   56: aload_0
    //   57: aload_1
    //   58: putfield colorActivityManager : Landroid/app/OplusActivityManager;
    //   61: aload_0
    //   62: monitorexit
    //   63: iconst_1
    //   64: ireturn
    //   65: astore_1
    //   66: aload_0
    //   67: monitorexit
    //   68: aload_1
    //   69: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #71	-> 2
    //   #72	-> 11
    //   #74	-> 13
    //   #77	-> 19
    //   #75	-> 22
    //   #76	-> 23
    //   #78	-> 32
    //   #79	-> 36
    //   #80	-> 44
    //   #82	-> 48
    //   #84	-> 61
    //   #70	-> 65
    // Exception table:
    //   from	to	target	type
    //   2	7	65	finally
    //   13	17	22	java/lang/Exception
    //   13	17	65	finally
    //   23	32	65	finally
    //   36	44	65	finally
    //   48	61	65	finally
  }
  
  private static final Singleton<IUserManager> iUserManagerSingleton = (Singleton<IUserManager>)new Object();
  
  private static final List<String> FILTER_PROFILE_PACKAGE;
  
  private static final String TAG = "MultiApp.Impl";
  
  private OplusActivityManager colorActivityManager;
  
  private static IUserManager getUserManagerService() {
    return (IUserManager)iUserManagerSingleton.get();
  }
  
  public List<String> getMultiAppList(int paramInt) {
    if (!isMultiAppSupport())
      return new ArrayList<>(); 
    if (3 == paramInt)
      try {
        return getInstalledPackages();
      } catch (Exception exception) {
        Log.e("MultiApp.Impl", "getMultiAppList ", exception);
        return new ArrayList<>();
      }  
    return this.colorActivityManager.getMultiAppList(paramInt);
  }
  
  public boolean setMultiAppConfig(OplusMultiAppConfig paramOplusMultiAppConfig) {
    boolean bool = isMultiAppSupport();
    boolean bool1 = false;
    if (!bool)
      return false; 
    try {
      int i = this.colorActivityManager.setMultiAppConfig(paramOplusMultiAppConfig);
      if (i >= 0)
        bool1 = true; 
      return bool1;
    } catch (Exception exception) {
      Log.e("MultiApp.Impl", "setMultiAppConfig", exception);
      return false;
    } 
  }
  
  public OplusMultiAppConfig getMultiAppConfig() {
    if (!isMultiAppSupport())
      return null; 
    try {
      return this.colorActivityManager.getMultiAppConfig();
    } catch (Exception exception) {
      Log.e("MultiApp.Impl", "setMultiAppConfig", exception);
      return null;
    } 
  }
  
  public String getMultiAppAlias(String paramString) {
    if (!isMultiAppSupport())
      return null; 
    try {
      return this.colorActivityManager.getMultiAppAlias(paramString);
    } catch (Exception exception) {
      Log.e("MultiApp.Impl", "getMultiAppAlias ", exception);
      return null;
    } 
  }
  
  public boolean setMultiAppAlias(String paramString1, String paramString2) {
    boolean bool = isMultiAppSupport();
    boolean bool1 = false;
    if (!bool)
      return false; 
    try {
      int i = this.colorActivityManager.setMultiAppAlias(paramString1, paramString2);
      if (i >= 0)
        bool1 = true; 
      return bool1;
    } catch (Exception exception) {
      Log.e("MultiApp.Impl", "setMultiAppAlias", exception);
      return false;
    } 
  }
  
  public int setMultiAppPackageStatus(String paramString, int paramInt) {
    if (!isMultiAppSupport())
      return -2; 
    if (TextUtils.isEmpty(paramString)) {
      Log.e("MultiApp.Impl", "setMultiAppPackageStatus pkgName is null");
      return -1;
    } 
    try {
      return this.colorActivityManager.setMultiAppStatus(paramString, paramInt);
    } catch (Exception exception) {
      Log.e("MultiApp.Impl", "setMultiAppStatus", exception);
      return -1;
    } 
  }
  
  public int getMaxCreateNum() {
    if (!isMultiAppSupport())
      return 0; 
    try {
      return this.colorActivityManager.getMultiAppMaxCreateNum();
    } catch (Exception exception) {
      Log.e("MultiApp.Impl", "getMaxCreateNum ", exception);
      return 0;
    } 
  }
  
  public boolean isMultiAppSupport() {
    try {
      if (this.mIsSupportCache == null) {
        if (!enforceActivityManager())
          return false; 
        this.mIsSupportCache = Boolean.valueOf(this.colorActivityManager.getIsSupportMultiApp());
      } 
      return this.mIsSupportCache.booleanValue();
    } catch (Exception exception) {
      Log.e("MultiApp.Impl", "getMultiAppList ", exception);
      return false;
    } 
  }
  
  public boolean isMultiAppUserHandle(UserHandle paramUserHandle) {
    boolean bool = isMultiAppSupport();
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (paramUserHandle == null) {
      Log.e("MultiApp.Impl", "isMultiAppUserHandle userHandle is null error!");
      return false;
    } 
    if (paramUserHandle.getIdentifier() == 999)
      bool1 = true; 
    return bool1;
  }
  
  public UserHandle getMultiAppUserHandle() {
    if (!isMultiAppSupport())
      return null; 
    IUserManager iUserManager = getUserManagerService();
    if (iUserManager == null) {
      Log.e("MultiApp.Impl", "getMultiAppUserHandle userManager is null");
      return null;
    } 
    byte b = 0;
    try {
      int[] arrayOfInt = iUserManager.getProfileIds(0, true);
      for (int i = arrayOfInt.length; b < i; ) {
        int j = arrayOfInt[b];
        if (999 == j)
          return new UserHandle(j); 
        b++;
      } 
    } catch (Exception exception) {
      Log.e("MultiApp.Impl", "getMultiAppUserHandle", exception);
    } 
    Log.v("MultiApp.Impl", "getMultiAppUserHandle return null");
    return null;
  }
  
  public boolean isMultiAppUserId(int paramInt) {
    boolean bool = isMultiAppSupport();
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (999 == paramInt)
      bool1 = true; 
    return bool1;
  }
  
  public boolean isCrossUserAuthority(String paramString, int paramInt) {
    if (!isMultiAppSupport())
      return false; 
    if (!isMultiAppUserId(paramInt))
      return false; 
    List<String> list = getMultiAppList(4);
    if (list != null && list.contains(paramString)) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("multi app ->  isCrossUserAuthority allow for ");
      stringBuilder1.append(paramString);
      Log.d("MultiApp.Impl", stringBuilder1.toString());
      return true;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("multi app ->  isCrossUserAuthority not allow for ");
    stringBuilder.append(paramString);
    Log.d("MultiApp.Impl", stringBuilder.toString());
    return false;
  }
  
  public boolean isProfileFilterPackage(String paramString) {
    if (!isMultiAppSupport())
      return false; 
    return FILTER_PROFILE_PACKAGE.contains(paramString);
  }
  
  public void scanFileIfNeed(int paramInt, String paramString) {
    if (!isMultiAppSupport())
      return; 
    try {
      this.colorActivityManager.scanFileIfNeed(paramInt, paramString);
    } catch (RemoteException remoteException) {
      Log.e("MultiApp.Impl", "scanFileIfNeed", (Throwable)remoteException);
    } 
  }
  
  private boolean isStorageLow() {
    IPackageManager iPackageManager = AppGlobals.getPackageManager();
    if (iPackageManager == null) {
      Log.e("MultiApp.Impl", "isStorageLow pm is null");
      return true;
    } 
    try {
      return iPackageManager.isStorageLow();
    } catch (Exception exception) {
      Log.e("MultiApp.Impl", "isStorageLow", exception);
      return true;
    } 
  }
  
  private List<String> getInstalledPackages() {
    if (!isMultiAppSupport())
      return new ArrayList<>(); 
    IPackageManager iPackageManager = AppGlobals.getPackageManager();
    if (iPackageManager == null) {
      Log.e("MultiApp.Impl", "getInstalledPackages pm is null");
      return new ArrayList<>();
    } 
    ArrayList<String> arrayList = new ArrayList();
    try {
      List list = iPackageManager.getInstalledPackages(0, 999).getList();
      for (PackageInfo packageInfo : list)
        arrayList.add(packageInfo.packageName); 
      return arrayList;
    } catch (Exception exception) {
      Log.e("MultiApp.Impl", "getInstalledPackages", exception);
      return arrayList;
    } 
  }
}
