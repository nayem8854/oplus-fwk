package com.oplus.multiapp;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.os.UserHandle;
import java.util.ArrayList;
import java.util.List;

public interface IOplusMultiApp extends IOplusCommonFeature {
  public static final IOplusMultiApp DEFAULT = (IOplusMultiApp)new Object();
  
  default OplusFeatureList.OplusIndex index() {
    return null;
  }
  
  default IOplusCommonFeature getDefault() {
    return DEFAULT;
  }
  
  default boolean isMultiAppSupport() {
    return false;
  }
  
  default int getMaxCreateNum() {
    return 0;
  }
  
  default List<String> getMultiAppList(int paramInt) {
    return new ArrayList<>();
  }
  
  default boolean setMultiAppConfig(OplusMultiAppConfig paramOplusMultiAppConfig) {
    return false;
  }
  
  default OplusMultiAppConfig getMultiAppConfig() {
    return null;
  }
  
  default String getMultiAppAlias(String paramString) {
    return "unkown";
  }
  
  default boolean setMultiAppAlias(String paramString1, String paramString2) {
    return false;
  }
  
  default boolean isMultiAppUserHandle(UserHandle paramUserHandle) {
    return false;
  }
  
  default UserHandle getMultiAppUserHandle() {
    return null;
  }
  
  default int setMultiAppPackageStatus(String paramString, int paramInt) {
    return -1;
  }
  
  default boolean isMultiAppUserId(int paramInt) {
    return false;
  }
  
  default boolean isCrossUserAuthority(String paramString, int paramInt) {
    return false;
  }
  
  default boolean isProfileFilterPackage(String paramString) {
    return false;
  }
  
  default void scanFileIfNeed(int paramInt, String paramString) {}
}
