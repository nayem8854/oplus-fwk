package com.oplus.multiapp;

import android.util.Log;
import java.lang.reflect.Constructor;

public abstract class BaseOplusCommonAppFactory {
  private static final String TAG = "BaseOplusCommonAppFactory";
  
  protected static Object newInstance(String paramString) throws Exception {
    Class<?> clazz = Class.forName(paramString);
    Constructor<?> constructor = clazz.getConstructor(new Class[0]);
    return constructor.newInstance(new Object[0]);
  }
  
  abstract IOplusMultiApp getOplusMultiApp();
  
  protected void warn(String paramString) {
    Log.w("BaseOplusCommonAppFactory", paramString);
  }
}
