package com.oplus.media;

import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.Surface;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;

public class OplusRecorder {
  public static final int HAMR_BITRATE = 12200;
  
  public static final int HAMR_BYTES_P_SEC = 1600;
  
  public static final int HWAV_BYTES_P_SEC = 88200;
  
  public static final int HWAV_SAMPLERATE = 44100;
  
  public static final int MEDIA_RECORDER_ERROR_UNKNOWN = 1;
  
  public static final int MEDIA_RECORDER_INFO_MAX_DURATION_REACHED = 800;
  
  public static final int MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED = 801;
  
  public static final int MEDIA_RECORDER_INFO_UNKNOWN = 1;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_COMPLETION_STATUS = 1000;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_DATA_KBYTES = 1009;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_DURATION_MS = 1003;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_ENCODED_FRAMES = 1005;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_INITIAL_DELAY_MS = 1007;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_LIST_END = 2000;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_LIST_START = 1000;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_MAX_CHUNK_DUR_MS = 1004;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_PROGRESS_IN_TIME = 1001;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_START_OFFSET_MS = 1008;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_TYPE = 1002;
  
  public static final int MEDIA_RECORDER_TRACK_INTER_CHUNK_TIME_MS = 1006;
  
  public static final int NAMR_BITRATE = 5150;
  
  public static final int NAMR_BYTES_P_SEC = 700;
  
  public static final int NWAV_BYTES_P_SEC = 16000;
  
  public static final int NWAV_SAMPLERATE = 8000;
  
  private static final String TAG = "OplusRecorder_Java";
  
  private EventHandler mEventHandler;
  
  private FileDescriptor mFd;
  
  private long mNativeContext;
  
  private OnErrorListener mOnErrorListener;
  
  private OnInfoListener mOnInfoListener;
  
  private String mPath;
  
  private Surface mSurface;
  
  static {
    Log.v("OplusRecorder_Java", "loadLibrary");
    System.loadLibrary("oplusrecorder");
    native_init();
  }
  
  public OplusRecorder() {
    Looper looper = Looper.myLooper();
    if (looper != null) {
      this.mEventHandler = new EventHandler(this, looper);
    } else {
      looper = Looper.getMainLooper();
      if (looper != null) {
        this.mEventHandler = new EventHandler(this, looper);
      } else {
        this.mEventHandler = null;
      } 
    } 
    Log.i("OplusRecorder_Java", "OplusRecorder()");
    native_setup(new WeakReference<>(this));
  }
  
  public void setPreviewDisplay(Surface paramSurface) {
    this.mSurface = paramSurface;
  }
  
  public final class AudioSource {
    public static final int CAMCORDER = 5;
    
    public static final int DEFAULT = 0;
    
    public static final int MIC = 1;
    
    public static final int VOICE_CALL = 4;
    
    public static final int VOICE_COMMUNICATION = 7;
    
    public static final int VOICE_DOWNLINK = 3;
    
    public static final int VOICE_RECOGNITION = 6;
    
    public static final int VOICE_UPLINK = 2;
    
    final OplusRecorder this$0;
  }
  
  public final class VideoSource {
    public static final int CAMERA = 1;
    
    public static final int DEFAULT = 0;
    
    public static final int GRALLOC_BUFFER = 2;
    
    final OplusRecorder this$0;
  }
  
  public final class OutputFormat {
    public static final int AAC_ADIF = 5;
    
    public static final int AAC_ADTS = 6;
    
    public static final int AMR_NB = 3;
    
    public static final int AMR_WB = 4;
    
    public static final int DEFAULT = 0;
    
    public static final int MP3 = 9;
    
    public static final int MPEG_4 = 2;
    
    public static final int OUTPUT_FORMAT_MPEG2TS = 8;
    
    public static final int OUTPUT_FORMAT_RTP_AVP = 7;
    
    public static final int RAW_AMR = 3;
    
    public static final int THREE_GPP = 1;
    
    public static final int WAV = 11;
    
    final OplusRecorder this$0;
  }
  
  public final class AudioEncoder {
    public static final int AAC = 3;
    
    public static final int AAC_ELD = 5;
    
    public static final int AMR_NB = 1;
    
    public static final int AMR_WB = 2;
    
    public static final int DEFAULT = 0;
    
    public static final int HE_AAC = 4;
    
    public static final int MPEG = 6;
    
    public static final int WAV = 8;
    
    final OplusRecorder this$0;
  }
  
  public final class VideoEncoder {
    public static final int DEFAULT = 0;
    
    public static final int H263 = 1;
    
    public static final int H264 = 2;
    
    public static final int MPEG_4_SP = 3;
    
    final OplusRecorder this$0;
  }
  
  public static final int getAudioSourceMax() {
    return 7;
  }
  
  public void setProfile(CamcorderProfile paramCamcorderProfile) {
    setOutputFormat(paramCamcorderProfile.fileFormat);
    setVideoFrameRate(paramCamcorderProfile.videoFrameRate);
    setVideoSize(paramCamcorderProfile.videoFrameWidth, paramCamcorderProfile.videoFrameHeight);
    setVideoEncodingBitRate(paramCamcorderProfile.videoBitRate);
    setVideoEncoder(paramCamcorderProfile.videoCodec);
    if (paramCamcorderProfile.quality < 1000 || paramCamcorderProfile.quality > 1007) {
      setAudioEncodingBitRate(paramCamcorderProfile.audioBitRate);
      setAudioChannels(paramCamcorderProfile.audioChannels);
      setAudioSamplingRate(paramCamcorderProfile.audioSampleRate);
      setAudioEncoder(paramCamcorderProfile.audioCodec);
    } 
  }
  
  public void setCaptureRate(double paramDouble) {
    setParameter(String.format("time-lapse-enable=1", new Object[0]));
    paramDouble = 1.0D / paramDouble;
    int i = (int)(1000.0D * paramDouble);
    setParameter(String.format("time-between-time-lapse-frame-capture=%d", new Object[] { Integer.valueOf(i) }));
  }
  
  public void setOrientationHint(int paramInt) {
    if (paramInt == 0 || paramInt == 90 || paramInt == 180 || paramInt == 270) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("video-param-rotation-angle-degrees=");
      stringBuilder1.append(paramInt);
      setParameter(stringBuilder1.toString());
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported angle: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void setLocation(float paramFloat1, float paramFloat2) {
    int i = (int)((paramFloat1 * 10000.0F) + 0.5D);
    int j = (int)((10000.0F * paramFloat2) + 0.5D);
    if (i <= 900000 && i >= -900000) {
      if (j <= 1800000 && j >= -1800000) {
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("param-geotag-latitude=");
        stringBuilder2.append(i);
        setParameter(stringBuilder2.toString());
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append("param-geotag-longitude=");
        stringBuilder2.append(j);
        setParameter(stringBuilder2.toString());
        return;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Longitude: ");
      stringBuilder1.append(paramFloat2);
      stringBuilder1.append(" out of range");
      String str1 = stringBuilder1.toString();
      throw new IllegalArgumentException(str1);
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Latitude: ");
    stringBuilder.append(paramFloat1);
    stringBuilder.append(" out of range.");
    String str = stringBuilder.toString();
    throw new IllegalArgumentException(str);
  }
  
  public void setAudioSamplingRate(int paramInt) {
    if (paramInt > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("audio-param-sampling-rate=");
      stringBuilder.append(paramInt);
      setParameter(stringBuilder.toString());
      return;
    } 
    throw new IllegalArgumentException("Audio sampling rate is not positive");
  }
  
  public void setAudioChannels(int paramInt) {
    if (paramInt > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("audio-param-number-of-channels=");
      stringBuilder.append(paramInt);
      setParameter(stringBuilder.toString());
      return;
    } 
    throw new IllegalArgumentException("Number of channels is not positive");
  }
  
  public void setAudioEncodingBitRate(int paramInt) {
    if (paramInt > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("audio-param-encoding-bitrate=");
      stringBuilder.append(paramInt);
      setParameter(stringBuilder.toString());
      return;
    } 
    throw new IllegalArgumentException("Audio encoding bit rate is not positive");
  }
  
  public void setVideoEncodingBitRate(int paramInt) {
    if (paramInt > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("video-param-encoding-bitrate=");
      stringBuilder.append(paramInt);
      setParameter(stringBuilder.toString());
      return;
    } 
    throw new IllegalArgumentException("Video encoding bit rate is not positive");
  }
  
  public void setAuxiliaryOutputFile(FileDescriptor paramFileDescriptor) {
    Log.w("OplusRecorder_Java", "setAuxiliaryOutputFile(FileDescriptor) is no longer supported.");
  }
  
  public void setAuxiliaryOutputFile(String paramString) {
    Log.w("OplusRecorder_Java", "setAuxiliaryOutputFile(String) is no longer supported.");
  }
  
  public void setOutputFile(FileDescriptor paramFileDescriptor) throws IllegalStateException {
    this.mPath = null;
    this.mFd = paramFileDescriptor;
  }
  
  public void setOutputFile(String paramString) throws IllegalStateException {
    this.mFd = null;
    this.mPath = paramString;
  }
  
  public void prepare() throws IllegalStateException, IOException {
    if (this.mPath != null) {
      FileOutputStream fileOutputStream = new FileOutputStream(this.mPath);
      try {
        _setOutputFile(fileOutputStream.getFD(), 0L, 0L);
      } finally {
        fileOutputStream.close();
      } 
    } else {
      FileDescriptor fileDescriptor = this.mFd;
      if (fileDescriptor != null) {
        _setOutputFile(fileDescriptor, 0L, 0L);
      } else {
        throw new IOException("No valid output file");
      } 
    } 
    _prepare();
  }
  
  public void reset() {
    native_reset();
    this.mEventHandler.removeCallbacksAndMessages(null);
  }
  
  public void setOnErrorListener(OnErrorListener paramOnErrorListener) {
    this.mOnErrorListener = paramOnErrorListener;
  }
  
  public void setOnInfoListener(OnInfoListener paramOnInfoListener) {
    this.mOnInfoListener = paramOnInfoListener;
  }
  
  class EventHandler extends Handler {
    private static final int MEDIA_RECORDER_EVENT_ERROR = 1;
    
    private static final int MEDIA_RECORDER_EVENT_INFO = 2;
    
    private static final int MEDIA_RECORDER_EVENT_LIST_END = 99;
    
    private static final int MEDIA_RECORDER_EVENT_LIST_START = 1;
    
    private static final int MEDIA_RECORDER_TRACK_EVENT_ERROR = 100;
    
    private static final int MEDIA_RECORDER_TRACK_EVENT_INFO = 101;
    
    private static final int MEDIA_RECORDER_TRACK_EVENT_LIST_END = 1000;
    
    private static final int MEDIA_RECORDER_TRACK_EVENT_LIST_START = 100;
    
    private OplusRecorder mOplusRecorder;
    
    final OplusRecorder this$0;
    
    public EventHandler(OplusRecorder param1OplusRecorder1, Looper param1Looper) {
      super(param1Looper);
      this.mOplusRecorder = param1OplusRecorder1;
    }
    
    public void handleMessage(Message param1Message) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mOplusRecorder : Lcom/oplus/media/OplusRecorder;
      //   4: invokestatic access$000 : (Lcom/oplus/media/OplusRecorder;)J
      //   7: lconst_0
      //   8: lcmp
      //   9: ifne -> 21
      //   12: ldc 'OplusRecorder_Java'
      //   14: ldc 'OplusRecorder went away with unhandled events'
      //   16: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   19: pop
      //   20: return
      //   21: aload_1
      //   22: getfield what : I
      //   25: istore_2
      //   26: iload_2
      //   27: iconst_1
      //   28: if_icmpeq -> 118
      //   31: iload_2
      //   32: iconst_2
      //   33: if_icmpeq -> 83
      //   36: iload_2
      //   37: bipush #100
      //   39: if_icmpeq -> 118
      //   42: iload_2
      //   43: bipush #101
      //   45: if_icmpeq -> 83
      //   48: new java/lang/StringBuilder
      //   51: dup
      //   52: invokespecial <init> : ()V
      //   55: astore_3
      //   56: aload_3
      //   57: ldc 'Unknown message type '
      //   59: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   62: pop
      //   63: aload_3
      //   64: aload_1
      //   65: getfield what : I
      //   68: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   71: pop
      //   72: ldc 'OplusRecorder_Java'
      //   74: aload_3
      //   75: invokevirtual toString : ()Ljava/lang/String;
      //   78: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   81: pop
      //   82: return
      //   83: aload_0
      //   84: getfield this$0 : Lcom/oplus/media/OplusRecorder;
      //   87: invokestatic access$200 : (Lcom/oplus/media/OplusRecorder;)Lcom/oplus/media/OplusRecorder$OnInfoListener;
      //   90: ifnull -> 117
      //   93: aload_0
      //   94: getfield this$0 : Lcom/oplus/media/OplusRecorder;
      //   97: invokestatic access$200 : (Lcom/oplus/media/OplusRecorder;)Lcom/oplus/media/OplusRecorder$OnInfoListener;
      //   100: aload_0
      //   101: getfield mOplusRecorder : Lcom/oplus/media/OplusRecorder;
      //   104: aload_1
      //   105: getfield arg1 : I
      //   108: aload_1
      //   109: getfield arg2 : I
      //   112: invokeinterface onInfo : (Lcom/oplus/media/OplusRecorder;II)V
      //   117: return
      //   118: aload_0
      //   119: getfield this$0 : Lcom/oplus/media/OplusRecorder;
      //   122: invokestatic access$100 : (Lcom/oplus/media/OplusRecorder;)Lcom/oplus/media/OplusRecorder$OnErrorListener;
      //   125: ifnull -> 152
      //   128: aload_0
      //   129: getfield this$0 : Lcom/oplus/media/OplusRecorder;
      //   132: invokestatic access$100 : (Lcom/oplus/media/OplusRecorder;)Lcom/oplus/media/OplusRecorder$OnErrorListener;
      //   135: aload_0
      //   136: getfield mOplusRecorder : Lcom/oplus/media/OplusRecorder;
      //   139: aload_1
      //   140: getfield arg1 : I
      //   143: aload_1
      //   144: getfield arg2 : I
      //   147: invokeinterface onError : (Lcom/oplus/media/OplusRecorder;II)V
      //   152: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #875	-> 0
      //   #876	-> 12
      //   #877	-> 20
      //   #879	-> 21
      //   #895	-> 48
      //   #896	-> 82
      //   #889	-> 83
      //   #890	-> 93
      //   #892	-> 117
      //   #882	-> 118
      //   #883	-> 128
      //   #885	-> 152
    }
  }
  
  private static void postEventFromNative(Object paramObject1, int paramInt1, int paramInt2, int paramInt3, Object paramObject2) {
    paramObject1 = ((WeakReference<OplusRecorder>)paramObject1).get();
    if (paramObject1 == null)
      return; 
    EventHandler eventHandler = ((OplusRecorder)paramObject1).mEventHandler;
    if (eventHandler != null) {
      paramObject2 = eventHandler.obtainMessage(paramInt1, paramInt2, paramInt3, paramObject2);
      ((OplusRecorder)paramObject1).mEventHandler.sendMessage((Message)paramObject2);
    } 
  }
  
  protected void finalize() {
    native_finalize();
  }
  
  private native void _prepare() throws IllegalStateException, IOException;
  
  private native void _setOutputFile(FileDescriptor paramFileDescriptor, long paramLong1, long paramLong2) throws IllegalStateException, IOException;
  
  private final native void native_finalize();
  
  private static final native void native_init();
  
  private native void native_reset();
  
  private final native void native_setup(Object paramObject) throws IllegalStateException;
  
  private native void setParameter(String paramString);
  
  public native void expandFile(FileDescriptor paramFileDescriptor, long paramLong1, long paramLong2, int paramInt) throws IllegalStateException;
  
  public native void expandFile(String paramString, int paramInt) throws IllegalStateException;
  
  public native int getMaxAmplitude() throws IllegalStateException;
  
  public native int getduration();
  
  public native void pause() throws IllegalStateException;
  
  public native void release();
  
  public native void resume() throws IllegalStateException;
  
  public native void setAudioEncoder(int paramInt) throws IllegalStateException;
  
  public native void setAudioSource(int paramInt) throws IllegalStateException;
  
  public native void setCamera(Camera paramCamera);
  
  public native void setMaxDuration(int paramInt) throws IllegalArgumentException;
  
  public native void setMaxFileSize(long paramLong) throws IllegalArgumentException;
  
  public native void setOutputFormat(int paramInt) throws IllegalStateException;
  
  public native void setVideoEncoder(int paramInt) throws IllegalStateException;
  
  public native void setVideoFrameRate(int paramInt) throws IllegalStateException;
  
  public native void setVideoSize(int paramInt1, int paramInt2) throws IllegalStateException;
  
  public native void setVideoSource(int paramInt) throws IllegalStateException;
  
  public native void start() throws IllegalStateException;
  
  public native void stop() throws IllegalStateException;
  
  public static interface OnErrorListener {
    void onError(OplusRecorder param1OplusRecorder, int param1Int1, int param1Int2);
  }
  
  public static interface OnInfoListener {
    void onInfo(OplusRecorder param1OplusRecorder, int param1Int1, int param1Int2);
  }
}
