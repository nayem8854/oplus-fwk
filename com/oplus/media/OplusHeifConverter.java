package com.oplus.media;

import android.graphics.ColorSpace;
import android.graphics.Rect;
import android.util.Log;
import android.view.Surface;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;

public class OplusHeifConverter {
  private static final int DECODE_BUFFER_SIZE = 16384;
  
  private static final String TAG = "OplusHeifConverter_Java";
  
  private static final int ftyp_box = 1718909296;
  
  private static final int heic_box = 1751476579;
  
  private static final int mif1_box = 1835623985;
  
  private long m10BitObject;
  
  private Surface mSurface;
  
  static {
    Log.v("OplusHeifConverter_Java", "loadLibrary");
    System.loadLibrary("oplus_heifconverter");
  }
  
  public class HeifDecodedFrame {
    public ColorSpace m_ColorSpace;
    
    public long m_buffer_id;
    
    public long m_buffer_id_sub;
    
    public int m_frame_height;
    
    public int m_frame_width;
    
    public boolean m_recycled;
    
    public byte[] m_yuvdata;
    
    final OplusHeifConverter this$0;
    
    public final boolean isRecycled() {
      return this.m_recycled;
    }
    
    public void recycle() {
      if (this.m_recycled)
        return; 
      OplusHeifConverter.nativeRecycle(this.m_buffer_id, this.m_buffer_id_sub);
      this.m_yuvdata = null;
      this.m_recycled = true;
    }
    
    public boolean render(Surface param1Surface, boolean param1Boolean) {
      boolean bool = false;
      if (this.m_recycled)
        return true; 
      if (param1Boolean) {
        try {
          param1Boolean = OplusHeifConverter.nativeRenderDirectBuffer(this.m_buffer_id, this.m_buffer_id_sub, this.m_frame_width, this.m_frame_height, param1Surface);
        } catch (Exception exception) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unable to native10BitSetSurfaceYUVdata stream: ");
          stringBuilder.append(exception);
          Log.e("OplusHeifConverter_Java", stringBuilder.toString());
          param1Boolean = bool;
        } 
      } else {
        param1Boolean = OplusHeifConverter.nativeRender(this.m_yuvdata, this.m_frame_width, this.m_frame_height, (Surface)exception);
      } 
      return param1Boolean;
    }
    
    public HeifDecodedFrame(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2, long param1Long) {
      this.m_yuvdata = param1ArrayOfbyte;
      this.m_frame_width = param1Int1;
      this.m_frame_height = param1Int2;
      this.m_buffer_id = param1Long;
    }
  }
  
  public static boolean convertHeifToJpegFromPath(String paramString, int paramInt, OutputStream paramOutputStream) {
    byte[] arrayOfByte = new byte[16384];
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(" ConvertHeif2JPEG start! quality ###");
    stringBuilder.append(paramInt);
    Log.e("OplusHeifConverter_Java", stringBuilder.toString());
    if (paramOutputStream != null) {
      if (paramInt >= 0 && paramInt <= 100) {
        boolean bool1 = false;
        FileInputStream fileInputStream1 = null;
        StringBuilder stringBuilder1 = null;
        stringBuilder = stringBuilder1;
        boolean bool2 = bool1;
        FileInputStream fileInputStream2 = fileInputStream1;
        try {
          FileInputStream fileInputStream5 = new FileInputStream();
          stringBuilder = stringBuilder1;
          bool2 = bool1;
          fileInputStream2 = fileInputStream1;
          this(paramString);
          FileInputStream fileInputStream3 = fileInputStream5;
          FileInputStream fileInputStream4 = fileInputStream3;
          bool2 = bool1;
          fileInputStream2 = fileInputStream3;
          Log.e("OplusHeifConverter_Java", " ConvertHeif2JPEG start");
          fileInputStream4 = fileInputStream3;
          bool2 = bool1;
          fileInputStream2 = fileInputStream3;
          bool1 = nativeHeifConvert(fileInputStream3, arrayOfByte, paramInt, paramOutputStream, new byte[4096]);
          fileInputStream4 = fileInputStream3;
          bool2 = bool1;
          fileInputStream2 = fileInputStream3;
          StringBuilder stringBuilder2 = new StringBuilder();
          fileInputStream4 = fileInputStream3;
          bool2 = bool1;
          fileInputStream2 = fileInputStream3;
          this();
          fileInputStream4 = fileInputStream3;
          bool2 = bool1;
          fileInputStream2 = fileInputStream3;
          stringBuilder2.append(" ConvertHeif2JPEG result: ");
          fileInputStream4 = fileInputStream3;
          bool2 = bool1;
          fileInputStream2 = fileInputStream3;
          stringBuilder2.append(bool1);
          fileInputStream4 = fileInputStream3;
          bool2 = bool1;
          fileInputStream2 = fileInputStream3;
          Log.e("OplusHeifConverter_Java", stringBuilder2.toString());
          boolean bool = bool1;
          try {
            fileInputStream3.close();
            bool2 = bool1;
            bool1 = bool2;
          } catch (IOException iOException) {
            bool2 = bool;
            bool1 = bool2;
          } 
        } catch (Exception exception) {
          FileInputStream fileInputStream = fileInputStream2;
          StringBuilder stringBuilder2 = new StringBuilder();
          fileInputStream = fileInputStream2;
          this();
          fileInputStream = fileInputStream2;
          stringBuilder2.append("Unable to ConvertHeif2JPEG stream: ");
          fileInputStream = fileInputStream2;
          stringBuilder2.append(exception);
          fileInputStream = fileInputStream2;
          Log.e("OplusHeifConverter_Java", stringBuilder2.toString());
          bool1 = bool2;
          if (fileInputStream2 != null) {
            boolean bool = bool2;
            fileInputStream2.close();
          } else {
            return bool1;
          } 
          bool1 = bool2;
        } finally {}
        return bool1;
      } 
      throw new IllegalArgumentException("quality must be 0..100");
    } 
    throw null;
  }
  
  public static boolean convertHeifToJpegFromStream(InputStream paramInputStream, int paramInt, OutputStream paramOutputStream) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(" convertHeif2JPEGFromStream start! quality ###");
    stringBuilder.append(paramInt);
    Log.e("OplusHeifConverter_Java", stringBuilder.toString());
    byte[] arrayOfByte = new byte[16384];
    if (paramOutputStream != null) {
      if (paramInt >= 0 && paramInt <= 100) {
        boolean bool1 = false;
        boolean bool2 = bool1;
        try {
          Log.e("OplusHeifConverter_Java", " ConvertHeif2JPEG start");
          bool2 = bool1;
          bool1 = nativeHeifConvert(paramInputStream, arrayOfByte, paramInt, paramOutputStream, new byte[4096]);
          bool2 = bool1;
          StringBuilder stringBuilder1 = new StringBuilder();
          bool2 = bool1;
          this();
          bool2 = bool1;
          stringBuilder1.append(" ConvertHeif2JPEG result: ");
          bool2 = bool1;
          stringBuilder1.append(bool1);
          bool2 = bool1;
          Log.e("OplusHeifConverter_Java", stringBuilder1.toString());
          bool2 = bool1;
        } catch (Exception exception) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Unable to ConvertHeif2JPEG stream: ");
          stringBuilder1.append(exception);
          Log.e("OplusHeifConverter_Java", stringBuilder1.toString());
        } 
        return bool2;
      } 
      throw new IllegalArgumentException("quality must be 0..100");
    } 
    throw null;
  }
  
  public int byteArrayToInt(byte[] paramArrayOfbyte, int paramInt) {
    return paramArrayOfbyte[paramInt + 3] & 0xFF | (paramArrayOfbyte[paramInt + 2] & 0xFF) << 8 | (paramArrayOfbyte[paramInt + 1] & 0xFF) << 16 | (paramArrayOfbyte[paramInt] & 0xFF) << 24;
  }
  
  private int getBoxInfo(HashSet<Integer> paramHashSet) {
    paramHashSet.add(Integer.valueOf(1751476579));
    paramHashSet.add(Integer.valueOf(1835623985));
    return 1718909296;
  }
  
  public boolean isHEIFFile(InputStream paramInputStream) {
    HashSet hashSet = new HashSet();
    int i = getBoxInfo(hashSet);
    try {
      DataInputStream dataInputStream = new DataInputStream();
      try {
        this(paramInputStream);
        byte[] arrayOfByte = new byte[1024];
        int j = dataInputStream.read(arrayOfByte);
        if (j > 8 && byteArrayToInt(arrayOfByte, 4) == i) {
          i = byteArrayToInt(arrayOfByte, 0);
          if (i <= 8) {
            Log.d("OplusHeifConverter_Java", "buffer not enought.");
            return false;
          } 
          int k = (i - 8) / 4;
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("brandCnt ");
          stringBuilder.append(k);
          stringBuilder.append(" size ");
          stringBuilder.append(i);
          Log.d("OplusHeifConverter_Java", stringBuilder.toString());
          for (i = 0; i < k && 
            i * 4 + 8 + 4 < j; i++) {
            int m = byteArrayToInt(arrayOfByte, i * 4 + 8);
            hashSet.remove(Integer.valueOf(m));
            stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("remove box ");
            stringBuilder.append(m);
            Log.d("OplusHeifConverter_Java", stringBuilder.toString());
          } 
          if (hashSet.isEmpty())
            return true; 
        } else {
          Log.d("OplusHeifConverter_Java", "Not mov file.");
          return false;
        } 
      } catch (FileNotFoundException null) {
        iOException.printStackTrace();
      } catch (IOException null) {}
    } catch (FileNotFoundException null) {
    
    } catch (IOException iOException) {
      iOException.printStackTrace();
    } 
    iOException.printStackTrace();
  }
  
  public int getFormat(InputStream paramInputStream) throws IOException {
    int i = 1835365473;
    int j = 1768977008;
    boolean bool = true;
    int k = paramInputStream.available();
    byte[] arrayOfByte = new byte[k];
    paramInputStream = new BufferedInputStream(paramInputStream);
    paramInputStream.read(arrayOfByte);
    int m = byteArrayToInt(arrayOfByte, 0);
    int n = 0 + 4;
    int i1 = byteArrayToInt(arrayOfByte, n);
    if (i1 != 1718909296) {
      Log.e("OplusHeifConverter_Java", "not a heif file!");
      paramInputStream.close();
      return 0;
    } 
    i1 = n + 4 + m - 8;
    while (bool) {
      m = byteArrayToInt(arrayOfByte, i1);
      i1 += 4;
      n = byteArrayToInt(arrayOfByte, i1);
      i1 += 4;
      if (n == 1835295092) {
        if (m == 1) {
          m = byteArrayToInt(arrayOfByte, i1);
          n = i1 + 4;
          i1 = byteArrayToInt(arrayOfByte, n);
          i1 = n + 4 + (m << 8) + i1 - 16;
        } 
      } else if (n == i) {
        i1 += 4;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("parse meta_box cur ");
        stringBuilder.append(i1);
        Log.d("OplusHeifConverter_Java", stringBuilder.toString());
      } else if (n == j) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("parse iprp_box cur  ");
        stringBuilder.append(i1);
        Log.d("OplusHeifConverter_Java", stringBuilder.toString());
      } else if (n == 1768973167) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("parse ipco_box cur  ");
        stringBuilder.append(i1);
        Log.d("OplusHeifConverter_Java", stringBuilder.toString());
      } else {
        if (n == 1752589123) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("cur hvcC_box  ");
          stringBuilder.append(i1);
          Log.d("OplusHeifConverter_Java", stringBuilder.toString());
          j = i1 + 17;
          i1 = arrayOfByte[j];
          j++;
          j = arrayOfByte[j];
          if ((i1 & 0x3) == 2 && (j & 0x3) == 2) {
            paramInputStream.close();
            Log.d("OplusHeifConverter_Java", "It is 10Bit Heif!");
            return 1;
          } 
          paramInputStream.close();
          Log.d("OplusHeifConverter_Java", "It is 8Bit Heif!");
          return 0;
        } 
        i1 += m - 8;
      } 
      if (i1 > k)
        bool = false; 
    } 
    paramInputStream.close();
    Log.d("OplusHeifConverter_Java", "It is 8Bit Heif!");
    return 0;
  }
  
  public int getFormat(FileDescriptor paramFileDescriptor) throws IOException {
    FileInputStream fileInputStream = new FileInputStream(paramFileDescriptor);
    int i = 1835365473;
    int j = 1768977008;
    boolean bool = true;
    int k = fileInputStream.available();
    byte[] arrayOfByte = new byte[k];
    BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
    bufferedInputStream.read(arrayOfByte);
    int m = byteArrayToInt(arrayOfByte, 0);
    int n = 0 + 4;
    int i1 = byteArrayToInt(arrayOfByte, n);
    if (i1 != 1718909296) {
      Log.e("OplusHeifConverter_Java", "not a heif file!");
      bufferedInputStream.close();
      fileInputStream.close();
      return 0;
    } 
    m = n + 4 + m - 8;
    while (bool) {
      i1 = byteArrayToInt(arrayOfByte, m);
      m += 4;
      n = byteArrayToInt(arrayOfByte, m);
      m += 4;
      if (n == 1835295092) {
        if (i1 == 1) {
          i1 = byteArrayToInt(arrayOfByte, m);
          m += 4;
          n = byteArrayToInt(arrayOfByte, m);
          m = m + 4 + (i1 << 8) + n - 16;
        } 
      } else if (n == i) {
        m += 4;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("parse meta_box cur ");
        stringBuilder.append(m);
        Log.d("OplusHeifConverter_Java", stringBuilder.toString());
      } else if (n == j) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("parse iprp_box cur  ");
        stringBuilder.append(m);
        Log.d("OplusHeifConverter_Java", stringBuilder.toString());
      } else if (n == 1768973167) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("parse ipco_box cur  ");
        stringBuilder.append(m);
        Log.d("OplusHeifConverter_Java", stringBuilder.toString());
      } else {
        if (n == 1752589123) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("cur hvcC_box  ");
          stringBuilder.append(m);
          Log.d("OplusHeifConverter_Java", stringBuilder.toString());
          j = m + 17;
          m = arrayOfByte[j];
          j++;
          j = arrayOfByte[j];
          if ((m & 0x3) == 2 && (j & 0x3) == 2) {
            bufferedInputStream.close();
            fileInputStream.close();
            Log.d("OplusHeifConverter_Java", "It is 10Bit Heif!");
            return 1;
          } 
          bufferedInputStream.close();
          fileInputStream.close();
          Log.d("OplusHeifConverter_Java", "It is 8Bit Heif!");
          return 0;
        } 
        m += i1 - 8;
      } 
      if (m > k)
        bool = false; 
    } 
    bufferedInputStream.close();
    fileInputStream.close();
    Log.d("OplusHeifConverter_Java", "It is 8Bit Heif!");
    return 0;
  }
  
  public boolean createDecoder() {
    this.m10BitObject = nativeCreateDecoder();
    return true;
  }
  
  public boolean destroyDecoder() {
    nativeDestroyDecoder(this.m10BitObject);
    return true;
  }
  
  public boolean decode(InputStream paramInputStream, int paramInt, Surface paramSurface) {
    boolean bool = false;
    this.mSurface = paramSurface;
    if (paramSurface == null)
      Log.e("OplusHeifConverter_Java", "sur is NULL!"); 
    try {
      boolean bool1 = nativeDecode(this.m10BitObject, paramInputStream, paramSurface, paramInt);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to nativeShow10BitHEIF stream: ");
      stringBuilder.append(exception);
      Log.e("OplusHeifConverter_Java", stringBuilder.toString());
    } 
    return bool;
  }
  
  public HeifDecodedFrame decode(InputStream paramInputStream, int paramInt, boolean paramBoolean) {
    HeifDecodedFrame heifDecodedFrame1, heifDecodedFrame2 = null;
    try {
      heifDecodedFrame1 = nativeGetDecodeFrame(this.m10BitObject, paramInputStream, paramInt, paramBoolean);
      heifDecodedFrame2 = heifDecodedFrame1;
      heifDecodedFrame1.m_recycled = false;
      heifDecodedFrame2 = heifDecodedFrame1;
      StringBuilder stringBuilder = new StringBuilder();
      heifDecodedFrame2 = heifDecodedFrame1;
      this();
      heifDecodedFrame2 = heifDecodedFrame1;
      stringBuilder.append("width: ");
      heifDecodedFrame2 = heifDecodedFrame1;
      stringBuilder.append(heifDecodedFrame1.m_frame_width);
      heifDecodedFrame2 = heifDecodedFrame1;
      stringBuilder.append(" height:");
      heifDecodedFrame2 = heifDecodedFrame1;
      stringBuilder.append(heifDecodedFrame1.m_frame_height);
      heifDecodedFrame2 = heifDecodedFrame1;
      stringBuilder.append(" isDirectBuffer: ");
      heifDecodedFrame2 = heifDecodedFrame1;
      stringBuilder.append(paramBoolean);
      heifDecodedFrame2 = heifDecodedFrame1;
      Log.e("OplusHeifConverter_Java", stringBuilder.toString());
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to nativeGet10BitYUVdata stream: ");
      stringBuilder.append(exception);
      Log.e("OplusHeifConverter_Java", stringBuilder.toString());
      heifDecodedFrame1 = heifDecodedFrame2;
    } 
    return heifDecodedFrame1;
  }
  
  public boolean decodeRegion(InputStream paramInputStream, Rect paramRect, int paramInt, Surface paramSurface) {
    boolean bool1;
    boolean bool = false;
    this.mSurface = paramSurface;
    if (paramRect == null) {
      Log.e("OplusHeifConverter_Java", "rect is null,decode whole image!");
      paramRect = new Rect(0, 0, 0, 0);
      bool1 = true;
    } else {
      bool1 = false;
    } 
    try {
      boolean bool2 = nativeDecodeRegion(this.m10BitObject, paramInputStream, paramRect.left, paramRect.top, paramRect.right - paramRect.left, paramRect.bottom - paramRect.top, paramSurface, paramInt, bool1);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to nativeShow10BitHEIF stream: ");
      stringBuilder.append(exception);
      Log.e("OplusHeifConverter_Java", stringBuilder.toString());
    } 
    return bool;
  }
  
  public HeifDecodedFrame decodeRegion(InputStream paramInputStream, Rect paramRect, int paramInt, boolean paramBoolean) {
    HeifDecodedFrame heifDecodedFrame1, heifDecodedFrame2;
    Rect rect2;
    boolean bool;
    Rect rect1 = null;
    if (paramRect == null) {
      Log.e("OplusHeifConverter_Java", "rect is NULL!,decode whole image");
      rect2 = new Rect(0, 0, 0, 0);
      bool = true;
    } else {
      bool = false;
      rect2 = paramRect;
    } 
    paramRect = rect1;
    try {
      heifDecodedFrame1 = nativeGetRegionDecodeFrame(this.m10BitObject, paramInputStream, rect2.left, rect2.top, rect2.right - rect2.left, rect2.bottom - rect2.top, paramInt, paramBoolean, bool);
      heifDecodedFrame2 = heifDecodedFrame1;
      heifDecodedFrame1.m_recycled = false;
      heifDecodedFrame2 = heifDecodedFrame1;
      StringBuilder stringBuilder = new StringBuilder();
      heifDecodedFrame2 = heifDecodedFrame1;
      this();
      heifDecodedFrame2 = heifDecodedFrame1;
      stringBuilder.append("width: ");
      heifDecodedFrame2 = heifDecodedFrame1;
      stringBuilder.append(heifDecodedFrame1.m_frame_width);
      heifDecodedFrame2 = heifDecodedFrame1;
      stringBuilder.append(" height:");
      heifDecodedFrame2 = heifDecodedFrame1;
      stringBuilder.append(heifDecodedFrame1.m_frame_height);
      heifDecodedFrame2 = heifDecodedFrame1;
      Log.e("OplusHeifConverter_Java", stringBuilder.toString());
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to nativeGet10BitYUVdata stream: ");
      stringBuilder.append(exception);
      Log.e("OplusHeifConverter_Java", stringBuilder.toString());
      heifDecodedFrame1 = heifDecodedFrame2;
    } 
    return heifDecodedFrame1;
  }
  
  public boolean decode(FileDescriptor paramFileDescriptor, int paramInt, Surface paramSurface) {
    boolean bool1 = false, bool2 = false;
    this.mSurface = paramSurface;
    if (paramSurface == null)
      Log.e("OplusHeifConverter_Java", "sur is NULL!"); 
    FileInputStream fileInputStream = new FileInputStream(paramFileDescriptor);
    try {
      boolean bool = nativeDecode(this.m10BitObject, fileInputStream, paramSurface, paramInt);
      bool1 = bool2;
      try {
        fileInputStream.close();
        bool1 = bool2;
      } catch (IOException iOException) {}
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Unable to nativeShow10BitHEIF stream: ");
      stringBuilder.append(exception);
      Log.e("OplusHeifConverter_Java", stringBuilder.toString());
      iOException.close();
      bool1 = bool2;
    } finally {}
    return bool1;
  }
  
  public HeifDecodedFrame decode(FileDescriptor paramFileDescriptor, int paramInt, boolean paramBoolean) {
    HeifDecodedFrame heifDecodedFrame;
    FileDescriptor fileDescriptor = null;
    FileInputStream fileInputStream = new FileInputStream(paramFileDescriptor);
    paramFileDescriptor = fileDescriptor;
    try {
      HeifDecodedFrame heifDecodedFrame2 = nativeGetDecodeFrame(this.m10BitObject, fileInputStream, paramInt, paramBoolean);
      heifDecodedFrame = heifDecodedFrame2;
      heifDecodedFrame2.m_recycled = false;
      HeifDecodedFrame heifDecodedFrame1 = heifDecodedFrame2;
      try {
        fileInputStream.close();
        heifDecodedFrame = heifDecodedFrame2;
      } catch (IOException iOException) {
        heifDecodedFrame = heifDecodedFrame1;
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Unable to nativeGet10BitYUVdata stream: ");
      stringBuilder.append(exception);
      Log.e("OplusHeifConverter_Java", stringBuilder.toString());
      HeifDecodedFrame heifDecodedFrame1 = heifDecodedFrame;
      fileInputStream.close();
    } finally {}
    return heifDecodedFrame;
  }
  
  public boolean decodeRegion(FileDescriptor paramFileDescriptor, Rect paramRect, int paramInt, Surface paramSurface) {
    // Byte code:
    //   0: iconst_0
    //   1: istore #5
    //   3: iconst_0
    //   4: istore #6
    //   6: aload_0
    //   7: aload #4
    //   9: putfield mSurface : Landroid/view/Surface;
    //   12: new java/io/FileInputStream
    //   15: dup
    //   16: aload_1
    //   17: invokespecial <init> : (Ljava/io/FileDescriptor;)V
    //   20: astore #7
    //   22: aload_2
    //   23: ifnonnull -> 52
    //   26: ldc 'OplusHeifConverter_Java'
    //   28: ldc 'rect is null!,decode whole image'
    //   30: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   33: pop
    //   34: new android/graphics/Rect
    //   37: dup
    //   38: iconst_0
    //   39: iconst_0
    //   40: iconst_0
    //   41: iconst_0
    //   42: invokespecial <init> : (IIII)V
    //   45: astore_2
    //   46: iconst_1
    //   47: istore #8
    //   49: goto -> 55
    //   52: iconst_0
    //   53: istore #8
    //   55: aload_0
    //   56: getfield m10BitObject : J
    //   59: lstore #9
    //   61: aload_2
    //   62: getfield left : I
    //   65: istore #11
    //   67: aload_2
    //   68: getfield top : I
    //   71: istore #12
    //   73: aload_2
    //   74: getfield right : I
    //   77: istore #13
    //   79: aload_2
    //   80: getfield left : I
    //   83: istore #14
    //   85: aload_2
    //   86: getfield bottom : I
    //   89: istore #15
    //   91: aload_2
    //   92: getfield top : I
    //   95: istore #16
    //   97: lload #9
    //   99: aload #7
    //   101: iload #11
    //   103: iload #12
    //   105: iload #13
    //   107: iload #14
    //   109: isub
    //   110: iload #15
    //   112: iload #16
    //   114: isub
    //   115: aload #4
    //   117: iload_3
    //   118: iload #8
    //   120: invokestatic nativeDecodeRegion : (JLjava/io/InputStream;IIIILandroid/view/Surface;II)Z
    //   123: istore #17
    //   125: iload #17
    //   127: istore #6
    //   129: iload #6
    //   131: istore #17
    //   133: aload #7
    //   135: invokevirtual close : ()V
    //   138: iload #6
    //   140: istore #17
    //   142: goto -> 205
    //   145: astore_1
    //   146: goto -> 142
    //   149: astore_1
    //   150: goto -> 158
    //   153: astore_1
    //   154: goto -> 209
    //   157: astore_1
    //   158: new java/lang/StringBuilder
    //   161: astore_2
    //   162: aload_2
    //   163: invokespecial <init> : ()V
    //   166: aload_2
    //   167: ldc 'Unable to nativeShow10BitHEIF stream: '
    //   169: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   172: pop
    //   173: aload_2
    //   174: aload_1
    //   175: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   178: pop
    //   179: ldc 'OplusHeifConverter_Java'
    //   181: aload_2
    //   182: invokevirtual toString : ()Ljava/lang/String;
    //   185: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   188: pop
    //   189: iload #5
    //   191: istore #17
    //   193: aload #7
    //   195: invokevirtual close : ()V
    //   198: iload #6
    //   200: istore #17
    //   202: goto -> 142
    //   205: iload #17
    //   207: ireturn
    //   208: astore_1
    //   209: aload #7
    //   211: invokevirtual close : ()V
    //   214: goto -> 218
    //   217: astore_2
    //   218: aload_1
    //   219: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #595	-> 0
    //   #596	-> 6
    //   #597	-> 12
    //   #598	-> 12
    //   #599	-> 22
    //   #600	-> 26
    //   #601	-> 34
    //   #602	-> 34
    //   #599	-> 52
    //   #606	-> 55
    //   #611	-> 129
    //   #613	-> 129
    //   #616	-> 142
    //   #614	-> 145
    //   #608	-> 149
    //   #611	-> 153
    //   #608	-> 157
    //   #609	-> 158
    //   #611	-> 189
    //   #613	-> 189
    //   #619	-> 205
    //   #611	-> 208
    //   #613	-> 209
    //   #616	-> 214
    //   #614	-> 217
    //   #618	-> 218
    // Exception table:
    //   from	to	target	type
    //   55	97	157	java/lang/Exception
    //   55	97	153	finally
    //   97	125	149	java/lang/Exception
    //   97	125	208	finally
    //   133	138	145	java/io/IOException
    //   158	189	208	finally
    //   193	198	145	java/io/IOException
    //   209	214	217	java/io/IOException
  }
  
  public HeifDecodedFrame decodeRegion(FileDescriptor paramFileDescriptor, Rect paramRect, int paramInt, boolean paramBoolean) {
    HeifDecodedFrame heifDecodedFrame;
    boolean bool;
    FileDescriptor fileDescriptor = null;
    FileInputStream fileInputStream = new FileInputStream(paramFileDescriptor);
    if (paramRect == null) {
      Log.e("OplusHeifConverter_Java", "rect is NULL!,decode whole image");
      paramRect = new Rect(0, 0, 0, 0);
      bool = true;
    } else {
      bool = false;
    } 
    paramFileDescriptor = fileDescriptor;
    try {
      HeifDecodedFrame heifDecodedFrame1 = nativeGetRegionDecodeFrame(this.m10BitObject, fileInputStream, paramRect.left, paramRect.top, paramRect.right - paramRect.left, paramRect.bottom - paramRect.top, paramInt, paramBoolean, bool);
      heifDecodedFrame = heifDecodedFrame1;
      heifDecodedFrame1.m_recycled = false;
      heifDecodedFrame = heifDecodedFrame1;
      StringBuilder stringBuilder = new StringBuilder();
      heifDecodedFrame = heifDecodedFrame1;
      this();
      heifDecodedFrame = heifDecodedFrame1;
      stringBuilder.append("width: ");
      heifDecodedFrame = heifDecodedFrame1;
      stringBuilder.append(heifDecodedFrame1.m_frame_width);
      heifDecodedFrame = heifDecodedFrame1;
      stringBuilder.append(" height: ");
      heifDecodedFrame = heifDecodedFrame1;
      stringBuilder.append(heifDecodedFrame1.m_frame_height);
      heifDecodedFrame = heifDecodedFrame1;
      Log.e("OplusHeifConverter_Java", stringBuilder.toString());
      HeifDecodedFrame heifDecodedFrame2 = heifDecodedFrame1;
      try {
        fileInputStream.close();
        heifDecodedFrame = heifDecodedFrame1;
      } catch (IOException iOException) {
        heifDecodedFrame = heifDecodedFrame2;
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Unable to nativeGet10BitYUVdata stream: ");
      stringBuilder.append(exception);
      Log.e("OplusHeifConverter_Java", stringBuilder.toString());
      HeifDecodedFrame heifDecodedFrame1 = heifDecodedFrame;
      fileInputStream.close();
    } finally {}
    return heifDecodedFrame;
  }
  
  private static native boolean nativeCheckPPS(byte[] paramArrayOfbyte);
  
  private static native long nativeCreateDecoder();
  
  private static native boolean nativeDecode(long paramLong, InputStream paramInputStream, Surface paramSurface, int paramInt);
  
  private static native boolean nativeDecodeRegion(long paramLong, InputStream paramInputStream, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Surface paramSurface, int paramInt5, int paramInt6);
  
  private static native void nativeDestroyDecoder(long paramLong);
  
  private static native HeifDecodedFrame nativeGetDecodeFrame(long paramLong, InputStream paramInputStream, int paramInt, boolean paramBoolean);
  
  private static native HeifDecodedFrame nativeGetRegionDecodeFrame(long paramLong, InputStream paramInputStream, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean, int paramInt6);
  
  private static native boolean nativeHeifConvert(InputStream paramInputStream, byte[] paramArrayOfbyte1, int paramInt, OutputStream paramOutputStream, byte[] paramArrayOfbyte2);
  
  private static native void nativeRecycle(long paramLong1, long paramLong2);
  
  private static native boolean nativeRender(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, Surface paramSurface);
  
  private static native boolean nativeRenderDirectBuffer(long paramLong1, long paramLong2, int paramInt1, int paramInt2, Surface paramSurface);
}
