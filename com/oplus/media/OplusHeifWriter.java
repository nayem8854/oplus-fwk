package com.oplus.media;

import android.util.Log;
import java.io.FileDescriptor;

public class OplusHeifWriter {
  public static final int COLOR_FMT_MAX = 3;
  
  public static final int COLOR_FMT_NV12 = 0;
  
  public static final int COLOR_FMT_P010 = 1;
  
  public static final int COLOR_FMT_RGBA8888 = 2;
  
  private static final String TAG = "OplusHeifWriter_Java";
  
  static final int maxValue = 100;
  
  static final int minValue = 0;
  
  static {
    Log.v("OplusHeifWriter_Java", "loadLibrary");
    System.loadLibrary("oplusheifwriter");
  }
  
  private long mNativeObject = nativeSetup();
  
  public boolean createPrimaryImage(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7) {
    if (paramInt6 > 0 && paramInt6 <= 100) {
      if (paramInt1 <= 0 || paramInt2 <= 0 || paramInt3 <= 0 || paramInt4 <= 0 || paramInt5 < 0 || paramInt5 >= 3) {
        Log.i("OplusHeifWriter_Java", "Input param error.");
        return false;
      } 
      long l = nativeCreate(this.mNativeObject, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" OplusHeifWriter start! quality: ");
      stringBuilder.append(paramInt6);
      Log.i("OplusHeifWriter_Java", stringBuilder.toString());
      if (l < 0L)
        return false; 
      return true;
    } 
    IllegalArgumentException illegalArgumentException = new IllegalArgumentException("quality range error");
    throw illegalArgumentException;
  }
  
  private void createTrack() {}
  
  private void addTrackSample() {}
  
  public boolean processPrimaryImage(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, FileDescriptor paramFileDescriptor) {
    long l = nativeProcessHeicPhotoFrame(this.mNativeObject, paramArrayOfbyte1, paramArrayOfbyte2, paramFileDescriptor);
    if (l < 0L)
      return false; 
    return true;
  }
  
  private void destory() {
    Log.i("OplusHeifWriter_Java", " OplusHeifWriter destory!");
    nativeDestory(this.mNativeObject);
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mNativeObject != 0L) {
        destory();
        this.mNativeObject = 0L;
      } 
      return;
    } finally {
      super.finalize();
    } 
  }
  
  private static native long nativeCreate(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7);
  
  private static native void nativeDestory(long paramLong);
  
  private static native long nativeProcessHeicPhotoFrame(long paramLong, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, FileDescriptor paramFileDescriptor);
  
  private static native long nativeSetup();
}
