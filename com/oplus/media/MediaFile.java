package com.oplus.media;

import android.content.Context;
import android.media.DecoderCapabilities;
import android.net.Uri;
import android.provider.Settings;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import libcore.content.type.MimeMap;

public class MediaFile {
  public static final int FILE_TYPE_3GPP = 23;
  
  public static final int FILE_TYPE_3GPP2 = 24;
  
  public static final int FILE_TYPE_AAC = 8;
  
  public static final int FILE_TYPE_AMR = 4;
  
  public static final int FILE_TYPE_APE = 1001;
  
  public static final int FILE_TYPE_APK = 10011;
  
  public static final int FILE_TYPE_ARW = 304;
  
  public static final int FILE_TYPE_ASF = 26;
  
  public static final int FILE_TYPE_AUDIO_3GPP = 11;
  
  public static final int FILE_TYPE_AVI = 29;
  
  public static final int FILE_TYPE_AWB = 5;
  
  public static final int FILE_TYPE_BMP = 34;
  
  public static final int FILE_TYPE_CHM = 10021;
  
  public static final int FILE_TYPE_CR2 = 301;
  
  public static final int FILE_TYPE_CSV = 10022;
  
  public static final int FILE_TYPE_CUE = 1003;
  
  public static final int FILE_TYPE_DNG = 300;
  
  public static final int FILE_TYPE_EBK = 10026;
  
  public static final int FILE_TYPE_EPUB = 10027;
  
  public static final int FILE_TYPE_FL = 51;
  
  public static final int FILE_TYPE_FLAC = 10;
  
  public static final int FILE_TYPE_FLV = 1101;
  
  public static final int FILE_TYPE_GIF = 32;
  
  public static final int FILE_TYPE_HEIF = 37;
  
  public static final int FILE_TYPE_HTML = 101;
  
  public static final int FILE_TYPE_HTTPLIVE = 44;
  
  public static final int FILE_TYPE_ICS = 10023;
  
  public static final int FILE_TYPE_IMY = 13;
  
  public static final int FILE_TYPE_JAR = 10002;
  
  public static final int FILE_TYPE_JPEG = 31;
  
  public static final int FILE_TYPE_M2TS = 1104;
  
  public static final int FILE_TYPE_M3U = 41;
  
  public static final int FILE_TYPE_M4A = 2;
  
  public static final int FILE_TYPE_M4V = 22;
  
  public static final int FILE_TYPE_MID = 11;
  
  public static final int FILE_TYPE_MKA = 9;
  
  public static final int FILE_TYPE_MKV = 27;
  
  public static final int FILE_TYPE_MOV = 1103;
  
  public static final int FILE_TYPE_MP2 = 1002;
  
  public static final int FILE_TYPE_MP2PS = 200;
  
  public static final int FILE_TYPE_MP2TS = 28;
  
  public static final int FILE_TYPE_MP3 = 1;
  
  public static final int FILE_TYPE_MP4 = 21;
  
  public static final int FILE_TYPE_MS_EXCEL = 105;
  
  public static final int FILE_TYPE_MS_POWERPOINT = 106;
  
  public static final int FILE_TYPE_MS_WORD = 104;
  
  public static final int FILE_TYPE_NEF = 302;
  
  public static final int FILE_TYPE_NRW = 303;
  
  public static final int FILE_TYPE_OGG = 7;
  
  public static final int FILE_TYPE_ORF = 306;
  
  public static final int FILE_TYPE_PDF = 102;
  
  public static final int FILE_TYPE_PEF = 308;
  
  public static final int FILE_TYPE_PLS = 42;
  
  public static final int FILE_TYPE_PNG = 33;
  
  public static final int FILE_TYPE_QT = 201;
  
  public static final int FILE_TYPE_RA = 1004;
  
  public static final int FILE_TYPE_RAF = 307;
  
  public static final int FILE_TYPE_RAR = 10001;
  
  public static final int FILE_TYPE_RV = 1102;
  
  public static final int FILE_TYPE_RW2 = 305;
  
  public static final int FILE_TYPE_SMF = 12;
  
  public static final int FILE_TYPE_SRW = 309;
  
  public static final int FILE_TYPE_TEXT = 100;
  
  public static final int FILE_TYPE_VCF = 10024;
  
  public static final int FILE_TYPE_VCS = 10025;
  
  public static final int FILE_TYPE_WAV = 3;
  
  public static final int FILE_TYPE_WBMP = 35;
  
  public static final int FILE_TYPE_WEBM = 30;
  
  public static final int FILE_TYPE_WEBP = 36;
  
  public static final int FILE_TYPE_WMA = 6;
  
  public static final int FILE_TYPE_WMV = 25;
  
  public static final int FILE_TYPE_WPL = 43;
  
  public static final int FILE_TYPE_XML = 103;
  
  public static final int FILE_TYPE_ZIP = 107;
  
  private static final int FIRST_APK_FILE_TYPE = 10011;
  
  private static final int FIRST_AUDIO_FILE_TYPE = 1;
  
  private static final int FIRST_COMPRESS_FILE_TYPE = 10001;
  
  private static final int FIRST_DOC_FILE_TYPE = 10021;
  
  private static final int FIRST_DRM_FILE_TYPE = 51;
  
  private static final int FIRST_FFMPEG_AUDIO_FILE_TYPE = 1001;
  
  private static final int FIRST_FFMPEG_VIDEO_FILE_TYPE = 1101;
  
  private static final int FIRST_IMAGE_FILE_TYPE = 31;
  
  private static final int FIRST_MIDI_FILE_TYPE = 11;
  
  private static final int FIRST_PLAYLIST_FILE_TYPE = 41;
  
  private static final int FIRST_POPULAR_DOC_FILE_TYPE = 100;
  
  private static final int FIRST_RAW_IMAGE_FILE_TYPE = 300;
  
  private static final int FIRST_VIDEO_FILE_TYPE = 21;
  
  private static final int FIRST_VIDEO_FILE_TYPE2 = 200;
  
  private static final int LAST_APK_FILE_TYPE = 10011;
  
  private static final int LAST_AUDIO_FILE_TYPE = 11;
  
  private static final int LAST_COMPRESS_FILE_TYPE = 10002;
  
  private static final int LAST_DOC_FILE_TYPE = 10027;
  
  private static final int LAST_DRM_FILE_TYPE = 51;
  
  private static final int LAST_FFMPEG_AUDIO_FILE_TYPE = 1004;
  
  private static final int LAST_FFMPEG_VIDEO_FILE_TYPE = 1104;
  
  private static final int LAST_IMAGE_FILE_TYPE = 37;
  
  private static final int LAST_MIDI_FILE_TYPE = 13;
  
  private static final int LAST_PLAYLIST_FILE_TYPE = 44;
  
  private static final int LAST_POPULAR_DOC_FILE_TYPE = 106;
  
  private static final int LAST_RAW_IMAGE_FILE_TYPE = 309;
  
  private static final int LAST_VIDEO_FILE_TYPE = 30;
  
  private static final int LAST_VIDEO_FILE_TYPE2 = 201;
  
  public static final int MEDIA_TYPE_APK = 10002;
  
  public static final int MEDIA_TYPE_COMPRESS = 10001;
  
  public static final int MEDIA_TYPE_DOC = 10003;
  
  public static final String OPLUS_DEFAULT_ALARM = "oppo_default_alarm";
  
  public static final String OPLUS_DEFAULT_NOTIFICATION = "oppo_default_notification";
  
  public static final String OPLUS_DEFAULT_NOTIFICATION_SIM2 = "oppo_default_notification_sim2";
  
  public static final String OPLUS_DEFAULT_RINGTONE = "oppo_default_ringtone";
  
  public static final String OPLUS_DEFAULT_RINGTONE_SIM2 = "oppo_default_ringtone_sim2";
  
  public static final String OPLUS_DEFAULT_SMS_NOTIFICATION = "oppo_default_sms_notification_sound";
  
  public static final int SCAN_ALL_FILE = 0;
  
  public static final int SCAN_AUDIO_FILE = 1;
  
  public static final int SCAN_IMAGE_FILE = 2;
  
  public static final int SCAN_OTHER_FILE = 8;
  
  public static final int SCAN_VIDEO_FILE = 4;
  
  private static final HashMap<Integer, String> sDeprecatedFormatToMimeTypeMap;
  
  private static final HashMap<String, Integer> sDeprecatedMimeTypeToFormatMap;
  
  public static class MediaFileType {
    public final int fileType;
    
    public final String mimeType;
    
    MediaFileType(int param1Int, String param1String) {
      this.fileType = param1Int;
      this.mimeType = param1String;
    }
  }
  
  private static final HashMap<String, MediaFileType> sFileTypeMap = new HashMap<>();
  
  private static final HashMap<String, Integer> sFileTypeToFormatMap;
  
  private static final HashMap<Integer, String> sFormatToMimeTypeMap;
  
  private static final HashMap<String, Integer> sMimeTypeMap = new HashMap<>();
  
  private static final HashMap<String, Integer> sMimeTypeToFormatMap;
  
  static {
    sFileTypeToFormatMap = new HashMap<>();
    sDeprecatedMimeTypeToFormatMap = new HashMap<>();
    sDeprecatedFormatToMimeTypeMap = new HashMap<>();
    addFileAndMineType("MP3", 1, "audio/mpeg", 12297, true);
    addFileAndMineType("MPGA", 1, "audio/mpeg", 12297, false);
    addFileAndMineType("M4A", 2, "audio/mp4", 12299, false);
    addFileAndMineType("WAV", 3, "audio/x-wav", 12296, true);
    addFileAndMineType("AMR", 4, "audio/amr");
    addFileAndMineType("3GPP", 11, "audio/3gpp");
    addFileAndMineType("AWB", 5, "audio/amr-wb");
    if (isWMAEnabled())
      addFileAndMineType("WMA", 6, "audio/x-ms-wma", 47361, true); 
    addFileAndMineType("OGG", 7, "audio/ogg", 47362, false);
    addFileAndMineType("OGG", 7, "application/ogg", 47362, true);
    addFileAndMineType("OGA", 7, "application/ogg", 47362, false);
    addFileAndMineType("AAC", 8, "audio/aac", 47363, true);
    addFileAndMineType("AAC", 8, "audio/aac-adts", 47363, false);
    addFileAndMineType("MKA", 9, "audio/x-matroska");
    addFileAndMineType("MID", 11, "audio/mid");
    addFileAndMineType("MID", 11, "audio/midi");
    addFileAndMineType("MIDI", 11, "audio/midi");
    addFileAndMineType("XMF", 11, "audio/midi");
    addFileAndMineType("RTTTL", 11, "audio/midi");
    addFileAndMineType("SMF", 12, "audio/sp-midi");
    addFileAndMineType("IMY", 13, "audio/imelody");
    addFileAndMineType("RTX", 11, "audio/midi");
    addFileAndMineType("OTA", 11, "audio/midi");
    addFileAndMineType("MXMF", 11, "audio/midi");
    addFileAndMineType("MPEG", 21, "video/mpeg", 12299, true);
    addFileAndMineType("MPG", 21, "video/mpeg", 12299, false);
    addFileAndMineType("MP4", 21, "video/mp4", 12299, false);
    addFileAndMineType("M4V", 22, "video/mp4", 12299, false);
    addFileAndMineType("MOV", 201, "video/quicktime", 12299, false);
    addFileAndMineType("3GP", 23, "video/3gpp", 47492, true);
    addFileAndMineType("3GPP", 23, "video/3gpp", 47492, false);
    addFileAndMineType("3G2", 24, "video/3gpp2", 47492, false);
    addFileAndMineType("3GPP2", 24, "video/3gpp2", 47492, false);
    addFileAndMineType("MKV", 27, "video/x-matroska");
    addFileAndMineType("WEBM", 30, "video/webm");
    addFileAndMineType("TS", 28, "video/mp2ts");
    addFileAndMineType("AVI", 29, "video/avi");
    if (isWMVEnabled()) {
      addFileAndMineType("WMV", 25, "video/x-ms-wmv", 47489, true);
      addFileAndMineType("ASF", 26, "video/x-ms-asf");
    } 
    addFileAndMineType("JPG", 31, "image/jpeg", 14337, true);
    addFileAndMineType("JPEG", 31, "image/jpeg", 14337, false);
    addFileAndMineType("GIF", 32, "image/gif", 14343, true);
    addFileAndMineType("PNG", 33, "image/png", 14347, true);
    addFileAndMineType("BMP", 34, "image/x-ms-bmp", 14340, true);
    addFileAndMineType("BMP", 34, "image/bmp", 14340, false);
    addFileAndMineType("WBMP", 35, "image/vnd.wap.wbmp", 14336, false);
    addFileAndMineType("WEBP", 36, "image/webp", 14336, false);
    addFileAndMineType("HEIC", 37, "image/heif", 14354, true);
    addFileAndMineType("HEIF", 37, "image/heif", 14354, false);
    addFileAndMineType("DNG", 300, "image/x-adobe-dng", 14353, true);
    addFileAndMineType("CR2", 301, "image/x-canon-cr2", 14349, false);
    addFileAndMineType("NEF", 302, "image/x-nikon-nef", 14338, false);
    addFileAndMineType("NRW", 303, "image/x-nikon-nrw", 14349, false);
    addFileAndMineType("ARW", 304, "image/x-sony-arw", 14349, false);
    addFileAndMineType("RW2", 305, "image/x-panasonic-rw2", 14349, false);
    addFileAndMineType("ORF", 306, "image/x-olympus-orf", 14349, false);
    addFileAndMineType("RAF", 307, "image/x-fuji-raf", 14336, false);
    addFileAndMineType("PEF", 308, "image/x-pentax-pef", 14349, false);
    addFileAndMineType("SRW", 309, "image/x-samsung-srw", 14349, false);
    addFileAndMineType("M3U", 41, "audio/x-mpegurl", 47633, true);
    addFileAndMineType("M3U", 41, "application/x-mpegurl", 47633, false);
    addFileAndMineType("PLS", 42, "audio/x-scpls", 47636, true);
    addFileAndMineType("WPL", 43, "application/vnd.ms-wpl", 47632, true);
    addFileAndMineType("M3U8", 44, "application/vnd.apple.mpegurl");
    addFileAndMineType("M3U8", 44, "audio/mpegurl");
    addFileAndMineType("M3U8", 44, "audio/x-mpegurl");
    addFileAndMineType("FL", 51, "application/x-android-drm-fl");
    addFileAndMineType("TXT", 100, "text/plain", 12292, true);
    addFileAndMineType("HTM", 101, "text/html", 12293, true);
    addFileAndMineType("HTML", 101, "text/html", 12293, false);
    addFileAndMineType("PDF", 102, "application/pdf");
    addFileAndMineType("DOC", 104, "application/msword", 47747, true);
    addFileAndMineType("XLS", 105, "application/vnd.ms-excel", 47749, true);
    addFileAndMineType("PPT", 106, "application/mspowerpoint", 47750, true);
    addFileAndMineType("FLAC", 10, "audio/flac", 47366, true);
    addFileAndMineType("ZIP", 107, "application/zip");
    addFileAndMineType("MPG", 200, "video/mp2p");
    addFileAndMineType("MPEG", 200, "video/mp2p");
    addFileAndMineType("APE", 1001, "audio/ape");
    addFileAndMineType("MP2", 1002, "audio/mpeg");
    addFileAndMineType("CUE", 1003, "audio/cue");
    addFileAndMineType("FLV", 1101, "video/x-flv");
    addFileAndMineType("F4V", 1101, "video/x-flv");
    addFileAndMineType("MOV", 1103, "video/x-quicktime");
    addFileAndMineType("M2TS", 1104, "video/m2ts");
    addFileAndMineType("DOCX", 104, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    addFileAndMineType("XLSX", 105, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    addFileAndMineType("PPTX", 106, "application/vnd.openxmlformats-officedocument.presentationml.presentation");
    addFileAndMineType("RAR", 10001, "application/rar");
    addFileAndMineType("JAR", 10002, "application/java-archive");
    addFileAndMineType("APK", 10011, "application/vnd.android.package-archive");
    addFileAndMineType("CHM", 10021, "application/x-expandedbook");
    addFileAndMineType("CSV", 10022, "text/comma-separated-values");
    addFileAndMineType("ICS", 10023, "text/calendar");
    addFileAndMineType("VCF", 10024, "text/x-vcard");
    addFileAndMineType("VCS", 10025, "text/x-vcalendar");
    addFileAndMineType("EBK2", 10026, "text/x-expandedbook");
    addFileAndMineType("EBK3", 10026, "text/x-expandedbook");
    addFileAndMineType("EPUB", 10027, "text/plain");
    sMimeTypeToFormatMap = new HashMap<>();
    sFormatToMimeTypeMap = new HashMap<>();
    addFileType(12297, "audio/mpeg");
    addFileType(12296, "audio/x-wav");
    addFileType(47361, "audio/x-ms-wma");
    addFileType(47362, "audio/ogg");
    addFileType(47363, "audio/aac");
    addFileType(47366, "audio/flac");
    addFileType(12295, "audio/x-aiff");
    addFileType(47491, "audio/mpeg");
    addFileType(12299, "video/mpeg");
    addFileType(47490, "video/mp4");
    addFileType(47492, "video/3gpp");
    addFileType(47492, "video/3gpp2");
    addFileType(12298, "video/avi");
    addFileType(47489, "video/x-ms-wmv");
    addFileType(12300, "video/x-ms-asf");
    addFileType(14337, "image/jpeg");
    addFileType(14343, "image/gif");
    addFileType(14347, "image/png");
    addFileType(14340, "image/x-ms-bmp");
    addFileType(14354, "image/heif");
    addFileType(14353, "image/x-adobe-dng");
    addFileType(14349, "image/tiff");
    addFileType(14349, "image/x-canon-cr2");
    addFileType(14349, "image/x-nikon-nrw");
    addFileType(14349, "image/x-sony-arw");
    addFileType(14349, "image/x-panasonic-rw2");
    addFileType(14349, "image/x-olympus-orf");
    addFileType(14349, "image/x-pentax-pef");
    addFileType(14349, "image/x-samsung-srw");
    addFileType(14338, "image/tiff");
    addFileType(14338, "image/x-nikon-nef");
    addFileType(14351, "image/jp2");
    addFileType(14352, "image/jpx");
    addFileType(47633, "audio/x-mpegurl");
    addFileType(47636, "audio/x-scpls");
    addFileType(47632, "application/vnd.ms-wpl");
    addFileType(47635, "video/x-ms-asf");
    addFileType(12292, "text/plain");
    addFileType(12293, "text/html");
    addFileType(47746, "text/xml");
    addFileType(47747, "application/msword");
    addFileType(47747, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    addFileType(47749, "application/vnd.ms-excel");
    addFileType(47749, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    addFileType(47750, "application/vnd.ms-powerpoint");
    addFileType(47750, "application/vnd.openxmlformats-officedocument.presentationml.presentation");
  }
  
  static void addFileAndMineType(String paramString1, int paramInt, String paramString2) {
    sFileTypeMap.put(paramString1, new MediaFileType(paramInt, paramString2));
    sMimeTypeMap.put(paramString2, Integer.valueOf(paramInt));
  }
  
  private static void addFileAndMineType(String paramString1, int paramInt1, String paramString2, int paramInt2, boolean paramBoolean) {
    addFileAndMineType(paramString1, paramInt1, paramString2);
    sFileTypeToFormatMap.put(paramString1, Integer.valueOf(paramInt2));
    sDeprecatedMimeTypeToFormatMap.put(paramString2, Integer.valueOf(paramInt2));
    if (paramBoolean) {
      Preconditions.checkArgument(sDeprecatedFormatToMimeTypeMap.containsKey(Integer.valueOf(paramInt2)) ^ true);
      sDeprecatedFormatToMimeTypeMap.put(Integer.valueOf(paramInt2), paramString2);
    } 
  }
  
  private static boolean isWMAEnabled() {
    List<DecoderCapabilities.AudioDecoder> list = DecoderCapabilities.getAudioDecoders();
    int i = list.size();
    for (byte b = 0; b < i; b++) {
      DecoderCapabilities.AudioDecoder audioDecoder = list.get(b);
      if (audioDecoder == DecoderCapabilities.AudioDecoder.AUDIO_DECODER_WMA)
        return true; 
    } 
    return false;
  }
  
  private static boolean isWMVEnabled() {
    List<DecoderCapabilities.VideoDecoder> list = DecoderCapabilities.getVideoDecoders();
    int i = list.size();
    for (byte b = 0; b < i; b++) {
      DecoderCapabilities.VideoDecoder videoDecoder = list.get(b);
      if (videoDecoder == DecoderCapabilities.VideoDecoder.VIDEO_DECODER_WMV)
        return true; 
    } 
    return false;
  }
  
  public static boolean isAudioFileType(int paramInt) {
    // Byte code:
    //   0: iconst_1
    //   1: istore_1
    //   2: iload_0
    //   3: iconst_1
    //   4: if_icmplt -> 15
    //   7: iload_1
    //   8: istore_2
    //   9: iload_0
    //   10: bipush #11
    //   12: if_icmple -> 50
    //   15: iload_0
    //   16: bipush #11
    //   18: if_icmplt -> 29
    //   21: iload_1
    //   22: istore_2
    //   23: iload_0
    //   24: bipush #13
    //   26: if_icmple -> 50
    //   29: iload_0
    //   30: sipush #1001
    //   33: if_icmplt -> 48
    //   36: iload_0
    //   37: sipush #1004
    //   40: if_icmpgt -> 48
    //   43: iload_1
    //   44: istore_2
    //   45: goto -> 50
    //   48: iconst_0
    //   49: istore_2
    //   50: iload_2
    //   51: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #410	-> 0
  }
  
  public static boolean isVideoFileType(int paramInt) {
    boolean bool;
    if ((paramInt >= 21 && paramInt <= 30) || (paramInt >= 200 && paramInt <= 201) || (paramInt >= 1101 && paramInt <= 1104)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isImageFileType(int paramInt) {
    boolean bool;
    if ((paramInt >= 31 && paramInt <= 37) || (paramInt >= 300 && paramInt <= 309)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isRawImageFileType(int paramInt) {
    boolean bool;
    if (paramInt >= 300 && paramInt <= 309) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isPlayListFileType(int paramInt) {
    boolean bool;
    if (paramInt >= 41 && paramInt <= 44) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isDrmFileType(int paramInt) {
    boolean bool;
    if (paramInt >= 51 && paramInt <= 51) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isCompressFileType(int paramInt) {
    boolean bool;
    if ((paramInt >= 10001 && paramInt <= 10002) || paramInt == 107) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isApkFileType(int paramInt) {
    boolean bool;
    if (paramInt >= 10011 && paramInt <= 10011) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isDocFileType(int paramInt) {
    boolean bool;
    if ((paramInt >= 10021 && paramInt <= 10027) || (paramInt >= 100 && paramInt <= 106 && paramInt != 103)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static MediaFileType getFileType(String paramString) {
    int i = paramString.lastIndexOf('.');
    if (i < 0)
      return null; 
    return sFileTypeMap.get(paramString.substring(i + 1).toUpperCase(Locale.ROOT));
  }
  
  public static boolean isMimeTypeMedia(String paramString) {
    int i = getFileTypeForMimeType(paramString);
    return (isAudioFileType(i) || isVideoFileType(i) || isImageFileType(i) || isPlayListFileType(i));
  }
  
  public static int getFileTypeForMimeType(String paramString) {
    int i;
    Integer integer = sMimeTypeMap.get(paramString);
    if (integer == null) {
      i = 0;
    } else {
      i = integer.intValue();
    } 
    return i;
  }
  
  public static Uri getDefaultRingtoneUri(Context paramContext) {
    return getUriFor(paramContext, "oppo_default_ringtone");
  }
  
  public static Uri getDefaultAlarmUri(Context paramContext) {
    return getUriFor(paramContext, "oppo_default_alarm");
  }
  
  public static Uri getDefaultNotificationUri(Context paramContext) {
    return getUriFor(paramContext, "oppo_default_notification");
  }
  
  public static Uri getDefaultRingtoneUriSIM2(Context paramContext) {
    return getUriFor(paramContext, "oppo_default_ringtone_sim2");
  }
  
  public static Uri getDefaultNotificationUriSIM2(Context paramContext) {
    return getUriFor(paramContext, "oppo_default_notification_sim2");
  }
  
  public static Uri getDefaultSmsNotificationUri(Context paramContext) {
    return getUriFor(paramContext, "oppo_default_sms_notification_sound");
  }
  
  private static Uri getUriFor(Context paramContext, String paramString) {
    String str = Settings.System.getString(paramContext.getContentResolver(), paramString);
    if (str != null)
      return Uri.parse(str); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(" not set?!!!");
    Log.e("MediaFile", stringBuilder.toString());
    return null;
  }
  
  static void addFileType(String paramString1, int paramInt, String paramString2) {}
  
  private static void addFileType(int paramInt, String paramString) {
    if (!sMimeTypeToFormatMap.containsKey(paramString))
      sMimeTypeToFormatMap.put(paramString, Integer.valueOf(paramInt)); 
    if (!sFormatToMimeTypeMap.containsKey(Integer.valueOf(paramInt)))
      sFormatToMimeTypeMap.put(Integer.valueOf(paramInt), paramString); 
  }
  
  public static boolean isExifMimeType(String paramString) {
    return isImageMimeType(paramString);
  }
  
  public static boolean isAudioMimeType(String paramString) {
    return normalizeMimeType(paramString).startsWith("audio/");
  }
  
  public static boolean isVideoMimeType(String paramString) {
    return normalizeMimeType(paramString).startsWith("video/");
  }
  
  public static boolean isImageMimeType(String paramString) {
    return normalizeMimeType(paramString).startsWith("image/");
  }
  
  public static boolean isPlayListMimeType(String paramString) {
    byte b;
    paramString = normalizeMimeType(paramString);
    switch (paramString.hashCode()) {
      default:
        b = -1;
        break;
      case 1872259501:
        if (paramString.equals("application/vnd.ms-wpl")) {
          b = 0;
          break;
        } 
      case 264230524:
        if (paramString.equals("audio/x-mpegurl")) {
          b = 1;
          break;
        } 
      case -432766831:
        if (paramString.equals("audio/mpegurl")) {
          b = 2;
          break;
        } 
      case -622808459:
        if (paramString.equals("application/vnd.apple.mpegurl")) {
          b = 4;
          break;
        } 
      case -979095690:
        if (paramString.equals("application/x-mpegurl")) {
          b = 3;
          break;
        } 
      case -1165508903:
        if (paramString.equals("audio/x-scpls")) {
          b = 5;
          break;
        } 
    } 
    if (b != 0 && b != 1 && b != 2 && b != 3 && b != 4 && b != 5)
      return false; 
    return true;
  }
  
  public static boolean isDrmMimeType(String paramString) {
    return normalizeMimeType(paramString).equals("application/x-android-drm-fl");
  }
  
  public static boolean isApkMimeType(String paramString) {
    return normalizeMimeType(paramString).equals("application/vnd.android.package-archive");
  }
  
  public static boolean isCompressMimeType(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic normalizeMimeType : (Ljava/lang/String;)Ljava/lang/String;
    //   4: astore_0
    //   5: aload_0
    //   6: invokevirtual hashCode : ()I
    //   9: istore_1
    //   10: iload_1
    //   11: ldc_w -1248333084
    //   14: if_icmpeq -> 64
    //   17: iload_1
    //   18: ldc_w -1248325150
    //   21: if_icmpeq -> 49
    //   24: iload_1
    //   25: ldc_w 2049276534
    //   28: if_icmpeq -> 34
    //   31: goto -> 79
    //   34: aload_0
    //   35: ldc_w 'application/java-archive'
    //   38: invokevirtual equals : (Ljava/lang/Object;)Z
    //   41: ifeq -> 31
    //   44: iconst_1
    //   45: istore_1
    //   46: goto -> 81
    //   49: aload_0
    //   50: ldc_w 'application/zip'
    //   53: invokevirtual equals : (Ljava/lang/Object;)Z
    //   56: ifeq -> 31
    //   59: iconst_2
    //   60: istore_1
    //   61: goto -> 81
    //   64: aload_0
    //   65: ldc_w 'application/rar'
    //   68: invokevirtual equals : (Ljava/lang/Object;)Z
    //   71: ifeq -> 31
    //   74: iconst_0
    //   75: istore_1
    //   76: goto -> 81
    //   79: iconst_m1
    //   80: istore_1
    //   81: iload_1
    //   82: ifeq -> 97
    //   85: iload_1
    //   86: iconst_1
    //   87: if_icmpeq -> 97
    //   90: iload_1
    //   91: iconst_2
    //   92: if_icmpeq -> 97
    //   95: iconst_0
    //   96: ireturn
    //   97: iconst_1
    //   98: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #643	-> 0
    //   #649	-> 95
    //   #647	-> 97
  }
  
  public static boolean isDocMimeType(String paramString) {
    byte b;
    paramString = normalizeMimeType(paramString);
    switch (paramString.hashCode()) {
      default:
        b = -1;
        break;
      case 1993842850:
        if (paramString.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
          b = 14;
          break;
        } 
      case 1496903267:
        if (paramString.equals("chemical/x-chemdraw")) {
          b = 0;
          break;
        } 
      case 904647503:
        if (paramString.equals("application/msword")) {
          b = 10;
          break;
        } 
      case 817335912:
        if (paramString.equals("text/plain")) {
          b = 7;
          break;
        } 
      case 501428239:
        if (paramString.equals("text/x-vcard")) {
          b = 4;
          break;
        } 
      case 262346941:
        if (paramString.equals("text/x-vcalendar")) {
          b = 5;
          break;
        } 
      case -366307023:
        if (paramString.equals("application/vnd.ms-excel")) {
          b = 11;
          break;
        } 
      case -958424608:
        if (paramString.equals("text/calendar")) {
          b = 3;
          break;
        } 
      case -1004747228:
        if (paramString.equals("text/csv")) {
          b = 2;
          break;
        } 
      case -1050893613:
        if (paramString.equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
          b = 13;
          break;
        } 
      case -1071817359:
        if (paramString.equals("application/vnd.ms-powerpoint")) {
          b = 12;
          break;
        } 
      case -1073633483:
        if (paramString.equals("application/vnd.openxmlformats-officedocument.presentationml.presentation")) {
          b = 15;
          break;
        } 
      case -1082243251:
        if (paramString.equals("text/html")) {
          b = 8;
          break;
        } 
      case -1248334925:
        if (paramString.equals("application/pdf")) {
          b = 9;
          break;
        } 
      case -2008589971:
        if (paramString.equals("application/epub+zip")) {
          b = 6;
          break;
        } 
      case -2135895576:
        if (paramString.equals("text/comma-separated-values")) {
          b = 1;
          break;
        } 
    } 
    switch (b) {
      default:
        return false;
      case 0:
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
      case 12:
      case 13:
      case 14:
      case 15:
        break;
    } 
    return true;
  }
  
  public static boolean isMediaMimeType(String paramString) {
    return (isAudioMimeType(paramString) || isVideoMimeType(paramString) || 
      isImageMimeType(paramString) || isPlayListMimeType(paramString));
  }
  
  public static boolean isRawImageMimeType(String paramString) {
    byte b;
    paramString = normalizeMimeType(paramString);
    switch (paramString.hashCode()) {
      default:
        b = -1;
        break;
      case 2111234748:
        if (paramString.equals("image/x-canon-cr2")) {
          b = 2;
          break;
        } 
      case 2099152524:
        if (paramString.equals("image/x-nikon-nrw")) {
          b = 3;
          break;
        } 
      case 2099152104:
        if (paramString.equals("image/x-nikon-nef")) {
          b = 9;
          break;
        } 
      case 1378106698:
        if (paramString.equals("image/x-olympus-orf")) {
          b = 6;
          break;
        } 
      case -332763809:
        if (paramString.equals("image/x-pentax-pef")) {
          b = 7;
          break;
        } 
      case -985160897:
        if (paramString.equals("image/x-panasonic-rw2")) {
          b = 5;
          break;
        } 
      case -1423313290:
        if (paramString.equals("image/x-adobe-dng")) {
          b = 0;
          break;
        } 
      case -1487103447:
        if (paramString.equals("image/tiff")) {
          b = 1;
          break;
        } 
      case -1594371159:
        if (paramString.equals("image/x-sony-arw")) {
          b = 4;
          break;
        } 
      case -1635437028:
        if (paramString.equals("image/x-samsung-srw")) {
          b = 8;
          break;
        } 
    } 
    switch (b) {
      default:
        return false;
      case 0:
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
        break;
    } 
    return true;
  }
  
  public static String getFileTitle(String paramString) {
    int i = paramString.lastIndexOf('/');
    String str = paramString;
    if (i >= 0) {
      i++;
      str = paramString;
      if (i < paramString.length())
        str = paramString.substring(i); 
    } 
    i = str.lastIndexOf('.');
    paramString = str;
    if (i > 0)
      paramString = str.substring(0, i); 
    return paramString;
  }
  
  public static String getFileExtension(String paramString) {
    if (paramString == null)
      return null; 
    int i = paramString.lastIndexOf('.');
    if (i >= 0)
      return paramString.substring(i + 1); 
    return null;
  }
  
  public static String getMimeType(String paramString, int paramInt) {
    paramString = getMimeTypeForFile(paramString);
    if (!"application/octet-stream".equals(paramString))
      return paramString; 
    return getMimeTypeForFormatCode(paramInt);
  }
  
  public static String getMimeTypeForFile(String paramString) {
    paramString = guessMimeTypeFromExtension(getFileExtension(paramString));
    if (paramString == null)
      paramString = "application/octet-stream"; 
    return paramString;
  }
  
  public static String getMimeTypeForFormatCode(int paramInt) {
    String str = sFormatToMimeTypeMap.get(Integer.valueOf(paramInt));
    if (str == null)
      str = "application/octet-stream"; 
    return str;
  }
  
  public static int getFormatCode(String paramString1, String paramString2) {
    int i = getFormatCodeForMimeType(paramString2);
    if (i != 12288)
      return i; 
    return getFormatCodeForFile(paramString1);
  }
  
  public static int getFormatCodeForFile(String paramString) {
    return getFormatCodeForMimeType(getMimeTypeForFile(paramString));
  }
  
  public static int getFormatCodeForMimeType(String paramString) {
    if (paramString == null)
      return 12288; 
    Integer integer2 = sMimeTypeToFormatMap.get(paramString);
    if (integer2 != null)
      return integer2.intValue(); 
    String str = normalizeMimeType(paramString);
    Integer integer1 = sMimeTypeToFormatMap.get(str);
    if (integer1 != null)
      return integer1.intValue(); 
    if (str.startsWith("audio/"))
      return 47360; 
    if (str.startsWith("video/"))
      return 47488; 
    if (str.startsWith("image/"))
      return 14336; 
    return 12288;
  }
  
  private static String normalizeMimeType(String paramString) {
    MimeMap mimeMap = MimeMap.getDefault();
    String str = mimeMap.guessExtensionFromMimeType(paramString);
    if (str != null) {
      String str1 = mimeMap.guessMimeTypeFromExtension(str);
      if (str1 != null)
        return str1; 
    } 
    if (paramString == null)
      paramString = "application/octet-stream"; 
    return paramString;
  }
  
  private static String guessMimeTypeFromExtension(String paramString) {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aload_0
    //   3: ifnull -> 288
    //   6: invokestatic getDefault : ()Llibcore/content/type/MimeMap;
    //   9: astore_1
    //   10: aload_1
    //   11: aload_0
    //   12: invokevirtual guessMimeTypeFromExtension : (Ljava/lang/String;)Ljava/lang/String;
    //   15: astore_1
    //   16: iconst_0
    //   17: istore_2
    //   18: iconst_0
    //   19: istore_3
    //   20: aload_1
    //   21: ifnull -> 92
    //   24: aload_1
    //   25: invokevirtual hashCode : ()I
    //   28: istore_2
    //   29: iload_2
    //   30: ldc_w -794081673
    //   33: if_icmpeq -> 61
    //   36: iload_2
    //   37: ldc_w 379957065
    //   40: if_icmpeq -> 46
    //   43: goto -> 74
    //   46: aload_1
    //   47: ldc_w 'audio/x-pn-realaudio-plugin'
    //   50: invokevirtual equals : (Ljava/lang/Object;)Z
    //   53: ifeq -> 43
    //   56: iconst_1
    //   57: istore_3
    //   58: goto -> 76
    //   61: aload_1
    //   62: ldc_w 'audio/x-pn-realaudio'
    //   65: invokevirtual equals : (Ljava/lang/Object;)Z
    //   68: ifeq -> 43
    //   71: goto -> 76
    //   74: iconst_m1
    //   75: istore_3
    //   76: iload_3
    //   77: ifeq -> 88
    //   80: iload_3
    //   81: iconst_1
    //   82: if_icmpeq -> 88
    //   85: goto -> 90
    //   88: aconst_null
    //   89: astore_1
    //   90: aload_1
    //   91: areturn
    //   92: aload_0
    //   93: getstatic java/util/Locale.ROOT : Ljava/util/Locale;
    //   96: invokevirtual toLowerCase : (Ljava/util/Locale;)Ljava/lang/String;
    //   99: astore_0
    //   100: aload_0
    //   101: invokevirtual hashCode : ()I
    //   104: lookupswitch default -> 156, 98867 -> 219, 99752 -> 204, 3106436 -> 189, 3106437 -> 174, 3298980 -> 159
    //   156: goto -> 234
    //   159: aload_0
    //   160: ldc_w 'm2ts'
    //   163: invokevirtual equals : (Ljava/lang/Object;)Z
    //   166: ifeq -> 156
    //   169: iconst_2
    //   170: istore_3
    //   171: goto -> 236
    //   174: aload_0
    //   175: ldc_w 'ebk3'
    //   178: invokevirtual equals : (Ljava/lang/Object;)Z
    //   181: ifeq -> 156
    //   184: iconst_4
    //   185: istore_3
    //   186: goto -> 236
    //   189: aload_0
    //   190: ldc_w 'ebk2'
    //   193: invokevirtual equals : (Ljava/lang/Object;)Z
    //   196: ifeq -> 156
    //   199: iconst_3
    //   200: istore_3
    //   201: goto -> 236
    //   204: aload_0
    //   205: ldc_w 'f4v'
    //   208: invokevirtual equals : (Ljava/lang/Object;)Z
    //   211: ifeq -> 156
    //   214: iconst_1
    //   215: istore_3
    //   216: goto -> 236
    //   219: aload_0
    //   220: ldc_w 'cue'
    //   223: invokevirtual equals : (Ljava/lang/Object;)Z
    //   226: ifeq -> 156
    //   229: iload_2
    //   230: istore_3
    //   231: goto -> 236
    //   234: iconst_m1
    //   235: istore_3
    //   236: iload_3
    //   237: ifeq -> 284
    //   240: iload_3
    //   241: iconst_1
    //   242: if_icmpeq -> 277
    //   245: iload_3
    //   246: iconst_2
    //   247: if_icmpeq -> 270
    //   250: iload_3
    //   251: iconst_3
    //   252: if_icmpeq -> 263
    //   255: iload_3
    //   256: iconst_4
    //   257: if_icmpeq -> 263
    //   260: goto -> 288
    //   263: ldc_w 'text/x-expandedbook'
    //   266: astore_1
    //   267: goto -> 288
    //   270: ldc_w 'video/m2ts'
    //   273: astore_1
    //   274: goto -> 288
    //   277: ldc_w 'video/x-flv'
    //   280: astore_1
    //   281: goto -> 288
    //   284: ldc_w 'audio/cue'
    //   287: astore_1
    //   288: aload_1
    //   289: ifnull -> 297
    //   292: aload_1
    //   293: astore_0
    //   294: goto -> 301
    //   297: ldc_w 'application/octet-stream'
    //   300: astore_0
    //   301: aload_0
    //   302: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #827	-> 0
    //   #828	-> 2
    //   #829	-> 6
    //   #830	-> 10
    //   #831	-> 16
    //   #832	-> 24
    //   #835	-> 88
    //   #836	-> 90
    //   #840	-> 90
    //   #842	-> 92
    //   #843	-> 100
    //   #855	-> 263
    //   #856	-> 267
    //   #851	-> 270
    //   #852	-> 274
    //   #848	-> 277
    //   #849	-> 281
    //   #845	-> 284
    //   #863	-> 288
  }
}
