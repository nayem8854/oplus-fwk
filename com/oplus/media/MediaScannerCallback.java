package com.oplus.media;

public interface MediaScannerCallback {
  void processFileBegin(String paramString);
  
  void processFileEnd(String paramString);
  
  void scanMtpFile(String paramString1, String paramString2, int paramInt1, int paramInt2);
  
  void setLocale(String paramString);
}
