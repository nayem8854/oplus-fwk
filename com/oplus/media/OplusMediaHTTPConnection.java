package com.oplus.media;

import android.media.MediaHTTPConnection;

public class OplusMediaHTTPConnection {
  private static final int MEDIA_ERROR_HTTP_PROTOCOL_ERROR = -214741;
  
  private static final String TAG = "MediaHTTPConnection";
  
  private static final boolean VERBOSE = false;
  
  MediaHTTPConnection mMediaHTTPConnection;
  
  public OplusMediaHTTPConnection(MediaHTTPConnection paramMediaHTTPConnection) {
    this.mMediaHTTPConnection = paramMediaHTTPConnection;
  }
  
  public int readAt(long paramLong, byte[] paramArrayOfbyte, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: lload_1
    //   4: aload_3
    //   5: iload #4
    //   7: iconst_0
    //   8: invokespecial readAt_internal : (J[BIZ)I
    //   11: istore #5
    //   13: iload #5
    //   15: istore #6
    //   17: iload #5
    //   19: ldc -214741
    //   21: if_icmpne -> 95
    //   24: new java/lang/StringBuilder
    //   27: astore #7
    //   29: aload #7
    //   31: invokespecial <init> : ()V
    //   34: aload #7
    //   36: ldc 'readAt '
    //   38: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   41: pop
    //   42: aload #7
    //   44: lload_1
    //   45: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   48: pop
    //   49: aload #7
    //   51: ldc ' / '
    //   53: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   56: pop
    //   57: aload #7
    //   59: iload #4
    //   61: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   64: pop
    //   65: aload #7
    //   67: ldc ' => protocol error, retry'
    //   69: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   72: pop
    //   73: ldc 'MediaHTTPConnection'
    //   75: aload #7
    //   77: invokevirtual toString : ()Ljava/lang/String;
    //   80: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   83: pop
    //   84: aload_0
    //   85: lload_1
    //   86: aload_3
    //   87: iload #4
    //   89: iconst_1
    //   90: invokespecial readAt_internal : (J[BIZ)I
    //   93: istore #6
    //   95: iload #6
    //   97: istore #5
    //   99: iload #6
    //   101: ldc -214741
    //   103: if_icmpne -> 163
    //   106: new java/lang/StringBuilder
    //   109: astore_3
    //   110: aload_3
    //   111: invokespecial <init> : ()V
    //   114: aload_3
    //   115: ldc 'readAt '
    //   117: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   120: pop
    //   121: aload_3
    //   122: lload_1
    //   123: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   126: pop
    //   127: aload_3
    //   128: ldc ' / '
    //   130: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   133: pop
    //   134: aload_3
    //   135: iload #4
    //   137: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   140: pop
    //   141: aload_3
    //   142: ldc ' => error, convert error'
    //   144: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   147: pop
    //   148: ldc 'MediaHTTPConnection'
    //   150: aload_3
    //   151: invokevirtual toString : ()Ljava/lang/String;
    //   154: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   157: pop
    //   158: sipush #-1010
    //   161: istore #5
    //   163: aload_0
    //   164: monitorexit
    //   165: iload #5
    //   167: ireturn
    //   168: astore_3
    //   169: aload_0
    //   170: monitorexit
    //   171: aload_3
    //   172: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #64	-> 2
    //   #65	-> 13
    //   #66	-> 24
    //   #67	-> 84
    //   #69	-> 95
    //   #70	-> 106
    //   #71	-> 158
    //   #73	-> 163
    //   #63	-> 168
    // Exception table:
    //   from	to	target	type
    //   2	13	168	finally
    //   24	84	168	finally
    //   84	95	168	finally
    //   106	158	168	finally
  }
  
  private int readAt_internal(long paramLong, byte[] paramArrayOfbyte, int paramInt, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new android/os/StrictMode$ThreadPolicy$Builder
    //   5: astore #6
    //   7: aload #6
    //   9: invokespecial <init> : ()V
    //   12: aload #6
    //   14: invokevirtual permitAll : ()Landroid/os/StrictMode$ThreadPolicy$Builder;
    //   17: invokevirtual build : ()Landroid/os/StrictMode$ThreadPolicy;
    //   20: astore #6
    //   22: aload #6
    //   24: invokestatic setThreadPolicy : (Landroid/os/StrictMode$ThreadPolicy;)V
    //   27: getstatic android/media/OplusMirrorMediaHTTPConnection.mCurrentOffset : Lcom/oplus/reflect/RefLong;
    //   30: astore #6
    //   32: aload #6
    //   34: ifnull -> 71
    //   37: getstatic android/media/OplusMirrorMediaHTTPConnection.mCurrentOffset : Lcom/oplus/reflect/RefLong;
    //   40: astore #7
    //   42: aload_0
    //   43: getfield mMediaHTTPConnection : Landroid/media/MediaHTTPConnection;
    //   46: astore #6
    //   48: lload_1
    //   49: aload #7
    //   51: aload #6
    //   53: invokevirtual get : (Ljava/lang/Object;)J
    //   56: lcmp
    //   57: ifne -> 76
    //   60: goto -> 71
    //   63: astore_3
    //   64: goto -> 201
    //   67: astore_3
    //   68: goto -> 206
    //   71: iload #5
    //   73: ifeq -> 104
    //   76: getstatic android/media/OplusMirrorMediaHTTPConnection.seekTo : Lcom/oplus/reflect/RefMethod;
    //   79: ifnull -> 104
    //   82: getstatic android/media/OplusMirrorMediaHTTPConnection.seekTo : Lcom/oplus/reflect/RefMethod;
    //   85: aload_0
    //   86: getfield mMediaHTTPConnection : Landroid/media/MediaHTTPConnection;
    //   89: iconst_1
    //   90: anewarray java/lang/Object
    //   93: dup
    //   94: iconst_0
    //   95: lload_1
    //   96: invokestatic valueOf : (J)Ljava/lang/Long;
    //   99: aastore
    //   100: invokevirtual call : (Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   103: pop
    //   104: sipush #-999
    //   107: istore #8
    //   109: getstatic android/media/OplusMirrorMediaHTTPConnection.mInputStream : Lcom/oplus/reflect/RefObject;
    //   112: ifnull -> 137
    //   115: getstatic android/media/OplusMirrorMediaHTTPConnection.mInputStream : Lcom/oplus/reflect/RefObject;
    //   118: aload_0
    //   119: getfield mMediaHTTPConnection : Landroid/media/MediaHTTPConnection;
    //   122: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   125: checkcast java/io/InputStream
    //   128: aload_3
    //   129: iconst_0
    //   130: iload #4
    //   132: invokevirtual read : ([BII)I
    //   135: istore #8
    //   137: iload #8
    //   139: istore #9
    //   141: iload #8
    //   143: iconst_m1
    //   144: if_icmpne -> 150
    //   147: iconst_0
    //   148: istore #9
    //   150: getstatic android/media/OplusMirrorMediaHTTPConnection.mCurrentOffset : Lcom/oplus/reflect/RefLong;
    //   153: ifnull -> 190
    //   156: getstatic android/media/OplusMirrorMediaHTTPConnection.mCurrentOffset : Lcom/oplus/reflect/RefLong;
    //   159: astore_3
    //   160: aload_0
    //   161: getfield mMediaHTTPConnection : Landroid/media/MediaHTTPConnection;
    //   164: astore #6
    //   166: aload_3
    //   167: aload #6
    //   169: invokevirtual get : (Ljava/lang/Object;)J
    //   172: lstore #10
    //   174: getstatic android/media/OplusMirrorMediaHTTPConnection.mCurrentOffset : Lcom/oplus/reflect/RefLong;
    //   177: aload_0
    //   178: getfield mMediaHTTPConnection : Landroid/media/MediaHTTPConnection;
    //   181: iload #9
    //   183: i2l
    //   184: lload #10
    //   186: ladd
    //   187: invokevirtual set : (Ljava/lang/Object;J)V
    //   190: aload_0
    //   191: monitorexit
    //   192: iload #9
    //   194: ireturn
    //   195: astore_3
    //   196: aload_0
    //   197: monitorexit
    //   198: iconst_m1
    //   199: ireturn
    //   200: astore_3
    //   201: aload_0
    //   202: monitorexit
    //   203: iconst_m1
    //   204: ireturn
    //   205: astore_3
    //   206: new java/lang/StringBuilder
    //   209: astore #6
    //   211: aload #6
    //   213: invokespecial <init> : ()V
    //   216: aload #6
    //   218: ldc 'readAt '
    //   220: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   223: pop
    //   224: aload #6
    //   226: lload_1
    //   227: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   230: pop
    //   231: aload #6
    //   233: ldc ' / '
    //   235: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   238: pop
    //   239: aload #6
    //   241: iload #4
    //   243: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   246: pop
    //   247: aload #6
    //   249: ldc ' => '
    //   251: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   254: pop
    //   255: aload #6
    //   257: aload_3
    //   258: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   261: pop
    //   262: ldc 'MediaHTTPConnection'
    //   264: aload #6
    //   266: invokevirtual toString : ()Ljava/lang/String;
    //   269: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   272: pop
    //   273: aload_0
    //   274: monitorexit
    //   275: sipush #-1010
    //   278: ireturn
    //   279: astore_3
    //   280: new java/lang/StringBuilder
    //   283: astore #6
    //   285: aload #6
    //   287: invokespecial <init> : ()V
    //   290: aload #6
    //   292: ldc 'readAt '
    //   294: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   297: pop
    //   298: aload #6
    //   300: lload_1
    //   301: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   304: pop
    //   305: aload #6
    //   307: ldc ' / '
    //   309: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   312: pop
    //   313: aload #6
    //   315: iload #4
    //   317: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   320: pop
    //   321: aload #6
    //   323: ldc ' => '
    //   325: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   328: pop
    //   329: aload #6
    //   331: aload_3
    //   332: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   335: pop
    //   336: ldc 'MediaHTTPConnection'
    //   338: aload #6
    //   340: invokevirtual toString : ()Ljava/lang/String;
    //   343: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   346: pop
    //   347: aload_0
    //   348: monitorexit
    //   349: sipush #-1010
    //   352: ireturn
    //   353: astore_3
    //   354: new java/lang/StringBuilder
    //   357: astore #6
    //   359: aload #6
    //   361: invokespecial <init> : ()V
    //   364: aload #6
    //   366: ldc 'readAt '
    //   368: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   371: pop
    //   372: aload #6
    //   374: lload_1
    //   375: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   378: pop
    //   379: aload #6
    //   381: ldc ' / '
    //   383: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   386: pop
    //   387: aload #6
    //   389: iload #4
    //   391: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   394: pop
    //   395: aload #6
    //   397: ldc ' => '
    //   399: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   402: pop
    //   403: aload #6
    //   405: aload_3
    //   406: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   409: pop
    //   410: ldc 'MediaHTTPConnection'
    //   412: aload #6
    //   414: invokevirtual toString : ()Ljava/lang/String;
    //   417: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   420: pop
    //   421: aload_0
    //   422: monitorexit
    //   423: ldc -214741
    //   425: ireturn
    //   426: astore_3
    //   427: aload_0
    //   428: monitorexit
    //   429: aload_3
    //   430: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #77	-> 2
    //   #78	-> 12
    //   #80	-> 22
    //   #85	-> 27
    //   #86	-> 48
    //   #125	-> 63
    //   #122	-> 67
    //   #86	-> 71
    //   #87	-> 76
    //   #88	-> 82
    //   #92	-> 104
    //   #93	-> 109
    //   #94	-> 115
    //   #97	-> 137
    //   #100	-> 147
    //   #103	-> 150
    //   #104	-> 156
    //   #105	-> 166
    //   #106	-> 174
    //   #115	-> 190
    //   #130	-> 195
    //   #135	-> 196
    //   #125	-> 200
    //   #129	-> 201
    //   #122	-> 205
    //   #123	-> 206
    //   #124	-> 273
    //   #119	-> 279
    //   #120	-> 280
    //   #121	-> 347
    //   #116	-> 353
    //   #117	-> 354
    //   #118	-> 421
    //   #76	-> 426
    // Exception table:
    //   from	to	target	type
    //   2	12	426	finally
    //   12	22	426	finally
    //   22	27	426	finally
    //   27	32	353	java/net/ProtocolException
    //   27	32	279	java/net/NoRouteToHostException
    //   27	32	205	java/net/UnknownServiceException
    //   27	32	200	java/io/IOException
    //   27	32	195	java/lang/Exception
    //   27	32	426	finally
    //   37	48	353	java/net/ProtocolException
    //   37	48	279	java/net/NoRouteToHostException
    //   37	48	67	java/net/UnknownServiceException
    //   37	48	63	java/io/IOException
    //   37	48	195	java/lang/Exception
    //   37	48	426	finally
    //   48	60	353	java/net/ProtocolException
    //   48	60	279	java/net/NoRouteToHostException
    //   48	60	67	java/net/UnknownServiceException
    //   48	60	63	java/io/IOException
    //   48	60	195	java/lang/Exception
    //   48	60	426	finally
    //   76	82	353	java/net/ProtocolException
    //   76	82	279	java/net/NoRouteToHostException
    //   76	82	67	java/net/UnknownServiceException
    //   76	82	63	java/io/IOException
    //   76	82	195	java/lang/Exception
    //   76	82	426	finally
    //   82	104	353	java/net/ProtocolException
    //   82	104	279	java/net/NoRouteToHostException
    //   82	104	67	java/net/UnknownServiceException
    //   82	104	63	java/io/IOException
    //   82	104	195	java/lang/Exception
    //   82	104	426	finally
    //   109	115	353	java/net/ProtocolException
    //   109	115	279	java/net/NoRouteToHostException
    //   109	115	67	java/net/UnknownServiceException
    //   109	115	63	java/io/IOException
    //   109	115	195	java/lang/Exception
    //   109	115	426	finally
    //   115	137	353	java/net/ProtocolException
    //   115	137	279	java/net/NoRouteToHostException
    //   115	137	67	java/net/UnknownServiceException
    //   115	137	63	java/io/IOException
    //   115	137	195	java/lang/Exception
    //   115	137	426	finally
    //   150	156	353	java/net/ProtocolException
    //   150	156	279	java/net/NoRouteToHostException
    //   150	156	67	java/net/UnknownServiceException
    //   150	156	63	java/io/IOException
    //   150	156	195	java/lang/Exception
    //   150	156	426	finally
    //   156	166	353	java/net/ProtocolException
    //   156	166	279	java/net/NoRouteToHostException
    //   156	166	67	java/net/UnknownServiceException
    //   156	166	63	java/io/IOException
    //   156	166	195	java/lang/Exception
    //   156	166	426	finally
    //   166	174	353	java/net/ProtocolException
    //   166	174	279	java/net/NoRouteToHostException
    //   166	174	67	java/net/UnknownServiceException
    //   166	174	63	java/io/IOException
    //   166	174	195	java/lang/Exception
    //   166	174	426	finally
    //   174	190	353	java/net/ProtocolException
    //   174	190	279	java/net/NoRouteToHostException
    //   174	190	67	java/net/UnknownServiceException
    //   174	190	63	java/io/IOException
    //   174	190	195	java/lang/Exception
    //   174	190	426	finally
    //   206	273	426	finally
    //   280	347	426	finally
    //   354	421	426	finally
  }
}
