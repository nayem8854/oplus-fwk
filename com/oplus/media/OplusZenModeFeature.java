package com.oplus.media;

import android.app.ActivityThread;
import android.content.Context;
import android.os.Parcel;
import android.os.Process;
import android.util.Log;
import java.lang.reflect.Method;

public class OplusZenModeFeature implements IOplusZenModeFeature {
  private Object mNotificationManager = null;
  
  private boolean mNeedMute = false;
  
  private boolean mInterceptFlag = false;
  
  private boolean mRecoverFlag = false;
  
  private int mStreamType = Integer.MIN_VALUE;
  
  private static final String ATLAS_KEY_AUDIO_CHECK_LISTINFO_BYNAME = "check_listinfo_byname";
  
  private static final String ATLAS_KEY_AUDIO_GET_LISTINFO_BYNAME = "get_listinfo_byname";
  
  public static final int MUTE_FLAG = 1;
  
  private static final String SYSTEM_NOTIFICATION_AUDIO_PATH = "/system/media/audio/notifications/";
  
  private static final String TAG = "OplusZenModeFeature";
  
  public static final int UNMUTE_FLAG = 0;
  
  private Method mAtlasGetParameters;
  
  private Object mAtlasInstance;
  
  private Method mshouldInterceptSound;
  
  public OplusZenModeFeature() {
    Log.d("OplusZenModeFeature", "new OplusZenModeFeature");
    try {
      Class<?> clazz = Class.forName("com.oppo.atlas.OppoAtlasManager");
      Method method = clazz.getMethod("getInstance", new Class[] { Context.class });
      this.mAtlasGetParameters = clazz.getMethod("getParameters", new Class[] { String.class });
      Object object = new Object();
      this();
      this.mAtlasInstance = method.invoke(object, new Object[] { null });
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public Parcel checkZenMode() {
    String str2;
    int i = this.mStreamType;
    if (i == 0 || i == 3)
      return null; 
    String str1 = ActivityThread.currentPackageName();
    if (str1 == null || str1.length() <= 0)
      return null; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("check_listinfo_byname=zenmode=");
    stringBuilder.append(str1);
    String str3 = stringBuilder.toString();
    stringBuilder = null;
    try {
      str2 = str3 = (String)this.mAtlasGetParameters.invoke(this.mAtlasInstance, new Object[] { str3 });
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    if (str2 != null && str2.equals("true")) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("The package name is ");
      stringBuilder1.append(str1);
      stringBuilder1.append("qq & wecha zenmode control !");
      Log.i("OplusZenModeFeature", stringBuilder1.toString());
      i = Process.myUid();
      if (this.mNotificationManager == null)
        try {
          Class<?> clazz = Class.forName("android.app.OplusNotificationManager");
          this.mshouldInterceptSound = clazz.getMethod("shouldInterceptSound", new Class[] { String.class, int.class });
          this.mNotificationManager = clazz.getConstructor(new Class[0]).newInstance(new Object[0]);
          this.mshouldInterceptSound.setAccessible(true);
        } catch (Exception exception) {
          exception.printStackTrace();
        }  
      Object object = this.mNotificationManager;
      if (object != null) {
        Method method = this.mshouldInterceptSound;
        if (method != null)
          try {
            this.mInterceptFlag = ((Boolean)method.invoke(object, new Object[] { str1, Integer.valueOf(i) })).booleanValue();
          } catch (Exception exception) {
            exception.printStackTrace();
          }  
      } 
      Parcel parcel = Parcel.obtain();
      parcel.writeInt(this.mInterceptFlag);
      return parcel;
    } 
    return null;
  }
  
  public Parcel checkWechatMute() {
    String str2, str1 = ActivityThread.currentPackageName();
    if (str1 == null || str1.length() <= 0)
      return null; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("get_listinfo_byname=zenmode=");
    stringBuilder.append(str1);
    String str3 = stringBuilder.toString();
    stringBuilder = null;
    try {
      str2 = str3 = (String)this.mAtlasGetParameters.invoke(this.mAtlasInstance, new Object[] { str3 });
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    if (str2 != null && str2.equals("wechat_mute")) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("The package name is ");
      stringBuilder1.append(str1);
      stringBuilder1.append("wecha zenmode control !");
      Log.i("OplusZenModeFeature", stringBuilder1.toString());
      int i = this.mStreamType;
      if (i == 5 || i == 2) {
        Parcel parcel = Parcel.obtain();
        parcel.writeInt(this.mInterceptFlag);
        return parcel;
      } 
    } 
    return null;
  }
  
  public void resetZenModeFlag() {
    this.mInterceptFlag = false;
  }
  
  public void setAudioStreamType(int paramInt) {
    this.mStreamType = paramInt;
  }
}
