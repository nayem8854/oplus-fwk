package com.oplus.media;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.os.Parcel;

public interface IOplusZenModeFeature extends IOplusCommonFeature {
  public static final IOplusZenModeFeature DEFAULT = (IOplusZenModeFeature)new Object();
  
  public static final int KEY_PARAMETER_INTERCEPT = 10011;
  
  public static final String NAME = "IOplusZenModeFeature";
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusZenModeFeature;
  }
  
  default IOplusCommonFeature getDefault() {
    return DEFAULT;
  }
  
  default Parcel checkZenMode() {
    return null;
  }
  
  default Parcel checkWechatMute() {
    return null;
  }
  
  default void resetZenModeFlag() {}
  
  default void setAudioStreamType(int paramInt) {}
}
