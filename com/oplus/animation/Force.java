package com.oplus.animation;

interface Force {
  float getAcceleration(float paramFloat1, float paramFloat2);
  
  boolean isAtEquilibrium(float paramFloat1, float paramFloat2);
}
