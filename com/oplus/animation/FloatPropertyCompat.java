package com.oplus.animation;

import android.util.FloatProperty;

public abstract class FloatPropertyCompat<T> {
  final String mPropertyName;
  
  public FloatPropertyCompat(String paramString) {
    this.mPropertyName = paramString;
  }
  
  public static <T> FloatPropertyCompat<T> createFloatPropertyCompat(FloatProperty<T> paramFloatProperty) {
    return (FloatPropertyCompat<T>)new Object(paramFloatProperty.getName(), paramFloatProperty);
  }
  
  public abstract float getValue(T paramT);
  
  public abstract void setValue(T paramT, float paramFloat);
}
