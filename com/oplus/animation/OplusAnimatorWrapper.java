package com.oplus.animation;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.view.View;

public class OplusAnimatorWrapper {
  private final Animator mAnimation;
  
  private final OnSetValuesCallback mCallback;
  
  protected final Class<?> mTagClass = getClass();
  
  public OplusAnimatorWrapper(Animator paramAnimator, OnSetValuesCallback paramOnSetValuesCallback) {
    this.mAnimation = paramAnimator;
    this.mCallback = paramOnSetValuesCallback;
  }
  
  public Animator getAnimation() {
    return this.mAnimation;
  }
  
  public void initialize() {
    if (this.mCallback != null && this.mAnimation instanceof ValueAnimator) {
      View view = getTarget();
      if (view == null)
        return; 
      view.setVisibility(0);
      this.mCallback.initialize(view);
      view.setPivotX(this.mCallback.getPivotXValue(view));
      view.setPivotY(this.mCallback.getPivotYValue(view));
      float f1 = this.mCallback.getStartValue(view);
      float f2 = this.mCallback.getEndValue(view);
      ((ValueAnimator)this.mAnimation).setFloatValues(new float[] { f1, f2 });
    } 
  }
  
  private View getTarget() {
    Animator animator = this.mAnimation;
    if (animator instanceof ObjectAnimator) {
      Object object = ((ObjectAnimator)animator).getTarget();
      if (object instanceof View)
        return (View)object; 
    } 
    return null;
  }
  
  public static interface OnSetValuesCallback {
    float getEndValue(View param1View);
    
    float getPivotXValue(View param1View);
    
    float getPivotYValue(View param1View);
    
    float getStartValue(View param1View);
    
    void initialize(View param1View);
  }
  
  class OnSetValuesCallbackAdapter implements OnSetValuesCallback {
    public void initialize(View param1View) {}
    
    public float getPivotXValue(View param1View) {
      return param1View.getPivotX();
    }
    
    public float getPivotYValue(View param1View) {
      return param1View.getPivotY();
    }
    
    public float getStartValue(View param1View) {
      return 0.0F;
    }
    
    public float getEndValue(View param1View) {
      return 0.0F;
    }
  }
}
