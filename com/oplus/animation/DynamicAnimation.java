package com.oplus.animation;

import android.view.View;
import java.util.ArrayList;

public abstract class DynamicAnimation<T extends DynamicAnimation<T>> implements AnimationHandler.AnimationFrameCallback {
  public static abstract class ViewProperty extends FloatPropertyCompat<View> {
    private ViewProperty(String param1String) {
      super(param1String);
    }
  }
  
  public static final ViewProperty TRANSLATION_X = (ViewProperty)new Object("translationX");
  
  public static final ViewProperty TRANSLATION_Y = (ViewProperty)new Object("translationY");
  
  public static final ViewProperty TRANSLATION_Z = (ViewProperty)new Object("translationZ");
  
  static {
    SCALE_X = (ViewProperty)new Object("scaleX");
    SCALE_Y = (ViewProperty)new Object("scaleY");
    ROTATION = (ViewProperty)new Object("rotation");
    ROTATION_X = (ViewProperty)new Object("rotationX");
    ROTATION_Y = (ViewProperty)new Object("rotationY");
    X = (ViewProperty)new Object("x");
    Y = (ViewProperty)new Object("y");
    Z = (ViewProperty)new Object("z");
    ALPHA = (ViewProperty)new Object("alpha");
    SCROLL_X = (ViewProperty)new Object("scrollX");
    SCROLL_Y = (ViewProperty)new Object("scrollY");
  }
  
  float mVelocity = 0.0F;
  
  float mValue = Float.MAX_VALUE;
  
  boolean mStartValueIsSet = false;
  
  boolean mRunning = false;
  
  float mMaxValue = Float.MAX_VALUE;
  
  float mMinValue = -Float.MAX_VALUE;
  
  private long mLastFrameTime = 0L;
  
  private final ArrayList<OnAnimationEndListener> mEndListeners = new ArrayList<>();
  
  private final ArrayList<OnAnimationUpdateListener> mUpdateListeners = new ArrayList<>();
  
  public static final ViewProperty ALPHA;
  
  public static final float MIN_VISIBLE_CHANGE_ALPHA = 0.00390625F;
  
  public static final float MIN_VISIBLE_CHANGE_PIXELS = 1.0F;
  
  public static final float MIN_VISIBLE_CHANGE_ROTATION_DEGREES = 0.1F;
  
  public static final float MIN_VISIBLE_CHANGE_SCALE = 0.002F;
  
  public static final ViewProperty ROTATION;
  
  public static final ViewProperty ROTATION_X;
  
  public static final ViewProperty ROTATION_Y;
  
  public static final ViewProperty SCALE_X;
  
  public static final ViewProperty SCALE_Y;
  
  public static final ViewProperty SCROLL_X;
  
  public static final ViewProperty SCROLL_Y;
  
  private static final float THRESHOLD_MULTIPLIER = 0.75F;
  
  private static final float UNSET = 3.4028235E38F;
  
  public static final ViewProperty X;
  
  public static final ViewProperty Y;
  
  public static final ViewProperty Z;
  
  private float mMinVisibleChange;
  
  final FloatPropertyCompat mProperty;
  
  final Object mTarget;
  
  class OnAnimationUpdateListener {
    public abstract void onAnimationUpdate(DynamicAnimation param1DynamicAnimation, float param1Float1, float param1Float2);
  }
  
  class OnAnimationEndListener {
    public abstract void onAnimationEnd(DynamicAnimation param1DynamicAnimation, boolean param1Boolean, float param1Float1, float param1Float2);
  }
  
  class MassState {
    float mValue;
    
    float mVelocity;
  }
  
  DynamicAnimation(final FloatValueHolder floatValueHolder) {
    this.mTarget = null;
    this.mProperty = new FloatPropertyCompat("FloatValueHolder") {
        final DynamicAnimation this$0;
        
        final FloatValueHolder val$floatValueHolder;
        
        public float getValue(Object param1Object) {
          return floatValueHolder.getValue();
        }
        
        public void setValue(Object param1Object, float param1Float) {
          floatValueHolder.setValue(param1Float);
        }
      };
    this.mMinVisibleChange = 1.0F;
  }
  
  <K> DynamicAnimation(K paramK, FloatPropertyCompat<K> paramFloatPropertyCompat) {
    this.mTarget = paramK;
    this.mProperty = paramFloatPropertyCompat;
    if (paramFloatPropertyCompat == ROTATION || paramFloatPropertyCompat == ROTATION_X || paramFloatPropertyCompat == ROTATION_Y) {
      this.mMinVisibleChange = 0.1F;
      return;
    } 
    if (paramFloatPropertyCompat == ALPHA) {
      this.mMinVisibleChange = 0.00390625F;
    } else {
      if (paramFloatPropertyCompat == SCALE_X || paramFloatPropertyCompat == SCALE_Y) {
        this.mMinVisibleChange = 0.00390625F;
        return;
      } 
      this.mMinVisibleChange = 1.0F;
    } 
  }
  
  public T setStartValue(float paramFloat) {
    this.mValue = paramFloat;
    this.mStartValueIsSet = true;
    return (T)this;
  }
  
  public T setStartVelocity(float paramFloat) {
    this.mVelocity = paramFloat;
    return (T)this;
  }
  
  public T setMaxValue(float paramFloat) {
    this.mMaxValue = paramFloat;
    return (T)this;
  }
  
  public T setMinValue(float paramFloat) {
    this.mMinValue = paramFloat;
    return (T)this;
  }
  
  public T addEndListener(OnAnimationEndListener paramOnAnimationEndListener) {
    if (!this.mEndListeners.contains(paramOnAnimationEndListener))
      this.mEndListeners.add(paramOnAnimationEndListener); 
    return (T)this;
  }
  
  public void removeEndListener(OnAnimationEndListener paramOnAnimationEndListener) {
    removeEntry(this.mEndListeners, paramOnAnimationEndListener);
  }
  
  public T addUpdateListener(OnAnimationUpdateListener paramOnAnimationUpdateListener) {
    if (!isRunning()) {
      if (!this.mUpdateListeners.contains(paramOnAnimationUpdateListener))
        this.mUpdateListeners.add(paramOnAnimationUpdateListener); 
      return (T)this;
    } 
    throw new UnsupportedOperationException("Error: Update listeners must be added beforethe animation.");
  }
  
  public void removeUpdateListener(OnAnimationUpdateListener paramOnAnimationUpdateListener) {
    removeEntry(this.mUpdateListeners, paramOnAnimationUpdateListener);
  }
  
  public T setMinimumVisibleChange(float paramFloat) {
    if (paramFloat > 0.0F) {
      this.mMinVisibleChange = paramFloat;
      setValueThreshold(0.75F * paramFloat);
      return (T)this;
    } 
    throw new IllegalArgumentException("Minimum visible change must be positive.");
  }
  
  public float getMinimumVisibleChange() {
    return this.mMinVisibleChange;
  }
  
  private static <T> void removeNullEntries(ArrayList<T> paramArrayList) {
    for (int i = paramArrayList.size() - 1; i >= 0; i--) {
      if (paramArrayList.get(i) == null)
        paramArrayList.remove(i); 
    } 
  }
  
  private static <T> void removeEntry(ArrayList<T> paramArrayList, T paramT) {
    int i = paramArrayList.indexOf(paramT);
    if (i >= 0)
      paramArrayList.set(i, null); 
  }
  
  public void start() {
    if (!this.mRunning)
      startAnimationInternal(); 
  }
  
  public void cancel() {
    if (this.mRunning)
      endAnimationInternal(true); 
  }
  
  public boolean isRunning() {
    return this.mRunning;
  }
  
  private void startAnimationInternal() {
    if (!this.mRunning) {
      this.mRunning = true;
      if (!this.mStartValueIsSet)
        this.mValue = getPropertyValue(); 
      float f = this.mValue;
      if (f <= this.mMaxValue && f >= this.mMinValue) {
        AnimationHandler.getInstance().addAnimationFrameCallback(this, 0L);
      } else {
        throw new IllegalArgumentException("Starting value need to be in between min value and max value");
      } 
    } 
  }
  
  public boolean doAnimationFrame(long paramLong) {
    long l = this.mLastFrameTime;
    if (l == 0L) {
      this.mLastFrameTime = paramLong;
      setPropertyValue(this.mValue);
      return false;
    } 
    this.mLastFrameTime = paramLong;
    boolean bool = updateValueAndVelocity(paramLong - l);
    float f = Math.min(this.mValue, this.mMaxValue);
    this.mValue = f = Math.max(f, this.mMinValue);
    setPropertyValue(f);
    if (bool)
      endAnimationInternal(false); 
    return bool;
  }
  
  private void endAnimationInternal(boolean paramBoolean) {
    this.mRunning = false;
    AnimationHandler.getInstance().removeCallback(this);
    this.mLastFrameTime = 0L;
    this.mStartValueIsSet = false;
    for (byte b = 0; b < this.mEndListeners.size(); b++) {
      if (this.mEndListeners.get(b) != null)
        ((OnAnimationEndListener)this.mEndListeners.get(b)).onAnimationEnd(this, paramBoolean, this.mValue, this.mVelocity); 
    } 
    removeNullEntries(this.mEndListeners);
  }
  
  void setPropertyValue(float paramFloat) {
    this.mProperty.setValue(this.mTarget, paramFloat);
    for (byte b = 0; b < this.mUpdateListeners.size(); b++) {
      if (this.mUpdateListeners.get(b) != null)
        ((OnAnimationUpdateListener)this.mUpdateListeners.get(b)).onAnimationUpdate(this, this.mValue, this.mVelocity); 
    } 
    removeNullEntries(this.mUpdateListeners);
  }
  
  float getValueThreshold() {
    return this.mMinVisibleChange * 0.75F;
  }
  
  private float getPropertyValue() {
    return this.mProperty.getValue(this.mTarget);
  }
  
  abstract float getAcceleration(float paramFloat1, float paramFloat2);
  
  abstract boolean isAtEquilibrium(float paramFloat1, float paramFloat2);
  
  abstract void setValueThreshold(float paramFloat);
  
  abstract boolean updateValueAndVelocity(long paramLong);
}
