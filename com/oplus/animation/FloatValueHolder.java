package com.oplus.animation;

public final class FloatValueHolder {
  private float mValue = 0.0F;
  
  public FloatValueHolder(float paramFloat) {
    setValue(paramFloat);
  }
  
  public void setValue(float paramFloat) {
    this.mValue = paramFloat;
  }
  
  public float getValue() {
    return this.mValue;
  }
  
  public FloatValueHolder() {}
}
