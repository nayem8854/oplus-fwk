package com.oplus.animation;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.ArrayMap;
import android.view.Choreographer;
import java.util.ArrayList;

class AnimationHandler {
  class AnimationCallbackDispatcher {
    final AnimationHandler this$0;
    
    public void dispatchAnimationFrame() {
      AnimationHandler.access$002(AnimationHandler.this, SystemClock.uptimeMillis());
      AnimationHandler animationHandler = AnimationHandler.this;
      animationHandler.doAnimationFrame(animationHandler.mCurrentFrameTime);
      if (AnimationHandler.this.mAnimationCallbacks.size() > 0)
        AnimationHandler.this.getProvider().postFrameCallback(); 
    }
  }
  
  public static final ThreadLocal<AnimationHandler> sAnimatorHandler = new ThreadLocal<>();
  
  private final ArrayMap<AnimationFrameCallback, Long> mDelayedCallbackStartTime = new ArrayMap();
  
  private final ArrayList<AnimationFrameCallback> mAnimationCallbacks = new ArrayList<>();
  
  private final AnimationCallbackDispatcher mCallbackDispatcher = new AnimationCallbackDispatcher();
  
  private long mCurrentFrameTime = 0L;
  
  private boolean mListDirty = false;
  
  private static final long FRAME_DELAY_MS = 10L;
  
  private AnimationFrameCallbackProvider mProvider;
  
  public static AnimationHandler getInstance() {
    if (sAnimatorHandler.get() == null)
      sAnimatorHandler.set(new AnimationHandler()); 
    return sAnimatorHandler.get();
  }
  
  public static long getFrameTime() {
    if (sAnimatorHandler.get() == null)
      return 0L; 
    return ((AnimationHandler)sAnimatorHandler.get()).mCurrentFrameTime;
  }
  
  public void setProvider(AnimationFrameCallbackProvider paramAnimationFrameCallbackProvider) {
    this.mProvider = paramAnimationFrameCallbackProvider;
  }
  
  private AnimationFrameCallbackProvider getProvider() {
    if (this.mProvider == null)
      if (Build.VERSION.SDK_INT >= 16) {
        this.mProvider = new FrameCallbackProvider16(this.mCallbackDispatcher);
      } else {
        this.mProvider = new FrameCallbackProvider14(this.mCallbackDispatcher);
      }  
    return this.mProvider;
  }
  
  public void addAnimationFrameCallback(AnimationFrameCallback paramAnimationFrameCallback, long paramLong) {
    if (this.mAnimationCallbacks.size() == 0)
      getProvider().postFrameCallback(); 
    if (!this.mAnimationCallbacks.contains(paramAnimationFrameCallback))
      this.mAnimationCallbacks.add(paramAnimationFrameCallback); 
    if (paramLong > 0L)
      this.mDelayedCallbackStartTime.put(paramAnimationFrameCallback, Long.valueOf(SystemClock.uptimeMillis() + paramLong)); 
  }
  
  public void removeCallback(AnimationFrameCallback paramAnimationFrameCallback) {
    this.mDelayedCallbackStartTime.remove(paramAnimationFrameCallback);
    int i = this.mAnimationCallbacks.indexOf(paramAnimationFrameCallback);
    if (i >= 0) {
      this.mAnimationCallbacks.set(i, null);
      this.mListDirty = true;
    } 
  }
  
  private void doAnimationFrame(long paramLong) {
    long l = SystemClock.uptimeMillis();
    for (byte b = 0; b < this.mAnimationCallbacks.size(); b++) {
      AnimationFrameCallback animationFrameCallback = this.mAnimationCallbacks.get(b);
      if (animationFrameCallback != null)
        if (isCallbackDue(animationFrameCallback, l))
          animationFrameCallback.doAnimationFrame(paramLong);  
    } 
    cleanUpList();
  }
  
  private boolean isCallbackDue(AnimationFrameCallback paramAnimationFrameCallback, long paramLong) {
    Long long_ = (Long)this.mDelayedCallbackStartTime.get(paramAnimationFrameCallback);
    if (long_ == null)
      return true; 
    if (long_.longValue() < paramLong) {
      this.mDelayedCallbackStartTime.remove(paramAnimationFrameCallback);
      return true;
    } 
    return false;
  }
  
  private void cleanUpList() {
    if (this.mListDirty) {
      for (int i = this.mAnimationCallbacks.size() - 1; i >= 0; i--) {
        if (this.mAnimationCallbacks.get(i) == null)
          this.mAnimationCallbacks.remove(i); 
      } 
      this.mListDirty = false;
    } 
  }
  
  class FrameCallbackProvider16 extends AnimationFrameCallbackProvider {
    private final Choreographer mChoreographer = Choreographer.getInstance();
    
    private final Choreographer.FrameCallback mChoreographerCallback;
    
    FrameCallbackProvider16(AnimationHandler this$0) {
      super((AnimationHandler.AnimationCallbackDispatcher)this$0);
      this.mChoreographerCallback = new Choreographer.FrameCallback() {
          final AnimationHandler.FrameCallbackProvider16 this$0;
          
          public void doFrame(long param2Long) {
            AnimationHandler.FrameCallbackProvider16.this.mDispatcher.dispatchAnimationFrame();
          }
        };
    }
    
    void postFrameCallback() {
      this.mChoreographer.postFrameCallback(this.mChoreographerCallback);
    }
  }
  
  class FrameCallbackProvider14 extends AnimationFrameCallbackProvider {
    private final Handler mHandler;
    
    private long mLastFrameTime = -1L;
    
    private final Runnable mRunnable;
    
    FrameCallbackProvider14(AnimationHandler this$0) {
      super((AnimationHandler.AnimationCallbackDispatcher)this$0);
      this.mRunnable = new Runnable() {
          final AnimationHandler.FrameCallbackProvider14 this$0;
          
          public void run() {
            AnimationHandler.FrameCallbackProvider14.access$402(this.this$0, SystemClock.uptimeMillis());
            this.this$0.mDispatcher.dispatchAnimationFrame();
          }
        };
      this.mHandler = new Handler(Looper.myLooper());
    }
    
    void postFrameCallback() {
      long l1 = SystemClock.uptimeMillis(), l2 = this.mLastFrameTime;
      l1 = Math.max(10L - l1 - l2, 0L);
      this.mHandler.postDelayed(this.mRunnable, l1);
    }
  }
  
  class null implements Runnable {
    final AnimationHandler.FrameCallbackProvider14 this$0;
    
    public void run() {
      AnimationHandler.FrameCallbackProvider14.access$402(this.this$0, SystemClock.uptimeMillis());
      this.this$0.mDispatcher.dispatchAnimationFrame();
    }
  }
  
  public static abstract class AnimationFrameCallbackProvider {
    final AnimationHandler.AnimationCallbackDispatcher mDispatcher;
    
    AnimationFrameCallbackProvider(AnimationHandler.AnimationCallbackDispatcher param1AnimationCallbackDispatcher) {
      this.mDispatcher = param1AnimationCallbackDispatcher;
    }
    
    abstract void postFrameCallback();
  }
  
  static interface AnimationFrameCallback {
    boolean doAnimationFrame(long param1Long);
  }
}
