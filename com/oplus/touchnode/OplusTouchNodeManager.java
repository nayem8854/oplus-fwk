package com.oplus.touchnode;

import android.app.OplusActivityTaskManager;
import android.os.Binder;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.util.Log;

public class OplusTouchNodeManager {
  public static final int BASELINE_RESULT_NODE = 14;
  
  public static final int BASELINE_TEST_NODE = 3;
  
  public static final int BLACK_SCREEN_RESULT_NODE = 15;
  
  public static final int BLACK_SCREEN_TEST_NODE = 13;
  
  public static final int CALIBRATION_NODE = 4;
  
  public static final int CHARGE_DETECT_NODE = 10;
  
  public static final int COORDINATE_NODE = 2;
  
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  public static final int DEBUG_BASELINE_NODE = 18;
  
  public static final int DEBUG_DELTA_NODE = 17;
  
  public static final int DEBUG_HEALTH_MONITOR_NODE = 20;
  
  public static final int DOUBLE_TAP_ENABLE_NODE = 1;
  
  public static final int DOUBLE_TAP_INDEP_NODE = 21;
  
  public static final int HEAD_SET_DETECT_NODE = 12;
  
  public static final int HOVER_SELFDATA_NODE = 19;
  
  public static final int KERNEL_GRIP_HANDLE_NODE = 8;
  
  public static final int OPLUS_TP_DIRECTION_NODE = 5;
  
  public static final int OPLUS_TP_LIMIT_ENABLE_NODE = 7;
  
  public static final int OPLUS_TP_LIMIT_WHITELIST_NODE = 6;
  
  public static final int REPORT_RATE_WHITE_LIST_NODE = 9;
  
  private static final String TAG = "OplusTouchNodeManager";
  
  public static final int TOUCH_OPTIMIZED_TIME_NODE = 22;
  
  public static final int TP_AGING_TEST_NODE = 16;
  
  public static final int WIRELESS_CHARGE_DETECT_NODE = 11;
  
  private static volatile OplusTouchNodeManager sInstance;
  
  public static OplusTouchNodeManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/touchnode/OplusTouchNodeManager.sInstance : Lcom/oplus/touchnode/OplusTouchNodeManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/touchnode/OplusTouchNodeManager
    //   8: monitorenter
    //   9: getstatic com/oplus/touchnode/OplusTouchNodeManager.sInstance : Lcom/oplus/touchnode/OplusTouchNodeManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/touchnode/OplusTouchNodeManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/touchnode/OplusTouchNodeManager.sInstance : Lcom/oplus/touchnode/OplusTouchNodeManager;
    //   27: ldc com/oplus/touchnode/OplusTouchNodeManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/touchnode/OplusTouchNodeManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/touchnode/OplusTouchNodeManager.sInstance : Lcom/oplus/touchnode/OplusTouchNodeManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #63	-> 0
    //   #64	-> 6
    //   #65	-> 9
    //   #66	-> 15
    //   #68	-> 27
    //   #70	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  private OplusActivityTaskManager mOAms = new OplusActivityTaskManager();
  
  public String readNodeFile(int paramInt) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("readNodeFile, uid: ");
      stringBuilder.append(Binder.getCallingUid());
      stringBuilder.append(", pid: ");
      stringBuilder.append(Binder.getCallingPid());
      stringBuilder.append(", nodeFlag: ");
      stringBuilder.append(paramInt);
      Log.d("OplusTouchNodeManager", stringBuilder.toString());
    } 
    try {
      return this.mOAms.readNodeFile(paramInt);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("readNodeFile failed, err: ");
      stringBuilder.append(remoteException);
      Log.e("OplusTouchNodeManager", stringBuilder.toString());
      return null;
    } 
  }
  
  public boolean writeNodeFile(int paramInt, String paramString) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("writeNodeFile, uid: ");
      stringBuilder.append(Binder.getCallingUid());
      stringBuilder.append(", pid: ");
      stringBuilder.append(Binder.getCallingPid());
      stringBuilder.append(", nodeFlag: ");
      stringBuilder.append(paramInt);
      stringBuilder.append(", info: ");
      stringBuilder.append(paramString);
      Log.d("OplusTouchNodeManager", stringBuilder.toString());
    } 
    try {
      return this.mOAms.writeNodeFile(paramInt, paramString);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("writeNodeFile failed, err: ");
      stringBuilder.append(remoteException);
      Log.e("OplusTouchNodeManager", stringBuilder.toString());
      return false;
    } 
  }
}
