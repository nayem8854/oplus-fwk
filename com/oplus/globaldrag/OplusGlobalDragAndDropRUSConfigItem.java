package com.oplus.globaldrag;

import android.os.Parcel;
import android.os.Parcelable;

public class OplusGlobalDragAndDropRUSConfigItem implements Parcelable {
  private int mAppVersionCode = 0;
  
  private int mFeatureVersionCode = 1;
  
  protected OplusGlobalDragAndDropRUSConfigItem(Parcel paramParcel) {
    this.mAttrStr = paramParcel.readStringNoHelper();
    this.mAppVersionCode = paramParcel.readInt();
    this.mFeatureVersionCode = paramParcel.readInt();
  }
  
  public static final Parcelable.Creator<OplusGlobalDragAndDropRUSConfigItem> CREATOR = new Parcelable.Creator<OplusGlobalDragAndDropRUSConfigItem>() {
      public OplusGlobalDragAndDropRUSConfigItem createFromParcel(Parcel param1Parcel) {
        return new OplusGlobalDragAndDropRUSConfigItem(param1Parcel);
      }
      
      public OplusGlobalDragAndDropRUSConfigItem[] newArray(int param1Int) {
        return new OplusGlobalDragAndDropRUSConfigItem[param1Int];
      }
    };
  
  private String mAttrStr;
  
  public String getAttrStr() {
    return this.mAttrStr;
  }
  
  public void setAttrStr(String paramString) {
    this.mAttrStr = paramString;
  }
  
  public int getAppVersionCode() {
    return this.mAppVersionCode;
  }
  
  public void setAppVersionCode(int paramInt) {
    this.mAppVersionCode = paramInt;
  }
  
  public int getFeatureVersionCode() {
    return this.mFeatureVersionCode;
  }
  
  public void setFeatureVersionCode(int paramInt) {
    this.mFeatureVersionCode = paramInt;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeStringNoHelper(this.mAttrStr);
    paramParcel.writeInt(this.mAppVersionCode);
    paramParcel.writeInt(this.mFeatureVersionCode);
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("OplusGlobalDragAndDropRUSConfigItem{");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("attr=");
    stringBuilder2.append(this.mAttrStr);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", AppVersionCode=");
    stringBuilder2.append(this.mAppVersionCode);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", FeatureVersionCode=");
    stringBuilder2.append(this.mFeatureVersionCode);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append("}");
    return stringBuilder1.toString();
  }
  
  public OplusGlobalDragAndDropRUSConfigItem() {}
}
