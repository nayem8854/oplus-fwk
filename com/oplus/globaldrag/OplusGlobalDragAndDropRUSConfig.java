package com.oplus.globaldrag;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public class OplusGlobalDragAndDropRUSConfig implements Parcelable {
  public OplusGlobalDragAndDropRUSConfig() {
    this.mSupportDragPkgList = new ArrayList<>();
    this.mSupportDropPkgList = new ArrayList<>();
    this.mUnSupportCpnList = new ArrayList<>();
  }
  
  protected OplusGlobalDragAndDropRUSConfig(Parcel paramParcel) {
    boolean bool;
    this.mSupportDragPkgList = new ArrayList<>();
    this.mSupportDropPkgList = new ArrayList<>();
    this.mUnSupportCpnList = new ArrayList<>();
    this.mVersion = paramParcel.readInt();
    if (paramParcel.readByte() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mGlobalDragShareWindowSwitch = bool;
    this.mFilterName = paramParcel.readStringNoHelper();
    this.mSupportDragPkgList = paramParcel.createTypedArrayList(OplusGlobalDragAndDropRUSConfigItem.CREATOR);
    this.mSupportDropPkgList = paramParcel.createTypedArrayList(OplusGlobalDragAndDropRUSConfigItem.CREATOR);
    this.mUnSupportCpnList = paramParcel.createTypedArrayList(OplusGlobalDragAndDropRUSConfigItem.CREATOR);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mVersion);
    paramParcel.writeByte((byte)this.mGlobalDragShareWindowSwitch);
    paramParcel.writeStringNoHelper(this.mFilterName);
    paramParcel.writeTypedList(this.mSupportDragPkgList);
    paramParcel.writeTypedList(this.mSupportDropPkgList);
    paramParcel.writeTypedList(this.mUnSupportCpnList);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<OplusGlobalDragAndDropRUSConfig> CREATOR = new Parcelable.Creator<OplusGlobalDragAndDropRUSConfig>() {
      public OplusGlobalDragAndDropRUSConfig createFromParcel(Parcel param1Parcel) {
        return new OplusGlobalDragAndDropRUSConfig(param1Parcel);
      }
      
      public OplusGlobalDragAndDropRUSConfig[] newArray(int param1Int) {
        return new OplusGlobalDragAndDropRUSConfig[param1Int];
      }
    };
  
  private String mFilterName;
  
  private boolean mGlobalDragShareWindowSwitch;
  
  private List<OplusGlobalDragAndDropRUSConfigItem> mSupportDragPkgList;
  
  private List<OplusGlobalDragAndDropRUSConfigItem> mSupportDropPkgList;
  
  private List<OplusGlobalDragAndDropRUSConfigItem> mUnSupportCpnList;
  
  private int mVersion;
  
  public int getVersion() {
    return this.mVersion;
  }
  
  public void setVersion(int paramInt) {
    this.mVersion = paramInt;
  }
  
  public boolean getGlobalDragShareSwitch() {
    return this.mGlobalDragShareWindowSwitch;
  }
  
  public void setGlobalDragShareSwitch(boolean paramBoolean) {
    this.mGlobalDragShareWindowSwitch = paramBoolean;
  }
  
  public List<OplusGlobalDragAndDropRUSConfigItem> getSupportDragPkgList() {
    return this.mSupportDragPkgList;
  }
  
  public void setSupportDragPkgList(List<OplusGlobalDragAndDropRUSConfigItem> paramList) {
    this.mSupportDragPkgList = paramList;
  }
  
  public List<OplusGlobalDragAndDropRUSConfigItem> getSupportDropPkgList() {
    return this.mSupportDropPkgList;
  }
  
  public void setSupportDropPkgList(List<OplusGlobalDragAndDropRUSConfigItem> paramList) {
    this.mSupportDropPkgList = paramList;
  }
  
  public List<OplusGlobalDragAndDropRUSConfigItem> getUnSupportCpnList() {
    return this.mUnSupportCpnList;
  }
  
  public void setUnSupportCpnList(List<OplusGlobalDragAndDropRUSConfigItem> paramList) {
    this.mUnSupportCpnList = paramList;
  }
  
  public String getFilterName() {
    return this.mFilterName;
  }
  
  public void setFilterName(String paramString) {
    this.mFilterName = paramString;
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("OplusGlobalDragAndDropRUSConfig{");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("version = ");
    stringBuilder2.append(this.mVersion);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nfilter-name = ");
    stringBuilder2.append(this.mFilterName);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nmGlobalDragShareWindowSwitch = ");
    stringBuilder2.append(this.mGlobalDragShareWindowSwitch);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nmSupportDragPkgList = ");
    stringBuilder2.append(this.mSupportDragPkgList);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nmSupportDropPkgList = ");
    stringBuilder2.append(this.mSupportDropPkgList);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append("}");
    return stringBuilder1.toString();
  }
}
