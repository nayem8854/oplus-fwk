package com.oplus.heimdall;

import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;

public class HeimdallManager {
  public static final String HEIMDALL_SERVICE = "heimdall";
  
  private static final String TAG = HeimdallManager.class.getSimpleName();
  
  private static IBinder.DeathRecipient sDeathRecipient;
  
  private static HeimdallManager sHeimdallManager = null;
  
  private static IHeimdallService sService;
  
  static {
    sDeathRecipient = (IBinder.DeathRecipient)new Object();
  }
  
  public static HeimdallManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/heimdall/HeimdallManager.sHeimdallManager : Lcom/oplus/heimdall/HeimdallManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/heimdall/HeimdallManager
    //   8: monitorenter
    //   9: getstatic com/oplus/heimdall/HeimdallManager.sHeimdallManager : Lcom/oplus/heimdall/HeimdallManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/heimdall/HeimdallManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/heimdall/HeimdallManager.sHeimdallManager : Lcom/oplus/heimdall/HeimdallManager;
    //   27: ldc com/oplus/heimdall/HeimdallManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/heimdall/HeimdallManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/heimdall/HeimdallManager.sHeimdallManager : Lcom/oplus/heimdall/HeimdallManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #45	-> 0
    //   #46	-> 6
    //   #47	-> 9
    //   #48	-> 15
    //   #50	-> 27
    //   #52	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  private HeimdallManager() {
    IBinder iBinder = null;
    if (sService == null) {
      iBinder = ServiceManager.getService("heimdall");
      sService = IHeimdallService.Stub.asInterface(iBinder);
    } 
    if (iBinder != null)
      try {
        iBinder.linkToDeath(sDeathRecipient, 0);
      } catch (RemoteException remoteException) {
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("exception happened when linkToDeathRecipient : ");
        stringBuilder.append(remoteException.getMessage());
        Log.e(str, stringBuilder.toString());
        sService = null;
      }  
  }
  
  public ICrashService getCrashService() {
    try {
      if (sService != null)
        return sService.getCrashService(); 
      return null;
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("failed to getCrashService : ");
      stringBuilder.append(remoteException.getMessage());
      Log.e(str, stringBuilder.toString());
      return null;
    } 
  }
  
  public ITraceService getTraceService() {
    try {
      if (sService != null)
        return sService.getTraceService(); 
      return null;
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("failed to getTraceService : ");
      stringBuilder.append(remoteException.getMessage());
      Log.e(str, stringBuilder.toString());
      return null;
    } 
  }
  
  public IRootService getRootService() {
    try {
      if (sService != null)
        return sService.getRootService(); 
      return null;
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("failed to getRootService : ");
      stringBuilder.append(remoteException.getMessage());
      Log.e(str, stringBuilder.toString());
      return null;
    } 
  }
}
