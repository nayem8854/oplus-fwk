package com.oplus.heimdall;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IRootCallback extends IInterface {
  void getRootStatus(boolean paramBoolean) throws RemoteException;
  
  class Default implements IRootCallback {
    public void getRootStatus(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRootCallback {
    private static final String DESCRIPTOR = "com.oplus.heimdall.IRootCallback";
    
    static final int TRANSACTION_getRootStatus = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.heimdall.IRootCallback");
    }
    
    public static IRootCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.heimdall.IRootCallback");
      if (iInterface != null && iInterface instanceof IRootCallback)
        return (IRootCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "getRootStatus";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.heimdall.IRootCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.heimdall.IRootCallback");
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      getRootStatus(bool);
      return true;
    }
    
    private static class Proxy implements IRootCallback {
      public static IRootCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.heimdall.IRootCallback";
      }
      
      public void getRootStatus(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.oplus.heimdall.IRootCallback");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && IRootCallback.Stub.getDefaultImpl() != null) {
            IRootCallback.Stub.getDefaultImpl().getRootStatus(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRootCallback param1IRootCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRootCallback != null) {
          Proxy.sDefaultImpl = param1IRootCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRootCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
