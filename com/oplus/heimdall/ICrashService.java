package com.oplus.heimdall;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface ICrashService extends IInterface {
  boolean addListener(String paramString1, String paramString2, ICrashListener paramICrashListener, List<String> paramList) throws RemoteException;
  
  boolean removeListener(String paramString1, String paramString2) throws RemoteException;
  
  class Default implements ICrashService {
    public boolean addListener(String param1String1, String param1String2, ICrashListener param1ICrashListener, List<String> param1List) throws RemoteException {
      return false;
    }
    
    public boolean removeListener(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICrashService {
    private static final String DESCRIPTOR = "com.oplus.heimdall.ICrashService";
    
    static final int TRANSACTION_addListener = 1;
    
    static final int TRANSACTION_removeListener = 2;
    
    public Stub() {
      attachInterface(this, "com.oplus.heimdall.ICrashService");
    }
    
    public static ICrashService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.heimdall.ICrashService");
      if (iInterface != null && iInterface instanceof ICrashService)
        return (ICrashService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "removeListener";
      } 
      return "addListener";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str1;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("com.oplus.heimdall.ICrashService");
          return true;
        } 
        param1Parcel1.enforceInterface("com.oplus.heimdall.ICrashService");
        String str = param1Parcel1.readString();
        str1 = param1Parcel1.readString();
        boolean bool1 = removeListener(str, str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      str1.enforceInterface("com.oplus.heimdall.ICrashService");
      String str2 = str1.readString();
      String str3 = str1.readString();
      ICrashListener iCrashListener = ICrashListener.Stub.asInterface(str1.readStrongBinder());
      ArrayList<String> arrayList = str1.createStringArrayList();
      boolean bool = addListener(str2, str3, iCrashListener, arrayList);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements ICrashService {
      public static ICrashService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.heimdall.ICrashService";
      }
      
      public boolean addListener(String param2String1, String param2String2, ICrashListener param2ICrashListener, List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.heimdall.ICrashService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2ICrashListener != null) {
            iBinder = param2ICrashListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeStringList(param2List);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && ICrashService.Stub.getDefaultImpl() != null) {
            bool1 = ICrashService.Stub.getDefaultImpl().addListener(param2String1, param2String2, param2ICrashListener, param2List);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeListener(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.heimdall.ICrashService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && ICrashService.Stub.getDefaultImpl() != null) {
            bool1 = ICrashService.Stub.getDefaultImpl().removeListener(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICrashService param1ICrashService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICrashService != null) {
          Proxy.sDefaultImpl = param1ICrashService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICrashService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
