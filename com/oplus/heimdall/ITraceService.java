package com.oplus.heimdall;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ITraceService extends IInterface {
  boolean addListener(String paramString, ITraceListener paramITraceListener) throws RemoteException;
  
  boolean isTraceOn() throws RemoteException;
  
  boolean removeListener(String paramString) throws RemoteException;
  
  class Default implements ITraceService {
    public boolean addListener(String param1String, ITraceListener param1ITraceListener) throws RemoteException {
      return false;
    }
    
    public boolean removeListener(String param1String) throws RemoteException {
      return false;
    }
    
    public boolean isTraceOn() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITraceService {
    private static final String DESCRIPTOR = "com.oplus.heimdall.ITraceService";
    
    static final int TRANSACTION_addListener = 1;
    
    static final int TRANSACTION_isTraceOn = 3;
    
    static final int TRANSACTION_removeListener = 2;
    
    public Stub() {
      attachInterface(this, "com.oplus.heimdall.ITraceService");
    }
    
    public static ITraceService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.heimdall.ITraceService");
      if (iInterface != null && iInterface instanceof ITraceService)
        return (ITraceService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "isTraceOn";
        } 
        return "removeListener";
      } 
      return "addListener";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str1;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.oplus.heimdall.ITraceService");
            return true;
          } 
          param1Parcel1.enforceInterface("com.oplus.heimdall.ITraceService");
          boolean bool2 = isTraceOn();
          param1Parcel2.writeNoException();
          param1Parcel2.writeInt(bool2);
          return true;
        } 
        param1Parcel1.enforceInterface("com.oplus.heimdall.ITraceService");
        str1 = param1Parcel1.readString();
        boolean bool1 = removeListener(str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      str1.enforceInterface("com.oplus.heimdall.ITraceService");
      String str2 = str1.readString();
      ITraceListener iTraceListener = ITraceListener.Stub.asInterface(str1.readStrongBinder());
      boolean bool = addListener(str2, iTraceListener);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements ITraceService {
      public static ITraceService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.heimdall.ITraceService";
      }
      
      public boolean addListener(String param2String, ITraceListener param2ITraceListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.heimdall.ITraceService");
          parcel1.writeString(param2String);
          if (param2ITraceListener != null) {
            iBinder = param2ITraceListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && ITraceService.Stub.getDefaultImpl() != null) {
            bool1 = ITraceService.Stub.getDefaultImpl().addListener(param2String, param2ITraceListener);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeListener(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.heimdall.ITraceService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && ITraceService.Stub.getDefaultImpl() != null) {
            bool1 = ITraceService.Stub.getDefaultImpl().removeListener(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isTraceOn() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.heimdall.ITraceService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && ITraceService.Stub.getDefaultImpl() != null) {
            bool1 = ITraceService.Stub.getDefaultImpl().isTraceOn();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITraceService param1ITraceService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITraceService != null) {
          Proxy.sDefaultImpl = param1ITraceService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITraceService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
