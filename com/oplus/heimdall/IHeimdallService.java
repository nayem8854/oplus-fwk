package com.oplus.heimdall;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IHeimdallService extends IInterface {
  ICrashService getCrashService() throws RemoteException;
  
  IRootService getRootService() throws RemoteException;
  
  ITraceService getTraceService() throws RemoteException;
  
  class Default implements IHeimdallService {
    public ICrashService getCrashService() throws RemoteException {
      return null;
    }
    
    public IRootService getRootService() throws RemoteException {
      return null;
    }
    
    public ITraceService getTraceService() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IHeimdallService {
    private static final String DESCRIPTOR = "com.oplus.heimdall.IHeimdallService";
    
    static final int TRANSACTION_getCrashService = 1;
    
    static final int TRANSACTION_getRootService = 2;
    
    static final int TRANSACTION_getTraceService = 3;
    
    public Stub() {
      attachInterface(this, "com.oplus.heimdall.IHeimdallService");
    }
    
    public static IHeimdallService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.heimdall.IHeimdallService");
      if (iInterface != null && iInterface instanceof IHeimdallService)
        return (IHeimdallService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "getTraceService";
        } 
        return "getRootService";
      } 
      return "getCrashService";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IBinder iBinder2, iBinder1;
      ITraceService iTraceService = null;
      IRootService iRootService2 = null;
      Parcel parcel = null;
      if (param1Int1 != 1) {
        IBinder iBinder;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.oplus.heimdall.IHeimdallService");
            return true;
          } 
          param1Parcel1.enforceInterface("com.oplus.heimdall.IHeimdallService");
          iTraceService = getTraceService();
          param1Parcel2.writeNoException();
          param1Parcel1 = parcel;
          if (iTraceService != null)
            iBinder = iTraceService.asBinder(); 
          param1Parcel2.writeStrongBinder(iBinder);
          return true;
        } 
        iBinder.enforceInterface("com.oplus.heimdall.IHeimdallService");
        iRootService2 = getRootService();
        param1Parcel2.writeNoException();
        ITraceService iTraceService1 = iTraceService;
        if (iRootService2 != null)
          iBinder2 = iRootService2.asBinder(); 
        param1Parcel2.writeStrongBinder(iBinder2);
        return true;
      } 
      iBinder2.enforceInterface("com.oplus.heimdall.IHeimdallService");
      ICrashService iCrashService = getCrashService();
      param1Parcel2.writeNoException();
      IRootService iRootService1 = iRootService2;
      if (iCrashService != null)
        iBinder1 = iCrashService.asBinder(); 
      param1Parcel2.writeStrongBinder(iBinder1);
      return true;
    }
    
    private static class Proxy implements IHeimdallService {
      public static IHeimdallService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.heimdall.IHeimdallService";
      }
      
      public ICrashService getCrashService() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.heimdall.IHeimdallService");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IHeimdallService.Stub.getDefaultImpl() != null)
            return IHeimdallService.Stub.getDefaultImpl().getCrashService(); 
          parcel2.readException();
          return ICrashService.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IRootService getRootService() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.heimdall.IHeimdallService");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IHeimdallService.Stub.getDefaultImpl() != null)
            return IHeimdallService.Stub.getDefaultImpl().getRootService(); 
          parcel2.readException();
          return IRootService.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ITraceService getTraceService() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.heimdall.IHeimdallService");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IHeimdallService.Stub.getDefaultImpl() != null)
            return IHeimdallService.Stub.getDefaultImpl().getTraceService(); 
          parcel2.readException();
          return ITraceService.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IHeimdallService param1IHeimdallService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IHeimdallService != null) {
          Proxy.sDefaultImpl = param1IHeimdallService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IHeimdallService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
