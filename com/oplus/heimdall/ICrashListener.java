package com.oplus.heimdall;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ICrashListener extends IInterface {
  void onNotify(String paramString) throws RemoteException;
  
  class Default implements ICrashListener {
    public void onNotify(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICrashListener {
    private static final String DESCRIPTOR = "com.oplus.heimdall.ICrashListener";
    
    static final int TRANSACTION_onNotify = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.heimdall.ICrashListener");
    }
    
    public static ICrashListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.heimdall.ICrashListener");
      if (iInterface != null && iInterface instanceof ICrashListener)
        return (ICrashListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onNotify";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.heimdall.ICrashListener");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.heimdall.ICrashListener");
      String str = param1Parcel1.readString();
      onNotify(str);
      return true;
    }
    
    private static class Proxy implements ICrashListener {
      public static ICrashListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.heimdall.ICrashListener";
      }
      
      public void onNotify(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.heimdall.ICrashListener");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ICrashListener.Stub.getDefaultImpl() != null) {
            ICrashListener.Stub.getDefaultImpl().onNotify(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICrashListener param1ICrashListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICrashListener != null) {
          Proxy.sDefaultImpl = param1ICrashListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICrashListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
