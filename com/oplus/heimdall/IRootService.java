package com.oplus.heimdall;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IRootService extends IInterface {
  boolean isRoot(String paramString) throws RemoteException;
  
  void isRootForceUpdate(String paramString, IRootCallback paramIRootCallback) throws RemoteException;
  
  class Default implements IRootService {
    public boolean isRoot(String param1String) throws RemoteException {
      return false;
    }
    
    public void isRootForceUpdate(String param1String, IRootCallback param1IRootCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRootService {
    private static final String DESCRIPTOR = "com.oplus.heimdall.IRootService";
    
    static final int TRANSACTION_isRoot = 1;
    
    static final int TRANSACTION_isRootForceUpdate = 2;
    
    public Stub() {
      attachInterface(this, "com.oplus.heimdall.IRootService");
    }
    
    public static IRootService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.heimdall.IRootService");
      if (iInterface != null && iInterface instanceof IRootService)
        return (IRootService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "isRootForceUpdate";
      } 
      return "isRoot";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IRootCallback iRootCallback;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("com.oplus.heimdall.IRootService");
          return true;
        } 
        param1Parcel1.enforceInterface("com.oplus.heimdall.IRootService");
        String str1 = param1Parcel1.readString();
        iRootCallback = IRootCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
        isRootForceUpdate(str1, iRootCallback);
        param1Parcel2.writeNoException();
        return true;
      } 
      iRootCallback.enforceInterface("com.oplus.heimdall.IRootService");
      String str = iRootCallback.readString();
      boolean bool = isRoot(str);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements IRootService {
      public static IRootService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.heimdall.IRootService";
      }
      
      public boolean isRoot(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.heimdall.IRootService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IRootService.Stub.getDefaultImpl() != null) {
            bool1 = IRootService.Stub.getDefaultImpl().isRoot(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void isRootForceUpdate(String param2String, IRootCallback param2IRootCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oplus.heimdall.IRootService");
          parcel1.writeString(param2String);
          if (param2IRootCallback != null) {
            iBinder = param2IRootCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IRootService.Stub.getDefaultImpl() != null) {
            IRootService.Stub.getDefaultImpl().isRootForceUpdate(param2String, param2IRootCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRootService param1IRootService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRootService != null) {
          Proxy.sDefaultImpl = param1IRootService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRootService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
