package com.oplus.direct;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.oplus.util.OplusLog;

public class OplusDirectFindCmd implements Parcelable {
  public static final Parcelable.Creator<OplusDirectFindCmd> CREATOR = new Parcelable.Creator<OplusDirectFindCmd>() {
      public OplusDirectFindCmd createFromParcel(Parcel param1Parcel) {
        return new OplusDirectFindCmd(param1Parcel);
      }
      
      public OplusDirectFindCmd[] newArray(int param1Int) {
        return new OplusDirectFindCmd[param1Int];
      }
    };
  
  public static final String EXTRA_CMD = "direct_find_cmd";
  
  public static final String EXTRA_ID_NAMES = "id_names";
  
  private static final String TAG = "OplusDirectFindCmd";
  
  private final Bundle mBundle = new Bundle();
  
  private IOplusDirectFindCallback mCallback = null;
  
  public OplusDirectFindCmd(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Cmd=");
    stringBuilder.append(this.mBundle);
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.mBundle.writeToParcel(paramParcel, paramInt);
    if (this.mCallback != null) {
      paramParcel.writeInt(1);
      paramParcel.writeStrongBinder(this.mCallback.asBinder());
    } else {
      paramParcel.writeInt(0);
    } 
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mBundle.readFromParcel(paramParcel);
    if (paramParcel.readInt() == 1)
      this.mCallback = IOplusDirectFindCallback.Stub.asInterface(paramParcel.readStrongBinder()); 
  }
  
  public Bundle getBundle() {
    return this.mBundle;
  }
  
  public void putCommand(String paramString) {
    this.mBundle.putString("direct_find_cmd", paramString);
  }
  
  public OplusDirectFindCmds getCommand() {
    OplusDirectFindCmds oplusDirectFindCmds = null;
    try {
      OplusDirectFindCmds oplusDirectFindCmds1 = OplusDirectFindCmds.valueOf(this.mBundle.getString("direct_find_cmd"));
      oplusDirectFindCmds = oplusDirectFindCmds1;
      if (oplusDirectFindCmds1 == null) {
        oplusDirectFindCmds = OplusDirectFindCmds.UNKNOWN;
        return oplusDirectFindCmds;
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("getCommand ERROR : ");
      stringBuilder.append(Log.getStackTraceString(exception));
      OplusLog.e("OplusDirectFindCmd", stringBuilder.toString());
      if (!false) {
        oplusDirectFindCmds = OplusDirectFindCmds.UNKNOWN;
        return oplusDirectFindCmds;
      } 
    } finally {}
    return oplusDirectFindCmds;
  }
  
  public void setCallback(IOplusDirectFindCallback paramIOplusDirectFindCallback) {
    this.mCallback = paramIOplusDirectFindCallback;
  }
  
  public IOplusDirectFindCallback getCallback() {
    return this.mCallback;
  }
  
  public OplusDirectFindCmd() {}
}
