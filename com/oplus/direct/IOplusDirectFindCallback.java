package com.oplus.direct;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusDirectFindCallback extends IInterface {
  void onDirectInfoFound(OplusDirectFindResult paramOplusDirectFindResult) throws RemoteException;
  
  class Default implements IOplusDirectFindCallback {
    public void onDirectInfoFound(OplusDirectFindResult param1OplusDirectFindResult) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusDirectFindCallback {
    private static final String DESCRIPTOR = "com.oplus.direct.IOplusDirectFindCallback";
    
    static final int TRANSACTION_onDirectInfoFound = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.direct.IOplusDirectFindCallback");
    }
    
    public static IOplusDirectFindCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.direct.IOplusDirectFindCallback");
      if (iInterface != null && iInterface instanceof IOplusDirectFindCallback)
        return (IOplusDirectFindCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onDirectInfoFound";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.direct.IOplusDirectFindCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.direct.IOplusDirectFindCallback");
      if (param1Parcel1.readInt() != 0) {
        OplusDirectFindResult oplusDirectFindResult = (OplusDirectFindResult)OplusDirectFindResult.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onDirectInfoFound((OplusDirectFindResult)param1Parcel1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IOplusDirectFindCallback {
      public static IOplusDirectFindCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.direct.IOplusDirectFindCallback";
      }
      
      public void onDirectInfoFound(OplusDirectFindResult param2OplusDirectFindResult) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.direct.IOplusDirectFindCallback");
          if (param2OplusDirectFindResult != null) {
            parcel1.writeInt(1);
            param2OplusDirectFindResult.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusDirectFindCallback.Stub.getDefaultImpl() != null) {
            IOplusDirectFindCallback.Stub.getDefaultImpl().onDirectInfoFound(param2OplusDirectFindResult);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusDirectFindCallback param1IOplusDirectFindCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusDirectFindCallback != null) {
          Proxy.sDefaultImpl = param1IOplusDirectFindCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusDirectFindCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
