package com.oplus.direct;

public enum OplusDirectFindCmds {
  FIND_FAVORITE, FIND_TEXT, SAVE_FAVORITE, UNKNOWN;
  
  private static final OplusDirectFindCmds[] $VALUES;
  
  static {
    FIND_FAVORITE = new OplusDirectFindCmds("FIND_FAVORITE", 1);
    SAVE_FAVORITE = new OplusDirectFindCmds("SAVE_FAVORITE", 2);
    OplusDirectFindCmds oplusDirectFindCmds = new OplusDirectFindCmds("UNKNOWN", 3);
    $VALUES = new OplusDirectFindCmds[] { FIND_TEXT, FIND_FAVORITE, SAVE_FAVORITE, oplusDirectFindCmds };
  }
}
