package com.oplus.direct;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public class OplusDirectFindResult implements Parcelable {
  public static final Parcelable.Creator<OplusDirectFindResult> CREATOR = new Parcelable.Creator<OplusDirectFindResult>() {
      public OplusDirectFindResult createFromParcel(Parcel param1Parcel) {
        return new OplusDirectFindResult(param1Parcel);
      }
      
      public OplusDirectFindResult[] newArray(int param1Int) {
        return new OplusDirectFindResult[param1Int];
      }
    };
  
  public static final String ERROR_NO_MAINWIN = "no_mainwin";
  
  public static final String ERROR_NO_TEXT = "no_text";
  
  public static final String ERROR_NO_VIEW = "no_view";
  
  public static final String ERROR_NO_VIEWROOT = "no_viewroot";
  
  public static final String ERROR_UNKNOWN_CMD = "unknown_cmd";
  
  public static final String EXTRA_ERROR = "direct_find_error";
  
  public static final String EXTRA_NO_IDNAMES = "no_idnames";
  
  public static final String EXTRA_RESULT_TEXT = "result_text";
  
  private static final String TAG = "OplusDirectFindResult";
  
  private final Bundle mBundle = new Bundle();
  
  public OplusDirectFindResult(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Result=");
    stringBuilder.append(this.mBundle);
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.mBundle.writeToParcel(paramParcel, paramInt);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mBundle.readFromParcel(paramParcel);
  }
  
  public Bundle getBundle() {
    return this.mBundle;
  }
  
  public OplusDirectFindResult() {}
}
