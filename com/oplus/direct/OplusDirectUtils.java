package com.oplus.direct;

import android.os.Bundle;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.util.Log;
import com.oplus.util.OplusLog;

public class OplusDirectUtils {
  public static final boolean DBG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  public static final String TAG = "DirectService";
  
  public static void onFindSuccess(IOplusDirectFindCallback paramIOplusDirectFindCallback, Bundle paramBundle) {
    if (paramIOplusDirectFindCallback != null)
      try {
        paramIOplusDirectFindCallback.onDirectInfoFound(createSuccessResult(paramBundle));
      } catch (RemoteException remoteException) {
        OplusLog.e("DirectService", Log.getStackTraceString((Throwable)remoteException));
      } catch (Exception exception) {
        OplusLog.e("DirectService", Log.getStackTraceString(exception));
      }  
  }
  
  public static void onFindFailed(IOplusDirectFindCallback paramIOplusDirectFindCallback, String paramString) {
    if (paramIOplusDirectFindCallback != null)
      try {
        paramIOplusDirectFindCallback.onDirectInfoFound(createFailedResult(paramString));
      } catch (RemoteException remoteException) {
        OplusLog.e("DirectService", Log.getStackTraceString((Throwable)remoteException));
      } catch (Exception exception) {
        OplusLog.e("DirectService", Log.getStackTraceString(exception));
      }  
  }
  
  private static OplusDirectFindResult createSuccessResult(Bundle paramBundle) {
    OplusDirectFindResult oplusDirectFindResult = new OplusDirectFindResult();
    if (paramBundle != null && !paramBundle.isEmpty()) {
      Bundle bundle = oplusDirectFindResult.getBundle();
      bundle.putAll(paramBundle);
    } 
    return oplusDirectFindResult;
  }
  
  private static OplusDirectFindResult createFailedResult(String paramString) {
    OplusDirectFindResult oplusDirectFindResult = new OplusDirectFindResult();
    Bundle bundle = oplusDirectFindResult.getBundle();
    bundle.putString("direct_find_error", paramString);
    return oplusDirectFindResult;
  }
}
