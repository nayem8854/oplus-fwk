package com.oplus.keylayout;

import android.app.OplusActivityTaskManager;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.util.Log;

public class OplusKeyLayoutManager {
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static final String TAG = "OplusKeyLayoutManager";
  
  private static OplusKeyLayoutManager sInstance;
  
  public static OplusKeyLayoutManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/keylayout/OplusKeyLayoutManager.sInstance : Lcom/oplus/keylayout/OplusKeyLayoutManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/keylayout/OplusKeyLayoutManager
    //   8: monitorenter
    //   9: getstatic com/oplus/keylayout/OplusKeyLayoutManager.sInstance : Lcom/oplus/keylayout/OplusKeyLayoutManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/keylayout/OplusKeyLayoutManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/keylayout/OplusKeyLayoutManager.sInstance : Lcom/oplus/keylayout/OplusKeyLayoutManager;
    //   27: ldc com/oplus/keylayout/OplusKeyLayoutManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/keylayout/OplusKeyLayoutManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/keylayout/OplusKeyLayoutManager.sInstance : Lcom/oplus/keylayout/OplusKeyLayoutManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #34	-> 0
    //   #35	-> 6
    //   #36	-> 9
    //   #37	-> 15
    //   #39	-> 27
    //   #41	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  private OplusActivityTaskManager mOAms = new OplusActivityTaskManager();
  
  public void setGimbalLaunchPkg(String paramString) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setGimbalLaunchPkg, pkgName: ");
      stringBuilder.append(paramString);
      Log.i("OplusKeyLayoutManager", stringBuilder.toString());
    } 
    try {
      if (this.mOAms != null)
        this.mOAms.setGimbalLaunchPkg(paramString); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setGimbalLaunchPkg remoteException ");
      stringBuilder.append(remoteException);
      Log.e("OplusKeyLayoutManager", stringBuilder.toString());
      remoteException.printStackTrace();
    } 
  }
}
