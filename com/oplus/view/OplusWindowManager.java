package com.oplus.view;

import android.os.RemoteException;
import android.view.OplusBaseLayoutParams;
import android.view.WindowManager;
import android.view.WindowManagerGlobal;
import com.oplus.util.OplusTypeCastingHelper;

public final class OplusWindowManager implements IOplusWindowManagerConstans {
  public static final String TAG = "OplusWindowManager";
  
  public static void setNoMoveAnimation(WindowManager.LayoutParams paramLayoutParams, boolean paramBoolean) {
    if (paramBoolean) {
      paramLayoutParams.privateFlags |= 0x40;
    } else {
      paramLayoutParams.privateFlags &= 0xFFFFFFBF;
    } 
  }
  
  public static void setNavigationBarColor(WindowManager.LayoutParams paramLayoutParams, int paramInt, boolean paramBoolean) {
    OplusBaseLayoutParams oplusBaseLayoutParams = typeCast(paramLayoutParams);
    if (oplusBaseLayoutParams != null)
      if (paramBoolean) {
        oplusBaseLayoutParams.mOplusLayoutParams.setUpdateNavigationBar(paramBoolean);
        oplusBaseLayoutParams.mOplusLayoutParams.setNavigationBarColor(paramInt);
      } else if (isColorLight(paramInt)) {
        oplusBaseLayoutParams.navigationBarVisibility |= Integer.MIN_VALUE;
        oplusBaseLayoutParams.navigationBarColor = paramInt;
      } else {
        oplusBaseLayoutParams.navigationBarVisibility &= Integer.MAX_VALUE;
        oplusBaseLayoutParams.navigationBarColor = paramInt;
      }  
  }
  
  public static boolean updateDarkNavigationBar(WindowManager.LayoutParams paramLayoutParams) {
    OplusBaseLayoutParams oplusBaseLayoutParams = typeCast(paramLayoutParams);
    if (oplusBaseLayoutParams != null) {
      if (!oplusBaseLayoutParams.mOplusLayoutParams.hasNavigationBar())
        return false; 
      if (!oplusBaseLayoutParams.mOplusLayoutParams.isUpdateNavigationBar())
        return false; 
      if (isColorLight(oplusBaseLayoutParams.mOplusLayoutParams.getNavigationBarColor()))
        return false; 
    } 
    return true;
  }
  
  public static void setHasStatusBar(WindowManager.LayoutParams paramLayoutParams, boolean paramBoolean) {
    OplusBaseLayoutParams oplusBaseLayoutParams = typeCast(paramLayoutParams);
    if (oplusBaseLayoutParams != null)
      oplusBaseLayoutParams.mOplusLayoutParams.setHasStatusBar(paramBoolean); 
  }
  
  public static void setHasNavigationBar(WindowManager.LayoutParams paramLayoutParams, boolean paramBoolean) {
    OplusBaseLayoutParams oplusBaseLayoutParams = typeCast(paramLayoutParams);
    if (oplusBaseLayoutParams != null)
      oplusBaseLayoutParams.mOplusLayoutParams.setHasNavigationBar(paramBoolean); 
  }
  
  public static void setCustomSystemBar(WindowManager.LayoutParams paramLayoutParams, boolean paramBoolean) {
    OplusBaseLayoutParams oplusBaseLayoutParams = typeCast(paramLayoutParams);
    if (oplusBaseLayoutParams != null)
      oplusBaseLayoutParams.mOplusLayoutParams.setCustomSystemBar(paramBoolean); 
  }
  
  public static void setSystemAppWindow(WindowManager.LayoutParams paramLayoutParams, boolean paramBoolean) {
    OplusBaseLayoutParams oplusBaseLayoutParams = typeCast(paramLayoutParams);
    if (oplusBaseLayoutParams != null)
      oplusBaseLayoutParams.mOplusLayoutParams.setSystemAppWindow(paramBoolean); 
  }
  
  public static void setFullScreenWindow(WindowManager.LayoutParams paramLayoutParams, boolean paramBoolean) {
    OplusBaseLayoutParams oplusBaseLayoutParams = typeCast(paramLayoutParams);
    if (oplusBaseLayoutParams != null)
      oplusBaseLayoutParams.mOplusLayoutParams.setFullScreenWindow(paramBoolean); 
  }
  
  public static boolean skipSystemUiVisibility(WindowManager.LayoutParams paramLayoutParams) {
    OplusBaseLayoutParams oplusBaseLayoutParams = typeCast(paramLayoutParams);
    if (oplusBaseLayoutParams != null)
      return oplusBaseLayoutParams.mOplusLayoutParams.getSkipSystemUiVisibility(); 
    return false;
  }
  
  public static void setSkipSystemUiVisibility(WindowManager.LayoutParams paramLayoutParams, boolean paramBoolean) {
    OplusBaseLayoutParams oplusBaseLayoutParams = typeCast(paramLayoutParams);
    if (oplusBaseLayoutParams != null)
      oplusBaseLayoutParams.mOplusLayoutParams.setSkipSystemUiVisibility(paramBoolean); 
  }
  
  public static boolean isUseLastStatusBarTint(WindowManager.LayoutParams paramLayoutParams) {
    OplusBaseLayoutParams oplusBaseLayoutParams = typeCast(paramLayoutParams);
    if (oplusBaseLayoutParams != null)
      return oplusBaseLayoutParams.mOplusLayoutParams.isUseLastStatusBarTint(); 
    return false;
  }
  
  public static void setUseLastStatusBarTint(WindowManager.LayoutParams paramLayoutParams, boolean paramBoolean) {
    OplusBaseLayoutParams oplusBaseLayoutParams = typeCast(paramLayoutParams);
    if (oplusBaseLayoutParams != null)
      oplusBaseLayoutParams.mOplusLayoutParams.setUseLastStatusBarTint(paramBoolean); 
  }
  
  public static boolean updateSpecialSystemBar(WindowManager.LayoutParams paramLayoutParams) {
    if (isUseLastStatusBarTint(paramLayoutParams))
      return true; 
    if (updateDarkNavigationBar(paramLayoutParams))
      return true; 
    return false;
  }
  
  @Deprecated
  public static boolean isInMultiWindowMode() {
    boolean bool;
    int i = -1;
    try {
      int j = WindowManagerGlobal.getWindowManagerService().getDockedStackSide();
    } catch (RemoteException remoteException) {}
    if (-1 != i) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isColorLight(int paramInt) {
    if (paramInt == 0)
      return false; 
    int i = (int)(((0xFF0000 & paramInt) >>> 16) * 0.299F + ((0xFF00 & paramInt) >>> 8) * 0.587F + (paramInt & 0xFF) * 0.114F);
    if (i > 192 && (0xFF000000 & paramInt) >>> 24 > 156)
      return true; 
    return false;
  }
  
  class LayoutParams extends IOplusWindowManagerConstans.BaseLayoutParams {}
  
  private static OplusBaseLayoutParams typeCast(WindowManager.LayoutParams paramLayoutParams) {
    return OplusTypeCastingHelper.<OplusBaseLayoutParams>typeCasting(OplusBaseLayoutParams.class, paramLayoutParams);
  }
}
