package com.oplus.view.inputmethod;

import android.content.Context;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;

public final class OplusInputMethodManager {
  private static final String DESCRIPTOR = "com.android.internal.view.IInputMethodManager";
  
  public static final int GET_DEFULT_INPUTMETHOD = 10004;
  
  public static final int HIDE_CURRENT_INPUTMETHOD = 10002;
  
  public static final int OPLUS_CALL_TRANSACTION_INDEX = 10000;
  
  public static final int OPLUS_FIRST_CALL_TRANSACTION = 10001;
  
  public static final int RESET_DEFULT_INPUTMETHOD = 10005;
  
  public static final int SET_DEFULT_INPUTMETHOD = 10003;
  
  private static final String TAG = "OplusInputMethodManager";
  
  private IBinder mRemote = null;
  
  public OplusInputMethodManager() {
    this.mRemote = ServiceManager.getService("input_method");
  }
  
  public void hideCurrentInputMethod() {
    Parcel parcel = Parcel.obtain();
    try {
      parcel.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
      this.mRemote.transact(10002, parcel, null, 1);
      parcel.recycle();
    } catch (RemoteException remoteException) {
      Log.e("OplusInputMethodManager", "hideCurrentInputMethod failed..");
      parcel.recycle();
    } finally {
      Exception exception;
    } 
  }
  
  public static void hideSoftInput(Context paramContext) {
    Parcel parcel = Parcel.obtain();
    try {
      IBinder iBinder = ServiceManager.getService("input_method");
      parcel.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
      iBinder.transact(10002, parcel, null, 1);
    } catch (RemoteException remoteException) {
      Log.e("OplusInputMethodManager", "hideSoftInput failed..");
    } finally {
      Exception exception;
    } 
    parcel.recycle();
  }
  
  public void setDefaultInputMethod(String paramString) {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
      parcel1.writeString(paramString);
      this.mRemote.transact(10003, parcel1, parcel2, 1);
      parcel2.readException();
    } catch (RemoteException remoteException) {
    
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public String getDefaultInputMethod() {
    null = "";
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      String str;
      parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
      this.mRemote.transact(10004, parcel1, parcel2, 1);
    } catch (RemoteException remoteException) {
    
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
    parcel1.recycle();
    parcel2.recycle();
    return (String)SYNTHETIC_LOCAL_VARIABLE_1;
  }
  
  public void clearDefaultInputMethod() {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
      this.mRemote.transact(10005, parcel1, parcel2, 1);
      parcel2.readException();
    } catch (RemoteException remoteException) {
    
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
}
