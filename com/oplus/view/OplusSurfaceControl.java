package com.oplus.view;

import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.graphics.Rect;
import android.util.Log;
import android.view.OplusWindowManager;
import android.view.SurfaceControl;
import com.oplus.util.OplusLog;

public final class OplusSurfaceControl {
  private static final String TAG = "ColorSurfaceControl";
  
  @Deprecated
  public static Bitmap screenshot(int paramInt1, int paramInt2) {
    Rect rect = new Rect(0, 0, paramInt1, paramInt2);
    return screenshot(rect, paramInt1, paramInt2, 0);
  }
  
  @Deprecated
  public static Bitmap screenshot(int paramInt1, int paramInt2, int paramInt3) {
    Rect rect = new Rect(0, 0, paramInt1, paramInt2);
    return SurfaceControl.screenshot(rect, paramInt1, paramInt2, 0);
  }
  
  @Deprecated
  public static Bitmap screenshot(Rect paramRect, int paramInt1, int paramInt2, int paramInt3) {
    return screenshot(paramRect, paramInt1, paramInt2, paramInt3, 0);
  }
  
  public static Bitmap screenshot(Rect paramRect, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    StringBuilder stringBuilder;
    if (paramInt1 == 0 || paramInt2 == 0) {
      StringBuilder stringBuilder1 = null;
      SurfaceControl.ScreenshotGraphicBuffer screenshotGraphicBuffer = null;
      try {
        OplusWindowManager oplusWindowManager = new OplusWindowManager();
        this();
        SurfaceControl surfaceControl = oplusWindowManager.getLongshotWindowByTypeForR(-1);
        StringBuilder stringBuilder2 = new StringBuilder();
        this();
        stringBuilder2.append("screenshot surfaceControl : ");
        stringBuilder2.append(surfaceControl);
        OplusLog.d("ColorSurfaceControl", stringBuilder2.toString());
        SurfaceControl.ScreenshotGraphicBuffer screenshotGraphicBuffer1 = SurfaceControl.captureLayers(surfaceControl, paramRect, 1.0F);
        if (screenshotGraphicBuffer1 != null) {
          Bitmap bitmap = Bitmap.wrapHardwareBuffer(screenshotGraphicBuffer1.getGraphicBuffer(), ColorSpace.get(ColorSpace.Named.SRGB));
        } else {
          OplusLog.e("ColorSurfaceControl", "screenshot buffer error");
          screenshotGraphicBuffer1 = screenshotGraphicBuffer;
        } 
      } catch (Exception exception) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("[ERROR] captureLayers : ");
        stringBuilder.append(Log.getStackTraceString(exception));
        OplusLog.e("ColorSurfaceControl", stringBuilder.toString());
        stringBuilder = stringBuilder1;
      } 
      return (Bitmap)stringBuilder;
    } 
    return SurfaceControl.screenshot((Rect)stringBuilder, paramInt1, paramInt2, paramInt4);
  }
}
