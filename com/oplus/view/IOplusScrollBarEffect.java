package com.oplus.view;

import android.graphics.Rect;
import android.view.MotionEvent;

public interface IOplusScrollBarEffect {
  void getDrawRect(Rect paramRect);
  
  int getThumbLength(int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  boolean isTouchPressed();
  
  void onOverScrolled(int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  void onTouchEvent(MotionEvent paramMotionEvent);
  
  public static interface ViewCallback {
    boolean awakenScrollBars();
    
    boolean isLayoutRtl();
  }
}
