package com.oplus.view;

import android.os.Parcel;
import android.os.Parcelable;

public class OplusLayoutParams implements Parcelable {
  public static final Parcelable.Creator<OplusLayoutParams> CREATOR = new Parcelable.Creator<OplusLayoutParams>() {
      public OplusLayoutParams createFromParcel(Parcel param1Parcel) {
        return new OplusLayoutParams(param1Parcel);
      }
      
      public OplusLayoutParams[] newArray(int param1Int) {
        return new OplusLayoutParams[param1Int];
      }
    };
  
  private int mSystemBarFlags = 0;
  
  private int mNavigationBarColor = 0;
  
  private static final int FLAG_CUSTOM_SYSTEM_BAR = 8;
  
  private static final int FLAG_FULL_SCREEN_WINDOW = 32;
  
  private static final int FLAG_HAS_NAVIGATION_BAR = 2;
  
  private static final int FLAG_HAS_STATUS_BAR = 1;
  
  private static final int FLAG_SKIP_SYSTEM_UI_VISIBILITY = 64;
  
  private static final int FLAG_SYSTEM_APP_WINDOW = 16;
  
  private static final int FLAG_UPDATE_NAVIGATION_BAR = 4;
  
  private static final int FLAG_USE_LAST_STATUS_BAR_TINT = 128;
  
  public OplusLayoutParams(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(" CLP[");
    int i = this.mSystemBarFlags;
    if (i != 0)
      stringBuilder.append(formatHex(i, "sysBarFlg")); 
    i = this.mNavigationBarColor;
    if (i != 0)
      stringBuilder.append(formatHex(i, "navColor")); 
    stringBuilder.append(" ]");
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mSystemBarFlags != ((OplusLayoutParams)paramObject).mSystemBarFlags)
      return false; 
    if (this.mNavigationBarColor != ((OplusLayoutParams)paramObject).mNavigationBarColor)
      return false; 
    return true;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mSystemBarFlags);
    paramParcel.writeInt(this.mNavigationBarColor);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mSystemBarFlags = paramParcel.readInt();
    this.mNavigationBarColor = paramParcel.readInt();
  }
  
  public void set(OplusLayoutParams paramOplusLayoutParams) {
    this.mSystemBarFlags = paramOplusLayoutParams.mSystemBarFlags;
    this.mNavigationBarColor = paramOplusLayoutParams.mNavigationBarColor;
  }
  
  public void setHasStatusBar(boolean paramBoolean) {
    setFlag(paramBoolean, 1);
  }
  
  public boolean hasStatusBar() {
    return hasFlag(1);
  }
  
  public void setHasNavigationBar(boolean paramBoolean) {
    setFlag(paramBoolean, 2);
  }
  
  public boolean hasNavigationBar() {
    return hasFlag(2);
  }
  
  public void setUpdateNavigationBar(boolean paramBoolean) {
    setFlag(paramBoolean, 4);
  }
  
  public boolean isUpdateNavigationBar() {
    return hasFlag(4);
  }
  
  public void setCustomSystemBar(boolean paramBoolean) {
    setFlag(paramBoolean, 8);
  }
  
  public boolean isCustomSystemBar() {
    return hasFlag(8);
  }
  
  public void setSystemAppWindow(boolean paramBoolean) {
    setFlag(paramBoolean, 16);
  }
  
  public boolean isSystemAppWindow() {
    return hasFlag(16);
  }
  
  public void setFullScreenWindow(boolean paramBoolean) {
    setFlag(paramBoolean, 32);
  }
  
  public boolean isFullScreenWindow() {
    return hasFlag(32);
  }
  
  public void setSkipSystemUiVisibility(boolean paramBoolean) {
    setFlag(paramBoolean, 64);
  }
  
  public boolean getSkipSystemUiVisibility() {
    return hasFlag(64);
  }
  
  public void setUseLastStatusBarTint(boolean paramBoolean) {
    setFlag(paramBoolean, 128);
  }
  
  public boolean isUseLastStatusBarTint() {
    return hasFlag(128);
  }
  
  public void setNavigationBarColor(int paramInt) {
    this.mNavigationBarColor = paramInt;
  }
  
  public int getNavigationBarColor() {
    return this.mNavigationBarColor;
  }
  
  private String formatHex(int paramInt, String paramString) {
    return String.format(" %s=#%08x", new Object[] { paramString, Integer.valueOf(paramInt) });
  }
  
  private void setFlag(boolean paramBoolean, int paramInt) {
    if (paramBoolean) {
      this.mSystemBarFlags |= paramInt;
    } else {
      this.mSystemBarFlags &= paramInt ^ 0xFFFFFFFF;
    } 
  }
  
  private boolean hasFlag(int paramInt) {
    boolean bool;
    if ((this.mSystemBarFlags & paramInt) == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public OplusLayoutParams() {}
}
