package com.oplus.view;

import android.text.TextUtils;
import android.view.WindowManager;

public final class OplusWindowUtils {
  private static final String[] DIRECT_APPS;
  
  public static final String PACKAGE_ASSISTANTSCREEN = "com.coloros.assistantscreen";
  
  public static final String PACKAGE_DIRECTSERVICE = "com.coloros.colordirectservice";
  
  public static final String PACKAGE_DIRECTUI = "com.coloros.directui";
  
  public static final String PACKAGE_EXSERVICEUI = "com.coloros.exserviceui";
  
  public static final String PACKAGE_FLOATASSISTANT = "com.coloros.floatassistant";
  
  public static final String PACKAGE_GALLERY = "com.coloros.gallery3d";
  
  public static final String PACKAGE_SCREENSHOT = "com.coloros.screenshot";
  
  public static final String PACKAGE_SYSTEMUI = "com.android.systemui";
  
  public static final String PACKAGE_TALKBACK = "com.google.android.marvin.talkback";
  
  private static final String[] SYSTEMUI_BARS = new String[] { "TickerPanel" };
  
  private static final String TITLE_DIALOG_VOLUME = "ColorVolumeDialogImpl";
  
  private static final String TITLE_EDGE_FLOATBAR = "ColorOSEdgeFloatBar";
  
  private static final String TITLE_EDGE_PANEL = "ColorOSEdgePanel";
  
  private static final String TITLE_NOTIFICATIONSHADE = "NotificationShade";
  
  private static final String TITLE_SHORTCUTS_PANEL = "ShortcutsPanel";
  
  static {
    DIRECT_APPS = new String[] { "com.coloros.directui", "com.coloros.colordirectservice" };
  }
  
  public static boolean isInputMethodWindow(int paramInt, CharSequence paramCharSequence) {
    if (paramInt != 2011)
      return false; 
    return "InputMethod".equals(paramCharSequence);
  }
  
  public static boolean isStatusBar(int paramInt) {
    boolean bool;
    if (2000 == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isNavigationBar(int paramInt) {
    boolean bool;
    if (2019 == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isEdgeFloatBar(int paramInt) {
    boolean bool;
    if (2314 == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isSystemHeadsUp(WindowManager.LayoutParams paramLayoutParams) {
    boolean bool;
    if ((paramLayoutParams.flags & 0x20) == 32) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isSystemFloatBar(int paramInt, CharSequence paramCharSequence) {
    if (isEdgeFloatBar(paramInt))
      return true; 
    return false;
  }
  
  public static boolean isSystemUiBar(int paramInt, CharSequence paramCharSequence) {
    if (isNavigationBar(paramInt))
      return true; 
    if (isStatusBar(paramInt))
      return true; 
    if (!TextUtils.isEmpty(paramCharSequence)) {
      String[] arrayOfString;
      int i;
      for (arrayOfString = SYSTEMUI_BARS, i = arrayOfString.length, paramInt = 0; paramInt < i; ) {
        String str = arrayOfString[paramInt];
        if (str.equals(paramCharSequence))
          return true; 
        paramInt++;
      } 
    } 
    return false;
  }
  
  public static boolean isShortcutsPanel(CharSequence paramCharSequence) {
    return "ShortcutsPanel".equals(paramCharSequence);
  }
  
  public static boolean isVolumeDialog(CharSequence paramCharSequence) {
    return "ColorVolumeDialogImpl".equals(paramCharSequence);
  }
  
  public static boolean isExServiceUiApp(String paramString) {
    return isPackage(paramString, "com.coloros.exserviceui");
  }
  
  public static boolean isScreenshotApp(String paramString) {
    return isPackage(paramString, "com.coloros.screenshot");
  }
  
  public static boolean isSystemUiApp(String paramString) {
    return isPackage(paramString, "com.android.systemui");
  }
  
  public static boolean isFloatAssistant(String paramString) {
    return isPackage(paramString, "com.coloros.floatassistant");
  }
  
  public static boolean isGallery(String paramString) {
    return isPackage(paramString, "com.coloros.gallery3d");
  }
  
  public static boolean isDirectApp(String paramString) {
    for (String str : DIRECT_APPS) {
      if (str.equals(paramString))
        return true; 
    } 
    return false;
  }
  
  public static boolean isEdgeFloatBarTitle(CharSequence paramCharSequence) {
    return "ColorOSEdgeFloatBar".equals(paramCharSequence);
  }
  
  public static boolean isEdgePanelTitle(CharSequence paramCharSequence) {
    return "ColorOSEdgePanel".equals(paramCharSequence);
  }
  
  public static boolean isExpand(WindowManager.LayoutParams paramLayoutParams) {
    if (paramLayoutParams.width != -1)
      return false; 
    if (paramLayoutParams.height != -1)
      return false; 
    return true;
  }
  
  public static boolean isSystemWindow(WindowManager.LayoutParams paramLayoutParams) {
    boolean bool;
    if (paramLayoutParams.type > 2000) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isTalkBack(String paramString) {
    return isPackage(paramString, "com.google.android.marvin.talkback");
  }
  
  public static boolean isAssistantScreen(String paramString) {
    return isPackage(paramString, "com.coloros.assistantscreen");
  }
  
  public static boolean isNotificationShade(CharSequence paramCharSequence) {
    return "NotificationShade".equals(paramCharSequence);
  }
  
  private static boolean isPackage(String paramString1, String paramString2) {
    return paramString2.equals(paramString1);
  }
}
