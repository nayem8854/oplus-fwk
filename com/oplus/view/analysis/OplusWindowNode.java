package com.oplus.view.analysis;

import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.view.OplusLongshotViewContent;
import android.view.OplusLongshotViewUtils;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public final class OplusWindowNode implements Parcelable {
  public static final Parcelable.Creator<OplusWindowNode> CREATOR = new Parcelable.Creator<OplusWindowNode>() {
      public OplusWindowNode createFromParcel(Parcel param1Parcel) {
        return new OplusWindowNode(param1Parcel);
      }
      
      public OplusWindowNode[] newArray(int param1Int) {
        return new OplusWindowNode[param1Int];
      }
    };
  
  private final Rect mTempRect = new Rect();
  
  private final Rect mCoverRect = new Rect();
  
  private final Rect mDecorRect = new Rect();
  
  private String mPackageName = null;
  
  private String mClassName = null;
  
  private long mTimeSpend = 0L;
  
  private int mSurfaceLayer = 0;
  
  private boolean mIsStatusBar = false;
  
  private boolean mIsNavigationBar = false;
  
  public OplusWindowNode(View paramView, boolean paramBoolean1, boolean paramBoolean2) {
    long l = SystemClock.uptimeMillis();
    paramView.getBoundsOnScreen(this.mDecorRect, true);
    if (paramView instanceof ViewGroup) {
      ArrayList arrayList = new ArrayList();
      OplusLongshotViewUtils oplusLongshotViewUtils = new OplusLongshotViewUtils(paramView.getContext());
      oplusLongshotViewUtils.findCoverRect(1, (ViewGroup)paramView, null, arrayList, null, null, null, true);
      for (OplusLongshotViewContent oplusLongshotViewContent : arrayList) {
        View view = oplusLongshotViewContent.getView();
        view.getBoundsOnScreen(this.mTempRect, true);
        this.mCoverRect.union(this.mTempRect);
      } 
    } 
    if (this.mCoverRect.isEmpty())
      this.mCoverRect.set(this.mDecorRect); 
    this.mPackageName = paramView.getContext().getPackageName();
    this.mClassName = paramView.getClass().getName();
    this.mTimeSpend = SystemClock.uptimeMillis() - l;
    this.mIsStatusBar = paramBoolean1;
    this.mIsNavigationBar = paramBoolean2;
  }
  
  public OplusWindowNode(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Window[");
    if (this.mIsStatusBar) {
      stringBuilder.append("StatusBar][");
    } else if (this.mIsNavigationBar) {
      stringBuilder.append("NavigationBar][");
    } 
    if (this.mPackageName != null) {
      stringBuilder.append("package=");
      stringBuilder.append(this.mPackageName.toString());
      stringBuilder.append(":");
    } 
    if (this.mClassName != null) {
      stringBuilder.append("class=");
      stringBuilder.append(this.mClassName.toString());
      stringBuilder.append(":");
    } 
    stringBuilder.append("decor=");
    stringBuilder.append(this.mDecorRect);
    stringBuilder.append("cover=");
    stringBuilder.append(this.mCoverRect);
    stringBuilder.append(":spend=");
    stringBuilder.append(this.mTimeSpend);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.mDecorRect.writeToParcel(paramParcel, paramInt);
    this.mCoverRect.writeToParcel(paramParcel, paramInt);
    writeString(paramParcel, this.mPackageName);
    writeString(paramParcel, this.mClassName);
    paramParcel.writeLong(this.mTimeSpend);
    paramParcel.writeInt(this.mSurfaceLayer);
    paramParcel.writeInt(this.mIsStatusBar);
    paramParcel.writeInt(this.mIsNavigationBar);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mDecorRect.readFromParcel(paramParcel);
    this.mCoverRect.readFromParcel(paramParcel);
    this.mPackageName = readString(paramParcel);
    this.mClassName = readString(paramParcel);
    this.mTimeSpend = paramParcel.readLong();
    this.mSurfaceLayer = paramParcel.readInt();
    int i = paramParcel.readInt();
    boolean bool1 = false;
    if (1 == i) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mIsStatusBar = bool2;
    boolean bool2 = bool1;
    if (1 == paramParcel.readInt())
      bool2 = true; 
    this.mIsNavigationBar = bool2;
  }
  
  public Rect getDecorRect() {
    return this.mDecorRect;
  }
  
  public Rect getCoverRect() {
    return this.mCoverRect;
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
  
  public String getClassName() {
    return this.mClassName;
  }
  
  public int getSurfaceLayer() {
    return this.mSurfaceLayer;
  }
  
  public void setSurfaceLayer(int paramInt) {
    this.mSurfaceLayer = paramInt;
  }
  
  public boolean isStatusBar() {
    return this.mIsStatusBar;
  }
  
  public void setStatusBar(boolean paramBoolean) {
    this.mIsStatusBar = paramBoolean;
  }
  
  public boolean isNavigationBar() {
    return this.mIsNavigationBar;
  }
  
  public void setNavigationBar(boolean paramBoolean) {
    this.mIsNavigationBar = paramBoolean;
  }
  
  private void writeString(Parcel paramParcel, String paramString) {
    if (paramString != null) {
      paramParcel.writeInt(1);
      paramParcel.writeString(paramString);
    } else {
      paramParcel.writeInt(0);
    } 
  }
  
  private String readString(Parcel paramParcel) {
    if (1 == paramParcel.readInt())
      return paramParcel.readString(); 
    return null;
  }
}
