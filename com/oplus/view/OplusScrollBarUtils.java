package com.oplus.view;

import com.android.internal.widget.ScrollBarUtils;

public class OplusScrollBarUtils {
  public static int getThumbLength(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    return ScrollBarUtils.getThumbLength(paramInt1, paramInt2, paramInt3, paramInt4);
  }
}
