package com.oplus.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.view.MotionEvent;

public class OplusScrollBarEffect implements IOplusScrollBarEffect {
  public static final IOplusScrollBarEffect NO_EFFECT = new NoEffect();
  
  private int mOverScrollLengthY = 0;
  
  private boolean mIsTouchPressed = false;
  
  private final int mMinHeightNormal;
  
  private final int mMinHeightOverScroll;
  
  private final int mPadding;
  
  private final IOplusScrollBarEffect.ViewCallback mViewCallback;
  
  public OplusScrollBarEffect(Context paramContext, IOplusScrollBarEffect.ViewCallback paramViewCallback) {
    this.mViewCallback = paramViewCallback;
    Resources resources = paramContext.getResources();
    this.mPadding = resources.getDimensionPixelSize(201654523);
    this.mMinHeightOverScroll = resources.getDimensionPixelSize(201654522);
    this.mMinHeightNormal = resources.getDimensionPixelSize(201654521);
  }
  
  public OplusScrollBarEffect(Resources paramResources, IOplusScrollBarEffect.ViewCallback paramViewCallback) {
    this.mViewCallback = paramViewCallback;
    this.mPadding = paramResources.getDimensionPixelSize(201654523);
    this.mMinHeightOverScroll = paramResources.getDimensionPixelSize(201654522);
    this.mMinHeightNormal = paramResources.getDimensionPixelSize(201654521);
  }
  
  public void getDrawRect(Rect paramRect) {
    paramRect.inset(0, this.mPadding);
    paramRect.offset(getOffsetX(), 0);
  }
  
  public int getThumbLength(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (this.mOverScrollLengthY != 0) {
      paramInt2 = this.mMinHeightOverScroll;
    } else {
      paramInt2 = this.mMinHeightNormal;
    } 
    paramInt1 = Math.round(paramInt1 * paramInt3 / paramInt4);
    paramInt3 = Math.max(paramInt1, this.mMinHeightNormal) - this.mOverScrollLengthY - this.mPadding * 2;
    paramInt1 = paramInt3;
    if (paramInt3 < paramInt2)
      paramInt1 = paramInt2; 
    return paramInt1;
  }
  
  public void onTouchEvent(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getActionMasked();
    if (i != 0) {
      if (i == 1 || i == 3) {
        this.mIsTouchPressed = false;
        this.mViewCallback.awakenScrollBars();
      } 
    } else {
      this.mIsTouchPressed = true;
    } 
  }
  
  public void onOverScrolled(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mOverScrollLengthY = getOverScrollLength(paramInt2, paramInt4);
  }
  
  public boolean isTouchPressed() {
    return this.mIsTouchPressed;
  }
  
  private int getOverScrollLength(int paramInt1, int paramInt2) {
    if (paramInt1 < 0)
      return -paramInt1; 
    if (paramInt1 > paramInt2)
      return paramInt1 - paramInt2; 
    return 0;
  }
  
  private int getOffsetX() {
    int i;
    if (this.mViewCallback.isLayoutRtl()) {
      i = this.mPadding;
    } else {
      i = -this.mPadding;
    } 
    return i;
  }
  
  private static class NoEffect implements IOplusScrollBarEffect {
    private NoEffect() {}
    
    public void getDrawRect(Rect param1Rect) {}
    
    public int getThumbLength(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      return OplusScrollBarUtils.getThumbLength(param1Int1, param1Int2, param1Int3, param1Int4);
    }
    
    public void onTouchEvent(MotionEvent param1MotionEvent) {}
    
    public void onOverScrolled(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {}
    
    public boolean isTouchPressed() {
      return false;
    }
  }
}
