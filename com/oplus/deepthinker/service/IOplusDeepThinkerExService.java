package com.oplus.deepthinker.service;

import android.app.job.JobInfo;
import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public interface IOplusDeepThinkerExService extends IOplusCommonFeature {
  public static final IOplusDeepThinkerExService DEFAULT = (IOplusDeepThinkerExService)new Object();
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusDeepThinkerExService;
  }
  
  default IOplusCommonFeature getDefault() {
    return DEFAULT;
  }
  
  default boolean onHandleDump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    return false;
  }
  
  default void onWakeFullnessChanged(int paramInt) {}
  
  default void onScheduleJob(JobInfo paramJobInfo) {}
  
  default void onReleaseWakelock(String paramString1, String paramString2, long paramLong) {}
  
  default void onDeliverAlarm(Object paramObject1, Object paramObject2, long paramLong) {}
}
