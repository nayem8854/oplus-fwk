package com.oplus.deepthinker;

public interface ServiceStateObserver {
  void onServiceDied();
}
