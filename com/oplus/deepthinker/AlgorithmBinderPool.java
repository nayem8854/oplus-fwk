package com.oplus.deepthinker;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Process;
import android.os.RemoteException;
import android.os.UserHandle;
import com.oplus.deepthinker.sdk.aidl.platform.IAlgorithmBinderPool;
import com.oplus.deepthinker.sdk.aidl.platform.IAlgorithmPlatform;
import com.oplus.deepthinker.sdk.aidl.proton.appactionpredict.IAppPredictAidlInterface;
import com.oplus.deepthinker.sdk.aidl.proton.appsort.IAppSort;
import com.oplus.deepthinker.sdk.aidl.proton.apptype.IAppType;
import com.oplus.deepthinker.sdk.aidl.proton.deepsleep.IDeepSleepPredict;
import com.oplus.deepthinker.sdk.aidl.proton.smartgps.ISmartGpsAidlInterface;
import com.oplus.deepthinker.sdk.common.utils.SDKLog;
import com.oplus.eventhub.sdk.aidl.IEventHandleService;
import java.lang.reflect.Method;
import java.util.Random;
import java.util.WeakHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AlgorithmBinderPool {
  private static final Object CONN_LOCK = new Object();
  
  private static final Object OBSERVER_LOCK = new Object();
  
  static {
    LOCK = new Object();
  }
  
  private volatile int mState = 0;
  
  private Executor mExecutor = Executors.newFixedThreadPool(5);
  
  private WeakHashMap<ServiceStateObserver, Object> mStateObservers = new WeakHashMap<>();
  
  private static final int LAZY_RETRY_BASE = 100;
  
  private static final Object LOCK;
  
  private static final int RECONNECT_SERVICE_TIMEOUT_MILLS = 2000;
  
  private static final String SERVER_ACTION = "com.coloros.deepthinker.services.AIAlgorithmService";
  
  private static final String SERVER_PACKAGE = "com.coloros.deepthinker";
  
  private static final int STATE_CONNECTED = 2;
  
  private static final int STATE_CONNECTING = 1;
  
  private static final int STATE_DISCONNECTED = 3;
  
  private static final int STATE_INIT = 0;
  
  private static final String TAG = "AlgorithmBinderPool";
  
  private static volatile AlgorithmBinderPool sInstance;
  
  private IAppPredictAidlInterface mAppPredictBinder;
  
  private IAppSort mAppSortBinder;
  
  private IAppType mAppTypeBinder;
  
  private IAlgorithmBinderPool mBinderPool;
  
  private ServiceConnection mBinderPoolConnection;
  
  private final Context mContext;
  
  private CountDownLatch mCountDownLatch;
  
  private int mCurrentRetryIndex;
  
  private IBinder.DeathRecipient mDeathRecipient;
  
  private IDeepSleepPredict mDeepSleepBinder;
  
  private IEventHandleService mEventHandleBinder;
  
  private Handler mHandler;
  
  private boolean mIsSystemUser;
  
  private boolean mLazyRetry;
  
  private UserHandle mMyUser;
  
  private IAlgorithmPlatform mPlatformBinder;
  
  private ISmartGpsAidlInterface mSmartGpsBinder;
  
  private void invalid() {
    if (this.mState == 3) {
      this.mPlatformBinder = null;
      this.mEventHandleBinder = null;
      this.mAppPredictBinder = null;
      this.mDeepSleepBinder = null;
      this.mAppTypeBinder = null;
      this.mAppSortBinder = null;
      this.mSmartGpsBinder = null;
    } 
  }
  
  public static AlgorithmBinderPool getInstance(Context paramContext) {
    // Byte code:
    //   0: getstatic com/oplus/deepthinker/AlgorithmBinderPool.sInstance : Lcom/oplus/deepthinker/AlgorithmBinderPool;
    //   3: ifnonnull -> 40
    //   6: ldc com/oplus/deepthinker/AlgorithmBinderPool
    //   8: monitorenter
    //   9: getstatic com/oplus/deepthinker/AlgorithmBinderPool.sInstance : Lcom/oplus/deepthinker/AlgorithmBinderPool;
    //   12: ifnonnull -> 28
    //   15: new com/oplus/deepthinker/AlgorithmBinderPool
    //   18: astore_1
    //   19: aload_1
    //   20: aload_0
    //   21: invokespecial <init> : (Landroid/content/Context;)V
    //   24: aload_1
    //   25: putstatic com/oplus/deepthinker/AlgorithmBinderPool.sInstance : Lcom/oplus/deepthinker/AlgorithmBinderPool;
    //   28: ldc com/oplus/deepthinker/AlgorithmBinderPool
    //   30: monitorexit
    //   31: goto -> 40
    //   34: astore_0
    //   35: ldc com/oplus/deepthinker/AlgorithmBinderPool
    //   37: monitorexit
    //   38: aload_0
    //   39: athrow
    //   40: getstatic com/oplus/deepthinker/AlgorithmBinderPool.sInstance : Lcom/oplus/deepthinker/AlgorithmBinderPool;
    //   43: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #109	-> 0
    //   #110	-> 6
    //   #111	-> 9
    //   #112	-> 15
    //   #114	-> 28
    //   #116	-> 40
    // Exception table:
    //   from	to	target	type
    //   9	15	34	finally
    //   15	28	34	finally
    //   28	31	34	finally
    //   35	38	34	finally
  }
  
  private void initIsSystemUser() {
    if (Process.myUid() == 1000) {
      this.mIsSystemUser = true;
    } else {
      try {
        PackageInfo packageInfo = this.mContext.getPackageManager().getPackageInfo(this.mContext.getPackageName(), 0);
        this.mIsSystemUser = "android.uid.system".equals(packageInfo.sharedUserId);
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("initIsSystemUser ");
        stringBuilder.append(exception);
        SDKLog.w("AlgorithmBinderPool", stringBuilder.toString());
      } 
    } 
  }
  
  private AlgorithmBinderPool(Context paramContext) {
    this.mDeathRecipient = new IBinder.DeathRecipient() {
        final AlgorithmBinderPool this$0;
        
        public void binderDied() {
          SDKLog.d("AlgorithmBinderPool", "Invoke binder died ");
          if (AlgorithmBinderPool.this.checkIfLegalDisconnectState()) {
            if (AlgorithmBinderPool.this.mBinderPool != null) {
              AlgorithmBinderPool.this.mBinderPool.asBinder().unlinkToDeath(AlgorithmBinderPool.this.mDeathRecipient, 0);
              AlgorithmBinderPool.access$102(AlgorithmBinderPool.this, null);
            } 
            synchronized (AlgorithmBinderPool.CONN_LOCK) {
              AlgorithmBinderPool.this.tryConnectBinder();
              AlgorithmBinderPool.this.onServiceDied();
            } 
          } else {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("binderDied: current illeagalState ");
            AlgorithmBinderPool algorithmBinderPool = AlgorithmBinderPool.this;
            stringBuilder.append(algorithmBinderPool.logState(algorithmBinderPool.getState()));
            SDKLog.w("AlgorithmBinderPool", stringBuilder.toString());
          } 
        }
      };
    this.mBinderPoolConnection = new ServiceConnection() {
        final AlgorithmBinderPool this$0;
        
        public void onServiceConnected(ComponentName param1ComponentName, IBinder param1IBinder) {
          SDKLog.d("AlgorithmBinderPool", "on Service Connected");
          AlgorithmBinderPool.access$102(AlgorithmBinderPool.this, IAlgorithmBinderPool.Stub.asInterface(param1IBinder));
          AlgorithmBinderPool.this.setState(2);
          try {
            AlgorithmBinderPool.this.mBinderPool.asBinder().linkToDeath(AlgorithmBinderPool.this.mDeathRecipient, 0);
          } catch (RemoteException remoteException) {
            SDKLog.e("AlgorithmBinderPool", "on Service linkToDeath error: ", (Throwable)remoteException);
          } 
          if (AlgorithmBinderPool.this.mCountDownLatch != null)
            AlgorithmBinderPool.this.mCountDownLatch.countDown(); 
        }
        
        public void onServiceDisconnected(ComponentName param1ComponentName) {
          SDKLog.d("AlgorithmBinderPool", "on Service Disconnected");
          if (AlgorithmBinderPool.this.checkIfLegalDisconnectState()) {
            AlgorithmBinderPool.access$102(AlgorithmBinderPool.this, null);
            AlgorithmBinderPool.this.onServiceDied();
          } else {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("onServiceDisconnected: current illeagalState ");
            AlgorithmBinderPool algorithmBinderPool = AlgorithmBinderPool.this;
            stringBuilder.append(algorithmBinderPool.logState(algorithmBinderPool.getState()));
            SDKLog.w("AlgorithmBinderPool", stringBuilder.toString());
          } 
        }
      };
    this.mContext = paramContext.getApplicationContext();
    initIsSystemUser();
  }
  
  private boolean checkIfLegalDisconnectState() {
    boolean bool = false;
    synchronized (OBSERVER_LOCK) {
      if (getState() == 2) {
        setState(3);
        bool = true;
      } 
      return bool;
    } 
  }
  
  public void registerServiceStateObserver(ServiceStateObserver paramServiceStateObserver) {
    synchronized (LOCK) {
      this.mStateObservers.putIfAbsent(paramServiceStateObserver, null);
      return;
    } 
  }
  
  public boolean isAvaliable() {
    boolean bool;
    if (getState() == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void onServiceDied() {
    if (isOnMainThread()) {
      this.mExecutor.execute(new _$$Lambda$AlgorithmBinderPool$2ICoYmphzEUoW2LwGy2HYbnuRAY(this));
    } else {
      deliveryOnServiceDied();
    } 
  }
  
  private void deliveryOnServiceDied() {
    // Byte code:
    //   0: iconst_0
    //   1: istore_1
    //   2: iconst_0
    //   3: anewarray com/oplus/deepthinker/ServiceStateObserver
    //   6: astore_2
    //   7: getstatic com/oplus/deepthinker/AlgorithmBinderPool.LOCK : Ljava/lang/Object;
    //   10: astore_3
    //   11: aload_3
    //   12: monitorenter
    //   13: aload_0
    //   14: getfield mStateObservers : Ljava/util/WeakHashMap;
    //   17: invokevirtual keySet : ()Ljava/util/Set;
    //   20: iconst_0
    //   21: anewarray com/oplus/deepthinker/ServiceStateObserver
    //   24: invokeinterface toArray : ([Ljava/lang/Object;)[Ljava/lang/Object;
    //   29: checkcast [Lcom/oplus/deepthinker/ServiceStateObserver;
    //   32: astore #4
    //   34: aload #4
    //   36: astore_2
    //   37: goto -> 52
    //   40: astore #4
    //   42: ldc 'AlgorithmBinderPool'
    //   44: ldc_w 'deliveryOnServiceDied'
    //   47: aload #4
    //   49: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   52: aload_3
    //   53: monitorexit
    //   54: aload_2
    //   55: arraylength
    //   56: istore #5
    //   58: iload_1
    //   59: iload #5
    //   61: if_icmpge -> 87
    //   64: aload_2
    //   65: iload_1
    //   66: aaload
    //   67: astore #4
    //   69: aload #4
    //   71: ifnull -> 81
    //   74: aload #4
    //   76: invokeinterface onServiceDied : ()V
    //   81: iinc #1, 1
    //   84: goto -> 58
    //   87: return
    //   88: astore_2
    //   89: aload_3
    //   90: monitorexit
    //   91: aload_2
    //   92: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #228	-> 0
    //   #229	-> 7
    //   #231	-> 13
    //   #234	-> 37
    //   #232	-> 40
    //   #233	-> 42
    //   #235	-> 52
    //   #237	-> 54
    //   #238	-> 69
    //   #239	-> 74
    //   #237	-> 81
    //   #242	-> 87
    //   #235	-> 88
    // Exception table:
    //   from	to	target	type
    //   13	34	40	finally
    //   42	52	88	finally
    //   52	54	88	finally
    //   89	91	88	finally
  }
  
  private void connectBinderPoolService() {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial isQ : ()Z
    //   4: ifne -> 23
    //   7: aload_0
    //   8: invokespecial isOnMainThread : ()Z
    //   11: ifeq -> 23
    //   14: ldc 'AlgorithmBinderPool'
    //   16: ldc_w 'bind service on main thread, ignore ileagal usage.'
    //   19: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)V
    //   22: return
    //   23: aload_0
    //   24: invokespecial getState : ()I
    //   27: iconst_2
    //   28: if_icmpeq -> 196
    //   31: aload_0
    //   32: invokespecial getState : ()I
    //   35: iconst_1
    //   36: if_icmpne -> 42
    //   39: goto -> 196
    //   42: aload_0
    //   43: invokespecial shouldDoConnect : ()Z
    //   46: ifne -> 58
    //   49: ldc 'AlgorithmBinderPool'
    //   51: ldc_w 'connectBinderPoolService: connect request intercept by lazyRetry.'
    //   54: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)V
    //   57: return
    //   58: ldc 'AlgorithmBinderPool'
    //   60: ldc_w 'start connect Binder PoolService'
    //   63: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)V
    //   66: aload_0
    //   67: iconst_1
    //   68: invokespecial setState : (I)V
    //   71: aload_0
    //   72: invokespecial bindServiceByPlatform : ()Z
    //   75: ifeq -> 169
    //   78: new java/util/concurrent/CountDownLatch
    //   81: dup
    //   82: iconst_1
    //   83: invokespecial <init> : (I)V
    //   86: astore_1
    //   87: aload_0
    //   88: aload_1
    //   89: putfield mCountDownLatch : Ljava/util/concurrent/CountDownLatch;
    //   92: aload_1
    //   93: ldc2_w 2000
    //   96: getstatic java/util/concurrent/TimeUnit.MILLISECONDS : Ljava/util/concurrent/TimeUnit;
    //   99: invokevirtual await : (JLjava/util/concurrent/TimeUnit;)Z
    //   102: pop
    //   103: aload_0
    //   104: invokespecial getState : ()I
    //   107: iconst_2
    //   108: if_icmpeq -> 116
    //   111: aload_0
    //   112: iconst_3
    //   113: invokespecial setState : (I)V
    //   116: aload_0
    //   117: iconst_0
    //   118: invokespecial setLazyRetryTag : (Z)V
    //   121: goto -> 187
    //   124: astore_1
    //   125: goto -> 149
    //   128: astore_1
    //   129: ldc 'AlgorithmBinderPool'
    //   131: ldc_w 'connectBinderPoolService error: '
    //   134: aload_1
    //   135: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   138: aload_0
    //   139: invokespecial getState : ()I
    //   142: iconst_2
    //   143: if_icmpeq -> 116
    //   146: goto -> 111
    //   149: aload_0
    //   150: invokespecial getState : ()I
    //   153: iconst_2
    //   154: if_icmpeq -> 162
    //   157: aload_0
    //   158: iconst_3
    //   159: invokespecial setState : (I)V
    //   162: aload_0
    //   163: iconst_0
    //   164: invokespecial setLazyRetryTag : (Z)V
    //   167: aload_1
    //   168: athrow
    //   169: aload_0
    //   170: iconst_3
    //   171: invokespecial setState : (I)V
    //   174: aload_0
    //   175: iconst_1
    //   176: invokespecial setLazyRetryTag : (Z)V
    //   179: ldc 'AlgorithmBinderPool'
    //   181: ldc_w 'Error: Bind Algorithm Service Failed, not exist!'
    //   184: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)V
    //   187: ldc 'AlgorithmBinderPool'
    //   189: ldc_w 'end connect Binder PoolService'
    //   192: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)V
    //   195: return
    //   196: ldc 'AlgorithmBinderPool'
    //   198: ldc_w 'Already connected or connecting, do not reconnect again.'
    //   201: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)V
    //   204: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #245	-> 0
    //   #247	-> 14
    //   #248	-> 22
    //   #251	-> 23
    //   #256	-> 42
    //   #257	-> 49
    //   #258	-> 57
    //   #261	-> 58
    //   #263	-> 66
    //   #265	-> 71
    //   #266	-> 78
    //   #269	-> 92
    //   #274	-> 103
    //   #275	-> 111
    //   #277	-> 116
    //   #278	-> 121
    //   #274	-> 124
    //   #270	-> 128
    //   #271	-> 129
    //   #274	-> 138
    //   #275	-> 146
    //   #274	-> 149
    //   #275	-> 157
    //   #277	-> 162
    //   #278	-> 167
    //   #280	-> 169
    //   #281	-> 174
    //   #283	-> 179
    //   #286	-> 187
    //   #287	-> 195
    //   #252	-> 196
    //   #253	-> 204
    // Exception table:
    //   from	to	target	type
    //   92	103	128	java/lang/InterruptedException
    //   92	103	124	finally
    //   129	138	124	finally
  }
  
  private boolean bindServiceByPlatform() {
    Intent intent = new Intent();
    intent.setAction("com.coloros.deepthinker.services.AIAlgorithmService");
    intent.setPackage("com.coloros.deepthinker");
    return bindService(intent, this.mBinderPoolConnection);
  }
  
  private boolean bindService(Intent paramIntent, ServiceConnection paramServiceConnection) {
    if (this.mIsSystemUser) {
      if (this.mMyUser == null)
        this.mMyUser = Process.myUserHandle(); 
      if (this.mHandler == null)
        try {
          HandlerThread handlerThread = new HandlerThread();
          this("deepthinker_sdk_inner");
          handlerThread.start();
          Handler handler = new Handler();
          this(handlerThread.getLooper());
          this.mHandler = handler;
        } catch (Exception null) {
          SDKLog.e("AlgorithmBinderPool", "on init handle", exception);
          return true;
        }  
      try {
        Class<?> clazz1 = this.mContext.getClass();
        Class<int> clazz = int.class;
        Method method = clazz1.getMethod("bindServiceAsUser", new Class[] { Intent.class, ServiceConnection.class, clazz, Handler.class, UserHandle.class });
        Context context = this.mContext;
        Boolean bool = (Boolean)method.invoke(context, new Object[] { exception, paramServiceConnection, Integer.valueOf(1), this.mHandler, this.mMyUser });
        if (bool != null)
          return bool.booleanValue(); 
      } catch (Exception exception) {
        SDKLog.e("AlgorithmBinderPool", "bindServiceAsUser", exception);
      } 
      return true;
    } 
    if (isQ())
      return this.mContext.bindService((Intent)exception, 1, this.mExecutor, paramServiceConnection); 
    return this.mContext.bindService((Intent)exception, paramServiceConnection, 1);
  }
  
  private boolean isQ() {
    boolean bool;
    if (Build.VERSION.SDK_INT >= 29) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private IBinder queryBinder(int paramInt) {
    IBinder iBinder1;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("queryBinder: code ");
    stringBuilder.append(paramInt);
    SDKLog.i("AlgorithmBinderPool", stringBuilder.toString());
    stringBuilder = null;
    IBinder iBinder2 = null;
    int i = getState();
    if (i != 2) {
      if (isOnMainThread()) {
        SDKLog.w("AlgorithmBinderPool", "queryBinder: query on main thread rejected by illegal state.\n[Tips: Try migrate api calls to async thread ]");
        return null;
      } 
      if (i == 1) {
        long l = System.currentTimeMillis();
        while (System.currentTimeMillis() - l <= 2000L) {
          try {
            Thread.sleep(500L);
          } finally {
            Exception exception = null;
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("queryBinder: thread sleep interrupted! ");
            stringBuilder1.append(exception);
          } 
          if (getState() != 1)
            break; 
        } 
      } else if (i == 3 || i == 0) {
        synchronized (CONN_LOCK) {
          tryConnectBinder();
        } 
      } 
    } 
    try {
      if (this.mBinderPool != null) {
        iBinder1 = iBinder2 = this.mBinderPool.queryBinder(paramInt);
      } else {
        SDKLog.d("AlgorithmBinderPool", "queryBinder: Binder pool is null");
        iBinder1 = iBinder2;
      } 
    } catch (RemoteException remoteException) {
      SDKLog.e("AlgorithmBinderPool", "queryBinder error: ", (Throwable)remoteException);
    } 
    return iBinder1;
  }
  
  private void tryConnectBinder() {
    if (getState() == 2 || getState() == 1) {
      SDKLog.w("AlgorithmBinderPool", "Already connected or connecting, do not reconnect again.");
      return;
    } 
    Thread thread = new Thread(new Runnable() {
          final AlgorithmBinderPool this$0;
          
          public void run() {
            SDKLog.d("AlgorithmBinderPool", "tryConnectBinder : start connect on async thread.");
            AlgorithmBinderPool.this.connectBinderPoolService();
            SDKLog.d("AlgorithmBinderPool", "tryConnectBinder : end connect on async thread.");
          }
        });
    thread.start();
    boolean bool = isOnMainThread();
    if (isQ() || !bool) {
      try {
        thread.join(2000L);
      } catch (InterruptedException interruptedException) {
        SDKLog.e("AlgorithmBinderPool", "tryConnectBinder: interrupted.", interruptedException);
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("tryConnectBinder end. On ");
      if (bool) {
        str = "Main";
      } else {
        str = "Async";
      } 
      stringBuilder.append(str);
      stringBuilder.append(" Thread, connect ");
      if (getState() == 2) {
        str = "sucess.";
      } else {
        str = "timeout.";
      } 
      stringBuilder.append(str);
      String str = stringBuilder.toString();
      SDKLog.d("AlgorithmBinderPool", str);
      return;
    } 
    SDKLog.d("AlgorithmBinderPool", "tryConnectBinder end. On Main Thread, reutrn directly.");
  }
  
  private void setLazyRetryTag(boolean paramBoolean) {
    this.mLazyRetry = paramBoolean;
    if (!paramBoolean)
      this.mCurrentRetryIndex = 0; 
  }
  
  private boolean shouldDoConnect() {
    boolean bool = this.mLazyRetry;
    boolean bool1 = true;
    if (bool) {
      int i = (new Random(System.currentTimeMillis())).nextInt(100);
      int j = this.mCurrentRetryIndex;
      this.mCurrentRetryIndex = j + 1;
      if (i > j)
        bool1 = false; 
      return bool1;
    } 
    return true;
  }
  
  private boolean isOnMainThread() {
    boolean bool;
    if (Looper.getMainLooper() == Looper.myLooper()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void setState(int paramInt) {
    synchronized (LOCK) {
      this.mState = paramInt;
      invalid();
      return;
    } 
  }
  
  private int getState() {
    synchronized (LOCK) {
      return this.mState;
    } 
  }
  
  private String logState(int paramInt) {
    String str;
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            str = "NoSuchState";
          } else {
            str = "DisconnectedState";
          } 
        } else {
          str = "ConnectedState";
        } 
      } else {
        str = "ConnectingState";
      } 
    } else {
      str = "InitState";
    } 
    return str;
  }
  
  private boolean isAlive(IBinder paramIBinder) {
    boolean bool;
    if (paramIBinder != null && paramIBinder.isBinderAlive() && paramIBinder.pingBinder()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public IAlgorithmPlatform getPlatformBinder() {
    if (this.mPlatformBinder != null && isAvaliable() && isAlive(this.mPlatformBinder.asBinder()))
      return this.mPlatformBinder; 
    IBinder iBinder = queryBinder(1);
    IAlgorithmPlatform iAlgorithmPlatform = IAlgorithmPlatform.Stub.asInterface(iBinder);
    return iAlgorithmPlatform;
  }
  
  public IEventHandleService getEventHandleBinder() {
    if (this.mEventHandleBinder != null && isAvaliable() && isAlive(this.mEventHandleBinder.asBinder()))
      return this.mEventHandleBinder; 
    IBinder iBinder = queryBinder(5);
    IEventHandleService iEventHandleService = IEventHandleService.Stub.asInterface(iBinder);
    return iEventHandleService;
  }
  
  public IAppPredictAidlInterface getAppPredictBinder() {
    if (this.mAppPredictBinder != null && isAvaliable() && isAlive(this.mAppPredictBinder.asBinder()))
      return this.mAppPredictBinder; 
    IBinder iBinder = queryBinder(2);
    IAppPredictAidlInterface iAppPredictAidlInterface = IAppPredictAidlInterface.Stub.asInterface(iBinder);
    return iAppPredictAidlInterface;
  }
  
  public IDeepSleepPredict getDeepSleepBinder() {
    if (this.mDeepSleepBinder != null && isAvaliable() && isAlive(this.mDeepSleepBinder.asBinder()))
      return this.mDeepSleepBinder; 
    IBinder iBinder = queryBinder(3);
    IDeepSleepPredict iDeepSleepPredict = IDeepSleepPredict.Stub.asInterface(iBinder);
    return iDeepSleepPredict;
  }
  
  public IAppType getAppTypeBinder() {
    if (this.mAppTypeBinder != null && isAvaliable() && isAlive(this.mAppTypeBinder.asBinder()))
      return this.mAppTypeBinder; 
    IBinder iBinder = queryBinder(4);
    IAppType iAppType = IAppType.Stub.asInterface(iBinder);
    return iAppType;
  }
  
  public IAppSort getAppSortBinder() {
    if (this.mAppSortBinder != null && isAvaliable() && isAlive(this.mAppSortBinder.asBinder()))
      return this.mAppSortBinder; 
    IBinder iBinder = queryBinder(6);
    IAppSort iAppSort = IAppSort.Stub.asInterface(iBinder);
    return iAppSort;
  }
  
  public ISmartGpsAidlInterface getSmartGpsBinder() {
    if (this.mSmartGpsBinder != null && isAvaliable() && isAlive(this.mSmartGpsBinder.asBinder()))
      return this.mSmartGpsBinder; 
    IBinder iBinder = queryBinder(9);
    ISmartGpsAidlInterface iSmartGpsAidlInterface = ISmartGpsAidlInterface.Stub.asInterface(iBinder);
    return iSmartGpsAidlInterface;
  }
  
  public void run(Runnable paramRunnable) {
    this.mExecutor.execute(paramRunnable);
  }
}
