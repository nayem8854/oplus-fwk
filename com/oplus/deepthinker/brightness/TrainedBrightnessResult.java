package com.oplus.deepthinker.brightness;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TrainedBrightnessResult implements Parcelable {
  public TrainedBrightnessResult(Map<String, List<TrainedBrightnessPoint>> paramMap) {
    this.mTrainedBrightnessPoints = paramMap;
  }
  
  public Map<String, List<TrainedBrightnessPoint>> getTrainedBrightnessPoints() {
    return this.mTrainedBrightnessPoints;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mTrainedBrightnessPoints.size());
    for (Map.Entry<String, List<TrainedBrightnessPoint>> entry : this.mTrainedBrightnessPoints.entrySet()) {
      paramParcel.writeString((String)entry.getKey());
      paramParcel.writeTypedList((List)entry.getValue());
    } 
  }
  
  protected TrainedBrightnessResult(Parcel paramParcel) {
    int i = paramParcel.readInt();
    this.mTrainedBrightnessPoints = new HashMap<>(i);
    for (byte b = 0; b < i; b++) {
      String str = paramParcel.readString();
      ArrayList<TrainedBrightnessPoint> arrayList = paramParcel.createTypedArrayList(TrainedBrightnessPoint.CREATOR);
      this.mTrainedBrightnessPoints.put(str, arrayList);
    } 
  }
  
  public static final Parcelable.Creator<TrainedBrightnessResult> CREATOR = new Parcelable.Creator<TrainedBrightnessResult>() {
      public TrainedBrightnessResult createFromParcel(Parcel param1Parcel) {
        return new TrainedBrightnessResult(param1Parcel);
      }
      
      public TrainedBrightnessResult[] newArray(int param1Int) {
        return new TrainedBrightnessResult[param1Int];
      }
    };
  
  private Map<String, List<TrainedBrightnessPoint>> mTrainedBrightnessPoints;
}
