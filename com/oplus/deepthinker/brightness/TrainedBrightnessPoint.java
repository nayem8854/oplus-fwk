package com.oplus.deepthinker.brightness;

import android.os.Parcel;
import android.os.Parcelable;

public class TrainedBrightnessPoint implements Parcelable {
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeFloat(this.x);
    paramParcel.writeFloat(this.y);
  }
  
  public TrainedBrightnessPoint(float paramFloat1, float paramFloat2) {
    this.x = paramFloat1;
    this.y = paramFloat2;
  }
  
  protected TrainedBrightnessPoint(Parcel paramParcel) {
    this.x = paramParcel.readFloat();
    this.y = paramParcel.readFloat();
  }
  
  public static final Parcelable.Creator<TrainedBrightnessPoint> CREATOR = new Parcelable.Creator<TrainedBrightnessPoint>() {
      public TrainedBrightnessPoint createFromParcel(Parcel param1Parcel) {
        return new TrainedBrightnessPoint(param1Parcel);
      }
      
      public TrainedBrightnessPoint[] newArray(int param1Int) {
        return new TrainedBrightnessPoint[param1Int];
      }
    };
  
  public float x;
  
  public float y;
}
