package com.oplus.deepthinker;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.os.Bundle;
import android.os.Handler;
import com.oplus.deepthinker.sdk.aidl.proton.appactionpredict.PredictAABResult;
import com.oplus.deepthinker.sdk.aidl.proton.appactionpredict.PredictResult;
import com.oplus.deepthinker.sdk.aidl.proton.deepsleep.DeepSleepPredictResult;
import com.oplus.deepthinker.sdk.aidl.proton.deepsleep.SleepRecord;
import com.oplus.deepthinker.sdk.aidl.proton.deepsleep.TotalPredictResult;
import com.oplus.deepthinker.sdk.aidl.proton.userprofile.WifiLocationLabel;
import com.oplus.eventhub.sdk.aidl.Event;
import com.oplus.eventhub.sdk.aidl.EventConfig;
import com.oplus.eventhub.sdk.aidl.EventRequestConfig;
import com.oplus.eventhub.sdk.aidl.IEventCallback;
import com.oplus.eventhub.sdk.aidl.TriggerEvent;
import java.util.List;
import java.util.Map;

public interface IOplusDeepThinkerManager extends IOplusCommonFeature {
  public static final IOplusDeepThinkerManager DEFAULT = (IOplusDeepThinkerManager)new Object();
  
  default IOplusDeepThinkerManager getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusDeepThinkerManager;
  }
  
  default void registerServiceStateObserver(ServiceStateObserver paramServiceStateObserver) {}
  
  default int getAlgorithmPlatformVersion() {
    return -1;
  }
  
  default PredictAABResult getPredictAABResult() {
    return null;
  }
  
  default List<PredictResult> getAppPredictResultMap(String paramString) {
    return null;
  }
  
  default PredictResult getAppPredictResult(String paramString) {
    return null;
  }
  
  default DeepSleepPredictResult getDeepSleepPredictResult() {
    return null;
  }
  
  default SleepRecord getLastDeepSleepRecord() {
    return null;
  }
  
  default TotalPredictResult getDeepSleepTotalPredictResult() {
    return null;
  }
  
  default DeepSleepPredictResult getPredictResultWithFeedBack() {
    return null;
  }
  
  default int getAppType(String paramString) {
    return -1;
  }
  
  default Map getAppTypeMap(List<String> paramList) {
    return null;
  }
  
  default void triggerHookEvent(TriggerEvent paramTriggerEvent) {}
  
  default void triggerHookEvent(int paramInt1, int paramInt2, String paramString, Bundle paramBundle) {}
  
  default void triggerHookEventAsync(Handler paramHandler, int paramInt1, int paramInt2, String paramString, Bundle paramBundle) {}
  
  default boolean registerCallback(IEventCallback paramIEventCallback, EventRequestConfig paramEventRequestConfig) {
    return false;
  }
  
  default boolean unregisterCallback(IEventCallback paramIEventCallback) {
    return false;
  }
  
  default int registerEventCallback(IEventCallback paramIEventCallback, EventConfig paramEventConfig) {
    return 8;
  }
  
  default int unregisterEventCallback(IEventCallback paramIEventCallback) {
    return 8;
  }
  
  default List<Event> getAvailableEvent() {
    return null;
  }
  
  default boolean isAvailableEvent(Event paramEvent) {
    return false;
  }
  
  default Bundle call(Bundle paramBundle) {
    return null;
  }
  
  default List<String> getAppQueueSortedByTime() {
    return null;
  }
  
  default List<String> getAppQueueSortedByCount() {
    return null;
  }
  
  default List<String> getAppQueueSortedByComplex() {
    return null;
  }
  
  default List<String> getSmartGpsBssidList() {
    return null;
  }
  
  default void run(Runnable paramRunnable) {}
  
  default List<WifiLocationLabel> getWifiLocationLabels() {
    return null;
  }
}
