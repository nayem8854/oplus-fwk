package com.oplus.deepthinker;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.os.RemoteException;
import com.oplus.deepthinker.sdk.aidl.platform.IAlgorithmPlatform;
import com.oplus.deepthinker.sdk.aidl.proton.appactionpredict.IAppPredictAidlInterface;
import com.oplus.deepthinker.sdk.aidl.proton.appactionpredict.PredictAABResult;
import com.oplus.deepthinker.sdk.aidl.proton.appactionpredict.PredictResult;
import com.oplus.deepthinker.sdk.aidl.proton.appsort.IAppSort;
import com.oplus.deepthinker.sdk.aidl.proton.apptype.IAppType;
import com.oplus.deepthinker.sdk.aidl.proton.deepsleep.DeepSleepPredictResult;
import com.oplus.deepthinker.sdk.aidl.proton.deepsleep.IDeepSleepPredict;
import com.oplus.deepthinker.sdk.aidl.proton.deepsleep.SleepRecord;
import com.oplus.deepthinker.sdk.aidl.proton.deepsleep.TotalPredictResult;
import com.oplus.deepthinker.sdk.aidl.proton.smartgps.ISmartGpsAidlInterface;
import com.oplus.deepthinker.sdk.aidl.proton.userprofile.WifiLocationLabel;
import com.oplus.deepthinker.sdk.common.utils.SDKLog;
import com.oplus.deepthinker.userprofile.utils.WifiLocationLabelUtils;
import com.oplus.eventhub.sdk.aidl.Event;
import com.oplus.eventhub.sdk.aidl.EventConfig;
import com.oplus.eventhub.sdk.aidl.EventRequestConfig;
import com.oplus.eventhub.sdk.aidl.IEventCallback;
import com.oplus.eventhub.sdk.aidl.IEventHandleService;
import com.oplus.eventhub.sdk.aidl.TriggerEvent;
import java.util.List;
import java.util.Map;

public class OplusDeepThinkerManager implements IOplusDeepThinkerManager {
  private static final String TAG = "OplusDeepThinkerManager";
  
  private static volatile OplusDeepThinkerManager sInstance;
  
  private AlgorithmBinderPool mAlgorithmBinderPool;
  
  private final Context mContext;
  
  private TriggerEventRunnable mTriggereventRunnable;
  
  private OplusDeepThinkerManager(Context paramContext) {
    this.mContext = paramContext = paramContext.getApplicationContext();
    this.mAlgorithmBinderPool = AlgorithmBinderPool.getInstance(paramContext);
  }
  
  public static OplusDeepThinkerManager getInstance(Context paramContext) {
    // Byte code:
    //   0: getstatic com/oplus/deepthinker/OplusDeepThinkerManager.sInstance : Lcom/oplus/deepthinker/OplusDeepThinkerManager;
    //   3: ifnonnull -> 40
    //   6: ldc com/oplus/deepthinker/OplusDeepThinkerManager
    //   8: monitorenter
    //   9: getstatic com/oplus/deepthinker/OplusDeepThinkerManager.sInstance : Lcom/oplus/deepthinker/OplusDeepThinkerManager;
    //   12: ifnonnull -> 28
    //   15: new com/oplus/deepthinker/OplusDeepThinkerManager
    //   18: astore_1
    //   19: aload_1
    //   20: aload_0
    //   21: invokespecial <init> : (Landroid/content/Context;)V
    //   24: aload_1
    //   25: putstatic com/oplus/deepthinker/OplusDeepThinkerManager.sInstance : Lcom/oplus/deepthinker/OplusDeepThinkerManager;
    //   28: ldc com/oplus/deepthinker/OplusDeepThinkerManager
    //   30: monitorexit
    //   31: goto -> 40
    //   34: astore_0
    //   35: ldc com/oplus/deepthinker/OplusDeepThinkerManager
    //   37: monitorexit
    //   38: aload_0
    //   39: athrow
    //   40: getstatic com/oplus/deepthinker/OplusDeepThinkerManager.sInstance : Lcom/oplus/deepthinker/OplusDeepThinkerManager;
    //   43: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #69	-> 0
    //   #70	-> 6
    //   #71	-> 9
    //   #72	-> 15
    //   #74	-> 28
    //   #76	-> 40
    // Exception table:
    //   from	to	target	type
    //   9	15	34	finally
    //   15	28	34	finally
    //   28	31	34	finally
    //   35	38	34	finally
  }
  
  public void registerServiceStateObserver(ServiceStateObserver paramServiceStateObserver) {
    this.mAlgorithmBinderPool.registerServiceStateObserver(paramServiceStateObserver);
  }
  
  public IAlgorithmPlatform getPlatformBinder() {
    return this.mAlgorithmBinderPool.getPlatformBinder();
  }
  
  public IAppPredictAidlInterface getAppPredictBinder() {
    return this.mAlgorithmBinderPool.getAppPredictBinder();
  }
  
  public IDeepSleepPredict getDeepSleepBinder() {
    return this.mAlgorithmBinderPool.getDeepSleepBinder();
  }
  
  public IAppType getAppTypeBinder() {
    return this.mAlgorithmBinderPool.getAppTypeBinder();
  }
  
  public IEventHandleService getEventHandleBinder() {
    return this.mAlgorithmBinderPool.getEventHandleBinder();
  }
  
  public IAppSort getAppSortBinder() {
    return this.mAlgorithmBinderPool.getAppSortBinder();
  }
  
  public ISmartGpsAidlInterface getSmartGpsBinder() {
    return this.mAlgorithmBinderPool.getSmartGpsBinder();
  }
  
  public int getAlgorithmPlatformVersion() {
    try {
      IAlgorithmPlatform iAlgorithmPlatform = getPlatformBinder();
      if (iAlgorithmPlatform != null)
        return iAlgorithmPlatform.getAlgorithmPlatformVersion(); 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", "getAlgorithmPlatformVersion", (Throwable)remoteException);
    } 
    return -1;
  }
  
  public PredictAABResult getPredictAABResult() {
    try {
      IAppPredictAidlInterface iAppPredictAidlInterface = getAppPredictBinder();
      if (iAppPredictAidlInterface != null)
        return iAppPredictAidlInterface.getPredictAABResult(); 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", "getPredictAABResult", (Throwable)remoteException);
    } 
    return null;
  }
  
  public List<PredictResult> getAppPredictResultMap(String paramString) {
    try {
      IAppPredictAidlInterface iAppPredictAidlInterface = getAppPredictBinder();
      if (iAppPredictAidlInterface != null)
        return iAppPredictAidlInterface.getAppPredictResultMap(paramString); 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", "getAppPredictResultMap", (Throwable)remoteException);
    } 
    return null;
  }
  
  public PredictResult getAppPredictResult(String paramString) {
    try {
      IAppPredictAidlInterface iAppPredictAidlInterface = getAppPredictBinder();
      if (iAppPredictAidlInterface != null)
        return iAppPredictAidlInterface.getAppPredictResult(paramString); 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", "getAppPredictResult", (Throwable)remoteException);
    } 
    return null;
  }
  
  public DeepSleepPredictResult getDeepSleepPredictResult() {
    try {
      IDeepSleepPredict iDeepSleepPredict = getDeepSleepBinder();
      if (iDeepSleepPredict != null)
        return iDeepSleepPredict.getDeepSleepPredictResult(); 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", "getDeepSleepPredictResult", (Throwable)remoteException);
    } 
    return null;
  }
  
  public SleepRecord getLastDeepSleepRecord() {
    try {
      IDeepSleepPredict iDeepSleepPredict = getDeepSleepBinder();
      if (iDeepSleepPredict != null)
        return iDeepSleepPredict.getLastDeepSleepRecord(); 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", "getLastDeepSleepRecord", (Throwable)remoteException);
    } 
    return null;
  }
  
  public TotalPredictResult getDeepSleepTotalPredictResult() {
    try {
      IDeepSleepPredict iDeepSleepPredict = getDeepSleepBinder();
      if (iDeepSleepPredict != null)
        return iDeepSleepPredict.getDeepSleepTotalPredictResult(); 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", "getDeepSleepTotalPredictResult", (Throwable)remoteException);
    } 
    return null;
  }
  
  public DeepSleepPredictResult getPredictResultWithFeedBack() {
    try {
      IDeepSleepPredict iDeepSleepPredict = getDeepSleepBinder();
      if (iDeepSleepPredict != null)
        return iDeepSleepPredict.getPredictResultWithFeedBack(); 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", remoteException.getMessage());
    } 
    return null;
  }
  
  public int getAppType(String paramString) {
    try {
      IAppType iAppType = getAppTypeBinder();
      if (iAppType != null)
        return iAppType.getAppType(paramString); 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", "getAppType", (Throwable)remoteException);
    } 
    return -1;
  }
  
  public Map getAppTypeMap(List<String> paramList) {
    try {
      IAppType iAppType = getAppTypeBinder();
      if (iAppType != null)
        return iAppType.getAppTypeMap(paramList); 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", "getAppTypeMap", (Throwable)remoteException);
    } 
    return null;
  }
  
  public List<String> getSmartGpsBssidList() {
    try {
      ISmartGpsAidlInterface iSmartGpsAidlInterface = getSmartGpsBinder();
      if (iSmartGpsAidlInterface != null)
        return iSmartGpsAidlInterface.getSmartGpsBssidList(); 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", "getSmartGpsBssidList", (Throwable)remoteException);
    } 
    return null;
  }
  
  public void triggerHookEvent(TriggerEvent paramTriggerEvent) {
    if (paramTriggerEvent == null) {
      SDKLog.e("OplusDeepThinkerManager", "triggerHookEvent null parameter.");
      return;
    } 
    try {
      IEventHandleService iEventHandleService = getEventHandleBinder();
      if (iEventHandleService != null)
        iEventHandleService.triggerHookEvent(paramTriggerEvent); 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", "triggerHookEvent", (Throwable)remoteException);
    } 
  }
  
  public void triggerHookEvent(int paramInt1, int paramInt2, String paramString, Bundle paramBundle) {
    TriggerEvent triggerEvent = new TriggerEvent(paramInt1, paramInt2, paramString, paramBundle);
    triggerHookEvent(triggerEvent);
  }
  
  public void triggerHookEventAsync(Handler paramHandler, int paramInt1, int paramInt2, String paramString, Bundle paramBundle) {
    if (paramHandler == null)
      return; 
    TriggerEventRunnable triggerEventRunnable = this.mTriggereventRunnable;
    if (triggerEventRunnable == null) {
      this.mTriggereventRunnable = new TriggerEventRunnable(paramInt1, paramInt2, paramString, paramBundle);
    } else {
      triggerEventRunnable.mEventID = paramInt1;
      this.mTriggereventRunnable.mUid = paramInt2;
      this.mTriggereventRunnable.mPkg = paramString;
      this.mTriggereventRunnable.mExtra = paramBundle;
    } 
    paramHandler.post(this.mTriggereventRunnable);
  }
  
  public boolean registerCallback(IEventCallback paramIEventCallback, EventRequestConfig paramEventRequestConfig) {
    if (paramIEventCallback == null || paramEventRequestConfig == null || paramEventRequestConfig.getAllEvents().isEmpty()) {
      SDKLog.e("OplusDeepThinkerManager", "registerCallback invalid para. null or without request config.");
      return false;
    } 
    try {
      int i = paramIEventCallback.hashCode();
      IEventHandleService iEventHandleService = getEventHandleBinder();
      if (iEventHandleService != null)
        return iEventHandleService.registerCallback(paramIEventCallback, i, paramEventRequestConfig); 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", "registerCallback", (Throwable)remoteException);
    } 
    return false;
  }
  
  public boolean unregisterCallback(IEventCallback paramIEventCallback) {
    if (paramIEventCallback == null) {
      SDKLog.e("OplusDeepThinkerManager", "unRegisterCallback null parameter.");
      return false;
    } 
    try {
      int i = paramIEventCallback.hashCode();
      IEventHandleService iEventHandleService = getEventHandleBinder();
      if (iEventHandleService != null)
        return iEventHandleService.unregisterCallback(i); 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", "unregisterCallback", (Throwable)remoteException);
    } 
    return false;
  }
  
  public int registerEventCallback(IEventCallback paramIEventCallback, EventConfig paramEventConfig) {
    if (!isVersionSupport())
      return 8; 
    if (paramIEventCallback == null || paramEventConfig == null || paramEventConfig.getAllEvents().isEmpty()) {
      SDKLog.e("OplusDeepThinkerManager", "registerEventCallback invalid para. null or without request config.");
      return 16;
    } 
    try {
      int i = paramIEventCallback.hashCode();
      int j = Process.myPid();
      IEventHandleService iEventHandleService = getEventHandleBinder();
      if (iEventHandleService != null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(String.valueOf(i));
        stringBuilder.append(j);
        return iEventHandleService.registerEventCallback(stringBuilder.toString(), paramIEventCallback, paramEventConfig);
      } 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", "registerEventCallback", (Throwable)remoteException);
    } 
    return 128;
  }
  
  public int unregisterEventCallback(IEventCallback paramIEventCallback) {
    if (!isVersionSupport())
      return 8; 
    if (paramIEventCallback == null) {
      SDKLog.e("OplusDeepThinkerManager", "unregisterEventCallback null parameter.");
      return 16;
    } 
    try {
      int i = paramIEventCallback.hashCode();
      int j = Process.myPid();
      IEventHandleService iEventHandleService = getEventHandleBinder();
      if (iEventHandleService != null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(String.valueOf(i));
        stringBuilder.append(j);
        return iEventHandleService.unregisterEventCallback(stringBuilder.toString());
      } 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", "unregisterEventCallback", (Throwable)remoteException);
    } 
    return 128;
  }
  
  public List<Event> getAvailableEvent() {
    if (!isVersionSupport()) {
      SDKLog.w("OplusDeepThinkerManager", "getAvailableEvent is not supported below android R.");
      return null;
    } 
    try {
      IEventHandleService iEventHandleService = getEventHandleBinder();
      if (iEventHandleService != null)
        return iEventHandleService.getAvailableEvent(); 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", "getAvailableEvent", (Throwable)remoteException);
    } 
    return null;
  }
  
  public boolean isAvailableEvent(Event paramEvent) {
    if (!isVersionSupport()) {
      SDKLog.w("OplusDeepThinkerManager", "isAvailableEvent is not supported below android R.");
      return false;
    } 
    try {
      IEventHandleService iEventHandleService = getEventHandleBinder();
      if (iEventHandleService != null)
        return iEventHandleService.isAvailableEvent(paramEvent); 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", "isAvailableEvent", (Throwable)remoteException);
    } 
    return false;
  }
  
  public Bundle call(Bundle paramBundle) {
    if (!isVersionSupport()) {
      SDKLog.w("OplusDeepThinkerManager", "call is not supported below android R.");
      return null;
    } 
    try {
      IEventHandleService iEventHandleService = getEventHandleBinder();
      if (iEventHandleService != null)
        return iEventHandleService.call(paramBundle); 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", "call", (Throwable)remoteException);
    } 
    return null;
  }
  
  private boolean isVersionSupport() {
    boolean bool;
    int i = Build.VERSION.SDK_INT;
    if (i >= 30) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public List<String> getAppQueueSortedByCount() {
    try {
      IAppSort iAppSort = getAppSortBinder();
      if (iAppSort != null)
        return iAppSort.getAppQueueSortedByCount(); 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", "getAppQueueSortedByCount", (Throwable)remoteException);
    } 
    return null;
  }
  
  public List<String> getAppQueueSortedByComplex() {
    try {
      IAppSort iAppSort = getAppSortBinder();
      if (iAppSort != null)
        return iAppSort.getAppQueueSortedByComplex(); 
    } catch (RemoteException remoteException) {
      SDKLog.e("OplusDeepThinkerManager", "getAppQueueSortedByComplex", (Throwable)remoteException);
    } 
    return null;
  }
  
  public List<WifiLocationLabel> getWifiLocationLabels() {
    return WifiLocationLabelUtils.getWifiLocationLabels(this.mContext);
  }
  
  class TriggerEventRunnable implements Runnable {
    int mEventID;
    
    Bundle mExtra;
    
    String mPkg;
    
    int mUid;
    
    final OplusDeepThinkerManager this$0;
    
    TriggerEventRunnable(int param1Int1, int param1Int2, String param1String, Bundle param1Bundle) {
      this.mEventID = param1Int1;
      this.mUid = param1Int2;
      this.mPkg = param1String;
      this.mExtra = param1Bundle;
    }
    
    public void run() {
      OplusDeepThinkerManager.this.triggerHookEvent(this.mEventID, this.mUid, this.mPkg, this.mExtra);
    }
  }
  
  public void run(Runnable paramRunnable) {
    this.mAlgorithmBinderPool.run(paramRunnable);
  }
}
