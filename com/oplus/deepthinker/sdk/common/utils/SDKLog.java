package com.oplus.deepthinker.sdk.common.utils;

import android.os.SystemProperties;
import android.util.Log;
import java.io.BufferedWriter;
import java.io.IOException;

public class SDKLog {
  private static final boolean DEBUG_DETAILS = false;
  
  private static final boolean DEBUG_IN_FILE = false;
  
  public static final boolean IS_QE_LOG_ON = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  static {
    boolean bool = false;
  }
  
  private static final boolean IS_QE_LOG_ON_MTK = SystemProperties.getBoolean("persist.sys.assert.enable", false);
  
  private static final String TAG = "DeepThinkerSDK";
  
  private static boolean isDebugTagOn;
  
  private static boolean mDevelopMode;
  
  private static boolean sSaveLogToFile;
  
  private static BufferedWriter sWriter;
  
  static {
    boolean bool1 = Log.isLoggable("DeepThinkerSDK", 3);
    if (IS_QE_LOG_ON || IS_QE_LOG_ON_MTK || bool1)
      bool = true; 
    mDevelopMode = bool;
  }
  
  public static void openSaveLogToFile() {
    // Byte code:
    //   0: ldc com/oplus/deepthinker/sdk/common/utils/SDKLog
    //   2: monitorenter
    //   3: iconst_0
    //   4: putstatic com/oplus/deepthinker/sdk/common/utils/SDKLog.sSaveLogToFile : Z
    //   7: ldc com/oplus/deepthinker/sdk/common/utils/SDKLog
    //   9: monitorexit
    //   10: return
    //   11: astore_0
    //   12: ldc com/oplus/deepthinker/sdk/common/utils/SDKLog
    //   14: monitorexit
    //   15: aload_0
    //   16: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #50	-> 3
    //   #51	-> 7
    //   #49	-> 11
    // Exception table:
    //   from	to	target	type
    //   3	7	11	finally
  }
  
  public static boolean isDevelopModeOn() {
    return mDevelopMode;
  }
  
  private static void saveLogToFile(String paramString1, String paramString2) {
    if (sSaveLogToFile) {
      BufferedWriter bufferedWriter = sWriter;
      if (bufferedWriter != null)
        try {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append(paramString1);
          stringBuilder.append(": ");
          stringBuilder.append(paramString2);
          stringBuilder.append("\n");
          bufferedWriter.write(stringBuilder.toString());
          sWriter.flush();
        } catch (IOException iOException) {
          iOException.printStackTrace();
        }  
    } 
  }
  
  public static void v(String paramString1, String paramString2) {
    if (IS_QE_LOG_ON) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(": ");
      stringBuilder.append(paramString2);
      Log.v("DeepThinkerSDK", stringBuilder.toString());
      saveLogToFile(paramString1, paramString2);
    } 
  }
  
  public static void v(String paramString) {
    if (IS_QE_LOG_ON) {
      Log.v("DeepThinkerSDK", paramString);
      saveLogToFile("DeepThinkerSDK", paramString);
    } 
  }
  
  public static void d(String paramString1, String paramString2) {
    if (IS_QE_LOG_ON) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(": ");
      stringBuilder.append(paramString2);
      Log.d("DeepThinkerSDK", stringBuilder.toString());
      saveLogToFile(paramString1, paramString2);
    } 
  }
  
  public static void d(String paramString) {
    if (IS_QE_LOG_ON) {
      Log.d("DeepThinkerSDK", paramString);
      saveLogToFile("DeepThinkerSDK", paramString);
    } 
  }
  
  public static void i(String paramString1, String paramString2) {
    if (IS_QE_LOG_ON) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(": ");
      stringBuilder.append(paramString2);
      Log.i("DeepThinkerSDK", stringBuilder.toString());
      saveLogToFile(paramString1, paramString2);
    } 
  }
  
  public static void i(String paramString) {
    if (IS_QE_LOG_ON) {
      Log.i("DeepThinkerSDK", paramString);
      saveLogToFile("DeepThinkerSDK", paramString);
    } 
  }
  
  public static void w(String paramString1, String paramString2) {
    if (IS_QE_LOG_ON) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(": ");
      stringBuilder.append(paramString2);
      Log.w("DeepThinkerSDK", stringBuilder.toString());
      saveLogToFile(paramString1, paramString2);
    } 
  }
  
  public static void w(String paramString) {
    if (IS_QE_LOG_ON) {
      Log.w("DeepThinkerSDK", paramString);
      saveLogToFile("DeepThinkerSDK", paramString);
    } 
  }
  
  public static void e(String paramString1, String paramString2) {
    if (IS_QE_LOG_ON) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(": ");
      stringBuilder.append(paramString2);
      Log.e("DeepThinkerSDK", stringBuilder.toString());
      saveLogToFile(paramString1, paramString2);
    } 
  }
  
  public static void e(String paramString) {
    if (IS_QE_LOG_ON) {
      Log.e("DeepThinkerSDK", paramString);
      saveLogToFile("DeepThinkerSDK", paramString);
    } 
  }
  
  public static void e(String paramString1, String paramString2, Throwable paramThrowable) {
    if (IS_QE_LOG_ON) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(":");
      stringBuilder.append(paramString2);
      Log.e("DeepThinkerSDK", stringBuilder.toString(), paramThrowable);
      saveLogToFile(paramString1, paramString2);
    } 
  }
}
