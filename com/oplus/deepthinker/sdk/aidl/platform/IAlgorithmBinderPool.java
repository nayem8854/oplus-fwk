package com.oplus.deepthinker.sdk.aidl.platform;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAlgorithmBinderPool extends IInterface {
  IBinder queryBinder(int paramInt) throws RemoteException;
  
  class Default implements IAlgorithmBinderPool {
    public IBinder queryBinder(int param1Int) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAlgorithmBinderPool {
    private static final String DESCRIPTOR = "com.oplus.deepthinker.sdk.aidl.platform.IAlgorithmBinderPool";
    
    static final int TRANSACTION_queryBinder = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.deepthinker.sdk.aidl.platform.IAlgorithmBinderPool");
    }
    
    public static IAlgorithmBinderPool asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.deepthinker.sdk.aidl.platform.IAlgorithmBinderPool");
      if (iInterface != null && iInterface instanceof IAlgorithmBinderPool)
        return (IAlgorithmBinderPool)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "queryBinder";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.deepthinker.sdk.aidl.platform.IAlgorithmBinderPool");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.deepthinker.sdk.aidl.platform.IAlgorithmBinderPool");
      param1Int1 = param1Parcel1.readInt();
      IBinder iBinder = queryBinder(param1Int1);
      param1Parcel2.writeNoException();
      param1Parcel2.writeStrongBinder(iBinder);
      return true;
    }
    
    private static class Proxy implements IAlgorithmBinderPool {
      public static IAlgorithmBinderPool sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.deepthinker.sdk.aidl.platform.IAlgorithmBinderPool";
      }
      
      public IBinder queryBinder(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.deepthinker.sdk.aidl.platform.IAlgorithmBinderPool");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IAlgorithmBinderPool.Stub.getDefaultImpl() != null)
            return IAlgorithmBinderPool.Stub.getDefaultImpl().queryBinder(param2Int); 
          parcel2.readException();
          return parcel2.readStrongBinder();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAlgorithmBinderPool param1IAlgorithmBinderPool) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAlgorithmBinderPool != null) {
          Proxy.sDefaultImpl = param1IAlgorithmBinderPool;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAlgorithmBinderPool getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
