package com.oplus.deepthinker.sdk.aidl.platform;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAlgorithmPlatform extends IInterface {
  int getAlgorithmPlatformVersion() throws RemoteException;
  
  class Default implements IAlgorithmPlatform {
    public int getAlgorithmPlatformVersion() throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAlgorithmPlatform {
    private static final String DESCRIPTOR = "com.oplus.deepthinker.sdk.aidl.platform.IAlgorithmPlatform";
    
    static final int TRANSACTION_getAlgorithmPlatformVersion = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.deepthinker.sdk.aidl.platform.IAlgorithmPlatform");
    }
    
    public static IAlgorithmPlatform asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.deepthinker.sdk.aidl.platform.IAlgorithmPlatform");
      if (iInterface != null && iInterface instanceof IAlgorithmPlatform)
        return (IAlgorithmPlatform)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "getAlgorithmPlatformVersion";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.deepthinker.sdk.aidl.platform.IAlgorithmPlatform");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.deepthinker.sdk.aidl.platform.IAlgorithmPlatform");
      param1Int1 = getAlgorithmPlatformVersion();
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(param1Int1);
      return true;
    }
    
    private static class Proxy implements IAlgorithmPlatform {
      public static IAlgorithmPlatform sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.deepthinker.sdk.aidl.platform.IAlgorithmPlatform";
      }
      
      public int getAlgorithmPlatformVersion() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.deepthinker.sdk.aidl.platform.IAlgorithmPlatform");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IAlgorithmPlatform.Stub.getDefaultImpl() != null)
            return IAlgorithmPlatform.Stub.getDefaultImpl().getAlgorithmPlatformVersion(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAlgorithmPlatform param1IAlgorithmPlatform) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAlgorithmPlatform != null) {
          Proxy.sDefaultImpl = param1IAlgorithmPlatform;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAlgorithmPlatform getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
