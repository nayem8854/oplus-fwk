package com.oplus.deepthinker.sdk.aidl.proton.apptype;

import android.os.Parcel;
import android.os.Parcelable;

public class AppInfo implements Parcelable {
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mPkgName);
    paramParcel.writeInt(this.mType);
  }
  
  public AppInfo(String paramString, int paramInt) {
    this.mPkgName = paramString;
    this.mType = paramInt;
  }
  
  protected AppInfo(Parcel paramParcel) {
    this.mPkgName = paramParcel.readString();
    this.mType = paramParcel.readInt();
  }
  
  public String getPkgName() {
    return this.mPkgName;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public static final Parcelable.Creator<AppInfo> CREATOR = new Parcelable.Creator<AppInfo>() {
      public AppInfo createFromParcel(Parcel param1Parcel) {
        return new AppInfo(param1Parcel);
      }
      
      public AppInfo[] newArray(int param1Int) {
        return new AppInfo[param1Int];
      }
    };
  
  private String mPkgName;
  
  private int mType;
}
