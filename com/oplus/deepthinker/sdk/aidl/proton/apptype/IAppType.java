package com.oplus.deepthinker.sdk.aidl.proton.apptype;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface IAppType extends IInterface {
  int getAppType(String paramString) throws RemoteException;
  
  Map getAppTypeMap(List<String> paramList) throws RemoteException;
  
  class Default implements IAppType {
    public int getAppType(String param1String) throws RemoteException {
      return 0;
    }
    
    public Map getAppTypeMap(List<String> param1List) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAppType {
    private static final String DESCRIPTOR = "com.oplus.deepthinker.sdk.aidl.proton.apptype.IAppType";
    
    static final int TRANSACTION_getAppType = 1;
    
    static final int TRANSACTION_getAppTypeMap = 2;
    
    public Stub() {
      attachInterface(this, "com.oplus.deepthinker.sdk.aidl.proton.apptype.IAppType");
    }
    
    public static IAppType asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.deepthinker.sdk.aidl.proton.apptype.IAppType");
      if (iInterface != null && iInterface instanceof IAppType)
        return (IAppType)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "getAppTypeMap";
      } 
      return "getAppType";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      Map map;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("com.oplus.deepthinker.sdk.aidl.proton.apptype.IAppType");
          return true;
        } 
        param1Parcel1.enforceInterface("com.oplus.deepthinker.sdk.aidl.proton.apptype.IAppType");
        ArrayList<String> arrayList = param1Parcel1.createStringArrayList();
        map = getAppTypeMap(arrayList);
        param1Parcel2.writeNoException();
        param1Parcel2.writeMap(map);
        return true;
      } 
      map.enforceInterface("com.oplus.deepthinker.sdk.aidl.proton.apptype.IAppType");
      String str = map.readString();
      param1Int1 = getAppType(str);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(param1Int1);
      return true;
    }
    
    private static class Proxy implements IAppType {
      public static IAppType sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.deepthinker.sdk.aidl.proton.apptype.IAppType";
      }
      
      public int getAppType(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.deepthinker.sdk.aidl.proton.apptype.IAppType");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IAppType.Stub.getDefaultImpl() != null)
            return IAppType.Stub.getDefaultImpl().getAppType(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Map getAppTypeMap(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.deepthinker.sdk.aidl.proton.apptype.IAppType");
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IAppType.Stub.getDefaultImpl() != null)
            return IAppType.Stub.getDefaultImpl().getAppTypeMap(param2List); 
          parcel2.readException();
          ClassLoader classLoader = getClass().getClassLoader();
          return parcel2.readHashMap(classLoader);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAppType param1IAppType) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAppType != null) {
          Proxy.sDefaultImpl = param1IAppType;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAppType getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
