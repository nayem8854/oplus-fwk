package com.oplus.deepthinker.sdk.aidl.proton.userprofile;

import android.net.Uri;

public class UserProfileConstants {
  public static final Uri WIFI_LABEL_URI = Uri.parse("content://com.oplus.deepthinker.provider.feature/profile/wifi_label");
}
