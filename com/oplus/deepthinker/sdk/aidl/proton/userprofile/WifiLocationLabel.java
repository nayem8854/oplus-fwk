package com.oplus.deepthinker.sdk.aidl.proton.userprofile;

import java.util.Set;

public class WifiLocationLabel {
  private double mAccuracy;
  
  private Set<String> mBssidSet;
  
  private int mClusterPointsNum;
  
  private long mExpiration = -1L;
  
  private double mLatitude;
  
  private double mLongitude;
  
  private float mRadius;
  
  private Set<String> mSsidSet;
  
  public WifiLocationLabel(double paramDouble1, double paramDouble2, float paramFloat, Set<String> paramSet1, Set<String> paramSet2) {
    this.mLongitude = paramDouble1;
    this.mLatitude = paramDouble2;
    this.mRadius = paramFloat;
    this.mBssidSet = paramSet1;
    this.mSsidSet = paramSet2;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("WifiLocationLabel{mLongitude=");
    stringBuilder.append(this.mLongitude);
    stringBuilder.append(", mLatitude=");
    stringBuilder.append(this.mLatitude);
    stringBuilder.append(", mRadius=");
    stringBuilder.append(this.mRadius);
    stringBuilder.append(", mClusterPointsNum=");
    stringBuilder.append(this.mClusterPointsNum);
    stringBuilder.append(", mExpiration=");
    stringBuilder.append(this.mExpiration);
    stringBuilder.append(", mAccuracy=");
    stringBuilder.append(this.mAccuracy);
    stringBuilder.append(", mSsidSet=");
    Set<String> set = this.mSsidSet;
    stringBuilder.append(set.toString());
    stringBuilder.append(", mBssidSet=");
    set = this.mBssidSet;
    stringBuilder.append(set.toString());
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public double getLongitude() {
    return this.mLongitude;
  }
  
  public double getLatitude() {
    return this.mLatitude;
  }
  
  public float getRadius() {
    return this.mRadius;
  }
  
  public long getExpiration() {
    return this.mExpiration;
  }
  
  public void setExpiration(long paramLong) {
    this.mExpiration = paramLong;
  }
  
  public int getClusterPointsNum() {
    return this.mClusterPointsNum;
  }
  
  public void setClusterPointsNum(int paramInt) {
    this.mClusterPointsNum = paramInt;
  }
  
  public double getAccuracy() {
    return this.mAccuracy;
  }
  
  public void setAccuracy(double paramDouble) {
    this.mAccuracy = paramDouble;
  }
  
  public Set<String> getBssidSet() {
    return this.mBssidSet;
  }
  
  public Set<String> getSsidSet() {
    return this.mSsidSet;
  }
}
