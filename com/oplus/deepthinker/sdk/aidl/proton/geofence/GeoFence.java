package com.oplus.deepthinker.sdk.aidl.proton.geofence;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;

public class GeoFence implements Parcelable {
  private long mExpiration = -1L;
  
  private int mStatus = 0;
  
  private int mDistanceToCenter = -1;
  
  private Location mCurLocation = null;
  
  protected GeoFence(Parcel paramParcel) {
    this.mLongitude = paramParcel.readDouble();
    this.mLatitude = paramParcel.readDouble();
    this.mRadius = paramParcel.readFloat();
    this.mExpiration = paramParcel.readLong();
    this.mStatus = paramParcel.readInt();
    this.mDistanceToCenter = paramParcel.readInt();
    this.mCurLocation = (Location)paramParcel.readParcelable(Location.class.getClassLoader());
  }
  
  public GeoFence(double paramDouble1, double paramDouble2, float paramFloat) {
    this.mLongitude = paramDouble1;
    this.mLatitude = paramDouble2;
    this.mRadius = paramFloat;
  }
  
  public static final Parcelable.Creator<GeoFence> CREATOR = new Parcelable.Creator<GeoFence>() {
      public GeoFence createFromParcel(Parcel param1Parcel) {
        return new GeoFence(param1Parcel);
      }
      
      public GeoFence[] newArray(int param1Int) {
        return new GeoFence[param1Int];
      }
    };
  
  public static final int DEFAULT_DISTANCE_TO_CENTER = -1;
  
  public static final int GEOFENCE_IN = 1;
  
  public static final int GEOFENCE_OUT = 2;
  
  public static final int GEOFENCE_UNKNOWN = 0;
  
  private static final String TAG = "GeoFence";
  
  private final double mLatitude;
  
  private final double mLongitude;
  
  private final float mRadius;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeDouble(this.mLongitude);
    paramParcel.writeDouble(this.mLatitude);
    paramParcel.writeFloat(this.mRadius);
    paramParcel.writeLong(this.mExpiration);
    paramParcel.writeInt(this.mStatus);
    paramParcel.writeInt(this.mDistanceToCenter);
    paramParcel.writeParcelable((Parcelable)this.mCurLocation, paramInt);
  }
  
  public boolean isExpired() {
    long l = this.mExpiration;
    boolean bool = false;
    if (l == -1L)
      return false; 
    if (l > SystemClock.elapsedRealtime())
      bool = true; 
    return bool;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("mLongitute: ");
    stringBuilder.append(this.mLongitude);
    stringBuilder.append(" , mLatitude: ");
    stringBuilder.append(this.mLatitude);
    stringBuilder.append(" , mRadius: ");
    stringBuilder.append(this.mRadius);
    stringBuilder.append(" , mExpiration: ");
    stringBuilder.append(this.mExpiration);
    stringBuilder.append(" , mStatus: ");
    stringBuilder.append(this.mStatus);
    stringBuilder.append(" , mDistanceToCenter: ");
    stringBuilder.append(this.mDistanceToCenter);
    return stringBuilder.toString();
  }
  
  public double getLongitude() {
    return this.mLongitude;
  }
  
  public double getLatitude() {
    return this.mLatitude;
  }
  
  public float getRadius() {
    return this.mRadius;
  }
  
  public void setExpiration(long paramLong) {
    this.mExpiration = paramLong;
  }
  
  public long getExpiration() {
    return this.mExpiration;
  }
  
  public void setStatus(int paramInt) {
    this.mStatus = paramInt;
  }
  
  public int getStatus() {
    return this.mStatus;
  }
  
  public void setDistanceToCenter(int paramInt) {
    this.mDistanceToCenter = paramInt;
  }
  
  public int getDistanceToCenter() {
    return this.mDistanceToCenter;
  }
  
  public void setLocation(Location paramLocation) {
    this.mCurLocation = paramLocation;
  }
  
  public Location getLocation() {
    return this.mCurLocation;
  }
}
