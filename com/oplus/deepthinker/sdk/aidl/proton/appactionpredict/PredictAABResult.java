package com.oplus.deepthinker.sdk.aidl.proton.appactionpredict;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class PredictAABResult implements Parcelable {
  public PredictAABResult(ArrayList<String> paramArrayList1, ArrayList<String> paramArrayList2, ArrayList<String> paramArrayList3, ArrayList<String> paramArrayList4) {
    this.mPredictResultMap = new HashMap<>();
    putArrayToMap(paramArrayList1, 10);
    putArrayToMap(paramArrayList2, 20);
    putArrayToMap(paramArrayList3, 30);
    putArrayToMap(paramArrayList4, 40);
  }
  
  private PredictAABResult() {}
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeMap(this.mPredictResultMap);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public Set<String> getPackages() {
    HashMap hashMap = this.mPredictResultMap;
    if (hashMap == null)
      return null; 
    return hashMap.keySet();
  }
  
  public int getPredictAppStandbyBucket(String paramString) {
    paramString = (String)this.mPredictResultMap.get(paramString);
    if (paramString instanceof Integer)
      return ((Integer)paramString).intValue(); 
    return -1;
  }
  
  public static final Parcelable.Creator<PredictAABResult> CREATOR = new Parcelable.Creator<PredictAABResult>() {
      public PredictAABResult createFromParcel(Parcel param1Parcel) {
        PredictAABResult predictAABResult = new PredictAABResult();
        PredictAABResult.access$102(predictAABResult, param1Parcel.readHashMap(PredictAABResult.class.getClassLoader()));
        return predictAABResult;
      }
      
      public PredictAABResult[] newArray(int param1Int) {
        return new PredictAABResult[param1Int];
      }
    };
  
  public static final int INVALID_BUCKET = -1;
  
  private static final String TAG = "PredictAABResult";
  
  private HashMap mPredictResultMap;
  
  private void putArrayToMap(ArrayList<String> paramArrayList, int paramInt) {
    for (String str : paramArrayList)
      this.mPredictResultMap.put(str, Integer.valueOf(paramInt)); 
  }
}
