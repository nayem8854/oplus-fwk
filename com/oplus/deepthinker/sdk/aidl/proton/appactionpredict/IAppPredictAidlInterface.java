package com.oplus.deepthinker.sdk.aidl.proton.appactionpredict;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IAppPredictAidlInterface extends IInterface {
  PredictResult getAppPredictResult(String paramString) throws RemoteException;
  
  List<PredictResult> getAppPredictResultMap(String paramString) throws RemoteException;
  
  PredictAABResult getPredictAABResult() throws RemoteException;
  
  class Default implements IAppPredictAidlInterface {
    public PredictAABResult getPredictAABResult() throws RemoteException {
      return null;
    }
    
    public List<PredictResult> getAppPredictResultMap(String param1String) throws RemoteException {
      return null;
    }
    
    public PredictResult getAppPredictResult(String param1String) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAppPredictAidlInterface {
    private static final String DESCRIPTOR = "com.oplus.deepthinker.sdk.aidl.proton.appactionpredict.IAppPredictAidlInterface";
    
    static final int TRANSACTION_getAppPredictResult = 3;
    
    static final int TRANSACTION_getAppPredictResultMap = 2;
    
    static final int TRANSACTION_getPredictAABResult = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.deepthinker.sdk.aidl.proton.appactionpredict.IAppPredictAidlInterface");
    }
    
    public static IAppPredictAidlInterface asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.deepthinker.sdk.aidl.proton.appactionpredict.IAppPredictAidlInterface");
      if (iInterface != null && iInterface instanceof IAppPredictAidlInterface)
        return (IAppPredictAidlInterface)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "getAppPredictResult";
        } 
        return "getAppPredictResultMap";
      } 
      return "getPredictAABResult";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      List<PredictResult> list;
      if (param1Int1 != 1) {
        PredictResult predictResult;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.oplus.deepthinker.sdk.aidl.proton.appactionpredict.IAppPredictAidlInterface");
            return true;
          } 
          param1Parcel1.enforceInterface("com.oplus.deepthinker.sdk.aidl.proton.appactionpredict.IAppPredictAidlInterface");
          String str1 = param1Parcel1.readString();
          predictResult = getAppPredictResult(str1);
          param1Parcel2.writeNoException();
          if (predictResult != null) {
            param1Parcel2.writeInt(1);
            predictResult.writeToParcel(param1Parcel2, 1);
          } else {
            param1Parcel2.writeInt(0);
          } 
          return true;
        } 
        predictResult.enforceInterface("com.oplus.deepthinker.sdk.aidl.proton.appactionpredict.IAppPredictAidlInterface");
        String str = predictResult.readString();
        list = getAppPredictResultMap(str);
        param1Parcel2.writeNoException();
        param1Parcel2.writeTypedList(list);
        return true;
      } 
      list.enforceInterface("com.oplus.deepthinker.sdk.aidl.proton.appactionpredict.IAppPredictAidlInterface");
      PredictAABResult predictAABResult = getPredictAABResult();
      param1Parcel2.writeNoException();
      if (predictAABResult != null) {
        param1Parcel2.writeInt(1);
        predictAABResult.writeToParcel(param1Parcel2, 1);
      } else {
        param1Parcel2.writeInt(0);
      } 
      return true;
    }
    
    private static class Proxy implements IAppPredictAidlInterface {
      public static IAppPredictAidlInterface sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.deepthinker.sdk.aidl.proton.appactionpredict.IAppPredictAidlInterface";
      }
      
      public PredictAABResult getPredictAABResult() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          PredictAABResult predictAABResult;
          parcel1.writeInterfaceToken("com.oplus.deepthinker.sdk.aidl.proton.appactionpredict.IAppPredictAidlInterface");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IAppPredictAidlInterface.Stub.getDefaultImpl() != null) {
            predictAABResult = IAppPredictAidlInterface.Stub.getDefaultImpl().getPredictAABResult();
            return predictAABResult;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            predictAABResult = (PredictAABResult)PredictAABResult.CREATOR.createFromParcel(parcel2);
          } else {
            predictAABResult = null;
          } 
          return predictAABResult;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<PredictResult> getAppPredictResultMap(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.deepthinker.sdk.aidl.proton.appactionpredict.IAppPredictAidlInterface");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IAppPredictAidlInterface.Stub.getDefaultImpl() != null)
            return IAppPredictAidlInterface.Stub.getDefaultImpl().getAppPredictResultMap(param2String); 
          parcel2.readException();
          return parcel2.createTypedArrayList(PredictResult.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PredictResult getAppPredictResult(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.deepthinker.sdk.aidl.proton.appactionpredict.IAppPredictAidlInterface");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IAppPredictAidlInterface.Stub.getDefaultImpl() != null)
            return IAppPredictAidlInterface.Stub.getDefaultImpl().getAppPredictResult(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            PredictResult predictResult = (PredictResult)PredictResult.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (PredictResult)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAppPredictAidlInterface param1IAppPredictAidlInterface) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAppPredictAidlInterface != null) {
          Proxy.sDefaultImpl = param1IAppPredictAidlInterface;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAppPredictAidlInterface getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
