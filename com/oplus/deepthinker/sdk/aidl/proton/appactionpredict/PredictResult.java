package com.oplus.deepthinker.sdk.aidl.proton.appactionpredict;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Map;
import java.util.Set;

public class PredictResult implements Parcelable {
  private PredictResult() {}
  
  public PredictResult(int paramInt, Map paramMap) {
    this.mPredictTime = paramInt;
    this.mPredictResultMap = paramMap;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mPredictTime);
    paramParcel.writeMap(this.mPredictResultMap);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public Set<String> getPackages() {
    Map map = this.mPredictResultMap;
    if (map == null)
      return null; 
    return map.keySet();
  }
  
  public int getCountdownTimeByPackage(String paramString) {
    paramString = (String)this.mPredictResultMap.get(paramString);
    if (paramString instanceof String)
      return Integer.parseInt(paramString); 
    return -1;
  }
  
  public int getPredictTime() {
    return this.mPredictTime;
  }
  
  public static final Parcelable.Creator<PredictResult> CREATOR = new Parcelable.Creator<PredictResult>() {
      public PredictResult createFromParcel(Parcel param1Parcel) {
        PredictResult predictResult = new PredictResult();
        PredictResult.access$102(predictResult, param1Parcel.readInt());
        PredictResult.access$202(predictResult, param1Parcel.readHashMap(PredictResult.class.getClassLoader()));
        return predictResult;
      }
      
      public PredictResult[] newArray(int param1Int) {
        return new PredictResult[param1Int];
      }
    };
  
  public static final int INVALID_TIME = -1;
  
  private static final String TAG = "PredictResult";
  
  private Map mPredictResultMap;
  
  private int mPredictTime;
}
