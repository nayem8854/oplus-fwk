package com.oplus.deepthinker.sdk.aidl.proton.smartgps;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface ISmartGpsAidlInterface extends IInterface {
  List<String> getSmartGpsBssidList() throws RemoteException;
  
  class Default implements ISmartGpsAidlInterface {
    public List<String> getSmartGpsBssidList() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISmartGpsAidlInterface {
    private static final String DESCRIPTOR = "com.oplus.deepthinker.sdk.aidl.proton.smartgps.ISmartGpsAidlInterface";
    
    static final int TRANSACTION_getSmartGpsBssidList = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.deepthinker.sdk.aidl.proton.smartgps.ISmartGpsAidlInterface");
    }
    
    public static ISmartGpsAidlInterface asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.deepthinker.sdk.aidl.proton.smartgps.ISmartGpsAidlInterface");
      if (iInterface != null && iInterface instanceof ISmartGpsAidlInterface)
        return (ISmartGpsAidlInterface)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "getSmartGpsBssidList";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.deepthinker.sdk.aidl.proton.smartgps.ISmartGpsAidlInterface");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.deepthinker.sdk.aidl.proton.smartgps.ISmartGpsAidlInterface");
      List<String> list = getSmartGpsBssidList();
      param1Parcel2.writeNoException();
      param1Parcel2.writeStringList(list);
      return true;
    }
    
    private static class Proxy implements ISmartGpsAidlInterface {
      public static ISmartGpsAidlInterface sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.deepthinker.sdk.aidl.proton.smartgps.ISmartGpsAidlInterface";
      }
      
      public List<String> getSmartGpsBssidList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.deepthinker.sdk.aidl.proton.smartgps.ISmartGpsAidlInterface");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ISmartGpsAidlInterface.Stub.getDefaultImpl() != null)
            return ISmartGpsAidlInterface.Stub.getDefaultImpl().getSmartGpsBssidList(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISmartGpsAidlInterface param1ISmartGpsAidlInterface) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISmartGpsAidlInterface != null) {
          Proxy.sDefaultImpl = param1ISmartGpsAidlInterface;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISmartGpsAidlInterface getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
