package com.oplus.deepthinker.sdk.aidl.proton.appsort;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IAppSort extends IInterface {
  List<String> getAppQueueSortedByComplex() throws RemoteException;
  
  List<String> getAppQueueSortedByCount() throws RemoteException;
  
  List<String> getAppQueueSortedByTime() throws RemoteException;
  
  class Default implements IAppSort {
    public List<String> getAppQueueSortedByTime() throws RemoteException {
      return null;
    }
    
    public List<String> getAppQueueSortedByCount() throws RemoteException {
      return null;
    }
    
    public List<String> getAppQueueSortedByComplex() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAppSort {
    private static final String DESCRIPTOR = "com.oplus.deepthinker.sdk.aidl.proton.appsort.IAppSort";
    
    static final int TRANSACTION_getAppQueueSortedByComplex = 3;
    
    static final int TRANSACTION_getAppQueueSortedByCount = 2;
    
    static final int TRANSACTION_getAppQueueSortedByTime = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.deepthinker.sdk.aidl.proton.appsort.IAppSort");
    }
    
    public static IAppSort asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.deepthinker.sdk.aidl.proton.appsort.IAppSort");
      if (iInterface != null && iInterface instanceof IAppSort)
        return (IAppSort)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "getAppQueueSortedByComplex";
        } 
        return "getAppQueueSortedByCount";
      } 
      return "getAppQueueSortedByTime";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.oplus.deepthinker.sdk.aidl.proton.appsort.IAppSort");
            return true;
          } 
          param1Parcel1.enforceInterface("com.oplus.deepthinker.sdk.aidl.proton.appsort.IAppSort");
          list = getAppQueueSortedByComplex();
          param1Parcel2.writeNoException();
          param1Parcel2.writeStringList(list);
          return true;
        } 
        list.enforceInterface("com.oplus.deepthinker.sdk.aidl.proton.appsort.IAppSort");
        list = getAppQueueSortedByCount();
        param1Parcel2.writeNoException();
        param1Parcel2.writeStringList(list);
        return true;
      } 
      list.enforceInterface("com.oplus.deepthinker.sdk.aidl.proton.appsort.IAppSort");
      List<String> list = getAppQueueSortedByTime();
      param1Parcel2.writeNoException();
      param1Parcel2.writeStringList(list);
      return true;
    }
    
    private static class Proxy implements IAppSort {
      public static IAppSort sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.deepthinker.sdk.aidl.proton.appsort.IAppSort";
      }
      
      public List<String> getAppQueueSortedByTime() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.deepthinker.sdk.aidl.proton.appsort.IAppSort");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IAppSort.Stub.getDefaultImpl() != null)
            return IAppSort.Stub.getDefaultImpl().getAppQueueSortedByTime(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getAppQueueSortedByCount() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.deepthinker.sdk.aidl.proton.appsort.IAppSort");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IAppSort.Stub.getDefaultImpl() != null)
            return IAppSort.Stub.getDefaultImpl().getAppQueueSortedByCount(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getAppQueueSortedByComplex() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.deepthinker.sdk.aidl.proton.appsort.IAppSort");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IAppSort.Stub.getDefaultImpl() != null)
            return IAppSort.Stub.getDefaultImpl().getAppQueueSortedByComplex(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAppSort param1IAppSort) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAppSort != null) {
          Proxy.sDefaultImpl = param1IAppSort;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAppSort getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
