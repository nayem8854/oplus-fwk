package com.oplus.deepthinker.sdk.aidl.proton.deepsleep;

import android.os.Parcel;
import android.os.Parcelable;

public class TotalPredictResult implements Parcelable {
  private DeepSleepCluster mSleepCluster = null;
  
  private DeepSleepCluster mWakeCluster = null;
  
  private TrainConfig mOptimalSleepConfig = null;
  
  private TrainConfig mOptimalWakeConfig = null;
  
  public TotalPredictResult(DeepSleepCluster paramDeepSleepCluster1, DeepSleepCluster paramDeepSleepCluster2) {
    this.mSleepCluster = paramDeepSleepCluster1;
    this.mWakeCluster = paramDeepSleepCluster2;
  }
  
  public TotalPredictResult(DeepSleepCluster paramDeepSleepCluster1, DeepSleepCluster paramDeepSleepCluster2, TrainConfig paramTrainConfig1, TrainConfig paramTrainConfig2) {
    this.mSleepCluster = paramDeepSleepCluster1;
    this.mWakeCluster = paramDeepSleepCluster2;
    this.mOptimalSleepConfig = paramTrainConfig1;
    this.mOptimalWakeConfig = paramTrainConfig2;
  }
  
  public DeepSleepCluster getSleepCluster() {
    return this.mSleepCluster;
  }
  
  public void setSleepCluster(DeepSleepCluster paramDeepSleepCluster) {
    this.mSleepCluster = paramDeepSleepCluster;
  }
  
  public DeepSleepCluster getWakeCluster() {
    return this.mWakeCluster;
  }
  
  public void setWakeCluster(DeepSleepCluster paramDeepSleepCluster) {
    this.mWakeCluster = paramDeepSleepCluster;
  }
  
  public void setOptimalSleepConfig(TrainConfig paramTrainConfig) {
    this.mOptimalSleepConfig = paramTrainConfig;
  }
  
  public void setOptimalWakeConfig(TrainConfig paramTrainConfig) {
    this.mOptimalWakeConfig = paramTrainConfig;
  }
  
  public TrainConfig getOptimalSleepConfig() {
    return this.mOptimalSleepConfig;
  }
  
  public TrainConfig getOptimalWakeConfig() {
    return this.mOptimalWakeConfig;
  }
  
  public String toString() {
    String str1, str3, str4;
    DeepSleepCluster deepSleepCluster1 = this.mSleepCluster;
    String str2 = "null";
    if (deepSleepCluster1 != null) {
      str1 = deepSleepCluster1.toString();
    } else {
      str1 = "null";
    } 
    DeepSleepCluster deepSleepCluster2 = this.mWakeCluster;
    if (deepSleepCluster2 != null) {
      str3 = deepSleepCluster2.toString();
    } else {
      str3 = "null";
    } 
    TrainConfig trainConfig1 = this.mOptimalSleepConfig;
    if (trainConfig1 != null) {
      str4 = trainConfig1.toString();
    } else {
      str4 = "null";
    } 
    TrainConfig trainConfig2 = this.mOptimalWakeConfig;
    if (trainConfig2 != null)
      str2 = trainConfig2.toString(); 
    return String.format("mSleepCluster=%s,mSleepConfig=%s,mWakeCluster=%s,mWakeConfig=%s", new Object[] { str1, str4, str3, str2 });
  }
  
  public static final Parcelable.Creator<TotalPredictResult> CREATOR = new Parcelable.Creator<TotalPredictResult>() {
      public TotalPredictResult createFromParcel(Parcel param1Parcel) {
        TotalPredictResult totalPredictResult = new TotalPredictResult(null, null, null, null);
        totalPredictResult.setSleepCluster((DeepSleepCluster)param1Parcel.readParcelable(getClass().getClassLoader()));
        totalPredictResult.setWakeCluster((DeepSleepCluster)param1Parcel.readParcelable(getClass().getClassLoader()));
        totalPredictResult.setOptimalSleepConfig((TrainConfig)param1Parcel.readParcelable(getClass().getClassLoader()));
        totalPredictResult.setOptimalWakeConfig((TrainConfig)param1Parcel.readParcelable(getClass().getClassLoader()));
        return totalPredictResult;
      }
      
      public TotalPredictResult[] newArray(int param1Int) {
        return new TotalPredictResult[param1Int];
      }
    };
  
  private static final String NULL = "null";
  
  private static final String TAG = "TotalPredictResult";
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.mSleepCluster, paramInt);
    paramParcel.writeParcelable(this.mWakeCluster, paramInt);
    paramParcel.writeParcelable(this.mOptimalSleepConfig, paramInt);
    paramParcel.writeParcelable(this.mOptimalWakeConfig, paramInt);
  }
}
