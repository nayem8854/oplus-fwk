package com.oplus.deepthinker.sdk.aidl.proton.deepsleep;

import android.os.Parcel;
import android.os.Parcelable;

public class SleepRecord implements Parcelable {
  private long mSleepTime = 0L;
  
  private long mWakeTime = 0L;
  
  public SleepRecord(long paramLong1, long paramLong2) {
    this.mSleepTime = paramLong1;
    this.mWakeTime = paramLong2;
  }
  
  public long getSleepTime() {
    return this.mSleepTime;
  }
  
  public long getWakeTime() {
    return this.mWakeTime;
  }
  
  public void setSleepTime(long paramLong) {
    this.mSleepTime = paramLong;
  }
  
  public void setWakeTime(long paramLong) {
    this.mWakeTime = paramLong;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mSleepTime);
    paramParcel.writeLong(this.mWakeTime);
  }
  
  public static final Parcelable.Creator<SleepRecord> CREATOR = new Parcelable.Creator<SleepRecord>() {
      public SleepRecord createFromParcel(Parcel param1Parcel) {
        SleepRecord sleepRecord = new SleepRecord(0L, 0L);
        SleepRecord.access$002(sleepRecord, param1Parcel.readLong());
        SleepRecord.access$102(sleepRecord, param1Parcel.readLong());
        return sleepRecord;
      }
      
      public SleepRecord[] newArray(int param1Int) {
        return new SleepRecord[param1Int];
      }
    };
}
