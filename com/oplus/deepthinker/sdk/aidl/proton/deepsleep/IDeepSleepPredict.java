package com.oplus.deepthinker.sdk.aidl.proton.deepsleep;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IDeepSleepPredict extends IInterface {
  DeepSleepPredictResult getDeepSleepPredictResult() throws RemoteException;
  
  TotalPredictResult getDeepSleepTotalPredictResult() throws RemoteException;
  
  SleepRecord getLastDeepSleepRecord() throws RemoteException;
  
  DeepSleepPredictResult getPredictResultWithFeedBack() throws RemoteException;
  
  class Default implements IDeepSleepPredict {
    public DeepSleepPredictResult getDeepSleepPredictResult() throws RemoteException {
      return null;
    }
    
    public SleepRecord getLastDeepSleepRecord() throws RemoteException {
      return null;
    }
    
    public TotalPredictResult getDeepSleepTotalPredictResult() throws RemoteException {
      return null;
    }
    
    public DeepSleepPredictResult getPredictResultWithFeedBack() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDeepSleepPredict {
    private static final String DESCRIPTOR = "com.oplus.deepthinker.sdk.aidl.proton.deepsleep.IDeepSleepPredict";
    
    static final int TRANSACTION_getDeepSleepPredictResult = 1;
    
    static final int TRANSACTION_getDeepSleepTotalPredictResult = 3;
    
    static final int TRANSACTION_getLastDeepSleepRecord = 2;
    
    static final int TRANSACTION_getPredictResultWithFeedBack = 4;
    
    public Stub() {
      attachInterface(this, "com.oplus.deepthinker.sdk.aidl.proton.deepsleep.IDeepSleepPredict");
    }
    
    public static IDeepSleepPredict asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.deepthinker.sdk.aidl.proton.deepsleep.IDeepSleepPredict");
      if (iInterface != null && iInterface instanceof IDeepSleepPredict)
        return (IDeepSleepPredict)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "getPredictResultWithFeedBack";
          } 
          return "getDeepSleepTotalPredictResult";
        } 
        return "getLastDeepSleepRecord";
      } 
      return "getDeepSleepPredictResult";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      SleepRecord sleepRecord;
      if (param1Int1 != 1) {
        TotalPredictResult totalPredictResult;
        if (param1Int1 != 2) {
          DeepSleepPredictResult deepSleepPredictResult1;
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("com.oplus.deepthinker.sdk.aidl.proton.deepsleep.IDeepSleepPredict");
              return true;
            } 
            param1Parcel1.enforceInterface("com.oplus.deepthinker.sdk.aidl.proton.deepsleep.IDeepSleepPredict");
            deepSleepPredictResult1 = getPredictResultWithFeedBack();
            param1Parcel2.writeNoException();
            if (deepSleepPredictResult1 != null) {
              param1Parcel2.writeInt(1);
              deepSleepPredictResult1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          } 
          deepSleepPredictResult1.enforceInterface("com.oplus.deepthinker.sdk.aidl.proton.deepsleep.IDeepSleepPredict");
          totalPredictResult = getDeepSleepTotalPredictResult();
          param1Parcel2.writeNoException();
          if (totalPredictResult != null) {
            param1Parcel2.writeInt(1);
            totalPredictResult.writeToParcel(param1Parcel2, 1);
          } else {
            param1Parcel2.writeInt(0);
          } 
          return true;
        } 
        totalPredictResult.enforceInterface("com.oplus.deepthinker.sdk.aidl.proton.deepsleep.IDeepSleepPredict");
        sleepRecord = getLastDeepSleepRecord();
        param1Parcel2.writeNoException();
        if (sleepRecord != null) {
          param1Parcel2.writeInt(1);
          sleepRecord.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      sleepRecord.enforceInterface("com.oplus.deepthinker.sdk.aidl.proton.deepsleep.IDeepSleepPredict");
      DeepSleepPredictResult deepSleepPredictResult = getDeepSleepPredictResult();
      param1Parcel2.writeNoException();
      if (deepSleepPredictResult != null) {
        param1Parcel2.writeInt(1);
        deepSleepPredictResult.writeToParcel(param1Parcel2, 1);
      } else {
        param1Parcel2.writeInt(0);
      } 
      return true;
    }
    
    private static class Proxy implements IDeepSleepPredict {
      public static IDeepSleepPredict sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.deepthinker.sdk.aidl.proton.deepsleep.IDeepSleepPredict";
      }
      
      public DeepSleepPredictResult getDeepSleepPredictResult() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          DeepSleepPredictResult deepSleepPredictResult;
          parcel1.writeInterfaceToken("com.oplus.deepthinker.sdk.aidl.proton.deepsleep.IDeepSleepPredict");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IDeepSleepPredict.Stub.getDefaultImpl() != null) {
            deepSleepPredictResult = IDeepSleepPredict.Stub.getDefaultImpl().getDeepSleepPredictResult();
            return deepSleepPredictResult;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            deepSleepPredictResult = (DeepSleepPredictResult)DeepSleepPredictResult.CREATOR.createFromParcel(parcel2);
          } else {
            deepSleepPredictResult = null;
          } 
          return deepSleepPredictResult;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public SleepRecord getLastDeepSleepRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          SleepRecord sleepRecord;
          parcel1.writeInterfaceToken("com.oplus.deepthinker.sdk.aidl.proton.deepsleep.IDeepSleepPredict");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IDeepSleepPredict.Stub.getDefaultImpl() != null) {
            sleepRecord = IDeepSleepPredict.Stub.getDefaultImpl().getLastDeepSleepRecord();
            return sleepRecord;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            sleepRecord = (SleepRecord)SleepRecord.CREATOR.createFromParcel(parcel2);
          } else {
            sleepRecord = null;
          } 
          return sleepRecord;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public TotalPredictResult getDeepSleepTotalPredictResult() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          TotalPredictResult totalPredictResult;
          parcel1.writeInterfaceToken("com.oplus.deepthinker.sdk.aidl.proton.deepsleep.IDeepSleepPredict");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IDeepSleepPredict.Stub.getDefaultImpl() != null) {
            totalPredictResult = IDeepSleepPredict.Stub.getDefaultImpl().getDeepSleepTotalPredictResult();
            return totalPredictResult;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            totalPredictResult = (TotalPredictResult)TotalPredictResult.CREATOR.createFromParcel(parcel2);
          } else {
            totalPredictResult = null;
          } 
          return totalPredictResult;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public DeepSleepPredictResult getPredictResultWithFeedBack() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          DeepSleepPredictResult deepSleepPredictResult;
          parcel1.writeInterfaceToken("com.oplus.deepthinker.sdk.aidl.proton.deepsleep.IDeepSleepPredict");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IDeepSleepPredict.Stub.getDefaultImpl() != null) {
            deepSleepPredictResult = IDeepSleepPredict.Stub.getDefaultImpl().getPredictResultWithFeedBack();
            return deepSleepPredictResult;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            deepSleepPredictResult = (DeepSleepPredictResult)DeepSleepPredictResult.CREATOR.createFromParcel(parcel2);
          } else {
            deepSleepPredictResult = null;
          } 
          return deepSleepPredictResult;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDeepSleepPredict param1IDeepSleepPredict) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDeepSleepPredict != null) {
          Proxy.sDefaultImpl = param1IDeepSleepPredict;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDeepSleepPredict getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
