package com.oplus.deepthinker.sdk.aidl.proton.deepsleep;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public class DeepSleepPredictResult implements Parcelable {
  private PredictResultType mResultType = PredictResultType.PREDICT_RESULT_TYPE_UNKNOWN;
  
  private List<DeepSleepCluster> mDeepSleepClusterList = null;
  
  private static final String TAG = "DeepSleepPredictResult";
  
  public DeepSleepPredictResult(PredictResultType paramPredictResultType, List<DeepSleepCluster> paramList) {
    this.mResultType = paramPredictResultType;
    this.mDeepSleepClusterList = paramList;
  }
  
  public PredictResultType getResultType() {
    return this.mResultType;
  }
  
  public List<DeepSleepCluster> getDeepSleepClusterList() {
    return this.mDeepSleepClusterList;
  }
  
  public void setResultType(PredictResultType paramPredictResultType) {
    this.mResultType = paramPredictResultType;
  }
  
  public void setClusterList(List<DeepSleepCluster> paramList) {
    this.mDeepSleepClusterList = paramList;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("DeepSleepPredictResult:resultType=");
    stringBuilder.append(this.mResultType);
    stringBuilder = new StringBuilder(stringBuilder.toString());
    List<DeepSleepCluster> list = this.mDeepSleepClusterList;
    if (list != null && list.size() > 0)
      for (DeepSleepCluster deepSleepCluster : this.mDeepSleepClusterList)
        stringBuilder.append(deepSleepCluster.toString());  
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    String str;
    PredictResultType predictResultType = this.mResultType;
    if (predictResultType == null) {
      predictResultType = null;
    } else {
      str = predictResultType.name();
    } 
    paramParcel.writeString(str);
    paramParcel.writeTypedList(this.mDeepSleepClusterList);
  }
  
  public static final Parcelable.Creator<DeepSleepPredictResult> CREATOR = new Parcelable.Creator<DeepSleepPredictResult>() {
      public DeepSleepPredictResult createFromParcel(Parcel param1Parcel) {
        DeepSleepPredictResult deepSleepPredictResult = new DeepSleepPredictResult(null, null);
        String str = param1Parcel.readString();
        if (str != null)
          DeepSleepPredictResult.access$002(deepSleepPredictResult, PredictResultType.valueOf(str)); 
        if (deepSleepPredictResult.mDeepSleepClusterList == null)
          DeepSleepPredictResult.access$102(deepSleepPredictResult, new ArrayList()); 
        param1Parcel.readTypedList(deepSleepPredictResult.mDeepSleepClusterList, DeepSleepCluster.CREATOR);
        return deepSleepPredictResult;
      }
      
      public DeepSleepPredictResult[] newArray(int param1Int) {
        return new DeepSleepPredictResult[param1Int];
      }
    };
}
