package com.oplus.deepthinker.sdk.aidl.proton.deepsleep;

import android.os.Parcel;
import android.os.Parcelable;
import com.oplus.deepthinker.sdk.common.utils.SDKLog;

public class DeepSleepCluster implements Parcelable, Cloneable {
  private double mSleepTimePeriod = 0.0D;
  
  private double mWakeTimePeriod = 0.0D;
  
  private double mMaxDistance = 0.0D;
  
  private double mSleepMinValue = 0.0D;
  
  private double mSleepMaxValue = 0.0D;
  
  private double mWakeMinValue = 0.0D;
  
  private double mWakeMaxValue = 0.0D;
  
  private int mClusterId = -1;
  
  private int mClusterNum = 0;
  
  public DeepSleepCluster(double paramDouble1, double paramDouble2) {
    this.mSleepTimePeriod = paramDouble1;
    this.mWakeTimePeriod = paramDouble2;
    this.mMaxDistance = 0.0D;
  }
  
  public DeepSleepCluster(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6) {
    this.mSleepTimePeriod = paramDouble1;
    this.mWakeTimePeriod = paramDouble2;
    this.mMaxDistance = 0.0D;
    this.mSleepMinValue = paramDouble3;
    this.mSleepMaxValue = paramDouble4;
    this.mWakeMinValue = paramDouble5;
    this.mWakeMaxValue = paramDouble6;
  }
  
  public DeepSleepCluster(double paramDouble1, double paramDouble2, double paramDouble3) {
    this.mSleepTimePeriod = paramDouble1;
    this.mWakeTimePeriod = paramDouble2;
    this.mMaxDistance = paramDouble3;
  }
  
  public DeepSleepCluster(int paramInt, double paramDouble1, double paramDouble2, double paramDouble3) {
    this.mSleepTimePeriod = paramDouble1;
    this.mWakeTimePeriod = paramDouble2;
    this.mMaxDistance = paramDouble3;
    this.mClusterId = paramInt;
  }
  
  public double getSleepTimePeriod() {
    return this.mSleepTimePeriod;
  }
  
  public double getWakeTimePeriod() {
    return this.mWakeTimePeriod;
  }
  
  public double getMaxDistance() {
    return this.mMaxDistance;
  }
  
  public int getClusterId() {
    return this.mClusterId;
  }
  
  public int getClusterNum() {
    return this.mClusterNum;
  }
  
  public void setClusterId(int paramInt) {
    this.mClusterId = paramInt;
  }
  
  public void setMaxDistance(double paramDouble) {
    this.mMaxDistance = paramDouble;
  }
  
  public void setClusterNum(int paramInt) {
    this.mClusterNum = paramInt;
  }
  
  public void setSleepTimePeriod(double paramDouble) {
    this.mSleepTimePeriod = paramDouble;
  }
  
  public void setWakeTimePeriod(double paramDouble) {
    this.mWakeTimePeriod = paramDouble;
  }
  
  public double getSleepMinValue() {
    return this.mSleepMinValue;
  }
  
  public double getSleepMaxValue() {
    return this.mSleepMaxValue;
  }
  
  public double getWakeMinValue() {
    return this.mWakeMinValue;
  }
  
  public double getWakeMaxValue() {
    return this.mWakeMaxValue;
  }
  
  public String toString() {
    int i = this.mClusterId;
    double d1 = this.mSleepTimePeriod, d2 = this.mWakeTimePeriod;
    int j = this.mClusterNum;
    double d3 = this.mMaxDistance;
    return String.format("DeepSleepCluster:clusterId=%d sleep=%.2f wake=%.2f clusterNum=%d maxDistance=%.2f", new Object[] { Integer.valueOf(i), Double.valueOf(d1), Double.valueOf(d2), Integer.valueOf(j), Double.valueOf(d3) });
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeDouble(this.mSleepTimePeriod);
    paramParcel.writeDouble(this.mWakeTimePeriod);
    paramParcel.writeDouble(this.mMaxDistance);
    paramParcel.writeInt(this.mClusterId);
    paramParcel.writeInt(this.mClusterNum);
    paramParcel.writeDouble(this.mSleepMinValue);
    paramParcel.writeDouble(this.mSleepMaxValue);
    paramParcel.writeDouble(this.mWakeMinValue);
    paramParcel.writeDouble(this.mWakeMaxValue);
  }
  
  public static final Parcelable.Creator<DeepSleepCluster> CREATOR = new Parcelable.Creator<DeepSleepCluster>() {
      public DeepSleepCluster createFromParcel(Parcel param1Parcel) {
        DeepSleepCluster deepSleepCluster = new DeepSleepCluster(0.0D, 0.0D);
        DeepSleepCluster.access$002(deepSleepCluster, param1Parcel.readDouble());
        DeepSleepCluster.access$102(deepSleepCluster, param1Parcel.readDouble());
        DeepSleepCluster.access$202(deepSleepCluster, param1Parcel.readDouble());
        DeepSleepCluster.access$302(deepSleepCluster, param1Parcel.readInt());
        DeepSleepCluster.access$402(deepSleepCluster, param1Parcel.readInt());
        DeepSleepCluster.access$502(deepSleepCluster, param1Parcel.readDouble());
        DeepSleepCluster.access$602(deepSleepCluster, param1Parcel.readDouble());
        DeepSleepCluster.access$702(deepSleepCluster, param1Parcel.readDouble());
        DeepSleepCluster.access$802(deepSleepCluster, param1Parcel.readDouble());
        return deepSleepCluster;
      }
      
      public DeepSleepCluster[] newArray(int param1Int) {
        return new DeepSleepCluster[param1Int];
      }
    };
  
  public static final int ANOMALY_TYPE = -1;
  
  private static final double DEFAULT_MAX_DISTANCE = 0.0D;
  
  private static final String TAG = "DeepSleepCluster";
  
  public DeepSleepCluster clone() {
    DeepSleepCluster deepSleepCluster = null;
    try {
      DeepSleepCluster deepSleepCluster1 = (DeepSleepCluster)super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
      SDKLog.e("DeepSleepCluster", cloneNotSupportedException.getMessage());
    } 
    return deepSleepCluster;
  }
}
