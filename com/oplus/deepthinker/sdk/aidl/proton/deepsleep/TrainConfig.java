package com.oplus.deepthinker.sdk.aidl.proton.deepsleep;

import android.os.Parcel;
import android.os.Parcelable;

public class TrainConfig implements Parcelable {
  private int mType = -1;
  
  private int mDayForPredict;
  
  private int mClusterMinPoints;
  
  private double mClusterEps;
  
  private static final String SPLIT = ",";
  
  private static final double SIGMA = 1.0E-4D;
  
  private static final int PRIME_NUM = 31;
  
  private static final int MULTIPLE = 1000;
  
  public TrainConfig(int paramInt1, double paramDouble, int paramInt2) {
    this.mClusterMinPoints = paramInt1;
    this.mClusterEps = paramDouble;
    this.mDayForPredict = paramInt2;
  }
  
  public TrainConfig(String paramString) {
    String[] arrayOfString = paramString.split(",");
    if (arrayOfString.length == 4) {
      this.mClusterMinPoints = Integer.valueOf(arrayOfString[0]).intValue();
      this.mClusterEps = Double.valueOf(arrayOfString[1]).doubleValue();
      this.mDayForPredict = Integer.valueOf(arrayOfString[2]).intValue();
      this.mType = Integer.valueOf(arrayOfString[3]).intValue();
    } 
  }
  
  public int getClusterMinPoints() {
    return this.mClusterMinPoints;
  }
  
  public void setClusterMinPoints(int paramInt) {
    this.mClusterMinPoints = paramInt;
  }
  
  public double getClusterEps() {
    return this.mClusterEps;
  }
  
  public void setClusterEps(double paramDouble) {
    this.mClusterEps = paramDouble;
  }
  
  public int getdayForPredict() {
    return this.mDayForPredict;
  }
  
  public void setdayForPredict(int paramInt) {
    this.mDayForPredict = paramInt;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public void setType(int paramInt) {
    this.mType = paramInt;
  }
  
  public String spliceParameter() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.mClusterMinPoints);
    stringBuilder.append(",");
    stringBuilder.append(this.mClusterEps);
    stringBuilder.append(",");
    stringBuilder.append(this.mDayForPredict);
    stringBuilder.append(",");
    stringBuilder.append(this.mType);
    return stringBuilder.toString();
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("TrainConfig{mClusterMinPoints=");
    stringBuilder.append(this.mClusterMinPoints);
    stringBuilder.append(", mClusterEps=");
    stringBuilder.append(this.mClusterEps);
    stringBuilder.append(", mDayForPredict=");
    stringBuilder.append(this.mDayForPredict);
    stringBuilder.append(", mType=");
    stringBuilder.append(this.mType);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public int hashCode() {
    int i = this.mClusterMinPoints;
    int j = (int)(this.mClusterEps * 1000.0D);
    int k = this.mDayForPredict;
    int m = this.mType;
    return ((i * 31 + j) * 31 + k) * 31 + m;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof TrainConfig))
      return false; 
    paramObject = paramObject;
    if (this.mClusterMinPoints != paramObject.getClusterMinPoints())
      return false; 
    if (Math.abs(this.mClusterEps - paramObject.getClusterEps()) < 1.0E-4D)
      return false; 
    if (this.mDayForPredict != paramObject.getdayForPredict())
      return false; 
    if (this.mType != paramObject.getType())
      bool = false; 
    return bool;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mClusterMinPoints);
    paramParcel.writeDouble(this.mClusterEps);
    paramParcel.writeInt(this.mDayForPredict);
    paramParcel.writeInt(this.mType);
  }
  
  public static final Parcelable.Creator<TrainConfig> CREATOR = new Parcelable.Creator<TrainConfig>() {
      public TrainConfig createFromParcel(Parcel param1Parcel) {
        TrainConfig trainConfig = new TrainConfig(0, 0.0D, 0);
        TrainConfig.access$002(trainConfig, param1Parcel.readInt());
        TrainConfig.access$102(trainConfig, param1Parcel.readDouble());
        TrainConfig.access$202(trainConfig, param1Parcel.readInt());
        TrainConfig.access$302(trainConfig, param1Parcel.readInt());
        return trainConfig;
      }
      
      public TrainConfig[] newArray(int param1Int) {
        return new TrainConfig[param1Int];
      }
    };
  
  private static final int CONFIG_DATA_LENGTH = 4;
}
