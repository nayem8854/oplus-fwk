package com.oplus.deepthinker.userprofile;

import android.net.Uri;

public class UserProfileConstants {
  public static final String COLUMN_CONFIDENCE = "confidence";
  
  public static final String COLUMN_DATA_CYCLE = "data_cycle";
  
  public static final String COLUMN_DETAIL = "detail";
  
  public static final String COLUMN_EXPIRATION_TIME = "expiration_time";
  
  public static final String COLUMN_LABEL_ID = "label_id";
  
  public static final String COLUMN_LABEL_RESULT = "label_result";
  
  public static final String COLUMN_PKG_NAME = "pkg_name";
  
  public static final int DEFAULT_LONG_TERM_DATA_CYCLE = 30;
  
  public static final int DEFAULT_SHORT_TERM_DATA_CYCLE = 1;
  
  public static final String FEATURE_ABILITY_USERPROFILE = "ability_userprofile";
  
  public static final String FEATURE_APP_IMPORTANCE = "ability_app_importance";
  
  public static final String FEATURE_APP_IMPORTANCE_GET_RESULT = "ability_app_importance_1";
  
  public static final String FEATURE_APP_IMPORTANCE_GET_RESULT_LOCKED = "ability_app_importance_2";
  
  public static final String FEATURE_APP_IMPORTANCE_PARAM_SERIALIZABLE_MAP = "ability_app_importance_param_0";
  
  public static final Uri FEATURE_PROVIDER = Uri.parse("content://com.oplus.deepthinker.provider.feature");
  
  public static final String KEY_DATA_CYCLE = "data_cycle";
  
  public static final String KEY_LABEL_ID = "label_id";
  
  public static final String KEY_LABEL_TYPE = "label_type";
  
  public static final int TYPE_APP_LABEL = 1;
  
  public static final int TYPE_USER_LABEL = 0;
  
  public static final String USERPROFILE_QUERY = "query_user_profile";
  
  public static final String USERPROFILE_QUERY_RESULT = "query_result_user_profile";
  
  public enum LabelId {
    ACTIVE_TIME_PERIOD,
    APP_ACTIVATION,
    APP_ACTIVE_TIME_PERIOD,
    APP_BG_VECTOR,
    APP_EXPLORE,
    APP_INACTIVE_TIME_PERIOD,
    APP_JUMP_RELATION,
    APP_LONG_TIME_USE,
    APP_MOBILE_TRAFFIC_SENSITIVE,
    APP_NOTIFICATION,
    APP_PREFERENCE,
    APP_WEEKDAY_ACTIVE_TIME_PERIOD,
    APP_WEEKDAY_INACTIVE_TIME_PERIOD,
    APP_WEEKEND_ACTIVE_TIME_PERIOD,
    APP_WEEKEND_INACTIVE_TIME_PERIOD,
    APP_WIFI_DEPENDENCY,
    BATTERY_CHARGE_HABIT,
    BATTERY_DISCHARGE_HABIT,
    BATTERY_USAGE,
    BT_SETTING_HABIT,
    CHARGING_LOCATION,
    EVENING_APPS,
    INACTIVE_TIME_PERIOD,
    MORNING_APPS,
    MUTE_SETTING_HABIT,
    NOON_APPS,
    NOTIFICATION_IMPORTANCE(2),
    POWER_ACCESSIBILITY(2),
    RESIDENCE(2),
    SLEEP_ADEQUACY(2),
    SLEEP_APPS(2),
    SLEEP_CHARGE(2),
    SLEEP_HABIT(2),
    TOP_APPS(2),
    TOP_POWER_COMSUMPTION_APPS(2),
    TRAFFIC_SENSITIVE(2),
    TRAFFIC_USAGE(2),
    WAKE_APPS(2),
    WIFI_DEPENDENCY(2),
    WIFI_LOCATION(2),
    WIFI_SETTING_HABIT(2);
    
    private static final LabelId[] $VALUES;
    
    private int value;
    
    static {
      MORNING_APPS = new LabelId("MORNING_APPS", 3, 5);
      NOON_APPS = new LabelId("NOON_APPS", 4, 6);
      EVENING_APPS = new LabelId("EVENING_APPS", 5, 7);
      SLEEP_APPS = new LabelId("SLEEP_APPS", 6, 8);
      APP_ACTIVE_TIME_PERIOD = new LabelId("APP_ACTIVE_TIME_PERIOD", 7, 9);
      TOP_POWER_COMSUMPTION_APPS = new LabelId("TOP_POWER_COMSUMPTION_APPS", 8, 10);
      BATTERY_USAGE = new LabelId("BATTERY_USAGE", 9, 11);
      POWER_ACCESSIBILITY = new LabelId("POWER_ACCESSIBILITY", 10, 12);
      ACTIVE_TIME_PERIOD = new LabelId("ACTIVE_TIME_PERIOD", 11, 13);
      INACTIVE_TIME_PERIOD = new LabelId("INACTIVE_TIME_PERIOD", 12, 14);
      TRAFFIC_USAGE = new LabelId("TRAFFIC_USAGE", 13, 15);
      WIFI_DEPENDENCY = new LabelId("WIFI_DEPENDENCY", 14, 16);
      TRAFFIC_SENSITIVE = new LabelId("TRAFFIC_SENSITIVE", 15, 17);
      SLEEP_HABIT = new LabelId("SLEEP_HABIT", 16, 18);
      SLEEP_CHARGE = new LabelId("SLEEP_CHARGE", 17, 19);
      SLEEP_ADEQUACY = new LabelId("SLEEP_ADEQUACY", 18, 20);
      WAKE_APPS = new LabelId("WAKE_APPS", 19, 21);
      APP_INACTIVE_TIME_PERIOD = new LabelId("APP_INACTIVE_TIME_PERIOD", 20, 22);
      CHARGING_LOCATION = new LabelId("CHARGING_LOCATION", 21, 23);
      WIFI_LOCATION = new LabelId("WIFI_LOCATION", 22, 24);
      BATTERY_CHARGE_HABIT = new LabelId("BATTERY_CHARGE_HABIT", 23, 25);
      BATTERY_DISCHARGE_HABIT = new LabelId("BATTERY_DISCHARGE_HABIT", 24, 26);
      WIFI_SETTING_HABIT = new LabelId("WIFI_SETTING_HABIT", 25, 27);
      BT_SETTING_HABIT = new LabelId("BT_SETTING_HABIT", 26, 28);
      MUTE_SETTING_HABIT = new LabelId("MUTE_SETTING_HABIT", 27, 29);
      APP_JUMP_RELATION = new LabelId("APP_JUMP_RELATION", 28, 30);
      APP_EXPLORE = new LabelId("APP_EXPLORE", 29, 31);
      RESIDENCE = new LabelId("RESIDENCE", 30, 32);
      APP_WIFI_DEPENDENCY = new LabelId("APP_WIFI_DEPENDENCY", 31, 33);
      APP_MOBILE_TRAFFIC_SENSITIVE = new LabelId("APP_MOBILE_TRAFFIC_SENSITIVE", 32, 34);
      APP_WEEKDAY_ACTIVE_TIME_PERIOD = new LabelId("APP_WEEKDAY_ACTIVE_TIME_PERIOD", 33, 35);
      APP_WEEKEND_ACTIVE_TIME_PERIOD = new LabelId("APP_WEEKEND_ACTIVE_TIME_PERIOD", 34, 36);
      APP_WEEKDAY_INACTIVE_TIME_PERIOD = new LabelId("APP_WEEKDAY_INACTIVE_TIME_PERIOD", 35, 37);
      APP_WEEKEND_INACTIVE_TIME_PERIOD = new LabelId("APP_WEEKEND_INACTIVE_TIME_PERIOD", 36, 38);
      APP_NOTIFICATION = new LabelId("APP_NOTIFICATION", 37, 39);
      APP_LONG_TIME_USE = new LabelId("APP_LONG_TIME_USE", 38, 40);
      APP_PREFERENCE = new LabelId("APP_PREFERENCE", 39, 41);
      LabelId labelId = new LabelId("APP_BG_VECTOR", 40, 42);
      $VALUES = new LabelId[] { 
          NOTIFICATION_IMPORTANCE, APP_ACTIVATION, TOP_APPS, MORNING_APPS, NOON_APPS, EVENING_APPS, SLEEP_APPS, APP_ACTIVE_TIME_PERIOD, TOP_POWER_COMSUMPTION_APPS, BATTERY_USAGE, 
          POWER_ACCESSIBILITY, ACTIVE_TIME_PERIOD, INACTIVE_TIME_PERIOD, TRAFFIC_USAGE, WIFI_DEPENDENCY, TRAFFIC_SENSITIVE, SLEEP_HABIT, SLEEP_CHARGE, SLEEP_ADEQUACY, WAKE_APPS, 
          APP_INACTIVE_TIME_PERIOD, CHARGING_LOCATION, WIFI_LOCATION, BATTERY_CHARGE_HABIT, BATTERY_DISCHARGE_HABIT, WIFI_SETTING_HABIT, BT_SETTING_HABIT, MUTE_SETTING_HABIT, APP_JUMP_RELATION, APP_EXPLORE, 
          RESIDENCE, APP_WIFI_DEPENDENCY, APP_MOBILE_TRAFFIC_SENSITIVE, APP_WEEKDAY_ACTIVE_TIME_PERIOD, APP_WEEKEND_ACTIVE_TIME_PERIOD, APP_WEEKDAY_INACTIVE_TIME_PERIOD, APP_WEEKEND_INACTIVE_TIME_PERIOD, APP_NOTIFICATION, APP_LONG_TIME_USE, APP_PREFERENCE, 
          labelId };
    }
    
    public final int getValue() {
      return this.value;
    }
    
    public final void setValue(int param1Int) {
      this.value = param1Int;
    }
    
    LabelId(int param1Int1) {
      this.value = param1Int1;
    }
  }
}
