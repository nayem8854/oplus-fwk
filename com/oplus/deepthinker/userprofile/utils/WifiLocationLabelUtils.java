package com.oplus.deepthinker.userprofile.utils;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import com.oplus.deepthinker.sdk.aidl.proton.userprofile.WifiLocationLabel;
import com.oplus.deepthinker.sdk.common.utils.SDKLog;
import com.oplus.deepthinker.userprofile.UserProfileConstants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class WifiLocationLabelUtils {
  private static final int INDEX_ACCURACY = 3;
  
  private static final int INDEX_BSSID = 1;
  
  private static final int INDEX_LATITUDE = 1;
  
  private static final int INDEX_LONGITUDE = 0;
  
  private static final int INDEX_POINT_SIZE = 2;
  
  private static final int INDEX_RADIUS = 4;
  
  private static final int INDEX_SSID = 0;
  
  private static final int LABEL_RESULT_LENGTH = 7;
  
  private static final String LABEL_SPLITER = "-";
  
  private static final String TAG = "WifiLocationLabelUtils";
  
  public static List<WifiLocationLabel> getWifiLocationLabels(Context paramContext) {
    ArrayList<WifiLocationLabel> arrayList = new ArrayList();
    Bundle bundle = new Bundle();
    bundle.putInt("label_type", 0);
    bundle.putInt("label_id", UserProfileConstants.LabelId.WIFI_LOCATION.getValue());
    bundle.putInt("data_cycle", 30);
    List<ContentValues> list = InnerUtils.queryUserProfile(paramContext, bundle);
    if (list == null || list.isEmpty()) {
      SDKLog.w("WifiLocationLabelUtils", "getWifiLocationLabel result is null or Empty");
      return arrayList;
    } 
    for (ContentValues contentValues : list) {
      try {
        String str2 = contentValues.getAsString("label_result");
        String str1 = contentValues.getAsString("detail");
        Long long_ = contentValues.getAsLong("expiration_time");
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("labelResult = ");
        stringBuilder.append(str2);
        stringBuilder.append(" , labelDetail = ");
        stringBuilder.append(str1);
        SDKLog.w("WifiLocationLabelUtils", stringBuilder.toString());
        arrayList.addAll(parseWifiLabels(str2, str1, long_));
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("getWifiLocationLabels Exception: ");
        stringBuilder.append(exception.getMessage());
        SDKLog.e("WifiLocationLabelUtils", stringBuilder.toString());
      } 
    } 
    return arrayList;
  }
  
  private static List<WifiLocationLabel> parseWifiLabels(String paramString1, String paramString2, Long paramLong) {
    ArrayList<WifiLocationLabel> arrayList = new ArrayList();
    if (paramString1 != null && paramString2 != null && paramLong != null)
      try {
        String[] arrayOfString = paramString1.split("-");
        for (String str : paramString2.split("-")) {
          HashSet hashSet2 = new HashSet();
          this(Arrays.asList((E[])str.split(":")[0].split(",")));
          HashSet hashSet1 = new HashSet();
          this(Arrays.asList((E[])str.split(":")[1].split(",")));
          String[] arrayOfString1 = arrayOfString[Arrays.asList((T[])null).indexOf(str)].split(",");
          if (arrayOfString1.length != 7) {
            SDKLog.w("WifiLocationLabelUtils", "labelResultFields.length != 7");
            return arrayList;
          } 
          WifiLocationLabel wifiLocationLabel = new WifiLocationLabel();
          double d = Double.parseDouble(arrayOfString1[0]);
          str = arrayOfString1[1];
          this(d, Double.parseDouble(str), Float.parseFloat(arrayOfString1[4]), hashSet1, hashSet2);
          wifiLocationLabel.setClusterPointsNum(Integer.parseInt(arrayOfString1[2]));
          wifiLocationLabel.setAccuracy(Double.parseDouble(arrayOfString1[3]));
          wifiLocationLabel.setExpiration(paramLong.longValue());
          arrayList.add(wifiLocationLabel);
        } 
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("parseWifiLabels Exception: ");
        stringBuilder.append(exception.getMessage());
        SDKLog.e("WifiLocationLabelUtils", stringBuilder.toString());
      }  
    return arrayList;
  }
}
