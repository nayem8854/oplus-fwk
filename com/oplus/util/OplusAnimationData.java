package com.oplus.util;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public final class OplusAnimationData implements Parcelable {
  public static final Parcelable.Creator<OplusAnimationData> CREATOR = new Parcelable.Creator<OplusAnimationData>() {
      public OplusAnimationData createFromParcel(Parcel param1Parcel) {
        return new OplusAnimationData(param1Parcel);
      }
      
      public OplusAnimationData[] newArray(int param1Int) {
        return new OplusAnimationData[param1Int];
      }
    };
  
  private boolean mAnimationEnable = false;
  
  private ArrayList<String> mAppStartCornerRadiusAnimationBlackPackages = new ArrayList<>();
  
  private ArrayList<String> mAppExitCornerRadiusAnimationBlackPackages = new ArrayList<>();
  
  private ArrayList<String> mAppExitAnimationBlackTokens = new ArrayList<>();
  
  public OplusAnimationData(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeStringList(this.mAppStartCornerRadiusAnimationBlackPackages);
    paramParcel.writeStringList(this.mAppExitCornerRadiusAnimationBlackPackages);
    paramParcel.writeStringList(this.mAppExitAnimationBlackTokens);
    paramParcel.writeByte((byte)this.mAnimationEnable);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    boolean bool;
    this.mAppStartCornerRadiusAnimationBlackPackages = paramParcel.createStringArrayList();
    this.mAppExitCornerRadiusAnimationBlackPackages = paramParcel.createStringArrayList();
    this.mAppExitAnimationBlackTokens = paramParcel.createStringArrayList();
    if (paramParcel.readByte() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mAnimationEnable = bool;
  }
  
  public ArrayList<String> getAppStartCornerRadiusAnimationBlackPackages() {
    return this.mAppStartCornerRadiusAnimationBlackPackages;
  }
  
  public boolean getAnimationEnabled() {
    return this.mAnimationEnable;
  }
  
  public void setAnimationEnabled(boolean paramBoolean) {
    this.mAnimationEnable = paramBoolean;
  }
  
  public ArrayList<String> getAppExitCornerRadiusAnimationBlackPackages() {
    return this.mAppExitCornerRadiusAnimationBlackPackages;
  }
  
  public void setAppStartCornerRadiusAnimationBlackPackages(List<String> paramList) {
    this.mAppStartCornerRadiusAnimationBlackPackages.clear();
    this.mAppStartCornerRadiusAnimationBlackPackages.addAll(paramList);
  }
  
  public void setAppExitCornerRadiusAnimationBlackPackages(List<String> paramList) {
    this.mAppExitCornerRadiusAnimationBlackPackages.clear();
    this.mAppExitCornerRadiusAnimationBlackPackages.addAll(paramList);
  }
  
  public ArrayList<String> getAppExitAnimationBlackTokens() {
    return this.mAppExitAnimationBlackTokens;
  }
  
  public void setAppExitAnimationBlackTokens(List<String> paramList) {
    this.mAppExitAnimationBlackTokens.clear();
    this.mAppExitAnimationBlackTokens.addAll(paramList);
  }
  
  public OplusAnimationData() {}
}
