package com.oplus.util;

import android.os.IBinder;
import android.os.ServiceManager;
import android.os.SystemProperties;

public abstract class OplusBaseServiceManager {
  public static final boolean DBG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  protected final IBinder mRemote;
  
  protected abstract void init(IBinder paramIBinder);
  
  public OplusBaseServiceManager(String paramString) {
    this(ServiceManager.getService(paramString));
  }
  
  public OplusBaseServiceManager(IBinder paramIBinder) {
    this.mRemote = paramIBinder;
    init(paramIBinder);
  }
}
