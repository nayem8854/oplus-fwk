package com.oplus.util;

import android.app.ActivityManager;
import android.app.ActivityManagerNative;
import android.app.ActivityThread;
import android.app.ContextImpl;
import android.app.IActivityManager;
import android.app.OplusActivityManager;
import android.content.Intent;
import android.content.pm.IPackageManager;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.OplusBaseConfiguration;
import android.os.Build;
import android.os.IBinder;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.Log;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class InstallFont {
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static final String TAG = "com.oplus.util.InstallFont";
  
  private static ArrayList<String> inputMethodList;
  
  private static ArrayList<String> mAppWhiteList = new ArrayList<>();
  
  static {
    inputMethodList = new ArrayList<>();
    mAppWhiteList.add("com.mediatek.mtklogger");
    mAppWhiteList.add("com.oppo.recents");
    mAppWhiteList.add("com.coloros.recents");
    mAppWhiteList.add("com.oppo.alarmclock");
    mAppWhiteList.add("com.coloros.alarmclock");
    mAppWhiteList.add("com.android.captiveportallogin");
    mAppWhiteList.add("com.android.systemui");
    mAppWhiteList.add("com.android.keyguard");
    mAppWhiteList.add("com.android.settings");
    mAppWhiteList.add("com.coloros.bootreg");
    mAppWhiteList.add("com.oppo.launcher");
    mAppWhiteList.add("com.oppo.weather");
    mAppWhiteList.add("com.coloros.weather");
    mAppWhiteList.add("com.oppo.music");
    mAppWhiteList.add("com.coloros.gallery3d");
    mAppWhiteList.add("com.nearme.themespace");
    mAppWhiteList.add("com.color.safecenter");
    mAppWhiteList.add("com.coloros.safecenter");
    mAppWhiteList.add("com.coloros.filemanager");
    mAppWhiteList.add("com.nearme.gamecenter");
    mAppWhiteList.add("com.android.contacts");
    mAppWhiteList.add("oppo.multimedia.soundrecorder");
    mAppWhiteList.add("com.coloros.soundrecorder");
    mAppWhiteList.add("com.android.providers.downloads");
    mAppWhiteList.add("com.oppo.backuprestore");
    mAppWhiteList.add("com.coloros.backuprestore");
    mAppWhiteList.add("com.oppo.reader");
    mAppWhiteList.add("com.android.mms");
    mAppWhiteList.add("com.oppo.usercenter");
    mAppWhiteList.add("com.oppo.community");
    mAppWhiteList.add("com.nearme.note");
    mAppWhiteList.add("com.android.email");
    mAppWhiteList.add("com.android.packageinstaller");
    mAppWhiteList.add("com.android.phone");
    mAppWhiteList.add("org.codeaurora.bluetooth");
    mAppWhiteList.add("com.android.bluetooth");
    mAppWhiteList.add("com.android.nfc");
    mAppWhiteList.add("com.android.incallui");
    mAppWhiteList.add("com.tencent.mm");
    mAppWhiteList.add("com.tencent.mobileqq");
    mAppWhiteList.add("com.coloros.screenrecorder");
    mAppWhiteList.add("com.nearme.themestore");
    mAppWhiteList.add("com.heytap.themestore");
    mAppWhiteList.add("com.google.android.marvin.talkback");
  }
  
  public InstallFont() {
    inputMethodList = findInputMethods();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("init InstallFont , inputMethodList size = ");
    stringBuilder.append(inputMethodList.size());
    logd(stringBuilder.toString());
  }
  
  public void killRecentPackage() {
    IActivityManager iActivityManager = ActivityManager.getService();
    int i = Process.myUserHandle().hashCode();
    List<ActivityManager.RecentTaskInfo> list = new ArrayList();
    try {
      List<ActivityManager.RecentTaskInfo> list1 = iActivityManager.getRecentTasks(100, 1, i).getList();
    } catch (Exception exception) {
      loge("killRecentPackage", exception);
    } 
    for (i = 0; i < list.size(); i++) {
      ActivityManager.RecentTaskInfo recentTaskInfo = list.get(i);
      String str = recentTaskInfo.baseIntent.getComponent().getPackageName();
      int j = recentTaskInfo.userId;
      if (!mAppWhiteList.contains(str) && !inputMethodList.contains(str) && !str.contains("com.oppo.autotest") && !str.contains("com.oppo.qe")) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" killRecentPackage_forceStopPackage = ");
        stringBuilder.append(str);
        stringBuilder.append(" , userId = ");
        stringBuilder.append(j);
        logd(stringBuilder.toString());
        try {
          iActivityManager.forceStopPackage(str, j);
        } catch (Exception exception) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Failed  killRecentPackage_forceStopPackage = ");
          stringBuilder.append(str);
          stringBuilder.append(" , userId = ");
          stringBuilder.append(j);
          loge(stringBuilder.toString(), exception);
        } 
      } 
    } 
  }
  
  public void killAppProcess() {
    ArrayList<ActivityManager.RunningAppProcessInfo> arrayList = new ArrayList();
    try {
      ArrayList<ActivityManager.RunningAppProcessInfo> arrayList1 = (ArrayList)ActivityManager.getService().getRunningAppProcesses();
    } catch (Exception exception) {
      loge("killAppProcess", exception);
    } 
    for (byte b = 0; b < arrayList.size(); b++) {
      ActivityManager.RunningAppProcessInfo runningAppProcessInfo = arrayList.get(b);
      String str = runningAppProcessInfo.processName;
      int i = runningAppProcessInfo.pid;
      if (str == null || (!str.contains("com.oppo.autotest") && !str.contains("com.oppo.qe"))) {
        if (str != null && (str.contains("com.tencent.mm") || str.contains("com.tencent.mobileqq")))
          try {
            killPidForce(i);
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append(" killPidForce processName = ");
            stringBuilder.append(str);
            logd(stringBuilder.toString());
          } catch (Exception exception) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(" Failed killPidForce processName = ");
            stringBuilder.append(str);
            stringBuilder.append(" , pid = ");
            stringBuilder.append(i);
            logd(stringBuilder.toString());
          }  
        for (byte b1 = 0; b1 < runningAppProcessInfo.pkgList.length; b1++) {
          str = runningAppProcessInfo.pkgList[b1];
          if (inputMethodList.contains(str)) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(" killAppProcess_killPidForce = ");
            stringBuilder.append(str);
            logd(stringBuilder.toString());
            try {
              killPidForce(i);
            } catch (Exception exception) {
              StringBuilder stringBuilder1 = new StringBuilder();
              stringBuilder1.append("Failed  killAppProcess_killPidForce = ");
              stringBuilder1.append(str);
              stringBuilder1.append(" , pid = ");
              stringBuilder1.append(i);
              loge(stringBuilder1.toString(), exception);
            } 
            break;
          } 
        } 
      } 
    } 
  }
  
  private static void killPidForce(int paramInt) {
    try {
      OplusActivityManager oplusActivityManager = new OplusActivityManager();
      this();
      oplusActivityManager.killPidForce(paramInt);
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public static ArrayList<String> findInputMethods() {
    ContextImpl contextImpl = ActivityThread.currentActivityThread().getSystemUiContext();
    ArrayList<String> arrayList = new ArrayList();
    PackageManager packageManager = contextImpl.getPackageManager();
    List<ResolveInfo> list = packageManager.queryIntentServices(new Intent("android.view.InputMethod"), 128);
    int i = list.size();
    for (byte b = 0; b < i; b++) {
      ResolveInfo resolveInfo = list.get(b);
      String str = (resolveInfo.getComponentInfo()).applicationInfo.packageName;
      arrayList.add(str);
    } 
    return arrayList;
  }
  
  public static void setConfig() {
    IActivityManager iActivityManager;
    if (Build.VERSION.SDK_INT > 25) {
      iActivityManager = ActivityManager.getService();
    } else {
      iActivityManager = ActivityManagerNative.getDefault();
    } 
    try {
      Configuration configuration = iActivityManager.getConfiguration();
      OplusBaseConfiguration oplusBaseConfiguration = (OplusBaseConfiguration)OplusTypeCastingHelper.typeCasting(OplusBaseConfiguration.class, configuration);
      Random random = new Random();
      this(System.currentTimeMillis());
      try {
        oplusBaseConfiguration.mOplusExtraConfiguration.mFlipFont = random.nextInt(10000 - 0 + 1) + 0;
        if (Build.VERSION.SDK_INT > 25) {
          iActivityManager.updateConfiguration(configuration);
          return;
        } 
        updateConfigurationReflect(iActivityManager, configuration);
      } catch (Error error) {
        try {
          Class<?> clazz = configuration.getClass();
          Field[] arrayOfField = clazz.getDeclaredFields();
          int i = arrayOfField.length;
          for (byte b = 0; b < i; b++) {
            if ("FlipFont".equalsIgnoreCase(arrayOfField[b].getName()))
              arrayOfField[b].setInt(configuration, random.nextInt(10000 - 0 + 1) + 0); 
          } 
        } catch (Exception exception) {
          exception.printStackTrace();
        } 
        if (Build.VERSION.SDK_INT > 25) {
          iActivityManager.updateConfiguration(configuration);
          return;
        } 
        updateConfigurationReflect(iActivityManager, configuration);
      } finally {}
    } catch (RemoteException remoteException) {
      loge("RemoteException e=", (Throwable)remoteException);
    } 
  }
  
  private static void updateConfigurationReflect(IActivityManager paramIActivityManager, Configuration paramConfiguration) {
    if (paramIActivityManager == null || paramConfiguration == null) {
      logd("updateConfigurationReflect, update fails, IActivityManager or Configuration is null.");
      return;
    } 
    try {
      Class<?> clazz = paramIActivityManager.getClass();
      if (clazz != null) {
        Method method = clazz.getMethod("updateConfiguration", new Class[] { Configuration.class });
        if (method != null)
          method.invoke(paramIActivityManager, new Object[] { paramConfiguration }); 
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("updateConfigurationReflect, e=");
      stringBuilder.append(exception);
      logd(stringBuilder.toString());
    } 
  }
  
  public static IPackageManager getPackageManager() {
    IBinder iBinder = ServiceManager.getService("package");
    return IPackageManager.Stub.asInterface(iBinder);
  }
  
  private static void logd(String paramString) {
    if (DEBUG)
      Log.d("com.oplus.util.InstallFont", paramString); 
  }
  
  private static void loge(String paramString, Throwable paramThrowable) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append(":");
      stringBuilder.append(paramThrowable.getMessage());
      Log.e("com.oplus.util.InstallFont", stringBuilder.toString(), paramThrowable);
    } 
  }
}
