package com.oplus.util;

import android.app.OplusActivityManager;
import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.FileObserver;
import android.os.FileUtils;
import android.os.Handler;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.provider.Settings;
import android.util.Slog;
import android.util.Xml;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;

public class OplusDisplayOptimizationUtils {
  public static boolean DEBUG_SWITCH = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static OplusDisplayOptimizationData sOptimizationData = null;
  
  private final Object mDisplayOptEnableLock = new Object();
  
  private final Object mDisplayOptWhiteListLock = new Object();
  
  private final Object mDisplayOptBlackListLock = new Object();
  
  private final Object mDisplayOptExcludeProcessListLock = new Object();
  
  private final Object mDisplayOptExcludeWindowListLock = new Object();
  
  private final Object mDisplayOptPolicyLock = new Object();
  
  private final Object mDisplayOptSpeicalListLock = new Object();
  
  private Context mContext = null;
  
  private boolean mEnableDisplatOpt = true;
  
  private boolean mEnableGraphicAccelerationSwitch = true;
  
  private int mEnablePolicy = 0;
  
  private List<String> mBlackList = new ArrayList<>();
  
  private List<String> mWhiteList = new ArrayList<>();
  
  private List<String> mExcludeProcessList = new ArrayList<>();
  
  private List<String> mSpecialList = new ArrayList<>();
  
  private List<String> mExcludeWindowList = new ArrayList<>();
  
  private FileObserverPolicy mDisplayOptFileObserver = null;
  
  private SwitchObserverPolicy mGraphicAccelerationSwitchObserver = null;
  
  private static final String COLOR_DISPLAY_OPTIMIZATION_CONFIG_FILE_PATH = "/data/oppo/coloros/gamespace/sys_display_opt_config.xml";
  
  private static final String COLOR_DISPLAY_OPTIMIZATION_DIR = "/data/oppo/coloros/gamespace";
  
  private static final String GRAPHICS_ACCELERATION_FOR_GAME_SPACE_MODE = "graphics_acceleration_for_game_space_mode";
  
  private static final int POLICY_OTHERS = 2;
  
  private static final int POLICY_USE_BLACK_LIST = 1;
  
  private static final int POLICY_USE_WHITE_LIST = 0;
  
  private static final int SWITCH_DEFAULT = 1;
  
  private static final String TAG = "OplusDisplayOptimizationUtils";
  
  private static final String TAG_BLACK = "black";
  
  private static final String TAG_ENABLE = "enable_display_opt";
  
  private static final String TAG_ENABLE_POLICY = "enable_policy";
  
  private static final String TAG_EXCLUDE_PROCESS = "excludeProcess";
  
  private static final String TAG_EXCLUDE_WINDOW = "excludeWindow";
  
  private static final String TAG_SPECIAL = "special";
  
  private static final String TAG_WHITE = "white";
  
  private static volatile OplusDisplayOptimizationUtils sDisplayOptUtils;
  
  static {
    sDisplayOptUtils = null;
  }
  
  public static OplusDisplayOptimizationUtils getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/util/OplusDisplayOptimizationUtils.sDisplayOptUtils : Lcom/oplus/util/OplusDisplayOptimizationUtils;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/util/OplusDisplayOptimizationUtils
    //   8: monitorenter
    //   9: getstatic com/oplus/util/OplusDisplayOptimizationUtils.sDisplayOptUtils : Lcom/oplus/util/OplusDisplayOptimizationUtils;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/util/OplusDisplayOptimizationUtils
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/util/OplusDisplayOptimizationUtils.sDisplayOptUtils : Lcom/oplus/util/OplusDisplayOptimizationUtils;
    //   27: ldc com/oplus/util/OplusDisplayOptimizationUtils
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/util/OplusDisplayOptimizationUtils
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/util/OplusDisplayOptimizationUtils.sDisplayOptUtils : Lcom/oplus/util/OplusDisplayOptimizationUtils;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #104	-> 0
    //   #105	-> 6
    //   #106	-> 9
    //   #107	-> 15
    //   #109	-> 27
    //   #111	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public void init(Context paramContext) {
    this.mContext = paramContext;
    if (sOptimizationData == null)
      sOptimizationData = new OplusDisplayOptimizationData(); 
    boolean bool = paramContext.getPackageManager().hasSystemFeature("oppo.graphics.acceleration.not.support");
    if (bool)
      synchronized (this.mDisplayOptEnableLock) {
        this.mEnableDisplatOpt = false;
        sOptimizationData.setDisplatOptEnabled(false);
        return;
      }  
    initDir();
    initFileObserver();
    initSwitchObserver();
    readDisplayOptConfig();
    updateGraphicAccelerationSwitch();
  }
  
  public OplusDisplayOptimizationData getOptimizationData() {
    if (sOptimizationData == null)
      sOptimizationData = new OplusDisplayOptimizationData(); 
    return sOptimizationData;
  }
  
  private void initDir() {
    if (DEBUG_SWITCH)
      Slog.i("OplusDisplayOptimizationUtils", "initDir start"); 
    File file1 = new File("/data/oppo/coloros/gamespace");
    File file2 = new File("/data/oppo/coloros/gamespace/sys_display_opt_config.xml");
    try {
      if (!file1.exists())
        file1.mkdirs(); 
      if (!file2.exists())
        file2.createNewFile(); 
    } catch (IOException iOException) {
      Slog.e("OplusDisplayOptimizationUtils", "initDir failed!!!");
      iOException.printStackTrace();
    } 
    changeModFile("/data/oppo/coloros/gamespace/sys_display_opt_config.xml");
  }
  
  private void initFileObserver() {
    FileObserverPolicy fileObserverPolicy = new FileObserverPolicy("/data/oppo/coloros/gamespace/sys_display_opt_config.xml");
    fileObserverPolicy.startWatching();
  }
  
  private void initSwitchObserver() {
    this.mGraphicAccelerationSwitchObserver = new SwitchObserverPolicy();
    Context context = this.mContext;
    if (context != null) {
      ContentResolver contentResolver = context.getContentResolver();
      Uri uri = Settings.Global.getUriFor("graphics_acceleration_for_game_space_mode");
      SwitchObserverPolicy switchObserverPolicy = this.mGraphicAccelerationSwitchObserver;
      contentResolver.registerContentObserver(uri, true, switchObserverPolicy);
    } 
  }
  
  private void changeModFile(String paramString) {
    FileUtils.setPermissions(paramString, 502, -1, -1);
  }
  
  public void readDisplayOptConfig() {
    if (DEBUG_SWITCH)
      Slog.i("OplusDisplayOptimizationUtils", "readDisplayOptConfigFile"); 
    File file = new File("/data/oppo/coloros/gamespace/sys_display_opt_config.xml");
    if (file.exists()) {
      if (file.length() == 0L) {
        loadDefaultDisplayOptList();
      } else {
        readConfigFromFileLocked(file);
      } 
    } else {
      Slog.i("OplusDisplayOptimizationUtils", "displayoptconfig file isn't exist!");
    } 
  }
  
  private void readConfigFromFileLocked(File paramFile) {
    if (DEBUG_SWITCH)
      Slog.i("OplusDisplayOptimizationUtils", "readConfigFromFileLocked start"); 
    ArrayList<String> arrayList1 = new ArrayList();
    ArrayList<Object> arrayList2 = new ArrayList();
    ArrayList<String> arrayList3 = new ArrayList();
    ArrayList<String> arrayList4 = new ArrayList();
    ArrayList<Object> arrayList5 = new ArrayList();
    FileInputStream fileInputStream1 = null, fileInputStream2 = null;
    FileInputStream fileInputStream3 = fileInputStream2, fileInputStream4 = fileInputStream1;
    try {
      StringBuilder stringBuilder;
      int i;
      FileInputStream fileInputStream6 = new FileInputStream();
      fileInputStream3 = fileInputStream2;
      fileInputStream4 = fileInputStream1;
      this(paramFile);
      FileInputStream fileInputStream5 = fileInputStream6;
      fileInputStream3 = fileInputStream5;
      fileInputStream4 = fileInputStream5;
      XmlPullParser xmlPullParser = Xml.newPullParser();
      fileInputStream3 = fileInputStream5;
      fileInputStream4 = fileInputStream5;
      xmlPullParser.setInput(fileInputStream5, null);
      do {
        fileInputStream3 = fileInputStream5;
        fileInputStream4 = fileInputStream5;
        i = xmlPullParser.next();
        if (i != 2)
          continue; 
        fileInputStream3 = fileInputStream5;
        fileInputStream4 = fileInputStream5;
        String str = xmlPullParser.getName();
        fileInputStream3 = fileInputStream5;
        fileInputStream4 = fileInputStream5;
        if (DEBUG_SWITCH) {
          fileInputStream3 = fileInputStream5;
          fileInputStream4 = fileInputStream5;
          StringBuilder stringBuilder1 = new StringBuilder();
          fileInputStream3 = fileInputStream5;
          fileInputStream4 = fileInputStream5;
          this();
          fileInputStream3 = fileInputStream5;
          fileInputStream4 = fileInputStream5;
          stringBuilder1.append(" readConfigFromFileLocked tagName=");
          fileInputStream3 = fileInputStream5;
          fileInputStream4 = fileInputStream5;
          stringBuilder1.append(str);
          fileInputStream3 = fileInputStream5;
          fileInputStream4 = fileInputStream5;
          Slog.i("OplusDisplayOptimizationUtils", stringBuilder1.toString());
        } 
        fileInputStream3 = fileInputStream5;
        fileInputStream4 = fileInputStream5;
        if ("enable_display_opt".equals(str)) {
          fileInputStream3 = fileInputStream5;
          fileInputStream4 = fileInputStream5;
          str = xmlPullParser.nextText();
          fileInputStream3 = fileInputStream5;
          fileInputStream4 = fileInputStream5;
          if (!str.equals("")) {
            fileInputStream3 = fileInputStream5;
            fileInputStream4 = fileInputStream5;
            Object object1 = this.mDisplayOptEnableLock;
            fileInputStream3 = fileInputStream5;
            synchronized (fileInputStream5) {
              boolean bool = Boolean.parseBoolean(str);
              sOptimizationData.setDisplatOptEnabled(bool);
              /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
              fileInputStream3 = fileInputStream5;
              fileInputStream4 = fileInputStream5;
              if (DEBUG_SWITCH) {
                fileInputStream3 = fileInputStream5;
                fileInputStream4 = fileInputStream5;
                object1 = new StringBuilder();
                fileInputStream3 = fileInputStream5;
                fileInputStream4 = fileInputStream5;
                super();
                fileInputStream3 = fileInputStream5;
                fileInputStream4 = fileInputStream5;
                object1.append(" readConfigFromFileLocked enable displayopt = ");
                fileInputStream3 = fileInputStream5;
                fileInputStream4 = fileInputStream5;
                object1.append(str);
                fileInputStream3 = fileInputStream5;
                fileInputStream4 = fileInputStream5;
                Slog.i("OplusDisplayOptimizationUtils", object1.toString());
              } 
            } 
          } 
        } else {
          Object object1;
          fileInputStream3 = fileInputStream5;
          fileInputStream4 = fileInputStream5;
          if ("enable_policy".equals(str)) {
            fileInputStream3 = fileInputStream5;
            fileInputStream4 = fileInputStream5;
            String str1 = xmlPullParser.nextText();
            fileInputStream3 = fileInputStream5;
            fileInputStream4 = fileInputStream5;
            if (!str1.equals("")) {
              fileInputStream3 = fileInputStream5;
              fileInputStream4 = fileInputStream5;
              object1 = this.mDisplayOptPolicyLock;
              fileInputStream3 = fileInputStream5;
              synchronized (fileInputStream5) {
                int j = Integer.parseInt(str1);
                if (j == 1 || j == 0) {
                  this.mEnablePolicy = j;
                  sOptimizationData.setEnablePolicy(j);
                } 
                /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
                fileInputStream3 = fileInputStream5;
                fileInputStream4 = fileInputStream5;
                if (DEBUG_SWITCH) {
                  fileInputStream3 = fileInputStream5;
                  fileInputStream4 = fileInputStream5;
                  object1 = new StringBuilder();
                  fileInputStream3 = fileInputStream5;
                  fileInputStream4 = fileInputStream5;
                  super();
                  fileInputStream3 = fileInputStream5;
                  fileInputStream4 = fileInputStream5;
                  object1.append(" readConfigFromFileLocked enable policy = ");
                  fileInputStream3 = fileInputStream5;
                  fileInputStream4 = fileInputStream5;
                  object1.append(str1);
                  fileInputStream3 = fileInputStream5;
                  fileInputStream4 = fileInputStream5;
                  Slog.i("OplusDisplayOptimizationUtils", object1.toString());
                } 
              } 
            } 
          } else {
            fileInputStream3 = fileInputStream5;
            fileInputStream4 = fileInputStream5;
            if ("white".equals(object1)) {
              fileInputStream3 = fileInputStream5;
              fileInputStream4 = fileInputStream5;
              String str1 = xmlPullParser.nextText();
              fileInputStream3 = fileInputStream5;
              fileInputStream4 = fileInputStream5;
              if (!str1.equals("")) {
                fileInputStream3 = fileInputStream5;
                fileInputStream4 = fileInputStream5;
                arrayList1.add(str1);
                fileInputStream3 = fileInputStream5;
                fileInputStream4 = fileInputStream5;
                if (DEBUG_SWITCH) {
                  fileInputStream3 = fileInputStream5;
                  fileInputStream4 = fileInputStream5;
                  object1 = new StringBuilder();
                  fileInputStream3 = fileInputStream5;
                  fileInputStream4 = fileInputStream5;
                  super();
                  fileInputStream3 = fileInputStream5;
                  fileInputStream4 = fileInputStream5;
                  object1.append(" readConfigFromFileLocked white pkg = ");
                  fileInputStream3 = fileInputStream5;
                  fileInputStream4 = fileInputStream5;
                  object1.append(str1);
                  fileInputStream3 = fileInputStream5;
                  fileInputStream4 = fileInputStream5;
                  Slog.i("OplusDisplayOptimizationUtils", object1.toString());
                } 
              } 
            } else {
              fileInputStream3 = fileInputStream5;
              fileInputStream4 = fileInputStream5;
              if ("black".equals(object1)) {
                fileInputStream3 = fileInputStream5;
                fileInputStream4 = fileInputStream5;
                object1 = xmlPullParser.nextText();
                fileInputStream3 = fileInputStream5;
                fileInputStream4 = fileInputStream5;
                if (!object1.equals("")) {
                  fileInputStream3 = fileInputStream5;
                  fileInputStream4 = fileInputStream5;
                  arrayList2.add(object1);
                  fileInputStream3 = fileInputStream5;
                  fileInputStream4 = fileInputStream5;
                  if (DEBUG_SWITCH) {
                    fileInputStream3 = fileInputStream5;
                    fileInputStream4 = fileInputStream5;
                    StringBuilder stringBuilder1 = new StringBuilder();
                    fileInputStream3 = fileInputStream5;
                    fileInputStream4 = fileInputStream5;
                    this();
                    fileInputStream3 = fileInputStream5;
                    fileInputStream4 = fileInputStream5;
                    stringBuilder1.append(" readConfigFromFileLocked black pkg = ");
                    fileInputStream3 = fileInputStream5;
                    fileInputStream4 = fileInputStream5;
                    stringBuilder1.append((String)object1);
                    fileInputStream3 = fileInputStream5;
                    fileInputStream4 = fileInputStream5;
                    Slog.i("OplusDisplayOptimizationUtils", stringBuilder1.toString());
                  } 
                } 
              } else {
                fileInputStream3 = fileInputStream5;
                fileInputStream4 = fileInputStream5;
                if ("special".equals(object1)) {
                  fileInputStream3 = fileInputStream5;
                  fileInputStream4 = fileInputStream5;
                  String str1 = xmlPullParser.nextText();
                  fileInputStream3 = fileInputStream5;
                  fileInputStream4 = fileInputStream5;
                  if (!str1.equals("")) {
                    fileInputStream3 = fileInputStream5;
                    fileInputStream4 = fileInputStream5;
                    arrayList3.add(str1);
                    fileInputStream3 = fileInputStream5;
                    fileInputStream4 = fileInputStream5;
                    if (DEBUG_SWITCH) {
                      fileInputStream3 = fileInputStream5;
                      fileInputStream4 = fileInputStream5;
                      object1 = new StringBuilder();
                      fileInputStream3 = fileInputStream5;
                      fileInputStream4 = fileInputStream5;
                      super();
                      fileInputStream3 = fileInputStream5;
                      fileInputStream4 = fileInputStream5;
                      object1.append(" readConfigFromFileLocked special pkg = ");
                      fileInputStream3 = fileInputStream5;
                      fileInputStream4 = fileInputStream5;
                      object1.append(str1);
                      fileInputStream3 = fileInputStream5;
                      fileInputStream4 = fileInputStream5;
                      Slog.i("OplusDisplayOptimizationUtils", object1.toString());
                    } 
                  } 
                } else {
                  fileInputStream3 = fileInputStream5;
                  fileInputStream4 = fileInputStream5;
                  if ("excludeProcess".equals(object1)) {
                    fileInputStream3 = fileInputStream5;
                    fileInputStream4 = fileInputStream5;
                    String str1 = xmlPullParser.nextText();
                    fileInputStream3 = fileInputStream5;
                    fileInputStream4 = fileInputStream5;
                    if (!str1.equals("")) {
                      fileInputStream3 = fileInputStream5;
                      fileInputStream4 = fileInputStream5;
                      arrayList4.add(str1);
                      fileInputStream3 = fileInputStream5;
                      fileInputStream4 = fileInputStream5;
                      if (DEBUG_SWITCH) {
                        fileInputStream3 = fileInputStream5;
                        fileInputStream4 = fileInputStream5;
                        object1 = new StringBuilder();
                        fileInputStream3 = fileInputStream5;
                        fileInputStream4 = fileInputStream5;
                        super();
                        fileInputStream3 = fileInputStream5;
                        fileInputStream4 = fileInputStream5;
                        object1.append(" readConfigFromFileLocked exclude process = ");
                        fileInputStream3 = fileInputStream5;
                        fileInputStream4 = fileInputStream5;
                        object1.append(str1);
                        fileInputStream3 = fileInputStream5;
                        fileInputStream4 = fileInputStream5;
                        Slog.i("OplusDisplayOptimizationUtils", object1.toString());
                      } 
                    } 
                  } else {
                    fileInputStream3 = fileInputStream5;
                    fileInputStream4 = fileInputStream5;
                    if ("excludeWindow".equals(object1)) {
                      fileInputStream3 = fileInputStream5;
                      fileInputStream4 = fileInputStream5;
                      object1 = xmlPullParser.nextText();
                      fileInputStream3 = fileInputStream5;
                      fileInputStream4 = fileInputStream5;
                      if (!object1.equals("")) {
                        fileInputStream3 = fileInputStream5;
                        fileInputStream4 = fileInputStream5;
                        arrayList5.add(object1);
                        fileInputStream3 = fileInputStream5;
                        fileInputStream4 = fileInputStream5;
                        if (DEBUG_SWITCH) {
                          fileInputStream3 = fileInputStream5;
                          fileInputStream4 = fileInputStream5;
                          StringBuilder stringBuilder1 = new StringBuilder();
                          fileInputStream3 = fileInputStream5;
                          fileInputStream4 = fileInputStream5;
                          this();
                          fileInputStream3 = fileInputStream5;
                          fileInputStream4 = fileInputStream5;
                          stringBuilder1.append(" readConfigFromFileLocked exclude window = ");
                          fileInputStream3 = fileInputStream5;
                          fileInputStream4 = fileInputStream5;
                          stringBuilder1.append((String)object1);
                          fileInputStream3 = fileInputStream5;
                          fileInputStream4 = fileInputStream5;
                          Slog.i("OplusDisplayOptimizationUtils", stringBuilder1.toString());
                        } 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
        } 
      } while (i != 1);
      fileInputStream3 = fileInputStream5;
      fileInputStream4 = fileInputStream5;
      Object object = this.mDisplayOptWhiteListLock;
      fileInputStream3 = fileInputStream5;
      synchronized (fileInputStream5) {
        this.mWhiteList.clear();
        this.mWhiteList.addAll(arrayList1);
        sOptimizationData.setWhiteList(arrayList1);
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        fileInputStream3 = fileInputStream5;
        fileInputStream4 = fileInputStream5;
        object = this.mDisplayOptBlackListLock;
        fileInputStream3 = fileInputStream5;
        synchronized (fileInputStream5) {
          this.mBlackList.clear();
          this.mBlackList.addAll(arrayList2);
          sOptimizationData.setBlackList(arrayList2);
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          fileInputStream3 = fileInputStream5;
          fileInputStream4 = fileInputStream5;
          object = this.mDisplayOptSpeicalListLock;
          fileInputStream3 = fileInputStream5;
          synchronized (fileInputStream5) {
            this.mSpecialList.clear();
            this.mSpecialList.addAll(arrayList3);
            sOptimizationData.setSpecialList(arrayList3);
            /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
            fileInputStream3 = fileInputStream5;
            fileInputStream4 = fileInputStream5;
            object = this.mDisplayOptExcludeProcessListLock;
            fileInputStream3 = fileInputStream5;
            synchronized (fileInputStream5) {
              this.mExcludeProcessList.clear();
              this.mExcludeProcessList.addAll(arrayList4);
              sOptimizationData.setExcludeProcessList(arrayList4);
              /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
              fileInputStream3 = fileInputStream5;
              fileInputStream4 = fileInputStream5;
              object = this.mDisplayOptExcludeWindowListLock;
              fileInputStream3 = fileInputStream5;
              synchronized (fileInputStream5) {
                this.mExcludeWindowList.clear();
                this.mExcludeWindowList.addAll(arrayList5);
                sOptimizationData.setExcludeWindowList(arrayList5);
                /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
                try {
                  fileInputStream5.close();
                } catch (IOException iOException) {
                  stringBuilder = new StringBuilder();
                  stringBuilder.append("Failed to close state FileInputStream ");
                  stringBuilder.append(iOException);
                  Slog.e("OplusDisplayOptimizationUtils", stringBuilder.toString());
                } 
              } 
            } 
          } 
        } 
      } 
    } catch (Exception exception) {
      fileInputStream3 = fileInputStream4;
      Slog.e("OplusDisplayOptimizationUtils", "failed parsing ", exception);
      fileInputStream3 = fileInputStream4;
      loadDefaultDisplayOptList();
      if (fileInputStream4 != null)
        try {
          fileInputStream4.close();
        } catch (IOException iOException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Failed to close state FileInputStream ");
          stringBuilder.append(iOException);
          Slog.e("OplusDisplayOptimizationUtils", stringBuilder.toString());
        }  
    } finally {}
  }
  
  private String getThis() {
    return toString();
  }
  
  public boolean isDisplayOptimizationAndSwitchEnabled() {
    // Byte code:
    //   0: iconst_0
    //   1: istore_1
    //   2: aload_0
    //   3: getfield mDisplayOptEnableLock : Ljava/lang/Object;
    //   6: astore_2
    //   7: aload_2
    //   8: monitorenter
    //   9: iload_1
    //   10: istore_3
    //   11: aload_0
    //   12: getfield mEnableDisplatOpt : Z
    //   15: ifeq -> 29
    //   18: iload_1
    //   19: istore_3
    //   20: aload_0
    //   21: getfield mEnableGraphicAccelerationSwitch : Z
    //   24: ifeq -> 29
    //   27: iconst_1
    //   28: istore_3
    //   29: aload_2
    //   30: monitorexit
    //   31: iload_3
    //   32: ireturn
    //   33: astore #4
    //   35: aload_2
    //   36: monitorexit
    //   37: aload #4
    //   39: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #346	-> 0
    //   #347	-> 2
    //   #348	-> 9
    //   #349	-> 27
    //   #351	-> 29
    //   #353	-> 31
    //   #351	-> 33
    // Exception table:
    //   from	to	target	type
    //   11	18	33	finally
    //   20	27	33	finally
    //   29	31	33	finally
    //   35	37	33	finally
  }
  
  public boolean isOnlyDisplayOptimizationEnabled() {
    boolean bool = false;
    synchronized (this.mDisplayOptEnableLock) {
      if (this.mEnableDisplatOpt)
        bool = true; 
      return bool;
    } 
  }
  
  public boolean inBlackPkgList(String paramString) {
    boolean bool = false;
    synchronized (this.mDisplayOptBlackListLock) {
      if (this.mBlackList.contains(paramString))
        bool = true; 
      return bool;
    } 
  }
  
  public boolean inWhitePkgList(String paramString) {
    boolean bool = false;
    synchronized (this.mDisplayOptWhiteListLock) {
      if (this.mWhiteList.contains(paramString))
        bool = true; 
      return bool;
    } 
  }
  
  public boolean inSpecialPkgList(String paramString) {
    boolean bool = false;
    synchronized (this.mDisplayOptSpeicalListLock) {
      if (this.mSpecialList.contains(paramString))
        bool = true; 
      return bool;
    } 
  }
  
  public boolean shouldOptimizeForPkg(String paramString) {
    boolean bool;
    if ((isOnlyDisplayOptimizationEnabled() && inSpecialPkgList(paramString)) || (
      isDisplayOptimizationAndSwitchEnabled() && considerPkgAccordingPolicy(paramString))) {
      bool = true;
    } else {
      bool = false;
    } 
    if (DEBUG_SWITCH) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("shouldOptimize = ");
      stringBuilder.append(bool);
      stringBuilder.append(",pkg = ");
      stringBuilder.append(paramString);
      Slog.i("OplusDisplayOptimizationUtils", stringBuilder.toString());
    } 
    return bool;
  }
  
  public boolean considerPkgAccordingPolicy(String paramString) {
    int i = 0;
    synchronized (this.mDisplayOptPolicyLock) {
      boolean bool;
      int j = this.mEnablePolicy;
      if (j != 0) {
        if (j == 1)
          i = inBlackPkgList(paramString) ^ true; 
      } else {
        bool = inWhitePkgList(paramString);
      } 
      return bool;
    } 
  }
  
  public boolean inExcludeProcessList(String paramString) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_2
    //   2: aload_0
    //   3: getfield mDisplayOptExcludeProcessListLock : Ljava/lang/Object;
    //   6: astore_3
    //   7: aload_3
    //   8: monitorenter
    //   9: iload_2
    //   10: istore #4
    //   12: aload_1
    //   13: ifnull -> 70
    //   16: aload_0
    //   17: getfield mExcludeProcessList : Ljava/util/List;
    //   20: invokeinterface iterator : ()Ljava/util/Iterator;
    //   25: astore #5
    //   27: iload_2
    //   28: istore #4
    //   30: aload #5
    //   32: invokeinterface hasNext : ()Z
    //   37: ifeq -> 70
    //   40: aload #5
    //   42: invokeinterface next : ()Ljava/lang/Object;
    //   47: checkcast java/lang/String
    //   50: astore #6
    //   52: aload_1
    //   53: aload #6
    //   55: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   58: ifeq -> 67
    //   61: iconst_1
    //   62: istore #4
    //   64: goto -> 70
    //   67: goto -> 27
    //   70: aload_3
    //   71: monitorexit
    //   72: iload #4
    //   74: ireturn
    //   75: astore_1
    //   76: aload_3
    //   77: monitorexit
    //   78: aload_1
    //   79: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #432	-> 0
    //   #433	-> 2
    //   #434	-> 9
    //   #435	-> 16
    //   #436	-> 52
    //   #437	-> 61
    //   #438	-> 64
    //   #440	-> 67
    //   #442	-> 70
    //   #444	-> 72
    //   #442	-> 75
    // Exception table:
    //   from	to	target	type
    //   16	27	75	finally
    //   30	52	75	finally
    //   52	61	75	finally
    //   70	72	75	finally
    //   76	78	75	finally
  }
  
  public boolean inExcludeWindowList(String paramString) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_2
    //   2: aload_0
    //   3: getfield mDisplayOptExcludeWindowListLock : Ljava/lang/Object;
    //   6: astore_3
    //   7: aload_3
    //   8: monitorenter
    //   9: iload_2
    //   10: istore #4
    //   12: aload_1
    //   13: ifnull -> 70
    //   16: aload_0
    //   17: getfield mExcludeWindowList : Ljava/util/List;
    //   20: invokeinterface iterator : ()Ljava/util/Iterator;
    //   25: astore #5
    //   27: iload_2
    //   28: istore #4
    //   30: aload #5
    //   32: invokeinterface hasNext : ()Z
    //   37: ifeq -> 70
    //   40: aload #5
    //   42: invokeinterface next : ()Ljava/lang/Object;
    //   47: checkcast java/lang/String
    //   50: astore #6
    //   52: aload_1
    //   53: aload #6
    //   55: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   58: ifeq -> 67
    //   61: iconst_1
    //   62: istore #4
    //   64: goto -> 70
    //   67: goto -> 27
    //   70: aload_3
    //   71: monitorexit
    //   72: iload #4
    //   74: ireturn
    //   75: astore_1
    //   76: aload_3
    //   77: monitorexit
    //   78: aload_1
    //   79: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #448	-> 0
    //   #449	-> 2
    //   #450	-> 9
    //   #451	-> 16
    //   #452	-> 52
    //   #453	-> 61
    //   #454	-> 64
    //   #456	-> 67
    //   #458	-> 70
    //   #460	-> 72
    //   #458	-> 75
    // Exception table:
    //   from	to	target	type
    //   16	27	75	finally
    //   30	52	75	finally
    //   52	61	75	finally
    //   70	72	75	finally
    //   76	78	75	finally
  }
  
  public boolean shouldExcludeForProcess(String paramString) {
    boolean bool;
    if (isOnlyDisplayOptimizationEnabled() && inExcludeProcessList(paramString)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean shouldExcludeForWindow(String paramString) {
    boolean bool;
    if (isOnlyDisplayOptimizationEnabled() && inExcludeWindowList(paramString)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void loadDefaultDisplayOptList() {
    if (DEBUG_SWITCH)
      Slog.i("OplusDisplayOptimizationUtils", "loadDefaultDisplayOptList"); 
    synchronized (this.mDisplayOptWhiteListLock) {
      synchronized (this.mDisplayOptBlackListLock) {
        this.mBlackList.clear();
        this.mBlackList.add("com.oppo.launcher");
        this.mBlackList.add("com.android.launcher");
        synchronized (this.mDisplayOptSpeicalListLock) {
          synchronized (this.mDisplayOptExcludeProcessListLock) {
            synchronized (this.mDisplayOptExcludeWindowListLock) {
              return;
            } 
          } 
        } 
      } 
    } 
  }
  
  public void initData() {
    try {
      OplusActivityManager oplusActivityManager = new OplusActivityManager();
      this();
      OplusDisplayOptimizationData oplusDisplayOptimizationData = oplusActivityManager.getDisplayOptimizationData();
      if (this.mWhiteList != null) {
        this.mWhiteList.clear();
        this.mWhiteList = oplusDisplayOptimizationData.getWhiteList();
      } 
      if (this.mBlackList != null) {
        this.mBlackList.clear();
        this.mBlackList = oplusDisplayOptimizationData.getBlackList();
      } 
      if (this.mSpecialList != null) {
        this.mSpecialList.clear();
        this.mSpecialList = oplusDisplayOptimizationData.getSpecialList();
      } 
      if (this.mExcludeWindowList != null) {
        this.mExcludeWindowList.clear();
        this.mExcludeWindowList = oplusDisplayOptimizationData.getExcludeWindowList();
      } 
      if (this.mExcludeProcessList != null) {
        this.mExcludeProcessList.clear();
        this.mExcludeProcessList = oplusDisplayOptimizationData.getExcludeProcessList();
      } 
      this.mEnableDisplatOpt = oplusDisplayOptimizationData.getDisplatOptEnabled();
      this.mEnableGraphicAccelerationSwitch = oplusDisplayOptimizationData.getGraphicAccelerationSwitchEnabled();
      this.mEnablePolicy = oplusDisplayOptimizationData.getEnablePolicy();
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("init data error , ");
      stringBuilder.append(remoteException);
      Slog.e("OplusDisplayOptimizationUtils", stringBuilder.toString());
    } 
  }
  
  class FileObserverPolicy extends FileObserver {
    private String mFocusPath;
    
    final OplusDisplayOptimizationUtils this$0;
    
    public FileObserverPolicy(String param1String) {
      super(param1String, 8);
      this.mFocusPath = param1String;
    }
    
    public void onEvent(int param1Int, String param1String) {
      if (param1Int == 8 && 
        this.mFocusPath.equals("/data/oppo/coloros/gamespace/sys_display_opt_config.xml")) {
        Slog.i("OplusDisplayOptimizationUtils", "focusPath COLOR_DISPLAY_OPTIMIZATION_CONFIG_FILE_PATH!");
        OplusDisplayOptimizationUtils.this.readDisplayOptConfig();
      } 
    }
  }
  
  private void updateGraphicAccelerationSwitch() {
    boolean bool1;
    Context context = this.mContext;
    if (context == null)
      return; 
    try {
      bool1 = Settings.Global.getInt(context.getContentResolver(), "graphics_acceleration_for_game_space_mode");
      if (DEBUG_SWITCH) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("reading Settings result = ");
        stringBuilder.append(bool1);
        Slog.i("OplusDisplayOptimizationUtils", stringBuilder.toString());
      } 
    } catch (android.provider.Settings.SettingNotFoundException settingNotFoundException) {
      boolean bool = true;
      bool1 = bool;
      if (DEBUG_SWITCH) {
        Slog.i("OplusDisplayOptimizationUtils", "SettingNotFoundException");
        bool1 = bool;
      } 
    } 
    boolean bool2 = true;
    if (bool1 != true)
      bool2 = false; 
    this.mEnableGraphicAccelerationSwitch = bool2;
    OplusDisplayOptimizationData oplusDisplayOptimizationData = sOptimizationData;
    if (oplusDisplayOptimizationData != null)
      oplusDisplayOptimizationData.setGraphicAccelerationSwitchEnabled(bool2); 
    if (DEBUG_SWITCH) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("updateGraphicAccelerationSwitch = ");
      stringBuilder.append(this.mEnableGraphicAccelerationSwitch);
      Slog.i("OplusDisplayOptimizationUtils", stringBuilder.toString());
    } 
  }
  
  class SwitchObserverPolicy extends ContentObserver {
    final OplusDisplayOptimizationUtils this$0;
    
    public SwitchObserverPolicy() {
      super(new Handler());
    }
    
    public void onChange(boolean param1Boolean) {
      OplusDisplayOptimizationUtils.this.updateGraphicAccelerationSwitch();
      super.onChange(param1Boolean);
    }
  }
}
