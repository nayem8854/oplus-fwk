package com.oplus.util;

public class OplusChangeTextUtil {
  public static final int G1 = 1;
  
  public static final int G2 = 2;
  
  public static final int G3 = 3;
  
  public static final int G4 = 4;
  
  public static final int G5 = 5;
  
  public static final int GN = 6;
  
  private static final float H1 = 0.9F;
  
  private static final float H2 = 1.0F;
  
  private static final float H3 = 1.1F;
  
  private static final float H4 = 1.25F;
  
  private static final float H5 = 1.45F;
  
  private static final float H6 = 1.65F;
  
  public static final float[] SCALE_LEVEL = new float[] { 0.9F, 1.0F, 1.1F, 1.25F, 1.45F, 1.65F };
  
  private static final String TAG = "OplusChangeTextUtil";
  
  public static float getSuitableFontSize(float paramFloat1, float paramFloat2, int paramInt) {
    if (paramInt < 2)
      return paramFloat1; 
    float[] arrayOfFloat = SCALE_LEVEL;
    int i = paramInt;
    if (paramInt > arrayOfFloat.length)
      i = arrayOfFloat.length; 
    paramFloat1 /= paramFloat2;
    if (i != 2) {
      if (i != 3) {
        arrayOfFloat = SCALE_LEVEL;
        if (paramFloat2 > arrayOfFloat[i - 1])
          return arrayOfFloat[i - 1] * paramFloat1; 
        return paramFloat1 * paramFloat2;
      } 
      if (paramFloat2 < 1.1F)
        return 1.0F * paramFloat1; 
      if (paramFloat2 < 1.45F)
        return 1.1F * paramFloat1; 
      return 1.25F * paramFloat1;
    } 
    if (paramFloat2 < 1.1F)
      return 1.0F * paramFloat1; 
    return 1.1F * paramFloat1;
  }
  
  private static float getSuitableFontScale(float paramFloat, int paramInt) {
    if (paramInt < 2)
      return paramFloat; 
    float[] arrayOfFloat = SCALE_LEVEL;
    int i = paramInt;
    if (paramInt > arrayOfFloat.length)
      i = arrayOfFloat.length; 
    if (i != 2) {
      if (i != 3) {
        arrayOfFloat = SCALE_LEVEL;
        if (paramFloat > arrayOfFloat[i - 1])
          return arrayOfFloat[i - 1]; 
        return paramFloat;
      } 
      if (paramFloat < 1.1F)
        return 1.0F; 
      if (paramFloat < 1.45F)
        return 1.1F; 
      return 1.25F;
    } 
    if (paramFloat < 1.1F)
      return 1.0F; 
    return 1.1F;
  }
}
