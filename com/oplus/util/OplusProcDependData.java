package com.oplus.util;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public final class OplusProcDependData implements Parcelable {
  public static final Parcelable.Creator<OplusProcDependData> CREATOR = new Parcelable.Creator<OplusProcDependData>() {
      public OplusProcDependData createFromParcel(Parcel param1Parcel) {
        return new OplusProcDependData(param1Parcel);
      }
      
      public OplusProcDependData[] newArray(int param1Int) {
        return new OplusProcDependData[param1Int];
      }
    };
  
  public int mPid = -1;
  
  public int mUid = -1;
  
  public String mProcessName = null;
  
  public String mPackageName = null;
  
  public List<ProcItem> mServices = new ArrayList<>();
  
  public List<ProcItem> mClients = new ArrayList<>();
  
  public OplusProcDependData(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("pid=");
    stringBuilder.append(this.mPid);
    stringBuilder.append(",");
    stringBuilder.append("uid=");
    stringBuilder.append(this.mUid);
    stringBuilder.append(",");
    stringBuilder.append("pkg=");
    stringBuilder.append(this.mPackageName);
    stringBuilder.append(",");
    stringBuilder.append("proc=");
    stringBuilder.append(this.mProcessName);
    stringBuilder.append(",");
    stringBuilder.append("depend ON=");
    stringBuilder.append(this.mServices);
    stringBuilder.append(",");
    stringBuilder.append("depend BY=");
    stringBuilder.append(this.mClients);
    stringBuilder.append(",");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mPid);
    paramParcel.writeInt(this.mUid);
    paramParcel.writeString(this.mProcessName);
    paramParcel.writeString(this.mPackageName);
    paramParcel.writeTypedList(this.mServices);
    paramParcel.writeTypedList(this.mClients);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mPid = paramParcel.readInt();
    this.mUid = paramParcel.readInt();
    this.mProcessName = paramParcel.readString();
    this.mPackageName = paramParcel.readString();
    ArrayList<ProcItem> arrayList = new ArrayList();
    paramParcel.readTypedList(arrayList, ProcItem.CREATOR);
    this.mClients = arrayList = new ArrayList<>();
    paramParcel.readTypedList(arrayList, ProcItem.CREATOR);
  }
  
  public OplusProcDependData() {}
  
  public static final class ProcItem implements Parcelable {
    public static final Parcelable.Creator<ProcItem> CREATOR = new Parcelable.Creator<ProcItem>() {
        public OplusProcDependData.ProcItem createFromParcel(Parcel param2Parcel) {
          return new OplusProcDependData.ProcItem(param2Parcel);
        }
        
        public OplusProcDependData.ProcItem[] newArray(int param2Int) {
          return new OplusProcDependData.ProcItem[param2Int];
        }
      };
    
    public int pid = -1;
    
    public int uid = -1;
    
    public String processName = null;
    
    public String packageName = null;
    
    public ProcItem(int param1Int1, int param1Int2, String param1String1, String param1String2) {
      this.pid = param1Int2;
      this.uid = param1Int1;
      this.processName = param1String2;
      this.packageName = param1String1;
    }
    
    public ProcItem(Parcel param1Parcel) {
      readFromParcel(param1Parcel);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this.pid);
      stringBuilder.append("+");
      int i = this.uid;
      stringBuilder.append(i);
      stringBuilder.append("+");
      String str = this.packageName;
      stringBuilder.append(str);
      stringBuilder.append("+");
      str = this.processName;
      stringBuilder.append(str);
      return stringBuilder.toString();
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = false;
      if (param1Object == null)
        return false; 
      if (param1Object instanceof ProcItem) {
        if (this.pid == ((ProcItem)param1Object).pid && this.uid == ((ProcItem)param1Object).uid)
          return true; 
        String str = this.packageName;
        if (str != null && str.equals(((ProcItem)param1Object).packageName)) {
          str = this.processName;
          if (str != null) {
            param1Object = ((ProcItem)param1Object).processName;
            if (str.equals(param1Object))
              bool = true; 
          } 
        } 
        return bool;
      } 
      return false;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.pid);
      param1Parcel.writeInt(this.uid);
      param1Parcel.writeString(this.processName);
      param1Parcel.writeString(this.packageName);
    }
    
    public void readFromParcel(Parcel param1Parcel) {
      this.pid = param1Parcel.readInt();
      this.uid = param1Parcel.readInt();
      this.processName = param1Parcel.readString();
      this.packageName = param1Parcel.readString();
    }
    
    public ProcItem() {}
  }
  
  class null implements Parcelable.Creator<ProcItem> {
    public OplusProcDependData.ProcItem createFromParcel(Parcel param1Parcel) {
      return new OplusProcDependData.ProcItem(param1Parcel);
    }
    
    public OplusProcDependData.ProcItem[] newArray(int param1Int) {
      return new OplusProcDependData.ProcItem[param1Int];
    }
  }
}
