package com.oplus.util;

import android.graphics.OplusTypefaceInjector;
import android.graphics.Typeface;
import android.os.SystemProperties;
import android.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class OplusBaseFontUtils {
  public static String DATA_FONT_DIRECTORY;
  
  protected static final String DATA_FONT_DIRECTORY_5D0 = "/data/system/font/";
  
  public static final String DATA_FONT_DIRECTORY_6D0 = "/data/format_unclear/font/";
  
  protected static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  protected static final List<String> DEFAULT_OPLUS_FONT_SYSTEM_LINKS;
  
  protected static final String FLIPED_CUSTOMIZED_FONT_NAME = "Customized-Regular.ttf";
  
  protected static final String FLIPED_OPLUSOS_FONT_NAME = "ColorOS-Regular.ttf";
  
  public static boolean FLIP_APP_ALL_FONTS = false;
  
  protected static final int FLIP_FONT_FLAG_MAX = 10001;
  
  public static final List<String> FLITER_CTS_APP_PKG_LIST;
  
  public static final List<String> FLITER_NOT_REPLACEFONT_APP_PKG_LIST;
  
  protected static final FontLinkInfo[] FONTINFOARRAY_ROM6D0;
  
  public static final int FONT_VARIATION_DEFAULT = 550;
  
  public static final int FONT_VARIATION_END = 1000;
  
  public static final int FONT_VARIATION_NIGHT = -50;
  
  public static final String FONT_VARIATION_SETTINGS = "font_variation_settings";
  
  public static final int FONT_VARIATION_START = 100;
  
  public static final int FONT_VARIATION_STEP = 10;
  
  public static final String FONT_VARIATION_WEIGHT = "'wght' ";
  
  public static final int FONT_WEIGHT_BOLD = 700;
  
  public static final Map<Integer, Integer> FONT_WEIGHT_CAST_WGHT;
  
  public static final int FONT_WEIGHT_MEDIUM = 500;
  
  public static final int FONT_WEIGHT_NORMAL = 400;
  
  protected static final int INVALID_FLIP_FONT = -1;
  
  protected static final int LINK_TARGET_FLIPFONT = 2;
  
  protected static final int LINK_TARGET_SYSTEM = 1;
  
  public static String OPLUS_SANS_VARIATION_FONT;
  
  public static final String SECOND_FONT_CONFIG_FILE = "/system_ext/etc/fonts_base.xml";
  
  public static final int STATUS_AUTO = 2;
  
  public static final int STATUS_DISABLE = 0;
  
  public static final int STATUS_ENABLE = 1;
  
  protected static final List<String> SUPPORT_FONT_VARIATION_LIST;
  
  protected static final List<String> SUPPORT_MEDIUM_FONT_LANGUAGE_LIST;
  
  protected static final String SYSTEM_FONT_DIRECTORY = "/system/fonts/";
  
  protected static final String TAG = "OppoFontUtils";
  
  public static boolean isCurrentLanguageSupportMediumFont;
  
  public static boolean isCurrentLanguageSupportVariationFont;
  
  public static boolean isFlipFontUsed;
  
  public static boolean isSearched;
  
  public static int mFontVariation;
  
  public static int mFontVariationAdaption;
  
  public static String mFontVariationSettings;
  
  public static int mFontVariationStatus;
  
  public static String mPackageName;
  
  protected static Typeface[] sCurrentTypefaces;
  
  protected static List<Typeface> sCurrentTypefacesArray;
  
  public static int sFlipFont;
  
  protected static List<FontLinkInfo> sFontLinkInfos;
  
  public static boolean sIsCheckCTS;
  
  public static final boolean sIsROM6d0FlipFont = OplusTypefaceInjector.sIsFBESupport;
  
  protected static boolean sNeedReplaceAllTypefaceApp;
  
  public static ConcurrentHashMap<String, Typeface> sOplusVariationCacheMap;
  
  public static boolean sReplaceFont;
  
  public static int sUserId;
  
  static {
    String str;
  }
  
  static {
    FLITER_CTS_APP_PKG_LIST = new ArrayList<>(Arrays.asList(new String[] { "android.theme.app", "android.graphics.cts", "android.widget.cts", "android.uirendering.cts", "android.text.cts" }));
    FLITER_NOT_REPLACEFONT_APP_PKG_LIST = new ArrayList<>(Arrays.asList(new String[] { "com.eterno" }));
    if (sIsROM6d0FlipFont) {
      str = "/data/format_unclear/font/";
    } else {
      str = "/data/system/font/";
    } 
    DATA_FONT_DIRECTORY = str;
    sCurrentTypefaces = null;
    sCurrentTypefacesArray = null;
    isFlipFontUsed = false;
    FLIP_APP_ALL_FONTS = false;
    sIsCheckCTS = false;
    sReplaceFont = true;
    sNeedReplaceAllTypefaceApp = false;
    DEFAULT_OPLUS_FONT_SYSTEM_LINKS = new ArrayList<>(Arrays.asList(new String[] { 
            "/system/fonts/SysFont-Thin.ttf", "/system/fonts/SysFont-ThinItalic.ttf", "/system/fonts/SysFont-Light.ttf", "/system/fonts/SysFont-LightItalic.ttf", "/system/fonts/SysFont-Regular.ttf", "/system/fonts/SysFont-Italic.ttf", "/system/fonts/SysFont-Medium.ttf", "/system/fonts/SysFont-MediumItalic.ttf", "/system/fonts/SysFont-Black.ttf", "/system/fonts/SysFont-BlackItalic.ttf", 
            "/system/fonts/SysFont-Bold.ttf", "/system/fonts/SysFont-BoldItalic.ttf", "/system/fonts/SysFont-Myanmar.ttf" }));
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(DATA_FONT_DIRECTORY);
    stringBuilder1.append("OplusOSUI-Thin.ttf");
    FontLinkInfo fontLinkInfo1 = new FontLinkInfo(stringBuilder1.toString(), "/system/fonts/Roboto-Thin.ttf");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(DATA_FONT_DIRECTORY);
    stringBuilder2.append("OplusOSUI-ThinItalic.ttf");
    FontLinkInfo fontLinkInfo2 = new FontLinkInfo(stringBuilder2.toString(), "/system/fonts/Roboto-ThinItalic.ttf");
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append(DATA_FONT_DIRECTORY);
    stringBuilder3.append("OplusOSUI-Light.ttf");
    FontLinkInfo fontLinkInfo3 = new FontLinkInfo(stringBuilder3.toString(), "/system/fonts/Roboto-Light.ttf");
    StringBuilder stringBuilder4 = new StringBuilder();
    stringBuilder4.append(DATA_FONT_DIRECTORY);
    stringBuilder4.append("OplusOSUI-LightItalic.ttf");
    FontLinkInfo fontLinkInfo4 = new FontLinkInfo(stringBuilder4.toString(), "/system/fonts/Roboto-LightItalic.ttf");
    StringBuilder stringBuilder5 = new StringBuilder();
    stringBuilder5.append(DATA_FONT_DIRECTORY);
    stringBuilder5.append("OplusOSUI-Regular.ttf");
    FontLinkInfo fontLinkInfo5 = new FontLinkInfo(stringBuilder5.toString(), "/system/fonts/Roboto-Regular.ttf");
    StringBuilder stringBuilder6 = new StringBuilder();
    stringBuilder6.append(DATA_FONT_DIRECTORY);
    stringBuilder6.append("OplusOSUI-Italic.ttf");
    FontLinkInfo fontLinkInfo6 = new FontLinkInfo(stringBuilder6.toString(), "/system/fonts/Roboto-Italic.ttf");
    StringBuilder stringBuilder7 = new StringBuilder();
    stringBuilder7.append(DATA_FONT_DIRECTORY);
    stringBuilder7.append("OplusOSUI-Medium.ttf");
    FontLinkInfo fontLinkInfo7 = new FontLinkInfo(stringBuilder7.toString(), "/system/fonts/Roboto-Medium.ttf");
    StringBuilder stringBuilder8 = new StringBuilder();
    stringBuilder8.append(DATA_FONT_DIRECTORY);
    stringBuilder8.append("OplusOSUI-MediumItalic.ttf");
    FontLinkInfo fontLinkInfo8 = new FontLinkInfo(stringBuilder8.toString(), "/system/fonts/Roboto-MediumItalic.ttf");
    StringBuilder stringBuilder9 = new StringBuilder();
    stringBuilder9.append(DATA_FONT_DIRECTORY);
    stringBuilder9.append("OplusOSUI-Black.ttf");
    FontLinkInfo fontLinkInfo9 = new FontLinkInfo(stringBuilder9.toString(), "/system/fonts/Roboto-Black.ttf");
    StringBuilder stringBuilder10 = new StringBuilder();
    stringBuilder10.append(DATA_FONT_DIRECTORY);
    stringBuilder10.append("OplusOSUI-BlackItalic.ttf");
    FontLinkInfo fontLinkInfo10 = new FontLinkInfo(stringBuilder10.toString(), "/system/fonts/Roboto-BlackItalic.ttf");
    StringBuilder stringBuilder11 = new StringBuilder();
    stringBuilder11.append(DATA_FONT_DIRECTORY);
    stringBuilder11.append("OplusOSUI-Bold.ttf");
    FontLinkInfo fontLinkInfo11 = new FontLinkInfo(stringBuilder11.toString(), "/system/fonts/Roboto-Bold.ttf");
    stringBuilder11 = new StringBuilder();
    stringBuilder11.append(DATA_FONT_DIRECTORY);
    stringBuilder11.append("OplusOSUI-BoldItalic.ttf");
    FONTINFOARRAY_ROM6D0 = new FontLinkInfo[] { 
        fontLinkInfo1, fontLinkInfo2, fontLinkInfo3, fontLinkInfo4, fontLinkInfo5, fontLinkInfo6, fontLinkInfo7, fontLinkInfo8, fontLinkInfo9, fontLinkInfo10, 
        fontLinkInfo11, new FontLinkInfo(stringBuilder11.toString(), "/system/fonts/Roboto-BoldItalic.ttf") };
    sFontLinkInfos = new ArrayList<>(Arrays.asList(FONTINFOARRAY_ROM6D0));
    SUPPORT_MEDIUM_FONT_LANGUAGE_LIST = new ArrayList<>(Arrays.asList(new String[] { 
            "en", "zh", "ja", "ko", "fr", "it", "de", "sv", "nl", "es", 
            "ru", "kk" }));
    SUPPORT_FONT_VARIATION_LIST = new ArrayList<>(Arrays.asList(new String[] { "en", "zh" }));
    sOplusVariationCacheMap = new ConcurrentHashMap<>();
    mFontVariationSettings = "'wght' 550";
    OPLUS_SANS_VARIATION_FONT = "sys-sans-en";
    FONT_WEIGHT_CAST_WGHT = Map.of(Integer.valueOf(100), Integer.valueOf(200), Integer.valueOf(300), Integer.valueOf(350), Integer.valueOf(400), Integer.valueOf(550), Integer.valueOf(500), Integer.valueOf(550), Integer.valueOf(700), Integer.valueOf(750), Integer.valueOf(900), Integer.valueOf(1000));
    mFontVariationAdaption = 0;
    mFontVariation = 0;
    mFontVariationStatus = 0;
  }
  
  protected static class FontLinkInfo {
    String mDataFontName;
    
    String mSystemFontName;
    
    FontLinkInfo(String param1String1, String param1String2) {
      this.mDataFontName = param1String1;
      this.mSystemFontName = param1String2;
    }
  }
  
  public static void deleteFontLink(String paramString) {}
  
  public static void createFontLink(String paramString) {}
  
  protected static void logd(String paramString) {
    if (DEBUG)
      Log.d("OppoFontUtils", paramString); 
  }
  
  protected static void loge(String paramString, Throwable paramThrowable) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append(":");
      stringBuilder.append(paramThrowable.getMessage());
      Log.e("OppoFontUtils", stringBuilder.toString(), paramThrowable);
    } 
  }
}
