package com.oplus.util;

public class OplusInnerTransaction {
  private static final int ACTIVITYMANAGER = 0;
  
  private static final int ACTIVITYTASKMANAGER = 0;
  
  public static final int IOPLUSACTIVITYMANAGER = 0;
  
  public static final int IOPLUSACTIVITYTASKMANAGER = 0;
  
  public static final int IOPLUSIRECTWINDOWMANAGER = 2;
  
  public static final int IOPLUSLONGSHOTWINDOWMANAGER = 1;
  
  public static final int IOPLUSRPACKAGEMANAGER = 0;
  
  public static final int IOPLUSRWINDOWMANAGER = 0;
  
  private static final int PACKAGEMANAGER = 0;
  
  private static final int WINDOWMANAGER = 0;
}
