package com.oplus.util;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public final class OplusPackageFreezeData implements Parcelable {
  public static final Parcelable.Creator<OplusPackageFreezeData> CREATOR = new Parcelable.Creator<OplusPackageFreezeData>() {
      public OplusPackageFreezeData createFromParcel(Parcel param1Parcel) {
        return new OplusPackageFreezeData(param1Parcel);
      }
      
      public OplusPackageFreezeData[] newArray(int param1Int) {
        return new OplusPackageFreezeData[param1Int];
      }
    };
  
  private int mPid = 0;
  
  private int mUid = 0;
  
  private int mCurAdj = 10000;
  
  private int mUserId = 0;
  
  private String mProcessName = "";
  
  private List<String> mPackageList = new ArrayList<>();
  
  public OplusPackageFreezeData(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mPid);
    paramParcel.writeInt(this.mUid);
    paramParcel.writeInt(this.mCurAdj);
    paramParcel.writeInt(this.mUserId);
    paramParcel.writeString(this.mProcessName);
    paramParcel.writeStringList(this.mPackageList);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mPid = paramParcel.readInt();
    this.mUid = paramParcel.readInt();
    this.mCurAdj = paramParcel.readInt();
    this.mUserId = paramParcel.readInt();
    this.mProcessName = paramParcel.readString();
    this.mPackageList = paramParcel.createStringArrayList();
  }
  
  public void setPid(int paramInt) {
    this.mPid = paramInt;
  }
  
  public int getPid() {
    return this.mPid;
  }
  
  public void setUid(int paramInt) {
    this.mUid = paramInt;
  }
  
  public int getUid() {
    return this.mUid;
  }
  
  public void setCurAdj(int paramInt) {
    this.mCurAdj = paramInt;
  }
  
  public int getCurAdj() {
    return this.mCurAdj;
  }
  
  public void setUserId(int paramInt) {
    this.mUserId = paramInt;
  }
  
  public int getUserId() {
    return this.mUserId;
  }
  
  public void setProcessName(String paramString) {
    this.mProcessName = paramString;
  }
  
  public String getProcessName() {
    return this.mProcessName;
  }
  
  public void setPackageList(List<String> paramList) {
    this.mPackageList.clear();
    if (paramList != null)
      this.mPackageList.addAll(paramList); 
  }
  
  public List<String> getPackageList() {
    return this.mPackageList;
  }
  
  public OplusPackageFreezeData() {}
}
