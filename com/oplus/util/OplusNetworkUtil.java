package com.oplus.util;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothPan;
import android.bluetooth.BluetoothProfile;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import oplus.util.OplusNetUtils;

public class OplusNetworkUtil {
  public static final int AIRPLANE_MODE_ON_STR = 0;
  
  private static final String DEFAULT_HTTP_URL = "http://connectivitycheck.gstatic.com/generate_204";
  
  private static final String KEY_NETWORK_MONITOR_AVAILABLE = "oppo.comm.network.monitor.available";
  
  private static final String KEY_NETWORK_MONITOR_PORTAL = "oppo.comm.network.monitor.portal";
  
  private static final String KEY_NETWORK_MONITOR_SSID = "oppo.comm.network.monitor.ssid";
  
  public static final int MOBILE_AND_WLAN_NETWORK_NOT_CONNECT_STR = 1;
  
  public static final int NETWORK_CONNECT_OK_STR = -1;
  
  public static final int NO_NETWORK_CONNECT_STR = 3;
  
  public static final String TAG = "OplusNetworkUtil";
  
  public static final int WLAN_NEED_LOGIN_STR = 2;
  
  private static String mCurrSSID;
  
  private static boolean mIsBluetoothTetherConnected;
  
  private static BluetoothProfile.ServiceListener mProfileServiceListener = new ProfileServiceListener();
  
  private static BluetoothPan mService;
  
  static {
    mIsBluetoothTetherConnected = false;
  }
  
  public static boolean isWifiConnected(Context paramContext) {
    ConnectivityManager connectivityManager = (ConnectivityManager)paramContext.getSystemService("connectivity");
    if (connectivityManager.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
      WifiManager wifiManager = (WifiManager)paramContext.getSystemService("wifi");
      WifiInfo wifiInfo = wifiManager.getConnectionInfo();
      if (wifiInfo != null)
        mCurrSSID = wifiInfo.getSSID(); 
      return true;
    } 
    return false;
  }
  
  public static boolean isMobileDataConnected(Context paramContext) {
    ConnectivityManager connectivityManager = (ConnectivityManager)paramContext.getSystemService("connectivity");
    if (connectivityManager.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED)
      return true; 
    return false;
  }
  
  public static boolean isSimInserted(Context paramContext, int paramInt) {
    TelephonyManager telephonyManager = (TelephonyManager)paramContext.getSystemService("phone");
    return telephonyManager.hasIccCard();
  }
  
  private static boolean hasSimCard(Context paramContext) {
    boolean bool = false;
    try {
      if (!isSimInserted(paramContext, 0)) {
        boolean bool1 = isSimInserted(paramContext, 1);
        if (bool1)
          bool = true; 
        return bool;
      } 
      bool = true;
    } catch (Exception exception) {
      bool = false;
      Log.e("OplusNetworkUtil", "", exception);
    } 
    return bool;
  }
  
  public static void onClickLoginBtn(Context paramContext) {
    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("http://connectivitycheck.gstatic.com/generate_204"));
    intent.setFlags(272629760);
    paramContext.startActivity(intent);
  }
  
  public static Boolean isAirplaneMode(Context paramContext) {
    ContentResolver contentResolver = paramContext.getContentResolver();
    boolean bool = false;
    if (Settings.Global.getInt(contentResolver, "airplane_mode_on", 0) != 0)
      bool = true; 
    return Boolean.valueOf(bool);
  }
  
  private static int getCaptivePortalStr(Context paramContext, String paramString) {
    paramString = Settings.Global.getString(paramContext.getContentResolver(), "oppo.comm.network.monitor.ssid");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("mCurrSSID=");
    stringBuilder.append(OplusNetUtils.normalStrMask(mCurrSSID));
    stringBuilder.append(", ssid=");
    stringBuilder.append(OplusNetUtils.normalStrMask(paramString));
    Log.d("OplusNetworkUtil", stringBuilder.toString());
    String str = mCurrSSID;
    if (str != null && paramString != null)
      if (str.equals(paramString)) {
        boolean bool2;
        ContentResolver contentResolver = paramContext.getContentResolver();
        boolean bool1 = false;
        if (Settings.Global.getInt(contentResolver, "oppo.comm.network.monitor.available", 0) == 1) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        if (bool2)
          return -1; 
        if (Settings.Global.getInt(paramContext.getContentResolver(), "oppo.comm.network.monitor.portal", 0) == 1) {
          bool2 = true;
        } else {
          bool2 = bool1;
        } 
        if (bool2)
          return 2; 
        return 3;
      }  
    return 3;
  }
  
  public static int getErrorString(Context paramContext, String paramString) {
    getBluetoothTether(paramContext);
    try {
      Thread.sleep(100L);
    } catch (InterruptedException interruptedException) {}
    closeProxy();
    if (isAirplaneMode(paramContext).booleanValue() && !isWifiConnected(paramContext) && !mIsBluetoothTetherConnected)
      return 0; 
    if (isWifiConnected(paramContext) || mIsBluetoothTetherConnected)
      return getCaptivePortalStr(paramContext, paramString); 
    if (hasSimCard(paramContext)) {
      if (isMobileDataConnected(paramContext))
        return -1; 
      return 1;
    } 
    return 3;
  }
  
  public static void getBluetoothTether(Context paramContext) {
    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    if (bluetoothAdapter != null) {
      int i = bluetoothAdapter.getState();
      if (i == 12)
        bluetoothAdapter.getProfileProxy(paramContext.getApplicationContext(), mProfileServiceListener, 5); 
    } 
  }
  
  class ProfileServiceListener implements BluetoothProfile.ServiceListener {
    private ProfileServiceListener() {}
    
    public void onServiceConnected(int param1Int, BluetoothProfile param1BluetoothProfile) {
      AtomicReference<BluetoothPan> atomicReference = new AtomicReference();
      atomicReference.set((BluetoothPan)param1BluetoothProfile);
      OplusNetworkUtil.access$102((BluetoothPan)param1BluetoothProfile);
      BluetoothPan bluetoothPan = atomicReference.get();
      if (bluetoothPan != null) {
        List list = bluetoothPan.getDevicesMatchingConnectionStates(new int[] { 2 });
        if (list != null && list.size() > 0)
          OplusNetworkUtil.access$202(true); 
      } 
    }
    
    public void onServiceDisconnected(int param1Int) {
      OplusNetworkUtil.access$202(false);
    }
  }
  
  private static void closeProxy() {
    if (mService != null)
      try {
        BluetoothAdapter.getDefaultAdapter().closeProfileProxy(5, (BluetoothProfile)mService);
      } finally {
        Exception exception = null;
      }  
  }
}
