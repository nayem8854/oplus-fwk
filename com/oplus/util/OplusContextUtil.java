package com.oplus.util;

import android.R;
import android.app.Activity;
import android.common.OplusFeatureCache;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.os.UserHandle;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import com.oplus.internal.R;
import com.oplus.theme.IOplusThemeStyle;

public class OplusContextUtil {
  private static final String METADATA_STYLE_TITLE = "color.support.options";
  
  private static final String METADATA_STYLE_VALUE = "true";
  
  private static final String TAG = "OplusContextUtil";
  
  private static boolean sIsColorStyleActivity;
  
  private static boolean sIsColorStyleApplication;
  
  private static boolean sIsColorStyleApplicationAssigned;
  
  private static int sLastActivityHash;
  
  private final boolean mIsOplusOSStyle;
  
  private final boolean mIsOplusStyle;
  
  public OplusContextUtil(Context paramContext) {
    boolean bool = isOplusStyle(paramContext);
    this.mIsOplusOSStyle = isOplusOSStyle(paramContext, bool);
  }
  
  public boolean isOplusStyle() {
    return this.mIsOplusStyle;
  }
  
  public boolean isOplusOSStyle() {
    return this.mIsOplusOSStyle;
  }
  
  public static boolean isOplusStyle(Context paramContext) {
    boolean bool = false;
    if (paramContext != null) {
      TypedArray typedArray = paramContext.getTheme().obtainStyledAttributes(R.styleable.OplusTheme);
      bool = typedArray.getBoolean(0, false);
      typedArray.recycle();
    } 
    return bool;
  }
  
  public static Context getOplusThemeContext(Context paramContext) {
    ContextThemeWrapper contextThemeWrapper;
    if (!isOplusStyle(paramContext))
      contextThemeWrapper = new ContextThemeWrapper(paramContext, ((IOplusThemeStyle)OplusFeatureCache.getOrCreate(IOplusThemeStyle.DEFAULT, new Object[0])).getSystemThemeStyle(201523202)); 
    return (Context)contextThemeWrapper;
  }
  
  public static int getResId(Context paramContext, int paramInt) {
    TypedValue typedValue = new TypedValue();
    paramContext.getResources().getValue(paramInt, typedValue, true);
    return typedValue.resourceId;
  }
  
  public static boolean isOplusOSStyle(Context paramContext, boolean paramBoolean) {
    if (paramBoolean)
      return true; 
    if (paramContext == null)
      return false; 
    paramBoolean = isOplusStyleInApplication(paramContext);
    if (paramBoolean)
      return true; 
    try {
      while (paramContext instanceof ContextWrapper && 
        !(paramContext instanceof Activity)) {
        Context context = ((ContextWrapper)paramContext).getBaseContext();
        if (paramContext == context)
          break; 
        paramContext = context;
      } 
    } catch (Exception exception) {
      OplusLog.e("OplusContextUtil", exception.toString());
    } 
    if (paramContext instanceof Activity)
      paramBoolean = isColorStyleInActivity(paramContext); 
    return paramBoolean;
  }
  
  private static boolean isColorStyleInActivity(Context paramContext) {
    try {
      if (sLastActivityHash == paramContext.hashCode())
        return sIsColorStyleActivity; 
      PackageManager packageManager = paramContext.getPackageManager();
      Activity activity = (Activity)paramContext;
      ActivityInfo activityInfo = packageManager.getActivityInfo(activity.getComponentName(), 128);
      if (activityInfo.metaData != null && 
        "true".equals(activityInfo.metaData.getString("color.support.options"))) {
        sIsColorStyleActivity = true;
        sLastActivityHash = paramContext.hashCode();
        return true;
      } 
    } catch (Exception exception) {
      OplusLog.e("OplusContextUtil", exception.toString());
    } 
    sIsColorStyleActivity = false;
    sLastActivityHash = paramContext.hashCode();
    return false;
  }
  
  private static boolean isOplusStyleInApplication(Context paramContext) {
    try {
      if (sIsColorStyleApplicationAssigned)
        return sIsColorStyleApplication; 
      PackageManager packageManager = paramContext.getPackageManager();
      ApplicationInfo applicationInfo = packageManager.getApplicationInfoAsUser(paramContext.getPackageName(), 128, UserHandle.getCallingUserId());
      if (applicationInfo != null && applicationInfo.metaData != null && 
        "true".equals(applicationInfo.metaData.getString("color.support.options"))) {
        sIsColorStyleApplication = true;
        sIsColorStyleApplicationAssigned = true;
        return true;
      } 
    } catch (Exception exception) {
      OplusLog.e("OplusContextUtil", exception.toString());
    } catch (AbstractMethodError abstractMethodError) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("AbstraceMethod not implemented by App : ");
      stringBuilder.append(abstractMethodError);
      OplusLog.e("OplusContextUtil", stringBuilder.toString());
    } 
    sIsColorStyleApplication = false;
    sIsColorStyleApplicationAssigned = true;
    return false;
  }
  
  public static boolean isOplusOSStyle(Context paramContext) {
    return isOplusOSStyle(paramContext, isOplusStyle(paramContext));
  }
  
  public static TypedArray getWindowStyle(Context paramContext) {
    return paramContext.obtainStyledAttributes(R.styleable.Window);
  }
  
  public static Activity getActivityContext(Context paramContext) {
    while (paramContext instanceof ContextWrapper) {
      if (paramContext instanceof Activity)
        return (Activity)paramContext; 
      ContextWrapper contextWrapper = (ContextWrapper)paramContext;
      Context context = contextWrapper.getBaseContext();
      if (context == paramContext)
        break; 
      paramContext = context;
    } 
    return null;
  }
  
  public static String getActivityContextName(Context paramContext) {
    Activity activity = getActivityContext(paramContext);
    if (activity == null)
      return null; 
    return activity.getClass().getName();
  }
  
  public static int getAttrColor(Context paramContext, int paramInt) {
    TypedArray typedArray = paramContext.getTheme().obtainStyledAttributes(new int[] { paramInt });
    paramInt = typedArray.getColor(0, 0);
    typedArray.recycle();
    return paramInt;
  }
}
