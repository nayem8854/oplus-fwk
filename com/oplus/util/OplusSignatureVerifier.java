package com.oplus.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Binder;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.collect.Sets;
import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Set;

public class OplusSignatureVerifier {
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static final String TAG = "SignatureVerifier";
  
  private static OplusSignatureUpdater mColorSignatureUpdater;
  
  private static Set<String> sWhiteList = Sets.newHashSet();
  
  public static void initUpdater(OplusSignatureUpdater paramOplusSignatureUpdater) {
    mColorSignatureUpdater = paramOplusSignatureUpdater;
  }
  
  public static boolean verificaionPass(Context paramContext) {
    StringBuilder stringBuilder;
    int i = Binder.getCallingUid();
    String str = getPackageForUid(paramContext, i);
    if (DEBUG) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("verificaionPass packageName:");
      stringBuilder1.append(str);
      Log.d("SignatureVerifier", stringBuilder1.toString());
    } 
    if (TextUtils.isEmpty(str))
      return false; 
    if (sWhiteList.contains(str)) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("verificaionPass sWhiteList contain ");
      stringBuilder.append(str);
      Log.d("SignatureVerifier", stringBuilder.toString());
      return true;
    } 
    if (isSystemApp((Context)stringBuilder, str)) {
      sWhiteList.add(str);
      stringBuilder = new StringBuilder();
      stringBuilder.append("verificaionPass isSystemApp sWhiteList add ");
      stringBuilder.append(str);
      Log.d("SignatureVerifier", stringBuilder.toString());
      return true;
    } 
    return verifySignature((Context)stringBuilder, str);
  }
  
  private static boolean isSystemApp(Context paramContext, String paramString) {
    try {
      ApplicationInfo applicationInfo = paramContext.getPackageManager().getApplicationInfo(paramString, 0);
      if ((applicationInfo.flags & 0x1) != 0 || (applicationInfo.flags & 0x80) != 0) {
        Log.d("SignatureVerifier", "isSystemApp true");
        return true;
      } 
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      nameNotFoundException.printStackTrace();
    } 
    return false;
  }
  
  private static boolean verifySignature(Context paramContext, String paramString) {
    String str = getMD5Signature(paramContext, paramString);
    OplusSignatureUpdater oplusSignatureUpdater = mColorSignatureUpdater;
    if (oplusSignatureUpdater == null) {
      Log.e("SignatureVerifier", "should initUpdater first");
      return false;
    } 
    List<String> list = oplusSignatureUpdater.getSignatures();
    if (list.contains(str)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("verifySignature contains signature:");
      stringBuilder.append(str);
      Log.d("SignatureVerifier", stringBuilder.toString());
      sWhiteList.add(paramString);
      return true;
    } 
    return false;
  }
  
  public static String getMD5Signature(Context paramContext, String paramString) {
    Signature[] arrayOfSignature = getSignatures(paramContext, paramString);
    if (arrayOfSignature == null) {
      Log.w("SignatureVerifier", "sigutures is null");
      return null;
    } 
    byte b = 0;
    byte[] arrayOfByte = arrayOfSignature[0].toByteArray();
    try {
      CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
      paramString = null;
      try {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream();
        this(arrayOfByte);
        X509Certificate x509Certificate = (X509Certificate)certificateFactory.generateCertificate(byteArrayInputStream);
        String str = paramString;
        try {
          MessageDigest messageDigest = MessageDigest.getInstance("MD5");
          str = paramString;
          byte[] arrayOfByte1 = messageDigest.digest(x509Certificate.getEncoded());
          str = paramString;
          StringBuilder stringBuilder1 = new StringBuilder();
          str = paramString;
          this();
          int i;
          for (str = paramString, i = arrayOfByte1.length; b < i; ) {
            byte b1 = arrayOfByte1[b];
            str = paramString;
            stringBuilder1.append(Integer.toHexString(b1 & 0xFF | 0x100).substring(1, 3));
            b++;
          } 
          str = paramString;
          paramString = stringBuilder1.toString();
          str = paramString;
          StringBuilder stringBuilder2 = new StringBuilder();
          str = paramString;
          this();
          str = paramString;
          stringBuilder2.append("getMD5Signature -- md5HexString = ");
          str = paramString;
          stringBuilder2.append(paramString);
          str = paramString;
          Log.d("SignatureVerifier", stringBuilder2.toString());
          str = paramString;
        } catch (Exception exception) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("getMD5Signature -- 3 error: ");
          stringBuilder.append(exception);
          Log.w("SignatureVerifier", stringBuilder.toString());
        } 
        return str;
      } catch (CertificateException certificateException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("getMD5Signature -- 2 error: ");
        stringBuilder.append(certificateException);
        Log.w("SignatureVerifier", stringBuilder.toString());
        return null;
      } 
    } catch (CertificateException certificateException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getMD5Signature -- 1 error: ");
      stringBuilder.append(certificateException);
      Log.w("SignatureVerifier", stringBuilder.toString());
      return null;
    } 
  }
  
  private static Signature[] getSignatures(Context paramContext, String paramString) {
    if (paramContext == null || paramString == null) {
      Log.w("SignatureVerifier", "getSignatures packageName is null");
      return null;
    } 
    PackageManager packageManager = paramContext.getPackageManager();
    try {
      PackageInfo packageInfo = packageManager.getPackageInfo(paramString, 64);
      if (packageInfo == null) {
        Log.w("SignatureVerifier", "getSignatures packageInfo is null");
        return null;
      } 
      return packageInfo.signatures;
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getSignatures error: ");
      stringBuilder.append(nameNotFoundException);
      Log.w("SignatureVerifier", stringBuilder.toString());
      return null;
    } 
  }
  
  private static String getPackageForUid(Context paramContext, int paramInt) {
    PackageManager packageManager = paramContext.getPackageManager();
    if (packageManager == null)
      return null; 
    String[] arrayOfString = packageManager.getPackagesForUid(paramInt);
    if (arrayOfString == null || arrayOfString.length == 0)
      return null; 
    return arrayOfString[0];
  }
}
