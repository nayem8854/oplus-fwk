package com.oplus.util;

public class OplusChineseDateAndSolarDate {
  static {
    System.loadLibrary("ChineseDateAndSolarDate");
  }
  
  public static int[] ChineseDateToSunDate(int paramInt1, int paramInt2, int paramInt3) {
    return NativeChineseDateToSunDate(paramInt1, paramInt2, paramInt3);
  }
  
  public static int[] SunDateToChineseDate(int paramInt1, int paramInt2, int paramInt3) {
    return NativeSunDateToChineseDate(paramInt1, paramInt2, paramInt3);
  }
  
  public static int GetChLeapMonth(int paramInt) {
    return NativeGetChineseLeapMonth(paramInt);
  }
  
  public static int GetChMonthDays(int paramInt1, int paramInt2) {
    return NativeGetChineseMonthDays(paramInt1, paramInt2);
  }
  
  public static int GetSolarMonthDays(int paramInt1, int paramInt2) {
    return NativeGetSunMonthDays(paramInt1, paramInt2);
  }
  
  private static native int[] NativeChineseDateToSunDate(int paramInt1, int paramInt2, int paramInt3);
  
  private static native int NativeGetChineseLeapMonth(int paramInt);
  
  private static native int NativeGetChineseMonthDays(int paramInt1, int paramInt2);
  
  private static native int NativeGetSunMonthDays(int paramInt1, int paramInt2);
  
  private static native int[] NativeSunDateToChineseDate(int paramInt1, int paramInt2, int paramInt3);
}
