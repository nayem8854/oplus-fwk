package com.oplus.util;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.OplusBaseConfiguration;
import android.graphics.Canvas;
import android.graphics.OplusBasePaint;
import android.graphics.OplusBaseTypeface;
import android.graphics.OplusTypefaceInjector;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.fonts.FontVariationAxis;
import android.os.Process;
import android.provider.Settings;
import android.system.ErrnoException;
import android.system.Os;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.Random;

public class OplusFontUtils extends OplusBaseFontUtils {
  public static void setFlipFont(Configuration paramConfiguration) {
    OplusBaseConfiguration oplusBaseConfiguration = (OplusBaseConfiguration)OplusTypeCastingHelper.typeCasting(OplusBaseConfiguration.class, paramConfiguration);
    if (oplusBaseConfiguration != null && oplusBaseConfiguration.mOplusExtraConfiguration != null && sFlipFont != oplusBaseConfiguration.mOplusExtraConfiguration.mFlipFont) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("SetFlipFont -- sFlipFont=");
      stringBuilder.append(sFlipFont);
      stringBuilder.append(", sIsROM6d0FlipFont= ");
      stringBuilder.append(sIsROM6d0FlipFont);
      stringBuilder.append(" --> mFlipFont=");
      stringBuilder.append(oplusBaseConfiguration.mOplusExtraConfiguration.mFlipFont);
      logd(stringBuilder.toString());
      renameFontFileName();
      sFlipFont = oplusBaseConfiguration.mOplusExtraConfiguration.mFlipFont;
      initFont(false);
      doUpdateTypeface(paramConfiguration);
    } 
  }
  
  private static void renameFontFileName() {
    String str = getCustomizedFontFile(true);
    File file = new File(str);
    if (file.exists() && 
      !file.renameTo(new File(getCustomizedFontFile(false))))
      Log.w("OppoFontUtils", "Failed to rename to new font file!"); 
  }
  
  public static void setFlipFontWhenUserChange(Configuration paramConfiguration) {
    OplusBaseConfiguration oplusBaseConfiguration = (OplusBaseConfiguration)OplusTypeCastingHelper.typeCasting(OplusBaseConfiguration.class, paramConfiguration);
    if (oplusBaseConfiguration != null && oplusBaseConfiguration.mOplusExtraConfiguration.mFontUserId >= 0) {
      int i = oplusBaseConfiguration.mOplusExtraConfiguration.mFontUserId;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setFlipFontWhenUserChange -- mUserId in mOppoExtraConfiguration = ");
      stringBuilder.append(i);
      stringBuilder.append(", sUserId = ");
      stringBuilder.append(sUserId);
      logd(stringBuilder.toString());
      sFlipFont = oplusBaseConfiguration.mOplusExtraConfiguration.mFlipFont;
      sUserId = i;
      initFont(true);
      doUpdateTypeface(paramConfiguration);
    } 
  }
  
  public static void updateTypefaceInCurrProcess(Configuration paramConfiguration) {
    OplusBaseConfiguration oplusBaseConfiguration = (OplusBaseConfiguration)OplusTypeCastingHelper.typeCasting(OplusBaseConfiguration.class, paramConfiguration);
    if (oplusBaseConfiguration == null)
      return; 
    if (oplusBaseConfiguration.mOplusExtraConfiguration.mFontUserId == -1) {
      logd("invalid mFontUserId in extraConfiguration -1, abandon");
      return;
    } 
    if (sFlipFont == oplusBaseConfiguration.mOplusExtraConfiguration.mFlipFont && sUserId == oplusBaseConfiguration.mOplusExtraConfiguration.mFontUserId)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("<updateTypefaceInCurrProcess> myTid = ");
    stringBuilder.append(Process.myTid());
    stringBuilder.append(", myUid = ");
    stringBuilder.append(Process.myUid());
    stringBuilder.append(", myPid = ");
    stringBuilder.append(Process.myPid());
    stringBuilder.append(", mFontUserId = ");
    stringBuilder.append(oplusBaseConfiguration.mOplusExtraConfiguration.mFontUserId);
    String str = stringBuilder.toString();
    logd(str);
    sFlipFont = oplusBaseConfiguration.mOplusExtraConfiguration.mFlipFont;
    sUserId = oplusBaseConfiguration.mOplusExtraConfiguration.mFontUserId;
    doUpdateTypeface(paramConfiguration);
  }
  
  private static void doUpdateTypeface(Configuration paramConfiguration) {
    ErrnoException errnoException2 = null;
    File file2 = null;
    File file1 = new File(getCustomizedFontFile(false));
    if (file1.exists()) {
      isFlipFontUsed = true;
    } else {
      isFlipFontUsed = false;
    } 
    oplus.content.res.OplusFontUtils.isFlipFontUsed = isFlipFontUsed;
    try {
      String str = Os.readlink((FONTINFOARRAY_ROM6D0[4]).mDataFontName);
      file1 = file2;
      if (!TextUtils.isEmpty(str)) {
        file1 = new File();
        this(str);
        Typeface typeface = Typeface.createFromFile(file1);
      } 
    } catch (RuntimeException runtimeException) {
      loge("RuntimeException initFont() createFromFile fail", runtimeException);
      File file = file2;
    } catch (ErrnoException errnoException1) {
      loge("Could not update selinux policy initFont createFromFile ", (Throwable)errnoException1);
      errnoException1 = errnoException2;
    } 
    boolean bool = false;
    if (sFlipFont == 10001)
      bool = true; 
    freeCaches();
    if (bool) {
      Typeface typeface = Typeface.DEFAULT;
      String str = (String)null;
      typeface.native_instance = (Typeface.create(str, 0)).native_instance;
      typeface = Typeface.DEFAULT_BOLD;
      typeface.native_instance = (Typeface.create(str, 1)).native_instance;
      typeface = Typeface.SANS_SERIF;
      typeface.native_instance = (Typeface.create("sans-serif", 0)).native_instance;
      typeface = Typeface.MONOSPACE;
      typeface.native_instance = (Typeface.create("monospace", 0)).native_instance;
      Typeface[] arrayOfTypeface = OplusTypefaceInjector.getSystemDefaultTypefaces();
      if (arrayOfTypeface != null) {
        Typeface typeface1 = Typeface.DEFAULT;
        typeface1 = Typeface.DEFAULT_BOLD;
        Typeface.create(str, 2);
        Typeface.create(str, 3);
      } 
    } 
    if (sCurrentTypefaces == null)
      sCurrentTypefaces = new Typeface[4]; 
    if (isFlipFontUsed) {
      if (errnoException1 != null) {
        sCurrentTypefaces[0] = Typeface.create((Typeface)errnoException1, 0);
        sCurrentTypefaces[1] = Typeface.create((Typeface)errnoException1, 1);
        sCurrentTypefaces[2] = Typeface.create((Typeface)errnoException1, 2);
        sCurrentTypefaces[3] = Typeface.create((Typeface)errnoException1, 3);
        sCurrentTypefacesArray = new ArrayList<>(Arrays.asList(sCurrentTypefaces));
      } 
    } else {
      if (sCurrentTypefaces != null) {
        if (sCurrentTypefaces[0] != null)
          (sCurrentTypefaces[0]).native_instance = Typeface.DEFAULT.native_instance; 
        if (sCurrentTypefaces[1] != null)
          (sCurrentTypefaces[1]).native_instance = Typeface.DEFAULT_BOLD.native_instance; 
        if (sCurrentTypefaces[2] != null) {
          Typeface typeface = sCurrentTypefaces[2];
          typeface.native_instance = (defaultFromStyle(2)).native_instance;
        } 
        if (sCurrentTypefaces[3] != null) {
          Typeface typeface = sCurrentTypefaces[3];
          typeface.native_instance = (defaultFromStyle(3)).native_instance;
        } 
      } 
      logd("Not using flip font");
    } 
  }
  
  public static void setAppTypeFace(String paramString) {
    if (FLITER_CTS_APP_PKG_LIST.contains(paramString))
      sIsCheckCTS = true; 
    if (FLITER_NOT_REPLACEFONT_APP_PKG_LIST.contains(paramString))
      sReplaceFont = false; 
  }
  
  public static void setNeedReplaceAllTypefaceApp(boolean paramBoolean) {
    sNeedReplaceAllTypefaceApp = paramBoolean;
  }
  
  public static boolean getNeedReplaceAllTypefaceApp() {
    return sNeedReplaceAllTypefaceApp;
  }
  
  public static Typeface flipTypeface(Typeface paramTypeface) {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic getBaseType : (Landroid/graphics/Typeface;)Landroid/graphics/OplusBaseTypeface;
    //   4: astore_1
    //   5: getstatic com/oplus/util/OplusFontUtils.isFlipFontUsed : Z
    //   8: ifeq -> 65
    //   11: getstatic com/oplus/util/OplusFontUtils.sReplaceFont : Z
    //   14: ifeq -> 65
    //   17: getstatic com/oplus/util/OplusFontUtils.sCurrentTypefaces : [Landroid/graphics/Typeface;
    //   20: ifnull -> 65
    //   23: aload_0
    //   24: ifnull -> 45
    //   27: aload_1
    //   28: ifnull -> 38
    //   31: aload_1
    //   32: getfield isLikeDefault : Z
    //   35: ifne -> 45
    //   38: aload_0
    //   39: invokestatic isSystemTypeface : (Landroid/graphics/Typeface;)Z
    //   42: ifeq -> 65
    //   45: aload_0
    //   46: ifnonnull -> 54
    //   49: iconst_0
    //   50: istore_2
    //   51: goto -> 59
    //   54: aload_0
    //   55: invokevirtual getStyle : ()I
    //   58: istore_2
    //   59: getstatic com/oplus/util/OplusFontUtils.sCurrentTypefaces : [Landroid/graphics/Typeface;
    //   62: iload_2
    //   63: aaload
    //   64: areturn
    //   65: getstatic com/oplus/util/OplusFontUtils.isFlipFontUsed : Z
    //   68: ifne -> 103
    //   71: aload_0
    //   72: ifnull -> 103
    //   75: getstatic com/oplus/util/OplusFontUtils.sCurrentTypefacesArray : Ljava/util/List;
    //   78: ifnull -> 103
    //   81: getstatic com/oplus/util/OplusFontUtils.sCurrentTypefacesArray : Ljava/util/List;
    //   84: astore_1
    //   85: aload_1
    //   86: aload_0
    //   87: invokeinterface contains : (Ljava/lang/Object;)Z
    //   92: ifeq -> 103
    //   95: aload_0
    //   96: invokevirtual getStyle : ()I
    //   99: invokestatic defaultFromStyle : (I)Landroid/graphics/Typeface;
    //   102: areturn
    //   103: getstatic com/oplus/util/OplusFontUtils.isFlipFontUsed : Z
    //   106: ifne -> 141
    //   109: aload_0
    //   110: ifnull -> 141
    //   113: getstatic android/graphics/OplusTypefaceInjector.OPLUSUI_MEDIUM : Landroid/graphics/Typeface;
    //   116: ifnull -> 141
    //   119: getstatic android/graphics/OplusTypefaceInjector.OPLUSUI_MEDIUM : Landroid/graphics/Typeface;
    //   122: astore_1
    //   123: aload_0
    //   124: aload_1
    //   125: invokevirtual equals : (Ljava/lang/Object;)Z
    //   128: ifeq -> 141
    //   131: getstatic com/oplus/util/OplusFontUtils.isCurrentLanguageSupportMediumFont : Z
    //   134: ifne -> 141
    //   137: getstatic android/graphics/Typeface.DEFAULT_BOLD : Landroid/graphics/Typeface;
    //   140: areturn
    //   141: aload_0
    //   142: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #249	-> 0
    //   #250	-> 5
    //   #255	-> 38
    //   #256	-> 45
    //   #257	-> 59
    //   #258	-> 65
    //   #261	-> 85
    //   #262	-> 95
    //   #263	-> 103
    //   #265	-> 123
    //   #267	-> 137
    //   #269	-> 141
  }
  
  public static void replaceFakeBoldToColorMedium(TextView paramTextView, Typeface paramTypeface, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: ifnonnull -> 5
    //   4: return
    //   5: aload_1
    //   6: invokestatic getBaseType : (Landroid/graphics/Typeface;)Landroid/graphics/OplusBaseTypeface;
    //   9: astore_3
    //   10: getstatic com/oplus/util/OplusFontUtils.sIsCheckCTS : Z
    //   13: ifne -> 106
    //   16: getstatic com/oplus/util/OplusFontUtils.mFontVariationStatus : I
    //   19: ifeq -> 106
    //   22: iload_2
    //   23: iconst_1
    //   24: if_icmpeq -> 32
    //   27: iload_2
    //   28: iconst_3
    //   29: if_icmpne -> 106
    //   32: aload_1
    //   33: ifnull -> 75
    //   36: aload_3
    //   37: ifnull -> 47
    //   40: aload_3
    //   41: getfield isLikeDefault : Z
    //   44: ifne -> 75
    //   47: getstatic android/graphics/Typeface.SANS_SERIF : Landroid/graphics/Typeface;
    //   50: astore_3
    //   51: aload_3
    //   52: aload_1
    //   53: invokevirtual equals : (Ljava/lang/Object;)Z
    //   56: ifne -> 75
    //   59: getstatic android/graphics/OplusTypefaceInjector.OPLUSUI_MEDIUM : Landroid/graphics/Typeface;
    //   62: ifnull -> 106
    //   65: aload_1
    //   66: getstatic android/graphics/OplusTypefaceInjector.OPLUSUI_MEDIUM : Landroid/graphics/Typeface;
    //   69: invokevirtual equals : (Ljava/lang/Object;)Z
    //   72: ifeq -> 106
    //   75: getstatic com/oplus/util/OplusFontUtils.isCurrentLanguageSupportMediumFont : Z
    //   78: ifeq -> 106
    //   81: getstatic android/graphics/OplusTypefaceInjector.OPLUSUI_MEDIUM : Landroid/graphics/Typeface;
    //   84: astore_1
    //   85: iload_2
    //   86: iconst_3
    //   87: if_icmpne -> 95
    //   90: iconst_2
    //   91: istore_2
    //   92: goto -> 97
    //   95: iconst_0
    //   96: istore_2
    //   97: aload_0
    //   98: aload_1
    //   99: iload_2
    //   100: invokevirtual setTypeface : (Landroid/graphics/Typeface;I)V
    //   103: goto -> 112
    //   106: aload_0
    //   107: aload_1
    //   108: iload_2
    //   109: invokevirtual setTypeface : (Landroid/graphics/Typeface;I)V
    //   112: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #273	-> 0
    //   #274	-> 4
    //   #277	-> 5
    //   #278	-> 10
    //   #280	-> 51
    //   #282	-> 81
    //   #284	-> 106
    //   #286	-> 112
  }
  
  public static Typeface create(Typeface paramTypeface, String paramString, int paramInt) {
    return Typeface.create(paramTypeface, paramInt);
  }
  
  public static Typeface defaultFromStyle(int paramInt) {
    Typeface[] arrayOfTypeface = OplusTypefaceInjector.getSystemDefaultTypefaces();
    if (arrayOfTypeface != null && paramInt < arrayOfTypeface.length && paramInt > -1)
      return arrayOfTypeface[paramInt]; 
    return null;
  }
  
  private static boolean checkAndCorrectFlipFontLink(boolean paramBoolean) {
    boolean bool = false;
    paramBoolean = false;
    File file = new File(getCustomizedFontFile(false));
    if (file.exists()) {
      isFlipFontUsed = relinkDataFontToTarget(2);
      oplus.content.res.OplusFontUtils.isFlipFontUsed = isFlipFontUsed;
      paramBoolean = bool;
    } else {
      logd("checkAndCorreectFlipFontLink flipedFontFile NOT exists");
      if (sFlipFont == 10001) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("checkAndCorrectFlipFontLink seems running CTS ");
        stringBuilder.append(sFlipFont);
        logd(stringBuilder.toString());
        paramBoolean = true;
      } 
      isFlipFontUsed = false;
      oplus.content.res.OplusFontUtils.isFlipFontUsed = isFlipFontUsed;
    } 
    if (!isFlipFontUsed)
      relinkDataFontToTarget(1); 
    return paramBoolean;
  }
  
  private static boolean relinkDataFontToTarget(int paramInt) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_1
    //   2: iload_1
    //   3: getstatic com/oplus/util/OplusFontUtils.sFontLinkInfos : Ljava/util/List;
    //   6: invokeinterface size : ()I
    //   11: if_icmpge -> 238
    //   14: getstatic com/oplus/util/OplusFontUtils.sFontLinkInfos : Ljava/util/List;
    //   17: iload_1
    //   18: invokeinterface get : (I)Ljava/lang/Object;
    //   23: checkcast com/oplus/util/OplusBaseFontUtils$FontLinkInfo
    //   26: astore_2
    //   27: new java/io/File
    //   30: dup
    //   31: aload_2
    //   32: getfield mDataFontName : Ljava/lang/String;
    //   35: invokespecial <init> : (Ljava/lang/String;)V
    //   38: astore_3
    //   39: ldc_w ''
    //   42: astore #4
    //   44: iload_0
    //   45: iconst_1
    //   46: if_icmpne -> 58
    //   49: aload_2
    //   50: getfield mSystemFontName : Ljava/lang/String;
    //   53: astore #4
    //   55: goto -> 69
    //   58: iload_0
    //   59: iconst_2
    //   60: if_icmpne -> 69
    //   63: iconst_0
    //   64: invokestatic getCustomizedFontFile : (Z)Ljava/lang/String;
    //   67: astore #4
    //   69: aload_3
    //   70: invokevirtual exists : ()Z
    //   73: ifne -> 140
    //   76: aload_3
    //   77: invokevirtual delete : ()Z
    //   80: pop
    //   81: new java/lang/StringBuilder
    //   84: astore_3
    //   85: aload_3
    //   86: invokespecial <init> : ()V
    //   89: aload_3
    //   90: ldc_w 'relink font targetFont = '
    //   93: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   96: pop
    //   97: aload_3
    //   98: aload #4
    //   100: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   103: pop
    //   104: aload_3
    //   105: ldc_w ', '
    //   108: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   111: pop
    //   112: aload_3
    //   113: aload_2
    //   114: getfield mDataFontName : Ljava/lang/String;
    //   117: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   120: pop
    //   121: aload_3
    //   122: invokevirtual toString : ()Ljava/lang/String;
    //   125: invokestatic logd : (Ljava/lang/String;)V
    //   128: aload #4
    //   130: aload_2
    //   131: getfield mDataFontName : Ljava/lang/String;
    //   134: invokestatic symlink : (Ljava/lang/String;Ljava/lang/String;)V
    //   137: goto -> 180
    //   140: aload_3
    //   141: invokevirtual exists : ()Z
    //   144: ifeq -> 180
    //   147: aload_2
    //   148: getfield mDataFontName : Ljava/lang/String;
    //   151: astore #5
    //   153: aload #5
    //   155: invokestatic readlink : (Ljava/lang/String;)Ljava/lang/String;
    //   158: aload #4
    //   160: invokevirtual equals : (Ljava/lang/Object;)Z
    //   163: ifne -> 180
    //   166: aload_3
    //   167: invokevirtual delete : ()Z
    //   170: pop
    //   171: aload #4
    //   173: aload_2
    //   174: getfield mDataFontName : Ljava/lang/String;
    //   177: invokestatic symlink : (Ljava/lang/String;Ljava/lang/String;)V
    //   180: iinc #1, 1
    //   183: goto -> 2
    //   186: astore_3
    //   187: new java/lang/StringBuilder
    //   190: dup
    //   191: invokespecial <init> : ()V
    //   194: astore #4
    //   196: aload #4
    //   198: ldc_w 'Could not update selinux policy check and correct flipfont: '
    //   201: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   204: pop
    //   205: aload #4
    //   207: aload_2
    //   208: getfield mDataFontName : Ljava/lang/String;
    //   211: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   214: pop
    //   215: aload #4
    //   217: invokevirtual toString : ()Ljava/lang/String;
    //   220: aload_3
    //   221: invokestatic loge : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   224: iconst_0
    //   225: ireturn
    //   226: astore #4
    //   228: ldc_w 'SELinux policy update check and correct flipfont'
    //   231: aload #4
    //   233: invokestatic loge : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   236: iconst_0
    //   237: ireturn
    //   238: iconst_1
    //   239: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #330	-> 0
    //   #331	-> 14
    //   #332	-> 27
    //   #334	-> 39
    //   #335	-> 44
    //   #336	-> 49
    //   #337	-> 58
    //   #338	-> 63
    //   #341	-> 69
    //   #342	-> 76
    //   #343	-> 81
    //   #345	-> 128
    //   #346	-> 140
    //   #347	-> 153
    //   #348	-> 166
    //   #349	-> 171
    //   #357	-> 180
    //   #330	-> 180
    //   #354	-> 186
    //   #355	-> 187
    //   #356	-> 224
    //   #351	-> 226
    //   #352	-> 228
    //   #353	-> 236
    //   #360	-> 238
    // Exception table:
    //   from	to	target	type
    //   49	55	226	java/lang/IllegalArgumentException
    //   49	55	186	android/system/ErrnoException
    //   63	69	226	java/lang/IllegalArgumentException
    //   63	69	186	android/system/ErrnoException
    //   69	76	226	java/lang/IllegalArgumentException
    //   69	76	186	android/system/ErrnoException
    //   76	81	226	java/lang/IllegalArgumentException
    //   76	81	186	android/system/ErrnoException
    //   81	128	226	java/lang/IllegalArgumentException
    //   81	128	186	android/system/ErrnoException
    //   128	137	226	java/lang/IllegalArgumentException
    //   128	137	186	android/system/ErrnoException
    //   140	153	226	java/lang/IllegalArgumentException
    //   140	153	186	android/system/ErrnoException
    //   153	166	226	java/lang/IllegalArgumentException
    //   153	166	186	android/system/ErrnoException
    //   166	171	226	java/lang/IllegalArgumentException
    //   166	171	186	android/system/ErrnoException
    //   171	180	226	java/lang/IllegalArgumentException
    //   171	180	186	android/system/ErrnoException
  }
  
  private static void initFont(boolean paramBoolean) {
    checkAndCorrectFlipFontLink(paramBoolean);
  }
  
  public static void handleFactoryReset() {
    if (sIsROM6d0FlipFont)
      try {
        File file = new File();
        this(getCustomizedFontFile(false));
        if (file.exists())
          file.delete(); 
      } catch (Exception exception) {
        loge("Failed handleFactoryReset", exception);
      }  
  }
  
  public static boolean handleTextViewSettypeface(Paint paramPaint, Typeface paramTypeface) {
    if (paramPaint != null) {
      paramTypeface = flipTypeface(paramTypeface);
      if (paramPaint.getTypeface() != paramTypeface) {
        paramPaint.setTypeface(paramTypeface);
        return true;
      } 
    } 
    return false;
  }
  
  public static boolean isCurrentLanguageSupportMediumFont() {
    Locale locale = Locale.getDefault();
    if (locale != null && SUPPORT_MEDIUM_FONT_LANGUAGE_LIST.contains(locale.getLanguage()))
      return true; 
    return false;
  }
  
  public static boolean isCurrentLanguageSupportVariationFont() {
    Locale locale = Locale.getDefault();
    if (locale != null && SUPPORT_FONT_VARIATION_LIST.contains(locale.getLanguage()))
      return true; 
    return false;
  }
  
  private static void freeCaches() {
    Canvas.freeCaches();
    Canvas.freeTextLayoutCaches();
  }
  
  private static String getCustomizedFontFile(boolean paramBoolean) {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(sUserId);
    stringBuilder1.append("/");
    String str = stringBuilder1.toString();
    if (sUserId == 0)
      str = ""; 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(DATA_FONT_DIRECTORY);
    stringBuilder2.append(str);
    if (paramBoolean) {
      str = "ColorOS-Regular.ttf";
    } else {
      str = "Customized-Regular.ttf";
    } 
    stringBuilder2.append(str);
    str = stringBuilder2.toString();
    return str;
  }
  
  private static void killRunningProcess(boolean paramBoolean) {
    try {
      Thread thread = new Thread() {
          public void run() {
            InstallFont installFont = new InstallFont();
            installFont.killRecentPackage();
            installFont.killAppProcess();
            super.run();
          }
        };
      super();
      thread.start();
    } catch (Exception exception) {
      loge("<killRunningProcess>:", exception);
    } catch (Error error) {
      loge("<killRunningProcess>:", error);
    } 
  }
  
  public static Typeface replaceTypefaceWithVariation(Typeface paramTypeface, Paint paramPaint) {
    if (!isNeedReplace(paramTypeface))
      return paramTypeface; 
    String str1 = paramPaint.getFontVariationSettings();
    String str2 = str1;
    if (str1 == null) {
      OplusBasePaint oplusBasePaint = (OplusBasePaint)OplusTypeCastingHelper.typeCasting(OplusBasePaint.class, paramPaint);
      oplusBasePaint.setFontVariationSettings(mFontVariationSettings, paramTypeface);
      str2 = mFontVariationSettings;
    } 
    Typeface typeface = paramTypeface;
    if (paramTypeface == null)
      typeface = Typeface.DEFAULT; 
    return getVariationFontFromCache(typeface, str2);
  }
  
  public static Typeface createTypefaceWithVariation(Typeface paramTypeface, String paramString) {
    OplusBaseTypeface oplusBaseTypeface;
    FontVariationAxis[] arrayOfFontVariationAxis = FontVariationAxis.fromFontVariationSettings(paramString);
    ArrayList<FontVariationAxis> arrayList = new ArrayList();
    byte b = 0;
    if (arrayOfFontVariationAxis == null)
      arrayOfFontVariationAxis = new FontVariationAxis[0]; 
    for (int i = arrayOfFontVariationAxis.length; b < i; ) {
      FontVariationAxis fontVariationAxis = arrayOfFontVariationAxis[b];
      if (paramTypeface.isSupportedAxes(fontVariationAxis.getOpenTypeTagValue()))
        arrayList.add(fontVariationAxis); 
      b++;
    } 
    if (!arrayList.isEmpty()) {
      Typeface typeface = Typeface.createFromTypefaceWithVariation(paramTypeface, arrayList);
      oplusBaseTypeface = OplusBaseTypeface.getBaseType(typeface);
      if (oplusBaseTypeface != null)
        oplusBaseTypeface.isLikeDefault = true; 
      return typeface;
    } 
    return (Typeface)oplusBaseTypeface;
  }
  
  private static Typeface covertSystemToOppoTypeface(Typeface paramTypeface) {
    Typeface typeface = paramTypeface;
    if (paramTypeface == null)
      typeface = Typeface.DEFAULT; 
    if (typeface.isBold() && typeface.isItalic()) {
      paramTypeface = OplusTypefaceInjector.OPLUSUI_VF_BOLD_ITALIC;
    } else if (typeface.isItalic()) {
      paramTypeface = OplusTypefaceInjector.OPLUSUI_VF_ITALIC;
    } else {
      if (typeface.getWeight() == 700 || typeface.getWeight() == 500) {
        paramTypeface = OplusTypefaceInjector.OPLUSUI_VF_BOLD;
        return paramTypeface;
      } 
      paramTypeface = OplusTypefaceInjector.OPLUSUI_VF;
    } 
    return paramTypeface;
  }
  
  private static void createTypefaceWithNewWght(String paramString) {
    Typeface typeface = OplusTypefaceInjector.OPLUSUI_VF;
    OplusTypefaceInjector.OPLUSUI_VF = getVariationFontFromCache(typeface, paramString);
  }
  
  public static void updateFontVariationConfiguration(Configuration paramConfiguration, int paramInt) {
    if ((0x1000000 & paramInt) != 0) {
      OplusBaseConfiguration oplusBaseConfiguration = (OplusBaseConfiguration)OplusTypeCastingHelper.typeCasting(OplusBaseConfiguration.class, paramConfiguration);
      paramInt = oplusBaseConfiguration.mOplusExtraConfiguration.mFontVariationSettings;
      int i = oplusBaseConfiguration.mOplusExtraConfiguration.mFontVariationSettings;
      mFontVariation = paramInt & 0xFFF;
      mFontVariationStatus = (i & 0xF000) >> 12;
      updateFontVariationRes();
    } 
    updateLanguageLocale(paramConfiguration);
  }
  
  public static void updateLanguageLocale(Configuration paramConfiguration) {
    isCurrentLanguageSupportVariationFont = isCurrentLanguageSupportVariationFont();
    isCurrentLanguageSupportMediumFont = isCurrentLanguageSupportMediumFont();
  }
  
  public static void initVariationFontVariable(Context paramContext) {
    mPackageName = paramContext.getPackageName();
    if (mFontVariationStatus == 2) {
      mFontVariationAdaption = OplusFontVariationAdaptionUtils.getInstance().getAdaptionValue(mPackageName);
      isSearched = true;
    } 
    if (mFontVariationAdaption != 0)
      updateFontVariationRes(); 
  }
  
  private static void updateFontVariationRes() {
    String str;
    int i = mFontVariationStatus;
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          str = mFontVariationSettings;
        } else {
          if (mFontVariationAdaption == 0 && !isSearched) {
            mFontVariationAdaption = OplusFontVariationAdaptionUtils.getInstance().getAdaptionValue(mPackageName);
            isSearched = true;
          } 
          if (mFontVariationAdaption != 0) {
            str = convertIntToString(mFontVariationAdaption);
          } else {
            str = convertIntToString(mFontVariation);
          } 
        } 
      } else {
        str = convertIntToString(mFontVariation);
      } 
    } else {
      str = convertIntToString(550);
    } 
    if (!str.equals(mFontVariationSettings))
      mFontVariationSettings = str; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("updateFontVariationConfiguration: mFontVariation = ");
    stringBuilder.append(mFontVariation);
    stringBuilder.append(" mFontVariationStatus = ");
    stringBuilder.append(mFontVariationStatus);
    stringBuilder.append(" mPackageName = ");
    stringBuilder.append(mPackageName);
    stringBuilder.append(" mFontVariationAdaption = ");
    stringBuilder.append(mFontVariationAdaption);
    logd(stringBuilder.toString());
  }
  
  public static String convertIntToString(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("'wght' ");
    stringBuilder.append(paramInt);
    return stringBuilder.toString();
  }
  
  public static int convertStringToInt(String paramString) {
    if (TextUtils.isEmpty(paramString))
      return 0; 
    String[] arrayOfString = paramString.split(" ");
    if (arrayOfString.length == 2)
      return Integer.parseInt(arrayOfString[1]); 
    return 0;
  }
  
  public static boolean isNeedReplace(Typeface paramTypeface) {
    int i = mFontVariationStatus;
    boolean bool = false;
    if (i == 0)
      return false; 
    if (!isCurrentLanguageSupportVariationFont)
      return false; 
    if (isFlipFontUsed)
      return false; 
    if (paramTypeface != null) {
      OplusBaseTypeface oplusBaseTypeface = OplusBaseTypeface.getBaseType(paramTypeface);
      if (oplusBaseTypeface == null || oplusBaseTypeface.isLikeDefault)
        bool = true; 
      return bool;
    } 
    return true;
  }
  
  private static Typeface getVariationFontFromCache(Typeface paramTypeface, String paramString) {
    int i = convertStringToInt(paramString);
    int j = i / 10;
    i = paramTypeface.getWeight();
    Integer integer = FONT_WEIGHT_CAST_WGHT.get(Integer.valueOf(i));
    if (integer == null) {
      i = 0;
    } else {
      i = integer.intValue() - 550;
    } 
    j = j * 10 + i;
    i = j;
    if (j >= 1000)
      i = 1000; 
    j = i;
    if (i <= 100)
      j = 100; 
    String str = createTypefaceKey(OPLUS_SANS_VARIATION_FONT, j, paramTypeface.getStyle(), paramTypeface.getWeight());
    Typeface typeface2 = sOplusVariationCacheMap.get(str);
    Typeface typeface1 = typeface2;
    if (typeface2 == null) {
      typeface1 = createTypefaceWithVariation(covertSystemToOppoTypeface(paramTypeface), convertIntToString(j));
      sOplusVariationCacheMap.put(str, typeface1);
    } 
    return typeface1;
  }
  
  public static String createTypefaceKey(String paramString, int paramInt1, int paramInt2, int paramInt3) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append("-");
    stringBuilder.append(Integer.toString(paramInt1));
    stringBuilder.append("-");
    stringBuilder.append(Integer.toString(paramInt2));
    stringBuilder.append("-");
    stringBuilder.append(Integer.toString(paramInt3));
    return stringBuilder.toString();
  }
  
  public static void updateConfigurationInUIMode(Context paramContext, Configuration paramConfiguration, int paramInt) {
    boolean bool;
    if ((paramConfiguration.uiMode & 0x20) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    ContentResolver contentResolver = paramContext.getContentResolver();
    int i = 550, j = Settings.System.getIntForUser(contentResolver, "font_variation_settings", 550, paramInt);
    paramInt = j & 0xFFF;
    int k = (j & 0xF000) >> 12;
    if (k == 2) {
      paramInt = i;
      if (bool)
        paramInt = 500; 
    } 
    i = k << 12 & 0xF000;
    OplusBaseConfiguration oplusBaseConfiguration = (OplusBaseConfiguration)OplusTypeCastingHelper.typeCasting(OplusBaseConfiguration.class, paramConfiguration);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("updateConfigurationInUIMode:  isNightMode=");
    stringBuilder.append(bool);
    stringBuilder.append(" status=");
    stringBuilder.append(i);
    stringBuilder.append(" variation=");
    stringBuilder.append(paramInt);
    logd(stringBuilder.toString());
    oplusBaseConfiguration.mOplusExtraConfiguration.mFontVariationSettings = 0xFFFF0000 & j | paramInt | i;
  }
  
  static {
    File file = new File(getCustomizedFontFile(false));
    sFlipFont = -1;
    if (file.exists()) {
      Random random = new Random(System.currentTimeMillis());
      sFlipFont = random.nextInt(10000 - 0 + 1) + 0;
      isFlipFontUsed = true;
      oplus.content.res.OplusFontUtils.isFlipFontUsed = isFlipFontUsed;
    } 
    sUserId = Process.myUserHandle().hashCode();
    initFont(false);
    OplusTypefaceInjector.OPLUSUI_MEDIUM = Typeface.create("sans-serif-medium", 0);
    OplusTypefaceInjector.OPLUSUI_VF = Typeface.create("sys-sans-en", 0);
    OplusTypefaceInjector.OPLUSUI_VF_BOLD = Typeface.create("sys-sans-en", 1);
    OplusTypefaceInjector.OPLUSUI_VF_ITALIC = Typeface.create("sys-sans-en", 2);
    OplusTypefaceInjector.OPLUSUI_VF_BOLD_ITALIC = Typeface.create("sys-sans-en", 3);
  }
}
