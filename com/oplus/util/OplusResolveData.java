package com.oplus.util;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashMap;
import java.util.List;

public final class OplusResolveData implements Parcelable {
  public static final Parcelable.Creator<OplusResolveData> CREATOR = new Parcelable.Creator<OplusResolveData>() {
      public OplusResolveData createFromParcel(Parcel param1Parcel) {
        return new OplusResolveData(param1Parcel);
      }
      
      public OplusResolveData[] newArray(int param1Int) {
        return new OplusResolveData[param1Int];
      }
    };
  
  private HashMap<String, List<String>> mMap1 = new HashMap<>();
  
  private HashMap<String, List<String>> mMap2 = new HashMap<>();
  
  private HashMap<String, List<String>> mMap3 = new HashMap<>();
  
  private HashMap<String, List<String>> mMap4 = new HashMap<>();
  
  private HashMap<String, List<String>> mMap5 = new HashMap<>();
  
  private HashMap<String, String> mMap6 = new HashMap<>();
  
  public OplusResolveData(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeMap(this.mMap1);
    paramParcel.writeMap(this.mMap2);
    paramParcel.writeMap(this.mMap3);
    paramParcel.writeMap(this.mMap4);
    paramParcel.writeMap(this.mMap5);
    paramParcel.writeMap(this.mMap6);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mMap1 = paramParcel.readHashMap(HashMap.class.getClassLoader());
    this.mMap2 = paramParcel.readHashMap(HashMap.class.getClassLoader());
    this.mMap3 = paramParcel.readHashMap(HashMap.class.getClassLoader());
    this.mMap4 = paramParcel.readHashMap(HashMap.class.getClassLoader());
    this.mMap5 = paramParcel.readHashMap(HashMap.class.getClassLoader());
    this.mMap6 = paramParcel.readHashMap(HashMap.class.getClassLoader());
  }
  
  public HashMap<String, List<String>> getBlackResolveMap() {
    return this.mMap1;
  }
  
  public HashMap<String, List<String>> getResolveMap() {
    return this.mMap2;
  }
  
  public HashMap<String, List<String>> getChooseMap() {
    return this.mMap3;
  }
  
  public HashMap<String, List<String>> getBlackChoosePackageMap() {
    return this.mMap4;
  }
  
  public HashMap<String, List<String>> getBlackChooseActivityMap() {
    return this.mMap5;
  }
  
  public HashMap<String, String> getIconName() {
    return this.mMap6;
  }
  
  public OplusResolveData() {}
}
