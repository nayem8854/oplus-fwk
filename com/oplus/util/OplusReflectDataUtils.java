package com.oplus.util;

import android.app.OplusActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.FileObserver;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.HashSet;
import org.xmlpull.v1.XmlPullParser;

public class OplusReflectDataUtils {
  private boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private FileObserverPolicy mReflectDataFileObserver = null;
  
  private final Object mAccidentallyReflectLock = new Object();
  
  private static OplusReflectData mReflectData = null;
  
  private static OplusReflectDataUtils mReflectUtils = null;
  
  private boolean mReflectEnable = true;
  
  private boolean mHasInit = false;
  
  private static final String COLOR_DIRECT_CONFIG_DIR = "/data/oppo/coloros/colordirect";
  
  private static final String COLOR_DIRECT_CONFIG_FILE_PATH = "/data/oppo/coloros/colordirect/sys_direct_widget_config_list.xml";
  
  private static final String TAG = "OplusReflectDataUtils";
  
  private static final String TAG_ENABLE = "reflect_enable";
  
  private static final String TAG_RELECT_ATTR_CLASS = "className";
  
  private static final String TAG_RELECT_ATTR_FIELD = "field";
  
  private static final String TAG_RELECT_ATTR_LEVEL = "fieldLevel";
  
  private static final String TAG_RELECT_ATTR_PACKAGE = "packageName";
  
  private static final String TAG_RELECT_ATTR_VERSION = "versionCode";
  
  private static final String TAG_RELECT_WIDGET = "reflect_widget";
  
  public static OplusReflectDataUtils getInstance() {
    if (mReflectUtils == null)
      mReflectUtils = new OplusReflectDataUtils(); 
    return mReflectUtils;
  }
  
  public void init() {
    initDir();
    initFileObserver();
    if (mReflectData == null)
      mReflectData = new OplusReflectData(); 
    synchronized (this.mAccidentallyReflectLock) {
      readConfigFile();
      return;
    } 
  }
  
  public OplusReflectData getInitData(Context paramContext) {
    if (!this.mHasInit) {
      byte b2;
      this.mHasInit = true;
      initData();
      String str = paramContext.getPackageName();
      byte b1 = -1;
      try {
        b2 = (paramContext.getPackageManager().getPackageInfo(str, 0)).versionCode;
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
        nameNotFoundException.printStackTrace();
        mReflectData = null;
        b2 = b1;
      } 
      if (mReflectData != null && !TextUtils.isEmpty(str) && b2 > 0) {
        mReflectData.initList(str, b2);
      } else {
        OplusReflectData oplusReflectData = mReflectData;
        if (oplusReflectData != null)
          oplusReflectData.setReflectEnable(false); 
      } 
    } 
    return mReflectData;
  }
  
  public OplusReflectData getData() {
    if (mReflectData == null)
      mReflectData = new OplusReflectData(); 
    return mReflectData;
  }
  
  private void initDir() {
    File file1 = new File("/data/oppo/coloros/colordirect");
    File file2 = new File("/data/oppo/coloros/colordirect/sys_direct_widget_config_list.xml");
    try {
      if (!file1.exists())
        file1.mkdirs(); 
      if (!file2.exists())
        file2.createNewFile(); 
    } catch (IOException iOException) {
      iOException.printStackTrace();
    } 
    changeModFile("/data/oppo/coloros/colordirect/sys_direct_widget_config_list.xml");
  }
  
  private void changeModFile(String paramString) {
    try {
      File file = new File();
      this(paramString);
      HashSet<PosixFilePermission> hashSet = new HashSet();
      this();
      hashSet.add(PosixFilePermission.OWNER_READ);
      hashSet.add(PosixFilePermission.OWNER_WRITE);
      hashSet.add(PosixFilePermission.OWNER_EXECUTE);
      hashSet.add(PosixFilePermission.GROUP_READ);
      hashSet.add(PosixFilePermission.GROUP_WRITE);
      hashSet.add(PosixFilePermission.OTHERS_READ);
      hashSet.add(PosixFilePermission.OTHERS_WRITE);
      Path path = Paths.get(file.getAbsolutePath(), new String[0]);
      Files.setPosixFilePermissions(path, hashSet);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" ");
      stringBuilder.append(exception);
      Log.w("OplusReflectDataUtils", stringBuilder.toString());
    } 
  }
  
  private void readConfigFile() {
    File file = new File("/data/oppo/coloros/colordirect/sys_direct_widget_config_list.xml");
    if (!file.exists())
      return; 
    OplusReflectData oplusReflectData1 = mReflectData;
    if (oplusReflectData1 == null) {
      mReflectData = new OplusReflectData();
    } else {
      oplusReflectData1.clearList();
    } 
    FileInputStream fileInputStream1 = null;
    OplusReflectData oplusReflectData2 = null;
    oplusReflectData1 = oplusReflectData2;
    FileInputStream fileInputStream2 = fileInputStream1;
    try {
      int i;
      FileInputStream fileInputStream5 = new FileInputStream();
      oplusReflectData1 = oplusReflectData2;
      fileInputStream2 = fileInputStream1;
      this(file);
      FileInputStream fileInputStream4 = fileInputStream5;
      FileInputStream fileInputStream3 = fileInputStream4;
      fileInputStream2 = fileInputStream4;
      XmlPullParser xmlPullParser = Xml.newPullParser();
      fileInputStream3 = fileInputStream4;
      fileInputStream2 = fileInputStream4;
      xmlPullParser.setInput(fileInputStream4, null);
      do {
        fileInputStream3 = fileInputStream4;
        fileInputStream2 = fileInputStream4;
        i = xmlPullParser.next();
        if (i != 2)
          continue; 
        fileInputStream3 = fileInputStream4;
        fileInputStream2 = fileInputStream4;
        String str = xmlPullParser.getName();
        fileInputStream3 = fileInputStream4;
        fileInputStream2 = fileInputStream4;
        if ("reflect_enable".equals(str)) {
          fileInputStream3 = fileInputStream4;
          fileInputStream2 = fileInputStream4;
          str = xmlPullParser.nextText();
          fileInputStream3 = fileInputStream4;
          fileInputStream2 = fileInputStream4;
          if ("true".equalsIgnoreCase(str)) {
            fileInputStream3 = fileInputStream4;
            fileInputStream2 = fileInputStream4;
            this.mReflectEnable = true;
          } else {
            fileInputStream3 = fileInputStream4;
            fileInputStream2 = fileInputStream4;
            if ("false".equalsIgnoreCase(str)) {
              fileInputStream3 = fileInputStream4;
              fileInputStream2 = fileInputStream4;
              this.mReflectEnable = false;
            } 
          } 
          fileInputStream3 = fileInputStream4;
          fileInputStream2 = fileInputStream4;
          mReflectData.setReflectEnable(this.mReflectEnable);
        } else {
          fileInputStream3 = fileInputStream4;
          fileInputStream2 = fileInputStream4;
          if ("reflect_widget".equals(str)) {
            fileInputStream3 = fileInputStream4;
            fileInputStream2 = fileInputStream4;
            OplusReflectWidget oplusReflectWidget = new OplusReflectWidget();
            fileInputStream3 = fileInputStream4;
            fileInputStream2 = fileInputStream4;
            this();
            fileInputStream3 = fileInputStream4;
            fileInputStream2 = fileInputStream4;
            int j = xmlPullParser.getAttributeCount();
            for (byte b = 0; b < j; b++) {
              fileInputStream3 = fileInputStream4;
              fileInputStream2 = fileInputStream4;
              String str1 = xmlPullParser.getAttributeName(b);
              fileInputStream3 = fileInputStream4;
              fileInputStream2 = fileInputStream4;
              str = xmlPullParser.getAttributeValue(b);
              fileInputStream3 = fileInputStream4;
              fileInputStream2 = fileInputStream4;
              if ("packageName".equals(str1)) {
                fileInputStream3 = fileInputStream4;
                fileInputStream2 = fileInputStream4;
                oplusReflectWidget.setPackageName(str);
              } else {
                fileInputStream3 = fileInputStream4;
                fileInputStream2 = fileInputStream4;
                if ("versionCode".equals(str1)) {
                  fileInputStream3 = fileInputStream4;
                  fileInputStream2 = fileInputStream4;
                  oplusReflectWidget.setVersionCode(Integer.parseInt(str));
                } else {
                  fileInputStream3 = fileInputStream4;
                  fileInputStream2 = fileInputStream4;
                  if ("className".equals(str1)) {
                    fileInputStream3 = fileInputStream4;
                    fileInputStream2 = fileInputStream4;
                    oplusReflectWidget.setClassName(str);
                  } else {
                    fileInputStream3 = fileInputStream4;
                    fileInputStream2 = fileInputStream4;
                    if ("fieldLevel".equals(str1)) {
                      fileInputStream3 = fileInputStream4;
                      fileInputStream2 = fileInputStream4;
                      oplusReflectWidget.setFieldLevel(Integer.parseInt(str));
                    } else {
                      fileInputStream3 = fileInputStream4;
                      fileInputStream2 = fileInputStream4;
                      if ("field".equals(str1)) {
                        fileInputStream3 = fileInputStream4;
                        fileInputStream2 = fileInputStream4;
                        oplusReflectWidget.setField(str);
                      } 
                    } 
                  } 
                } 
              } 
            } 
            fileInputStream3 = fileInputStream4;
            fileInputStream2 = fileInputStream4;
            mReflectData.addReflectWidget(oplusReflectWidget);
          } 
        } 
      } while (i != 1);
      try {
        fileInputStream4.close();
      } catch (IOException iOException) {
        iOException.printStackTrace();
      } 
    } catch (Exception exception) {
      FileInputStream fileInputStream = fileInputStream2;
      exception.printStackTrace();
      if (fileInputStream2 != null)
        fileInputStream2.close(); 
    } finally {}
  }
  
  private void initFileObserver() {
    FileObserverPolicy fileObserverPolicy = new FileObserverPolicy("/data/oppo/coloros/colordirect/sys_direct_widget_config_list.xml");
    fileObserverPolicy.startWatching();
  }
  
  class FileObserverPolicy extends FileObserver {
    private String focusPath;
    
    final OplusReflectDataUtils this$0;
    
    public FileObserverPolicy(String param1String) {
      super(param1String, 8);
      this.focusPath = param1String;
    }
    
    public void onEvent(int param1Int, String param1String) {
      if (param1Int == 8 && 
        this.focusPath.equals("/data/oppo/coloros/colordirect/sys_direct_widget_config_list.xml"))
        synchronized (OplusReflectDataUtils.this.mAccidentallyReflectLock) {
          OplusReflectDataUtils.this.readConfigFile();
        }  
    }
  }
  
  public void initData() {
    try {
      OplusActivityManager oplusActivityManager = new OplusActivityManager();
      this();
      mReflectData = oplusActivityManager.getReflectData();
    } catch (RemoteException remoteException) {
      mReflectData = null;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("init data error , ");
      stringBuilder.append(remoteException);
      Log.e("OplusReflectDataUtils", stringBuilder.toString());
    } 
  }
  
  public Field getTextField(Context paramContext, Class<? extends View> paramClass) {
    Field field;
    String str1 = null, str2 = null;
    OplusReflectWidget oplusReflectWidget = null;
    OplusReflectData oplusReflectData = getInitData(paramContext);
    String str3 = str2;
    if (oplusReflectData != null) {
      str3 = str2;
      if (oplusReflectData.isReflectEnable()) {
        OplusReflectWidget oplusReflectWidget1 = oplusReflectData.findWidget(paramContext, paramContext.getPackageName(), paramClass.getName());
        str3 = str2;
        if (oplusReflectWidget1 != null) {
          str3 = str1;
          try {
            Field field1;
            int i = oplusReflectWidget1.getFieldLevel();
            str3 = str1;
            str2 = oplusReflectWidget1.getField();
            if (i == 0) {
              str3 = str1;
              field1 = paramClass.getDeclaredField(str2);
            } else if (i == 1) {
              str3 = str1;
              field1 = paramClass.getSuperclass().getDeclaredField(str2);
            } else {
              oplusReflectWidget1 = oplusReflectWidget;
              if (i == 2) {
                str3 = str1;
                field1 = paramClass.getSuperclass().getSuperclass().getDeclaredField(str2);
              } 
            } 
            if (field1 != null) {
              Field field2 = field1;
              field1.setAccessible(true);
            } 
            field = field1;
          } catch (Exception exception) {
            exception.printStackTrace();
          } 
        } 
      } 
    } 
    return field;
  }
}
