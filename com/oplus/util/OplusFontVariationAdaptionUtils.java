package com.oplus.util;

import android.app.OplusActivityManager;
import android.content.Context;
import android.os.FileObserver;
import android.os.SystemProperties;
import android.util.ArrayMap;
import android.util.Log;
import android.util.Xml;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.HashSet;
import org.xmlpull.v1.XmlPullParser;

public class OplusFontVariationAdaptionUtils {
  private static OplusFontVariationAdaptionData mData = null;
  
  private static OplusFontVariationAdaptionUtils mUtils = null;
  
  private final Object mLock = new Object();
  
  private boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private FileObserverPolicy mFileObserver = null;
  
  private ArrayList<String> mList1 = new ArrayList<>();
  
  private ArrayList<String> mList2 = new ArrayList<>();
  
  private boolean mEnable = true;
  
  private ArrayMap<String, String> mDefaultMap = new ArrayMap();
  
  private ArrayMap<String, String> mApkMap = new ArrayMap();
  
  private static final String FONT_VARIATION_FILE = "/data/oppo/coloros/oppoguardelf/sys_font_variation_config.xml";
  
  private static final String TAG = "ColorFontVariationAdaption";
  
  private static final String TAG_APK_NAME = "apk-name";
  
  private static final String TAG_ENABLE = "enable";
  
  private static final String TAG_WGHT = "wght";
  
  private Context mContext;
  
  private String mXmlEnable;
  
  public static OplusFontVariationAdaptionUtils getInstance() {
    if (mUtils == null)
      mUtils = new OplusFontVariationAdaptionUtils(); 
    return mUtils;
  }
  
  public void init(Context paramContext) {
    this.mContext = paramContext;
    initDir();
    initFileObserver();
    if (mData == null)
      mData = new OplusFontVariationAdaptionData(); 
    this.mList1 = mData.getAppNameList();
    this.mList2 = mData.getWghtValueList();
    synchronized (this.mLock) {
      readConfigFile();
      return;
    } 
  }
  
  private void initDefaultList() {
    this.mDefaultMap.clear();
  }
  
  public OplusFontVariationAdaptionData getData() {
    if (mData == null)
      mData = new OplusFontVariationAdaptionData(); 
    return mData;
  }
  
  private void initDir() {
    File file = new File("/data/oppo/coloros/oppoguardelf/sys_font_variation_config.xml");
    try {
      if (!file.exists())
        file.createNewFile(); 
    } catch (IOException iOException) {
      iOException.printStackTrace();
    } 
    changeModFile("/data/oppo/coloros/oppoguardelf/sys_font_variation_config.xml");
  }
  
  private void changeModFile(String paramString) {
    try {
      File file = new File();
      this(paramString);
      HashSet<PosixFilePermission> hashSet = new HashSet();
      this();
      hashSet.add(PosixFilePermission.OWNER_READ);
      hashSet.add(PosixFilePermission.OWNER_WRITE);
      hashSet.add(PosixFilePermission.OWNER_EXECUTE);
      hashSet.add(PosixFilePermission.GROUP_READ);
      hashSet.add(PosixFilePermission.GROUP_WRITE);
      hashSet.add(PosixFilePermission.OTHERS_READ);
      hashSet.add(PosixFilePermission.OTHERS_WRITE);
      Path path = Paths.get(file.getAbsolutePath(), new String[0]);
      Files.setPosixFilePermissions(path, hashSet);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" ");
      stringBuilder.append(exception);
      Log.w("ColorFontVariationAdaption", stringBuilder.toString());
    } 
  }
  
  private void readConfigFile() {
    File file = new File("/data/oppo/coloros/oppoguardelf/sys_font_variation_config.xml");
    if (!file.exists())
      return; 
    this.mList1.clear();
    this.mList2.clear();
    FileInputStream fileInputStream1 = null, fileInputStream2 = null;
    FileInputStream fileInputStream3 = fileInputStream2, fileInputStream4 = fileInputStream1;
    try {
      int i;
      FileInputStream fileInputStream = new FileInputStream();
      fileInputStream3 = fileInputStream2;
      fileInputStream4 = fileInputStream1;
      this(file);
      fileInputStream1 = fileInputStream;
      fileInputStream3 = fileInputStream1;
      fileInputStream4 = fileInputStream1;
      XmlPullParser xmlPullParser = Xml.newPullParser();
      fileInputStream3 = fileInputStream1;
      fileInputStream4 = fileInputStream1;
      xmlPullParser.setInput(fileInputStream1, null);
      String str = "";
      do {
        fileInputStream3 = fileInputStream1;
        fileInputStream4 = fileInputStream1;
        i = xmlPullParser.next();
        String str1 = str;
        if (i == 2) {
          fileInputStream3 = fileInputStream1;
          fileInputStream4 = fileInputStream1;
          String str2 = xmlPullParser.getName();
          fileInputStream3 = fileInputStream1;
          fileInputStream4 = fileInputStream1;
          if ("apk-name".equals(str2)) {
            fileInputStream3 = fileInputStream1;
            fileInputStream4 = fileInputStream1;
            str = xmlPullParser.getAttributeValue(null, "name");
            fileInputStream3 = fileInputStream1;
            fileInputStream4 = fileInputStream1;
            this.mList1.add(str);
          } 
          fileInputStream3 = fileInputStream1;
          fileInputStream4 = fileInputStream1;
          if ("wght".equals(str2)) {
            fileInputStream3 = fileInputStream1;
            fileInputStream4 = fileInputStream1;
            str1 = xmlPullParser.getAttributeValue(null, "value");
            fileInputStream3 = fileInputStream1;
            fileInputStream4 = fileInputStream1;
            this.mList2.add(str1);
            fileInputStream3 = fileInputStream1;
            fileInputStream4 = fileInputStream1;
            this.mApkMap.put(str, str1);
          } 
          str1 = str;
          fileInputStream3 = fileInputStream1;
          fileInputStream4 = fileInputStream1;
          if ("enable".equals(str2)) {
            fileInputStream3 = fileInputStream1;
            fileInputStream4 = fileInputStream1;
            this.mXmlEnable = xmlPullParser.nextText();
            fileInputStream3 = fileInputStream1;
            fileInputStream4 = fileInputStream1;
            if (this.DEBUG) {
              fileInputStream3 = fileInputStream1;
              fileInputStream4 = fileInputStream1;
              StringBuilder stringBuilder = new StringBuilder();
              fileInputStream3 = fileInputStream1;
              fileInputStream4 = fileInputStream1;
              this();
              fileInputStream3 = fileInputStream1;
              fileInputStream4 = fileInputStream1;
              stringBuilder.append("mEnable = ");
              fileInputStream3 = fileInputStream1;
              fileInputStream4 = fileInputStream1;
              stringBuilder.append(this.mXmlEnable);
              fileInputStream3 = fileInputStream1;
              fileInputStream4 = fileInputStream1;
              Log.d("ColorFontVariationAdaption", stringBuilder.toString());
            } 
            fileInputStream3 = fileInputStream1;
            fileInputStream4 = fileInputStream1;
            mData.setEnable(this.mXmlEnable);
            str1 = str;
          } 
        } 
        str = str1;
      } while (i != 1);
      try {
        fileInputStream1.close();
      } catch (IOException iOException) {
        iOException.printStackTrace();
      } 
    } catch (Exception exception) {
      fileInputStream3 = fileInputStream4;
      exception.printStackTrace();
      if (fileInputStream4 != null)
        fileInputStream4.close(); 
    } finally {}
  }
  
  private void initFileObserver() {
    FileObserverPolicy fileObserverPolicy = new FileObserverPolicy("/data/oppo/coloros/oppoguardelf/sys_font_variation_config.xml");
    fileObserverPolicy.startWatching();
  }
  
  public void initData(Context paramContext) {
    try {
      OplusActivityManager oplusActivityManager = new OplusActivityManager();
      this();
      OplusFontVariationAdaptionData oplusFontVariationAdaptionData = oplusActivityManager.getFontVariationAdaption();
      this.mList1 = oplusFontVariationAdaptionData.getAppNameList();
      this.mList2 = oplusFontVariationAdaptionData.getWghtValueList();
      initDefaultList();
      coverListToMap();
      if ("false".equals(oplusFontVariationAdaptionData.getEnable()))
        this.mEnable = false; 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("init data Exception , ");
      stringBuilder.append(exception);
      Log.e("ColorFontVariationAdaption", stringBuilder.toString());
    } 
  }
  
  private void coverListToMap() {
    for (String str : this.mList1)
      this.mApkMap.put(str, this.mList2.get(this.mList1.indexOf(str))); 
  }
  
  public int getAdaptionValue(String paramString) {
    initData(this.mContext);
    if (this.DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("packageName: ");
      stringBuilder.append(paramString);
      Log.d("ColorFontVariationAdaption", stringBuilder.toString());
      stringBuilder = new StringBuilder();
      stringBuilder.append("mApkMap: ");
      stringBuilder.append(this.mApkMap);
      stringBuilder.append(", mDefaultMap:");
      stringBuilder.append(this.mDefaultMap);
      Log.d("ColorFontVariationAdaption", stringBuilder.toString());
    } 
    boolean bool = this.mEnable;
    int i = 0;
    byte b = 0;
    if (!bool)
      return 0; 
    if (!this.mDefaultMap.isEmpty() && this.mDefaultMap.containsKey(paramString)) {
      paramString = (String)this.mDefaultMap.get(paramString);
      if (paramString == null) {
        i = b;
      } else {
        i = Integer.parseInt(paramString);
      } 
      return i;
    } 
    if (!this.mApkMap.isEmpty() && this.mApkMap.containsKey(paramString)) {
      paramString = (String)this.mApkMap.get(paramString);
      if (paramString != null)
        i = Integer.parseInt(paramString); 
      return i;
    } 
    return 0;
  }
  
  class FileObserverPolicy extends FileObserver {
    private String focusPath;
    
    final OplusFontVariationAdaptionUtils this$0;
    
    public FileObserverPolicy(String param1String) {
      super(param1String, 8);
      this.focusPath = param1String;
    }
    
    public void onEvent(int param1Int, String param1String) {
      if (OplusFontVariationAdaptionUtils.this.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("FileObserver:");
        stringBuilder.append(param1Int);
        stringBuilder.append(",");
        stringBuilder.append(param1String);
        Log.d("ColorFontVariationAdaption", stringBuilder.toString());
      } 
      if (param1Int == 8)
        synchronized (OplusFontVariationAdaptionUtils.this.mLock) {
          OplusFontVariationAdaptionUtils.this.readConfigFile();
        }  
    }
  }
  
  public ArrayMap<String, String> getmApkMap() {
    return this.mApkMap;
  }
}
