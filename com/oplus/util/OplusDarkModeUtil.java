package com.oplus.util;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;
import com.android.internal.graphics.ColorUtils;
import java.lang.reflect.Method;

public class OplusDarkModeUtil {
  public static boolean isNightMode(Context paramContext) {
    boolean bool;
    Configuration configuration = paramContext.getResources().getConfiguration();
    int i = configuration.uiMode;
    if (32 == (i & 0x30)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static void setForceDarkAllow(View paramView, boolean paramBoolean) {
    if (Build.VERSION.SDK_INT >= 29) {
      paramView.setForceDarkAllowed(paramBoolean);
    } else {
      try {
        Method method = View.class.getDeclaredMethod("setForceDarkAllowed", new Class[] { boolean.class });
        method.setAccessible(true);
        method.invoke(paramView, new Object[] { Boolean.valueOf(paramBoolean) });
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
    } 
  }
  
  public static int makeDarkLimit(int paramInt, float paramFloat) {
    float[] arrayOfFloat = new float[3];
    ColorUtils.colorToHSL(paramInt, arrayOfFloat);
    paramFloat = Math.max(paramFloat, 1.0F - arrayOfFloat[2]);
    if (paramFloat < arrayOfFloat[2]) {
      arrayOfFloat[2] = paramFloat;
      return ColorUtils.HSLToColor(arrayOfFloat);
    } 
    return paramInt;
  }
  
  public static int makeLight(int paramInt) {
    float[] arrayOfFloat = new float[3];
    ColorUtils.colorToHSL(paramInt, arrayOfFloat);
    float f = 1.0F - arrayOfFloat[2];
    if (f > arrayOfFloat[2]) {
      arrayOfFloat[2] = f;
      return ColorUtils.HSLToColor(arrayOfFloat);
    } 
    return paramInt;
  }
  
  public static int makeDark(int paramInt) {
    float[] arrayOfFloat = new float[3];
    ColorUtils.colorToHSL(paramInt, arrayOfFloat);
    float f = 1.0F - arrayOfFloat[2];
    if (f < arrayOfFloat[2]) {
      arrayOfFloat[2] = f;
      return ColorUtils.HSLToColor(arrayOfFloat);
    } 
    return paramInt;
  }
  
  public static void makeImageViewDark(ImageView paramImageView) {
    if (paramImageView != null)
      paramImageView.setColorFilter(getDarkFilter()); 
  }
  
  public static void makeDrawableDark(Drawable paramDrawable) {
    if (paramDrawable != null)
      paramDrawable.setColorFilter(getDarkFilter()); 
  }
  
  private static ColorFilter getDarkFilter() {
    return (ColorFilter)new LightingColorFilter(-2236963, 0);
  }
}
