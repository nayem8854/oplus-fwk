package com.oplus.util;

import android.app.ActivityManager;
import android.app.OplusActivityManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.FileObserver;
import android.os.Handler;
import android.os.OplusSystemProperties;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.util.Slog;
import com.android.internal.content.PackageMonitor;
import com.oplus.content.OplusFeatureConfigManager;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class OplusDisplayCompatUtils {
  public static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static OplusDisplayCompatData sCompatData = null;
  
  private static List<String> sIncludeImmersiveList = new ArrayList<>();
  
  static {
    sExcludeImmersivedList = new ArrayList<>();
    sDisplayCompatUtils = null;
    sIncludeImmersiveList.clear();
    sIncludeImmersiveList.add("com.nearme.gamecenter.ddz.nearme.gamecenter");
    sIncludeImmersiveList.add("com.oppo.reader");
    sIncludeImmersiveList.add("com.oppo.book");
    sIncludeImmersiveList.add("com.google.android.inputmethod.latin");
    sIncludeImmersiveList.add("com.oppo.cameracom.android.ctslocker");
    sIncludeImmersiveList.add("jp.softbank.mb.parentalcontrols");
    sIncludeImmersiveList.add("com.android.vending");
    sIncludeImmersiveList.add("com.android.chrome");
    sIncludeImmersiveList.add("com.google.android.dialer");
    sIncludeImmersiveList.add("com.android.permissioncontroller");
    sIncludeImmersiveList.add("com.google.android.permissioncontroller");
    sIncludeImmersiveList.add("com.google.android.apps.maps");
    sIncludeImmersiveList.add("com.heytap.yoli");
    sIncludeImmersiveList.add("com.google.android.gms");
    sExcludeImmersivedList.clear();
    sExcludeImmersivedList.add("com.android.calculator2");
    sExcludeImmersivedList.add("com.android.calendar");
    sExcludeImmersivedList.add("com.ctsi.emm");
    sExcludeImmersivedList.add("com.justsy.launcher");
    sExcludeImmersivedList.add("com.justsy.portal");
    sExcludeImmersivedList.add("com.justsy.mdm");
  }
  
  private final Object mLock = new Object();
  
  private final MyPackageMonitor mMyPackageMonitor = new MyPackageMonitor();
  
  private Context mContext = null;
  
  private PackageManager mPackageManager = null;
  
  private boolean mEnableDisplayCompat = true;
  
  private boolean mHasHeteromorphismFeature = false;
  
  private boolean mImmersiveDefault = false;
  
  private int mDisplayCutoutType = 0;
  
  private List<String> mWhiteList = new ArrayList<>();
  
  private List<String> mBlackList = new ArrayList<>();
  
  private List<String> mLocalCompatAppsList = new ArrayList<>();
  
  private List<String> mLocalFullScreenAppsList = new ArrayList<>();
  
  private HashMap<String, String> mCompatPackageList = new HashMap<>();
  
  private List<String> mLocalDefaultModeList = new ArrayList<>();
  
  private List<String> mLocalShowModeList = new ArrayList<>();
  
  private List<String> mLocalHideModeList = new ArrayList<>();
  
  private List<String> mInstalledCompatList = new ArrayList<>();
  
  private List<String> mInstalledImeList = new ArrayList<>();
  
  private List<String> mAlreadyShowDialogAppsList = new ArrayList<>();
  
  private List<String> mRusImmersiveAppsList = new ArrayList<>();
  
  private List<String> mRusNonImmersiveAppsList = new ArrayList<>();
  
  private List<String> mInstalledThirdPartyAppList = new ArrayList<>();
  
  private List<String> mNeedAdjustSizeAppList = new ArrayList<>();
  
  private FileObserverPolicy mDisplayCompatFileObserver = null;
  
  private LocalCompatSettingsObserverPolicy mLocalCompatSettingsObserver = null;
  
  private LocalCutoutSettingsObserverPolicy mLocalCutoutSettingsObserver = null;
  
  private LocalShowDialogSettingsObserverPolicy mLocalShowDialogSettingsObserver = null;
  
  private static final int CUTOUT_MODE_DEFAULT = 0;
  
  private static final int CUTOUT_MODE_HIDE = 2;
  
  private static final int CUTOUT_MODE_SHOW = 1;
  
  private static final float DEFAULT_MAX_ASPECT_RATIO = 2.0F;
  
  private static final int DISPLAY_CUTOUT_POSITION_LEFT = 1;
  
  private static final int DISPLAY_CUTOUT_POSITION_MIDDLE = 2;
  
  private static final int DISPLAY_CUTOUT_POSITION_NONE = 0;
  
  private static final String KEY_APP_LIST_CUTOUT_DEFAULT = "key_display_nonimmersive_local_apps";
  
  private static final String KEY_APP_LIST_CUTOUT_DEFAULT_OLD = "key_display_nonimmersive_local_apps";
  
  private static final String KEY_APP_LIST_CUTOUT_HIDE = "cutout_hide_app_list";
  
  private static final String KEY_APP_LIST_CUTOUT_SHOW = "key_display_immersive_local_apps";
  
  private static final String KEY_APP_LIST_CUTOUT_SHOW_OLD = "key_display_immersive_local_apps";
  
  private static final String KEY_LOCAL_COMPAT_APPS = "key_display_compat_local_apps_v1";
  
  private static final String KEY_LOCAL_FULLSCREEN_APPS = "key_display_fullscreen_local_apps_v1";
  
  private static final String KEY_SHOW_FULLSCREEN_DIALOG_APPS = "key_display_show_dialog_local_apps";
  
  private static final String OPLUS_DISPLAY_COMPAT_CONFIG_DIR = "/data/oppo/coloros/displaycompat";
  
  private static final String OPLUS_DISPLAY_COMPAT_CONFIG_FILE_PATH = "/data/oppo/coloros/displaycompat/sys_display_compat_config.xml";
  
  public static final int OPLUS_LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHOW = 5;
  
  private static final String TAG = "OplusDisplayCompatUtils";
  
  private static final String TAG_BLACK = "black";
  
  private static final String TAG_COMPAT = "compat";
  
  private static final String TAG_ENABLE = "enable_display_compat";
  
  private static final String TAG_ENABLE_IMMERSIVE = "enable_display_immersive";
  
  private static final String TAG_IMMERSIVE = "immersive";
  
  private static final String TAG_NONIMMERSIVE = "nonimmersive";
  
  private static final String TAG_SIZE = "size";
  
  private static final String TAG_WHITE = "white";
  
  private static final String VERSION_NAME_EMPTY = "empty";
  
  private static volatile OplusDisplayCompatUtils sDisplayCompatUtils;
  
  private static List<String> sExcludeImmersivedList;
  
  private BroadcastReceiver mMultiUserReceiver;
  
  private final BroadcastReceiver mReceiver;
  
  public static OplusDisplayCompatUtils getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/util/OplusDisplayCompatUtils.sDisplayCompatUtils : Lcom/oplus/util/OplusDisplayCompatUtils;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/util/OplusDisplayCompatUtils
    //   8: monitorenter
    //   9: getstatic com/oplus/util/OplusDisplayCompatUtils.sDisplayCompatUtils : Lcom/oplus/util/OplusDisplayCompatUtils;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/util/OplusDisplayCompatUtils
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/util/OplusDisplayCompatUtils.sDisplayCompatUtils : Lcom/oplus/util/OplusDisplayCompatUtils;
    //   27: ldc com/oplus/util/OplusDisplayCompatUtils
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/util/OplusDisplayCompatUtils
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/util/OplusDisplayCompatUtils.sDisplayCompatUtils : Lcom/oplus/util/OplusDisplayCompatUtils;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #211	-> 0
    //   #212	-> 6
    //   #213	-> 9
    //   #214	-> 15
    //   #216	-> 27
    //   #218	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public void init(Context paramContext) {
    this.mContext = paramContext;
    this.mPackageManager = paramContext.getPackageManager();
    if (sCompatData == null)
      sCompatData = new OplusDisplayCompatData(); 
    boolean bool = OplusFeatureConfigManager.getInstacne().hasFeature("oplus.software.display.screen_heteromorphism");
    if (bool) {
      setDisplayCutoutType();
      synchronized (this.mLock) {
        this.mHasHeteromorphismFeature = true;
        sCompatData.setHasHeteromorphismFeature(true);
      } 
    } 
    initDir();
    initFileObserver();
    initLocalCompatSettingsObserver();
    initLocalCutoutSettingsObserver();
    initLocalShowDialogSettingsObserver();
    loadLocalCompatAppList();
    loadLocalFullScreenAppList();
    refreshLocalDefaultModeList();
    refreshLocalShowModeList();
    refreshLocalHideModeList();
    loadInstalledImeAppList();
    loadLocalShowDialogAppList();
    loadInstalledCompatAppList();
    loadInstalledThirdPartyApps();
    readDisplayCompatConfig();
    registerPackageMonitor();
    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction("android.intent.action.BOOT_COMPLETED");
    this.mContext.registerReceiver(this.mReceiver, intentFilter);
    intentFilter = new IntentFilter();
    intentFilter.addAction("android.intent.action.USER_SWITCHED");
    intentFilter.addAction("android.intent.action.USER_ADDED");
    intentFilter.addAction("android.intent.action.USER_REMOVED");
    this.mContext.registerReceiver(this.mMultiUserReceiver, intentFilter);
  }
  
  public void initData(Context paramContext) {
    this.mContext = paramContext;
    if (paramContext != null)
      this.mPackageManager = paramContext.getPackageManager(); 
    initData();
  }
  
  public void initData() {
    try {
      OplusActivityManager oplusActivityManager = new OplusActivityManager();
      this();
      OplusDisplayCompatData oplusDisplayCompatData = oplusActivityManager.getDisplayCompatData();
      sCompatData = oplusDisplayCompatData;
      this.mEnableDisplayCompat = oplusDisplayCompatData.getDisplayCompatEnabled();
      this.mHasHeteromorphismFeature = oplusDisplayCompatData.hasHeteromorphismFeature();
      if (this.mWhiteList != null) {
        this.mWhiteList.clear();
        this.mWhiteList = oplusDisplayCompatData.getWhiteList();
      } 
      if (this.mBlackList != null) {
        this.mBlackList.clear();
        this.mBlackList = oplusDisplayCompatData.getBlackList();
      } 
      if (this.mLocalCompatAppsList != null) {
        this.mLocalCompatAppsList.clear();
        this.mLocalCompatAppsList = oplusDisplayCompatData.getLocalCompatList();
      } 
      if (this.mLocalFullScreenAppsList != null) {
        this.mLocalFullScreenAppsList.clear();
        this.mLocalFullScreenAppsList = oplusDisplayCompatData.getLocalFullScreenList();
      } 
      if (this.mLocalDefaultModeList != null) {
        this.mLocalDefaultModeList.clear();
        this.mLocalDefaultModeList = oplusDisplayCompatData.getLocalCutoutDefaultList();
      } 
      if (this.mLocalShowModeList != null) {
        this.mLocalShowModeList.clear();
        this.mLocalShowModeList = oplusDisplayCompatData.getLocalCutoutShowList();
      } 
      if (this.mLocalHideModeList != null) {
        this.mLocalHideModeList.clear();
        this.mLocalHideModeList = oplusDisplayCompatData.getLocalCutoutHideList();
      } 
      if (this.mCompatPackageList != null) {
        this.mCompatPackageList.clear();
        this.mCompatPackageList = oplusDisplayCompatData.getCompatPackageList();
      } 
      if (this.mInstalledCompatList != null) {
        this.mInstalledCompatList.clear();
        this.mInstalledCompatList = oplusDisplayCompatData.getInstalledCompatList();
      } 
      if (this.mInstalledImeList != null) {
        this.mInstalledImeList.clear();
        this.mInstalledImeList = oplusDisplayCompatData.getInstalledImeList();
      } 
      if (this.mAlreadyShowDialogAppsList != null) {
        this.mAlreadyShowDialogAppsList.clear();
        this.mAlreadyShowDialogAppsList = oplusDisplayCompatData.getShowDialogAppList();
      } 
      this.mImmersiveDefault = oplusDisplayCompatData.getRusImmersiveDefault();
      if (this.mRusImmersiveAppsList != null) {
        this.mRusImmersiveAppsList.clear();
        this.mRusImmersiveAppsList = oplusDisplayCompatData.getRusImmersiveList();
      } 
      if (this.mRusNonImmersiveAppsList != null) {
        this.mRusNonImmersiveAppsList.clear();
        this.mRusNonImmersiveAppsList = oplusDisplayCompatData.getRusNonImmersiveList();
      } 
      if (this.mInstalledThirdPartyAppList != null) {
        this.mInstalledThirdPartyAppList.clear();
        this.mInstalledThirdPartyAppList = oplusDisplayCompatData.getInstalledThirdPartyAppList();
      } 
      if (this.mNeedAdjustSizeAppList != null) {
        this.mNeedAdjustSizeAppList.clear();
        this.mNeedAdjustSizeAppList = oplusDisplayCompatData.getNeedAdjustSizeAppList();
      } 
      this.mDisplayCutoutType = oplusDisplayCompatData.getDisplayCutoutType();
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("init data error , ");
      stringBuilder.append(remoteException);
      Slog.e("OplusDisplayCompatUtils", stringBuilder.toString());
    } 
  }
  
  public OplusDisplayCompatData getDisplayCompatData() {
    if (sCompatData == null)
      sCompatData = new OplusDisplayCompatData(); 
    return sCompatData;
  }
  
  private void initDir() {
    if (DEBUG)
      Slog.i("OplusDisplayCompatUtils", "initDir start"); 
    File file1 = new File("/data/oppo/coloros/displaycompat");
    File file2 = new File("/data/oppo/coloros/displaycompat/sys_display_compat_config.xml");
    try {
      if (!file1.exists())
        file1.mkdirs(); 
      if (!file2.exists())
        file2.createNewFile(); 
    } catch (IOException iOException) {
      Slog.e("OplusDisplayCompatUtils", "initDir failed!!!");
      iOException.printStackTrace();
    } 
    changeModFile("/data/oppo/coloros/displaycompat/sys_display_compat_config.xml");
  }
  
  private void initFileObserver() {
    FileObserverPolicy fileObserverPolicy = new FileObserverPolicy("/data/oppo/coloros/displaycompat/sys_display_compat_config.xml");
    fileObserverPolicy.startWatching();
  }
  
  private void initLocalCompatSettingsObserver() {
    this.mLocalCompatSettingsObserver = new LocalCompatSettingsObserverPolicy();
    Context context = this.mContext;
    if (context != null) {
      ContentResolver contentResolver1 = context.getContentResolver();
      Uri uri1 = Settings.Global.getUriFor("key_display_compat_local_apps_v1");
      LocalCompatSettingsObserverPolicy localCompatSettingsObserverPolicy2 = this.mLocalCompatSettingsObserver;
      contentResolver1.registerContentObserver(uri1, true, localCompatSettingsObserverPolicy2);
      ContentResolver contentResolver2 = this.mContext.getContentResolver();
      Uri uri2 = Settings.Global.getUriFor("key_display_fullscreen_local_apps_v1");
      LocalCompatSettingsObserverPolicy localCompatSettingsObserverPolicy1 = this.mLocalCompatSettingsObserver;
      contentResolver2.registerContentObserver(uri2, true, localCompatSettingsObserverPolicy1);
    } 
  }
  
  private void initLocalCutoutSettingsObserver() {
    this.mLocalCutoutSettingsObserver = new LocalCutoutSettingsObserverPolicy();
    Context context = this.mContext;
    if (context != null) {
      ContentResolver contentResolver3 = context.getContentResolver();
      Uri uri3 = Settings.Global.getUriFor("key_display_nonimmersive_local_apps");
      LocalCutoutSettingsObserverPolicy localCutoutSettingsObserverPolicy2 = this.mLocalCutoutSettingsObserver;
      contentResolver3.registerContentObserver(uri3, true, localCutoutSettingsObserverPolicy2);
      ContentResolver contentResolver1 = this.mContext.getContentResolver();
      Uri uri1 = Settings.Global.getUriFor("key_display_immersive_local_apps");
      LocalCutoutSettingsObserverPolicy localCutoutSettingsObserverPolicy3 = this.mLocalCutoutSettingsObserver;
      contentResolver1.registerContentObserver(uri1, true, localCutoutSettingsObserverPolicy3);
      ContentResolver contentResolver2 = this.mContext.getContentResolver();
      Uri uri2 = Settings.Global.getUriFor("cutout_hide_app_list");
      LocalCutoutSettingsObserverPolicy localCutoutSettingsObserverPolicy1 = this.mLocalCutoutSettingsObserver;
      contentResolver2.registerContentObserver(uri2, true, localCutoutSettingsObserverPolicy1);
    } 
  }
  
  private void initLocalShowDialogSettingsObserver() {
    this.mLocalShowDialogSettingsObserver = new LocalShowDialogSettingsObserverPolicy();
    Context context = this.mContext;
    if (context != null) {
      ContentResolver contentResolver = context.getContentResolver();
      Uri uri = Settings.Global.getUriFor("key_display_show_dialog_local_apps");
      LocalShowDialogSettingsObserverPolicy localShowDialogSettingsObserverPolicy = this.mLocalShowDialogSettingsObserver;
      contentResolver.registerContentObserver(uri, true, localShowDialogSettingsObserverPolicy);
    } 
  }
  
  private void changeModFile(String paramString) {
    try {
      File file = new File();
      this(paramString);
      HashSet<PosixFilePermission> hashSet = new HashSet();
      this();
      hashSet.add(PosixFilePermission.OWNER_READ);
      hashSet.add(PosixFilePermission.OWNER_WRITE);
      hashSet.add(PosixFilePermission.OWNER_EXECUTE);
      hashSet.add(PosixFilePermission.GROUP_READ);
      hashSet.add(PosixFilePermission.GROUP_WRITE);
      hashSet.add(PosixFilePermission.OTHERS_READ);
      hashSet.add(PosixFilePermission.OTHERS_WRITE);
      Path path = Paths.get(file.getAbsolutePath(), new String[0]);
      Files.setPosixFilePermissions(path, hashSet);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" ");
      stringBuilder.append(exception);
      Slog.w("OplusDisplayCompatUtils", stringBuilder.toString());
    } 
  }
  
  public void readDisplayCompatConfig() {
    if (DEBUG)
      Slog.i("OplusDisplayCompatUtils", "readDisplayCompatConfigFile"); 
    File file = new File("/data/oppo/coloros/displaycompat/sys_display_compat_config.xml");
    if (file.exists()) {
      if (file.length() == 0L) {
        loadDefaultDisplayCompatList();
      } else {
        readConfigFromFileLocked(file);
      } 
    } else {
      Slog.i("OplusDisplayCompatUtils", "displaycompatconfig file isn't exist!");
    } 
  }
  
  private void readConfigFromFileLocked(File paramFile) {
    // Byte code:
    //   0: getstatic com/oplus/util/OplusDisplayCompatUtils.DEBUG : Z
    //   3: ifeq -> 15
    //   6: ldc 'OplusDisplayCompatUtils'
    //   8: ldc_w 'readConfigFromFileLocked start'
    //   11: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   14: pop
    //   15: new java/util/ArrayList
    //   18: dup
    //   19: invokespecial <init> : ()V
    //   22: astore_2
    //   23: new java/util/ArrayList
    //   26: dup
    //   27: invokespecial <init> : ()V
    //   30: astore_3
    //   31: new java/util/HashMap
    //   34: dup
    //   35: invokespecial <init> : ()V
    //   38: astore #4
    //   40: new java/util/ArrayList
    //   43: dup
    //   44: invokespecial <init> : ()V
    //   47: astore #5
    //   49: new java/util/ArrayList
    //   52: dup
    //   53: invokespecial <init> : ()V
    //   56: astore #6
    //   58: new java/util/ArrayList
    //   61: dup
    //   62: invokespecial <init> : ()V
    //   65: astore #7
    //   67: aconst_null
    //   68: astore #8
    //   70: aconst_null
    //   71: astore #9
    //   73: aconst_null
    //   74: astore #10
    //   76: aconst_null
    //   77: astore #11
    //   79: new java/io/FileInputStream
    //   82: astore #12
    //   84: aload #11
    //   86: astore #9
    //   88: aload #8
    //   90: astore #10
    //   92: aload #12
    //   94: aload_1
    //   95: invokespecial <init> : (Ljava/io/File;)V
    //   98: aload #12
    //   100: astore_1
    //   101: aload_1
    //   102: astore #9
    //   104: aload_1
    //   105: astore #10
    //   107: invokestatic newPullParser : ()Lorg/xmlpull/v1/XmlPullParser;
    //   110: astore #11
    //   112: aload_1
    //   113: astore #9
    //   115: aload_1
    //   116: astore #10
    //   118: aload #11
    //   120: aload_1
    //   121: aconst_null
    //   122: invokeinterface setInput : (Ljava/io/InputStream;Ljava/lang/String;)V
    //   127: aload_1
    //   128: astore #9
    //   130: aload_1
    //   131: astore #10
    //   133: aload #11
    //   135: invokeinterface next : ()I
    //   140: istore #13
    //   142: iload #13
    //   144: iconst_2
    //   145: if_icmpne -> 899
    //   148: aload_1
    //   149: astore #9
    //   151: aload_1
    //   152: astore #10
    //   154: aload #11
    //   156: invokeinterface getName : ()Ljava/lang/String;
    //   161: astore #12
    //   163: aload_1
    //   164: astore #9
    //   166: aload_1
    //   167: astore #10
    //   169: ldc 'enable_display_compat'
    //   171: aload #12
    //   173: invokevirtual equals : (Ljava/lang/Object;)Z
    //   176: ifeq -> 276
    //   179: aload_1
    //   180: astore #9
    //   182: aload_1
    //   183: astore #10
    //   185: aload #11
    //   187: invokeinterface nextText : ()Ljava/lang/String;
    //   192: astore #8
    //   194: aload_1
    //   195: astore #9
    //   197: aload_1
    //   198: astore #10
    //   200: aload #8
    //   202: ldc_w ''
    //   205: invokevirtual equals : (Ljava/lang/Object;)Z
    //   208: ifne -> 273
    //   211: aload_1
    //   212: astore #9
    //   214: aload_1
    //   215: astore #10
    //   217: aload_0
    //   218: getfield mLock : Ljava/lang/Object;
    //   221: astore #12
    //   223: aload_1
    //   224: astore #9
    //   226: aload_1
    //   227: astore #10
    //   229: aload #12
    //   231: monitorenter
    //   232: aload #8
    //   234: invokestatic parseBoolean : (Ljava/lang/String;)Z
    //   237: istore #14
    //   239: aload_0
    //   240: iload #14
    //   242: putfield mEnableDisplayCompat : Z
    //   245: getstatic com/oplus/util/OplusDisplayCompatUtils.sCompatData : Lcom/oplus/util/OplusDisplayCompatData;
    //   248: iload #14
    //   250: invokevirtual setDisplatOptEnabled : (Z)V
    //   253: aload #12
    //   255: monitorexit
    //   256: goto -> 273
    //   259: astore #8
    //   261: aload #12
    //   263: monitorexit
    //   264: aload_1
    //   265: astore #9
    //   267: aload_1
    //   268: astore #10
    //   270: aload #8
    //   272: athrow
    //   273: goto -> 899
    //   276: aload_1
    //   277: astore #9
    //   279: aload_1
    //   280: astore #10
    //   282: ldc 'white'
    //   284: aload #12
    //   286: invokevirtual equals : (Ljava/lang/Object;)Z
    //   289: ifeq -> 342
    //   292: aload_1
    //   293: astore #9
    //   295: aload_1
    //   296: astore #10
    //   298: aload #11
    //   300: invokeinterface nextText : ()Ljava/lang/String;
    //   305: astore #12
    //   307: aload_1
    //   308: astore #9
    //   310: aload_1
    //   311: astore #10
    //   313: aload #12
    //   315: ldc_w ''
    //   318: invokevirtual equals : (Ljava/lang/Object;)Z
    //   321: ifne -> 339
    //   324: aload_1
    //   325: astore #9
    //   327: aload_1
    //   328: astore #10
    //   330: aload_2
    //   331: aload #12
    //   333: invokeinterface add : (Ljava/lang/Object;)Z
    //   338: pop
    //   339: goto -> 899
    //   342: aload_1
    //   343: astore #9
    //   345: aload_1
    //   346: astore #10
    //   348: ldc 'black'
    //   350: aload #12
    //   352: invokevirtual equals : (Ljava/lang/Object;)Z
    //   355: ifeq -> 408
    //   358: aload_1
    //   359: astore #9
    //   361: aload_1
    //   362: astore #10
    //   364: aload #11
    //   366: invokeinterface nextText : ()Ljava/lang/String;
    //   371: astore #12
    //   373: aload_1
    //   374: astore #9
    //   376: aload_1
    //   377: astore #10
    //   379: aload #12
    //   381: ldc_w ''
    //   384: invokevirtual equals : (Ljava/lang/Object;)Z
    //   387: ifne -> 405
    //   390: aload_1
    //   391: astore #9
    //   393: aload_1
    //   394: astore #10
    //   396: aload_3
    //   397: aload #12
    //   399: invokeinterface add : (Ljava/lang/Object;)Z
    //   404: pop
    //   405: goto -> 899
    //   408: aload_1
    //   409: astore #9
    //   411: aload_1
    //   412: astore #10
    //   414: ldc 'compat'
    //   416: aload #12
    //   418: invokevirtual equals : (Ljava/lang/Object;)Z
    //   421: ifeq -> 508
    //   424: aload_1
    //   425: astore #9
    //   427: aload_1
    //   428: astore #10
    //   430: aload #11
    //   432: aconst_null
    //   433: ldc_w 'package'
    //   436: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   441: astore #15
    //   443: aload #15
    //   445: ifnull -> 505
    //   448: aload_1
    //   449: astore #9
    //   451: aload_1
    //   452: astore #10
    //   454: aload #11
    //   456: aconst_null
    //   457: ldc_w 'versionName'
    //   460: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   465: astore #8
    //   467: aload #8
    //   469: astore #12
    //   471: aload_1
    //   472: astore #9
    //   474: aload_1
    //   475: astore #10
    //   477: aload #8
    //   479: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   482: ifeq -> 489
    //   485: ldc 'empty'
    //   487: astore #12
    //   489: aload_1
    //   490: astore #9
    //   492: aload_1
    //   493: astore #10
    //   495: aload #4
    //   497: aload #15
    //   499: aload #12
    //   501: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   504: pop
    //   505: goto -> 899
    //   508: aload_1
    //   509: astore #9
    //   511: aload_1
    //   512: astore #10
    //   514: ldc 'enable_display_immersive'
    //   516: aload #12
    //   518: invokevirtual equals : (Ljava/lang/Object;)Z
    //   521: ifeq -> 621
    //   524: aload_1
    //   525: astore #9
    //   527: aload_1
    //   528: astore #10
    //   530: aload #11
    //   532: invokeinterface nextText : ()Ljava/lang/String;
    //   537: astore #8
    //   539: aload_1
    //   540: astore #9
    //   542: aload_1
    //   543: astore #10
    //   545: aload #8
    //   547: ldc_w ''
    //   550: invokevirtual equals : (Ljava/lang/Object;)Z
    //   553: ifne -> 618
    //   556: aload_1
    //   557: astore #9
    //   559: aload_1
    //   560: astore #10
    //   562: aload_0
    //   563: getfield mLock : Ljava/lang/Object;
    //   566: astore #12
    //   568: aload_1
    //   569: astore #9
    //   571: aload_1
    //   572: astore #10
    //   574: aload #12
    //   576: monitorenter
    //   577: aload #8
    //   579: invokestatic parseBoolean : (Ljava/lang/String;)Z
    //   582: istore #14
    //   584: aload_0
    //   585: iload #14
    //   587: putfield mImmersiveDefault : Z
    //   590: getstatic com/oplus/util/OplusDisplayCompatUtils.sCompatData : Lcom/oplus/util/OplusDisplayCompatData;
    //   593: iload #14
    //   595: invokevirtual setRusImmersiveDefault : (Z)V
    //   598: aload #12
    //   600: monitorexit
    //   601: goto -> 618
    //   604: astore #8
    //   606: aload #12
    //   608: monitorexit
    //   609: aload_1
    //   610: astore #9
    //   612: aload_1
    //   613: astore #10
    //   615: aload #8
    //   617: athrow
    //   618: goto -> 899
    //   621: aload_1
    //   622: astore #9
    //   624: aload_1
    //   625: astore #10
    //   627: ldc 'immersive'
    //   629: aload #12
    //   631: invokevirtual equals : (Ljava/lang/Object;)Z
    //   634: ifeq -> 688
    //   637: aload_1
    //   638: astore #9
    //   640: aload_1
    //   641: astore #10
    //   643: aload #11
    //   645: invokeinterface nextText : ()Ljava/lang/String;
    //   650: astore #12
    //   652: aload_1
    //   653: astore #9
    //   655: aload_1
    //   656: astore #10
    //   658: aload #12
    //   660: ldc_w ''
    //   663: invokevirtual equals : (Ljava/lang/Object;)Z
    //   666: ifne -> 685
    //   669: aload_1
    //   670: astore #9
    //   672: aload_1
    //   673: astore #10
    //   675: aload #5
    //   677: aload #12
    //   679: invokeinterface add : (Ljava/lang/Object;)Z
    //   684: pop
    //   685: goto -> 899
    //   688: aload_1
    //   689: astore #9
    //   691: aload_1
    //   692: astore #10
    //   694: ldc 'nonimmersive'
    //   696: aload #12
    //   698: invokevirtual equals : (Ljava/lang/Object;)Z
    //   701: ifeq -> 835
    //   704: aload_1
    //   705: astore #9
    //   707: aload_1
    //   708: astore #10
    //   710: aload #11
    //   712: invokeinterface nextText : ()Ljava/lang/String;
    //   717: astore #12
    //   719: aload_1
    //   720: astore #9
    //   722: aload_1
    //   723: astore #10
    //   725: aload #12
    //   727: ldc_w ''
    //   730: invokevirtual equals : (Ljava/lang/Object;)Z
    //   733: ifne -> 899
    //   736: aload_1
    //   737: astore #9
    //   739: aload_1
    //   740: astore #10
    //   742: aload #6
    //   744: aload #12
    //   746: invokeinterface add : (Ljava/lang/Object;)Z
    //   751: pop
    //   752: aload_1
    //   753: astore #9
    //   755: aload_1
    //   756: astore #10
    //   758: getstatic com/oplus/util/OplusDisplayCompatUtils.DEBUG : Z
    //   761: ifeq -> 899
    //   764: aload_1
    //   765: astore #9
    //   767: aload_1
    //   768: astore #10
    //   770: new java/lang/StringBuilder
    //   773: astore #8
    //   775: aload_1
    //   776: astore #9
    //   778: aload_1
    //   779: astore #10
    //   781: aload #8
    //   783: invokespecial <init> : ()V
    //   786: aload_1
    //   787: astore #9
    //   789: aload_1
    //   790: astore #10
    //   792: aload #8
    //   794: ldc_w 'readConfigFromFileLocked nonImmersive : '
    //   797: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   800: pop
    //   801: aload_1
    //   802: astore #9
    //   804: aload_1
    //   805: astore #10
    //   807: aload #8
    //   809: aload #12
    //   811: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   814: pop
    //   815: aload_1
    //   816: astore #9
    //   818: aload_1
    //   819: astore #10
    //   821: ldc 'OplusDisplayCompatUtils'
    //   823: aload #8
    //   825: invokevirtual toString : ()Ljava/lang/String;
    //   828: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   831: pop
    //   832: goto -> 899
    //   835: aload_1
    //   836: astore #9
    //   838: aload_1
    //   839: astore #10
    //   841: ldc 'size'
    //   843: aload #12
    //   845: invokevirtual equals : (Ljava/lang/Object;)Z
    //   848: ifeq -> 899
    //   851: aload_1
    //   852: astore #9
    //   854: aload_1
    //   855: astore #10
    //   857: aload #11
    //   859: invokeinterface nextText : ()Ljava/lang/String;
    //   864: astore #12
    //   866: aload_1
    //   867: astore #9
    //   869: aload_1
    //   870: astore #10
    //   872: aload #12
    //   874: ldc_w ''
    //   877: invokevirtual equals : (Ljava/lang/Object;)Z
    //   880: ifne -> 899
    //   883: aload_1
    //   884: astore #9
    //   886: aload_1
    //   887: astore #10
    //   889: aload #7
    //   891: aload #12
    //   893: invokeinterface add : (Ljava/lang/Object;)Z
    //   898: pop
    //   899: iload #13
    //   901: iconst_1
    //   902: if_icmpne -> 1359
    //   905: aload_1
    //   906: astore #9
    //   908: aload_1
    //   909: astore #10
    //   911: aload_0
    //   912: getfield mLock : Ljava/lang/Object;
    //   915: astore #12
    //   917: aload_1
    //   918: astore #9
    //   920: aload_1
    //   921: astore #10
    //   923: aload #12
    //   925: monitorenter
    //   926: aload_0
    //   927: getfield mWhiteList : Ljava/util/List;
    //   930: invokeinterface clear : ()V
    //   935: aload_0
    //   936: getfield mWhiteList : Ljava/util/List;
    //   939: aload_2
    //   940: invokeinterface addAll : (Ljava/util/Collection;)Z
    //   945: pop
    //   946: getstatic com/oplus/util/OplusDisplayCompatUtils.sCompatData : Lcom/oplus/util/OplusDisplayCompatData;
    //   949: aload_0
    //   950: getfield mWhiteList : Ljava/util/List;
    //   953: invokevirtual setWhiteList : (Ljava/util/List;)V
    //   956: aload #12
    //   958: monitorexit
    //   959: aload_1
    //   960: astore #9
    //   962: aload_1
    //   963: astore #10
    //   965: aload_0
    //   966: getfield mLock : Ljava/lang/Object;
    //   969: astore #12
    //   971: aload_1
    //   972: astore #9
    //   974: aload_1
    //   975: astore #10
    //   977: aload #12
    //   979: monitorenter
    //   980: aload_0
    //   981: getfield mBlackList : Ljava/util/List;
    //   984: invokeinterface clear : ()V
    //   989: aload_0
    //   990: getfield mBlackList : Ljava/util/List;
    //   993: aload_3
    //   994: invokeinterface addAll : (Ljava/util/Collection;)Z
    //   999: pop
    //   1000: getstatic com/oplus/util/OplusDisplayCompatUtils.sCompatData : Lcom/oplus/util/OplusDisplayCompatData;
    //   1003: aload_0
    //   1004: getfield mBlackList : Ljava/util/List;
    //   1007: invokevirtual setBlackList : (Ljava/util/List;)V
    //   1010: aload #12
    //   1012: monitorexit
    //   1013: aload_1
    //   1014: astore #9
    //   1016: aload_1
    //   1017: astore #10
    //   1019: aload_0
    //   1020: getfield mLock : Ljava/lang/Object;
    //   1023: astore #12
    //   1025: aload_1
    //   1026: astore #9
    //   1028: aload_1
    //   1029: astore #10
    //   1031: aload #12
    //   1033: monitorenter
    //   1034: aload_0
    //   1035: getfield mCompatPackageList : Ljava/util/HashMap;
    //   1038: invokevirtual clear : ()V
    //   1041: aload_0
    //   1042: getfield mCompatPackageList : Ljava/util/HashMap;
    //   1045: aload #4
    //   1047: invokevirtual putAll : (Ljava/util/Map;)V
    //   1050: getstatic com/oplus/util/OplusDisplayCompatUtils.sCompatData : Lcom/oplus/util/OplusDisplayCompatData;
    //   1053: aload_0
    //   1054: getfield mCompatPackageList : Ljava/util/HashMap;
    //   1057: invokevirtual setCompatPackageList : (Ljava/util/HashMap;)V
    //   1060: aload #12
    //   1062: monitorexit
    //   1063: aload_1
    //   1064: astore #9
    //   1066: aload_1
    //   1067: astore #10
    //   1069: aload_0
    //   1070: getfield mLock : Ljava/lang/Object;
    //   1073: astore #12
    //   1075: aload_1
    //   1076: astore #9
    //   1078: aload_1
    //   1079: astore #10
    //   1081: aload #12
    //   1083: monitorenter
    //   1084: aload_0
    //   1085: getfield mRusImmersiveAppsList : Ljava/util/List;
    //   1088: invokeinterface clear : ()V
    //   1093: aload_0
    //   1094: getfield mRusImmersiveAppsList : Ljava/util/List;
    //   1097: aload #5
    //   1099: invokeinterface addAll : (Ljava/util/Collection;)Z
    //   1104: pop
    //   1105: getstatic com/oplus/util/OplusDisplayCompatUtils.sCompatData : Lcom/oplus/util/OplusDisplayCompatData;
    //   1108: aload_0
    //   1109: getfield mRusImmersiveAppsList : Ljava/util/List;
    //   1112: invokevirtual setRusImmersiveList : (Ljava/util/List;)V
    //   1115: aload #12
    //   1117: monitorexit
    //   1118: aload_1
    //   1119: astore #9
    //   1121: aload_1
    //   1122: astore #10
    //   1124: aload_0
    //   1125: getfield mLock : Ljava/lang/Object;
    //   1128: astore #12
    //   1130: aload_1
    //   1131: astore #9
    //   1133: aload_1
    //   1134: astore #10
    //   1136: aload #12
    //   1138: monitorenter
    //   1139: aload_0
    //   1140: getfield mRusNonImmersiveAppsList : Ljava/util/List;
    //   1143: invokeinterface clear : ()V
    //   1148: aload_0
    //   1149: getfield mRusNonImmersiveAppsList : Ljava/util/List;
    //   1152: aload #6
    //   1154: invokeinterface addAll : (Ljava/util/Collection;)Z
    //   1159: pop
    //   1160: getstatic com/oplus/util/OplusDisplayCompatUtils.sCompatData : Lcom/oplus/util/OplusDisplayCompatData;
    //   1163: aload_0
    //   1164: getfield mRusNonImmersiveAppsList : Ljava/util/List;
    //   1167: invokevirtual setRusNonImmersiveList : (Ljava/util/List;)V
    //   1170: aload #12
    //   1172: monitorexit
    //   1173: aload_1
    //   1174: astore #9
    //   1176: aload_1
    //   1177: astore #10
    //   1179: aload_0
    //   1180: getfield mLock : Ljava/lang/Object;
    //   1183: astore #12
    //   1185: aload_1
    //   1186: astore #9
    //   1188: aload_1
    //   1189: astore #10
    //   1191: aload #12
    //   1193: monitorenter
    //   1194: aload_0
    //   1195: getfield mNeedAdjustSizeAppList : Ljava/util/List;
    //   1198: invokeinterface clear : ()V
    //   1203: aload_0
    //   1204: getfield mNeedAdjustSizeAppList : Ljava/util/List;
    //   1207: aload #7
    //   1209: invokeinterface addAll : (Ljava/util/Collection;)Z
    //   1214: pop
    //   1215: getstatic com/oplus/util/OplusDisplayCompatUtils.sCompatData : Lcom/oplus/util/OplusDisplayCompatData;
    //   1218: aload_0
    //   1219: getfield mNeedAdjustSizeAppList : Ljava/util/List;
    //   1222: invokevirtual setNeedAdjustSizeAppList : (Ljava/util/List;)V
    //   1225: aload #12
    //   1227: monitorexit
    //   1228: aload_1
    //   1229: invokevirtual close : ()V
    //   1232: goto -> 1423
    //   1235: astore_1
    //   1236: new java/lang/StringBuilder
    //   1239: dup
    //   1240: invokespecial <init> : ()V
    //   1243: astore #9
    //   1245: aload #9
    //   1247: ldc_w 'Failed to close state FileInputStream '
    //   1250: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1253: pop
    //   1254: aload #9
    //   1256: aload_1
    //   1257: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1260: pop
    //   1261: ldc 'OplusDisplayCompatUtils'
    //   1263: aload #9
    //   1265: invokevirtual toString : ()Ljava/lang/String;
    //   1268: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1271: pop
    //   1272: goto -> 1423
    //   1275: astore #8
    //   1277: aload #12
    //   1279: monitorexit
    //   1280: aload_1
    //   1281: astore #9
    //   1283: aload_1
    //   1284: astore #10
    //   1286: aload #8
    //   1288: athrow
    //   1289: astore #8
    //   1291: aload #12
    //   1293: monitorexit
    //   1294: aload_1
    //   1295: astore #9
    //   1297: aload_1
    //   1298: astore #10
    //   1300: aload #8
    //   1302: athrow
    //   1303: astore #8
    //   1305: aload #12
    //   1307: monitorexit
    //   1308: aload_1
    //   1309: astore #9
    //   1311: aload_1
    //   1312: astore #10
    //   1314: aload #8
    //   1316: athrow
    //   1317: astore #8
    //   1319: aload #12
    //   1321: monitorexit
    //   1322: aload_1
    //   1323: astore #9
    //   1325: aload_1
    //   1326: astore #10
    //   1328: aload #8
    //   1330: athrow
    //   1331: astore #8
    //   1333: aload #12
    //   1335: monitorexit
    //   1336: aload_1
    //   1337: astore #9
    //   1339: aload_1
    //   1340: astore #10
    //   1342: aload #8
    //   1344: athrow
    //   1345: astore #8
    //   1347: aload #12
    //   1349: monitorexit
    //   1350: aload_1
    //   1351: astore #9
    //   1353: aload_1
    //   1354: astore #10
    //   1356: aload #8
    //   1358: athrow
    //   1359: goto -> 127
    //   1362: astore_1
    //   1363: goto -> 1371
    //   1366: astore_1
    //   1367: goto -> 1375
    //   1370: astore_1
    //   1371: goto -> 1424
    //   1374: astore_1
    //   1375: aload #10
    //   1377: astore #9
    //   1379: ldc 'OplusDisplayCompatUtils'
    //   1381: ldc_w 'failed parsing '
    //   1384: aload_1
    //   1385: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   1388: pop
    //   1389: aload #10
    //   1391: astore #9
    //   1393: aload_0
    //   1394: invokespecial loadDefaultDisplayCompatList : ()V
    //   1397: aload #10
    //   1399: ifnull -> 1232
    //   1402: aload #10
    //   1404: invokevirtual close : ()V
    //   1407: goto -> 1232
    //   1410: astore_1
    //   1411: new java/lang/StringBuilder
    //   1414: dup
    //   1415: invokespecial <init> : ()V
    //   1418: astore #9
    //   1420: goto -> 1245
    //   1423: return
    //   1424: aload #9
    //   1426: ifnull -> 1479
    //   1429: aload #9
    //   1431: invokevirtual close : ()V
    //   1434: goto -> 1479
    //   1437: astore #10
    //   1439: new java/lang/StringBuilder
    //   1442: dup
    //   1443: invokespecial <init> : ()V
    //   1446: astore #9
    //   1448: aload #9
    //   1450: ldc_w 'Failed to close state FileInputStream '
    //   1453: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1456: pop
    //   1457: aload #9
    //   1459: aload #10
    //   1461: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1464: pop
    //   1465: ldc 'OplusDisplayCompatUtils'
    //   1467: aload #9
    //   1469: invokevirtual toString : ()Ljava/lang/String;
    //   1472: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1475: pop
    //   1476: goto -> 1479
    //   1479: aload_1
    //   1480: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #480	-> 0
    //   #481	-> 6
    //   #483	-> 15
    //   #484	-> 23
    //   #485	-> 31
    //   #486	-> 40
    //   #487	-> 49
    //   #488	-> 58
    //   #490	-> 67
    //   #492	-> 79
    //   #494	-> 101
    //   #495	-> 112
    //   #497	-> 127
    //   #499	-> 127
    //   #500	-> 142
    //   #501	-> 148
    //   #502	-> 163
    //   #503	-> 179
    //   #504	-> 194
    //   #505	-> 211
    //   #506	-> 232
    //   #507	-> 245
    //   #508	-> 253
    //   #510	-> 273
    //   #511	-> 292
    //   #512	-> 307
    //   #513	-> 324
    //   #515	-> 339
    //   #516	-> 358
    //   #517	-> 373
    //   #518	-> 390
    //   #520	-> 405
    //   #521	-> 424
    //   #522	-> 443
    //   #523	-> 448
    //   #524	-> 467
    //   #525	-> 485
    //   #527	-> 489
    //   #529	-> 505
    //   #530	-> 524
    //   #531	-> 539
    //   #532	-> 556
    //   #533	-> 577
    //   #534	-> 590
    //   #535	-> 598
    //   #537	-> 618
    //   #538	-> 637
    //   #539	-> 652
    //   #540	-> 669
    //   #542	-> 685
    //   #543	-> 704
    //   #544	-> 719
    //   #545	-> 736
    //   #546	-> 752
    //   #547	-> 764
    //   #550	-> 835
    //   #551	-> 851
    //   #552	-> 866
    //   #553	-> 883
    //   #557	-> 899
    //   #559	-> 905
    //   #560	-> 926
    //   #561	-> 935
    //   #562	-> 946
    //   #563	-> 956
    //   #564	-> 959
    //   #565	-> 980
    //   #566	-> 989
    //   #567	-> 1000
    //   #568	-> 1010
    //   #569	-> 1013
    //   #570	-> 1034
    //   #571	-> 1041
    //   #572	-> 1050
    //   #573	-> 1060
    //   #575	-> 1063
    //   #576	-> 1084
    //   #577	-> 1093
    //   #578	-> 1105
    //   #579	-> 1115
    //   #581	-> 1118
    //   #582	-> 1139
    //   #583	-> 1148
    //   #584	-> 1160
    //   #585	-> 1170
    //   #587	-> 1173
    //   #588	-> 1194
    //   #589	-> 1203
    //   #590	-> 1215
    //   #591	-> 1225
    //   #598	-> 1228
    //   #599	-> 1228
    //   #603	-> 1232
    //   #601	-> 1235
    //   #602	-> 1236
    //   #604	-> 1272
    //   #591	-> 1275
    //   #585	-> 1289
    //   #579	-> 1303
    //   #573	-> 1317
    //   #568	-> 1331
    //   #563	-> 1345
    //   #557	-> 1359
    //   #597	-> 1362
    //   #593	-> 1366
    //   #597	-> 1370
    //   #593	-> 1374
    //   #594	-> 1375
    //   #595	-> 1389
    //   #598	-> 1397
    //   #599	-> 1402
    //   #601	-> 1410
    //   #602	-> 1411
    //   #605	-> 1423
    //   #598	-> 1424
    //   #599	-> 1429
    //   #601	-> 1437
    //   #602	-> 1439
    //   #603	-> 1479
    //   #604	-> 1479
    // Exception table:
    //   from	to	target	type
    //   79	84	1374	java/lang/Exception
    //   79	84	1370	finally
    //   92	98	1366	java/lang/Exception
    //   92	98	1362	finally
    //   107	112	1366	java/lang/Exception
    //   107	112	1362	finally
    //   118	127	1366	java/lang/Exception
    //   118	127	1362	finally
    //   133	142	1366	java/lang/Exception
    //   133	142	1362	finally
    //   154	163	1366	java/lang/Exception
    //   154	163	1362	finally
    //   169	179	1366	java/lang/Exception
    //   169	179	1362	finally
    //   185	194	1366	java/lang/Exception
    //   185	194	1362	finally
    //   200	211	1366	java/lang/Exception
    //   200	211	1362	finally
    //   217	223	1366	java/lang/Exception
    //   217	223	1362	finally
    //   229	232	1366	java/lang/Exception
    //   229	232	1362	finally
    //   232	245	259	finally
    //   245	253	259	finally
    //   253	256	259	finally
    //   261	264	259	finally
    //   270	273	1366	java/lang/Exception
    //   270	273	1362	finally
    //   282	292	1366	java/lang/Exception
    //   282	292	1362	finally
    //   298	307	1366	java/lang/Exception
    //   298	307	1362	finally
    //   313	324	1366	java/lang/Exception
    //   313	324	1362	finally
    //   330	339	1366	java/lang/Exception
    //   330	339	1362	finally
    //   348	358	1366	java/lang/Exception
    //   348	358	1362	finally
    //   364	373	1366	java/lang/Exception
    //   364	373	1362	finally
    //   379	390	1366	java/lang/Exception
    //   379	390	1362	finally
    //   396	405	1366	java/lang/Exception
    //   396	405	1362	finally
    //   414	424	1366	java/lang/Exception
    //   414	424	1362	finally
    //   430	443	1366	java/lang/Exception
    //   430	443	1362	finally
    //   454	467	1366	java/lang/Exception
    //   454	467	1362	finally
    //   477	485	1366	java/lang/Exception
    //   477	485	1362	finally
    //   495	505	1366	java/lang/Exception
    //   495	505	1362	finally
    //   514	524	1366	java/lang/Exception
    //   514	524	1362	finally
    //   530	539	1366	java/lang/Exception
    //   530	539	1362	finally
    //   545	556	1366	java/lang/Exception
    //   545	556	1362	finally
    //   562	568	1366	java/lang/Exception
    //   562	568	1362	finally
    //   574	577	1366	java/lang/Exception
    //   574	577	1362	finally
    //   577	590	604	finally
    //   590	598	604	finally
    //   598	601	604	finally
    //   606	609	604	finally
    //   615	618	1366	java/lang/Exception
    //   615	618	1362	finally
    //   627	637	1366	java/lang/Exception
    //   627	637	1362	finally
    //   643	652	1366	java/lang/Exception
    //   643	652	1362	finally
    //   658	669	1366	java/lang/Exception
    //   658	669	1362	finally
    //   675	685	1366	java/lang/Exception
    //   675	685	1362	finally
    //   694	704	1366	java/lang/Exception
    //   694	704	1362	finally
    //   710	719	1366	java/lang/Exception
    //   710	719	1362	finally
    //   725	736	1366	java/lang/Exception
    //   725	736	1362	finally
    //   742	752	1366	java/lang/Exception
    //   742	752	1362	finally
    //   758	764	1366	java/lang/Exception
    //   758	764	1362	finally
    //   770	775	1366	java/lang/Exception
    //   770	775	1362	finally
    //   781	786	1366	java/lang/Exception
    //   781	786	1362	finally
    //   792	801	1366	java/lang/Exception
    //   792	801	1362	finally
    //   807	815	1366	java/lang/Exception
    //   807	815	1362	finally
    //   821	832	1366	java/lang/Exception
    //   821	832	1362	finally
    //   841	851	1366	java/lang/Exception
    //   841	851	1362	finally
    //   857	866	1366	java/lang/Exception
    //   857	866	1362	finally
    //   872	883	1366	java/lang/Exception
    //   872	883	1362	finally
    //   889	899	1366	java/lang/Exception
    //   889	899	1362	finally
    //   911	917	1366	java/lang/Exception
    //   911	917	1362	finally
    //   923	926	1366	java/lang/Exception
    //   923	926	1362	finally
    //   926	935	1345	finally
    //   935	946	1345	finally
    //   946	956	1345	finally
    //   956	959	1345	finally
    //   965	971	1366	java/lang/Exception
    //   965	971	1362	finally
    //   977	980	1366	java/lang/Exception
    //   977	980	1362	finally
    //   980	989	1331	finally
    //   989	1000	1331	finally
    //   1000	1010	1331	finally
    //   1010	1013	1331	finally
    //   1019	1025	1366	java/lang/Exception
    //   1019	1025	1362	finally
    //   1031	1034	1366	java/lang/Exception
    //   1031	1034	1362	finally
    //   1034	1041	1317	finally
    //   1041	1050	1317	finally
    //   1050	1060	1317	finally
    //   1060	1063	1317	finally
    //   1069	1075	1366	java/lang/Exception
    //   1069	1075	1362	finally
    //   1081	1084	1366	java/lang/Exception
    //   1081	1084	1362	finally
    //   1084	1093	1303	finally
    //   1093	1105	1303	finally
    //   1105	1115	1303	finally
    //   1115	1118	1303	finally
    //   1124	1130	1366	java/lang/Exception
    //   1124	1130	1362	finally
    //   1136	1139	1366	java/lang/Exception
    //   1136	1139	1362	finally
    //   1139	1148	1289	finally
    //   1148	1160	1289	finally
    //   1160	1170	1289	finally
    //   1170	1173	1289	finally
    //   1179	1185	1366	java/lang/Exception
    //   1179	1185	1362	finally
    //   1191	1194	1366	java/lang/Exception
    //   1191	1194	1362	finally
    //   1194	1203	1275	finally
    //   1203	1215	1275	finally
    //   1215	1225	1275	finally
    //   1225	1228	1275	finally
    //   1228	1232	1235	java/io/IOException
    //   1277	1280	1275	finally
    //   1286	1289	1366	java/lang/Exception
    //   1286	1289	1362	finally
    //   1291	1294	1289	finally
    //   1300	1303	1366	java/lang/Exception
    //   1300	1303	1362	finally
    //   1305	1308	1303	finally
    //   1314	1317	1366	java/lang/Exception
    //   1314	1317	1362	finally
    //   1319	1322	1317	finally
    //   1328	1331	1366	java/lang/Exception
    //   1328	1331	1362	finally
    //   1333	1336	1331	finally
    //   1342	1345	1366	java/lang/Exception
    //   1342	1345	1362	finally
    //   1347	1350	1345	finally
    //   1356	1359	1366	java/lang/Exception
    //   1356	1359	1362	finally
    //   1379	1389	1362	finally
    //   1393	1397	1362	finally
    //   1402	1407	1410	java/io/IOException
    //   1429	1434	1437	java/io/IOException
  }
  
  private void registerPackageMonitor() {
    Context context = this.mContext;
    if (context == null)
      return; 
    this.mMyPackageMonitor.register(context, null, UserHandle.ALL, true);
  }
  
  private String getThis() {
    return toString();
  }
  
  private String getVersionNameFromCompatPkg(String paramString) {
    paramString = this.mCompatPackageList.get(paramString);
    return paramString;
  }
  
  public boolean isOnlyDisplayCompatEnabled() {
    return this.mEnableDisplayCompat;
  }
  
  public boolean hasHeteromorphismFeature() {
    boolean bool = false;
    synchronized (this.mLock) {
      if (this.mHasHeteromorphismFeature)
        bool = true; 
      return bool;
    } 
  }
  
  public boolean getImmersiveDefault() {
    synchronized (this.mLock) {
      return this.mImmersiveDefault;
    } 
  }
  
  public boolean inWhitePkgList(String paramString) {
    boolean bool = false;
    synchronized (this.mLock) {
      if (this.mWhiteList.contains(paramString))
        bool = true; 
      return bool;
    } 
  }
  
  public boolean inBlackPkgList(String paramString) {
    boolean bool = false;
    synchronized (this.mLock) {
      if (this.mBlackList.contains(paramString))
        bool = true; 
      return bool;
    } 
  }
  
  public boolean inRusImmersivePkgList(String paramString) {
    boolean bool = false;
    synchronized (this.mLock) {
      if (this.mRusImmersiveAppsList.contains(paramString))
        bool = true; 
      return bool;
    } 
  }
  
  public boolean inRusNonImmersivePkgList(String paramString) {
    if (this.mDisplayCutoutType != 1) {
      if (DEBUG)
        Log.d("OplusDisplayCompatUtils", "because this is not a left cutout, the nonimmersive list is not working"); 
      return false;
    } 
    boolean bool = false;
    synchronized (this.mLock) {
      if (this.mRusNonImmersiveAppsList.contains(paramString)) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("inRusNonImmersivePkgList: ");
        stringBuilder.append(paramString);
        Log.d("OplusDisplayCompatUtils", stringBuilder.toString());
        bool = true;
      } 
      return bool;
    } 
  }
  
  public boolean inLocalCompatPkgList(String paramString) {
    boolean bool = false;
    synchronized (this.mLock) {
      if (this.mLocalCompatAppsList.contains(paramString))
        bool = true; 
      return bool;
    } 
  }
  
  public boolean inLocalFullScreenPkgList(String paramString) {
    boolean bool = false;
    synchronized (this.mLock) {
      if (this.mLocalFullScreenAppsList.contains(paramString))
        bool = true; 
      return bool;
    } 
  }
  
  public boolean inLocalNonImmersivePkgList(String paramString) {
    return this.mLocalDefaultModeList.contains(paramString);
  }
  
  public boolean inLocalImmersivePkgList(String paramString) {
    return this.mLocalShowModeList.contains(paramString);
  }
  
  public boolean inInstalledCompatPkgList(String paramString) {
    boolean bool = false;
    synchronized (this.mLock) {
      if (this.mInstalledCompatList.contains(paramString))
        bool = true; 
      return bool;
    } 
  }
  
  public boolean inInstalledThirdPartyAppList(String paramString) {
    return this.mInstalledThirdPartyAppList.contains(paramString);
  }
  
  public boolean inInstalledImeList(String paramString) {
    return this.mInstalledImeList.contains(paramString);
  }
  
  public boolean inAlreadyShowDialogList(String paramString) {
    boolean bool = false;
    synchronized (this.mLock) {
      if (this.mAlreadyShowDialogAppsList.contains(paramString))
        bool = true; 
      return bool;
    } 
  }
  
  public boolean inNeedAdujstSizeList(String paramString) {
    boolean bool = false;
    synchronized (this.mLock) {
      if (this.mNeedAdjustSizeAppList.contains(paramString))
        bool = true; 
      return bool;
    } 
  }
  
  public boolean inCompatPkgList(String paramString) {
    boolean bool = false;
    synchronized (this.mLock) {
      if (this.mCompatPackageList.containsKey(paramString))
        bool = true; 
      return bool;
    } 
  }
  
  public boolean needCompatPkgByVersionName(String paramString) {
    boolean bool = false;
    String str = getVersionNameFromCompatPkg(paramString);
    if (str != null) {
      if ("empty".equals(str)) {
        bool = true;
      } else {
        paramString = getVersionNameFromInstalledPkg(paramString);
        try {
          int i = compareVersion(paramString, str);
          if (i < 0) {
            bool = true;
          } else {
            bool = false;
          } 
        } catch (Exception exception) {}
      } 
    } else {
      bool = true;
    } 
    return bool;
  }
  
  public boolean shouldCompatAdjustForPkg(String paramString) {
    boolean bool1 = false;
    boolean bool2 = bool1;
    if (isOnlyDisplayCompatEnabled())
      if (inInstalledImeList(paramString)) {
        bool2 = false;
      } else if (inBlackPkgList(paramString)) {
        bool2 = false;
      } else if (inWhitePkgList(paramString)) {
        bool2 = true;
      } else if (inLocalFullScreenPkgList(paramString)) {
        bool2 = false;
      } else {
        bool2 = bool1;
        if (inInstalledCompatPkgList(paramString))
          bool2 = true; 
      }  
    return bool2;
  }
  
  public boolean neverLayoutInDisplayCutout(String paramString) {
    if (inRusNonImmersivePkgList(paramString) && !inLocalImmersivePkgList(paramString))
      return true; 
    return false;
  }
  
  public boolean shouldNonImmersiveAdjustForPkg(String paramString) {
    boolean bool;
    if (getImmersiveDefault()) {
      bool = false;
    } else {
      bool = inInstalledThirdPartyAppList(paramString);
    } 
    if (inInstalledImeList(paramString))
      bool = true; 
    if (inLocalImmersivePkgList(paramString)) {
      bool = false;
    } else if (inLocalNonImmersivePkgList(paramString)) {
      bool = true;
    } else if (inRusImmersivePkgList(paramString)) {
      bool = false;
    } else if (inRusNonImmersivePkgList(paramString)) {
      bool = true;
    } else if (shouldCompatAdjustForPkg(paramString)) {
      bool = true;
    } 
    return bool;
  }
  
  public int getAppCutoutMode(String paramString) {
    byte b = 1;
    if (inInstalledThirdPartyAppList(paramString) || inInstalledImeList(paramString))
      b = 0; 
    if (this.mLocalDefaultModeList.contains(paramString)) {
      b = 0;
    } else if (this.mLocalShowModeList.contains(paramString)) {
      b = 1;
    } else if (this.mLocalHideModeList.contains(paramString)) {
      b = 2;
    } else if (inRusImmersivePkgList(paramString)) {
      b = 1;
    } else if (inRusNonImmersivePkgList(paramString)) {
      b = 2;
    } 
    return b;
  }
  
  public float getMaxAspectRatio(ActivityInfo paramActivityInfo) {
    float f = paramActivityInfo.maxAspectRatio;
    if (!paramActivityInfo.packageName.startsWith("android.server.cts")) {
      String str = paramActivityInfo.packageName;
      if (!str.startsWith("android.server.wm")) {
        float f1;
        boolean bool = shouldCompatAdjustForPkg(paramActivityInfo.packageName);
        if (bool) {
          f1 = 1.7778F;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(paramActivityInfo.packageName);
          stringBuilder.append(", maxAspectRatio: ");
          stringBuilder.append(paramActivityInfo.maxAspectRatio);
          stringBuilder.append(" >>> ");
          stringBuilder.append(1.7778F);
          Slog.d("OplusDisplayCompatUtils", stringBuilder.toString());
        } else {
          f1 = f;
          if (f != 0.0F) {
            f1 = 0.0F;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(paramActivityInfo.packageName);
            stringBuilder.append(", maxAspectRatio: ");
            stringBuilder.append(paramActivityInfo.maxAspectRatio);
            stringBuilder.append(" >>> ");
            stringBuilder.append(0.0F);
            Slog.d("OplusDisplayCompatUtils", stringBuilder.toString());
          } 
        } 
        return f1;
      } 
    } 
    return f;
  }
  
  public boolean shouldHideFullscreenButtonForPkg(String paramString) {
    boolean bool = false;
    if (inWhitePkgList(paramString))
      bool = true; 
    return bool;
  }
  
  public boolean shouldShowFullscreenDialogForPkg(String paramString) {
    boolean bool = true;
    if (inAlreadyShowDialogList(paramString))
      bool = false; 
    return bool;
  }
  
  public boolean shouldAdjustRealSizeForPkg(String paramString) {
    boolean bool = false;
    if (inNeedAdujstSizeList(paramString))
      bool = true; 
    return bool;
  }
  
  public void updateLocalAppsListForPkg(String paramString) {
    if (this.mContext == null)
      return; 
    synchronized (this.mLock) {
      if (this.mLocalCompatAppsList.contains(paramString)) {
        this.mLocalCompatAppsList.remove(paramString);
        StringBuilder stringBuilder = new StringBuilder();
        this();
        for (String str : this.mLocalCompatAppsList) {
          stringBuilder.append(str);
          stringBuilder.append(",");
        } 
        Settings.Global.putString(this.mContext.getContentResolver(), "key_display_compat_local_apps_v1", stringBuilder.toString());
      } 
      synchronized (this.mLock) {
        if (!this.mLocalFullScreenAppsList.contains(paramString)) {
          this.mLocalFullScreenAppsList.add(paramString);
          StringBuilder stringBuilder = new StringBuilder();
          this();
          for (String str : this.mLocalFullScreenAppsList) {
            stringBuilder.append(str);
            stringBuilder.append(",");
          } 
          Settings.Global.putString(this.mContext.getContentResolver(), "key_display_fullscreen_local_apps_v1", stringBuilder.toString());
        } 
        return;
      } 
    } 
  }
  
  public void updateLocalImmersiveListForPkg(String paramString) {}
  
  public void updateLocalShowDialogListForPkg(String paramString) {
    if (this.mContext == null)
      return; 
    synchronized (this.mLock) {
      if (!this.mAlreadyShowDialogAppsList.contains(paramString)) {
        this.mAlreadyShowDialogAppsList.add(paramString);
        StringBuilder stringBuilder = new StringBuilder();
        this();
        for (String str : this.mAlreadyShowDialogAppsList) {
          stringBuilder.append(str);
          stringBuilder.append(",");
        } 
        Settings.Global.putString(this.mContext.getContentResolver(), "key_display_show_dialog_local_apps", stringBuilder.toString());
      } 
      return;
    } 
  }
  
  public void removeLocalShowDialogListForPkg(String paramString) {
    if (this.mContext == null)
      return; 
    synchronized (this.mLock) {
      if (this.mAlreadyShowDialogAppsList.contains(paramString)) {
        this.mAlreadyShowDialogAppsList.remove(paramString);
        StringBuilder stringBuilder = new StringBuilder();
        this();
        for (String str : this.mAlreadyShowDialogAppsList) {
          stringBuilder.append(str);
          stringBuilder.append(",");
        } 
        Settings.Global.putString(this.mContext.getContentResolver(), "key_display_show_dialog_local_apps", stringBuilder.toString());
      } 
      return;
    } 
  }
  
  private void loadDefaultDisplayCompatList() {
    synchronized (this.mLock) {
      synchronized (this.mLock) {
        if (this.mBlackList != null) {
          this.mBlackList.clear();
          this.mBlackList.add("com.justsy.launcher");
          this.mBlackList.add("com.justsy.portal");
          this.mBlackList.add("com.justsy.mdm");
          this.mBlackList.add("com.ctsi.emm");
          if (sCompatData != null)
            sCompatData.setBlackList(this.mBlackList); 
        } 
        synchronized (this.mLock) {
          if (this.mRusNonImmersiveAppsList != null) {
            this.mRusNonImmersiveAppsList.clear();
            this.mRusNonImmersiveAppsList.add("com.walkgame.ismarttv");
            this.mRusNonImmersiveAppsList.add("net.fetnet.fetvod");
            this.mRusNonImmersiveAppsList.add("com.justsy.launcher");
            this.mRusNonImmersiveAppsList.add("com.justsy.portal");
            this.mRusNonImmersiveAppsList.add("com.justsy.mdm");
            if (sCompatData != null)
              sCompatData.setRusNonImmersiveList(this.mRusNonImmersiveAppsList); 
          } 
          return;
        } 
      } 
    } 
  }
  
  private int compareVersion(String paramString1, String paramString2) {
    int k, m, n;
    boolean bool = paramString1.equals(paramString2);
    boolean bool1 = false;
    if (bool)
      return 0; 
    int i = 0;
    int j = 0;
    while (true) {
      String str;
      k = paramString1.indexOf('.', i);
      m = paramString2.indexOf('.', j);
      if (k < 0) {
        str = paramString1.substring(i);
      } else {
        str = paramString1.substring(i, k);
      } 
      i = Integer.parseInt(str);
      if (m < 0) {
        str = paramString2.substring(j);
      } else {
        str = paramString2.substring(j, m);
      } 
      j = Integer.parseInt(str);
      n = Integer.valueOf(i).compareTo(Integer.valueOf(j));
      k++;
      m++;
      if (n == 0 && k > 0) {
        i = k;
        j = m;
        if (m <= 0)
          break; 
        continue;
      } 
      break;
    } 
    if (n == 0) {
      if (k > m)
        return containsNonZeroValue(paramString1, k); 
      if (k < m) {
        j = bool1;
        if (containsNonZeroValue(paramString2, m))
          j = -1; 
        return j;
      } 
    } 
    return n;
  }
  
  private boolean containsNonZeroValue(String paramString, int paramInt) {
    for (; paramInt < paramString.length(); paramInt++) {
      char c = paramString.charAt(paramInt);
      if (c != '0' && c != '.')
        return true; 
    } 
    return false;
  }
  
  private String getVersionNameFromInstalledPkg(String paramString) {
    String str1 = String.valueOf(2147483647);
    PackageManager packageManager = this.mPackageManager;
    String str2 = str1;
    if (packageManager != null)
      try {
        str2 = (packageManager.getPackageInfo(paramString, 0)).versionName;
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
        str2 = str1;
      }  
    return str2;
  }
  
  private boolean supportFullScreen(String paramString) {
    return supportFullScreen(paramString, null);
  }
  
  private boolean supportFullScreen(String paramString, PackageInfo paramPackageInfo) {
    // Byte code:
    //   0: fconst_2
    //   1: fstore_3
    //   2: iconst_0
    //   3: istore #4
    //   5: iconst_0
    //   6: istore #5
    //   8: aload_0
    //   9: getfield mPackageManager : Landroid/content/pm/PackageManager;
    //   12: astore #6
    //   14: iconst_1
    //   15: istore #7
    //   17: fload_3
    //   18: fstore #8
    //   20: iload #4
    //   22: istore #9
    //   24: aload #6
    //   26: ifnull -> 349
    //   29: fload_3
    //   30: fstore #10
    //   32: iload #5
    //   34: istore #9
    //   36: fload_3
    //   37: fstore #8
    //   39: aload_1
    //   40: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   43: ifne -> 337
    //   46: fload_3
    //   47: fstore #10
    //   49: iload #5
    //   51: istore #9
    //   53: fload_3
    //   54: fstore #8
    //   56: aload_0
    //   57: aload_1
    //   58: invokevirtual inInstalledImeList : (Ljava/lang/String;)Z
    //   61: ifne -> 337
    //   64: fload_3
    //   65: fstore #8
    //   67: getstatic com/oplus/util/OplusDisplayCompatUtils.sIncludeImmersiveList : Ljava/util/List;
    //   70: astore #6
    //   72: fload_3
    //   73: fstore #8
    //   75: aload #6
    //   77: aload_1
    //   78: invokeinterface contains : (Ljava/lang/Object;)Z
    //   83: ifne -> 215
    //   86: fload_3
    //   87: fstore #10
    //   89: iload #5
    //   91: istore #9
    //   93: fload_3
    //   94: fstore #8
    //   96: aload_1
    //   97: ldc_w 'com.oppo'
    //   100: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   103: ifne -> 337
    //   106: fload_3
    //   107: fstore #10
    //   109: iload #5
    //   111: istore #9
    //   113: fload_3
    //   114: fstore #8
    //   116: aload_1
    //   117: ldc_w 'com.coloros'
    //   120: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   123: ifne -> 337
    //   126: fload_3
    //   127: fstore #10
    //   129: iload #5
    //   131: istore #9
    //   133: fload_3
    //   134: fstore #8
    //   136: aload_1
    //   137: ldc_w 'com.nearme'
    //   140: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   143: ifne -> 337
    //   146: fload_3
    //   147: fstore #10
    //   149: iload #5
    //   151: istore #9
    //   153: fload_3
    //   154: fstore #8
    //   156: aload_1
    //   157: ldc_w 'com.heytap'
    //   160: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   163: ifne -> 337
    //   166: fload_3
    //   167: fstore #10
    //   169: iload #5
    //   171: istore #9
    //   173: fload_3
    //   174: fstore #8
    //   176: aload_1
    //   177: ldc_w 'com.cootek.smartinputv5.language'
    //   180: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   183: ifne -> 337
    //   186: fload_3
    //   187: fstore #8
    //   189: getstatic com/oplus/util/OplusDisplayCompatUtils.sExcludeImmersivedList : Ljava/util/List;
    //   192: astore #6
    //   194: fload_3
    //   195: fstore #10
    //   197: iload #5
    //   199: istore #9
    //   201: fload_3
    //   202: fstore #8
    //   204: aload #6
    //   206: aload_1
    //   207: invokeinterface contains : (Ljava/lang/Object;)Z
    //   212: ifne -> 337
    //   215: aload_2
    //   216: astore #6
    //   218: aload_2
    //   219: ifnonnull -> 241
    //   222: fload_3
    //   223: fstore #8
    //   225: aload_0
    //   226: getfield mPackageManager : Landroid/content/pm/PackageManager;
    //   229: aload_1
    //   230: sipush #8192
    //   233: invokestatic getCurrentUser : ()I
    //   236: invokevirtual getPackageInfoAsUser : (Ljava/lang/String;II)Landroid/content/pm/PackageInfo;
    //   239: astore #6
    //   241: fload_3
    //   242: fstore #10
    //   244: iload #5
    //   246: istore #9
    //   248: aload #6
    //   250: ifnull -> 337
    //   253: fload_3
    //   254: fstore #10
    //   256: iload #5
    //   258: istore #9
    //   260: fload_3
    //   261: fstore #8
    //   263: aload #6
    //   265: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   268: getfield flags : I
    //   271: iconst_1
    //   272: iand
    //   273: ifne -> 337
    //   276: fload_3
    //   277: fstore #8
    //   279: aload #6
    //   281: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   284: getfield maxAspectRatio : F
    //   287: fstore #10
    //   289: fload #10
    //   291: fstore_3
    //   292: fload #10
    //   294: fstore #8
    //   296: aload #6
    //   298: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   301: getfield targetSdkVersion : I
    //   304: bipush #26
    //   306: if_icmplt -> 321
    //   309: fload #10
    //   311: fstore_3
    //   312: fload #10
    //   314: fconst_0
    //   315: fcmpg
    //   316: ifgt -> 321
    //   319: fconst_2
    //   320: fstore_3
    //   321: fload_3
    //   322: fstore #8
    //   324: aload #6
    //   326: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   329: getfield privateFlags : I
    //   332: istore #9
    //   334: fload_3
    //   335: fstore #10
    //   337: fload #10
    //   339: fstore #8
    //   341: goto -> 349
    //   344: astore_1
    //   345: iload #4
    //   347: istore #9
    //   349: iload #7
    //   351: istore #11
    //   353: fload #8
    //   355: fconst_2
    //   356: fcmpl
    //   357: ifge -> 392
    //   360: iload #7
    //   362: istore #11
    //   364: iload #9
    //   366: sipush #1024
    //   369: iand
    //   370: ifne -> 392
    //   373: iload #9
    //   375: sipush #4096
    //   378: iand
    //   379: ifeq -> 389
    //   382: iload #7
    //   384: istore #11
    //   386: goto -> 392
    //   389: iconst_0
    //   390: istore #11
    //   392: iload #11
    //   394: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1284	-> 0
    //   #1285	-> 0
    //   #1286	-> 2
    //   #1288	-> 8
    //   #1290	-> 29
    //   #1291	-> 46
    //   #1292	-> 72
    //   #1293	-> 86
    //   #1294	-> 106
    //   #1295	-> 126
    //   #1296	-> 146
    //   #1297	-> 166
    //   #1298	-> 194
    //   #1300	-> 215
    //   #1301	-> 222
    //   #1303	-> 241
    //   #1305	-> 276
    //   #1306	-> 289
    //   #1308	-> 319
    //   #1310	-> 321
    //   #1316	-> 337
    //   #1314	-> 344
    //   #1319	-> 349
    //   #1322	-> 392
    // Exception table:
    //   from	to	target	type
    //   39	46	344	android/content/pm/PackageManager$NameNotFoundException
    //   56	64	344	android/content/pm/PackageManager$NameNotFoundException
    //   67	72	344	android/content/pm/PackageManager$NameNotFoundException
    //   75	86	344	android/content/pm/PackageManager$NameNotFoundException
    //   96	106	344	android/content/pm/PackageManager$NameNotFoundException
    //   116	126	344	android/content/pm/PackageManager$NameNotFoundException
    //   136	146	344	android/content/pm/PackageManager$NameNotFoundException
    //   156	166	344	android/content/pm/PackageManager$NameNotFoundException
    //   176	186	344	android/content/pm/PackageManager$NameNotFoundException
    //   189	194	344	android/content/pm/PackageManager$NameNotFoundException
    //   204	215	344	android/content/pm/PackageManager$NameNotFoundException
    //   225	241	344	android/content/pm/PackageManager$NameNotFoundException
    //   263	276	344	android/content/pm/PackageManager$NameNotFoundException
    //   279	289	344	android/content/pm/PackageManager$NameNotFoundException
    //   296	309	344	android/content/pm/PackageManager$NameNotFoundException
    //   324	334	344	android/content/pm/PackageManager$NameNotFoundException
  }
  
  private boolean isInstalledThirdPartyApp(String paramString) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_2
    //   2: iconst_0
    //   3: istore_3
    //   4: iload_2
    //   5: istore #4
    //   7: aload_0
    //   8: getfield mPackageManager : Landroid/content/pm/PackageManager;
    //   11: ifnull -> 190
    //   14: iload_3
    //   15: istore #4
    //   17: aload_1
    //   18: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   21: ifne -> 183
    //   24: getstatic com/oplus/util/OplusDisplayCompatUtils.sIncludeImmersiveList : Ljava/util/List;
    //   27: astore #5
    //   29: aload #5
    //   31: aload_1
    //   32: invokeinterface contains : (Ljava/lang/Object;)Z
    //   37: ifne -> 124
    //   40: iload_3
    //   41: istore #4
    //   43: aload_1
    //   44: ldc_w 'com.oppo'
    //   47: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   50: ifne -> 183
    //   53: iload_3
    //   54: istore #4
    //   56: aload_1
    //   57: ldc_w 'com.coloros'
    //   60: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   63: ifne -> 183
    //   66: iload_3
    //   67: istore #4
    //   69: aload_1
    //   70: ldc_w 'com.nearme'
    //   73: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   76: ifne -> 183
    //   79: iload_3
    //   80: istore #4
    //   82: aload_1
    //   83: ldc_w 'com.cootek.smartinputv5.language'
    //   86: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   89: ifne -> 183
    //   92: iload_3
    //   93: istore #4
    //   95: aload_1
    //   96: ldc_w 'com.heytap'
    //   99: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   102: ifne -> 183
    //   105: getstatic com/oplus/util/OplusDisplayCompatUtils.sExcludeImmersivedList : Ljava/util/List;
    //   108: astore #5
    //   110: iload_3
    //   111: istore #4
    //   113: aload #5
    //   115: aload_1
    //   116: invokeinterface contains : (Ljava/lang/Object;)Z
    //   121: ifne -> 183
    //   124: aload_0
    //   125: getfield mPackageManager : Landroid/content/pm/PackageManager;
    //   128: aload_1
    //   129: sipush #8192
    //   132: invokestatic getCurrentUser : ()I
    //   135: invokevirtual getPackageInfoAsUser : (Ljava/lang/String;II)Landroid/content/pm/PackageInfo;
    //   138: astore #5
    //   140: iload_3
    //   141: istore #4
    //   143: aload #5
    //   145: ifnull -> 183
    //   148: getstatic com/oplus/util/OplusDisplayCompatUtils.sIncludeImmersiveList : Ljava/util/List;
    //   151: aload_1
    //   152: invokeinterface contains : (Ljava/lang/Object;)Z
    //   157: ifne -> 180
    //   160: aload #5
    //   162: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   165: getfield flags : I
    //   168: istore #6
    //   170: iload_3
    //   171: istore #4
    //   173: iload #6
    //   175: iconst_1
    //   176: iand
    //   177: ifne -> 183
    //   180: iconst_1
    //   181: istore #4
    //   183: goto -> 190
    //   186: astore_1
    //   187: iload_2
    //   188: istore #4
    //   190: iload #4
    //   192: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1326	-> 0
    //   #1328	-> 4
    //   #1330	-> 14
    //   #1331	-> 29
    //   #1332	-> 40
    //   #1333	-> 53
    //   #1334	-> 66
    //   #1335	-> 79
    //   #1336	-> 92
    //   #1337	-> 110
    //   #1340	-> 124
    //   #1341	-> 140
    //   #1344	-> 180
    //   #1349	-> 183
    //   #1347	-> 186
    //   #1352	-> 190
    // Exception table:
    //   from	to	target	type
    //   17	29	186	android/content/pm/PackageManager$NameNotFoundException
    //   29	40	186	android/content/pm/PackageManager$NameNotFoundException
    //   43	53	186	android/content/pm/PackageManager$NameNotFoundException
    //   56	66	186	android/content/pm/PackageManager$NameNotFoundException
    //   69	79	186	android/content/pm/PackageManager$NameNotFoundException
    //   82	92	186	android/content/pm/PackageManager$NameNotFoundException
    //   95	110	186	android/content/pm/PackageManager$NameNotFoundException
    //   113	124	186	android/content/pm/PackageManager$NameNotFoundException
    //   124	140	186	android/content/pm/PackageManager$NameNotFoundException
    //   148	170	186	android/content/pm/PackageManager$NameNotFoundException
  }
  
  class FileObserverPolicy extends FileObserver {
    private String mFocusPath;
    
    final OplusDisplayCompatUtils this$0;
    
    public FileObserverPolicy(String param1String) {
      super(param1String, 8);
      this.mFocusPath = param1String;
    }
    
    public void onEvent(int param1Int, String param1String) {
      if (param1Int == 8 && 
        this.mFocusPath.equals("/data/oppo/coloros/displaycompat/sys_display_compat_config.xml")) {
        Slog.i("OplusDisplayCompatUtils", "FileObserver: onEvent");
        OplusDisplayCompatUtils.this.readDisplayCompatConfig();
      } 
    }
  }
  
  private void loadLocalCompatAppList() {
    Context context = this.mContext;
    if (context == null)
      return; 
    null = Settings.Global.getString(context.getContentResolver(), "key_display_compat_local_apps_v1");
    synchronized (this.mLock) {
      this.mLocalCompatAppsList.clear();
      if (!TextUtils.isEmpty(null)) {
        ArrayList<String> arrayList = new ArrayList();
        this(Arrays.asList((E[])null.split(",")));
        this.mLocalCompatAppsList = arrayList;
      } 
      sCompatData.setLocalCompatList(this.mLocalCompatAppsList);
      return;
    } 
  }
  
  private void loadLocalFullScreenAppList() {
    Context context = this.mContext;
    if (context == null)
      return; 
    null = Settings.Global.getString(context.getContentResolver(), "key_display_fullscreen_local_apps_v1");
    synchronized (this.mLock) {
      this.mLocalFullScreenAppsList.clear();
      if (!TextUtils.isEmpty(null)) {
        ArrayList<String> arrayList = new ArrayList();
        this(Arrays.asList((E[])null.split(",")));
        this.mLocalFullScreenAppsList = arrayList;
      } 
      sCompatData.setLocalFullScreenList(this.mLocalFullScreenAppsList);
      return;
    } 
  }
  
  private void refreshLocalDefaultModeList() {
    Context context = this.mContext;
    if (context == null)
      return; 
    null = Settings.Global.getString(context.getContentResolver(), "key_display_nonimmersive_local_apps");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("refreshLocalDefaultModeList:");
    stringBuilder.append(null);
    Log.d("OplusDisplayCompatUtils", stringBuilder.toString());
    synchronized (this.mLock) {
      this.mLocalDefaultModeList.clear();
      if (!TextUtils.isEmpty(null)) {
        ArrayList<String> arrayList = new ArrayList();
        this(Arrays.asList((E[])null.split(",")));
        this.mLocalDefaultModeList = arrayList;
      } 
      sCompatData.setLocalCutoutDefaultList(this.mLocalDefaultModeList);
      return;
    } 
  }
  
  private void refreshLocalShowModeList() {
    Context context = this.mContext;
    if (context == null)
      return; 
    null = Settings.Global.getString(context.getContentResolver(), "key_display_immersive_local_apps");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("refreshLocalShowModeList:");
    stringBuilder.append(null);
    Log.d("OplusDisplayCompatUtils", stringBuilder.toString());
    synchronized (this.mLock) {
      this.mLocalShowModeList.clear();
      if (!TextUtils.isEmpty(null)) {
        ArrayList<String> arrayList = new ArrayList();
        this(Arrays.asList((E[])null.split(",")));
        this.mLocalShowModeList = arrayList;
      } 
      sCompatData.setLocalCutoutShowList(this.mLocalShowModeList);
      return;
    } 
  }
  
  private void refreshLocalHideModeList() {
    Context context = this.mContext;
    if (context == null)
      return; 
    null = Settings.Global.getString(context.getContentResolver(), "cutout_hide_app_list");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("refreshLocalHideModeList:");
    stringBuilder.append(null);
    Log.d("OplusDisplayCompatUtils", stringBuilder.toString());
    synchronized (this.mLock) {
      this.mLocalHideModeList.clear();
      if (!TextUtils.isEmpty(null)) {
        ArrayList<String> arrayList = new ArrayList();
        this(Arrays.asList((E[])null.split(",")));
        this.mLocalHideModeList = arrayList;
      } 
      sCompatData.setLocalCutoutHideList(this.mLocalHideModeList);
      return;
    } 
  }
  
  private void loadLocalShowDialogAppList() {
    Context context = this.mContext;
    if (context == null)
      return; 
    String str = Settings.Global.getString(context.getContentResolver(), "key_display_show_dialog_local_apps");
    if (str != null)
      synchronized (this.mLock) {
        this.mAlreadyShowDialogAppsList.clear();
        ArrayList<String> arrayList = new ArrayList();
        this(Arrays.asList((E[])str.split(",")));
        this.mAlreadyShowDialogAppsList = arrayList;
        sCompatData.setShowDialogAppList(arrayList);
      }  
  }
  
  private void loadInstalledImeAppList() {
    if (this.mContext == null)
      return; 
    null = new ArrayList();
    try {
      PackageManager packageManager = this.mContext.getPackageManager();
      Intent intent = new Intent();
      this("android.view.InputMethod");
      int i = ActivityManager.getCurrentUser();
      List<ResolveInfo> list = packageManager.queryIntentServicesAsUser(intent, 131200, i);
      if (list != null) {
        int j = list.size();
        for (i = 0; i < j; i++) {
          ResolveInfo resolveInfo = list.get(i);
          if (resolveInfo != null) {
            String str = resolveInfo.serviceInfo.packageName;
            null.add(str);
          } 
        } 
      } 
    } catch (Exception exception) {}
    synchronized (this.mLock) {
      this.mInstalledImeList.clear();
      this.mInstalledImeList.addAll(null);
      sCompatData.setInstalledImeList(this.mInstalledImeList);
      return;
    } 
  }
  
  private void loadInstalledCompatAppList() {
    if (this.mContext == null)
      return; 
    long l = System.currentTimeMillis();
    null = new ArrayList();
    PackageManager packageManager = this.mContext.getPackageManager();
    try {
      List list = packageManager.getInstalledPackagesAsUser(0, ActivityManager.getCurrentUser());
      for (PackageInfo packageInfo : list) {
        if (packageInfo != null)
          try {
            String str = packageInfo.packageName;
            if (!supportFullScreen(str, packageInfo))
              null.add(str); 
          } catch (Exception exception) {} 
      } 
    } catch (Exception exception) {}
    synchronized (this.mLock) {
      this.mInstalledCompatList.clear();
      this.mInstalledCompatList.addAll(null);
      sCompatData.setInstalledCompatList(this.mInstalledCompatList);
      long l1 = System.currentTimeMillis();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("loadInstalledCompatAppList time cost =");
      stringBuilder.append(l1 - l);
      Slog.i("OplusDisplayCompatUtils", stringBuilder.toString());
      return;
    } 
  }
  
  private void loadInstalledThirdPartyApps() {
    if (this.mContext == null)
      return; 
    null = new ArrayList();
    PackageManager packageManager = this.mContext.getPackageManager();
    try {
      List list = packageManager.getInstalledPackagesAsUser(0, ActivityManager.getCurrentUser());
      for (PackageInfo packageInfo : list) {
        if (packageInfo == null || packageInfo.packageName == null)
          continue; 
        String str = packageInfo.packageName;
        if (sIncludeImmersiveList.contains(str)) {
          null.add(str);
          continue;
        } 
        int i = packageInfo.applicationInfo.flags;
        boolean bool = true;
        if ((i & 0x1) == 0) {
          i = 1;
        } else {
          i = 0;
        } 
        if (!str.startsWith("com.oppo") && 
          !str.startsWith("com.coloros") && 
          !str.startsWith("com.nearme") && 
          !str.startsWith("com.heytap")) {
          List<String> list1 = sExcludeImmersivedList;
          if (!list1.contains(str))
            bool = false; 
        } 
        if (i != 0 && !bool) {
          null.add(str);
          if (DEBUG) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("thirdPartyAppsList add : ");
            stringBuilder.append(str);
            Log.d("OplusDisplayCompatUtils", stringBuilder.toString());
          } 
        } 
      } 
    } catch (Exception exception) {
      exception.printStackTrace();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("fail to loadInstalledThirdPartyApps: ");
      stringBuilder.append(exception.toString());
      Log.d("OplusDisplayCompatUtils", stringBuilder.toString());
    } 
    synchronized (this.mLock) {
      this.mInstalledThirdPartyAppList.clear();
      this.mInstalledThirdPartyAppList.addAll(null);
      sCompatData.setInstalledThirdPartyAppList(this.mInstalledThirdPartyAppList);
      return;
    } 
  }
  
  class LocalCompatSettingsObserverPolicy extends ContentObserver {
    final OplusDisplayCompatUtils this$0;
    
    public LocalCompatSettingsObserverPolicy() {
      super(new Handler());
    }
    
    public void onChange(boolean param1Boolean) {
      OplusDisplayCompatUtils.this.loadLocalCompatAppList();
      OplusDisplayCompatUtils.this.loadLocalFullScreenAppList();
      super.onChange(param1Boolean);
    }
  }
  
  class LocalCutoutSettingsObserverPolicy extends ContentObserver {
    final OplusDisplayCompatUtils this$0;
    
    public LocalCutoutSettingsObserverPolicy() {
      super(new Handler());
    }
    
    public void onChange(boolean param1Boolean) {
      OplusDisplayCompatUtils.this.refreshLocalDefaultModeList();
      OplusDisplayCompatUtils.this.refreshLocalShowModeList();
      OplusDisplayCompatUtils.this.refreshLocalHideModeList();
      super.onChange(param1Boolean);
    }
  }
  
  class LocalShowDialogSettingsObserverPolicy extends ContentObserver {
    final OplusDisplayCompatUtils this$0;
    
    public LocalShowDialogSettingsObserverPolicy() {
      super(new Handler());
    }
    
    public void onChange(boolean param1Boolean) {
      OplusDisplayCompatUtils.this.loadLocalShowDialogAppList();
      super.onChange(param1Boolean);
    }
  }
  
  class MyPackageMonitor extends PackageMonitor {
    final OplusDisplayCompatUtils this$0;
    
    private MyPackageMonitor() {}
    
    public void onPackageRemoved(String param1String, int param1Int) {
      if (!TextUtils.isEmpty(param1String)) {
        OplusDisplayCompatUtils.this.loadInstalledImeAppList();
        if (OplusDisplayCompatUtils.this.inInstalledCompatPkgList(param1String))
          synchronized (OplusDisplayCompatUtils.this.mLock) {
            OplusDisplayCompatUtils.this.mInstalledCompatList.remove(param1String);
            OplusDisplayCompatUtils.sCompatData.setInstalledCompatList(OplusDisplayCompatUtils.this.mInstalledCompatList);
          }  
        if (OplusDisplayCompatUtils.this.inInstalledThirdPartyAppList(param1String))
          synchronized (OplusDisplayCompatUtils.this.mLock) {
            OplusDisplayCompatUtils.this.mInstalledThirdPartyAppList.remove(param1String);
            OplusDisplayCompatUtils.sCompatData.setInstalledThirdPartyAppList(OplusDisplayCompatUtils.this.mInstalledThirdPartyAppList);
          }  
        OplusDisplayCompatUtils.this.removeLocalShowDialogListForPkg(param1String);
      } 
    }
    
    public void onPackageAdded(String param1String, int param1Int) {
      if (!TextUtils.isEmpty(param1String)) {
        OplusDisplayCompatUtils.this.loadInstalledImeAppList();
        if (!OplusDisplayCompatUtils.this.inInstalledImeList(param1String) && !OplusDisplayCompatUtils.this.supportFullScreen(param1String))
          synchronized (OplusDisplayCompatUtils.this.mLock) {
            OplusDisplayCompatUtils.this.mInstalledCompatList.add(param1String);
            OplusDisplayCompatUtils.sCompatData.setInstalledCompatList(OplusDisplayCompatUtils.this.mInstalledCompatList);
          }  
        if (!OplusDisplayCompatUtils.this.inInstalledThirdPartyAppList(param1String) && OplusDisplayCompatUtils.this.isInstalledThirdPartyApp(param1String))
          synchronized (OplusDisplayCompatUtils.this.mLock) {
            OplusDisplayCompatUtils.this.mInstalledThirdPartyAppList.add(param1String);
            OplusDisplayCompatUtils.sCompatData.setInstalledThirdPartyAppList(OplusDisplayCompatUtils.this.mInstalledThirdPartyAppList);
          }  
      } 
    }
    
    public void onPackageModified(String param1String) {
      if (!TextUtils.isEmpty(param1String)) {
        OplusDisplayCompatUtils.this.loadInstalledImeAppList();
        if (!OplusDisplayCompatUtils.this.inInstalledImeList(param1String)) {
          boolean bool = OplusDisplayCompatUtils.this.supportFullScreen(param1String);
          Object object = OplusDisplayCompatUtils.this.mLock;
          /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          if (bool)
            try {
              if (OplusDisplayCompatUtils.this.inInstalledCompatPkgList(param1String))
                OplusDisplayCompatUtils.this.mInstalledCompatList.remove(param1String); 
            } finally {} 
          if (!bool && !OplusDisplayCompatUtils.this.inInstalledCompatPkgList(param1String))
            OplusDisplayCompatUtils.this.mInstalledCompatList.add(param1String); 
          OplusDisplayCompatUtils.sCompatData.setInstalledCompatList(OplusDisplayCompatUtils.this.mInstalledCompatList);
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        } 
      } 
    }
  }
  
  private OplusDisplayCompatUtils() {
    this.mReceiver = (BroadcastReceiver)new Object(this);
    this.mMultiUserReceiver = (BroadcastReceiver)new Object(this);
  }
  
  private void setDisplayCutoutType() {
    try {
      String str = OplusSystemProperties.get("ro.oplus.display.screen.heteromorphism", "");
      StringBuilder stringBuilder1 = new StringBuilder();
      this();
      stringBuilder1.append("cutout size: ");
      stringBuilder1.append(str);
      Log.d("OplusDisplayCompatUtils", stringBuilder1.toString());
      if (str == null)
        return; 
      String[] arrayOfString = str.split("[,:]");
      if (arrayOfString.length == 4) {
        int i = Integer.parseInt(arrayOfString[0]);
        int j = Integer.parseInt(arrayOfString[2]);
        if (i < 50 && j < 300) {
          this.mDisplayCutoutType = 1;
        } else {
          this.mDisplayCutoutType = 2;
        } 
      } 
      sCompatData.setDisplayCutoutType(this.mDisplayCutoutType);
    } catch (Exception exception) {
      exception.printStackTrace();
      Log.d("OplusDisplayCompatUtils", "fail to set display cutout type");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("set display cutout type : ");
    stringBuilder.append(this.mDisplayCutoutType);
    Log.d("OplusDisplayCompatUtils", stringBuilder.toString());
  }
}
