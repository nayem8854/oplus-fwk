package com.oplus.util;

import android.os.Parcel;
import android.os.Parcelable;

public class OplusReflectWidget implements Parcelable {
  public static final Parcelable.Creator<OplusReflectWidget> CREATOR;
  
  public static final OplusReflectWidget DEFAULT_WIDGET = new OplusReflectWidget("com.tencent.mm", 1280, "com.tencent.mm.ui.widget.MMNeatTextView", 1, "mText");
  
  public static final OplusReflectWidget DEFAULT_WIDGET_WECHAT_1420 = new OplusReflectWidget("com.tencent.mm", 1420, "com.tencent.mm.ui.widget.MMNeat7extView", 1, "mText");
  
  private String className;
  
  private String field;
  
  private int fieldLevel;
  
  private String packageName;
  
  private int versionCode;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.packageName);
    paramParcel.writeInt(this.versionCode);
    paramParcel.writeString(this.className);
    paramParcel.writeInt(this.fieldLevel);
    paramParcel.writeString(this.field);
  }
  
  public OplusReflectWidget() {}
  
  public OplusReflectWidget(String paramString1, int paramInt1, String paramString2, int paramInt2, String paramString3) {
    this.packageName = paramString1;
    this.versionCode = paramInt1;
    this.className = paramString2;
    this.fieldLevel = paramInt2;
    this.field = paramString3;
  }
  
  public String getPackageName() {
    return this.packageName;
  }
  
  public void setPackageName(String paramString) {
    this.packageName = paramString;
  }
  
  public int getVersionCode() {
    return this.versionCode;
  }
  
  public void setVersionCode(int paramInt) {
    this.versionCode = paramInt;
  }
  
  public String getClassName() {
    return this.className;
  }
  
  public void setClassName(String paramString) {
    this.className = paramString;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.versionCode != ((OplusReflectWidget)paramObject).versionCode)
      return false; 
    if (this.fieldLevel != ((OplusReflectWidget)paramObject).fieldLevel)
      return false; 
    if (!this.packageName.equals(((OplusReflectWidget)paramObject).packageName))
      return false; 
    if (!this.className.equals(((OplusReflectWidget)paramObject).className))
      return false; 
    return this.field.equals(((OplusReflectWidget)paramObject).field);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("OplusReflectWidget{packageName='");
    stringBuilder.append(this.packageName);
    stringBuilder.append('\'');
    stringBuilder.append(", versionCode=");
    stringBuilder.append(this.versionCode);
    stringBuilder.append(", className='");
    stringBuilder.append(this.className);
    stringBuilder.append('\'');
    stringBuilder.append(", fieldLevel=");
    stringBuilder.append(this.fieldLevel);
    stringBuilder.append(", field='");
    stringBuilder.append(this.field);
    stringBuilder.append('\'');
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public int hashCode() {
    int i = this.packageName.hashCode();
    int j = this.versionCode;
    int k = this.className.hashCode();
    int m = this.fieldLevel;
    int n = this.field.hashCode();
    return (((i * 31 + j) * 31 + k) * 31 + m) * 31 + n;
  }
  
  public void setFieldLevel(int paramInt) {
    this.fieldLevel = paramInt;
  }
  
  public int getFieldLevel() {
    return this.fieldLevel;
  }
  
  public String getField() {
    return this.field;
  }
  
  public void setField(String paramString) {
    this.field = paramString;
  }
  
  protected OplusReflectWidget(Parcel paramParcel) {
    this.packageName = paramParcel.readString();
    this.versionCode = paramParcel.readInt();
    this.className = paramParcel.readString();
    this.fieldLevel = paramParcel.readInt();
    this.field = paramParcel.readString();
  }
  
  static {
    CREATOR = new Parcelable.Creator<OplusReflectWidget>() {
        public OplusReflectWidget createFromParcel(Parcel param1Parcel) {
          return new OplusReflectWidget(param1Parcel);
        }
        
        public OplusReflectWidget[] newArray(int param1Int) {
          return new OplusReflectWidget[param1Int];
        }
      };
  }
}
