package com.oplus.util;

import android.content.Context;
import android.util.Slog;
import android.util.SparseArray;
import android.util.Xml;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class OplusStandardRusHelper extends RomUpdateHelper {
  public static final String BUILD_SDK_NAME = "build_sdk";
  
  private static final int CONST_FOUR = 4;
  
  private static final int CONST_THREE = 3;
  
  private static final int CONST_ZERO = 0;
  
  private static final int LETTER_NUM = 26;
  
  public static final String OS_VERSION_NAME = "os_version";
  
  private static final String TAG = "OplusStandardRusHelper";
  
  public static final String VERSION_NAME = "version";
  
  private static final HashMap<String, WeakReference<OplusStandardRusHelper>> mRefMap = new HashMap<>();
  
  private OplusRusHelperCallback mCallback;
  
  protected final Context mContext;
  
  private String mSystemFile;
  
  class MatchMode extends Enum<MatchMode> {
    private static final MatchMode[] $VALUES;
    
    public static final MatchMode MODE_CONTAIN_MODE_MATCH;
    
    public static MatchMode[] values() {
      return (MatchMode[])$VALUES.clone();
    }
    
    public static MatchMode valueOf(String param1String) {
      return Enum.<MatchMode>valueOf(MatchMode.class, param1String);
    }
    
    private MatchMode(OplusStandardRusHelper this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static final MatchMode MODE_NORMAL_MODE_MATCH = new MatchMode("MODE_NORMAL_MODE_MATCH", 0);
    
    static {
      MatchMode matchMode = new MatchMode("MODE_CONTAIN_MODE_MATCH", 1);
      $VALUES = new MatchMode[] { MODE_NORMAL_MODE_MATCH, matchMode };
    }
  }
  
  class OplusRusHelperCallback {
    final OplusStandardRusHelper this$0;
    
    public void onUpdate() {}
  }
  
  public OplusStandardRusHelper(Context paramContext, String paramString1, String paramString2, String paramString3) {
    super(paramContext, paramString1, paramString2, paramString3);
    this.mContext = paramContext;
    this.mCallback = new OplusRusHelperCallback() {
        final OplusStandardRusHelper this$0;
      };
    this.mSystemFile = paramString2;
    recordReferences(paramString1, this);
    setUpdateInfo(new StandardUpdateInfo(), new StandardUpdateInfo());
    try {
      init();
    } catch (Exception exception) {}
  }
  
  public void init() {
    super.init();
    String str = readFromFile(new File(this.mSystemFile));
    StandardUpdateInfo standardUpdateInfo1 = (StandardUpdateInfo)getUpdateInfo(false);
    StandardUpdateInfo standardUpdateInfo2 = (StandardUpdateInfo)getUpdateInfo(true);
    standardUpdateInfo1.parseContentFromXML(str);
    if (!checkValidityForData(standardUpdateInfo1, standardUpdateInfo2))
      standardUpdateInfo2.clone(standardUpdateInfo1); 
  }
  
  public void setRusCallback(OplusRusHelperCallback paramOplusRusHelperCallback) {
    this.mCallback = paramOplusRusHelperCallback;
  }
  
  private boolean checkValidityForData(StandardUpdateInfo paramStandardUpdateInfo1, StandardUpdateInfo paramStandardUpdateInfo2) {
    if (paramStandardUpdateInfo1.getOsVersion() != 0 && 
      paramStandardUpdateInfo1.getOsVersion() != paramStandardUpdateInfo2.getOsVersion()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("os version not match, data invalid, system:");
      stringBuilder.append(paramStandardUpdateInfo1.getOsVersion());
      stringBuilder.append(" data:");
      stringBuilder.append(paramStandardUpdateInfo2.getOsVersion());
      Slog.w("OplusStandardRusHelper", stringBuilder.toString());
      return false;
    } 
    if (paramStandardUpdateInfo1.getBuildSdk() != 0 && 
      paramStandardUpdateInfo1.getBuildSdk() != paramStandardUpdateInfo2.getBuildSdk()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("build sdk not match, data invalid, system:");
      stringBuilder.append(paramStandardUpdateInfo1.getBuildSdk());
      stringBuilder.append(" data:");
      stringBuilder.append(paramStandardUpdateInfo2.getBuildSdk());
      Slog.w("OplusStandardRusHelper", stringBuilder.toString());
      return false;
    } 
    if (paramStandardUpdateInfo1.getVersion() > paramStandardUpdateInfo2.getVersion()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("lower data version, data invalid, system:");
      stringBuilder.append(paramStandardUpdateInfo1.getVersion());
      stringBuilder.append(" data:");
      stringBuilder.append(paramStandardUpdateInfo2.getVersion());
      Slog.w("OplusStandardRusHelper", stringBuilder.toString());
      return false;
    } 
    return true;
  }
  
  private void recordReferences(String paramString, OplusStandardRusHelper paramOplusStandardRusHelper) {
    if (mRefMap.containsKey(paramString)) {
      if (((WeakReference)mRefMap.get(paramString)).get() != null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("multible RusHelper for same type:");
        stringBuilder.append(paramString);
        Slog.w("OplusStandardRusHelper", stringBuilder.toString());
      } 
      mRefMap.replace(paramString, new WeakReference<>(paramOplusStandardRusHelper));
    } 
    mRefMap.put(paramString, new WeakReference<>(paramOplusStandardRusHelper));
  }
  
  public int getTimeInWhiteList(int paramInt, String paramString) {
    if (paramString == null)
      return 0; 
    StandardUpdateInfo standardUpdateInfo = (StandardUpdateInfo)getUpdateInfo(true);
    if (standardUpdateInfo == null)
      return 0; 
    paramInt = standardUpdateInfo.getTimeInWhiteList(paramInt, paramString);
    return paramInt;
  }
  
  protected class StandardUpdateInfo extends RomUpdateHelper.UpdateInfo {
    private int mBuildSdk;
    
    private int mOsVersion;
    
    private final ReentrantReadWriteLock.ReadLock mR;
    
    private final ReentrantReadWriteLock mRWLock;
    
    private final ReentrantReadWriteLock.WriteLock mW;
    
    private final SparseArray<ArrayList<String>> mWhiteList = new SparseArray();
    
    final OplusStandardRusHelper this$0;
    
    public StandardUpdateInfo() {
      ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock();
      this.mR = reentrantReadWriteLock.readLock();
      this.mW = this.mRWLock.writeLock();
      this.mOsVersion = 0;
      this.mBuildSdk = 0;
    }
    
    public void parseContentFromXML(String param1String) {
      if (param1String == null)
        return; 
      StringReader stringReader1 = null, stringReader2 = null, stringReader3 = null;
      this.mVersion = -1L;
      this.mOsVersion = 0;
      this.mBuildSdk = 0;
      this.mW.lock();
      this.mWhiteList.clear();
      StringReader stringReader4 = stringReader3, stringReader5 = stringReader1, stringReader6 = stringReader2;
      try {
        XmlPullParser xmlPullParser = Xml.newPullParser();
        stringReader4 = stringReader3;
        stringReader5 = stringReader1;
        stringReader6 = stringReader2;
        StringReader stringReader8 = new StringReader();
        stringReader4 = stringReader3;
        stringReader5 = stringReader1;
        stringReader6 = stringReader2;
        this(param1String);
        StringReader stringReader7 = stringReader8;
        stringReader4 = stringReader7;
        stringReader5 = stringReader7;
        stringReader6 = stringReader7;
        xmlPullParser.setInput(stringReader7);
        stringReader4 = stringReader7;
        stringReader5 = stringReader7;
        stringReader6 = stringReader7;
        int i = xmlPullParser.getEventType();
        while (i != 1) {
          if (i == 2) {
            stringReader4 = stringReader7;
            stringReader5 = stringReader7;
            stringReader6 = stringReader7;
            char[] arrayOfChar = xmlPullParser.getName().toCharArray();
            stringReader4 = stringReader7;
            stringReader5 = stringReader7;
            stringReader6 = stringReader7;
            if (arrayOfChar.length > 3) {
              stringReader4 = stringReader7;
              stringReader5 = stringReader7;
              stringReader6 = stringReader7;
              xmlPullParser.next();
              stringReader4 = stringReader7;
              stringReader5 = stringReader7;
              stringReader6 = stringReader7;
              updateConfigVersion(String.valueOf(arrayOfChar), xmlPullParser.getText());
              stringReader4 = stringReader7;
              stringReader5 = stringReader7;
              stringReader6 = stringReader7;
              updateOsVersion(String.valueOf(arrayOfChar), xmlPullParser.getText());
              stringReader4 = stringReader7;
              stringReader5 = stringReader7;
              stringReader6 = stringReader7;
              updateSdkVersion(String.valueOf(arrayOfChar), xmlPullParser.getText());
            } else {
              stringReader4 = stringReader7;
              stringReader5 = stringReader7;
              stringReader6 = stringReader7;
              i = char2int(arrayOfChar);
              stringReader4 = stringReader7;
              stringReader5 = stringReader7;
              stringReader6 = stringReader7;
              xmlPullParser.next();
              if (i >= 0) {
                stringReader4 = stringReader7;
                stringReader5 = stringReader7;
                stringReader6 = stringReader7;
                ArrayList<String> arrayList = (ArrayList)this.mWhiteList.get(i);
                if (arrayList == null) {
                  stringReader4 = stringReader7;
                  stringReader5 = stringReader7;
                  stringReader6 = stringReader7;
                  arrayList = new ArrayList();
                  stringReader4 = stringReader7;
                  stringReader5 = stringReader7;
                  stringReader6 = stringReader7;
                  this();
                  stringReader4 = stringReader7;
                  stringReader5 = stringReader7;
                  stringReader6 = stringReader7;
                  arrayList.add(xmlPullParser.getText());
                  stringReader4 = stringReader7;
                  stringReader5 = stringReader7;
                  stringReader6 = stringReader7;
                  this.mWhiteList.put(i, arrayList);
                } else {
                  stringReader4 = stringReader7;
                  stringReader5 = stringReader7;
                  stringReader6 = stringReader7;
                  arrayList.add(xmlPullParser.getText());
                } 
              } 
            } 
          } 
          stringReader4 = stringReader7;
          stringReader5 = stringReader7;
          stringReader6 = stringReader7;
          i = xmlPullParser.next();
        } 
        this.mW.unlock();
        if (false) {
          try {
            throw new NullPointerException();
          } catch (IOException iOException1) {
            OplusStandardRusHelper.this.log("Got execption close permReader.", iOException1);
          } 
        } else {
          iOException1.close();
        } 
        OplusStandardRusHelper.this.mCallback.onUpdate();
        return;
      } catch (XmlPullParserException xmlPullParserException) {
        stringReader4 = stringReader6;
        OplusStandardRusHelper.this.log("Got execption parsing permissions.", (Exception)xmlPullParserException);
        this.mW.unlock();
        if (false) {
          try {
            throw new NullPointerException();
          } catch (IOException iOException1) {
            OplusStandardRusHelper.this.log("Got execption close permReader.", iOException1);
          } 
        } else if (stringReader6 != null) {
          stringReader6.close();
        } 
        return;
      } catch (IOException iOException1) {
        stringReader4 = stringReader5;
        OplusStandardRusHelper.this.log("Got execption parsing permissions.", iOException1);
        this.mW.unlock();
        if (false) {
          try {
            throw new NullPointerException();
          } catch (IOException iOException2) {
            OplusStandardRusHelper.this.log("Got execption close permReader.", iOException2);
          } 
        } else if (stringReader5 != null) {
          stringReader5.close();
        } 
        return;
      } finally {}
      this.mW.unlock();
      if (false) {
        try {
          throw new NullPointerException();
        } catch (IOException iOException) {
          OplusStandardRusHelper.this.log("Got execption close permReader.", iOException);
        } 
      } else if (iOException != null) {
        iOException.close();
      } 
      throw param1String;
    }
    
    private void updateConfigVersion(String param1String1, String param1String2) {
      if ("version".equals(param1String1))
        this.mVersion = Long.parseLong(param1String2); 
    }
    
    private void updateOsVersion(String param1String1, String param1String2) {
      if ("os_version".equals(param1String1))
        this.mOsVersion = Integer.parseInt(param1String2); 
    }
    
    private void updateSdkVersion(String param1String1, String param1String2) {
      if ("build_sdk".equals(param1String1))
        this.mBuildSdk = Integer.parseInt(param1String2); 
    }
    
    public int getOsVersion() {
      return this.mOsVersion;
    }
    
    public int getBuildSdk() {
      return this.mBuildSdk;
    }
    
    public boolean updateToLowerVersion(String param1String) {
      long l = -1L;
      int i = 0;
      int j = 0;
      if (param1String == null)
        return true; 
      StringReader stringReader1 = null, stringReader2 = null, stringReader3 = null;
      StringReader stringReader4 = stringReader3, stringReader5 = stringReader1, stringReader6 = stringReader2;
      try {
        long l1;
        int i1, i2;
        XmlPullParser xmlPullParser = Xml.newPullParser();
        stringReader4 = stringReader3;
        stringReader5 = stringReader1;
        stringReader6 = stringReader2;
        StringReader stringReader8 = new StringReader();
        stringReader4 = stringReader3;
        stringReader5 = stringReader1;
        stringReader6 = stringReader2;
        this(param1String);
        StringReader stringReader7 = stringReader8;
        stringReader4 = stringReader7;
        stringReader5 = stringReader7;
        stringReader6 = stringReader7;
        xmlPullParser.setInput(stringReader7);
        stringReader4 = stringReader7;
        stringReader5 = stringReader7;
        stringReader6 = stringReader7;
        int k = xmlPullParser.getEventType();
        int m = 0;
        int n = 0;
        boolean bool = false;
        while (true) {
          l1 = l;
          i1 = i;
          i2 = j;
          if (k != 1) {
            int i3;
            boolean bool1;
            if (k != 2) {
              i2 = j;
              i3 = m;
              bool1 = bool;
            } else {
              stringReader4 = stringReader7;
              stringReader5 = stringReader7;
              stringReader6 = stringReader7;
              String str = xmlPullParser.getName();
              l1 = l;
              k = m;
              if (!m) {
                l1 = l;
                k = m;
                stringReader4 = stringReader7;
                stringReader5 = stringReader7;
                stringReader6 = stringReader7;
                if ("version".equals(str)) {
                  stringReader4 = stringReader7;
                  stringReader5 = stringReader7;
                  stringReader6 = stringReader7;
                  xmlPullParser.next();
                  stringReader4 = stringReader7;
                  stringReader5 = stringReader7;
                  stringReader6 = stringReader7;
                  xmlPullParser.getText();
                  stringReader4 = stringReader7;
                  stringReader5 = stringReader7;
                  stringReader6 = stringReader7;
                  l1 = Long.parseLong(xmlPullParser.getText());
                  k = 1;
                } 
              } 
              i1 = i;
              m = n;
              if (!n) {
                i1 = i;
                m = n;
                stringReader4 = stringReader7;
                stringReader5 = stringReader7;
                stringReader6 = stringReader7;
                if ("os_version".equals(str)) {
                  stringReader4 = stringReader7;
                  stringReader5 = stringReader7;
                  stringReader6 = stringReader7;
                  xmlPullParser.next();
                  stringReader4 = stringReader7;
                  stringReader5 = stringReader7;
                  stringReader6 = stringReader7;
                  xmlPullParser.getText();
                  stringReader4 = stringReader7;
                  stringReader5 = stringReader7;
                  stringReader6 = stringReader7;
                  i1 = Integer.parseInt(xmlPullParser.getText());
                  m = 1;
                } 
              } 
              l = l1;
              i = i1;
              i2 = j;
              i3 = k;
              n = m;
              bool1 = bool;
              if (!bool) {
                l = l1;
                i = i1;
                i2 = j;
                i3 = k;
                n = m;
                bool1 = bool;
                stringReader4 = stringReader7;
                stringReader5 = stringReader7;
                stringReader6 = stringReader7;
                if ("build_sdk".equals(str)) {
                  stringReader4 = stringReader7;
                  stringReader5 = stringReader7;
                  stringReader6 = stringReader7;
                  xmlPullParser.next();
                  stringReader4 = stringReader7;
                  stringReader5 = stringReader7;
                  stringReader6 = stringReader7;
                  xmlPullParser.getText();
                  stringReader4 = stringReader7;
                  stringReader5 = stringReader7;
                  stringReader6 = stringReader7;
                  i2 = Integer.parseInt(xmlPullParser.getText());
                  bool1 = true;
                  n = m;
                  i3 = k;
                  i = i1;
                  l = l1;
                } 
              } 
            } 
            if (i3 != 0 && n && bool1) {
              l1 = l;
              i1 = i;
              break;
            } 
            stringReader4 = stringReader7;
            stringReader5 = stringReader7;
            stringReader6 = stringReader7;
            k = xmlPullParser.next();
            j = i2;
            m = i3;
            bool = bool1;
            continue;
          } 
          break;
        } 
        if (false) {
          try {
            throw new NullPointerException();
          } catch (IOException iOException1) {
            OplusStandardRusHelper.this.log("Got execption close permReader.", iOException1);
          } 
        } else {
          iOException1.close();
        } 
        if (getOsVersion() != 0 && getOsVersion() > i1) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("lower os version, data invalid, current:");
          stringBuilder.append(getOsVersion());
          stringBuilder.append(" new:");
          stringBuilder.append(i1);
          Slog.w("OplusStandardRusHelper", stringBuilder.toString());
          return true;
        } 
        if (getBuildSdk() != 0 && getBuildSdk() > i2) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("lower build sdk, data invalid, current:");
          stringBuilder.append(getBuildSdk());
          stringBuilder.append(" new:");
          stringBuilder.append(i2);
          Slog.w("OplusStandardRusHelper", stringBuilder.toString());
          return true;
        } 
        if (getVersion() != -1L && getVersion() > l1) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("lower version, data invalid, current:");
          stringBuilder.append(getVersion());
          stringBuilder.append(" new:");
          stringBuilder.append(l1);
          Slog.w("OplusStandardRusHelper", stringBuilder.toString());
          return true;
        } 
        return false;
      } catch (XmlPullParserException xmlPullParserException) {
        stringReader4 = stringReader6;
        OplusStandardRusHelper.this.log("Got execption parsing permissions.", (Exception)xmlPullParserException);
        if (false) {
          try {
            throw new NullPointerException();
          } catch (IOException iOException1) {
            OplusStandardRusHelper.this.log("Got execption close permReader.", iOException1);
          } 
        } else if (stringReader6 != null) {
          stringReader6.close();
        } 
        return true;
      } catch (IOException iOException1) {
        stringReader4 = stringReader5;
        OplusStandardRusHelper.this.log("Got execption parsing permissions.", iOException1);
        if (false) {
          try {
            throw new NullPointerException();
          } catch (IOException iOException2) {
            OplusStandardRusHelper.this.log("Got execption close permReader.", iOException2);
          } 
        } else if (stringReader5 != null) {
          stringReader5.close();
        } 
        return true;
      } finally {}
      if (false) {
        try {
          throw new NullPointerException();
        } catch (IOException iOException) {
          OplusStandardRusHelper.this.log("Got execption close permReader.", iOException);
        } 
      } else if (iOException != null) {
        iOException.close();
      } 
      throw param1String;
    }
    
    public boolean clone(RomUpdateHelper.UpdateInfo param1UpdateInfo) {
      StandardUpdateInfo standardUpdateInfo = (StandardUpdateInfo)param1UpdateInfo;
      SparseArray<ArrayList<String>> sparseArray = standardUpdateInfo.getAllList();
      if (sparseArray == null || sparseArray.size() == 0) {
        OplusStandardRusHelper.this.log("Source object is empty");
        return false;
      } 
      this.mW.lock();
      this.mWhiteList.clear();
      this.mVersion = standardUpdateInfo.getVersion();
      this.mOsVersion = standardUpdateInfo.getOsVersion();
      this.mBuildSdk = standardUpdateInfo.getBuildSdk();
      for (byte b = 0; b < sparseArray.size(); b++) {
        int i = sparseArray.keyAt(b);
        ArrayList arrayList = (ArrayList)sparseArray.get(i);
        this.mWhiteList.put(i, arrayList.clone());
      } 
      this.mW.unlock();
      return true;
    }
    
    public boolean insert(int param1Int, String param1String) {
      this.mR.lock();
      ArrayList<String> arrayList = (ArrayList)this.mWhiteList.get(param1Int);
      this.mR.unlock();
      if (arrayList != null) {
        arrayList.add(param1String);
        return true;
      } 
      return false;
    }
    
    public void clear() {
      this.mW.lock();
      this.mWhiteList.clear();
      this.mW.unlock();
      this.mVersion = -1L;
      this.mOsVersion = 0;
      this.mBuildSdk = 0;
    }
    
    private int char2int(char[] param1ArrayOfchar) {
      int i = 0;
      if (param1ArrayOfchar.length < 1)
        return -1; 
      for (byte b = 0; b < param1ArrayOfchar.length; b++)
        i = (int)(i + (param1ArrayOfchar[b] - 97) * Math.pow(26.0D, (param1ArrayOfchar.length - b - 1))); 
      return i;
    }
    
    public String dumpToString() {
      StringBuilder stringBuilder1 = new StringBuilder();
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("UpdateInfo [");
      stringBuilder2.append(hashCode());
      stringBuilder2.append(", version = ");
      stringBuilder2.append(getVersion());
      stringBuilder2.append("]\n");
      stringBuilder1.append(stringBuilder2.toString());
      this.mR.lock();
      for (byte b = 0; b < this.mWhiteList.size(); b++) {
        int i = this.mWhiteList.keyAt(b);
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append("type = ");
        stringBuilder2.append(i);
        stringBuilder1.append(stringBuilder2.toString());
        ArrayList arrayList = (ArrayList)this.mWhiteList.get(i);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(", value = ");
        stringBuilder.append(arrayList);
        stringBuilder.append("\n");
        stringBuilder1.append(stringBuilder.toString());
      } 
      this.mR.unlock();
      return stringBuilder1.toString();
    }
    
    public boolean isInWhiteList(int param1Int, String param1String) {
      this.mR.lock();
      if (this.mWhiteList.indexOfKey(param1Int) >= 0 && (
        (ArrayList)this.mWhiteList.get(param1Int)).contains(param1String)) {
        this.mR.unlock();
        return true;
      } 
      this.mR.unlock();
      return false;
    }
    
    public ArrayList<String> getOneList(int param1Int) {
      this.mR.lock();
      ArrayList<String> arrayList = (ArrayList)this.mWhiteList.get(param1Int);
      this.mR.unlock();
      if (arrayList != null) {
        arrayList = (ArrayList)arrayList.clone();
      } else {
        arrayList = null;
      } 
      return arrayList;
    }
    
    public SparseArray<ArrayList<String>> getAllList() {
      this.mR.lock();
      SparseArray<ArrayList<String>> sparseArray = this.mWhiteList.clone();
      this.mR.unlock();
      return sparseArray;
    }
    
    public int getTimeInWhiteList(int param1Int, String param1String) {
      this.mR.lock();
      if (this.mWhiteList.indexOfKey(param1Int) >= 0) {
        String str = ((ArrayList)this.mWhiteList.get(param1Int)).toString();
        this.mR.unlock();
        if (str.contains(param1String)) {
          str = str.substring(1, str.length() - 1);
          String[] arrayOfString = str.split(", ");
          String str1 = "";
          for (param1Int = 0; param1Int < arrayOfString.length; param1Int++, str1 = str) {
            str = str1;
            if (param1String.equals(arrayOfString[param1Int])) {
              str = str1;
              if (param1Int < arrayOfString.length - 1)
                str = arrayOfString[param1Int + 1]; 
            } 
          } 
          try {
            param1Int = Integer.parseInt(str1);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("getTimeInWhiteList msec = ");
            stringBuilder.append(param1Int);
            Slog.d("OplusStandardRusHelper", stringBuilder.toString());
            return param1Int;
          } catch (NumberFormatException numberFormatException) {
            return 0;
          } 
        } 
        return 0;
      } 
      this.mR.unlock();
      return 0;
    }
  }
  
  public String dumpToString() {
    StandardUpdateInfo standardUpdateInfo = (StandardUpdateInfo)getUpdateInfo(true);
    return standardUpdateInfo.dumpToString();
  }
  
  public SparseArray<ArrayList<String>> getAllList() {
    return ((StandardUpdateInfo)getUpdateInfo(true)).getAllList();
  }
  
  public ArrayList<String> getOneList(int paramInt) {
    return ((StandardUpdateInfo)getUpdateInfo(true)).getOneList(paramInt);
  }
  
  public boolean isInWhiteList(int paramInt, String paramString) {
    return isInWhiteList(paramInt, paramString, false);
  }
  
  public boolean isInWhiteList(int paramInt, String paramString, boolean paramBoolean) {
    ArrayList<String> arrayList;
    if (paramString == null)
      return false; 
    StandardUpdateInfo standardUpdateInfo = (StandardUpdateInfo)getUpdateInfo(true);
    if (standardUpdateInfo == null)
      return false; 
    if (paramBoolean) {
      arrayList = standardUpdateInfo.getOneList(paramInt);
      if (arrayList != null)
        return isContained(arrayList, paramString); 
      return false;
    } 
    return arrayList.isInWhiteList(paramInt, paramString);
  }
  
  public static boolean isInWhiteList(String paramString1, int paramInt, String paramString2) throws IllegalStateException {
    return isInWhiteList(paramString1, paramInt, paramString2, MatchMode.MODE_NORMAL_MODE_MATCH);
  }
  
  public static boolean isInWhiteList(String paramString1, int paramInt, String paramString2, MatchMode paramMatchMode) throws IllegalStateException {
    if (mRefMap.containsKey(paramString1)) {
      WeakReference<OplusStandardRusHelper> weakReference = mRefMap.get(paramString1);
      if (weakReference != null && weakReference.get() != null) {
        int i = null.$SwitchMap$com$oplus$util$OplusStandardRusHelper$MatchMode[paramMatchMode.ordinal()];
        if (i != 1) {
          if (i != 2) {
            Slog.w("OplusStandardRusHelper", "Unknown mode");
            return false;
          } 
          return ((OplusStandardRusHelper)weakReference.get()).isInWhiteList(paramInt, paramString2, true);
        } 
        return ((OplusStandardRusHelper)weakReference.get()).isInWhiteList(paramInt, paramString2, false);
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString1);
      stringBuilder1.append(" ref may have been expired");
      Slog.w("OplusStandardRusHelper", stringBuilder1.toString());
      mRefMap.remove(paramString1);
      return false;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString1);
    stringBuilder.append(" Rushelper has not been initialized");
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public boolean insertValueInList(int paramInt, String paramString) {
    return super.insertValueInList(paramInt, paramString);
  }
  
  private boolean isContained(ArrayList<String> paramArrayList, String paramString) {
    for (byte b = 0; b < paramArrayList.size(); b++) {
      if (paramString != null && (
        paramString.contains(paramArrayList.get(b)) || (
        (String)paramArrayList.get(b)).contains(paramString)))
        return true; 
    } 
    return false;
  }
}
