package com.oplus.util;

import java.util.List;

public abstract class OplusSignatureUpdater {
  protected abstract List<String> getSignatures();
}
