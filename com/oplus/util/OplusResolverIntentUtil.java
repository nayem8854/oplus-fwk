package com.oplus.util;

import android.content.Intent;
import java.util.Collection;

public class OplusResolverIntentUtil {
  public static final String DEFAULT_APP_APPLICATION = "application";
  
  public static final String DEFAULT_APP_AUDIO = "audio";
  
  public static final String DEFAULT_APP_BROWSER = "browser";
  
  public static final String DEFAULT_APP_CAMERA = "camera";
  
  public static final String DEFAULT_APP_CONTACTS = "contacts";
  
  public static final String DEFAULT_APP_DIALER = "dialer";
  
  public static final String DEFAULT_APP_EMAIL = "email";
  
  public static final String DEFAULT_APP_EXCEL = "excel";
  
  public static final String DEFAULT_APP_GALLERY = "gallery";
  
  public static final String DEFAULT_APP_LAUNCHER = "launcher";
  
  public static final String DEFAULT_APP_MARKET = "market";
  
  public static final String DEFAULT_APP_MESSAGE = "message";
  
  public static final String DEFAULT_APP_PDF = "pdf";
  
  public static final String DEFAULT_APP_PPT = "ppt";
  
  public static final String DEFAULT_APP_TEXT = "txt";
  
  public static final String DEFAULT_APP_VIDEO = "video";
  
  public static final String DEFAULT_APP_WORD = "word";
  
  private static final String TAG = "OplusResolverIntentUtil";
  
  public static boolean isChooserAction(Intent paramIntent) {
    if (paramIntent == null)
      return false; 
    String str = paramIntent.getAction();
    if (str != null && (
      str.equals("android.intent.action.SEND") || str.equals("android.intent.action.SEND_MULTIPLE")))
      return true; 
    return false;
  }
  
  public static String getIntentType(Intent paramIntent) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getAction : ()Ljava/lang/String;
    //   4: astore_1
    //   5: aload_0
    //   6: invokevirtual getCategories : ()Ljava/util/Set;
    //   9: astore_2
    //   10: aload_0
    //   11: invokevirtual getScheme : ()Ljava/lang/String;
    //   14: astore_3
    //   15: aload_0
    //   16: invokevirtual getType : ()Ljava/lang/String;
    //   19: astore #4
    //   21: aload_0
    //   22: invokevirtual getData : ()Landroid/net/Uri;
    //   25: astore_0
    //   26: aload_0
    //   27: ifnull -> 38
    //   30: aload_0
    //   31: invokevirtual getHost : ()Ljava/lang/String;
    //   34: astore_0
    //   35: goto -> 40
    //   38: aconst_null
    //   39: astore_0
    //   40: ldc 'others'
    //   42: astore #5
    //   44: aload_1
    //   45: ldc 'android.intent.action.MAIN'
    //   47: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   50: ifeq -> 68
    //   53: aload_2
    //   54: ldc 'android.intent.category.HOME'
    //   56: invokestatic isInDataSet : (Ljava/util/Collection;Ljava/lang/Object;)Z
    //   59: ifeq -> 68
    //   62: ldc 'launcher'
    //   64: astore_0
    //   65: goto -> 716
    //   68: aload_3
    //   69: ldc 'sms'
    //   71: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   74: ifne -> 713
    //   77: aload_3
    //   78: ldc 'mms'
    //   80: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   83: ifne -> 713
    //   86: aload_3
    //   87: ldc 'smsto'
    //   89: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   92: ifne -> 713
    //   95: aload_3
    //   96: ldc 'mmsto'
    //   98: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   101: ifeq -> 107
    //   104: goto -> 713
    //   107: aload_1
    //   108: ldc 'android.intent.action.DIAL'
    //   110: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   113: ifne -> 707
    //   116: aload_3
    //   117: ldc 'tel'
    //   119: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   122: ifne -> 707
    //   125: aload #4
    //   127: ldc 'vnd.android.cursor.dir/calls'
    //   129: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   132: ifeq -> 138
    //   135: goto -> 707
    //   138: aload #4
    //   140: ldc 'vnd.android.cursor.dir/contact'
    //   142: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   145: ifne -> 701
    //   148: aload #4
    //   150: ldc 'vnd.android.cursor.dir/phone_v2'
    //   152: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   155: ifeq -> 161
    //   158: goto -> 701
    //   161: aload_3
    //   162: ldc 'http'
    //   164: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   167: ifne -> 695
    //   170: aload_3
    //   171: ldc 'https'
    //   173: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   176: ifeq -> 182
    //   179: goto -> 695
    //   182: aload_1
    //   183: ldc 'android.media.action.IMAGE_CAPTURE'
    //   185: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   188: ifne -> 689
    //   191: aload_1
    //   192: ldc 'android.media.action.VIDEO_CAPTURE'
    //   194: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   197: ifne -> 689
    //   200: aload_1
    //   201: ldc 'android.media.action.VIDEO_CAMERA'
    //   203: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   206: ifne -> 689
    //   209: aload_1
    //   210: ldc 'android.media.action.STILL_IMAGE_CAMERA'
    //   212: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   215: ifne -> 689
    //   218: aload_1
    //   219: ldc 'com.oppo.action.CAMERA'
    //   221: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   224: ifeq -> 230
    //   227: goto -> 689
    //   230: aload #4
    //   232: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   235: ifne -> 254
    //   238: aload #4
    //   240: ldc 'image'
    //   242: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   245: ifeq -> 254
    //   248: ldc 'gallery'
    //   250: astore_0
    //   251: goto -> 716
    //   254: aload #4
    //   256: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   259: ifne -> 278
    //   262: aload #4
    //   264: ldc 'audio'
    //   266: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   269: ifeq -> 278
    //   272: ldc 'audio'
    //   274: astore_0
    //   275: goto -> 716
    //   278: aload #4
    //   280: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   283: ifne -> 302
    //   286: aload #4
    //   288: ldc 'video'
    //   290: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   293: ifeq -> 302
    //   296: ldc 'video'
    //   298: astore_0
    //   299: goto -> 716
    //   302: aload_3
    //   303: ldc 'mailto'
    //   305: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   308: ifeq -> 317
    //   311: ldc 'email'
    //   313: astore_0
    //   314: goto -> 716
    //   317: aload #4
    //   319: ldc 'text/plain'
    //   321: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   324: ifeq -> 333
    //   327: ldc 'txt'
    //   329: astore_0
    //   330: goto -> 716
    //   333: aload #4
    //   335: ldc 'application/pdf'
    //   337: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   340: ifeq -> 349
    //   343: ldc 'pdf'
    //   345: astore_0
    //   346: goto -> 716
    //   349: aload #4
    //   351: ldc 'application/msword'
    //   353: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   356: ifne -> 683
    //   359: aload #4
    //   361: ldc 'application/ms-word'
    //   363: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   366: ifne -> 683
    //   369: aload #4
    //   371: ldc 'application/vnd.ms-word'
    //   373: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   376: ifne -> 683
    //   379: aload #4
    //   381: ldc 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    //   383: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   386: ifne -> 683
    //   389: aload #4
    //   391: ldc 'application/vnd.openxmlformats-officedocument.wordprocessingml.template'
    //   393: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   396: ifne -> 683
    //   399: aload #4
    //   401: ldc 'application/vnd.ms-word.document.macroenabled.12'
    //   403: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   406: ifne -> 683
    //   409: aload #4
    //   411: ldc 'application/vnd.ms-word.template.macroenabled.12'
    //   413: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   416: ifeq -> 422
    //   419: goto -> 683
    //   422: aload #4
    //   424: ldc 'application/msexcel'
    //   426: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   429: ifne -> 677
    //   432: aload #4
    //   434: ldc 'application/ms-excel'
    //   436: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   439: ifne -> 677
    //   442: aload #4
    //   444: ldc 'application/vnd.ms-excel'
    //   446: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   449: ifne -> 677
    //   452: aload #4
    //   454: ldc 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    //   456: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   459: ifne -> 677
    //   462: aload #4
    //   464: ldc 'application/vnd.openxmlformats-officedocument.spreadsheetml.template'
    //   466: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   469: ifne -> 677
    //   472: aload #4
    //   474: ldc 'application/vnd.ms-excel.sheet.macroenabled.12'
    //   476: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   479: ifne -> 677
    //   482: aload #4
    //   484: ldc 'application/vnd.ms-excel.template.macroenabled.12'
    //   486: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   489: ifne -> 677
    //   492: aload #4
    //   494: ldc 'application/vnd.ms-excel.addin.macroenabled.12'
    //   496: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   499: ifne -> 677
    //   502: aload #4
    //   504: ldc 'application/vnd.ms-excel.sheet.binary.macroenabled.12'
    //   506: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   509: ifeq -> 515
    //   512: goto -> 677
    //   515: aload #4
    //   517: ldc 'application/mspowerpoint'
    //   519: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   522: ifne -> 671
    //   525: aload #4
    //   527: ldc 'application/ms-powerpoint'
    //   529: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   532: ifne -> 671
    //   535: aload #4
    //   537: ldc 'application/vnd.ms-powerpoint'
    //   539: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   542: ifne -> 671
    //   545: aload #4
    //   547: ldc 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
    //   549: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   552: ifne -> 671
    //   555: aload #4
    //   557: ldc 'application/vnd.openxmlformats-officedocument.presentationml.template'
    //   559: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   562: ifne -> 671
    //   565: aload #4
    //   567: ldc 'application/vnd.openxmlformats-officedocument.presentationml.slideshow'
    //   569: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   572: ifne -> 671
    //   575: aload #4
    //   577: ldc 'application/vnd.ms-powerpoint.presentation.macroenabled.12'
    //   579: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   582: ifne -> 671
    //   585: aload #4
    //   587: ldc 'application/vnd.ms-powerpoint.template.macroenabled.12'
    //   589: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   592: ifne -> 671
    //   595: aload #4
    //   597: ldc 'application/vnd.ms-powerpoint.slideshow.macroenabled.12'
    //   599: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   602: ifne -> 671
    //   605: aload #4
    //   607: ldc 'application/vnd.ms-powerpoint.addin.macroenabled.12'
    //   609: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   612: ifne -> 671
    //   615: aload #4
    //   617: ldc 'application/vnd.ms-powerpoint.slide.macroenabled.12'
    //   619: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   622: ifeq -> 628
    //   625: goto -> 671
    //   628: aload_3
    //   629: ldc 'market'
    //   631: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   634: ifeq -> 652
    //   637: aload_0
    //   638: ldc 'details'
    //   640: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   643: ifeq -> 652
    //   646: ldc 'market'
    //   648: astore_0
    //   649: goto -> 716
    //   652: aload #5
    //   654: astore_0
    //   655: aload #4
    //   657: ldc 'application/vnd.android.package-archive'
    //   659: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   662: ifeq -> 716
    //   665: ldc 'application'
    //   667: astore_0
    //   668: goto -> 716
    //   671: ldc 'ppt'
    //   673: astore_0
    //   674: goto -> 716
    //   677: ldc 'excel'
    //   679: astore_0
    //   680: goto -> 716
    //   683: ldc 'word'
    //   685: astore_0
    //   686: goto -> 716
    //   689: ldc 'camera'
    //   691: astore_0
    //   692: goto -> 716
    //   695: ldc 'browser'
    //   697: astore_0
    //   698: goto -> 716
    //   701: ldc 'contacts'
    //   703: astore_0
    //   704: goto -> 716
    //   707: ldc 'dialer'
    //   709: astore_0
    //   710: goto -> 716
    //   713: ldc 'message'
    //   715: astore_0
    //   716: new java/lang/StringBuilder
    //   719: dup
    //   720: invokespecial <init> : ()V
    //   723: astore #5
    //   725: aload #5
    //   727: ldc 'The MIME data type of this intent is '
    //   729: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   732: pop
    //   733: aload #5
    //   735: aload_0
    //   736: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   739: pop
    //   740: ldc 'OplusResolverIntentUtil'
    //   742: aload #5
    //   744: invokevirtual toString : ()Ljava/lang/String;
    //   747: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   750: pop
    //   751: aload_0
    //   752: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #66	-> 0
    //   #67	-> 5
    //   #68	-> 10
    //   #69	-> 15
    //   #70	-> 21
    //   #71	-> 26
    //   #72	-> 40
    //   #75	-> 44
    //   #76	-> 53
    //   #77	-> 62
    //   #80	-> 68
    //   #81	-> 86
    //   #85	-> 107
    //   #86	-> 125
    //   #90	-> 138
    //   #91	-> 148
    //   #95	-> 161
    //   #99	-> 182
    //   #100	-> 191
    //   #101	-> 200
    //   #102	-> 209
    //   #103	-> 218
    //   #107	-> 230
    //   #108	-> 248
    //   #111	-> 254
    //   #112	-> 272
    //   #115	-> 278
    //   #116	-> 296
    //   #119	-> 302
    //   #120	-> 311
    //   #123	-> 317
    //   #124	-> 327
    //   #127	-> 333
    //   #128	-> 343
    //   #131	-> 349
    //   #132	-> 359
    //   #133	-> 369
    //   #134	-> 379
    //   #136	-> 389
    //   #138	-> 399
    //   #139	-> 409
    //   #143	-> 422
    //   #144	-> 432
    //   #145	-> 442
    //   #146	-> 452
    //   #148	-> 462
    //   #150	-> 472
    //   #151	-> 482
    //   #152	-> 492
    //   #153	-> 502
    //   #157	-> 515
    //   #158	-> 525
    //   #159	-> 535
    //   #161	-> 545
    //   #163	-> 555
    //   #165	-> 565
    //   #167	-> 575
    //   #169	-> 585
    //   #171	-> 595
    //   #172	-> 605
    //   #173	-> 615
    //   #177	-> 628
    //   #178	-> 646
    //   #179	-> 652
    //   #180	-> 665
    //   #174	-> 671
    //   #154	-> 677
    //   #140	-> 683
    //   #104	-> 689
    //   #96	-> 695
    //   #92	-> 701
    //   #87	-> 707
    //   #82	-> 713
    //   #183	-> 716
    //   #184	-> 751
  }
  
  public static <T> boolean isInDataSet(Collection<T> paramCollection, T paramT) {
    if (paramCollection == null || paramCollection.isEmpty())
      return false; 
    if (paramT == null)
      return false; 
    return paramCollection.contains(paramT);
  }
}
