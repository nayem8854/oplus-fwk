package com.oplus.util;

import android.content.Context;
import android.content.Intent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class OplusKillMethodProvider {
  private static final String ACTION_CLEAR_SPEC_APP = "oppo.intent.action.REQUEST_CLEAR_SPEC_APP";
  
  private static final String ACTION_ONEKEY_CLEAR = "oppo.intent.action.REQUEST_APP_CLEAN_RUNNING";
  
  private static final String CALLED_PACKAGE = "com.coloros.athena";
  
  private static final String CALLER_PACKAGE = "caller_package";
  
  private static final String FILTER_APP_LIST = "filterapplist";
  
  private static final String LIST = "list";
  
  private static final String PID = "pid";
  
  private static final String P_NAME = "p_name";
  
  private static final String REASON = "reason";
  
  private static final int REQUEST_TYPE_KILL = 12;
  
  private static final int REQUEST_TYPE_KILL_OR_STOP = 11;
  
  private static final int REQUEST_TYPE_REMOVE_TASK = 14;
  
  private static final int REQUEST_TYPE_STOP = 13;
  
  private static final String TAG = "OplusKillMethodProvider";
  
  private static final String TYPE = "type";
  
  public static void killProcess(Context paramContext, int paramInt, String paramString1, String paramString2) {
    try {
      Intent intent = new Intent();
      this("oppo.intent.action.REQUEST_CLEAR_SPEC_APP");
      intent.setPackage("com.coloros.athena");
      intent.putExtra("pid", paramInt);
      intent.putExtra("p_name", paramString1);
      intent.putExtra("caller_package", paramContext.getPackageName());
      intent.putExtra("reason", paramString2);
      intent.putExtra("type", 12);
      paramContext.startService(intent);
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public static void killPackage(Context paramContext, String paramString1, String paramString2) {
    try {
      Intent intent = new Intent();
      this("oppo.intent.action.REQUEST_CLEAR_SPEC_APP");
      intent.setPackage("com.coloros.athena");
      intent.putExtra("p_name", paramString1);
      intent.putExtra("caller_package", paramContext.getPackageName());
      intent.putExtra("reason", paramString2);
      intent.putExtra("type", 12);
      paramContext.startService(intent);
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public static void killProcessList(Context paramContext, List<KillInfo> paramList, String paramString) {
    if (paramList == null || paramList.isEmpty())
      return; 
    try {
      ArrayList<String> arrayList = new ArrayList();
      this();
      for (KillInfo killInfo : paramList)
        arrayList.add(killInfo.getInfoString()); 
      Intent intent = new Intent();
      this("oppo.intent.action.REQUEST_CLEAR_SPEC_APP");
      intent.setPackage("com.coloros.athena");
      intent.putStringArrayListExtra("list", arrayList);
      intent.putExtra("caller_package", paramContext.getPackageName());
      intent.putExtra("reason", paramString);
      intent.putExtra("type", 12);
      paramContext.startService(intent);
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public static void stopPackageConfig(Context paramContext, List<String> paramList, String paramString) {
    try {
      ArrayList arrayList = new ArrayList();
      this((Collection)paramList);
      Intent intent = new Intent();
      this("oppo.intent.action.REQUEST_APP_CLEAN_RUNNING");
      intent.setPackage("com.coloros.athena");
      intent.putExtra("caller_package", paramContext.getPackageName());
      intent.putExtra("filterapplist", arrayList);
      intent.putExtra("reason", paramString);
      paramContext.startService(intent);
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public static class KillInfo {
    public int pid;
    
    public String procName;
    
    public String toString() {
      return getInfoString();
    }
    
    String getInfoString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this.pid);
      stringBuilder.append("|");
      stringBuilder.append(this.procName);
      return stringBuilder.toString();
    }
  }
}
