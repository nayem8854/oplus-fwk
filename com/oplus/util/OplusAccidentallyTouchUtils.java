package com.oplus.util;

import android.app.OplusActivityManager;
import android.content.Context;
import android.content.pm.OplusPackageManager;
import android.content.res.Configuration;
import android.os.FileObserver;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Xml;
import android.view.MotionEvent;
import android.view.View;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;

public class OplusAccidentallyTouchUtils {
  private boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static final boolean PROPERTY_ENABLE = SystemProperties.getBoolean("debug.accidentally.touch", true);
  
  private FileObserverPolicy mAccidentallyTouchFileObserver = null;
  
  private final Object mAccidentallyTouchLock = new Object();
  
  private static OplusAccidentallyTouchData mTouchData = null;
  
  private static OplusAccidentallyTouchUtils mTouchUtils = null;
  
  private List<String> mXmlEdgeList = null;
  
  private List<String> mXmlSingleList = null;
  
  private List<String> mXmlMultiList = null;
  
  private List<String> mXmlWhiteList = null;
  
  private ArrayList<String> mBezelList = new ArrayList<>();
  
  private boolean mBezelEnable = true;
  
  private String mBezelArea = "7";
  
  private ArrayList<String> mDefaultList = new ArrayList<>(Arrays.asList(new String[] { "com.tencent.mm" }));
  
  private static final String ACCIDENTALLYTOUCH_FILE = "/data/oppo/coloros/oppoguardelf/sys_accidentally_touch_config_list.xml";
  
  private static final String ACCIDENTALLYTOUCH_PATH = "/data/oppo/coloros/oppoguardelf";
  
  private static final int DENSITY_XXHIGH = 3;
  
  private static final int OFFSET_1 = 5;
  
  private static final int OFFSET_2 = 80;
  
  private static final String TAG = "ColorAccidentallyTouch";
  
  private static final String TAG_BEZEL = "bezel";
  
  private static final String TAG_BEZEL_AREA = "bezel_area";
  
  private static final String TAG_BEZEL_ENABLE = "bezel_enable";
  
  private static final String TAG_EDGE_ENABLE = "edge_enable";
  
  private static final String TAG_EDGE_PKG = "edge_pkg";
  
  private static final String TAG_EDGE_T = "edge_t";
  
  private static final String TAG_EDGE_T1 = "edge_t1";
  
  private static final String TAG_EDGE_T2 = "edge_t2";
  
  private static final String TAG_ENABLE = "enable_accidentallytouch";
  
  private static final String TAG_LEFT_OFFSET = "left_offset";
  
  private static final String TAG_POINT_LEFT_OFFSET = "point_left_offset";
  
  private static final String TAG_POINT_RIGHT_OFFSET = "point_right_offset";
  
  private static final String TAG_RIGHT_OFFSET = "right_offset";
  
  private int mActivePointerId;
  
  private Context mContext;
  
  private int mDisplayWidth;
  
  private boolean mEdgeEnable;
  
  private ArrayList<String> mEdgeList;
  
  private int mEdgeT;
  
  private int mEdgeT1;
  
  private int mEdgeT2;
  
  private boolean mEnableAccidentallyTouch;
  
  private boolean mEnableMultiSence;
  
  private boolean mEnableSingleSence;
  
  private MotionEvent mExtraEvent;
  
  private boolean mIsEdgePkg;
  
  private boolean mIsMultiSence;
  
  private boolean mIsSingleSence;
  
  private boolean mIsWhiteApp;
  
  private int mLeftOffset;
  
  private ArrayList<String> mMultiList;
  
  private boolean mNeedExtraEvent;
  
  private int mPointLeftOffset;
  
  private int mPointRightOffset;
  
  private int mRightOffset;
  
  private int mScrapPointerId;
  
  private ArrayList<String> mSingleList;
  
  private boolean mSmallScreenMode;
  
  private ArrayList<String> mWhiteList;
  
  private String mXmlEdgeEnable;
  
  private String mXmlEdgeT;
  
  private String mXmlEdgeT1;
  
  private String mXmlEdgeT2;
  
  private String mXmlEnable;
  
  private String mXmlLeftOffset;
  
  private String mXmlPointLeftOffset;
  
  private String mXmlPointRightOffset;
  
  private String mXmlRightOffset;
  
  public static OplusAccidentallyTouchUtils getInstance() {
    if (mTouchUtils == null)
      mTouchUtils = new OplusAccidentallyTouchUtils(); 
    return mTouchUtils;
  }
  
  public void init() {
    initDir();
    initFileObserver();
    if (mTouchData == null)
      mTouchData = new OplusAccidentallyTouchData(); 
    this.mXmlSingleList = mTouchData.getSingleTouchList();
    this.mXmlMultiList = mTouchData.getMultiTouchList();
    this.mXmlWhiteList = mTouchData.getTouchWhiteList();
    this.mXmlEdgeList = mTouchData.getEdgeList();
    synchronized (this.mAccidentallyTouchLock) {
      readConfigFile();
      return;
    } 
  }
  
  public OplusAccidentallyTouchData getTouchData() {
    if (mTouchData == null)
      mTouchData = new OplusAccidentallyTouchData(); 
    return mTouchData;
  }
  
  private void initDir() {
    File file1 = new File("/data/oppo/coloros/oppoguardelf");
    File file2 = new File("/data/oppo/coloros/oppoguardelf/sys_accidentally_touch_config_list.xml");
    try {
      if (!file1.exists())
        file1.mkdirs(); 
      if (!file2.exists())
        file2.createNewFile(); 
    } catch (IOException iOException) {
      iOException.printStackTrace();
    } 
    changeModFile("/data/oppo/coloros/oppoguardelf/sys_accidentally_touch_config_list.xml");
  }
  
  private void changeModFile(String paramString) {
    try {
      File file = new File();
      this(paramString);
      HashSet<PosixFilePermission> hashSet = new HashSet();
      this();
      hashSet.add(PosixFilePermission.OWNER_READ);
      hashSet.add(PosixFilePermission.OWNER_WRITE);
      hashSet.add(PosixFilePermission.OWNER_EXECUTE);
      hashSet.add(PosixFilePermission.GROUP_READ);
      hashSet.add(PosixFilePermission.GROUP_WRITE);
      hashSet.add(PosixFilePermission.OTHERS_READ);
      hashSet.add(PosixFilePermission.OTHERS_WRITE);
      Path path = Paths.get(file.getAbsolutePath(), new String[0]);
      Files.setPosixFilePermissions(path, hashSet);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" ");
      stringBuilder.append(exception);
      Log.w("ColorAccidentallyTouch", stringBuilder.toString());
    } 
  }
  
  private void readConfigFile() {
    File file = new File("/data/oppo/coloros/oppoguardelf/sys_accidentally_touch_config_list.xml");
    if (!file.exists())
      return; 
    this.mXmlSingleList.clear();
    this.mXmlMultiList.clear();
    this.mXmlWhiteList.clear();
    this.mXmlEdgeList.clear();
    this.mBezelList.clear();
    FileInputStream fileInputStream1 = null, fileInputStream2 = null;
    FileInputStream fileInputStream3 = fileInputStream2, fileInputStream4 = fileInputStream1;
    try {
      int i;
      FileInputStream fileInputStream = new FileInputStream();
      fileInputStream3 = fileInputStream2;
      fileInputStream4 = fileInputStream1;
      this(file);
      fileInputStream3 = fileInputStream;
      fileInputStream4 = fileInputStream;
      XmlPullParser xmlPullParser = Xml.newPullParser();
      fileInputStream3 = fileInputStream;
      fileInputStream4 = fileInputStream;
      xmlPullParser.setInput(fileInputStream, null);
      do {
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        i = xmlPullParser.next();
        if (i != 2)
          continue; 
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        String str = xmlPullParser.getName();
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        boolean bool = "o".equals(str);
        if (bool) {
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          String str1 = xmlPullParser.getAttributeValue(null, "att");
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          if (this.DEBUG) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            StringBuilder stringBuilder = new StringBuilder();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append("single-toush list : ");
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append(str1);
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            Log.d("ColorAccidentallyTouch", stringBuilder.toString());
          } 
          if (str1 != null) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this.mXmlSingleList.add(str1);
          } 
        } 
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        if ("p".equals(str)) {
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          String str1 = xmlPullParser.getAttributeValue(null, "att");
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          if (this.DEBUG) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            StringBuilder stringBuilder = new StringBuilder();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append("multi-touch list : ");
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append(str1);
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            Log.d("ColorAccidentallyTouch", stringBuilder.toString());
          } 
          if (str1 != null) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this.mXmlMultiList.add(str1);
          } 
        } 
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        if ("w".equals(str)) {
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          String str1 = xmlPullParser.getAttributeValue(null, "att");
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          if (this.DEBUG) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            StringBuilder stringBuilder = new StringBuilder();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append("white list : ");
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append(str1);
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            Log.d("ColorAccidentallyTouch", stringBuilder.toString());
          } 
          if (str1 != null) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this.mXmlWhiteList.add(str1);
          } 
        } 
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        if ("bezel_enable".equals(str)) {
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          String str1 = xmlPullParser.nextText();
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          if (this.DEBUG) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            StringBuilder stringBuilder = new StringBuilder();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append("bezel enable : ");
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append(str1);
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            Log.d("ColorAccidentallyTouch", stringBuilder.toString());
          } 
          if (str1 != null) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            if ("true".equals(str1)) {
              fileInputStream3 = fileInputStream;
              fileInputStream4 = fileInputStream;
              this.mBezelEnable = true;
            } else {
              fileInputStream3 = fileInputStream;
              fileInputStream4 = fileInputStream;
              this.mBezelEnable = false;
            } 
          } 
        } 
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        if ("bezel".equals(str)) {
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          String str1 = xmlPullParser.getAttributeValue(null, "att");
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          if (this.DEBUG) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            StringBuilder stringBuilder = new StringBuilder();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append("bezel list : ");
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append(str1);
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            Log.d("ColorAccidentallyTouch", stringBuilder.toString());
          } 
          if (str1 != null) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this.mBezelList.add(str1);
          } 
        } 
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        if ("bezel_area".equals(str)) {
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          this.mBezelArea = xmlPullParser.nextText();
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          if (this.DEBUG) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            StringBuilder stringBuilder = new StringBuilder();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append("bezel_area = ");
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append(this.mBezelArea);
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            Log.d("ColorAccidentallyTouch", stringBuilder.toString());
          } 
        } 
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        if ("enable_accidentallytouch".equals(str)) {
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          this.mXmlEnable = xmlPullParser.nextText();
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          if (this.DEBUG) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            StringBuilder stringBuilder = new StringBuilder();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append("mXmlEnable = ");
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append(this.mXmlEnable);
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            Log.d("ColorAccidentallyTouch", stringBuilder.toString());
          } 
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          mTouchData.setAccidentalltyTouchEnable(this.mXmlEnable);
        } 
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        if ("left_offset".equals(str)) {
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          this.mXmlLeftOffset = xmlPullParser.nextText();
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          if (this.DEBUG) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            StringBuilder stringBuilder = new StringBuilder();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append("mXmlLeftOffset = ");
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append(this.mXmlLeftOffset);
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            Log.d("ColorAccidentallyTouch", stringBuilder.toString());
          } 
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          mTouchData.setLeftOffset(this.mXmlLeftOffset);
        } 
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        if ("right_offset".equals(str)) {
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          this.mXmlRightOffset = xmlPullParser.nextText();
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          if (this.DEBUG) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            StringBuilder stringBuilder = new StringBuilder();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append("mXmlRightOffset = ");
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append(this.mXmlRightOffset);
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            Log.d("ColorAccidentallyTouch", stringBuilder.toString());
          } 
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          mTouchData.setRightOffset(this.mXmlRightOffset);
        } 
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        if ("point_left_offset".equals(str)) {
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          this.mXmlPointLeftOffset = xmlPullParser.nextText();
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          if (this.DEBUG) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            StringBuilder stringBuilder = new StringBuilder();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append("mXmlPointLeftOffset = ");
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append(this.mXmlPointLeftOffset);
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            Log.d("ColorAccidentallyTouch", stringBuilder.toString());
          } 
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          mTouchData.setPointLeftOffset(this.mXmlPointLeftOffset);
        } 
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        if ("point_right_offset".equals(str)) {
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          this.mXmlPointRightOffset = xmlPullParser.nextText();
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          if (this.DEBUG) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            StringBuilder stringBuilder = new StringBuilder();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append("mXmlPointRightOffset = ");
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append(this.mXmlPointRightOffset);
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            Log.d("ColorAccidentallyTouch", stringBuilder.toString());
          } 
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          mTouchData.setPointRightOffset(this.mXmlPointRightOffset);
        } 
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        if ("edge_enable".equals(str)) {
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          this.mXmlEdgeEnable = xmlPullParser.nextText();
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          if (this.DEBUG) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            StringBuilder stringBuilder = new StringBuilder();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append("mXmlEdgeEnable = ");
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append(this.mXmlEdgeEnable);
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            Log.d("ColorAccidentallyTouch", stringBuilder.toString());
          } 
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          mTouchData.setEdgeEnable(this.mXmlEdgeEnable);
        } 
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        if ("edge_pkg".equals(str)) {
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          String str1 = xmlPullParser.nextText();
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          if (this.DEBUG) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            StringBuilder stringBuilder = new StringBuilder();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append("edge list: ");
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append(str1);
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            Log.d("ColorAccidentallyTouch", stringBuilder.toString());
          } 
          if (str1 != null) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this.mXmlEdgeList.add(str1);
          } 
        } 
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        if ("edge_t".equals(str)) {
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          this.mXmlEdgeT = xmlPullParser.nextText();
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          if (this.DEBUG) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            StringBuilder stringBuilder = new StringBuilder();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append("mXmlEdgeT = ");
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append(this.mXmlEdgeT);
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            Log.d("ColorAccidentallyTouch", stringBuilder.toString());
          } 
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          mTouchData.setEdgeT(this.mXmlEdgeT);
        } 
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        if ("edge_t1".equals(str)) {
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          this.mXmlEdgeT1 = xmlPullParser.nextText();
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          if (this.DEBUG) {
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            StringBuilder stringBuilder = new StringBuilder();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            this();
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append("mXmlEdgeT1 = ");
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            stringBuilder.append(this.mXmlEdgeT1);
            fileInputStream3 = fileInputStream;
            fileInputStream4 = fileInputStream;
            Log.d("ColorAccidentallyTouch", stringBuilder.toString());
          } 
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          mTouchData.setEdgeT1(this.mXmlEdgeT1);
        } 
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        if (!"edge_t2".equals(str))
          continue; 
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        this.mXmlEdgeT2 = xmlPullParser.nextText();
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        if (this.DEBUG) {
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          StringBuilder stringBuilder = new StringBuilder();
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          this();
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          stringBuilder.append("mXmlEdgeT2 = ");
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          stringBuilder.append(this.mXmlEdgeT2);
          fileInputStream3 = fileInputStream;
          fileInputStream4 = fileInputStream;
          Log.d("ColorAccidentallyTouch", stringBuilder.toString());
        } 
        fileInputStream3 = fileInputStream;
        fileInputStream4 = fileInputStream;
        mTouchData.setEdgeT2(this.mXmlEdgeT2);
      } while (i != 1);
      try {
        fileInputStream.close();
      } catch (IOException iOException) {
        iOException.printStackTrace();
      } 
    } catch (Exception exception) {
      fileInputStream3 = fileInputStream4;
      exception.printStackTrace();
      if (fileInputStream4 != null)
        fileInputStream4.close(); 
    } finally {}
  }
  
  private void initFileObserver() {
    FileObserverPolicy fileObserverPolicy = new FileObserverPolicy("/data/oppo/coloros/oppoguardelf/sys_accidentally_touch_config_list.xml");
    fileObserverPolicy.startWatching();
  }
  
  private class FileObserverPolicy extends FileObserver {
    private String focusPath;
    
    final OplusAccidentallyTouchUtils this$0;
    
    public FileObserverPolicy(String param1String) {
      super(param1String, 8);
      this.focusPath = param1String;
    }
    
    public void onEvent(int param1Int, String param1String) {
      if (OplusAccidentallyTouchUtils.this.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("FileObserver:");
        stringBuilder.append(param1Int);
        stringBuilder.append(",");
        stringBuilder.append(param1String);
        Log.d("ColorAccidentallyTouch", stringBuilder.toString());
      } 
      if (param1Int == 8)
        synchronized (OplusAccidentallyTouchUtils.this.mAccidentallyTouchLock) {
          Log.d("ColorAccidentallyTouch", "readConfigFile");
          OplusAccidentallyTouchUtils.this.readConfigFile();
        }  
    }
  }
  
  public boolean isBezelEnable(String paramString) {
    if (!this.mBezelEnable || this.mBezelList == null || TextUtils.isEmpty(paramString))
      return false; 
    for (byte b = 0; b < this.mBezelList.size(); b++) {
      if (paramString.contains(this.mBezelList.get(b)))
        return true; 
    } 
    return false;
  }
  
  public String getBezelArea() {
    return this.mBezelArea;
  }
  
  private OplusAccidentallyTouchUtils() {
    this.mEnableAccidentallyTouch = true;
    this.mLeftOffset = 5;
    this.mRightOffset = 5;
    this.mPointLeftOffset = 80;
    this.mPointRightOffset = 80;
    this.mEdgeEnable = true;
    this.mEdgeT = 10;
    this.mEdgeT1 = 10;
    this.mEdgeT2 = 30;
    this.mEdgeList = new ArrayList<>();
    this.mIsEdgePkg = false;
    this.mSingleList = new ArrayList<>();
    this.mMultiList = new ArrayList<>();
    this.mWhiteList = new ArrayList<>();
    this.mEnableSingleSence = true;
    this.mEnableMultiSence = true;
    this.mIsSingleSence = false;
    this.mIsMultiSence = false;
    this.mIsWhiteApp = false;
    this.mSmallScreenMode = false;
    this.mDisplayWidth = 1080;
  }
  
  public MotionEvent getExtraEvent() {
    if (this.mNeedExtraEvent)
      return this.mExtraEvent; 
    return null;
  }
  
  public MotionEvent getMotionEvent(MotionEvent paramMotionEvent, View paramView) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_3
    //   2: aload_0
    //   3: iconst_0
    //   4: putfield mNeedExtraEvent : Z
    //   7: aload_1
    //   8: invokevirtual getActionMasked : ()I
    //   11: istore #4
    //   13: iload #4
    //   15: ifeq -> 1013
    //   18: iload #4
    //   20: iconst_1
    //   21: if_icmpeq -> 882
    //   24: iload #4
    //   26: iconst_2
    //   27: if_icmpeq -> 703
    //   30: iload #4
    //   32: iconst_5
    //   33: if_icmpeq -> 241
    //   36: iload #4
    //   38: bipush #6
    //   40: if_icmpeq -> 49
    //   43: aload_1
    //   44: astore #5
    //   46: goto -> 1182
    //   49: aload_1
    //   50: invokevirtual getActionIndex : ()I
    //   53: istore #4
    //   55: aload_0
    //   56: getfield mIsMultiSence : Z
    //   59: ifeq -> 121
    //   62: aload_0
    //   63: getfield mScrapPointerId : I
    //   66: aload_1
    //   67: iload #4
    //   69: invokevirtual getPointerId : (I)I
    //   72: if_icmpne -> 121
    //   75: aload_0
    //   76: iconst_0
    //   77: putfield mIsMultiSence : Z
    //   80: aload_0
    //   81: getfield DEBUG : Z
    //   84: ifeq -> 119
    //   87: new java/lang/StringBuilder
    //   90: dup
    //   91: invokespecial <init> : ()V
    //   94: astore_2
    //   95: aload_2
    //   96: ldc_w 'accidentally touch scrap 2: '
    //   99: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   102: pop
    //   103: aload_2
    //   104: aload_1
    //   105: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   108: pop
    //   109: ldc 'ColorAccidentallyTouch'
    //   111: aload_2
    //   112: invokevirtual toString : ()Ljava/lang/String;
    //   115: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   118: pop
    //   119: aconst_null
    //   120: areturn
    //   121: aload_1
    //   122: astore #5
    //   124: aload_0
    //   125: getfield mIsMultiSence : Z
    //   128: ifeq -> 1182
    //   131: aload_0
    //   132: getfield DEBUG : Z
    //   135: ifeq -> 170
    //   138: new java/lang/StringBuilder
    //   141: dup
    //   142: invokespecial <init> : ()V
    //   145: astore_2
    //   146: aload_2
    //   147: ldc_w 'original event: '
    //   150: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   153: pop
    //   154: aload_2
    //   155: aload_1
    //   156: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   159: pop
    //   160: ldc 'ColorAccidentallyTouch'
    //   162: aload_2
    //   163: invokevirtual toString : ()Ljava/lang/String;
    //   166: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   169: pop
    //   170: aload_1
    //   171: aload_1
    //   172: invokevirtual getPointerIdBits : ()I
    //   175: aload_0
    //   176: aload_1
    //   177: invokespecial getFirstIds : (Landroid/view/MotionEvent;)I
    //   180: isub
    //   181: invokevirtual split : (I)Landroid/view/MotionEvent;
    //   184: astore_1
    //   185: aload_1
    //   186: aload_1
    //   187: invokevirtual getEventTime : ()J
    //   190: invokevirtual setDownTime : (J)V
    //   193: aload_1
    //   194: astore #5
    //   196: aload_0
    //   197: getfield DEBUG : Z
    //   200: ifeq -> 1182
    //   203: new java/lang/StringBuilder
    //   206: dup
    //   207: invokespecial <init> : ()V
    //   210: astore_2
    //   211: aload_2
    //   212: ldc_w 'accidentally touch dispatch 2: '
    //   215: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   218: pop
    //   219: aload_2
    //   220: aload_1
    //   221: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   224: pop
    //   225: ldc 'ColorAccidentallyTouch'
    //   227: aload_2
    //   228: invokevirtual toString : ()Ljava/lang/String;
    //   231: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   234: pop
    //   235: aload_1
    //   236: astore #5
    //   238: goto -> 1182
    //   241: aload_1
    //   242: iconst_0
    //   243: invokevirtual getX : (I)F
    //   246: f2i
    //   247: istore #4
    //   249: aload_0
    //   250: iconst_0
    //   251: putfield mIsMultiSence : Z
    //   254: iload #4
    //   256: iflt -> 268
    //   259: iload #4
    //   261: aload_0
    //   262: getfield mPointLeftOffset : I
    //   265: if_icmple -> 290
    //   268: aload_0
    //   269: getfield mDisplayWidth : I
    //   272: istore_3
    //   273: iload #4
    //   275: iload_3
    //   276: aload_0
    //   277: getfield mPointRightOffset : I
    //   280: isub
    //   281: if_icmplt -> 296
    //   284: iload #4
    //   286: iload_3
    //   287: if_icmpgt -> 296
    //   290: iconst_1
    //   291: istore #6
    //   293: goto -> 299
    //   296: iconst_0
    //   297: istore #6
    //   299: aload_0
    //   300: getfield DEBUG : Z
    //   303: ifeq -> 453
    //   306: new java/lang/StringBuilder
    //   309: dup
    //   310: invokespecial <init> : ()V
    //   313: astore #5
    //   315: aload #5
    //   317: ldc_w 'original event: '
    //   320: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   323: pop
    //   324: aload #5
    //   326: aload_1
    //   327: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   330: pop
    //   331: ldc 'ColorAccidentallyTouch'
    //   333: aload #5
    //   335: invokevirtual toString : ()Ljava/lang/String;
    //   338: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   341: pop
    //   342: new java/lang/StringBuilder
    //   345: dup
    //   346: invokespecial <init> : ()V
    //   349: astore #5
    //   351: aload #5
    //   353: ldc_w 'inEdge:'
    //   356: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   359: pop
    //   360: aload #5
    //   362: iload #6
    //   364: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   367: pop
    //   368: aload #5
    //   370: ldc_w ', actionX:'
    //   373: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   376: pop
    //   377: aload #5
    //   379: iload #4
    //   381: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   384: pop
    //   385: aload #5
    //   387: ldc_w ', mDisplayWidth:'
    //   390: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   393: pop
    //   394: aload #5
    //   396: aload_0
    //   397: getfield mDisplayWidth : I
    //   400: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   403: pop
    //   404: aload #5
    //   406: ldc_w ', mPointLeftOffset:'
    //   409: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   412: pop
    //   413: aload #5
    //   415: aload_0
    //   416: getfield mPointLeftOffset : I
    //   419: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   422: pop
    //   423: aload #5
    //   425: ldc_w ', mPointRightOffset:'
    //   428: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   431: pop
    //   432: aload #5
    //   434: aload_0
    //   435: getfield mPointRightOffset : I
    //   438: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   441: pop
    //   442: ldc 'ColorAccidentallyTouch'
    //   444: aload #5
    //   446: invokevirtual toString : ()Ljava/lang/String;
    //   449: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   452: pop
    //   453: iload #6
    //   455: ifne -> 468
    //   458: aload_1
    //   459: astore #5
    //   461: aload_0
    //   462: getfield mIsWhiteApp : Z
    //   465: ifeq -> 1182
    //   468: aload_0
    //   469: getfield mEnableMultiSence : Z
    //   472: ifne -> 481
    //   475: aload_1
    //   476: astore #5
    //   478: goto -> 1182
    //   481: aload_1
    //   482: invokevirtual getPointerCount : ()I
    //   485: iconst_1
    //   486: if_icmpgt -> 495
    //   489: aload_1
    //   490: astore #5
    //   492: goto -> 1182
    //   495: aload_1
    //   496: invokevirtual getActionIndex : ()I
    //   499: ifne -> 527
    //   502: aload_1
    //   503: astore #5
    //   505: aload_0
    //   506: getfield DEBUG : Z
    //   509: ifeq -> 1182
    //   512: ldc 'ColorAccidentallyTouch'
    //   514: ldc_w 'the action index is 0, break'
    //   517: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   520: pop
    //   521: aload_1
    //   522: astore #5
    //   524: goto -> 1182
    //   527: aload_0
    //   528: iconst_0
    //   529: putfield mIsSingleSence : Z
    //   532: aload_0
    //   533: iconst_1
    //   534: putfield mIsMultiSence : Z
    //   537: aload_1
    //   538: invokevirtual getPointerCount : ()I
    //   541: iconst_2
    //   542: if_icmpne -> 632
    //   545: aload_0
    //   546: aload_1
    //   547: iconst_0
    //   548: invokevirtual getPointerId : (I)I
    //   551: putfield mScrapPointerId : I
    //   554: aload_1
    //   555: invokestatic obtain : (Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    //   558: astore #7
    //   560: aload #7
    //   562: iconst_3
    //   563: invokevirtual setAction : (I)V
    //   566: aload_0
    //   567: getfield DEBUG : Z
    //   570: ifeq -> 610
    //   573: new java/lang/StringBuilder
    //   576: dup
    //   577: invokespecial <init> : ()V
    //   580: astore #5
    //   582: aload #5
    //   584: ldc_w 'accidentally touch add: '
    //   587: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   590: pop
    //   591: aload #5
    //   593: aload #7
    //   595: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   598: pop
    //   599: ldc 'ColorAccidentallyTouch'
    //   601: aload #5
    //   603: invokevirtual toString : ()Ljava/lang/String;
    //   606: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   609: pop
    //   610: aload_2
    //   611: ifnull -> 621
    //   614: aload_2
    //   615: aload #7
    //   617: invokevirtual dispatchPointerEvent : (Landroid/view/MotionEvent;)Z
    //   620: pop
    //   621: aload_0
    //   622: iconst_1
    //   623: putfield mNeedExtraEvent : Z
    //   626: aload_0
    //   627: aload #7
    //   629: putfield mExtraEvent : Landroid/view/MotionEvent;
    //   632: aload_1
    //   633: aload_1
    //   634: invokevirtual getPointerIdBits : ()I
    //   637: aload_0
    //   638: aload_1
    //   639: invokespecial getFirstIds : (Landroid/view/MotionEvent;)I
    //   642: isub
    //   643: invokevirtual split : (I)Landroid/view/MotionEvent;
    //   646: astore_1
    //   647: aload_1
    //   648: aload_1
    //   649: invokevirtual getEventTime : ()J
    //   652: invokevirtual setDownTime : (J)V
    //   655: aload_1
    //   656: astore #5
    //   658: aload_0
    //   659: getfield DEBUG : Z
    //   662: ifeq -> 1182
    //   665: new java/lang/StringBuilder
    //   668: dup
    //   669: invokespecial <init> : ()V
    //   672: astore_2
    //   673: aload_2
    //   674: ldc_w 'accidentally touch dispatch 1: '
    //   677: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   680: pop
    //   681: aload_2
    //   682: aload_1
    //   683: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   686: pop
    //   687: ldc 'ColorAccidentallyTouch'
    //   689: aload_2
    //   690: invokevirtual toString : ()Ljava/lang/String;
    //   693: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   696: pop
    //   697: aload_1
    //   698: astore #5
    //   700: goto -> 1182
    //   703: aload_0
    //   704: getfield mIsSingleSence : Z
    //   707: ifeq -> 808
    //   710: aload_1
    //   711: invokevirtual getRawX : ()F
    //   714: f2i
    //   715: istore #4
    //   717: iload #4
    //   719: aload_0
    //   720: getfield mLeftOffset : I
    //   723: if_icmplt -> 746
    //   726: iload #4
    //   728: aload_0
    //   729: getfield mDisplayWidth : I
    //   732: aload_0
    //   733: getfield mRightOffset : I
    //   736: isub
    //   737: if_icmpgt -> 746
    //   740: iconst_1
    //   741: istore #4
    //   743: goto -> 749
    //   746: iconst_0
    //   747: istore #4
    //   749: iload #4
    //   751: ifeq -> 806
    //   754: aload_0
    //   755: iconst_0
    //   756: putfield mIsSingleSence : Z
    //   759: aload_1
    //   760: iconst_0
    //   761: invokevirtual setAction : (I)V
    //   764: aload_0
    //   765: getfield DEBUG : Z
    //   768: ifeq -> 808
    //   771: new java/lang/StringBuilder
    //   774: dup
    //   775: invokespecial <init> : ()V
    //   778: astore_2
    //   779: aload_2
    //   780: ldc_w 'accidentally touch dispatch 3: '
    //   783: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   786: pop
    //   787: aload_2
    //   788: aload_1
    //   789: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   792: pop
    //   793: ldc 'ColorAccidentallyTouch'
    //   795: aload_2
    //   796: invokevirtual toString : ()Ljava/lang/String;
    //   799: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   802: pop
    //   803: goto -> 808
    //   806: aconst_null
    //   807: areturn
    //   808: aload_1
    //   809: astore #5
    //   811: aload_0
    //   812: getfield mIsMultiSence : Z
    //   815: ifeq -> 1182
    //   818: aload_1
    //   819: invokevirtual getPointerCount : ()I
    //   822: iconst_1
    //   823: if_icmple -> 855
    //   826: aload_1
    //   827: aload_1
    //   828: invokevirtual getPointerIdBits : ()I
    //   831: aload_0
    //   832: aload_1
    //   833: invokespecial getFirstIds : (Landroid/view/MotionEvent;)I
    //   836: isub
    //   837: invokevirtual split : (I)Landroid/view/MotionEvent;
    //   840: astore #5
    //   842: aload #5
    //   844: aload #5
    //   846: invokevirtual getEventTime : ()J
    //   849: invokevirtual setDownTime : (J)V
    //   852: goto -> 1182
    //   855: aload_1
    //   856: invokevirtual getActionIndex : ()I
    //   859: istore #4
    //   861: aload_0
    //   862: getfield mScrapPointerId : I
    //   865: aload_1
    //   866: iload #4
    //   868: invokevirtual getPointerId : (I)I
    //   871: if_icmpne -> 876
    //   874: aconst_null
    //   875: areturn
    //   876: aload_1
    //   877: astore #5
    //   879: goto -> 1182
    //   882: aload_0
    //   883: getfield mIsSingleSence : Z
    //   886: ifeq -> 935
    //   889: aload_0
    //   890: iconst_0
    //   891: putfield mIsSingleSence : Z
    //   894: aload_0
    //   895: getfield DEBUG : Z
    //   898: ifeq -> 933
    //   901: new java/lang/StringBuilder
    //   904: dup
    //   905: invokespecial <init> : ()V
    //   908: astore_2
    //   909: aload_2
    //   910: ldc_w 'accidentally touch scrap 3: '
    //   913: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   916: pop
    //   917: aload_2
    //   918: aload_1
    //   919: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   922: pop
    //   923: ldc 'ColorAccidentallyTouch'
    //   925: aload_2
    //   926: invokevirtual toString : ()Ljava/lang/String;
    //   929: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   932: pop
    //   933: aconst_null
    //   934: areturn
    //   935: aload_1
    //   936: astore #5
    //   938: aload_0
    //   939: getfield mIsMultiSence : Z
    //   942: ifeq -> 1182
    //   945: aload_1
    //   946: invokevirtual getActionIndex : ()I
    //   949: istore #4
    //   951: aload_1
    //   952: astore #5
    //   954: aload_0
    //   955: getfield mScrapPointerId : I
    //   958: aload_1
    //   959: iload #4
    //   961: invokevirtual getPointerId : (I)I
    //   964: if_icmpne -> 1182
    //   967: aload_0
    //   968: iconst_0
    //   969: putfield mIsMultiSence : Z
    //   972: aload_0
    //   973: getfield DEBUG : Z
    //   976: ifeq -> 1011
    //   979: new java/lang/StringBuilder
    //   982: dup
    //   983: invokespecial <init> : ()V
    //   986: astore_2
    //   987: aload_2
    //   988: ldc_w 'accidentally touch scrap 4: '
    //   991: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   994: pop
    //   995: aload_2
    //   996: aload_1
    //   997: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1000: pop
    //   1001: ldc 'ColorAccidentallyTouch'
    //   1003: aload_2
    //   1004: invokevirtual toString : ()Ljava/lang/String;
    //   1007: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   1010: pop
    //   1011: aconst_null
    //   1012: areturn
    //   1013: aload_0
    //   1014: getfield mContext : Landroid/content/Context;
    //   1017: astore #5
    //   1019: aload #5
    //   1021: ifnull -> 1039
    //   1024: aload_0
    //   1025: aload #5
    //   1027: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   1030: invokevirtual getDisplayMetrics : ()Landroid/util/DisplayMetrics;
    //   1033: getfield widthPixels : I
    //   1036: putfield mDisplayWidth : I
    //   1039: aload_2
    //   1040: ifnonnull -> 1053
    //   1043: aload_0
    //   1044: iconst_1
    //   1045: putfield mEnableSingleSence : Z
    //   1048: aload_0
    //   1049: iconst_1
    //   1050: putfield mEnableMultiSence : Z
    //   1053: aload_0
    //   1054: iconst_0
    //   1055: putfield mIsSingleSence : Z
    //   1058: aload_0
    //   1059: iconst_0
    //   1060: putfield mIsMultiSence : Z
    //   1063: aload_1
    //   1064: invokevirtual getRawX : ()F
    //   1067: f2i
    //   1068: istore #8
    //   1070: iload #8
    //   1072: aload_0
    //   1073: getfield mLeftOffset : I
    //   1076: if_icmpgt -> 1084
    //   1079: iload #8
    //   1081: ifge -> 1115
    //   1084: aload_0
    //   1085: getfield mDisplayWidth : I
    //   1088: istore #9
    //   1090: iload_3
    //   1091: istore #4
    //   1093: iload #8
    //   1095: iload #9
    //   1097: aload_0
    //   1098: getfield mRightOffset : I
    //   1101: isub
    //   1102: if_icmplt -> 1118
    //   1105: iload_3
    //   1106: istore #4
    //   1108: iload #8
    //   1110: iload #9
    //   1112: if_icmpgt -> 1118
    //   1115: iconst_1
    //   1116: istore #4
    //   1118: aload_1
    //   1119: astore #5
    //   1121: aload_0
    //   1122: getfield mEnableSingleSence : Z
    //   1125: ifeq -> 1182
    //   1128: aload_1
    //   1129: astore #5
    //   1131: iload #4
    //   1133: ifeq -> 1182
    //   1136: aload_0
    //   1137: iconst_1
    //   1138: putfield mIsSingleSence : Z
    //   1141: aload_0
    //   1142: getfield DEBUG : Z
    //   1145: ifeq -> 1180
    //   1148: new java/lang/StringBuilder
    //   1151: dup
    //   1152: invokespecial <init> : ()V
    //   1155: astore_2
    //   1156: aload_2
    //   1157: ldc_w 'accidentally touch scrap 1: '
    //   1160: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1163: pop
    //   1164: aload_2
    //   1165: aload_1
    //   1166: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1169: pop
    //   1170: ldc 'ColorAccidentallyTouch'
    //   1172: aload_2
    //   1173: invokevirtual toString : ()Ljava/lang/String;
    //   1176: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   1179: pop
    //   1180: aconst_null
    //   1181: areturn
    //   1182: aload #5
    //   1184: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #437	-> 0
    //   #438	-> 7
    //   #545	-> 49
    //   #546	-> 55
    //   #547	-> 75
    //   #548	-> 80
    //   #549	-> 87
    //   #551	-> 119
    //   #553	-> 121
    //   #554	-> 131
    //   #555	-> 138
    //   #558	-> 170
    //   #559	-> 185
    //   #560	-> 193
    //   #561	-> 203
    //   #463	-> 241
    //   #464	-> 249
    //   #465	-> 254
    //   #468	-> 299
    //   #469	-> 306
    //   #470	-> 342
    //   #476	-> 453
    //   #477	-> 468
    //   #478	-> 475
    //   #480	-> 481
    //   #481	-> 489
    //   #483	-> 495
    //   #484	-> 502
    //   #485	-> 512
    //   #489	-> 527
    //   #490	-> 532
    //   #492	-> 537
    //   #493	-> 545
    //   #494	-> 554
    //   #495	-> 560
    //   #496	-> 566
    //   #497	-> 573
    //   #499	-> 610
    //   #500	-> 614
    //   #502	-> 621
    //   #503	-> 626
    //   #506	-> 632
    //   #507	-> 647
    //   #508	-> 655
    //   #509	-> 665
    //   #515	-> 703
    //   #516	-> 710
    //   #517	-> 717
    //   #518	-> 749
    //   #519	-> 754
    //   #521	-> 759
    //   #522	-> 764
    //   #523	-> 771
    //   #526	-> 806
    //   #530	-> 808
    //   #531	-> 818
    //   #533	-> 826
    //   #534	-> 842
    //   #536	-> 855
    //   #537	-> 861
    //   #538	-> 874
    //   #540	-> 876
    //   #567	-> 882
    //   #568	-> 889
    //   #569	-> 894
    //   #570	-> 901
    //   #572	-> 933
    //   #575	-> 935
    //   #576	-> 945
    //   #577	-> 951
    //   #578	-> 967
    //   #579	-> 972
    //   #580	-> 979
    //   #582	-> 1011
    //   #440	-> 1013
    //   #441	-> 1024
    //   #443	-> 1039
    //   #444	-> 1043
    //   #445	-> 1048
    //   #448	-> 1053
    //   #449	-> 1058
    //   #450	-> 1063
    //   #451	-> 1070
    //   #453	-> 1118
    //   #454	-> 1136
    //   #455	-> 1141
    //   #456	-> 1148
    //   #458	-> 1180
    //   #587	-> 1182
  }
  
  public MotionEvent updatePointerEvent(MotionEvent paramMotionEvent, View paramView, Configuration paramConfiguration) {
    return paramMotionEvent;
  }
  
  public List<Object> handlePointerEvent(MotionEvent paramMotionEvent, View paramView, Configuration paramConfiguration) {
    ArrayList<Integer> arrayList = new ArrayList();
    arrayList.add(0, Integer.valueOf(0));
    arrayList.add(1, paramMotionEvent);
    return (List)arrayList;
  }
  
  public void initData(Context paramContext) {
    try {
      Context context = paramContext.getApplicationContext();
      if (context == null)
        this.mContext = paramContext; 
      OplusActivityManager oplusActivityManager = new OplusActivityManager();
      this();
      OplusAccidentallyTouchData oplusAccidentallyTouchData = oplusActivityManager.getAccidentallyTouchData();
      this.mSingleList = oplusAccidentallyTouchData.getSingleTouchList();
      this.mMultiList = oplusAccidentallyTouchData.getMultiTouchList();
      this.mWhiteList = oplusAccidentallyTouchData.getTouchWhiteList();
      String str2 = oplusAccidentallyTouchData.getLeftOffset();
      String str3 = oplusAccidentallyTouchData.getRightOffset();
      String str1 = oplusAccidentallyTouchData.getPointLeftOffset();
      String str4 = oplusAccidentallyTouchData.getPointRightOffset();
      String str5 = oplusAccidentallyTouchData.getAccidentalltyTouchEnable();
      String str6 = oplusAccidentallyTouchData.getEdgeEnable();
      String str7 = oplusAccidentallyTouchData.getEdgeT();
      String str8 = oplusAccidentallyTouchData.getEdgeT1();
      String str9 = oplusAccidentallyTouchData.getEdgeT2();
      ArrayList<String> arrayList = oplusAccidentallyTouchData.getEdgeList();
      this.mIsEdgePkg = isInList(arrayList, paramContext.getPackageName());
      if (str6 != null)
        if ("true".equals(str6)) {
          this.mEdgeEnable = true;
        } else if ("false".equals(str6)) {
          this.mEdgeEnable = false;
        }  
      if (str7 != null)
        this.mEdgeT = Integer.parseInt(str7); 
      if (str8 != null)
        this.mEdgeT1 = Integer.parseInt(str8); 
      if (str9 != null)
        this.mEdgeT2 = Integer.parseInt(str9); 
      this.mLeftOffset = 5;
      this.mRightOffset = 5;
      this.mPointLeftOffset = 80;
      this.mPointRightOffset = 80;
      if (str2 != null)
        this.mLeftOffset = Integer.parseInt(str2); 
      if (str3 != null)
        this.mRightOffset = Integer.parseInt(str3); 
      if (str1 != null)
        this.mPointLeftOffset = Integer.parseInt(str1); 
      if (str4 != null)
        this.mPointRightOffset = Integer.parseInt(str4); 
      if (str5 != null)
        if ("true".equals(str5)) {
          this.mEnableAccidentallyTouch = true;
        } else {
          this.mEnableAccidentallyTouch = false;
        }  
      if (!isInList(this.mSingleList, paramContext.getPackageName())) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mEnableSingleSence = bool;
      if (!isInList(this.mMultiList, paramContext.getPackageName())) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mEnableMultiSence = bool;
      this.mIsWhiteApp = isInList(this.mWhiteList, paramContext.getPackageName());
      OplusPackageManager oplusPackageManager = new OplusPackageManager();
      this();
      boolean bool = oplusPackageManager.isClosedSuperFirewall();
      if (bool || !PROPERTY_ENABLE)
        this.mEnableAccidentallyTouch = false; 
      DisplayMetrics displayMetrics = paramContext.getResources().getDisplayMetrics();
      float f = displayMetrics.density / 3.0F;
      this.mDisplayWidth = displayMetrics.widthPixels;
      this.mPointLeftOffset = (int)(this.mPointLeftOffset * f);
      this.mPointRightOffset = (int)(this.mPointRightOffset * f);
      if (this.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("density:");
        stringBuilder.append(displayMetrics.density);
        stringBuilder.append(", mEnableMulti:");
        stringBuilder.append(this.mEnableMultiSence);
        stringBuilder.append(", pointLeftOffset:");
        stringBuilder.append(str1);
        stringBuilder.append(", mLeftOffset:");
        stringBuilder.append(this.mLeftOffset);
        stringBuilder.append(", mPointLeftOffset:");
        stringBuilder.append(this.mPointLeftOffset);
        Log.d("ColorAccidentallyTouch", stringBuilder.toString());
      } 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("init data RemoteException , ");
      stringBuilder.append(remoteException);
      Log.e("ColorAccidentallyTouch", stringBuilder.toString());
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("init data Exception , ");
      stringBuilder.append(exception);
      Log.e("ColorAccidentallyTouch", stringBuilder.toString());
    } catch (Error error) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("init data Error , ");
      stringBuilder.append(error);
      Log.e("ColorAccidentallyTouch", stringBuilder.toString());
    } 
  }
  
  private boolean isInList(ArrayList<String> paramArrayList, String paramString) {
    if (paramArrayList != null && !paramArrayList.isEmpty() && paramArrayList.contains(paramString))
      return true; 
    if (!this.mDefaultList.isEmpty() && this.mDefaultList.contains(paramString))
      return true; 
    return false;
  }
  
  private int getFirstIds(MotionEvent paramMotionEvent) {
    if (paramMotionEvent != null)
      return 1 << paramMotionEvent.getPointerId(0); 
    return 1;
  }
  
  public boolean getEdgeEnable() {
    return this.mEdgeEnable;
  }
  
  public int getEdgeT() {
    return this.mEdgeT;
  }
  
  public int getOriEdgeT1() {
    return this.mEdgeT1;
  }
  
  public boolean isEdgePkg() {
    return this.mIsEdgePkg;
  }
  
  public ArrayList<String> getEdgeList() {
    return this.mEdgeList;
  }
  
  public int getEdgeT1() {
    if (this.mIsEdgePkg)
      return this.mEdgeT; 
    return this.mEdgeT1;
  }
  
  public int getEdgeT2() {
    return this.mEdgeT2;
  }
}
