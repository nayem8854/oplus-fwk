package com.oplus.util;

import android.content.Context;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class OplusUnitConversionUtils {
  private static final String B = " B";
  
  private static final String B_S = " B/s";
  
  private static final String GB = " GB";
  
  private static final String GB_S = " GB/s";
  
  private static final double HUNDRED = 100.0D;
  
  private static final String KB = " KB";
  
  private static final String KB_S = " KB/s";
  
  private static final String MB = " MB";
  
  private static final String MB_S = " MB/s";
  
  private static final double MILLION = 1000000.0D;
  
  private static final String NOPOINT = "0";
  
  private static final String ONEPOINT = "0.0";
  
  private static final String PB = " PB";
  
  private static final String PB_S = " PB/s";
  
  private static final String SIXPOINT = "0.00000";
  
  private static final double SPECIAL = 1024.0D;
  
  private static final int SQUARE_FIVE = 5;
  
  private static final int SQUARE_FOUR = 4;
  
  private static final int SQUARE_THREE = 3;
  
  private static final String TAG = "OplusUnitConversionUtils";
  
  private static final String TB = " TB";
  
  private static final String TB_S = " TB/s";
  
  private static final double TEN = 10.0D;
  
  private static final double THOUSAND = 1000.0D;
  
  private static final String TWOPOINT = "0.00";
  
  private Context mContext;
  
  private String mMoreDownLoad = null;
  
  private String mMostDownLoad = null;
  
  private String mSpecialPoint = "0.98";
  
  public OplusUnitConversionUtils(Context paramContext) {
    this.mContext = paramContext;
    this.mMoreDownLoad = paramContext.getResources().getString(201589018);
    this.mMostDownLoad = paramContext.getResources().getString(201589019);
    this.mSpecialPoint = formatLocaleNumber(0.98D, "0.00");
  }
  
  private boolean isChinese() {
    String str = (this.mContext.getResources().getConfiguration()).locale.getCountry();
    if (str != null && (str.equalsIgnoreCase("CN") || 
      str.equalsIgnoreCase("TW") || 
      str.equalsIgnoreCase("HK")))
      return true; 
    return false;
  }
  
  private String formatNumber(double paramDouble, String paramString, boolean paramBoolean) {
    DecimalFormat decimalFormat = new DecimalFormat(paramString, new DecimalFormatSymbols(Locale.CHINA));
    if (!paramBoolean) {
      decimalFormat.setRoundingMode(RoundingMode.FLOOR);
    } else {
      decimalFormat.setRoundingMode(RoundingMode.HALF_UP);
    } 
    return decimalFormat.format(paramDouble);
  }
  
  private String formatLocaleNumber(double paramDouble, String paramString) {
    DecimalFormat decimalFormat = new DecimalFormat(paramString, new DecimalFormatSymbols((this.mContext.getResources().getConfiguration()).locale));
    return decimalFormat.format(paramDouble);
  }
  
  private String getChineseDownloadValue(long paramLong) {
    String str;
    if (0L <= paramLong && paramLong < 10000.0D) {
      long l = paramLong;
      if (paramLong == 0L)
        l = paramLong + 1L; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(l);
      stringBuilder.append(" ");
      str = stringBuilder.toString();
    } else if (10000.0D <= paramLong && paramLong < 100000.0D) {
      double d = Double.valueOf(formatNumber(paramLong / 10000.0D, "0.0", true)).doubleValue();
      int i = (int)d;
      if (d == i) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(i);
        stringBuilder.append(this.mMoreDownLoad);
        str = stringBuilder.toString();
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(d);
        stringBuilder.append(this.mMoreDownLoad);
        str = stringBuilder.toString();
      } 
    } else if (100000.0D <= paramLong && paramLong < 1000000.0D) {
      double d = Double.valueOf(formatNumber(paramLong / 10000.0D, "0.0", true)).doubleValue();
      int i = (int)d;
      if (d == i) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(i);
        stringBuilder.append(this.mMoreDownLoad);
        str = stringBuilder.toString();
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(d);
        stringBuilder.append(this.mMoreDownLoad);
        str = stringBuilder.toString();
      } 
    } else if (1000000.0D <= paramLong && paramLong < 1.0E7D) {
      double d = Double.valueOf(formatNumber(paramLong / 10000.0D, "0.00", true)).doubleValue();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append((int)d);
      stringBuilder.append(this.mMoreDownLoad);
      str = stringBuilder.toString();
    } else if (1.0E7D <= paramLong && paramLong < 1.0E8D) {
      double d = Double.valueOf(formatNumber(paramLong / 10000.0D, "0.00", true)).doubleValue();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append((int)d);
      stringBuilder.append(this.mMoreDownLoad);
      str = stringBuilder.toString();
    } else {
      if (paramLong >= 1.0E8D) {
        double d = Double.valueOf(formatNumber(paramLong / 1.0E8D, "0.00000", true)).doubleValue();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(formatNumber(d, "0.0", false));
        stringBuilder.append(this.mMostDownLoad);
        str = stringBuilder.toString();
        return str;
      } 
      throw new IllegalArgumentException("the value of the incoming is wrong");
    } 
    return str;
  }
  
  private String getEnglishDownloadValue(long paramLong) {
    String str;
    if (0L <= paramLong && paramLong < 10000.0D) {
      long l = paramLong;
      if (paramLong == 0L)
        l = paramLong + 1L; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(l);
      stringBuilder.append(" ");
      str = stringBuilder.toString();
    } else if (10000.0D <= paramLong && paramLong < 100000.0D) {
      double d = Double.valueOf(formatNumber(paramLong / 10000.0D, "0.0", true)).doubleValue();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append((int)(10.0D * d));
      stringBuilder.append(this.mMoreDownLoad);
      str = stringBuilder.toString();
    } else if (100000.0D <= paramLong && paramLong < 1000000.0D) {
      double d = Double.valueOf(formatNumber(paramLong / 10000.0D, "0.0", true)).doubleValue();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append((int)(10.0D * d));
      stringBuilder.append(this.mMoreDownLoad);
      str = stringBuilder.toString();
    } else if (1000000.0D <= paramLong && paramLong < 1.0E7D) {
      str = formatNumber(paramLong / 10000.0D, "0.00", true);
      double d = Double.valueOf(str).doubleValue() / 100.0D;
      int i = (int)d;
      if (d == i) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(i);
        stringBuilder.append(this.mMostDownLoad);
        str = stringBuilder.toString();
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(Double.valueOf(str));
        stringBuilder.append(this.mMostDownLoad);
        str = stringBuilder.toString();
      } 
    } else if (1.0E7D <= paramLong && paramLong < 1.0E8D) {
      String str1 = formatNumber(paramLong / 10000.0D, "0.00", true);
      double d = Double.valueOf(str1).doubleValue() / 100.0D;
      int i = (int)d;
      if (d == i) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(i);
        stringBuilder.append(this.mMostDownLoad);
        str = stringBuilder.toString();
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(Double.valueOf(str1));
        stringBuilder.append(this.mMostDownLoad);
        str = stringBuilder.toString();
      } 
    } else {
      if (paramLong >= 1.0E8D) {
        double d = Double.valueOf(formatNumber(paramLong / 1.0E8D, "0.00000", true)).doubleValue();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append((int)(Double.valueOf(formatNumber(d, "0.0", false)).doubleValue() * 100.0D));
        stringBuilder.append(this.mMostDownLoad);
        str = stringBuilder.toString();
        return str;
      } 
      throw new IllegalArgumentException("the value of the incoming is wrong");
    } 
    return str;
  }
  
  private String getChineseStripValue(long paramLong) {
    String str;
    if (0L <= paramLong && paramLong < 10000.0D) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramLong);
      stringBuilder.append(" ");
      str = stringBuilder.toString();
    } else if (10000.0D <= paramLong && paramLong < 1000000.0D) {
      double d = Double.valueOf(formatNumber(paramLong / 10000.0D, "0.0", true)).doubleValue();
      int i = (int)d;
      if (d == i) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(i);
        stringBuilder.append(this.mMoreDownLoad);
        str = stringBuilder.toString();
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(d);
        stringBuilder.append(this.mMoreDownLoad);
        str = stringBuilder.toString();
      } 
    } else {
      if (1000000.0D <= paramLong && paramLong < 1.0E8D) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(formatNumber(paramLong / 10000.0D, "0", true));
        stringBuilder.append(this.mMoreDownLoad);
        str = stringBuilder.toString();
        return str;
      } 
      throw new IllegalArgumentException("the value of the incoming is wrong");
    } 
    return str;
  }
  
  private String getEnglishStripValue(long paramLong) {
    String str;
    if (0L <= paramLong && paramLong < 10000.0D) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramLong);
      stringBuilder.append(" ");
      str = stringBuilder.toString();
    } else if (10000.0D <= paramLong && paramLong < 1000000.0D) {
      double d = Double.valueOf(formatNumber(paramLong / 10000.0D, "0.0", true)).doubleValue();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append((int)(10.0D * d));
      stringBuilder.append(this.mMoreDownLoad);
      str = stringBuilder.toString();
    } else {
      if (1000000.0D <= paramLong && paramLong < 1.0E8D) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append((int)(Double.valueOf(formatNumber(paramLong / 10000.0D, "0", true)).doubleValue() * 10.0D));
        stringBuilder.append(this.mMoreDownLoad);
        str = stringBuilder.toString();
        return str;
      } 
      throw new IllegalArgumentException("the value of the incoming is wrong");
    } 
    return str;
  }
  
  public String getUnitValue(long paramLong) {
    return getTransformUnitValue(paramLong, 1024.0D);
  }
  
  public String getUnitThousandValue(long paramLong) {
    return getTransformUnitValue(paramLong, 1000.0D);
  }
  
  public String getTransformUnitValue(long paramLong, double paramDouble) throws IllegalArgumentException {
    String str;
    if (0L <= paramLong && paramLong < 1000.0D) {
      str = formatNumber(paramLong, "0", true);
      paramLong = Long.valueOf(str).longValue();
      str = formatLocaleNumber(Double.valueOf(str).doubleValue(), "0");
      if (1000.0D <= paramLong && paramLong < 1024.0D)
        return getUnitValue(paramLong); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append(" B");
      str = stringBuilder.toString();
    } else if (1000.0D <= paramLong && paramLong < 1024000.0D) {
      str = formatNumber(paramLong / paramDouble, "0", true);
      paramLong = Long.valueOf(str).longValue() * (long)paramDouble;
      str = formatLocaleNumber(Double.valueOf(str).doubleValue(), "0");
      if (1024000.0D <= paramLong && paramLong < Math.pow(1024.0D, 2.0D) * 100.0D)
        return getTransformUnitValue(paramLong, paramDouble); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append(" KB");
      str = stringBuilder.toString();
    } else if (1024000.0D <= paramLong && paramLong < Math.pow(1024.0D, 2.0D) * 100.0D) {
      str = formatNumber(paramLong / Math.pow(paramDouble, 2.0D), "0.0", true);
      paramLong = (long)(Double.valueOf(str).doubleValue() * Math.pow(paramDouble, 2.0D));
      str = formatLocaleNumber(Double.valueOf(str).doubleValue(), "0.0");
      if (Math.pow(1024.0D, 2.0D) * 100.0D <= paramLong && paramLong < Math.pow(1024.0D, 2.0D) * 1000.0D)
        return getTransformUnitValue(paramLong, paramDouble); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append(" MB");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 2.0D) * 100.0D <= paramLong && paramLong < Math.pow(1024.0D, 2.0D) * 1000.0D) {
      String str1 = formatNumber(paramLong / Math.pow(paramDouble, 2.0D), "0", true);
      paramLong = (long)(Double.valueOf(str1).doubleValue() * Math.pow(paramDouble, 2.0D));
      String str2 = formatLocaleNumber(Double.valueOf(str1).doubleValue(), "0");
      if (Math.pow(1024.0D, 2.0D) * 1000.0D <= paramLong && paramLong < Math.pow(1024.0D, 3.0D))
        return getTransformUnitValue(paramLong, paramDouble); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append(" MB");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 2.0D) * 1000.0D <= paramLong && paramLong < Math.pow(1024.0D, 3.0D)) {
      if (paramDouble == 1000.0D) {
        str = formatNumber(paramLong / Math.pow(paramDouble, 3.0D), "0.00", true);
        paramLong = (long)(Double.valueOf(str).doubleValue() * Math.pow(paramDouble, 3.0D));
        str = formatLocaleNumber(Double.valueOf(str).doubleValue(), "0.00");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str);
        stringBuilder.append(" GB");
        str = stringBuilder.toString();
      } else {
        if (paramDouble == 1024.0D) {
          if (paramLong > Math.pow(1024.0D, 2.0D) * 1023.0D)
            return getUnitValue((long)Math.pow(1024.0D, 3.0D)); 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(this.mSpecialPoint);
          stringBuilder.append(" GB");
          return stringBuilder.toString();
        } 
        str = null;
      } 
    } else if (Math.pow(1024.0D, 3.0D) <= paramLong && paramLong < Math.pow(1024.0D, 3.0D) * 10.0D) {
      str = formatNumber(paramLong / Math.pow(paramDouble, 3.0D), "0.00", true);
      paramLong = (long)(Double.valueOf(str).doubleValue() * Math.pow(paramDouble, 3.0D));
      str = formatLocaleNumber(Double.valueOf(str).doubleValue(), "0.00");
      if (Math.pow(1024.0D, 3.0D) * 10.0D <= paramLong && paramLong < Math.pow(1024.0D, 3.0D) * 100.0D)
        return getTransformUnitValue(paramLong, paramDouble); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append(" GB");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 3.0D) * 10.0D <= paramLong && paramLong < Math.pow(1024.0D, 3.0D) * 100.0D) {
      String str1 = formatNumber(paramLong / Math.pow(paramDouble, 3.0D), "0.0", true);
      paramLong = (long)(Double.valueOf(str1).doubleValue() * Math.pow(paramDouble, 3.0D));
      String str2 = formatLocaleNumber(Double.valueOf(str1).doubleValue(), "0.0");
      if (Math.pow(1024.0D, 3.0D) * 100.0D <= paramLong && paramLong < Math.pow(1024.0D, 3.0D) * 1000.0D)
        return getTransformUnitValue(paramLong, paramDouble); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append(" GB");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 3.0D) * 100.0D <= paramLong && paramLong < Math.pow(1024.0D, 3.0D) * 1000.0D) {
      String str1 = formatNumber(paramLong / Math.pow(paramDouble, 3.0D), "0", true);
      paramLong = (long)(Double.valueOf(str1).doubleValue() * Math.pow(paramDouble, 3.0D));
      String str2 = formatLocaleNumber(Double.valueOf(str1).doubleValue(), "0");
      if (Math.pow(1024.0D, 3.0D) * 1000.0D <= paramLong && paramLong < Math.pow(1024.0D, 4.0D))
        return getTransformUnitValue(paramLong, paramDouble); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append(" GB");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 3.0D) * 1000.0D <= paramLong && paramLong < Math.pow(1024.0D, 4.0D)) {
      if (paramDouble == 1000.0D) {
        String str1 = formatNumber(paramLong / Math.pow(paramDouble, 4.0D), "0.00", true);
        paramLong = (long)(Double.valueOf(str1).doubleValue() * Math.pow(paramDouble, 4.0D));
        String str2 = formatLocaleNumber(Double.valueOf(str1).doubleValue(), "0.00");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str2);
        stringBuilder.append(" TB");
        str = stringBuilder.toString();
      } else {
        if (paramDouble == 1024.0D) {
          if (paramLong > Math.pow(1024.0D, 3.0D) * 1023.0D)
            return getUnitValue((long)Math.pow(1024.0D, 4.0D)); 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(this.mSpecialPoint);
          stringBuilder.append(" TB");
          return stringBuilder.toString();
        } 
        str = null;
      } 
    } else if (Math.pow(1024.0D, 4.0D) <= paramLong && paramLong < Math.pow(1024.0D, 4.0D) * 10.0D) {
      String str1 = formatNumber(paramLong / Math.pow(paramDouble, 4.0D), "0.00", true);
      paramLong = (long)(Double.valueOf(str1).doubleValue() * Math.pow(paramDouble, 4.0D));
      String str2 = formatLocaleNumber(Double.valueOf(str1).doubleValue(), "0.00");
      if (Math.pow(1024.0D, 4.0D) * 10.0D <= paramLong && paramLong < Math.pow(1024.0D, 4.0D) * 100.0D)
        return getTransformUnitValue(paramLong, paramDouble); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append(" TB");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 4.0D) * 10.0D <= paramLong && paramLong < Math.pow(1024.0D, 4.0D) * 100.0D) {
      str = formatNumber(paramLong / Math.pow(paramDouble, 4.0D), "0.0", true);
      paramLong = (long)(Double.valueOf(str).doubleValue() * Math.pow(paramDouble, 4.0D));
      str = formatLocaleNumber(Double.valueOf(str).doubleValue(), "0.0");
      if (Math.pow(1024.0D, 4.0D) * 100.0D <= paramLong && paramLong < Math.pow(1024.0D, 4.0D) * 1000.0D)
        return getTransformUnitValue(paramLong, paramDouble); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append(" TB");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 4.0D) * 100.0D <= paramLong && paramLong < Math.pow(1024.0D, 4.0D) * 1000.0D) {
      str = formatNumber(paramLong / Math.pow(paramDouble, 4.0D), "0", true);
      paramLong = (long)(Double.valueOf(str).doubleValue() * Math.pow(paramDouble, 4.0D));
      str = formatLocaleNumber(Double.valueOf(str).doubleValue(), "0");
      if (Math.pow(1024.0D, 4.0D) * 1000.0D <= paramLong && paramLong < Math.pow(1024.0D, 5.0D))
        return getTransformUnitValue(paramLong, paramDouble); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append(" TB");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 4.0D) * 1000.0D <= paramLong && paramLong < Math.pow(1024.0D, 5.0D)) {
      if (paramDouble == 1000.0D) {
        str = formatNumber(paramLong / Math.pow(paramDouble, 5.0D), "0.00", true);
        paramLong = (long)(Double.valueOf(str).doubleValue() * Math.pow(paramDouble, 5.0D));
        str = formatLocaleNumber(Double.valueOf(str).doubleValue(), "0.00");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str);
        stringBuilder.append(" PB");
        str = stringBuilder.toString();
      } else {
        if (paramDouble == 1024.0D) {
          if (paramLong > Math.pow(1024.0D, 4.0D) * 1023.0D)
            return getUnitValue((long)Math.pow(1024.0D, 5.0D)); 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(this.mSpecialPoint);
          stringBuilder.append(" PB");
          return stringBuilder.toString();
        } 
        str = null;
      } 
    } else if (Math.pow(1024.0D, 5.0D) <= paramLong && paramLong < Math.pow(1024.0D, 5.0D) * 10.0D) {
      str = formatNumber(paramLong / Math.pow(1024.0D, 5.0D), "0.00", true);
      paramLong = (long)(Double.valueOf(str).doubleValue() * Math.pow(1024.0D, 5.0D));
      str = formatLocaleNumber(Double.valueOf(str).doubleValue(), "0.00");
      if (Math.pow(1024.0D, 5.0D) * 10.0D <= paramLong && paramLong < Math.pow(1024.0D, 5.0D) * 100.0D)
        return getUnitValue(paramLong); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append(" PB");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 5.0D) * 10.0D <= paramLong && paramLong < Math.pow(1024.0D, 5.0D) * 100.0D) {
      str = formatNumber(paramLong / Math.pow(1024.0D, 5.0D), "0.0", true);
      paramLong = (long)(Double.valueOf(str).doubleValue() * Math.pow(1024.0D, 5.0D));
      str = formatLocaleNumber(Double.valueOf(str).doubleValue(), "0.0");
      if (Math.pow(1024.0D, 5.0D) * 100.0D <= paramLong && paramLong < Math.pow(1024.0D, 5.0D) * 1000.0D)
        return getUnitValue(paramLong); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append(" PB");
      str = stringBuilder.toString();
    } else {
      if (Math.pow(1024.0D, 5.0D) * 100.0D <= paramLong && paramLong < Math.pow(1024.0D, 5.0D) * 1000.0D) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(formatLocaleNumber(paramLong / Math.pow(1024.0D, 5.0D), "0"));
        stringBuilder.append(" PB");
        str = stringBuilder.toString();
        return str;
      } 
      throw new IllegalArgumentException("the value of the incoming is wrong");
    } 
    return str;
  }
  
  public String getSpeedValue(long paramLong) throws IllegalArgumentException {
    String str;
    if (0L <= paramLong && paramLong < 1000.0D) {
      str = formatNumber(paramLong, "0", true);
      paramLong = Long.valueOf(str).longValue();
      str = formatLocaleNumber(Double.valueOf(str).doubleValue(), "0");
      if (1000.0D <= paramLong && paramLong < 1024.0D)
        return getUnitValue(paramLong); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append(" B/s");
      str = stringBuilder.toString();
    } else if (1000.0D <= paramLong && paramLong < 1024000.0D) {
      str = formatNumber(paramLong / 1024.0D, "0", true);
      paramLong = Long.valueOf(str).longValue() * 1024L;
      str = formatLocaleNumber(Double.valueOf(str).doubleValue(), "0");
      if (1024000.0D <= paramLong && paramLong < Math.pow(1024.0D, 2.0D) * 100.0D)
        return getUnitValue(paramLong); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append(" KB/s");
      str = stringBuilder.toString();
    } else if (1024000.0D <= paramLong && paramLong < Math.pow(1024.0D, 2.0D) * 100.0D) {
      String str1 = formatNumber(paramLong / Math.pow(1024.0D, 2.0D), "0.0", true);
      paramLong = (long)(Double.valueOf(str1).doubleValue() * Math.pow(1024.0D, 2.0D));
      String str2 = formatLocaleNumber(Double.valueOf(str1).doubleValue(), "0.0");
      if (Math.pow(1024.0D, 2.0D) * 100.0D <= paramLong && paramLong < Math.pow(1024.0D, 2.0D) * 1000.0D)
        return getUnitValue(paramLong); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append(" MB/s");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 2.0D) * 100.0D <= paramLong && paramLong < Math.pow(1024.0D, 2.0D) * 1000.0D) {
      str = formatNumber(paramLong / Math.pow(1024.0D, 2.0D), "0", true);
      paramLong = (long)(Double.valueOf(str).doubleValue() * Math.pow(1024.0D, 2.0D));
      str = formatLocaleNumber(Double.valueOf(str).doubleValue(), "0");
      if (Math.pow(1024.0D, 2.0D) * 1000.0D <= paramLong && paramLong < Math.pow(1024.0D, 3.0D))
        return getUnitValue(paramLong); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append(" MB/s");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 2.0D) * 1000.0D <= paramLong && paramLong < Math.pow(1024.0D, 3.0D)) {
      if (paramLong > Math.pow(1024.0D, 2.0D) * 1023.0D)
        return getUnitValue((long)Math.pow(1024.0D, 3.0D)); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this.mSpecialPoint);
      stringBuilder.append(" GB/s");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 3.0D) <= paramLong && paramLong < Math.pow(1024.0D, 3.0D) * 10.0D) {
      str = formatNumber(paramLong / Math.pow(1024.0D, 3.0D), "0.00", true);
      paramLong = (long)(Double.valueOf(str).doubleValue() * Math.pow(1024.0D, 3.0D));
      str = formatLocaleNumber(Double.valueOf(str).doubleValue(), "0.00");
      if (Math.pow(1024.0D, 3.0D) * 10.0D <= paramLong && paramLong < Math.pow(1024.0D, 3.0D) * 100.0D)
        return getUnitValue(paramLong); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append(" GB/s");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 3.0D) * 10.0D <= paramLong && paramLong < Math.pow(1024.0D, 3.0D) * 100.0D) {
      String str1 = formatNumber(paramLong / Math.pow(1024.0D, 3.0D), "0.0", true);
      paramLong = (long)(Double.valueOf(str1).doubleValue() * Math.pow(1024.0D, 3.0D));
      String str2 = formatLocaleNumber(Double.valueOf(str1).doubleValue(), "0.0");
      if (Math.pow(1024.0D, 3.0D) * 100.0D <= paramLong && paramLong < Math.pow(1024.0D, 3.0D) * 1000.0D)
        return getUnitValue(paramLong); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append(" GB/s");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 3.0D) * 100.0D <= paramLong && paramLong < Math.pow(1024.0D, 3.0D) * 1000.0D) {
      String str1 = formatNumber(paramLong / Math.pow(1024.0D, 3.0D), "0", true);
      paramLong = (long)(Double.valueOf(str1).doubleValue() * Math.pow(1024.0D, 3.0D));
      String str2 = formatLocaleNumber(Double.valueOf(str1).doubleValue(), "0");
      if (Math.pow(1024.0D, 3.0D) * 1000.0D <= paramLong && paramLong < Math.pow(1024.0D, 4.0D))
        return getUnitValue(paramLong); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append(" GB/s");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 3.0D) * 1000.0D <= paramLong && paramLong < Math.pow(1024.0D, 4.0D)) {
      if (paramLong > Math.pow(1024.0D, 3.0D) * 1023.0D)
        return getUnitValue((long)Math.pow(1024.0D, 4.0D)); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this.mSpecialPoint);
      stringBuilder.append(" TB/s");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 4.0D) <= paramLong && paramLong < Math.pow(1024.0D, 4.0D) * 10.0D) {
      str = formatNumber(paramLong / Math.pow(1024.0D, 4.0D), "0.00", true);
      paramLong = (long)(Double.valueOf(str).doubleValue() * Math.pow(1024.0D, 4.0D));
      str = formatLocaleNumber(Double.valueOf(str).doubleValue(), "0.00");
      if (Math.pow(1024.0D, 4.0D) * 10.0D <= paramLong && paramLong < Math.pow(1024.0D, 4.0D) * 100.0D)
        return getUnitValue(paramLong); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append(" TB/s");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 4.0D) * 10.0D <= paramLong && paramLong < Math.pow(1024.0D, 4.0D) * 100.0D) {
      String str1 = formatNumber(paramLong / Math.pow(1024.0D, 4.0D), "0.0", true);
      paramLong = (long)(Double.valueOf(str1).doubleValue() * Math.pow(1024.0D, 4.0D));
      String str2 = formatLocaleNumber(Double.valueOf(str1).doubleValue(), "0.0");
      if (Math.pow(1024.0D, 4.0D) * 100.0D <= paramLong && paramLong < Math.pow(1024.0D, 4.0D) * 1000.0D)
        return getUnitValue(paramLong); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append(" TB/s");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 4.0D) * 100.0D <= paramLong && paramLong < Math.pow(1024.0D, 4.0D) * 1000.0D) {
      String str1 = formatNumber(paramLong / Math.pow(1024.0D, 4.0D), "0", true);
      paramLong = (long)(Double.valueOf(str1).doubleValue() * Math.pow(1024.0D, 4.0D));
      if (Math.pow(1024.0D, 4.0D) * 1000.0D <= paramLong && paramLong < Math.pow(1024.0D, 5.0D))
        return getUnitValue(paramLong); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str1);
      stringBuilder.append(" TB/s");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 4.0D) * 1000.0D <= paramLong && paramLong < Math.pow(1024.0D, 5.0D)) {
      if (paramLong > Math.pow(1024.0D, 4.0D) * 1023.0D)
        return getUnitValue((long)Math.pow(1024.0D, 5.0D)); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this.mSpecialPoint);
      stringBuilder.append(" PB/s");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 5.0D) <= paramLong && paramLong < Math.pow(1024.0D, 5.0D) * 10.0D) {
      String str1 = formatNumber(paramLong / Math.pow(1024.0D, 5.0D), "0.00", true);
      paramLong = (long)(Double.valueOf(str1).doubleValue() * Math.pow(1024.0D, 5.0D));
      String str2 = formatLocaleNumber(Double.valueOf(str1).doubleValue(), "0.00");
      if (Math.pow(1024.0D, 5.0D) * 10.0D <= paramLong && paramLong < Math.pow(1024.0D, 5.0D) * 100.0D)
        return getUnitValue(paramLong); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append(" PB/s");
      str = stringBuilder.toString();
    } else if (Math.pow(1024.0D, 5.0D) * 10.0D <= paramLong && paramLong < Math.pow(1024.0D, 5.0D) * 100.0D) {
      str = formatNumber(paramLong / Math.pow(1024.0D, 5.0D), "0.0", true);
      paramLong = (long)(Double.valueOf(str).doubleValue() * Math.pow(1024.0D, 5.0D));
      str = formatLocaleNumber(Double.valueOf(str).doubleValue(), "0.0");
      if (Math.pow(1024.0D, 5.0D) * 100.0D <= paramLong && paramLong < Math.pow(1024.0D, 5.0D) * 1000.0D)
        return getUnitValue(paramLong); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append(" PB/s");
      str = stringBuilder.toString();
    } else {
      if (Math.pow(1024.0D, 5.0D) * 100.0D <= paramLong && paramLong < Math.pow(1024.0D, 5.0D) * 1000.0D) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(formatLocaleNumber(paramLong / Math.pow(1024.0D, 5.0D), "0"));
        stringBuilder.append(" PB/s");
        str = stringBuilder.toString();
        return str;
      } 
      throw new IllegalArgumentException("the value of the incoming is wrong");
    } 
    return str;
  }
  
  public String getDownLoadValue(long paramLong) throws IllegalArgumentException {
    String str;
    if (isChinese()) {
      str = getChineseDownloadValue(paramLong);
    } else {
      str = getEnglishDownloadValue(paramLong);
    } 
    return str;
  }
  
  public String getStripValue(long paramLong) throws IllegalArgumentException {
    String str;
    if (isChinese()) {
      str = getChineseStripValue(paramLong);
    } else {
      str = getEnglishStripValue(paramLong);
    } 
    return str;
  }
}
