package com.oplus.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import java.util.List;

public class OplusResolverUtil {
  private static final String TEST_ACTIVITY_BOTH = ".TwoGroupsActivity";
  
  private static final String TEST_ACTIVITY_FIRST = ".FirstActivity";
  
  private static final String TEST_CTS_ACTION = "android.dynamicmime.preferred.TEST_ACTION";
  
  private static final String TEST_PACKAGE = "android.dynamicmime.preferred";
  
  private static final String TEST_TYPE = "text/plain";
  
  public static boolean isResolverCtsTest(Context paramContext, Intent paramIntent, ResolveInfo paramResolveInfo) {
    boolean bool = paramContext instanceof com.android.internal.app.ResolverActivity;
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (paramResolveInfo == null)
      return false; 
    if (paramIntent != null && ("android.dynamicmime.preferred.TEST_ACTION".equals(paramIntent.getAction()) || "android.intent.action.SEND".equals(paramIntent.getAction())) && 
      "text/plain".equals(paramIntent.getType()) && 
      paramResolveInfo.activityInfo != null && 
      "android.dynamicmime.preferred".equals(paramResolveInfo.activityInfo.packageName)) {
      String str = paramResolveInfo.loadLabel(paramContext.getPackageManager()).toString();
      if ("TestApp.FirstActivity".equals(str) || 
        "TestApp.TwoGroupsActivity".equals(str))
        bool1 = true; 
      return bool1;
    } 
    return false;
  }
  
  public static boolean isChooserCtsTest(Context paramContext, Intent paramIntent) {
    if (!(paramContext instanceof com.android.internal.app.ChooserActivity))
      return false; 
    if ((!"android.intent.action.SEND".equals(paramIntent.getAction()) && !"android.dynamicmime.preferred.TEST_ACTION".equals(paramIntent.getAction())) || 
      !"test/cts".equals(paramIntent.getType()))
      return false; 
    return true;
  }
  
  public static void sortCtsTest(Context paramContext, Intent paramIntent, List<ResolveInfo> paramList) {
    for (ResolveInfo resolveInfo : paramList) {
      if (isResolverCtsTest(paramContext, paramIntent, resolveInfo)) {
        paramList.remove(resolveInfo);
        paramList.add(0, resolveInfo);
        break;
      } 
    } 
  }
}
