package com.oplus.util;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.provider.Settings;
import android.text.TextUtils;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class OplusAccessibilityUtil {
  private static final String TALKBACK_SERVICE = "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService";
  
  public static Set<ComponentName> getEnabledServicesFromSettings(Context paramContext) {
    return getEnabledServicesFromSettings(paramContext, -2);
  }
  
  public static Set<ComponentName> getEnabledServicesFromSettings(Context paramContext, int paramInt) {
    ContentResolver contentResolver = paramContext.getContentResolver();
    String str = Settings.Secure.getStringForUser(contentResolver, "enabled_accessibility_services", paramInt);
    if (str == null)
      return Collections.emptySet(); 
    HashSet<ComponentName> hashSet = new HashSet();
    TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(':');
    simpleStringSplitter.setString(str);
    while (simpleStringSplitter.hasNext()) {
      str = simpleStringSplitter.next();
      ComponentName componentName = ComponentName.unflattenFromString(str);
      if (componentName != null)
        hashSet.add(componentName); 
    } 
    return hashSet;
  }
  
  public static boolean isTalkbackEnabled(Context paramContext) {
    Set<ComponentName> set = getEnabledServicesFromSettings(paramContext);
    if (set == null || set.isEmpty())
      return false; 
    for (ComponentName componentName : set) {
      if (TextUtils.equals(componentName.flattenToString(), "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService"))
        return true; 
    } 
    return false;
  }
}
