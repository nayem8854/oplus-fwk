package com.oplus.util;

import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;

public class OplusRoundRectUtil {
  private static OplusRoundRectUtil sInstance;
  
  private Path mPath = new Path();
  
  public static OplusRoundRectUtil getInstance() {
    // Byte code:
    //   0: ldc com/oplus/util/OplusRoundRectUtil
    //   2: monitorenter
    //   3: getstatic com/oplus/util/OplusRoundRectUtil.sInstance : Lcom/oplus/util/OplusRoundRectUtil;
    //   6: ifnonnull -> 21
    //   9: new com/oplus/util/OplusRoundRectUtil
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic com/oplus/util/OplusRoundRectUtil.sInstance : Lcom/oplus/util/OplusRoundRectUtil;
    //   21: getstatic com/oplus/util/OplusRoundRectUtil.sInstance : Lcom/oplus/util/OplusRoundRectUtil;
    //   24: astore_0
    //   25: ldc com/oplus/util/OplusRoundRectUtil
    //   27: monitorexit
    //   28: aload_0
    //   29: areturn
    //   30: astore_0
    //   31: ldc com/oplus/util/OplusRoundRectUtil
    //   33: monitorexit
    //   34: aload_0
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #16	-> 3
    //   #17	-> 9
    //   #19	-> 21
    //   #15	-> 30
    // Exception table:
    //   from	to	target	type
    //   3	9	30	finally
    //   9	21	30	finally
    //   21	25	30	finally
  }
  
  public Path getPath(Rect paramRect, float paramFloat) {
    return getPath(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom, paramFloat);
  }
  
  public Path getPath(RectF paramRectF, float paramFloat) {
    return getPath(paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramFloat);
  }
  
  public Path getPath(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5) {
    return getPath(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, true, true, true, true);
  }
  
  public Path getPath(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4) {
    Path path = this.mPath;
    if (path != null)
      path.reset(); 
    if (paramFloat5 < 0.0F)
      paramFloat5 = 0.0F; 
    float f1 = paramFloat3 - paramFloat1;
    float f2 = paramFloat4 - paramFloat2;
    if ((paramFloat5 / Math.min(f1 / 2.0F, f2 / 2.0F)) > 0.5D) {
      paramFloat3 = (paramFloat5 / Math.min(f1 / 2.0F, f2 / 2.0F) - 0.5F) / 0.4F;
      paramFloat3 = Math.min(1.0F, paramFloat3);
      paramFloat3 = 1.0F - 0.13877845F * paramFloat3;
    } else {
      paramFloat3 = 1.0F;
    } 
    if (paramFloat5 / Math.min(f1 / 2.0F, f2 / 2.0F) > 0.6F) {
      paramFloat4 = (paramFloat5 / Math.min(f1 / 2.0F, f2 / 2.0F) - 0.6F) / 0.3F;
      paramFloat4 = Math.min(1.0F, paramFloat4);
      paramFloat4 = 0.042454004F * paramFloat4 + 1.0F;
    } else {
      paramFloat4 = 1.0F;
    } 
    this.mPath.moveTo(f1 / 2.0F + paramFloat1, paramFloat2);
    if (!paramBoolean2) {
      this.mPath.lineTo(paramFloat1 + f1, paramFloat2);
    } else {
      this.mPath.lineTo(Math.max(f1 / 2.0F, f1 - paramFloat5 / 100.0F * 128.19F * paramFloat3) + paramFloat1, paramFloat2);
      this.mPath.cubicTo(paramFloat1 + f1 - paramFloat5 / 100.0F * 83.62F * paramFloat4, paramFloat2, paramFloat1 + f1 - paramFloat5 / 100.0F * 67.45F, paramFloat2 + paramFloat5 / 100.0F * 4.64F, paramFloat1 + f1 - paramFloat5 / 100.0F * 51.16F, paramFloat2 + paramFloat5 / 100.0F * 13.36F);
      this.mPath.cubicTo(paramFloat1 + f1 - paramFloat5 / 100.0F * 34.86F, paramFloat2 + paramFloat5 / 100.0F * 22.07F, paramFloat1 + f1 - paramFloat5 / 100.0F * 22.07F, paramFloat2 + paramFloat5 / 100.0F * 34.86F, paramFloat1 + f1 - paramFloat5 / 100.0F * 13.36F, paramFloat2 + paramFloat5 / 100.0F * 51.16F);
      this.mPath.cubicTo(paramFloat1 + f1 - paramFloat5 / 100.0F * 4.64F, paramFloat2 + paramFloat5 / 100.0F * 67.45F, paramFloat1 + f1, paramFloat2 + paramFloat5 / 100.0F * 83.62F * paramFloat4, paramFloat1 + f1, paramFloat2 + Math.min(f2 / 2.0F, paramFloat5 / 100.0F * 128.19F * paramFloat3));
    } 
    if (!paramBoolean4) {
      this.mPath.lineTo(paramFloat1 + f1, paramFloat2 + f2);
    } else {
      this.mPath.lineTo(paramFloat1 + f1, Math.max(f2 / 2.0F, f2 - paramFloat5 / 100.0F * 128.19F * paramFloat3) + paramFloat2);
      this.mPath.cubicTo(paramFloat1 + f1, paramFloat2 + f2 - paramFloat5 / 100.0F * 83.62F * paramFloat4, paramFloat1 + f1 - paramFloat5 / 100.0F * 4.64F, paramFloat2 + f2 - paramFloat5 / 100.0F * 67.45F, paramFloat1 + f1 - paramFloat5 / 100.0F * 13.36F, paramFloat2 + f2 - paramFloat5 / 100.0F * 51.16F);
      this.mPath.cubicTo(paramFloat1 + f1 - paramFloat5 / 100.0F * 22.07F, paramFloat2 + f2 - paramFloat5 / 100.0F * 34.86F, paramFloat1 + f1 - paramFloat5 / 100.0F * 34.86F, paramFloat2 + f2 - paramFloat5 / 100.0F * 22.07F, paramFloat1 + f1 - paramFloat5 / 100.0F * 51.16F, paramFloat2 + f2 - paramFloat5 / 100.0F * 13.36F);
      this.mPath.cubicTo(paramFloat1 + f1 - paramFloat5 / 100.0F * 67.45F, paramFloat2 + f2 - paramFloat5 / 100.0F * 4.64F, paramFloat1 + f1 - paramFloat5 / 100.0F * 83.62F * paramFloat4, paramFloat2 + f2, paramFloat1 + Math.max(f1 / 2.0F, f1 - paramFloat5 / 100.0F * 128.19F * paramFloat3), paramFloat2 + f2);
    } 
    if (!paramBoolean3) {
      this.mPath.lineTo(paramFloat1, paramFloat2 + f2);
    } else {
      this.mPath.lineTo(Math.min(f1 / 2.0F, paramFloat5 / 100.0F * 128.19F * paramFloat3) + paramFloat1, paramFloat2 + f2);
      this.mPath.cubicTo(paramFloat1 + paramFloat5 / 100.0F * 83.62F * paramFloat4, paramFloat2 + f2, paramFloat1 + paramFloat5 / 100.0F * 67.45F, paramFloat2 + f2 - paramFloat5 / 100.0F * 4.64F, paramFloat1 + paramFloat5 / 100.0F * 51.16F, paramFloat2 + f2 - paramFloat5 / 100.0F * 13.36F);
      this.mPath.cubicTo(paramFloat1 + paramFloat5 / 100.0F * 34.86F, paramFloat2 + f2 - paramFloat5 / 100.0F * 22.07F, paramFloat1 + paramFloat5 / 100.0F * 22.07F, paramFloat2 + f2 - paramFloat5 / 100.0F * 34.86F, paramFloat1 + paramFloat5 / 100.0F * 13.36F, paramFloat2 + f2 - paramFloat5 / 100.0F * 51.16F);
      this.mPath.cubicTo(paramFloat5 / 100.0F * 4.64F + paramFloat1, paramFloat2 + f2 - paramFloat5 / 100.0F * 67.45F, paramFloat1, paramFloat2 + f2 - paramFloat5 / 100.0F * 83.62F * paramFloat4, paramFloat1, paramFloat2 + Math.max(f2 / 2.0F, f2 - paramFloat5 / 100.0F * 128.19F * paramFloat3));
    } 
    if (!paramBoolean1) {
      this.mPath.lineTo(paramFloat1, paramFloat2);
    } else {
      this.mPath.lineTo(paramFloat1, Math.min(f2 / 2.0F, paramFloat5 / 100.0F * 128.19F * paramFloat3) + paramFloat2);
      this.mPath.cubicTo(paramFloat1, paramFloat2 + paramFloat5 / 100.0F * 83.62F * paramFloat4, paramFloat1 + paramFloat5 / 100.0F * 4.64F, paramFloat2 + paramFloat5 / 100.0F * 67.45F, paramFloat1 + paramFloat5 / 100.0F * 13.36F, paramFloat2 + paramFloat5 / 100.0F * 51.16F);
      this.mPath.cubicTo(paramFloat1 + paramFloat5 / 100.0F * 22.07F, paramFloat2 + paramFloat5 / 100.0F * 34.86F, paramFloat1 + paramFloat5 / 100.0F * 34.86F, paramFloat2 + paramFloat5 / 100.0F * 22.07F, paramFloat1 + paramFloat5 / 100.0F * 51.16F, paramFloat2 + paramFloat5 / 100.0F * 13.36F);
      this.mPath.cubicTo(paramFloat5 / 100.0F * 67.45F + paramFloat1, paramFloat5 / 100.0F * 4.64F + paramFloat2, paramFloat5 / 100.0F * 83.62F * paramFloat4 + paramFloat1, paramFloat2, paramFloat1 + Math.min(f1 / 2.0F, paramFloat5 / 100.0F * 128.19F * paramFloat3), paramFloat2);
    } 
    this.mPath.close();
    return this.mPath;
  }
}
