package com.oplus.util;

import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.Log;

public class OplusLog {
  private static final String KEY_EXTRA_TAG = "log.tag.extra";
  
  private static final String STACK_TAG = "StackTrace";
  
  private static final boolean STACK_TRACE = false;
  
  public static void v(String paramString1, String paramString2) {
    Log.v(paramString1, paramString2);
  }
  
  public static void v(boolean paramBoolean, String paramString1, String paramString2) {
    if (paramBoolean)
      Log.v(paramString1, paramString2); 
  }
  
  public static void v(String paramString1, String paramString2, Throwable paramThrowable) {
    Log.v(paramString1, paramString2, paramThrowable);
  }
  
  public static void v(boolean paramBoolean, String paramString1, String paramString2, Throwable paramThrowable) {
    if (paramBoolean)
      Log.v(paramString1, paramString2, paramThrowable); 
  }
  
  public static void v(Class<?> paramClass, Object... paramVarArgs) {
    Log.v(getTag(paramClass), buildMessage(paramVarArgs));
  }
  
  public static void v(String paramString1, String paramString2, Object... paramVarArgs) {
    if (getDebug(paramString1))
      Log.v(paramString2, buildMessage(paramVarArgs)); 
  }
  
  public static void v(String paramString, Class<?> paramClass, Object... paramVarArgs) {
    if (getDebug(paramString))
      Log.v(getTag(paramClass), buildMessage(paramVarArgs)); 
  }
  
  public static void v(String paramString1, String paramString2, Throwable paramThrowable, Object... paramVarArgs) {
    if (getDebug(paramString1))
      Log.v(paramString2, buildMessage(paramVarArgs), paramThrowable); 
  }
  
  public static void v(String paramString, Class<?> paramClass, Throwable paramThrowable, Object... paramVarArgs) {
    if (getDebug(paramString))
      Log.v(getTag(paramClass), buildMessage(paramVarArgs), paramThrowable); 
  }
  
  public static void d(String paramString1, String paramString2) {
    Log.d(paramString1, paramString2);
  }
  
  public static void d(boolean paramBoolean, String paramString1, String paramString2) {
    if (paramBoolean)
      Log.d(paramString1, paramString2); 
  }
  
  public static void d(String paramString1, String paramString2, Throwable paramThrowable) {
    Log.d(paramString1, paramString2, paramThrowable);
  }
  
  public static void d(boolean paramBoolean, String paramString1, String paramString2, Throwable paramThrowable) {
    if (paramBoolean)
      Log.d(paramString1, paramString2, paramThrowable); 
  }
  
  public static void d(Class<?> paramClass, Object... paramVarArgs) {
    Log.d(getTag(paramClass), buildMessage(paramVarArgs));
  }
  
  public static void d(String paramString1, String paramString2, Object... paramVarArgs) {
    if (getDebug(paramString1))
      Log.d(paramString2, buildMessage(paramVarArgs)); 
  }
  
  public static void d(String paramString, Class<?> paramClass, Object... paramVarArgs) {
    if (getDebug(paramString))
      Log.d(getTag(paramClass), buildMessage(paramVarArgs)); 
  }
  
  public static void d(String paramString1, String paramString2, Throwable paramThrowable, Object... paramVarArgs) {
    if (getDebug(paramString1))
      Log.d(paramString2, buildMessage(paramVarArgs), paramThrowable); 
  }
  
  public static void d(String paramString, Class<?> paramClass, Throwable paramThrowable, Object... paramVarArgs) {
    if (getDebug(paramString))
      Log.d(getTag(paramClass), buildMessage(paramVarArgs), paramThrowable); 
  }
  
  public static void i(String paramString1, String paramString2) {
    Log.i(paramString1, paramString2);
  }
  
  public static void i(boolean paramBoolean, String paramString1, String paramString2) {
    if (paramBoolean)
      Log.i(paramString1, paramString2); 
  }
  
  public static void i(String paramString1, String paramString2, Throwable paramThrowable) {
    Log.i(paramString1, paramString2, paramThrowable);
  }
  
  public static void i(boolean paramBoolean, String paramString1, String paramString2, Throwable paramThrowable) {
    if (paramBoolean)
      Log.i(paramString1, paramString2, paramThrowable); 
  }
  
  public static void i(Class<?> paramClass, Object... paramVarArgs) {
    Log.i(getTag(paramClass), buildMessage(paramVarArgs));
  }
  
  public static void i(String paramString1, String paramString2, Throwable paramThrowable, Object... paramVarArgs) {
    if (getDebug(paramString1))
      Log.i(paramString2, buildMessage(paramVarArgs), paramThrowable); 
  }
  
  public static void i(String paramString1, String paramString2, Object... paramVarArgs) {
    if (getDebug(paramString1))
      Log.i(paramString2, buildMessage(paramVarArgs)); 
  }
  
  public static void i(String paramString, Class<?> paramClass, Object... paramVarArgs) {
    if (getDebug(paramString))
      Log.i(getTag(paramClass), buildMessage(paramVarArgs)); 
  }
  
  public static void i(String paramString, Class<?> paramClass, Throwable paramThrowable, Object... paramVarArgs) {
    if (getDebug(paramString))
      Log.i(getTag(paramClass), buildMessage(paramVarArgs), paramThrowable); 
  }
  
  public static void w(String paramString1, String paramString2) {
    Log.w(paramString1, paramString2);
  }
  
  public static void w(boolean paramBoolean, String paramString1, String paramString2) {
    if (paramBoolean)
      Log.w(paramString1, paramString2); 
  }
  
  public static void w(String paramString1, String paramString2, Throwable paramThrowable) {
    Log.w(paramString1, paramString2, paramThrowable);
  }
  
  public static void w(boolean paramBoolean, String paramString1, String paramString2, Throwable paramThrowable) {
    if (paramBoolean)
      Log.w(paramString1, paramString2, paramThrowable); 
  }
  
  public static void w(Class<?> paramClass, Object... paramVarArgs) {
    Log.w(getTag(paramClass), buildMessage(paramVarArgs));
  }
  
  public static void w(String paramString1, String paramString2, Object... paramVarArgs) {
    if (getDebug(paramString1))
      Log.w(paramString2, buildMessage(paramVarArgs)); 
  }
  
  public static void w(String paramString, Class<?> paramClass, Object... paramVarArgs) {
    if (getDebug(paramString))
      Log.w(getTag(paramClass), buildMessage(paramVarArgs)); 
  }
  
  public static void w(String paramString1, String paramString2, Throwable paramThrowable, Object... paramVarArgs) {
    if (getDebug(paramString1))
      Log.w(paramString2, buildMessage(paramVarArgs), paramThrowable); 
  }
  
  public static void w(String paramString, Class<?> paramClass, Throwable paramThrowable, Object... paramVarArgs) {
    if (getDebug(paramString))
      Log.w(getTag(paramClass), buildMessage(paramVarArgs), paramThrowable); 
  }
  
  public static void e(String paramString1, String paramString2) {
    Log.e(paramString1, paramString2);
  }
  
  public static void e(boolean paramBoolean, String paramString1, String paramString2) {
    if (paramBoolean)
      Log.e(paramString1, paramString2); 
  }
  
  public static void e(Class<?> paramClass, Object... paramVarArgs) {
    Log.e(getTag(paramClass), buildMessage(paramVarArgs));
  }
  
  public static void e(String paramString1, String paramString2, Object... paramVarArgs) {
    if (getDebug(paramString1))
      Log.e(paramString2, buildMessage(paramVarArgs)); 
  }
  
  public static void e(String paramString, Class<?> paramClass, Object... paramVarArgs) {
    if (getDebug(paramString))
      Log.e(getTag(paramClass), buildMessage(paramVarArgs)); 
  }
  
  public static void e(String paramString1, String paramString2, Throwable paramThrowable) {
    Log.e(paramString1, paramString2, paramThrowable);
  }
  
  public static void e(boolean paramBoolean, String paramString1, String paramString2, Throwable paramThrowable) {
    if (paramBoolean)
      Log.e(paramString1, paramString2, paramThrowable); 
  }
  
  public static void e(String paramString1, String paramString2, Throwable paramThrowable, Object... paramVarArgs) {
    if (getDebug(paramString1))
      Log.e(paramString2, buildMessage(paramVarArgs), paramThrowable); 
  }
  
  public static void e(String paramString, Class<?> paramClass, Throwable paramThrowable, Object... paramVarArgs) {
    if (getDebug(paramString))
      Log.e(getTag(paramClass), buildMessage(paramVarArgs), paramThrowable); 
  }
  
  public static void wtf(String paramString1, String paramString2) {
    Log.wtf(paramString1, paramString2);
  }
  
  public static void wtf(boolean paramBoolean, String paramString1, String paramString2) {
    if (paramBoolean)
      Log.wtf(paramString1, paramString2); 
  }
  
  public static void wtf(String paramString, Throwable paramThrowable) {
    Log.wtf(paramString, paramThrowable);
  }
  
  public static void wtf(boolean paramBoolean, String paramString, Throwable paramThrowable) {
    if (paramBoolean)
      Log.wtf(paramString, paramThrowable); 
  }
  
  public static void wtf(String paramString1, String paramString2, Object... paramVarArgs) {
    if (getDebug(paramString1))
      Log.wtf(paramString2, new Exception(buildMessage(paramVarArgs))); 
  }
  
  public static void wtf(String paramString, Class<?> paramClass, Object... paramVarArgs) {
    if (getDebug(paramString))
      Log.wtf(getTag(paramClass), new Exception(buildMessage(paramVarArgs))); 
  }
  
  public static void printStackTrace(StackTraceElement[] paramArrayOfStackTraceElement, String paramString, boolean paramBoolean) {}
  
  public static boolean getDebug(String paramString) {
    return "1".equals(SystemProperties.get(paramString));
  }
  
  public static String getTag(Class<?> paramClass) {
    return joinString(new String[] { getExtraTag(), paramClass.getSimpleName() });
  }
  
  public static String getTag(Object paramObject) {
    return getTag(paramObject.getClass());
  }
  
  public static String buildMessage(Object[] paramArrayOfObject) {
    if (paramArrayOfObject == null)
      return ""; 
    StringBuilder stringBuilder = new StringBuilder();
    int i;
    byte b;
    for (i = paramArrayOfObject.length, b = 0; b < i; ) {
      Object object = paramArrayOfObject[b];
      if (object != null)
        stringBuilder.append(object.toString()); 
      b++;
    } 
    return stringBuilder.toString();
  }
  
  public static String joinString(String... paramVarArgs) {
    return buildMessage((Object[])paramVarArgs);
  }
  
  private static String getExtraTag() {
    String str = SystemProperties.get("log.tag.extra");
    if (TextUtils.isEmpty(str)) {
      str = "";
    } else {
      str = joinString(new String[] { str, ":" });
    } 
    return str;
  }
}
