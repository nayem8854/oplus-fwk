package com.oplus.util;

import android.content.Context;
import android.opengl.Matrix;
import android.provider.Settings;

public class OplusReduceSaturationUtil {
  private static final float[] DEFAULT_MATRIX_GRAYSCALE = new float[] { 
      0.2126F, 0.2126F, 0.2126F, 0.0F, 0.7152F, 0.7152F, 0.7152F, 0.0F, 0.0722F, 0.0722F, 
      0.0722F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F };
  
  private static final String DISPLAY_ADJUST_URI = "color_dispaly_adjust";
  
  private static final float[] MATRIX_INVERT_COLOR = new float[] { 
      -0.08247F, -0.08247F, -0.08247F, 0.0F, -0.08247F, -0.08247F, -0.08247F, 0.0F, -0.08247F, -0.08247F, 
      -0.08247F, 0.0F, 0.31615F, 0.31615F, 0.31615F, 1.0F };
  
  private static float[] multiply(float[] paramArrayOffloat1, float[] paramArrayOffloat2) {
    if (paramArrayOffloat1 == null)
      return paramArrayOffloat2; 
    float[] arrayOfFloat = new float[16];
    Matrix.multiplyMM(arrayOfFloat, 0, paramArrayOffloat1, 0, paramArrayOffloat2, 0);
    return arrayOfFloat;
  }
  
  public static void setReduceOn(Context paramContext, Boolean paramBoolean) {
    double[] arrayOfDouble = new double[16];
    if (paramBoolean.booleanValue()) {
      byte b = 0;
      while (true) {
        float[] arrayOfFloat = DEFAULT_MATRIX_GRAYSCALE;
        if (b < arrayOfFloat.length) {
          arrayOfDouble[b] = arrayOfFloat[b];
          b++;
          continue;
        } 
        break;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(Double.toString(arrayOfDouble[0]));
      stringBuilder.append(",");
      String str = stringBuilder.toString();
      for (b = 1; b < arrayOfDouble.length; b++) {
        if (b != arrayOfDouble.length - 1) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append(str);
          stringBuilder1.append(Double.toString(arrayOfDouble[b]));
          stringBuilder1.append(",");
          str = stringBuilder1.toString();
        } else {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append(str);
          stringBuilder1.append(Double.toString(arrayOfDouble[b]));
          str = stringBuilder1.toString();
        } 
      } 
    } else {
      paramBoolean = null;
    } 
    Settings.System.putString(paramContext.getContentResolver(), "color_dispaly_adjust", (String)paramBoolean);
  }
  
  public static void setInverseOn(Context paramContext, Boolean paramBoolean) {
    double[] arrayOfDouble = new double[16];
    float[] arrayOfFloat = multiply(DEFAULT_MATRIX_GRAYSCALE, MATRIX_INVERT_COLOR);
    if (paramBoolean.booleanValue()) {
      byte b;
      for (b = 0; b < arrayOfFloat.length; b++)
        arrayOfDouble[b] = arrayOfFloat[b]; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(Double.toString(arrayOfDouble[0]));
      stringBuilder.append(",");
      String str = stringBuilder.toString();
      for (b = 1; b < arrayOfDouble.length; b++) {
        if (b != arrayOfDouble.length - 1) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append(str);
          stringBuilder1.append(Double.toString(arrayOfDouble[b]));
          stringBuilder1.append(",");
          str = stringBuilder1.toString();
        } else {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append(str);
          stringBuilder1.append(Double.toString(arrayOfDouble[b]));
          str = stringBuilder1.toString();
        } 
      } 
    } else {
      paramBoolean = null;
    } 
    Settings.System.putString(paramContext.getContentResolver(), "color_dispaly_adjust", (String)paramBoolean);
  }
}
