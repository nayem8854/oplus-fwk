package com.oplus.util;

public interface IOplusBaseServiceManager {
  public static final int OPLUS_CALL_TRANSACTION_INDEX = 10000;
  
  public static final int OPLUS_FIRST_CALL_TRANSACTION = 10001;
}
