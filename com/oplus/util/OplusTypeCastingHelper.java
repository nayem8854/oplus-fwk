package com.oplus.util;

public final class OplusTypeCastingHelper {
  public static <T> T typeCasting(Class<T> paramClass, Object paramObject) {
    if (paramObject != null && paramClass.isInstance(paramObject))
      return (T)paramObject; 
    return null;
  }
}
