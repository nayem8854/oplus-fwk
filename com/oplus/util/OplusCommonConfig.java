package com.oplus.util;

import android.app.OplusActivityManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.UserHandle;
import android.util.Log;

public class OplusCommonConfig {
  private static final String TAG = "OplusCommonConfig";
  
  public static final int TO_AMS = 1;
  
  public static final int TO_PMS = 2;
  
  private static volatile OplusCommonConfig sConfig = null;
  
  private OplusActivityManager mOppoAm = null;
  
  private OplusCommonConfig() {
    this.mOppoAm = new OplusActivityManager();
  }
  
  public static OplusCommonConfig getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/util/OplusCommonConfig.sConfig : Lcom/oplus/util/OplusCommonConfig;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/util/OplusCommonConfig
    //   8: monitorenter
    //   9: getstatic com/oplus/util/OplusCommonConfig.sConfig : Lcom/oplus/util/OplusCommonConfig;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/util/OplusCommonConfig
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/util/OplusCommonConfig.sConfig : Lcom/oplus/util/OplusCommonConfig;
    //   27: ldc com/oplus/util/OplusCommonConfig
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/util/OplusCommonConfig
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/util/OplusCommonConfig.sConfig : Lcom/oplus/util/OplusCommonConfig;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #41	-> 0
    //   #42	-> 6
    //   #43	-> 9
    //   #44	-> 15
    //   #46	-> 27
    //   #48	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public boolean putConfigInfo(String paramString, Bundle paramBundle, int paramInt) {
    if (this.mOppoAm == null)
      this.mOppoAm = new OplusActivityManager(); 
    OplusActivityManager oplusActivityManager = this.mOppoAm;
    if (oplusActivityManager != null)
      try {
        return oplusActivityManager.putConfigInfo(paramString, paramBundle, paramInt, UserHandle.myUserId());
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("putConfigInfo ");
        stringBuilder.append(paramString);
        stringBuilder.append(" failed!");
        Log.e("OplusCommonConfig", stringBuilder.toString());
      }  
    return false;
  }
  
  public boolean putConfigInfoAsUser(String paramString, Bundle paramBundle, int paramInt1, int paramInt2) {
    if (this.mOppoAm == null)
      this.mOppoAm = new OplusActivityManager(); 
    OplusActivityManager oplusActivityManager = this.mOppoAm;
    if (oplusActivityManager != null)
      try {
        return oplusActivityManager.putConfigInfo(paramString, paramBundle, paramInt1, paramInt2);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("putConfigInfoAsUser ");
        stringBuilder.append(paramString);
        stringBuilder.append(" failed!");
        Log.e("OplusCommonConfig", stringBuilder.toString());
      }  
    return false;
  }
  
  public Bundle getConfigInfo(String paramString, int paramInt) {
    if (this.mOppoAm == null)
      this.mOppoAm = new OplusActivityManager(); 
    OplusActivityManager oplusActivityManager = this.mOppoAm;
    if (oplusActivityManager != null)
      try {
        return oplusActivityManager.getConfigInfo(paramString, paramInt, UserHandle.myUserId());
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("getConfigInfo ");
        stringBuilder.append(paramString);
        stringBuilder.append(" failed!");
        Log.e("OplusCommonConfig", stringBuilder.toString());
      }  
    return null;
  }
  
  public Bundle getConfigInfoAsUser(String paramString, int paramInt1, int paramInt2) {
    if (this.mOppoAm == null)
      this.mOppoAm = new OplusActivityManager(); 
    OplusActivityManager oplusActivityManager = this.mOppoAm;
    if (oplusActivityManager != null)
      try {
        return oplusActivityManager.getConfigInfo(paramString, paramInt1, paramInt2);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("getConfigInfoAsUser ");
        stringBuilder.append(paramString);
        stringBuilder.append(" failed!");
        Log.e("OplusCommonConfig", stringBuilder.toString());
      }  
    return null;
  }
}
