package com.oplus.util;

import android.content.res.Resources;
import android.view.View;

public class OplusViewUtil {
  private static final String TAG = "OplusViewUtil";
  
  public static int makeUnspecifiedMeasureSpec() {
    return View.MeasureSpec.makeMeasureSpec(0, 0);
  }
  
  public static int makeAtMostMeasureSpec(int paramInt) {
    return View.MeasureSpec.makeMeasureSpec(paramInt, -2147483648);
  }
  
  public static int makeExactlyMeasureSpec(int paramInt) {
    return View.MeasureSpec.makeMeasureSpec(paramInt, 1073741824);
  }
  
  public static String dumpViewDetail(View paramView) {
    StringBuilder stringBuilder = new StringBuilder();
    try {
      stringBuilder.append(paramView.getClass().getName());
      stringBuilder.append('{');
      stringBuilder.append(Integer.toHexString(System.identityHashCode(paramView)));
      stringBuilder.append(' ');
      stringBuilder.append(paramView.getLeft());
      stringBuilder.append(',');
      stringBuilder.append(paramView.getTop());
      stringBuilder.append('-');
      stringBuilder.append(paramView.getRight());
      stringBuilder.append(',');
      stringBuilder.append(paramView.getBottom());
      int i = paramView.getId();
      if (i != -1) {
        stringBuilder.append(" #");
        stringBuilder.append(Integer.toHexString(i));
        OplusResourcesUtil.dumpResourceInternal(paramView.getResources(), i, stringBuilder, false);
      } 
      stringBuilder.append("}");
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    return stringBuilder.toString();
  }
  
  public static String dumpView(View paramView) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramView.getClass().getName());
    int i = paramView.getId();
    if (i != -1) {
      stringBuilder.append("[");
      OplusResourcesUtil.dumpResourceInternal(paramView.getResources(), i, stringBuilder, false);
      stringBuilder.append("]");
    } 
    return stringBuilder.toString();
  }
  
  public static String getIdName(Resources paramResources, int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    if (paramInt != -1)
      OplusResourcesUtil.dumpResourceInternal(paramResources, paramInt, stringBuilder, true); 
    return stringBuilder.toString();
  }
  
  public static String getViewIdName(View paramView) {
    return getIdName(paramView.getResources(), paramView.getId());
  }
}
