package com.oplus.util;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class OplusDisplayCompatData implements Parcelable {
  public static final Parcelable.Creator<OplusDisplayCompatData> CREATOR = new Parcelable.Creator<OplusDisplayCompatData>() {
      public OplusDisplayCompatData createFromParcel(Parcel param1Parcel) {
        return new OplusDisplayCompatData(param1Parcel);
      }
      
      public OplusDisplayCompatData[] newArray(int param1Int) {
        return new OplusDisplayCompatData[param1Int];
      }
    };
  
  private boolean mEnableDisplayCompat = true;
  
  private boolean mHasHeteromorphismFeature = false;
  
  private boolean mRusImmersiveDefault = false;
  
  private List<String> mWhiteList = new ArrayList<>();
  
  private List<String> mBlackList = new ArrayList<>();
  
  private HashMap<String, String> mCompatPackageList = new HashMap<>();
  
  private List<String> mLocalCompatList = new ArrayList<>();
  
  private List<String> mLocalFullScreenList = new ArrayList<>();
  
  private List<String> mLocalCutoutDefaultList = new ArrayList<>();
  
  private List<String> mLocalCutoutShowList = new ArrayList<>();
  
  private List<String> mLocalCutoutHideList = new ArrayList<>();
  
  private List<String> mInstalledCompatList = new ArrayList<>();
  
  private List<String> mInstalledImeList = new ArrayList<>();
  
  private List<String> mShowDialogAppsList = new ArrayList<>();
  
  private List<String> mRusImmersiveList = new ArrayList<>();
  
  private List<String> mRusNonImmersiveList = new ArrayList<>();
  
  private List<String> mInstalledThirdPartyAppList = new ArrayList<>();
  
  private List<String> mNeedAdjustSizeList = new ArrayList<>();
  
  private List<String> mCutoutLeftBlackList = new ArrayList<>();
  
  private int mDisplayCutoutType = 0;
  
  private static final boolean DBG = false;
  
  private static final String TAG = "OplusDisplayCompatData";
  
  public OplusDisplayCompatData(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeStringList(this.mWhiteList);
    paramParcel.writeStringList(this.mBlackList);
    paramParcel.writeStringList(this.mRusImmersiveList);
    paramParcel.writeStringList(this.mRusNonImmersiveList);
    paramParcel.writeStringList(this.mLocalCompatList);
    paramParcel.writeStringList(this.mLocalFullScreenList);
    paramParcel.writeStringList(this.mLocalCutoutDefaultList);
    paramParcel.writeStringList(this.mLocalCutoutShowList);
    paramParcel.writeStringList(this.mLocalCutoutHideList);
    paramParcel.writeStringList(this.mInstalledCompatList);
    paramParcel.writeStringList(this.mInstalledImeList);
    paramParcel.writeStringList(this.mInstalledThirdPartyAppList);
    paramParcel.writeStringList(this.mShowDialogAppsList);
    paramParcel.writeStringList(this.mNeedAdjustSizeList);
    paramParcel.writeMap(this.mCompatPackageList);
    paramParcel.writeByte((byte)this.mEnableDisplayCompat);
    paramParcel.writeByte((byte)this.mHasHeteromorphismFeature);
    paramParcel.writeByte((byte)this.mRusImmersiveDefault);
    paramParcel.writeInt(this.mDisplayCutoutType);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    boolean bool2;
    this.mWhiteList = paramParcel.createStringArrayList();
    this.mBlackList = paramParcel.createStringArrayList();
    this.mRusImmersiveList = paramParcel.createStringArrayList();
    this.mRusNonImmersiveList = paramParcel.createStringArrayList();
    this.mLocalCompatList = paramParcel.createStringArrayList();
    this.mLocalFullScreenList = paramParcel.createStringArrayList();
    this.mLocalCutoutDefaultList = paramParcel.createStringArrayList();
    this.mLocalCutoutShowList = paramParcel.createStringArrayList();
    this.mLocalCutoutHideList = paramParcel.createStringArrayList();
    this.mInstalledCompatList = paramParcel.createStringArrayList();
    this.mInstalledImeList = paramParcel.createStringArrayList();
    this.mInstalledThirdPartyAppList = paramParcel.createStringArrayList();
    this.mShowDialogAppsList = paramParcel.createStringArrayList();
    this.mNeedAdjustSizeList = paramParcel.createStringArrayList();
    this.mCompatPackageList = paramParcel.readHashMap(HashMap.class.getClassLoader());
    byte b = paramParcel.readByte();
    boolean bool1 = true;
    if (b != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mEnableDisplayCompat = bool2;
    if (paramParcel.readByte() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mHasHeteromorphismFeature = bool2;
    if (paramParcel.readByte() != 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.mRusImmersiveDefault = bool2;
    this.mDisplayCutoutType = paramParcel.readInt();
  }
  
  public HashMap<String, String> getCompatPackageList() {
    return this.mCompatPackageList;
  }
  
  public void setCompatPackageList(HashMap<String, String> paramHashMap) {
    this.mCompatPackageList = paramHashMap;
  }
  
  public boolean getDisplayCompatEnabled() {
    return this.mEnableDisplayCompat;
  }
  
  public void setDisplatOptEnabled(boolean paramBoolean) {
    this.mEnableDisplayCompat = paramBoolean;
  }
  
  public boolean hasHeteromorphismFeature() {
    return this.mHasHeteromorphismFeature;
  }
  
  public void setHasHeteromorphismFeature(boolean paramBoolean) {
    this.mHasHeteromorphismFeature = paramBoolean;
  }
  
  public List<String> getWhiteList() {
    return this.mWhiteList;
  }
  
  public void setWhiteList(List<String> paramList) {
    this.mWhiteList = paramList;
  }
  
  public List<String> getBlackList() {
    return this.mBlackList;
  }
  
  public void setBlackList(List<String> paramList) {
    this.mBlackList = paramList;
  }
  
  public List<String> getLocalCompatList() {
    return this.mLocalCompatList;
  }
  
  public void setLocalCompatList(List<String> paramList) {
    this.mLocalCompatList = paramList;
  }
  
  public List<String> getLocalFullScreenList() {
    return this.mLocalFullScreenList;
  }
  
  public void setLocalFullScreenList(List<String> paramList) {
    this.mLocalFullScreenList = paramList;
  }
  
  public List<String> getLocalNonImmersiveList() {
    return this.mLocalCutoutDefaultList;
  }
  
  public void setLocalNonImmersiveList(List<String> paramList) {
    this.mLocalCutoutDefaultList = paramList;
  }
  
  public List<String> getLocalImmersiveList() {
    return this.mLocalCutoutShowList;
  }
  
  public void setLocalImmersiveList(List<String> paramList) {
    this.mLocalCutoutShowList = paramList;
  }
  
  public List<String> getInstalledCompatList() {
    return this.mInstalledCompatList;
  }
  
  public void setInstalledCompatList(List<String> paramList) {
    this.mInstalledCompatList = paramList;
  }
  
  public List<String> getInstalledImeList() {
    return this.mInstalledImeList;
  }
  
  public void setInstalledImeList(List<String> paramList) {
    this.mInstalledImeList = paramList;
  }
  
  public List<String> getShowDialogAppList() {
    return this.mShowDialogAppsList;
  }
  
  public void setShowDialogAppList(List<String> paramList) {
    this.mShowDialogAppsList = paramList;
  }
  
  public boolean getRusImmersiveDefault() {
    return this.mRusImmersiveDefault;
  }
  
  public void setRusImmersiveDefault(boolean paramBoolean) {
    this.mRusImmersiveDefault = paramBoolean;
  }
  
  public List<String> getRusImmersiveList() {
    return this.mRusImmersiveList;
  }
  
  public void setRusImmersiveList(List<String> paramList) {
    this.mRusImmersiveList = paramList;
  }
  
  public List<String> getRusNonImmersiveList() {
    return this.mRusNonImmersiveList;
  }
  
  public void setRusNonImmersiveList(List<String> paramList) {
    this.mRusNonImmersiveList = paramList;
  }
  
  public List<String> getInstalledThirdPartyAppList() {
    return this.mInstalledThirdPartyAppList;
  }
  
  public void setInstalledThirdPartyAppList(List<String> paramList) {
    this.mInstalledThirdPartyAppList = paramList;
  }
  
  public List<String> getNeedAdjustSizeAppList() {
    return this.mNeedAdjustSizeList;
  }
  
  public void setNeedAdjustSizeAppList(List<String> paramList) {
    this.mNeedAdjustSizeList = paramList;
  }
  
  public int getDisplayCutoutType() {
    return this.mDisplayCutoutType;
  }
  
  public void setDisplayCutoutType(int paramInt) {
    this.mDisplayCutoutType = paramInt;
  }
  
  public void setLocalCutoutDefaultList(List<String> paramList) {
    this.mLocalCutoutDefaultList = paramList;
  }
  
  public List<String> getLocalCutoutDefaultList() {
    return this.mLocalCutoutDefaultList;
  }
  
  public void setLocalCutoutShowList(List<String> paramList) {
    this.mLocalCutoutShowList = paramList;
  }
  
  public List<String> getLocalCutoutShowList() {
    return this.mLocalCutoutShowList;
  }
  
  public void setLocalCutoutHideList(List<String> paramList) {
    this.mLocalCutoutHideList = paramList;
  }
  
  public List<String> getLocalCutoutHideList() {
    return this.mLocalCutoutHideList;
  }
  
  public OplusDisplayCompatData() {}
}
