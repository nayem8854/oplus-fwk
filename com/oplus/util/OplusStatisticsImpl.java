package com.oplus.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.SystemProperties;
import android.util.Slog;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import oplus.util.OplusStatistics;
import org.json.JSONArray;
import org.json.JSONObject;

public class OplusStatisticsImpl implements OplusStatistics.IOplusStatistics {
  private static final String APP_ID = "appId";
  
  private static final String APP_NAME = "appName";
  
  private static final String APP_PACKAGE = "appPackage";
  
  private static final String APP_VERSION = "appVersion";
  
  private static final String ATOM_EVENT_ID = "unusual_frequence_info";
  
  private static final int ATOM_ID = 20185;
  
  private static final String ATOM_LOG_MAP_KEY_CALL_COUNT = "call_count";
  
  private static final String ATOM_LOG_MAP_KEY_GAP_TIME = "gapTime";
  
  private static final String ATOM_LOG_TAG = "atomReport";
  
  private static final int COUNT_LIMIT = 20;
  
  private static final String DATA_TYPE = "dataType";
  
  private static final int DATA_TYPE_COMMON = 1006;
  
  private static final int DATA_TYPE_COMMON_LIST = 1010;
  
  private static final String EVENT_ID = "eventID";
  
  private static final long GAP_TIME_LIMIT = 10000L;
  
  private static final String LOG_MAP = "logMap";
  
  private static final String LOG_TAG = "logTag";
  
  private static final String MAP_LIST = "mapList";
  
  private static final String PKG_NAME_ATOM = "com.coloros.deepthinker";
  
  private static final String PKG_NAME_DCS = "com.nearme.statistics.rom";
  
  private static final String SERVICE_NAME_ATOM = "com.coloros.atom.services.AtomReceiverService";
  
  private static final String SERVICE_NAME_DCS = "com.nearme.statistics.rom.service.ReceiverService";
  
  private static final int SINGLE_DATA_MAX_LENGTH = 50000;
  
  private static final String SSOID = "ssoid";
  
  private static final String SYSTEM = "system";
  
  private static final String TAG = "OplusStatistics--";
  
  private int mAtomCount = 0;
  
  private long mAtomStartTime = 0L;
  
  private ExecutorService mSingleThreadExecutor = Executors.newSingleThreadExecutor();
  
  public void onCommonSync(Context paramContext, OplusStatistics.EventData paramEventData, int paramInt) {
    if (SystemProperties.getBoolean("persist.sys.assert.panic", false)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onCommon: ");
      stringBuilder.append(paramEventData);
      Slog.d("OplusStatistics--", stringBuilder.toString());
    } 
    if ((paramInt & 0x1) == 1)
      sendDataToDCS(paramContext, paramEventData, "logMap", getCommonObject(paramEventData.logMap).toString(), 1006); 
    if ((paramInt & 0x2) == 2) {
      if (this.mAtomCount == 0)
        this.mAtomStartTime = System.currentTimeMillis(); 
      this.mAtomCount++;
      sendDataToAtom(paramContext, paramEventData);
      long l = System.currentTimeMillis();
      if (l - this.mAtomStartTime > 10000L) {
        if (this.mAtomCount > 20) {
          HashMap<Object, Object> hashMap = new HashMap<>();
          hashMap.put("gapTime", String.valueOf(l - this.mAtomStartTime));
          hashMap.put("call_count", String.valueOf(this.mAtomCount));
          hashMap.put("appPackage", "system");
          hashMap.put("logTag", paramEventData.logTag);
          hashMap.put("eventID", paramEventData.eventId);
          hashMap.put("logMap", getCommonObject(paramEventData.logMap).toString());
          paramEventData = new OplusStatistics.EventData();
          paramEventData.appId = 20185;
          paramEventData.logTag = "atomReport";
          paramEventData.eventId = "unusual_frequence_info";
          paramEventData.logMap = hashMap;
          onCommon(paramContext, paramEventData, 1);
          Slog.w("OplusStatistics--", "onCommon too frequently ");
        } 
        this.mAtomCount = 0;
      } 
    } 
  }
  
  public void onCommon(final Context context, final OplusStatistics.EventData data, final int flagSendTo) {
    Runnable runnable = new Runnable() {
        final OplusStatisticsImpl this$0;
        
        final Context val$context;
        
        final OplusStatistics.EventData val$data;
        
        final int val$flagSendTo;
        
        public void run() {
          OplusStatisticsImpl.this.onCommonSync(context, data, flagSendTo);
        }
      };
    this.mSingleThreadExecutor.execute(runnable);
  }
  
  public void onCommon(final Context context, List<OplusStatistics.EventData> paramList, int paramInt) {
    Map<String, List<OplusStatistics.EventData>> map = groupEvent(paramList);
    for (List<OplusStatistics.EventData> list : map.values()) {
      final OplusStatistics.EventData firstEventData = list.get(0);
      if (SystemProperties.getBoolean("persist.sys.assert.panic", false)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onCommon: ");
        stringBuilder.append(eventData);
        Slog.d("OplusStatistics--", stringBuilder.toString());
      } 
      Runnable runnable = new Runnable() {
          final OplusStatisticsImpl this$0;
          
          final Context val$context;
          
          final List val$eventDataList;
          
          final OplusStatistics.EventData val$firstEventData;
          
          public void run() {
            JSONArray jSONArray = new JSONArray();
            for (OplusStatistics.EventData eventData : eventDataList) {
              JSONObject jSONObject = OplusStatisticsImpl.this.getCommonObject(eventData.logMap);
              jSONArray.put(jSONObject);
            } 
            String str = "";
            try {
              String str1 = jSONArray.toString();
              str = str1;
              int i = str1.length();
              if (i >= 50000)
                return; 
              str = str1;
            } catch (OutOfMemoryError outOfMemoryError) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("onCommon--Error:");
              stringBuilder.append(outOfMemoryError);
              Slog.d("OplusStatistics--", stringBuilder.toString());
            } 
            OplusStatisticsImpl.this.sendDataToDCS(context, firstEventData, "mapList", str, 1010);
          }
        };
      this.mSingleThreadExecutor.execute(runnable);
    } 
  }
  
  private void sendDataToAtom(Context paramContext, OplusStatistics.EventData paramEventData) {
    Intent intent = new Intent();
    intent.setComponent(new ComponentName("com.coloros.deepthinker", "com.coloros.atom.services.AtomReceiverService"));
    intent.putExtra("appId", paramEventData.appId);
    intent.putExtra("appPackage", "system");
    intent.putExtra("logTag", paramEventData.logTag);
    intent.putExtra("eventID", paramEventData.eventId);
    intent.putExtra("logMap", getCommonObject(paramEventData.logMap).toString());
    startService(paramContext, intent);
  }
  
  private void sendDataToDCS(Context paramContext, OplusStatistics.EventData paramEventData, String paramString1, String paramString2, int paramInt) {
    Intent intent = new Intent();
    intent.setComponent(new ComponentName("com.nearme.statistics.rom", "com.nearme.statistics.rom.service.ReceiverService"));
    intent.putExtra("appPackage", "system");
    intent.putExtra("appName", "system");
    intent.putExtra("appVersion", "system");
    intent.putExtra("ssoid", "system");
    intent.putExtra("appId", paramEventData.appId);
    intent.putExtra("logTag", paramEventData.logTag);
    intent.putExtra("eventID", paramEventData.eventId);
    intent.putExtra(paramString1, paramString2);
    intent.putExtra("dataType", paramInt);
    startService(paramContext, intent);
  }
  
  private JSONObject getCommonObject(Map<String, String> paramMap) {
    JSONObject jSONObject = new JSONObject();
    if (paramMap != null && !paramMap.isEmpty())
      try {
        for (String str : paramMap.keySet())
          jSONObject.put(str, paramMap.get(str)); 
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("getCommonObject Exception: ");
        stringBuilder.append(exception);
        Slog.w("OplusStatistics--", stringBuilder.toString());
      }  
    return jSONObject;
  }
  
  private void startService(Context paramContext, Intent paramIntent) {
    if (paramContext == null || paramIntent == null) {
      Slog.w("OplusStatistics--", "startDcsService failed, Params is null.");
      return;
    } 
    try {
      paramContext.startService(paramIntent);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("startDcsService Exception: ");
      stringBuilder.append(exception);
      Slog.w("OplusStatistics--", stringBuilder.toString());
    } 
  }
  
  private Map<String, List<OplusStatistics.EventData>> groupEvent(List<OplusStatistics.EventData> paramList) {
    HashMap<Object, Object> hashMap = new HashMap<>();
    for (OplusStatistics.EventData eventData : paramList) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(eventData.appId);
      stringBuilder.append(eventData.logTag);
      stringBuilder.append(eventData.eventId);
      String str = stringBuilder.toString();
      List<OplusStatistics.EventData> list2 = (List)hashMap.get(str);
      List<OplusStatistics.EventData> list1 = list2;
      if (list2 == null) {
        list1 = new ArrayList();
        hashMap.put(str, list1);
      } 
      list1.add(eventData);
    } 
    return (Map)hashMap;
  }
}
