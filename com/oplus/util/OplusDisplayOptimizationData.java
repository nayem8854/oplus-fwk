package com.oplus.util;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public final class OplusDisplayOptimizationData implements Parcelable {
  public static final Parcelable.Creator<OplusDisplayOptimizationData> CREATOR = new Parcelable.Creator<OplusDisplayOptimizationData>() {
      public OplusDisplayOptimizationData createFromParcel(Parcel param1Parcel) {
        return new OplusDisplayOptimizationData(param1Parcel);
      }
      
      public OplusDisplayOptimizationData[] newArray(int param1Int) {
        return new OplusDisplayOptimizationData[param1Int];
      }
    };
  
  private boolean mEnableDisplatOpt = true;
  
  private boolean mEnableGraphicAccelerationSwitch = true;
  
  private List<String> mExcludeProcessList = new ArrayList<>();
  
  private List<String> mWhiteList = new ArrayList<>();
  
  private List<String> mBlackList = new ArrayList<>();
  
  private List<String> mSpecialList = new ArrayList<>();
  
  private List<String> mExcludeWindowList = new ArrayList<>();
  
  private int mEnablePolicy = 0;
  
  private static final boolean DBG = false;
  
  private static final String TAG = "OplusDisplayOptimizationData";
  
  public OplusDisplayOptimizationData(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeStringList(this.mExcludeProcessList);
    paramParcel.writeStringList(this.mWhiteList);
    paramParcel.writeStringList(this.mBlackList);
    paramParcel.writeStringList(this.mSpecialList);
    paramParcel.writeStringList(this.mExcludeWindowList);
    paramParcel.writeByte((byte)this.mEnableDisplatOpt);
    paramParcel.writeByte((byte)this.mEnableGraphicAccelerationSwitch);
    paramParcel.writeInt(this.mEnablePolicy);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    boolean bool2;
    this.mExcludeProcessList = paramParcel.createStringArrayList();
    this.mWhiteList = paramParcel.createStringArrayList();
    this.mBlackList = paramParcel.createStringArrayList();
    this.mSpecialList = paramParcel.createStringArrayList();
    this.mExcludeWindowList = paramParcel.createStringArrayList();
    byte b = paramParcel.readByte();
    boolean bool1 = true;
    if (b != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mEnableDisplatOpt = bool2;
    if (paramParcel.readByte() != 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.mEnableGraphicAccelerationSwitch = bool2;
    this.mEnablePolicy = paramParcel.readInt();
  }
  
  public List<String> getExcludeProcessList() {
    return this.mExcludeProcessList;
  }
  
  public void setExcludeProcessList(List<String> paramList) {
    this.mExcludeProcessList = paramList;
  }
  
  public boolean getDisplatOptEnabled() {
    return this.mEnableDisplatOpt;
  }
  
  public void setDisplatOptEnabled(boolean paramBoolean) {
    this.mEnableDisplatOpt = paramBoolean;
  }
  
  public List<String> getWhiteList() {
    return this.mWhiteList;
  }
  
  public void setWhiteList(List<String> paramList) {
    this.mWhiteList = paramList;
  }
  
  public List<String> getBlackList() {
    return this.mBlackList;
  }
  
  public void setBlackList(List<String> paramList) {
    this.mBlackList = paramList;
  }
  
  public List<String> getSpecialList() {
    return this.mSpecialList;
  }
  
  public void setSpecialList(List<String> paramList) {
    this.mSpecialList = paramList;
  }
  
  public List<String> getExcludeWindowList() {
    return this.mExcludeWindowList;
  }
  
  public void setExcludeWindowList(List<String> paramList) {
    this.mExcludeWindowList = paramList;
  }
  
  public boolean getGraphicAccelerationSwitchEnabled() {
    return this.mEnableGraphicAccelerationSwitch;
  }
  
  public void setGraphicAccelerationSwitchEnabled(boolean paramBoolean) {
    this.mEnableGraphicAccelerationSwitch = paramBoolean;
  }
  
  public int getEnablePolicy() {
    return this.mEnablePolicy;
  }
  
  public void setEnablePolicy(int paramInt) {
    this.mEnablePolicy = paramInt;
  }
  
  public OplusDisplayOptimizationData() {}
}
