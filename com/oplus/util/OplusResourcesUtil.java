package com.oplus.util;

import android.content.res.Resources;
import android.content.res.TypedArray;

public class OplusResourcesUtil {
  public static String[][] loadStringArrays(Resources paramResources, int paramInt) {
    TypedArray typedArray = paramResources.obtainTypedArray(paramInt);
    int i = typedArray.length();
    String[][] arrayOfString = new String[i][];
    for (paramInt = 0; paramInt < i; paramInt++) {
      int j = typedArray.getResourceId(paramInt, 0);
      if (j != 0)
        arrayOfString[paramInt] = paramResources.getStringArray(j); 
    } 
    typedArray.recycle();
    return arrayOfString;
  }
  
  public static String dumpResource(Resources paramResources, int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[");
    dumpResourceInternal(paramResources, paramInt, stringBuilder, false);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  static void dumpResourceInternal(Resources paramResources, int paramInt, StringBuilder paramStringBuilder, boolean paramBoolean) {
    if (paramResources == null)
      return; 
    if (!Resources.resourceHasPackage(paramInt))
      return; 
    int i = 0xFF000000 & paramInt;
    if (i != 16777216) {
      if (i != 2130706432) {
        try {
          String str1 = paramResources.getResourcePackageName(paramInt);
          String str2 = paramResources.getResourceTypeName(paramInt);
        } catch (android.content.res.Resources.NotFoundException notFoundException) {
        
        } catch (Exception exception) {}
      } else {
        String str1 = getAppPackageName(paramBoolean, (Resources)exception, paramInt);
        String str2 = exception.getResourceTypeName(paramInt);
      } 
    } else {
      String str1 = "android";
      String str2 = exception.getResourceTypeName(paramInt);
    } 
  }
  
  private static String getAppPackageName(boolean paramBoolean, Resources paramResources, int paramInt) {
    if (paramBoolean)
      return paramResources.getResourcePackageName(paramInt); 
    return "app";
  }
}
