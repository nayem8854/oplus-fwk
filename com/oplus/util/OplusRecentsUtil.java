package com.oplus.util;

import android.util.Log;

public class OplusRecentsUtil {
  public static final String LOCK_APPS_FILE_NAME = "locked_apps.xml";
  
  public static final int NAVIGATION_MODE_GUESTURE = 2;
  
  public static final String RECENT_TASK_FILES_PATH = "/data/oppo/coloros/recenttask";
  
  private static final String TAG = "OplusRecentsUtil";
  
  public static boolean sDebug = false;
  
  public static boolean isThumbnailNeedStretch(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7) {
    if (sDebug) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isThumbnailNeedStretch thumbnailWidth:");
      stringBuilder.append(paramInt1);
      stringBuilder.append(" thumbnailHeight:");
      stringBuilder.append(paramInt2);
      stringBuilder.append(" navigationMode:");
      stringBuilder.append(paramInt3);
      stringBuilder.append(" navigationMode:");
      stringBuilder.append(paramInt3);
      stringBuilder.append(" displayWidth:");
      stringBuilder.append(paramInt4);
      stringBuilder.append(" diplayHeight:");
      stringBuilder.append(paramInt5);
      stringBuilder.append(" navigationBarHight:");
      stringBuilder.append(paramInt6);
      stringBuilder.append(" statusBarHight:");
      stringBuilder.append(paramInt7);
      Log.d("OplusRecentsUtil", stringBuilder.toString());
    } 
    int i = Math.max(paramInt4, paramInt5);
    paramInt4 = Math.min(paramInt4, paramInt5);
    double d = Math.min(paramInt1, paramInt2) / Math.max(paramInt1, paramInt2);
    if (paramInt3 == 2) {
      if (sDebug) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("isThumbnailNeedStretch thumbnailRatio:");
        stringBuilder.append(d);
        stringBuilder.append(" (dw / dh):");
        stringBuilder.append(paramInt4 / i);
        stringBuilder.append(" ((dw - statusBarHight) / dh):");
        stringBuilder.append((paramInt4 - paramInt7) / i);
        Log.d("OplusRecentsUtil", stringBuilder.toString());
      } 
      return (d == paramInt4 / i || d == (paramInt4 - paramInt7) / i);
    } 
    if (sDebug) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isThumbnailNeedStretch thumbnailRatio:");
      stringBuilder.append(d);
      stringBuilder.append(" (dw / (dh - navigationBarHight)):");
      stringBuilder.append(paramInt4 / (i - paramInt6));
      stringBuilder.append(" ((dw - statusBarHight) / (dh - navigationBarHight)):");
      stringBuilder.append((paramInt4 - paramInt7) / (i - paramInt6));
      Log.d("OplusRecentsUtil", stringBuilder.toString());
    } 
    return (d == paramInt4 / (i - paramInt6) || d == (paramInt4 - paramInt7) / (i - paramInt6));
  }
}
