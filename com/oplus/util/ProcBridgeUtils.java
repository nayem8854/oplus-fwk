package com.oplus.util;

public class ProcBridgeUtils {
  private static final String TAG = "ProcBridgeUtils";
  
  public static void writeProcNode(String paramString1, String paramString2) {
    writeNativeProcNode(paramString1, paramString2);
  }
  
  private static native int writeNativeProcNode(String paramString1, String paramString2);
  
  public static String readProcNode(String paramString) {
    paramString = readNativeProcNode(paramString);
    if (paramString != null) {
      String[] arrayOfString = paramString.split("\n|\r");
      if (arrayOfString.length >= 1 && arrayOfString[0] != null)
        return arrayOfString[0]; 
    } 
    return null;
  }
  
  private static native String readNativeProcNode(String paramString);
}
