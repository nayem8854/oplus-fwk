package com.oplus.util;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public final class OplusSecureKeyboardData implements Parcelable {
  public static final Parcelable.Creator<OplusSecureKeyboardData> CREATOR = new Parcelable.Creator<OplusSecureKeyboardData>() {
      public OplusSecureKeyboardData createFromParcel(Parcel param1Parcel) {
        return new OplusSecureKeyboardData(param1Parcel);
      }
      
      public OplusSecureKeyboardData[] newArray(int param1Int) {
        return new OplusSecureKeyboardData[param1Int];
      }
    };
  
  private ArrayList<String> mList1 = new ArrayList<>();
  
  private ArrayList<String> mList2 = new ArrayList<>();
  
  private String mEnable = "true";
  
  public OplusSecureKeyboardData(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mEnable);
    paramParcel.writeStringList(this.mList1);
    paramParcel.writeStringList(this.mList2);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mEnable = paramParcel.readString();
    this.mList1 = paramParcel.createStringArrayList();
    this.mList2 = paramParcel.createStringArrayList();
  }
  
  public void setEnable(String paramString) {
    this.mEnable = paramString;
  }
  
  public String getEnable() {
    return this.mEnable;
  }
  
  public ArrayList<String> getNormalAppList() {
    return this.mList1;
  }
  
  public ArrayList<String> getInputMethodAppList() {
    return this.mList2;
  }
  
  public OplusSecureKeyboardData() {}
}
