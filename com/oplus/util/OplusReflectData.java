package com.oplus.util;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public class OplusReflectData implements Parcelable {
  static {
    ArrayList<OplusReflectWidget> arrayList = new ArrayList();
    arrayList.add(OplusReflectWidget.DEFAULT_WIDGET);
    mLocalReflectList.add(OplusReflectWidget.DEFAULT_WIDGET_WECHAT_1420);
  }
  
  public OplusReflectData() {
    this.mReflectEnable = true;
    this.mReflectAppList = new ArrayList<>();
    this.mReflectList = new ArrayList<>();
  }
  
  protected OplusReflectData(Parcel paramParcel) {
    boolean bool = true;
    this.mReflectEnable = true;
    this.mReflectAppList = new ArrayList<>();
    this.mReflectList = new ArrayList<>();
    if (paramParcel.readByte() == 0)
      bool = false; 
    this.mReflectEnable = bool;
    this.mReflectList = paramParcel.createTypedArrayList(OplusReflectWidget.CREATOR);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByte(this.mReflectEnable);
    paramParcel.writeTypedList(this.mReflectList);
  }
  
  public static final Parcelable.Creator<OplusReflectData> CREATOR = new Parcelable.Creator<OplusReflectData>() {
      public OplusReflectData createFromParcel(Parcel param1Parcel) {
        return new OplusReflectData(param1Parcel);
      }
      
      public OplusReflectData[] newArray(int param1Int) {
        return new OplusReflectData[param1Int];
      }
    };
  
  private static final ArrayList<OplusReflectWidget> mLocalReflectList;
  
  private ArrayList<OplusReflectWidget> mReflectAppList;
  
  private boolean mReflectEnable;
  
  private ArrayList<OplusReflectWidget> mReflectList;
  
  public boolean isReflectEnable() {
    return this.mReflectEnable;
  }
  
  public void setReflectEnable(boolean paramBoolean) {
    this.mReflectEnable = paramBoolean;
  }
  
  public ArrayList<OplusReflectWidget> getReflectList() {
    return this.mReflectList;
  }
  
  public void setReflectList(ArrayList<OplusReflectWidget> paramArrayList) {
    this.mReflectList = paramArrayList;
  }
  
  public void clearList() {
    ArrayList<OplusReflectWidget> arrayList = this.mReflectList;
    if (arrayList != null)
      arrayList.clear(); 
  }
  
  public void addReflectWidget(OplusReflectWidget paramOplusReflectWidget) {
    if (this.mReflectList == null)
      this.mReflectList = new ArrayList<>(); 
    this.mReflectList.add(paramOplusReflectWidget);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("OplusReflectData{mReflectEnable=");
    stringBuilder.append(this.mReflectEnable);
    stringBuilder.append(", mReflectList=");
    stringBuilder.append(this.mReflectList);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  private OplusReflectWidget findWidgetImpl(ArrayList<OplusReflectWidget> paramArrayList, Context paramContext, String paramString1, String paramString2) {
    for (byte b = 0; b < paramArrayList.size(); b++) {
      OplusReflectWidget oplusReflectWidget = paramArrayList.get(b);
      if (paramString2.equals(oplusReflectWidget.getClassName()))
        return oplusReflectWidget; 
    } 
    return null;
  }
  
  public OplusReflectWidget findWidget(Context paramContext, String paramString1, String paramString2) {
    ArrayList<OplusReflectWidget> arrayList = this.mReflectAppList;
    if (arrayList != null && arrayList.size() >= 1)
      return findWidgetImpl(this.mReflectAppList, paramContext, paramString1, paramString2); 
    return null;
  }
  
  public void initList(String paramString, int paramInt) {
    this.mReflectAppList.clear();
    ArrayList<OplusReflectWidget> arrayList = this.mReflectList;
    if (arrayList != null && arrayList.size() >= 1) {
      initAppWidgetImpl(this.mReflectList, this.mReflectAppList, paramString, paramInt);
    } else {
      initAppWidgetImpl(mLocalReflectList, this.mReflectAppList, paramString, paramInt);
    } 
  }
  
  private void initAppWidgetImpl(ArrayList<OplusReflectWidget> paramArrayList1, ArrayList<OplusReflectWidget> paramArrayList2, String paramString, int paramInt) {
    for (byte b = 0; b < paramArrayList1.size(); b++) {
      OplusReflectWidget oplusReflectWidget = paramArrayList1.get(b);
      if (paramString.equals(oplusReflectWidget.getPackageName()) && paramInt >= oplusReflectWidget.getVersionCode())
        paramArrayList2.add(oplusReflectWidget); 
    } 
    if (paramArrayList2.size() < 1)
      setReflectEnable(false); 
  }
}
