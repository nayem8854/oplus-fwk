package com.oplus.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.util.Slog;
import android.util.SparseIntArray;
import com.color.notification.redpackage.RedPackageAssistRUSManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OplusNavigationBarUtil {
  private static final String TAG = OplusNavigationBarUtil.class.getSimpleName();
  
  private static final String[] mDefaultAdaptationAppNames = new String[0];
  
  private static final int[] mDefaultAppColors = new int[0];
  
  private static final String[] mDefaultNotAdaptationActivityNames = new String[0];
  
  static {
    mDefaultNotAdaptationActivityColors = new int[0];
    mDefaultAdapationApps = new ArrayList<>();
    mStatusDefaultAdapationApps = new ArrayList<>();
    mDefaultNotAdapationActivities = new ArrayList<>();
    mObject = new Object();
    sColorNavigationBarUtil = null;
  }
  
  private boolean mHasInitialized = false;
  
  private boolean mReadNavData = false;
  
  private boolean mReadStatusData = false;
  
  private boolean mUseDefualtData = true;
  
  private int mUpdateNavCount = 0;
  
  private int mUpdateStaCount = 0;
  
  private static final String ACTIVITY_COLOR = "activityColor";
  
  private static final String ACTIVITY_NAME = "activityName";
  
  private static final int ALPHA_BIT_NUM = 4;
  
  private static final int COLOR_ALPHA_OPAQUE = -16777216;
  
  private static final int COLOR_BIT_NUM = 6;
  
  private static final boolean DEBUG_OPPO_SYSTEMBAR = false;
  
  private static final String DEFAULT_COLOR = "default_color";
  
  private static final int HEX_NUM = 16;
  
  private static final String IS_NEED_PALETTE = "is_need_palette";
  
  private static final int MAX_COUNT = 20;
  
  private static final String NAVBAR_BACKGROUND = "nav_bg";
  
  private static final String NAV_BG_COLOR = "bg_color";
  
  private static final String PKG = "pkg";
  
  private static final String PKG_VERSION = "pkg_version";
  
  private static final List<AdaptationAppInfo> mDefaultAdapationApps;
  
  private static final List<AdaptationActivityInfo> mDefaultNotAdapationActivities;
  
  private static final int[] mDefaultNotAdaptationActivityColors;
  
  private static final Object mObject;
  
  private static final List<AdaptationAppInfo> mStatusDefaultAdapationApps;
  
  private static volatile OplusNavigationBarUtil sColorNavigationBarUtil;
  
  private Context mContext;
  
  class AdaptationActivityInfo {
    String mActivityName;
    
    int mDefaultColor;
    
    final OplusNavigationBarUtil this$0;
  }
  
  class AdaptationAppInfo {
    Map<String, String> mActivityColorList;
    
    SparseIntArray mColorArray;
    
    int mDefaultColor;
    
    boolean mIsNeedPalette;
    
    int[] mKeys;
    
    String mPkg;
    
    final OplusNavigationBarUtil this$0;
  }
  
  public static OplusNavigationBarUtil getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/util/OplusNavigationBarUtil.sColorNavigationBarUtil : Lcom/oplus/util/OplusNavigationBarUtil;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/util/OplusNavigationBarUtil
    //   8: monitorenter
    //   9: getstatic com/oplus/util/OplusNavigationBarUtil.sColorNavigationBarUtil : Lcom/oplus/util/OplusNavigationBarUtil;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/util/OplusNavigationBarUtil
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/util/OplusNavigationBarUtil.sColorNavigationBarUtil : Lcom/oplus/util/OplusNavigationBarUtil;
    //   27: ldc com/oplus/util/OplusNavigationBarUtil
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/util/OplusNavigationBarUtil
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/util/OplusNavigationBarUtil.sColorNavigationBarUtil : Lcom/oplus/util/OplusNavigationBarUtil;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #130	-> 0
    //   #131	-> 6
    //   #132	-> 9
    //   #133	-> 15
    //   #135	-> 27
    //   #137	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public void init(Context paramContext) {
    this.mContext = paramContext;
    this.mHasInitialized = true;
    updateAppNavBarDefaultList();
    registerContentObserver();
    RedPackageAssistRUSManager.getInstance().init(this.mContext);
  }
  
  private void registerContentObserver() {
    if (this.mContext == null) {
      Log.w(TAG, "color navigation bar util isn't init.");
      return;
    } 
    Uri uri = Uri.parse("content://com.android.systemui/navigationbar");
    this.mContext.getContentResolver().registerContentObserver(uri, true, new NavBarContentObserver());
    uri = Uri.parse("content://com.android.systemui/statusbar");
    this.mContext.getContentResolver().registerContentObserver(uri, true, new StatusBarContentObserver());
  }
  
  private void updateAppNavBarDefaultList() {
    synchronized (mObject) {
      mDefaultAdapationApps.clear();
      mDefaultNotAdapationActivities.clear();
      int i = mDefaultAdaptationAppNames.length;
      byte b;
      for (b = 0; b < i; b++)
        addAdaptationApp(mDefaultAdaptationAppNames[b], mDefaultAppColors[b]); 
      i = mDefaultNotAdaptationActivityNames.length;
      for (b = 0; b < i; b++)
        addNotAdaptationActivity(mDefaultNotAdaptationActivityNames[b], mDefaultNotAdaptationActivityColors[b]); 
      this.mUseDefualtData = true;
      return;
    } 
  }
  
  private void addAdaptationApp(String paramString, int paramInt) {
    addAdaptationApp(paramString, paramInt, false);
  }
  
  private void addAdaptationApp(String paramString, int paramInt, boolean paramBoolean) {
    addAdaptationApp(paramString, paramInt, paramBoolean, null);
  }
  
  private void addAdaptationApp(String paramString, int paramInt, boolean paramBoolean, Map<String, String> paramMap) {
    AdaptationAppInfo adaptationAppInfo = new AdaptationAppInfo();
    adaptationAppInfo.mPkg = paramString;
    adaptationAppInfo.mDefaultColor = paramInt;
    adaptationAppInfo.mIsNeedPalette = paramBoolean;
    adaptationAppInfo.mActivityColorList = paramMap;
    mDefaultAdapationApps.add(adaptationAppInfo);
  }
  
  private void addStatusAdaptationApp(String paramString, int paramInt, Map<String, String> paramMap) {
    AdaptationAppInfo adaptationAppInfo = new AdaptationAppInfo();
    adaptationAppInfo.mPkg = paramString;
    adaptationAppInfo.mDefaultColor = paramInt;
    adaptationAppInfo.mActivityColorList = paramMap;
    mStatusDefaultAdapationApps.add(adaptationAppInfo);
  }
  
  private void addNotAdaptationActivity(String paramString, int paramInt) {
    AdaptationActivityInfo adaptationActivityInfo = new AdaptationActivityInfo();
    adaptationActivityInfo.mActivityName = paramString;
    adaptationActivityInfo.mDefaultColor = paramInt;
    mDefaultNotAdapationActivities.add(adaptationActivityInfo);
  }
  
  private void updateNavBgColorListFromDB() {
    if (!this.mHasInitialized) {
      Log.w(TAG, "color navigation bar util isn't init.");
      return;
    } 
    Object object = new Object(this);
    object.start();
  }
  
  private void updateStatusBgColorListFromDB() {
    if (!this.mHasInitialized) {
      Log.w(TAG, "color navigation bar util isn't init.");
      return;
    } 
    Object object = new Object(this);
    object.start();
  }
  
  public boolean isHasInitialized() {
    return this.mHasInitialized;
  }
  
  private int stringColorToIntColor(String paramString) {
    int i = paramString.length();
    if (i < 6) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Color String Error! colorString:");
      stringBuilder.append(paramString);
      Slog.e(str, stringBuilder.toString());
      return 0;
    } 
    String str2 = paramString.substring(0, i - 6);
    String str1 = paramString.substring(i - 6, i);
    paramString = str2;
    if (str2.equals(""))
      paramString = "ff"; 
    if (str1.equals(""))
      return 0; 
    i = Integer.valueOf(paramString, 16).intValue();
    return Integer.valueOf(str1, 16).intValue() | i << 24;
  }
  
  class NavBarContentObserver extends ContentObserver {
    final OplusNavigationBarUtil this$0;
    
    public NavBarContentObserver() {
      super(new Handler());
    }
    
    public void onChange(boolean param1Boolean) {
      OplusNavigationBarUtil.this.updateNavBgColorListFromDB();
    }
  }
  
  class StatusBarContentObserver extends ContentObserver {
    final OplusNavigationBarUtil this$0;
    
    public StatusBarContentObserver() {
      super(new Handler());
    }
    
    public void onChange(boolean param1Boolean) {
      OplusNavigationBarUtil.this.updateStatusBgColorListFromDB();
    }
  }
  
  public boolean isActivityNeedPalette(String paramString1, String paramString2) {
    if (!this.mHasInitialized) {
      Log.w(TAG, "color navigation bar util isn't init.");
      return false;
    } 
    if (!this.mReadNavData && this.mUpdateNavCount < 20) {
      updateNavBgColorListFromDB();
      this.mUpdateNavCount++;
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isActivityNeedPalette mUpdateNavCount:");
      stringBuilder.append(this.mUpdateNavCount);
      Slog.d(str, stringBuilder.toString());
    } 
    synchronized (mObject) {
      int i = mDefaultAdapationApps.size();
      for (byte b = 0; b < i; b++) {
        AdaptationAppInfo adaptationAppInfo = mDefaultAdapationApps.get(b);
        if (adaptationAppInfo.mActivityColorList != null)
          for (Map.Entry<String, String> entry : adaptationAppInfo.mActivityColorList.entrySet()) {
            if (((String)entry.getKey()).equals(paramString2))
              return false; 
          }  
        if (adaptationAppInfo.mPkg.equals(paramString1))
          return adaptationAppInfo.mIsNeedPalette; 
      } 
      return false;
    } 
  }
  
  public int getNavBarColorFromAdaptation(String paramString1, String paramString2) {
    if (!this.mHasInitialized) {
      Log.w(TAG, "color navigation bar util isn't init.");
      return 0;
    } 
    if (!this.mReadNavData && this.mUpdateNavCount < 20) {
      updateNavBgColorListFromDB();
      this.mUpdateNavCount++;
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getNavBarOplusFromAdaptation mUpdateNavCount:");
      stringBuilder.append(this.mUpdateNavCount);
      Slog.d(str, stringBuilder.toString());
    } 
    synchronized (mObject) {
      if (this.mUseDefualtData) {
        int k = mDefaultNotAdapationActivities.size();
        for (int m = 0; m < k; m++) {
          AdaptationActivityInfo adaptationActivityInfo = mDefaultNotAdapationActivities.get(m);
          if (adaptationActivityInfo.mActivityName.equals(paramString2)) {
            paramString1 = TAG;
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("the defualt activity:");
            stringBuilder.append(paramString2);
            stringBuilder.append(" color: ");
            stringBuilder.append(Integer.toHexString(adaptationActivityInfo.mDefaultColor));
            Slog.d(paramString1, stringBuilder.toString());
            m = adaptationActivityInfo.mDefaultColor;
            return m;
          } 
        } 
      } 
      int i = mDefaultAdapationApps.size();
      for (int j = 0; j < i; j++) {
        AdaptationAppInfo adaptationAppInfo = mDefaultAdapationApps.get(j);
        if (adaptationAppInfo.mPkg.equals(paramString1)) {
          if (adaptationAppInfo.mActivityColorList != null)
            for (Map.Entry<String, String> entry : adaptationAppInfo.mActivityColorList.entrySet()) {
              if (entry.getValue() == null || ((String)entry.getValue()).equals(""))
                return 0; 
              if (((String)entry.getKey()).equals(paramString2)) {
                j = Integer.valueOf((String)entry.getValue(), 16).intValue();
                return 0xFF000000 | j;
              } 
            }  
          j = adaptationAppInfo.mDefaultColor;
          return j;
        } 
      } 
      return 0;
    } 
  }
  
  public int getStatusBarColorFromAdaptation(String paramString1, String paramString2) {
    if (!this.mHasInitialized) {
      Log.w(TAG, "color navigation bar util isn't init.");
      return 0;
    } 
    if (!this.mReadStatusData && this.mUpdateStaCount < 20) {
      updateStatusBgColorListFromDB();
      this.mUpdateStaCount++;
    } 
    synchronized (mObject) {
      int i = mStatusDefaultAdapationApps.size();
      for (int j = 0; j < i; j++) {
        AdaptationAppInfo adaptationAppInfo = mStatusDefaultAdapationApps.get(j);
        if (adaptationAppInfo.mPkg.equals(paramString1)) {
          if (adaptationAppInfo.mActivityColorList != null && !adaptationAppInfo.mActivityColorList.equals(""))
            for (Map.Entry<String, String> entry : adaptationAppInfo.mActivityColorList.entrySet()) {
              if (((String)entry.getKey()).equals(paramString2)) {
                j = stringColorToIntColor((String)entry.getValue());
                return j;
              } 
            }  
          j = adaptationAppInfo.mDefaultColor;
          return j;
        } 
      } 
      return 0;
    } 
  }
  
  public int getImeBgColorFromAdaptation(String paramString) {
    return this.mContext.getColor(201719829);
  }
  
  public void setImePackageInGestureMode(boolean paramBoolean) {
    String str = TAG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("setImePackageInGestureMode isImeInGestureMode:");
    stringBuilder.append(paramBoolean);
    Log.i(str, stringBuilder.toString());
  }
  
  public static String getVersion(Context paramContext, String paramString) {
    try {
      PackageManager packageManager = paramContext.getPackageManager();
      PackageInfo packageInfo = packageManager.getPackageInfo(paramString, 0);
      return packageInfo.versionName;
    } catch (Exception exception) {
      paramString = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("GetVersion failed! e:");
      stringBuilder.append(exception);
      Log.e(paramString, stringBuilder.toString());
      return null;
    } 
  }
  
  public static boolean compareVersion(String paramString1, String paramString2) {
    int i;
    String str = TAG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("A:");
    stringBuilder.append(paramString1);
    stringBuilder.append(" B:");
    stringBuilder.append(paramString2);
    Log.i(str, stringBuilder.toString());
    if (paramString1 == null || paramString1.equals("") || paramString2 == null || paramString2.equals(""))
      return false; 
    if (paramString1.equals(paramString2))
      return true; 
    String[] arrayOfString1 = paramString1.split("\\.");
    String[] arrayOfString2 = paramString2.split("\\.");
    if (arrayOfString1.length < arrayOfString2.length) {
      i = arrayOfString1.length;
    } else {
      i = arrayOfString2.length;
    } 
    for (byte b = 0; b < i; b++) {
      if (Integer.parseInt(arrayOfString2[b]) > Integer.parseInt(arrayOfString1[b])) {
        str = TAG;
        stringBuilder = new StringBuilder();
        stringBuilder.append("B:");
        stringBuilder.append(Integer.parseInt(arrayOfString2[b]));
        stringBuilder.append(" > A:");
        stringBuilder.append(Integer.parseInt(arrayOfString1[b]));
        Log.i(str, stringBuilder.toString());
        return true;
      } 
      if (Integer.parseInt(arrayOfString2[b]) < Integer.parseInt(arrayOfString1[b])) {
        str = TAG;
        stringBuilder = new StringBuilder();
        stringBuilder.append("B:");
        stringBuilder.append(Integer.parseInt(arrayOfString2[b]));
        stringBuilder.append(" < A:");
        stringBuilder.append(Integer.parseInt(arrayOfString1[b]));
        Log.i(str, stringBuilder.toString());
        return false;
      } 
    } 
    return false;
  }
}
