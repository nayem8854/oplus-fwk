package com.oplus.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.ContactsContract;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Locale;

public class OplusSpecialNumberUtils {
  private String mName = null;
  
  private Locale mLocale = null;
  
  private String mLanguage = null;
  
  private int mLanguageInt = 1;
  
  private final int ZH_LANGUAGE = 2;
  
  private final int EN_LANGUAGE = 1;
  
  private final int TW_LANGUAGE = 3;
  
  private byte[] mImagedata = null;
  
  public static final Uri SPECIAL_NUMBER_CONTENT_URI = Uri.withAppendedPath(ContactsContract.AUTHORITY_URI, "special_contacts");
  
  private static final String[] CALLER_ID_PROJECTION;
  
  public static final String SpecialNumberTable = "special_contacts";
  
  private static final String TAG = "OppoSpecialNumberUtils";
  
  private Context mContext;
  
  public static class OplusSpecialNumColumns {
    public static final String CN_NAME = "cn_name";
    
    public static final String EN_NAME = "en_name";
    
    public static final String NUMBER = "number";
    
    public static final String OPPO_URL = "oppo_url";
    
    public static final String PHOTO_DATA = "photo_data";
    
    public static final String TW_NAME = "tw_name";
    
    public static final String _ID = "_id";
  }
  
  static {
    CALLER_ID_PROJECTION = new String[] { "number", "en_name", "cn_name", "tw_name", "photo_data" };
  }
  
  public OplusSpecialNumberUtils(Context paramContext) {
    this.mContext = paramContext;
    getLanguage();
  }
  
  public boolean numberNeedSpecialHandle(String paramString) {
    boolean bool = isSpecialNumber(paramString);
    if (!bool)
      return false; 
    bool = isNumberStoredInContacts(paramString);
    if (!bool)
      return false; 
    return true;
  }
  
  public boolean isSpecialNumber(String paramString) {
    Cursor cursor1;
    if (paramString == null || "" == paramString)
      return false; 
    String str1 = paramString.replace("-", "").replace(" ", "");
    Cursor cursor2 = null;
    String str2 = null;
    paramString = str2;
    Cursor cursor3 = cursor2;
    try {
      ContentResolver contentResolver = this.mContext.getContentResolver();
      paramString = str2;
      cursor3 = cursor2;
      Uri uri = SPECIAL_NUMBER_CONTENT_URI;
      paramString = str2;
      cursor3 = cursor2;
      String[] arrayOfString = CALLER_ID_PROJECTION;
      paramString = str2;
      cursor3 = cursor2;
      StringBuilder stringBuilder = new StringBuilder();
      paramString = str2;
      cursor3 = cursor2;
      this();
      paramString = str2;
      cursor3 = cursor2;
      stringBuilder.append("number='");
      paramString = str2;
      cursor3 = cursor2;
      stringBuilder.append(str1);
      paramString = str2;
      cursor3 = cursor2;
      stringBuilder.append("'");
      paramString = str2;
      cursor3 = cursor2;
      cursor2 = contentResolver.query(uri, arrayOfString, stringBuilder.toString(), null, null);
      if (cursor2 != null) {
        cursor1 = cursor2;
        cursor3 = cursor2;
        if (cursor2.moveToFirst()) {
          cursor1 = cursor2;
          cursor3 = cursor2;
          if (this.mLanguageInt == 1) {
            cursor1 = cursor2;
            cursor3 = cursor2;
            int j = cursor2.getColumnIndex("en_name");
            cursor1 = cursor2;
            cursor3 = cursor2;
            this.mName = cursor2.getString(j);
          } else {
            cursor1 = cursor2;
            cursor3 = cursor2;
            if (this.mLanguageInt == 2) {
              cursor1 = cursor2;
              cursor3 = cursor2;
              int j = cursor2.getColumnIndex("cn_name");
              cursor1 = cursor2;
              cursor3 = cursor2;
              this.mName = cursor2.getString(j);
            } else {
              cursor1 = cursor2;
              cursor3 = cursor2;
              if (this.mLanguageInt == 3) {
                cursor1 = cursor2;
                cursor3 = cursor2;
                int j = cursor2.getColumnIndex("tw_name");
                cursor1 = cursor2;
                cursor3 = cursor2;
                this.mName = cursor2.getString(j);
              } 
            } 
          } 
          cursor1 = cursor2;
          cursor3 = cursor2;
          int i = cursor2.getColumnIndex("photo_data");
          cursor1 = cursor2;
          cursor3 = cursor2;
          this.mImagedata = cursor2.getBlob(i);
          if (cursor2 != null)
            cursor2.close(); 
          return true;
        } 
        if (cursor2 != null)
          cursor2.close(); 
        return false;
      } 
      if (cursor2 != null)
        cursor2.close(); 
      return false;
    } catch (Exception exception) {
      cursor1 = cursor3;
      exception.printStackTrace();
      if (cursor3 != null)
        cursor3.close(); 
      return false;
    } finally {}
    if (cursor1 != null)
      cursor1.close(); 
    throw cursor3;
  }
  
  public boolean isNumberStoredInContacts(String paramString) {
    Cursor cursor1 = null, cursor2 = null;
    Cursor cursor3 = cursor2, cursor4 = cursor1;
    try {
      ContentResolver contentResolver = this.mContext.getContentResolver();
      cursor3 = cursor2;
      cursor4 = cursor1;
      Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
      cursor3 = cursor2;
      cursor4 = cursor1;
      StringBuilder stringBuilder = new StringBuilder();
      cursor3 = cursor2;
      cursor4 = cursor1;
      this();
      cursor3 = cursor2;
      cursor4 = cursor1;
      stringBuilder.append("data1='");
      cursor3 = cursor2;
      cursor4 = cursor1;
      stringBuilder.append(paramString);
      cursor3 = cursor2;
      cursor4 = cursor1;
      stringBuilder.append("'");
      cursor3 = cursor2;
      cursor4 = cursor1;
      paramString = stringBuilder.toString();
      cursor3 = cursor2;
      cursor4 = cursor1;
      Cursor cursor = contentResolver.query(uri, new String[] { "display_name" }, paramString, null, null);
      if (cursor != null) {
        cursor3 = cursor;
        cursor4 = cursor;
        boolean bool = cursor.moveToFirst();
        if (bool) {
          if (cursor != null)
            cursor.close(); 
          return false;
        } 
        if (cursor != null)
          cursor.close(); 
        return true;
      } 
      if (cursor != null)
        cursor.close(); 
      return true;
    } catch (Exception exception) {
      cursor3 = cursor4;
      exception.printStackTrace();
      if (cursor4 != null)
        cursor4.close(); 
      return true;
    } finally {}
    if (cursor3 != null)
      cursor3.close(); 
    throw paramString;
  }
  
  public String getNameOfnumber() {
    return this.mName;
  }
  
  public InputStream getInputStreamImageOfnumber() {
    if (this.mImagedata == null)
      return null; 
    return new ByteArrayInputStream(this.mImagedata);
  }
  
  public Drawable getImageOfnumber() {
    byte[] arrayOfByte = this.mImagedata;
    if (arrayOfByte == null)
      return null; 
    Bitmap bitmap = BitmapFactory.decodeByteArray(arrayOfByte, 0, arrayOfByte.length);
    return (Drawable)new BitmapDrawable(bitmap);
  }
  
  private void getLanguage() {
    Locale locale = Locale.getDefault();
    String str = locale.getISO3Country();
    if (str.equals("CHN")) {
      this.mLanguageInt = 2;
    } else if (this.mLanguage.equals("USA")) {
      this.mLanguageInt = 1;
    } else if (this.mLanguage.equals("TWN")) {
      this.mLanguageInt = 3;
    } 
  }
}
