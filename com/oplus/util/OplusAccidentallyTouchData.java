package com.oplus.util;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public final class OplusAccidentallyTouchData implements Parcelable {
  public static final Parcelable.Creator<OplusAccidentallyTouchData> CREATOR = new Parcelable.Creator<OplusAccidentallyTouchData>() {
      public OplusAccidentallyTouchData createFromParcel(Parcel param1Parcel) {
        return new OplusAccidentallyTouchData(param1Parcel);
      }
      
      public OplusAccidentallyTouchData[] newArray(int param1Int) {
        return new OplusAccidentallyTouchData[param1Int];
      }
    };
  
  private ArrayList<String> mSingleList = new ArrayList<>();
  
  private ArrayList<String> mMultiList = new ArrayList<>();
  
  private ArrayList<String> mWhiteList = new ArrayList<>();
  
  private ArrayList<String> mEdgeList = new ArrayList<>();
  
  private static final boolean DBG = false;
  
  private static final String TAG = "OplusAccidentallyTouchData";
  
  private String mEdgeEnable;
  
  private String mEdgeT;
  
  private String mEdgeT1;
  
  private String mEdgeT2;
  
  private String mIsEnable;
  
  private String mLeftOffset;
  
  private String mPointLeftOffset;
  
  private String mPointRightOffset;
  
  private String mRightOffset;
  
  public OplusAccidentallyTouchData(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mEdgeEnable);
    paramParcel.writeString(this.mEdgeT);
    paramParcel.writeString(this.mEdgeT1);
    paramParcel.writeString(this.mEdgeT2);
    paramParcel.writeString(this.mIsEnable);
    paramParcel.writeString(this.mLeftOffset);
    paramParcel.writeString(this.mRightOffset);
    paramParcel.writeString(this.mPointLeftOffset);
    paramParcel.writeString(this.mPointRightOffset);
    paramParcel.writeStringList(this.mSingleList);
    paramParcel.writeStringList(this.mMultiList);
    paramParcel.writeStringList(this.mWhiteList);
    paramParcel.writeStringList(this.mEdgeList);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mEdgeEnable = paramParcel.readString();
    this.mEdgeT = paramParcel.readString();
    this.mEdgeT1 = paramParcel.readString();
    this.mEdgeT2 = paramParcel.readString();
    this.mIsEnable = paramParcel.readString();
    this.mLeftOffset = paramParcel.readString();
    this.mRightOffset = paramParcel.readString();
    this.mPointLeftOffset = paramParcel.readString();
    this.mPointRightOffset = paramParcel.readString();
    this.mSingleList = paramParcel.createStringArrayList();
    this.mMultiList = paramParcel.createStringArrayList();
    this.mWhiteList = paramParcel.createStringArrayList();
    this.mEdgeList = paramParcel.createStringArrayList();
  }
  
  public void setLeftOffset(String paramString) {
    this.mLeftOffset = paramString;
  }
  
  public void setRightOffset(String paramString) {
    this.mRightOffset = paramString;
  }
  
  public void setPointLeftOffset(String paramString) {
    this.mPointLeftOffset = paramString;
  }
  
  public void setPointRightOffset(String paramString) {
    this.mPointRightOffset = paramString;
  }
  
  public void setAccidentalltyTouchEnable(String paramString) {
    this.mIsEnable = paramString;
  }
  
  public void setEdgeEnable(String paramString) {
    this.mEdgeEnable = paramString;
  }
  
  public void setEdgeT(String paramString) {
    this.mEdgeT = paramString;
  }
  
  public void setEdgeT1(String paramString) {
    this.mEdgeT1 = paramString;
  }
  
  public void setEdgeT2(String paramString) {
    this.mEdgeT2 = paramString;
  }
  
  public String getEdgeEnable() {
    return this.mEdgeEnable;
  }
  
  public String getEdgeT() {
    return this.mEdgeT;
  }
  
  public String getEdgeT1() {
    return this.mEdgeT1;
  }
  
  public String getEdgeT2() {
    return this.mEdgeT2;
  }
  
  public ArrayList<String> getEdgeList() {
    return this.mEdgeList;
  }
  
  public String getLeftOffset() {
    return this.mLeftOffset;
  }
  
  public String getRightOffset() {
    return this.mRightOffset;
  }
  
  public String getPointLeftOffset() {
    return this.mPointLeftOffset;
  }
  
  public String getPointRightOffset() {
    return this.mPointRightOffset;
  }
  
  public String getAccidentalltyTouchEnable() {
    return this.mIsEnable;
  }
  
  public ArrayList<String> getSingleTouchList() {
    return this.mSingleList;
  }
  
  public ArrayList<String> getMultiTouchList() {
    return this.mMultiList;
  }
  
  public ArrayList<String> getTouchWhiteList() {
    return this.mWhiteList;
  }
  
  public OplusAccidentallyTouchData() {}
}
