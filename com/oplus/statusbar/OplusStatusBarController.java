package com.oplus.statusbar;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.OplusBaseConfiguration;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.util.Log;
import android.util.Singleton;
import android.view.OplusWindowManager;
import android.view.View;
import android.view.WindowInsets;
import com.android.internal.graphics.ColorUtils;
import com.oplus.util.OplusDarkModeUtil;
import com.oplus.util.OplusTypeCastingHelper;

public class OplusStatusBarController {
  private static final int ALPHA = 156;
  
  private static boolean DEBUG_OPPO_SYSTEMBAR = false;
  
  private static final int GRAY_LEVEL = 150;
  
  public static final int NAVIGATION_BAR = 1;
  
  public static final int OPLUS_NAVIGATION_BAR_COLOR = -1;
  
  public static final int STATUS_BAR = 0;
  
  private static final String TAG = OplusStatusBarController.class.getSimpleName();
  
  private static final Singleton<OplusStatusBarController> sColorStatusBarControllerSingleton;
  
  private int mNavigationGuardColor = 0;
  
  private OplusWindowManager mWm = null;
  
  static {
    DEBUG_OPPO_SYSTEMBAR = false;
    sColorStatusBarControllerSingleton = (Singleton<OplusStatusBarController>)new Object();
  }
  
  private OplusStatusBarController() {
    this.mWm = new OplusWindowManager();
    DEBUG_OPPO_SYSTEMBAR = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  }
  
  public static OplusStatusBarController getInstance() {
    return (OplusStatusBarController)sColorStatusBarControllerSingleton.get();
  }
  
  public int caculateSystemBarColor(Context paramContext, String paramString1, String paramString2, int paramInt1, int paramInt2) {
    if (OplusDarkModeUtil.isNightMode(paramContext))
      return paramInt1; 
    paramInt2 = getBarColorFromAdaptation(paramString1, paramString2, paramInt2);
    if (paramInt2 != 0)
      paramInt1 = paramInt2; 
    return paramInt1;
  }
  
  public int getBottomInset(boolean paramBoolean, int paramInt, WindowInsets paramWindowInsets) {
    if (paramBoolean)
      paramInt = paramWindowInsets.getSystemWindowInsetBottom(); 
    return paramInt;
  }
  
  public void updateOplusNavigationGuardColor(Context paramContext, int paramInt1, int paramInt2, View paramView) {
    this.mNavigationGuardColor = getImeBgColor(paramContext, paramInt1, paramInt2);
  }
  
  public int getNavigationGuardColor() {
    return this.mNavigationGuardColor;
  }
  
  private int getImeBgColor(Context paramContext, int paramInt1, int paramInt2) {
    if (paramInt2 != -16777216 && paramInt2 != -1)
      return paramInt2; 
    if (DEBUG_OPPO_SYSTEMBAR) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getImeBgColor: ");
      stringBuilder.append(paramInt1);
      Log.d(str, stringBuilder.toString());
    } 
    return paramInt1;
  }
  
  private int getBarColorFromAdaptation(String paramString1, String paramString2, int paramInt) {
    boolean bool = false;
    byte b = 0;
    int i = b;
    try {
      if (this.mWm != null)
        if (paramInt == 0) {
          i = this.mWm.getStatusBarOplusFromAdaptation(paramString1, paramString2);
        } else {
          i = b;
          if (paramInt == 1)
            i = this.mWm.getNavBarOplusFromAdaptation(paramString1, paramString2); 
        }  
    } catch (RemoteException remoteException) {
      paramString2 = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getNavBarOplusFromAdaptation ");
      stringBuilder.append(remoteException);
      Log.w(paramString2, stringBuilder.toString());
      i = bool;
    } 
    return i;
  }
  
  private boolean isColorLight(int paramInt) {
    if (paramInt == 0)
      return false; 
    int i = (int)(((0xFF0000 & paramInt) >>> 16) * 0.299D + ((0xFF00 & paramInt) >>> 8) * 0.587D + (paramInt & 0xFF) * 0.114D);
    if (i > 150 && (0xFF000000 & paramInt) >>> 24 > 156)
      return true; 
    return false;
  }
  
  public static int getDefaultNavigationBarColor(Context paramContext) {
    if (OplusDarkModeUtil.isNightMode(paramContext))
      return getDarkModeBackgroundColor(paramContext); 
    return -1;
  }
  
  public static int getDarkModeBackgroundColor(Context paramContext) {
    int i = -16777216;
    if (paramContext == null)
      return -16777216; 
    Configuration configuration = paramContext.getResources().getConfiguration();
    OplusBaseConfiguration oplusBaseConfiguration = OplusTypeCastingHelper.<OplusBaseConfiguration>typeCasting(OplusBaseConfiguration.class, configuration);
    if (oplusBaseConfiguration != null) {
      float f = (oplusBaseConfiguration.getOplusExtraConfiguration()).mDarkModeBackgroundMaxL;
      int j = ColorUtils.LABToColor(f, 0.0D, 0.0D);
      i = j;
      if (DEBUG_OPPO_SYSTEMBAR) {
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("getDarkModeBackgroundColor: bgColor = ");
        stringBuilder.append(Integer.toHexString(j));
        Log.d(str, stringBuilder.toString());
        i = j;
      } 
    } 
    return i;
  }
}
