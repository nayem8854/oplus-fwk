package com.oplus.nec;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusNecService extends IInterface {
  void addNecEventListener(String paramString, IOnNecEventListener paramIOnNecEventListener) throws RemoteException;
  
  void clearCellAppsRttRecord() throws RemoteException;
  
  void clearCellDnsRecord() throws RemoteException;
  
  void clearCellHttpRecord() throws RemoteException;
  
  void clearCellNetTotalRecord() throws RemoteException;
  
  void clearCellTcpRecord() throws RemoteException;
  
  void clearWlanAppsRttRecord() throws RemoteException;
  
  void clearWlanDnsRecord() throws RemoteException;
  
  void clearWlanHttpRecord() throws RemoteException;
  
  void clearWlanNetTotalRecord() throws RemoteException;
  
  void clearWlanPowerRecord() throws RemoteException;
  
  void clearWlanTcpRecord() throws RemoteException;
  
  String getCellAppsRttRecord() throws RemoteException;
  
  String getCellDnsRecord() throws RemoteException;
  
  String getCellHttpRecord() throws RemoteException;
  
  String getCellNetTotalRecord() throws RemoteException;
  
  String getCellTcpRecord() throws RemoteException;
  
  String getWlanAppsRttRecord() throws RemoteException;
  
  String getWlanDnsRecord() throws RemoteException;
  
  String getWlanHttpRecord() throws RemoteException;
  
  String getWlanNetTotalRecord() throws RemoteException;
  
  String getWlanPowerRecord() throws RemoteException;
  
  String getWlanTcpRecord() throws RemoteException;
  
  String onCollectPwrStatistic(boolean paramBoolean) throws RemoteException;
  
  void onStandbyStart(boolean paramBoolean) throws RemoteException;
  
  void removeNecEventListener(String paramString, IOnNecEventListener paramIOnNecEventListener) throws RemoteException;
  
  void reportNecEvent(int paramInt1, int paramInt2, Bundle paramBundle) throws RemoteException;
  
  class Default implements IOplusNecService {
    public void addNecEventListener(String param1String, IOnNecEventListener param1IOnNecEventListener) throws RemoteException {}
    
    public void removeNecEventListener(String param1String, IOnNecEventListener param1IOnNecEventListener) throws RemoteException {}
    
    public void reportNecEvent(int param1Int1, int param1Int2, Bundle param1Bundle) throws RemoteException {}
    
    public String getCellDnsRecord() throws RemoteException {
      return null;
    }
    
    public String getWlanDnsRecord() throws RemoteException {
      return null;
    }
    
    public String getCellHttpRecord() throws RemoteException {
      return null;
    }
    
    public String getWlanHttpRecord() throws RemoteException {
      return null;
    }
    
    public String getCellTcpRecord() throws RemoteException {
      return null;
    }
    
    public String getWlanTcpRecord() throws RemoteException {
      return null;
    }
    
    public String getCellAppsRttRecord() throws RemoteException {
      return null;
    }
    
    public String getWlanAppsRttRecord() throws RemoteException {
      return null;
    }
    
    public String getCellNetTotalRecord() throws RemoteException {
      return null;
    }
    
    public String getWlanNetTotalRecord() throws RemoteException {
      return null;
    }
    
    public void clearCellDnsRecord() throws RemoteException {}
    
    public void clearWlanDnsRecord() throws RemoteException {}
    
    public void clearCellHttpRecord() throws RemoteException {}
    
    public void clearWlanHttpRecord() throws RemoteException {}
    
    public void clearCellTcpRecord() throws RemoteException {}
    
    public void clearWlanTcpRecord() throws RemoteException {}
    
    public void clearCellAppsRttRecord() throws RemoteException {}
    
    public void clearWlanAppsRttRecord() throws RemoteException {}
    
    public void clearCellNetTotalRecord() throws RemoteException {}
    
    public void clearWlanNetTotalRecord() throws RemoteException {}
    
    public String getWlanPowerRecord() throws RemoteException {
      return null;
    }
    
    public void clearWlanPowerRecord() throws RemoteException {}
    
    public void onStandbyStart(boolean param1Boolean) throws RemoteException {}
    
    public String onCollectPwrStatistic(boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusNecService {
    private static final String DESCRIPTOR = "com.oplus.nec.IOplusNecService";
    
    static final int TRANSACTION_addNecEventListener = 1;
    
    static final int TRANSACTION_clearCellAppsRttRecord = 20;
    
    static final int TRANSACTION_clearCellDnsRecord = 14;
    
    static final int TRANSACTION_clearCellHttpRecord = 16;
    
    static final int TRANSACTION_clearCellNetTotalRecord = 22;
    
    static final int TRANSACTION_clearCellTcpRecord = 18;
    
    static final int TRANSACTION_clearWlanAppsRttRecord = 21;
    
    static final int TRANSACTION_clearWlanDnsRecord = 15;
    
    static final int TRANSACTION_clearWlanHttpRecord = 17;
    
    static final int TRANSACTION_clearWlanNetTotalRecord = 23;
    
    static final int TRANSACTION_clearWlanPowerRecord = 25;
    
    static final int TRANSACTION_clearWlanTcpRecord = 19;
    
    static final int TRANSACTION_getCellAppsRttRecord = 10;
    
    static final int TRANSACTION_getCellDnsRecord = 4;
    
    static final int TRANSACTION_getCellHttpRecord = 6;
    
    static final int TRANSACTION_getCellNetTotalRecord = 12;
    
    static final int TRANSACTION_getCellTcpRecord = 8;
    
    static final int TRANSACTION_getWlanAppsRttRecord = 11;
    
    static final int TRANSACTION_getWlanDnsRecord = 5;
    
    static final int TRANSACTION_getWlanHttpRecord = 7;
    
    static final int TRANSACTION_getWlanNetTotalRecord = 13;
    
    static final int TRANSACTION_getWlanPowerRecord = 24;
    
    static final int TRANSACTION_getWlanTcpRecord = 9;
    
    static final int TRANSACTION_onCollectPwrStatistic = 27;
    
    static final int TRANSACTION_onStandbyStart = 26;
    
    static final int TRANSACTION_removeNecEventListener = 2;
    
    static final int TRANSACTION_reportNecEvent = 3;
    
    public Stub() {
      attachInterface(this, "com.oplus.nec.IOplusNecService");
    }
    
    public static IOplusNecService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.nec.IOplusNecService");
      if (iInterface != null && iInterface instanceof IOplusNecService)
        return (IOplusNecService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 27:
          return "onCollectPwrStatistic";
        case 26:
          return "onStandbyStart";
        case 25:
          return "clearWlanPowerRecord";
        case 24:
          return "getWlanPowerRecord";
        case 23:
          return "clearWlanNetTotalRecord";
        case 22:
          return "clearCellNetTotalRecord";
        case 21:
          return "clearWlanAppsRttRecord";
        case 20:
          return "clearCellAppsRttRecord";
        case 19:
          return "clearWlanTcpRecord";
        case 18:
          return "clearCellTcpRecord";
        case 17:
          return "clearWlanHttpRecord";
        case 16:
          return "clearCellHttpRecord";
        case 15:
          return "clearWlanDnsRecord";
        case 14:
          return "clearCellDnsRecord";
        case 13:
          return "getWlanNetTotalRecord";
        case 12:
          return "getCellNetTotalRecord";
        case 11:
          return "getWlanAppsRttRecord";
        case 10:
          return "getCellAppsRttRecord";
        case 9:
          return "getWlanTcpRecord";
        case 8:
          return "getCellTcpRecord";
        case 7:
          return "getWlanHttpRecord";
        case 6:
          return "getCellHttpRecord";
        case 5:
          return "getWlanDnsRecord";
        case 4:
          return "getCellDnsRecord";
        case 3:
          return "reportNecEvent";
        case 2:
          return "removeNecEventListener";
        case 1:
          break;
      } 
      return "addNecEventListener";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String str1;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 27:
            param1Parcel1.enforceInterface("com.oplus.nec.IOplusNecService");
            bool1 = bool2;
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            str1 = onCollectPwrStatistic(bool1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 26:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            if (str1.readInt() != 0)
              bool1 = true; 
            onStandbyStart(bool1);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            clearWlanPowerRecord();
            param1Parcel2.writeNoException();
            return true;
          case 24:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            str1 = getWlanPowerRecord();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 23:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            clearWlanNetTotalRecord();
            param1Parcel2.writeNoException();
            return true;
          case 22:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            clearCellNetTotalRecord();
            param1Parcel2.writeNoException();
            return true;
          case 21:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            clearWlanAppsRttRecord();
            param1Parcel2.writeNoException();
            return true;
          case 20:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            clearCellAppsRttRecord();
            param1Parcel2.writeNoException();
            return true;
          case 19:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            clearWlanTcpRecord();
            param1Parcel2.writeNoException();
            return true;
          case 18:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            clearCellTcpRecord();
            param1Parcel2.writeNoException();
            return true;
          case 17:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            clearWlanHttpRecord();
            param1Parcel2.writeNoException();
            return true;
          case 16:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            clearCellHttpRecord();
            param1Parcel2.writeNoException();
            return true;
          case 15:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            clearWlanDnsRecord();
            param1Parcel2.writeNoException();
            return true;
          case 14:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            clearCellDnsRecord();
            param1Parcel2.writeNoException();
            return true;
          case 13:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            str1 = getWlanNetTotalRecord();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 12:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            str1 = getCellNetTotalRecord();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 11:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            str1 = getWlanAppsRttRecord();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 10:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            str1 = getCellAppsRttRecord();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 9:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            str1 = getWlanTcpRecord();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 8:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            str1 = getCellTcpRecord();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 7:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            str1 = getWlanHttpRecord();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 6:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            str1 = getCellHttpRecord();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 5:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            str1 = getWlanDnsRecord();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 4:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            str1 = getCellDnsRecord();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 3:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            param1Int1 = str1.readInt();
            param1Int2 = str1.readInt();
            if (str1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            reportNecEvent(param1Int1, param1Int2, (Bundle)str1);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str1.enforceInterface("com.oplus.nec.IOplusNecService");
            str2 = str1.readString();
            iOnNecEventListener = IOnNecEventListener.Stub.asInterface(str1.readStrongBinder());
            removeNecEventListener(str2, iOnNecEventListener);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        iOnNecEventListener.enforceInterface("com.oplus.nec.IOplusNecService");
        String str2 = iOnNecEventListener.readString();
        IOnNecEventListener iOnNecEventListener = IOnNecEventListener.Stub.asInterface(iOnNecEventListener.readStrongBinder());
        addNecEventListener(str2, iOnNecEventListener);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("com.oplus.nec.IOplusNecService");
      return true;
    }
    
    private static class Proxy implements IOplusNecService {
      public static IOplusNecService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.nec.IOplusNecService";
      }
      
      public void addNecEventListener(String param2String, IOnNecEventListener param2IOnNecEventListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          parcel1.writeString(param2String);
          if (param2IOnNecEventListener != null) {
            iBinder = param2IOnNecEventListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null) {
            IOplusNecService.Stub.getDefaultImpl().addNecEventListener(param2String, param2IOnNecEventListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeNecEventListener(String param2String, IOnNecEventListener param2IOnNecEventListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          parcel1.writeString(param2String);
          if (param2IOnNecEventListener != null) {
            iBinder = param2IOnNecEventListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null) {
            IOplusNecService.Stub.getDefaultImpl().removeNecEventListener(param2String, param2IOnNecEventListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportNecEvent(int param2Int1, int param2Int2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null) {
            IOplusNecService.Stub.getDefaultImpl().reportNecEvent(param2Int1, param2Int2, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getCellDnsRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null)
            return IOplusNecService.Stub.getDefaultImpl().getCellDnsRecord(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getWlanDnsRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null)
            return IOplusNecService.Stub.getDefaultImpl().getWlanDnsRecord(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getCellHttpRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null)
            return IOplusNecService.Stub.getDefaultImpl().getCellHttpRecord(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getWlanHttpRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null)
            return IOplusNecService.Stub.getDefaultImpl().getWlanHttpRecord(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getCellTcpRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null)
            return IOplusNecService.Stub.getDefaultImpl().getCellTcpRecord(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getWlanTcpRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null)
            return IOplusNecService.Stub.getDefaultImpl().getWlanTcpRecord(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getCellAppsRttRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null)
            return IOplusNecService.Stub.getDefaultImpl().getCellAppsRttRecord(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getWlanAppsRttRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null)
            return IOplusNecService.Stub.getDefaultImpl().getWlanAppsRttRecord(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getCellNetTotalRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null)
            return IOplusNecService.Stub.getDefaultImpl().getCellNetTotalRecord(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getWlanNetTotalRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null)
            return IOplusNecService.Stub.getDefaultImpl().getWlanNetTotalRecord(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearCellDnsRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null) {
            IOplusNecService.Stub.getDefaultImpl().clearCellDnsRecord();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearWlanDnsRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null) {
            IOplusNecService.Stub.getDefaultImpl().clearWlanDnsRecord();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearCellHttpRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null) {
            IOplusNecService.Stub.getDefaultImpl().clearCellHttpRecord();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearWlanHttpRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null) {
            IOplusNecService.Stub.getDefaultImpl().clearWlanHttpRecord();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearCellTcpRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null) {
            IOplusNecService.Stub.getDefaultImpl().clearCellTcpRecord();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearWlanTcpRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null) {
            IOplusNecService.Stub.getDefaultImpl().clearWlanTcpRecord();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearCellAppsRttRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null) {
            IOplusNecService.Stub.getDefaultImpl().clearCellAppsRttRecord();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearWlanAppsRttRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null) {
            IOplusNecService.Stub.getDefaultImpl().clearWlanAppsRttRecord();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearCellNetTotalRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null) {
            IOplusNecService.Stub.getDefaultImpl().clearCellNetTotalRecord();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearWlanNetTotalRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null) {
            IOplusNecService.Stub.getDefaultImpl().clearWlanNetTotalRecord();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getWlanPowerRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null)
            return IOplusNecService.Stub.getDefaultImpl().getWlanPowerRecord(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearWlanPowerRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IOplusNecService.Stub.getDefaultImpl() != null) {
            IOplusNecService.Stub.getDefaultImpl().clearWlanPowerRecord();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onStandbyStart(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool1 && IOplusNecService.Stub.getDefaultImpl() != null) {
            IOplusNecService.Stub.getDefaultImpl().onStandbyStart(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String onCollectPwrStatistic(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.oplus.nec.IOplusNecService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool1 && IOplusNecService.Stub.getDefaultImpl() != null)
            return IOplusNecService.Stub.getDefaultImpl().onCollectPwrStatistic(param2Boolean); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusNecService param1IOplusNecService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusNecService != null) {
          Proxy.sDefaultImpl = param1IOplusNecService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusNecService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
