package com.oplus.nec;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ServiceManager;
import android.telephony.TelephonyManager;
import android.util.Log;

public class OplusNecManager {
  public static final int CALL_EVENT_CALL_DROP = 9005;
  
  public static final int CALL_EVENT_CALL_FAIL = 9004;
  
  public static final int CALL_EVENT_NO_ERR = 9001;
  
  public static final int CALL_EVENT_TELEPHONY_LOGIC_ERR = 9003;
  
  public static final int CALL_EVENT_UNKNOWN_ERR = 9002;
  
  public static final boolean DBG = true;
  
  public static final String EVENT_ID = "evt_id";
  
  public static final String LOG_TAG = "OplusNecManager";
  
  public static final String NEC_ACTION = "com.oppo.telephony.action.ACTION_REPORT_NEC";
  
  public static final String NEC_BROADCAST_PERMISSION = "oppo.permission.OPPO_COMPONENT_SAFE";
  
  public static final String NEC_DATA = "nec_data";
  
  public static final int NEC_EVENT_CALL_ERROR = 8107;
  
  public static final int NEC_EVENT_CDMA_RESET_ACTIVE_TIME = 8102;
  
  public static final int NEC_EVENT_DATA_CALL_IP_TYPE = 8007;
  
  public static final int NEC_EVENT_DATA_CONNECT = 8002;
  
  public static final int NEC_EVENT_DATA_CONNECT_RET = 8003;
  
  public static final int NEC_EVENT_DATA_DISCONNECT = 8004;
  
  public static final int NEC_EVENT_DATA_DISCONNECT_RET = 8005;
  
  public static final int NEC_EVENT_DATA_ENABLE_CHANGE = 8021;
  
  public static final int NEC_EVENT_DATA_LIMIT_STATE = 8022;
  
  public static final int NEC_EVENT_DATA_LOST_REASON = 8023;
  
  public static final int NEC_EVENT_GAME_DELAY_ERROR = 8015;
  
  public static final int NEC_EVENT_HANG_UP_DELAY_TIME = 8101;
  
  public static final int NEC_EVENT_IMS_CALL_STATE_CHANGED = 8106;
  
  public static final int NEC_EVENT_IMS_KEYLOG = 8109;
  
  public static final int NEC_EVENT_IMS_STATE_CHANGED = 8105;
  
  public static final int NEC_EVENT_IMS_VOLTE_VOPS_QUEREY = 8108;
  
  public static final int NEC_EVENT_NO_DATA_FLOW_ERROR = 8011;
  
  public static final int NEC_EVENT_NO_DATA_FLOW_RECOVERY_ERROR = 8012;
  
  public static final int NEC_EVENT_NO_DATA_ICON_ERROR = 8008;
  
  public static final int NEC_EVENT_NWDIAG_SERVICE_INITED = 8006;
  
  public static final int NEC_EVENT_OOS_CHANGED = 8001;
  
  public static final int NEC_EVENT_PRECISE_CALL_STATE_CHANGED = 8103;
  
  public static final int NEC_EVENT_PREF_NETWORK_MODE_CHANGED = 8018;
  
  public static final int NEC_EVENT_REG_INFO_CHANGED = 8017;
  
  public static final int NEC_EVENT_SIGNALSTRENGTH_CHANGED = 8000;
  
  public static final int NEC_EVENT_SLOW_DATA_FLOW_ERROR = 8013;
  
  public static final int NEC_EVENT_SLOW_DATA_FLOW_RECOVERY = 8014;
  
  public static final int NEC_EVENT_SRVCC_STATE_CHANGED = 8104;
  
  private static int SIM_COUNT = 0;
  
  private static final int SIM_NUM = TelephonyManager.getDefault().getPhoneCount();
  
  public static final String SLOT_ID = "slot_id";
  
  public static final String SRV_NAME = "oplus_nec";
  
  private static OplusNecManager sInstance;
  
  public Context mContext;
  
  private int[] mDataNetworkType;
  
  private int[] mDataRegState;
  
  private IOplusNecService mNecService;
  
  private int[] mVoiceNetworkType;
  
  private int[] mVoiceRegState;
  
  public static OplusNecManager getInstance(Context paramContext) {
    // Byte code:
    //   0: ldc com/oplus/nec/OplusNecManager
    //   2: monitorenter
    //   3: getstatic com/oplus/nec/OplusNecManager.sInstance : Lcom/oplus/nec/OplusNecManager;
    //   6: ifnonnull -> 22
    //   9: new com/oplus/nec/OplusNecManager
    //   12: astore_1
    //   13: aload_1
    //   14: aload_0
    //   15: invokespecial <init> : (Landroid/content/Context;)V
    //   18: aload_1
    //   19: putstatic com/oplus/nec/OplusNecManager.sInstance : Lcom/oplus/nec/OplusNecManager;
    //   22: getstatic com/oplus/nec/OplusNecManager.sInstance : Lcom/oplus/nec/OplusNecManager;
    //   25: astore_0
    //   26: ldc com/oplus/nec/OplusNecManager
    //   28: monitorexit
    //   29: aload_0
    //   30: areturn
    //   31: astore_0
    //   32: ldc com/oplus/nec/OplusNecManager
    //   34: monitorexit
    //   35: aload_0
    //   36: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #99	-> 0
    //   #100	-> 3
    //   #101	-> 9
    //   #103	-> 22
    //   #104	-> 31
    // Exception table:
    //   from	to	target	type
    //   3	9	31	finally
    //   9	22	31	finally
    //   22	29	31	finally
    //   32	35	31	finally
  }
  
  protected OplusNecManager(Context paramContext) {
    int i = SIM_NUM;
    this.mVoiceRegState = new int[i];
    this.mVoiceNetworkType = new int[i];
    this.mDataRegState = new int[i];
    this.mDataNetworkType = new int[i];
    this.mContext = paramContext;
    SIM_COUNT = ((TelephonyManager)paramContext.getSystemService("phone")).getPhoneCount();
    this.mNecService = IOplusNecService.Stub.asInterface(ServiceManager.getService("oplus_nec"));
  }
  
  public void notifyNwDiagnoseInitComplete() {
    necLog("notifyNwDiagnoseInitComplete...");
    Intent intent = new Intent("com.oppo.telephony.action.ACTION_REPORT_NEC");
    intent.putExtra("evt_id", 8006);
    Context context = this.mContext;
    if (context != null)
      context.sendBroadcast(intent, "oppo.permission.OPPO_COMPONENT_SAFE"); 
  }
  
  public void broadcastNecEvent(int paramInt1, int paramInt2, Bundle paramBundle) {
    if (this.mContext == null || !isValidSlotId(paramInt1))
      return; 
    Intent intent = new Intent("com.oppo.telephony.action.ACTION_REPORT_NEC");
    intent.putExtra("slot_id", paramInt1);
    intent.putExtra("evt_id", paramInt2);
    intent.putExtra("nec_data", paramBundle);
    Context context = this.mContext;
    if (context != null)
      context.sendBroadcast(intent, "oppo.permission.OPPO_COMPONENT_SAFE"); 
  }
  
  public void broadcastServiceStateChanged(boolean paramBoolean, int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("broadcastServiceStateChanged slotId:");
    stringBuilder.append(paramInt);
    stringBuilder.append("oos:");
    stringBuilder.append(paramBoolean);
    necLog(stringBuilder.toString());
  }
  
  public boolean isApnSupported(String paramString) {
    if ("default".equals(paramString) || "mms".equals(paramString) || "ims".equals(paramString))
      return true; 
    return false;
  }
  
  public void broadcastDataConnect(int paramInt, String paramString) {
    StringBuilder stringBuilder;
    if (paramString == null || !isApnSupported(paramString)) {
      Log.e("OplusNecManager", "broadcastDataConnect paras is null ");
      return;
    } 
    if (paramInt < 0 || paramInt >= SIM_COUNT) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("invalid subId: ");
      stringBuilder.append(paramInt);
      Log.e("OplusNecManager", stringBuilder.toString());
      return;
    } 
    Bundle bundle = new Bundle();
    bundle.putString("type", (String)stringBuilder);
    broadcastNecEvent(paramInt, 8002, bundle);
  }
  
  public void broadcastDataConnectResult(int paramInt, String paramString, boolean paramBoolean) {
    StringBuilder stringBuilder;
    if (paramString == null || !isApnSupported(paramString)) {
      Log.e("OplusNecManager", "broadcastDataConnected paras is null ");
      return;
    } 
    if (paramInt < 0 || paramInt >= SIM_COUNT) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("invalid subId: ");
      stringBuilder.append(paramInt);
      Log.e("OplusNecManager", stringBuilder.toString());
      return;
    } 
    Bundle bundle = new Bundle();
    bundle.putString("type", (String)stringBuilder);
    bundle.putBoolean("success", paramBoolean);
    broadcastNecEvent(paramInt, 8003, bundle);
  }
  
  public void broadcastDataDisconnect(int paramInt, String paramString) {
    StringBuilder stringBuilder;
    if (paramString == null || !isApnSupported(paramString)) {
      Log.e("OplusNecManager", "broadcastDataDisconnect paras is null ");
      return;
    } 
    if (paramInt < 0 || paramInt >= SIM_COUNT) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("invalid subId: ");
      stringBuilder.append(paramInt);
      Log.e("OplusNecManager", stringBuilder.toString());
      return;
    } 
    Bundle bundle = new Bundle();
    bundle.putString("type", (String)stringBuilder);
    broadcastNecEvent(paramInt, 8004, bundle);
  }
  
  public void broadcastDataDisconnectComplete(int paramInt, String paramString) {
    StringBuilder stringBuilder;
    if (paramString == null || !isApnSupported(paramString)) {
      Log.e("OplusNecManager", "broadcastDataDisconnectComplete paras is null ");
      return;
    } 
    if (paramInt < 0 || paramInt >= SIM_COUNT) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("invalid subId: ");
      stringBuilder.append(paramInt);
      Log.e("OplusNecManager", stringBuilder.toString());
      return;
    } 
    Bundle bundle = new Bundle();
    bundle.putString("type", (String)stringBuilder);
    broadcastNecEvent(paramInt, 8005, bundle);
  }
  
  public void broadcastDataCallInternetProtocolType(int paramInt1, int paramInt2) {
    if (paramInt1 < 0 || paramInt1 >= SIM_COUNT) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("invalid subId: ");
      stringBuilder.append(paramInt1);
      Log.e("OplusNecManager", stringBuilder.toString());
      return;
    } 
    Bundle bundle = new Bundle();
    bundle.putInt("protocol", paramInt2);
    broadcastNecEvent(paramInt1, 8007, bundle);
  }
  
  public void broadcastNoDataIconError(int paramInt1, int paramInt2, int paramInt3, String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    if (paramInt1 < 0 || paramInt1 >= SIM_NUM) {
      stringBuilder.append("invalid subId: ");
      stringBuilder.append(paramInt1);
      Log.e("OplusNecManager", stringBuilder.toString());
      return;
    } 
    Bundle bundle = new Bundle();
    bundle.putInt("errorcode", paramInt2);
    bundle.putInt("protocol", paramInt3);
    bundle.putString("cause", paramString);
    broadcastNecEvent(paramInt1, 8008, bundle);
  }
  
  public void broadcastNoDataFlowError(int paramInt1, int paramInt2, String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    if (paramInt1 < 0 || paramInt1 >= SIM_NUM) {
      stringBuilder.append("invalid subId: ");
      stringBuilder.append(paramInt1);
      Log.e("OplusNecManager", stringBuilder.toString());
      return;
    } 
    Bundle bundle = new Bundle();
    bundle.putInt("errorcode", paramInt2);
    bundle.putString("mNoDataFlowReason", paramString);
    broadcastNecEvent(paramInt1, 8011, bundle);
  }
  
  public void broadcastNoDataFlowRecoveryError(int paramInt1, int paramInt2, String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    if (paramInt1 < 0 || paramInt1 >= SIM_NUM) {
      stringBuilder.append("invalid subId: ");
      stringBuilder.append(paramInt1);
      Log.e("OplusNecManager", stringBuilder.toString());
      return;
    } 
    Bundle bundle = new Bundle();
    bundle.putInt("errorcode", paramInt2);
    bundle.putString("recovery", paramString);
    broadcastNecEvent(paramInt1, 8012, bundle);
  }
  
  public void broadcastSlowDataFlowError(int paramInt1, int paramInt2, String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    if (paramInt1 < 0 || paramInt1 >= SIM_NUM) {
      stringBuilder.append("invalid subId: ");
      stringBuilder.append(paramInt1);
      Log.e("OplusNecManager", stringBuilder.toString());
      return;
    } 
    Bundle bundle = new Bundle();
    bundle.putInt("errorcode", paramInt2);
    bundle.putString("score", paramString);
    broadcastNecEvent(paramInt1, 8013, bundle);
  }
  
  public void broadcastSlowDataFlowRecovery(int paramInt1, int paramInt2, String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    if (paramInt1 < 0 || paramInt1 >= SIM_NUM) {
      stringBuilder.append("invalid subId: ");
      stringBuilder.append(paramInt1);
      Log.e("OplusNecManager", stringBuilder.toString());
      return;
    } 
    Bundle bundle = new Bundle();
    bundle.putInt("errorcode", paramInt2);
    bundle.putString("score", paramString);
    broadcastNecEvent(paramInt1, 8014, bundle);
  }
  
  public void broadcastGameLargeDelayError(int paramInt1, int paramInt2, String paramString) {
    if (!isValidSlotId(paramInt1)) {
      Log.e("OplusNecManager", "broadcastGameLargeDelayError invalid slotId ");
      return;
    } 
    Bundle bundle = new Bundle();
    bundle.putInt("errorcode", paramInt2);
    bundle.putString("gameError", paramString);
    broadcastNecEvent(paramInt1, 8015, bundle);
  }
  
  public void broadcastLimitState(int paramInt, boolean paramBoolean) {
    if (!isValidSlotId(paramInt)) {
      Log.e("OplusNecManager", "broadcastLimitState invalid slotId ");
      return;
    } 
    Bundle bundle = new Bundle();
    bundle.putBoolean("limitState", paramBoolean);
    broadcastNecEvent(paramInt, 8022, bundle);
  }
  
  public void broadcastPreferredNetworkMode(int paramInt1, int paramInt2) {
    if (!isValidSlotId(paramInt1)) {
      Log.e("OplusNecManager", "broadcastPreferredNetworkMode invalid slotId ");
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("broadcastPreferredNetworkMode ok ,slotId: ");
    stringBuilder.append(paramInt1);
    stringBuilder.append("preferredMode: ");
    stringBuilder.append(paramInt2);
    Log.d("OplusNecManager", stringBuilder.toString());
    Bundle bundle = new Bundle();
    bundle.putInt("preferredMode", paramInt2);
    broadcastNecEvent(paramInt1, 8018, bundle);
  }
  
  public void broadcastDataEnabledChanged(int paramInt, boolean paramBoolean) {
    if (!isValidSlotId(paramInt)) {
      Log.e("OplusNecManager", "broadcastDataEnabledChanged invalid slotId ");
      return;
    } 
    Bundle bundle = new Bundle();
    bundle.putBoolean("enabled", paramBoolean);
    broadcastNecEvent(paramInt, 8021, bundle);
  }
  
  public void broadcastLostConnectionReason(int paramInt1, int paramInt2, int paramInt3) {
    if (!isValidSlotId(paramInt1)) {
      Log.e("OplusNecManager", "broadcastLostConnectionReason invalid slotId ");
      return;
    } 
    Bundle bundle = new Bundle();
    bundle.putInt("errorcode", paramInt2);
    bundle.putInt("lostReason", paramInt3);
    broadcastNecEvent(paramInt1, 8023, bundle);
  }
  
  public void broadcastHangUpDelayTimer(int paramInt1, long paramLong, int paramInt2) {
    if (!isValidSlotId(paramInt1)) {
      Log.e("OplusNecManager", "broadcastHangUpDelayTimer invalid slotId ");
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("broadcastHangUpDelayTimer ok ");
    stringBuilder.append(paramInt1);
    stringBuilder.append(" ");
    stringBuilder.append(paramLong);
    stringBuilder.append(" ");
    stringBuilder.append(paramInt2);
    Log.d("OplusNecManager", stringBuilder.toString());
    Bundle bundle = new Bundle();
    bundle.putLong("millis", paramLong);
    bundle.putInt("csOrIms", paramInt2);
    broadcastNecEvent(paramInt1, 8101, bundle);
  }
  
  public void broadcastCdmaResetActiveTimer(int paramInt1, int paramInt2) {
    if (!isValidSlotId(paramInt1)) {
      Log.e("OplusNecManager", "broadcastCdmaResetActiveTimer invalid slotId ");
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("broadcastCdmaResetActiveTimer ok ");
    stringBuilder.append(paramInt1);
    stringBuilder.append("networkType:");
    stringBuilder.append(paramInt2);
    Log.d("OplusNecManager", stringBuilder.toString());
    Bundle bundle = new Bundle();
    bundle.putInt("networkType", paramInt2);
    broadcastNecEvent(paramInt1, 8102, bundle);
  }
  
  public void broadcastPreciseCallStateChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7) {
    if (!isValidSlotId(paramInt1)) {
      Log.e("OplusNecManager", "broadcastPreciseCallStateChanged invalid slotId ");
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("broadcastPreciseCallStateChanged ok ");
    stringBuilder.append(paramInt1);
    stringBuilder.append(" ");
    stringBuilder.append(paramInt2);
    stringBuilder.append(" ");
    stringBuilder.append(paramInt3);
    stringBuilder.append(" ");
    stringBuilder.append(paramInt4);
    stringBuilder.append(" ");
    stringBuilder.append(paramInt5);
    stringBuilder.append(" ");
    stringBuilder.append(paramInt6);
    Log.d("OplusNecManager", stringBuilder.toString());
    Bundle bundle = new Bundle();
    bundle.putInt("ring", paramInt2);
    bundle.putInt("foreground", paramInt3);
    bundle.putInt("background", paramInt4);
    bundle.putInt("cause", paramInt5);
    bundle.putInt("preciseCause", paramInt6);
    bundle.putInt("disconnectState", paramInt7);
    broadcastNecEvent(paramInt1, 8103, bundle);
  }
  
  public void broadcastSrvccStateChanged(int paramInt1, int paramInt2) {
    if (!isValidSlotId(paramInt1)) {
      Log.e("OplusNecManager", "broadcastSrvccStateChanged invalid slotId ");
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("broadcastSrvccStateChanged ok ");
    stringBuilder.append(paramInt1);
    stringBuilder.append(" ");
    stringBuilder.append(paramInt2);
    Log.d("OplusNecManager", stringBuilder.toString());
    Bundle bundle = new Bundle();
    bundle.putInt("srvccState", paramInt2);
    broadcastNecEvent(paramInt1, 8104, bundle);
  }
  
  public void broadcastCallError(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString, boolean paramBoolean1, boolean paramBoolean2) {
    if (!isValidSlotId(paramInt1)) {
      Log.e("OplusNecManager", "broadcastCallError invalid slotId ");
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("broadcastCallError  slotId:");
    stringBuilder.append(paramInt1);
    stringBuilder.append(" ,event:");
    stringBuilder.append(paramInt2);
    stringBuilder.append(",desc: ");
    stringBuilder.append(paramString);
    stringBuilder.append(",cause: ");
    stringBuilder.append(paramInt3);
    stringBuilder.append(",preciseCause:");
    stringBuilder.append(paramInt4);
    Log.d("OplusNecManager", stringBuilder.toString());
    Bundle bundle = new Bundle();
    bundle.putInt("slotId", paramInt1);
    bundle.putInt("cause", paramInt3);
    bundle.putInt("preciseCause", paramInt4);
    bundle.putInt("event", paramInt2);
    bundle.putString("desc", paramString);
    bundle.putBoolean("isImsCall", paramBoolean1);
    bundle.putBoolean("isIncoming", paramBoolean2);
    broadcastNecEvent(paramInt1, 8107, bundle);
  }
  
  public void broadcastVolteCallKeylog(int paramInt1, int paramInt2, String paramString) {
    if (!isValidSlotId(paramInt1)) {
      Log.e("OplusNecManager", "broadcastVolteCallKeylog invalid slotId ");
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("broadcastVolteCallKeylog  slotId:");
    stringBuilder.append(paramInt1);
    stringBuilder.append(" ,event:");
    stringBuilder.append(paramInt2);
    stringBuilder.append(", desc:");
    stringBuilder.append(paramString);
    Log.d("OplusNecManager", stringBuilder.toString());
    Bundle bundle = new Bundle();
    bundle.putInt("slotId", paramInt1);
    bundle.putInt("event", paramInt2);
    bundle.putString("desc", paramString);
    broadcastNecEvent(paramInt1, 8109, bundle);
  }
  
  public void broadcastImsRegisterState(int paramInt, boolean paramBoolean) {
    if (!isValidSlotId(paramInt)) {
      Log.e("OplusNecManager", "broadcastImsRegisterState invalid slotId ");
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("broadcastImsRegisterState ok ");
    stringBuilder.append(paramInt);
    stringBuilder.append(" ");
    stringBuilder.append(paramBoolean);
    Log.d("OplusNecManager", stringBuilder.toString());
    Bundle bundle = new Bundle();
    bundle.putBoolean("imsRegisterState", paramBoolean);
    broadcastNecEvent(paramInt, 8105, bundle);
  }
  
  public void broadcastVolteVopsOrSettingChanged(int paramInt1, int paramInt2, boolean paramBoolean) {
    if (!isValidSlotId(paramInt1)) {
      Log.e("OplusNecManager", "broadcastVolteVopsOrSettingChanged invalid slotId ");
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("broadcastVolteVopsOrSettingChanged  slotId:");
    stringBuilder.append(paramInt1);
    stringBuilder.append(" ,event:");
    stringBuilder.append(paramInt2);
    stringBuilder.append(", isVolteEnabled:");
    stringBuilder.append(paramBoolean);
    Log.d("OplusNecManager", stringBuilder.toString());
    Bundle bundle = new Bundle();
    bundle.putInt("slotId", paramInt1);
    bundle.putInt("event", paramInt2);
    bundle.putBoolean("volteEnabled", paramBoolean);
    broadcastNecEvent(paramInt1, 8108, bundle);
  }
  
  public void broadcastRegInfoChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    if (!isValidSlotId(paramInt5)) {
      Log.e("OplusNecManager", "broadcastRegInfoChanged invalid slotId ");
      return;
    } 
    if (paramInt1 == this.mVoiceRegState[paramInt5] && paramInt2 == this.mVoiceNetworkType[paramInt5] && paramInt3 == this.mDataRegState[paramInt5] && paramInt4 == this.mDataNetworkType[paramInt5])
      return; 
    this.mVoiceRegState[paramInt5] = paramInt1;
    this.mVoiceNetworkType[paramInt5] = paramInt2;
    this.mDataRegState[paramInt5] = paramInt3;
    this.mDataNetworkType[paramInt5] = paramInt4;
    Bundle bundle = new Bundle();
    bundle.putInt("voiceRegState", paramInt1);
    bundle.putInt("voiceNetworkType", paramInt2);
    bundle.putInt("dataRegState", paramInt3);
    bundle.putInt("dataNetworkType", paramInt4);
    broadcastNecEvent(paramInt5, 8017, bundle);
  }
  
  private boolean isValidSlotId(int paramInt) {
    if (paramInt < 0 || paramInt > SIM_COUNT)
      return false; 
    return true;
  }
  
  private static void necLog(String paramString) {
    Log.d("OplusNecManager", paramString);
  }
}
