package com.oplus.nec;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOnNecEventListener extends IInterface {
  String onCollectPwrStatistic(boolean paramBoolean) throws RemoteException;
  
  void onNecEventReport(int paramInt1, int paramInt2, Bundle paramBundle) throws RemoteException;
  
  void onStandbyStart(boolean paramBoolean) throws RemoteException;
  
  class Default implements IOnNecEventListener {
    public void onNecEventReport(int param1Int1, int param1Int2, Bundle param1Bundle) throws RemoteException {}
    
    public void onStandbyStart(boolean param1Boolean) throws RemoteException {}
    
    public String onCollectPwrStatistic(boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOnNecEventListener {
    private static final String DESCRIPTOR = "com.oplus.nec.IOnNecEventListener";
    
    static final int TRANSACTION_onCollectPwrStatistic = 3;
    
    static final int TRANSACTION_onNecEventReport = 1;
    
    static final int TRANSACTION_onStandbyStart = 2;
    
    public Stub() {
      attachInterface(this, "com.oplus.nec.IOnNecEventListener");
    }
    
    public static IOnNecEventListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.nec.IOnNecEventListener");
      if (iInterface != null && iInterface instanceof IOnNecEventListener)
        return (IOnNecEventListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onCollectPwrStatistic";
        } 
        return "onStandbyStart";
      } 
      return "onNecEventReport";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1) {
        boolean bool1 = false, bool2 = false;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.oplus.nec.IOnNecEventListener");
            return true;
          } 
          param1Parcel1.enforceInterface("com.oplus.nec.IOnNecEventListener");
          bool1 = bool2;
          if (param1Parcel1.readInt() != 0)
            bool1 = true; 
          str = onCollectPwrStatistic(bool1);
          param1Parcel2.writeNoException();
          param1Parcel2.writeString(str);
          return true;
        } 
        str.enforceInterface("com.oplus.nec.IOnNecEventListener");
        if (str.readInt() != 0)
          bool1 = true; 
        onStandbyStart(bool1);
        param1Parcel2.writeNoException();
        return true;
      } 
      str.enforceInterface("com.oplus.nec.IOnNecEventListener");
      param1Int2 = str.readInt();
      param1Int1 = str.readInt();
      if (str.readInt() != 0) {
        Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str);
      } else {
        str = null;
      } 
      onNecEventReport(param1Int2, param1Int1, (Bundle)str);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IOnNecEventListener {
      public static IOnNecEventListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.nec.IOnNecEventListener";
      }
      
      public void onNecEventReport(int param2Int1, int param2Int2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.oplus.nec.IOnNecEventListener");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOnNecEventListener.Stub.getDefaultImpl() != null) {
            IOnNecEventListener.Stub.getDefaultImpl().onNecEventReport(param2Int1, param2Int2, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onStandbyStart(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.oplus.nec.IOnNecEventListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool1 && IOnNecEventListener.Stub.getDefaultImpl() != null) {
            IOnNecEventListener.Stub.getDefaultImpl().onStandbyStart(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String onCollectPwrStatistic(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.oplus.nec.IOnNecEventListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool1 && IOnNecEventListener.Stub.getDefaultImpl() != null)
            return IOnNecEventListener.Stub.getDefaultImpl().onCollectPwrStatistic(param2Boolean); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOnNecEventListener param1IOnNecEventListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOnNecEventListener != null) {
          Proxy.sDefaultImpl = param1IOnNecEventListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOnNecEventListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
