package com.oplus.acmutils;

public class OplusAcmUtils {
  private static final String TAG = "OplusAcmUtils";
  
  public static int sOplusAcmOpenDev() {
    return nativeOpenDev();
  }
  
  public static int sOplusAcmCloseDev() {
    return nativeCloseDev();
  }
  
  public static int sOplusAcmSetAcmOpstat(int paramInt) {
    paramInt = nativeSetAcmOpstat(paramInt);
    return paramInt;
  }
  
  public static int sOplusAcmGetAcmOpstat() {
    return nativeGetAcmOpstat();
  }
  
  public static int sOplusAcmAddPkgName(String paramString, long paramLong) {
    return nativeAddPkgName(paramString, paramLong);
  }
  
  public static int sOplusAcmDelPkgName(String paramString) {
    return nativeDelPkgName(paramString);
  }
  
  public static long sOplusAcmGetPkgFlag(String paramString) {
    return nativeGetPkgFlag(paramString);
  }
  
  public static int sOplusAcmAddDirName(String paramString, long paramLong) {
    return nativeAddDirName(paramString, paramLong);
  }
  
  public static int sOplusAcmDelDirName(String paramString) {
    return nativeDelDirName(paramString);
  }
  
  public static long sOplusAcmGetDirFlag(String paramString) {
    return nativeGetDirFlag(paramString);
  }
  
  public static int sOplusAcmAddNomediaDirName(String paramString) {
    return nativeAddNomediaDirName(paramString);
  }
  
  public static int sOplusAcmDelNomediaDirName(String paramString) {
    return nativeDelNomediaDirName(paramString);
  }
  
  public static int sOplusAcmSearchNomediaDir(String paramString) {
    return nativeSearchNomediaDir(paramString);
  }
  
  private static native int nativeAddDirName(String paramString, long paramLong);
  
  private static native int nativeAddNomediaDirName(String paramString);
  
  private static native int nativeAddPkgName(String paramString, long paramLong);
  
  private static native int nativeCloseDev();
  
  private static native int nativeDelDirName(String paramString);
  
  private static native int nativeDelNomediaDirName(String paramString);
  
  private static native int nativeDelPkgName(String paramString);
  
  private static native int nativeGetAcmOpstat();
  
  private static native long nativeGetDirFlag(String paramString);
  
  private static native long nativeGetPkgFlag(String paramString);
  
  private static native int nativeOpenDev();
  
  private static native int nativeSearchNomediaDir(String paramString);
  
  private static native int nativeSetAcmOpstat(int paramInt);
}
