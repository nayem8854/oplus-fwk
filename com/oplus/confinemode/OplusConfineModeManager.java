package com.oplus.confinemode;

import android.app.OplusActivityManager;
import android.content.Context;
import android.os.Binder;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.util.ArrayMap;
import android.util.Log;
import java.util.List;
import java.util.Map;

public class OplusConfineModeManager {
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  public static final int OPLUS_CONFINE_MODE_CHILDREN = 2;
  
  public static final int OPLUS_CONFINE_MODE_DRIVE = 1;
  
  public static final int OPLUS_CONFINE_MODE_FOCUS = 4;
  
  public static final int OPLUS_CONFINE_MODE_GAME_FOCUS = 8;
  
  public static final int OPLUS_CONFINE_MODE_NORMAL = 0;
  
  public static final int OPLUS_PERMIT_TYPE_APPEND = 2;
  
  public static final int OPLUS_PERMIT_TYPE_CLEAR = 0;
  
  public static final int OPLUS_PERMIT_TYPE_CPN = 8;
  
  public static final int OPLUS_PERMIT_TYPE_PKG = 4;
  
  public static final int OPLUS_PERMIT_TYPE_REPLACE = 1;
  
  private static final String TAG = "OplusConfineModeManager";
  
  private static volatile OplusConfineModeManager sInstance;
  
  private OplusActivityManager mOAms;
  
  private final Map<ConfineModeObserver, IOplusConfineModeObserver> mObservers = (Map<ConfineModeObserver, IOplusConfineModeObserver>)new ArrayMap();
  
  private OplusConfineModeManager() {
    this.mOAms = new OplusActivityManager();
  }
  
  public static OplusConfineModeManager getInstance() {
    // Byte code:
    //   0: getstatic com/oplus/confinemode/OplusConfineModeManager.sInstance : Lcom/oplus/confinemode/OplusConfineModeManager;
    //   3: ifnonnull -> 39
    //   6: ldc com/oplus/confinemode/OplusConfineModeManager
    //   8: monitorenter
    //   9: getstatic com/oplus/confinemode/OplusConfineModeManager.sInstance : Lcom/oplus/confinemode/OplusConfineModeManager;
    //   12: ifnonnull -> 27
    //   15: new com/oplus/confinemode/OplusConfineModeManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/oplus/confinemode/OplusConfineModeManager.sInstance : Lcom/oplus/confinemode/OplusConfineModeManager;
    //   27: ldc com/oplus/confinemode/OplusConfineModeManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/oplus/confinemode/OplusConfineModeManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/oplus/confinemode/OplusConfineModeManager.sInstance : Lcom/oplus/confinemode/OplusConfineModeManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #59	-> 0
    //   #60	-> 6
    //   #61	-> 9
    //   #62	-> 15
    //   #64	-> 27
    //   #66	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public void setConfineMode(int paramInt, boolean paramBoolean) {
    try {
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("setConfineMode: mode=");
        stringBuilder.append(paramInt);
        stringBuilder.append(", on=");
        stringBuilder.append(paramBoolean);
        Log.v("OplusConfineModeManager", stringBuilder.toString());
      } 
      this.mOAms.setConfineMode(paramInt, paramBoolean);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setConfineMode remoteException:");
      stringBuilder.append(remoteException);
      Log.e("OplusConfineModeManager", stringBuilder.toString());
    } 
  }
  
  public int getConfineMode() {
    try {
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("getConfineMode, caller:");
        stringBuilder.append(Binder.getCallingUid());
        Log.v("OplusConfineModeManager", stringBuilder.toString());
      } 
      return this.mOAms.getConfineMode();
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getConfineMode remoteException:");
      stringBuilder.append(remoteException);
      Log.e("OplusConfineModeManager", stringBuilder.toString());
      return 0;
    } 
  }
  
  public void setPermitList(int paramInt1, int paramInt2, List<String> paramList, boolean paramBoolean) {
    try {
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("setPermitList: mode=");
        stringBuilder.append(paramInt1);
        stringBuilder.append(", type=");
        stringBuilder.append(paramInt2);
        stringBuilder.append(", isMultiApp=");
        stringBuilder.append(paramBoolean);
        Log.v("OplusConfineModeManager", stringBuilder.toString());
      } 
      this.mOAms.setPermitList(paramInt1, paramInt2, paramList, paramBoolean);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setPermitList remoteException:");
      stringBuilder.append(remoteException);
      Log.e("OplusConfineModeManager", stringBuilder.toString());
    } 
  }
  
  public boolean registerConfineModeObserver(Context paramContext, ConfineModeObserver paramConfineModeObserver) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("registerConfineModeObserver, context: ");
      stringBuilder.append(paramContext);
      stringBuilder.append(", observer: ");
      stringBuilder.append(paramConfineModeObserver);
      Log.d("OplusConfineModeManager", stringBuilder.toString());
    } 
    if (paramContext == null || paramConfineModeObserver == null)
      return false; 
    synchronized (this.mObservers) {
      if (this.mObservers.get(paramConfineModeObserver) != null) {
        Log.e("OplusConfineModeManager", "already register before");
        return false;
      } 
      ConfineModeObserverDelegate confineModeObserverDelegate = new ConfineModeObserverDelegate();
      this(this, paramConfineModeObserver);
      try {
        if (this.mOAms != null) {
          boolean bool = this.mOAms.registerConfineModeObserver(confineModeObserverDelegate);
          if (bool)
            this.mObservers.put(paramConfineModeObserver, confineModeObserverDelegate); 
          return bool;
        } 
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("registerConfineModeObserver failed, err: ");
        stringBuilder.append(remoteException);
        Log.e("OplusConfineModeManager", stringBuilder.toString());
      } 
      return false;
    } 
  }
  
  public boolean unregisterConfineModeObserver(Context paramContext, ConfineModeObserver paramConfineModeObserver) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("unregisterConfineModeObserver, context: ");
      stringBuilder.append(paramContext);
      stringBuilder.append(", observer: ");
      stringBuilder.append(paramConfineModeObserver);
      Log.d("OplusConfineModeManager", stringBuilder.toString());
    } 
    if (paramContext == null || paramConfineModeObserver == null)
      return false; 
    synchronized (this.mObservers) {
      IOplusConfineModeObserver iOplusConfineModeObserver = this.mObservers.get(paramConfineModeObserver);
      if (iOplusConfineModeObserver != null)
        try {
          if (this.mOAms != null) {
            boolean bool = this.mOAms.unregisterConfineModeObserver(iOplusConfineModeObserver);
            if (bool)
              this.mObservers.remove(paramConfineModeObserver); 
            return bool;
          } 
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("unregisterConfineModeObserver failed, err: ");
          stringBuilder.append(remoteException);
          Log.e("OplusConfineModeManager", stringBuilder.toString());
        }  
      return false;
    } 
  }
  
  public static interface ConfineModeObserver {
    void onChange(int param1Int1, int param1Int2, int param1Int3);
  }
  
  class ConfineModeObserverDelegate extends IOplusConfineModeObserver.Stub {
    private final OplusConfineModeManager.ConfineModeObserver mObserver;
    
    final OplusConfineModeManager this$0;
    
    public ConfineModeObserverDelegate(OplusConfineModeManager.ConfineModeObserver param1ConfineModeObserver) {
      this.mObserver = param1ConfineModeObserver;
    }
    
    public void onChange(int param1Int1, int param1Int2, int param1Int3) {
      if (OplusConfineModeManager.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("confine mode changed, oldMode: ");
        stringBuilder.append(param1Int1);
        stringBuilder.append(", newMode: ");
        stringBuilder.append(param1Int2);
        stringBuilder.append(", userId: ");
        stringBuilder.append(param1Int3);
        Log.d("OplusConfineModeManager", stringBuilder.toString());
      } 
      this.mObserver.onChange(param1Int1, param1Int2, param1Int3);
    }
  }
}
