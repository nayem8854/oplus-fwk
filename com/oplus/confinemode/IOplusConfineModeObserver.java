package com.oplus.confinemode;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusConfineModeObserver extends IInterface {
  void onChange(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  class Default implements IOplusConfineModeObserver {
    public void onChange(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusConfineModeObserver {
    private static final String DESCRIPTOR = "com.oplus.confinemode.IOplusConfineModeObserver";
    
    static final int TRANSACTION_onChange = 1;
    
    public Stub() {
      attachInterface(this, "com.oplus.confinemode.IOplusConfineModeObserver");
    }
    
    public static IOplusConfineModeObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.oplus.confinemode.IOplusConfineModeObserver");
      if (iInterface != null && iInterface instanceof IOplusConfineModeObserver)
        return (IOplusConfineModeObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onChange";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.oplus.confinemode.IOplusConfineModeObserver");
        return true;
      } 
      param1Parcel1.enforceInterface("com.oplus.confinemode.IOplusConfineModeObserver");
      int i = param1Parcel1.readInt();
      param1Int2 = param1Parcel1.readInt();
      param1Int1 = param1Parcel1.readInt();
      onChange(i, param1Int2, param1Int1);
      return true;
    }
    
    private static class Proxy implements IOplusConfineModeObserver {
      public static IOplusConfineModeObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.oplus.confinemode.IOplusConfineModeObserver";
      }
      
      public void onChange(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.oplus.confinemode.IOplusConfineModeObserver");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOplusConfineModeObserver.Stub.getDefaultImpl() != null) {
            IOplusConfineModeObserver.Stub.getDefaultImpl().onChange(param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusConfineModeObserver param1IOplusConfineModeObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusConfineModeObserver != null) {
          Proxy.sDefaultImpl = param1IOplusConfineModeObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusConfineModeObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
