package javax.microedition.khronos.opengles;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

public interface GL11Ext extends GL {
  public static final int GL_MATRIX_INDEX_ARRAY_BUFFER_BINDING_OES = 35742;
  
  public static final int GL_MATRIX_INDEX_ARRAY_OES = 34884;
  
  public static final int GL_MATRIX_INDEX_ARRAY_POINTER_OES = 34889;
  
  public static final int GL_MATRIX_INDEX_ARRAY_SIZE_OES = 34886;
  
  public static final int GL_MATRIX_INDEX_ARRAY_STRIDE_OES = 34888;
  
  public static final int GL_MATRIX_INDEX_ARRAY_TYPE_OES = 34887;
  
  public static final int GL_MATRIX_PALETTE_OES = 34880;
  
  public static final int GL_MAX_PALETTE_MATRICES_OES = 34882;
  
  public static final int GL_MAX_VERTEX_UNITS_OES = 34468;
  
  public static final int GL_TEXTURE_CROP_RECT_OES = 35741;
  
  public static final int GL_WEIGHT_ARRAY_BUFFER_BINDING_OES = 34974;
  
  public static final int GL_WEIGHT_ARRAY_OES = 34477;
  
  public static final int GL_WEIGHT_ARRAY_POINTER_OES = 34476;
  
  public static final int GL_WEIGHT_ARRAY_SIZE_OES = 34475;
  
  public static final int GL_WEIGHT_ARRAY_STRIDE_OES = 34474;
  
  public static final int GL_WEIGHT_ARRAY_TYPE_OES = 34473;
  
  void glCurrentPaletteMatrixOES(int paramInt);
  
  void glDrawTexfOES(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5);
  
  void glDrawTexfvOES(FloatBuffer paramFloatBuffer);
  
  void glDrawTexfvOES(float[] paramArrayOffloat, int paramInt);
  
  void glDrawTexiOES(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5);
  
  void glDrawTexivOES(IntBuffer paramIntBuffer);
  
  void glDrawTexivOES(int[] paramArrayOfint, int paramInt);
  
  void glDrawTexsOES(short paramShort1, short paramShort2, short paramShort3, short paramShort4, short paramShort5);
  
  void glDrawTexsvOES(ShortBuffer paramShortBuffer);
  
  void glDrawTexsvOES(short[] paramArrayOfshort, int paramInt);
  
  void glDrawTexxOES(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5);
  
  void glDrawTexxvOES(IntBuffer paramIntBuffer);
  
  void glDrawTexxvOES(int[] paramArrayOfint, int paramInt);
  
  void glEnable(int paramInt);
  
  void glEnableClientState(int paramInt);
  
  void glLoadPaletteFromModelViewMatrixOES();
  
  void glMatrixIndexPointerOES(int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  void glMatrixIndexPointerOES(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer);
  
  void glTexParameterfv(int paramInt1, int paramInt2, float[] paramArrayOffloat, int paramInt3);
  
  void glWeightPointerOES(int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  void glWeightPointerOES(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer);
}
