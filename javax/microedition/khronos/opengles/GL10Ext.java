package javax.microedition.khronos.opengles;

import java.nio.IntBuffer;

public interface GL10Ext extends GL {
  int glQueryMatrixxOES(IntBuffer paramIntBuffer1, IntBuffer paramIntBuffer2);
  
  int glQueryMatrixxOES(int[] paramArrayOfint1, int paramInt1, int[] paramArrayOfint2, int paramInt2);
}
