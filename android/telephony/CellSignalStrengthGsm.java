package android.telephony;

import android.hardware.radio.V1_0.GsmSignalStrength;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PersistableBundle;
import com.android.telephony.Rlog;
import java.util.Objects;

public final class CellSignalStrengthGsm extends CellSignalStrength implements Parcelable {
  public static final Parcelable.Creator<CellSignalStrengthGsm> CREATOR;
  
  private static final boolean DBG = false;
  
  private static final int GSM_RSSI_GOOD = -97;
  
  private static final int GSM_RSSI_GREAT = -89;
  
  private static final int GSM_RSSI_MAX = -51;
  
  private static final int GSM_RSSI_MIN = -113;
  
  private static final int GSM_RSSI_MODERATE = -103;
  
  private static final int GSM_RSSI_POOR = -107;
  
  private static final String LOG_TAG = "CellSignalStrengthGsm";
  
  private static final CellSignalStrengthGsm sInvalid;
  
  private static final int[] sRssiThresholds = new int[] { -107, -103, -97, -89 };
  
  private int mBitErrorRate;
  
  private int mLevel;
  
  private int mRssi;
  
  private int mTimingAdvance;
  
  public CellSignalStrengthGsm() {
    setDefaultValues();
  }
  
  public CellSignalStrengthGsm(int paramInt1, int paramInt2, int paramInt3) {
    this.mRssi = inRangeOrUnavailable(paramInt1, -113, -51);
    this.mBitErrorRate = inRangeOrUnavailable(paramInt2, 0, 7, 99);
    this.mTimingAdvance = inRangeOrUnavailable(paramInt3, 0, 219);
    updateLevel((PersistableBundle)null, (ServiceState)null);
  }
  
  public CellSignalStrengthGsm(GsmSignalStrength paramGsmSignalStrength) {
    this(getRssiDbmFromAsu(paramGsmSignalStrength.signalStrength), paramGsmSignalStrength.bitErrorRate, paramGsmSignalStrength.timingAdvance);
    if (this.mRssi == Integer.MAX_VALUE)
      setDefaultValues(); 
  }
  
  public CellSignalStrengthGsm(CellSignalStrengthGsm paramCellSignalStrengthGsm) {
    copyFrom(paramCellSignalStrengthGsm);
  }
  
  protected void copyFrom(CellSignalStrengthGsm paramCellSignalStrengthGsm) {
    this.mRssi = paramCellSignalStrengthGsm.mRssi;
    this.mBitErrorRate = paramCellSignalStrengthGsm.mBitErrorRate;
    this.mTimingAdvance = paramCellSignalStrengthGsm.mTimingAdvance;
    this.mLevel = paramCellSignalStrengthGsm.mLevel;
  }
  
  public CellSignalStrengthGsm copy() {
    return new CellSignalStrengthGsm(this);
  }
  
  public void setDefaultValues() {
    this.mRssi = Integer.MAX_VALUE;
    this.mBitErrorRate = Integer.MAX_VALUE;
    this.mTimingAdvance = Integer.MAX_VALUE;
    this.mLevel = 0;
  }
  
  public int getLevel() {
    return this.mLevel;
  }
  
  public void updateLevel(PersistableBundle paramPersistableBundle, ServiceState paramServiceState) {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull -> 11
    //   4: getstatic android/telephony/CellSignalStrengthGsm.sRssiThresholds : [I
    //   7: astore_1
    //   8: goto -> 34
    //   11: aload_1
    //   12: ldc 'gsm_rssi_thresholds_int_array'
    //   14: invokevirtual getIntArray : (Ljava/lang/String;)[I
    //   17: astore_2
    //   18: aload_2
    //   19: ifnull -> 30
    //   22: aload_2
    //   23: astore_1
    //   24: aload_2
    //   25: arraylength
    //   26: iconst_4
    //   27: if_icmpeq -> 34
    //   30: getstatic android/telephony/CellSignalStrengthGsm.sRssiThresholds : [I
    //   33: astore_1
    //   34: iconst_4
    //   35: istore_3
    //   36: aload_0
    //   37: getfield mRssi : I
    //   40: istore #4
    //   42: iload #4
    //   44: bipush #-113
    //   46: if_icmplt -> 87
    //   49: iload #4
    //   51: bipush #-51
    //   53: if_icmple -> 59
    //   56: goto -> 87
    //   59: iload_3
    //   60: ifle -> 81
    //   63: aload_0
    //   64: getfield mRssi : I
    //   67: aload_1
    //   68: iload_3
    //   69: iconst_1
    //   70: isub
    //   71: iaload
    //   72: if_icmpge -> 81
    //   75: iinc #3, -1
    //   78: goto -> 59
    //   81: aload_0
    //   82: iload_3
    //   83: putfield mLevel : I
    //   86: return
    //   87: aload_0
    //   88: iconst_0
    //   89: putfield mLevel : I
    //   92: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #118	-> 0
    //   #119	-> 4
    //   #121	-> 11
    //   #122	-> 18
    //   #123	-> 30
    //   #126	-> 34
    //   #127	-> 36
    //   #131	-> 59
    //   #132	-> 81
    //   #133	-> 86
    //   #128	-> 87
    //   #129	-> 92
  }
  
  public int getTimingAdvance() {
    return this.mTimingAdvance;
  }
  
  public int getDbm() {
    return this.mRssi;
  }
  
  public int getAsuLevel() {
    return getAsuFromRssiDbm(this.mRssi);
  }
  
  public int getRssi() {
    return this.mRssi;
  }
  
  public int getBitErrorRate() {
    return this.mBitErrorRate;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mRssi), Integer.valueOf(this.mBitErrorRate), Integer.valueOf(this.mTimingAdvance) });
  }
  
  static {
    sInvalid = new CellSignalStrengthGsm();
    CREATOR = new Parcelable.Creator<CellSignalStrengthGsm>() {
        public CellSignalStrengthGsm createFromParcel(Parcel param1Parcel) {
          return new CellSignalStrengthGsm(param1Parcel);
        }
        
        public CellSignalStrengthGsm[] newArray(int param1Int) {
          return new CellSignalStrengthGsm[param1Int];
        }
      };
  }
  
  public boolean isValid() {
    return equals(sInvalid) ^ true;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof CellSignalStrengthGsm;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    bool = bool1;
    if (this.mRssi == ((CellSignalStrengthGsm)paramObject).mRssi) {
      bool = bool1;
      if (this.mBitErrorRate == ((CellSignalStrengthGsm)paramObject).mBitErrorRate) {
        bool = bool1;
        if (this.mTimingAdvance == ((CellSignalStrengthGsm)paramObject).mTimingAdvance) {
          bool = bool1;
          if (this.mLevel == ((CellSignalStrengthGsm)paramObject).mLevel)
            bool = true; 
        } 
      } 
    } 
    return bool;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CellSignalStrengthGsm: rssi=");
    stringBuilder.append(this.mRssi);
    stringBuilder.append(" ber=");
    stringBuilder.append(this.mBitErrorRate);
    stringBuilder.append(" mTa=");
    stringBuilder.append(this.mTimingAdvance);
    stringBuilder.append(" mLevel=");
    stringBuilder.append(this.mLevel);
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mRssi);
    paramParcel.writeInt(this.mBitErrorRate);
    paramParcel.writeInt(this.mTimingAdvance);
    paramParcel.writeInt(this.mLevel);
  }
  
  private CellSignalStrengthGsm(Parcel paramParcel) {
    this.mRssi = paramParcel.readInt();
    this.mBitErrorRate = paramParcel.readInt();
    this.mTimingAdvance = paramParcel.readInt();
    this.mLevel = paramParcel.readInt();
  }
  
  public int describeContents() {
    return 0;
  }
  
  private static void log(String paramString) {
    Rlog.w("CellSignalStrengthGsm", paramString);
  }
}
