package android.telephony;

import android.hardware.radio.V1_0.CellInfo;
import android.hardware.radio.V1_2.CellInfo;
import android.hardware.radio.V1_4.CellInfo;
import android.hardware.radio.V1_5.CellInfo;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.telephony.Rlog;

public final class CellInfoCdma extends CellInfo implements Parcelable {
  public CellInfoCdma() {
    this.mCellIdentityCdma = new CellIdentityCdma();
    this.mCellSignalStrengthCdma = new CellSignalStrengthCdma();
  }
  
  public CellInfoCdma(CellInfoCdma paramCellInfoCdma) {
    super(paramCellInfoCdma);
    this.mCellIdentityCdma = paramCellInfoCdma.mCellIdentityCdma.copy();
    this.mCellSignalStrengthCdma = paramCellInfoCdma.mCellSignalStrengthCdma.copy();
  }
  
  public CellInfoCdma(CellInfo paramCellInfo) {
    super(paramCellInfo);
    android.hardware.radio.V1_0.CellInfoCdma cellInfoCdma = paramCellInfo.cdma.get(0);
    this.mCellIdentityCdma = new CellIdentityCdma(cellInfoCdma.cellIdentityCdma);
    this.mCellSignalStrengthCdma = new CellSignalStrengthCdma(cellInfoCdma.signalStrengthCdma, cellInfoCdma.signalStrengthEvdo);
  }
  
  public CellInfoCdma(CellInfo paramCellInfo) {
    super(paramCellInfo);
    android.hardware.radio.V1_2.CellInfoCdma cellInfoCdma = paramCellInfo.cdma.get(0);
    this.mCellIdentityCdma = new CellIdentityCdma(cellInfoCdma.cellIdentityCdma);
    this.mCellSignalStrengthCdma = new CellSignalStrengthCdma(cellInfoCdma.signalStrengthCdma, cellInfoCdma.signalStrengthEvdo);
  }
  
  public CellInfoCdma(CellInfo paramCellInfo, long paramLong) {
    super(paramCellInfo, paramLong);
    android.hardware.radio.V1_2.CellInfoCdma cellInfoCdma = paramCellInfo.info.cdma();
    this.mCellIdentityCdma = new CellIdentityCdma(cellInfoCdma.cellIdentityCdma);
    this.mCellSignalStrengthCdma = new CellSignalStrengthCdma(cellInfoCdma.signalStrengthCdma, cellInfoCdma.signalStrengthEvdo);
  }
  
  public CellInfoCdma(CellInfo paramCellInfo, long paramLong) {
    super(paramCellInfo, paramLong);
    android.hardware.radio.V1_2.CellInfoCdma cellInfoCdma = paramCellInfo.ratSpecificInfo.cdma();
    this.mCellIdentityCdma = new CellIdentityCdma(cellInfoCdma.cellIdentityCdma);
    this.mCellSignalStrengthCdma = new CellSignalStrengthCdma(cellInfoCdma.signalStrengthCdma, cellInfoCdma.signalStrengthEvdo);
  }
  
  public CellIdentityCdma getCellIdentity() {
    return this.mCellIdentityCdma;
  }
  
  public void setCellIdentity(CellIdentityCdma paramCellIdentityCdma) {
    this.mCellIdentityCdma = paramCellIdentityCdma;
  }
  
  public CellSignalStrengthCdma getCellSignalStrength() {
    return this.mCellSignalStrengthCdma;
  }
  
  public CellInfo sanitizeLocationInfo() {
    CellInfoCdma cellInfoCdma = new CellInfoCdma(this);
    cellInfoCdma.mCellIdentityCdma = this.mCellIdentityCdma.sanitizeLocationInfo();
    return cellInfoCdma;
  }
  
  public void setCellSignalStrength(CellSignalStrengthCdma paramCellSignalStrengthCdma) {
    this.mCellSignalStrengthCdma = paramCellSignalStrengthCdma;
  }
  
  public int hashCode() {
    return super.hashCode() + this.mCellIdentityCdma.hashCode() + this.mCellSignalStrengthCdma.hashCode();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = super.equals(paramObject);
    boolean bool1 = false;
    if (!bool)
      return false; 
    try {
      CellInfoCdma cellInfoCdma = (CellInfoCdma)paramObject;
      if (this.mCellIdentityCdma.equals(cellInfoCdma.mCellIdentityCdma)) {
        paramObject = this.mCellSignalStrengthCdma;
        CellSignalStrengthCdma cellSignalStrengthCdma = cellInfoCdma.mCellSignalStrengthCdma;
        bool = paramObject.equals(cellSignalStrengthCdma);
        if (bool)
          bool1 = true; 
      } 
      return bool1;
    } catch (ClassCastException classCastException) {
      return false;
    } 
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append("CellInfoCdma:{");
    stringBuffer.append(super.toString());
    stringBuffer.append(" ");
    stringBuffer.append(this.mCellIdentityCdma);
    stringBuffer.append(" ");
    stringBuffer.append(this.mCellSignalStrengthCdma);
    stringBuffer.append("}");
    return stringBuffer.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcel(paramParcel, paramInt, 2);
    this.mCellIdentityCdma.writeToParcel(paramParcel, paramInt);
    this.mCellSignalStrengthCdma.writeToParcel(paramParcel, paramInt);
  }
  
  private CellInfoCdma(Parcel paramParcel) {
    super(paramParcel);
    this.mCellIdentityCdma = (CellIdentityCdma)CellIdentityCdma.CREATOR.createFromParcel(paramParcel);
    this.mCellSignalStrengthCdma = (CellSignalStrengthCdma)CellSignalStrengthCdma.CREATOR.createFromParcel(paramParcel);
  }
  
  public static final Parcelable.Creator<CellInfoCdma> CREATOR = (Parcelable.Creator<CellInfoCdma>)new Object();
  
  private static final boolean DBG = false;
  
  private static final String LOG_TAG = "CellInfoCdma";
  
  private CellIdentityCdma mCellIdentityCdma;
  
  private CellSignalStrengthCdma mCellSignalStrengthCdma;
  
  protected static CellInfoCdma createFromParcelBody(Parcel paramParcel) {
    return new CellInfoCdma(paramParcel);
  }
  
  private static void log(String paramString) {
    Rlog.w("CellInfoCdma", paramString);
  }
}
