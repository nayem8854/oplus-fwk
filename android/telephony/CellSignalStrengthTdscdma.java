package android.telephony;

import android.hardware.radio.V1_0.TdScdmaSignalStrength;
import android.hardware.radio.V1_2.TdscdmaSignalStrength;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PersistableBundle;
import com.android.telephony.Rlog;
import java.util.Objects;

public final class CellSignalStrengthTdscdma extends CellSignalStrength implements Parcelable {
  public static final Parcelable.Creator<CellSignalStrengthTdscdma> CREATOR;
  
  private static final boolean DBG = false;
  
  private static final String LOG_TAG = "CellSignalStrengthTdscdma";
  
  private static final int TDSCDMA_RSCP_GOOD = -73;
  
  private static final int TDSCDMA_RSCP_GREAT = -49;
  
  private static final int TDSCDMA_RSCP_MAX = -24;
  
  private static final int TDSCDMA_RSCP_MIN = -120;
  
  private static final int TDSCDMA_RSCP_MODERATE = -97;
  
  private static final int TDSCDMA_RSCP_POOR = -110;
  
  public CellSignalStrengthTdscdma() {
    setDefaultValues();
  }
  
  public CellSignalStrengthTdscdma(int paramInt1, int paramInt2, int paramInt3) {
    this.mRssi = inRangeOrUnavailable(paramInt1, -113, -51);
    this.mBitErrorRate = inRangeOrUnavailable(paramInt2, 0, 7, 99);
    this.mRscp = inRangeOrUnavailable(paramInt3, -120, -24);
    updateLevel((PersistableBundle)null, (ServiceState)null);
  }
  
  public CellSignalStrengthTdscdma(TdScdmaSignalStrength paramTdScdmaSignalStrength) {
    this(2147483647, 2147483647, i);
    int i;
    if (this.mRssi == Integer.MAX_VALUE && this.mRscp == Integer.MAX_VALUE)
      setDefaultValues(); 
  }
  
  public CellSignalStrengthTdscdma(TdscdmaSignalStrength paramTdscdmaSignalStrength) {
    this(i, j, k);
    if (this.mRssi == Integer.MAX_VALUE && this.mRscp == Integer.MAX_VALUE)
      setDefaultValues(); 
  }
  
  public CellSignalStrengthTdscdma(CellSignalStrengthTdscdma paramCellSignalStrengthTdscdma) {
    copyFrom(paramCellSignalStrengthTdscdma);
  }
  
  protected void copyFrom(CellSignalStrengthTdscdma paramCellSignalStrengthTdscdma) {
    this.mRssi = paramCellSignalStrengthTdscdma.mRssi;
    this.mBitErrorRate = paramCellSignalStrengthTdscdma.mBitErrorRate;
    this.mRscp = paramCellSignalStrengthTdscdma.mRscp;
    this.mLevel = paramCellSignalStrengthTdscdma.mLevel;
  }
  
  public CellSignalStrengthTdscdma copy() {
    return new CellSignalStrengthTdscdma(this);
  }
  
  public void setDefaultValues() {
    this.mRssi = Integer.MAX_VALUE;
    this.mBitErrorRate = Integer.MAX_VALUE;
    this.mRscp = Integer.MAX_VALUE;
    this.mLevel = 0;
  }
  
  public int getLevel() {
    return this.mLevel;
  }
  
  public void updateLevel(PersistableBundle paramPersistableBundle, ServiceState paramServiceState) {
    int i = this.mRscp;
    if (i > -24) {
      this.mLevel = 0;
    } else if (i >= -49) {
      this.mLevel = 4;
    } else if (i >= -73) {
      this.mLevel = 3;
    } else if (i >= -97) {
      this.mLevel = 2;
    } else if (i >= -110) {
      this.mLevel = 1;
    } else {
      this.mLevel = 0;
    } 
  }
  
  public int getDbm() {
    return this.mRscp;
  }
  
  public int getRscp() {
    return this.mRscp;
  }
  
  public int getRssi() {
    return this.mRssi;
  }
  
  public int getBitErrorRate() {
    return this.mBitErrorRate;
  }
  
  public int getAsuLevel() {
    int i = this.mRscp;
    if (i != Integer.MAX_VALUE)
      return getAsuFromRscpDbm(i); 
    i = this.mRssi;
    if (i != Integer.MAX_VALUE)
      return getAsuFromRssiDbm(i); 
    return getAsuFromRscpDbm(2147483647);
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mRssi), Integer.valueOf(this.mBitErrorRate), Integer.valueOf(this.mRscp), Integer.valueOf(this.mLevel) });
  }
  
  private static final CellSignalStrengthTdscdma sInvalid = new CellSignalStrengthTdscdma();
  
  private int mBitErrorRate;
  
  private int mLevel;
  
  private int mRscp;
  
  private int mRssi;
  
  public boolean isValid() {
    return equals(sInvalid) ^ true;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof CellSignalStrengthTdscdma;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    bool = bool1;
    if (this.mRssi == ((CellSignalStrengthTdscdma)paramObject).mRssi) {
      bool = bool1;
      if (this.mBitErrorRate == ((CellSignalStrengthTdscdma)paramObject).mBitErrorRate) {
        bool = bool1;
        if (this.mRscp == ((CellSignalStrengthTdscdma)paramObject).mRscp) {
          bool = bool1;
          if (this.mLevel == ((CellSignalStrengthTdscdma)paramObject).mLevel)
            bool = true; 
        } 
      } 
    } 
    return bool;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CellSignalStrengthTdscdma: rssi=");
    stringBuilder.append(this.mRssi);
    stringBuilder.append(" ber=");
    stringBuilder.append(this.mBitErrorRate);
    stringBuilder.append(" rscp=");
    stringBuilder.append(this.mRscp);
    stringBuilder.append(" level=");
    stringBuilder.append(this.mLevel);
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mRssi);
    paramParcel.writeInt(this.mBitErrorRate);
    paramParcel.writeInt(this.mRscp);
    paramParcel.writeInt(this.mLevel);
  }
  
  private CellSignalStrengthTdscdma(Parcel paramParcel) {
    this.mRssi = paramParcel.readInt();
    this.mBitErrorRate = paramParcel.readInt();
    this.mRscp = paramParcel.readInt();
    this.mLevel = paramParcel.readInt();
  }
  
  public int describeContents() {
    return 0;
  }
  
  static {
    CREATOR = new Parcelable.Creator<CellSignalStrengthTdscdma>() {
        public CellSignalStrengthTdscdma createFromParcel(Parcel param1Parcel) {
          return new CellSignalStrengthTdscdma(param1Parcel);
        }
        
        public CellSignalStrengthTdscdma[] newArray(int param1Int) {
          return new CellSignalStrengthTdscdma[param1Int];
        }
      };
  }
  
  private static void log(String paramString) {
    Rlog.w("CellSignalStrengthTdscdma", paramString);
  }
}
