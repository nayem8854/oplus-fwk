package android.telephony;

import android.annotation.SystemApi;
import android.compat.Compatibility;
import android.net.LinkProperties;
import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.data.ApnSetting;
import java.util.Objects;

public final class PreciseDataConnectionState implements Parcelable {
  private int mState = -1;
  
  private int mNetworkType = 0;
  
  private int mFailCause = 0;
  
  private int mApnTypes = 0;
  
  private String mApn = "";
  
  private LinkProperties mLinkProperties = null;
  
  private ApnSetting mApnSetting = null;
  
  @Deprecated
  public PreciseDataConnectionState(int paramInt1, int paramInt2, int paramInt3, String paramString, LinkProperties paramLinkProperties, int paramInt4) {
    this(paramInt1, paramInt2, paramInt3, paramString, paramLinkProperties, paramInt4, null);
  }
  
  public PreciseDataConnectionState(int paramInt1, int paramInt2, int paramInt3, String paramString, LinkProperties paramLinkProperties, int paramInt4, ApnSetting paramApnSetting) {
    this.mState = paramInt1;
    this.mNetworkType = paramInt2;
    this.mApnTypes = paramInt3;
    this.mApn = paramString;
    this.mLinkProperties = paramLinkProperties;
    this.mFailCause = paramInt4;
    this.mApnSetting = paramApnSetting;
  }
  
  private PreciseDataConnectionState(Parcel paramParcel) {
    this.mState = paramParcel.readInt();
    this.mNetworkType = paramParcel.readInt();
    this.mApnTypes = paramParcel.readInt();
    this.mApn = paramParcel.readString();
    this.mLinkProperties = (LinkProperties)paramParcel.readParcelable(null);
    this.mFailCause = paramParcel.readInt();
    this.mApnSetting = (ApnSetting)paramParcel.readParcelable(null);
  }
  
  @SystemApi
  @Deprecated
  public int getDataConnectionState() {
    if (this.mState == 4 && 
      !Compatibility.isChangeEnabled(148535736L))
      return 2; 
    return this.mState;
  }
  
  public int getState() {
    return this.mState;
  }
  
  @SystemApi
  @Deprecated
  public int getDataConnectionNetworkType() {
    return this.mNetworkType;
  }
  
  public int getNetworkType() {
    return this.mNetworkType;
  }
  
  @SystemApi
  @Deprecated
  public int getDataConnectionApnTypeBitMask() {
    return this.mApnTypes;
  }
  
  @SystemApi
  @Deprecated
  public String getDataConnectionApn() {
    return this.mApn;
  }
  
  @SystemApi
  @Deprecated
  public LinkProperties getDataConnectionLinkProperties() {
    return this.mLinkProperties;
  }
  
  public LinkProperties getLinkProperties() {
    return this.mLinkProperties;
  }
  
  @SystemApi
  @Deprecated
  public int getDataConnectionFailCause() {
    return this.mFailCause;
  }
  
  public int getLastCauseCode() {
    return this.mFailCause;
  }
  
  public ApnSetting getApnSetting() {
    return this.mApnSetting;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mState);
    paramParcel.writeInt(this.mNetworkType);
    paramParcel.writeInt(this.mApnTypes);
    paramParcel.writeString(this.mApn);
    paramParcel.writeParcelable((Parcelable)this.mLinkProperties, paramInt);
    paramParcel.writeInt(this.mFailCause);
    paramParcel.writeParcelable(this.mApnSetting, paramInt);
  }
  
  public static final Parcelable.Creator<PreciseDataConnectionState> CREATOR = new Parcelable.Creator<PreciseDataConnectionState>() {
      public PreciseDataConnectionState createFromParcel(Parcel param1Parcel) {
        return new PreciseDataConnectionState(param1Parcel);
      }
      
      public PreciseDataConnectionState[] newArray(int param1Int) {
        return new PreciseDataConnectionState[param1Int];
      }
    };
  
  private static final long GET_DATA_CONNECTION_STATE_R_VERSION = 148535736L;
  
  public int hashCode() {
    int i = this.mState, j = this.mNetworkType, k = this.mApnTypes;
    String str = this.mApn;
    LinkProperties linkProperties = this.mLinkProperties;
    int m = this.mFailCause;
    ApnSetting apnSetting = this.mApnSetting;
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), str, linkProperties, Integer.valueOf(m), apnSetting });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof PreciseDataConnectionState;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    if (Objects.equals(this.mApn, ((PreciseDataConnectionState)paramObject).mApn) && this.mApnTypes == ((PreciseDataConnectionState)paramObject).mApnTypes && this.mFailCause == ((PreciseDataConnectionState)paramObject).mFailCause) {
      LinkProperties linkProperties1 = this.mLinkProperties, linkProperties2 = ((PreciseDataConnectionState)paramObject).mLinkProperties;
      if (Objects.equals(linkProperties1, linkProperties2) && this.mNetworkType == ((PreciseDataConnectionState)paramObject).mNetworkType && this.mState == ((PreciseDataConnectionState)paramObject).mState) {
        ApnSetting apnSetting = this.mApnSetting;
        paramObject = ((PreciseDataConnectionState)paramObject).mApnSetting;
        if (Objects.equals(apnSetting, paramObject))
          bool1 = true; 
      } 
    } 
    return bool1;
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("Data Connection state: ");
    stringBuilder2.append(this.mState);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", Network type: ");
    stringBuilder2.append(this.mNetworkType);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", APN types: ");
    stringBuilder2.append(ApnSetting.getApnTypesStringFromBitmask(this.mApnTypes));
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", APN: ");
    stringBuilder2.append(this.mApn);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", Link properties: ");
    stringBuilder2.append(this.mLinkProperties);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", Fail cause: ");
    stringBuilder2.append(DataFailCause.toString(this.mFailCause));
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", Apn Setting: ");
    stringBuilder2.append(this.mApnSetting);
    stringBuilder1.append(stringBuilder2.toString());
    return stringBuilder1.toString();
  }
  
  public PreciseDataConnectionState() {}
}
