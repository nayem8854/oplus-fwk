package android.telephony;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.ParcelUuid;
import android.os.Parcelable;
import com.android.internal.util.IndentingPrintWriter;
import com.android.telephony.Rlog;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public final class AnomalyReporter {
  private static final String TAG = "AnomalyReporter";
  
  private static Context sContext = null;
  
  private static String sDebugPackageName;
  
  private static Map<UUID, Integer> sEvents = new ConcurrentHashMap<>();
  
  static {
    sDebugPackageName = null;
  }
  
  public static void reportAnomaly(UUID paramUUID, String paramString) {
    StringBuilder stringBuilder;
    boolean bool;
    if (sContext == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("AnomalyReporter not yet initialized, dropping event=");
      stringBuilder.append(paramUUID);
      Rlog.w("AnomalyReporter", stringBuilder.toString());
      return;
    } 
    if (sEvents.containsKey(paramUUID)) {
      bool = ((Integer)sEvents.get(paramUUID)).intValue() + 1;
    } else {
      bool = true;
    } 
    Integer integer = Integer.valueOf(bool);
    sEvents.put(paramUUID, integer);
    if (integer.intValue() > 1)
      return; 
    if (sDebugPackageName == null)
      return; 
    Intent intent = new Intent("android.telephony.action.ANOMALY_REPORTED");
    intent.putExtra("android.telephony.extra.ANOMALY_ID", (Parcelable)new ParcelUuid(paramUUID));
    if (stringBuilder != null)
      intent.putExtra("android.telephony.extra.ANOMALY_DESCRIPTION", (String)stringBuilder); 
    intent.setPackage(sDebugPackageName);
    sContext.sendBroadcast(intent, "android.permission.READ_PRIVILEGED_PHONE_STATE");
  }
  
  public static void initialize(Context paramContext) {
    if (paramContext != null) {
      paramContext.enforceCallingOrSelfPermission("android.permission.MODIFY_PHONE_STATE", "This app does not have privileges to send debug events");
      sContext = paramContext;
      PackageManager packageManager = paramContext.getPackageManager();
      if (packageManager == null)
        return; 
      List list = packageManager.queryBroadcastReceivers(new Intent("android.telephony.action.ANOMALY_REPORTED"), 1835008);
      if (list == null || list.isEmpty())
        return; 
      if (list.size() > 1)
        Rlog.e("AnomalyReporter", "Multiple Anomaly Receivers installed."); 
      for (ResolveInfo resolveInfo : list) {
        if (resolveInfo.activityInfo != null) {
          String str = resolveInfo.activityInfo.packageName;
          if (packageManager.checkPermission("android.permission.READ_PRIVILEGED_PHONE_STATE", str) == 0) {
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("Found a valid package ");
            stringBuilder1.append(resolveInfo.activityInfo.packageName);
            Rlog.d("AnomalyReporter", stringBuilder1.toString());
            sDebugPackageName = resolveInfo.activityInfo.packageName;
            break;
          } 
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Found package without proper permissions or no activity");
        stringBuilder.append(resolveInfo.activityInfo.packageName);
        Rlog.w("AnomalyReporter", stringBuilder.toString());
      } 
      return;
    } 
    throw new IllegalArgumentException("AnomalyReporter needs a non-null context.");
  }
  
  public static void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    String str;
    if (sContext == null)
      return; 
    IndentingPrintWriter indentingPrintWriter = new IndentingPrintWriter(paramPrintWriter, "  ");
    sContext.enforceCallingOrSelfPermission("android.permission.DUMP", "Requires DUMP");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("Initialized=");
    if (sContext != null) {
      str = "Yes";
    } else {
      str = "No";
    } 
    stringBuilder2.append(str);
    indentingPrintWriter.println(stringBuilder2.toString());
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("Debug Package=");
    stringBuilder1.append(sDebugPackageName);
    indentingPrintWriter.println(stringBuilder1.toString());
    indentingPrintWriter.println("Anomaly Counts:");
    indentingPrintWriter.increaseIndent();
    for (UUID uUID : sEvents.keySet()) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(uUID);
      stringBuilder1.append(": ");
      stringBuilder1.append(sEvents.get(uUID));
      indentingPrintWriter.println(stringBuilder1.toString());
    } 
    indentingPrintWriter.decreaseIndent();
    indentingPrintWriter.flush();
  }
}
