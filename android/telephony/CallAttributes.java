package android.telephony;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

@SystemApi
public final class CallAttributes implements Parcelable {
  public CallAttributes(PreciseCallState paramPreciseCallState, int paramInt, CallQuality paramCallQuality) {
    this.mPreciseCallState = paramPreciseCallState;
    this.mNetworkType = paramInt;
    this.mCallQuality = paramCallQuality;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("mPreciseCallState=");
    stringBuilder.append(this.mPreciseCallState);
    stringBuilder.append(" mNetworkType=");
    stringBuilder.append(this.mNetworkType);
    stringBuilder.append(" mCallQuality=");
    stringBuilder.append(this.mCallQuality);
    return stringBuilder.toString();
  }
  
  private CallAttributes(Parcel paramParcel) {
    this.mPreciseCallState = (PreciseCallState)paramParcel.readParcelable(PreciseCallState.class.getClassLoader());
    this.mNetworkType = paramParcel.readInt();
    this.mCallQuality = (CallQuality)paramParcel.readParcelable(CallQuality.class.getClassLoader());
  }
  
  public PreciseCallState getPreciseCallState() {
    return this.mPreciseCallState;
  }
  
  public int getNetworkType() {
    return this.mNetworkType;
  }
  
  public CallQuality getCallQuality() {
    return this.mCallQuality;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mPreciseCallState, Integer.valueOf(this.mNetworkType), this.mCallQuality });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    if (paramObject == null || !(paramObject instanceof CallAttributes) || hashCode() != paramObject.hashCode())
      return false; 
    if (this == paramObject)
      return true; 
    CallAttributes callAttributes = (CallAttributes)paramObject;
    if (Objects.equals(this.mPreciseCallState, callAttributes.mPreciseCallState) && this.mNetworkType == callAttributes.mNetworkType) {
      paramObject = this.mCallQuality;
      CallQuality callQuality = callAttributes.mCallQuality;
      if (Objects.equals(paramObject, callQuality))
        bool = true; 
    } 
    return bool;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.mPreciseCallState, paramInt);
    paramParcel.writeInt(this.mNetworkType);
    paramParcel.writeParcelable(this.mCallQuality, paramInt);
  }
  
  public static final Parcelable.Creator<CallAttributes> CREATOR = new Parcelable.Creator() {
      public CallAttributes createFromParcel(Parcel param1Parcel) {
        return new CallAttributes(param1Parcel);
      }
      
      public CallAttributes[] newArray(int param1Int) {
        return new CallAttributes[param1Int];
      }
    };
  
  private CallQuality mCallQuality;
  
  private int mNetworkType;
  
  private PreciseCallState mPreciseCallState;
}
