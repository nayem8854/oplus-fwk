package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

public final class RadioAccessSpecifier implements Parcelable {
  public RadioAccessSpecifier(int paramInt, int[] paramArrayOfint1, int[] paramArrayOfint2) {
    this.mRadioAccessNetwork = paramInt;
    if (paramArrayOfint1 != null) {
      this.mBands = (int[])paramArrayOfint1.clone();
    } else {
      this.mBands = null;
    } 
    if (paramArrayOfint2 != null) {
      this.mChannels = (int[])paramArrayOfint2.clone();
    } else {
      this.mChannels = null;
    } 
  }
  
  public int getRadioAccessNetwork() {
    return this.mRadioAccessNetwork;
  }
  
  public int[] getBands() {
    int[] arrayOfInt = this.mBands;
    if (arrayOfInt == null) {
      arrayOfInt = null;
    } else {
      arrayOfInt = (int[])arrayOfInt.clone();
    } 
    return arrayOfInt;
  }
  
  public int[] getChannels() {
    int[] arrayOfInt = this.mChannels;
    if (arrayOfInt == null) {
      arrayOfInt = null;
    } else {
      arrayOfInt = (int[])arrayOfInt.clone();
    } 
    return arrayOfInt;
  }
  
  public static final Parcelable.Creator<RadioAccessSpecifier> CREATOR = new Parcelable.Creator<RadioAccessSpecifier>() {
      public RadioAccessSpecifier createFromParcel(Parcel param1Parcel) {
        return new RadioAccessSpecifier(param1Parcel);
      }
      
      public RadioAccessSpecifier[] newArray(int param1Int) {
        return new RadioAccessSpecifier[param1Int];
      }
    };
  
  private int[] mBands;
  
  private int[] mChannels;
  
  private int mRadioAccessNetwork;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mRadioAccessNetwork);
    paramParcel.writeIntArray(this.mBands);
    paramParcel.writeIntArray(this.mChannels);
  }
  
  private RadioAccessSpecifier(Parcel paramParcel) {
    this.mRadioAccessNetwork = paramParcel.readInt();
    this.mBands = paramParcel.createIntArray();
    this.mChannels = paramParcel.createIntArray();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    try {
      RadioAccessSpecifier radioAccessSpecifier = (RadioAccessSpecifier)paramObject;
      if (paramObject == null)
        return false; 
      if (this.mRadioAccessNetwork == radioAccessSpecifier.mRadioAccessNetwork) {
        int[] arrayOfInt = this.mBands;
        paramObject = radioAccessSpecifier.mBands;
        if (Arrays.equals(arrayOfInt, (int[])paramObject)) {
          paramObject = this.mChannels;
          int[] arrayOfInt1 = radioAccessSpecifier.mChannels;
          if (Arrays.equals((int[])paramObject, arrayOfInt1))
            bool = true; 
        } 
      } 
      return bool;
    } catch (ClassCastException classCastException) {
      return false;
    } 
  }
  
  public int hashCode() {
    int i = this.mRadioAccessNetwork, arrayOfInt[] = this.mBands;
    int j = Arrays.hashCode(arrayOfInt);
    arrayOfInt = this.mChannels;
    int k = Arrays.hashCode(arrayOfInt);
    return i * 31 + j * 37 + k * 39;
  }
}
