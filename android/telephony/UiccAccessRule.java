package android.telephony;

import android.annotation.SystemApi;
import android.content.pm.PackageInfo;
import android.content.pm.Signature;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.android.internal.telephony.uicc.IccUtils;
import com.android.telephony.Rlog;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@SystemApi
public final class UiccAccessRule implements Parcelable {
  public static final Parcelable.Creator<UiccAccessRule> CREATOR = new Parcelable.Creator<UiccAccessRule>() {
      public UiccAccessRule createFromParcel(Parcel param1Parcel) {
        return new UiccAccessRule(param1Parcel);
      }
      
      public UiccAccessRule[] newArray(int param1Int) {
        return new UiccAccessRule[param1Int];
      }
    };
  
  private static final int ENCODING_VERSION = 1;
  
  private static final String TAG = "UiccAccessRule";
  
  private final long mAccessType;
  
  private final byte[] mCertificateHash;
  
  private final String mPackageName;
  
  public static byte[] encodeRules(UiccAccessRule[] paramArrayOfUiccAccessRule) {
    if (paramArrayOfUiccAccessRule == null)
      return null; 
    try {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      this();
      DataOutputStream dataOutputStream = new DataOutputStream();
      this(byteArrayOutputStream);
      dataOutputStream.writeInt(1);
      dataOutputStream.writeInt(paramArrayOfUiccAccessRule.length);
      int i;
      byte b;
      for (i = paramArrayOfUiccAccessRule.length, b = 0; b < i; ) {
        UiccAccessRule uiccAccessRule = paramArrayOfUiccAccessRule[b];
        dataOutputStream.writeInt(uiccAccessRule.mCertificateHash.length);
        dataOutputStream.write(uiccAccessRule.mCertificateHash);
        if (uiccAccessRule.mPackageName != null) {
          dataOutputStream.writeBoolean(true);
          dataOutputStream.writeUTF(uiccAccessRule.mPackageName);
        } else {
          dataOutputStream.writeBoolean(false);
        } 
        dataOutputStream.writeLong(uiccAccessRule.mAccessType);
        b++;
      } 
      dataOutputStream.close();
      return byteArrayOutputStream.toByteArray();
    } catch (IOException iOException) {
      throw new IllegalStateException("ByteArrayOutputStream should never lead to an IOException", iOException);
    } 
  }
  
  public static UiccAccessRule[] decodeRules(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte == null)
      return null; 
    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(paramArrayOfbyte);
    try {
      DataInputStream dataInputStream = new DataInputStream();
      this(byteArrayInputStream);
      try {
        dataInputStream.readInt();
        int i = dataInputStream.readInt();
        UiccAccessRule[] arrayOfUiccAccessRule = new UiccAccessRule[i];
        for (byte b = 0; b < i; b++) {
          int j = dataInputStream.readInt();
          byte[] arrayOfByte = new byte[j];
          dataInputStream.readFully(arrayOfByte);
          if (dataInputStream.readBoolean()) {
            String str = dataInputStream.readUTF();
          } else {
            byteArrayInputStream = null;
          } 
          long l = dataInputStream.readLong();
          arrayOfUiccAccessRule[b] = new UiccAccessRule(arrayOfByte, (String)byteArrayInputStream, l);
        } 
        dataInputStream.close();
        return arrayOfUiccAccessRule;
      } finally {
        try {
          dataInputStream.close();
        } finally {
          dataInputStream = null;
        } 
      } 
    } catch (IOException iOException) {
      throw new IllegalStateException("ByteArrayInputStream should never lead to an IOException", iOException);
    } 
  }
  
  public UiccAccessRule(byte[] paramArrayOfbyte, String paramString, long paramLong) {
    this.mCertificateHash = paramArrayOfbyte;
    this.mPackageName = paramString;
    this.mAccessType = paramLong;
  }
  
  UiccAccessRule(Parcel paramParcel) {
    this.mCertificateHash = paramParcel.createByteArray();
    this.mPackageName = paramParcel.readString();
    this.mAccessType = paramParcel.readLong();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByteArray(this.mCertificateHash);
    paramParcel.writeString(this.mPackageName);
    paramParcel.writeLong(this.mAccessType);
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
  
  public String getCertificateHexString() {
    return IccUtils.bytesToHexString(this.mCertificateHash);
  }
  
  public int getCarrierPrivilegeStatus(PackageInfo paramPackageInfo) {
    List<Signature> list = getSignatures(paramPackageInfo);
    if (!list.isEmpty()) {
      for (Signature signature : list) {
        int i = getCarrierPrivilegeStatus(signature, paramPackageInfo.packageName);
        if (i != 0)
          return i; 
      } 
      return 0;
    } 
    throw new IllegalArgumentException("Must use GET_SIGNING_CERTIFICATES when looking up package info");
  }
  
  public int getCarrierPrivilegeStatus(Signature paramSignature, String paramString) {
    byte[] arrayOfByte2 = getCertHash(paramSignature, "SHA-1");
    byte[] arrayOfByte1 = getCertHash(paramSignature, "SHA-256");
    if (matches(arrayOfByte2, paramString) || matches(arrayOfByte1, paramString))
      return 1; 
    return 0;
  }
  
  private boolean matches(byte[] paramArrayOfbyte, String paramString) {
    if (paramArrayOfbyte != null && Arrays.equals(this.mCertificateHash, paramArrayOfbyte)) {
      String str = this.mPackageName;
      if (TextUtils.isEmpty(str) || this.mPackageName.equals(paramString))
        return true; 
    } 
    return false;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    UiccAccessRule uiccAccessRule = (UiccAccessRule)paramObject;
    if (Arrays.equals(this.mCertificateHash, uiccAccessRule.mCertificateHash)) {
      paramObject = this.mPackageName;
      String str = uiccAccessRule.mPackageName;
      if (Objects.equals(paramObject, str) && this.mAccessType == uiccAccessRule.mAccessType)
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = Arrays.hashCode(this.mCertificateHash);
    int j = Objects.hashCode(this.mPackageName);
    int k = Objects.hashCode(Long.valueOf(this.mAccessType));
    return ((1 * 31 + i) * 31 + j) * 31 + k;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("cert: ");
    stringBuilder.append(IccUtils.bytesToHexString(this.mCertificateHash));
    stringBuilder.append(" pkg: ");
    stringBuilder.append(this.mPackageName);
    stringBuilder.append(" access: ");
    stringBuilder.append(this.mAccessType);
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static List<Signature> getSignatures(PackageInfo paramPackageInfo) {
    // Byte code:
    //   0: aload_0
    //   1: getfield signatures : [Landroid/content/pm/Signature;
    //   4: astore_1
    //   5: aload_0
    //   6: getfield signingInfo : Landroid/content/pm/SigningInfo;
    //   9: astore_2
    //   10: aload_1
    //   11: astore_0
    //   12: aload_2
    //   13: ifnull -> 33
    //   16: aload_2
    //   17: invokevirtual getSigningCertificateHistory : ()[Landroid/content/pm/Signature;
    //   20: astore_0
    //   21: aload_2
    //   22: invokevirtual hasMultipleSigners : ()Z
    //   25: ifeq -> 33
    //   28: aload_2
    //   29: invokevirtual getApkContentsSigners : ()[Landroid/content/pm/Signature;
    //   32: astore_0
    //   33: aload_0
    //   34: ifnonnull -> 44
    //   37: getstatic java/util/Collections.EMPTY_LIST : Ljava/util/List;
    //   40: astore_0
    //   41: goto -> 49
    //   44: aload_0
    //   45: invokestatic asList : ([Ljava/lang/Object;)Ljava/util/List;
    //   48: astore_0
    //   49: aload_0
    //   50: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #264	-> 0
    //   #265	-> 5
    //   #267	-> 10
    //   #268	-> 16
    //   #269	-> 21
    //   #270	-> 28
    //   #274	-> 33
  }
  
  public static byte[] getCertHash(Signature paramSignature, String paramString) {
    try {
      MessageDigest messageDigest = MessageDigest.getInstance(paramString);
      return messageDigest.digest(paramSignature.toByteArray());
    } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("NoSuchAlgorithmException: ");
      stringBuilder.append(noSuchAlgorithmException);
      Rlog.e("UiccAccessRule", stringBuilder.toString());
      return null;
    } 
  }
}
