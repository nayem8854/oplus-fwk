package android.telephony;

import android.content.Context;
import android.content.res.Resources;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.telephony.EncodeException;
import com.android.internal.telephony.GsmAlphabet;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class OplusTelephonyFunction {
  public static final int CONCATENATED_8_BIT_REFERENCE_LENGTH = 5;
  
  private static final boolean DBG = false;
  
  private static final List<String> LONG_NAME_OPERATOR;
  
  public static final int MAX_LONG_NAME_LENGTH = 18;
  
  private static final String OP_SIMLOCK_OPERATOR = "persist.oplus.network.operator";
  
  public static final int PORT_ADDRESS_16_REFERENCE_LENGTH = 6;
  
  private static String PROJECT_MULTISIM_CONFIG = SystemProperties.get("persist.radio.multisim.config", "unknow");
  
  private static final String TAG = "OplusTelephonyFunction";
  
  static {
    LONG_NAME_OPERATOR = Arrays.asList(new String[] { "28602", "52505", "45406" });
  }
  
  public static String stripSeparators(String paramString) {
    if (paramString == null)
      return null; 
    int i = paramString.length();
    StringBuilder stringBuilder = new StringBuilder(i);
    for (byte b = 0; b < i; b++) {
      char c = paramString.charAt(b);
      if (PhoneNumberUtils.isNonSeparator(c))
        stringBuilder.append(c); 
    } 
    return stringBuilder.toString();
  }
  
  public static byte[][] divideDataMessage(byte[] paramArrayOfbyte) {
    int i = paramArrayOfbyte.length;
    int j = 133;
    if (i > 133)
      j = 133 - 5; 
    int k = (i + j - 1) / j;
    byte b = 0;
    byte[][] arrayOfByte = new byte[k][];
    while (i > 0) {
      if (i > j) {
        k = j;
      } else {
        k = i;
      } 
      i -= k;
      arrayOfByte[b] = new byte[k];
      System.arraycopy(paramArrayOfbyte, b * j, arrayOfByte[b], 0, k);
      b++;
    } 
    return arrayOfByte;
  }
  
  public static ByteBuffer createBufferWithNativeByteOrder(byte[] paramArrayOfbyte) {
    ByteBuffer byteBuffer = ByteBuffer.wrap(paramArrayOfbyte);
    byteBuffer.order(ByteOrder.nativeOrder());
    return byteBuffer;
  }
  
  public static int getMinMatch() {
    byte b;
    String str = SystemProperties.get("persist.sys.oplus.region", "CN");
    if (str.equalsIgnoreCase("CN")) {
      b = 11;
    } else {
      b = 7;
    } 
    return b;
  }
  
  private static int countGsmSeptets(CharSequence paramCharSequence, boolean paramBoolean, int paramInt) throws EncodeException {
    paramInt = 0;
    int i = paramCharSequence.length();
    int j = 0;
    while (paramInt < i) {
      j += GsmAlphabet.countGsmSeptets(paramCharSequence.charAt(paramInt), paramBoolean);
      paramInt++;
    } 
    return j;
  }
  
  public static byte[] stringToGsm8BitOrUCSPackedForADN(String paramString) {
    byte[] arrayOfByte;
    if (paramString == null)
      return null; 
    try {
      int i = countGsmSeptets(paramString, true, 1);
      byte[] arrayOfByte1 = new byte[i];
      GsmAlphabet.stringToGsm8BitUnpackedField(paramString, arrayOfByte1, 0, arrayOfByte1.length);
      arrayOfByte = arrayOfByte1;
    } catch (EncodeException encodeException) {
      try {
        byte[] arrayOfByte1 = arrayOfByte.getBytes("utf-16be");
        arrayOfByte = new byte[arrayOfByte1.length + 1];
        arrayOfByte[0] = Byte.MIN_VALUE;
        System.arraycopy(arrayOfByte1, 0, arrayOfByte, 1, arrayOfByte1.length);
        return arrayOfByte;
      } catch (UnsupportedEncodingException unsupportedEncodingException) {
        Log.e("OplusTelephonyFunction", "unsurport encoding.", unsupportedEncodingException);
        return null;
      } 
    } 
    return (byte[])unsupportedEncodingException;
  }
  
  public static int dmAutoRegisterSmsOrigPort(String paramString) {
    byte b = 0;
    int i = b;
    if (!TextUtils.isEmpty(paramString)) {
      int j = paramString.indexOf(":");
      i = b;
      if (-1 != j)
        try {
          i = Integer.parseInt(paramString.substring(j + 1).toString());
        } catch (NumberFormatException numberFormatException) {
          i = b;
        }  
    } 
    return i;
  }
  
  public static String dmAutoRegisterSmsAddress(String paramString) {
    String str = paramString;
    if (!TextUtils.isEmpty(paramString)) {
      int i = paramString.indexOf(":");
      str = paramString;
      if (-1 != i)
        if (i == 0) {
          str = null;
        } else {
          try {
            str = paramString.substring(0, i).toString();
          } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
            str = paramString;
          } 
        }  
    } 
    return str;
  }
  
  public static String oplusGetPlmnOverride(Context paramContext, String paramString, ServiceState paramServiceState) {
    Locale locale = Locale.getDefault();
    String str3 = locale.getLanguage().toLowerCase();
    String str4 = locale.getCountry().toLowerCase();
    String str2 = str3;
    if (str3.equals("zh"))
      if (str4.equals("cn")) {
        str2 = "zh_cn";
      } else {
        str2 = "zh_tw";
      }  
    if (!TextUtils.isEmpty(paramString))
      try {
        if (OplusSpnOverride.getInstance(str2).containsCarrier(paramString)) {
          str = OplusSpnOverride.getInstance(str2).getSpn(paramString);
          return str;
        } 
        if (OplusSpnOverride.getInstance("en").containsCarrier(paramString)) {
          str = OplusSpnOverride.getInstance("en").getSpn(paramString);
          return str;
        } 
        Resources resources = str.getResources();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("mccmnc");
        stringBuilder.append(paramString);
        String str = str.getString(resources.getIdentifier(stringBuilder.toString(), "string", "com.android.phone"));
        return str;
      } catch (Exception exception) {} 
    if (paramServiceState == null)
      return null; 
    str2 = paramServiceState.getOperatorAlphaShort();
    str3 = paramServiceState.getOperatorAlphaLong();
    String str1 = str2;
    if (!TextUtils.isEmpty(str3)) {
      str1 = str2;
      if (str3.length() <= 18)
        str1 = str3; 
    } 
    if (TextUtils.isEmpty(str1) || str1.equals(paramString))
      str1 = paramServiceState.getOperatorAlphaLong(); 
    return str1;
  }
  
  public static boolean oplusGetSingleSimCard() {
    boolean bool1 = isOpenMarketSingleSimCard();
    boolean bool2 = bool1;
    if (!bool1)
      bool2 = isOperatorSingleSimCard(); 
    return bool2;
  }
  
  private static boolean isOpenMarketSingleSimCard() {
    return isOperatorSingleSimCard();
  }
  
  private static boolean isOperatorSingleSimCard() {
    if ("ssss".equals(PROJECT_MULTISIM_CONFIG) || "ss".equals(PROJECT_MULTISIM_CONFIG))
      return true; 
    return false;
  }
  
  public static boolean oplusIsSimLockedEnabledTH() {
    if (isOperatorSingleSimCard())
      return true; 
    return false;
  }
  
  public static boolean oplusIsSimLockedEnabled() {
    if (!"-1".equals(SystemProperties.get("persist.oplus.network.operator", "-1")) && 
      !"0".equals(SystemProperties.get("persist.oplus.network.operator", "-1")))
      return true; 
    return false;
  }
}
