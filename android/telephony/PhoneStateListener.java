package android.telephony;

import android.annotation.SystemApi;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerExecutor;
import android.os.Looper;
import android.telephony.emergency.EmergencyNumber;
import android.telephony.ims.ImsReasonInfo;
import com.android.internal.telephony.IPhoneStateListener;
import dalvik.system.VMRuntime;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

public class PhoneStateListener {
  private static final boolean DBG = false;
  
  public static final int DEFAULT_PER_PID_REGISTRATION_LIMIT = 50;
  
  public static final String FLAG_PER_PID_REGISTRATION_LIMIT = "phone_state_listener_per_pid_registration_limit";
  
  public static final int LISTEN_ACTIVE_DATA_SUBSCRIPTION_ID_CHANGE = 4194304;
  
  public static final int LISTEN_ALWAYS_REPORTED_SIGNAL_STRENGTH = 512;
  
  public static final int LISTEN_BARRING_INFO = -2147483648;
  
  @SystemApi
  public static final int LISTEN_CALL_ATTRIBUTES_CHANGED = 67108864;
  
  public static final int LISTEN_CALL_DISCONNECT_CAUSES = 33554432;
  
  public static final int LISTEN_CALL_FORWARDING_INDICATOR = 8;
  
  public static final int LISTEN_CALL_STATE = 32;
  
  public static final int LISTEN_CARRIER_NETWORK_CHANGE = 65536;
  
  public static final int LISTEN_CELL_INFO = 1024;
  
  public static final int LISTEN_CELL_LOCATION = 16;
  
  public static final int LISTEN_DATA_ACTIVATION_STATE = 262144;
  
  public static final int LISTEN_DATA_ACTIVITY = 128;
  
  @Deprecated
  public static final int LISTEN_DATA_CONNECTION_REAL_TIME_INFO = 8192;
  
  public static final int LISTEN_DATA_CONNECTION_STATE = 64;
  
  public static final int LISTEN_DISPLAY_INFO_CHANGED = 1048576;
  
  public static final int LISTEN_EMERGENCY_NUMBER_LIST = 16777216;
  
  public static final int LISTEN_IMS_CALL_DISCONNECT_CAUSES = 134217728;
  
  public static final int LISTEN_MESSAGE_WAITING_INDICATOR = 4;
  
  public static final int LISTEN_NONE = 0;
  
  @Deprecated
  public static final int LISTEN_OEM_HOOK_RAW_EVENT = 32768;
  
  @SystemApi
  public static final int LISTEN_OUTGOING_EMERGENCY_CALL = 268435456;
  
  @SystemApi
  public static final int LISTEN_OUTGOING_EMERGENCY_SMS = 536870912;
  
  public static final int LISTEN_PHONE_CAPABILITY_CHANGE = 2097152;
  
  @SystemApi
  public static final int LISTEN_PRECISE_CALL_STATE = 2048;
  
  public static final int LISTEN_PRECISE_DATA_CONNECTION_STATE = 4096;
  
  @SystemApi
  public static final int LISTEN_RADIO_POWER_STATE_CHANGED = 8388608;
  
  public static final int LISTEN_REGISTRATION_FAILURE = 1073741824;
  
  public static final int LISTEN_SERVICE_STATE = 1;
  
  @Deprecated
  public static final int LISTEN_SIGNAL_STRENGTH = 2;
  
  public static final int LISTEN_SIGNAL_STRENGTHS = 256;
  
  @SystemApi
  public static final int LISTEN_SRVCC_STATE_CHANGED = 16384;
  
  public static final int LISTEN_USER_MOBILE_DATA_STATE = 524288;
  
  @SystemApi
  public static final int LISTEN_VOICE_ACTIVATION_STATE = 131072;
  
  private static final String LOG_TAG = "PhoneStateListener";
  
  public static final long PHONE_STATE_LISTENER_LIMIT_CHANGE_ID = 150880553L;
  
  public final IPhoneStateListener callback;
  
  protected Integer mSubId;
  
  public PhoneStateListener() {
    this((Integer)null, Looper.myLooper());
  }
  
  public PhoneStateListener(Looper paramLooper) {
    this((Integer)null, paramLooper);
  }
  
  public PhoneStateListener(Integer paramInteger) {
    this(paramInteger, Looper.myLooper());
    if (paramInteger == null || VMRuntime.getRuntime().getTargetSdkVersion() < 29)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("PhoneStateListener with subId: ");
    stringBuilder.append(paramInteger);
    stringBuilder.append(" is not supported, use default constructor");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public PhoneStateListener(Integer paramInteger, Looper paramLooper) {
    this(paramInteger, (Executor)new HandlerExecutor(new Handler(paramLooper)));
    if (paramInteger == null || VMRuntime.getRuntime().getTargetSdkVersion() < 29)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("PhoneStateListener with subId: ");
    stringBuilder.append(paramInteger);
    stringBuilder.append(" is not supported, use default constructor");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public PhoneStateListener(Executor paramExecutor) {
    this((Integer)null, paramExecutor);
  }
  
  private PhoneStateListener(Integer paramInteger, Executor paramExecutor) {
    if (paramExecutor != null) {
      this.mSubId = paramInteger;
      this.callback = (IPhoneStateListener)new IPhoneStateListenerStub(this, paramExecutor);
      return;
    } 
    throw new IllegalArgumentException("PhoneStateListener Executor must be non-null");
  }
  
  public void onServiceStateChanged(ServiceState paramServiceState) {}
  
  @Deprecated
  public void onSignalStrengthChanged(int paramInt) {}
  
  public void onMessageWaitingIndicatorChanged(boolean paramBoolean) {}
  
  public void onCallForwardingIndicatorChanged(boolean paramBoolean) {}
  
  public void onCellLocationChanged(CellLocation paramCellLocation) {}
  
  public void onCallStateChanged(int paramInt, String paramString) {}
  
  public void onDataConnectionStateChanged(int paramInt) {}
  
  public void onDataConnectionStateChanged(int paramInt1, int paramInt2) {}
  
  public void onDataActivity(int paramInt) {}
  
  public void onSignalStrengthsChanged(SignalStrength paramSignalStrength) {}
  
  public void onCellInfoChanged(List<CellInfo> paramList) {}
  
  @SystemApi
  public void onPreciseCallStateChanged(PreciseCallState paramPreciseCallState) {}
  
  public void onCallDisconnectCauseChanged(int paramInt1, int paramInt2) {}
  
  public void onImsCallDisconnectCauseChanged(ImsReasonInfo paramImsReasonInfo) {}
  
  public void onPreciseDataConnectionStateChanged(PreciseDataConnectionState paramPreciseDataConnectionState) {}
  
  public void onDataConnectionRealTimeInfoChanged(DataConnectionRealTimeInfo paramDataConnectionRealTimeInfo) {}
  
  @SystemApi
  public void onSrvccStateChanged(int paramInt) {}
  
  @SystemApi
  public void onVoiceActivationStateChanged(int paramInt) {}
  
  public void onDataActivationStateChanged(int paramInt) {}
  
  public void onUserMobileDataStateChanged(boolean paramBoolean) {}
  
  public void onDisplayInfoChanged(TelephonyDisplayInfo paramTelephonyDisplayInfo) {}
  
  public void onEmergencyNumberListChanged(Map<Integer, List<EmergencyNumber>> paramMap) {}
  
  @SystemApi
  public void onOutgoingEmergencyCall(EmergencyNumber paramEmergencyNumber) {}
  
  @SystemApi
  public void onOutgoingEmergencySms(EmergencyNumber paramEmergencyNumber) {}
  
  public void onOemHookRawEvent(byte[] paramArrayOfbyte) {}
  
  public void onPhoneCapabilityChanged(PhoneCapability paramPhoneCapability) {}
  
  public void onActiveDataSubscriptionIdChanged(int paramInt) {}
  
  @SystemApi
  public void onCallAttributesChanged(CallAttributes paramCallAttributes) {}
  
  @SystemApi
  public void onRadioPowerStateChanged(int paramInt) {}
  
  public void onCarrierNetworkChange(boolean paramBoolean) {}
  
  public void onRegistrationFailed(CellIdentity paramCellIdentity, String paramString, int paramInt1, int paramInt2, int paramInt3) {}
  
  public void onBarringInfoChanged(BarringInfo paramBarringInfo) {}
  
  class IPhoneStateListenerStub extends IPhoneStateListener.Stub {
    private Executor mExecutor;
    
    private WeakReference<PhoneStateListener> mPhoneStateListenerWeakRef;
    
    IPhoneStateListenerStub(PhoneStateListener this$0, Executor param1Executor) {
      this.mPhoneStateListenerWeakRef = new WeakReference<>(this$0);
      this.mExecutor = param1Executor;
    }
    
    public void onServiceStateChanged(ServiceState param1ServiceState) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$uC5syhzl229gIpaK7Jfs__OCJxQ(this, phoneStateListener, param1ServiceState));
    }
    
    public void onSignalStrengthChanged(int param1Int) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$M39is_Zyt8D7Camw2NS4EGTDn_s(this, phoneStateListener, param1Int));
    }
    
    public void onMessageWaitingIndicatorChanged(boolean param1Boolean) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$okPCYOx4UxYuvUHlM2iS425QGIg(this, phoneStateListener, param1Boolean));
    }
    
    public void onCallForwardingIndicatorChanged(boolean param1Boolean) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$1M3m0i6211i2YjWyTDT7l0bJm3I(this, phoneStateListener, param1Boolean));
    }
    
    public void onCellLocationChanged(CellIdentity param1CellIdentity) {
      CellLocation cellLocation;
      if (param1CellIdentity == null) {
        cellLocation = CellLocation.getEmpty();
      } else {
        cellLocation = cellLocation.asCellLocation();
      } 
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$Hbn6_eZxY2p3rjOfStodI04A8E8(this, phoneStateListener, cellLocation));
    }
    
    public void onCallStateChanged(int param1Int, String param1String) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$oDAZqs8paeefe_3k_uRKV5plQW4(this, phoneStateListener, param1Int, param1String));
    }
    
    public void onDataConnectionStateChanged(int param1Int1, int param1Int2) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      if (param1Int1 == 4 && 
        VMRuntime.getRuntime().getTargetSdkVersion() < 30) {
        Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$NjMtWvO8dQakD688KRREWiYI4JI(this, phoneStateListener, param1Int2));
      } else {
        Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$pLr_IfJJu1u_YG6I5LI0iHTuBi0(this, phoneStateListener, param1Int1, param1Int2));
      } 
    }
    
    public void onDataActivity(int param1Int) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$l57DgyMDrONq3sajd_dBE967ClU(this, phoneStateListener, param1Int));
    }
    
    public void onSignalStrengthsChanged(SignalStrength param1SignalStrength) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$j6NpsS_PE3VHutxIDEmwFHop7Yc(this, phoneStateListener, param1SignalStrength));
    }
    
    public void onCellInfoChanged(List<CellInfo> param1List) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$yvQnAlFGg5EWDG2vcA9X_4xnalA(this, phoneStateListener, param1List));
    }
    
    public void onPreciseCallStateChanged(PreciseCallState param1PreciseCallState) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$bELzxgwsPigyVKYkAXBO2BjcSm8(this, phoneStateListener, param1PreciseCallState));
    }
    
    public void onCallDisconnectCauseChanged(int param1Int1, int param1Int2) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$icX71zgNszuMfnDaCmahcqWacFM(this, phoneStateListener, param1Int1, param1Int2));
    }
    
    public void onPreciseDataConnectionStateChanged(PreciseDataConnectionState param1PreciseDataConnectionState) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$RC2x2ijetA_pQrLa4QakzMBjh_k(this, phoneStateListener, param1PreciseDataConnectionState));
    }
    
    public void onDataConnectionRealTimeInfoChanged(DataConnectionRealTimeInfo param1DataConnectionRealTimeInfo) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$OfwFKKtcQHRmtv70FCopw6FDAAU(this, phoneStateListener, param1DataConnectionRealTimeInfo));
    }
    
    public void onSrvccStateChanged(int param1Int) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$nR7W5ox6SCgPxtH9IRcENwKeFI4(this, phoneStateListener, param1Int));
    }
    
    public void onVoiceActivationStateChanged(int param1Int) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$5rF2IFj8mrb7uZc0HMKiuCodUn0(this, phoneStateListener, param1Int));
    }
    
    public void onDataActivationStateChanged(int param1Int) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$t2gWJ_jA36kAdNXSmlzw85aU_tM(this, phoneStateListener, param1Int));
    }
    
    public void onUserMobileDataStateChanged(boolean param1Boolean) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$5uu_05j4ojTh9mEHkN_ynQqQRGM(this, phoneStateListener, param1Boolean));
    }
    
    public void onDisplayInfoChanged(TelephonyDisplayInfo param1TelephonyDisplayInfo) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$Ju_ddK8E8x5tfEZmkvwBIYJDPvE(this, phoneStateListener, param1TelephonyDisplayInfo));
    }
    
    public void onOemHookRawEvent(byte[] param1ArrayOfbyte) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$DwLKzyC4oFq0Am_zrmIKCBlAkSw(this, phoneStateListener, param1ArrayOfbyte));
    }
    
    public void onCarrierNetworkChange(boolean param1Boolean) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$SLDsZb_RTXJpIvKJwCENgXrSXcU(this, phoneStateListener, param1Boolean));
    }
    
    public void onEmergencyNumberListChanged(Map param1Map) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$d9DVwzLraeX80tegF_wEzf_k2FI(this, phoneStateListener, param1Map));
    }
    
    public void onOutgoingEmergencyCall(EmergencyNumber param1EmergencyNumber) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$BsQWpotqnIoEH_U0akIHaOPMJEw(this, phoneStateListener, param1EmergencyNumber));
    }
    
    public void onOutgoingEmergencySms(EmergencyNumber param1EmergencyNumber) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$T_JEoJRE8dPShC5x0Epb3dZudWU(this, phoneStateListener, param1EmergencyNumber));
    }
    
    public void onPhoneCapabilityChanged(PhoneCapability param1PhoneCapability) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$wjRj9OdX5wSkxhrYxPCG_Ew6YXQ(this, phoneStateListener, param1PhoneCapability));
    }
    
    public void onRadioPowerStateChanged(int param1Int) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$VxFLOQdMp2vINeouS7TeF9r_gG0(this, phoneStateListener, param1Int));
    }
    
    public void onCallAttributesChanged(CallAttributes param1CallAttributes) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$XUG0CXbGDJ3aeL69w_T91MxLWmQ(this, phoneStateListener, param1CallAttributes));
    }
    
    public void onActiveDataSubIdChanged(int param1Int) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$GJ2YJ4ARy5_u2bWutnqrYMAsLYA(this, phoneStateListener, param1Int));
    }
    
    public void onImsCallDisconnectCauseChanged(ImsReasonInfo param1ImsReasonInfo) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$kBtYmLansNh43SPn9TbXXwzfjhU(this, phoneStateListener, param1ImsReasonInfo));
    }
    
    public void onRegistrationFailed(CellIdentity param1CellIdentity, String param1String, int param1Int1, int param1Int2, int param1Int3) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$GgIjtfA23XjACIgpJdRWSJCTMJY(this, phoneStateListener, param1CellIdentity, param1String, param1Int1, param1Int2, param1Int3));
    }
    
    public void onBarringInfoChanged(BarringInfo param1BarringInfo) {
      PhoneStateListener phoneStateListener = this.mPhoneStateListenerWeakRef.get();
      if (phoneStateListener == null)
        return; 
      Binder.withCleanCallingIdentity(new _$$Lambda$PhoneStateListener$IPhoneStateListenerStub$jTiEa3GYrDHi81x7zieU4nSnaeQ(this, phoneStateListener, param1BarringInfo));
    }
  }
  
  private void log(String paramString) {
    Rlog.d("PhoneStateListener", paramString);
  }
}
