package android.telephony;

import android.os.PersistableBundle;

public abstract class CellSignalStrength {
  public static final int NUM_SIGNAL_STRENGTH_BINS = 5;
  
  protected static final int NUM_SIGNAL_STRENGTH_THRESHOLDS = 4;
  
  public static final int SIGNAL_STRENGTH_GOOD = 3;
  
  public static final int SIGNAL_STRENGTH_GREAT = 4;
  
  public static final int SIGNAL_STRENGTH_MODERATE = 2;
  
  public static final int SIGNAL_STRENGTH_NONE_OR_UNKNOWN = 0;
  
  public static final int SIGNAL_STRENGTH_POOR = 1;
  
  protected static final int getRssiDbmFromAsu(int paramInt) {
    if (paramInt > 31 || paramInt < 0)
      return Integer.MAX_VALUE; 
    return paramInt * 2 - 113;
  }
  
  protected static final int getAsuFromRssiDbm(int paramInt) {
    if (paramInt == Integer.MAX_VALUE)
      return 99; 
    return (paramInt + 113) / 2;
  }
  
  protected static final int getRscpDbmFromAsu(int paramInt) {
    if (paramInt > 96 || paramInt < 0)
      return Integer.MAX_VALUE; 
    return paramInt - 120;
  }
  
  protected static final int getAsuFromRscpDbm(int paramInt) {
    if (paramInt == Integer.MAX_VALUE)
      return 255; 
    return paramInt + 120;
  }
  
  protected static final int getEcNoDbFromAsu(int paramInt) {
    if (paramInt > 49 || paramInt < 0)
      return Integer.MAX_VALUE; 
    return paramInt / 2 - 24;
  }
  
  protected static final int inRangeOrUnavailable(int paramInt1, int paramInt2, int paramInt3) {
    if (paramInt1 < paramInt2 || paramInt1 > paramInt3)
      return Integer.MAX_VALUE; 
    return paramInt1;
  }
  
  protected static final int inRangeOrUnavailable(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if ((paramInt1 < paramInt2 || paramInt1 > paramInt3) && paramInt1 != paramInt4)
      return Integer.MAX_VALUE; 
    return paramInt1;
  }
  
  public static final int getNumSignalStrengthLevels() {
    return 5;
  }
  
  public abstract CellSignalStrength copy();
  
  public abstract boolean equals(Object paramObject);
  
  public abstract int getAsuLevel();
  
  public abstract int getDbm();
  
  public abstract int getLevel();
  
  public abstract int hashCode();
  
  public abstract boolean isValid();
  
  public abstract void setDefaultValues();
  
  public abstract void updateLevel(PersistableBundle paramPersistableBundle, ServiceState paramServiceState);
}
