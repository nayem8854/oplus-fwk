package android.telephony;

import android.hardware.radio.V1_4.CellInfo;
import android.hardware.radio.V1_5.CellInfo;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public final class CellInfoNr extends CellInfo {
  public CellInfoNr() {
    this.mCellIdentity = new CellIdentityNr();
    this.mCellSignalStrength = new CellSignalStrengthNr();
  }
  
  private CellInfoNr(Parcel paramParcel) {
    super(paramParcel);
    this.mCellIdentity = (CellIdentityNr)CellIdentityNr.CREATOR.createFromParcel(paramParcel);
    this.mCellSignalStrength = (CellSignalStrengthNr)CellSignalStrengthNr.CREATOR.createFromParcel(paramParcel);
  }
  
  private CellInfoNr(CellInfoNr paramCellInfoNr, boolean paramBoolean) {
    super(paramCellInfoNr);
    CellIdentityNr cellIdentityNr;
    if (paramBoolean) {
      cellIdentityNr = paramCellInfoNr.mCellIdentity.sanitizeLocationInfo();
    } else {
      cellIdentityNr = paramCellInfoNr.mCellIdentity;
    } 
    this.mCellIdentity = cellIdentityNr;
    this.mCellSignalStrength = paramCellInfoNr.mCellSignalStrength;
  }
  
  public CellInfoNr(CellInfo paramCellInfo, long paramLong) {
    super(paramCellInfo, paramLong);
    android.hardware.radio.V1_4.CellInfoNr cellInfoNr = paramCellInfo.info.nr();
    this.mCellIdentity = new CellIdentityNr(cellInfoNr.cellidentity);
    this.mCellSignalStrength = new CellSignalStrengthNr(cellInfoNr.signalStrength);
  }
  
  public CellInfoNr(CellInfo paramCellInfo, long paramLong) {
    super(paramCellInfo, paramLong);
    android.hardware.radio.V1_5.CellInfoNr cellInfoNr = paramCellInfo.ratSpecificInfo.nr();
    this.mCellIdentity = new CellIdentityNr(cellInfoNr.cellIdentityNr);
    this.mCellSignalStrength = new CellSignalStrengthNr(cellInfoNr.signalStrengthNr);
  }
  
  public CellIdentity getCellIdentity() {
    return this.mCellIdentity;
  }
  
  public void setCellIdentity(CellIdentityNr paramCellIdentityNr) {
    this.mCellIdentity = paramCellIdentityNr;
  }
  
  public CellSignalStrength getCellSignalStrength() {
    return this.mCellSignalStrength;
  }
  
  public CellInfo sanitizeLocationInfo() {
    return new CellInfoNr(this, true);
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(super.hashCode()), this.mCellIdentity, this.mCellSignalStrength });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof CellInfoNr;
    boolean bool1 = false;
    if (!bool)
      return false; 
    CellInfoNr cellInfoNr = (CellInfoNr)paramObject;
    if (super.equals(cellInfoNr) && this.mCellIdentity.equals(cellInfoNr.mCellIdentity)) {
      paramObject = this.mCellSignalStrength;
      CellSignalStrengthNr cellSignalStrengthNr = cellInfoNr.mCellSignalStrength;
      if (paramObject.equals(cellSignalStrengthNr))
        bool1 = true; 
    } 
    return bool1;
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("CellInfoNr:{");
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append(" ");
    stringBuilder3.append(super.toString());
    stringBuilder1.append(stringBuilder3.toString());
    stringBuilder3 = new StringBuilder();
    stringBuilder3.append(" ");
    stringBuilder3.append(this.mCellIdentity);
    String str2 = stringBuilder3.toString();
    stringBuilder1.append(str2);
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" ");
    stringBuilder2.append(this.mCellSignalStrength);
    String str1 = stringBuilder2.toString();
    stringBuilder1.append(str1);
    stringBuilder1.append(" }");
    return stringBuilder1.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcel(paramParcel, paramInt, 6);
    this.mCellIdentity.writeToParcel(paramParcel, paramInt);
    this.mCellSignalStrength.writeToParcel(paramParcel, paramInt);
  }
  
  public static final Parcelable.Creator<CellInfoNr> CREATOR = (Parcelable.Creator<CellInfoNr>)new Object();
  
  private static final String TAG = "CellInfoNr";
  
  private CellIdentityNr mCellIdentity;
  
  private final CellSignalStrengthNr mCellSignalStrength;
  
  protected static CellInfoNr createFromParcelBody(Parcel paramParcel) {
    return new CellInfoNr(paramParcel);
  }
}
