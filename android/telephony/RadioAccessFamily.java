package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.telephony.RILConstants;

public class RadioAccessFamily implements Parcelable {
  private static final int CDMA = 72;
  
  public RadioAccessFamily(int paramInt1, int paramInt2) {
    this.mPhoneId = paramInt1;
    this.mRadioAccessFamily = paramInt2;
  }
  
  public int getPhoneId() {
    return this.mPhoneId;
  }
  
  public int getRadioAccessFamily() {
    return this.mRadioAccessFamily;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{ mPhoneId = ");
    stringBuilder.append(this.mPhoneId);
    stringBuilder.append(", mRadioAccessFamily = ");
    stringBuilder.append(this.mRadioAccessFamily);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mPhoneId);
    paramParcel.writeInt(this.mRadioAccessFamily);
  }
  
  public static final Parcelable.Creator<RadioAccessFamily> CREATOR = new Parcelable.Creator<RadioAccessFamily>() {
      public RadioAccessFamily createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        return new RadioAccessFamily(i, j);
      }
      
      public RadioAccessFamily[] newArray(int param1Int) {
        return new RadioAccessFamily[param1Int];
      }
    };
  
  private static final int EVDO = 10288;
  
  private static final int GSM = 32771;
  
  private static final int HS = 17280;
  
  private static final int LTE = 266240;
  
  private static final int NR = 524288;
  
  public static final int RAF_1xRTT = 64;
  
  public static final int RAF_EDGE = 2;
  
  public static final int RAF_EHRPD = 8192;
  
  public static final int RAF_EVDO_0 = 16;
  
  public static final int RAF_EVDO_A = 32;
  
  public static final int RAF_EVDO_B = 2048;
  
  public static final int RAF_GPRS = 1;
  
  public static final int RAF_GSM = 32768;
  
  public static final int RAF_HSDPA = 128;
  
  public static final int RAF_HSPA = 512;
  
  public static final int RAF_HSPAP = 16384;
  
  public static final int RAF_HSUPA = 256;
  
  public static final int RAF_IS95A = 8;
  
  public static final int RAF_IS95B = 8;
  
  public static final int RAF_LTE = 4096;
  
  public static final int RAF_LTE_CA = 262144;
  
  public static final int RAF_NR = 524288;
  
  public static final int RAF_TD_SCDMA = 65536;
  
  public static final int RAF_UMTS = 4;
  
  public static final int RAF_UNKNOWN = 0;
  
  private static final int WCDMA = 17284;
  
  private int mPhoneId;
  
  private int mRadioAccessFamily;
  
  public static int getRafFromNetworkType(int paramInt) {
    switch (paramInt) {
      default:
        return 0;
      case 33:
        return 916479;
      case 32:
        return 906119;
      case 31:
        return 873348;
      case 30:
        return 888835;
      case 29:
        return 856064;
      case 28:
        return 807812;
      case 27:
        return 850943;
      case 26:
        return 840583;
      case 25:
        return 800888;
      case 24:
        return 790528;
      case 23:
        return 524288;
      case 22:
        return 392191;
      case 21:
        return 125951;
      case 20:
        return 381831;
      case 19:
        return 349060;
      case 18:
        return 115591;
      case 17:
        return 364547;
      case 16:
        return 98307;
      case 15:
        return 331776;
      case 14:
        return 82820;
      case 13:
        return 65536;
      case 12:
        return 283524;
      case 11:
        return 266240;
      case 10:
        return 326655;
      case 9:
        return 316295;
      case 8:
        return 276600;
      case 7:
        return 60415;
      case 6:
        return 10288;
      case 5:
        return 72;
      case 4:
        return 10360;
      case 3:
        return 50055;
      case 2:
        return 17284;
      case 1:
        return 32771;
      case 0:
        break;
    } 
    return 50055;
  }
  
  private static int getAdjustedRaf(int paramInt) {
    if ((paramInt & 0x8003) > 0)
      paramInt = 0x8003 | paramInt; 
    if ((paramInt & 0x4384) > 0)
      paramInt |= 0x4384; 
    if ((paramInt & 0x48) > 0)
      paramInt |= 0x48; 
    if ((paramInt & 0x2830) > 0)
      paramInt |= 0x2830; 
    if ((paramInt & 0x41000) > 0)
      paramInt = 0x41000 | paramInt; 
    if ((paramInt & 0x80000) > 0)
      paramInt = 0x80000 | paramInt; 
    return paramInt;
  }
  
  public static int getNetworkTypeFromRaf(int paramInt) {
    paramInt = getAdjustedRaf(paramInt);
    switch (paramInt) {
      default:
        return RILConstants.PREFERRED_NETWORK_MODE;
      case 916479:
        return 33;
      case 906119:
        return 32;
      case 888835:
        return 30;
      case 873348:
        return 31;
      case 856064:
        return 29;
      case 850943:
        return 27;
      case 840583:
        return 26;
      case 807812:
        return 28;
      case 800888:
        return 25;
      case 790528:
        return 24;
      case 524288:
        return 23;
      case 392191:
        return 22;
      case 381831:
        return 20;
      case 364547:
        return 17;
      case 349060:
        return 19;
      case 331776:
        return 15;
      case 326655:
        return 10;
      case 316295:
        return 9;
      case 283524:
        return 12;
      case 276600:
        return 8;
      case 266240:
        return 11;
      case 125951:
        return 21;
      case 115591:
        return 18;
      case 98307:
        return 16;
      case 82820:
        return 14;
      case 65536:
        return 13;
      case 60415:
        return 7;
      case 50055:
        return 0;
      case 32771:
        return 1;
      case 17284:
        return 2;
      case 10360:
        return 4;
      case 10288:
        return 6;
      case 72:
        break;
    } 
    return 5;
  }
  
  public static int singleRafTypeFromString(String paramString) {
    byte b;
    switch (paramString.hashCode()) {
      default:
        b = -1;
        break;
      case 2056938943:
        if (paramString.equals("EVDO_B")) {
          b = 11;
          break;
        } 
      case 2056938942:
        if (paramString.equals("EVDO_A")) {
          b = 7;
          break;
        } 
      case 2056938925:
        if (paramString.equals("EVDO_0")) {
          b = 6;
          break;
        } 
      case 82410124:
        if (paramString.equals("WCDMA")) {
          b = 20;
          break;
        } 
      case 69946172:
        if (paramString.equals("IS95B")) {
          b = 4;
          break;
        } 
      case 69946171:
        if (paramString.equals("IS95A")) {
          b = 3;
          break;
        } 
      case 69050395:
        if (paramString.equals("HSUPA")) {
          b = 9;
          break;
        } 
      case 69045140:
        if (paramString.equals("HSPAP")) {
          b = 14;
          break;
        } 
      case 69034058:
        if (paramString.equals("HSDPA")) {
          b = 8;
          break;
        } 
      case 65949251:
        if (paramString.equals("EHRPD")) {
          b = 12;
          break;
        } 
      case 47955627:
        if (paramString.equals("1XRTT")) {
          b = 5;
          break;
        } 
      case 2608919:
        if (paramString.equals("UMTS")) {
          b = 2;
          break;
        } 
      case 2227260:
        if (paramString.equals("HSPA")) {
          b = 10;
          break;
        } 
      case 2194666:
        if (paramString.equals("GPRS")) {
          b = 0;
          break;
        } 
      case 2140412:
        if (paramString.equals("EVDO")) {
          b = 19;
          break;
        } 
      case 2123197:
        if (paramString.equals("EDGE")) {
          b = 1;
          break;
        } 
      case 2063797:
        if (paramString.equals("CDMA")) {
          b = 18;
          break;
        } 
      case 75709:
        if (paramString.equals("LTE")) {
          b = 13;
          break;
        } 
      case 70881:
        if (paramString.equals("GSM")) {
          b = 15;
          break;
        } 
      case 2500:
        if (paramString.equals("NR")) {
          b = 22;
          break;
        } 
      case 2315:
        if (paramString.equals("HS")) {
          b = 17;
          break;
        } 
      case -908593671:
        if (paramString.equals("TD_SCDMA")) {
          b = 16;
          break;
        } 
      case -2039427040:
        if (paramString.equals("LTE_CA")) {
          b = 21;
          break;
        } 
    } 
    switch (b) {
      default:
        return 0;
      case 22:
        return 524288;
      case 21:
        return 262144;
      case 20:
        return 17284;
      case 19:
        return 10288;
      case 18:
        return 72;
      case 17:
        return 17280;
      case 16:
        return 65536;
      case 15:
        return 32768;
      case 14:
        return 16384;
      case 13:
        return 4096;
      case 12:
        return 8192;
      case 11:
        return 2048;
      case 10:
        return 512;
      case 9:
        return 256;
      case 8:
        return 128;
      case 7:
        return 32;
      case 6:
        return 16;
      case 5:
        return 64;
      case 4:
        return 8;
      case 3:
        return 8;
      case 2:
        return 4;
      case 1:
        return 2;
      case 0:
        break;
    } 
    return 1;
  }
  
  public static int rafTypeFromString(String paramString) {
    paramString = paramString.toUpperCase();
    String[] arrayOfString = paramString.split("\\|");
    int i = 0;
    int j;
    byte b;
    for (j = arrayOfString.length, b = 0; b < j; ) {
      paramString = arrayOfString[b];
      int k = singleRafTypeFromString(paramString.trim());
      if (k == 0)
        return k; 
      i |= k;
      b++;
    } 
    return i;
  }
  
  public static int compare(long paramLong1, long paramLong2) {
    long[] arrayOfLong = new long[4];
    arrayOfLong[0] = 524288L;
    arrayOfLong[1] = 397312L;
    arrayOfLong[2] = 93108L;
    arrayOfLong[3] = 32843L;
    int i;
    byte b;
    for (i = arrayOfLong.length, b = 0; b < i; ) {
      long l = arrayOfLong[b];
      int j = 0;
      if (((paramLong2 ^ 0xFFFFFFFFFFFFFFFFL) & paramLong1 & l) != 0L)
        j = 0 + 1; 
      int k = j;
      if (((paramLong1 ^ 0xFFFFFFFFFFFFFFFFL) & paramLong2 & l) != 0L)
        k = j - 1; 
      if (k != 0)
        return k; 
      b++;
    } 
    return Long.bitCount(paramLong1) - Long.bitCount(paramLong2);
  }
}
