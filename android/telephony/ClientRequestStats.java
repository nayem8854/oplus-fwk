package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import java.util.ArrayList;
import java.util.List;

public final class ClientRequestStats implements Parcelable {
  public static final Parcelable.Creator<ClientRequestStats> CREATOR = new Parcelable.Creator<ClientRequestStats>() {
      public ClientRequestStats createFromParcel(Parcel param1Parcel) {
        return new ClientRequestStats(param1Parcel);
      }
      
      public ClientRequestStats[] newArray(int param1Int) {
        return new ClientRequestStats[param1Int];
      }
    };
  
  private long mCompletedRequestsWakelockTime = 0L;
  
  private long mCompletedRequestsCount = 0L;
  
  private long mPendingRequestsWakelockTime = 0L;
  
  private long mPendingRequestsCount = 0L;
  
  private SparseArray<TelephonyHistogram> mRequestHistograms = new SparseArray<>();
  
  private static final int REQUEST_HISTOGRAM_BUCKET_COUNT = 5;
  
  private String mCallingPackage;
  
  public ClientRequestStats(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public ClientRequestStats(ClientRequestStats paramClientRequestStats) {
    this.mCallingPackage = paramClientRequestStats.getCallingPackage();
    this.mCompletedRequestsCount = paramClientRequestStats.getCompletedRequestsCount();
    this.mCompletedRequestsWakelockTime = paramClientRequestStats.getCompletedRequestsWakelockTime();
    this.mPendingRequestsCount = paramClientRequestStats.getPendingRequestsCount();
    this.mPendingRequestsWakelockTime = paramClientRequestStats.getPendingRequestsWakelockTime();
    List<TelephonyHistogram> list = paramClientRequestStats.getRequestHistograms();
    for (TelephonyHistogram telephonyHistogram : list)
      this.mRequestHistograms.put(telephonyHistogram.getId(), telephonyHistogram); 
  }
  
  public String getCallingPackage() {
    return this.mCallingPackage;
  }
  
  public void setCallingPackage(String paramString) {
    this.mCallingPackage = paramString;
  }
  
  public long getCompletedRequestsWakelockTime() {
    return this.mCompletedRequestsWakelockTime;
  }
  
  public void addCompletedWakelockTime(long paramLong) {
    this.mCompletedRequestsWakelockTime += paramLong;
  }
  
  public long getPendingRequestsWakelockTime() {
    return this.mPendingRequestsWakelockTime;
  }
  
  public void setPendingRequestsWakelockTime(long paramLong) {
    this.mPendingRequestsWakelockTime = paramLong;
  }
  
  public long getCompletedRequestsCount() {
    return this.mCompletedRequestsCount;
  }
  
  public void incrementCompletedRequestsCount() {
    this.mCompletedRequestsCount++;
  }
  
  public long getPendingRequestsCount() {
    return this.mPendingRequestsCount;
  }
  
  public void setPendingRequestsCount(long paramLong) {
    this.mPendingRequestsCount = paramLong;
  }
  
  public List<TelephonyHistogram> getRequestHistograms() {
    synchronized (this.mRequestHistograms) {
      ArrayList<TelephonyHistogram> arrayList = new ArrayList();
      this(this.mRequestHistograms.size());
      for (byte b = 0; b < this.mRequestHistograms.size(); b++) {
        TelephonyHistogram telephonyHistogram = new TelephonyHistogram();
        this(this.mRequestHistograms.valueAt(b));
        arrayList.add(telephonyHistogram);
      } 
      return arrayList;
    } 
  }
  
  public void updateRequestHistograms(int paramInt1, int paramInt2) {
    synchronized (this.mRequestHistograms) {
      TelephonyHistogram telephonyHistogram1 = this.mRequestHistograms.get(paramInt1);
      TelephonyHistogram telephonyHistogram2 = telephonyHistogram1;
      if (telephonyHistogram1 == null) {
        telephonyHistogram2 = new TelephonyHistogram();
        this(1, paramInt1, 5);
        this.mRequestHistograms.put(paramInt1, telephonyHistogram2);
      } 
      telephonyHistogram2.addTimeTaken(paramInt2);
      return;
    } 
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ClientRequestStats{mCallingPackage='");
    stringBuilder.append(this.mCallingPackage);
    stringBuilder.append('\'');
    stringBuilder.append(", mCompletedRequestsWakelockTime=");
    stringBuilder.append(this.mCompletedRequestsWakelockTime);
    stringBuilder.append(", mCompletedRequestsCount=");
    stringBuilder.append(this.mCompletedRequestsCount);
    stringBuilder.append(", mPendingRequestsWakelockTime=");
    stringBuilder.append(this.mPendingRequestsWakelockTime);
    stringBuilder.append(", mPendingRequestsCount=");
    stringBuilder.append(this.mPendingRequestsCount);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mCallingPackage = paramParcel.readString();
    this.mCompletedRequestsWakelockTime = paramParcel.readLong();
    this.mCompletedRequestsCount = paramParcel.readLong();
    this.mPendingRequestsWakelockTime = paramParcel.readLong();
    this.mPendingRequestsCount = paramParcel.readLong();
    ArrayList arrayList = new ArrayList();
    paramParcel.readTypedList(arrayList, TelephonyHistogram.CREATOR);
    for (TelephonyHistogram telephonyHistogram : arrayList)
      this.mRequestHistograms.put(telephonyHistogram.getId(), telephonyHistogram); 
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mCallingPackage);
    paramParcel.writeLong(this.mCompletedRequestsWakelockTime);
    paramParcel.writeLong(this.mCompletedRequestsCount);
    paramParcel.writeLong(this.mPendingRequestsWakelockTime);
    paramParcel.writeLong(this.mPendingRequestsCount);
    paramParcel.writeTypedList(getRequestHistograms());
  }
  
  public ClientRequestStats() {}
}
