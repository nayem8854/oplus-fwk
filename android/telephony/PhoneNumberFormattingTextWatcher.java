package android.telephony;

import android.text.Editable;
import android.text.TextWatcher;
import com.android.i18n.phonenumbers.AsYouTypeFormatter;
import com.android.i18n.phonenumbers.PhoneNumberUtil;
import java.util.Locale;

public class PhoneNumberFormattingTextWatcher implements TextWatcher {
  private AsYouTypeFormatter mFormatter;
  
  private boolean mSelfChange = false;
  
  private boolean mStopFormatting;
  
  public PhoneNumberFormattingTextWatcher() {
    this(Locale.getDefault().getCountry());
  }
  
  public PhoneNumberFormattingTextWatcher(String paramString) {
    if (paramString != null) {
      this.mFormatter = PhoneNumberUtil.getInstance().getAsYouTypeFormatter(paramString);
      return;
    } 
    throw new IllegalArgumentException();
  }
  
  public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
    if (this.mSelfChange || this.mStopFormatting)
      return; 
    if (paramInt2 > 0 && hasSeparator(paramCharSequence, paramInt1, paramInt2))
      stopFormatting(); 
  }
  
  public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
    if (this.mSelfChange || this.mStopFormatting)
      return; 
    if (paramInt3 > 0 && hasSeparator(paramCharSequence, paramInt1, paramInt3))
      stopFormatting(); 
  }
  
  public void afterTextChanged(Editable paramEditable) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mStopFormatting : Z
    //   6: istore_2
    //   7: iconst_1
    //   8: istore_3
    //   9: iload_2
    //   10: ifeq -> 35
    //   13: aload_1
    //   14: invokeinterface length : ()I
    //   19: ifeq -> 25
    //   22: goto -> 27
    //   25: iconst_0
    //   26: istore_3
    //   27: aload_0
    //   28: iload_3
    //   29: putfield mStopFormatting : Z
    //   32: aload_0
    //   33: monitorexit
    //   34: return
    //   35: aload_0
    //   36: getfield mSelfChange : Z
    //   39: istore_3
    //   40: iload_3
    //   41: ifeq -> 47
    //   44: aload_0
    //   45: monitorexit
    //   46: return
    //   47: aload_0
    //   48: aload_1
    //   49: aload_1
    //   50: invokestatic getSelectionEnd : (Ljava/lang/CharSequence;)I
    //   53: invokespecial reformat : (Ljava/lang/CharSequence;I)Ljava/lang/String;
    //   56: astore #4
    //   58: aload #4
    //   60: ifnull -> 122
    //   63: aload_0
    //   64: getfield mFormatter : Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;
    //   67: invokevirtual getRememberedPosition : ()I
    //   70: istore #5
    //   72: aload_0
    //   73: iconst_1
    //   74: putfield mSelfChange : Z
    //   77: aload_1
    //   78: iconst_0
    //   79: aload_1
    //   80: invokeinterface length : ()I
    //   85: aload #4
    //   87: iconst_0
    //   88: aload #4
    //   90: invokevirtual length : ()I
    //   93: invokeinterface replace : (IILjava/lang/CharSequence;II)Landroid/text/Editable;
    //   98: pop
    //   99: aload #4
    //   101: aload_1
    //   102: invokevirtual toString : ()Ljava/lang/String;
    //   105: invokevirtual equals : (Ljava/lang/Object;)Z
    //   108: ifeq -> 117
    //   111: aload_1
    //   112: iload #5
    //   114: invokestatic setSelection : (Landroid/text/Spannable;I)V
    //   117: aload_0
    //   118: iconst_0
    //   119: putfield mSelfChange : Z
    //   122: aload_1
    //   123: iconst_0
    //   124: aload_1
    //   125: invokeinterface length : ()I
    //   130: invokestatic ttsSpanAsPhoneNumber : (Landroid/text/Spannable;II)V
    //   133: aload_0
    //   134: monitorexit
    //   135: return
    //   136: astore_1
    //   137: aload_0
    //   138: monitorexit
    //   139: aload_1
    //   140: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #100	-> 2
    //   #102	-> 13
    //   #103	-> 32
    //   #105	-> 35
    //   #107	-> 44
    //   #109	-> 47
    //   #110	-> 58
    //   #111	-> 63
    //   #112	-> 72
    //   #113	-> 77
    //   #116	-> 99
    //   #117	-> 111
    //   #119	-> 117
    //   #121	-> 122
    //   #122	-> 133
    //   #99	-> 136
    // Exception table:
    //   from	to	target	type
    //   2	7	136	finally
    //   13	22	136	finally
    //   27	32	136	finally
    //   35	40	136	finally
    //   47	58	136	finally
    //   63	72	136	finally
    //   72	77	136	finally
    //   77	99	136	finally
    //   99	111	136	finally
    //   111	117	136	finally
    //   117	122	136	finally
    //   122	133	136	finally
  }
  
  private String reformat(CharSequence paramCharSequence, int paramInt) {
    String str = null;
    this.mFormatter.clear();
    char c = Character.MIN_VALUE;
    boolean bool = false;
    int i = paramCharSequence.length();
    for (byte b = 0; b < i; b++, str = str1, c = c2) {
      char c1 = paramCharSequence.charAt(b);
      String str1 = str;
      char c2 = c;
      boolean bool1 = bool;
      if (PhoneNumberUtils.isNonSeparator(c1)) {
        bool1 = bool;
        if (c) {
          str = getFormattedNumber(c, bool);
          bool1 = false;
        } 
        c2 = c1;
        str1 = str;
      } 
      bool = bool1;
      if (b == paramInt - 1)
        bool = true; 
    } 
    if (c != '\000')
      str = getFormattedNumber(c, bool); 
    return str;
  }
  
  private String getFormattedNumber(char paramChar, boolean paramBoolean) {
    String str;
    if (paramBoolean) {
      str = this.mFormatter.inputDigitAndRememberPosition(paramChar);
    } else {
      str = this.mFormatter.inputDigit(paramChar);
    } 
    return str;
  }
  
  private void stopFormatting() {
    this.mStopFormatting = true;
    this.mFormatter.clear();
  }
  
  private boolean hasSeparator(CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    for (int i = paramInt1; i < paramInt1 + paramInt2; i++) {
      char c = paramCharSequence.charAt(i);
      if (!PhoneNumberUtils.isNonSeparator(c))
        return true; 
    } 
    return false;
  }
}
