package android.telephony;

import android.app.PendingIntent;
import android.compat.Compatibility;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.os.TelephonyServiceManager;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.telephony.IIntegerConsumer;
import com.android.internal.telephony.IOplusTelephonyExt;
import com.android.internal.telephony.IOplusTelephonyExtCallback;
import com.android.internal.telephony.ISms;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.OplusFeature;
import com.android.internal.telephony.util.ReflectionHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class OplusTelephonyManager {
  public static final String AUTO_NR_MODE_KEY = "autoNrMode";
  
  public static final String BUNDLE_CONTENT = "content";
  
  public static final String BUNDLE_CONTENT2 = "content2";
  
  public static final String BUNDLE_DATA_CHECK_RESULT = "result";
  
  public static final int CARD_TYPE_CM = 2;
  
  public static final int CARD_TYPE_CT = 1;
  
  public static final int CARD_TYPE_CU = 3;
  
  public static final int CARD_TYPE_OTHER = 4;
  
  public static final int CARD_TYPE_TEST = 9;
  
  public static final int CARD_TYPE_UNKNOWN = -1;
  
  public static final int DS_CK_KEY_INDEX = 0;
  
  public static final int DS_CK_KEY_VAL = 1;
  
  public static final int DS_CK_KEY_VALUE_COUNT = 2;
  
  public static final String DS_CK_RUS_FEATURE_NAME = "DS_CK_FEATURE";
  
  public static final String DS_CK_RUS_SETTING_KEY = "RUS_DS_CK_FEATURE";
  
  public static final String DS_CK_SPERATOR = "##";
  
  public static final String DS_CK_START_AFTER_KEY = "=";
  
  public static final String DS_CK__APN_NETS = "DS_CK__APN_NETS";
  
  public static final String DS_CK__APN_WAPS = "DS_CK__APN_WAPS";
  
  public static final String DS_CK__DNS_CN_0 = "DS_CK__DNS_CN_0";
  
  public static final String DS_CK__DNS_CN_1 = "DS_CK__DNS_CN_1";
  
  public static final String DS_CK__DNS_MUL_DURATION = "DS_CK__DNS_MUL_DURATION";
  
  public static final String DS_CK__DNS_MUL_UID_COUNT = "DS_CK__DNS_MUL_UID_COUNT";
  
  public static final String DS_CK__DNS_SIG_DURATION = "DS_CK__DNS_SIG_DURATION";
  
  public static final String DS_CK__DNS_SIG_UID_COUNT = "DS_CK__DNS_SIG_UID_COUNT";
  
  public static final String DS_CK__DNS_US_0 = "DS_CK__DNS_US_0";
  
  public static final String DS_CK__DNS_US_1 = "DS_CK__DNS_US_1";
  
  public static final String DS_CK__DNS_WHOLE_STAGE_TIMEOUT = "DS_CK__DNS_WHOLE_STAGE_TIMEOUT";
  
  public static final String DS_CK__GENERATE_204_CN_0 = "DS_CK__GENERATE_204_CN_0";
  
  public static final String DS_CK__GENERATE_204_CN_1 = "DS_CK__GENERATE_204_CN_1";
  
  public static final String DS_CK__GENERATE_204_US_0 = "DS_CK__GENERATE_204_US_0";
  
  public static final String DS_CK__GENERATE_204_US_1 = "DS_CK__GENERATE_204_US_1";
  
  public static final String DS_CK__HTTP_WHOLE_STAGE_TIMEOUT = "DS_CK__HTTP_WHOLE_STAGE_TIMEOUT";
  
  public static final String DS_CK__LOGS = "DS_CK__LOGS";
  
  public static final String DS_CK__MT_MMS_DURATION = "DS_CK__MT_MMS_DURATION";
  
  public static final String DS_CK__MT_SMS_DURATION = "DS_CK__MT_SMS_DURATION";
  
  public static final String DS_CK__NHS_ENABLE = "DS_CK__NHS_ENABLE";
  
  public static final String DS_CK__NOTIF_ACTION = "DS_CK__NOTIF_ACTION";
  
  public static final String DS_CK__NOTIF_DURATION = "DS_CK__NOTIF_DURATION";
  
  public static final String DS_CK__NOTIF_ENABLE = "DS_CK__NOTIF_ENABLE";
  
  public static final String DS_CK__NOTIF_HTTP_TIMEOUT_CONN = "DS_CK__NOTIF_HTTP_TIMEOUT_CONN";
  
  public static final String DS_CK__NOTIF_HTTP_TIMEOUT_READ = "DS_CK__NOTIF_HTTP_TIMEOUT_CONN";
  
  public static final String DS_CK__NOTIF_INTERVAL = "DS_CK__NOTIF_INTERVAL";
  
  public static final String DS_CK__NOTIF_MAX_COUNT = "DS_CK__NOTIF_MAX_COUNT";
  
  public static final String DS_CK__SET_CONNECTION_TIMEOUT = "DS_CK__SET_CONNECTION_TIMEOUT";
  
  public static final String DS_CK__SET_READ_TIMEOUT = "DS_CK__SET_READ_TIMEOUT";
  
  public static final int DS_RUS_KEY_VAL = 1;
  
  public static final int DS_RUS_KEY_VALUE_COUNT = 2;
  
  public static final String DS_RUS_SPERATOR = "##";
  
  public static final String DS_RUS_START_AFTER_KEY = "=";
  
  public static final int EVENT_CALL_BASE = 4000;
  
  public static final int EVENT_CALL_GET_AUTO_ANSWER = 4004;
  
  public static final int EVENT_CALL_GET_ECC_LIST = 4002;
  
  public static final int EVENT_CALL_SET_AUTO_ANSWER = 4005;
  
  public static final int EVENT_CALL_SET_ECC_LIST = 4003;
  
  public static final int EVENT_CALL_UPDATE_VOLTE_FR2 = 4001;
  
  public static final int EVENT_COMMON_BASE = 1000;
  
  public static final int EVENT_COMMON_MATCH_UNLOCK = 1002;
  
  public static final int EVENT_COMMON_OEM_RIL_REQUEST = 1001;
  
  public static final int EVENT_DATA_ACTION_APN_RESTORE = 3010;
  
  public static final int EVENT_DATA_ACTION_APN_TO_NET = 3012;
  
  public static final int EVENT_DATA_ACTION_AUTO_PLMN = 3007;
  
  public static final int EVENT_DATA_ACTION_RETRY_PDN = 3008;
  
  public static final int EVENT_DATA_BASE = 3000;
  
  public static final int EVENT_DATA_CHECK_APN_MANUAL_ADD = 3009;
  
  public static final int EVENT_DATA_CHECK_APN_WAP = 3011;
  
  public static final int EVENT_DATA_CHECK_DNS = 3001;
  
  public static final int EVENT_DATA_CHECK_HTTP = 3002;
  
  public static final int EVENT_DATA_CHECK_PDN = 3005;
  
  public static final int EVENT_DATA_CHECK_SIGNAL = 3004;
  
  public static final int EVENT_DATA_CHECK_SIM = 3003;
  
  public static final int EVENT_DATA_CHECK_VPN = 3006;
  
  public static final int EVENT_DATA_REPORT_GAME_LATENCY = 3013;
  
  public static final int EVENT_DATA_REPORT_GAME_LEVEL = 3014;
  
  public static final String EVENT_KEY = "event";
  
  public static final int EVENT_MAX = 7000;
  
  public static final int EVENT_PIN_PUK_RETRY_UPDATE = 6030;
  
  public static final int EVENT_REG_BASE = 2000;
  
  public static final int EVENT_REG_GET_5G_SIGNAL = 2003;
  
  public static final int EVENT_REG_GET_LTE_CA_STATE = 2010;
  
  public static final int EVENT_REG_GET_NR_MODE = 2009;
  
  public static final int EVENT_REG_GET_RADIO_INFO = 2002;
  
  public static final int EVENT_REG_GET_RADIO_ON = 2007;
  
  public static final int EVENT_REG_GET_REGION_NETLOCK_STATE_INFO = 2011;
  
  public static final int EVENT_REG_GET_REGION_NETLOCK_TEST_INFO = 2013;
  
  public static final int EVENT_REG_IS_CAPABILITY_SWITCH = 2015;
  
  public static final int EVENT_REG_LTE_CA_STATE = 2006;
  
  public static final int EVENT_REG_OEM_COMMON_REQ = 2004;
  
  public static final int EVENT_REG_SET_CELL_INFO_LIST_RATE = 2001;
  
  public static final int EVENT_REG_SET_NR_MODE = 2008;
  
  public static final int EVENT_REG_SET_REGION_NETLOCK_STATE_INFO = 2012;
  
  public static final int EVENT_REG_SET_REGION_NETLOCK_TEST_INFO = 2014;
  
  public static final int EVENT_REG_UPDATE_PPLMN_LIST = 2005;
  
  public static final int EVENT_RF_BASE = 5000;
  
  public static final int EVENT_RF_GET_ASDIV_STATE = 5015;
  
  public static final int EVENT_RF_GET_BAND_MODE = 5002;
  
  public static final int EVENT_RF_GET_LAB_ANTPOS = 5017;
  
  public static final int EVENT_RF_GET_RFFE_DEV_INFO = 5011;
  
  public static final int EVENT_RF_GET_TX_POWER = 5018;
  
  public static final int EVENT_RF_GET_TX_RX_INFO = 5006;
  
  public static final int EVENT_RF_LOCK_GSM_ARFCN = 5008;
  
  public static final int EVENT_RF_LOCK_LTE_CELL = 5009;
  
  public static final int EVENT_RF_NOTICE_UPDATE_VOLTE_FR = 5010;
  
  public static final int EVENT_RF_QUERY_BAND_MODE = 5003;
  
  public static final int EVENT_RF_RFFE_CMD = 5014;
  
  public static final int EVENT_RF_SET_BAND_MODE = 5001;
  
  public static final int EVENT_RF_SET_FILTER_ARFCN = 5007;
  
  public static final int EVENT_RF_SET_LAB_ANTPOS = 5016;
  
  public static final int EVENT_RF_SET_MODEM_GPIO = 5005;
  
  public static final int EVENT_RF_SET_NV_PROCESS_CMD = 5004;
  
  public static final int EVENT_RF_SET_SAR_RF_STATE_V2 = 5012;
  
  public static final int EVENT_RF_SET_SAR_RF_STATE_V3 = 5013;
  
  public static final int EVENT_SIM_BASE = 6000;
  
  public static final int EVENT_SIM_GET_HOTSWAP_STATE = 6005;
  
  public static final int EVENT_SIM_GET_IF_TEST_SIM = 6001;
  
  public static final int EVENT_SIM_GET_IMPI = 6003;
  
  public static final int EVENT_SIM_GET_IMS_TYPE = 6002;
  
  public static final int EVENT_SIM_GET_OPERATOR_SWITCH_ENABLE = 6006;
  
  public static final int EVENT_SIM_GET_OP_ID = 6004;
  
  public static final int EVENT_SIM_GET_SIMLOCK_ACTIVATE_TIME = 6026;
  
  public static final int EVENT_SIM_GET_SIMLOCK_COMBO_TYPE = 6011;
  
  public static final int EVENT_SIM_GET_SIMLOCK_CURRENTRETRY = 6013;
  
  public static final int EVENT_SIM_GET_SIMLOCK_FACTORY_RESET_TIME = 6020;
  
  public static final int EVENT_SIM_GET_SIMLOCK_FEATURE = 6022;
  
  public static final int EVENT_SIM_GET_SIMLOCK_FEESTATE = 6015;
  
  public static final int EVENT_SIM_GET_SIMLOCK_IS_REGION_VIETNAM = 6018;
  
  public static final int EVENT_SIM_GET_SIMLOCK_LOCKDEVICE = 6028;
  
  public static final int EVENT_SIM_GET_SIMLOCK_LOCKMARK = 6021;
  
  public static final int EVENT_SIM_GET_SIMLOCK_LOCKTYPE = 6009;
  
  public static final int EVENT_SIM_GET_SIMLOCK_MAXRETRY = 6012;
  
  public static final int EVENT_SIM_GET_SIMLOCK_OPERATOR = 6008;
  
  public static final int EVENT_SIM_GET_SIMLOCK_POPUP_TYPE = 6010;
  
  public static final int EVENT_SIM_GET_SIMLOCK_SIM1_STATE = 6016;
  
  public static final int EVENT_SIM_GET_SIMLOCK_SIM2_STATE = 6017;
  
  public static final int EVENT_SIM_GET_VSIM_ID = 6007;
  
  public static final int EVENT_SIM_SET_SIMLOCK_ACCUMULATED_TIME = 6024;
  
  public static final int EVENT_SIM_SET_SIMLOCK_ACTIVATE_TIME = 6025;
  
  public static final int EVENT_SIM_SET_SIMLOCK_FACTORY_RESET_TIME = 6019;
  
  public static final int EVENT_SIM_SET_SIMLOCK_FEESTATE = 6014;
  
  public static final int EVENT_SIM_SET_SIMLOCK_KDDI_SIMLOCK_FILE = 6031;
  
  public static final int EVENT_SIM_SET_SIMLOCK_LOCK = 6029;
  
  public static final int EVENT_SIM_SET_SIMLOCK_UNLOCK = 6023;
  
  public static final int EVENT_SIM_SET_VSIM_ID = 6027;
  
  private static final long GET_TARGET_SDK_VERSION_CODE_CHANGE_EXT = 145147529L;
  
  public static final String PACKAGE_KEY = "package";
  
  public static final int PLATFORM_TYPE_MTK = 2;
  
  public static final int PLATFORM_TYPE_NONE = 0;
  
  public static final int PLATFORM_TYPE_QCOM = 1;
  
  public static final String RESULT_KEY = "result";
  
  public static final String SA_PRE_KEY = "saPreEnabled";
  
  public static final String SERVICE_NAME = "oplus_telephony_ext";
  
  private static final String TAG = "OplusTelephonyManager";
  
  private static final boolean isMTKPlatform;
  
  private static final boolean isQcomPlatform;
  
  public static final String[] sDsCkDataKeys = new String[] { 
      "DS_CK__GENERATE_204_CN_0", "DS_CK__GENERATE_204_CN_1", "DS_CK__GENERATE_204_US_0", "DS_CK__GENERATE_204_US_1", "DS_CK__DNS_CN_0", "DS_CK__DNS_CN_1", "DS_CK__DNS_US_0", "DS_CK__DNS_US_1", "DS_CK__DNS_WHOLE_STAGE_TIMEOUT", "DS_CK__HTTP_WHOLE_STAGE_TIMEOUT", 
      "DS_CK__SET_CONNECTION_TIMEOUT", "DS_CK__SET_READ_TIMEOUT", "DS_CK__APN_WAPS", "DS_CK__APN_NETS", "DS_CK__NOTIF_MAX_COUNT", "DS_CK__NOTIF_DURATION", "DS_CK__NOTIF_INTERVAL", "DS_CK__MT_SMS_DURATION", "DS_CK__MT_MMS_DURATION", "DS_CK__DNS_MUL_UID_COUNT", 
      "DS_CK__DNS_SIG_UID_COUNT", "DS_CK__DNS_MUL_DURATION", "DS_CK__DNS_SIG_DURATION", "DS_CK__NOTIF_HTTP_TIMEOUT_CONN", "DS_CK__NOTIF_HTTP_TIMEOUT_CONN", "DS_CK__NHS_ENABLE", "DS_CK__NOTIF_ENABLE", "DS_CK__LOGS", "DS_CK__NOTIF_ACTION" };
  
  private static OplusTelephonyManager sInstance;
  
  private static final int sPriorityHigh = 3;
  
  private static final int sPriorityLow = 0;
  
  private static final int sValiddityPeriodlow = 5;
  
  private static final int sValidityPeriodHight = 635040;
  
  private final Context mContext;
  
  private IOplusTelephonyExt mOplusTelephonyService;
  
  static {
    isQcomPlatform = SystemProperties.get("ro.boot.hardware", "unknow").toLowerCase().startsWith("qcom");
    isMTKPlatform = SystemProperties.get("ro.boot.hardware", "unknow").toLowerCase().startsWith("mt");
    sInstance = null;
  }
  
  @Deprecated
  public static OplusTelephonyManager getInstance(Context paramContext) {
    // Byte code:
    //   0: ldc android/telephony/OplusTelephonyManager
    //   2: monitorenter
    //   3: getstatic android/telephony/OplusTelephonyManager.sInstance : Landroid/telephony/OplusTelephonyManager;
    //   6: ifnonnull -> 22
    //   9: new android/telephony/OplusTelephonyManager
    //   12: astore_1
    //   13: aload_1
    //   14: aload_0
    //   15: invokespecial <init> : (Landroid/content/Context;)V
    //   18: aload_1
    //   19: putstatic android/telephony/OplusTelephonyManager.sInstance : Landroid/telephony/OplusTelephonyManager;
    //   22: getstatic android/telephony/OplusTelephonyManager.sInstance : Landroid/telephony/OplusTelephonyManager;
    //   25: astore_0
    //   26: ldc android/telephony/OplusTelephonyManager
    //   28: monitorexit
    //   29: aload_0
    //   30: areturn
    //   31: astore_0
    //   32: ldc android/telephony/OplusTelephonyManager
    //   34: monitorexit
    //   35: aload_0
    //   36: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #320	-> 0
    //   #321	-> 3
    //   #322	-> 9
    //   #324	-> 22
    //   #325	-> 31
    // Exception table:
    //   from	to	target	type
    //   3	9	31	finally
    //   9	22	31	finally
    //   22	29	31	finally
    //   32	35	31	finally
  }
  
  public OplusTelephonyManager(Context paramContext) {
    Context context = paramContext.getApplicationContext();
    if (context != null) {
      if (Objects.equals(paramContext.getFeatureId(), context.getFeatureId())) {
        this.mContext = context;
      } else {
        this.mContext = context.createFeatureContext(paramContext.getFeatureId());
      } 
    } else {
      this.mContext = paramContext;
    } 
    this.mOplusTelephonyService = IOplusTelephonyExt.Stub.asInterface(ServiceManager.getService("oplus_telephony_ext"));
  }
  
  public static boolean isValidEvent(int paramInt) {
    boolean bool;
    if (paramInt > 1000 && paramInt < 7000) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public Bundle requestForTelephonyEvent(int paramInt1, int paramInt2, Bundle paramBundle) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("requestForTelephonyEvent.slotId:");
    stringBuilder.append(paramInt1);
    stringBuilder.append(", eventId:");
    stringBuilder.append(paramInt2);
    log(stringBuilder.toString());
    if (!SubscriptionManager.isValidSlotIndex(paramInt1) || !isValidEvent(paramInt2))
      return null; 
    try {
      if (this.mOplusTelephonyService != null) {
        Bundle bundle = paramBundle;
        if (paramBundle == null) {
          bundle = new Bundle();
          this();
        } 
        bundle.putString("package", getOpPackageName());
        return this.mOplusTelephonyService.requestForTelephonyEvent(paramInt1, paramInt2, bundle);
      } 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    return null;
  }
  
  public void registerCallback(String paramString, IOplusTelephonyExtCallback paramIOplusTelephonyExtCallback) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("registerCallback:");
    stringBuilder.append(paramIOplusTelephonyExtCallback);
    stringBuilder.append(", package:");
    stringBuilder.append(paramString);
    log(stringBuilder.toString());
    try {
      if (!getOpPackageName().equals(paramString)) {
        log("registerCallback : packageName was not matched");
        return;
      } 
      if (this.mOplusTelephonyService != null)
        this.mOplusTelephonyService.registerCallback(paramString, paramIOplusTelephonyExtCallback); 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public void unRegisterCallback(IOplusTelephonyExtCallback paramIOplusTelephonyExtCallback) {
    try {
      if (this.mOplusTelephonyService != null)
        this.mOplusTelephonyService.unRegisterCallback(paramIOplusTelephonyExtCallback); 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  private String getOpPackageName() {
    Context context = this.mContext;
    if (context != null)
      return context.getOpPackageName(); 
    return "";
  }
  
  public static int getProductPlatform() {
    if (isQcomPlatform)
      return 1; 
    if (isMTKPlatform)
      return 2; 
    return 0;
  }
  
  public static boolean isMTKPlatform() {
    return isMTKPlatform;
  }
  
  public static boolean isQcomPlatform() {
    return isQcomPlatform;
  }
  
  public static void log(String paramString) {
    Rlog.d("OplusTelephonyManager", paramString);
  }
  
  public int getCardType(int paramInt) {
    byte b = -1;
    if (!SubscriptionManager.isValidPhoneId(paramInt))
      return -1; 
    int i = b;
    try {
      if (this.mOplusTelephonyService != null)
        i = this.mOplusTelephonyService.getCardType(paramInt); 
    } catch (Exception exception) {
      exception.printStackTrace();
      i = b;
    } 
    return i;
  }
  
  public int getSoftSimCardSlotId() {
    try {
      if (this.mOplusTelephonyService != null)
        return this.mOplusTelephonyService.getSoftSimCardSlotId(); 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    return -1;
  }
  
  public boolean getIfTestSim(int paramInt) {
    Bundle bundle = requestForTelephonyEvent(paramInt, 6001, null);
    boolean bool1 = false;
    boolean bool2 = bool1;
    if (bundle != null)
      try {
        bool2 = bundle.getBoolean("result");
      } catch (Exception exception) {
        exception.printStackTrace();
        bool2 = bool1;
      }  
    return bool2;
  }
  
  public String getImsType(int paramInt) {
    Bundle bundle = requestForTelephonyEvent(paramInt, 6002, null);
    String str1 = null;
    String str2 = str1;
    if (bundle != null)
      try {
        str2 = bundle.getString("result");
      } catch (Exception exception) {
        exception.printStackTrace();
        str2 = str1;
      }  
    return str2;
  }
  
  public String getImpi(int paramInt) {
    Bundle bundle = requestForTelephonyEvent(paramInt, 6003, null);
    String str1 = null;
    String str2 = str1;
    if (bundle != null)
      try {
        str2 = bundle.getString("result");
      } catch (Exception exception) {
        exception.printStackTrace();
        str2 = str1;
      }  
    return str2;
  }
  
  public String getOpId(int paramInt) {
    Bundle bundle = requestForTelephonyEvent(paramInt, 6004, null);
    String str1 = null;
    String str2 = str1;
    if (bundle != null)
      try {
        str2 = bundle.getString("result");
      } catch (Exception exception) {
        exception.printStackTrace();
        str2 = str1;
      }  
    return str2;
  }
  
  public String getHotswapState(int paramInt) {
    Bundle bundle = requestForTelephonyEvent(paramInt, 6005, null);
    String str1 = null;
    String str2 = str1;
    if (bundle != null)
      try {
        str2 = bundle.getString("result");
      } catch (Exception exception) {
        exception.printStackTrace();
        str2 = str1;
      }  
    return str2;
  }
  
  public boolean getOperatorSwitchEnable(int paramInt) {
    Bundle bundle = requestForTelephonyEvent(paramInt, 6006, null);
    if (bundle != null)
      return bundle.getBoolean("result"); 
    return false;
  }
  
  public int getVsimId() {
    Bundle bundle = requestForTelephonyEvent(0, 6007, null);
    if (bundle != null)
      return bundle.getInt("result"); 
    return -1;
  }
  
  public int setVsimId(int paramInt) {
    Bundle bundle = new Bundle();
    bundle.putInt("content", paramInt);
    bundle = requestForTelephonyEvent(0, 6027, bundle);
    byte b = -1;
    paramInt = b;
    if (bundle != null)
      try {
        paramInt = bundle.getInt("result");
      } catch (Exception exception) {
        exception.printStackTrace();
        paramInt = b;
      }  
    return paramInt;
  }
  
  public int setDisplayNumberExt(String paramString, int paramInt) {
    try {
      if (this.mOplusTelephonyService != null)
        return this.mOplusTelephonyService.setDisplayNumberExt(paramString, paramInt); 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    return -1;
  }
  
  public String getOperatorNumericForData(int paramInt) {
    Exception exception1 = null;
    String str = null;
    if (!SubscriptionManager.isValidSubscriptionId(paramInt))
      return null; 
    try {
      if (this.mOplusTelephonyService != null) {
        paramInt = SubscriptionManager.getPhoneId(paramInt);
        if (!SubscriptionManager.isValidPhoneId(paramInt))
          return null; 
        str = this.mOplusTelephonyService.getOperatorNumericForData(paramInt);
      } 
    } catch (Exception exception2) {
      exception2.printStackTrace();
      exception2 = exception1;
    } 
    return (String)exception2;
  }
  
  public void startMobileDataHongbaoPolicy(int paramInt1, int paramInt2, String paramString1, String paramString2) {
    try {
      if (this.mOplusTelephonyService != null)
        this.mOplusTelephonyService.startMobileDataHongbaoPolicy(paramInt1, paramInt2, paramString1, paramString2); 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public static int[] calculateLengthOem(String paramString, boolean paramBoolean, int paramInt1, int paramInt2) {
    return OplusSmsMessage.calculateLengthOem(paramString, paramBoolean, paramInt1, paramInt2);
  }
  
  public ArrayList<String> divideMessageOem(String paramString, int paramInt1, int paramInt2) {
    if (OplusFeature.OPLUS_FEATURE_SMS_7BIT16BIT && (paramInt1 == 1 || paramInt1 == 3)) {
      if (isQcomPlatform)
        return divideMessageOemInner(paramString, paramInt2, paramInt1); 
      if (isMTKPlatform) {
        Class<int> clazz = int.class;
        Object object = ReflectionHelper.callDeclaredMethodOrThrow(null, "mediatek.telephony.MtkSmsManager", "getSmsManagerForSubscriptionId", new Class[] { clazz }, new Object[] { Integer.valueOf(paramInt2) });
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("mtkSmsManager=");
        stringBuilder.append(object);
        log(stringBuilder.toString());
        if (object != null) {
          Class<int> clazz1 = int.class;
          ReflectionHelper.callDeclaredMethodOrThrow(object, "mediatek.telephony.MtkSmsManager", "divideMessage", new Class[] { String.class, clazz1 }, new Object[] { paramString, Integer.valueOf(paramInt1) });
        } 
      } else {
        log("divideMessageOem, unkonw platform");
      } 
    } 
    return getSmsManagerForSubscriber(paramInt2).divideMessage(paramString);
  }
  
  private ArrayList<String> divideMessageOemInner(String paramString, int paramInt1, int paramInt2) {
    if (paramString != null)
      try {
        ArrayList<String> arrayList = OplusSmsMessage.oemFragmentText(paramString, paramInt1, paramInt2);
        if (arrayList != null) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("divideMessageOem,subId=");
          stringBuilder.append(paramInt1);
          stringBuilder.append(" ret.size()=");
          stringBuilder.append(arrayList.size());
          stringBuilder.append(" encodingType=");
          stringBuilder.append(paramInt2);
          log(stringBuilder.toString());
        } 
        return arrayList;
      } catch (Exception exception) {
        exception.printStackTrace();
        return SmsMessage.fragmentText(paramString, paramInt1);
      }  
    throw new IllegalArgumentException("text is null");
  }
  
  public void sendMultipartTextMessageOem(String paramString1, String paramString2, ArrayList<String> paramArrayList, ArrayList<PendingIntent> paramArrayList1, ArrayList<PendingIntent> paramArrayList2, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3, int paramInt4) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("encodingType=");
    stringBuilder.append(paramInt3);
    stringBuilder.append(", paltform=");
    stringBuilder.append(isQcomPlatform);
    log(stringBuilder.toString());
    if (OplusFeature.OPLUS_FEATURE_SMS_7BIT16BIT && (paramInt3 == 1 || paramInt3 == 3)) {
      if (isQcomPlatform) {
        sendMultipartTextMessageInternalOem(paramString1, paramString2, paramArrayList, paramArrayList1, paramArrayList2, true, paramInt1, paramBoolean, paramInt2, paramInt3, paramInt4);
      } else if (isMTKPlatform) {
        Class<int> clazz = int.class;
        Object object = ReflectionHelper.callDeclaredMethodOrThrow(null, "mediatek.telephony.MtkSmsManager", "getSmsManagerForSubscriptionId", new Class[] { clazz }, new Object[] { Integer.valueOf(paramInt4) });
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("mtkSmsManager=");
        stringBuilder1.append(object);
        log(stringBuilder1.toString());
        if (object != null) {
          ArrayList arrayList1 = new ArrayList();
          Class<?> clazz2 = arrayList1.getClass();
          Class<int> clazz1 = int.class;
          ArrayList arrayList2 = new ArrayList();
          Class<?> clazz4 = arrayList2.getClass(), clazz3 = (new ArrayList()).getClass();
          ReflectionHelper.callDeclaredMethodOrThrow(object, "mediatek.telephony.MtkSmsManager", "sendMultipartTextMessageWithEncodingType", new Class[] { String.class, String.class, clazz2, clazz1, clazz4, clazz3 }, new Object[] { paramString1, paramString2, paramArrayList, Integer.valueOf(paramInt3), paramArrayList1, paramArrayList2 });
        } 
      } else {
        log("sendMultipartTextMessageOem, unkonw platform");
      } 
      return;
    } 
    getSmsManagerForSubscriber(paramInt4).sendMultipartTextMessage(paramString1, paramString2, paramArrayList, paramArrayList1, paramArrayList2, paramInt1, paramBoolean, paramInt2);
  }
  
  private void sendMultipartTextMessageInternalOem(String paramString1, String paramString2, List<String> paramList, List<PendingIntent> paramList1, List<PendingIntent> paramList2, boolean paramBoolean1, int paramInt1, boolean paramBoolean2, int paramInt2, int paramInt3, int paramInt4) {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   4: ifne -> 329
    //   7: aload_3
    //   8: ifnull -> 318
    //   11: aload_3
    //   12: invokeinterface size : ()I
    //   17: iconst_1
    //   18: if_icmplt -> 318
    //   21: iload #7
    //   23: iflt -> 38
    //   26: iload #7
    //   28: iconst_3
    //   29: if_icmple -> 35
    //   32: goto -> 38
    //   35: goto -> 41
    //   38: iconst_m1
    //   39: istore #7
    //   41: iload #9
    //   43: iconst_5
    //   44: if_icmplt -> 61
    //   47: iload #9
    //   49: ldc_w 635040
    //   52: if_icmple -> 58
    //   55: goto -> 61
    //   58: goto -> 64
    //   61: iconst_m1
    //   62: istore #9
    //   64: aload_3
    //   65: invokeinterface size : ()I
    //   70: iconst_1
    //   71: if_icmple -> 212
    //   74: iload #6
    //   76: ifeq -> 113
    //   79: aload_0
    //   80: new android/telephony/OplusTelephonyManager$1
    //   83: dup
    //   84: aload_0
    //   85: aload_1
    //   86: aload_2
    //   87: aload_3
    //   88: aload #4
    //   90: aload #5
    //   92: iload #6
    //   94: iload #7
    //   96: iload #8
    //   98: iload #9
    //   100: iload #10
    //   102: invokespecial <init> : (Landroid/telephony/OplusTelephonyManager;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZIZII)V
    //   105: iload #11
    //   107: invokespecial resolveSubscriptionForOperation : (Landroid/telephony/OplusTelephonyManager$SubscriptionResolverResult;I)V
    //   110: goto -> 209
    //   113: aload_0
    //   114: getfield mOplusTelephonyService : Lcom/android/internal/telephony/IOplusTelephonyExt;
    //   117: ifnull -> 160
    //   120: aload_0
    //   121: getfield mOplusTelephonyService : Lcom/android/internal/telephony/IOplusTelephonyExt;
    //   124: astore #12
    //   126: aload #12
    //   128: iload #11
    //   130: aconst_null
    //   131: aload_1
    //   132: aload_2
    //   133: aload_3
    //   134: aload #4
    //   136: aload #5
    //   138: iload #6
    //   140: iload #7
    //   142: iload #8
    //   144: iload #9
    //   146: iload #10
    //   148: invokeinterface sendMultipartTextForSubscriberWithOptionsOem : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZIZII)V
    //   153: goto -> 160
    //   156: astore_1
    //   157: goto -> 164
    //   160: goto -> 209
    //   163: astore_1
    //   164: new java/lang/StringBuilder
    //   167: dup
    //   168: invokespecial <init> : ()V
    //   171: astore_2
    //   172: aload_2
    //   173: ldc_w 'sendMultipartTextMessageInternal (no persist): Couldn't send SMS - '
    //   176: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   179: pop
    //   180: aload_2
    //   181: aload_1
    //   182: invokevirtual getMessage : ()Ljava/lang/String;
    //   185: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   188: pop
    //   189: aload_2
    //   190: invokevirtual toString : ()Ljava/lang/String;
    //   193: astore_1
    //   194: ldc_w 'OplusTelephonyManager'
    //   197: aload_1
    //   198: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   201: pop
    //   202: aload #4
    //   204: bipush #31
    //   206: invokestatic notifySmsError : (Ljava/util/List;I)V
    //   209: goto -> 317
    //   212: aconst_null
    //   213: astore #13
    //   215: aload #13
    //   217: astore #12
    //   219: aload #4
    //   221: ifnull -> 251
    //   224: aload #13
    //   226: astore #12
    //   228: aload #4
    //   230: invokeinterface size : ()I
    //   235: ifle -> 251
    //   238: aload #4
    //   240: iconst_0
    //   241: invokeinterface get : (I)Ljava/lang/Object;
    //   246: checkcast android/app/PendingIntent
    //   249: astore #12
    //   251: aload #5
    //   253: ifnull -> 282
    //   256: aload #5
    //   258: invokeinterface size : ()I
    //   263: ifle -> 282
    //   266: aload #5
    //   268: iconst_0
    //   269: invokeinterface get : (I)Ljava/lang/Object;
    //   274: checkcast android/app/PendingIntent
    //   277: astore #4
    //   279: goto -> 285
    //   282: aconst_null
    //   283: astore #4
    //   285: aload_0
    //   286: aload_1
    //   287: aload_2
    //   288: aload_3
    //   289: iconst_0
    //   290: invokeinterface get : (I)Ljava/lang/Object;
    //   295: checkcast java/lang/String
    //   298: aload #12
    //   300: aload #4
    //   302: iload #6
    //   304: iload #7
    //   306: iload #8
    //   308: iload #9
    //   310: iload #10
    //   312: iload #11
    //   314: invokespecial sendTextMessageInternalOem : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;ZIZIII)V
    //   317: return
    //   318: new java/lang/IllegalArgumentException
    //   321: dup
    //   322: ldc_w 'Invalid message body'
    //   325: invokespecial <init> : (Ljava/lang/String;)V
    //   328: athrow
    //   329: new java/lang/IllegalArgumentException
    //   332: dup
    //   333: ldc_w 'Invalid destinationAddress'
    //   336: invokespecial <init> : (Ljava/lang/String;)V
    //   339: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #718	-> 0
    //   #721	-> 7
    //   #725	-> 21
    //   #726	-> 38
    //   #729	-> 41
    //   #730	-> 61
    //   #733	-> 64
    //   #734	-> 74
    //   #735	-> 74
    //   #736	-> 74
    //   #737	-> 79
    //   #762	-> 113
    //   #763	-> 120
    //   #768	-> 156
    //   #762	-> 160
    //   #772	-> 160
    //   #768	-> 163
    //   #769	-> 164
    //   #770	-> 180
    //   #769	-> 194
    //   #771	-> 202
    //   #774	-> 209
    //   #775	-> 212
    //   #776	-> 215
    //   #777	-> 215
    //   #778	-> 238
    //   #780	-> 251
    //   #781	-> 266
    //   #783	-> 282
    //   #787	-> 317
    //   #721	-> 318
    //   #722	-> 318
    //   #719	-> 329
    // Exception table:
    //   from	to	target	type
    //   113	120	163	android/os/RemoteException
    //   120	126	163	android/os/RemoteException
    //   126	153	156	android/os/RemoteException
  }
  
  private void sendTextMessageInternalOem(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, boolean paramBoolean1, int paramInt1, boolean paramBoolean2, int paramInt2, int paramInt3, int paramInt4) {
    if (!TextUtils.isEmpty(paramString1)) {
      if (!TextUtils.isEmpty(paramString3)) {
        if (paramInt1 < 0 || paramInt1 > 3)
          paramInt1 = -1; 
        if (paramInt2 < 5 || paramInt2 > 635040)
          paramInt2 = -1; 
        if (paramBoolean1) {
          resolveSubscriptionForOperation((SubscriptionResolverResult)new Object(this, paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2, paramBoolean1, paramInt1, paramBoolean2, paramInt2, paramInt3), paramInt4);
        } else {
          try {
            if (this.mOplusTelephonyService != null)
              this.mOplusTelephonyService.sendTextForSubscriberWithOptionsOem(paramInt4, null, paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2, paramBoolean1, paramInt1, paramBoolean2, paramInt2, paramInt3); 
          } catch (RemoteException remoteException) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("sendTextMessageInternal(no persist): Couldn't send SMS, exception - ");
            stringBuilder.append(remoteException.getMessage());
            String str = stringBuilder.toString();
            Log.e("OplusTelephonyManager", str);
            notifySmsError(paramPendingIntent1, 31);
          } 
        } 
        return;
      } 
      throw new IllegalArgumentException("Invalid message body");
    } 
    throw new IllegalArgumentException("Invalid destinationAddress");
  }
  
  private void resolveSubscriptionForOperation(SubscriptionResolverResult paramSubscriptionResolverResult, int paramInt) {
    boolean bool = false;
    boolean bool1 = false;
    try {
      ISms iSms = getISmsService();
      if (iSms != null)
        bool1 = iSms.isSmsSimPickActivityNeeded(paramInt); 
    } catch (RemoteException remoteException) {
      Log.e("OplusTelephonyManager", "resolveSubscriptionForOperation", (Throwable)remoteException);
      bool1 = bool;
    } 
    if (!bool1) {
      sendResolverResult(paramSubscriptionResolverResult, paramInt, false);
      return;
    } 
    Log.d("OplusTelephonyManager", "resolveSubscriptionForOperation isSmsSimPickActivityNeeded is true for calling package. ");
    try {
      ITelephony iTelephony = getITelephony();
      Object object = new Object();
      super(this, paramSubscriptionResolverResult);
      if (iTelephony != null)
        ReflectionHelper.callDeclaredMethod(iTelephony, "com.android.internal.telephony.ITelephony", "enqueueSmsPickResult", new Class[] { String.class, String.class, IIntegerConsumer.class }, new Object[] { null, null, object }); 
    } catch (Exception exception) {
      Log.e("OplusTelephonyManager", "Unable to launch activity", exception);
      sendResolverResult(paramSubscriptionResolverResult, paramInt, true);
    } 
  }
  
  private void sendResolverResult(SubscriptionResolverResult paramSubscriptionResolverResult, int paramInt, boolean paramBoolean) {
    if (SubscriptionManager.isValidSubscriptionId(paramInt)) {
      paramSubscriptionResolverResult.onSuccess(paramInt);
      return;
    } 
    if (!Compatibility.isChangeEnabled(145147529L) && !paramBoolean) {
      paramSubscriptionResolverResult.onSuccess(paramInt);
    } else {
      paramSubscriptionResolverResult.onFailure();
    } 
  }
  
  private static ISms getISmsService() {
    TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
    TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getSmsServiceRegisterer();
    IBinder iBinder = serviceRegisterer.get();
    return ISms.Stub.asInterface(iBinder);
  }
  
  private static ITelephony getITelephony() {
    TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
    TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getTelephonyServiceRegisterer();
    IBinder iBinder = serviceRegisterer.get();
    ITelephony iTelephony = ITelephony.Stub.asInterface(iBinder);
    if (iTelephony != null)
      return iTelephony; 
    throw new RuntimeException("Could not find Telephony Service.");
  }
  
  private static void notifySmsError(PendingIntent paramPendingIntent, int paramInt) {
    if (paramPendingIntent != null)
      try {
        paramPendingIntent.send(paramInt);
      } catch (android.app.PendingIntent.CanceledException canceledException) {} 
  }
  
  private static void notifySmsError(List<PendingIntent> paramList, int paramInt) {
    if (paramList != null)
      for (PendingIntent pendingIntent : paramList)
        notifySmsError(pendingIntent, paramInt);  
  }
  
  public static SmsManager getSmsManagerForSubscriber(int paramInt) {
    return SmsManager.getSmsManagerForSubscriptionId(paramInt);
  }
  
  public void eventDataCheckDns() {
    try {
      if (this.mOplusTelephonyService != null)
        this.mOplusTelephonyService.eventDataCheckDns(); 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public void sendRecoveryRequest(int paramInt1, int paramInt2) {
    try {
      if (this.mOplusTelephonyService != null)
        this.mOplusTelephonyService.sendRecoveryRequest(paramInt1, paramInt2); 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public long getActionExecuteTime(int paramInt1, int paramInt2) {
    long l1 = 0L;
    long l2 = l1;
    try {
      if (this.mOplusTelephonyService != null)
        l2 = this.mOplusTelephonyService.getActionExecuteTime(paramInt1, paramInt2); 
    } catch (Exception exception) {
      exception.printStackTrace();
      l2 = l1;
    } 
    return l2;
  }
  
  public int getLastAction(int paramInt) {
    try {
      if (this.mOplusTelephonyService != null)
        return this.mOplusTelephonyService.getLastAction(paramInt); 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    return -1;
  }
  
  public void eventDataCheckHttp() {
    try {
      if (this.mOplusTelephonyService != null)
        this.mOplusTelephonyService.eventDataCheckHttp(); 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public void eventDataCheckPdn() {
    try {
      if (this.mOplusTelephonyService != null)
        this.mOplusTelephonyService.eventDataCheckPdn(); 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public void eventDataActionAutoPlmn() {
    try {
      if (this.mOplusTelephonyService != null)
        this.mOplusTelephonyService.eventDataActionAutoPlmn(); 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public void eventDataActionRetryPdn() {
    try {
      if (this.mOplusTelephonyService != null)
        this.mOplusTelephonyService.eventDataActionRetryPdn(); 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public void ishVoLTESupport() {
    try {
      if (this.mOplusTelephonyService != null)
        this.mOplusTelephonyService.ishVoLTESupport(); 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public void reportNetWorkLatency(Context paramContext, String paramString) {
    try {
      if (!OplusFeature.OPLUS_FEATURE_LOG_GAME_ERR)
        return; 
      if (checkRevokeByGame(paramContext) && this.mOplusTelephonyService != null)
        this.mOplusTelephonyService.reportNetWorkLatency(paramString); 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public void reportNetWorkLevel(Context paramContext, String paramString) {
    try {
      if (!OplusFeature.OPLUS_FEATURE_LOG_GAME_ERR)
        return; 
      if (checkRevokeByGame(paramContext) && this.mOplusTelephonyService != null)
        this.mOplusTelephonyService.reportNetWorkLevel(paramString); 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public void reportGameEnterOrLeave(Context paramContext, int paramInt, String paramString, boolean paramBoolean) {
    try {
      if (!OplusFeature.OPLUS_FEATURE_LOG_GAME_ERR)
        return; 
      if (checkRevokeByGame(paramContext) && this.mOplusTelephonyService != null)
        this.mOplusTelephonyService.reportGameEnterOrLeave(paramInt, paramString, paramBoolean); 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  private boolean checkRevokeByGame(Context paramContext) {
    boolean bool = false;
    if (paramContext != null) {
      ApplicationInfo applicationInfo = paramContext.getApplicationInfo();
      if (applicationInfo != null) {
        String str = applicationInfo.packageName;
        if ("com.coloros.gamespace".equals(str) && 
          paramContext.checkCallingOrSelfPermission("oppo.permission.OPPO_COMPONENT_SAFE") == 0)
          bool = true; 
      } 
      if (!bool) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("checkRevokeByGame false,pkgInfo=");
        stringBuilder.append(applicationInfo);
        Rlog.e("OplusTelephonyManager", stringBuilder.toString());
      } 
      return bool;
    } 
    Rlog.e("OplusTelephonyManager", "checkRevokeByGame, err");
    return false;
  }
  
  public String[] getLteCdmaImsi(int paramInt) {
    try {
      if (this.mOplusTelephonyService != null)
        return this.mOplusTelephonyService.getLteCdmaImsi(paramInt); 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    Rlog.e("OplusTelephonyManager", "getLteCdmaImsi, err");
    return null;
  }
  
  private static interface SubscriptionResolverResult {
    void onFailure();
    
    void onSuccess(int param1Int);
  }
}
