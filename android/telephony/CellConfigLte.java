package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public class CellConfigLte implements Parcelable {
  public CellConfigLte() {
    this.mIsEndcAvailable = false;
  }
  
  public CellConfigLte(android.hardware.radio.V1_4.CellConfigLte paramCellConfigLte) {
    this.mIsEndcAvailable = paramCellConfigLte.isEndcAvailable;
  }
  
  public CellConfigLte(boolean paramBoolean) {
    this.mIsEndcAvailable = paramBoolean;
  }
  
  public CellConfigLte(CellConfigLte paramCellConfigLte) {
    this.mIsEndcAvailable = paramCellConfigLte.mIsEndcAvailable;
  }
  
  boolean isEndcAvailable() {
    return this.mIsEndcAvailable;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Boolean.valueOf(this.mIsEndcAvailable) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof CellConfigLte;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    if (this.mIsEndcAvailable == ((CellConfigLte)paramObject).mIsEndcAvailable)
      bool1 = true; 
    return bool1;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeBoolean(this.mIsEndcAvailable);
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(getClass().getName());
    stringBuilder1.append(" :{");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" isEndcAvailable = ");
    stringBuilder2.append(this.mIsEndcAvailable);
    String str = stringBuilder2.toString();
    stringBuilder1.append(str);
    stringBuilder1.append(" }");
    return stringBuilder1.toString();
  }
  
  private CellConfigLte(Parcel paramParcel) {
    this.mIsEndcAvailable = paramParcel.readBoolean();
  }
  
  public static final Parcelable.Creator<CellConfigLte> CREATOR = new Parcelable.Creator<CellConfigLte>() {
      public CellConfigLte createFromParcel(Parcel param1Parcel) {
        return new CellConfigLte(param1Parcel);
      }
      
      public CellConfigLte[] newArray(int param1Int) {
        return new CellConfigLte[0];
      }
    };
  
  private final boolean mIsEndcAvailable;
}
