package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.gsm.GsmCellLocation;
import android.util.ArraySet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public final class CellIdentityTdscdma extends CellIdentity {
  public static final Parcelable.Creator<CellIdentityTdscdma> CREATOR;
  
  private static final boolean DBG = false;
  
  private static final int MAX_CID = 268435455;
  
  private static final int MAX_CPID = 127;
  
  private static final int MAX_LAC = 65535;
  
  private static final int MAX_UARFCN = 65535;
  
  private static final String TAG = CellIdentityTdscdma.class.getSimpleName();
  
  private final ArraySet<String> mAdditionalPlmns;
  
  private final int mCid;
  
  private final int mCpid;
  
  private ClosedSubscriberGroupInfo mCsgInfo;
  
  private final int mLac;
  
  private final int mUarfcn;
  
  public CellIdentityTdscdma() {
    super(TAG, 5, null, null, null, null);
    this.mLac = Integer.MAX_VALUE;
    this.mCid = Integer.MAX_VALUE;
    this.mCpid = Integer.MAX_VALUE;
    this.mUarfcn = Integer.MAX_VALUE;
    this.mAdditionalPlmns = new ArraySet<>();
    this.mCsgInfo = null;
    this.mGlobalCellId = null;
  }
  
  public CellIdentityTdscdma(String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString3, String paramString4, Collection<String> paramCollection, ClosedSubscriberGroupInfo paramClosedSubscriberGroupInfo) {
    super(TAG, 5, paramString1, paramString2, paramString3, paramString4);
    this.mLac = inRangeOrUnavailable(paramInt1, 0, 65535);
    this.mCid = inRangeOrUnavailable(paramInt2, 0, 268435455);
    this.mCpid = inRangeOrUnavailable(paramInt3, 0, 127);
    this.mUarfcn = inRangeOrUnavailable(paramInt4, 0, 65535);
    this.mAdditionalPlmns = new ArraySet<>(paramCollection.size());
    for (String paramString1 : paramCollection) {
      if (isValidPlmn(paramString1))
        this.mAdditionalPlmns.add(paramString1); 
    } 
    this.mCsgInfo = paramClosedSubscriberGroupInfo;
    updateGlobalCellId();
  }
  
  private CellIdentityTdscdma(CellIdentityTdscdma paramCellIdentityTdscdma) {
    this(paramCellIdentityTdscdma.mMccStr, paramCellIdentityTdscdma.mMncStr, paramCellIdentityTdscdma.mLac, paramCellIdentityTdscdma.mCid, paramCellIdentityTdscdma.mCpid, paramCellIdentityTdscdma.mUarfcn, paramCellIdentityTdscdma.mAlphaLong, paramCellIdentityTdscdma.mAlphaShort, paramCellIdentityTdscdma.mAdditionalPlmns, paramCellIdentityTdscdma.mCsgInfo);
  }
  
  public CellIdentityTdscdma(android.hardware.radio.V1_0.CellIdentityTdscdma paramCellIdentityTdscdma) {
    this(str1, str2, i, j, k, 2147483647, "", "", (Collection)list, (ClosedSubscriberGroupInfo)null);
  }
  
  public CellIdentityTdscdma(android.hardware.radio.V1_2.CellIdentityTdscdma paramCellIdentityTdscdma) {
    this(str1, str2, i, j, k, m, str3, str4, (Collection)list, (ClosedSubscriberGroupInfo)null);
  }
  
  public CellIdentityTdscdma(android.hardware.radio.V1_5.CellIdentityTdscdma paramCellIdentityTdscdma) {
    this(str1, str2, i, j, k, m, str3, str4, arrayList, (ClosedSubscriberGroupInfo)paramCellIdentityTdscdma);
  }
  
  public CellIdentityTdscdma sanitizeLocationInfo() {
    return new CellIdentityTdscdma(this.mMccStr, this.mMncStr, 2147483647, 2147483647, 2147483647, 2147483647, this.mAlphaLong, this.mAlphaShort, this.mAdditionalPlmns, null);
  }
  
  CellIdentityTdscdma copy() {
    return new CellIdentityTdscdma(this);
  }
  
  protected void updateGlobalCellId() {
    this.mGlobalCellId = null;
    String str = getPlmn();
    if (str == null)
      return; 
    if (this.mLac == Integer.MAX_VALUE || this.mCid == Integer.MAX_VALUE)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str);
    stringBuilder.append(String.format("%04x%04x", new Object[] { Integer.valueOf(this.mLac), Integer.valueOf(this.mCid) }));
    this.mGlobalCellId = stringBuilder.toString();
  }
  
  public String getMccString() {
    return this.mMccStr;
  }
  
  public String getMncString() {
    return this.mMncStr;
  }
  
  public String getMobileNetworkOperator() {
    if (this.mMccStr == null || this.mMncStr == null)
      return null; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.mMccStr);
    stringBuilder.append(this.mMncStr);
    return stringBuilder.toString();
  }
  
  public int getLac() {
    return this.mLac;
  }
  
  public int getCid() {
    return this.mCid;
  }
  
  public int getCpid() {
    return this.mCpid;
  }
  
  public int getUarfcn() {
    return this.mUarfcn;
  }
  
  public int getChannelNumber() {
    return this.mUarfcn;
  }
  
  public Set<String> getAdditionalPlmns() {
    return Collections.unmodifiableSet(this.mAdditionalPlmns);
  }
  
  public ClosedSubscriberGroupInfo getClosedSubscriberGroupInfo() {
    return this.mCsgInfo;
  }
  
  public GsmCellLocation asCellLocation() {
    GsmCellLocation gsmCellLocation = new GsmCellLocation();
    int i = this.mLac;
    if (i == Integer.MAX_VALUE)
      i = -1; 
    int j = this.mCid;
    if (j == Integer.MAX_VALUE)
      j = -1; 
    gsmCellLocation.setLacAndCid(i, j);
    gsmCellLocation.setPsc(-1);
    return gsmCellLocation;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof CellIdentityTdscdma))
      return false; 
    CellIdentityTdscdma cellIdentityTdscdma = (CellIdentityTdscdma)paramObject;
    if (this.mLac == cellIdentityTdscdma.mLac && this.mCid == cellIdentityTdscdma.mCid && this.mCpid == cellIdentityTdscdma.mCpid && this.mUarfcn == cellIdentityTdscdma.mUarfcn) {
      ArraySet<String> arraySet1 = this.mAdditionalPlmns, arraySet2 = cellIdentityTdscdma.mAdditionalPlmns;
      if (arraySet1.equals(arraySet2)) {
        ClosedSubscriberGroupInfo closedSubscriberGroupInfo2 = this.mCsgInfo, closedSubscriberGroupInfo1 = cellIdentityTdscdma.mCsgInfo;
        if (Objects.equals(closedSubscriberGroupInfo2, closedSubscriberGroupInfo1) && 
          super.equals(paramObject))
          return null; 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = this.mLac, j = this.mCid, k = this.mCpid, m = this.mUarfcn;
    ArraySet<String> arraySet = this.mAdditionalPlmns;
    int n = arraySet.hashCode();
    ClosedSubscriberGroupInfo closedSubscriberGroupInfo = this.mCsgInfo;
    int i1 = super.hashCode();
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), closedSubscriberGroupInfo, Integer.valueOf(i1) });
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(TAG);
    stringBuilder.append(":{ mMcc=");
    stringBuilder.append(this.mMccStr);
    stringBuilder.append(" mMnc=");
    stringBuilder.append(this.mMncStr);
    stringBuilder.append(" mAlphaLong=");
    stringBuilder.append(this.mAlphaLong);
    stringBuilder.append(" mAlphaShort=");
    stringBuilder.append(this.mAlphaShort);
    stringBuilder.append(" mLac=");
    stringBuilder.append(this.mLac);
    stringBuilder.append(" mCid=");
    stringBuilder.append(this.mCid);
    stringBuilder.append(" mCpid=");
    stringBuilder.append(this.mCpid);
    stringBuilder.append(" mUarfcn=");
    stringBuilder.append(this.mUarfcn);
    stringBuilder.append(" mAdditionalPlmns=");
    stringBuilder.append(this.mAdditionalPlmns);
    stringBuilder.append(" mCsgInfo=");
    stringBuilder.append(this.mCsgInfo);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    super.writeToParcel(paramParcel, 5);
    paramParcel.writeInt(this.mLac);
    paramParcel.writeInt(this.mCid);
    paramParcel.writeInt(this.mCpid);
    paramParcel.writeInt(this.mUarfcn);
    paramParcel.writeArraySet(this.mAdditionalPlmns);
    paramParcel.writeParcelable(this.mCsgInfo, paramInt);
  }
  
  private CellIdentityTdscdma(Parcel paramParcel) {
    super(TAG, 5, paramParcel);
    this.mLac = paramParcel.readInt();
    this.mCid = paramParcel.readInt();
    this.mCpid = paramParcel.readInt();
    this.mUarfcn = paramParcel.readInt();
    this.mAdditionalPlmns = paramParcel.readArraySet(null);
    this.mCsgInfo = (ClosedSubscriberGroupInfo)paramParcel.readParcelable(null);
    updateGlobalCellId();
  }
  
  static {
    CREATOR = (Parcelable.Creator<CellIdentityTdscdma>)new Object();
  }
  
  protected static CellIdentityTdscdma createFromParcelBody(Parcel paramParcel) {
    return new CellIdentityTdscdma(paramParcel);
  }
}
