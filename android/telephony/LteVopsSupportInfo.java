package android.telephony;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;

@SystemApi
public final class LteVopsSupportInfo implements Parcelable {
  public LteVopsSupportInfo(int paramInt1, int paramInt2) {
    this.mVopsSupport = paramInt1;
    this.mEmcBearerSupport = paramInt2;
  }
  
  public int getVopsSupport() {
    return this.mVopsSupport;
  }
  
  public int getEmcBearerSupport() {
    return this.mEmcBearerSupport;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mVopsSupport);
    paramParcel.writeInt(this.mEmcBearerSupport);
  }
  
  public boolean equals(Object paramObject) {
    boolean bool1 = false;
    if (paramObject == null || !(paramObject instanceof LteVopsSupportInfo))
      return false; 
    if (this == paramObject)
      return true; 
    paramObject = paramObject;
    boolean bool2 = bool1;
    if (this.mVopsSupport == ((LteVopsSupportInfo)paramObject).mVopsSupport) {
      bool2 = bool1;
      if (this.mEmcBearerSupport == ((LteVopsSupportInfo)paramObject).mEmcBearerSupport)
        bool2 = true; 
    } 
    return bool2;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mVopsSupport), Integer.valueOf(this.mEmcBearerSupport) });
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("LteVopsSupportInfo :  mVopsSupport = ");
    stringBuilder.append(this.mVopsSupport);
    stringBuilder.append(" mEmcBearerSupport = ");
    stringBuilder.append(this.mEmcBearerSupport);
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<LteVopsSupportInfo> CREATOR = new Parcelable.Creator<LteVopsSupportInfo>() {
      public LteVopsSupportInfo createFromParcel(Parcel param1Parcel) {
        return new LteVopsSupportInfo(param1Parcel);
      }
      
      public LteVopsSupportInfo[] newArray(int param1Int) {
        return new LteVopsSupportInfo[param1Int];
      }
    };
  
  public static final int LTE_STATUS_NOT_AVAILABLE = 1;
  
  public static final int LTE_STATUS_NOT_SUPPORTED = 3;
  
  public static final int LTE_STATUS_SUPPORTED = 2;
  
  private final int mEmcBearerSupport;
  
  private final int mVopsSupport;
  
  private LteVopsSupportInfo(Parcel paramParcel) {
    this.mVopsSupport = paramParcel.readInt();
    this.mEmcBearerSupport = paramParcel.readInt();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class LteVopsStatus implements Annotation {}
}
