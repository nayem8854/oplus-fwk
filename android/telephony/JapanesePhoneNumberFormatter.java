package android.telephony;

import android.text.Editable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import res.Hex;

class JapanesePhoneNumberFormatter {
  private static long[] $d2j$hex$411a6177$decode_J(String src) {
    byte[] d = Hex.decode_B(src);
    ByteBuffer b = ByteBuffer.wrap(d);
    b.order(ByteOrder.LITTLE_ENDIAN);
    LongBuffer s = b.asLongBuffer();
    long[] data = new long[d.length / 8];
    s.get(data);
    return data;
  }
  
  private static int[] $d2j$hex$411a6177$decode_I(String src) {
    byte[] d = Hex.decode_B(src);
    ByteBuffer b = ByteBuffer.wrap(d);
    b.order(ByteOrder.LITTLE_ENDIAN);
    IntBuffer s = b.asIntBuffer();
    int[] data = new int[d.length / 4];
    s.get(data);
    return data;
  }
  
  private static short[] $d2j$hex$411a6177$decode_S(String src) {
    byte[] d = Hex.decode_B(src);
    ByteBuffer b = ByteBuffer.wrap(d);
    b.order(ByteOrder.LITTLE_ENDIAN);
    ShortBuffer s = b.asShortBuffer();
    short[] data = new short[d.length / 2];
    s.get(data);
    return data;
  }
  
  private static short[] FORMAT_MAP = $d2j$hex$411a6177$decode_S("9cff0a00dc00f1ff9a011202b0049e020c0324049cffe7ff14002800460064009600be00c800d200dcff9cff9cffddffddffddff1e009cff9cff9cffddffddffddffddffddffddffddffd3ffddffddff9cff9cff9cffddffddffddffddff3200ddff3c00ddffddffd3ffddffd3ffddffddffd3ffddffddffddffddffd3ffddffddffddffddffd3ffd3ffddff9cff9cffddffddffddff50005a009cff9cff9cffddffddffddffddffddffddffd3ffd3ffddffddffddffddffddffddffddffddffd3ffddffddffddffe7ffe7ffddffddff6e0078008200ddff8c00e7ffddffe7ffddffddffddffddffddffd3ffe7ffddffddffe7ffddffddffddffddffddffe7ffd3ffddffddffddffddffddffd3ffddffddffddffddffddffddffddffddffddffddffddffd3ffd3ffddffddff9cff9cffddffa000aa00b400ddffddff9cff9cffddffddffd3ffddffd3ffd3ffddffddffddffddffddffddffddffddffddffddffddffddffd3ffddffddffddffddffddffd3ffd3ffd3ffddffd3ffddffe7ffe7ffddffddffddffddffddffe7ffddffddffe7ffe7ffddffddffddffddffddffddffe7ffe7ffe7ffddffddffddffddffddffe7ffddffddffe7ff9cff9cffe600fa0004010e014001540168018601ddffe7ffe7fff000ddffddffddffe7ffddffddffe7ffddffddffddffe7ffe7ffe7ffe7ffe7ffe7ffe7ffe7ffe7ffddffddffddffe7ffddffddffe7ffddffddffddffddffddffe7ffddffddffddffe7ffddffe7ffe7ffe7ffddff180122012c013601ddffe7ffe7ffe7ffe7ffe7ffe7ffe7ffddffddffe7ffe7ffddffddffddffddffddffddffddffddffddffe7ffe7ffddffddffddffe7ffe7ffe7ffe7ffe7ffe7ffddffddffddffddffddffddffddffddffddffddffddffe7ffddff4a01ddffddffddffddffddffe7ffddffddffddffddffddffe7ffe7ffe7ffe7ffddffe7ffe7ffe7ffddffe7ffddffddff5e01ddffe7ffddffddffddffddffddffddffddffe7ffe7ffddffe7ffddff7201ddffddffe7ffddffddff7c01e7ffddffddffe7ffe7ffddffddffddffddffddffe7ffddffe7ffe7ffe7ffe7ffddffddffddffddffe7ffddffe7ff9001ddffddffddffddffe7ffddffe7ffddffddffddffddffe7ffe7ffe7ffe7ffe7fff1fff1ffa401cc01e7ffe7ffd601e001f401fe01f1ffe7ffae01e7ffe7ffe7ffe7ffe7ffb801c201e7ffddffddffddffddffddffddffddffddffddffe7ffe7ffddffddffe7ffe7ffe7ffddffddffddfff1ffe7fff1fff1fff1fff1fff1ffe7ffe7fff1ffe7ffe7ffe7ffe7ffe7ffe7ffddffe7ffddffddffddffe7ffe7ffddffe7ffddffddffddffe7ffe7ffea01f1ffe7ffe7ffe7ffddffddffe7ffddffddfff1ffddffddffddffddffddffddffddffddfff1ffddffe7ffe7ffe7ffe7ffe7ffe7ffe7ffe7ffe7ffe7ffe7ffe7ffddffddffddffe7ffe7ffe7ff08029cff9cffd3ff9cffd3ff9cffd3ff9cffd3ff9cffe6ff9cffe7ff1c0244024e025802620276028002e7ffddffddffddffe7ffe7ffddffddffddff2602ddffddffe7ffe7ffe7ffe7ff30023a02e7ffddffddffddffddffddffe7ffe7ffe7ffe7ffe7ffe7ffe7ffe7ffe7ffe7ffddffe7ffe7ffddffe7ffe7ffe7ffe7ffe7ffe7ffddffddffe7ffddffddffe7ffddffddffe7ffddffddffddffddffddffddffe7ff9cffddffddffddffddffddffddffddffddffddffdcff9cffddffddffddffddff6c02ddffddff9cffddffddffddffddffddffddffddffddffddffd3ffe7ffddffe7ffe7ffddffddffddffddffe7ffe7ffe7ffe7ffe7ffe7ffddffddffddff8a02ddff9402ddffddffddffddffd3ffddffddffddffddffd3ffddffddffddffddffddffddffddffddffddffe7ffe6ff9cffa802b202bc02e7ffd002da02e7ffe402e7ffddffe7ffe7ffe7ffddffe7ffe7ffe7ffe7ffe7ffe7ffe7ffe7ffe7ffddffddffddffddffddffddff9cffddffddffddffddffc602ddffddffddffddffddffddffddffddffddffddffddffd3ffddffe7ffddffe7ffddffe7ffddffddffddffddffe7ffddffddffddffddffddffe7ffddffe7ffddffddffddffddffe7ffe7ffee02f8020203ddffddffddffe7ffddffe7ffe7ffe7ffe7ffddffddffddffe7ffe7ffddffddffddffddffe7ffe7ffddffddffe7ffe7ffddffddffddffddffddffe7ffe7ffddffddff16039cff2003520384039803ac03060410041a04dcffe6ffe6ffe6ffe6ffe6ffe6ffe6ffe6ffe6ffddffe7ffe7ffddff2a03e7ffddffddffe7ff3403e7ffddffe7ffe7ffddffddffddffddffddffe7ffe7ffddff3e03ddff4803ddffe7ffddffddffe7ffddffe7ffe7ffe7ffe7ffe7ffe7ffe7ffe7ffe7ff9cffe7ffe7ffe7ff9cff9cff9cff9cff9cff9cffe7ffe7ffddffddffddffddff5c03ddff66037003e7ffddffddffddffddffddffddffddffddffddffddffddffddffddffddffddffddffd3ffd3ffddff9cff9cff9cff9cff9cff9cff7a039cff9cff9cffe7ffd3ffd3ffe7ffd3ffd3ffe7ffd3ffd3ffd3ffe7ffe7ffe7ffe7ffe7ffddffddff8e03ddffe7ffddffddffddffddffddffddffddffd3ffddffddff9cffa203ddffddffddffddffddffddffddffddff9cff9cffd3ff9cffd3ff9cff9cff9cff9cff9cffe7ffe7ffe7ffb603e7ffca03de03ddffe803f203ddffddffddffddffddffddffc003ddffddffddffd3ffd3ffd3ffd3ffd3ffd3ffddffd3ffd3ffd3ffddffddffe7ffddffddffd403ddffddffddffddff9cff9cffe7ffe7ff9cff9cff9cff9cff9cff9cffe7ffddffddffddffddffddffddffddffddffddffe7ffddffddffddffddffddffddffddffddffe7ffe7ffddffddffddffe7ffe7ffddffddffddfffc03d3ffd3ffddffddffd3ffd3ffd3ffd3ffd3ffd3ffe7ffe7ffe7ffe7ffe7ffddffe7ffddffe7ffddffddffe7ffe7ffddffddffddffe7ffddffe7ffddffe7ffe7ffddffddffddffddffddffddffddffe7ffe6ff9cff2e0438044204560460046a0474048804ddffe7ffe7ffe7ffe7ffe7ffe7ffe7ffe7ffe7ffddffe7ffe7ffe7ffe7ffe7ffe7ffe7ffe7ffe7ffddff9cffddffddffddff9cffddffddffddff4c04ddffddffddffddffddffddffd3ffddffddffddffddffe7ffddffe7ffddffddffddffddffe7ffddffe7ffe7ffe7ffe7ffddffddffddffddffddffddffe7ffe7ffddffddffddffe7ffe7ffddffddffddff7e04e7ffddffddffddffddffddffddffe7ffe7ffddffddffd3ffddffddffddffddffddffddffddffddff9204e7ffddff9c04ddffa604ddffe7ffe7ff9cff9cffd3ffd3ff9cff9cff9cff9cff9cff9cffe7ffddffddffddffddffddffddffe7ffe7ffddffddffddffddffddffddffddffddffddffddffd3ffe6fff1fff1fff1fff1fff1fff1fff1fff1fff1ff");
  
  private static byte[] $d2j$hex$411a6177$decode_B(String src) {
    char[] d = src.toCharArray();
    byte[] ret = new byte[src.length() / 2];
    for (int i = 0; i < ret.length; i++) {
      char h = d[2 * i];
      char l = d[2 * i + 1];
      int hh = 0;
      if (h >= '0' && h <= '9') {
        hh = h - 48;
      } else if (h >= 'a' && h <= 'f') {
        hh = h - 97 + 10;
      } else if (h >= 'A' && h <= 'F') {
        hh = h - 65 + 10;
      } else {
        throw new RuntimeException();
      } 
      int ll = 0;
      if (l >= '0' && l <= '9') {
        ll = h - 48;
      } else if (l >= 'a' && l <= 'f') {
        ll = h - 97 + 10;
      } else if (l >= 'A' && l <= 'F') {
        ll = h - 65 + 10;
      } else {
        throw new RuntimeException();
      } 
      d[i] = (char)(hh << 4 | ll);
    } 
    return ret;
  }
  
  public static void format(Editable paramEditable) {
    byte b = 1;
    int i = paramEditable.length();
    if (i > 3 && 
      paramEditable.subSequence(0, 3).toString().equals("+81")) {
      b = 3;
    } else if (i < 1 || paramEditable.charAt(0) != '0') {
      return;
    } 
    CharSequence charSequence = paramEditable.subSequence(0, i);
    i = 0;
    while (i < paramEditable.length()) {
      if (paramEditable.charAt(i) == '-') {
        paramEditable.delete(i, i + 1);
        continue;
      } 
      i++;
    } 
    int j = paramEditable.length();
    i = b;
    short s = 0;
    while (i < j) {
      char c = paramEditable.charAt(i);
      if (!Character.isDigit(c)) {
        paramEditable.replace(0, j, charSequence);
        return;
      } 
      s = FORMAT_MAP[s + c - 48];
      if (s < 0) {
        if (s <= -100) {
          paramEditable.replace(0, j, charSequence);
          return;
        } 
        i = Math.abs(s) % 10 + b;
        if (j > i)
          paramEditable.insert(i, "-"); 
        i = Math.abs(s) / 10 + b;
        if (j > i)
          paramEditable.insert(i, "-"); 
        break;
      } 
      i++;
    } 
    if (j > 3 && b == 3)
      paramEditable.insert(b, "-"); 
  }
}
