package android.telephony;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.android.telephony.Rlog;
import java.util.Objects;
import java.util.UUID;

public abstract class CellIdentity implements Parcelable {
  protected CellIdentity(String paramString1, int paramInt, String paramString2, String paramString3, String paramString4, String paramString5) {
    this.mTag = paramString1;
    this.mType = paramInt;
    if (paramString2 == null || isMcc(paramString2)) {
      this.mMccStr = paramString2;
    } else if (paramString2.isEmpty() || paramString2.equals(String.valueOf(2147483647))) {
      this.mMccStr = null;
    } else {
      this.mMccStr = null;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("invalid MCC format: ");
      stringBuilder.append(paramString2);
      log(stringBuilder.toString());
    } 
    if (paramString3 == null || isMnc(paramString3)) {
      this.mMncStr = paramString3;
    } else if (paramString3.isEmpty() || paramString3.equals(String.valueOf(2147483647))) {
      this.mMncStr = null;
    } else {
      this.mMncStr = null;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("invalid MNC format: ");
      stringBuilder.append(paramString3);
      log(stringBuilder.toString());
    } 
    if ((this.mMccStr != null && this.mMncStr == null) || (this.mMccStr == null && this.mMncStr != null)) {
      UUID uUID = UUID.fromString("a3ab0b9d-f2aa-4baf-911d-7096c0d4645a");
      AnomalyReporter.reportAnomaly(uUID, "CellIdentity Missing Half of PLMN ID");
    } 
    this.mAlphaLong = paramString4;
    this.mAlphaShort = paramString5;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public String getMccString() {
    return this.mMccStr;
  }
  
  public String getMncString() {
    return this.mMncStr;
  }
  
  public int getChannelNumber() {
    return -1;
  }
  
  public CharSequence getOperatorAlphaLong() {
    return this.mAlphaLong;
  }
  
  public void setOperatorAlphaLong(String paramString) {
    this.mAlphaLong = paramString;
  }
  
  public CharSequence getOperatorAlphaShort() {
    return this.mAlphaShort;
  }
  
  public void setOperatorAlphaShort(String paramString) {
    this.mAlphaShort = paramString;
  }
  
  public String getGlobalCellId() {
    return this.mGlobalCellId;
  }
  
  public boolean isSameCell(CellIdentity paramCellIdentity) {
    if (paramCellIdentity == null)
      return false; 
    if (getClass() != paramCellIdentity.getClass())
      return false; 
    if (getGlobalCellId() == null || paramCellIdentity.getGlobalCellId() == null)
      return false; 
    return TextUtils.equals(getGlobalCellId(), paramCellIdentity.getGlobalCellId());
  }
  
  public String getPlmn() {
    if (this.mMccStr == null || this.mMncStr == null)
      return null; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.mMccStr);
    stringBuilder.append(this.mMncStr);
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof CellIdentity;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    if (this.mType == ((CellIdentity)paramObject).mType) {
      String str1 = this.mMccStr, str2 = ((CellIdentity)paramObject).mMccStr;
      if (TextUtils.equals(str1, str2)) {
        str2 = this.mMncStr;
        str1 = ((CellIdentity)paramObject).mMncStr;
        if (TextUtils.equals(str2, str1)) {
          str2 = this.mAlphaLong;
          str1 = ((CellIdentity)paramObject).mAlphaLong;
          if (TextUtils.equals(str2, str1)) {
            str2 = this.mAlphaShort;
            paramObject = ((CellIdentity)paramObject).mAlphaShort;
            if (TextUtils.equals(str2, (CharSequence)paramObject))
              bool1 = true; 
          } 
        } 
      } 
    } 
    return bool1;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mAlphaLong, this.mAlphaShort, this.mMccStr, this.mMncStr, Integer.valueOf(this.mType) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(paramInt);
    paramParcel.writeString(this.mMccStr);
    paramParcel.writeString(this.mMncStr);
    paramParcel.writeString(this.mAlphaLong);
    paramParcel.writeString(this.mAlphaShort);
  }
  
  public static boolean isValidPlmn(String paramString) {
    int i = paramString.length();
    boolean bool1 = false;
    if (i < 5 || 
      paramString.length() > 6)
      return false; 
    boolean bool2 = bool1;
    if (isMcc(paramString.substring(0, 3))) {
      bool2 = bool1;
      if (isMnc(paramString.substring(3)))
        bool2 = true; 
    } 
    return bool2;
  }
  
  protected CellIdentity(String paramString, int paramInt, Parcel paramParcel) {
    this(paramString, paramInt, str2, str3, str4, str1);
  }
  
  public static final Parcelable.Creator<CellIdentity> CREATOR = new Parcelable.Creator<CellIdentity>() {
      public CellIdentity createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        switch (i) {
          default:
            throw new IllegalArgumentException("Bad Cell identity Parcel");
          case 6:
            return CellIdentityNr.createFromParcelBody(param1Parcel);
          case 5:
            return CellIdentityTdscdma.createFromParcelBody(param1Parcel);
          case 4:
            return CellIdentityWcdma.createFromParcelBody(param1Parcel);
          case 3:
            return CellIdentityLte.createFromParcelBody(param1Parcel);
          case 2:
            return CellIdentityCdma.createFromParcelBody(param1Parcel);
          case 1:
            break;
        } 
        return CellIdentityGsm.createFromParcelBody(param1Parcel);
      }
      
      public CellIdentity[] newArray(int param1Int) {
        return new CellIdentity[param1Int];
      }
    };
  
  public static final int INVALID_CHANNEL_NUMBER = -1;
  
  public static final int MCC_LENGTH = 3;
  
  public static final int MNC_MAX_LENGTH = 3;
  
  public static final int MNC_MIN_LENGTH = 2;
  
  protected String mAlphaLong;
  
  protected String mAlphaShort;
  
  protected String mGlobalCellId;
  
  protected final String mMccStr;
  
  protected final String mMncStr;
  
  protected final String mTag;
  
  protected final int mType;
  
  protected void log(String paramString) {
    Rlog.w(this.mTag, paramString);
  }
  
  protected static final int inRangeOrUnavailable(int paramInt1, int paramInt2, int paramInt3) {
    if (paramInt1 < paramInt2 || paramInt1 > paramInt3)
      return Integer.MAX_VALUE; 
    return paramInt1;
  }
  
  protected static final long inRangeOrUnavailable(long paramLong1, long paramLong2, long paramLong3) {
    if (paramLong1 < paramLong2 || paramLong1 > paramLong3)
      return Long.MAX_VALUE; 
    return paramLong1;
  }
  
  protected static final int inRangeOrUnavailable(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if ((paramInt1 < paramInt2 || paramInt1 > paramInt3) && paramInt1 != paramInt4)
      return Integer.MAX_VALUE; 
    return paramInt1;
  }
  
  private static boolean isMcc(String paramString) {
    if (paramString.length() != 3)
      return false; 
    for (byte b = 0; b < 3; b++) {
      if (paramString.charAt(b) < '0' || paramString.charAt(b) > '9')
        return false; 
    } 
    return true;
  }
  
  private static boolean isMnc(String paramString) {
    if (paramString.length() < 2 || paramString.length() > 3)
      return false; 
    for (byte b = 0; b < paramString.length(); b++) {
      if (paramString.charAt(b) < '0' || paramString.charAt(b) > '9')
        return false; 
    } 
    return true;
  }
  
  public static CellIdentity create(android.hardware.radio.V1_0.CellIdentity paramCellIdentity) {
    if (paramCellIdentity == null)
      return null; 
    int i = paramCellIdentity.cellInfoType;
    if (i != 1) {
      if (i != 2) {
        if (i != 3) {
          if (i != 4) {
            if (i == 5)
              if (paramCellIdentity.cellIdentityTdscdma.size() == 1)
                return new CellIdentityTdscdma(paramCellIdentity.cellIdentityTdscdma.get(0));  
          } else if (paramCellIdentity.cellIdentityWcdma.size() == 1) {
            return new CellIdentityWcdma(paramCellIdentity.cellIdentityWcdma.get(0));
          } 
        } else if (paramCellIdentity.cellIdentityLte.size() == 1) {
          return new CellIdentityLte(paramCellIdentity.cellIdentityLte.get(0));
        } 
      } else if (paramCellIdentity.cellIdentityCdma.size() == 1) {
        return new CellIdentityCdma(paramCellIdentity.cellIdentityCdma.get(0));
      } 
    } else if (paramCellIdentity.cellIdentityGsm.size() == 1) {
      return new CellIdentityGsm(paramCellIdentity.cellIdentityGsm.get(0));
    } 
    return null;
  }
  
  public static CellIdentity create(android.hardware.radio.V1_2.CellIdentity paramCellIdentity) {
    if (paramCellIdentity == null)
      return null; 
    int i = paramCellIdentity.cellInfoType;
    if (i != 1) {
      if (i != 2) {
        if (i != 3) {
          if (i != 4) {
            if (i == 5)
              if (paramCellIdentity.cellIdentityTdscdma.size() == 1)
                return new CellIdentityTdscdma(paramCellIdentity.cellIdentityTdscdma.get(0));  
          } else if (paramCellIdentity.cellIdentityWcdma.size() == 1) {
            return new CellIdentityWcdma(paramCellIdentity.cellIdentityWcdma.get(0));
          } 
        } else if (paramCellIdentity.cellIdentityLte.size() == 1) {
          return new CellIdentityLte(paramCellIdentity.cellIdentityLte.get(0));
        } 
      } else if (paramCellIdentity.cellIdentityCdma.size() == 1) {
        return new CellIdentityCdma(paramCellIdentity.cellIdentityCdma.get(0));
      } 
    } else if (paramCellIdentity.cellIdentityGsm.size() == 1) {
      return new CellIdentityGsm(paramCellIdentity.cellIdentityGsm.get(0));
    } 
    return null;
  }
  
  public static CellIdentity create(android.hardware.radio.V1_5.CellIdentity paramCellIdentity) {
    if (paramCellIdentity == null)
      return null; 
    switch (paramCellIdentity.getDiscriminator()) {
      default:
        return null;
      case 6:
        return new CellIdentityNr(paramCellIdentity.nr());
      case 5:
        return new CellIdentityLte(paramCellIdentity.lte());
      case 4:
        return new CellIdentityCdma(paramCellIdentity.cdma());
      case 3:
        return new CellIdentityTdscdma(paramCellIdentity.tdscdma());
      case 2:
        return new CellIdentityWcdma(paramCellIdentity.wcdma());
      case 1:
        break;
    } 
    return new CellIdentityGsm(paramCellIdentity.gsm());
  }
  
  @SystemApi
  public abstract CellLocation asCellLocation();
  
  @SystemApi
  public abstract CellIdentity sanitizeLocationInfo();
  
  protected abstract void updateGlobalCellId();
}
