package android.telephony;

import android.hardware.radio.V1_0.CellInfo;
import android.hardware.radio.V1_2.CellInfo;
import android.hardware.radio.V1_4.CellInfo;
import android.hardware.radio.V1_5.CellInfo;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.telephony.Rlog;
import java.util.Objects;

public final class CellInfoWcdma extends CellInfo implements Parcelable {
  public CellInfoWcdma() {
    this.mCellIdentityWcdma = new CellIdentityWcdma();
    this.mCellSignalStrengthWcdma = new CellSignalStrengthWcdma();
  }
  
  public CellInfoWcdma(CellInfoWcdma paramCellInfoWcdma) {
    super(paramCellInfoWcdma);
    this.mCellIdentityWcdma = paramCellInfoWcdma.mCellIdentityWcdma.copy();
    this.mCellSignalStrengthWcdma = paramCellInfoWcdma.mCellSignalStrengthWcdma.copy();
  }
  
  public CellInfoWcdma(CellInfo paramCellInfo) {
    super(paramCellInfo);
    android.hardware.radio.V1_0.CellInfoWcdma cellInfoWcdma = paramCellInfo.wcdma.get(0);
    this.mCellIdentityWcdma = new CellIdentityWcdma(cellInfoWcdma.cellIdentityWcdma);
    this.mCellSignalStrengthWcdma = new CellSignalStrengthWcdma(cellInfoWcdma.signalStrengthWcdma);
  }
  
  public CellInfoWcdma(CellInfo paramCellInfo) {
    super(paramCellInfo);
    android.hardware.radio.V1_2.CellInfoWcdma cellInfoWcdma = paramCellInfo.wcdma.get(0);
    this.mCellIdentityWcdma = new CellIdentityWcdma(cellInfoWcdma.cellIdentityWcdma);
    this.mCellSignalStrengthWcdma = new CellSignalStrengthWcdma(cellInfoWcdma.signalStrengthWcdma);
  }
  
  public CellInfoWcdma(CellInfo paramCellInfo, long paramLong) {
    super(paramCellInfo, paramLong);
    android.hardware.radio.V1_2.CellInfoWcdma cellInfoWcdma = paramCellInfo.info.wcdma();
    this.mCellIdentityWcdma = new CellIdentityWcdma(cellInfoWcdma.cellIdentityWcdma);
    this.mCellSignalStrengthWcdma = new CellSignalStrengthWcdma(cellInfoWcdma.signalStrengthWcdma);
  }
  
  public CellInfoWcdma(CellInfo paramCellInfo, long paramLong) {
    super(paramCellInfo, paramLong);
    android.hardware.radio.V1_5.CellInfoWcdma cellInfoWcdma = paramCellInfo.ratSpecificInfo.wcdma();
    this.mCellIdentityWcdma = new CellIdentityWcdma(cellInfoWcdma.cellIdentityWcdma);
    this.mCellSignalStrengthWcdma = new CellSignalStrengthWcdma(cellInfoWcdma.signalStrengthWcdma);
  }
  
  public CellIdentityWcdma getCellIdentity() {
    return this.mCellIdentityWcdma;
  }
  
  public void setCellIdentity(CellIdentityWcdma paramCellIdentityWcdma) {
    this.mCellIdentityWcdma = paramCellIdentityWcdma;
  }
  
  public CellSignalStrengthWcdma getCellSignalStrength() {
    return this.mCellSignalStrengthWcdma;
  }
  
  public CellInfo sanitizeLocationInfo() {
    CellInfoWcdma cellInfoWcdma = new CellInfoWcdma(this);
    cellInfoWcdma.mCellIdentityWcdma = this.mCellIdentityWcdma.sanitizeLocationInfo();
    return cellInfoWcdma;
  }
  
  public void setCellSignalStrength(CellSignalStrengthWcdma paramCellSignalStrengthWcdma) {
    this.mCellSignalStrengthWcdma = paramCellSignalStrengthWcdma;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(super.hashCode()), this.mCellIdentityWcdma, this.mCellSignalStrengthWcdma });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = super.equals(paramObject);
    boolean bool1 = false;
    if (!bool)
      return false; 
    try {
      CellInfoWcdma cellInfoWcdma = (CellInfoWcdma)paramObject;
      if (this.mCellIdentityWcdma.equals(cellInfoWcdma.mCellIdentityWcdma)) {
        paramObject = this.mCellSignalStrengthWcdma;
        CellSignalStrengthWcdma cellSignalStrengthWcdma = cellInfoWcdma.mCellSignalStrengthWcdma;
        bool = paramObject.equals(cellSignalStrengthWcdma);
        if (bool)
          bool1 = true; 
      } 
      return bool1;
    } catch (ClassCastException classCastException) {
      return false;
    } 
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append("CellInfoWcdma:{");
    stringBuffer.append(super.toString());
    stringBuffer.append(" ");
    stringBuffer.append(this.mCellIdentityWcdma);
    stringBuffer.append(" ");
    stringBuffer.append(this.mCellSignalStrengthWcdma);
    stringBuffer.append("}");
    return stringBuffer.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcel(paramParcel, paramInt, 4);
    this.mCellIdentityWcdma.writeToParcel(paramParcel, paramInt);
    this.mCellSignalStrengthWcdma.writeToParcel(paramParcel, paramInt);
  }
  
  private CellInfoWcdma(Parcel paramParcel) {
    super(paramParcel);
    this.mCellIdentityWcdma = (CellIdentityWcdma)CellIdentityWcdma.CREATOR.createFromParcel(paramParcel);
    this.mCellSignalStrengthWcdma = (CellSignalStrengthWcdma)CellSignalStrengthWcdma.CREATOR.createFromParcel(paramParcel);
  }
  
  public static final Parcelable.Creator<CellInfoWcdma> CREATOR = (Parcelable.Creator<CellInfoWcdma>)new Object();
  
  private static final boolean DBG = false;
  
  private static final String LOG_TAG = "CellInfoWcdma";
  
  private CellIdentityWcdma mCellIdentityWcdma;
  
  private CellSignalStrengthWcdma mCellSignalStrengthWcdma;
  
  protected static CellInfoWcdma createFromParcelBody(Parcel paramParcel) {
    return new CellInfoWcdma(paramParcel);
  }
  
  private static void log(String paramString) {
    Rlog.w("CellInfoWcdma", paramString);
  }
}
