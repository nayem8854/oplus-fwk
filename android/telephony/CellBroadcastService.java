package android.telephony;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteCallback;
import android.telephony.cdma.CdmaSmsCbProgramData;
import java.util.List;
import java.util.function.Consumer;

@SystemApi
public abstract class CellBroadcastService extends Service {
  public static final String CELL_BROADCAST_SERVICE_INTERFACE = "android.telephony.CellBroadcastService";
  
  private final ICellBroadcastService.Stub mStubWrapper = new ICellBroadcastServiceWrapper();
  
  public abstract CharSequence getCellBroadcastAreaInfo(int paramInt);
  
  public IBinder onBind(Intent paramIntent) {
    return (IBinder)this.mStubWrapper;
  }
  
  public abstract void onCdmaCellBroadcastSms(int paramInt1, byte[] paramArrayOfbyte, int paramInt2);
  
  public abstract void onCdmaScpMessage(int paramInt, List<CdmaSmsCbProgramData> paramList, String paramString, Consumer<Bundle> paramConsumer);
  
  public abstract void onGsmCellBroadcastSms(int paramInt, byte[] paramArrayOfbyte);
  
  public class ICellBroadcastServiceWrapper extends ICellBroadcastService.Stub {
    final CellBroadcastService this$0;
    
    public void handleGsmCellBroadcastSms(int param1Int, byte[] param1ArrayOfbyte) {
      CellBroadcastService.this.onGsmCellBroadcastSms(param1Int, param1ArrayOfbyte);
    }
    
    public void handleCdmaCellBroadcastSms(int param1Int1, byte[] param1ArrayOfbyte, int param1Int2) {
      CellBroadcastService.this.onCdmaCellBroadcastSms(param1Int1, param1ArrayOfbyte, param1Int2);
    }
    
    public void handleCdmaScpMessage(int param1Int, List<CdmaSmsCbProgramData> param1List, String param1String, RemoteCallback param1RemoteCallback) {
      _$$Lambda$CellBroadcastService$ICellBroadcastServiceWrapper$NEnRk_Dx_nskeAgBu1oOuDgKlNM _$$Lambda$CellBroadcastService$ICellBroadcastServiceWrapper$NEnRk_Dx_nskeAgBu1oOuDgKlNM = new _$$Lambda$CellBroadcastService$ICellBroadcastServiceWrapper$NEnRk_Dx_nskeAgBu1oOuDgKlNM(param1RemoteCallback);
      CellBroadcastService.this.onCdmaScpMessage(param1Int, param1List, param1String, _$$Lambda$CellBroadcastService$ICellBroadcastServiceWrapper$NEnRk_Dx_nskeAgBu1oOuDgKlNM);
    }
    
    public CharSequence getCellBroadcastAreaInfo(int param1Int) {
      return CellBroadcastService.this.getCellBroadcastAreaInfo(param1Int);
    }
  }
}
