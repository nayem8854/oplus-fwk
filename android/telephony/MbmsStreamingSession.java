package android.telephony;

import android.annotation.SystemApi;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.telephony.mbms.InternalStreamingServiceCallback;
import android.telephony.mbms.InternalStreamingSessionCallback;
import android.telephony.mbms.MbmsStreamingSessionCallback;
import android.telephony.mbms.MbmsUtils;
import android.telephony.mbms.StreamingService;
import android.telephony.mbms.StreamingServiceCallback;
import android.telephony.mbms.StreamingServiceInfo;
import android.telephony.mbms.vendor.IMbmsStreamingService;
import android.util.ArraySet;
import android.util.Log;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class MbmsStreamingSession implements AutoCloseable {
  private static AtomicBoolean sIsInitialized = new AtomicBoolean(false);
  
  private AtomicReference<IMbmsStreamingService> mService = new AtomicReference<>(null);
  
  private IBinder.DeathRecipient mDeathRecipient = (IBinder.DeathRecipient)new Object(this);
  
  private Set<StreamingService> mKnownActiveStreamingServices = new ArraySet<>();
  
  private int mSubscriptionId = -1;
  
  private static final String LOG_TAG = "MbmsStreamingSession";
  
  @SystemApi
  public static final String MBMS_STREAMING_SERVICE_ACTION = "android.telephony.action.EmbmsStreaming";
  
  public static final String MBMS_STREAMING_SERVICE_OVERRIDE_METADATA = "mbms-streaming-service-override";
  
  private final Context mContext;
  
  private InternalStreamingSessionCallback mInternalCallback;
  
  private ServiceConnection mServiceConnection;
  
  private MbmsStreamingSession(Context paramContext, Executor paramExecutor, int paramInt, MbmsStreamingSessionCallback paramMbmsStreamingSessionCallback) {
    this.mContext = paramContext;
    this.mSubscriptionId = paramInt;
    this.mInternalCallback = new InternalStreamingSessionCallback(paramMbmsStreamingSessionCallback, paramExecutor);
  }
  
  public static MbmsStreamingSession create(Context paramContext, Executor paramExecutor, final int result, final MbmsStreamingSessionCallback callback) {
    if (sIsInitialized.compareAndSet(false, true)) {
      MbmsStreamingSession mbmsStreamingSession = new MbmsStreamingSession(paramContext, paramExecutor, result, callback);
      result = mbmsStreamingSession.bindAndInitialize();
      if (result != 0) {
        sIsInitialized.set(false);
        paramExecutor.execute(new Runnable() {
              final MbmsStreamingSessionCallback val$callback;
              
              final int val$result;
              
              public void run() {
                callback.onError(result, null);
              }
            });
        return null;
      } 
      return mbmsStreamingSession;
    } 
    throw new IllegalStateException("Cannot create two instances of MbmsStreamingSession");
  }
  
  public static MbmsStreamingSession create(Context paramContext, Executor paramExecutor, MbmsStreamingSessionCallback paramMbmsStreamingSessionCallback) {
    return create(paramContext, paramExecutor, SubscriptionManager.getDefaultSubscriptionId(), paramMbmsStreamingSessionCallback);
  }
  
  public void close() {
    try {
      IMbmsStreamingService iMbmsStreamingService = this.mService.get();
      if (iMbmsStreamingService == null || this.mServiceConnection == null)
        return; 
      iMbmsStreamingService.dispose(this.mSubscriptionId);
      for (StreamingService streamingService : this.mKnownActiveStreamingServices)
        streamingService.getCallback().stop(); 
      this.mKnownActiveStreamingServices.clear();
      this.mContext.unbindService(this.mServiceConnection);
    } catch (RemoteException remoteException) {
    
    } finally {
      this.mService.set(null);
      sIsInitialized.set(false);
      this.mServiceConnection = null;
      this.mInternalCallback.stop();
    } 
  }
  
  public void requestUpdateStreamingServices(List<String> paramList) {
    IMbmsStreamingService iMbmsStreamingService = this.mService.get();
    if (iMbmsStreamingService != null) {
      try {
        int i = iMbmsStreamingService.requestUpdateStreamingServices(this.mSubscriptionId, paramList);
        if (i != -1) {
          if (i != 0)
            sendErrorToApp(i, null); 
        } else {
          close();
          IllegalStateException illegalStateException = new IllegalStateException();
          this("Middleware must not return an unknown error code");
          throw illegalStateException;
        } 
      } catch (RemoteException remoteException) {
        Log.w("MbmsStreamingSession", "Remote process died");
        this.mService.set(null);
        sIsInitialized.set(false);
        sendErrorToApp(3, null);
      } 
      return;
    } 
    throw new IllegalStateException("Middleware not yet bound");
  }
  
  public StreamingService startStreaming(StreamingServiceInfo paramStreamingServiceInfo, Executor paramExecutor, StreamingServiceCallback paramStreamingServiceCallback) {
    IMbmsStreamingService iMbmsStreamingService = this.mService.get();
    if (iMbmsStreamingService != null) {
      InternalStreamingServiceCallback internalStreamingServiceCallback = new InternalStreamingServiceCallback(paramStreamingServiceCallback, paramExecutor);
      StreamingService streamingService = new StreamingService(this.mSubscriptionId, iMbmsStreamingService, this, paramStreamingServiceInfo, internalStreamingServiceCallback);
      this.mKnownActiveStreamingServices.add(streamingService);
      try {
        int i = this.mSubscriptionId;
        String str = paramStreamingServiceInfo.getServiceId();
        i = iMbmsStreamingService.startStreaming(i, str, internalStreamingServiceCallback);
        if (i != -1) {
          if (i != 0) {
            sendErrorToApp(i, null);
            return null;
          } 
          return streamingService;
        } 
        close();
        IllegalStateException illegalStateException = new IllegalStateException();
        this("Middleware must not return an unknown error code");
        throw illegalStateException;
      } catch (RemoteException remoteException) {
        Log.w("MbmsStreamingSession", "Remote process died");
        this.mService.set(null);
        sIsInitialized.set(false);
        sendErrorToApp(3, null);
        return null;
      } 
    } 
    throw new IllegalStateException("Middleware not yet bound");
  }
  
  public void onStreamingServiceStopped(StreamingService paramStreamingService) {
    this.mKnownActiveStreamingServices.remove(paramStreamingService);
  }
  
  private int bindAndInitialize() {
    Object object = new Object(this);
    return MbmsUtils.startBinding(this.mContext, "android.telephony.action.EmbmsStreaming", (ServiceConnection)object);
  }
  
  private void sendErrorToApp(int paramInt, String paramString) {
    try {
      this.mInternalCallback.onError(paramInt, paramString);
    } catch (RemoteException remoteException) {}
  }
}
