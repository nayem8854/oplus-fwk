package android.telephony;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class NetworkRegistrationInfo implements Parcelable {
  private NetworkRegistrationInfo(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean, List<Integer> paramList, CellIdentity paramCellIdentity, String paramString) {
    this.mDomain = paramInt1;
    this.mTransportType = paramInt2;
    this.mRegistrationState = paramInt3;
    if (paramInt3 == 5) {
      paramInt1 = 1;
    } else {
      paramInt1 = 0;
    } 
    this.mRoamingType = paramInt1;
    this.mAccessNetworkTechnology = paramInt4;
    this.mRejectCause = paramInt5;
    if (paramList != null) {
      paramList = new ArrayList<>(paramList);
    } else {
      paramList = new ArrayList<>();
    } 
    this.mAvailableServices = (ArrayList<Integer>)paramList;
    this.mCellIdentity = paramCellIdentity;
    this.mEmergencyOnly = paramBoolean;
    this.mNrState = 0;
    this.mRplmn = paramString;
  }
  
  public NetworkRegistrationInfo(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean1, List<Integer> paramList, CellIdentity paramCellIdentity, String paramString, boolean paramBoolean2, int paramInt6, int paramInt7, int paramInt8) {
    this(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramBoolean1, paramList, paramCellIdentity, paramString);
    this.mVoiceSpecificInfo = new VoiceSpecificRegistrationInfo(paramBoolean2, paramInt6, paramInt7, paramInt8);
  }
  
  public NetworkRegistrationInfo(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean1, List<Integer> paramList, CellIdentity paramCellIdentity, String paramString, int paramInt6, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, LteVopsSupportInfo paramLteVopsSupportInfo, boolean paramBoolean5) {
    this(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramBoolean1, paramList, paramCellIdentity, paramString);
    this.mDataSpecificInfo = new DataSpecificRegistrationInfo(paramInt6, paramBoolean2, paramBoolean3, paramBoolean4, paramLteVopsSupportInfo, paramBoolean5);
    updateNrState();
  }
  
  private NetworkRegistrationInfo(Parcel paramParcel) {
    this.mDomain = paramParcel.readInt();
    this.mTransportType = paramParcel.readInt();
    this.mRegistrationState = paramParcel.readInt();
    this.mRoamingType = paramParcel.readInt();
    this.mAccessNetworkTechnology = paramParcel.readInt();
    this.mRejectCause = paramParcel.readInt();
    this.mEmergencyOnly = paramParcel.readBoolean();
    ArrayList<Integer> arrayList = new ArrayList();
    paramParcel.readList(arrayList, Integer.class.getClassLoader());
    this.mCellIdentity = (CellIdentity)paramParcel.readParcelable(CellIdentity.class.getClassLoader());
    ClassLoader classLoader = VoiceSpecificRegistrationInfo.class.getClassLoader();
    this.mVoiceSpecificInfo = (VoiceSpecificRegistrationInfo)paramParcel.readParcelable(classLoader);
    classLoader = DataSpecificRegistrationInfo.class.getClassLoader();
    this.mDataSpecificInfo = (DataSpecificRegistrationInfo)paramParcel.readParcelable(classLoader);
    this.mNrState = paramParcel.readInt();
    this.mRplmn = paramParcel.readString();
  }
  
  public NetworkRegistrationInfo(NetworkRegistrationInfo paramNetworkRegistrationInfo) {
    this.mDomain = paramNetworkRegistrationInfo.mDomain;
    this.mTransportType = paramNetworkRegistrationInfo.mTransportType;
    this.mRegistrationState = paramNetworkRegistrationInfo.mRegistrationState;
    this.mRoamingType = paramNetworkRegistrationInfo.mRoamingType;
    this.mAccessNetworkTechnology = paramNetworkRegistrationInfo.mAccessNetworkTechnology;
    this.mRejectCause = paramNetworkRegistrationInfo.mRejectCause;
    this.mEmergencyOnly = paramNetworkRegistrationInfo.mEmergencyOnly;
    this.mAvailableServices = new ArrayList<>(paramNetworkRegistrationInfo.mAvailableServices);
    if (paramNetworkRegistrationInfo.mCellIdentity != null) {
      Parcel parcel = Parcel.obtain();
      paramNetworkRegistrationInfo.mCellIdentity.writeToParcel(parcel, 0);
      parcel.setDataPosition(0);
      this.mCellIdentity = (CellIdentity)CellIdentity.CREATOR.createFromParcel(parcel);
    } 
    if (paramNetworkRegistrationInfo.mVoiceSpecificInfo != null)
      this.mVoiceSpecificInfo = new VoiceSpecificRegistrationInfo(paramNetworkRegistrationInfo.mVoiceSpecificInfo); 
    if (paramNetworkRegistrationInfo.mDataSpecificInfo != null)
      this.mDataSpecificInfo = new DataSpecificRegistrationInfo(paramNetworkRegistrationInfo.mDataSpecificInfo); 
    this.mNrState = paramNetworkRegistrationInfo.mNrState;
    this.mRplmn = paramNetworkRegistrationInfo.mRplmn;
  }
  
  public int getTransportType() {
    return this.mTransportType;
  }
  
  public int getDomain() {
    return this.mDomain;
  }
  
  public int getNrState() {
    return this.mNrState;
  }
  
  public void setNrState(int paramInt) {
    this.mNrState = paramInt;
  }
  
  @SystemApi
  public int getRegistrationState() {
    return this.mRegistrationState;
  }
  
  public boolean isRegistered() {
    int i = this.mRegistrationState;
    boolean bool1 = true, bool2 = bool1;
    if (i != 1)
      if (i == 5) {
        bool2 = bool1;
      } else {
        bool2 = false;
      }  
    return bool2;
  }
  
  public boolean isSearching() {
    boolean bool;
    if (this.mRegistrationState == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String getRegisteredPlmn() {
    return this.mRplmn;
  }
  
  public boolean isRoaming() {
    boolean bool;
    if (this.mRoamingType != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isInService() {
    int i = this.mRegistrationState;
    boolean bool1 = true, bool2 = bool1;
    if (i != 1)
      if (i == 5) {
        bool2 = bool1;
      } else {
        bool2 = false;
      }  
    return bool2;
  }
  
  public void setRoamingType(int paramInt) {
    this.mRoamingType = paramInt;
  }
  
  @SystemApi
  public int getRoamingType() {
    return this.mRoamingType;
  }
  
  @SystemApi
  public boolean isEmergencyEnabled() {
    return this.mEmergencyOnly;
  }
  
  public List<Integer> getAvailableServices() {
    return Collections.unmodifiableList(this.mAvailableServices);
  }
  
  public int getAccessNetworkTechnology() {
    return this.mAccessNetworkTechnology;
  }
  
  public void setAccessNetworkTechnology(int paramInt) {
    int i = paramInt;
    if (paramInt == 19) {
      paramInt = 13;
      DataSpecificRegistrationInfo dataSpecificRegistrationInfo = this.mDataSpecificInfo;
      i = paramInt;
      if (dataSpecificRegistrationInfo != null) {
        dataSpecificRegistrationInfo.setIsUsingCarrierAggregation(true);
        i = paramInt;
      } 
    } 
    this.mAccessNetworkTechnology = i;
  }
  
  @SystemApi
  public int getRejectCause() {
    return this.mRejectCause;
  }
  
  public CellIdentity getCellIdentity() {
    return this.mCellIdentity;
  }
  
  public VoiceSpecificRegistrationInfo getVoiceSpecificInfo() {
    return this.mVoiceSpecificInfo;
  }
  
  @SystemApi
  public DataSpecificRegistrationInfo getDataSpecificInfo() {
    return this.mDataSpecificInfo;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static String serviceTypeToString(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 3) {
          if (paramInt != 4) {
            if (paramInt != 5) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Unknown service type ");
              stringBuilder.append(paramInt);
              return stringBuilder.toString();
            } 
            return "EMERGENCY";
          } 
          return "VIDEO";
        } 
        return "SMS";
      } 
      return "DATA";
    } 
    return "VOICE";
  }
  
  public static String registrationStateToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt != 4) {
              if (paramInt != 5) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Unknown reg state ");
                stringBuilder.append(paramInt);
                return stringBuilder.toString();
              } 
              return "ROAMING";
            } 
            return "UNKNOWN";
          } 
          return "DENIED";
        } 
        return "NOT_REG_SEARCHING";
      } 
      return "HOME";
    } 
    return "NOT_REG_OR_SEARCHING";
  }
  
  private static String nrStateToString(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 3)
          return "NONE"; 
        return "CONNECTED";
      } 
      return "NOT_RESTRICTED";
    } 
    return "RESTRICTED";
  }
  
  static String domainToString(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 3)
          return "UNKNOWN"; 
        return "CS_PS";
      } 
      return "PS";
    } 
    return "CS";
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder("NetworkRegistrationInfo{");
    stringBuilder1.append(" domain=");
    stringBuilder1.append(domainToString(this.mDomain));
    stringBuilder1.append(" transportType=");
    int i = this.mTransportType;
    String str = AccessNetworkConstants.transportTypeToString(i);
    stringBuilder1.append(str);
    stringBuilder1.append(" registrationState=");
    stringBuilder1.append(registrationStateToString(this.mRegistrationState));
    stringBuilder1.append(" roamingType=");
    stringBuilder1.append(ServiceState.roamingTypeToString(this.mRoamingType));
    stringBuilder1.append(" accessNetworkTechnology=");
    i = this.mAccessNetworkTechnology;
    stringBuilder1.append(TelephonyManager.getNetworkTypeName(i));
    stringBuilder1.append(" rejectCause=");
    stringBuilder1.append(this.mRejectCause);
    stringBuilder1.append(" emergencyEnabled=");
    stringBuilder1.append(this.mEmergencyOnly);
    stringBuilder1.append(" availableServices=");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("[");
    ArrayList<Integer> arrayList = this.mAvailableServices;
    if (arrayList != null) {
      Stream<CharSequence> stream = arrayList.stream().map((Function)_$$Lambda$NetworkRegistrationInfo$1JuZmO5PoYGZY8bHhZYwvmqwOB0.INSTANCE);
      String str1 = stream.collect(Collectors.joining(","));
    } else {
      arrayList = null;
    } 
    stringBuilder2.append((String)arrayList);
    stringBuilder2.append("]");
    null = stringBuilder2.toString();
    stringBuilder1.append(null);
    stringBuilder1.append(" cellIdentity=");
    stringBuilder1.append(this.mCellIdentity);
    stringBuilder1.append(" voiceSpecificInfo=");
    stringBuilder1.append(this.mVoiceSpecificInfo);
    stringBuilder1.append(" dataSpecificInfo=");
    stringBuilder1.append(this.mDataSpecificInfo);
    stringBuilder1.append(" nrState=");
    stringBuilder1.append(nrStateToString(this.mNrState));
    stringBuilder1.append(" rRplmn=");
    stringBuilder1.append(this.mRplmn);
    stringBuilder1.append("}");
    return stringBuilder1.toString();
  }
  
  public int hashCode() {
    int i = this.mDomain, j = this.mTransportType, k = this.mRegistrationState, m = this.mRoamingType, n = this.mAccessNetworkTechnology;
    int i1 = this.mRejectCause;
    boolean bool = this.mEmergencyOnly;
    ArrayList<Integer> arrayList = this.mAvailableServices;
    CellIdentity cellIdentity = this.mCellIdentity;
    VoiceSpecificRegistrationInfo voiceSpecificRegistrationInfo = this.mVoiceSpecificInfo;
    DataSpecificRegistrationInfo dataSpecificRegistrationInfo = this.mDataSpecificInfo;
    int i2 = this.mNrState;
    String str = this.mRplmn;
    return Objects.hash(new Object[] { 
          Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), Boolean.valueOf(bool), arrayList, cellIdentity, voiceSpecificRegistrationInfo, 
          dataSpecificRegistrationInfo, Integer.valueOf(i2), str });
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof NetworkRegistrationInfo))
      return false; 
    paramObject = paramObject;
    if (this.mDomain == ((NetworkRegistrationInfo)paramObject).mDomain && this.mTransportType == ((NetworkRegistrationInfo)paramObject).mTransportType && this.mRegistrationState == ((NetworkRegistrationInfo)paramObject).mRegistrationState && this.mRoamingType == ((NetworkRegistrationInfo)paramObject).mRoamingType && this.mAccessNetworkTechnology == ((NetworkRegistrationInfo)paramObject).mAccessNetworkTechnology && this.mRejectCause == ((NetworkRegistrationInfo)paramObject).mRejectCause && this.mEmergencyOnly == ((NetworkRegistrationInfo)paramObject).mEmergencyOnly) {
      ArrayList<Integer> arrayList1 = this.mAvailableServices, arrayList2 = ((NetworkRegistrationInfo)paramObject).mAvailableServices;
      if (arrayList1.equals(arrayList2)) {
        CellIdentity cellIdentity1 = this.mCellIdentity, cellIdentity2 = ((NetworkRegistrationInfo)paramObject).mCellIdentity;
        if (Objects.equals(cellIdentity1, cellIdentity2)) {
          VoiceSpecificRegistrationInfo voiceSpecificRegistrationInfo1 = this.mVoiceSpecificInfo, voiceSpecificRegistrationInfo2 = ((NetworkRegistrationInfo)paramObject).mVoiceSpecificInfo;
          if (Objects.equals(voiceSpecificRegistrationInfo1, voiceSpecificRegistrationInfo2)) {
            DataSpecificRegistrationInfo dataSpecificRegistrationInfo2 = this.mDataSpecificInfo, dataSpecificRegistrationInfo1 = ((NetworkRegistrationInfo)paramObject).mDataSpecificInfo;
            if (Objects.equals(dataSpecificRegistrationInfo2, dataSpecificRegistrationInfo1)) {
              String str2 = this.mRplmn, str1 = ((NetworkRegistrationInfo)paramObject).mRplmn;
              if (TextUtils.equals(str2, str1) && this.mNrState == ((NetworkRegistrationInfo)paramObject).mNrState)
                return null; 
            } 
          } 
        } 
      } 
    } 
    return false;
  }
  
  @SystemApi
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mDomain);
    paramParcel.writeInt(this.mTransportType);
    paramParcel.writeInt(this.mRegistrationState);
    paramParcel.writeInt(this.mRoamingType);
    paramParcel.writeInt(this.mAccessNetworkTechnology);
    paramParcel.writeInt(this.mRejectCause);
    paramParcel.writeBoolean(this.mEmergencyOnly);
    paramParcel.writeList(this.mAvailableServices);
    paramParcel.writeParcelable(this.mCellIdentity, 0);
    paramParcel.writeParcelable(this.mVoiceSpecificInfo, 0);
    paramParcel.writeParcelable(this.mDataSpecificInfo, 0);
    paramParcel.writeInt(this.mNrState);
    paramParcel.writeString(this.mRplmn);
  }
  
  public void updateNrState() {
    this.mNrState = 0;
    DataSpecificRegistrationInfo dataSpecificRegistrationInfo = this.mDataSpecificInfo;
    if (dataSpecificRegistrationInfo != null && dataSpecificRegistrationInfo.isEnDcAvailable)
      if (!this.mDataSpecificInfo.isDcNrRestricted && this.mDataSpecificInfo.isNrAvailable) {
        this.mNrState = 2;
      } else {
        this.mNrState = 1;
      }  
  }
  
  public static final Parcelable.Creator<NetworkRegistrationInfo> CREATOR = new Parcelable.Creator<NetworkRegistrationInfo>() {
      public NetworkRegistrationInfo createFromParcel(Parcel param1Parcel) {
        return new NetworkRegistrationInfo(param1Parcel);
      }
      
      public NetworkRegistrationInfo[] newArray(int param1Int) {
        return new NetworkRegistrationInfo[param1Int];
      }
    };
  
  public static final int DOMAIN_CS = 1;
  
  public static final int DOMAIN_CS_PS = 3;
  
  public static final int DOMAIN_PS = 2;
  
  public static final int DOMAIN_UNKNOWN = 0;
  
  public static final int NR_STATE_CONNECTED = 3;
  
  public static final int NR_STATE_NONE = 0;
  
  public static final int NR_STATE_NOT_RESTRICTED = 2;
  
  public static final int NR_STATE_RESTRICTED = 1;
  
  @SystemApi
  public static final int REGISTRATION_STATE_DENIED = 3;
  
  @SystemApi
  public static final int REGISTRATION_STATE_HOME = 1;
  
  @SystemApi
  public static final int REGISTRATION_STATE_NOT_REGISTERED_OR_SEARCHING = 0;
  
  @SystemApi
  public static final int REGISTRATION_STATE_NOT_REGISTERED_SEARCHING = 2;
  
  @SystemApi
  public static final int REGISTRATION_STATE_ROAMING = 5;
  
  @SystemApi
  public static final int REGISTRATION_STATE_UNKNOWN = 4;
  
  public static final int SERVICE_TYPE_DATA = 2;
  
  public static final int SERVICE_TYPE_EMERGENCY = 5;
  
  public static final int SERVICE_TYPE_SMS = 3;
  
  public static final int SERVICE_TYPE_UNKNOWN = 0;
  
  public static final int SERVICE_TYPE_VIDEO = 4;
  
  public static final int SERVICE_TYPE_VOICE = 1;
  
  private int mAccessNetworkTechnology;
  
  private final ArrayList<Integer> mAvailableServices;
  
  private CellIdentity mCellIdentity;
  
  private DataSpecificRegistrationInfo mDataSpecificInfo;
  
  private final int mDomain;
  
  private final boolean mEmergencyOnly;
  
  private int mNrState;
  
  private final int mRegistrationState;
  
  private final int mRejectCause;
  
  private int mRoamingType;
  
  private String mRplmn;
  
  private final int mTransportType;
  
  private VoiceSpecificRegistrationInfo mVoiceSpecificInfo;
  
  public NetworkRegistrationInfo sanitizeLocationInfo() {
    NetworkRegistrationInfo networkRegistrationInfo = copy();
    networkRegistrationInfo.mCellIdentity = null;
    return networkRegistrationInfo;
  }
  
  private NetworkRegistrationInfo copy() {
    Parcel parcel = Parcel.obtain();
    writeToParcel(parcel, 0);
    parcel.setDataPosition(0);
    NetworkRegistrationInfo networkRegistrationInfo = new NetworkRegistrationInfo(parcel);
    parcel.recycle();
    return networkRegistrationInfo;
  }
  
  @SystemApi
  class Builder {
    private int mAccessNetworkTechnology;
    
    private List<Integer> mAvailableServices;
    
    private CellIdentity mCellIdentity;
    
    private int mDomain;
    
    private boolean mEmergencyOnly;
    
    private int mRegistrationState;
    
    private int mRejectCause;
    
    private String mRplmn = "";
    
    private int mTransportType;
    
    public Builder setDomain(int param1Int) {
      this.mDomain = param1Int;
      return this;
    }
    
    public Builder setTransportType(int param1Int) {
      this.mTransportType = param1Int;
      return this;
    }
    
    public Builder setRegistrationState(int param1Int) {
      this.mRegistrationState = param1Int;
      return this;
    }
    
    public Builder setAccessNetworkTechnology(int param1Int) {
      this.mAccessNetworkTechnology = param1Int;
      return this;
    }
    
    public Builder setRejectCause(int param1Int) {
      this.mRejectCause = param1Int;
      return this;
    }
    
    @SystemApi
    public Builder setEmergencyOnly(boolean param1Boolean) {
      this.mEmergencyOnly = param1Boolean;
      return this;
    }
    
    @SystemApi
    public Builder setAvailableServices(List<Integer> param1List) {
      this.mAvailableServices = param1List;
      return this;
    }
    
    @SystemApi
    public Builder setCellIdentity(CellIdentity param1CellIdentity) {
      this.mCellIdentity = param1CellIdentity;
      return this;
    }
    
    public Builder setRegisteredPlmn(String param1String) {
      this.mRplmn = param1String;
      return this;
    }
    
    @SystemApi
    public NetworkRegistrationInfo build() {
      return new NetworkRegistrationInfo(this.mDomain, this.mTransportType, this.mRegistrationState, this.mAccessNetworkTechnology, this.mRejectCause, this.mEmergencyOnly, this.mAvailableServices, this.mCellIdentity, this.mRplmn);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Domain implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class NRState implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class RegistrationState implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ServiceType implements Annotation {}
}
