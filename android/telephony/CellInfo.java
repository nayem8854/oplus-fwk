package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;

public abstract class CellInfo implements Parcelable {
  public static final int CONNECTION_NONE = 0;
  
  public static final int CONNECTION_PRIMARY_SERVING = 1;
  
  public static final int CONNECTION_SECONDARY_SERVING = 2;
  
  public static final int CONNECTION_UNKNOWN = 2147483647;
  
  protected CellInfo() {
    this.mRegistered = false;
    this.mTimeStamp = Long.MAX_VALUE;
    this.mCellConnectionStatus = 0;
  }
  
  protected CellInfo(CellInfo paramCellInfo) {
    this.mRegistered = paramCellInfo.mRegistered;
    this.mTimeStamp = paramCellInfo.mTimeStamp;
    this.mCellConnectionStatus = paramCellInfo.mCellConnectionStatus;
  }
  
  public boolean isRegistered() {
    return this.mRegistered;
  }
  
  public void setRegistered(boolean paramBoolean) {
    this.mRegistered = paramBoolean;
  }
  
  public long getTimestampMillis() {
    return this.mTimeStamp / 1000000L;
  }
  
  @Deprecated
  public long getTimeStamp() {
    return this.mTimeStamp;
  }
  
  public void setTimeStamp(long paramLong) {
    this.mTimeStamp = paramLong;
  }
  
  public CellInfo sanitizeLocationInfo() {
    return null;
  }
  
  public int getCellConnectionStatus() {
    return this.mCellConnectionStatus;
  }
  
  public void setCellConnectionStatus(int paramInt) {
    this.mCellConnectionStatus = paramInt;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mCellConnectionStatus), Boolean.valueOf(this.mRegistered), Long.valueOf(this.mTimeStamp) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof CellInfo))
      return false; 
    paramObject = paramObject;
    if (this.mCellConnectionStatus != ((CellInfo)paramObject).mCellConnectionStatus || this.mRegistered != ((CellInfo)paramObject).mRegistered || this.mTimeStamp != ((CellInfo)paramObject).mTimeStamp)
      bool = false; 
    return bool;
  }
  
  public String toString() {
    String str;
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append("mRegistered=");
    if (this.mRegistered) {
      str = "YES";
    } else {
      str = "NO";
    } 
    stringBuffer.append(str);
    stringBuffer.append(" mTimeStamp=");
    stringBuffer.append(this.mTimeStamp);
    stringBuffer.append("ns");
    stringBuffer.append(" mCellConnectionStatus=");
    stringBuffer.append(this.mCellConnectionStatus);
    return stringBuffer.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  protected void writeToParcel(Parcel paramParcel, int paramInt1, int paramInt2) {
    paramParcel.writeInt(paramInt2);
    paramParcel.writeInt(this.mRegistered);
    paramParcel.writeLong(this.mTimeStamp);
    paramParcel.writeInt(this.mCellConnectionStatus);
  }
  
  protected CellInfo(Parcel paramParcel) {
    int i = paramParcel.readInt();
    boolean bool = true;
    if (i != 1)
      bool = false; 
    this.mRegistered = bool;
    this.mTimeStamp = paramParcel.readLong();
    this.mCellConnectionStatus = paramParcel.readInt();
  }
  
  public static final Parcelable.Creator<CellInfo> CREATOR = new Parcelable.Creator<CellInfo>() {
      public CellInfo createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        switch (i) {
          default:
            throw new RuntimeException("Bad CellInfo Parcel");
          case 6:
            return CellInfoNr.createFromParcelBody(param1Parcel);
          case 5:
            return CellInfoTdscdma.createFromParcelBody(param1Parcel);
          case 4:
            return CellInfoWcdma.createFromParcelBody(param1Parcel);
          case 3:
            return CellInfoLte.createFromParcelBody(param1Parcel);
          case 2:
            return CellInfoCdma.createFromParcelBody(param1Parcel);
          case 1:
            break;
        } 
        return CellInfoGsm.createFromParcelBody(param1Parcel);
      }
      
      public CellInfo[] newArray(int param1Int) {
        return new CellInfo[param1Int];
      }
    };
  
  public static final int TIMESTAMP_TYPE_ANTENNA = 1;
  
  public static final int TIMESTAMP_TYPE_JAVA_RIL = 4;
  
  public static final int TIMESTAMP_TYPE_MODEM = 2;
  
  public static final int TIMESTAMP_TYPE_OEM_RIL = 3;
  
  public static final int TIMESTAMP_TYPE_UNKNOWN = 0;
  
  public static final int TYPE_CDMA = 2;
  
  public static final int TYPE_GSM = 1;
  
  public static final int TYPE_LTE = 3;
  
  public static final int TYPE_NR = 6;
  
  public static final int TYPE_TDSCDMA = 5;
  
  public static final int TYPE_UNKNOWN = 0;
  
  public static final int TYPE_WCDMA = 4;
  
  public static final int UNAVAILABLE = 2147483647;
  
  public static final long UNAVAILABLE_LONG = 9223372036854775807L;
  
  private int mCellConnectionStatus;
  
  private boolean mRegistered;
  
  private long mTimeStamp;
  
  protected CellInfo(android.hardware.radio.V1_0.CellInfo paramCellInfo) {
    this.mRegistered = paramCellInfo.registered;
    this.mTimeStamp = paramCellInfo.timeStamp;
    this.mCellConnectionStatus = Integer.MAX_VALUE;
  }
  
  protected CellInfo(android.hardware.radio.V1_2.CellInfo paramCellInfo) {
    this.mRegistered = paramCellInfo.registered;
    this.mTimeStamp = paramCellInfo.timeStamp;
    this.mCellConnectionStatus = paramCellInfo.connectionStatus;
  }
  
  protected CellInfo(android.hardware.radio.V1_4.CellInfo paramCellInfo, long paramLong) {
    this.mRegistered = paramCellInfo.isRegistered;
    this.mTimeStamp = paramLong;
    this.mCellConnectionStatus = paramCellInfo.connectionStatus;
  }
  
  protected CellInfo(android.hardware.radio.V1_5.CellInfo paramCellInfo, long paramLong) {
    this.mRegistered = paramCellInfo.registered;
    this.mTimeStamp = paramLong;
    this.mCellConnectionStatus = paramCellInfo.connectionStatus;
  }
  
  public static CellInfo create(android.hardware.radio.V1_0.CellInfo paramCellInfo) {
    if (paramCellInfo == null)
      return null; 
    int i = paramCellInfo.cellInfoType;
    if (i != 1) {
      if (i != 2) {
        if (i != 3) {
          if (i != 4) {
            if (i != 5)
              return null; 
            return new CellInfoTdscdma(paramCellInfo);
          } 
          return new CellInfoWcdma(paramCellInfo);
        } 
        return new CellInfoLte(paramCellInfo);
      } 
      return new CellInfoCdma(paramCellInfo);
    } 
    return new CellInfoGsm(paramCellInfo);
  }
  
  public static CellInfo create(android.hardware.radio.V1_2.CellInfo paramCellInfo) {
    if (paramCellInfo == null)
      return null; 
    int i = paramCellInfo.cellInfoType;
    if (i != 1) {
      if (i != 2) {
        if (i != 3) {
          if (i != 4) {
            if (i != 5)
              return null; 
            return new CellInfoTdscdma(paramCellInfo);
          } 
          return new CellInfoWcdma(paramCellInfo);
        } 
        return new CellInfoLte(paramCellInfo);
      } 
      return new CellInfoCdma(paramCellInfo);
    } 
    return new CellInfoGsm(paramCellInfo);
  }
  
  public static CellInfo create(android.hardware.radio.V1_4.CellInfo paramCellInfo, long paramLong) {
    if (paramCellInfo == null)
      return null; 
    byte b = paramCellInfo.info.getDiscriminator();
    if (b != 0) {
      if (b != 1) {
        if (b != 2) {
          if (b != 3) {
            if (b != 4) {
              if (b != 5)
                return null; 
              return new CellInfoNr(paramCellInfo, paramLong);
            } 
            return new CellInfoLte(paramCellInfo, paramLong);
          } 
          return new CellInfoTdscdma(paramCellInfo, paramLong);
        } 
        return new CellInfoWcdma(paramCellInfo, paramLong);
      } 
      return new CellInfoCdma(paramCellInfo, paramLong);
    } 
    return new CellInfoGsm(paramCellInfo, paramLong);
  }
  
  public static CellInfo create(android.hardware.radio.V1_5.CellInfo paramCellInfo, long paramLong) {
    if (paramCellInfo == null)
      return null; 
    byte b = paramCellInfo.ratSpecificInfo.getDiscriminator();
    if (b != 0) {
      if (b != 1) {
        if (b != 2) {
          if (b != 3) {
            if (b != 4) {
              if (b != 5)
                return null; 
              return new CellInfoCdma(paramCellInfo, paramLong);
            } 
            return new CellInfoNr(paramCellInfo, paramLong);
          } 
          return new CellInfoLte(paramCellInfo, paramLong);
        } 
        return new CellInfoTdscdma(paramCellInfo, paramLong);
      } 
      return new CellInfoWcdma(paramCellInfo, paramLong);
    } 
    return new CellInfoGsm(paramCellInfo, paramLong);
  }
  
  public abstract CellIdentity getCellIdentity();
  
  public abstract CellSignalStrength getCellSignalStrength();
  
  public abstract void writeToParcel(Parcel paramParcel, int paramInt);
  
  @Retention(RetentionPolicy.SOURCE)
  class CellConnectionStatus implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Type implements Annotation {}
}
