package android.telephony;

import android.annotation.SystemApi;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.telephony.mbms.GroupCall;
import android.telephony.mbms.GroupCallCallback;
import android.telephony.mbms.InternalGroupCallCallback;
import android.telephony.mbms.InternalGroupCallSessionCallback;
import android.telephony.mbms.MbmsGroupCallSessionCallback;
import android.telephony.mbms.MbmsUtils;
import android.telephony.mbms.vendor.IMbmsGroupCallService;
import android.util.ArraySet;
import android.util.Log;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class MbmsGroupCallSession implements AutoCloseable {
  private static AtomicBoolean sIsInitialized = new AtomicBoolean(false);
  
  private AtomicReference<IMbmsGroupCallService> mService = new AtomicReference<>(null);
  
  private IBinder.DeathRecipient mDeathRecipient = (IBinder.DeathRecipient)new Object(this);
  
  private Set<GroupCall> mKnownActiveGroupCalls = new ArraySet<>();
  
  private static final String LOG_TAG = "MbmsGroupCallSession";
  
  @SystemApi
  public static final String MBMS_GROUP_CALL_SERVICE_ACTION = "android.telephony.action.EmbmsGroupCall";
  
  public static final String MBMS_GROUP_CALL_SERVICE_OVERRIDE_METADATA = "mbms-group-call-service-override";
  
  private final Context mContext;
  
  private InternalGroupCallSessionCallback mInternalCallback;
  
  private ServiceConnection mServiceConnection;
  
  private int mSubscriptionId;
  
  private MbmsGroupCallSession(Context paramContext, Executor paramExecutor, int paramInt, MbmsGroupCallSessionCallback paramMbmsGroupCallSessionCallback) {
    this.mContext = paramContext;
    this.mSubscriptionId = paramInt;
    this.mInternalCallback = new InternalGroupCallSessionCallback(paramMbmsGroupCallSessionCallback, paramExecutor);
  }
  
  public static MbmsGroupCallSession create(Context paramContext, final int result, Executor paramExecutor, final MbmsGroupCallSessionCallback callback) {
    if (sIsInitialized.compareAndSet(false, true)) {
      MbmsGroupCallSession mbmsGroupCallSession = new MbmsGroupCallSession(paramContext, paramExecutor, result, callback);
      result = mbmsGroupCallSession.bindAndInitialize();
      if (result != 0) {
        sIsInitialized.set(false);
        paramExecutor.execute(new Runnable() {
              final MbmsGroupCallSessionCallback val$callback;
              
              final int val$result;
              
              public void run() {
                callback.onError(result, null);
              }
            });
        return null;
      } 
      return mbmsGroupCallSession;
    } 
    throw new IllegalStateException("Cannot create two instances of MbmsGroupCallSession");
  }
  
  public static MbmsGroupCallSession create(Context paramContext, Executor paramExecutor, MbmsGroupCallSessionCallback paramMbmsGroupCallSessionCallback) {
    return create(paramContext, SubscriptionManager.getDefaultSubscriptionId(), paramExecutor, paramMbmsGroupCallSessionCallback);
  }
  
  public void close() {
    try {
      IMbmsGroupCallService iMbmsGroupCallService = this.mService.get();
      if (iMbmsGroupCallService == null || this.mServiceConnection == null)
        return; 
      iMbmsGroupCallService.dispose(this.mSubscriptionId);
      for (GroupCall groupCall : this.mKnownActiveGroupCalls)
        groupCall.getCallback().stop(); 
      this.mKnownActiveGroupCalls.clear();
      this.mContext.unbindService(this.mServiceConnection);
    } catch (RemoteException remoteException) {
    
    } finally {
      this.mService.set(null);
      sIsInitialized.set(false);
      this.mServiceConnection = null;
      this.mInternalCallback.stop();
    } 
  }
  
  public GroupCall startGroupCall(long paramLong, List<Integer> paramList1, List<Integer> paramList2, Executor paramExecutor, GroupCallCallback paramGroupCallCallback) {
    IMbmsGroupCallService iMbmsGroupCallService = this.mService.get();
    if (iMbmsGroupCallService != null) {
      InternalGroupCallCallback internalGroupCallCallback = new InternalGroupCallCallback(paramGroupCallCallback, paramExecutor);
      GroupCall groupCall = new GroupCall(this.mSubscriptionId, iMbmsGroupCallService, this, paramLong, internalGroupCallCallback);
      this.mKnownActiveGroupCalls.add(groupCall);
      try {
        int i = iMbmsGroupCallService.startGroupCall(this.mSubscriptionId, paramLong, paramList1, paramList2, internalGroupCallCallback);
        if (i != -1) {
          if (i != 0) {
            this.mInternalCallback.onError(i, null);
            return null;
          } 
          return groupCall;
        } 
        close();
        IllegalStateException illegalStateException = new IllegalStateException();
        this("Middleware must not return an unknown error code");
        throw illegalStateException;
      } catch (RemoteException remoteException) {
        Log.w("MbmsGroupCallSession", "Remote process died");
        this.mService.set(null);
        sIsInitialized.set(false);
        this.mInternalCallback.onError(3, null);
        return null;
      } 
    } 
    throw new IllegalStateException("Middleware not yet bound");
  }
  
  public void onGroupCallStopped(GroupCall paramGroupCall) {
    this.mKnownActiveGroupCalls.remove(paramGroupCall);
  }
  
  private int bindAndInitialize() {
    Object object = new Object(this);
    return MbmsUtils.startBinding(this.mContext, "android.telephony.action.EmbmsGroupCall", (ServiceConnection)object);
  }
}
