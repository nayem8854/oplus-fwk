package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class CdmaEriInformation implements Parcelable {
  public CdmaEriInformation(int paramInt1, int paramInt2) {
    this.mIconIndex = paramInt1;
    this.mIconMode = paramInt2;
  }
  
  public int getEriIconIndex() {
    return this.mIconIndex;
  }
  
  public void setEriIconIndex(int paramInt) {
    this.mIconIndex = paramInt;
  }
  
  public int getEriIconMode() {
    return this.mIconMode;
  }
  
  public void setEriIconMode(int paramInt) {
    this.mIconMode = paramInt;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mIconIndex);
    paramParcel.writeInt(this.mIconMode);
  }
  
  public int describeContents() {
    return 0;
  }
  
  private CdmaEriInformation(Parcel paramParcel) {
    this.mIconIndex = paramParcel.readInt();
    this.mIconMode = paramParcel.readInt();
  }
  
  public static final Parcelable.Creator<CdmaEriInformation> CREATOR = new Parcelable.Creator<CdmaEriInformation>() {
      public CdmaEriInformation createFromParcel(Parcel param1Parcel) {
        return new CdmaEriInformation(param1Parcel);
      }
      
      public CdmaEriInformation[] newArray(int param1Int) {
        return new CdmaEriInformation[param1Int];
      }
    };
  
  public static final int ERI_FLASH = 2;
  
  public static final int ERI_ICON_MODE_FLASH = 1;
  
  public static final int ERI_ICON_MODE_NORMAL = 0;
  
  public static final int ERI_OFF = 1;
  
  public static final int ERI_ON = 0;
  
  private int mIconIndex;
  
  private int mIconMode;
  
  @Retention(RetentionPolicy.SOURCE)
  class EriIconIndex implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class EriIconMode implements Annotation {}
}
