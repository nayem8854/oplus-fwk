package android.telephony;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.telecom.PhoneAccountHandle;

public final class VisualVoicemailSms implements Parcelable {
  VisualVoicemailSms(Builder paramBuilder) {
    this.mPhoneAccountHandle = paramBuilder.mPhoneAccountHandle;
    this.mPrefix = paramBuilder.mPrefix;
    this.mFields = paramBuilder.mFields;
    this.mMessageBody = paramBuilder.mMessageBody;
  }
  
  public PhoneAccountHandle getPhoneAccountHandle() {
    return this.mPhoneAccountHandle;
  }
  
  public String getPrefix() {
    return this.mPrefix;
  }
  
  public Bundle getFields() {
    return this.mFields;
  }
  
  public String getMessageBody() {
    return this.mMessageBody;
  }
  
  class Builder {
    private Bundle mFields;
    
    private String mMessageBody;
    
    private PhoneAccountHandle mPhoneAccountHandle;
    
    private String mPrefix;
    
    public VisualVoicemailSms build() {
      return new VisualVoicemailSms(this);
    }
    
    public Builder setPhoneAccountHandle(PhoneAccountHandle param1PhoneAccountHandle) {
      this.mPhoneAccountHandle = param1PhoneAccountHandle;
      return this;
    }
    
    public Builder setPrefix(String param1String) {
      this.mPrefix = param1String;
      return this;
    }
    
    public Builder setFields(Bundle param1Bundle) {
      this.mFields = param1Bundle;
      return this;
    }
    
    public Builder setMessageBody(String param1String) {
      this.mMessageBody = param1String;
      return this;
    }
  }
  
  public static final Parcelable.Creator<VisualVoicemailSms> CREATOR = new Parcelable.Creator<VisualVoicemailSms>() {
      public VisualVoicemailSms createFromParcel(Parcel param1Parcel) {
        VisualVoicemailSms.Builder builder2 = new VisualVoicemailSms.Builder();
        builder2 = builder2.setPhoneAccountHandle((PhoneAccountHandle)param1Parcel.readParcelable(null));
        builder2 = builder2.setPrefix(param1Parcel.readString());
        builder2 = builder2.setFields(param1Parcel.readBundle());
        VisualVoicemailSms.Builder builder1 = builder2.setMessageBody(param1Parcel.readString());
        return builder1.build();
      }
      
      public VisualVoicemailSms[] newArray(int param1Int) {
        return new VisualVoicemailSms[param1Int];
      }
    };
  
  private final Bundle mFields;
  
  private final String mMessageBody;
  
  private final PhoneAccountHandle mPhoneAccountHandle;
  
  private final String mPrefix;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(getPhoneAccountHandle(), paramInt);
    paramParcel.writeString(getPrefix());
    paramParcel.writeBundle(getFields());
    paramParcel.writeString(getMessageBody());
  }
}
