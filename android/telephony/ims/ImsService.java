package android.telephony.ims;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.telephony.ims.aidl.IImsConfig;
import android.telephony.ims.aidl.IImsMmTelFeature;
import android.telephony.ims.aidl.IImsRcsFeature;
import android.telephony.ims.aidl.IImsRegistration;
import android.telephony.ims.aidl.IImsServiceController;
import android.telephony.ims.aidl.IImsServiceControllerListener;
import android.telephony.ims.feature.ImsFeature;
import android.telephony.ims.feature.MmTelFeature;
import android.telephony.ims.feature.RcsFeature;
import android.telephony.ims.stub.ImsConfigImplBase;
import android.telephony.ims.stub.ImsFeatureConfiguration;
import android.telephony.ims.stub.ImsRegistrationImplBase;
import android.util.Log;
import android.util.SparseArray;
import com.android.ims.internal.IImsFeatureStatusCallback;

@SystemApi
public class ImsService extends Service {
  private static final String LOG_TAG = "ImsService";
  
  public static final String SERVICE_INTERFACE = "android.telephony.ims.ImsService";
  
  private final SparseArray<SparseArray<ImsFeature>> mFeaturesBySlot = new SparseArray<>();
  
  public static class Listener extends IImsServiceControllerListener.Stub {
    public void onUpdateSupportedImsFeatures(ImsFeatureConfiguration param1ImsFeatureConfiguration) {}
  }
  
  protected final IBinder mImsServiceController = (IBinder)new IImsServiceController.Stub() {
      final ImsService this$0;
      
      public void setListener(IImsServiceControllerListener param1IImsServiceControllerListener) {
        ImsService.access$002(ImsService.this, param1IImsServiceControllerListener);
      }
      
      public IImsMmTelFeature createMmTelFeature(int param1Int, IImsFeatureStatusCallback param1IImsFeatureStatusCallback) {
        return ImsService.this.createMmTelFeatureInternal(param1Int, param1IImsFeatureStatusCallback);
      }
      
      public IImsRcsFeature createRcsFeature(int param1Int, IImsFeatureStatusCallback param1IImsFeatureStatusCallback) {
        return ImsService.this.createRcsFeatureInternal(param1Int, param1IImsFeatureStatusCallback);
      }
      
      public void removeImsFeature(int param1Int1, int param1Int2, IImsFeatureStatusCallback param1IImsFeatureStatusCallback) {
        ImsService.this.removeImsFeature(param1Int1, param1Int2, param1IImsFeatureStatusCallback);
      }
      
      public ImsFeatureConfiguration querySupportedImsFeatures() {
        return ImsService.this.querySupportedImsFeatures();
      }
      
      public void notifyImsServiceReadyForFeatureCreation() {
        ImsService.this.readyForFeatureCreation();
      }
      
      public IImsConfig getConfig(int param1Int) {
        ImsConfigImplBase imsConfigImplBase = ImsService.this.getConfig(param1Int);
        if (imsConfigImplBase != null) {
          IImsConfig iImsConfig = imsConfigImplBase.getIImsConfig();
        } else {
          imsConfigImplBase = null;
        } 
        return (IImsConfig)imsConfigImplBase;
      }
      
      public IImsRegistration getRegistration(int param1Int) {
        ImsRegistrationImplBase imsRegistrationImplBase = ImsService.this.getRegistration(param1Int);
        if (imsRegistrationImplBase != null) {
          IImsRegistration iImsRegistration = imsRegistrationImplBase.getBinder();
        } else {
          imsRegistrationImplBase = null;
        } 
        return (IImsRegistration)imsRegistrationImplBase;
      }
      
      public void enableIms(int param1Int) {
        ImsService.this.enableIms(param1Int);
      }
      
      public void disableIms(int param1Int) {
        ImsService.this.disableIms(param1Int);
      }
    };
  
  private IImsServiceControllerListener mListener;
  
  public IBinder onBind(Intent paramIntent) {
    if ("android.telephony.ims.ImsService".equals(paramIntent.getAction())) {
      Log.i("ImsService", "ImsService Bound.");
      return this.mImsServiceController;
    } 
    return null;
  }
  
  public SparseArray<ImsFeature> getFeatures(int paramInt) {
    return this.mFeaturesBySlot.get(paramInt);
  }
  
  private IImsMmTelFeature createMmTelFeatureInternal(int paramInt, IImsFeatureStatusCallback paramIImsFeatureStatusCallback) {
    MmTelFeature mmTelFeature = createMmTelFeature(paramInt);
    if (mmTelFeature != null) {
      setupFeature(mmTelFeature, paramInt, 1, paramIImsFeatureStatusCallback);
      return mmTelFeature.getBinder();
    } 
    Log.e("ImsService", "createMmTelFeatureInternal: null feature returned.");
    return null;
  }
  
  private IImsRcsFeature createRcsFeatureInternal(int paramInt, IImsFeatureStatusCallback paramIImsFeatureStatusCallback) {
    RcsFeature rcsFeature = createRcsFeature(paramInt);
    if (rcsFeature != null) {
      setupFeature(rcsFeature, paramInt, 2, paramIImsFeatureStatusCallback);
      return rcsFeature.getBinder();
    } 
    Log.e("ImsService", "createRcsFeatureInternal: null feature returned.");
    return null;
  }
  
  private void setupFeature(ImsFeature paramImsFeature, int paramInt1, int paramInt2, IImsFeatureStatusCallback paramIImsFeatureStatusCallback) {
    paramImsFeature.initialize((Context)this, paramInt1);
    paramImsFeature.addImsFeatureStatusCallback(paramIImsFeatureStatusCallback);
    addImsFeature(paramInt1, paramInt2, paramImsFeature);
  }
  
  private void addImsFeature(int paramInt1, int paramInt2, ImsFeature paramImsFeature) {
    synchronized (this.mFeaturesBySlot) {
      SparseArray<ImsFeature> sparseArray1 = this.mFeaturesBySlot.get(paramInt1);
      SparseArray<ImsFeature> sparseArray2 = sparseArray1;
      if (sparseArray1 == null) {
        sparseArray2 = new SparseArray();
        this();
        this.mFeaturesBySlot.put(paramInt1, sparseArray2);
      } 
      sparseArray2.put(paramInt2, paramImsFeature);
      return;
    } 
  }
  
  private void removeImsFeature(int paramInt1, int paramInt2, IImsFeatureStatusCallback paramIImsFeatureStatusCallback) {
    synchronized (this.mFeaturesBySlot) {
      StringBuilder stringBuilder;
      SparseArray<ImsFeature> sparseArray = this.mFeaturesBySlot.get(paramInt1);
      if (sparseArray == null) {
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Can not remove ImsFeature. No ImsFeatures exist on slot ");
        stringBuilder.append(paramInt1);
        Log.w("ImsService", stringBuilder.toString());
        return;
      } 
      ImsFeature imsFeature = sparseArray.get(paramInt2);
      if (imsFeature == null) {
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Can not remove ImsFeature. No feature with type ");
        stringBuilder.append(paramInt2);
        stringBuilder.append(" exists on slot ");
        stringBuilder.append(paramInt1);
        Log.w("ImsService", stringBuilder.toString());
        return;
      } 
      imsFeature.removeImsFeatureStatusCallback((IImsFeatureStatusCallback)stringBuilder);
      imsFeature.onFeatureRemoved();
      sparseArray.remove(paramInt2);
      return;
    } 
  }
  
  public ImsFeatureConfiguration querySupportedImsFeatures() {
    return new ImsFeatureConfiguration();
  }
  
  public final void onUpdateSupportedImsFeatures(ImsFeatureConfiguration paramImsFeatureConfiguration) throws RemoteException {
    IImsServiceControllerListener iImsServiceControllerListener = this.mListener;
    if (iImsServiceControllerListener != null) {
      iImsServiceControllerListener.onUpdateSupportedImsFeatures(paramImsFeatureConfiguration);
      return;
    } 
    throw new IllegalStateException("Framework is not ready");
  }
  
  public void readyForFeatureCreation() {}
  
  public void enableIms(int paramInt) {}
  
  public void disableIms(int paramInt) {}
  
  public MmTelFeature createMmTelFeature(int paramInt) {
    return null;
  }
  
  public RcsFeature createRcsFeature(int paramInt) {
    return null;
  }
  
  public ImsConfigImplBase getConfig(int paramInt) {
    return new ImsConfigImplBase();
  }
  
  public ImsRegistrationImplBase getRegistration(int paramInt) {
    return new ImsRegistrationImplBase();
  }
}
