package android.telephony.ims;

import android.annotation.SystemApi;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.telephony.Rlog;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

@SystemApi
public final class ImsConferenceState implements Parcelable {
  public final HashMap<String, Bundle> mParticipants = new HashMap<>();
  
  public static final String USER = "user";
  
  private static final String TAG = "ImsConferenceState";
  
  public static final String STATUS_SEND_RECV = "sendrecv";
  
  public static final String STATUS_SEND_ONLY = "sendonly";
  
  public static final String STATUS_PENDING = "pending";
  
  public static final String STATUS_ON_HOLD = "on-hold";
  
  public static final String STATUS_MUTED_VIA_FOCUS = "muted-via-focus";
  
  public static final String STATUS_DISCONNECTING = "disconnecting";
  
  public static final String STATUS_DISCONNECTED = "disconnected";
  
  public static final String STATUS_DIALING_OUT = "dialing-out";
  
  public static final String STATUS_DIALING_IN = "dialing-in";
  
  public static final String STATUS_CONNECT_FAIL = "connect-fail";
  
  public static final String STATUS_CONNECTED = "connected";
  
  public static final String STATUS_ALERTING = "alerting";
  
  public static final String STATUS = "status";
  
  public static final String SIP_STATUS_CODE = "sipstatuscode";
  
  public static final String ENDPOINT = "endpoint";
  
  public static final String DISPLAY_TEXT = "display-text";
  
  private ImsConferenceState(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mParticipants.size());
    if (this.mParticipants.size() > 0) {
      Set<Map.Entry<String, Bundle>> set = this.mParticipants.entrySet();
      if (set != null) {
        Iterator<Map.Entry<String, Bundle>> iterator = set.iterator();
        while (iterator.hasNext()) {
          Map.Entry entry = iterator.next();
          paramParcel.writeString((String)entry.getKey());
          paramParcel.writeParcelable((Parcelable)entry.getValue(), 0);
        } 
      } 
    } 
  }
  
  private void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.readInt();
    for (byte b = 0; b < i; b++) {
      String str = paramParcel.readString();
      Bundle bundle = (Bundle)paramParcel.readParcelable(null);
      this.mParticipants.put(str, bundle);
    } 
  }
  
  public static final Parcelable.Creator<ImsConferenceState> CREATOR = new Parcelable.Creator<ImsConferenceState>() {
      public ImsConferenceState createFromParcel(Parcel param1Parcel) {
        return new ImsConferenceState(param1Parcel);
      }
      
      public ImsConferenceState[] newArray(int param1Int) {
        return new ImsConferenceState[param1Int];
      }
    };
  
  public static int getConnectionStateForStatus(String paramString) {
    if (paramString.equals("pending"))
      return 0; 
    if (paramString.equals("dialing-in"))
      return 2; 
    if (paramString.equals("alerting") || 
      paramString.equals("dialing-out"))
      return 3; 
    if (paramString.equals("on-hold") || 
      paramString.equals("sendonly"))
      return 5; 
    if (paramString.equals("connected") || 
      paramString.equals("muted-via-focus") || 
      paramString.equals("disconnecting") || 
      paramString.equals("sendrecv"))
      return 4; 
    if (paramString.equals("disconnected"))
      return 6; 
    return 4;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[");
    stringBuilder.append(ImsConferenceState.class.getSimpleName());
    stringBuilder.append(" ");
    if (this.mParticipants.size() > 0) {
      Set<Map.Entry<String, Bundle>> set = this.mParticipants.entrySet();
      if (set != null) {
        Iterator<Map.Entry<String, Bundle>> iterator = set.iterator();
        stringBuilder.append("<");
        while (iterator.hasNext()) {
          Map.Entry entry = iterator.next();
          stringBuilder.append(Rlog.pii("ImsConferenceState", entry.getKey()));
          stringBuilder.append(": ");
          Bundle bundle = (Bundle)entry.getValue();
          for (String str : bundle.keySet()) {
            stringBuilder.append(str);
            stringBuilder.append("=");
            if ("endpoint".equals(str) || "user".equals(str)) {
              stringBuilder.append(Rlog.pii("ImsConferenceState", bundle.get(str)));
            } else {
              stringBuilder.append(bundle.get(str));
            } 
            stringBuilder.append(", ");
          } 
        } 
        stringBuilder.append(">");
      } 
    } 
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public ImsConferenceState() {}
}
