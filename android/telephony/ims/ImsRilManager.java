package android.telephony.ims;

import android.content.Context;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.telephony.ims.aidl.IImsRil;
import android.telephony.ims.aidl.IImsRilInd;
import android.util.Log;
import java.util.ArrayList;
import java.util.function.Consumer;

public class ImsRilManager {
  private final String TAG = "ImsRilManager";
  
  private ArrayList<ICleanupListener> mCleanupListeners = new ArrayList<>();
  
  private Context mContext;
  
  private IImsRil mImsRil;
  
  private int numPhoens;
  
  public void addCleanupListener(ICleanupListener paramICleanupListener) {
    if (paramICleanupListener != null)
      this.mCleanupListeners.add(paramICleanupListener); 
  }
  
  public void removeCleanupListener(ICleanupListener paramICleanupListener) {
    if (paramICleanupListener != null)
      this.mCleanupListeners.remove(paramICleanupListener); 
  }
  
  public ImsRilManager(Context paramContext) {
    this(paramContext, null);
  }
  
  public ImsRilManager(Context paramContext, IImsRil paramIImsRil) {
    this.mContext = paramContext;
    this.mImsRil = paramIImsRil;
    this.numPhoens = TelephonyManager.from(paramContext).getPhoneCount();
  }
  
  void validateInvariants(int paramInt) throws Exception {
    checkBinder();
    checkPhoneId(paramInt);
  }
  
  private void checkBinder() throws Exception {
    if (this.mImsRil != null)
      return; 
    throw new Exception("ImsRil Service is not running");
  }
  
  private void checkPhoneId(int paramInt) throws Exception {
    if (paramInt >= 0 && paramInt < this.numPhoens)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("phoneId ");
    stringBuilder.append(paramInt);
    stringBuilder.append(" is not valid");
    Log.i("ImsRilManager", stringBuilder.toString());
    throw new Exception("invalid phoneId");
  }
  
  public void dispose() {
    this.mCleanupListeners.forEach((Consumer<? super ICleanupListener>)_$$Lambda$ImsRilManager$YUKV_r7FoU0S1Kv8Mz86OSHdiks.INSTANCE);
    this.mImsRil = null;
    this.mContext = null;
  }
  
  public void commonReqToIms(int paramInt1, int paramInt2, Message paramMessage) throws Exception {
    validateInvariants(paramInt1);
    this.mImsRil.commonReqToIms(paramInt1, paramInt2, paramMessage);
  }
  
  public void registerIndication(IImsRilInd paramIImsRilInd) throws Exception {
    this.mImsRil.registerIndication(paramIImsRilInd);
  }
  
  public void unRegisterIndication(IImsRilInd paramIImsRilInd) throws Exception {
    this.mImsRil.unRegisterIndication(paramIImsRilInd);
  }
  
  public static interface ICleanupListener {
    void dispose();
  }
}
