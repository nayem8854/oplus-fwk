package android.telephony.ims;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class ImsStreamMediaProfile implements Parcelable {
  public int mVideoQuality;
  
  public int mVideoDirection;
  
  public int mRttMode;
  
  public boolean mIsReceivingRttAudio = false;
  
  public int mAudioQuality;
  
  public int mAudioDirection;
  
  public static final int VIDEO_QUALITY_VGA_PORTRAIT = 16;
  
  public static final int VIDEO_QUALITY_VGA_LANDSCAPE = 8;
  
  public static final int VIDEO_QUALITY_QVGA_PORTRAIT = 4;
  
  public static final int VIDEO_QUALITY_QVGA_LANDSCAPE = 2;
  
  public static final int VIDEO_QUALITY_QCIF = 1;
  
  public static final int VIDEO_QUALITY_NONE = 0;
  
  private static final String TAG = "ImsStreamMediaProfile";
  
  public static final int RTT_MODE_FULL = 1;
  
  public static final int RTT_MODE_DISABLED = 0;
  
  public static final int DIRECTION_SEND_RECEIVE = 3;
  
  public static final int DIRECTION_SEND = 2;
  
  public static final int DIRECTION_RECEIVE = 1;
  
  public static final int DIRECTION_INVALID = -1;
  
  public static final int DIRECTION_INACTIVE = 0;
  
  public ImsStreamMediaProfile(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public ImsStreamMediaProfile(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this.mAudioQuality = paramInt1;
    this.mAudioDirection = paramInt2;
    this.mVideoQuality = paramInt3;
    this.mVideoDirection = paramInt4;
    this.mRttMode = paramInt5;
  }
  
  public ImsStreamMediaProfile() {
    this.mAudioQuality = 0;
    this.mAudioDirection = 3;
    this.mVideoQuality = 0;
    this.mVideoDirection = -1;
    this.mRttMode = 0;
  }
  
  public ImsStreamMediaProfile(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mAudioQuality = paramInt1;
    this.mAudioDirection = paramInt2;
    this.mVideoQuality = paramInt3;
    this.mVideoDirection = paramInt4;
  }
  
  public ImsStreamMediaProfile(int paramInt) {
    this.mRttMode = paramInt;
  }
  
  public void copyFrom(ImsStreamMediaProfile paramImsStreamMediaProfile) {
    this.mAudioQuality = paramImsStreamMediaProfile.mAudioQuality;
    this.mAudioDirection = paramImsStreamMediaProfile.mAudioDirection;
    this.mVideoQuality = paramImsStreamMediaProfile.mVideoQuality;
    this.mVideoDirection = paramImsStreamMediaProfile.mVideoDirection;
    this.mRttMode = paramImsStreamMediaProfile.mRttMode;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{ audioQuality=");
    stringBuilder.append(this.mAudioQuality);
    stringBuilder.append(", audioDirection=");
    stringBuilder.append(this.mAudioDirection);
    stringBuilder.append(", videoQuality=");
    stringBuilder.append(this.mVideoQuality);
    stringBuilder.append(", videoDirection=");
    stringBuilder.append(this.mVideoDirection);
    stringBuilder.append(", rttMode=");
    stringBuilder.append(this.mRttMode);
    stringBuilder.append(", hasRttAudioSpeech=");
    stringBuilder.append(this.mIsReceivingRttAudio);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mAudioQuality);
    paramParcel.writeInt(this.mAudioDirection);
    paramParcel.writeInt(this.mVideoQuality);
    paramParcel.writeInt(this.mVideoDirection);
    paramParcel.writeInt(this.mRttMode);
    paramParcel.writeBoolean(this.mIsReceivingRttAudio);
  }
  
  private void readFromParcel(Parcel paramParcel) {
    this.mAudioQuality = paramParcel.readInt();
    this.mAudioDirection = paramParcel.readInt();
    this.mVideoQuality = paramParcel.readInt();
    this.mVideoDirection = paramParcel.readInt();
    this.mRttMode = paramParcel.readInt();
    this.mIsReceivingRttAudio = paramParcel.readBoolean();
  }
  
  public static final Parcelable.Creator<ImsStreamMediaProfile> CREATOR = new Parcelable.Creator<ImsStreamMediaProfile>() {
      public ImsStreamMediaProfile createFromParcel(Parcel param1Parcel) {
        return new ImsStreamMediaProfile(param1Parcel);
      }
      
      public ImsStreamMediaProfile[] newArray(int param1Int) {
        return new ImsStreamMediaProfile[param1Int];
      }
    };
  
  public static final int AUDIO_QUALITY_QCELP13K = 3;
  
  public static final int AUDIO_QUALITY_NONE = 0;
  
  public static final int AUDIO_QUALITY_GSM_HR = 10;
  
  public static final int AUDIO_QUALITY_GSM_FR = 9;
  
  public static final int AUDIO_QUALITY_GSM_EFR = 8;
  
  public static final int AUDIO_QUALITY_G729 = 16;
  
  public static final int AUDIO_QUALITY_G723 = 12;
  
  public static final int AUDIO_QUALITY_G722 = 14;
  
  public static final int AUDIO_QUALITY_G711U = 11;
  
  public static final int AUDIO_QUALITY_G711AB = 15;
  
  public static final int AUDIO_QUALITY_G711A = 13;
  
  public static final int AUDIO_QUALITY_EVS_WB = 18;
  
  public static final int AUDIO_QUALITY_EVS_SWB = 19;
  
  public static final int AUDIO_QUALITY_EVS_NB = 17;
  
  public static final int AUDIO_QUALITY_EVS_FB = 20;
  
  public static final int AUDIO_QUALITY_EVRC_WB = 6;
  
  public static final int AUDIO_QUALITY_EVRC_NW = 7;
  
  public static final int AUDIO_QUALITY_EVRC_B = 5;
  
  public static final int AUDIO_QUALITY_EVRC = 4;
  
  public static final int AUDIO_QUALITY_AMR_WB = 2;
  
  public static final int AUDIO_QUALITY_AMR = 1;
  
  public boolean isRttCall() {
    int i = this.mRttMode;
    boolean bool = true;
    if (i != 1)
      bool = false; 
    return bool;
  }
  
  public void setRttMode(int paramInt) {
    this.mRttMode = paramInt;
  }
  
  public void setReceivingRttAudio(boolean paramBoolean) {
    this.mIsReceivingRttAudio = paramBoolean;
  }
  
  public int getAudioQuality() {
    return this.mAudioQuality;
  }
  
  public int getAudioDirection() {
    return this.mAudioDirection;
  }
  
  public int getVideoQuality() {
    return this.mVideoQuality;
  }
  
  public int getVideoDirection() {
    return this.mVideoDirection;
  }
  
  public int getRttMode() {
    return this.mRttMode;
  }
  
  public boolean isReceivingRttAudio() {
    return this.mIsReceivingRttAudio;
  }
}
