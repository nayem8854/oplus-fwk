package android.telephony.ims;

public interface ImsRilConstant {
  public static final int COMMON_REQID_VOPS = 1;
  
  public static final int EVENT_COMMON_REQ_TO_IMS = 100;
  
  public static final int EVENT_COMMON_REQ_TO_IMS_DONE = 101;
}
