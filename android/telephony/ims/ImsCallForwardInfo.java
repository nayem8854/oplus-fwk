package android.telephony.ims;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public final class ImsCallForwardInfo implements Parcelable {
  public static final int CDIV_CF_REASON_ALL = 4;
  
  public static final int CDIV_CF_REASON_ALL_CONDITIONAL = 5;
  
  public static final int CDIV_CF_REASON_BUSY = 1;
  
  public static final int CDIV_CF_REASON_NOT_LOGGED_IN = 6;
  
  public static final int CDIV_CF_REASON_NOT_REACHABLE = 3;
  
  public static final int CDIV_CF_REASON_NO_REPLY = 2;
  
  public static final int CDIV_CF_REASON_UNCONDITIONAL = 0;
  
  public ImsCallForwardInfo() {}
  
  public ImsCallForwardInfo(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString, int paramInt5) {
    this.mCondition = paramInt1;
    this.mStatus = paramInt2;
    this.mToA = paramInt3;
    this.mServiceClass = paramInt4;
    this.mNumber = paramString;
    this.mTimeSeconds = paramInt5;
  }
  
  public ImsCallForwardInfo(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mCondition);
    paramParcel.writeInt(this.mStatus);
    paramParcel.writeInt(this.mToA);
    paramParcel.writeString(this.mNumber);
    paramParcel.writeInt(this.mTimeSeconds);
    paramParcel.writeInt(this.mServiceClass);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(super.toString());
    stringBuilder.append(", Condition: ");
    stringBuilder.append(this.mCondition);
    stringBuilder.append(", Status: ");
    if (this.mStatus == 0) {
      null = "disabled";
    } else {
      null = "enabled";
    } 
    stringBuilder.append(null);
    stringBuilder.append(", ToA: ");
    stringBuilder.append(this.mToA);
    stringBuilder.append(", Service Class: ");
    stringBuilder.append(this.mServiceClass);
    stringBuilder.append(", Number=");
    stringBuilder.append(this.mNumber);
    stringBuilder.append(", Time (seconds): ");
    stringBuilder.append(this.mTimeSeconds);
    return stringBuilder.toString();
  }
  
  private void readFromParcel(Parcel paramParcel) {
    this.mCondition = paramParcel.readInt();
    this.mStatus = paramParcel.readInt();
    this.mToA = paramParcel.readInt();
    this.mNumber = paramParcel.readString();
    this.mTimeSeconds = paramParcel.readInt();
    this.mServiceClass = paramParcel.readInt();
  }
  
  public static final Parcelable.Creator<ImsCallForwardInfo> CREATOR = new Parcelable.Creator<ImsCallForwardInfo>() {
      public ImsCallForwardInfo createFromParcel(Parcel param1Parcel) {
        return new ImsCallForwardInfo(param1Parcel);
      }
      
      public ImsCallForwardInfo[] newArray(int param1Int) {
        return new ImsCallForwardInfo[param1Int];
      }
    };
  
  public static final int STATUS_ACTIVE = 1;
  
  public static final int STATUS_NOT_ACTIVE = 0;
  
  public static final int TYPE_OF_ADDRESS_INTERNATIONAL = 145;
  
  public static final int TYPE_OF_ADDRESS_UNKNOWN = 129;
  
  public int mCondition;
  
  public String mNumber;
  
  public int mServiceClass;
  
  public int mStatus;
  
  public int mTimeSeconds;
  
  public int mToA;
  
  public int getCondition() {
    return this.mCondition;
  }
  
  public int getStatus() {
    return this.mStatus;
  }
  
  public int getToA() {
    return this.mToA;
  }
  
  public int getServiceClass() {
    return this.mServiceClass;
  }
  
  public String getNumber() {
    return this.mNumber;
  }
  
  public int getTimeSeconds() {
    return this.mTimeSeconds;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class CallForwardReasons implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class CallForwardStatus implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class TypeOfAddress implements Annotation {}
}
