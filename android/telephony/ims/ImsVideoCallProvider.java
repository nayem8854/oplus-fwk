package android.telephony.ims;

import android.annotation.SystemApi;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.telecom.VideoProfile;
import android.view.Surface;
import com.android.ims.internal.IImsVideoCallCallback;
import com.android.ims.internal.IImsVideoCallProvider;
import com.android.internal.os.SomeArgs;

@SystemApi
public abstract class ImsVideoCallProvider {
  private static final int MSG_REQUEST_CALL_DATA_USAGE = 10;
  
  private static final int MSG_REQUEST_CAMERA_CAPABILITIES = 9;
  
  private static final int MSG_SEND_SESSION_MODIFY_REQUEST = 7;
  
  private static final int MSG_SEND_SESSION_MODIFY_RESPONSE = 8;
  
  private static final int MSG_SET_CALLBACK = 1;
  
  private static final int MSG_SET_CAMERA = 2;
  
  private static final int MSG_SET_DEVICE_ORIENTATION = 5;
  
  private static final int MSG_SET_DISPLAY_SURFACE = 4;
  
  private static final int MSG_SET_PAUSE_IMAGE = 11;
  
  private static final int MSG_SET_PREVIEW_SURFACE = 3;
  
  private static final int MSG_SET_ZOOM = 6;
  
  private final ImsVideoCallProviderBinder mBinder;
  
  private IImsVideoCallCallback mCallback;
  
  private final Handler mProviderHandler = (Handler)new Object(this, Looper.getMainLooper());
  
  class ImsVideoCallProviderBinder extends IImsVideoCallProvider.Stub {
    final ImsVideoCallProvider this$0;
    
    private ImsVideoCallProviderBinder() {}
    
    public void setCallback(IImsVideoCallCallback param1IImsVideoCallCallback) {
      ImsVideoCallProvider.this.mProviderHandler.obtainMessage(1, param1IImsVideoCallCallback).sendToTarget();
    }
    
    public void setCamera(String param1String, int param1Int) {
      SomeArgs someArgs = SomeArgs.obtain();
      someArgs.arg1 = param1String;
      someArgs.argi1 = param1Int;
      ImsVideoCallProvider.this.mProviderHandler.obtainMessage(2, someArgs).sendToTarget();
    }
    
    public void setPreviewSurface(Surface param1Surface) {
      ImsVideoCallProvider.this.mProviderHandler.obtainMessage(3, param1Surface).sendToTarget();
    }
    
    public void setDisplaySurface(Surface param1Surface) {
      ImsVideoCallProvider.this.mProviderHandler.obtainMessage(4, param1Surface).sendToTarget();
    }
    
    public void setDeviceOrientation(int param1Int) {
      ImsVideoCallProvider.this.mProviderHandler.obtainMessage(5, param1Int, 0).sendToTarget();
    }
    
    public void setZoom(float param1Float) {
      ImsVideoCallProvider.this.mProviderHandler.obtainMessage(6, Float.valueOf(param1Float)).sendToTarget();
    }
    
    public void sendSessionModifyRequest(VideoProfile param1VideoProfile1, VideoProfile param1VideoProfile2) {
      SomeArgs someArgs = SomeArgs.obtain();
      someArgs.arg1 = param1VideoProfile1;
      someArgs.arg2 = param1VideoProfile2;
      ImsVideoCallProvider.this.mProviderHandler.obtainMessage(7, someArgs).sendToTarget();
    }
    
    public void sendSessionModifyResponse(VideoProfile param1VideoProfile) {
      Message message = ImsVideoCallProvider.this.mProviderHandler.obtainMessage(8, param1VideoProfile);
      message.sendToTarget();
    }
    
    public void requestCameraCapabilities() {
      ImsVideoCallProvider.this.mProviderHandler.obtainMessage(9).sendToTarget();
    }
    
    public void requestCallDataUsage() {
      ImsVideoCallProvider.this.mProviderHandler.obtainMessage(10).sendToTarget();
    }
    
    public void setPauseImage(Uri param1Uri) {
      ImsVideoCallProvider.this.mProviderHandler.obtainMessage(11, param1Uri).sendToTarget();
    }
  }
  
  public ImsVideoCallProvider() {
    this.mBinder = new ImsVideoCallProviderBinder();
  }
  
  public final IImsVideoCallProvider getInterface() {
    return this.mBinder;
  }
  
  public void onSetCamera(String paramString, int paramInt) {}
  
  public void receiveSessionModifyRequest(VideoProfile paramVideoProfile) {
    IImsVideoCallCallback iImsVideoCallCallback = this.mCallback;
    if (iImsVideoCallCallback != null)
      try {
        iImsVideoCallCallback.receiveSessionModifyRequest(paramVideoProfile);
      } catch (RemoteException remoteException) {} 
  }
  
  public void receiveSessionModifyResponse(int paramInt, VideoProfile paramVideoProfile1, VideoProfile paramVideoProfile2) {
    IImsVideoCallCallback iImsVideoCallCallback = this.mCallback;
    if (iImsVideoCallCallback != null)
      try {
        iImsVideoCallCallback.receiveSessionModifyResponse(paramInt, paramVideoProfile1, paramVideoProfile2);
      } catch (RemoteException remoteException) {} 
  }
  
  public void handleCallSessionEvent(int paramInt) {
    IImsVideoCallCallback iImsVideoCallCallback = this.mCallback;
    if (iImsVideoCallCallback != null)
      try {
        iImsVideoCallCallback.handleCallSessionEvent(paramInt);
      } catch (RemoteException remoteException) {} 
  }
  
  public void changePeerDimensions(int paramInt1, int paramInt2) {
    IImsVideoCallCallback iImsVideoCallCallback = this.mCallback;
    if (iImsVideoCallCallback != null)
      try {
        iImsVideoCallCallback.changePeerDimensions(paramInt1, paramInt2);
      } catch (RemoteException remoteException) {} 
  }
  
  public void changeCallDataUsage(long paramLong) {
    IImsVideoCallCallback iImsVideoCallCallback = this.mCallback;
    if (iImsVideoCallCallback != null)
      try {
        iImsVideoCallCallback.changeCallDataUsage(paramLong);
      } catch (RemoteException remoteException) {} 
  }
  
  public void changeCameraCapabilities(VideoProfile.CameraCapabilities paramCameraCapabilities) {
    IImsVideoCallCallback iImsVideoCallCallback = this.mCallback;
    if (iImsVideoCallCallback != null)
      try {
        iImsVideoCallCallback.changeCameraCapabilities(paramCameraCapabilities);
      } catch (RemoteException remoteException) {} 
  }
  
  public void changeVideoQuality(int paramInt) {
    IImsVideoCallCallback iImsVideoCallCallback = this.mCallback;
    if (iImsVideoCallCallback != null)
      try {
        iImsVideoCallCallback.changeVideoQuality(paramInt);
      } catch (RemoteException remoteException) {} 
  }
  
  public abstract void onRequestCallDataUsage();
  
  public abstract void onRequestCameraCapabilities();
  
  public abstract void onSendSessionModifyRequest(VideoProfile paramVideoProfile1, VideoProfile paramVideoProfile2);
  
  public abstract void onSendSessionModifyResponse(VideoProfile paramVideoProfile);
  
  public abstract void onSetCamera(String paramString);
  
  public abstract void onSetDeviceOrientation(int paramInt);
  
  public abstract void onSetDisplaySurface(Surface paramSurface);
  
  public abstract void onSetPauseImage(Uri paramUri);
  
  public abstract void onSetPreviewSurface(Surface paramSurface);
  
  public abstract void onSetZoom(float paramFloat);
}
