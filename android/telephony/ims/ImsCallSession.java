package android.telephony.ims;

import android.os.Message;
import android.os.RemoteException;
import android.telephony.CallQuality;
import android.telephony.ims.aidl.IImsCallSessionListener;
import android.util.Log;
import com.android.ims.internal.IImsCallSession;
import com.android.ims.internal.IImsVideoCallProvider;

public class ImsCallSession {
  private static final String TAG = "ImsCallSession";
  
  public static class State {
    public static final int ESTABLISHED = 4;
    
    public static final int ESTABLISHING = 3;
    
    public static final int IDLE = 0;
    
    public static final int INITIATED = 1;
    
    public static final int INVALID = -1;
    
    public static final int NEGOTIATING = 2;
    
    public static final int REESTABLISHING = 6;
    
    public static final int RENEGOTIATING = 5;
    
    public static final int TERMINATED = 8;
    
    public static final int TERMINATING = 7;
    
    public static String toString(int param1Int) {
      switch (param1Int) {
        default:
          return "UNKNOWN";
        case 8:
          return "TERMINATED";
        case 7:
          return "TERMINATING";
        case 6:
          return "REESTABLISHING";
        case 5:
          return "RENEGOTIATING";
        case 4:
          return "ESTABLISHED";
        case 3:
          return "ESTABLISHING";
        case 2:
          return "NEGOTIATING";
        case 1:
          return "INITIATED";
        case 0:
          break;
      } 
      return "IDLE";
    }
  }
  
  public static class Listener {
    public void callSessionInitiating(ImsCallSession param1ImsCallSession, ImsStreamMediaProfile param1ImsStreamMediaProfile) {}
    
    public void callSessionProgressing(ImsCallSession param1ImsCallSession, ImsStreamMediaProfile param1ImsStreamMediaProfile) {}
    
    public void callSessionStarted(ImsCallSession param1ImsCallSession, ImsCallProfile param1ImsCallProfile) {}
    
    @Deprecated
    public void callSessionStartFailed(ImsCallSession param1ImsCallSession, ImsReasonInfo param1ImsReasonInfo) {}
    
    public void callSessionInitiatingFailed(ImsCallSession param1ImsCallSession, ImsReasonInfo param1ImsReasonInfo) {}
    
    public void callSessionTerminated(ImsCallSession param1ImsCallSession, ImsReasonInfo param1ImsReasonInfo) {}
    
    public void callSessionHeld(ImsCallSession param1ImsCallSession, ImsCallProfile param1ImsCallProfile) {}
    
    public void callSessionHoldFailed(ImsCallSession param1ImsCallSession, ImsReasonInfo param1ImsReasonInfo) {}
    
    public void callSessionHoldReceived(ImsCallSession param1ImsCallSession, ImsCallProfile param1ImsCallProfile) {}
    
    public void callSessionResumed(ImsCallSession param1ImsCallSession, ImsCallProfile param1ImsCallProfile) {}
    
    public void callSessionResumeFailed(ImsCallSession param1ImsCallSession, ImsReasonInfo param1ImsReasonInfo) {}
    
    public void callSessionResumeReceived(ImsCallSession param1ImsCallSession, ImsCallProfile param1ImsCallProfile) {}
    
    public void callSessionMergeStarted(ImsCallSession param1ImsCallSession1, ImsCallSession param1ImsCallSession2, ImsCallProfile param1ImsCallProfile) {}
    
    public void callSessionMergeComplete(ImsCallSession param1ImsCallSession) {}
    
    public void callSessionMergeFailed(ImsCallSession param1ImsCallSession, ImsReasonInfo param1ImsReasonInfo) {}
    
    public void callSessionUpdated(ImsCallSession param1ImsCallSession, ImsCallProfile param1ImsCallProfile) {}
    
    public void callSessionUpdateFailed(ImsCallSession param1ImsCallSession, ImsReasonInfo param1ImsReasonInfo) {}
    
    public void callSessionUpdateReceived(ImsCallSession param1ImsCallSession, ImsCallProfile param1ImsCallProfile) {}
    
    public void callSessionConferenceExtended(ImsCallSession param1ImsCallSession1, ImsCallSession param1ImsCallSession2, ImsCallProfile param1ImsCallProfile) {}
    
    public void callSessionConferenceExtendFailed(ImsCallSession param1ImsCallSession, ImsReasonInfo param1ImsReasonInfo) {}
    
    public void callSessionConferenceExtendReceived(ImsCallSession param1ImsCallSession1, ImsCallSession param1ImsCallSession2, ImsCallProfile param1ImsCallProfile) {}
    
    public void callSessionInviteParticipantsRequestDelivered(ImsCallSession param1ImsCallSession) {}
    
    public void callSessionInviteParticipantsRequestFailed(ImsCallSession param1ImsCallSession, ImsReasonInfo param1ImsReasonInfo) {}
    
    public void callSessionRemoveParticipantsRequestDelivered(ImsCallSession param1ImsCallSession) {}
    
    public void callSessionRemoveParticipantsRequestFailed(ImsCallSession param1ImsCallSession, ImsReasonInfo param1ImsReasonInfo) {}
    
    public void callSessionConferenceStateUpdated(ImsCallSession param1ImsCallSession, ImsConferenceState param1ImsConferenceState) {}
    
    public void callSessionUssdMessageReceived(ImsCallSession param1ImsCallSession, int param1Int, String param1String) {}
    
    public void callSessionMayHandover(ImsCallSession param1ImsCallSession, int param1Int1, int param1Int2) {}
    
    public void callSessionHandover(ImsCallSession param1ImsCallSession, int param1Int1, int param1Int2, ImsReasonInfo param1ImsReasonInfo) {}
    
    public void callSessionHandoverFailed(ImsCallSession param1ImsCallSession, int param1Int1, int param1Int2, ImsReasonInfo param1ImsReasonInfo) {}
    
    public void callSessionTtyModeReceived(ImsCallSession param1ImsCallSession, int param1Int) {}
    
    public void callSessionMultipartyStateChanged(ImsCallSession param1ImsCallSession, boolean param1Boolean) {}
    
    public void callSessionSuppServiceReceived(ImsCallSession param1ImsCallSession, ImsSuppServiceNotification param1ImsSuppServiceNotification) {}
    
    public void callSessionRttModifyRequestReceived(ImsCallSession param1ImsCallSession, ImsCallProfile param1ImsCallProfile) {}
    
    public void callSessionRttModifyResponseReceived(int param1Int) {}
    
    public void callSessionRttMessageReceived(String param1String) {}
    
    public void callSessionRttAudioIndicatorChanged(ImsStreamMediaProfile param1ImsStreamMediaProfile) {}
    
    public void callSessionTransferred(ImsCallSession param1ImsCallSession) {}
    
    public void callSessionTransferFailed(ImsCallSession param1ImsCallSession, ImsReasonInfo param1ImsReasonInfo) {}
    
    public void callQualityChanged(CallQuality param1CallQuality) {}
  }
  
  private boolean mClosed = false;
  
  private Listener mListener;
  
  private final IImsCallSession miSession;
  
  public ImsCallSession(IImsCallSession paramIImsCallSession) {
    this.miSession = paramIImsCallSession;
    if (paramIImsCallSession != null) {
      try {
        IImsCallSessionListenerProxy iImsCallSessionListenerProxy = new IImsCallSessionListenerProxy();
        this(this);
        paramIImsCallSession.setListener(iImsCallSessionListenerProxy);
      } catch (RemoteException remoteException) {}
    } else {
      this.mClosed = true;
    } 
  }
  
  public ImsCallSession(IImsCallSession paramIImsCallSession, Listener paramListener) {
    this(paramIImsCallSession);
    setListener(paramListener);
  }
  
  public void close() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mClosed : Z
    //   6: ifeq -> 12
    //   9: aload_0
    //   10: monitorexit
    //   11: return
    //   12: aload_0
    //   13: getfield miSession : Lcom/android/ims/internal/IImsCallSession;
    //   16: invokeinterface close : ()V
    //   21: aload_0
    //   22: iconst_1
    //   23: putfield mClosed : Z
    //   26: goto -> 30
    //   29: astore_1
    //   30: aload_0
    //   31: monitorexit
    //   32: return
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #529	-> 0
    //   #530	-> 2
    //   #531	-> 9
    //   #535	-> 12
    //   #536	-> 21
    //   #538	-> 26
    //   #537	-> 29
    //   #539	-> 30
    //   #540	-> 32
    //   #539	-> 33
    // Exception table:
    //   from	to	target	type
    //   2	9	33	finally
    //   9	11	33	finally
    //   12	21	29	android/os/RemoteException
    //   12	21	33	finally
    //   21	26	29	android/os/RemoteException
    //   21	26	33	finally
    //   30	32	33	finally
    //   34	36	33	finally
  }
  
  public String getCallId() {
    if (this.mClosed)
      return null; 
    try {
      return this.miSession.getCallId();
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public ImsCallProfile getCallProfile() {
    if (this.mClosed)
      return null; 
    try {
      return this.miSession.getCallProfile();
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public ImsCallProfile getLocalCallProfile() {
    if (this.mClosed)
      return null; 
    try {
      return this.miSession.getLocalCallProfile();
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public ImsCallProfile getRemoteCallProfile() {
    if (this.mClosed)
      return null; 
    try {
      return this.miSession.getRemoteCallProfile();
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public IImsVideoCallProvider getVideoCallProvider() {
    if (this.mClosed)
      return null; 
    try {
      return this.miSession.getVideoCallProvider();
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public String getProperty(String paramString) {
    if (this.mClosed)
      return null; 
    try {
      return this.miSession.getProperty(paramString);
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public int getState() {
    if (this.mClosed)
      return -1; 
    try {
      return this.miSession.getState();
    } catch (RemoteException remoteException) {
      return -1;
    } 
  }
  
  public boolean isAlive() {
    if (this.mClosed)
      return false; 
    int i = getState();
    switch (i) {
      default:
        return false;
      case 0:
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
        break;
    } 
    return true;
  }
  
  public IImsCallSession getSession() {
    return this.miSession;
  }
  
  public boolean isInCall() {
    if (this.mClosed)
      return false; 
    try {
      return this.miSession.isInCall();
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public void setListener(Listener paramListener) {
    this.mListener = paramListener;
  }
  
  public void setMute(boolean paramBoolean) {
    if (this.mClosed)
      return; 
    try {
      this.miSession.setMute(paramBoolean);
    } catch (RemoteException remoteException) {}
  }
  
  public void start(String paramString, ImsCallProfile paramImsCallProfile) {
    if (this.mClosed)
      return; 
    try {
      this.miSession.start(paramString, paramImsCallProfile);
    } catch (RemoteException remoteException) {}
  }
  
  public void start(String[] paramArrayOfString, ImsCallProfile paramImsCallProfile) {
    if (this.mClosed)
      return; 
    try {
      this.miSession.startConference(paramArrayOfString, paramImsCallProfile);
    } catch (RemoteException remoteException) {}
  }
  
  public void accept(int paramInt, ImsStreamMediaProfile paramImsStreamMediaProfile) {
    if (this.mClosed)
      return; 
    try {
      this.miSession.accept(paramInt, paramImsStreamMediaProfile);
    } catch (RemoteException remoteException) {}
  }
  
  public void deflect(String paramString) {
    if (this.mClosed)
      return; 
    try {
      this.miSession.deflect(paramString);
    } catch (RemoteException remoteException) {}
  }
  
  public void reject(int paramInt) {
    if (this.mClosed)
      return; 
    try {
      this.miSession.reject(paramInt);
    } catch (RemoteException remoteException) {}
  }
  
  public void transfer(String paramString, boolean paramBoolean) {
    if (this.mClosed)
      return; 
    try {
      this.miSession.transfer(paramString, paramBoolean);
    } catch (RemoteException remoteException) {}
  }
  
  public void transfer(ImsCallSession paramImsCallSession) {
    if (this.mClosed)
      return; 
    if (paramImsCallSession != null)
      try {
        this.miSession.consultativeTransfer(paramImsCallSession.getSession());
      } catch (RemoteException remoteException) {} 
  }
  
  public void terminate(int paramInt) {
    if (this.mClosed)
      return; 
    try {
      this.miSession.terminate(paramInt);
    } catch (RemoteException remoteException) {}
  }
  
  public void hold(ImsStreamMediaProfile paramImsStreamMediaProfile) {
    if (this.mClosed)
      return; 
    try {
      this.miSession.hold(paramImsStreamMediaProfile);
    } catch (RemoteException remoteException) {}
  }
  
  public void resume(ImsStreamMediaProfile paramImsStreamMediaProfile) {
    if (this.mClosed)
      return; 
    try {
      this.miSession.resume(paramImsStreamMediaProfile);
    } catch (RemoteException remoteException) {}
  }
  
  public void merge() {
    if (this.mClosed)
      return; 
    try {
      this.miSession.merge();
    } catch (RemoteException remoteException) {}
  }
  
  public void update(int paramInt, ImsStreamMediaProfile paramImsStreamMediaProfile) {
    if (this.mClosed)
      return; 
    try {
      this.miSession.update(paramInt, paramImsStreamMediaProfile);
    } catch (RemoteException remoteException) {}
  }
  
  public void extendToConference(String[] paramArrayOfString) {
    if (this.mClosed)
      return; 
    try {
      this.miSession.extendToConference(paramArrayOfString);
    } catch (RemoteException remoteException) {}
  }
  
  public void inviteParticipants(String[] paramArrayOfString) {
    if (this.mClosed)
      return; 
    try {
      this.miSession.inviteParticipants(paramArrayOfString);
    } catch (RemoteException remoteException) {}
  }
  
  public void removeParticipants(String[] paramArrayOfString) {
    if (this.mClosed)
      return; 
    try {
      this.miSession.removeParticipants(paramArrayOfString);
    } catch (RemoteException remoteException) {}
  }
  
  public void sendDtmf(char paramChar, Message paramMessage) {
    if (this.mClosed)
      return; 
    try {
      this.miSession.sendDtmf(paramChar, paramMessage);
    } catch (RemoteException remoteException) {}
  }
  
  public void startDtmf(char paramChar) {
    if (this.mClosed)
      return; 
    try {
      this.miSession.startDtmf(paramChar);
    } catch (RemoteException remoteException) {}
  }
  
  public void stopDtmf() {
    if (this.mClosed)
      return; 
    try {
      this.miSession.stopDtmf();
    } catch (RemoteException remoteException) {}
  }
  
  public void sendUssd(String paramString) {
    if (this.mClosed)
      return; 
    try {
      this.miSession.sendUssd(paramString);
    } catch (RemoteException remoteException) {}
  }
  
  public boolean isMultiparty() {
    if (this.mClosed)
      return false; 
    try {
      return this.miSession.isMultiparty();
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public void sendRttMessage(String paramString) {
    if (this.mClosed)
      return; 
    try {
      this.miSession.sendRttMessage(paramString);
    } catch (RemoteException remoteException) {}
  }
  
  public void sendRttModifyRequest(ImsCallProfile paramImsCallProfile) {
    if (this.mClosed)
      return; 
    try {
      this.miSession.sendRttModifyRequest(paramImsCallProfile);
    } catch (RemoteException remoteException) {}
  }
  
  public void sendRttModifyResponse(boolean paramBoolean) {
    if (this.mClosed)
      return; 
    try {
      this.miSession.sendRttModifyResponse(paramBoolean);
    } catch (RemoteException remoteException) {}
  }
  
  class IImsCallSessionListenerProxy extends IImsCallSessionListener.Stub {
    final ImsCallSession this$0;
    
    private IImsCallSessionListenerProxy() {}
    
    public void callSessionInitiating(ImsStreamMediaProfile param1ImsStreamMediaProfile) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionInitiating(ImsCallSession.this, param1ImsStreamMediaProfile); 
    }
    
    public void callSessionProgressing(ImsStreamMediaProfile param1ImsStreamMediaProfile) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionProgressing(ImsCallSession.this, param1ImsStreamMediaProfile); 
    }
    
    public void callSessionInitiated(ImsCallProfile param1ImsCallProfile) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionStarted(ImsCallSession.this, param1ImsCallProfile); 
    }
    
    public void callSessionInitiatedFailed(ImsReasonInfo param1ImsReasonInfo) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionStartFailed(ImsCallSession.this, param1ImsReasonInfo); 
    }
    
    public void callSessionInitiatingFailed(ImsReasonInfo param1ImsReasonInfo) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionInitiatingFailed(ImsCallSession.this, param1ImsReasonInfo); 
    }
    
    public void callSessionTerminated(ImsReasonInfo param1ImsReasonInfo) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionTerminated(ImsCallSession.this, param1ImsReasonInfo); 
    }
    
    public void callSessionHeld(ImsCallProfile param1ImsCallProfile) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionHeld(ImsCallSession.this, param1ImsCallProfile); 
    }
    
    public void callSessionHoldFailed(ImsReasonInfo param1ImsReasonInfo) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionHoldFailed(ImsCallSession.this, param1ImsReasonInfo); 
    }
    
    public void callSessionHoldReceived(ImsCallProfile param1ImsCallProfile) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionHoldReceived(ImsCallSession.this, param1ImsCallProfile); 
    }
    
    public void callSessionResumed(ImsCallProfile param1ImsCallProfile) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionResumed(ImsCallSession.this, param1ImsCallProfile); 
    }
    
    public void callSessionResumeFailed(ImsReasonInfo param1ImsReasonInfo) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionResumeFailed(ImsCallSession.this, param1ImsReasonInfo); 
    }
    
    public void callSessionResumeReceived(ImsCallProfile param1ImsCallProfile) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionResumeReceived(ImsCallSession.this, param1ImsCallProfile); 
    }
    
    public void callSessionMergeStarted(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) {
      Log.d("ImsCallSession", "callSessionMergeStarted");
    }
    
    public void callSessionMergeComplete(IImsCallSession param1IImsCallSession) {
      if (ImsCallSession.this.mListener != null)
        if (param1IImsCallSession != null) {
          ImsCallSession imsCallSession = new ImsCallSession(param1IImsCallSession);
          ImsCallSession.access$102(imsCallSession, ImsCallSession.this.mListener);
          ImsCallSession.this.mListener.callSessionMergeComplete(imsCallSession);
        } else {
          ImsCallSession.this.mListener.callSessionMergeComplete(null);
        }  
    }
    
    public void callSessionMergeFailed(ImsReasonInfo param1ImsReasonInfo) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionMergeFailed(ImsCallSession.this, param1ImsReasonInfo); 
    }
    
    public void callSessionUpdated(ImsCallProfile param1ImsCallProfile) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionUpdated(ImsCallSession.this, param1ImsCallProfile); 
    }
    
    public void callSessionUpdateFailed(ImsReasonInfo param1ImsReasonInfo) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionUpdateFailed(ImsCallSession.this, param1ImsReasonInfo); 
    }
    
    public void callSessionUpdateReceived(ImsCallProfile param1ImsCallProfile) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionUpdateReceived(ImsCallSession.this, param1ImsCallProfile); 
    }
    
    public void callSessionConferenceExtended(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionConferenceExtended(ImsCallSession.this, new ImsCallSession(param1IImsCallSession), param1ImsCallProfile); 
    }
    
    public void callSessionConferenceExtendFailed(ImsReasonInfo param1ImsReasonInfo) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionConferenceExtendFailed(ImsCallSession.this, param1ImsReasonInfo); 
    }
    
    public void callSessionConferenceExtendReceived(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionConferenceExtendReceived(ImsCallSession.this, new ImsCallSession(param1IImsCallSession), param1ImsCallProfile); 
    }
    
    public void callSessionInviteParticipantsRequestDelivered() {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionInviteParticipantsRequestDelivered(ImsCallSession.this); 
    }
    
    public void callSessionInviteParticipantsRequestFailed(ImsReasonInfo param1ImsReasonInfo) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionInviteParticipantsRequestFailed(ImsCallSession.this, param1ImsReasonInfo); 
    }
    
    public void callSessionRemoveParticipantsRequestDelivered() {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionRemoveParticipantsRequestDelivered(ImsCallSession.this); 
    }
    
    public void callSessionRemoveParticipantsRequestFailed(ImsReasonInfo param1ImsReasonInfo) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionRemoveParticipantsRequestFailed(ImsCallSession.this, param1ImsReasonInfo); 
    }
    
    public void callSessionConferenceStateUpdated(ImsConferenceState param1ImsConferenceState) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionConferenceStateUpdated(ImsCallSession.this, param1ImsConferenceState); 
    }
    
    public void callSessionUssdMessageReceived(int param1Int, String param1String) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionUssdMessageReceived(ImsCallSession.this, param1Int, param1String); 
    }
    
    public void callSessionMayHandover(int param1Int1, int param1Int2) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionMayHandover(ImsCallSession.this, param1Int1, param1Int2); 
    }
    
    public void callSessionHandover(int param1Int1, int param1Int2, ImsReasonInfo param1ImsReasonInfo) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionHandover(ImsCallSession.this, param1Int1, param1Int2, param1ImsReasonInfo); 
    }
    
    public void callSessionHandoverFailed(int param1Int1, int param1Int2, ImsReasonInfo param1ImsReasonInfo) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionHandoverFailed(ImsCallSession.this, param1Int1, param1Int2, param1ImsReasonInfo); 
    }
    
    public void callSessionTtyModeReceived(int param1Int) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionTtyModeReceived(ImsCallSession.this, param1Int); 
    }
    
    public void callSessionMultipartyStateChanged(boolean param1Boolean) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionMultipartyStateChanged(ImsCallSession.this, param1Boolean); 
    }
    
    public void callSessionSuppServiceReceived(ImsSuppServiceNotification param1ImsSuppServiceNotification) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionSuppServiceReceived(ImsCallSession.this, param1ImsSuppServiceNotification); 
    }
    
    public void callSessionRttModifyRequestReceived(ImsCallProfile param1ImsCallProfile) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionRttModifyRequestReceived(ImsCallSession.this, param1ImsCallProfile); 
    }
    
    public void callSessionRttModifyResponseReceived(int param1Int) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionRttModifyResponseReceived(param1Int); 
    }
    
    public void callSessionRttMessageReceived(String param1String) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionRttMessageReceived(param1String); 
    }
    
    public void callSessionRttAudioIndicatorChanged(ImsStreamMediaProfile param1ImsStreamMediaProfile) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionRttAudioIndicatorChanged(param1ImsStreamMediaProfile); 
    }
    
    public void callSessionTransferred() {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionTransferred(ImsCallSession.this); 
    }
    
    public void callSessionTransferFailed(ImsReasonInfo param1ImsReasonInfo) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callSessionTransferFailed(ImsCallSession.this, param1ImsReasonInfo); 
    }
    
    public void callQualityChanged(CallQuality param1CallQuality) {
      if (ImsCallSession.this.mListener != null)
        ImsCallSession.this.mListener.callQualityChanged(param1CallQuality); 
    }
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[ImsCallSession objId:");
    stringBuilder.append(System.identityHashCode(this));
    stringBuilder.append(" state:");
    stringBuilder.append(State.toString(getState()));
    stringBuilder.append(" callId:");
    stringBuilder.append(getCallId());
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
}
