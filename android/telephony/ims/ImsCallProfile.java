package android.telephony.ims;

import android.annotation.SystemApi;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.telecom.VideoProfile;
import android.telephony.emergency.EmergencyNumber;
import android.util.Log;
import com.android.internal.telephony.util.TelephonyUtils;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

@SystemApi
public final class ImsCallProfile implements Parcelable {
  public int mRestrictCause = 0;
  
  private int mEmergencyServiceCategories = 0;
  
  private List<String> mEmergencyUrns = new ArrayList<>();
  
  private int mEmergencyCallRouting = 0;
  
  private boolean mEmergencyCallTesting = false;
  
  private boolean mHasKnownUserIntentEmergency = false;
  
  public ImsCallProfile(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public ImsCallProfile() {
    this.mServiceType = 1;
    this.mCallType = 1;
    this.mCallExtras = new Bundle();
    this.mMediaProfile = new ImsStreamMediaProfile();
  }
  
  public ImsCallProfile(int paramInt1, int paramInt2) {
    this.mServiceType = paramInt1;
    this.mCallType = paramInt2;
    this.mCallExtras = new Bundle();
    this.mMediaProfile = new ImsStreamMediaProfile();
  }
  
  public ImsCallProfile(int paramInt1, int paramInt2, Bundle paramBundle, ImsStreamMediaProfile paramImsStreamMediaProfile) {
    this.mServiceType = paramInt1;
    this.mCallType = paramInt2;
    this.mCallExtras = paramBundle;
    this.mMediaProfile = paramImsStreamMediaProfile;
  }
  
  public String getCallExtra(String paramString) {
    return getCallExtra(paramString, "");
  }
  
  public String getCallExtra(String paramString1, String paramString2) {
    Bundle bundle = this.mCallExtras;
    if (bundle == null)
      return paramString2; 
    return bundle.getString(paramString1, paramString2);
  }
  
  public boolean getCallExtraBoolean(String paramString) {
    return getCallExtraBoolean(paramString, false);
  }
  
  public boolean getCallExtraBoolean(String paramString, boolean paramBoolean) {
    Bundle bundle = this.mCallExtras;
    if (bundle == null)
      return paramBoolean; 
    return bundle.getBoolean(paramString, paramBoolean);
  }
  
  public int getCallExtraInt(String paramString) {
    return getCallExtraInt(paramString, -1);
  }
  
  public int getCallExtraInt(String paramString, int paramInt) {
    Bundle bundle = this.mCallExtras;
    if (bundle == null)
      return paramInt; 
    return bundle.getInt(paramString, paramInt);
  }
  
  public void setCallExtra(String paramString1, String paramString2) {
    Bundle bundle = this.mCallExtras;
    if (bundle != null)
      bundle.putString(paramString1, paramString2); 
  }
  
  public void setCallExtraBoolean(String paramString, boolean paramBoolean) {
    Bundle bundle = this.mCallExtras;
    if (bundle != null)
      bundle.putBoolean(paramString, paramBoolean); 
  }
  
  public void setCallExtraInt(String paramString, int paramInt) {
    Bundle bundle = this.mCallExtras;
    if (bundle != null)
      bundle.putInt(paramString, paramInt); 
  }
  
  public void setCallRestrictCause(int paramInt) {
    this.mRestrictCause = paramInt;
  }
  
  public void updateCallType(ImsCallProfile paramImsCallProfile) {
    this.mCallType = paramImsCallProfile.mCallType;
  }
  
  public void updateCallExtras(ImsCallProfile paramImsCallProfile) {
    this.mCallExtras.clear();
    this.mCallExtras = (Bundle)paramImsCallProfile.mCallExtras.clone();
  }
  
  public void updateMediaProfile(ImsCallProfile paramImsCallProfile) {
    this.mMediaProfile = paramImsCallProfile.mMediaProfile;
  }
  
  public void setCallerNumberVerificationStatus(int paramInt) {
    this.mCallerNumberVerificationStatus = paramInt;
  }
  
  public int getCallerNumberVerificationStatus() {
    return this.mCallerNumberVerificationStatus;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{ serviceType=");
    stringBuilder.append(this.mServiceType);
    stringBuilder.append(", callType=");
    stringBuilder.append(this.mCallType);
    stringBuilder.append(", restrictCause=");
    stringBuilder.append(this.mRestrictCause);
    stringBuilder.append(", mediaProfile=");
    ImsStreamMediaProfile imsStreamMediaProfile = this.mMediaProfile;
    if (imsStreamMediaProfile != null) {
      null = imsStreamMediaProfile.toString();
    } else {
      null = "null";
    } 
    stringBuilder.append(null);
    stringBuilder.append(", emergencyServiceCategories=");
    stringBuilder.append(this.mEmergencyServiceCategories);
    stringBuilder.append(", emergencyUrns=");
    stringBuilder.append(this.mEmergencyUrns);
    stringBuilder.append(", emergencyCallRouting=");
    stringBuilder.append(this.mEmergencyCallRouting);
    stringBuilder.append(", emergencyCallTesting=");
    stringBuilder.append(this.mEmergencyCallTesting);
    stringBuilder.append(", hasKnownUserIntentEmergency=");
    stringBuilder.append(this.mHasKnownUserIntentEmergency);
    stringBuilder.append(", mRestrictCause=");
    stringBuilder.append(this.mRestrictCause);
    stringBuilder.append(", mCallerNumberVerstat= ");
    stringBuilder.append(this.mCallerNumberVerificationStatus);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    Bundle bundle = maybeCleanseExtras(this.mCallExtras);
    paramParcel.writeInt(this.mServiceType);
    paramParcel.writeInt(this.mCallType);
    paramParcel.writeBundle(bundle);
    paramParcel.writeParcelable(this.mMediaProfile, 0);
    paramParcel.writeInt(this.mEmergencyServiceCategories);
    paramParcel.writeStringList(this.mEmergencyUrns);
    paramParcel.writeInt(this.mEmergencyCallRouting);
    paramParcel.writeBoolean(this.mEmergencyCallTesting);
    paramParcel.writeBoolean(this.mHasKnownUserIntentEmergency);
    paramParcel.writeInt(this.mRestrictCause);
    paramParcel.writeInt(this.mCallerNumberVerificationStatus);
  }
  
  private void readFromParcel(Parcel paramParcel) {
    this.mServiceType = paramParcel.readInt();
    this.mCallType = paramParcel.readInt();
    this.mCallExtras = paramParcel.readBundle();
    this.mMediaProfile = (ImsStreamMediaProfile)paramParcel.readParcelable(ImsStreamMediaProfile.class.getClassLoader());
    this.mEmergencyServiceCategories = paramParcel.readInt();
    this.mEmergencyUrns = paramParcel.createStringArrayList();
    this.mEmergencyCallRouting = paramParcel.readInt();
    this.mEmergencyCallTesting = paramParcel.readBoolean();
    this.mHasKnownUserIntentEmergency = paramParcel.readBoolean();
    this.mRestrictCause = paramParcel.readInt();
    this.mCallerNumberVerificationStatus = paramParcel.readInt();
  }
  
  public static final Parcelable.Creator<ImsCallProfile> CREATOR = new Parcelable.Creator<ImsCallProfile>() {
      public ImsCallProfile createFromParcel(Parcel param1Parcel) {
        return new ImsCallProfile(param1Parcel);
      }
      
      public ImsCallProfile[] newArray(int param1Int) {
        return new ImsCallProfile[param1Int];
      }
    };
  
  public static final int CALL_RESTRICT_CAUSE_DISABLED = 2;
  
  public static final int CALL_RESTRICT_CAUSE_HD = 3;
  
  public static final int CALL_RESTRICT_CAUSE_NONE = 0;
  
  public static final int CALL_RESTRICT_CAUSE_RAT = 1;
  
  public static final int CALL_TYPE_UNKNOWN = -1;
  
  public static final int CALL_TYPE_VIDEO_N_VOICE = 3;
  
  public static final int CALL_TYPE_VOICE = 2;
  
  public static final int CALL_TYPE_VOICE_N_VIDEO = 1;
  
  public static final int CALL_TYPE_VS = 8;
  
  public static final int CALL_TYPE_VS_RX = 10;
  
  public static final int CALL_TYPE_VS_TX = 9;
  
  public static final int CALL_TYPE_VT = 4;
  
  public static final int CALL_TYPE_VT_NODIR = 7;
  
  public static final int CALL_TYPE_VT_RX = 6;
  
  public static final int CALL_TYPE_VT_TX = 5;
  
  public static final int DIALSTRING_NORMAL = 0;
  
  public static final int DIALSTRING_SS_CONF = 1;
  
  public static final int DIALSTRING_USSD = 2;
  
  public static final String EXTRA_ADDITIONAL_CALL_INFO = "AdditionalCallInfo";
  
  public static final String EXTRA_ADDITIONAL_SIP_INVITE_FIELDS = "android.telephony.ims.extra.ADDITIONAL_SIP_INVITE_FIELDS";
  
  public static final String EXTRA_CALL_DISCONNECT_CAUSE = "android.telephony.ims.extra.CALL_DISCONNECT_CAUSE";
  
  public static final String EXTRA_CALL_MODE_CHANGEABLE = "call_mode_changeable";
  
  public static final String EXTRA_CALL_NETWORK_TYPE = "android.telephony.ims.extra.CALL_NETWORK_TYPE";
  
  @Deprecated
  public static final String EXTRA_CALL_RAT_TYPE = "CallRadioTech";
  
  @Deprecated
  public static final String EXTRA_CALL_RAT_TYPE_ALT = "callRadioTech";
  
  public static final String EXTRA_CHILD_NUMBER = "ChildNum";
  
  public static final String EXTRA_CNA = "cna";
  
  public static final String EXTRA_CNAP = "cnap";
  
  public static final String EXTRA_CODEC = "Codec";
  
  public static final String EXTRA_CONFERENCE = "conference";
  
  public static final String EXTRA_CONFERENCE_AVAIL = "conference_avail";
  
  public static final String EXTRA_DIALSTRING = "dialstring";
  
  public static final String EXTRA_DISPLAY_TEXT = "DisplayText";
  
  public static final String EXTRA_EMERGENCY_CALL = "e_call";
  
  public static final String EXTRA_FORWARDED_NUMBER = "android.telephony.ims.extra.FORWARDED_NUMBER";
  
  public static final String EXTRA_IS_CALL_PULL = "CallPull";
  
  public static final String EXTRA_OEM_EXTRAS = "android.telephony.ims.extra.OEM_EXTRAS";
  
  public static final String EXTRA_OI = "oi";
  
  public static final String EXTRA_OIR = "oir";
  
  public static final String EXTRA_REMOTE_URI = "remote_uri";
  
  public static final String EXTRA_RETRY_CALL_FAIL_NETWORKTYPE = "android.telephony.ims.extra.RETRY_CALL_FAIL_NETWORKTYPE";
  
  public static final String EXTRA_RETRY_CALL_FAIL_REASON = "android.telephony.ims.extra.RETRY_CALL_FAIL_REASON";
  
  public static final String EXTRA_USSD = "ussd";
  
  public static final String EXTRA_VMS = "vms";
  
  public static final int OIR_DEFAULT = 0;
  
  public static final int OIR_PRESENTATION_NOT_RESTRICTED = 2;
  
  public static final int OIR_PRESENTATION_PAYPHONE = 4;
  
  public static final int OIR_PRESENTATION_RESTRICTED = 1;
  
  public static final int OIR_PRESENTATION_UNKNOWN = 3;
  
  public static final int SERVICE_TYPE_EMERGENCY = 2;
  
  public static final int SERVICE_TYPE_NONE = 0;
  
  public static final int SERVICE_TYPE_NORMAL = 1;
  
  private static final String TAG = "ImsCallProfile";
  
  public static final int VERIFICATION_STATUS_FAILED = 2;
  
  public static final int VERIFICATION_STATUS_NOT_VERIFIED = 0;
  
  public static final int VERIFICATION_STATUS_PASSED = 1;
  
  public Bundle mCallExtras;
  
  public int mCallType;
  
  private int mCallerNumberVerificationStatus;
  
  public ImsStreamMediaProfile mMediaProfile;
  
  public int mServiceType;
  
  public int getServiceType() {
    return this.mServiceType;
  }
  
  public int getCallType() {
    return this.mCallType;
  }
  
  public int getRestrictCause() {
    return this.mRestrictCause;
  }
  
  public Bundle getCallExtras() {
    return this.mCallExtras;
  }
  
  public Bundle getProprietaryCallExtras() {
    Bundle bundle = this.mCallExtras;
    if (bundle == null)
      return new Bundle(); 
    bundle = bundle.getBundle("android.telephony.ims.extra.OEM_EXTRAS");
    if (bundle == null)
      return new Bundle(); 
    return new Bundle(bundle);
  }
  
  public ImsStreamMediaProfile getMediaProfile() {
    return this.mMediaProfile;
  }
  
  public static int getVideoStateFromImsCallProfile(ImsCallProfile paramImsCallProfile) {
    int i = getVideoStateFromCallType(paramImsCallProfile.mCallType);
    if (paramImsCallProfile.isVideoPaused() && !VideoProfile.isAudioOnly(i)) {
      i |= 0x4;
    } else {
      i &= 0xFFFFFFFB;
    } 
    return i;
  }
  
  public static int getVideoStateFromCallType(int paramInt) {
    if (paramInt != 2) {
      if (paramInt != 4) {
        if (paramInt != 5) {
          if (paramInt != 6) {
            paramInt = 0;
          } else {
            paramInt = 2;
          } 
        } else {
          paramInt = 1;
        } 
      } else {
        paramInt = 3;
      } 
    } else {
      paramInt = 0;
    } 
    return paramInt;
  }
  
  public static int getCallTypeFromVideoState(int paramInt) {
    boolean bool1 = isVideoStateSet(paramInt, 1);
    boolean bool2 = isVideoStateSet(paramInt, 2);
    boolean bool3 = isVideoStateSet(paramInt, 4);
    if (bool3)
      return 7; 
    if (bool1 && !bool2)
      return 5; 
    if (!bool1 && bool2)
      return 6; 
    if (bool1 && bool2)
      return 4; 
    return 2;
  }
  
  public static int presentationToOIR(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 3) {
          if (paramInt != 4)
            return 0; 
          return 4;
        } 
        return 3;
      } 
      return 1;
    } 
    return 2;
  }
  
  public static int presentationToOir(int paramInt) {
    return presentationToOIR(paramInt);
  }
  
  public static int OIRToPresentation(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 4)
          return 3; 
        return 4;
      } 
      return 1;
    } 
    return 2;
  }
  
  public boolean isVideoPaused() {
    boolean bool;
    if (this.mMediaProfile.mVideoDirection == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isVideoCall() {
    return VideoProfile.isVideo(getVideoStateFromCallType(this.mCallType));
  }
  
  private Bundle maybeCleanseExtras(Bundle paramBundle) {
    if (paramBundle == null)
      return null; 
    int i = paramBundle.size();
    Bundle bundle = TelephonyUtils.filterValues(paramBundle);
    int j = bundle.size();
    if (i != j) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("maybeCleanseExtras: ");
      stringBuilder.append(i - j);
      stringBuilder.append(" extra values were removed - only primitive types and system parcelables are permitted.");
      Log.i("ImsCallProfile", stringBuilder.toString());
    } 
    return bundle;
  }
  
  private static boolean isVideoStateSet(int paramInt1, int paramInt2) {
    boolean bool;
    if ((paramInt1 & paramInt2) == paramInt2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setEmergencyCallInfo(EmergencyNumber paramEmergencyNumber, boolean paramBoolean) {
    boolean bool;
    setEmergencyServiceCategories(paramEmergencyNumber.getEmergencyServiceCategoryBitmaskInternalDial());
    setEmergencyUrns(paramEmergencyNumber.getEmergencyUrns());
    setEmergencyCallRouting(paramEmergencyNumber.getEmergencyCallRouting());
    if (paramEmergencyNumber.getEmergencyNumberSourceBitmask() == 32) {
      bool = true;
    } else {
      bool = false;
    } 
    setEmergencyCallTesting(bool);
    setHasKnownUserIntentEmergency(paramBoolean);
  }
  
  public void setEmergencyServiceCategories(int paramInt) {
    this.mEmergencyServiceCategories = paramInt;
  }
  
  public void setEmergencyUrns(List<String> paramList) {
    this.mEmergencyUrns = paramList;
  }
  
  public void setEmergencyCallRouting(int paramInt) {
    this.mEmergencyCallRouting = paramInt;
  }
  
  public void setEmergencyCallTesting(boolean paramBoolean) {
    this.mEmergencyCallTesting = paramBoolean;
  }
  
  public void setHasKnownUserIntentEmergency(boolean paramBoolean) {
    this.mHasKnownUserIntentEmergency = paramBoolean;
  }
  
  public int getEmergencyServiceCategories() {
    return this.mEmergencyServiceCategories;
  }
  
  public List<String> getEmergencyUrns() {
    return this.mEmergencyUrns;
  }
  
  public int getEmergencyCallRouting() {
    return this.mEmergencyCallRouting;
  }
  
  public boolean isEmergencyCallTesting() {
    return this.mEmergencyCallTesting;
  }
  
  public boolean hasKnownUserIntentEmergency() {
    return this.mHasKnownUserIntentEmergency;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class CallRestrictCause implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class VerificationStatus implements Annotation {}
}
