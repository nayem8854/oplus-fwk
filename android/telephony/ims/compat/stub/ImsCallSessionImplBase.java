package android.telephony.ims.compat.stub;

import android.os.Message;
import android.os.RemoteException;
import android.telephony.CallQuality;
import android.telephony.ServiceState;
import android.telephony.ims.ImsCallProfile;
import android.telephony.ims.ImsConferenceState;
import android.telephony.ims.ImsReasonInfo;
import android.telephony.ims.ImsStreamMediaProfile;
import android.telephony.ims.ImsSuppServiceNotification;
import android.telephony.ims.aidl.IImsCallSessionListener;
import com.android.ims.internal.IImsCallSession;
import com.android.ims.internal.IImsCallSessionListener;
import com.android.ims.internal.IImsVideoCallProvider;

public class ImsCallSessionImplBase extends IImsCallSession.Stub {
  public final void setListener(IImsCallSessionListener paramIImsCallSessionListener) throws RemoteException {
    setListener(new ImsCallSessionListenerConverter(paramIImsCallSessionListener));
  }
  
  public void setListener(IImsCallSessionListener paramIImsCallSessionListener) {}
  
  public void close() {}
  
  public String getCallId() {
    return null;
  }
  
  public ImsCallProfile getCallProfile() {
    return null;
  }
  
  public ImsCallProfile getLocalCallProfile() {
    return null;
  }
  
  public ImsCallProfile getRemoteCallProfile() {
    return null;
  }
  
  public String getProperty(String paramString) {
    return null;
  }
  
  public int getState() {
    return -1;
  }
  
  public boolean isInCall() {
    return false;
  }
  
  public void setMute(boolean paramBoolean) {}
  
  public void start(String paramString, ImsCallProfile paramImsCallProfile) {}
  
  public void startConference(String[] paramArrayOfString, ImsCallProfile paramImsCallProfile) {}
  
  public void accept(int paramInt, ImsStreamMediaProfile paramImsStreamMediaProfile) {}
  
  public void deflect(String paramString) {}
  
  public void transfer(String paramString, boolean paramBoolean) {}
  
  public void consultativeTransfer(IImsCallSession paramIImsCallSession) {}
  
  public void reject(int paramInt) {}
  
  public void terminate(int paramInt) {}
  
  public void hold(ImsStreamMediaProfile paramImsStreamMediaProfile) {}
  
  public void resume(ImsStreamMediaProfile paramImsStreamMediaProfile) {}
  
  public void merge() {}
  
  public void update(int paramInt, ImsStreamMediaProfile paramImsStreamMediaProfile) {}
  
  public void extendToConference(String[] paramArrayOfString) {}
  
  public void inviteParticipants(String[] paramArrayOfString) {}
  
  public void removeParticipants(String[] paramArrayOfString) {}
  
  public void sendDtmf(char paramChar, Message paramMessage) {}
  
  public void startDtmf(char paramChar) {}
  
  public void stopDtmf() {}
  
  public void sendUssd(String paramString) {}
  
  public IImsVideoCallProvider getVideoCallProvider() {
    return null;
  }
  
  public boolean isMultiparty() {
    return false;
  }
  
  public void sendRttModifyRequest(ImsCallProfile paramImsCallProfile) {}
  
  public void sendRttModifyResponse(boolean paramBoolean) {}
  
  public void sendRttMessage(String paramString) {}
  
  private class ImsCallSessionListenerConverter extends IImsCallSessionListener.Stub {
    private final IImsCallSessionListener mNewListener;
    
    final ImsCallSessionImplBase this$0;
    
    public ImsCallSessionListenerConverter(IImsCallSessionListener param1IImsCallSessionListener) {
      this.mNewListener = param1IImsCallSessionListener;
    }
    
    public void callSessionProgressing(IImsCallSession param1IImsCallSession, ImsStreamMediaProfile param1ImsStreamMediaProfile) throws RemoteException {
      this.mNewListener.callSessionProgressing(param1ImsStreamMediaProfile);
    }
    
    public void callSessionStarted(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) throws RemoteException {
      this.mNewListener.callSessionInitiated(param1ImsCallProfile);
    }
    
    public void callSessionStartFailed(IImsCallSession param1IImsCallSession, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {
      this.mNewListener.callSessionInitiatedFailed(param1ImsReasonInfo);
    }
    
    public void callSessionTerminated(IImsCallSession param1IImsCallSession, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {
      this.mNewListener.callSessionTerminated(param1ImsReasonInfo);
    }
    
    public void callSessionHeld(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) throws RemoteException {
      this.mNewListener.callSessionHeld(param1ImsCallProfile);
    }
    
    public void callSessionHoldFailed(IImsCallSession param1IImsCallSession, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {
      this.mNewListener.callSessionHoldFailed(param1ImsReasonInfo);
    }
    
    public void callSessionHoldReceived(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) throws RemoteException {
      this.mNewListener.callSessionHoldReceived(param1ImsCallProfile);
    }
    
    public void callSessionResumed(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) throws RemoteException {
      this.mNewListener.callSessionResumed(param1ImsCallProfile);
    }
    
    public void callSessionResumeFailed(IImsCallSession param1IImsCallSession, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {
      this.mNewListener.callSessionResumeFailed(param1ImsReasonInfo);
    }
    
    public void callSessionResumeReceived(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) throws RemoteException {
      this.mNewListener.callSessionResumeReceived(param1ImsCallProfile);
    }
    
    public void callSessionMergeStarted(IImsCallSession param1IImsCallSession1, IImsCallSession param1IImsCallSession2, ImsCallProfile param1ImsCallProfile) throws RemoteException {
      this.mNewListener.callSessionMergeStarted(param1IImsCallSession2, param1ImsCallProfile);
    }
    
    public void callSessionMergeComplete(IImsCallSession param1IImsCallSession) throws RemoteException {
      this.mNewListener.callSessionMergeComplete(param1IImsCallSession);
    }
    
    public void callSessionMergeFailed(IImsCallSession param1IImsCallSession, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {
      this.mNewListener.callSessionMergeFailed(param1ImsReasonInfo);
    }
    
    public void callSessionUpdated(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) throws RemoteException {
      this.mNewListener.callSessionUpdated(param1ImsCallProfile);
    }
    
    public void callSessionUpdateFailed(IImsCallSession param1IImsCallSession, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {
      this.mNewListener.callSessionUpdateFailed(param1ImsReasonInfo);
    }
    
    public void callSessionUpdateReceived(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) throws RemoteException {
      this.mNewListener.callSessionUpdateReceived(param1ImsCallProfile);
    }
    
    public void callSessionConferenceExtended(IImsCallSession param1IImsCallSession1, IImsCallSession param1IImsCallSession2, ImsCallProfile param1ImsCallProfile) throws RemoteException {
      this.mNewListener.callSessionConferenceExtended(param1IImsCallSession2, param1ImsCallProfile);
    }
    
    public void callSessionConferenceExtendFailed(IImsCallSession param1IImsCallSession, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {
      this.mNewListener.callSessionConferenceExtendFailed(param1ImsReasonInfo);
    }
    
    public void callSessionConferenceExtendReceived(IImsCallSession param1IImsCallSession1, IImsCallSession param1IImsCallSession2, ImsCallProfile param1ImsCallProfile) throws RemoteException {
      this.mNewListener.callSessionConferenceExtendReceived(param1IImsCallSession2, param1ImsCallProfile);
    }
    
    public void callSessionInviteParticipantsRequestDelivered(IImsCallSession param1IImsCallSession) throws RemoteException {
      this.mNewListener.callSessionInviteParticipantsRequestDelivered();
    }
    
    public void callSessionInviteParticipantsRequestFailed(IImsCallSession param1IImsCallSession, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {
      this.mNewListener.callSessionInviteParticipantsRequestFailed(param1ImsReasonInfo);
    }
    
    public void callSessionRemoveParticipantsRequestDelivered(IImsCallSession param1IImsCallSession) throws RemoteException {
      this.mNewListener.callSessionRemoveParticipantsRequestDelivered();
    }
    
    public void callSessionRemoveParticipantsRequestFailed(IImsCallSession param1IImsCallSession, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {
      this.mNewListener.callSessionRemoveParticipantsRequestFailed(param1ImsReasonInfo);
    }
    
    public void callSessionConferenceStateUpdated(IImsCallSession param1IImsCallSession, ImsConferenceState param1ImsConferenceState) throws RemoteException {
      this.mNewListener.callSessionConferenceStateUpdated(param1ImsConferenceState);
    }
    
    public void callSessionUssdMessageReceived(IImsCallSession param1IImsCallSession, int param1Int, String param1String) throws RemoteException {
      this.mNewListener.callSessionUssdMessageReceived(param1Int, param1String);
    }
    
    public void callSessionHandover(IImsCallSession param1IImsCallSession, int param1Int1, int param1Int2, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {
      IImsCallSessionListener iImsCallSessionListener = this.mNewListener;
      param1Int1 = ServiceState.rilRadioTechnologyToNetworkType(param1Int1);
      param1Int2 = ServiceState.rilRadioTechnologyToNetworkType(param1Int2);
      iImsCallSessionListener.callSessionHandover(param1Int1, param1Int2, param1ImsReasonInfo);
    }
    
    public void callSessionHandoverFailed(IImsCallSession param1IImsCallSession, int param1Int1, int param1Int2, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {
      IImsCallSessionListener iImsCallSessionListener = this.mNewListener;
      param1Int1 = ServiceState.rilRadioTechnologyToNetworkType(param1Int1);
      param1Int2 = ServiceState.rilRadioTechnologyToNetworkType(param1Int2);
      iImsCallSessionListener.callSessionHandoverFailed(param1Int1, param1Int2, param1ImsReasonInfo);
    }
    
    public void callSessionMayHandover(IImsCallSession param1IImsCallSession, int param1Int1, int param1Int2) throws RemoteException {
      IImsCallSessionListener iImsCallSessionListener = this.mNewListener;
      param1Int1 = ServiceState.rilRadioTechnologyToNetworkType(param1Int1);
      param1Int2 = ServiceState.rilRadioTechnologyToNetworkType(param1Int2);
      iImsCallSessionListener.callSessionMayHandover(param1Int1, param1Int2);
    }
    
    public void callSessionTtyModeReceived(IImsCallSession param1IImsCallSession, int param1Int) throws RemoteException {
      this.mNewListener.callSessionTtyModeReceived(param1Int);
    }
    
    public void callSessionMultipartyStateChanged(IImsCallSession param1IImsCallSession, boolean param1Boolean) throws RemoteException {
      this.mNewListener.callSessionMultipartyStateChanged(param1Boolean);
    }
    
    public void callSessionSuppServiceReceived(IImsCallSession param1IImsCallSession, ImsSuppServiceNotification param1ImsSuppServiceNotification) throws RemoteException {
      this.mNewListener.callSessionSuppServiceReceived(param1ImsSuppServiceNotification);
    }
    
    public void callSessionRttModifyRequestReceived(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) throws RemoteException {
      this.mNewListener.callSessionRttModifyRequestReceived(param1ImsCallProfile);
    }
    
    public void callSessionRttModifyResponseReceived(int param1Int) throws RemoteException {
      this.mNewListener.callSessionRttModifyResponseReceived(param1Int);
    }
    
    public void callSessionRttMessageReceived(String param1String) throws RemoteException {
      this.mNewListener.callSessionRttMessageReceived(param1String);
    }
    
    public void callSessionRttAudioIndicatorChanged(ImsStreamMediaProfile param1ImsStreamMediaProfile) throws RemoteException {
      this.mNewListener.callSessionRttAudioIndicatorChanged(param1ImsStreamMediaProfile);
    }
    
    public void callSessionTransferred() throws RemoteException {
      this.mNewListener.callSessionTransferred();
    }
    
    public void callSessionTransferFailed(ImsReasonInfo param1ImsReasonInfo) throws RemoteException {
      this.mNewListener.callSessionTransferFailed(param1ImsReasonInfo);
    }
    
    public void callQualityChanged(CallQuality param1CallQuality) throws RemoteException {
      this.mNewListener.callQualityChanged(param1CallQuality);
    }
  }
}
