package android.telephony.ims.compat.stub;

import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import com.android.ims.ImsConfigListener;
import com.android.ims.internal.IImsConfig;
import java.lang.ref.WeakReference;
import java.util.HashMap;

public class ImsConfigImplBase {
  private static final String TAG = "ImsConfigImplBase";
  
  ImsConfigStub mImsConfigStub;
  
  public ImsConfigImplBase(Context paramContext) {
    this.mImsConfigStub = new ImsConfigStub(this, paramContext);
  }
  
  public int getProvisionedValue(int paramInt) throws RemoteException {
    return -1;
  }
  
  public String getProvisionedStringValue(int paramInt) throws RemoteException {
    return null;
  }
  
  public int setProvisionedValue(int paramInt1, int paramInt2) throws RemoteException {
    return 1;
  }
  
  public int setProvisionedStringValue(int paramInt, String paramString) throws RemoteException {
    return 1;
  }
  
  public void getFeatureValue(int paramInt1, int paramInt2, ImsConfigListener paramImsConfigListener) throws RemoteException {}
  
  public void setFeatureValue(int paramInt1, int paramInt2, int paramInt3, ImsConfigListener paramImsConfigListener) throws RemoteException {}
  
  public boolean getVolteProvisioned() throws RemoteException {
    return false;
  }
  
  public void getVideoQuality(ImsConfigListener paramImsConfigListener) throws RemoteException {}
  
  public void setVideoQuality(int paramInt, ImsConfigListener paramImsConfigListener) throws RemoteException {}
  
  public IImsConfig getIImsConfig() {
    return this.mImsConfigStub;
  }
  
  public final void notifyProvisionedValueChanged(int paramInt1, int paramInt2) {
    this.mImsConfigStub.updateCachedValue(paramInt1, paramInt2, true);
  }
  
  public final void notifyProvisionedValueChanged(int paramInt, String paramString) {
    this.mImsConfigStub.updateCachedValue(paramInt, paramString, true);
  }
  
  class ImsConfigStub extends IImsConfig.Stub {
    Context mContext;
    
    WeakReference<ImsConfigImplBase> mImsConfigImplBaseWeakReference;
    
    private HashMap<Integer, Integer> mProvisionedIntValue = new HashMap<>();
    
    private HashMap<Integer, String> mProvisionedStringValue = new HashMap<>();
    
    public ImsConfigStub(ImsConfigImplBase this$0, Context param1Context) {
      this.mContext = param1Context;
      this.mImsConfigImplBaseWeakReference = new WeakReference<>(this$0);
    }
    
    public int getProvisionedValue(int param1Int) throws RemoteException {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mProvisionedIntValue : Ljava/util/HashMap;
      //   6: iload_1
      //   7: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   10: invokevirtual containsKey : (Ljava/lang/Object;)Z
      //   13: ifeq -> 38
      //   16: aload_0
      //   17: getfield mProvisionedIntValue : Ljava/util/HashMap;
      //   20: iload_1
      //   21: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   24: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   27: checkcast java/lang/Integer
      //   30: invokevirtual intValue : ()I
      //   33: istore_1
      //   34: aload_0
      //   35: monitorexit
      //   36: iload_1
      //   37: ireturn
      //   38: aload_0
      //   39: invokespecial getImsConfigImpl : ()Landroid/telephony/ims/compat/stub/ImsConfigImplBase;
      //   42: iload_1
      //   43: invokevirtual getProvisionedValue : (I)I
      //   46: istore_2
      //   47: iload_2
      //   48: iconst_m1
      //   49: if_icmpeq -> 59
      //   52: aload_0
      //   53: iload_1
      //   54: iload_2
      //   55: iconst_0
      //   56: invokevirtual updateCachedValue : (IIZ)V
      //   59: aload_0
      //   60: monitorexit
      //   61: iload_2
      //   62: ireturn
      //   63: astore_3
      //   64: aload_0
      //   65: monitorexit
      //   66: aload_3
      //   67: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #236	-> 2
      //   #237	-> 16
      //   #239	-> 38
      //   #240	-> 47
      //   #241	-> 52
      //   #243	-> 59
      //   #235	-> 63
      // Exception table:
      //   from	to	target	type
      //   2	16	63	finally
      //   16	34	63	finally
      //   38	47	63	finally
      //   52	59	63	finally
    }
    
    public String getProvisionedStringValue(int param1Int) throws RemoteException {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mProvisionedIntValue : Ljava/util/HashMap;
      //   6: iload_1
      //   7: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   10: invokevirtual containsKey : (Ljava/lang/Object;)Z
      //   13: ifeq -> 35
      //   16: aload_0
      //   17: getfield mProvisionedStringValue : Ljava/util/HashMap;
      //   20: iload_1
      //   21: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   24: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   27: checkcast java/lang/String
      //   30: astore_2
      //   31: aload_0
      //   32: monitorexit
      //   33: aload_2
      //   34: areturn
      //   35: aload_0
      //   36: invokespecial getImsConfigImpl : ()Landroid/telephony/ims/compat/stub/ImsConfigImplBase;
      //   39: iload_1
      //   40: invokevirtual getProvisionedStringValue : (I)Ljava/lang/String;
      //   43: astore_2
      //   44: aload_2
      //   45: ifnull -> 55
      //   48: aload_0
      //   49: iload_1
      //   50: aload_2
      //   51: iconst_0
      //   52: invokevirtual updateCachedValue : (ILjava/lang/String;Z)V
      //   55: aload_0
      //   56: monitorexit
      //   57: aload_2
      //   58: areturn
      //   59: astore_2
      //   60: aload_0
      //   61: monitorexit
      //   62: aload_2
      //   63: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #257	-> 2
      //   #258	-> 16
      //   #260	-> 35
      //   #261	-> 44
      //   #262	-> 48
      //   #264	-> 55
      //   #256	-> 59
      // Exception table:
      //   from	to	target	type
      //   2	16	59	finally
      //   16	31	59	finally
      //   35	44	59	finally
      //   48	55	59	finally
    }
    
    public int setProvisionedValue(int param1Int1, int param1Int2) throws RemoteException {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mProvisionedIntValue : Ljava/util/HashMap;
      //   6: iload_1
      //   7: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   10: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
      //   13: pop
      //   14: aload_0
      //   15: invokespecial getImsConfigImpl : ()Landroid/telephony/ims/compat/stub/ImsConfigImplBase;
      //   18: iload_1
      //   19: iload_2
      //   20: invokevirtual setProvisionedValue : (II)I
      //   23: istore_3
      //   24: iload_3
      //   25: ifne -> 38
      //   28: aload_0
      //   29: iload_1
      //   30: iload_2
      //   31: iconst_1
      //   32: invokevirtual updateCachedValue : (IIZ)V
      //   35: goto -> 104
      //   38: new java/lang/StringBuilder
      //   41: astore #4
      //   43: aload #4
      //   45: invokespecial <init> : ()V
      //   48: aload #4
      //   50: ldc 'Set provision value of '
      //   52: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   55: pop
      //   56: aload #4
      //   58: iload_1
      //   59: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   62: pop
      //   63: aload #4
      //   65: ldc ' to '
      //   67: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   70: pop
      //   71: aload #4
      //   73: iload_2
      //   74: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   77: pop
      //   78: aload #4
      //   80: ldc ' failed with error code '
      //   82: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   85: pop
      //   86: aload #4
      //   88: iload_3
      //   89: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   92: pop
      //   93: ldc 'ImsConfigImplBase'
      //   95: aload #4
      //   97: invokevirtual toString : ()Ljava/lang/String;
      //   100: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   103: pop
      //   104: aload_0
      //   105: monitorexit
      //   106: iload_3
      //   107: ireturn
      //   108: astore #4
      //   110: aload_0
      //   111: monitorexit
      //   112: aload #4
      //   114: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #280	-> 2
      //   #281	-> 14
      //   #282	-> 24
      //   #283	-> 28
      //   #285	-> 38
      //   #289	-> 104
      //   #279	-> 108
      // Exception table:
      //   from	to	target	type
      //   2	14	108	finally
      //   14	24	108	finally
      //   28	35	108	finally
      //   38	104	108	finally
    }
    
    public int setProvisionedStringValue(int param1Int, String param1String) throws RemoteException {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mProvisionedStringValue : Ljava/util/HashMap;
      //   6: iload_1
      //   7: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   10: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
      //   13: pop
      //   14: aload_0
      //   15: invokespecial getImsConfigImpl : ()Landroid/telephony/ims/compat/stub/ImsConfigImplBase;
      //   18: iload_1
      //   19: aload_2
      //   20: invokevirtual setProvisionedStringValue : (ILjava/lang/String;)I
      //   23: istore_3
      //   24: iload_3
      //   25: ifne -> 35
      //   28: aload_0
      //   29: iload_1
      //   30: aload_2
      //   31: iconst_1
      //   32: invokevirtual updateCachedValue : (ILjava/lang/String;Z)V
      //   35: aload_0
      //   36: monitorexit
      //   37: iload_3
      //   38: ireturn
      //   39: astore_2
      //   40: aload_0
      //   41: monitorexit
      //   42: aload_2
      //   43: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #305	-> 2
      //   #306	-> 14
      //   #307	-> 24
      //   #308	-> 28
      //   #311	-> 35
      //   #304	-> 39
      // Exception table:
      //   from	to	target	type
      //   2	14	39	finally
      //   14	24	39	finally
      //   28	35	39	finally
    }
    
    public void getFeatureValue(int param1Int1, int param1Int2, ImsConfigListener param1ImsConfigListener) throws RemoteException {
      getImsConfigImpl().getFeatureValue(param1Int1, param1Int2, param1ImsConfigListener);
    }
    
    public void setFeatureValue(int param1Int1, int param1Int2, int param1Int3, ImsConfigListener param1ImsConfigListener) throws RemoteException {
      getImsConfigImpl().setFeatureValue(param1Int1, param1Int2, param1Int3, param1ImsConfigListener);
    }
    
    public boolean getVolteProvisioned() throws RemoteException {
      return getImsConfigImpl().getVolteProvisioned();
    }
    
    public void getVideoQuality(ImsConfigListener param1ImsConfigListener) throws RemoteException {
      getImsConfigImpl().getVideoQuality(param1ImsConfigListener);
    }
    
    public void setVideoQuality(int param1Int, ImsConfigListener param1ImsConfigListener) throws RemoteException {
      getImsConfigImpl().setVideoQuality(param1Int, param1ImsConfigListener);
    }
    
    private ImsConfigImplBase getImsConfigImpl() throws RemoteException {
      ImsConfigImplBase imsConfigImplBase = this.mImsConfigImplBaseWeakReference.get();
      if (imsConfigImplBase != null)
        return imsConfigImplBase; 
      throw new RemoteException("Fail to get ImsConfigImpl");
    }
    
    private void sendImsConfigChangedIntent(int param1Int1, int param1Int2) {
      sendImsConfigChangedIntent(param1Int1, Integer.toString(param1Int2));
    }
    
    private void sendImsConfigChangedIntent(int param1Int, String param1String) {
      Intent intent = new Intent("com.android.intent.action.IMS_CONFIG_CHANGED");
      intent.putExtra("item", param1Int);
      intent.putExtra("value", param1String);
      Context context = this.mContext;
      if (context != null)
        context.sendBroadcast(intent); 
    }
    
    protected void updateCachedValue(int param1Int1, int param1Int2, boolean param1Boolean) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mProvisionedIntValue : Ljava/util/HashMap;
      //   6: iload_1
      //   7: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   10: iload_2
      //   11: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   14: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   17: pop
      //   18: iload_3
      //   19: ifeq -> 28
      //   22: aload_0
      //   23: iload_1
      //   24: iload_2
      //   25: invokespecial sendImsConfigChangedIntent : (II)V
      //   28: aload_0
      //   29: monitorexit
      //   30: return
      //   31: astore #4
      //   33: aload_0
      //   34: monitorexit
      //   35: aload #4
      //   37: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #380	-> 2
      //   #381	-> 18
      //   #382	-> 22
      //   #384	-> 28
      //   #379	-> 31
      // Exception table:
      //   from	to	target	type
      //   2	18	31	finally
      //   22	28	31	finally
    }
    
    protected void updateCachedValue(int param1Int, String param1String, boolean param1Boolean) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mProvisionedStringValue : Ljava/util/HashMap;
      //   6: iload_1
      //   7: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   10: aload_2
      //   11: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   14: pop
      //   15: iload_3
      //   16: ifeq -> 25
      //   19: aload_0
      //   20: iload_1
      //   21: aload_2
      //   22: invokespecial sendImsConfigChangedIntent : (ILjava/lang/String;)V
      //   25: aload_0
      //   26: monitorexit
      //   27: return
      //   28: astore_2
      //   29: aload_0
      //   30: monitorexit
      //   31: aload_2
      //   32: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #388	-> 2
      //   #389	-> 15
      //   #390	-> 19
      //   #392	-> 25
      //   #387	-> 28
      // Exception table:
      //   from	to	target	type
      //   2	15	28	finally
      //   19	25	28	finally
    }
  }
}
