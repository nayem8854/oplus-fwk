package android.telephony.ims.compat.feature;

import android.content.Context;
import android.os.IInterface;
import android.os.RemoteException;
import android.util.Log;
import com.android.ims.internal.IImsFeatureStatusCallback;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.WeakHashMap;

public abstract class ImsFeature {
  private final Set<IImsFeatureStatusCallback> mStatusCallbacks = Collections.newSetFromMap(new WeakHashMap<>());
  
  private int mState = 0;
  
  private int mSlotId = -1;
  
  protected Context mContext;
  
  public static final int STATE_READY = 2;
  
  public static final int STATE_NOT_AVAILABLE = 0;
  
  public static final int STATE_INITIALIZING = 1;
  
  public static final int RCS = 2;
  
  public static final int MMTEL = 1;
  
  public static final int MAX = 3;
  
  private static final String LOG_TAG = "ImsFeature";
  
  public static final int INVALID = -1;
  
  public static final int EMERGENCY_MMTEL = 0;
  
  public void setContext(Context paramContext) {
    this.mContext = paramContext;
  }
  
  public void setSlotId(int paramInt) {
    this.mSlotId = paramInt;
  }
  
  public int getFeatureState() {
    return this.mState;
  }
  
  protected final void setFeatureState(int paramInt) {
    if (this.mState != paramInt) {
      this.mState = paramInt;
      notifyFeatureState(paramInt);
    } 
  }
  
  public void addImsFeatureStatusCallback(IImsFeatureStatusCallback paramIImsFeatureStatusCallback) {
    if (paramIImsFeatureStatusCallback == null)
      return; 
    try {
      paramIImsFeatureStatusCallback.notifyImsFeatureStatus(this.mState);
      synchronized (this.mStatusCallbacks) {
        this.mStatusCallbacks.add(paramIImsFeatureStatusCallback);
      } 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Couldn't notify feature state: ");
      stringBuilder.append(remoteException.getMessage());
      Log.w("ImsFeature", stringBuilder.toString());
    } 
  }
  
  public void removeImsFeatureStatusCallback(IImsFeatureStatusCallback paramIImsFeatureStatusCallback) {
    if (paramIImsFeatureStatusCallback == null)
      return; 
    synchronized (this.mStatusCallbacks) {
      this.mStatusCallbacks.remove(paramIImsFeatureStatusCallback);
      return;
    } 
  }
  
  private void notifyFeatureState(int paramInt) {
    synchronized (this.mStatusCallbacks) {
      Iterator<IImsFeatureStatusCallback> iterator = this.mStatusCallbacks.iterator();
      while (iterator.hasNext()) {
        IImsFeatureStatusCallback iImsFeatureStatusCallback = iterator.next();
        try {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("notifying ImsFeatureState=");
          stringBuilder.append(paramInt);
          Log.i("ImsFeature", stringBuilder.toString());
          iImsFeatureStatusCallback.notifyImsFeatureStatus(paramInt);
        } catch (RemoteException remoteException) {
          iterator.remove();
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Couldn't notify feature state: ");
          stringBuilder.append(remoteException.getMessage());
          Log.w("ImsFeature", stringBuilder.toString());
        } 
      } 
      return;
    } 
  }
  
  public abstract IInterface getBinder();
  
  public abstract void onFeatureReady();
  
  public abstract void onFeatureRemoved();
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ImsState {}
}
