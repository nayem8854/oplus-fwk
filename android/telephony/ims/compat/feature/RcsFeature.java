package android.telephony.ims.compat.feature;

import android.os.IInterface;
import com.android.ims.internal.IImsRcsFeature;

public class RcsFeature extends ImsFeature {
  private final IImsRcsFeature mImsRcsBinder = (IImsRcsFeature)new Object(this);
  
  public void onFeatureReady() {}
  
  public void onFeatureRemoved() {}
  
  public final IImsRcsFeature getBinder() {
    return this.mImsRcsBinder;
  }
}
