package android.telephony.ims.compat.feature;

import android.app.PendingIntent;
import android.os.IInterface;
import android.os.Message;
import android.telephony.ims.ImsCallProfile;
import android.telephony.ims.stub.ImsEcbmImplBase;
import android.telephony.ims.stub.ImsMultiEndpointImplBase;
import android.telephony.ims.stub.ImsUtImplBase;
import com.android.ims.internal.IImsCallSession;
import com.android.ims.internal.IImsCallSessionListener;
import com.android.ims.internal.IImsConfig;
import com.android.ims.internal.IImsMMTelFeature;
import com.android.ims.internal.IImsRegistrationListener;

public class MMTelFeature extends ImsFeature {
  private final IImsMMTelFeature mImsMMTelBinder;
  
  private final Object mLock = new Object();
  
  public MMTelFeature() {
    this.mImsMMTelBinder = (IImsMMTelFeature)new Object(this);
  }
  
  public final IImsMMTelFeature getBinder() {
    return this.mImsMMTelBinder;
  }
  
  public int startSession(PendingIntent paramPendingIntent, IImsRegistrationListener paramIImsRegistrationListener) {
    return 0;
  }
  
  public void endSession(int paramInt) {}
  
  public boolean isConnected(int paramInt1, int paramInt2) {
    return false;
  }
  
  public boolean isOpened() {
    return false;
  }
  
  public void addRegistrationListener(IImsRegistrationListener paramIImsRegistrationListener) {}
  
  public void removeRegistrationListener(IImsRegistrationListener paramIImsRegistrationListener) {}
  
  public ImsCallProfile createCallProfile(int paramInt1, int paramInt2, int paramInt3) {
    return null;
  }
  
  public IImsCallSession createCallSession(int paramInt, ImsCallProfile paramImsCallProfile, IImsCallSessionListener paramIImsCallSessionListener) {
    return null;
  }
  
  public IImsCallSession getPendingCallSession(int paramInt, String paramString) {
    return null;
  }
  
  public ImsUtImplBase getUtInterface() {
    return null;
  }
  
  public IImsConfig getConfigInterface() {
    return null;
  }
  
  public void turnOnIms() {}
  
  public void turnOffIms() {}
  
  public ImsEcbmImplBase getEcbmInterface() {
    return null;
  }
  
  public void setUiTTYMode(int paramInt, Message paramMessage) {}
  
  public ImsMultiEndpointImplBase getMultiEndpointInterface() {
    return null;
  }
  
  public void onFeatureReady() {}
  
  public void onFeatureRemoved() {}
}
