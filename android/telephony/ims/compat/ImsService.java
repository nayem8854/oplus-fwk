package android.telephony.ims.compat;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.telephony.ims.compat.feature.ImsFeature;
import android.telephony.ims.compat.feature.MMTelFeature;
import android.telephony.ims.compat.feature.RcsFeature;
import android.util.Log;
import android.util.SparseArray;
import com.android.ims.internal.IImsFeatureStatusCallback;
import com.android.ims.internal.IImsMMTelFeature;
import com.android.ims.internal.IImsRcsFeature;
import com.android.ims.internal.IImsServiceController;

public class ImsService extends Service {
  private static final String LOG_TAG = "ImsService(Compat)";
  
  public static final String SERVICE_INTERFACE = "android.telephony.ims.compat.ImsService";
  
  private final SparseArray<SparseArray<ImsFeature>> mFeaturesBySlot = new SparseArray<>();
  
  protected final IBinder mImsServiceController = (IBinder)new IImsServiceController.Stub() {
      final ImsService this$0;
      
      public IImsMMTelFeature createEmergencyMMTelFeature(int param1Int, IImsFeatureStatusCallback param1IImsFeatureStatusCallback) {
        return ImsService.this.createEmergencyMMTelFeatureInternal(param1Int, param1IImsFeatureStatusCallback);
      }
      
      public IImsMMTelFeature createMMTelFeature(int param1Int, IImsFeatureStatusCallback param1IImsFeatureStatusCallback) {
        return ImsService.this.createMMTelFeatureInternal(param1Int, param1IImsFeatureStatusCallback);
      }
      
      public IImsRcsFeature createRcsFeature(int param1Int, IImsFeatureStatusCallback param1IImsFeatureStatusCallback) {
        return ImsService.this.createRcsFeatureInternal(param1Int, param1IImsFeatureStatusCallback);
      }
      
      public void removeImsFeature(int param1Int1, int param1Int2, IImsFeatureStatusCallback param1IImsFeatureStatusCallback) throws RemoteException {
        ImsService.this.removeImsFeature(param1Int1, param1Int2, param1IImsFeatureStatusCallback);
      }
    };
  
  public IBinder onBind(Intent paramIntent) {
    if ("android.telephony.ims.compat.ImsService".equals(paramIntent.getAction())) {
      Log.i("ImsService(Compat)", "ImsService(Compat) Bound.");
      return this.mImsServiceController;
    } 
    return null;
  }
  
  public SparseArray<ImsFeature> getFeatures(int paramInt) {
    return this.mFeaturesBySlot.get(paramInt);
  }
  
  private IImsMMTelFeature createEmergencyMMTelFeatureInternal(int paramInt, IImsFeatureStatusCallback paramIImsFeatureStatusCallback) {
    MMTelFeature mMTelFeature = onCreateEmergencyMMTelImsFeature(paramInt);
    if (mMTelFeature != null) {
      setupFeature(mMTelFeature, paramInt, 0, paramIImsFeatureStatusCallback);
      return mMTelFeature.getBinder();
    } 
    return null;
  }
  
  private IImsMMTelFeature createMMTelFeatureInternal(int paramInt, IImsFeatureStatusCallback paramIImsFeatureStatusCallback) {
    MMTelFeature mMTelFeature = onCreateMMTelImsFeature(paramInt);
    if (mMTelFeature != null) {
      setupFeature(mMTelFeature, paramInt, 1, paramIImsFeatureStatusCallback);
      return mMTelFeature.getBinder();
    } 
    return null;
  }
  
  private IImsRcsFeature createRcsFeatureInternal(int paramInt, IImsFeatureStatusCallback paramIImsFeatureStatusCallback) {
    RcsFeature rcsFeature = onCreateRcsFeature(paramInt);
    if (rcsFeature != null) {
      setupFeature(rcsFeature, paramInt, 2, paramIImsFeatureStatusCallback);
      return rcsFeature.getBinder();
    } 
    return null;
  }
  
  private void setupFeature(ImsFeature paramImsFeature, int paramInt1, int paramInt2, IImsFeatureStatusCallback paramIImsFeatureStatusCallback) {
    paramImsFeature.setContext((Context)this);
    paramImsFeature.setSlotId(paramInt1);
    paramImsFeature.addImsFeatureStatusCallback(paramIImsFeatureStatusCallback);
    addImsFeature(paramInt1, paramInt2, paramImsFeature);
    paramImsFeature.onFeatureReady();
  }
  
  private void addImsFeature(int paramInt1, int paramInt2, ImsFeature paramImsFeature) {
    synchronized (this.mFeaturesBySlot) {
      SparseArray<ImsFeature> sparseArray1 = this.mFeaturesBySlot.get(paramInt1);
      SparseArray<ImsFeature> sparseArray2 = sparseArray1;
      if (sparseArray1 == null) {
        sparseArray2 = new SparseArray();
        this();
        this.mFeaturesBySlot.put(paramInt1, sparseArray2);
      } 
      sparseArray2.put(paramInt2, paramImsFeature);
      return;
    } 
  }
  
  private void removeImsFeature(int paramInt1, int paramInt2, IImsFeatureStatusCallback paramIImsFeatureStatusCallback) {
    synchronized (this.mFeaturesBySlot) {
      StringBuilder stringBuilder;
      SparseArray<ImsFeature> sparseArray = this.mFeaturesBySlot.get(paramInt1);
      if (sparseArray == null) {
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Can not remove ImsFeature. No ImsFeatures exist on slot ");
        stringBuilder.append(paramInt1);
        Log.w("ImsService(Compat)", stringBuilder.toString());
        return;
      } 
      ImsFeature imsFeature = sparseArray.get(paramInt2);
      if (imsFeature == null) {
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Can not remove ImsFeature. No feature with type ");
        stringBuilder.append(paramInt2);
        stringBuilder.append(" exists on slot ");
        stringBuilder.append(paramInt1);
        Log.w("ImsService(Compat)", stringBuilder.toString());
        return;
      } 
      imsFeature.removeImsFeatureStatusCallback((IImsFeatureStatusCallback)stringBuilder);
      imsFeature.onFeatureRemoved();
      sparseArray.remove(paramInt2);
      return;
    } 
  }
  
  public MMTelFeature onCreateEmergencyMMTelImsFeature(int paramInt) {
    return null;
  }
  
  public MMTelFeature onCreateMMTelImsFeature(int paramInt) {
    return null;
  }
  
  public RcsFeature onCreateRcsFeature(int paramInt) {
    return null;
  }
}
