package android.telephony.ims;

import android.annotation.SystemApi;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.telephony.Rlog;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public final class ImsExternalCallState implements Parcelable {
  public static final int CALL_STATE_CONFIRMED = 1;
  
  public static final int CALL_STATE_TERMINATED = 2;
  
  public ImsExternalCallState() {}
  
  public ImsExternalCallState(int paramInt1, Uri paramUri, boolean paramBoolean1, int paramInt2, int paramInt3, boolean paramBoolean2) {
    this.mCallId = paramInt1;
    this.mAddress = paramUri;
    this.mIsPullable = paramBoolean1;
    this.mCallState = paramInt2;
    this.mCallType = paramInt3;
    this.mIsHeld = paramBoolean2;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ImsExternalCallState = ");
    stringBuilder.append(this);
    Rlog.d("ImsExternalCallState", stringBuilder.toString());
  }
  
  public ImsExternalCallState(int paramInt1, Uri paramUri1, Uri paramUri2, boolean paramBoolean1, int paramInt2, int paramInt3, boolean paramBoolean2) {
    this.mCallId = paramInt1;
    this.mAddress = paramUri1;
    this.mLocalAddress = paramUri2;
    this.mIsPullable = paramBoolean1;
    this.mCallState = paramInt2;
    this.mCallType = paramInt3;
    this.mIsHeld = paramBoolean2;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ImsExternalCallState = ");
    stringBuilder.append(this);
    Rlog.d("ImsExternalCallState", stringBuilder.toString());
  }
  
  public ImsExternalCallState(String paramString, Uri paramUri1, Uri paramUri2, boolean paramBoolean1, int paramInt1, int paramInt2, boolean paramBoolean2) {
    this.mCallId = getIdForString(paramString);
    this.mAddress = paramUri1;
    this.mLocalAddress = paramUri2;
    this.mIsPullable = paramBoolean1;
    this.mCallState = paramInt1;
    this.mCallType = paramInt2;
    this.mIsHeld = paramBoolean2;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ImsExternalCallState = ");
    stringBuilder.append(this);
    Rlog.d("ImsExternalCallState", stringBuilder.toString());
  }
  
  public ImsExternalCallState(Parcel paramParcel) {
    boolean bool2;
    this.mCallId = paramParcel.readInt();
    ClassLoader classLoader = ImsExternalCallState.class.getClassLoader();
    this.mAddress = (Uri)paramParcel.readParcelable(classLoader);
    this.mLocalAddress = (Uri)paramParcel.readParcelable(classLoader);
    int i = paramParcel.readInt();
    boolean bool1 = true;
    if (i != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mIsPullable = bool2;
    this.mCallState = paramParcel.readInt();
    this.mCallType = paramParcel.readInt();
    if (paramParcel.readInt() != 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.mIsHeld = bool2;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ImsExternalCallState const = ");
    stringBuilder.append(this);
    Rlog.d("ImsExternalCallState", stringBuilder.toString());
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mCallId);
    paramParcel.writeParcelable((Parcelable)this.mAddress, 0);
    paramParcel.writeParcelable((Parcelable)this.mLocalAddress, 0);
    paramParcel.writeInt(this.mIsPullable);
    paramParcel.writeInt(this.mCallState);
    paramParcel.writeInt(this.mCallType);
    paramParcel.writeInt(this.mIsHeld);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ImsExternalCallState writeToParcel = ");
    stringBuilder.append(paramParcel.toString());
    Rlog.d("ImsExternalCallState", stringBuilder.toString());
  }
  
  public static final Parcelable.Creator<ImsExternalCallState> CREATOR = new Parcelable.Creator<ImsExternalCallState>() {
      public ImsExternalCallState createFromParcel(Parcel param1Parcel) {
        return new ImsExternalCallState(param1Parcel);
      }
      
      public ImsExternalCallState[] newArray(int param1Int) {
        return new ImsExternalCallState[param1Int];
      }
    };
  
  private static final String TAG = "ImsExternalCallState";
  
  private Uri mAddress;
  
  private int mCallId;
  
  private int mCallState;
  
  private int mCallType;
  
  private boolean mIsHeld;
  
  private boolean mIsPullable;
  
  private Uri mLocalAddress;
  
  public int getCallId() {
    return this.mCallId;
  }
  
  public Uri getAddress() {
    return this.mAddress;
  }
  
  public Uri getLocalAddress() {
    return this.mLocalAddress;
  }
  
  public boolean isCallPullable() {
    return this.mIsPullable;
  }
  
  public int getCallState() {
    return this.mCallState;
  }
  
  public int getCallType() {
    return this.mCallType;
  }
  
  public boolean isCallHeld() {
    return this.mIsHeld;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ImsExternalCallState { mCallId = ");
    stringBuilder.append(this.mCallId);
    stringBuilder.append(", mAddress = ");
    Uri uri = this.mAddress;
    stringBuilder.append(Rlog.pii("ImsExternalCallState", uri));
    stringBuilder.append(", mLocalAddress = ");
    uri = this.mLocalAddress;
    stringBuilder.append(Rlog.pii("ImsExternalCallState", uri));
    stringBuilder.append(", mIsPullable = ");
    stringBuilder.append(this.mIsPullable);
    stringBuilder.append(", mCallState = ");
    stringBuilder.append(this.mCallState);
    stringBuilder.append(", mCallType = ");
    stringBuilder.append(this.mCallType);
    stringBuilder.append(", mIsHeld = ");
    stringBuilder.append(this.mIsHeld);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  private int getIdForString(String paramString) {
    try {
      return Integer.parseInt(paramString);
    } catch (NumberFormatException numberFormatException) {
      return paramString.hashCode();
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ExternalCallState implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ExternalCallType implements Annotation {}
}
