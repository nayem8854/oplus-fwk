package android.telephony.ims;

import android.annotation.SystemApi;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceSpecificException;
import android.os.TelephonyServiceManager;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyFrameworkInitializer;
import android.telephony.ims.aidl.IImsConfigCallback;
import com.android.internal.telephony.ITelephony;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.Executor;

@SystemApi
public class ProvisioningManager {
  public static final int KEY_1X_EPDG_TIMER_SEC = 64;
  
  public static final int KEY_1X_THRESHOLD = 59;
  
  public static final int KEY_AMR_BANDWIDTH_EFFICIENT_PAYLOAD_TYPE = 50;
  
  public static final int KEY_AMR_CODEC_MODE_SET_VALUES = 0;
  
  public static final int KEY_AMR_DEFAULT_ENCODING_MODE = 53;
  
  public static final int KEY_AMR_OCTET_ALIGNED_PAYLOAD_TYPE = 49;
  
  public static final int KEY_AMR_WB_BANDWIDTH_EFFICIENT_PAYLOAD_TYPE = 48;
  
  public static final int KEY_AMR_WB_CODEC_MODE_SET_VALUES = 1;
  
  public static final int KEY_AMR_WB_OCTET_ALIGNED_PAYLOAD_TYPE = 47;
  
  public static final int KEY_DTMF_NB_PAYLOAD_TYPE = 52;
  
  public static final int KEY_DTMF_WB_PAYLOAD_TYPE = 51;
  
  public static final int KEY_EAB_PROVISIONING_STATUS = 25;
  
  public static final int KEY_ENABLE_SILENT_REDIAL = 6;
  
  public static final int KEY_LOCAL_BREAKOUT_PCSCF_ADDRESS = 31;
  
  public static final int KEY_LTE_EPDG_TIMER_SEC = 62;
  
  public static final int KEY_LTE_THRESHOLD_1 = 56;
  
  public static final int KEY_LTE_THRESHOLD_2 = 57;
  
  public static final int KEY_LTE_THRESHOLD_3 = 58;
  
  public static final int KEY_MINIMUM_SIP_SESSION_EXPIRATION_TIMER_SEC = 3;
  
  public static final int KEY_MOBILE_DATA_ENABLED = 29;
  
  public static final int KEY_MULTIENDPOINT_ENABLED = 65;
  
  public static final int KEY_RCS_AVAILABILITY_CACHE_EXPIRATION_SEC = 19;
  
  public static final int KEY_RCS_CAPABILITIES_CACHE_EXPIRATION_SEC = 18;
  
  public static final int KEY_RCS_CAPABILITIES_POLL_INTERVAL_SEC = 20;
  
  public static final int KEY_RCS_CAPABILITY_DISCOVERY_ENABLED = 17;
  
  public static final int KEY_RCS_CAPABILITY_POLL_LIST_SUB_EXP_SEC = 23;
  
  public static final int KEY_RCS_MAX_NUM_ENTRIES_IN_RCL = 22;
  
  public static final int KEY_RCS_PUBLISH_OFFLINE_AVAILABILITY_TIMER_SEC = 16;
  
  public static final int KEY_RCS_PUBLISH_SOURCE_THROTTLE_MS = 21;
  
  public static final int KEY_RCS_PUBLISH_TIMER_SEC = 15;
  
  public static final int KEY_REGISTRATION_DOMAIN_NAME = 12;
  
  public static final int KEY_REGISTRATION_RETRY_BASE_TIME_SEC = 33;
  
  public static final int KEY_REGISTRATION_RETRY_MAX_TIME_SEC = 34;
  
  public static final int KEY_RTP_SPEECH_END_PORT = 36;
  
  public static final int KEY_RTP_SPEECH_START_PORT = 35;
  
  public static final int KEY_RTT_ENABLED = 66;
  
  public static final int KEY_SIP_ACK_RECEIPT_WAIT_TIME_MS = 43;
  
  public static final int KEY_SIP_ACK_RETRANSMIT_WAIT_TIME_MS = 44;
  
  public static final int KEY_SIP_INVITE_ACK_WAIT_TIME_MS = 38;
  
  public static final int KEY_SIP_INVITE_CANCELLATION_TIMER_MS = 4;
  
  public static final int KEY_SIP_INVITE_REQUEST_TRANSMIT_INTERVAL_MS = 37;
  
  public static final int KEY_SIP_INVITE_RESPONSE_RETRANSMIT_INTERVAL_MS = 42;
  
  public static final int KEY_SIP_INVITE_RESPONSE_RETRANSMIT_WAIT_TIME_MS = 39;
  
  public static final int KEY_SIP_KEEP_ALIVE_ENABLED = 32;
  
  public static final int KEY_SIP_NON_INVITE_REQUEST_RETRANSMISSION_WAIT_TIME_MS = 45;
  
  public static final int KEY_SIP_NON_INVITE_REQUEST_RETRANSMIT_INTERVAL_MS = 40;
  
  public static final int KEY_SIP_NON_INVITE_RESPONSE_RETRANSMISSION_WAIT_TIME_MS = 46;
  
  public static final int KEY_SIP_NON_INVITE_TRANSACTION_TIMEOUT_TIMER_MS = 41;
  
  public static final int KEY_SIP_SESSION_TIMER_SEC = 2;
  
  public static final int KEY_SMS_FORMAT = 13;
  
  public static final int KEY_SMS_OVER_IP_ENABLED = 14;
  
  public static final int KEY_SMS_PUBLIC_SERVICE_IDENTITY = 54;
  
  public static final int KEY_T1_TIMER_VALUE_MS = 7;
  
  public static final int KEY_T2_TIMER_VALUE_MS = 8;
  
  public static final int KEY_TF_TIMER_VALUE_MS = 9;
  
  public static final int KEY_TRANSITION_TO_LTE_DELAY_MS = 5;
  
  public static final int KEY_USE_GZIP_FOR_LIST_SUBSCRIPTION = 24;
  
  public static final int KEY_USSD_ENABLED = 100;
  
  public static final int KEY_VIDEO_QUALITY = 55;
  
  public static final int KEY_VOICE_OVER_WIFI_ENABLED_OVERRIDE = 28;
  
  public static final int KEY_VOICE_OVER_WIFI_MODE_OVERRIDE = 27;
  
  public static final int KEY_VOICE_OVER_WIFI_ROAMING_ENABLED_OVERRIDE = 26;
  
  public static final int KEY_VOLTE_PROVISIONING_STATUS = 10;
  
  public static final int KEY_VOLTE_USER_OPT_IN_STATUS = 30;
  
  public static final int KEY_VT_PROVISIONING_STATUS = 11;
  
  public static final int KEY_WIFI_EPDG_TIMER_SEC = 63;
  
  public static final int KEY_WIFI_THRESHOLD_A = 60;
  
  public static final int KEY_WIFI_THRESHOLD_B = 61;
  
  public static final int PROVISIONING_RESULT_UNKNOWN = -1;
  
  public static final int PROVISIONING_VALUE_DISABLED = 0;
  
  public static final int PROVISIONING_VALUE_ENABLED = 1;
  
  public static final int SMS_FORMAT_3GPP = 1;
  
  public static final int SMS_FORMAT_3GPP2 = 0;
  
  public static final String STRING_QUERY_RESULT_ERROR_GENERIC = "STRING_QUERY_RESULT_ERROR_GENERIC";
  
  public static final String STRING_QUERY_RESULT_ERROR_NOT_READY = "STRING_QUERY_RESULT_ERROR_NOT_READY";
  
  public static final int VIDEO_QUALITY_HIGH = 1;
  
  public static final int VIDEO_QUALITY_LOW = 0;
  
  private int mSubId;
  
  public static class Callback {
    class CallbackBinder extends IImsConfigCallback.Stub {
      private Executor mExecutor;
      
      private final ProvisioningManager.Callback mLocalConfigurationCallback = ProvisioningManager.Callback.this;
      
      public final void onIntConfigChanged(int param2Int1, int param2Int2) {
        long l = Binder.clearCallingIdentity();
        try {
          Executor executor = this.mExecutor;
          _$$Lambda$ProvisioningManager$Callback$CallbackBinder$R_8jXQuOM7aV7dIwYBzcWwV_YpM _$$Lambda$ProvisioningManager$Callback$CallbackBinder$R_8jXQuOM7aV7dIwYBzcWwV_YpM = new _$$Lambda$ProvisioningManager$Callback$CallbackBinder$R_8jXQuOM7aV7dIwYBzcWwV_YpM();
          this(this, param2Int1, param2Int2);
          executor.execute(_$$Lambda$ProvisioningManager$Callback$CallbackBinder$R_8jXQuOM7aV7dIwYBzcWwV_YpM);
          return;
        } finally {
          restoreCallingIdentity(l);
        } 
      }
      
      public final void onStringConfigChanged(int param2Int, String param2String) {
        long l = Binder.clearCallingIdentity();
        try {
          Executor executor = this.mExecutor;
          _$$Lambda$ProvisioningManager$Callback$CallbackBinder$Jpca2nAZetlBE8jSLFKlsbgUVeI _$$Lambda$ProvisioningManager$Callback$CallbackBinder$Jpca2nAZetlBE8jSLFKlsbgUVeI = new _$$Lambda$ProvisioningManager$Callback$CallbackBinder$Jpca2nAZetlBE8jSLFKlsbgUVeI();
          this(this, param2Int, param2String);
          executor.execute(_$$Lambda$ProvisioningManager$Callback$CallbackBinder$Jpca2nAZetlBE8jSLFKlsbgUVeI);
          return;
        } finally {
          restoreCallingIdentity(l);
        } 
      }
      
      private void setExecutor(Executor param2Executor) {
        this.mExecutor = param2Executor;
      }
      
      private CallbackBinder(ProvisioningManager.Callback this$0) {}
    }
    
    private final CallbackBinder mBinder = new CallbackBinder();
    
    public void onProvisioningIntChanged(int param1Int1, int param1Int2) {}
    
    public void onProvisioningStringChanged(int param1Int, String param1String) {}
    
    public final IImsConfigCallback getBinder() {
      return this.mBinder;
    }
    
    public void setExecutor(Executor param1Executor) {
      this.mBinder.setExecutor(param1Executor);
    }
  }
  
  class CallbackBinder extends IImsConfigCallback.Stub {
    private Executor mExecutor;
    
    private final ProvisioningManager.Callback mLocalConfigurationCallback;
    
    private CallbackBinder(ProvisioningManager this$0) {
      this.mLocalConfigurationCallback = (ProvisioningManager.Callback)this$0;
    }
    
    public final void onIntConfigChanged(int param1Int1, int param1Int2) {
      long l = Binder.clearCallingIdentity();
      try {
        Executor executor = this.mExecutor;
        _$$Lambda$ProvisioningManager$Callback$CallbackBinder$R_8jXQuOM7aV7dIwYBzcWwV_YpM _$$Lambda$ProvisioningManager$Callback$CallbackBinder$R_8jXQuOM7aV7dIwYBzcWwV_YpM = new _$$Lambda$ProvisioningManager$Callback$CallbackBinder$R_8jXQuOM7aV7dIwYBzcWwV_YpM();
        this(this, param1Int1, param1Int2);
        executor.execute(_$$Lambda$ProvisioningManager$Callback$CallbackBinder$R_8jXQuOM7aV7dIwYBzcWwV_YpM);
        return;
      } finally {
        restoreCallingIdentity(l);
      } 
    }
    
    public final void onStringConfigChanged(int param1Int, String param1String) {
      long l = Binder.clearCallingIdentity();
      try {
        Executor executor = this.mExecutor;
        _$$Lambda$ProvisioningManager$Callback$CallbackBinder$Jpca2nAZetlBE8jSLFKlsbgUVeI _$$Lambda$ProvisioningManager$Callback$CallbackBinder$Jpca2nAZetlBE8jSLFKlsbgUVeI = new _$$Lambda$ProvisioningManager$Callback$CallbackBinder$Jpca2nAZetlBE8jSLFKlsbgUVeI();
        this(this, param1Int, param1String);
        executor.execute(_$$Lambda$ProvisioningManager$Callback$CallbackBinder$Jpca2nAZetlBE8jSLFKlsbgUVeI);
        return;
      } finally {
        restoreCallingIdentity(l);
      } 
    }
    
    private void setExecutor(Executor param1Executor) {
      this.mExecutor = param1Executor;
    }
  }
  
  public static ProvisioningManager createForSubscriptionId(int paramInt) {
    if (SubscriptionManager.isValidSubscriptionId(paramInt))
      return new ProvisioningManager(paramInt); 
    throw new IllegalArgumentException("Invalid subscription ID");
  }
  
  private ProvisioningManager(int paramInt) {
    this.mSubId = paramInt;
  }
  
  public void registerProvisioningChangedCallback(Executor paramExecutor, Callback paramCallback) throws ImsException {
    paramCallback.setExecutor(paramExecutor);
    try {
      getITelephony().registerImsProvisioningChangedCallback(this.mSubId, paramCallback.getBinder());
      return;
    } catch (ServiceSpecificException serviceSpecificException) {
      throw new ImsException(serviceSpecificException.getMessage(), serviceSpecificException.errorCode);
    } catch (RemoteException|IllegalStateException remoteException) {
      throw new ImsException(remoteException.getMessage(), 1);
    } 
  }
  
  public void unregisterProvisioningChangedCallback(Callback paramCallback) {
    try {
      getITelephony().unregisterImsProvisioningChangedCallback(this.mSubId, paramCallback.getBinder());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public int getProvisioningIntValue(int paramInt) {
    try {
      return getITelephony().getImsProvisioningInt(this.mSubId, paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public String getProvisioningStringValue(int paramInt) {
    try {
      return getITelephony().getImsProvisioningString(this.mSubId, paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public int setProvisioningIntValue(int paramInt1, int paramInt2) {
    try {
      return getITelephony().setImsProvisioningInt(this.mSubId, paramInt1, paramInt2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public int setProvisioningStringValue(int paramInt, String paramString) {
    try {
      return getITelephony().setImsProvisioningString(this.mSubId, paramInt, paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public void setProvisioningStatusForCapability(int paramInt1, int paramInt2, boolean paramBoolean) {
    try {
      getITelephony().setImsProvisioningStatusForCapability(this.mSubId, paramInt1, paramInt2, paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public boolean getProvisioningStatusForCapability(int paramInt1, int paramInt2) {
    try {
      return getITelephony().getImsProvisioningStatusForCapability(this.mSubId, paramInt1, paramInt2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public boolean getRcsProvisioningStatusForCapability(int paramInt) {
    try {
      return getITelephony().getRcsProvisioningStatusForCapability(this.mSubId, paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public void setRcsProvisioningStatusForCapability(int paramInt, boolean paramBoolean) {
    try {
      getITelephony().setRcsProvisioningStatusForCapability(this.mSubId, paramInt, paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public void notifyRcsAutoConfigurationReceived(byte[] paramArrayOfbyte, boolean paramBoolean) {
    if (paramArrayOfbyte != null)
      try {
        getITelephony().notifyRcsAutoConfigurationReceived(this.mSubId, paramArrayOfbyte, paramBoolean);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      }  
    throw new IllegalArgumentException("Must include a non-null config XML file.");
  }
  
  private static ITelephony getITelephony() {
    TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
    TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getTelephonyServiceRegisterer();
    IBinder iBinder = serviceRegisterer.get();
    ITelephony iTelephony = ITelephony.Stub.asInterface(iBinder);
    if (iTelephony != null)
      return iTelephony; 
    throw new RuntimeException("Could not find Telephony Service.");
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface StringResultError {}
}
