package android.telephony.ims;

import android.content.Context;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.TelephonyServiceManager;
import android.telephony.TelephonyFrameworkInitializer;
import android.telephony.ims.aidl.IImsCapabilityCallback;
import android.telephony.ims.aidl.IImsRcsController;
import android.telephony.ims.feature.RcsFeature;
import android.util.Log;
import com.android.internal.telephony.IIntegerConsumer;
import java.util.concurrent.Executor;
import java.util.function.Consumer;

public class ImsRcsManager {
  public static final String ACTION_SHOW_CAPABILITY_DISCOVERY_OPT_IN = "android.telephony.ims.action.SHOW_CAPABILITY_DISCOVERY_OPT_IN";
  
  private static final String TAG = "ImsRcsManager";
  
  private final Context mContext;
  
  private final int mSubId;
  
  public static class AvailabilityCallback {
    class CapabilityBinder extends IImsCapabilityCallback.Stub {
      private Executor mExecutor;
      
      private final ImsRcsManager.AvailabilityCallback mLocalCallback;
      
      CapabilityBinder(ImsRcsManager.AvailabilityCallback this$0) {
        this.mLocalCallback = this$0;
      }
      
      public void onCapabilitiesStatusChanged(int param2Int) {
        if (this.mLocalCallback == null)
          return; 
        long l = Binder.clearCallingIdentity();
        try {
          Executor executor = this.mExecutor;
          _$$Lambda$ImsRcsManager$AvailabilityCallback$CapabilityBinder$PSokka9KL9vX_vXezL4volLutJg _$$Lambda$ImsRcsManager$AvailabilityCallback$CapabilityBinder$PSokka9KL9vX_vXezL4volLutJg = new _$$Lambda$ImsRcsManager$AvailabilityCallback$CapabilityBinder$PSokka9KL9vX_vXezL4volLutJg();
          this(this, param2Int);
          executor.execute(_$$Lambda$ImsRcsManager$AvailabilityCallback$CapabilityBinder$PSokka9KL9vX_vXezL4volLutJg);
          return;
        } finally {
          restoreCallingIdentity(l);
        } 
      }
      
      public void onQueryCapabilityConfiguration(int param2Int1, int param2Int2, boolean param2Boolean) {}
      
      public void onChangeCapabilityConfigurationError(int param2Int1, int param2Int2, int param2Int3) {}
      
      private void setExecutor(Executor param2Executor) {
        this.mExecutor = param2Executor;
      }
    }
    
    private final CapabilityBinder mBinder = new CapabilityBinder(this);
    
    public void onAvailabilityChanged(RcsFeature.RcsImsCapabilities param1RcsImsCapabilities) {}
    
    public final IImsCapabilityCallback getBinder() {
      return this.mBinder;
    }
    
    private void setExecutor(Executor param1Executor) {
      this.mBinder.setExecutor(param1Executor);
    }
  }
  
  class CapabilityBinder extends IImsCapabilityCallback.Stub {
    private Executor mExecutor;
    
    private final ImsRcsManager.AvailabilityCallback mLocalCallback;
    
    CapabilityBinder(ImsRcsManager this$0) {
      this.mLocalCallback = (ImsRcsManager.AvailabilityCallback)this$0;
    }
    
    public void onCapabilitiesStatusChanged(int param1Int) {
      if (this.mLocalCallback == null)
        return; 
      long l = Binder.clearCallingIdentity();
      try {
        Executor executor = this.mExecutor;
        _$$Lambda$ImsRcsManager$AvailabilityCallback$CapabilityBinder$PSokka9KL9vX_vXezL4volLutJg _$$Lambda$ImsRcsManager$AvailabilityCallback$CapabilityBinder$PSokka9KL9vX_vXezL4volLutJg = new _$$Lambda$ImsRcsManager$AvailabilityCallback$CapabilityBinder$PSokka9KL9vX_vXezL4volLutJg();
        this(this, param1Int);
        executor.execute(_$$Lambda$ImsRcsManager$AvailabilityCallback$CapabilityBinder$PSokka9KL9vX_vXezL4volLutJg);
        return;
      } finally {
        restoreCallingIdentity(l);
      } 
    }
    
    public void onQueryCapabilityConfiguration(int param1Int1, int param1Int2, boolean param1Boolean) {}
    
    public void onChangeCapabilityConfigurationError(int param1Int1, int param1Int2, int param1Int3) {}
    
    private void setExecutor(Executor param1Executor) {
      this.mExecutor = param1Executor;
    }
  }
  
  public ImsRcsManager(Context paramContext, int paramInt) {
    this.mSubId = paramInt;
    this.mContext = paramContext;
  }
  
  public RcsUceAdapter getUceAdapter() {
    return new RcsUceAdapter(this.mContext, this.mSubId);
  }
  
  public void registerImsRegistrationCallback(Executor paramExecutor, RegistrationManager.RegistrationCallback paramRegistrationCallback) throws ImsException {
    if (paramRegistrationCallback != null) {
      if (paramExecutor != null) {
        IImsRcsController iImsRcsController = getIImsRcsController();
        if (iImsRcsController != null) {
          paramRegistrationCallback.setExecutor(paramExecutor);
          try {
            iImsRcsController.registerImsRegistrationCallback(this.mSubId, paramRegistrationCallback.getBinder());
            return;
          } catch (RemoteException|IllegalStateException remoteException) {
            throw new ImsException(remoteException.getMessage(), 1);
          } 
        } 
        Log.e("ImsRcsManager", "Register registration callback: IImsRcsController is null");
        throw new ImsException("Cannot find remote IMS service", 1);
      } 
      throw new IllegalArgumentException("Must include a non-null Executor.");
    } 
    throw new IllegalArgumentException("Must include a non-null RegistrationCallback.");
  }
  
  public void unregisterImsRegistrationCallback(RegistrationManager.RegistrationCallback paramRegistrationCallback) {
    if (paramRegistrationCallback != null) {
      IImsRcsController iImsRcsController = getIImsRcsController();
      if (iImsRcsController != null)
        try {
          iImsRcsController.unregisterImsRegistrationCallback(this.mSubId, paramRegistrationCallback.getBinder());
          return;
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowAsRuntimeException();
        }  
      Log.e("ImsRcsManager", "Unregister registration callback: IImsRcsController is null");
      throw new IllegalStateException("Cannot find remote IMS service");
    } 
    throw new IllegalArgumentException("Must include a non-null RegistrationCallback.");
  }
  
  public void getRegistrationState(Executor paramExecutor, Consumer<Integer> paramConsumer) {
    if (paramConsumer != null) {
      if (paramExecutor != null) {
        IImsRcsController iImsRcsController = getIImsRcsController();
        if (iImsRcsController != null)
          try {
            int i = this.mSubId;
            Object object = new Object();
            super(this, paramExecutor, paramConsumer);
            iImsRcsController.getImsRcsRegistrationState(i, (IIntegerConsumer)object);
            return;
          } catch (RemoteException remoteException) {
            throw remoteException.rethrowAsRuntimeException();
          }  
        Log.e("ImsRcsManager", "Get registration state error: IImsRcsController is null");
        throw new IllegalStateException("Cannot find remote IMS service");
      } 
      throw new IllegalArgumentException("Must include a non-null Executor.");
    } 
    throw new IllegalArgumentException("Must include a non-null stateCallback.");
  }
  
  public void getRegistrationTransportType(Executor paramExecutor, Consumer<Integer> paramConsumer) {
    if (paramConsumer != null) {
      if (paramExecutor != null) {
        IImsRcsController iImsRcsController = getIImsRcsController();
        if (iImsRcsController != null)
          try {
            int i = this.mSubId;
            Object object = new Object();
            super(this, paramExecutor, paramConsumer);
            iImsRcsController.getImsRcsRegistrationTransportType(i, (IIntegerConsumer)object);
            return;
          } catch (RemoteException remoteException) {
            throw remoteException.rethrowAsRuntimeException();
          }  
        Log.e("ImsRcsManager", "Get registration transport type error: IImsRcsController is null");
        throw new IllegalStateException("Cannot find remote IMS service");
      } 
      throw new IllegalArgumentException("Must include a non-null Executor.");
    } 
    throw new IllegalArgumentException("Must include a non-null transportTypeCallback.");
  }
  
  public void registerRcsAvailabilityCallback(Executor paramExecutor, AvailabilityCallback paramAvailabilityCallback) throws ImsException {
    if (paramAvailabilityCallback != null) {
      if (paramExecutor != null) {
        IImsRcsController iImsRcsController = getIImsRcsController();
        if (iImsRcsController != null) {
          paramAvailabilityCallback.setExecutor(paramExecutor);
          try {
            iImsRcsController.registerRcsAvailabilityCallback(this.mSubId, paramAvailabilityCallback.getBinder());
            return;
          } catch (RemoteException remoteException) {
            Log.e("ImsRcsManager", "Error calling IImsRcsController#registerRcsAvailabilityCallback", (Throwable)remoteException);
            throw new ImsException("Remote IMS Service is not available", 1);
          } 
        } 
        Log.e("ImsRcsManager", "Register availability callback: IImsRcsController is null");
        throw new ImsException("Cannot find remote IMS service", 1);
      } 
      throw new IllegalArgumentException("Must include a non-null Executor.");
    } 
    throw new IllegalArgumentException("Must include a non-null AvailabilityCallback.");
  }
  
  public void unregisterRcsAvailabilityCallback(AvailabilityCallback paramAvailabilityCallback) throws ImsException {
    if (paramAvailabilityCallback != null) {
      IImsRcsController iImsRcsController = getIImsRcsController();
      if (iImsRcsController != null)
        try {
          iImsRcsController.unregisterRcsAvailabilityCallback(this.mSubId, paramAvailabilityCallback.getBinder());
          return;
        } catch (RemoteException remoteException) {
          Log.e("ImsRcsManager", "Error calling IImsRcsController#unregisterRcsAvailabilityCallback", (Throwable)remoteException);
          throw new ImsException("Remote IMS Service is not available", 1);
        }  
      Log.e("ImsRcsManager", "Unregister availability callback: IImsRcsController is null");
      throw new ImsException("Cannot find remote IMS service", 1);
    } 
    throw new IllegalArgumentException("Must include a non-null AvailabilityCallback.");
  }
  
  public boolean isCapable(int paramInt1, int paramInt2) throws ImsException {
    IImsRcsController iImsRcsController = getIImsRcsController();
    if (iImsRcsController != null)
      try {
        return iImsRcsController.isCapable(this.mSubId, paramInt1, paramInt2);
      } catch (RemoteException remoteException) {
        Log.e("ImsRcsManager", "Error calling IImsRcsController#isCapable", (Throwable)remoteException);
        throw new ImsException("Remote IMS Service is not available", 1);
      }  
    Log.e("ImsRcsManager", "isCapable: IImsRcsController is null");
    throw new ImsException("Cannot find remote IMS service", 1);
  }
  
  public boolean isAvailable(int paramInt) throws ImsException {
    IImsRcsController iImsRcsController = getIImsRcsController();
    if (iImsRcsController != null)
      try {
        return iImsRcsController.isAvailable(this.mSubId, paramInt);
      } catch (RemoteException remoteException) {
        Log.e("ImsRcsManager", "Error calling IImsRcsController#isAvailable", (Throwable)remoteException);
        throw new ImsException("Remote IMS Service is not available", 1);
      }  
    Log.e("ImsRcsManager", "isAvailable: IImsRcsController is null");
    throw new ImsException("Cannot find remote IMS service", 1);
  }
  
  private IImsRcsController getIImsRcsController() {
    TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
    TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getTelephonyImsServiceRegisterer();
    IBinder iBinder = serviceRegisterer.get();
    return IImsRcsController.Stub.asInterface(iBinder);
  }
}
