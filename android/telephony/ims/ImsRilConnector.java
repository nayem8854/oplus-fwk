package android.telephony.ims;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.telephony.ims.aidl.IImsRil;
import android.util.Log;

public class ImsRilConnector {
  private static final int IMS_EXT_SERVICE_CONNECT = 1;
  
  private static final int IMS_RETRY_TIMEOUT_MS = 500;
  
  private final String TAG = "ImsRilConnector";
  
  private boolean mBound = false;
  
  private Handler mConnectionRetryHandler = null;
  
  private final Context mContext;
  
  private IImsRil mImsRil;
  
  private ServiceConnection mImsRilServiceConnection;
  
  private IListener mListener;
  
  private ImsRilManager mRilMgr;
  
  public ImsRilConnector(Context paramContext, IListener paramIListener) throws Exception {
    this(paramContext, paramIListener, Looper.myLooper());
  }
  
  public void connect() {
    bindImsRilService();
  }
  
  public void disconnect() {
    this.mListener = null;
    unbindImsRilService();
    cleanUp();
  }
  
  class ConnectionRetryHandler extends Handler {
    final ImsRilConnector this$0;
    
    public ConnectionRetryHandler() {}
    
    public ConnectionRetryHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      if (param1Message.what == 1)
        ImsRilConnector.this.bindImsRilService(); 
    }
  }
  
  public void bindImsRilService() {
    Intent intent = new Intent();
    intent.setClassName("org.codeaurora.ims", "org.codeaurora.ims.ImsRilService");
    this.mBound = this.mContext.bindService(intent, this.mImsRilServiceConnection, 1);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Attempt to bind QtiImsService service returned with: ");
    stringBuilder.append(this.mBound);
    Log.d("ImsRilConnector", stringBuilder.toString());
    if (!this.mBound)
      this.mConnectionRetryHandler.sendEmptyMessageDelayed(1, 500L); 
  }
  
  protected void unbindImsRilService() {
    Context context = this.mContext;
    if (context != null && this.mBound) {
      context.unbindService(this.mImsRilServiceConnection);
      this.mBound = false;
    } 
  }
  
  public ImsRilConnector(Context paramContext, IListener paramIListener, Looper paramLooper) throws Exception {
    this.mImsRilServiceConnection = (ServiceConnection)new Object(this);
    if (paramContext != null && paramIListener != null && paramLooper != null) {
      this.mContext = paramContext;
      this.mListener = paramIListener;
      this.mConnectionRetryHandler = new ConnectionRetryHandler(paramLooper);
      return;
    } 
    throw new Exception("context, listener and looper should not be null ");
  }
  
  private void cleanUp() {
    this.mConnectionRetryHandler.removeMessages(1);
    this.mImsRil = null;
    ImsRilManager imsRilManager = this.mRilMgr;
    if (imsRilManager != null)
      imsRilManager.dispose(); 
  }
  
  public static interface IListener {
    void onConnectionAvailable(ImsRilManager param1ImsRilManager);
    
    void onConnectionUnavailable();
  }
}
