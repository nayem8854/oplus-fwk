package android.telephony.ims.stub;

import android.annotation.SystemApi;
import android.os.Message;
import android.os.RemoteException;
import android.telephony.ims.ImsCallProfile;
import android.telephony.ims.ImsCallSessionListener;
import android.telephony.ims.ImsStreamMediaProfile;
import android.telephony.ims.ImsVideoCallProvider;
import android.telephony.ims.aidl.IImsCallSessionListener;
import com.android.ims.internal.IImsCallSession;
import com.android.ims.internal.IImsVideoCallProvider;

@SystemApi
public class ImsCallSessionImplBase implements AutoCloseable {
  public static final int USSD_MODE_NOTIFY = 0;
  
  public static final int USSD_MODE_REQUEST = 1;
  
  public static class State {
    public static final int ESTABLISHED = 4;
    
    public static final int ESTABLISHING = 3;
    
    public static final int IDLE = 0;
    
    public static final int INITIATED = 1;
    
    public static final int INVALID = -1;
    
    public static final int NEGOTIATING = 2;
    
    public static final int REESTABLISHING = 6;
    
    public static final int RENEGOTIATING = 5;
    
    public static final int TERMINATED = 8;
    
    public static final int TERMINATING = 7;
    
    public static String toString(int param1Int) {
      switch (param1Int) {
        default:
          return "UNKNOWN";
        case 8:
          return "TERMINATED";
        case 7:
          return "TERMINATING";
        case 6:
          return "REESTABLISHING";
        case 5:
          return "RENEGOTIATING";
        case 4:
          return "ESTABLISHED";
        case 3:
          return "ESTABLISHING";
        case 2:
          return "NEGOTIATING";
        case 1:
          return "INITIATED";
        case 0:
          break;
      } 
      return "IDLE";
    }
  }
  
  private IImsCallSession mServiceImpl = (IImsCallSession)new Object(this);
  
  public final void setListener(IImsCallSessionListener paramIImsCallSessionListener) throws RemoteException {
    setListener(new ImsCallSessionListener(paramIImsCallSessionListener));
  }
  
  public void setListener(ImsCallSessionListener paramImsCallSessionListener) {}
  
  public void close() {}
  
  public String getCallId() {
    return null;
  }
  
  public ImsCallProfile getCallProfile() {
    return null;
  }
  
  public ImsCallProfile getLocalCallProfile() {
    return null;
  }
  
  public ImsCallProfile getRemoteCallProfile() {
    return null;
  }
  
  public String getProperty(String paramString) {
    return null;
  }
  
  public int getState() {
    return -1;
  }
  
  public boolean isInCall() {
    return false;
  }
  
  public void setMute(boolean paramBoolean) {}
  
  public void start(String paramString, ImsCallProfile paramImsCallProfile) {}
  
  public void startConference(String[] paramArrayOfString, ImsCallProfile paramImsCallProfile) {}
  
  public void accept(int paramInt, ImsStreamMediaProfile paramImsStreamMediaProfile) {}
  
  public void deflect(String paramString) {}
  
  public void reject(int paramInt) {}
  
  public void transfer(String paramString, boolean paramBoolean) {}
  
  public void transfer(ImsCallSessionImplBase paramImsCallSessionImplBase) {}
  
  public void terminate(int paramInt) {}
  
  public void hold(ImsStreamMediaProfile paramImsStreamMediaProfile) {}
  
  public void resume(ImsStreamMediaProfile paramImsStreamMediaProfile) {}
  
  public void merge() {}
  
  public void update(int paramInt, ImsStreamMediaProfile paramImsStreamMediaProfile) {}
  
  public void extendToConference(String[] paramArrayOfString) {}
  
  public void inviteParticipants(String[] paramArrayOfString) {}
  
  public void removeParticipants(String[] paramArrayOfString) {}
  
  public void sendDtmf(char paramChar, Message paramMessage) {}
  
  public void startDtmf(char paramChar) {}
  
  public void stopDtmf() {}
  
  public void sendUssd(String paramString) {}
  
  public IImsVideoCallProvider getVideoCallProvider() {
    ImsVideoCallProvider imsVideoCallProvider = getImsVideoCallProvider();
    if (imsVideoCallProvider != null) {
      IImsVideoCallProvider iImsVideoCallProvider = imsVideoCallProvider.getInterface();
    } else {
      imsVideoCallProvider = null;
    } 
    return (IImsVideoCallProvider)imsVideoCallProvider;
  }
  
  public ImsVideoCallProvider getImsVideoCallProvider() {
    return null;
  }
  
  public boolean isMultiparty() {
    return false;
  }
  
  public void sendRttModifyRequest(ImsCallProfile paramImsCallProfile) {}
  
  public void sendRttModifyResponse(boolean paramBoolean) {}
  
  public void sendRttMessage(String paramString) {}
  
  public IImsCallSession getServiceImpl() {
    return this.mServiceImpl;
  }
  
  public void setServiceImpl(IImsCallSession paramIImsCallSession) {
    this.mServiceImpl = paramIImsCallSession;
  }
}
