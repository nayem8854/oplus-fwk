package android.telephony.ims.stub;

import android.annotation.SystemApi;
import android.content.Context;
import android.os.PersistableBundle;
import android.os.RemoteException;
import android.telephony.ims.aidl.IImsConfig;
import android.telephony.ims.aidl.IImsConfigCallback;
import android.util.Log;
import com.android.internal.telephony.util.RemoteCallbackListExt;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.HashMap;

@SystemApi
public class ImsConfigImplBase {
  public static final int CONFIG_RESULT_FAILED = 1;
  
  public static final int CONFIG_RESULT_SUCCESS = 0;
  
  public static final int CONFIG_RESULT_UNKNOWN = -1;
  
  private static final String TAG = "ImsConfigImplBase";
  
  class ImsConfigStub extends IImsConfig.Stub {
    WeakReference<ImsConfigImplBase> mImsConfigImplBaseWeakReference;
    
    private HashMap<Integer, Integer> mProvisionedIntValue = new HashMap<>();
    
    private HashMap<Integer, String> mProvisionedStringValue = new HashMap<>();
    
    public ImsConfigStub(ImsConfigImplBase this$0) {
      this.mImsConfigImplBaseWeakReference = new WeakReference<>(this$0);
    }
    
    public void addImsConfigCallback(IImsConfigCallback param1IImsConfigCallback) throws RemoteException {
      getImsConfigImpl().addImsConfigCallback(param1IImsConfigCallback);
    }
    
    public void removeImsConfigCallback(IImsConfigCallback param1IImsConfigCallback) throws RemoteException {
      getImsConfigImpl().removeImsConfigCallback(param1IImsConfigCallback);
    }
    
    public int getConfigInt(int param1Int) throws RemoteException {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mProvisionedIntValue : Ljava/util/HashMap;
      //   6: iload_1
      //   7: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   10: invokevirtual containsKey : (Ljava/lang/Object;)Z
      //   13: ifeq -> 38
      //   16: aload_0
      //   17: getfield mProvisionedIntValue : Ljava/util/HashMap;
      //   20: iload_1
      //   21: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   24: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   27: checkcast java/lang/Integer
      //   30: invokevirtual intValue : ()I
      //   33: istore_1
      //   34: aload_0
      //   35: monitorexit
      //   36: iload_1
      //   37: ireturn
      //   38: aload_0
      //   39: invokespecial getImsConfigImpl : ()Landroid/telephony/ims/stub/ImsConfigImplBase;
      //   42: iload_1
      //   43: invokevirtual getConfigInt : (I)I
      //   46: istore_2
      //   47: iload_2
      //   48: iconst_m1
      //   49: if_icmpeq -> 59
      //   52: aload_0
      //   53: iload_1
      //   54: iload_2
      //   55: iconst_0
      //   56: invokevirtual updateCachedValue : (IIZ)V
      //   59: aload_0
      //   60: monitorexit
      //   61: iload_2
      //   62: ireturn
      //   63: astore_3
      //   64: aload_0
      //   65: monitorexit
      //   66: aload_3
      //   67: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #110	-> 2
      //   #111	-> 16
      //   #113	-> 38
      //   #114	-> 47
      //   #115	-> 52
      //   #117	-> 59
      //   #109	-> 63
      // Exception table:
      //   from	to	target	type
      //   2	16	63	finally
      //   16	34	63	finally
      //   38	47	63	finally
      //   52	59	63	finally
    }
    
    public String getConfigString(int param1Int) throws RemoteException {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mProvisionedIntValue : Ljava/util/HashMap;
      //   6: iload_1
      //   7: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   10: invokevirtual containsKey : (Ljava/lang/Object;)Z
      //   13: ifeq -> 35
      //   16: aload_0
      //   17: getfield mProvisionedStringValue : Ljava/util/HashMap;
      //   20: iload_1
      //   21: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   24: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   27: checkcast java/lang/String
      //   30: astore_2
      //   31: aload_0
      //   32: monitorexit
      //   33: aload_2
      //   34: areturn
      //   35: aload_0
      //   36: invokespecial getImsConfigImpl : ()Landroid/telephony/ims/stub/ImsConfigImplBase;
      //   39: iload_1
      //   40: invokevirtual getConfigString : (I)Ljava/lang/String;
      //   43: astore_2
      //   44: aload_2
      //   45: ifnull -> 55
      //   48: aload_0
      //   49: iload_1
      //   50: aload_2
      //   51: iconst_0
      //   52: invokevirtual updateCachedValue : (ILjava/lang/String;Z)V
      //   55: aload_0
      //   56: monitorexit
      //   57: aload_2
      //   58: areturn
      //   59: astore_2
      //   60: aload_0
      //   61: monitorexit
      //   62: aload_2
      //   63: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #131	-> 2
      //   #132	-> 16
      //   #134	-> 35
      //   #135	-> 44
      //   #136	-> 48
      //   #138	-> 55
      //   #130	-> 59
      // Exception table:
      //   from	to	target	type
      //   2	16	59	finally
      //   16	31	59	finally
      //   35	44	59	finally
      //   48	55	59	finally
    }
    
    public int setConfigInt(int param1Int1, int param1Int2) throws RemoteException {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mProvisionedIntValue : Ljava/util/HashMap;
      //   6: iload_1
      //   7: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   10: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
      //   13: pop
      //   14: aload_0
      //   15: invokespecial getImsConfigImpl : ()Landroid/telephony/ims/stub/ImsConfigImplBase;
      //   18: iload_1
      //   19: iload_2
      //   20: invokevirtual setConfig : (II)I
      //   23: istore_3
      //   24: iload_3
      //   25: ifne -> 38
      //   28: aload_0
      //   29: iload_1
      //   30: iload_2
      //   31: iconst_1
      //   32: invokevirtual updateCachedValue : (IIZ)V
      //   35: goto -> 104
      //   38: new java/lang/StringBuilder
      //   41: astore #4
      //   43: aload #4
      //   45: invokespecial <init> : ()V
      //   48: aload #4
      //   50: ldc 'Set provision value of '
      //   52: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   55: pop
      //   56: aload #4
      //   58: iload_1
      //   59: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   62: pop
      //   63: aload #4
      //   65: ldc ' to '
      //   67: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   70: pop
      //   71: aload #4
      //   73: iload_2
      //   74: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   77: pop
      //   78: aload #4
      //   80: ldc ' failed with error code '
      //   82: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   85: pop
      //   86: aload #4
      //   88: iload_3
      //   89: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   92: pop
      //   93: ldc 'ImsConfigImplBase'
      //   95: aload #4
      //   97: invokevirtual toString : ()Ljava/lang/String;
      //   100: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   103: pop
      //   104: aload_0
      //   105: monitorexit
      //   106: iload_3
      //   107: ireturn
      //   108: astore #4
      //   110: aload_0
      //   111: monitorexit
      //   112: aload #4
      //   114: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #155	-> 2
      //   #156	-> 14
      //   #157	-> 24
      //   #158	-> 28
      //   #160	-> 38
      //   #164	-> 104
      //   #154	-> 108
      // Exception table:
      //   from	to	target	type
      //   2	14	108	finally
      //   14	24	108	finally
      //   28	35	108	finally
      //   38	104	108	finally
    }
    
    public int setConfigString(int param1Int, String param1String) throws RemoteException {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mProvisionedStringValue : Ljava/util/HashMap;
      //   6: iload_1
      //   7: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   10: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
      //   13: pop
      //   14: aload_0
      //   15: invokespecial getImsConfigImpl : ()Landroid/telephony/ims/stub/ImsConfigImplBase;
      //   18: iload_1
      //   19: aload_2
      //   20: invokevirtual setConfig : (ILjava/lang/String;)I
      //   23: istore_3
      //   24: iload_3
      //   25: ifne -> 35
      //   28: aload_0
      //   29: iload_1
      //   30: aload_2
      //   31: iconst_1
      //   32: invokevirtual updateCachedValue : (ILjava/lang/String;Z)V
      //   35: aload_0
      //   36: monitorexit
      //   37: iload_3
      //   38: ireturn
      //   39: astore_2
      //   40: aload_0
      //   41: monitorexit
      //   42: aload_2
      //   43: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #181	-> 2
      //   #182	-> 14
      //   #183	-> 24
      //   #184	-> 28
      //   #187	-> 35
      //   #180	-> 39
      // Exception table:
      //   from	to	target	type
      //   2	14	39	finally
      //   14	24	39	finally
      //   28	35	39	finally
    }
    
    public void updateImsCarrierConfigs(PersistableBundle param1PersistableBundle) throws RemoteException {
      getImsConfigImpl().updateImsCarrierConfigs(param1PersistableBundle);
    }
    
    private ImsConfigImplBase getImsConfigImpl() throws RemoteException {
      ImsConfigImplBase imsConfigImplBase = this.mImsConfigImplBaseWeakReference.get();
      if (imsConfigImplBase != null)
        return imsConfigImplBase; 
      throw new RemoteException("Fail to get ImsConfigImpl");
    }
    
    public void notifyRcsAutoConfigurationReceived(byte[] param1ArrayOfbyte, boolean param1Boolean) throws RemoteException {
      getImsConfigImpl().notifyRcsAutoConfigurationReceived(param1ArrayOfbyte, param1Boolean);
    }
    
    private void notifyImsConfigChanged(int param1Int1, int param1Int2) throws RemoteException {
      getImsConfigImpl().notifyConfigChanged(param1Int1, param1Int2);
    }
    
    private void notifyImsConfigChanged(int param1Int, String param1String) throws RemoteException {
      getImsConfigImpl().notifyConfigChanged(param1Int, param1String);
    }
    
    protected void updateCachedValue(int param1Int1, int param1Int2, boolean param1Boolean) throws RemoteException {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mProvisionedIntValue : Ljava/util/HashMap;
      //   6: iload_1
      //   7: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   10: iload_2
      //   11: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   14: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   17: pop
      //   18: iload_3
      //   19: ifeq -> 28
      //   22: aload_0
      //   23: iload_1
      //   24: iload_2
      //   25: invokespecial notifyImsConfigChanged : (II)V
      //   28: aload_0
      //   29: monitorexit
      //   30: return
      //   31: astore #4
      //   33: aload_0
      //   34: monitorexit
      //   35: aload #4
      //   37: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #220	-> 2
      //   #221	-> 18
      //   #222	-> 22
      //   #224	-> 28
      //   #219	-> 31
      // Exception table:
      //   from	to	target	type
      //   2	18	31	finally
      //   22	28	31	finally
    }
    
    protected void updateCachedValue(int param1Int, String param1String, boolean param1Boolean) throws RemoteException {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mProvisionedStringValue : Ljava/util/HashMap;
      //   6: iload_1
      //   7: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   10: aload_2
      //   11: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   14: pop
      //   15: iload_3
      //   16: ifeq -> 25
      //   19: aload_0
      //   20: iload_1
      //   21: aload_2
      //   22: invokespecial notifyImsConfigChanged : (ILjava/lang/String;)V
      //   25: aload_0
      //   26: monitorexit
      //   27: return
      //   28: astore_2
      //   29: aload_0
      //   30: monitorexit
      //   31: aload_2
      //   32: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #228	-> 2
      //   #229	-> 15
      //   #230	-> 19
      //   #232	-> 25
      //   #227	-> 28
      // Exception table:
      //   from	to	target	type
      //   2	15	28	finally
      //   19	25	28	finally
    }
  }
  
  private final RemoteCallbackListExt<IImsConfigCallback> mCallbacks = new RemoteCallbackListExt();
  
  ImsConfigStub mImsConfigStub;
  
  public ImsConfigImplBase(Context paramContext) {
    this.mImsConfigStub = new ImsConfigStub(this);
  }
  
  public ImsConfigImplBase() {
    this.mImsConfigStub = new ImsConfigStub(this);
  }
  
  private void addImsConfigCallback(IImsConfigCallback paramIImsConfigCallback) {
    this.mCallbacks.register(paramIImsConfigCallback);
  }
  
  private void removeImsConfigCallback(IImsConfigCallback paramIImsConfigCallback) {
    this.mCallbacks.unregister(paramIImsConfigCallback);
  }
  
  private final void notifyConfigChanged(int paramInt1, int paramInt2) {
    RemoteCallbackListExt<IImsConfigCallback> remoteCallbackListExt = this.mCallbacks;
    if (remoteCallbackListExt == null)
      return; 
    remoteCallbackListExt.broadcastAction(new _$$Lambda$ImsConfigImplBase$yL4863k_FoQyqg_FX2mWsLMqbyA(paramInt1, paramInt2));
  }
  
  private void notifyConfigChanged(int paramInt, String paramString) {
    RemoteCallbackListExt<IImsConfigCallback> remoteCallbackListExt = this.mCallbacks;
    if (remoteCallbackListExt == null)
      return; 
    remoteCallbackListExt.broadcastAction(new _$$Lambda$ImsConfigImplBase$GAuYvQ8qBc7KgCJhNp4Pt4j5t_0(paramInt, paramString));
  }
  
  public IImsConfig getIImsConfig() {
    return this.mImsConfigStub;
  }
  
  public final void notifyProvisionedValueChanged(int paramInt1, int paramInt2) {
    try {
      this.mImsConfigStub.updateCachedValue(paramInt1, paramInt2, true);
    } catch (RemoteException remoteException) {
      Log.w("ImsConfigImplBase", "notifyProvisionedValueChanged(int): Framework connection is dead.");
    } 
  }
  
  public final void notifyProvisionedValueChanged(int paramInt, String paramString) {
    try {
      this.mImsConfigStub.updateCachedValue(paramInt, paramString, true);
    } catch (RemoteException remoteException) {
      Log.w("ImsConfigImplBase", "notifyProvisionedValueChanged(string): Framework connection is dead.");
    } 
  }
  
  public void notifyRcsAutoConfigurationReceived(byte[] paramArrayOfbyte, boolean paramBoolean) {}
  
  public int setConfig(int paramInt1, int paramInt2) {
    return 1;
  }
  
  public int setConfig(int paramInt, String paramString) {
    return 1;
  }
  
  public int getConfigInt(int paramInt) {
    return -1;
  }
  
  public String getConfigString(int paramInt) {
    return null;
  }
  
  public void updateImsCarrierConfigs(PersistableBundle paramPersistableBundle) {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SetConfigResult {}
}
