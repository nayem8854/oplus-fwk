package android.telephony.ims.stub;

import android.annotation.SystemApi;
import android.os.RemoteException;
import android.telephony.ims.ImsExternalCallState;
import android.util.Log;
import com.android.ims.internal.IImsExternalCallStateListener;
import com.android.ims.internal.IImsMultiEndpoint;
import java.util.List;

@SystemApi
public class ImsMultiEndpointImplBase {
  private static final String TAG = "MultiEndpointImplBase";
  
  private IImsMultiEndpoint mImsMultiEndpoint = (IImsMultiEndpoint)new Object(this);
  
  private IImsExternalCallStateListener mListener;
  
  public IImsMultiEndpoint getIImsMultiEndpoint() {
    return this.mImsMultiEndpoint;
  }
  
  public final void onImsExternalCallStateUpdate(List<ImsExternalCallState> paramList) {
    Log.d("MultiEndpointImplBase", "ims external call state update triggered.");
    IImsExternalCallStateListener iImsExternalCallStateListener = this.mListener;
    if (iImsExternalCallStateListener != null)
      try {
        iImsExternalCallStateListener.onImsExternalCallStateUpdate(paramList);
      } catch (RemoteException remoteException) {
        throw new RuntimeException(remoteException);
      }  
  }
  
  public void requestImsExternalCallStateInfo() {
    Log.d("MultiEndpointImplBase", "requestImsExternalCallStateInfo() not implemented");
  }
}
