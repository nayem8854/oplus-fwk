package android.telephony.ims.stub;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.ims.feature.ImsFeature;
import android.util.ArraySet;
import java.util.Set;

@SystemApi
public final class ImsFeatureConfiguration implements Parcelable {
  class FeatureSlotPair {
    public final int featureType;
    
    public final int slotId;
    
    public FeatureSlotPair(ImsFeatureConfiguration this$0, int param1Int1) {
      this.slotId = this$0;
      this.featureType = param1Int1;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      if (this.slotId != ((FeatureSlotPair)param1Object).slotId)
        return false; 
      if (this.featureType != ((FeatureSlotPair)param1Object).featureType)
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      int i = this.slotId;
      int j = this.featureType;
      return i * 31 + j;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("{s=");
      stringBuilder.append(this.slotId);
      stringBuilder.append(", f=");
      stringBuilder.append(ImsFeature.FEATURE_LOG_MAP.get(Integer.valueOf(this.featureType)));
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  class Builder {
    ImsFeatureConfiguration mConfig = new ImsFeatureConfiguration();
    
    public Builder addFeature(int param1Int1, int param1Int2) {
      this.mConfig.addFeature(param1Int1, param1Int2);
      return this;
    }
    
    public ImsFeatureConfiguration build() {
      return this.mConfig;
    }
  }
  
  public ImsFeatureConfiguration() {
    this.mFeatures = new ArraySet<>();
  }
  
  public ImsFeatureConfiguration(Set<FeatureSlotPair> paramSet) {
    ArraySet<FeatureSlotPair> arraySet = new ArraySet();
    if (paramSet != null)
      arraySet.addAll(paramSet); 
  }
  
  public Set<FeatureSlotPair> getServiceFeatures() {
    return new ArraySet<>(this.mFeatures);
  }
  
  void addFeature(int paramInt1, int paramInt2) {
    this.mFeatures.add(new FeatureSlotPair(paramInt1, paramInt2));
  }
  
  protected ImsFeatureConfiguration(Parcel paramParcel) {
    int i = paramParcel.readInt();
    this.mFeatures = new ArraySet<>(i);
    for (byte b = 0; b < i; b++)
      this.mFeatures.add(new FeatureSlotPair(paramParcel.readInt(), paramParcel.readInt())); 
  }
  
  public static final Parcelable.Creator<ImsFeatureConfiguration> CREATOR = new Parcelable.Creator<ImsFeatureConfiguration>() {
      public ImsFeatureConfiguration createFromParcel(Parcel param1Parcel) {
        return new ImsFeatureConfiguration(param1Parcel);
      }
      
      public ImsFeatureConfiguration[] newArray(int param1Int) {
        return new ImsFeatureConfiguration[param1Int];
      }
    };
  
  private final Set<FeatureSlotPair> mFeatures;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    FeatureSlotPair[] arrayOfFeatureSlotPair = new FeatureSlotPair[this.mFeatures.size()];
    this.mFeatures.toArray(arrayOfFeatureSlotPair);
    paramParcel.writeInt(arrayOfFeatureSlotPair.length);
    for (int i = arrayOfFeatureSlotPair.length; paramInt < i; ) {
      FeatureSlotPair featureSlotPair = arrayOfFeatureSlotPair[paramInt];
      paramParcel.writeInt(featureSlotPair.slotId);
      paramParcel.writeInt(featureSlotPair.featureType);
      paramInt++;
    } 
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof ImsFeatureConfiguration))
      return false; 
    paramObject = paramObject;
    return this.mFeatures.equals(((ImsFeatureConfiguration)paramObject).mFeatures);
  }
  
  public int hashCode() {
    return this.mFeatures.hashCode();
  }
}
