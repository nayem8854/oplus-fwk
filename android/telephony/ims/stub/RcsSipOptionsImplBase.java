package android.telephony.ims.stub;

import android.net.Uri;
import android.os.RemoteException;
import android.telephony.ims.ImsException;
import android.telephony.ims.RcsContactUceCapability;
import android.util.Log;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class RcsSipOptionsImplBase extends RcsCapabilityExchange {
  private static final String LOG_TAG = "RcsSipOptionsImplBase";
  
  public static final int RESPONSE_BAD_REQUEST = 5;
  
  public static final int RESPONSE_DOES_NOT_EXIST_ANYWHERE = 4;
  
  public static final int RESPONSE_GENERIC_FAILURE = -1;
  
  public static final int RESPONSE_NOT_FOUND = 3;
  
  public static final int RESPONSE_REQUEST_TIMEOUT = 2;
  
  public static final int RESPONSE_SUCCESS = 0;
  
  public static final int RESPONSE_TEMPORARILY_UNAVAILABLE = 1;
  
  public final void onCapabilityRequestResponse(int paramInt1, String paramString, RcsContactUceCapability paramRcsContactUceCapability, int paramInt2) throws ImsException {
    try {
      getListener().onCapabilityRequestResponseOptions(paramInt1, paramString, paramRcsContactUceCapability, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw new ImsException(remoteException.getMessage(), 1);
    } 
  }
  
  public final void onRemoteCapabilityRequest(Uri paramUri, RcsContactUceCapability paramRcsContactUceCapability, int paramInt) throws ImsException {
    try {
      getListener().onRemoteCapabilityRequest(paramUri, paramRcsContactUceCapability, paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw new ImsException(remoteException.getMessage(), 1);
    } 
  }
  
  public void sendCapabilityRequest(Uri paramUri, RcsContactUceCapability paramRcsContactUceCapability, int paramInt) {
    Log.w("RcsSipOptionsImplBase", "sendCapabilityRequest called with no implementation.");
    try {
      getListener().onCommandUpdate(8, paramInt);
    } catch (RemoteException|ImsException remoteException) {}
  }
  
  public void respondToCapabilityRequest(String paramString, RcsContactUceCapability paramRcsContactUceCapability, int paramInt) {
    Log.w("RcsSipOptionsImplBase", "respondToCapabilityRequest called with no implementation.");
    try {
      getListener().onCommandUpdate(8, paramInt);
    } catch (RemoteException|ImsException remoteException) {}
  }
  
  public void respondToCapabilityRequestWithError(Uri paramUri, int paramInt1, String paramString, int paramInt2) {
    Log.w("RcsSipOptionsImplBase", "respondToCapabiltyRequestWithError called with no implementation.");
    try {
      getListener().onCommandUpdate(8, paramInt2);
    } catch (RemoteException|ImsException remoteException) {}
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class SipResponseCode implements Annotation {}
}
