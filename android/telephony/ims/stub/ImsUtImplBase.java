package android.telephony.ims.stub;

import android.annotation.SystemApi;
import android.os.Bundle;
import android.telephony.ims.ImsUtListener;
import com.android.ims.internal.IImsUt;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public class ImsUtImplBase {
  public static final int CALL_BARRING_ALL = 7;
  
  public static final int CALL_BARRING_ALL_INCOMING = 1;
  
  public static final int CALL_BARRING_ALL_OUTGOING = 2;
  
  public static final int CALL_BARRING_ANONYMOUS_INCOMING = 6;
  
  public static final int CALL_BARRING_INCOMING_ALL_SERVICES = 9;
  
  public static final int CALL_BARRING_OUTGOING_ALL_SERVICES = 8;
  
  public static final int CALL_BARRING_OUTGOING_INTL = 3;
  
  public static final int CALL_BARRING_OUTGOING_INTL_EXCL_HOME = 4;
  
  public static final int CALL_BARRING_SPECIFIC_INCOMING_CALLS = 10;
  
  public static final int CALL_BLOCKING_INCOMING_WHEN_ROAMING = 5;
  
  public static final int INVALID_RESULT = -1;
  
  private IImsUt.Stub mServiceImpl = (IImsUt.Stub)new Object(this);
  
  public void close() {}
  
  public int queryCallBarring(int paramInt) {
    return -1;
  }
  
  public int queryCallBarringForServiceClass(int paramInt1, int paramInt2) {
    return -1;
  }
  
  public int queryCallForward(int paramInt, String paramString) {
    return -1;
  }
  
  public int queryCFForServiceClass(int paramInt1, String paramString, int paramInt2) {
    return -1;
  }
  
  public int queryCallWaiting() {
    return -1;
  }
  
  public int queryCLIR() {
    return queryClir();
  }
  
  public int queryCLIP() {
    return queryClip();
  }
  
  public int queryCOLR() {
    return queryColr();
  }
  
  public int queryCOLP() {
    return queryColp();
  }
  
  public int queryClir() {
    return -1;
  }
  
  public int queryClip() {
    return -1;
  }
  
  public int queryColr() {
    return -1;
  }
  
  public int queryColp() {
    return -1;
  }
  
  public int transact(Bundle paramBundle) {
    return -1;
  }
  
  public int updateCallBarring(int paramInt1, int paramInt2, String[] paramArrayOfString) {
    return -1;
  }
  
  public int updateCallBarringForServiceClass(int paramInt1, int paramInt2, String[] paramArrayOfString, int paramInt3) {
    return -1;
  }
  
  public int updateCallBarringWithPassword(int paramInt1, int paramInt2, String[] paramArrayOfString, int paramInt3, String paramString) {
    return -1;
  }
  
  public int updateCallForward(int paramInt1, int paramInt2, String paramString, int paramInt3, int paramInt4) {
    return 0;
  }
  
  public int updateCallWaiting(boolean paramBoolean, int paramInt) {
    return -1;
  }
  
  public int updateCLIR(int paramInt) {
    return updateClir(paramInt);
  }
  
  public int updateCLIP(boolean paramBoolean) {
    return updateClip(paramBoolean);
  }
  
  public int updateCOLR(int paramInt) {
    return updateColr(paramInt);
  }
  
  public int updateCOLP(boolean paramBoolean) {
    return updateColp(paramBoolean);
  }
  
  public int updateClir(int paramInt) {
    return -1;
  }
  
  public int updateClip(boolean paramBoolean) {
    return -1;
  }
  
  public int updateColr(int paramInt) {
    return -1;
  }
  
  public int updateColp(boolean paramBoolean) {
    return -1;
  }
  
  public void setListener(ImsUtListener paramImsUtListener) {}
  
  public IImsUt getInterface() {
    return this.mServiceImpl;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface CallBarringMode {}
}
