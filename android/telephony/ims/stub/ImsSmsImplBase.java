package android.telephony.ims.stub;

import android.annotation.SystemApi;
import android.os.RemoteException;
import android.telephony.SmsMessage;
import android.telephony.ims.aidl.IImsSmsListener;
import android.util.Log;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public class ImsSmsImplBase {
  public static final int DELIVER_STATUS_ERROR_GENERIC = 2;
  
  public static final int DELIVER_STATUS_ERROR_NO_MEMORY = 3;
  
  public static final int DELIVER_STATUS_ERROR_REQUEST_NOT_SUPPORTED = 4;
  
  public static final int DELIVER_STATUS_OK = 1;
  
  private static final String LOG_TAG = "SmsImplBase";
  
  public static final int RESULT_NO_NETWORK_ERROR = -1;
  
  public static final int SEND_STATUS_ERROR = 2;
  
  public static final int SEND_STATUS_ERROR_FALLBACK = 4;
  
  public static final int SEND_STATUS_ERROR_RETRY = 3;
  
  public static final int SEND_STATUS_OK = 1;
  
  public static final int STATUS_REPORT_STATUS_ERROR = 2;
  
  public static final int STATUS_REPORT_STATUS_OK = 1;
  
  private IImsSmsListener mListener;
  
  private final Object mLock = new Object();
  
  public final void registerSmsListener(IImsSmsListener paramIImsSmsListener) {
    synchronized (this.mLock) {
      this.mListener = paramIImsSmsListener;
      return;
    } 
  }
  
  public void sendSms(int paramInt1, int paramInt2, String paramString1, String paramString2, boolean paramBoolean, byte[] paramArrayOfbyte) {
    try {
      onSendSmsResult(paramInt1, paramInt2, 2, 1);
    } catch (RuntimeException runtimeException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Can not send sms: ");
      stringBuilder.append(runtimeException.getMessage());
      Log.e("SmsImplBase", stringBuilder.toString());
    } 
  }
  
  public void acknowledgeSms(int paramInt1, int paramInt2, int paramInt3) {
    Log.e("SmsImplBase", "acknowledgeSms() not implemented.");
  }
  
  public void acknowledgeSmsReport(int paramInt1, int paramInt2, int paramInt3) {
    Log.e("SmsImplBase", "acknowledgeSmsReport() not implemented.");
  }
  
  public final void onSmsReceived(int paramInt, String paramString, byte[] paramArrayOfbyte) throws RuntimeException {
    synchronized (this.mLock) {
      IImsSmsListener iImsSmsListener = this.mListener;
      if (iImsSmsListener != null) {
        try {
          this.mListener.onSmsReceived(paramInt, paramString, paramArrayOfbyte);
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Can not deliver sms: ");
          stringBuilder.append(remoteException.getMessage());
          Log.e("SmsImplBase", stringBuilder.toString());
          SmsMessage smsMessage = SmsMessage.createFromPdu(paramArrayOfbyte, paramString);
          if (smsMessage != null && smsMessage.mWrappedSmsMessage != null) {
            acknowledgeSms(paramInt, smsMessage.mWrappedSmsMessage.mMessageRef, 2);
          } else {
            Log.w("SmsImplBase", "onSmsReceived: Invalid pdu entered.");
            acknowledgeSms(paramInt, 0, 2);
          } 
        } 
        return;
      } 
      RuntimeException runtimeException = new RuntimeException();
      this("Feature not ready.");
      throw runtimeException;
    } 
  }
  
  public final void onSendSmsResultSuccess(int paramInt1, int paramInt2) throws RuntimeException {
    synchronized (this.mLock) {
      IImsSmsListener iImsSmsListener = this.mListener;
      if (iImsSmsListener != null) {
        try {
          this.mListener.onSendSmsResult(paramInt1, paramInt2, 1, 0, -1);
        } catch (RemoteException remoteException) {
          remoteException.rethrowFromSystemServer();
        } 
        return;
      } 
      RuntimeException runtimeException = new RuntimeException();
      this("Feature not ready.");
      throw runtimeException;
    } 
  }
  
  @Deprecated
  public final void onSendSmsResult(int paramInt1, int paramInt2, int paramInt3, int paramInt4) throws RuntimeException {
    synchronized (this.mLock) {
      IImsSmsListener iImsSmsListener = this.mListener;
      if (iImsSmsListener != null) {
        try {
          this.mListener.onSendSmsResult(paramInt1, paramInt2, paramInt3, paramInt4, -1);
        } catch (RemoteException remoteException) {
          remoteException.rethrowFromSystemServer();
        } 
        return;
      } 
      RuntimeException runtimeException = new RuntimeException();
      this("Feature not ready.");
      throw runtimeException;
    } 
  }
  
  public final void onSendSmsResultError(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) throws RuntimeException {
    synchronized (this.mLock) {
      IImsSmsListener iImsSmsListener = this.mListener;
      if (iImsSmsListener != null) {
        try {
          this.mListener.onSendSmsResult(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
        } catch (RemoteException remoteException) {
          remoteException.rethrowFromSystemServer();
        } 
        return;
      } 
      RuntimeException runtimeException = new RuntimeException();
      this("Feature not ready.");
      throw runtimeException;
    } 
  }
  
  @Deprecated
  public final void onSmsStatusReportReceived(int paramInt1, int paramInt2, String paramString, byte[] paramArrayOfbyte) throws RuntimeException {
    synchronized (this.mLock) {
      IImsSmsListener iImsSmsListener = this.mListener;
      if (iImsSmsListener != null) {
        try {
          this.mListener.onSmsStatusReportReceived(paramInt1, paramString, paramArrayOfbyte);
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Can not process sms status report: ");
          stringBuilder.append(remoteException.getMessage());
          Log.e("SmsImplBase", stringBuilder.toString());
          acknowledgeSmsReport(paramInt1, paramInt2, 2);
        } 
        return;
      } 
      RuntimeException runtimeException = new RuntimeException();
      this("Feature not ready.");
      throw runtimeException;
    } 
  }
  
  public final void onSmsStatusReportReceived(int paramInt, String paramString, byte[] paramArrayOfbyte) throws RuntimeException {
    synchronized (this.mLock) {
      IImsSmsListener iImsSmsListener = this.mListener;
      if (iImsSmsListener != null) {
        try {
          this.mListener.onSmsStatusReportReceived(paramInt, paramString, paramArrayOfbyte);
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Can not process sms status report: ");
          stringBuilder.append(remoteException.getMessage());
          Log.e("SmsImplBase", stringBuilder.toString());
          SmsMessage smsMessage = SmsMessage.createFromPdu(paramArrayOfbyte, paramString);
          if (smsMessage != null && smsMessage.mWrappedSmsMessage != null) {
            acknowledgeSmsReport(paramInt, smsMessage.mWrappedSmsMessage.mMessageRef, 2);
          } else {
            Log.w("SmsImplBase", "onSmsStatusReportReceivedWithoutMessageRef: Invalid pdu entered.");
            acknowledgeSmsReport(paramInt, 0, 2);
          } 
        } 
        return;
      } 
      RuntimeException runtimeException = new RuntimeException();
      this("Feature not ready.");
      throw runtimeException;
    } 
  }
  
  public String getSmsFormat() {
    return "3gpp";
  }
  
  public void onReady() {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface DeliverStatusResult {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SendStatusResult {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface StatusReportResult {}
}
