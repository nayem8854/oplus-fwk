package android.telephony.ims.stub;

import android.annotation.SystemApi;
import android.os.RemoteException;
import android.util.Log;
import com.android.ims.internal.IImsEcbm;
import com.android.ims.internal.IImsEcbmListener;

@SystemApi
public class ImsEcbmImplBase {
  private static final String TAG = "ImsEcbmImplBase";
  
  private IImsEcbm mImsEcbm = (IImsEcbm)new Object(this);
  
  private IImsEcbmListener mListener;
  
  public IImsEcbm getImsEcbm() {
    return this.mImsEcbm;
  }
  
  public void exitEmergencyCallbackMode() {
    Log.d("ImsEcbmImplBase", "exitEmergencyCallbackMode() not implemented");
  }
  
  public final void enteredEcbm() {
    Log.d("ImsEcbmImplBase", "Entered ECBM.");
    IImsEcbmListener iImsEcbmListener = this.mListener;
    if (iImsEcbmListener != null)
      try {
        iImsEcbmListener.enteredECBM();
      } catch (RemoteException remoteException) {
        throw new RuntimeException(remoteException);
      }  
  }
  
  public final void exitedEcbm() {
    Log.d("ImsEcbmImplBase", "Exited ECBM.");
    IImsEcbmListener iImsEcbmListener = this.mListener;
    if (iImsEcbmListener != null)
      try {
        iImsEcbmListener.exitedECBM();
      } catch (RemoteException remoteException) {
        throw new RuntimeException(remoteException);
      }  
  }
}
