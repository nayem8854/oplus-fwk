package android.telephony.ims.stub;

import android.annotation.SystemApi;
import android.net.Uri;
import android.os.RemoteException;
import android.telephony.ims.ImsReasonInfo;
import android.telephony.ims.aidl.IImsRegistration;
import android.telephony.ims.aidl.IImsRegistrationCallback;
import android.util.Log;
import com.android.internal.telephony.util.RemoteCallbackListExt;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public class ImsRegistrationImplBase {
  private final IImsRegistration mBinder = (IImsRegistration)new Object(this);
  
  private final RemoteCallbackListExt<IImsRegistrationCallback> mCallbacks = new RemoteCallbackListExt();
  
  private final Object mLock = new Object();
  
  private int mConnectionType = -1;
  
  private int mRegistrationState = -1;
  
  private ImsReasonInfo mLastDisconnectCause = new ImsReasonInfo();
  
  private static final String LOG_TAG = "ImsRegistrationImplBase";
  
  private static final int REGISTRATION_STATE_UNKNOWN = -1;
  
  public static final int REGISTRATION_TECH_IWLAN = 1;
  
  public static final int REGISTRATION_TECH_LTE = 0;
  
  public static final int REGISTRATION_TECH_NONE = -1;
  
  public final IImsRegistration getBinder() {
    return this.mBinder;
  }
  
  private void addRegistrationCallback(IImsRegistrationCallback paramIImsRegistrationCallback) throws RemoteException {
    this.mCallbacks.register(paramIImsRegistrationCallback);
    updateNewCallbackWithState(paramIImsRegistrationCallback);
  }
  
  private void removeRegistrationCallback(IImsRegistrationCallback paramIImsRegistrationCallback) {
    this.mCallbacks.unregister(paramIImsRegistrationCallback);
  }
  
  public final void onRegistered(int paramInt) {
    updateToState(paramInt, 2);
    this.mCallbacks.broadcastAction(new _$$Lambda$ImsRegistrationImplBase$cWwTXSDsk_bWPbsDJYI__DUBMnE(paramInt));
  }
  
  public final void onRegistering(int paramInt) {
    updateToState(paramInt, 1);
    this.mCallbacks.broadcastAction(new _$$Lambda$ImsRegistrationImplBase$sbjuTvW_brOSWMR74UInSZEIQB0(paramInt));
  }
  
  public final void onDeregistered(ImsReasonInfo paramImsReasonInfo) {
    updateToDisconnectedState(paramImsReasonInfo);
    if (paramImsReasonInfo == null)
      paramImsReasonInfo = new ImsReasonInfo(); 
    this.mCallbacks.broadcastAction(new _$$Lambda$ImsRegistrationImplBase$s7PspXVbCf1Q_WSzodP2glP9TjI(paramImsReasonInfo));
  }
  
  public final void onTechnologyChangeFailed(int paramInt, ImsReasonInfo paramImsReasonInfo) {
    if (paramImsReasonInfo == null)
      paramImsReasonInfo = new ImsReasonInfo(); 
    this.mCallbacks.broadcastAction(new _$$Lambda$ImsRegistrationImplBase$wDtW65cPmn_jF6dfimhBTfdg1kI(paramInt, paramImsReasonInfo));
  }
  
  public final void onSubscriberAssociatedUriChanged(Uri[] paramArrayOfUri) {
    this.mCallbacks.broadcastAction(new _$$Lambda$ImsRegistrationImplBase$wwtkoeOtGwMjG5I0_ZTfjNpGU_s(paramArrayOfUri));
  }
  
  private void updateToState(int paramInt1, int paramInt2) {
    synchronized (this.mLock) {
      this.mConnectionType = paramInt1;
      this.mRegistrationState = paramInt2;
      this.mLastDisconnectCause = null;
      return;
    } 
  }
  
  private void updateToDisconnectedState(ImsReasonInfo paramImsReasonInfo) {
    synchronized (this.mLock) {
      updateToState(-1, 0);
      if (paramImsReasonInfo != null) {
        this.mLastDisconnectCause = paramImsReasonInfo;
      } else {
        Log.w("ImsRegistrationImplBase", "updateToDisconnectedState: no ImsReasonInfo provided.");
        paramImsReasonInfo = new ImsReasonInfo();
        this();
        this.mLastDisconnectCause = paramImsReasonInfo;
      } 
      return;
    } 
  }
  
  public final int getConnectionType() {
    synchronized (this.mLock) {
      return this.mConnectionType;
    } 
  }
  
  private void updateNewCallbackWithState(IImsRegistrationCallback paramIImsRegistrationCallback) throws RemoteException {
    synchronized (this.mLock) {
      int i = this.mRegistrationState;
      ImsReasonInfo imsReasonInfo = this.mLastDisconnectCause;
      if (i != 0) {
        if (i != 1) {
          if (i == 2)
            paramIImsRegistrationCallback.onRegistered(getConnectionType()); 
        } else {
          paramIImsRegistrationCallback.onRegistering(getConnectionType());
        } 
      } else {
        paramIImsRegistrationCallback.onDeregistered(imsReasonInfo);
      } 
      return;
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ImsRegistrationTech {}
}
