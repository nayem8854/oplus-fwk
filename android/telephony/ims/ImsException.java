package android.telephony.ims;

import android.annotation.SystemApi;
import android.text.TextUtils;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class ImsException extends Exception {
  public static final int CODE_ERROR_INVALID_SUBSCRIPTION = 3;
  
  public static final int CODE_ERROR_SERVICE_UNAVAILABLE = 1;
  
  public static final int CODE_ERROR_UNSPECIFIED = 0;
  
  public static final int CODE_ERROR_UNSUPPORTED_OPERATION = 2;
  
  private int mCode = 0;
  
  @SystemApi
  public ImsException(String paramString) {
    super(getMessage(paramString, 0));
  }
  
  @SystemApi
  public ImsException(String paramString, int paramInt) {
    super(getMessage(paramString, paramInt));
    this.mCode = paramInt;
  }
  
  @SystemApi
  public ImsException(String paramString, int paramInt, Throwable paramThrowable) {
    super(getMessage(paramString, paramInt), paramThrowable);
    this.mCode = paramInt;
  }
  
  public int getCode() {
    return this.mCode;
  }
  
  private static String getMessage(String paramString, int paramInt) {
    if (!TextUtils.isEmpty(paramString)) {
      StringBuilder stringBuilder1 = new StringBuilder(paramString);
      stringBuilder1.append(" (code: ");
      stringBuilder1.append(paramInt);
      stringBuilder1.append(")");
      return stringBuilder1.toString();
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("code: ");
    stringBuilder.append(paramInt);
    return stringBuilder.toString();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ImsErrorCode {}
}
