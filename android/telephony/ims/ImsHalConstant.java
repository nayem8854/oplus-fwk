package android.telephony.ims;

public interface ImsHalConstant {
  public static final int REQUEST_QUERY_VOPS_STATUS = 1;
  
  public static final int REQUEST_SEND_OEM_COMMAND = 2;
}
