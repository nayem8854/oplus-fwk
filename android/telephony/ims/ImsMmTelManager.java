package android.telephony.ims;

import android.annotation.SystemApi;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceSpecificException;
import android.os.TelephonyServiceManager;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyFrameworkInitializer;
import android.telephony.ims.aidl.IImsCapabilityCallback;
import android.telephony.ims.feature.MmTelFeature;
import com.android.internal.telephony.IIntegerConsumer;
import com.android.internal.telephony.ITelephony;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.Executor;
import java.util.function.Consumer;

public class ImsMmTelManager implements RegistrationManager {
  public static final int WIFI_MODE_CELLULAR_PREFERRED = 1;
  
  public static final int WIFI_MODE_WIFI_ONLY = 0;
  
  public static final int WIFI_MODE_WIFI_PREFERRED = 2;
  
  private final int mSubId;
  
  @Retention(RetentionPolicy.SOURCE)
  class WiFiCallingMode implements Annotation {}
  
  @SystemApi
  @Deprecated
  public static class RegistrationCallback extends RegistrationManager.RegistrationCallback {
    public void onRegistered(int param1Int) {}
    
    public void onRegistering(int param1Int) {}
    
    public void onUnregistered(ImsReasonInfo param1ImsReasonInfo) {}
    
    public void onTechnologyChangeFailed(int param1Int, ImsReasonInfo param1ImsReasonInfo) {}
  }
  
  class CapabilityCallback {
    class CapabilityBinder extends IImsCapabilityCallback.Stub {
      private Executor mExecutor;
      
      private final ImsMmTelManager.CapabilityCallback mLocalCallback;
      
      CapabilityBinder(ImsMmTelManager.CapabilityCallback this$0) {
        this.mLocalCallback = this$0;
      }
      
      public void onCapabilitiesStatusChanged(int param2Int) {
        if (this.mLocalCallback == null)
          return; 
        long l = Binder.clearCallingIdentity();
        try {
          Executor executor = this.mExecutor;
          _$$Lambda$ImsMmTelManager$CapabilityCallback$CapabilityBinder$4YNlUy9HsD02E7Sbv2VeVtbao08 _$$Lambda$ImsMmTelManager$CapabilityCallback$CapabilityBinder$4YNlUy9HsD02E7Sbv2VeVtbao08 = new _$$Lambda$ImsMmTelManager$CapabilityCallback$CapabilityBinder$4YNlUy9HsD02E7Sbv2VeVtbao08();
          this(this, param2Int);
          executor.execute(_$$Lambda$ImsMmTelManager$CapabilityCallback$CapabilityBinder$4YNlUy9HsD02E7Sbv2VeVtbao08);
          return;
        } finally {
          restoreCallingIdentity(l);
        } 
      }
      
      public void onQueryCapabilityConfiguration(int param2Int1, int param2Int2, boolean param2Boolean) {}
      
      public void onChangeCapabilityConfigurationError(int param2Int1, int param2Int2, int param2Int3) {}
      
      private void setExecutor(Executor param2Executor) {
        this.mExecutor = param2Executor;
      }
    }
    
    private final CapabilityBinder mBinder = new CapabilityBinder(this);
    
    public void onCapabilitiesStatusChanged(MmTelFeature.MmTelCapabilities param1MmTelCapabilities) {}
    
    public final IImsCapabilityCallback getBinder() {
      return this.mBinder;
    }
    
    public final void setExecutor(Executor param1Executor) {
      this.mBinder.setExecutor(param1Executor);
    }
  }
  
  @SystemApi
  @Deprecated
  public static ImsMmTelManager createForSubscriptionId(int paramInt) {
    if (SubscriptionManager.isValidSubscriptionId(paramInt))
      return new ImsMmTelManager(paramInt); 
    throw new IllegalArgumentException("Invalid subscription ID");
  }
  
  public ImsMmTelManager(int paramInt) {
    this.mSubId = paramInt;
  }
  
  @SystemApi
  @Deprecated
  public void registerImsRegistrationCallback(Executor paramExecutor, RegistrationCallback paramRegistrationCallback) throws ImsException {
    if (paramRegistrationCallback != null) {
      if (paramExecutor != null) {
        paramRegistrationCallback.setExecutor(paramExecutor);
        ITelephony iTelephony = getITelephony();
        if (iTelephony != null)
          try {
            iTelephony.registerImsRegistrationCallback(this.mSubId, paramRegistrationCallback.getBinder());
            return;
          } catch (ServiceSpecificException serviceSpecificException) {
            if (serviceSpecificException.errorCode == 3)
              throw new IllegalArgumentException(serviceSpecificException.getMessage()); 
            throw new ImsException(serviceSpecificException.getMessage(), serviceSpecificException.errorCode);
          } catch (RemoteException|IllegalStateException remoteException) {
            throw new ImsException(remoteException.getMessage(), 1);
          }  
        throw new ImsException("Could not find Telephony Service.", 1);
      } 
      throw new IllegalArgumentException("Must include a non-null Executor.");
    } 
    throw new IllegalArgumentException("Must include a non-null RegistrationCallback.");
  }
  
  public void registerImsRegistrationCallback(Executor paramExecutor, RegistrationManager.RegistrationCallback paramRegistrationCallback) throws ImsException {
    if (paramRegistrationCallback != null) {
      if (paramExecutor != null) {
        paramRegistrationCallback.setExecutor(paramExecutor);
        ITelephony iTelephony = getITelephony();
        if (iTelephony != null)
          try {
            iTelephony.registerImsRegistrationCallback(this.mSubId, paramRegistrationCallback.getBinder());
            return;
          } catch (ServiceSpecificException serviceSpecificException) {
            throw new ImsException(serviceSpecificException.getMessage(), serviceSpecificException.errorCode);
          } catch (RemoteException|IllegalStateException remoteException) {
            throw new ImsException(remoteException.getMessage(), 1);
          }  
        throw new ImsException("Could not find Telephony Service.", 1);
      } 
      throw new IllegalArgumentException("Must include a non-null Executor.");
    } 
    throw new IllegalArgumentException("Must include a non-null RegistrationCallback.");
  }
  
  @SystemApi
  @Deprecated
  public void unregisterImsRegistrationCallback(RegistrationCallback paramRegistrationCallback) {
    if (paramRegistrationCallback != null) {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        try {
          iTelephony.unregisterImsRegistrationCallback(this.mSubId, paramRegistrationCallback.getBinder());
          return;
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowAsRuntimeException();
        }  
      throw new RuntimeException("Could not find Telephony Service.");
    } 
    throw new IllegalArgumentException("Must include a non-null RegistrationCallback.");
  }
  
  public void unregisterImsRegistrationCallback(RegistrationManager.RegistrationCallback paramRegistrationCallback) {
    if (paramRegistrationCallback != null) {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        try {
          iTelephony.unregisterImsRegistrationCallback(this.mSubId, paramRegistrationCallback.getBinder());
          return;
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowAsRuntimeException();
        }  
      throw new RuntimeException("Could not find Telephony Service.");
    } 
    throw new IllegalArgumentException("Must include a non-null RegistrationCallback.");
  }
  
  @SystemApi
  public void getRegistrationState(Executor paramExecutor, Consumer<Integer> paramConsumer) {
    if (paramConsumer != null) {
      if (paramExecutor != null) {
        ITelephony iTelephony = getITelephony();
        if (iTelephony != null)
          try {
            int i = this.mSubId;
            Object object = new Object();
            super(this, paramExecutor, paramConsumer);
            iTelephony.getImsMmTelRegistrationState(i, (IIntegerConsumer)object);
            return;
          } catch (RemoteException remoteException) {
            throw remoteException.rethrowAsRuntimeException();
          }  
        throw new RuntimeException("Could not find Telephony Service.");
      } 
      throw new IllegalArgumentException("Must include a non-null Executor.");
    } 
    throw new IllegalArgumentException("Must include a non-null callback.");
  }
  
  public void getRegistrationTransportType(Executor paramExecutor, Consumer<Integer> paramConsumer) {
    if (paramConsumer != null) {
      if (paramExecutor != null) {
        ITelephony iTelephony = getITelephony();
        if (iTelephony != null)
          try {
            int i = this.mSubId;
            Object object = new Object();
            super(this, paramExecutor, paramConsumer);
            iTelephony.getImsMmTelRegistrationTransportType(i, (IIntegerConsumer)object);
            return;
          } catch (RemoteException remoteException) {
            throw remoteException.rethrowAsRuntimeException();
          }  
        throw new RuntimeException("Could not find Telephony Service.");
      } 
      throw new IllegalArgumentException("Must include a non-null Executor.");
    } 
    throw new IllegalArgumentException("Must include a non-null callback.");
  }
  
  public void registerMmTelCapabilityCallback(Executor paramExecutor, CapabilityCallback paramCapabilityCallback) throws ImsException {
    if (paramCapabilityCallback != null) {
      if (paramExecutor != null) {
        paramCapabilityCallback.setExecutor(paramExecutor);
        ITelephony iTelephony = getITelephony();
        if (iTelephony != null)
          try {
            iTelephony.registerMmTelCapabilityCallback(this.mSubId, paramCapabilityCallback.getBinder());
            return;
          } catch (ServiceSpecificException serviceSpecificException) {
            throw new ImsException(serviceSpecificException.getMessage(), serviceSpecificException.errorCode);
          } catch (RemoteException remoteException) {
            throw remoteException.rethrowAsRuntimeException();
          } catch (IllegalStateException illegalStateException) {
            throw new ImsException(illegalStateException.getMessage(), 1);
          }  
        throw new ImsException("Could not find Telephony Service.", 1);
      } 
      throw new IllegalArgumentException("Must include a non-null Executor.");
    } 
    throw new IllegalArgumentException("Must include a non-null RegistrationCallback.");
  }
  
  public void unregisterMmTelCapabilityCallback(CapabilityCallback paramCapabilityCallback) {
    if (paramCapabilityCallback != null) {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        try {
          iTelephony.unregisterMmTelCapabilityCallback(this.mSubId, paramCapabilityCallback.getBinder());
          return;
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowAsRuntimeException();
        }  
      throw new RuntimeException("Could not find Telephony Service.");
    } 
    throw new IllegalArgumentException("Must include a non-null RegistrationCallback.");
  }
  
  public boolean isAdvancedCallingSettingEnabled() {
    ITelephony iTelephony = getITelephony();
    if (iTelephony != null)
      try {
        return iTelephony.isAdvancedCallingSettingEnabled(this.mSubId);
      } catch (ServiceSpecificException serviceSpecificException) {
        if (serviceSpecificException.errorCode == 3)
          throw new IllegalArgumentException(serviceSpecificException.getMessage()); 
        throw new RuntimeException(serviceSpecificException.getMessage());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      }  
    throw new RuntimeException("Could not find Telephony Service.");
  }
  
  @SystemApi
  public void setAdvancedCallingSettingEnabled(boolean paramBoolean) {
    ITelephony iTelephony = getITelephony();
    if (iTelephony != null)
      try {
        iTelephony.setAdvancedCallingSettingEnabled(this.mSubId, paramBoolean);
        return;
      } catch (ServiceSpecificException serviceSpecificException) {
        if (serviceSpecificException.errorCode == 3)
          throw new IllegalArgumentException(serviceSpecificException.getMessage()); 
        throw new RuntimeException(serviceSpecificException.getMessage());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      }  
    throw new RuntimeException("Could not find Telephony Service.");
  }
  
  @SystemApi
  public boolean isCapable(int paramInt1, int paramInt2) {
    ITelephony iTelephony = getITelephony();
    if (iTelephony != null)
      try {
        return iTelephony.isCapable(this.mSubId, paramInt1, paramInt2);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      }  
    throw new RuntimeException("Could not find Telephony Service.");
  }
  
  @SystemApi
  public boolean isAvailable(int paramInt1, int paramInt2) {
    ITelephony iTelephony = getITelephony();
    if (iTelephony != null)
      try {
        return iTelephony.isAvailable(this.mSubId, paramInt1, paramInt2);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      }  
    throw new RuntimeException("Could not find Telephony Service.");
  }
  
  @SystemApi
  public void isSupported(int paramInt1, int paramInt2, Executor paramExecutor, Consumer<Boolean> paramConsumer) throws ImsException {
    if (paramConsumer != null) {
      if (paramExecutor != null) {
        ITelephony iTelephony = getITelephony();
        if (iTelephony != null)
          try {
            iTelephony = getITelephony();
            int i = this.mSubId;
            Object object = new Object();
            super(this, paramExecutor, paramConsumer);
            iTelephony.isMmTelCapabilitySupported(i, (IIntegerConsumer)object, paramInt1, paramInt2);
            return;
          } catch (ServiceSpecificException serviceSpecificException) {
            throw new ImsException(serviceSpecificException.getMessage(), serviceSpecificException.errorCode);
          } catch (RemoteException remoteException) {
            remoteException.rethrowAsRuntimeException();
            return;
          }  
        throw new ImsException("Could not find Telephony Service.", 1);
      } 
      throw new IllegalArgumentException("Must include a non-null Executor.");
    } 
    throw new IllegalArgumentException("Must include a non-null Consumer.");
  }
  
  public boolean isVtSettingEnabled() {
    ITelephony iTelephony = getITelephony();
    if (iTelephony != null)
      try {
        return iTelephony.isVtSettingEnabled(this.mSubId);
      } catch (ServiceSpecificException serviceSpecificException) {
        if (serviceSpecificException.errorCode == 3)
          throw new IllegalArgumentException(serviceSpecificException.getMessage()); 
        throw new RuntimeException(serviceSpecificException.getMessage());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      }  
    throw new RuntimeException("Could not find Telephony Service.");
  }
  
  @SystemApi
  public void setVtSettingEnabled(boolean paramBoolean) {
    ITelephony iTelephony = getITelephony();
    if (iTelephony != null)
      try {
        iTelephony.setVtSettingEnabled(this.mSubId, paramBoolean);
        return;
      } catch (ServiceSpecificException serviceSpecificException) {
        if (serviceSpecificException.errorCode == 3)
          throw new IllegalArgumentException(serviceSpecificException.getMessage()); 
        throw new RuntimeException(serviceSpecificException.getMessage());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      }  
    throw new RuntimeException("Could not find Telephony Service.");
  }
  
  public boolean isVoWiFiSettingEnabled() {
    ITelephony iTelephony = getITelephony();
    if (iTelephony != null)
      try {
        return iTelephony.isVoWiFiSettingEnabled(this.mSubId);
      } catch (ServiceSpecificException serviceSpecificException) {
        if (serviceSpecificException.errorCode == 3)
          throw new IllegalArgumentException(serviceSpecificException.getMessage()); 
        throw new RuntimeException(serviceSpecificException.getMessage());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      }  
    throw new RuntimeException("Could not find Telephony Service.");
  }
  
  @SystemApi
  public void setVoWiFiSettingEnabled(boolean paramBoolean) {
    ITelephony iTelephony = getITelephony();
    if (iTelephony != null)
      try {
        iTelephony.setVoWiFiSettingEnabled(this.mSubId, paramBoolean);
        return;
      } catch (ServiceSpecificException serviceSpecificException) {
        if (serviceSpecificException.errorCode == 3)
          throw new IllegalArgumentException(serviceSpecificException.getMessage()); 
        throw new RuntimeException(serviceSpecificException.getMessage());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      }  
    throw new RuntimeException("Could not find Telephony Service.");
  }
  
  public boolean isVoWiFiRoamingSettingEnabled() {
    ITelephony iTelephony = getITelephony();
    if (iTelephony != null)
      try {
        return iTelephony.isVoWiFiRoamingSettingEnabled(this.mSubId);
      } catch (ServiceSpecificException serviceSpecificException) {
        if (serviceSpecificException.errorCode == 3)
          throw new IllegalArgumentException(serviceSpecificException.getMessage()); 
        throw new RuntimeException(serviceSpecificException.getMessage());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      }  
    throw new RuntimeException("Could not find Telephony Service.");
  }
  
  @SystemApi
  public void setVoWiFiRoamingSettingEnabled(boolean paramBoolean) {
    ITelephony iTelephony = getITelephony();
    if (iTelephony != null)
      try {
        iTelephony.setVoWiFiRoamingSettingEnabled(this.mSubId, paramBoolean);
        return;
      } catch (ServiceSpecificException serviceSpecificException) {
        if (serviceSpecificException.errorCode == 3)
          throw new IllegalArgumentException(serviceSpecificException.getMessage()); 
        throw new RuntimeException(serviceSpecificException.getMessage());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      }  
    throw new RuntimeException("Could not find Telephony Service.");
  }
  
  @SystemApi
  public void setVoWiFiNonPersistent(boolean paramBoolean, int paramInt) {
    ITelephony iTelephony = getITelephony();
    if (iTelephony != null)
      try {
        iTelephony.setVoWiFiNonPersistent(this.mSubId, paramBoolean, paramInt);
        return;
      } catch (ServiceSpecificException serviceSpecificException) {
        if (serviceSpecificException.errorCode == 3)
          throw new IllegalArgumentException(serviceSpecificException.getMessage()); 
        throw new RuntimeException(serviceSpecificException.getMessage());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      }  
    throw new RuntimeException("Could not find Telephony Service.");
  }
  
  public int getVoWiFiModeSetting() {
    ITelephony iTelephony = getITelephony();
    if (iTelephony != null)
      try {
        return iTelephony.getVoWiFiModeSetting(this.mSubId);
      } catch (ServiceSpecificException serviceSpecificException) {
        if (serviceSpecificException.errorCode == 3)
          throw new IllegalArgumentException(serviceSpecificException.getMessage()); 
        throw new RuntimeException(serviceSpecificException.getMessage());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      }  
    throw new RuntimeException("Could not find Telephony Service.");
  }
  
  @SystemApi
  public void setVoWiFiModeSetting(int paramInt) {
    ITelephony iTelephony = getITelephony();
    if (iTelephony != null)
      try {
        iTelephony.setVoWiFiModeSetting(this.mSubId, paramInt);
        return;
      } catch (ServiceSpecificException serviceSpecificException) {
        if (serviceSpecificException.errorCode == 3)
          throw new IllegalArgumentException(serviceSpecificException.getMessage()); 
        throw new RuntimeException(serviceSpecificException.getMessage());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      }  
    throw new RuntimeException("Could not find Telephony Service.");
  }
  
  @SystemApi
  public int getVoWiFiRoamingModeSetting() {
    ITelephony iTelephony = getITelephony();
    if (iTelephony != null)
      try {
        return iTelephony.getVoWiFiRoamingModeSetting(this.mSubId);
      } catch (ServiceSpecificException serviceSpecificException) {
        if (serviceSpecificException.errorCode == 3)
          throw new IllegalArgumentException(serviceSpecificException.getMessage()); 
        throw new RuntimeException(serviceSpecificException.getMessage());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      }  
    throw new RuntimeException("Could not find Telephony Service.");
  }
  
  @SystemApi
  public void setVoWiFiRoamingModeSetting(int paramInt) {
    ITelephony iTelephony = getITelephony();
    if (iTelephony != null)
      try {
        iTelephony.setVoWiFiRoamingModeSetting(this.mSubId, paramInt);
        return;
      } catch (ServiceSpecificException serviceSpecificException) {
        if (serviceSpecificException.errorCode == 3)
          throw new IllegalArgumentException(serviceSpecificException.getMessage()); 
        throw new RuntimeException(serviceSpecificException.getMessage());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      }  
    throw new RuntimeException("Could not find Telephony Service.");
  }
  
  @SystemApi
  public void setRttCapabilitySetting(boolean paramBoolean) {
    ITelephony iTelephony = getITelephony();
    if (iTelephony != null)
      try {
        iTelephony.setRttCapabilitySetting(this.mSubId, paramBoolean);
        return;
      } catch (ServiceSpecificException serviceSpecificException) {
        if (serviceSpecificException.errorCode == 3)
          throw new IllegalArgumentException(serviceSpecificException.getMessage()); 
        throw new RuntimeException(serviceSpecificException.getMessage());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      }  
    throw new RuntimeException("Could not find Telephony Service.");
  }
  
  public boolean isTtyOverVolteEnabled() {
    ITelephony iTelephony = getITelephony();
    if (iTelephony != null)
      try {
        return iTelephony.isTtyOverVolteEnabled(this.mSubId);
      } catch (ServiceSpecificException serviceSpecificException) {
        if (serviceSpecificException.errorCode == 3)
          throw new IllegalArgumentException(serviceSpecificException.getMessage()); 
        throw new RuntimeException(serviceSpecificException.getMessage());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      }  
    throw new RuntimeException("Could not find Telephony Service.");
  }
  
  @SystemApi
  public void getFeatureState(Executor paramExecutor, Consumer<Integer> paramConsumer) throws ImsException {
    if (paramExecutor != null) {
      if (paramConsumer != null) {
        ITelephony iTelephony = getITelephony();
        if (iTelephony != null)
          try {
            int i = this.mSubId;
            Object object = new Object();
            super(this, paramExecutor, paramConsumer);
            iTelephony.getImsMmTelFeatureState(i, (IIntegerConsumer)object);
            return;
          } catch (ServiceSpecificException serviceSpecificException) {
            throw new ImsException(serviceSpecificException.getMessage(), serviceSpecificException.errorCode);
          } catch (RemoteException remoteException) {
            remoteException.rethrowAsRuntimeException();
            return;
          }  
        throw new ImsException("Could not find Telephony Service.", 1);
      } 
      throw new IllegalArgumentException("Must include a non-null Consumer.");
    } 
    throw new IllegalArgumentException("Must include a non-null Executor.");
  }
  
  private static ITelephony getITelephony() {
    TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
    TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getTelephonyServiceRegisterer();
    IBinder iBinder = serviceRegisterer.get();
    return ITelephony.Stub.asInterface(iBinder);
  }
}
