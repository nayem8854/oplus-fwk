package android.telephony.ims;

import android.net.Uri;
import android.os.Binder;
import android.telephony.ims.aidl.IImsRegistrationCallback;
import android.util.Log;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.function.Consumer;

public interface RegistrationManager {
  public static final Map<Integer, Integer> IMS_REG_TO_ACCESS_TYPE_MAP = new HashMap<Integer, Integer>() {
    
    };
  
  public static final int REGISTRATION_STATE_NOT_REGISTERED = 0;
  
  public static final int REGISTRATION_STATE_REGISTERED = 2;
  
  public static final int REGISTRATION_STATE_REGISTERING = 1;
  
  void getRegistrationState(Executor paramExecutor, Consumer<Integer> paramConsumer);
  
  void getRegistrationTransportType(Executor paramExecutor, Consumer<Integer> paramConsumer);
  
  void registerImsRegistrationCallback(Executor paramExecutor, RegistrationCallback paramRegistrationCallback) throws ImsException;
  
  void unregisterImsRegistrationCallback(RegistrationCallback paramRegistrationCallback);
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ImsRegistrationState {}
  
  public static class RegistrationCallback {
    class RegistrationBinder extends IImsRegistrationCallback.Stub {
      private Executor mExecutor;
      
      private final RegistrationManager.RegistrationCallback mLocalCallback;
      
      RegistrationBinder(RegistrationManager.RegistrationCallback this$0) {
        this.mLocalCallback = this$0;
      }
      
      public void onRegistered(int param2Int) {
        if (this.mLocalCallback == null)
          return; 
        long l = Binder.clearCallingIdentity();
        try {
          Executor executor = this.mExecutor;
          _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$uTxkp6C02qJxic1W_dkZRCQ6aRw _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$uTxkp6C02qJxic1W_dkZRCQ6aRw = new _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$uTxkp6C02qJxic1W_dkZRCQ6aRw();
          this(this, param2Int);
          executor.execute(_$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$uTxkp6C02qJxic1W_dkZRCQ6aRw);
          return;
        } finally {
          restoreCallingIdentity(l);
        } 
      }
      
      public void onRegistering(int param2Int) {
        if (this.mLocalCallback == null)
          return; 
        long l = Binder.clearCallingIdentity();
        try {
          Executor executor = this.mExecutor;
          _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$U5KsDZQk3N6Mv43G9MidRPHRmv8 _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$U5KsDZQk3N6Mv43G9MidRPHRmv8 = new _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$U5KsDZQk3N6Mv43G9MidRPHRmv8();
          this(this, param2Int);
          executor.execute(_$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$U5KsDZQk3N6Mv43G9MidRPHRmv8);
          return;
        } finally {
          restoreCallingIdentity(l);
        } 
      }
      
      public void onDeregistered(ImsReasonInfo param2ImsReasonInfo) {
        if (this.mLocalCallback == null)
          return; 
        long l = Binder.clearCallingIdentity();
        try {
          Executor executor = this.mExecutor;
          _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$APeqso3VzZZ0eUf5slP1k5xoCME _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$APeqso3VzZZ0eUf5slP1k5xoCME = new _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$APeqso3VzZZ0eUf5slP1k5xoCME();
          this(this, param2ImsReasonInfo);
          executor.execute(_$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$APeqso3VzZZ0eUf5slP1k5xoCME);
          return;
        } finally {
          restoreCallingIdentity(l);
        } 
      }
      
      public void onTechnologyChangeFailed(int param2Int, ImsReasonInfo param2ImsReasonInfo) {
        if (this.mLocalCallback == null)
          return; 
        long l = Binder.clearCallingIdentity();
        try {
          Executor executor = this.mExecutor;
          _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$fgbmOxWK5ZyS5zNpLgTSXknOOJ4 _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$fgbmOxWK5ZyS5zNpLgTSXknOOJ4 = new _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$fgbmOxWK5ZyS5zNpLgTSXknOOJ4();
          this(this, param2Int, param2ImsReasonInfo);
          executor.execute(_$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$fgbmOxWK5ZyS5zNpLgTSXknOOJ4);
          return;
        } finally {
          restoreCallingIdentity(l);
        } 
      }
      
      public void onSubscriberAssociatedUriChanged(Uri[] param2ArrayOfUri) {
        if (this.mLocalCallback == null)
          return; 
        long l = Binder.clearCallingIdentity();
        try {
          Executor executor = this.mExecutor;
          _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$DX__dWIBwwX2oqDoRnq49RndG7s _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$DX__dWIBwwX2oqDoRnq49RndG7s = new _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$DX__dWIBwwX2oqDoRnq49RndG7s();
          this(this, param2ArrayOfUri);
          executor.execute(_$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$DX__dWIBwwX2oqDoRnq49RndG7s);
          return;
        } finally {
          restoreCallingIdentity(l);
        } 
      }
      
      private void setExecutor(Executor param2Executor) {
        this.mExecutor = param2Executor;
      }
      
      private static int getAccessType(int param2Int) {
        if (!RegistrationManager.IMS_REG_TO_ACCESS_TYPE_MAP.containsKey(Integer.valueOf(param2Int))) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("RegistrationBinder - invalid regType returned: ");
          stringBuilder.append(param2Int);
          Log.w("RegistrationManager", stringBuilder.toString());
          return -1;
        } 
        return ((Integer)RegistrationManager.IMS_REG_TO_ACCESS_TYPE_MAP.get(Integer.valueOf(param2Int))).intValue();
      }
    }
    
    private final RegistrationBinder mBinder = new RegistrationBinder(this);
    
    public void onRegistered(int param1Int) {}
    
    public void onRegistering(int param1Int) {}
    
    public void onUnregistered(ImsReasonInfo param1ImsReasonInfo) {}
    
    public void onTechnologyChangeFailed(int param1Int, ImsReasonInfo param1ImsReasonInfo) {}
    
    public void onSubscriberAssociatedUriChanged(Uri[] param1ArrayOfUri) {}
    
    public final IImsRegistrationCallback getBinder() {
      return this.mBinder;
    }
    
    public void setExecutor(Executor param1Executor) {
      this.mBinder.setExecutor(param1Executor);
    }
  }
  
  class RegistrationBinder extends IImsRegistrationCallback.Stub {
    private Executor mExecutor;
    
    private final RegistrationManager.RegistrationCallback mLocalCallback;
    
    RegistrationBinder(RegistrationManager this$0) {
      this.mLocalCallback = (RegistrationManager.RegistrationCallback)this$0;
    }
    
    public void onRegistered(int param1Int) {
      if (this.mLocalCallback == null)
        return; 
      long l = Binder.clearCallingIdentity();
      try {
        Executor executor = this.mExecutor;
        _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$uTxkp6C02qJxic1W_dkZRCQ6aRw _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$uTxkp6C02qJxic1W_dkZRCQ6aRw = new _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$uTxkp6C02qJxic1W_dkZRCQ6aRw();
        this(this, param1Int);
        executor.execute(_$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$uTxkp6C02qJxic1W_dkZRCQ6aRw);
        return;
      } finally {
        restoreCallingIdentity(l);
      } 
    }
    
    public void onRegistering(int param1Int) {
      if (this.mLocalCallback == null)
        return; 
      long l = Binder.clearCallingIdentity();
      try {
        Executor executor = this.mExecutor;
        _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$U5KsDZQk3N6Mv43G9MidRPHRmv8 _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$U5KsDZQk3N6Mv43G9MidRPHRmv8 = new _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$U5KsDZQk3N6Mv43G9MidRPHRmv8();
        this(this, param1Int);
        executor.execute(_$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$U5KsDZQk3N6Mv43G9MidRPHRmv8);
        return;
      } finally {
        restoreCallingIdentity(l);
      } 
    }
    
    public void onDeregistered(ImsReasonInfo param1ImsReasonInfo) {
      if (this.mLocalCallback == null)
        return; 
      long l = Binder.clearCallingIdentity();
      try {
        Executor executor = this.mExecutor;
        _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$APeqso3VzZZ0eUf5slP1k5xoCME _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$APeqso3VzZZ0eUf5slP1k5xoCME = new _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$APeqso3VzZZ0eUf5slP1k5xoCME();
        this(this, param1ImsReasonInfo);
        executor.execute(_$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$APeqso3VzZZ0eUf5slP1k5xoCME);
        return;
      } finally {
        restoreCallingIdentity(l);
      } 
    }
    
    public void onTechnologyChangeFailed(int param1Int, ImsReasonInfo param1ImsReasonInfo) {
      if (this.mLocalCallback == null)
        return; 
      long l = Binder.clearCallingIdentity();
      try {
        Executor executor = this.mExecutor;
        _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$fgbmOxWK5ZyS5zNpLgTSXknOOJ4 _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$fgbmOxWK5ZyS5zNpLgTSXknOOJ4 = new _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$fgbmOxWK5ZyS5zNpLgTSXknOOJ4();
        this(this, param1Int, param1ImsReasonInfo);
        executor.execute(_$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$fgbmOxWK5ZyS5zNpLgTSXknOOJ4);
        return;
      } finally {
        restoreCallingIdentity(l);
      } 
    }
    
    public void onSubscriberAssociatedUriChanged(Uri[] param1ArrayOfUri) {
      if (this.mLocalCallback == null)
        return; 
      long l = Binder.clearCallingIdentity();
      try {
        Executor executor = this.mExecutor;
        _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$DX__dWIBwwX2oqDoRnq49RndG7s _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$DX__dWIBwwX2oqDoRnq49RndG7s = new _$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$DX__dWIBwwX2oqDoRnq49RndG7s();
        this(this, param1ArrayOfUri);
        executor.execute(_$$Lambda$RegistrationManager$RegistrationCallback$RegistrationBinder$DX__dWIBwwX2oqDoRnq49RndG7s);
        return;
      } finally {
        restoreCallingIdentity(l);
      } 
    }
    
    private void setExecutor(Executor param1Executor) {
      this.mExecutor = param1Executor;
    }
    
    private static int getAccessType(int param1Int) {
      if (!RegistrationManager.IMS_REG_TO_ACCESS_TYPE_MAP.containsKey(Integer.valueOf(param1Int))) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("RegistrationBinder - invalid regType returned: ");
        stringBuilder.append(param1Int);
        Log.w("RegistrationManager", stringBuilder.toString());
        return -1;
      } 
      return ((Integer)RegistrationManager.IMS_REG_TO_ACCESS_TYPE_MAP.get(Integer.valueOf(param1Int))).intValue();
    }
  }
}
