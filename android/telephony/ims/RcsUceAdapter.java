package android.telephony.ims;

import android.annotation.SystemApi;
import android.content.Context;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.TelephonyServiceManager;
import android.telephony.TelephonyFrameworkInitializer;
import android.telephony.ims.aidl.IImsRcsController;
import android.telephony.ims.aidl.IRcsUceControllerCallback;
import android.util.Log;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;
import java.util.concurrent.Executor;

public class RcsUceAdapter {
  public static final int ERROR_ALREADY_IN_QUEUE = 13;
  
  public static final int ERROR_FORBIDDEN = 6;
  
  public static final int ERROR_GENERIC_FAILURE = 1;
  
  public static final int ERROR_INSUFFICIENT_MEMORY = 11;
  
  public static final int ERROR_LOST_NETWORK = 12;
  
  public static final int ERROR_NOT_AUTHORIZED = 5;
  
  public static final int ERROR_NOT_AVAILABLE = 3;
  
  public static final int ERROR_NOT_ENABLED = 2;
  
  public static final int ERROR_NOT_FOUND = 7;
  
  public static final int ERROR_NOT_REGISTERED = 4;
  
  public static final int ERROR_REQUEST_TIMEOUT = 10;
  
  public static final int ERROR_REQUEST_TOO_LARGE = 8;
  
  public static final int PUBLISH_STATE_NOT_PUBLISHED = 2;
  
  public static final int PUBLISH_STATE_OK = 1;
  
  public static final int PUBLISH_STATE_OTHER_ERROR = 6;
  
  public static final int PUBLISH_STATE_RCS_PROVISION_ERROR = 4;
  
  public static final int PUBLISH_STATE_REQUEST_TIMEOUT = 5;
  
  public static final int PUBLISH_STATE_VOLTE_PROVISION_ERROR = 3;
  
  private static final String TAG = "RcsUceAdapter";
  
  private final Context mContext;
  
  private final int mSubId;
  
  public static class CapabilitiesCallback {
    public void onCapabilitiesReceived(List<RcsContactUceCapability> param1List) {}
    
    public void onError(int param1Int) {}
  }
  
  RcsUceAdapter(Context paramContext, int paramInt) {
    this.mContext = paramContext;
    this.mSubId = paramInt;
  }
  
  public void requestCapabilities(Executor paramExecutor, List<Uri> paramList, CapabilitiesCallback paramCapabilitiesCallback) throws ImsException {
    if (paramCapabilitiesCallback != null) {
      if (paramExecutor != null) {
        if (paramList != null) {
          IImsRcsController iImsRcsController = getIImsRcsController();
          if (iImsRcsController != null) {
            Object object = new Object(this, paramExecutor, paramCapabilitiesCallback);
            try {
              int i = this.mSubId;
              String str1 = this.mContext.getOpPackageName();
              Context context = this.mContext;
              String str2 = context.getAttributionTag();
              iImsRcsController.requestCapabilities(i, str1, str2, paramList, (IRcsUceControllerCallback)object);
              return;
            } catch (RemoteException remoteException) {
              Log.e("RcsUceAdapter", "Error calling IImsRcsController#requestCapabilities", (Throwable)remoteException);
              throw new ImsException("Remote IMS Service is not available", 1);
            } 
          } 
          Log.e("RcsUceAdapter", "requestCapabilities: IImsRcsController is null");
          throw new ImsException("Can not find remote IMS service", 1);
        } 
        throw new IllegalArgumentException("Must include non-null contact number list.");
      } 
      throw new IllegalArgumentException("Must include a non-null Executor.");
    } 
    throw new IllegalArgumentException("Must include a non-null AvailabilityCallback.");
  }
  
  public int getUcePublishState() throws ImsException {
    IImsRcsController iImsRcsController = getIImsRcsController();
    if (iImsRcsController != null)
      try {
        return iImsRcsController.getUcePublishState(this.mSubId);
      } catch (RemoteException remoteException) {
        Log.e("RcsUceAdapter", "Error calling IImsRcsController#getUcePublishState", (Throwable)remoteException);
        throw new ImsException("Remote IMS Service is not available", 1);
      }  
    Log.e("RcsUceAdapter", "getUcePublishState: IImsRcsController is null");
    throw new ImsException("Can not find remote IMS service", 1);
  }
  
  public boolean isUceSettingEnabled() throws ImsException {
    IImsRcsController iImsRcsController = getIImsRcsController();
    if (iImsRcsController != null)
      try {
        int i = this.mSubId;
        String str1 = this.mContext.getOpPackageName();
        Context context = this.mContext;
        String str2 = context.getAttributionTag();
        return iImsRcsController.isUceSettingEnabled(i, str1, str2);
      } catch (RemoteException remoteException) {
        Log.e("RcsUceAdapter", "Error calling IImsRcsController#isUceSettingEnabled", (Throwable)remoteException);
        throw new ImsException("Remote IMS Service is not available", 1);
      }  
    Log.e("RcsUceAdapter", "isUceSettingEnabled: IImsRcsController is null");
    throw new ImsException("Can not find remote IMS service", 1);
  }
  
  @SystemApi
  public void setUceSettingEnabled(boolean paramBoolean) throws ImsException {
    IImsRcsController iImsRcsController = getIImsRcsController();
    if (iImsRcsController != null)
      try {
        iImsRcsController.setUceSettingEnabled(this.mSubId, paramBoolean);
        return;
      } catch (RemoteException remoteException) {
        Log.e("RcsUceAdapter", "Error calling IImsRcsController#setUceSettingEnabled", (Throwable)remoteException);
        throw new ImsException("Remote IMS Service is not available", 1);
      }  
    Log.e("RcsUceAdapter", "setUceSettingEnabled: IImsRcsController is null");
    throw new ImsException("Can not find remote IMS service", 1);
  }
  
  private IImsRcsController getIImsRcsController() {
    TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
    TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getTelephonyImsServiceRegisterer();
    IBinder iBinder = serviceRegisterer.get();
    return IImsRcsController.Stub.asInterface(iBinder);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ErrorCode {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface PublishState {}
}
