package android.telephony.ims;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class RcsContactUceCapability implements Parcelable {
  @Retention(RetentionPolicy.SOURCE)
  class CapabilityFlag implements Annotation {}
  
  class Builder {
    private final RcsContactUceCapability mCapabilities;
    
    public Builder(RcsContactUceCapability this$0) {
      this.mCapabilities = new RcsContactUceCapability((Uri)this$0);
    }
    
    public Builder add(long param1Long, Uri param1Uri) {
      RcsContactUceCapability.access$078(this.mCapabilities, param1Long);
      long l2;
      for (long l1 = 0L; param1Long < 32L; param1Long++, l2 = l1) {
        long l = (1 << (int)param1Long) & l2;
        l1 = l2;
        if (l != 0L) {
          this.mCapabilities.mServiceMap.put(Long.valueOf(l), param1Uri);
          l1 = l2 & (l ^ 0xFFFFFFFFFFFFFFFFL);
        } 
        if (l1 == 0L)
          break; 
      } 
      return this;
    }
    
    public Builder add(long param1Long) {
      RcsContactUceCapability.access$078(this.mCapabilities, param1Long);
      return this;
    }
    
    public Builder add(String param1String) {
      this.mCapabilities.mExtensionTags.add(param1String);
      return this;
    }
    
    public RcsContactUceCapability build() {
      return this.mCapabilities;
    }
  }
  
  private List<String> mExtensionTags = new ArrayList<>();
  
  private Map<Long, Uri> mServiceMap = new HashMap<>();
  
  RcsContactUceCapability(Uri paramUri) {
    this.mContactUri = paramUri;
  }
  
  private RcsContactUceCapability(Parcel paramParcel) {
    this.mContactUri = (Uri)paramParcel.readParcelable(Uri.class.getClassLoader());
    this.mCapabilities = paramParcel.readLong();
    paramParcel.readStringList(this.mExtensionTags);
    int i = paramParcel.readInt();
    for (byte b = 0; b < i; b++)
      this.mServiceMap.put(Long.valueOf(paramParcel.readLong()), (Uri)paramParcel.readParcelable(Uri.class.getClassLoader())); 
  }
  
  public static final Parcelable.Creator<RcsContactUceCapability> CREATOR = new Parcelable.Creator<RcsContactUceCapability>() {
      public RcsContactUceCapability createFromParcel(Parcel param1Parcel) {
        return new RcsContactUceCapability(param1Parcel);
      }
      
      public RcsContactUceCapability[] newArray(int param1Int) {
        return new RcsContactUceCapability[param1Int];
      }
    };
  
  public static final int CAPABILITY_CALL_COMPOSER = 4194304;
  
  public static final int CAPABILITY_CHAT_BOT = 67108864;
  
  public static final int CAPABILITY_CHAT_BOT_ROLE = 134217728;
  
  public static final int CAPABILITY_CHAT_SESSION = 2;
  
  public static final int CAPABILITY_CHAT_SESSION_STORE_FORWARD = 4;
  
  public static final int CAPABILITY_CHAT_STANDALONE = 1;
  
  public static final int CAPABILITY_DISCOVERY_VIA_PRESENCE = 4096;
  
  public static final int CAPABILITY_FILE_TRANSFER = 8;
  
  public static final int CAPABILITY_FILE_TRANSFER_HTTP = 64;
  
  public static final int CAPABILITY_FILE_TRANSFER_SMS = 128;
  
  public static final int CAPABILITY_FILE_TRANSFER_STORE_FORWARD = 32;
  
  public static final int CAPABILITY_FILE_TRANSFER_THUMBNAIL = 16;
  
  public static final int CAPABILITY_GEOLOCATION_PULL = 131072;
  
  public static final int CAPABILITY_GEOLOCATION_PULL_FILE_TRANSFER = 262144;
  
  public static final int CAPABILITY_GEOLOCATION_PUSH = 32768;
  
  public static final int CAPABILITY_GEOLOCATION_PUSH_SMS = 65536;
  
  public static final int CAPABILITY_IMAGE_SHARE = 256;
  
  public static final int CAPABILITY_IP_VIDEO_CALL = 16384;
  
  public static final int CAPABILITY_IP_VOICE_CALL = 8192;
  
  public static final int CAPABILITY_MMTEL_CALL_COMPOSER = 1073741824;
  
  public static final int CAPABILITY_PLUG_IN = 268435456;
  
  public static final int CAPABILITY_POST_CALL = 8388608;
  
  public static final int CAPABILITY_RCS_VIDEO_CALL = 1048576;
  
  public static final int CAPABILITY_RCS_VIDEO_ONLY_CALL = 2097152;
  
  public static final int CAPABILITY_RCS_VOICE_CALL = 524288;
  
  public static final int CAPABILITY_SHARED_MAP = 16777216;
  
  public static final int CAPABILITY_SHARED_SKETCH = 33554432;
  
  public static final int CAPABILITY_SOCIAL_PRESENCE = 2048;
  
  public static final int CAPABILITY_STANDALONE_CHAT_BOT = 536870912;
  
  public static final int CAPABILITY_VIDEO_SHARE = 1024;
  
  public static final int CAPABILITY_VIDEO_SHARE_DURING_CS_CALL = 512;
  
  private long mCapabilities;
  
  private final Uri mContactUri;
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.mContactUri, 0);
    paramParcel.writeLong(this.mCapabilities);
    paramParcel.writeStringList(this.mExtensionTags);
    paramInt = this.mServiceMap.keySet().size();
    paramParcel.writeInt(paramInt);
    for (Iterator<Long> iterator = this.mServiceMap.keySet().iterator(); iterator.hasNext(); ) {
      long l = ((Long)iterator.next()).longValue();
      paramParcel.writeLong(l);
      paramParcel.writeParcelable((Parcelable)this.mServiceMap.get(Long.valueOf(l)), 0);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
  
  public boolean isCapable(long paramLong) {
    boolean bool;
    if ((this.mCapabilities & paramLong) > 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isCapable(String paramString) {
    return this.mExtensionTags.contains(paramString);
  }
  
  public List<String> getCapableExtensionTags() {
    return Collections.unmodifiableList(this.mExtensionTags);
  }
  
  public Uri getServiceUri(long paramLong) {
    Map<Long, Uri> map = this.mServiceMap;
    Uri uri2 = null, uri1 = map.getOrDefault(Long.valueOf(paramLong), null);
    if (uri1 == null) {
      if (isCapable(paramLong))
        uri2 = getContactUri(); 
      return uri2;
    } 
    return uri1;
  }
  
  public Uri getContactUri() {
    return this.mContactUri;
  }
}
