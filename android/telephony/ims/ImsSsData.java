package android.telephony.ims;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.telephony.Rlog;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SystemApi
public final class ImsSsData implements Parcelable {
  public static final Parcelable.Creator<ImsSsData> CREATOR;
  
  public static final int RESULT_SUCCESS = 0;
  
  public static final int SERVICE_CLASS_DATA = 2;
  
  public static final int SERVICE_CLASS_DATA_CIRCUIT_ASYNC = 32;
  
  public static final int SERVICE_CLASS_DATA_CIRCUIT_SYNC = 16;
  
  public static final int SERVICE_CLASS_DATA_PACKET_ACCESS = 64;
  
  public static final int SERVICE_CLASS_DATA_PAD = 128;
  
  public static final int SERVICE_CLASS_FAX = 4;
  
  public static final int SERVICE_CLASS_NONE = 0;
  
  public static final int SERVICE_CLASS_SMS = 8;
  
  public static final int SERVICE_CLASS_VOICE = 1;
  
  public static final int SS_ACTIVATION = 0;
  
  public static final int SS_ALL_BARRING = 18;
  
  public static final int SS_ALL_DATA_TELESERVICES = 3;
  
  public static final int SS_ALL_TELESERVICES_EXCEPT_SMS = 5;
  
  public static final int SS_ALL_TELESEVICES = 1;
  
  public static final int SS_ALL_TELE_AND_BEARER_SERVICES = 0;
  
  public static final int SS_BAIC = 16;
  
  public static final int SS_BAIC_ROAMING = 17;
  
  public static final int SS_BAOC = 13;
  
  public static final int SS_BAOIC = 14;
  
  public static final int SS_BAOIC_EXC_HOME = 15;
  
  public static final int SS_CFU = 0;
  
  public static final int SS_CFUT = 6;
  
  public static final int SS_CF_ALL = 4;
  
  public static final int SS_CF_ALL_CONDITIONAL = 5;
  
  public static final int SS_CF_BUSY = 1;
  
  public static final int SS_CF_NOT_REACHABLE = 3;
  
  public static final int SS_CF_NO_REPLY = 2;
  
  public static final int SS_CLIP = 7;
  
  public static final int SS_CLIR = 8;
  
  public static final int SS_CNAP = 11;
  
  public static final int SS_COLP = 9;
  
  public static final int SS_COLR = 10;
  
  public static final int SS_DEACTIVATION = 1;
  
  public static final int SS_ERASURE = 4;
  
  public static final int SS_INCOMING_BARRING = 20;
  
  public static final int SS_INCOMING_BARRING_ANONYMOUS = 22;
  
  public static final int SS_INCOMING_BARRING_DN = 21;
  
  public static final int SS_INTERROGATION = 2;
  
  public static final int SS_OUTGOING_BARRING = 19;
  
  public static final int SS_REGISTRATION = 3;
  
  public static final int SS_SMS_SERVICES = 4;
  
  public static final int SS_TELEPHONY = 2;
  
  public static final int SS_WAIT = 12;
  
  private static final String TAG = ImsSsData.class.getCanonicalName();
  
  private List<ImsCallForwardInfo> mCfInfo;
  
  private List<ImsSsInfo> mImsSsInfo;
  
  private int[] mSsInfo;
  
  public final int requestType;
  
  public final int result;
  
  public final int serviceClass;
  
  public final int serviceType;
  
  public final int teleserviceType;
  
  @Retention(RetentionPolicy.SOURCE)
  class TeleserviceType implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ServiceType implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ServiceClassFlags implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class RequestType implements Annotation {}
  
  class Builder {
    private ImsSsData mImsSsData;
    
    public Builder(ImsSsData this$0, int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      this.mImsSsData = new ImsSsData(this$0, param1Int1, param1Int2, param1Int3, param1Int4);
    }
    
    public Builder setSuppServiceInfo(List<ImsSsInfo> param1List) {
      ImsSsData.access$002(this.mImsSsData, param1List);
      return this;
    }
    
    public Builder setCallForwardingInfo(List<ImsCallForwardInfo> param1List) {
      ImsSsData.access$102(this.mImsSsData, param1List);
      return this;
    }
    
    public ImsSsData build() {
      return this.mImsSsData;
    }
  }
  
  public ImsSsData(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this.serviceType = paramInt1;
    this.requestType = paramInt2;
    this.teleserviceType = paramInt3;
    this.serviceClass = paramInt4;
    this.result = paramInt5;
  }
  
  private ImsSsData(Parcel paramParcel) {
    this.serviceType = paramParcel.readInt();
    this.requestType = paramParcel.readInt();
    this.teleserviceType = paramParcel.readInt();
    this.serviceClass = paramParcel.readInt();
    this.result = paramParcel.readInt();
    this.mSsInfo = paramParcel.createIntArray();
    this.mCfInfo = paramParcel.readParcelableList(new ArrayList(), getClass().getClassLoader());
    this.mImsSsInfo = paramParcel.readParcelableList(new ArrayList(), getClass().getClassLoader());
  }
  
  static {
    CREATOR = new Parcelable.Creator<ImsSsData>() {
        public ImsSsData createFromParcel(Parcel param1Parcel) {
          return new ImsSsData(param1Parcel);
        }
        
        public ImsSsData[] newArray(int param1Int) {
          return new ImsSsData[param1Int];
        }
      };
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(getServiceType());
    paramParcel.writeInt(getRequestType());
    paramParcel.writeInt(getTeleserviceType());
    paramParcel.writeInt(getServiceClass());
    paramParcel.writeInt(getResult());
    paramParcel.writeIntArray(this.mSsInfo);
    paramParcel.writeParcelableList(this.mCfInfo, 0);
    paramParcel.writeParcelableList(this.mImsSsInfo, 0);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public boolean isTypeCF() {
    int i = getServiceType();
    boolean bool = true;
    if (i != 0 && getServiceType() != 1 && 
      getServiceType() != 2 && getServiceType() != 3 && 
      getServiceType() != 4 && getServiceType() != 5)
      bool = false; 
    return bool;
  }
  
  public boolean isTypeCf() {
    return isTypeCF();
  }
  
  public boolean isTypeUnConditional() {
    return (getServiceType() == 0 || getServiceType() == 4);
  }
  
  public boolean isTypeCW() {
    boolean bool;
    if (getServiceType() == 12) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isTypeCw() {
    return isTypeCW();
  }
  
  public boolean isTypeClip() {
    boolean bool;
    if (getServiceType() == 7) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isTypeColr() {
    boolean bool;
    if (getServiceType() == 10) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isTypeColp() {
    boolean bool;
    if (getServiceType() == 9) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isTypeClir() {
    boolean bool;
    if (getServiceType() == 8) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isTypeIcb() {
    return (getServiceType() == 21 || 
      getServiceType() == 22);
  }
  
  public boolean isTypeBarring() {
    return (getServiceType() == 13 || getServiceType() == 14 || 
      getServiceType() == 15 || getServiceType() == 16 || 
      getServiceType() == 17 || getServiceType() == 18 || 
      getServiceType() == 19 || 
      getServiceType() == 20);
  }
  
  public boolean isTypeInterrogation() {
    boolean bool;
    if (getRequestType() == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getRequestType() {
    return this.requestType;
  }
  
  public int getServiceType() {
    return this.serviceType;
  }
  
  public int getTeleserviceType() {
    return this.teleserviceType;
  }
  
  public int getServiceClass() {
    return this.serviceClass;
  }
  
  public int getResult() {
    return this.result;
  }
  
  public void setSuppServiceInfo(int[] paramArrayOfint) {
    this.mSsInfo = paramArrayOfint;
  }
  
  public void setImsSpecificSuppServiceInfo(ImsSsInfo[] paramArrayOfImsSsInfo) {
    this.mImsSsInfo = Arrays.asList(paramArrayOfImsSsInfo);
  }
  
  public void setCallForwardingInfo(ImsCallForwardInfo[] paramArrayOfImsCallForwardInfo) {
    this.mCfInfo = Arrays.asList(paramArrayOfImsCallForwardInfo);
  }
  
  public int[] getSuppServiceInfoCompat() {
    int[] arrayOfInt = this.mSsInfo;
    if (arrayOfInt != null)
      return arrayOfInt; 
    arrayOfInt = new int[2];
    List<ImsSsInfo> list = this.mImsSsInfo;
    if (list == null || list.size() == 0) {
      Rlog.e(TAG, "getSuppServiceInfoCompat: Could not parse mImsSsInfo, returning empty int[]");
      return arrayOfInt;
    } 
    if (isTypeClir()) {
      arrayOfInt[0] = ((ImsSsInfo)this.mImsSsInfo.get(0)).getClirOutgoingState();
      arrayOfInt[1] = ((ImsSsInfo)this.mImsSsInfo.get(0)).getClirInterrogationStatus();
      return arrayOfInt;
    } 
    if (isTypeColr())
      arrayOfInt[0] = ((ImsSsInfo)this.mImsSsInfo.get(0)).getProvisionStatus(); 
    arrayOfInt[0] = ((ImsSsInfo)this.mImsSsInfo.get(0)).getStatus();
    arrayOfInt[1] = ((ImsSsInfo)this.mImsSsInfo.get(0)).getProvisionStatus();
    return arrayOfInt;
  }
  
  public List<ImsSsInfo> getSuppServiceInfo() {
    return this.mImsSsInfo;
  }
  
  public List<ImsCallForwardInfo> getCallForwardInfo() {
    return this.mCfInfo;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[ImsSsData] ServiceType: ");
    stringBuilder.append(getServiceType());
    stringBuilder.append(" RequestType: ");
    stringBuilder.append(getRequestType());
    stringBuilder.append(" TeleserviceType: ");
    stringBuilder.append(getTeleserviceType());
    stringBuilder.append(" ServiceClass: ");
    stringBuilder.append(getServiceClass());
    stringBuilder.append(" Result: ");
    stringBuilder.append(getResult());
    return stringBuilder.toString();
  }
}
