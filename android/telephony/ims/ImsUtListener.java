package android.telephony.ims;

import android.annotation.SystemApi;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import com.android.ims.internal.IImsUtListener;

@SystemApi
public class ImsUtListener {
  @Deprecated
  public static final String BUNDLE_KEY_CLIR = "queryClir";
  
  @Deprecated
  public static final String BUNDLE_KEY_SSINFO = "imsSsInfo";
  
  private static final String LOG_TAG = "ImsUtListener";
  
  private IImsUtListener mServiceInterface;
  
  public void onUtConfigurationUpdated(int paramInt) {
    try {
      this.mServiceInterface.utConfigurationUpdated(null, paramInt);
    } catch (RemoteException remoteException) {
      Log.w("ImsUtListener", "utConfigurationUpdated: remote exception");
    } 
  }
  
  public void onUtConfigurationUpdateFailed(int paramInt, ImsReasonInfo paramImsReasonInfo) {
    try {
      this.mServiceInterface.utConfigurationUpdateFailed(null, paramInt, paramImsReasonInfo);
    } catch (RemoteException remoteException) {
      Log.w("ImsUtListener", "utConfigurationUpdateFailed: remote exception");
    } 
  }
  
  @Deprecated
  public void onUtConfigurationQueried(int paramInt, Bundle paramBundle) {
    try {
      this.mServiceInterface.utConfigurationQueried(null, paramInt, paramBundle);
    } catch (RemoteException remoteException) {
      Log.w("ImsUtListener", "utConfigurationQueried: remote exception");
    } 
  }
  
  public void onLineIdentificationSupplementaryServiceResponse(int paramInt, ImsSsInfo paramImsSsInfo) {
    try {
      this.mServiceInterface.lineIdentificationSupplementaryServiceResponse(paramInt, paramImsSsInfo);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void onUtConfigurationQueryFailed(int paramInt, ImsReasonInfo paramImsReasonInfo) {
    try {
      this.mServiceInterface.utConfigurationQueryFailed(null, paramInt, paramImsReasonInfo);
    } catch (RemoteException remoteException) {
      Log.w("ImsUtListener", "utConfigurationQueryFailed: remote exception");
    } 
  }
  
  public void onUtConfigurationCallBarringQueried(int paramInt, ImsSsInfo[] paramArrayOfImsSsInfo) {
    try {
      this.mServiceInterface.utConfigurationCallBarringQueried(null, paramInt, paramArrayOfImsSsInfo);
    } catch (RemoteException remoteException) {
      Log.w("ImsUtListener", "utConfigurationCallBarringQueried: remote exception");
    } 
  }
  
  public void onUtConfigurationCallForwardQueried(int paramInt, ImsCallForwardInfo[] paramArrayOfImsCallForwardInfo) {
    try {
      this.mServiceInterface.utConfigurationCallForwardQueried(null, paramInt, paramArrayOfImsCallForwardInfo);
    } catch (RemoteException remoteException) {
      Log.w("ImsUtListener", "utConfigurationCallForwardQueried: remote exception");
    } 
  }
  
  public void onUtConfigurationCallWaitingQueried(int paramInt, ImsSsInfo[] paramArrayOfImsSsInfo) {
    try {
      this.mServiceInterface.utConfigurationCallWaitingQueried(null, paramInt, paramArrayOfImsSsInfo);
    } catch (RemoteException remoteException) {
      Log.w("ImsUtListener", "utConfigurationCallWaitingQueried: remote exception");
    } 
  }
  
  public void onSupplementaryServiceIndication(ImsSsData paramImsSsData) {
    try {
      this.mServiceInterface.onSupplementaryServiceIndication(paramImsSsData);
    } catch (RemoteException remoteException) {
      Log.w("ImsUtListener", "onSupplementaryServiceIndication: remote exception");
    } 
  }
  
  public ImsUtListener(IImsUtListener paramIImsUtListener) {
    this.mServiceInterface = paramIImsUtListener;
  }
}
