package android.telephony.ims.feature;

import android.annotation.SystemApi;
import android.os.Bundle;
import android.os.IInterface;
import android.os.Message;
import android.os.RemoteException;
import android.telephony.ims.ImsCallProfile;
import android.telephony.ims.ImsReasonInfo;
import android.telephony.ims.aidl.IImsMmTelFeature;
import android.telephony.ims.aidl.IImsMmTelListener;
import android.telephony.ims.aidl.IImsSmsListener;
import android.telephony.ims.stub.ImsCallSessionImplBase;
import android.telephony.ims.stub.ImsEcbmImplBase;
import android.telephony.ims.stub.ImsMultiEndpointImplBase;
import android.telephony.ims.stub.ImsSmsImplBase;
import android.telephony.ims.stub.ImsUtImplBase;
import com.android.ims.internal.IImsCallSession;
import com.android.ims.internal.IImsEcbm;
import com.android.ims.internal.IImsMultiEndpoint;
import com.android.ims.internal.IImsUt;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class MmTelFeature extends ImsFeature {
  @SystemApi
  public static final String EXTRA_IS_UNKNOWN_CALL = "android.telephony.ims.feature.extra.IS_UNKNOWN_CALL";
  
  @SystemApi
  public static final String EXTRA_IS_USSD = "android.telephony.ims.feature.extra.IS_USSD";
  
  private static final String LOG_TAG = "MmTelFeature";
  
  @SystemApi
  public static final int PROCESS_CALL_CSFB = 1;
  
  @SystemApi
  public static final int PROCESS_CALL_IMS = 0;
  
  private final IImsMmTelFeature mImsMMTelBinder = (IImsMmTelFeature)new Object(this);
  
  private IImsMmTelListener mListener;
  
  @Retention(RetentionPolicy.SOURCE)
  class ProcessCallResult implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class MmTelCapability implements Annotation {}
  
  public static class MmTelCapabilities extends ImsFeature.Capabilities {
    public static final int CAPABILITY_TYPE_SMS = 8;
    
    public static final int CAPABILITY_TYPE_UT = 4;
    
    public static final int CAPABILITY_TYPE_VIDEO = 2;
    
    public static final int CAPABILITY_TYPE_VOICE = 1;
    
    @SystemApi
    public MmTelCapabilities() {}
    
    @SystemApi
    @Deprecated
    public MmTelCapabilities(ImsFeature.Capabilities param1Capabilities) {
      this.mCapabilities = param1Capabilities.mCapabilities;
    }
    
    @SystemApi
    public MmTelCapabilities(int param1Int) {
      super(param1Int);
    }
    
    @SystemApi
    public final void addCapabilities(int param1Int) {
      super.addCapabilities(param1Int);
    }
    
    @SystemApi
    public final void removeCapabilities(int param1Int) {
      super.removeCapabilities(param1Int);
    }
    
    @SystemApi
    public final boolean isCapable(int param1Int) {
      return super.isCapable(param1Int);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder("MmTel Capabilities - [");
      stringBuilder.append("Voice: ");
      stringBuilder.append(isCapable(1));
      stringBuilder.append(" Video: ");
      stringBuilder.append(isCapable(2));
      stringBuilder.append(" UT: ");
      stringBuilder.append(isCapable(4));
      stringBuilder.append(" SMS: ");
      stringBuilder.append(isCapable(8));
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
    
    @Retention(RetentionPolicy.SOURCE)
    class MmTelCapability implements Annotation {}
  }
  
  class Listener extends IImsMmTelListener.Stub {
    @SystemApi
    public void onIncomingCall(IImsCallSession param1IImsCallSession, Bundle param1Bundle) {}
    
    @SystemApi
    public void onRejectedCall(ImsCallProfile param1ImsCallProfile, ImsReasonInfo param1ImsReasonInfo) {}
    
    @SystemApi
    public void onVoiceMessageCountUpdate(int param1Int) {}
  }
  
  private void setListener(IImsMmTelListener paramIImsMmTelListener) {
    synchronized (this.mLock) {
      this.mListener = paramIImsMmTelListener;
      if (paramIImsMmTelListener != null)
        onFeatureReady(); 
      return;
    } 
  }
  
  private IImsMmTelListener getListener() {
    synchronized (this.mLock) {
      return this.mListener;
    } 
  }
  
  @SystemApi
  public final MmTelCapabilities queryCapabilityStatus() {
    return new MmTelCapabilities(super.queryCapabilityStatus());
  }
  
  @SystemApi
  public final void notifyCapabilitiesStatusChanged(MmTelCapabilities paramMmTelCapabilities) {
    if (paramMmTelCapabilities != null) {
      notifyCapabilitiesStatusChanged(paramMmTelCapabilities);
      return;
    } 
    throw new IllegalArgumentException("MmTelCapabilities must be non-null!");
  }
  
  @SystemApi
  public final void notifyIncomingCall(ImsCallSessionImplBase paramImsCallSessionImplBase, Bundle paramBundle) {
    if (paramImsCallSessionImplBase != null && paramBundle != null) {
      IImsMmTelListener iImsMmTelListener = getListener();
      if (iImsMmTelListener != null)
        try {
          iImsMmTelListener.onIncomingCall(paramImsCallSessionImplBase.getServiceImpl(), paramBundle);
          return;
        } catch (RemoteException remoteException) {
          throw new RuntimeException(remoteException);
        }  
      throw new IllegalStateException("Session is not available.");
    } 
    throw new IllegalArgumentException("ImsCallSessionImplBase and Bundle can not be null.");
  }
  
  @SystemApi
  public final void notifyRejectedCall(ImsCallProfile paramImsCallProfile, ImsReasonInfo paramImsReasonInfo) {
    if (paramImsCallProfile != null && paramImsReasonInfo != null) {
      IImsMmTelListener iImsMmTelListener = getListener();
      if (iImsMmTelListener != null)
        try {
          iImsMmTelListener.onRejectedCall(paramImsCallProfile, paramImsReasonInfo);
          return;
        } catch (RemoteException remoteException) {
          throw new RuntimeException(remoteException);
        }  
      throw new IllegalStateException("Session is not available.");
    } 
    throw new IllegalArgumentException("ImsCallProfile and ImsReasonInfo must not be null.");
  }
  
  public final void notifyIncomingCallSession(IImsCallSession paramIImsCallSession, Bundle paramBundle) {
    IImsMmTelListener iImsMmTelListener = getListener();
    if (iImsMmTelListener != null)
      try {
        iImsMmTelListener.onIncomingCall(paramIImsCallSession, paramBundle);
        return;
      } catch (RemoteException remoteException) {
        throw new RuntimeException(remoteException);
      }  
    throw new IllegalStateException("Session is not available.");
  }
  
  @SystemApi
  public final void notifyVoiceMessageCountUpdate(int paramInt) {
    IImsMmTelListener iImsMmTelListener = getListener();
    if (iImsMmTelListener != null)
      try {
        iImsMmTelListener.onVoiceMessageCountUpdate(paramInt);
        return;
      } catch (RemoteException remoteException) {
        throw new RuntimeException(remoteException);
      }  
    throw new IllegalStateException("Session is not available.");
  }
  
  @SystemApi
  public boolean queryCapabilityConfiguration(int paramInt1, int paramInt2) {
    return false;
  }
  
  @SystemApi
  public void changeEnabledCapabilities(CapabilityChangeRequest paramCapabilityChangeRequest, ImsFeature.CapabilityCallbackProxy paramCapabilityCallbackProxy) {}
  
  @SystemApi
  public ImsCallProfile createCallProfile(int paramInt1, int paramInt2) {
    return null;
  }
  
  public IImsCallSession createCallSessionInterface(ImsCallProfile paramImsCallProfile) throws RemoteException {
    ImsCallSessionImplBase imsCallSessionImplBase = createCallSession(paramImsCallProfile);
    if (imsCallSessionImplBase != null) {
      IImsCallSession iImsCallSession = imsCallSessionImplBase.getServiceImpl();
    } else {
      imsCallSessionImplBase = null;
    } 
    return (IImsCallSession)imsCallSessionImplBase;
  }
  
  @SystemApi
  public ImsCallSessionImplBase createCallSession(ImsCallProfile paramImsCallProfile) {
    return null;
  }
  
  @SystemApi
  public int shouldProcessCall(String[] paramArrayOfString) {
    return 0;
  }
  
  protected IImsUt getUtInterface() throws RemoteException {
    ImsUtImplBase imsUtImplBase = getUt();
    if (imsUtImplBase != null) {
      IImsUt iImsUt = imsUtImplBase.getInterface();
    } else {
      imsUtImplBase = null;
    } 
    return (IImsUt)imsUtImplBase;
  }
  
  protected IImsEcbm getEcbmInterface() throws RemoteException {
    ImsEcbmImplBase imsEcbmImplBase = getEcbm();
    if (imsEcbmImplBase != null) {
      IImsEcbm iImsEcbm = imsEcbmImplBase.getImsEcbm();
    } else {
      imsEcbmImplBase = null;
    } 
    return (IImsEcbm)imsEcbmImplBase;
  }
  
  public IImsMultiEndpoint getMultiEndpointInterface() throws RemoteException {
    ImsMultiEndpointImplBase imsMultiEndpointImplBase = getMultiEndpoint();
    if (imsMultiEndpointImplBase != null) {
      IImsMultiEndpoint iImsMultiEndpoint = imsMultiEndpointImplBase.getIImsMultiEndpoint();
    } else {
      imsMultiEndpointImplBase = null;
    } 
    return (IImsMultiEndpoint)imsMultiEndpointImplBase;
  }
  
  @SystemApi
  public ImsUtImplBase getUt() {
    return new ImsUtImplBase();
  }
  
  @SystemApi
  public ImsEcbmImplBase getEcbm() {
    return new ImsEcbmImplBase();
  }
  
  @SystemApi
  public ImsMultiEndpointImplBase getMultiEndpoint() {
    return new ImsMultiEndpointImplBase();
  }
  
  @SystemApi
  public void setUiTtyMode(int paramInt, Message paramMessage) {}
  
  private void setSmsListener(IImsSmsListener paramIImsSmsListener) {
    getSmsImplementation().registerSmsListener(paramIImsSmsListener);
  }
  
  private void sendSms(int paramInt1, int paramInt2, String paramString1, String paramString2, boolean paramBoolean, byte[] paramArrayOfbyte) {
    getSmsImplementation().sendSms(paramInt1, paramInt2, paramString1, paramString2, paramBoolean, paramArrayOfbyte);
  }
  
  private void acknowledgeSms(int paramInt1, int paramInt2, int paramInt3) {
    getSmsImplementation().acknowledgeSms(paramInt1, paramInt2, paramInt3);
  }
  
  private void acknowledgeSmsReport(int paramInt1, int paramInt2, int paramInt3) {
    getSmsImplementation().acknowledgeSmsReport(paramInt1, paramInt2, paramInt3);
  }
  
  private void onSmsReady() {
    getSmsImplementation().onReady();
  }
  
  @SystemApi
  public ImsSmsImplBase getSmsImplementation() {
    return new ImsSmsImplBase();
  }
  
  private String getSmsFormat() {
    return getSmsImplementation().getSmsFormat();
  }
  
  @SystemApi
  public void onFeatureRemoved() {}
  
  @SystemApi
  public void onFeatureReady() {}
  
  public final IImsMmTelFeature getBinder() {
    return this.mImsMMTelBinder;
  }
}
