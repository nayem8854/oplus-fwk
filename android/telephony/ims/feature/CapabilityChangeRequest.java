package android.telephony.ims.feature;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.ArraySet;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@SystemApi
public final class CapabilityChangeRequest implements Parcelable {
  class CapabilityPair {
    private final int mCapability;
    
    private final int radioTech;
    
    public CapabilityPair(CapabilityChangeRequest this$0, int param1Int1) {
      this.mCapability = this$0;
      this.radioTech = param1Int1;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (!(param1Object instanceof CapabilityPair))
        return false; 
      param1Object = param1Object;
      if (getCapability() != param1Object.getCapability())
        return false; 
      if (getRadioTech() != param1Object.getRadioTech())
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      int i = getCapability();
      int j = getRadioTech();
      return i * 31 + j;
    }
    
    public int getCapability() {
      return this.mCapability;
    }
    
    public int getRadioTech() {
      return this.radioTech;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("CapabilityPair{mCapability=");
      stringBuilder.append(this.mCapability);
      stringBuilder.append(", radioTech=");
      stringBuilder.append(this.radioTech);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
  }
  
  public CapabilityChangeRequest() {
    this.mCapabilitiesToEnable = new ArraySet<>();
    this.mCapabilitiesToDisable = new ArraySet<>();
  }
  
  public void addCapabilitiesToEnableForTech(int paramInt1, int paramInt2) {
    addAllCapabilities(this.mCapabilitiesToEnable, paramInt1, paramInt2);
  }
  
  public void addCapabilitiesToDisableForTech(int paramInt1, int paramInt2) {
    addAllCapabilities(this.mCapabilitiesToDisable, paramInt1, paramInt2);
  }
  
  public List<CapabilityPair> getCapabilitiesToEnable() {
    return new ArrayList<>(this.mCapabilitiesToEnable);
  }
  
  public List<CapabilityPair> getCapabilitiesToDisable() {
    return new ArrayList<>(this.mCapabilitiesToDisable);
  }
  
  private void addAllCapabilities(Set<CapabilityPair> paramSet, int paramInt1, int paramInt2) {
    long l = Long.highestOneBit(paramInt1);
    int i;
    for (i = 1; i <= l; i *= 2) {
      if ((i & paramInt1) > 0)
        paramSet.add(new CapabilityPair(i, paramInt2)); 
    } 
  }
  
  protected CapabilityChangeRequest(Parcel paramParcel) {
    int i = paramParcel.readInt();
    this.mCapabilitiesToEnable = new ArraySet<>(i);
    byte b;
    for (b = 0; b < i; b++) {
      Set<CapabilityPair> set = this.mCapabilitiesToEnable;
      int j = paramParcel.readInt();
      CapabilityPair capabilityPair = new CapabilityPair(j, paramParcel.readInt());
      set.add(capabilityPair);
    } 
    i = paramParcel.readInt();
    this.mCapabilitiesToDisable = new ArraySet<>(i);
    for (b = 0; b < i; b++) {
      Set<CapabilityPair> set = this.mCapabilitiesToDisable;
      int j = paramParcel.readInt();
      CapabilityPair capabilityPair = new CapabilityPair(j, paramParcel.readInt());
      set.add(capabilityPair);
    } 
  }
  
  public static final Parcelable.Creator<CapabilityChangeRequest> CREATOR = new Parcelable.Creator<CapabilityChangeRequest>() {
      public CapabilityChangeRequest createFromParcel(Parcel param1Parcel) {
        return new CapabilityChangeRequest(param1Parcel);
      }
      
      public CapabilityChangeRequest[] newArray(int param1Int) {
        return new CapabilityChangeRequest[param1Int];
      }
    };
  
  private final Set<CapabilityPair> mCapabilitiesToDisable;
  
  private final Set<CapabilityPair> mCapabilitiesToEnable;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mCapabilitiesToEnable.size());
    for (CapabilityPair capabilityPair : this.mCapabilitiesToEnable) {
      paramParcel.writeInt(capabilityPair.getCapability());
      paramParcel.writeInt(capabilityPair.getRadioTech());
    } 
    paramParcel.writeInt(this.mCapabilitiesToDisable.size());
    for (CapabilityPair capabilityPair : this.mCapabilitiesToDisable) {
      paramParcel.writeInt(capabilityPair.getCapability());
      paramParcel.writeInt(capabilityPair.getRadioTech());
    } 
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CapabilityChangeRequest{mCapabilitiesToEnable=");
    stringBuilder.append(this.mCapabilitiesToEnable);
    stringBuilder.append(", mCapabilitiesToDisable=");
    stringBuilder.append(this.mCapabilitiesToDisable);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof CapabilityChangeRequest))
      return false; 
    paramObject = paramObject;
    if (!this.mCapabilitiesToEnable.equals(((CapabilityChangeRequest)paramObject).mCapabilitiesToEnable))
      return false; 
    return this.mCapabilitiesToDisable.equals(((CapabilityChangeRequest)paramObject).mCapabilitiesToDisable);
  }
  
  public int hashCode() {
    int i = this.mCapabilitiesToEnable.hashCode();
    int j = this.mCapabilitiesToDisable.hashCode();
    return i * 31 + j;
  }
}
