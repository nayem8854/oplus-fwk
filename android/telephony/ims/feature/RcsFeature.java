package android.telephony.ims.feature;

import android.annotation.SystemApi;
import android.net.Uri;
import android.os.IInterface;
import android.os.RemoteException;
import android.telephony.ims.RcsContactUceCapability;
import android.telephony.ims.aidl.IImsCapabilityCallback;
import android.telephony.ims.aidl.IImsRcsFeature;
import android.telephony.ims.aidl.IRcsFeatureListener;
import android.telephony.ims.stub.RcsPresenceExchangeImplBase;
import android.telephony.ims.stub.RcsSipOptionsImplBase;
import android.util.Log;
import com.android.internal.telephony.util.TelephonyUtils;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.function.Supplier;

@SystemApi
public class RcsFeature extends ImsFeature {
  private static final String LOG_TAG = "RcsFeature";
  
  private final RcsFeatureBinder mImsRcsBinder;
  
  private IRcsFeatureListener mListenerBinder;
  
  private RcsPresenceExchangeImplBase mPresExchange;
  
  private RcsSipOptionsImplBase mSipOptions;
  
  class RcsFeatureBinder extends IImsRcsFeature.Stub {
    private final Executor mExecutor;
    
    private final RcsFeature mReference;
    
    RcsFeatureBinder(RcsFeature this$0, Executor param1Executor) {
      this.mReference = this$0;
      this.mExecutor = param1Executor;
    }
    
    public void setListener(IRcsFeatureListener param1IRcsFeatureListener) {
      this.mReference.setListener(param1IRcsFeatureListener);
    }
    
    public int queryCapabilityStatus() throws RemoteException {
      return ((Integer)executeMethodAsyncForResult(new _$$Lambda$RcsFeature$RcsFeatureBinder$M_Xsfh7yLPmebDSvMzAvEPPUmE0(this), "queryCapabilityStatus")).intValue();
    }
    
    public void addCapabilityCallback(IImsCapabilityCallback param1IImsCapabilityCallback) throws RemoteException {
      executeMethodAsync(new _$$Lambda$RcsFeature$RcsFeatureBinder$FP3fQsjEKVwpq4qlvcr28BKvPZ8(this, param1IImsCapabilityCallback), "addCapabilityCallback");
    }
    
    public void removeCapabilityCallback(IImsCapabilityCallback param1IImsCapabilityCallback) throws RemoteException {
      executeMethodAsync(new _$$Lambda$RcsFeature$RcsFeatureBinder$xfOc6cnT5pzpCqMa06Osbyq4DSg(this, param1IImsCapabilityCallback), "removeCapabilityCallback");
    }
    
    public void changeCapabilitiesConfiguration(CapabilityChangeRequest param1CapabilityChangeRequest, IImsCapabilityCallback param1IImsCapabilityCallback) throws RemoteException {
      executeMethodAsync(new _$$Lambda$RcsFeature$RcsFeatureBinder$KzhM6fJ3lOKAUZU1vw70hi3DCU4(this, param1CapabilityChangeRequest, param1IImsCapabilityCallback), "changeCapabilitiesConfiguration");
    }
    
    public void queryCapabilityConfiguration(int param1Int1, int param1Int2, IImsCapabilityCallback param1IImsCapabilityCallback) throws RemoteException {
      executeMethodAsync(new _$$Lambda$RcsFeature$RcsFeatureBinder$MVZfrdsQjdvMg2JJ3V1ZgyNoaXs(this, param1Int1, param1Int2, param1IImsCapabilityCallback), "queryCapabilityConfiguration");
    }
    
    public int getFeatureState() throws RemoteException {
      RcsFeature rcsFeature = this.mReference;
      Objects.requireNonNull(rcsFeature);
      return ((Integer)executeMethodAsyncForResult(new _$$Lambda$45UAFJHVNK8x4IbCIjJCvNdDYHU(rcsFeature), "getFeatureState")).intValue();
    }
    
    public void requestCapabilities(List<Uri> param1List, int param1Int) throws RemoteException {
      executeMethodAsync(new _$$Lambda$RcsFeature$RcsFeatureBinder$dDflkKfpW9nVgt1b4_Uw__AF8m8(this, param1List, param1Int), "requestCapabilities");
    }
    
    public void updateCapabilities(RcsContactUceCapability param1RcsContactUceCapability, int param1Int) throws RemoteException {
      executeMethodAsync(new _$$Lambda$RcsFeature$RcsFeatureBinder$mMh1klMwhxUYl0X2ql2T5uAS8oY(this, param1RcsContactUceCapability, param1Int), "updateCapabilities");
    }
    
    public void sendCapabilityRequest(Uri param1Uri, RcsContactUceCapability param1RcsContactUceCapability, int param1Int) throws RemoteException {
      executeMethodAsync(new _$$Lambda$RcsFeature$RcsFeatureBinder$KBfbIAfuyQhgeV5OXetqYAOMpDE(this, param1Uri, param1RcsContactUceCapability, param1Int), "sendCapabilityRequest");
    }
    
    public void respondToCapabilityRequest(String param1String, RcsContactUceCapability param1RcsContactUceCapability, int param1Int) throws RemoteException {
      executeMethodAsync(new _$$Lambda$RcsFeature$RcsFeatureBinder$uINaboXBbmj01kSBzEjvFkkEyrQ(this, param1String, param1RcsContactUceCapability, param1Int), "respondToCapabilityRequest");
    }
    
    public void respondToCapabilityRequestWithError(Uri param1Uri, int param1Int1, String param1String, int param1Int2) throws RemoteException {
      executeMethodAsync(new _$$Lambda$RcsFeature$RcsFeatureBinder$KfGHlTIPDjp_wBV1DAurP8YpIZw(this, param1Uri, param1Int1, param1String, param1Int2), "respondToCapabilityRequestWithError");
    }
    
    private void executeMethodAsync(Runnable param1Runnable, String param1String) throws RemoteException {
      try {
        _$$Lambda$RcsFeature$RcsFeatureBinder$BKNRXehVxeScxlIdApDhMRQovpo _$$Lambda$RcsFeature$RcsFeatureBinder$BKNRXehVxeScxlIdApDhMRQovpo = new _$$Lambda$RcsFeature$RcsFeatureBinder$BKNRXehVxeScxlIdApDhMRQovpo();
        this(param1Runnable);
        CompletableFuture<Void> completableFuture = CompletableFuture.runAsync(_$$Lambda$RcsFeature$RcsFeatureBinder$BKNRXehVxeScxlIdApDhMRQovpo, this.mExecutor);
        completableFuture.join();
        return;
      } catch (CancellationException|java.util.concurrent.CompletionException cancellationException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("RcsFeatureBinder - ");
        stringBuilder.append(param1String);
        stringBuilder.append(" exception: ");
        stringBuilder.append(cancellationException.getMessage());
        param1String = stringBuilder.toString();
        Log.w("RcsFeature", param1String);
        throw new RemoteException(cancellationException.getMessage());
      } 
    }
    
    private <T> T executeMethodAsyncForResult(Supplier<T> param1Supplier, String param1String) throws RemoteException {
      CompletableFuture<?> completableFuture = CompletableFuture.supplyAsync(new _$$Lambda$RcsFeature$RcsFeatureBinder$Ca_Lrg0AjuEm0MywLfQlW_OLbac(param1Supplier), this.mExecutor);
      try {
        return (T)completableFuture.get();
      } catch (ExecutionException|InterruptedException executionException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("RcsFeatureBinder - ");
        stringBuilder.append(param1String);
        stringBuilder.append(" exception: ");
        stringBuilder.append(executionException.getMessage());
        param1String = stringBuilder.toString();
        Log.w("RcsFeature", param1String);
        throw new RemoteException(executionException.getMessage());
      } 
    }
  }
  
  public static class RcsImsCapabilities extends ImsFeature.Capabilities {
    public static final int CAPABILITY_TYPE_NONE = 0;
    
    public static final int CAPABILITY_TYPE_OPTIONS_UCE = 1;
    
    public static final int CAPABILITY_TYPE_PRESENCE_UCE = 2;
    
    public RcsImsCapabilities(int param1Int) {
      super(param1Int);
    }
    
    private RcsImsCapabilities(ImsFeature.Capabilities param1Capabilities) {
      super(param1Capabilities.getMask());
    }
    
    public void addCapabilities(int param1Int) {
      super.addCapabilities(param1Int);
    }
    
    public void removeCapabilities(int param1Int) {
      super.removeCapabilities(param1Int);
    }
    
    public boolean isCapable(int param1Int) {
      return super.isCapable(param1Int);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    class RcsImsCapabilityFlag implements Annotation {}
  }
  
  public RcsFeature() {
    this.mImsRcsBinder = new RcsFeatureBinder(this, (Executor)_$$Lambda$_14QHG018Z6p13d3hzJuGTWnNeo.INSTANCE);
  }
  
  public RcsFeature(Executor paramExecutor) {
    if (paramExecutor != null) {
      this.mImsRcsBinder = new RcsFeatureBinder(this, paramExecutor);
      return;
    } 
    throw new IllegalArgumentException("executor can not be null.");
  }
  
  public final RcsImsCapabilities queryCapabilityStatus() {
    return new RcsImsCapabilities(super.queryCapabilityStatus());
  }
  
  public final void notifyCapabilitiesStatusChanged(RcsImsCapabilities paramRcsImsCapabilities) {
    if (paramRcsImsCapabilities != null) {
      notifyCapabilitiesStatusChanged(paramRcsImsCapabilities);
      return;
    } 
    throw new IllegalArgumentException("RcsImsCapabilities must be non-null!");
  }
  
  public boolean queryCapabilityConfiguration(int paramInt1, int paramInt2) {
    return false;
  }
  
  public void changeEnabledCapabilities(CapabilityChangeRequest paramCapabilityChangeRequest, ImsFeature.CapabilityCallbackProxy paramCapabilityCallbackProxy) {}
  
  public RcsSipOptionsImplBase getOptionsExchangeImpl() {
    return new RcsSipOptionsImplBase();
  }
  
  public RcsPresenceExchangeImplBase getPresenceExchangeImpl() {
    return new RcsPresenceExchangeImplBase();
  }
  
  public void onFeatureRemoved() {}
  
  public void onFeatureReady() {}
  
  public final IImsRcsFeature getBinder() {
    return this.mImsRcsBinder;
  }
  
  public IRcsFeatureListener getListener() {
    synchronized (this.mLock) {
      return this.mListenerBinder;
    } 
  }
  
  private void setListener(IRcsFeatureListener paramIRcsFeatureListener) {
    synchronized (this.mLock) {
      this.mListenerBinder = paramIRcsFeatureListener;
      if (paramIRcsFeatureListener != null)
        onFeatureReady(); 
      return;
    } 
  }
  
  private RcsPresenceExchangeImplBase getPresenceExchangeInternal() {
    synchronized (this.mLock) {
      if (this.mPresExchange == null) {
        RcsPresenceExchangeImplBase rcsPresenceExchangeImplBase = getPresenceExchangeImpl();
        rcsPresenceExchangeImplBase.initialize(this);
      } 
      return this.mPresExchange;
    } 
  }
  
  private RcsSipOptionsImplBase getOptionsExchangeInternal() {
    synchronized (this.mLock) {
      if (this.mSipOptions == null) {
        RcsSipOptionsImplBase rcsSipOptionsImplBase = getOptionsExchangeImpl();
        rcsSipOptionsImplBase.initialize(this);
      } 
      return this.mSipOptions;
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class RcsImsCapabilityFlag implements Annotation {}
}
