package android.telephony.ims.feature;

import android.annotation.SystemApi;
import android.content.Context;
import android.os.IInterface;
import android.os.RemoteException;
import android.telephony.ims.aidl.IImsCapabilityCallback;
import android.util.Log;
import com.android.ims.internal.IImsFeatureStatusCallback;
import com.android.internal.telephony.util.RemoteCallbackListExt;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.Map;

@SystemApi
public abstract class ImsFeature {
  public static final Map<Integer, String> FEATURE_LOG_MAP = new HashMap<Integer, String>() {
    
    };
  
  public static final Map<Integer, String> STATE_LOG_MAP = new HashMap<Integer, String>() {
    
    };
  
  protected static class CapabilityCallbackProxy {
    private final IImsCapabilityCallback mCallback;
    
    public CapabilityCallbackProxy(IImsCapabilityCallback param1IImsCapabilityCallback) {
      this.mCallback = param1IImsCapabilityCallback;
    }
    
    public void onChangeCapabilityConfigurationError(int param1Int1, int param1Int2, int param1Int3) {
      IImsCapabilityCallback iImsCapabilityCallback = this.mCallback;
      if (iImsCapabilityCallback == null)
        return; 
      try {
        iImsCapabilityCallback.onChangeCapabilityConfigurationError(param1Int1, param1Int2, param1Int3);
      } catch (RemoteException remoteException) {
        Log.e("ImsFeature", "onChangeCapabilityConfigurationError called on dead binder.");
      } 
    }
  }
  
  @SystemApi
  @Deprecated
  public static class Capabilities {
    protected int mCapabilities = 0;
    
    protected Capabilities(int param1Int) {
      this.mCapabilities = param1Int;
    }
    
    public void addCapabilities(int param1Int) {
      this.mCapabilities |= param1Int;
    }
    
    public void removeCapabilities(int param1Int) {
      this.mCapabilities &= param1Int ^ 0xFFFFFFFF;
    }
    
    public boolean isCapable(int param1Int) {
      boolean bool;
      if ((this.mCapabilities & param1Int) == param1Int) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public Capabilities copy() {
      return new Capabilities(this.mCapabilities);
    }
    
    public int getMask() {
      return this.mCapabilities;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (!(param1Object instanceof Capabilities))
        return false; 
      param1Object = param1Object;
      if (this.mCapabilities != ((Capabilities)param1Object).mCapabilities)
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      return this.mCapabilities;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Capabilities: ");
      stringBuilder.append(Integer.toBinaryString(this.mCapabilities));
      return stringBuilder.toString();
    }
    
    public Capabilities() {}
  }
  
  protected final Object mLock = new Object();
  
  private final RemoteCallbackListExt<IImsFeatureStatusCallback> mStatusCallbacks = new RemoteCallbackListExt();
  
  private int mState = 0;
  
  private int mSlotId = -1;
  
  private final RemoteCallbackListExt<IImsCapabilityCallback> mCapabilityCallbacks = new RemoteCallbackListExt();
  
  private Capabilities mCapabilityStatus = new Capabilities();
  
  @SystemApi
  public static final int CAPABILITY_ERROR_GENERIC = -1;
  
  @SystemApi
  public static final int CAPABILITY_SUCCESS = 0;
  
  @SystemApi
  public static final int FEATURE_EMERGENCY_MMTEL = 0;
  
  public static final int FEATURE_INVALID = -1;
  
  public static final int FEATURE_MAX = 3;
  
  @SystemApi
  public static final int FEATURE_MMTEL = 1;
  
  @SystemApi
  public static final int FEATURE_RCS = 2;
  
  private static final String LOG_TAG = "ImsFeature";
  
  @SystemApi
  public static final int STATE_INITIALIZING = 1;
  
  @SystemApi
  public static final int STATE_READY = 2;
  
  @SystemApi
  public static final int STATE_UNAVAILABLE = 0;
  
  protected Context mContext;
  
  public final void initialize(Context paramContext, int paramInt) {
    this.mContext = paramContext;
    this.mSlotId = paramInt;
  }
  
  @SystemApi
  public final int getSlotIndex() {
    return this.mSlotId;
  }
  
  @SystemApi
  public int getFeatureState() {
    synchronized (this.mLock) {
      return this.mState;
    } 
  }
  
  @SystemApi
  public final void setFeatureState(int paramInt) {
    synchronized (this.mLock) {
      if (this.mState != paramInt) {
        this.mState = paramInt;
        notifyFeatureState(paramInt);
      } 
      return;
    } 
  }
  
  public void addImsFeatureStatusCallback(IImsFeatureStatusCallback paramIImsFeatureStatusCallback) {
    try {
      paramIImsFeatureStatusCallback.notifyImsFeatureStatus(getFeatureState());
      this.mStatusCallbacks.register(paramIImsFeatureStatusCallback);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Couldn't notify feature state: ");
      stringBuilder.append(remoteException.getMessage());
      Log.w("ImsFeature", stringBuilder.toString());
    } 
  }
  
  public void removeImsFeatureStatusCallback(IImsFeatureStatusCallback paramIImsFeatureStatusCallback) {
    this.mStatusCallbacks.unregister(paramIImsFeatureStatusCallback);
  }
  
  private void notifyFeatureState(int paramInt) {
    this.mStatusCallbacks.broadcastAction(new _$$Lambda$ImsFeature$rPSMsRhoup9jfT6nt1MV2qhomrM(paramInt));
  }
  
  public final void addCapabilityCallback(IImsCapabilityCallback paramIImsCapabilityCallback) {
    this.mCapabilityCallbacks.register(paramIImsCapabilityCallback);
    try {
      paramIImsCapabilityCallback.onCapabilitiesStatusChanged((queryCapabilityStatus()).mCapabilities);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("addCapabilityCallback: error accessing callback: ");
      stringBuilder.append(remoteException.getMessage());
      Log.w("ImsFeature", stringBuilder.toString());
    } 
  }
  
  final void removeCapabilityCallback(IImsCapabilityCallback paramIImsCapabilityCallback) {
    this.mCapabilityCallbacks.unregister(paramIImsCapabilityCallback);
  }
  
  final void queryCapabilityConfigurationInternal(int paramInt1, int paramInt2, IImsCapabilityCallback paramIImsCapabilityCallback) {
    boolean bool = queryCapabilityConfiguration(paramInt1, paramInt2);
    if (paramIImsCapabilityCallback != null)
      try {
        paramIImsCapabilityCallback.onQueryCapabilityConfiguration(paramInt1, paramInt2, bool);
      } catch (RemoteException remoteException) {
        Log.e("ImsFeature", "queryCapabilityConfigurationInternal called on dead binder!");
      }  
  }
  
  public Capabilities queryCapabilityStatus() {
    synchronized (this.mLock) {
      return this.mCapabilityStatus.copy();
    } 
  }
  
  public final void requestChangeEnabledCapabilities(CapabilityChangeRequest paramCapabilityChangeRequest, IImsCapabilityCallback paramIImsCapabilityCallback) {
    if (paramCapabilityChangeRequest != null) {
      changeEnabledCapabilities(paramCapabilityChangeRequest, new CapabilityCallbackProxy(paramIImsCapabilityCallback));
      return;
    } 
    throw new IllegalArgumentException("ImsFeature#requestChangeEnabledCapabilities called with invalid params.");
  }
  
  protected final void notifyCapabilitiesStatusChanged(Capabilities paramCapabilities) {
    synchronized (this.mLock) {
      this.mCapabilityStatus = paramCapabilities.copy();
      this.mCapabilityCallbacks.broadcastAction(new _$$Lambda$ImsFeature$9bLETU1BeS_dFzQnbBBs3kwaz_8(paramCapabilities));
      return;
    } 
  }
  
  public abstract void changeEnabledCapabilities(CapabilityChangeRequest paramCapabilityChangeRequest, CapabilityCallbackProxy paramCapabilityCallbackProxy);
  
  protected abstract IInterface getBinder();
  
  public abstract void onFeatureReady();
  
  public abstract void onFeatureRemoved();
  
  public abstract boolean queryCapabilityConfiguration(int paramInt1, int paramInt2);
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface FeatureType {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ImsCapabilityError {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ImsState {}
}
