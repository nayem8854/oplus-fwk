package android.telephony.ims;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

@SystemApi
public final class ImsSuppServiceNotification implements Parcelable {
  public ImsSuppServiceNotification(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString, String[] paramArrayOfString) {
    this.notificationType = paramInt1;
    this.code = paramInt2;
    this.index = paramInt3;
    this.type = paramInt4;
    this.number = paramString;
    this.history = paramArrayOfString;
  }
  
  public ImsSuppServiceNotification(Parcel paramParcel) {
    this.notificationType = paramParcel.readInt();
    this.code = paramParcel.readInt();
    this.index = paramParcel.readInt();
    this.type = paramParcel.readInt();
    this.number = paramParcel.readString();
    this.history = paramParcel.createStringArray();
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{ notificationType=");
    stringBuilder.append(this.notificationType);
    stringBuilder.append(", code=");
    stringBuilder.append(this.code);
    stringBuilder.append(", index=");
    stringBuilder.append(this.index);
    stringBuilder.append(", type=");
    stringBuilder.append(this.type);
    stringBuilder.append(", number=");
    stringBuilder.append(this.number);
    stringBuilder.append(", history=");
    String[] arrayOfString = this.history;
    stringBuilder.append(Arrays.toString((Object[])arrayOfString));
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.notificationType);
    paramParcel.writeInt(this.code);
    paramParcel.writeInt(this.index);
    paramParcel.writeInt(this.type);
    paramParcel.writeString(this.number);
    paramParcel.writeStringArray(this.history);
  }
  
  public static final Parcelable.Creator<ImsSuppServiceNotification> CREATOR = new Parcelable.Creator<ImsSuppServiceNotification>() {
      public ImsSuppServiceNotification createFromParcel(Parcel param1Parcel) {
        return new ImsSuppServiceNotification(param1Parcel);
      }
      
      public ImsSuppServiceNotification[] newArray(int param1Int) {
        return new ImsSuppServiceNotification[param1Int];
      }
    };
  
  private static final String TAG = "ImsSuppServiceNotification";
  
  public final int code;
  
  public final String[] history;
  
  public final int index;
  
  public final int notificationType;
  
  public final String number;
  
  public final int type;
}
