package android.telephony.ims;

import android.annotation.SystemApi;
import android.os.RemoteException;
import android.telephony.CallQuality;
import android.telephony.ServiceState;
import android.telephony.ims.aidl.IImsCallSessionListener;
import android.telephony.ims.stub.ImsCallSessionImplBase;
import com.android.ims.internal.IImsCallSession;

@SystemApi
public class ImsCallSessionListener {
  private final IImsCallSessionListener mListener;
  
  public ImsCallSessionListener(IImsCallSessionListener paramIImsCallSessionListener) {
    this.mListener = paramIImsCallSessionListener;
  }
  
  public void callSessionInitiating(ImsStreamMediaProfile paramImsStreamMediaProfile) {
    try {
      this.mListener.callSessionInitiating(paramImsStreamMediaProfile);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionProgressing(ImsStreamMediaProfile paramImsStreamMediaProfile) {
    try {
      this.mListener.callSessionProgressing(paramImsStreamMediaProfile);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionInitiated(ImsCallProfile paramImsCallProfile) {
    try {
      this.mListener.callSessionInitiated(paramImsCallProfile);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionInitiatedFailed(ImsReasonInfo paramImsReasonInfo) {
    try {
      this.mListener.callSessionInitiatedFailed(paramImsReasonInfo);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionInitiatingFailed(ImsReasonInfo paramImsReasonInfo) {
    try {
      this.mListener.callSessionInitiatingFailed(paramImsReasonInfo);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionTerminated(ImsReasonInfo paramImsReasonInfo) {
    try {
      this.mListener.callSessionTerminated(paramImsReasonInfo);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionHeld(ImsCallProfile paramImsCallProfile) {
    try {
      this.mListener.callSessionHeld(paramImsCallProfile);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionHoldFailed(ImsReasonInfo paramImsReasonInfo) {
    try {
      this.mListener.callSessionHoldFailed(paramImsReasonInfo);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionHoldReceived(ImsCallProfile paramImsCallProfile) {
    try {
      this.mListener.callSessionHoldReceived(paramImsCallProfile);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionResumed(ImsCallProfile paramImsCallProfile) {
    try {
      this.mListener.callSessionResumed(paramImsCallProfile);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionResumeFailed(ImsReasonInfo paramImsReasonInfo) {
    try {
      this.mListener.callSessionResumeFailed(paramImsReasonInfo);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionResumeReceived(ImsCallProfile paramImsCallProfile) {
    try {
      this.mListener.callSessionResumeReceived(paramImsCallProfile);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionMergeStarted(ImsCallSessionImplBase paramImsCallSessionImplBase, ImsCallProfile paramImsCallProfile) {
    try {
      IImsCallSessionListener iImsCallSessionListener = this.mListener;
      if (paramImsCallSessionImplBase != null) {
        IImsCallSession iImsCallSession = paramImsCallSessionImplBase.getServiceImpl();
      } else {
        paramImsCallSessionImplBase = null;
      } 
      iImsCallSessionListener.callSessionMergeStarted((IImsCallSession)paramImsCallSessionImplBase, paramImsCallProfile);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionMergeStarted(IImsCallSession paramIImsCallSession, ImsCallProfile paramImsCallProfile) {
    try {
      this.mListener.callSessionMergeStarted(paramIImsCallSession, paramImsCallProfile);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionMergeComplete(ImsCallSessionImplBase paramImsCallSessionImplBase) {
    try {
      IImsCallSessionListener iImsCallSessionListener = this.mListener;
      if (paramImsCallSessionImplBase != null) {
        IImsCallSession iImsCallSession = paramImsCallSessionImplBase.getServiceImpl();
      } else {
        paramImsCallSessionImplBase = null;
      } 
      iImsCallSessionListener.callSessionMergeComplete((IImsCallSession)paramImsCallSessionImplBase);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionMergeComplete(IImsCallSession paramIImsCallSession) {
    try {
      this.mListener.callSessionMergeComplete(paramIImsCallSession);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionMergeFailed(ImsReasonInfo paramImsReasonInfo) {
    try {
      this.mListener.callSessionMergeFailed(paramImsReasonInfo);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionUpdated(ImsCallProfile paramImsCallProfile) {
    try {
      this.mListener.callSessionUpdated(paramImsCallProfile);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionUpdateFailed(ImsReasonInfo paramImsReasonInfo) {
    try {
      this.mListener.callSessionUpdateFailed(paramImsReasonInfo);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionUpdateReceived(ImsCallProfile paramImsCallProfile) {
    try {
      this.mListener.callSessionUpdateReceived(paramImsCallProfile);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionConferenceExtended(ImsCallSessionImplBase paramImsCallSessionImplBase, ImsCallProfile paramImsCallProfile) {
    try {
      IImsCallSessionListener iImsCallSessionListener = this.mListener;
      if (paramImsCallSessionImplBase != null) {
        IImsCallSession iImsCallSession = paramImsCallSessionImplBase.getServiceImpl();
      } else {
        paramImsCallSessionImplBase = null;
      } 
      iImsCallSessionListener.callSessionConferenceExtended((IImsCallSession)paramImsCallSessionImplBase, paramImsCallProfile);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionConferenceExtended(IImsCallSession paramIImsCallSession, ImsCallProfile paramImsCallProfile) {
    try {
      this.mListener.callSessionConferenceExtended(paramIImsCallSession, paramImsCallProfile);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionConferenceExtendFailed(ImsReasonInfo paramImsReasonInfo) {
    try {
      this.mListener.callSessionConferenceExtendFailed(paramImsReasonInfo);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionConferenceExtendReceived(ImsCallSessionImplBase paramImsCallSessionImplBase, ImsCallProfile paramImsCallProfile) {
    try {
      IImsCallSessionListener iImsCallSessionListener = this.mListener;
      if (paramImsCallSessionImplBase != null) {
        IImsCallSession iImsCallSession = paramImsCallSessionImplBase.getServiceImpl();
      } else {
        paramImsCallSessionImplBase = null;
      } 
      iImsCallSessionListener.callSessionConferenceExtendReceived((IImsCallSession)paramImsCallSessionImplBase, paramImsCallProfile);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionConferenceExtendReceived(IImsCallSession paramIImsCallSession, ImsCallProfile paramImsCallProfile) {
    try {
      this.mListener.callSessionConferenceExtendReceived(paramIImsCallSession, paramImsCallProfile);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionInviteParticipantsRequestDelivered() {
    try {
      this.mListener.callSessionInviteParticipantsRequestDelivered();
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionInviteParticipantsRequestFailed(ImsReasonInfo paramImsReasonInfo) {
    try {
      this.mListener.callSessionInviteParticipantsRequestFailed(paramImsReasonInfo);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionRemoveParticipantsRequestDelivered() {
    try {
      this.mListener.callSessionRemoveParticipantsRequestDelivered();
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionRemoveParticipantsRequestFailed(ImsReasonInfo paramImsReasonInfo) {
    try {
      this.mListener.callSessionInviteParticipantsRequestFailed(paramImsReasonInfo);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionConferenceStateUpdated(ImsConferenceState paramImsConferenceState) {
    try {
      this.mListener.callSessionConferenceStateUpdated(paramImsConferenceState);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionUssdMessageReceived(int paramInt, String paramString) {
    try {
      this.mListener.callSessionUssdMessageReceived(paramInt, paramString);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void callSessionMayHandover(int paramInt1, int paramInt2) {
    paramInt1 = ServiceState.rilRadioTechnologyToNetworkType(paramInt1);
    paramInt2 = ServiceState.rilRadioTechnologyToNetworkType(paramInt2);
    onMayHandover(paramInt1, paramInt2);
  }
  
  public void onMayHandover(int paramInt1, int paramInt2) {
    try {
      this.mListener.callSessionMayHandover(paramInt1, paramInt2);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void callSessionHandover(int paramInt1, int paramInt2, ImsReasonInfo paramImsReasonInfo) {
    paramInt1 = ServiceState.rilRadioTechnologyToNetworkType(paramInt1);
    paramInt2 = ServiceState.rilRadioTechnologyToNetworkType(paramInt2);
    onHandover(paramInt1, paramInt2, paramImsReasonInfo);
  }
  
  public void onHandover(int paramInt1, int paramInt2, ImsReasonInfo paramImsReasonInfo) {
    try {
      this.mListener.callSessionHandover(paramInt1, paramInt2, paramImsReasonInfo);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void callSessionHandoverFailed(int paramInt1, int paramInt2, ImsReasonInfo paramImsReasonInfo) {
    paramInt1 = ServiceState.rilRadioTechnologyToNetworkType(paramInt1);
    paramInt2 = ServiceState.rilRadioTechnologyToNetworkType(paramInt2);
    onHandoverFailed(paramInt1, paramInt2, paramImsReasonInfo);
  }
  
  public void onHandoverFailed(int paramInt1, int paramInt2, ImsReasonInfo paramImsReasonInfo) {
    try {
      this.mListener.callSessionHandoverFailed(paramInt1, paramInt2, paramImsReasonInfo);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionTtyModeReceived(int paramInt) {
    try {
      this.mListener.callSessionTtyModeReceived(paramInt);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionMultipartyStateChanged(boolean paramBoolean) {
    try {
      this.mListener.callSessionMultipartyStateChanged(paramBoolean);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionSuppServiceReceived(ImsSuppServiceNotification paramImsSuppServiceNotification) {
    try {
      this.mListener.callSessionSuppServiceReceived(paramImsSuppServiceNotification);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionRttModifyRequestReceived(ImsCallProfile paramImsCallProfile) {
    try {
      this.mListener.callSessionRttModifyRequestReceived(paramImsCallProfile);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionRttModifyResponseReceived(int paramInt) {
    try {
      this.mListener.callSessionRttModifyResponseReceived(paramInt);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionRttMessageReceived(String paramString) {
    try {
      this.mListener.callSessionRttMessageReceived(paramString);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callSessionRttAudioIndicatorChanged(ImsStreamMediaProfile paramImsStreamMediaProfile) {
    try {
      this.mListener.callSessionRttAudioIndicatorChanged(paramImsStreamMediaProfile);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void callQualityChanged(CallQuality paramCallQuality) {
    try {
      this.mListener.callQualityChanged(paramCallQuality);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
}
