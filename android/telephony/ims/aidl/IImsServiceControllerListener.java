package android.telephony.ims.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.ims.stub.ImsFeatureConfiguration;

public interface IImsServiceControllerListener extends IInterface {
  void onUpdateSupportedImsFeatures(ImsFeatureConfiguration paramImsFeatureConfiguration) throws RemoteException;
  
  class Default implements IImsServiceControllerListener {
    public void onUpdateSupportedImsFeatures(ImsFeatureConfiguration param1ImsFeatureConfiguration) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsServiceControllerListener {
    private static final String DESCRIPTOR = "android.telephony.ims.aidl.IImsServiceControllerListener";
    
    static final int TRANSACTION_onUpdateSupportedImsFeatures = 1;
    
    public Stub() {
      attachInterface(this, "android.telephony.ims.aidl.IImsServiceControllerListener");
    }
    
    public static IImsServiceControllerListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.ims.aidl.IImsServiceControllerListener");
      if (iInterface != null && iInterface instanceof IImsServiceControllerListener)
        return (IImsServiceControllerListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onUpdateSupportedImsFeatures";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.telephony.ims.aidl.IImsServiceControllerListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsServiceControllerListener");
      if (param1Parcel1.readInt() != 0) {
        ImsFeatureConfiguration imsFeatureConfiguration = (ImsFeatureConfiguration)ImsFeatureConfiguration.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onUpdateSupportedImsFeatures((ImsFeatureConfiguration)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IImsServiceControllerListener {
      public static IImsServiceControllerListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.ims.aidl.IImsServiceControllerListener";
      }
      
      public void onUpdateSupportedImsFeatures(ImsFeatureConfiguration param2ImsFeatureConfiguration) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsServiceControllerListener");
          if (param2ImsFeatureConfiguration != null) {
            parcel.writeInt(1);
            param2ImsFeatureConfiguration.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IImsServiceControllerListener.Stub.getDefaultImpl() != null) {
            IImsServiceControllerListener.Stub.getDefaultImpl().onUpdateSupportedImsFeatures(param2ImsFeatureConfiguration);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsServiceControllerListener param1IImsServiceControllerListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsServiceControllerListener != null) {
          Proxy.sDefaultImpl = param1IImsServiceControllerListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsServiceControllerListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
