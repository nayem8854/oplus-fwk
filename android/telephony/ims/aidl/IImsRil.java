package android.telephony.ims.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Message;
import android.os.Parcel;
import android.os.RemoteException;

public interface IImsRil extends IInterface {
  void commonReqToIms(int paramInt1, int paramInt2, Message paramMessage) throws RemoteException;
  
  void registerIndication(IImsRilInd paramIImsRilInd) throws RemoteException;
  
  void unRegisterIndication(IImsRilInd paramIImsRilInd) throws RemoteException;
  
  class Default implements IImsRil {
    public void commonReqToIms(int param1Int1, int param1Int2, Message param1Message) throws RemoteException {}
    
    public void registerIndication(IImsRilInd param1IImsRilInd) throws RemoteException {}
    
    public void unRegisterIndication(IImsRilInd param1IImsRilInd) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsRil {
    private static final String DESCRIPTOR = "android.telephony.ims.aidl.IImsRil";
    
    static final int TRANSACTION_commonReqToIms = 1;
    
    static final int TRANSACTION_registerIndication = 2;
    
    static final int TRANSACTION_unRegisterIndication = 3;
    
    public Stub() {
      attachInterface(this, "android.telephony.ims.aidl.IImsRil");
    }
    
    public static IImsRil asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.ims.aidl.IImsRil");
      if (iInterface != null && iInterface instanceof IImsRil)
        return (IImsRil)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "unRegisterIndication";
        } 
        return "registerIndication";
      } 
      return "commonReqToIms";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IImsRilInd iImsRilInd;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.telephony.ims.aidl.IImsRil");
            return true;
          } 
          param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsRil");
          iImsRilInd = IImsRilInd.Stub.asInterface(param1Parcel1.readStrongBinder());
          unRegisterIndication(iImsRilInd);
          param1Parcel2.writeNoException();
          return true;
        } 
        iImsRilInd.enforceInterface("android.telephony.ims.aidl.IImsRil");
        iImsRilInd = IImsRilInd.Stub.asInterface(iImsRilInd.readStrongBinder());
        registerIndication(iImsRilInd);
        param1Parcel2.writeNoException();
        return true;
      } 
      iImsRilInd.enforceInterface("android.telephony.ims.aidl.IImsRil");
      param1Int1 = iImsRilInd.readInt();
      param1Int2 = iImsRilInd.readInt();
      if (iImsRilInd.readInt() != 0) {
        Message message = (Message)Message.CREATOR.createFromParcel((Parcel)iImsRilInd);
      } else {
        iImsRilInd = null;
      } 
      commonReqToIms(param1Int1, param1Int2, (Message)iImsRilInd);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IImsRil {
      public static IImsRil sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.ims.aidl.IImsRil";
      }
      
      public void commonReqToIms(int param2Int1, int param2Int2, Message param2Message) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsRil");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2Message != null) {
            parcel1.writeInt(1);
            param2Message.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IImsRil.Stub.getDefaultImpl() != null) {
            IImsRil.Stub.getDefaultImpl().commonReqToIms(param2Int1, param2Int2, param2Message);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerIndication(IImsRilInd param2IImsRilInd) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsRil");
          if (param2IImsRilInd != null) {
            iBinder = param2IImsRilInd.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IImsRil.Stub.getDefaultImpl() != null) {
            IImsRil.Stub.getDefaultImpl().registerIndication(param2IImsRilInd);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unRegisterIndication(IImsRilInd param2IImsRilInd) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsRil");
          if (param2IImsRilInd != null) {
            iBinder = param2IImsRilInd.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IImsRil.Stub.getDefaultImpl() != null) {
            IImsRil.Stub.getDefaultImpl().unRegisterIndication(param2IImsRilInd);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsRil param1IImsRil) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsRil != null) {
          Proxy.sDefaultImpl = param1IImsRil;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsRil getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
