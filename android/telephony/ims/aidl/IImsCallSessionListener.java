package android.telephony.ims.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.CallQuality;
import android.telephony.ims.ImsCallProfile;
import android.telephony.ims.ImsConferenceState;
import android.telephony.ims.ImsReasonInfo;
import android.telephony.ims.ImsStreamMediaProfile;
import android.telephony.ims.ImsSuppServiceNotification;
import com.android.ims.internal.IImsCallSession;

public interface IImsCallSessionListener extends IInterface {
  void callQualityChanged(CallQuality paramCallQuality) throws RemoteException;
  
  void callSessionConferenceExtendFailed(ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionConferenceExtendReceived(IImsCallSession paramIImsCallSession, ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionConferenceExtended(IImsCallSession paramIImsCallSession, ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionConferenceStateUpdated(ImsConferenceState paramImsConferenceState) throws RemoteException;
  
  void callSessionHandover(int paramInt1, int paramInt2, ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionHandoverFailed(int paramInt1, int paramInt2, ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionHeld(ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionHoldFailed(ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionHoldReceived(ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionInitiated(ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionInitiatedFailed(ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionInitiating(ImsStreamMediaProfile paramImsStreamMediaProfile) throws RemoteException;
  
  void callSessionInitiatingFailed(ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionInviteParticipantsRequestDelivered() throws RemoteException;
  
  void callSessionInviteParticipantsRequestFailed(ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionMayHandover(int paramInt1, int paramInt2) throws RemoteException;
  
  void callSessionMergeComplete(IImsCallSession paramIImsCallSession) throws RemoteException;
  
  void callSessionMergeFailed(ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionMergeStarted(IImsCallSession paramIImsCallSession, ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionMultipartyStateChanged(boolean paramBoolean) throws RemoteException;
  
  void callSessionProgressing(ImsStreamMediaProfile paramImsStreamMediaProfile) throws RemoteException;
  
  void callSessionRemoveParticipantsRequestDelivered() throws RemoteException;
  
  void callSessionRemoveParticipantsRequestFailed(ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionResumeFailed(ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionResumeReceived(ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionResumed(ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionRttAudioIndicatorChanged(ImsStreamMediaProfile paramImsStreamMediaProfile) throws RemoteException;
  
  void callSessionRttMessageReceived(String paramString) throws RemoteException;
  
  void callSessionRttModifyRequestReceived(ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionRttModifyResponseReceived(int paramInt) throws RemoteException;
  
  void callSessionSuppServiceReceived(ImsSuppServiceNotification paramImsSuppServiceNotification) throws RemoteException;
  
  void callSessionTerminated(ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionTransferFailed(ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionTransferred() throws RemoteException;
  
  void callSessionTtyModeReceived(int paramInt) throws RemoteException;
  
  void callSessionUpdateFailed(ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionUpdateReceived(ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionUpdated(ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionUssdMessageReceived(int paramInt, String paramString) throws RemoteException;
  
  class Default implements IImsCallSessionListener {
    public void callSessionInitiating(ImsStreamMediaProfile param1ImsStreamMediaProfile) throws RemoteException {}
    
    public void callSessionProgressing(ImsStreamMediaProfile param1ImsStreamMediaProfile) throws RemoteException {}
    
    public void callSessionInitiated(ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionInitiatedFailed(ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionInitiatingFailed(ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionTerminated(ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionHeld(ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionHoldFailed(ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionHoldReceived(ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionResumed(ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionResumeFailed(ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionResumeReceived(ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionMergeStarted(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionMergeComplete(IImsCallSession param1IImsCallSession) throws RemoteException {}
    
    public void callSessionMergeFailed(ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionUpdated(ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionUpdateFailed(ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionUpdateReceived(ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionConferenceExtended(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionConferenceExtendFailed(ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionConferenceExtendReceived(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionInviteParticipantsRequestDelivered() throws RemoteException {}
    
    public void callSessionInviteParticipantsRequestFailed(ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionRemoveParticipantsRequestDelivered() throws RemoteException {}
    
    public void callSessionRemoveParticipantsRequestFailed(ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionConferenceStateUpdated(ImsConferenceState param1ImsConferenceState) throws RemoteException {}
    
    public void callSessionUssdMessageReceived(int param1Int, String param1String) throws RemoteException {}
    
    public void callSessionHandover(int param1Int1, int param1Int2, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionHandoverFailed(int param1Int1, int param1Int2, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionMayHandover(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void callSessionTtyModeReceived(int param1Int) throws RemoteException {}
    
    public void callSessionMultipartyStateChanged(boolean param1Boolean) throws RemoteException {}
    
    public void callSessionSuppServiceReceived(ImsSuppServiceNotification param1ImsSuppServiceNotification) throws RemoteException {}
    
    public void callSessionRttModifyRequestReceived(ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionRttModifyResponseReceived(int param1Int) throws RemoteException {}
    
    public void callSessionRttMessageReceived(String param1String) throws RemoteException {}
    
    public void callSessionRttAudioIndicatorChanged(ImsStreamMediaProfile param1ImsStreamMediaProfile) throws RemoteException {}
    
    public void callSessionTransferred() throws RemoteException {}
    
    public void callSessionTransferFailed(ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callQualityChanged(CallQuality param1CallQuality) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsCallSessionListener {
    private static final String DESCRIPTOR = "android.telephony.ims.aidl.IImsCallSessionListener";
    
    static final int TRANSACTION_callQualityChanged = 40;
    
    static final int TRANSACTION_callSessionConferenceExtendFailed = 20;
    
    static final int TRANSACTION_callSessionConferenceExtendReceived = 21;
    
    static final int TRANSACTION_callSessionConferenceExtended = 19;
    
    static final int TRANSACTION_callSessionConferenceStateUpdated = 26;
    
    static final int TRANSACTION_callSessionHandover = 28;
    
    static final int TRANSACTION_callSessionHandoverFailed = 29;
    
    static final int TRANSACTION_callSessionHeld = 7;
    
    static final int TRANSACTION_callSessionHoldFailed = 8;
    
    static final int TRANSACTION_callSessionHoldReceived = 9;
    
    static final int TRANSACTION_callSessionInitiated = 3;
    
    static final int TRANSACTION_callSessionInitiatedFailed = 4;
    
    static final int TRANSACTION_callSessionInitiating = 1;
    
    static final int TRANSACTION_callSessionInitiatingFailed = 5;
    
    static final int TRANSACTION_callSessionInviteParticipantsRequestDelivered = 22;
    
    static final int TRANSACTION_callSessionInviteParticipantsRequestFailed = 23;
    
    static final int TRANSACTION_callSessionMayHandover = 30;
    
    static final int TRANSACTION_callSessionMergeComplete = 14;
    
    static final int TRANSACTION_callSessionMergeFailed = 15;
    
    static final int TRANSACTION_callSessionMergeStarted = 13;
    
    static final int TRANSACTION_callSessionMultipartyStateChanged = 32;
    
    static final int TRANSACTION_callSessionProgressing = 2;
    
    static final int TRANSACTION_callSessionRemoveParticipantsRequestDelivered = 24;
    
    static final int TRANSACTION_callSessionRemoveParticipantsRequestFailed = 25;
    
    static final int TRANSACTION_callSessionResumeFailed = 11;
    
    static final int TRANSACTION_callSessionResumeReceived = 12;
    
    static final int TRANSACTION_callSessionResumed = 10;
    
    static final int TRANSACTION_callSessionRttAudioIndicatorChanged = 37;
    
    static final int TRANSACTION_callSessionRttMessageReceived = 36;
    
    static final int TRANSACTION_callSessionRttModifyRequestReceived = 34;
    
    static final int TRANSACTION_callSessionRttModifyResponseReceived = 35;
    
    static final int TRANSACTION_callSessionSuppServiceReceived = 33;
    
    static final int TRANSACTION_callSessionTerminated = 6;
    
    static final int TRANSACTION_callSessionTransferFailed = 39;
    
    static final int TRANSACTION_callSessionTransferred = 38;
    
    static final int TRANSACTION_callSessionTtyModeReceived = 31;
    
    static final int TRANSACTION_callSessionUpdateFailed = 17;
    
    static final int TRANSACTION_callSessionUpdateReceived = 18;
    
    static final int TRANSACTION_callSessionUpdated = 16;
    
    static final int TRANSACTION_callSessionUssdMessageReceived = 27;
    
    public Stub() {
      attachInterface(this, "android.telephony.ims.aidl.IImsCallSessionListener");
    }
    
    public static IImsCallSessionListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.ims.aidl.IImsCallSessionListener");
      if (iInterface != null && iInterface instanceof IImsCallSessionListener)
        return (IImsCallSessionListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 40:
          return "callQualityChanged";
        case 39:
          return "callSessionTransferFailed";
        case 38:
          return "callSessionTransferred";
        case 37:
          return "callSessionRttAudioIndicatorChanged";
        case 36:
          return "callSessionRttMessageReceived";
        case 35:
          return "callSessionRttModifyResponseReceived";
        case 34:
          return "callSessionRttModifyRequestReceived";
        case 33:
          return "callSessionSuppServiceReceived";
        case 32:
          return "callSessionMultipartyStateChanged";
        case 31:
          return "callSessionTtyModeReceived";
        case 30:
          return "callSessionMayHandover";
        case 29:
          return "callSessionHandoverFailed";
        case 28:
          return "callSessionHandover";
        case 27:
          return "callSessionUssdMessageReceived";
        case 26:
          return "callSessionConferenceStateUpdated";
        case 25:
          return "callSessionRemoveParticipantsRequestFailed";
        case 24:
          return "callSessionRemoveParticipantsRequestDelivered";
        case 23:
          return "callSessionInviteParticipantsRequestFailed";
        case 22:
          return "callSessionInviteParticipantsRequestDelivered";
        case 21:
          return "callSessionConferenceExtendReceived";
        case 20:
          return "callSessionConferenceExtendFailed";
        case 19:
          return "callSessionConferenceExtended";
        case 18:
          return "callSessionUpdateReceived";
        case 17:
          return "callSessionUpdateFailed";
        case 16:
          return "callSessionUpdated";
        case 15:
          return "callSessionMergeFailed";
        case 14:
          return "callSessionMergeComplete";
        case 13:
          return "callSessionMergeStarted";
        case 12:
          return "callSessionResumeReceived";
        case 11:
          return "callSessionResumeFailed";
        case 10:
          return "callSessionResumed";
        case 9:
          return "callSessionHoldReceived";
        case 8:
          return "callSessionHoldFailed";
        case 7:
          return "callSessionHeld";
        case 6:
          return "callSessionTerminated";
        case 5:
          return "callSessionInitiatingFailed";
        case 4:
          return "callSessionInitiatedFailed";
        case 3:
          return "callSessionInitiated";
        case 2:
          return "callSessionProgressing";
        case 1:
          break;
      } 
      return "callSessionInitiating";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IImsCallSession iImsCallSession;
      if (param1Int1 != 1598968902) {
        String str;
        IImsCallSession iImsCallSession1;
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 40:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (param1Parcel1.readInt() != 0) {
              CallQuality callQuality = (CallQuality)CallQuality.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            callQualityChanged((CallQuality)param1Parcel1);
            return true;
          case 39:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (param1Parcel1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            callSessionTransferFailed((ImsReasonInfo)param1Parcel1);
            return true;
          case 38:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            callSessionTransferred();
            return true;
          case 37:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (param1Parcel1.readInt() != 0) {
              ImsStreamMediaProfile imsStreamMediaProfile = (ImsStreamMediaProfile)ImsStreamMediaProfile.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            callSessionRttAudioIndicatorChanged((ImsStreamMediaProfile)param1Parcel1);
            return true;
          case 36:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            str = param1Parcel1.readString();
            callSessionRttMessageReceived(str);
            return true;
          case 35:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            param1Int1 = str.readInt();
            callSessionRttModifyResponseReceived(param1Int1);
            return true;
          case 34:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (str.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            callSessionRttModifyRequestReceived((ImsCallProfile)str);
            return true;
          case 33:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (str.readInt() != 0) {
              ImsSuppServiceNotification imsSuppServiceNotification = (ImsSuppServiceNotification)ImsSuppServiceNotification.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            callSessionSuppServiceReceived((ImsSuppServiceNotification)str);
            return true;
          case 32:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (str.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            callSessionMultipartyStateChanged(bool);
            return true;
          case 31:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            param1Int1 = str.readInt();
            callSessionTtyModeReceived(param1Int1);
            return true;
          case 30:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            param1Int1 = str.readInt();
            param1Int2 = str.readInt();
            callSessionMayHandover(param1Int1, param1Int2);
            return true;
          case 29:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            param1Int1 = str.readInt();
            param1Int2 = str.readInt();
            if (str.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            callSessionHandoverFailed(param1Int1, param1Int2, (ImsReasonInfo)str);
            return true;
          case 28:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            param1Int2 = str.readInt();
            param1Int1 = str.readInt();
            if (str.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            callSessionHandover(param1Int2, param1Int1, (ImsReasonInfo)str);
            return true;
          case 27:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            param1Int1 = str.readInt();
            str = str.readString();
            callSessionUssdMessageReceived(param1Int1, str);
            return true;
          case 26:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (str.readInt() != 0) {
              ImsConferenceState imsConferenceState = (ImsConferenceState)ImsConferenceState.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            callSessionConferenceStateUpdated((ImsConferenceState)str);
            return true;
          case 25:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (str.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            callSessionRemoveParticipantsRequestFailed((ImsReasonInfo)str);
            return true;
          case 24:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            callSessionRemoveParticipantsRequestDelivered();
            return true;
          case 23:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (str.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            callSessionInviteParticipantsRequestFailed((ImsReasonInfo)str);
            return true;
          case 22:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            callSessionInviteParticipantsRequestDelivered();
            return true;
          case 21:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(str.readStrongBinder());
            if (str.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            callSessionConferenceExtendReceived(iImsCallSession, (ImsCallProfile)str);
            return true;
          case 20:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (str.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            callSessionConferenceExtendFailed((ImsReasonInfo)str);
            return true;
          case 19:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(str.readStrongBinder());
            if (str.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            callSessionConferenceExtended(iImsCallSession, (ImsCallProfile)str);
            return true;
          case 18:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (str.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            callSessionUpdateReceived((ImsCallProfile)str);
            return true;
          case 17:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (str.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            callSessionUpdateFailed((ImsReasonInfo)str);
            return true;
          case 16:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (str.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            callSessionUpdated((ImsCallProfile)str);
            return true;
          case 15:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (str.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            callSessionMergeFailed((ImsReasonInfo)str);
            return true;
          case 14:
            str.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            iImsCallSession1 = IImsCallSession.Stub.asInterface(str.readStrongBinder());
            callSessionMergeComplete(iImsCallSession1);
            return true;
          case 13:
            iImsCallSession1.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            if (iImsCallSession1.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionMergeStarted(iImsCallSession, (ImsCallProfile)iImsCallSession1);
            return true;
          case 12:
            iImsCallSession1.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (iImsCallSession1.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionResumeReceived((ImsCallProfile)iImsCallSession1);
            return true;
          case 11:
            iImsCallSession1.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (iImsCallSession1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionResumeFailed((ImsReasonInfo)iImsCallSession1);
            return true;
          case 10:
            iImsCallSession1.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (iImsCallSession1.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionResumed((ImsCallProfile)iImsCallSession1);
            return true;
          case 9:
            iImsCallSession1.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (iImsCallSession1.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionHoldReceived((ImsCallProfile)iImsCallSession1);
            return true;
          case 8:
            iImsCallSession1.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (iImsCallSession1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionHoldFailed((ImsReasonInfo)iImsCallSession1);
            return true;
          case 7:
            iImsCallSession1.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (iImsCallSession1.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionHeld((ImsCallProfile)iImsCallSession1);
            return true;
          case 6:
            iImsCallSession1.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (iImsCallSession1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionTerminated((ImsReasonInfo)iImsCallSession1);
            return true;
          case 5:
            iImsCallSession1.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (iImsCallSession1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionInitiatingFailed((ImsReasonInfo)iImsCallSession1);
            return true;
          case 4:
            iImsCallSession1.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (iImsCallSession1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionInitiatedFailed((ImsReasonInfo)iImsCallSession1);
            return true;
          case 3:
            iImsCallSession1.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (iImsCallSession1.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionInitiated((ImsCallProfile)iImsCallSession1);
            return true;
          case 2:
            iImsCallSession1.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
            if (iImsCallSession1.readInt() != 0) {
              ImsStreamMediaProfile imsStreamMediaProfile = (ImsStreamMediaProfile)ImsStreamMediaProfile.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionProgressing((ImsStreamMediaProfile)iImsCallSession1);
            return true;
          case 1:
            break;
        } 
        iImsCallSession1.enforceInterface("android.telephony.ims.aidl.IImsCallSessionListener");
        if (iImsCallSession1.readInt() != 0) {
          ImsStreamMediaProfile imsStreamMediaProfile = (ImsStreamMediaProfile)ImsStreamMediaProfile.CREATOR.createFromParcel((Parcel)iImsCallSession1);
        } else {
          iImsCallSession1 = null;
        } 
        callSessionInitiating((ImsStreamMediaProfile)iImsCallSession1);
        return true;
      } 
      iImsCallSession.writeString("android.telephony.ims.aidl.IImsCallSessionListener");
      return true;
    }
    
    private static class Proxy implements IImsCallSessionListener {
      public static IImsCallSessionListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.ims.aidl.IImsCallSessionListener";
      }
      
      public void callSessionInitiating(ImsStreamMediaProfile param2ImsStreamMediaProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsStreamMediaProfile != null) {
            parcel.writeInt(1);
            param2ImsStreamMediaProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionInitiating(param2ImsStreamMediaProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionProgressing(ImsStreamMediaProfile param2ImsStreamMediaProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsStreamMediaProfile != null) {
            parcel.writeInt(1);
            param2ImsStreamMediaProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionProgressing(param2ImsStreamMediaProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionInitiated(ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionInitiated(param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionInitiatedFailed(ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionInitiatedFailed(param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionInitiatingFailed(ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionInitiatingFailed(param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionTerminated(ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionTerminated(param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionHeld(ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionHeld(param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionHoldFailed(ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionHoldFailed(param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionHoldReceived(ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionHoldReceived(param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionResumed(ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionResumed(param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionResumeFailed(ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionResumeFailed(param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionResumeReceived(ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(12, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionResumeReceived(param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionMergeStarted(IImsCallSession param2IImsCallSession, ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(13, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionMergeStarted(param2IImsCallSession, param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionMergeComplete(IImsCallSession param2IImsCallSession) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(14, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionMergeComplete(param2IImsCallSession);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionMergeFailed(ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(15, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionMergeFailed(param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionUpdated(ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(16, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionUpdated(param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionUpdateFailed(ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(17, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionUpdateFailed(param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionUpdateReceived(ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(18, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionUpdateReceived(param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionConferenceExtended(IImsCallSession param2IImsCallSession, ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(19, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionConferenceExtended(param2IImsCallSession, param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionConferenceExtendFailed(ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(20, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionConferenceExtendFailed(param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionConferenceExtendReceived(IImsCallSession param2IImsCallSession, ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(21, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionConferenceExtendReceived(param2IImsCallSession, param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionInviteParticipantsRequestDelivered() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          boolean bool = this.mRemote.transact(22, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionInviteParticipantsRequestDelivered();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionInviteParticipantsRequestFailed(ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(23, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionInviteParticipantsRequestFailed(param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionRemoveParticipantsRequestDelivered() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          boolean bool = this.mRemote.transact(24, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionRemoveParticipantsRequestDelivered();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionRemoveParticipantsRequestFailed(ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(25, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionRemoveParticipantsRequestFailed(param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionConferenceStateUpdated(ImsConferenceState param2ImsConferenceState) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsConferenceState != null) {
            parcel.writeInt(1);
            param2ImsConferenceState.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(26, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionConferenceStateUpdated(param2ImsConferenceState);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionUssdMessageReceived(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(27, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionUssdMessageReceived(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionHandover(int param2Int1, int param2Int2, ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(28, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionHandover(param2Int1, param2Int2, param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionHandoverFailed(int param2Int1, int param2Int2, ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(29, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionHandoverFailed(param2Int1, param2Int2, param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionMayHandover(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(30, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionMayHandover(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionTtyModeReceived(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(31, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionTtyModeReceived(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionMultipartyStateChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(32, parcel, null, 1);
          if (!bool1 && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionMultipartyStateChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionSuppServiceReceived(ImsSuppServiceNotification param2ImsSuppServiceNotification) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsSuppServiceNotification != null) {
            parcel.writeInt(1);
            param2ImsSuppServiceNotification.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(33, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionSuppServiceReceived(param2ImsSuppServiceNotification);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionRttModifyRequestReceived(ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(34, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionRttModifyRequestReceived(param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionRttModifyResponseReceived(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(35, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionRttModifyResponseReceived(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionRttMessageReceived(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(36, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionRttMessageReceived(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionRttAudioIndicatorChanged(ImsStreamMediaProfile param2ImsStreamMediaProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsStreamMediaProfile != null) {
            parcel.writeInt(1);
            param2ImsStreamMediaProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(37, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionRttAudioIndicatorChanged(param2ImsStreamMediaProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionTransferred() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          boolean bool = this.mRemote.transact(38, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionTransferred();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionTransferFailed(ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(39, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionTransferFailed(param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callQualityChanged(CallQuality param2CallQuality) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCallSessionListener");
          if (param2CallQuality != null) {
            parcel.writeInt(1);
            param2CallQuality.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(40, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callQualityChanged(param2CallQuality);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsCallSessionListener param1IImsCallSessionListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsCallSessionListener != null) {
          Proxy.sDefaultImpl = param1IImsCallSessionListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsCallSessionListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
