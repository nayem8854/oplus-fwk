package android.telephony.ims.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IImsRegistration extends IInterface {
  void addRegistrationCallback(IImsRegistrationCallback paramIImsRegistrationCallback) throws RemoteException;
  
  int getRegistrationTechnology() throws RemoteException;
  
  void removeRegistrationCallback(IImsRegistrationCallback paramIImsRegistrationCallback) throws RemoteException;
  
  class Default implements IImsRegistration {
    public int getRegistrationTechnology() throws RemoteException {
      return 0;
    }
    
    public void addRegistrationCallback(IImsRegistrationCallback param1IImsRegistrationCallback) throws RemoteException {}
    
    public void removeRegistrationCallback(IImsRegistrationCallback param1IImsRegistrationCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsRegistration {
    private static final String DESCRIPTOR = "android.telephony.ims.aidl.IImsRegistration";
    
    static final int TRANSACTION_addRegistrationCallback = 2;
    
    static final int TRANSACTION_getRegistrationTechnology = 1;
    
    static final int TRANSACTION_removeRegistrationCallback = 3;
    
    public Stub() {
      attachInterface(this, "android.telephony.ims.aidl.IImsRegistration");
    }
    
    public static IImsRegistration asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.ims.aidl.IImsRegistration");
      if (iInterface != null && iInterface instanceof IImsRegistration)
        return (IImsRegistration)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "removeRegistrationCallback";
        } 
        return "addRegistrationCallback";
      } 
      return "getRegistrationTechnology";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IImsRegistrationCallback iImsRegistrationCallback;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.telephony.ims.aidl.IImsRegistration");
            return true;
          } 
          param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsRegistration");
          iImsRegistrationCallback = IImsRegistrationCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
          removeRegistrationCallback(iImsRegistrationCallback);
          return true;
        } 
        iImsRegistrationCallback.enforceInterface("android.telephony.ims.aidl.IImsRegistration");
        iImsRegistrationCallback = IImsRegistrationCallback.Stub.asInterface(iImsRegistrationCallback.readStrongBinder());
        addRegistrationCallback(iImsRegistrationCallback);
        return true;
      } 
      iImsRegistrationCallback.enforceInterface("android.telephony.ims.aidl.IImsRegistration");
      param1Int1 = getRegistrationTechnology();
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(param1Int1);
      return true;
    }
    
    private static class Proxy implements IImsRegistration {
      public static IImsRegistration sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.ims.aidl.IImsRegistration";
      }
      
      public int getRegistrationTechnology() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsRegistration");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IImsRegistration.Stub.getDefaultImpl() != null)
            return IImsRegistration.Stub.getDefaultImpl().getRegistrationTechnology(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addRegistrationCallback(IImsRegistrationCallback param2IImsRegistrationCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsRegistration");
          if (param2IImsRegistrationCallback != null) {
            iBinder = param2IImsRegistrationCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IImsRegistration.Stub.getDefaultImpl() != null) {
            IImsRegistration.Stub.getDefaultImpl().addRegistrationCallback(param2IImsRegistrationCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeRegistrationCallback(IImsRegistrationCallback param2IImsRegistrationCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsRegistration");
          if (param2IImsRegistrationCallback != null) {
            iBinder = param2IImsRegistrationCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IImsRegistration.Stub.getDefaultImpl() != null) {
            IImsRegistration.Stub.getDefaultImpl().removeRegistrationCallback(param2IImsRegistrationCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsRegistration param1IImsRegistration) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsRegistration != null) {
          Proxy.sDefaultImpl = param1IImsRegistration;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsRegistration getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
