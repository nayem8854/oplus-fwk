package android.telephony.ims.aidl;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.ims.ImsCallProfile;
import android.telephony.ims.ImsReasonInfo;
import com.android.ims.internal.IImsCallSession;

public interface IImsMmTelListener extends IInterface {
  void onIncomingCall(IImsCallSession paramIImsCallSession, Bundle paramBundle) throws RemoteException;
  
  void onRejectedCall(ImsCallProfile paramImsCallProfile, ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void onVoiceMessageCountUpdate(int paramInt) throws RemoteException;
  
  class Default implements IImsMmTelListener {
    public void onIncomingCall(IImsCallSession param1IImsCallSession, Bundle param1Bundle) throws RemoteException {}
    
    public void onRejectedCall(ImsCallProfile param1ImsCallProfile, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void onVoiceMessageCountUpdate(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsMmTelListener {
    private static final String DESCRIPTOR = "android.telephony.ims.aidl.IImsMmTelListener";
    
    static final int TRANSACTION_onIncomingCall = 1;
    
    static final int TRANSACTION_onRejectedCall = 2;
    
    static final int TRANSACTION_onVoiceMessageCountUpdate = 3;
    
    public Stub() {
      attachInterface(this, "android.telephony.ims.aidl.IImsMmTelListener");
    }
    
    public static IImsMmTelListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.ims.aidl.IImsMmTelListener");
      if (iInterface != null && iInterface instanceof IImsMmTelListener)
        return (IImsMmTelListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onVoiceMessageCountUpdate";
        } 
        return "onRejectedCall";
      } 
      return "onIncomingCall";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.telephony.ims.aidl.IImsMmTelListener");
            return true;
          } 
          param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsMmTelListener");
          param1Int1 = param1Parcel1.readInt();
          onVoiceMessageCountUpdate(param1Int1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsMmTelListener");
        if (param1Parcel1.readInt() != 0) {
          ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel2 = null;
        } 
        if (param1Parcel1.readInt() != 0) {
          ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onRejectedCall((ImsCallProfile)param1Parcel2, (ImsReasonInfo)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsMmTelListener");
      IImsCallSession iImsCallSession = IImsCallSession.Stub.asInterface(param1Parcel1.readStrongBinder());
      if (param1Parcel1.readInt() != 0) {
        Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onIncomingCall(iImsCallSession, (Bundle)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IImsMmTelListener {
      public static IImsMmTelListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.ims.aidl.IImsMmTelListener";
      }
      
      public void onIncomingCall(IImsCallSession param2IImsCallSession, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IImsMmTelListener.Stub.getDefaultImpl() != null) {
            IImsMmTelListener.Stub.getDefaultImpl().onIncomingCall(param2IImsCallSession, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRejectedCall(ImsCallProfile param2ImsCallProfile, ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelListener");
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IImsMmTelListener.Stub.getDefaultImpl() != null) {
            IImsMmTelListener.Stub.getDefaultImpl().onRejectedCall(param2ImsCallProfile, param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onVoiceMessageCountUpdate(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IImsMmTelListener.Stub.getDefaultImpl() != null) {
            IImsMmTelListener.Stub.getDefaultImpl().onVoiceMessageCountUpdate(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsMmTelListener param1IImsMmTelListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsMmTelListener != null) {
          Proxy.sDefaultImpl = param1IImsMmTelListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsMmTelListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
