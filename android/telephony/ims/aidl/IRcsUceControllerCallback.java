package android.telephony.ims.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.ims.RcsContactUceCapability;
import java.util.ArrayList;
import java.util.List;

public interface IRcsUceControllerCallback extends IInterface {
  void onCapabilitiesReceived(List<RcsContactUceCapability> paramList) throws RemoteException;
  
  void onError(int paramInt) throws RemoteException;
  
  class Default implements IRcsUceControllerCallback {
    public void onCapabilitiesReceived(List<RcsContactUceCapability> param1List) throws RemoteException {}
    
    public void onError(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRcsUceControllerCallback {
    private static final String DESCRIPTOR = "android.telephony.ims.aidl.IRcsUceControllerCallback";
    
    static final int TRANSACTION_onCapabilitiesReceived = 1;
    
    static final int TRANSACTION_onError = 2;
    
    public Stub() {
      attachInterface(this, "android.telephony.ims.aidl.IRcsUceControllerCallback");
    }
    
    public static IRcsUceControllerCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.ims.aidl.IRcsUceControllerCallback");
      if (iInterface != null && iInterface instanceof IRcsUceControllerCallback)
        return (IRcsUceControllerCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onError";
      } 
      return "onCapabilitiesReceived";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.telephony.ims.aidl.IRcsUceControllerCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("android.telephony.ims.aidl.IRcsUceControllerCallback");
        param1Int1 = param1Parcel1.readInt();
        onError(param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.telephony.ims.aidl.IRcsUceControllerCallback");
      ArrayList<RcsContactUceCapability> arrayList = param1Parcel1.createTypedArrayList(RcsContactUceCapability.CREATOR);
      onCapabilitiesReceived(arrayList);
      return true;
    }
    
    private static class Proxy implements IRcsUceControllerCallback {
      public static IRcsUceControllerCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.ims.aidl.IRcsUceControllerCallback";
      }
      
      public void onCapabilitiesReceived(List<RcsContactUceCapability> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IRcsUceControllerCallback");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IRcsUceControllerCallback.Stub.getDefaultImpl() != null) {
            IRcsUceControllerCallback.Stub.getDefaultImpl().onCapabilitiesReceived(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onError(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IRcsUceControllerCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IRcsUceControllerCallback.Stub.getDefaultImpl() != null) {
            IRcsUceControllerCallback.Stub.getDefaultImpl().onError(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRcsUceControllerCallback param1IRcsUceControllerCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRcsUceControllerCallback != null) {
          Proxy.sDefaultImpl = param1IRcsUceControllerCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRcsUceControllerCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
