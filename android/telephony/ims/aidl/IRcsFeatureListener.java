package android.telephony.ims.aidl;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.ims.RcsContactUceCapability;
import java.util.ArrayList;
import java.util.List;

public interface IRcsFeatureListener extends IInterface {
  void onCapabilityRequestResponseOptions(int paramInt1, String paramString, RcsContactUceCapability paramRcsContactUceCapability, int paramInt2) throws RemoteException;
  
  void onCapabilityRequestResponsePresence(List<RcsContactUceCapability> paramList, int paramInt) throws RemoteException;
  
  void onCommandUpdate(int paramInt1, int paramInt2) throws RemoteException;
  
  void onNetworkResponse(int paramInt1, String paramString, int paramInt2) throws RemoteException;
  
  void onNotifyUpdateCapabilities(int paramInt) throws RemoteException;
  
  void onRemoteCapabilityRequest(Uri paramUri, RcsContactUceCapability paramRcsContactUceCapability, int paramInt) throws RemoteException;
  
  void onUnpublish() throws RemoteException;
  
  class Default implements IRcsFeatureListener {
    public void onCommandUpdate(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onNetworkResponse(int param1Int1, String param1String, int param1Int2) throws RemoteException {}
    
    public void onCapabilityRequestResponsePresence(List<RcsContactUceCapability> param1List, int param1Int) throws RemoteException {}
    
    public void onNotifyUpdateCapabilities(int param1Int) throws RemoteException {}
    
    public void onUnpublish() throws RemoteException {}
    
    public void onCapabilityRequestResponseOptions(int param1Int1, String param1String, RcsContactUceCapability param1RcsContactUceCapability, int param1Int2) throws RemoteException {}
    
    public void onRemoteCapabilityRequest(Uri param1Uri, RcsContactUceCapability param1RcsContactUceCapability, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRcsFeatureListener {
    private static final String DESCRIPTOR = "android.telephony.ims.aidl.IRcsFeatureListener";
    
    static final int TRANSACTION_onCapabilityRequestResponseOptions = 6;
    
    static final int TRANSACTION_onCapabilityRequestResponsePresence = 3;
    
    static final int TRANSACTION_onCommandUpdate = 1;
    
    static final int TRANSACTION_onNetworkResponse = 2;
    
    static final int TRANSACTION_onNotifyUpdateCapabilities = 4;
    
    static final int TRANSACTION_onRemoteCapabilityRequest = 7;
    
    static final int TRANSACTION_onUnpublish = 5;
    
    public Stub() {
      attachInterface(this, "android.telephony.ims.aidl.IRcsFeatureListener");
    }
    
    public static IRcsFeatureListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.ims.aidl.IRcsFeatureListener");
      if (iInterface != null && iInterface instanceof IRcsFeatureListener)
        return (IRcsFeatureListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "onRemoteCapabilityRequest";
        case 6:
          return "onCapabilityRequestResponseOptions";
        case 5:
          return "onUnpublish";
        case 4:
          return "onNotifyUpdateCapabilities";
        case 3:
          return "onCapabilityRequestResponsePresence";
        case 2:
          return "onNetworkResponse";
        case 1:
          break;
      } 
      return "onCommandUpdate";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        ArrayList<RcsContactUceCapability> arrayList;
        RcsContactUceCapability rcsContactUceCapability;
        String str1;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IRcsFeatureListener");
            if (param1Parcel1.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              rcsContactUceCapability = (RcsContactUceCapability)RcsContactUceCapability.CREATOR.createFromParcel(param1Parcel1);
            } else {
              rcsContactUceCapability = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            onRemoteCapabilityRequest((Uri)param1Parcel2, rcsContactUceCapability, param1Int1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IRcsFeatureListener");
            param1Int1 = param1Parcel1.readInt();
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              RcsContactUceCapability rcsContactUceCapability1 = (RcsContactUceCapability)RcsContactUceCapability.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int2 = param1Parcel1.readInt();
            onCapabilityRequestResponseOptions(param1Int1, str1, (RcsContactUceCapability)param1Parcel2, param1Int2);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IRcsFeatureListener");
            onUnpublish();
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IRcsFeatureListener");
            param1Int1 = param1Parcel1.readInt();
            onNotifyUpdateCapabilities(param1Int1);
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IRcsFeatureListener");
            arrayList = param1Parcel1.createTypedArrayList(RcsContactUceCapability.CREATOR);
            param1Int1 = param1Parcel1.readInt();
            onCapabilityRequestResponsePresence(arrayList, param1Int1);
            return true;
          case 2:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IRcsFeatureListener");
            param1Int2 = param1Parcel1.readInt();
            str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            onNetworkResponse(param1Int2, str, param1Int1);
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("android.telephony.ims.aidl.IRcsFeatureListener");
        param1Int2 = param1Parcel1.readInt();
        param1Int1 = param1Parcel1.readInt();
        onCommandUpdate(param1Int2, param1Int1);
        return true;
      } 
      str.writeString("android.telephony.ims.aidl.IRcsFeatureListener");
      return true;
    }
    
    private static class Proxy implements IRcsFeatureListener {
      public static IRcsFeatureListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.ims.aidl.IRcsFeatureListener";
      }
      
      public void onCommandUpdate(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IRcsFeatureListener");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IRcsFeatureListener.Stub.getDefaultImpl() != null) {
            IRcsFeatureListener.Stub.getDefaultImpl().onCommandUpdate(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onNetworkResponse(int param2Int1, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IRcsFeatureListener");
          parcel.writeInt(param2Int1);
          parcel.writeString(param2String);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IRcsFeatureListener.Stub.getDefaultImpl() != null) {
            IRcsFeatureListener.Stub.getDefaultImpl().onNetworkResponse(param2Int1, param2String, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCapabilityRequestResponsePresence(List<RcsContactUceCapability> param2List, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IRcsFeatureListener");
          parcel.writeTypedList(param2List);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IRcsFeatureListener.Stub.getDefaultImpl() != null) {
            IRcsFeatureListener.Stub.getDefaultImpl().onCapabilityRequestResponsePresence(param2List, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onNotifyUpdateCapabilities(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IRcsFeatureListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IRcsFeatureListener.Stub.getDefaultImpl() != null) {
            IRcsFeatureListener.Stub.getDefaultImpl().onNotifyUpdateCapabilities(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onUnpublish() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IRcsFeatureListener");
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IRcsFeatureListener.Stub.getDefaultImpl() != null) {
            IRcsFeatureListener.Stub.getDefaultImpl().onUnpublish();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCapabilityRequestResponseOptions(int param2Int1, String param2String, RcsContactUceCapability param2RcsContactUceCapability, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IRcsFeatureListener");
          parcel.writeInt(param2Int1);
          parcel.writeString(param2String);
          if (param2RcsContactUceCapability != null) {
            parcel.writeInt(1);
            param2RcsContactUceCapability.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IRcsFeatureListener.Stub.getDefaultImpl() != null) {
            IRcsFeatureListener.Stub.getDefaultImpl().onCapabilityRequestResponseOptions(param2Int1, param2String, param2RcsContactUceCapability, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRemoteCapabilityRequest(Uri param2Uri, RcsContactUceCapability param2RcsContactUceCapability, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IRcsFeatureListener");
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2RcsContactUceCapability != null) {
            parcel.writeInt(1);
            param2RcsContactUceCapability.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IRcsFeatureListener.Stub.getDefaultImpl() != null) {
            IRcsFeatureListener.Stub.getDefaultImpl().onRemoteCapabilityRequest(param2Uri, param2RcsContactUceCapability, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRcsFeatureListener param1IRcsFeatureListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRcsFeatureListener != null) {
          Proxy.sDefaultImpl = param1IRcsFeatureListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRcsFeatureListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
