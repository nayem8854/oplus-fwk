package android.telephony.ims.aidl;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.telephony.ims.ImsReasonInfo;

public interface IImsRegistrationCallback extends IInterface {
  void onDeregistered(ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void onRegistered(int paramInt) throws RemoteException;
  
  void onRegistering(int paramInt) throws RemoteException;
  
  void onSubscriberAssociatedUriChanged(Uri[] paramArrayOfUri) throws RemoteException;
  
  void onTechnologyChangeFailed(int paramInt, ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  class Default implements IImsRegistrationCallback {
    public void onRegistered(int param1Int) throws RemoteException {}
    
    public void onRegistering(int param1Int) throws RemoteException {}
    
    public void onDeregistered(ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void onTechnologyChangeFailed(int param1Int, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void onSubscriberAssociatedUriChanged(Uri[] param1ArrayOfUri) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsRegistrationCallback {
    private static final String DESCRIPTOR = "android.telephony.ims.aidl.IImsRegistrationCallback";
    
    static final int TRANSACTION_onDeregistered = 3;
    
    static final int TRANSACTION_onRegistered = 1;
    
    static final int TRANSACTION_onRegistering = 2;
    
    static final int TRANSACTION_onSubscriberAssociatedUriChanged = 5;
    
    static final int TRANSACTION_onTechnologyChangeFailed = 4;
    
    public Stub() {
      attachInterface(this, "android.telephony.ims.aidl.IImsRegistrationCallback");
    }
    
    public static IImsRegistrationCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.ims.aidl.IImsRegistrationCallback");
      if (iInterface != null && iInterface instanceof IImsRegistrationCallback)
        return (IImsRegistrationCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "onSubscriberAssociatedUriChanged";
            } 
            return "onTechnologyChangeFailed";
          } 
          return "onDeregistered";
        } 
        return "onRegistering";
      } 
      return "onRegistered";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      Uri[] arrayOfUri;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.telephony.ims.aidl.IImsRegistrationCallback");
                return true;
              } 
              param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsRegistrationCallback");
              arrayOfUri = (Uri[])param1Parcel1.createTypedArray(Uri.CREATOR);
              onSubscriberAssociatedUriChanged(arrayOfUri);
              return true;
            } 
            arrayOfUri.enforceInterface("android.telephony.ims.aidl.IImsRegistrationCallback");
            param1Int1 = arrayOfUri.readInt();
            if (arrayOfUri.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)arrayOfUri);
            } else {
              arrayOfUri = null;
            } 
            onTechnologyChangeFailed(param1Int1, (ImsReasonInfo)arrayOfUri);
            return true;
          } 
          arrayOfUri.enforceInterface("android.telephony.ims.aidl.IImsRegistrationCallback");
          if (arrayOfUri.readInt() != 0) {
            ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)arrayOfUri);
          } else {
            arrayOfUri = null;
          } 
          onDeregistered((ImsReasonInfo)arrayOfUri);
          return true;
        } 
        arrayOfUri.enforceInterface("android.telephony.ims.aidl.IImsRegistrationCallback");
        param1Int1 = arrayOfUri.readInt();
        onRegistering(param1Int1);
        return true;
      } 
      arrayOfUri.enforceInterface("android.telephony.ims.aidl.IImsRegistrationCallback");
      param1Int1 = arrayOfUri.readInt();
      onRegistered(param1Int1);
      return true;
    }
    
    private static class Proxy implements IImsRegistrationCallback {
      public static IImsRegistrationCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.ims.aidl.IImsRegistrationCallback";
      }
      
      public void onRegistered(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsRegistrationCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IImsRegistrationCallback.Stub.getDefaultImpl() != null) {
            IImsRegistrationCallback.Stub.getDefaultImpl().onRegistered(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRegistering(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsRegistrationCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IImsRegistrationCallback.Stub.getDefaultImpl() != null) {
            IImsRegistrationCallback.Stub.getDefaultImpl().onRegistering(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDeregistered(ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsRegistrationCallback");
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IImsRegistrationCallback.Stub.getDefaultImpl() != null) {
            IImsRegistrationCallback.Stub.getDefaultImpl().onDeregistered(param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTechnologyChangeFailed(int param2Int, ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsRegistrationCallback");
          parcel.writeInt(param2Int);
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IImsRegistrationCallback.Stub.getDefaultImpl() != null) {
            IImsRegistrationCallback.Stub.getDefaultImpl().onTechnologyChangeFailed(param2Int, param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSubscriberAssociatedUriChanged(Uri[] param2ArrayOfUri) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsRegistrationCallback");
          parcel.writeTypedArray((Parcelable[])param2ArrayOfUri, 0);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IImsRegistrationCallback.Stub.getDefaultImpl() != null) {
            IImsRegistrationCallback.Stub.getDefaultImpl().onSubscriberAssociatedUriChanged(param2ArrayOfUri);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsRegistrationCallback param1IImsRegistrationCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsRegistrationCallback != null) {
          Proxy.sDefaultImpl = param1IImsRegistrationCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsRegistrationCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
