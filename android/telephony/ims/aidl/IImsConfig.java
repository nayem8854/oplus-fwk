package android.telephony.ims.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.PersistableBundle;
import android.os.RemoteException;

public interface IImsConfig extends IInterface {
  void addImsConfigCallback(IImsConfigCallback paramIImsConfigCallback) throws RemoteException;
  
  int getConfigInt(int paramInt) throws RemoteException;
  
  String getConfigString(int paramInt) throws RemoteException;
  
  void notifyRcsAutoConfigurationReceived(byte[] paramArrayOfbyte, boolean paramBoolean) throws RemoteException;
  
  void removeImsConfigCallback(IImsConfigCallback paramIImsConfigCallback) throws RemoteException;
  
  int setConfigInt(int paramInt1, int paramInt2) throws RemoteException;
  
  int setConfigString(int paramInt, String paramString) throws RemoteException;
  
  void updateImsCarrierConfigs(PersistableBundle paramPersistableBundle) throws RemoteException;
  
  class Default implements IImsConfig {
    public void addImsConfigCallback(IImsConfigCallback param1IImsConfigCallback) throws RemoteException {}
    
    public void removeImsConfigCallback(IImsConfigCallback param1IImsConfigCallback) throws RemoteException {}
    
    public int getConfigInt(int param1Int) throws RemoteException {
      return 0;
    }
    
    public String getConfigString(int param1Int) throws RemoteException {
      return null;
    }
    
    public int setConfigInt(int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int setConfigString(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public void updateImsCarrierConfigs(PersistableBundle param1PersistableBundle) throws RemoteException {}
    
    public void notifyRcsAutoConfigurationReceived(byte[] param1ArrayOfbyte, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsConfig {
    private static final String DESCRIPTOR = "android.telephony.ims.aidl.IImsConfig";
    
    static final int TRANSACTION_addImsConfigCallback = 1;
    
    static final int TRANSACTION_getConfigInt = 3;
    
    static final int TRANSACTION_getConfigString = 4;
    
    static final int TRANSACTION_notifyRcsAutoConfigurationReceived = 8;
    
    static final int TRANSACTION_removeImsConfigCallback = 2;
    
    static final int TRANSACTION_setConfigInt = 5;
    
    static final int TRANSACTION_setConfigString = 6;
    
    static final int TRANSACTION_updateImsCarrierConfigs = 7;
    
    public Stub() {
      attachInterface(this, "android.telephony.ims.aidl.IImsConfig");
    }
    
    public static IImsConfig asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.ims.aidl.IImsConfig");
      if (iInterface != null && iInterface instanceof IImsConfig)
        return (IImsConfig)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "notifyRcsAutoConfigurationReceived";
        case 7:
          return "updateImsCarrierConfigs";
        case 6:
          return "setConfigString";
        case 5:
          return "setConfigInt";
        case 4:
          return "getConfigString";
        case 3:
          return "getConfigInt";
        case 2:
          return "removeImsConfigCallback";
        case 1:
          break;
      } 
      return "addImsConfigCallback";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String str;
        byte[] arrayOfByte;
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsConfig");
            arrayOfByte = param1Parcel1.createByteArray();
            if (param1Parcel1.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            notifyRcsAutoConfigurationReceived(arrayOfByte, bool);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsConfig");
            if (param1Parcel1.readInt() != 0) {
              PersistableBundle persistableBundle = (PersistableBundle)PersistableBundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            updateImsCarrierConfigs((PersistableBundle)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsConfig");
            param1Int1 = param1Parcel1.readInt();
            str = param1Parcel1.readString();
            param1Int1 = setConfigString(param1Int1, str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 5:
            str.enforceInterface("android.telephony.ims.aidl.IImsConfig");
            param1Int2 = str.readInt();
            param1Int1 = str.readInt();
            param1Int1 = setConfigInt(param1Int2, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 4:
            str.enforceInterface("android.telephony.ims.aidl.IImsConfig");
            param1Int1 = str.readInt();
            str = getConfigString(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str);
            return true;
          case 3:
            str.enforceInterface("android.telephony.ims.aidl.IImsConfig");
            param1Int1 = str.readInt();
            param1Int1 = getConfigInt(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 2:
            str.enforceInterface("android.telephony.ims.aidl.IImsConfig");
            iImsConfigCallback = IImsConfigCallback.Stub.asInterface(str.readStrongBinder());
            removeImsConfigCallback(iImsConfigCallback);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        iImsConfigCallback.enforceInterface("android.telephony.ims.aidl.IImsConfig");
        IImsConfigCallback iImsConfigCallback = IImsConfigCallback.Stub.asInterface(iImsConfigCallback.readStrongBinder());
        addImsConfigCallback(iImsConfigCallback);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.telephony.ims.aidl.IImsConfig");
      return true;
    }
    
    private static class Proxy implements IImsConfig {
      public static IImsConfig sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.ims.aidl.IImsConfig";
      }
      
      public void addImsConfigCallback(IImsConfigCallback param2IImsConfigCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsConfig");
          if (param2IImsConfigCallback != null) {
            iBinder = param2IImsConfigCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IImsConfig.Stub.getDefaultImpl() != null) {
            IImsConfig.Stub.getDefaultImpl().addImsConfigCallback(param2IImsConfigCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeImsConfigCallback(IImsConfigCallback param2IImsConfigCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsConfig");
          if (param2IImsConfigCallback != null) {
            iBinder = param2IImsConfigCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IImsConfig.Stub.getDefaultImpl() != null) {
            IImsConfig.Stub.getDefaultImpl().removeImsConfigCallback(param2IImsConfigCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getConfigInt(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsConfig");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IImsConfig.Stub.getDefaultImpl() != null) {
            param2Int = IImsConfig.Stub.getDefaultImpl().getConfigInt(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getConfigString(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsConfig");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IImsConfig.Stub.getDefaultImpl() != null)
            return IImsConfig.Stub.getDefaultImpl().getConfigString(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setConfigInt(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsConfig");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IImsConfig.Stub.getDefaultImpl() != null) {
            param2Int1 = IImsConfig.Stub.getDefaultImpl().setConfigInt(param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setConfigString(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsConfig");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IImsConfig.Stub.getDefaultImpl() != null) {
            param2Int = IImsConfig.Stub.getDefaultImpl().setConfigString(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateImsCarrierConfigs(PersistableBundle param2PersistableBundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsConfig");
          if (param2PersistableBundle != null) {
            parcel1.writeInt(1);
            param2PersistableBundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IImsConfig.Stub.getDefaultImpl() != null) {
            IImsConfig.Stub.getDefaultImpl().updateImsCarrierConfigs(param2PersistableBundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyRcsAutoConfigurationReceived(byte[] param2ArrayOfbyte, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsConfig");
          parcel1.writeByteArray(param2ArrayOfbyte);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool1 && IImsConfig.Stub.getDefaultImpl() != null) {
            IImsConfig.Stub.getDefaultImpl().notifyRcsAutoConfigurationReceived(param2ArrayOfbyte, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsConfig param1IImsConfig) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsConfig != null) {
          Proxy.sDefaultImpl = param1IImsConfig;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsConfig getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
