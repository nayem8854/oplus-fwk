package android.telephony.ims.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Message;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.ims.ImsCallProfile;
import android.telephony.ims.feature.CapabilityChangeRequest;
import com.android.ims.internal.IImsCallSession;
import com.android.ims.internal.IImsEcbm;
import com.android.ims.internal.IImsMultiEndpoint;
import com.android.ims.internal.IImsUt;

public interface IImsMmTelFeature extends IInterface {
  void acknowledgeSms(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void acknowledgeSmsReport(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void addCapabilityCallback(IImsCapabilityCallback paramIImsCapabilityCallback) throws RemoteException;
  
  void changeCapabilitiesConfiguration(CapabilityChangeRequest paramCapabilityChangeRequest, IImsCapabilityCallback paramIImsCapabilityCallback) throws RemoteException;
  
  ImsCallProfile createCallProfile(int paramInt1, int paramInt2) throws RemoteException;
  
  IImsCallSession createCallSession(ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  IImsEcbm getEcbmInterface() throws RemoteException;
  
  int getFeatureState() throws RemoteException;
  
  IImsMultiEndpoint getMultiEndpointInterface() throws RemoteException;
  
  String getSmsFormat() throws RemoteException;
  
  IImsUt getUtInterface() throws RemoteException;
  
  void onSmsReady() throws RemoteException;
  
  void queryCapabilityConfiguration(int paramInt1, int paramInt2, IImsCapabilityCallback paramIImsCapabilityCallback) throws RemoteException;
  
  int queryCapabilityStatus() throws RemoteException;
  
  void removeCapabilityCallback(IImsCapabilityCallback paramIImsCapabilityCallback) throws RemoteException;
  
  void sendSms(int paramInt1, int paramInt2, String paramString1, String paramString2, boolean paramBoolean, byte[] paramArrayOfbyte) throws RemoteException;
  
  void setListener(IImsMmTelListener paramIImsMmTelListener) throws RemoteException;
  
  void setSmsListener(IImsSmsListener paramIImsSmsListener) throws RemoteException;
  
  void setUiTtyMode(int paramInt, Message paramMessage) throws RemoteException;
  
  int shouldProcessCall(String[] paramArrayOfString) throws RemoteException;
  
  class Default implements IImsMmTelFeature {
    public void setListener(IImsMmTelListener param1IImsMmTelListener) throws RemoteException {}
    
    public int getFeatureState() throws RemoteException {
      return 0;
    }
    
    public ImsCallProfile createCallProfile(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public IImsCallSession createCallSession(ImsCallProfile param1ImsCallProfile) throws RemoteException {
      return null;
    }
    
    public int shouldProcessCall(String[] param1ArrayOfString) throws RemoteException {
      return 0;
    }
    
    public IImsUt getUtInterface() throws RemoteException {
      return null;
    }
    
    public IImsEcbm getEcbmInterface() throws RemoteException {
      return null;
    }
    
    public void setUiTtyMode(int param1Int, Message param1Message) throws RemoteException {}
    
    public IImsMultiEndpoint getMultiEndpointInterface() throws RemoteException {
      return null;
    }
    
    public int queryCapabilityStatus() throws RemoteException {
      return 0;
    }
    
    public void addCapabilityCallback(IImsCapabilityCallback param1IImsCapabilityCallback) throws RemoteException {}
    
    public void removeCapabilityCallback(IImsCapabilityCallback param1IImsCapabilityCallback) throws RemoteException {}
    
    public void changeCapabilitiesConfiguration(CapabilityChangeRequest param1CapabilityChangeRequest, IImsCapabilityCallback param1IImsCapabilityCallback) throws RemoteException {}
    
    public void queryCapabilityConfiguration(int param1Int1, int param1Int2, IImsCapabilityCallback param1IImsCapabilityCallback) throws RemoteException {}
    
    public void setSmsListener(IImsSmsListener param1IImsSmsListener) throws RemoteException {}
    
    public void sendSms(int param1Int1, int param1Int2, String param1String1, String param1String2, boolean param1Boolean, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void acknowledgeSms(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void acknowledgeSmsReport(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public String getSmsFormat() throws RemoteException {
      return null;
    }
    
    public void onSmsReady() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsMmTelFeature {
    private static final String DESCRIPTOR = "android.telephony.ims.aidl.IImsMmTelFeature";
    
    static final int TRANSACTION_acknowledgeSms = 17;
    
    static final int TRANSACTION_acknowledgeSmsReport = 18;
    
    static final int TRANSACTION_addCapabilityCallback = 11;
    
    static final int TRANSACTION_changeCapabilitiesConfiguration = 13;
    
    static final int TRANSACTION_createCallProfile = 3;
    
    static final int TRANSACTION_createCallSession = 4;
    
    static final int TRANSACTION_getEcbmInterface = 7;
    
    static final int TRANSACTION_getFeatureState = 2;
    
    static final int TRANSACTION_getMultiEndpointInterface = 9;
    
    static final int TRANSACTION_getSmsFormat = 19;
    
    static final int TRANSACTION_getUtInterface = 6;
    
    static final int TRANSACTION_onSmsReady = 20;
    
    static final int TRANSACTION_queryCapabilityConfiguration = 14;
    
    static final int TRANSACTION_queryCapabilityStatus = 10;
    
    static final int TRANSACTION_removeCapabilityCallback = 12;
    
    static final int TRANSACTION_sendSms = 16;
    
    static final int TRANSACTION_setListener = 1;
    
    static final int TRANSACTION_setSmsListener = 15;
    
    static final int TRANSACTION_setUiTtyMode = 8;
    
    static final int TRANSACTION_shouldProcessCall = 5;
    
    public Stub() {
      attachInterface(this, "android.telephony.ims.aidl.IImsMmTelFeature");
    }
    
    public static IImsMmTelFeature asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.ims.aidl.IImsMmTelFeature");
      if (iInterface != null && iInterface instanceof IImsMmTelFeature)
        return (IImsMmTelFeature)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 20:
          return "onSmsReady";
        case 19:
          return "getSmsFormat";
        case 18:
          return "acknowledgeSmsReport";
        case 17:
          return "acknowledgeSms";
        case 16:
          return "sendSms";
        case 15:
          return "setSmsListener";
        case 14:
          return "queryCapabilityConfiguration";
        case 13:
          return "changeCapabilitiesConfiguration";
        case 12:
          return "removeCapabilityCallback";
        case 11:
          return "addCapabilityCallback";
        case 10:
          return "queryCapabilityStatus";
        case 9:
          return "getMultiEndpointInterface";
        case 8:
          return "setUiTtyMode";
        case 7:
          return "getEcbmInterface";
        case 6:
          return "getUtInterface";
        case 5:
          return "shouldProcessCall";
        case 4:
          return "createCallSession";
        case 3:
          return "createCallProfile";
        case 2:
          return "getFeatureState";
        case 1:
          break;
      } 
      return "setListener";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        String str1;
        byte[] arrayOfByte;
        IImsSmsListener iImsSmsListener;
        IImsCapabilityCallback iImsCapabilityCallback1;
        IBinder iBinder2;
        String[] arrayOfString;
        IImsUt iImsUt1;
        IBinder iBinder1;
        ImsCallProfile imsCallProfile;
        IImsCallSession iImsCallSession;
        IImsMultiEndpoint iImsMultiEndpoint;
        IImsEcbm iImsEcbm;
        IImsUt iImsUt2;
        int i;
        boolean bool;
        IBinder iBinder3 = null, iBinder4 = null;
        String str2 = null;
        IImsCapabilityCallback iImsCapabilityCallback2 = null;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 20:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsMmTelFeature");
            onSmsReady();
            return true;
          case 19:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsMmTelFeature");
            str1 = getSmsFormat();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 18:
            str1.enforceInterface("android.telephony.ims.aidl.IImsMmTelFeature");
            param1Int1 = str1.readInt();
            param1Int2 = str1.readInt();
            i = str1.readInt();
            acknowledgeSmsReport(param1Int1, param1Int2, i);
            return true;
          case 17:
            str1.enforceInterface("android.telephony.ims.aidl.IImsMmTelFeature");
            i = str1.readInt();
            param1Int1 = str1.readInt();
            param1Int2 = str1.readInt();
            acknowledgeSms(i, param1Int1, param1Int2);
            return true;
          case 16:
            str1.enforceInterface("android.telephony.ims.aidl.IImsMmTelFeature");
            param1Int2 = str1.readInt();
            param1Int1 = str1.readInt();
            str = str1.readString();
            str2 = str1.readString();
            if (str1.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            arrayOfByte = str1.createByteArray();
            sendSms(param1Int2, param1Int1, str, str2, bool, arrayOfByte);
            return true;
          case 15:
            arrayOfByte.enforceInterface("android.telephony.ims.aidl.IImsMmTelFeature");
            iImsSmsListener = IImsSmsListener.Stub.asInterface(arrayOfByte.readStrongBinder());
            setSmsListener(iImsSmsListener);
            str.writeNoException();
            return true;
          case 14:
            iImsSmsListener.enforceInterface("android.telephony.ims.aidl.IImsMmTelFeature");
            param1Int1 = iImsSmsListener.readInt();
            param1Int2 = iImsSmsListener.readInt();
            iImsCapabilityCallback1 = IImsCapabilityCallback.Stub.asInterface(iImsSmsListener.readStrongBinder());
            queryCapabilityConfiguration(param1Int1, param1Int2, iImsCapabilityCallback1);
            return true;
          case 13:
            iImsCapabilityCallback1.enforceInterface("android.telephony.ims.aidl.IImsMmTelFeature");
            if (iImsCapabilityCallback1.readInt() != 0) {
              CapabilityChangeRequest capabilityChangeRequest = (CapabilityChangeRequest)CapabilityChangeRequest.CREATOR.createFromParcel((Parcel)iImsCapabilityCallback1);
            } else {
              str = null;
            } 
            iImsCapabilityCallback1 = IImsCapabilityCallback.Stub.asInterface(iImsCapabilityCallback1.readStrongBinder());
            changeCapabilitiesConfiguration((CapabilityChangeRequest)str, iImsCapabilityCallback1);
            return true;
          case 12:
            iImsCapabilityCallback1.enforceInterface("android.telephony.ims.aidl.IImsMmTelFeature");
            iImsCapabilityCallback1 = IImsCapabilityCallback.Stub.asInterface(iImsCapabilityCallback1.readStrongBinder());
            removeCapabilityCallback(iImsCapabilityCallback1);
            return true;
          case 11:
            iImsCapabilityCallback1.enforceInterface("android.telephony.ims.aidl.IImsMmTelFeature");
            iImsCapabilityCallback1 = IImsCapabilityCallback.Stub.asInterface(iImsCapabilityCallback1.readStrongBinder());
            addCapabilityCallback(iImsCapabilityCallback1);
            return true;
          case 10:
            iImsCapabilityCallback1.enforceInterface("android.telephony.ims.aidl.IImsMmTelFeature");
            param1Int1 = queryCapabilityStatus();
            str.writeNoException();
            str.writeInt(param1Int1);
            return true;
          case 9:
            iImsCapabilityCallback1.enforceInterface("android.telephony.ims.aidl.IImsMmTelFeature");
            iImsMultiEndpoint = getMultiEndpointInterface();
            str.writeNoException();
            iImsCapabilityCallback1 = iImsCapabilityCallback2;
            if (iImsMultiEndpoint != null)
              iBinder2 = iImsMultiEndpoint.asBinder(); 
            str.writeStrongBinder(iBinder2);
            return true;
          case 8:
            iBinder2.enforceInterface("android.telephony.ims.aidl.IImsMmTelFeature");
            param1Int1 = iBinder2.readInt();
            if (iBinder2.readInt() != 0) {
              Message message = (Message)Message.CREATOR.createFromParcel((Parcel)iBinder2);
            } else {
              iBinder2 = null;
            } 
            setUiTtyMode(param1Int1, (Message)iBinder2);
            str.writeNoException();
            return true;
          case 7:
            iBinder2.enforceInterface("android.telephony.ims.aidl.IImsMmTelFeature");
            iImsEcbm = getEcbmInterface();
            str.writeNoException();
            iBinder2 = iBinder3;
            if (iImsEcbm != null)
              iBinder2 = iImsEcbm.asBinder(); 
            str.writeStrongBinder(iBinder2);
            return true;
          case 6:
            iBinder2.enforceInterface("android.telephony.ims.aidl.IImsMmTelFeature");
            iImsUt2 = getUtInterface();
            str.writeNoException();
            iBinder2 = iBinder4;
            if (iImsUt2 != null)
              iBinder2 = iImsUt2.asBinder(); 
            str.writeStrongBinder(iBinder2);
            return true;
          case 5:
            iBinder2.enforceInterface("android.telephony.ims.aidl.IImsMmTelFeature");
            arrayOfString = iBinder2.createStringArray();
            param1Int1 = shouldProcessCall(arrayOfString);
            str.writeNoException();
            str.writeInt(param1Int1);
            return true;
          case 4:
            arrayOfString.enforceInterface("android.telephony.ims.aidl.IImsMmTelFeature");
            if (arrayOfString.readInt() != 0) {
              ImsCallProfile imsCallProfile1 = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)arrayOfString);
            } else {
              arrayOfString = null;
            } 
            iImsCallSession = createCallSession((ImsCallProfile)arrayOfString);
            str.writeNoException();
            iImsUt1 = iImsUt2;
            if (iImsCallSession != null)
              iBinder1 = iImsCallSession.asBinder(); 
            str.writeStrongBinder(iBinder1);
            return true;
          case 3:
            iBinder1.enforceInterface("android.telephony.ims.aidl.IImsMmTelFeature");
            param1Int2 = iBinder1.readInt();
            param1Int1 = iBinder1.readInt();
            imsCallProfile = createCallProfile(param1Int2, param1Int1);
            str.writeNoException();
            if (imsCallProfile != null) {
              str.writeInt(1);
              imsCallProfile.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 2:
            imsCallProfile.enforceInterface("android.telephony.ims.aidl.IImsMmTelFeature");
            param1Int1 = getFeatureState();
            str.writeNoException();
            str.writeInt(param1Int1);
            return true;
          case 1:
            break;
        } 
        imsCallProfile.enforceInterface("android.telephony.ims.aidl.IImsMmTelFeature");
        IImsMmTelListener iImsMmTelListener = IImsMmTelListener.Stub.asInterface(imsCallProfile.readStrongBinder());
        setListener(iImsMmTelListener);
        str.writeNoException();
        return true;
      } 
      str.writeString("android.telephony.ims.aidl.IImsMmTelFeature");
      return true;
    }
    
    private static class Proxy implements IImsMmTelFeature {
      public static IImsMmTelFeature sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.ims.aidl.IImsMmTelFeature";
      }
      
      public void setListener(IImsMmTelListener param2IImsMmTelListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelFeature");
          if (param2IImsMmTelListener != null) {
            iBinder = param2IImsMmTelListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IImsMmTelFeature.Stub.getDefaultImpl() != null) {
            IImsMmTelFeature.Stub.getDefaultImpl().setListener(param2IImsMmTelListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getFeatureState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelFeature");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IImsMmTelFeature.Stub.getDefaultImpl() != null)
            return IImsMmTelFeature.Stub.getDefaultImpl().getFeatureState(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ImsCallProfile createCallProfile(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ImsCallProfile imsCallProfile;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelFeature");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IImsMmTelFeature.Stub.getDefaultImpl() != null) {
            imsCallProfile = IImsMmTelFeature.Stub.getDefaultImpl().createCallProfile(param2Int1, param2Int2);
            return imsCallProfile;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel(parcel2);
          } else {
            imsCallProfile = null;
          } 
          return imsCallProfile;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsCallSession createCallSession(ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelFeature");
          if (param2ImsCallProfile != null) {
            parcel1.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IImsMmTelFeature.Stub.getDefaultImpl() != null)
            return IImsMmTelFeature.Stub.getDefaultImpl().createCallSession(param2ImsCallProfile); 
          parcel2.readException();
          return IImsCallSession.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int shouldProcessCall(String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelFeature");
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IImsMmTelFeature.Stub.getDefaultImpl() != null)
            return IImsMmTelFeature.Stub.getDefaultImpl().shouldProcessCall(param2ArrayOfString); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsUt getUtInterface() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelFeature");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IImsMmTelFeature.Stub.getDefaultImpl() != null)
            return IImsMmTelFeature.Stub.getDefaultImpl().getUtInterface(); 
          parcel2.readException();
          return IImsUt.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsEcbm getEcbmInterface() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelFeature");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IImsMmTelFeature.Stub.getDefaultImpl() != null)
            return IImsMmTelFeature.Stub.getDefaultImpl().getEcbmInterface(); 
          parcel2.readException();
          return IImsEcbm.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUiTtyMode(int param2Int, Message param2Message) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelFeature");
          parcel1.writeInt(param2Int);
          if (param2Message != null) {
            parcel1.writeInt(1);
            param2Message.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IImsMmTelFeature.Stub.getDefaultImpl() != null) {
            IImsMmTelFeature.Stub.getDefaultImpl().setUiTtyMode(param2Int, param2Message);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsMultiEndpoint getMultiEndpointInterface() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelFeature");
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IImsMmTelFeature.Stub.getDefaultImpl() != null)
            return IImsMmTelFeature.Stub.getDefaultImpl().getMultiEndpointInterface(); 
          parcel2.readException();
          return IImsMultiEndpoint.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int queryCapabilityStatus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelFeature");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IImsMmTelFeature.Stub.getDefaultImpl() != null)
            return IImsMmTelFeature.Stub.getDefaultImpl().queryCapabilityStatus(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addCapabilityCallback(IImsCapabilityCallback param2IImsCapabilityCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelFeature");
          if (param2IImsCapabilityCallback != null) {
            iBinder = param2IImsCapabilityCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IImsMmTelFeature.Stub.getDefaultImpl() != null) {
            IImsMmTelFeature.Stub.getDefaultImpl().addCapabilityCallback(param2IImsCapabilityCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeCapabilityCallback(IImsCapabilityCallback param2IImsCapabilityCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelFeature");
          if (param2IImsCapabilityCallback != null) {
            iBinder = param2IImsCapabilityCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(12, parcel, null, 1);
          if (!bool && IImsMmTelFeature.Stub.getDefaultImpl() != null) {
            IImsMmTelFeature.Stub.getDefaultImpl().removeCapabilityCallback(param2IImsCapabilityCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void changeCapabilitiesConfiguration(CapabilityChangeRequest param2CapabilityChangeRequest, IImsCapabilityCallback param2IImsCapabilityCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelFeature");
          if (param2CapabilityChangeRequest != null) {
            parcel.writeInt(1);
            param2CapabilityChangeRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IImsCapabilityCallback != null) {
            iBinder = param2IImsCapabilityCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(13, parcel, null, 1);
          if (!bool && IImsMmTelFeature.Stub.getDefaultImpl() != null) {
            IImsMmTelFeature.Stub.getDefaultImpl().changeCapabilitiesConfiguration(param2CapabilityChangeRequest, param2IImsCapabilityCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void queryCapabilityConfiguration(int param2Int1, int param2Int2, IImsCapabilityCallback param2IImsCapabilityCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelFeature");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2IImsCapabilityCallback != null) {
            iBinder = param2IImsCapabilityCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(14, parcel, null, 1);
          if (!bool && IImsMmTelFeature.Stub.getDefaultImpl() != null) {
            IImsMmTelFeature.Stub.getDefaultImpl().queryCapabilityConfiguration(param2Int1, param2Int2, param2IImsCapabilityCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setSmsListener(IImsSmsListener param2IImsSmsListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelFeature");
          if (param2IImsSmsListener != null) {
            iBinder = param2IImsSmsListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IImsMmTelFeature.Stub.getDefaultImpl() != null) {
            IImsMmTelFeature.Stub.getDefaultImpl().setSmsListener(param2IImsSmsListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendSms(int param2Int1, int param2Int2, String param2String1, String param2String2, boolean param2Boolean, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelFeature");
          try {
            parcel.writeInt(param2Int1);
            try {
              parcel.writeInt(param2Int2);
              try {
                parcel.writeString(param2String1);
                try {
                  boolean bool;
                  parcel.writeString(param2String2);
                  if (param2Boolean) {
                    bool = true;
                  } else {
                    bool = false;
                  } 
                  parcel.writeInt(bool);
                  try {
                    parcel.writeByteArray(param2ArrayOfbyte);
                    try {
                      boolean bool1 = this.mRemote.transact(16, parcel, null, 1);
                      if (!bool1 && IImsMmTelFeature.Stub.getDefaultImpl() != null) {
                        IImsMmTelFeature.Stub.getDefaultImpl().sendSms(param2Int1, param2Int2, param2String1, param2String2, param2Boolean, param2ArrayOfbyte);
                        parcel.recycle();
                        return;
                      } 
                      parcel.recycle();
                      return;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2String1;
      }
      
      public void acknowledgeSms(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelFeature");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(17, parcel, null, 1);
          if (!bool && IImsMmTelFeature.Stub.getDefaultImpl() != null) {
            IImsMmTelFeature.Stub.getDefaultImpl().acknowledgeSms(param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void acknowledgeSmsReport(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelFeature");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(18, parcel, null, 1);
          if (!bool && IImsMmTelFeature.Stub.getDefaultImpl() != null) {
            IImsMmTelFeature.Stub.getDefaultImpl().acknowledgeSmsReport(param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public String getSmsFormat() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelFeature");
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IImsMmTelFeature.Stub.getDefaultImpl() != null)
            return IImsMmTelFeature.Stub.getDefaultImpl().getSmsFormat(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onSmsReady() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsMmTelFeature");
          boolean bool = this.mRemote.transact(20, parcel, null, 1);
          if (!bool && IImsMmTelFeature.Stub.getDefaultImpl() != null) {
            IImsMmTelFeature.Stub.getDefaultImpl().onSmsReady();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsMmTelFeature param1IImsMmTelFeature) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsMmTelFeature != null) {
          Proxy.sDefaultImpl = param1IImsMmTelFeature;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsMmTelFeature getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
