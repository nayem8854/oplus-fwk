package android.telephony.ims.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IImsSmsListener extends IInterface {
  void onSendSmsResult(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) throws RemoteException;
  
  void onSmsReceived(int paramInt, String paramString, byte[] paramArrayOfbyte) throws RemoteException;
  
  void onSmsStatusReportReceived(int paramInt, String paramString, byte[] paramArrayOfbyte) throws RemoteException;
  
  class Default implements IImsSmsListener {
    public void onSendSmsResult(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5) throws RemoteException {}
    
    public void onSmsStatusReportReceived(int param1Int, String param1String, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void onSmsReceived(int param1Int, String param1String, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsSmsListener {
    private static final String DESCRIPTOR = "android.telephony.ims.aidl.IImsSmsListener";
    
    static final int TRANSACTION_onSendSmsResult = 1;
    
    static final int TRANSACTION_onSmsReceived = 3;
    
    static final int TRANSACTION_onSmsStatusReportReceived = 2;
    
    public Stub() {
      attachInterface(this, "android.telephony.ims.aidl.IImsSmsListener");
    }
    
    public static IImsSmsListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.ims.aidl.IImsSmsListener");
      if (iInterface != null && iInterface instanceof IImsSmsListener)
        return (IImsSmsListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onSmsReceived";
        } 
        return "onSmsStatusReportReceived";
      } 
      return "onSendSmsResult";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      byte[] arrayOfByte;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.telephony.ims.aidl.IImsSmsListener");
            return true;
          } 
          param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsSmsListener");
          param1Int1 = param1Parcel1.readInt();
          String str1 = param1Parcel1.readString();
          arrayOfByte = param1Parcel1.createByteArray();
          onSmsReceived(param1Int1, str1, arrayOfByte);
          return true;
        } 
        arrayOfByte.enforceInterface("android.telephony.ims.aidl.IImsSmsListener");
        param1Int1 = arrayOfByte.readInt();
        String str = arrayOfByte.readString();
        arrayOfByte = arrayOfByte.createByteArray();
        onSmsStatusReportReceived(param1Int1, str, arrayOfByte);
        return true;
      } 
      arrayOfByte.enforceInterface("android.telephony.ims.aidl.IImsSmsListener");
      param1Int2 = arrayOfByte.readInt();
      param1Int1 = arrayOfByte.readInt();
      int i = arrayOfByte.readInt();
      int j = arrayOfByte.readInt();
      int k = arrayOfByte.readInt();
      onSendSmsResult(param1Int2, param1Int1, i, j, k);
      return true;
    }
    
    private static class Proxy implements IImsSmsListener {
      public static IImsSmsListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.ims.aidl.IImsSmsListener";
      }
      
      public void onSendSmsResult(int param2Int1, int param2Int2, int param2Int3, int param2Int4, int param2Int5) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsSmsListener");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          parcel.writeInt(param2Int4);
          parcel.writeInt(param2Int5);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IImsSmsListener.Stub.getDefaultImpl() != null) {
            IImsSmsListener.Stub.getDefaultImpl().onSendSmsResult(param2Int1, param2Int2, param2Int3, param2Int4, param2Int5);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSmsStatusReportReceived(int param2Int, String param2String, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsSmsListener");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          parcel.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IImsSmsListener.Stub.getDefaultImpl() != null) {
            IImsSmsListener.Stub.getDefaultImpl().onSmsStatusReportReceived(param2Int, param2String, param2ArrayOfbyte);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSmsReceived(int param2Int, String param2String, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsSmsListener");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          parcel.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IImsSmsListener.Stub.getDefaultImpl() != null) {
            IImsSmsListener.Stub.getDefaultImpl().onSmsReceived(param2Int, param2String, param2ArrayOfbyte);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsSmsListener param1IImsSmsListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsSmsListener != null) {
          Proxy.sDefaultImpl = param1IImsSmsListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsSmsListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
