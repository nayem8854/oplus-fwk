package android.telephony.ims.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IImsCapabilityCallback extends IInterface {
  void onCapabilitiesStatusChanged(int paramInt) throws RemoteException;
  
  void onChangeCapabilityConfigurationError(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void onQueryCapabilityConfiguration(int paramInt1, int paramInt2, boolean paramBoolean) throws RemoteException;
  
  class Default implements IImsCapabilityCallback {
    public void onQueryCapabilityConfiguration(int param1Int1, int param1Int2, boolean param1Boolean) throws RemoteException {}
    
    public void onChangeCapabilityConfigurationError(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void onCapabilitiesStatusChanged(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsCapabilityCallback {
    private static final String DESCRIPTOR = "android.telephony.ims.aidl.IImsCapabilityCallback";
    
    static final int TRANSACTION_onCapabilitiesStatusChanged = 3;
    
    static final int TRANSACTION_onChangeCapabilityConfigurationError = 2;
    
    static final int TRANSACTION_onQueryCapabilityConfiguration = 1;
    
    public Stub() {
      attachInterface(this, "android.telephony.ims.aidl.IImsCapabilityCallback");
    }
    
    public static IImsCapabilityCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.ims.aidl.IImsCapabilityCallback");
      if (iInterface != null && iInterface instanceof IImsCapabilityCallback)
        return (IImsCapabilityCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onCapabilitiesStatusChanged";
        } 
        return "onChangeCapabilityConfigurationError";
      } 
      return "onQueryCapabilityConfiguration";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.telephony.ims.aidl.IImsCapabilityCallback");
            return true;
          } 
          param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsCapabilityCallback");
          param1Int1 = param1Parcel1.readInt();
          onCapabilitiesStatusChanged(param1Int1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsCapabilityCallback");
        int i = param1Parcel1.readInt();
        param1Int1 = param1Parcel1.readInt();
        param1Int2 = param1Parcel1.readInt();
        onChangeCapabilityConfigurationError(i, param1Int1, param1Int2);
        return true;
      } 
      param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsCapabilityCallback");
      param1Int2 = param1Parcel1.readInt();
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      onQueryCapabilityConfiguration(param1Int2, param1Int1, bool);
      return true;
    }
    
    private static class Proxy implements IImsCapabilityCallback {
      public static IImsCapabilityCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.ims.aidl.IImsCapabilityCallback";
      }
      
      public void onQueryCapabilityConfiguration(int param2Int1, int param2Int2, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCapabilityCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && IImsCapabilityCallback.Stub.getDefaultImpl() != null) {
            IImsCapabilityCallback.Stub.getDefaultImpl().onQueryCapabilityConfiguration(param2Int1, param2Int2, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onChangeCapabilityConfigurationError(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCapabilityCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IImsCapabilityCallback.Stub.getDefaultImpl() != null) {
            IImsCapabilityCallback.Stub.getDefaultImpl().onChangeCapabilityConfigurationError(param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCapabilitiesStatusChanged(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsCapabilityCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IImsCapabilityCallback.Stub.getDefaultImpl() != null) {
            IImsCapabilityCallback.Stub.getDefaultImpl().onCapabilitiesStatusChanged(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsCapabilityCallback param1IImsCapabilityCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsCapabilityCallback != null) {
          Proxy.sDefaultImpl = param1IImsCapabilityCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsCapabilityCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
