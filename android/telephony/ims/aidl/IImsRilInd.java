package android.telephony.ims.aidl;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IImsRilInd extends IInterface {
  void onImsRilInd(int paramInt1, int paramInt2, Bundle paramBundle) throws RemoteException;
  
  class Default implements IImsRilInd {
    public void onImsRilInd(int param1Int1, int param1Int2, Bundle param1Bundle) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsRilInd {
    private static final String DESCRIPTOR = "android.telephony.ims.aidl.IImsRilInd";
    
    static final int TRANSACTION_onImsRilInd = 1;
    
    public Stub() {
      attachInterface(this, "android.telephony.ims.aidl.IImsRilInd");
    }
    
    public static IImsRilInd asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.ims.aidl.IImsRilInd");
      if (iInterface != null && iInterface instanceof IImsRilInd)
        return (IImsRilInd)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onImsRilInd";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.telephony.ims.aidl.IImsRilInd");
        return true;
      } 
      param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsRilInd");
      param1Int1 = param1Parcel1.readInt();
      param1Int2 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onImsRilInd(param1Int1, param1Int2, (Bundle)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IImsRilInd {
      public static IImsRilInd sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.ims.aidl.IImsRilInd";
      }
      
      public void onImsRilInd(int param2Int1, int param2Int2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsRilInd");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IImsRilInd.Stub.getDefaultImpl() != null) {
            IImsRilInd.Stub.getDefaultImpl().onImsRilInd(param2Int1, param2Int2, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsRilInd param1IImsRilInd) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsRilInd != null) {
          Proxy.sDefaultImpl = param1IImsRilInd;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsRilInd getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
