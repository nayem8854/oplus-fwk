package android.telephony.ims.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.ims.stub.ImsFeatureConfiguration;
import com.android.ims.internal.IImsFeatureStatusCallback;

public interface IImsServiceController extends IInterface {
  IImsMmTelFeature createMmTelFeature(int paramInt, IImsFeatureStatusCallback paramIImsFeatureStatusCallback) throws RemoteException;
  
  IImsRcsFeature createRcsFeature(int paramInt, IImsFeatureStatusCallback paramIImsFeatureStatusCallback) throws RemoteException;
  
  void disableIms(int paramInt) throws RemoteException;
  
  void enableIms(int paramInt) throws RemoteException;
  
  IImsConfig getConfig(int paramInt) throws RemoteException;
  
  IImsRegistration getRegistration(int paramInt) throws RemoteException;
  
  void notifyImsServiceReadyForFeatureCreation() throws RemoteException;
  
  ImsFeatureConfiguration querySupportedImsFeatures() throws RemoteException;
  
  void removeImsFeature(int paramInt1, int paramInt2, IImsFeatureStatusCallback paramIImsFeatureStatusCallback) throws RemoteException;
  
  void setListener(IImsServiceControllerListener paramIImsServiceControllerListener) throws RemoteException;
  
  class Default implements IImsServiceController {
    public void setListener(IImsServiceControllerListener param1IImsServiceControllerListener) throws RemoteException {}
    
    public IImsMmTelFeature createMmTelFeature(int param1Int, IImsFeatureStatusCallback param1IImsFeatureStatusCallback) throws RemoteException {
      return null;
    }
    
    public IImsRcsFeature createRcsFeature(int param1Int, IImsFeatureStatusCallback param1IImsFeatureStatusCallback) throws RemoteException {
      return null;
    }
    
    public ImsFeatureConfiguration querySupportedImsFeatures() throws RemoteException {
      return null;
    }
    
    public void notifyImsServiceReadyForFeatureCreation() throws RemoteException {}
    
    public void removeImsFeature(int param1Int1, int param1Int2, IImsFeatureStatusCallback param1IImsFeatureStatusCallback) throws RemoteException {}
    
    public IImsConfig getConfig(int param1Int) throws RemoteException {
      return null;
    }
    
    public IImsRegistration getRegistration(int param1Int) throws RemoteException {
      return null;
    }
    
    public void enableIms(int param1Int) throws RemoteException {}
    
    public void disableIms(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsServiceController {
    private static final String DESCRIPTOR = "android.telephony.ims.aidl.IImsServiceController";
    
    static final int TRANSACTION_createMmTelFeature = 2;
    
    static final int TRANSACTION_createRcsFeature = 3;
    
    static final int TRANSACTION_disableIms = 10;
    
    static final int TRANSACTION_enableIms = 9;
    
    static final int TRANSACTION_getConfig = 7;
    
    static final int TRANSACTION_getRegistration = 8;
    
    static final int TRANSACTION_notifyImsServiceReadyForFeatureCreation = 5;
    
    static final int TRANSACTION_querySupportedImsFeatures = 4;
    
    static final int TRANSACTION_removeImsFeature = 6;
    
    static final int TRANSACTION_setListener = 1;
    
    public Stub() {
      attachInterface(this, "android.telephony.ims.aidl.IImsServiceController");
    }
    
    public static IImsServiceController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.ims.aidl.IImsServiceController");
      if (iInterface != null && iInterface instanceof IImsServiceController)
        return (IImsServiceController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 10:
          return "disableIms";
        case 9:
          return "enableIms";
        case 8:
          return "getRegistration";
        case 7:
          return "getConfig";
        case 6:
          return "removeImsFeature";
        case 5:
          return "notifyImsServiceReadyForFeatureCreation";
        case 4:
          return "querySupportedImsFeatures";
        case 3:
          return "createRcsFeature";
        case 2:
          return "createMmTelFeature";
        case 1:
          break;
      } 
      return "setListener";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        IBinder iBinder3;
        IImsFeatureStatusCallback iImsFeatureStatusCallback3;
        ImsFeatureConfiguration imsFeatureConfiguration;
        IImsFeatureStatusCallback iImsFeatureStatusCallback2;
        IImsConfig iImsConfig1;
        IBinder iBinder2;
        IImsFeatureStatusCallback iImsFeatureStatusCallback1;
        IImsRcsFeature iImsRcsFeature1;
        IBinder iBinder1;
        IImsConfig iImsConfig2;
        IImsMmTelFeature iImsMmTelFeature;
        IBinder iBinder4 = null;
        IImsRegistration iImsRegistration = null;
        IImsRcsFeature iImsRcsFeature2 = null;
        Parcel parcel = null;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 10:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsServiceController");
            param1Int1 = param1Parcel1.readInt();
            disableIms(param1Int1);
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsServiceController");
            param1Int1 = param1Parcel1.readInt();
            enableIms(param1Int1);
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsServiceController");
            param1Int1 = param1Parcel1.readInt();
            iImsRegistration = getRegistration(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel1 = parcel;
            if (iImsRegistration != null)
              iBinder3 = iImsRegistration.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder3);
            return true;
          case 7:
            iBinder3.enforceInterface("android.telephony.ims.aidl.IImsServiceController");
            param1Int1 = iBinder3.readInt();
            iImsConfig2 = getConfig(param1Int1);
            param1Parcel2.writeNoException();
            iBinder3 = iBinder4;
            if (iImsConfig2 != null)
              iBinder3 = iImsConfig2.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder3);
            return true;
          case 6:
            iBinder3.enforceInterface("android.telephony.ims.aidl.IImsServiceController");
            param1Int1 = iBinder3.readInt();
            param1Int2 = iBinder3.readInt();
            iImsFeatureStatusCallback3 = IImsFeatureStatusCallback.Stub.asInterface(iBinder3.readStrongBinder());
            removeImsFeature(param1Int1, param1Int2, iImsFeatureStatusCallback3);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            iImsFeatureStatusCallback3.enforceInterface("android.telephony.ims.aidl.IImsServiceController");
            notifyImsServiceReadyForFeatureCreation();
            param1Parcel2.writeNoException();
            return true;
          case 4:
            iImsFeatureStatusCallback3.enforceInterface("android.telephony.ims.aidl.IImsServiceController");
            imsFeatureConfiguration = querySupportedImsFeatures();
            param1Parcel2.writeNoException();
            if (imsFeatureConfiguration != null) {
              param1Parcel2.writeInt(1);
              imsFeatureConfiguration.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            imsFeatureConfiguration.enforceInterface("android.telephony.ims.aidl.IImsServiceController");
            param1Int1 = imsFeatureConfiguration.readInt();
            iImsFeatureStatusCallback2 = IImsFeatureStatusCallback.Stub.asInterface(imsFeatureConfiguration.readStrongBinder());
            iImsRcsFeature2 = createRcsFeature(param1Int1, iImsFeatureStatusCallback2);
            param1Parcel2.writeNoException();
            iImsConfig1 = iImsConfig2;
            if (iImsRcsFeature2 != null)
              iBinder2 = iImsRcsFeature2.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder2);
            return true;
          case 2:
            iBinder2.enforceInterface("android.telephony.ims.aidl.IImsServiceController");
            param1Int1 = iBinder2.readInt();
            iImsFeatureStatusCallback1 = IImsFeatureStatusCallback.Stub.asInterface(iBinder2.readStrongBinder());
            iImsMmTelFeature = createMmTelFeature(param1Int1, iImsFeatureStatusCallback1);
            param1Parcel2.writeNoException();
            iImsRcsFeature1 = iImsRcsFeature2;
            if (iImsMmTelFeature != null)
              iBinder1 = iImsMmTelFeature.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder1);
            return true;
          case 1:
            break;
        } 
        iBinder1.enforceInterface("android.telephony.ims.aidl.IImsServiceController");
        IImsServiceControllerListener iImsServiceControllerListener = IImsServiceControllerListener.Stub.asInterface(iBinder1.readStrongBinder());
        setListener(iImsServiceControllerListener);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.telephony.ims.aidl.IImsServiceController");
      return true;
    }
    
    private static class Proxy implements IImsServiceController {
      public static IImsServiceController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.ims.aidl.IImsServiceController";
      }
      
      public void setListener(IImsServiceControllerListener param2IImsServiceControllerListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsServiceController");
          if (param2IImsServiceControllerListener != null) {
            iBinder = param2IImsServiceControllerListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IImsServiceController.Stub.getDefaultImpl() != null) {
            IImsServiceController.Stub.getDefaultImpl().setListener(param2IImsServiceControllerListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsMmTelFeature createMmTelFeature(int param2Int, IImsFeatureStatusCallback param2IImsFeatureStatusCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsServiceController");
          parcel1.writeInt(param2Int);
          if (param2IImsFeatureStatusCallback != null) {
            iBinder = param2IImsFeatureStatusCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IImsServiceController.Stub.getDefaultImpl() != null)
            return IImsServiceController.Stub.getDefaultImpl().createMmTelFeature(param2Int, param2IImsFeatureStatusCallback); 
          parcel2.readException();
          return IImsMmTelFeature.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsRcsFeature createRcsFeature(int param2Int, IImsFeatureStatusCallback param2IImsFeatureStatusCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsServiceController");
          parcel1.writeInt(param2Int);
          if (param2IImsFeatureStatusCallback != null) {
            iBinder = param2IImsFeatureStatusCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IImsServiceController.Stub.getDefaultImpl() != null)
            return IImsServiceController.Stub.getDefaultImpl().createRcsFeature(param2Int, param2IImsFeatureStatusCallback); 
          parcel2.readException();
          return IImsRcsFeature.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ImsFeatureConfiguration querySupportedImsFeatures() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ImsFeatureConfiguration imsFeatureConfiguration;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsServiceController");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IImsServiceController.Stub.getDefaultImpl() != null) {
            imsFeatureConfiguration = IImsServiceController.Stub.getDefaultImpl().querySupportedImsFeatures();
            return imsFeatureConfiguration;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            imsFeatureConfiguration = (ImsFeatureConfiguration)ImsFeatureConfiguration.CREATOR.createFromParcel(parcel2);
          } else {
            imsFeatureConfiguration = null;
          } 
          return imsFeatureConfiguration;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyImsServiceReadyForFeatureCreation() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsServiceController");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IImsServiceController.Stub.getDefaultImpl() != null) {
            IImsServiceController.Stub.getDefaultImpl().notifyImsServiceReadyForFeatureCreation();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeImsFeature(int param2Int1, int param2Int2, IImsFeatureStatusCallback param2IImsFeatureStatusCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsServiceController");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2IImsFeatureStatusCallback != null) {
            iBinder = param2IImsFeatureStatusCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IImsServiceController.Stub.getDefaultImpl() != null) {
            IImsServiceController.Stub.getDefaultImpl().removeImsFeature(param2Int1, param2Int2, param2IImsFeatureStatusCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsConfig getConfig(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsServiceController");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IImsServiceController.Stub.getDefaultImpl() != null)
            return IImsServiceController.Stub.getDefaultImpl().getConfig(param2Int); 
          parcel2.readException();
          return IImsConfig.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsRegistration getRegistration(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsServiceController");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IImsServiceController.Stub.getDefaultImpl() != null)
            return IImsServiceController.Stub.getDefaultImpl().getRegistration(param2Int); 
          parcel2.readException();
          return IImsRegistration.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enableIms(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsServiceController");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IImsServiceController.Stub.getDefaultImpl() != null) {
            IImsServiceController.Stub.getDefaultImpl().enableIms(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void disableIms(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsServiceController");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IImsServiceController.Stub.getDefaultImpl() != null) {
            IImsServiceController.Stub.getDefaultImpl().disableIms(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsServiceController param1IImsServiceController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsServiceController != null) {
          Proxy.sDefaultImpl = param1IImsServiceController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsServiceController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
