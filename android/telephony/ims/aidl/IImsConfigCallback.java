package android.telephony.ims.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IImsConfigCallback extends IInterface {
  void onIntConfigChanged(int paramInt1, int paramInt2) throws RemoteException;
  
  void onStringConfigChanged(int paramInt, String paramString) throws RemoteException;
  
  class Default implements IImsConfigCallback {
    public void onIntConfigChanged(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onStringConfigChanged(int param1Int, String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsConfigCallback {
    private static final String DESCRIPTOR = "android.telephony.ims.aidl.IImsConfigCallback";
    
    static final int TRANSACTION_onIntConfigChanged = 1;
    
    static final int TRANSACTION_onStringConfigChanged = 2;
    
    public Stub() {
      attachInterface(this, "android.telephony.ims.aidl.IImsConfigCallback");
    }
    
    public static IImsConfigCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.ims.aidl.IImsConfigCallback");
      if (iInterface != null && iInterface instanceof IImsConfigCallback)
        return (IImsConfigCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onStringConfigChanged";
      } 
      return "onIntConfigChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.telephony.ims.aidl.IImsConfigCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsConfigCallback");
        param1Int1 = param1Parcel1.readInt();
        str = param1Parcel1.readString();
        onStringConfigChanged(param1Int1, str);
        return true;
      } 
      str.enforceInterface("android.telephony.ims.aidl.IImsConfigCallback");
      param1Int1 = str.readInt();
      param1Int2 = str.readInt();
      onIntConfigChanged(param1Int1, param1Int2);
      return true;
    }
    
    private static class Proxy implements IImsConfigCallback {
      public static IImsConfigCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.ims.aidl.IImsConfigCallback";
      }
      
      public void onIntConfigChanged(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsConfigCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IImsConfigCallback.Stub.getDefaultImpl() != null) {
            IImsConfigCallback.Stub.getDefaultImpl().onIntConfigChanged(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onStringConfigChanged(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsConfigCallback");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IImsConfigCallback.Stub.getDefaultImpl() != null) {
            IImsConfigCallback.Stub.getDefaultImpl().onStringConfigChanged(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsConfigCallback param1IImsConfigCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsConfigCallback != null) {
          Proxy.sDefaultImpl = param1IImsConfigCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsConfigCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
