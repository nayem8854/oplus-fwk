package android.telephony.ims.aidl;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.ims.RcsContactUceCapability;
import android.telephony.ims.feature.CapabilityChangeRequest;
import java.util.ArrayList;
import java.util.List;

public interface IImsRcsFeature extends IInterface {
  void addCapabilityCallback(IImsCapabilityCallback paramIImsCapabilityCallback) throws RemoteException;
  
  void changeCapabilitiesConfiguration(CapabilityChangeRequest paramCapabilityChangeRequest, IImsCapabilityCallback paramIImsCapabilityCallback) throws RemoteException;
  
  int getFeatureState() throws RemoteException;
  
  void queryCapabilityConfiguration(int paramInt1, int paramInt2, IImsCapabilityCallback paramIImsCapabilityCallback) throws RemoteException;
  
  int queryCapabilityStatus() throws RemoteException;
  
  void removeCapabilityCallback(IImsCapabilityCallback paramIImsCapabilityCallback) throws RemoteException;
  
  void requestCapabilities(List<Uri> paramList, int paramInt) throws RemoteException;
  
  void respondToCapabilityRequest(String paramString, RcsContactUceCapability paramRcsContactUceCapability, int paramInt) throws RemoteException;
  
  void respondToCapabilityRequestWithError(Uri paramUri, int paramInt1, String paramString, int paramInt2) throws RemoteException;
  
  void sendCapabilityRequest(Uri paramUri, RcsContactUceCapability paramRcsContactUceCapability, int paramInt) throws RemoteException;
  
  void setListener(IRcsFeatureListener paramIRcsFeatureListener) throws RemoteException;
  
  void updateCapabilities(RcsContactUceCapability paramRcsContactUceCapability, int paramInt) throws RemoteException;
  
  class Default implements IImsRcsFeature {
    public void setListener(IRcsFeatureListener param1IRcsFeatureListener) throws RemoteException {}
    
    public int queryCapabilityStatus() throws RemoteException {
      return 0;
    }
    
    public int getFeatureState() throws RemoteException {
      return 0;
    }
    
    public void addCapabilityCallback(IImsCapabilityCallback param1IImsCapabilityCallback) throws RemoteException {}
    
    public void removeCapabilityCallback(IImsCapabilityCallback param1IImsCapabilityCallback) throws RemoteException {}
    
    public void changeCapabilitiesConfiguration(CapabilityChangeRequest param1CapabilityChangeRequest, IImsCapabilityCallback param1IImsCapabilityCallback) throws RemoteException {}
    
    public void queryCapabilityConfiguration(int param1Int1, int param1Int2, IImsCapabilityCallback param1IImsCapabilityCallback) throws RemoteException {}
    
    public void requestCapabilities(List<Uri> param1List, int param1Int) throws RemoteException {}
    
    public void updateCapabilities(RcsContactUceCapability param1RcsContactUceCapability, int param1Int) throws RemoteException {}
    
    public void sendCapabilityRequest(Uri param1Uri, RcsContactUceCapability param1RcsContactUceCapability, int param1Int) throws RemoteException {}
    
    public void respondToCapabilityRequest(String param1String, RcsContactUceCapability param1RcsContactUceCapability, int param1Int) throws RemoteException {}
    
    public void respondToCapabilityRequestWithError(Uri param1Uri, int param1Int1, String param1String, int param1Int2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsRcsFeature {
    private static final String DESCRIPTOR = "android.telephony.ims.aidl.IImsRcsFeature";
    
    static final int TRANSACTION_addCapabilityCallback = 4;
    
    static final int TRANSACTION_changeCapabilitiesConfiguration = 6;
    
    static final int TRANSACTION_getFeatureState = 3;
    
    static final int TRANSACTION_queryCapabilityConfiguration = 7;
    
    static final int TRANSACTION_queryCapabilityStatus = 2;
    
    static final int TRANSACTION_removeCapabilityCallback = 5;
    
    static final int TRANSACTION_requestCapabilities = 8;
    
    static final int TRANSACTION_respondToCapabilityRequest = 11;
    
    static final int TRANSACTION_respondToCapabilityRequestWithError = 12;
    
    static final int TRANSACTION_sendCapabilityRequest = 10;
    
    static final int TRANSACTION_setListener = 1;
    
    static final int TRANSACTION_updateCapabilities = 9;
    
    public Stub() {
      attachInterface(this, "android.telephony.ims.aidl.IImsRcsFeature");
    }
    
    public static IImsRcsFeature asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.ims.aidl.IImsRcsFeature");
      if (iInterface != null && iInterface instanceof IImsRcsFeature)
        return (IImsRcsFeature)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 12:
          return "respondToCapabilityRequestWithError";
        case 11:
          return "respondToCapabilityRequest";
        case 10:
          return "sendCapabilityRequest";
        case 9:
          return "updateCapabilities";
        case 8:
          return "requestCapabilities";
        case 7:
          return "queryCapabilityConfiguration";
        case 6:
          return "changeCapabilitiesConfiguration";
        case 5:
          return "removeCapabilityCallback";
        case 4:
          return "addCapabilityCallback";
        case 3:
          return "getFeatureState";
        case 2:
          return "queryCapabilityStatus";
        case 1:
          break;
      } 
      return "setListener";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ArrayList<Uri> arrayList;
      if (param1Int1 != 1598968902) {
        IImsCapabilityCallback iImsCapabilityCallback;
        String str;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 12:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsRcsFeature");
            if (param1Parcel1.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int2 = param1Parcel1.readInt();
            str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            respondToCapabilityRequestWithError((Uri)param1Parcel2, param1Int2, str, param1Int1);
            return true;
          case 11:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsRcsFeature");
            str = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              RcsContactUceCapability rcsContactUceCapability = (RcsContactUceCapability)RcsContactUceCapability.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            respondToCapabilityRequest(str, (RcsContactUceCapability)param1Parcel2, param1Int1);
            return true;
          case 10:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsRcsFeature");
            if (param1Parcel1.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              RcsContactUceCapability rcsContactUceCapability = (RcsContactUceCapability)RcsContactUceCapability.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            sendCapabilityRequest((Uri)param1Parcel2, (RcsContactUceCapability)str, param1Int1);
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsRcsFeature");
            if (param1Parcel1.readInt() != 0) {
              RcsContactUceCapability rcsContactUceCapability = (RcsContactUceCapability)RcsContactUceCapability.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            updateCapabilities((RcsContactUceCapability)param1Parcel2, param1Int1);
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsRcsFeature");
            arrayList = param1Parcel1.createTypedArrayList(Uri.CREATOR);
            param1Int1 = param1Parcel1.readInt();
            requestCapabilities(arrayList, param1Int1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsRcsFeature");
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            iImsCapabilityCallback = IImsCapabilityCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            queryCapabilityConfiguration(param1Int1, param1Int2, iImsCapabilityCallback);
            return true;
          case 6:
            iImsCapabilityCallback.enforceInterface("android.telephony.ims.aidl.IImsRcsFeature");
            if (iImsCapabilityCallback.readInt() != 0) {
              CapabilityChangeRequest capabilityChangeRequest = (CapabilityChangeRequest)CapabilityChangeRequest.CREATOR.createFromParcel((Parcel)iImsCapabilityCallback);
            } else {
              arrayList = null;
            } 
            iImsCapabilityCallback = IImsCapabilityCallback.Stub.asInterface(iImsCapabilityCallback.readStrongBinder());
            changeCapabilitiesConfiguration((CapabilityChangeRequest)arrayList, iImsCapabilityCallback);
            return true;
          case 5:
            iImsCapabilityCallback.enforceInterface("android.telephony.ims.aidl.IImsRcsFeature");
            iImsCapabilityCallback = IImsCapabilityCallback.Stub.asInterface(iImsCapabilityCallback.readStrongBinder());
            removeCapabilityCallback(iImsCapabilityCallback);
            return true;
          case 4:
            iImsCapabilityCallback.enforceInterface("android.telephony.ims.aidl.IImsRcsFeature");
            iImsCapabilityCallback = IImsCapabilityCallback.Stub.asInterface(iImsCapabilityCallback.readStrongBinder());
            addCapabilityCallback(iImsCapabilityCallback);
            return true;
          case 3:
            iImsCapabilityCallback.enforceInterface("android.telephony.ims.aidl.IImsRcsFeature");
            param1Int1 = getFeatureState();
            arrayList.writeNoException();
            arrayList.writeInt(param1Int1);
            return true;
          case 2:
            iImsCapabilityCallback.enforceInterface("android.telephony.ims.aidl.IImsRcsFeature");
            param1Int1 = queryCapabilityStatus();
            arrayList.writeNoException();
            arrayList.writeInt(param1Int1);
            return true;
          case 1:
            break;
        } 
        iImsCapabilityCallback.enforceInterface("android.telephony.ims.aidl.IImsRcsFeature");
        IRcsFeatureListener iRcsFeatureListener = IRcsFeatureListener.Stub.asInterface(iImsCapabilityCallback.readStrongBinder());
        setListener(iRcsFeatureListener);
        arrayList.writeNoException();
        return true;
      } 
      arrayList.writeString("android.telephony.ims.aidl.IImsRcsFeature");
      return true;
    }
    
    private static class Proxy implements IImsRcsFeature {
      public static IImsRcsFeature sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.ims.aidl.IImsRcsFeature";
      }
      
      public void setListener(IRcsFeatureListener param2IRcsFeatureListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsFeature");
          if (param2IRcsFeatureListener != null) {
            iBinder = param2IRcsFeatureListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IImsRcsFeature.Stub.getDefaultImpl() != null) {
            IImsRcsFeature.Stub.getDefaultImpl().setListener(param2IRcsFeatureListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int queryCapabilityStatus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsFeature");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IImsRcsFeature.Stub.getDefaultImpl() != null)
            return IImsRcsFeature.Stub.getDefaultImpl().queryCapabilityStatus(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getFeatureState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsFeature");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IImsRcsFeature.Stub.getDefaultImpl() != null)
            return IImsRcsFeature.Stub.getDefaultImpl().getFeatureState(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addCapabilityCallback(IImsCapabilityCallback param2IImsCapabilityCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsFeature");
          if (param2IImsCapabilityCallback != null) {
            iBinder = param2IImsCapabilityCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IImsRcsFeature.Stub.getDefaultImpl() != null) {
            IImsRcsFeature.Stub.getDefaultImpl().addCapabilityCallback(param2IImsCapabilityCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeCapabilityCallback(IImsCapabilityCallback param2IImsCapabilityCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsFeature");
          if (param2IImsCapabilityCallback != null) {
            iBinder = param2IImsCapabilityCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IImsRcsFeature.Stub.getDefaultImpl() != null) {
            IImsRcsFeature.Stub.getDefaultImpl().removeCapabilityCallback(param2IImsCapabilityCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void changeCapabilitiesConfiguration(CapabilityChangeRequest param2CapabilityChangeRequest, IImsCapabilityCallback param2IImsCapabilityCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsFeature");
          if (param2CapabilityChangeRequest != null) {
            parcel.writeInt(1);
            param2CapabilityChangeRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IImsCapabilityCallback != null) {
            iBinder = param2IImsCapabilityCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IImsRcsFeature.Stub.getDefaultImpl() != null) {
            IImsRcsFeature.Stub.getDefaultImpl().changeCapabilitiesConfiguration(param2CapabilityChangeRequest, param2IImsCapabilityCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void queryCapabilityConfiguration(int param2Int1, int param2Int2, IImsCapabilityCallback param2IImsCapabilityCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsFeature");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2IImsCapabilityCallback != null) {
            iBinder = param2IImsCapabilityCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IImsRcsFeature.Stub.getDefaultImpl() != null) {
            IImsRcsFeature.Stub.getDefaultImpl().queryCapabilityConfiguration(param2Int1, param2Int2, param2IImsCapabilityCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestCapabilities(List<Uri> param2List, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsFeature");
          parcel.writeTypedList(param2List);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IImsRcsFeature.Stub.getDefaultImpl() != null) {
            IImsRcsFeature.Stub.getDefaultImpl().requestCapabilities(param2List, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void updateCapabilities(RcsContactUceCapability param2RcsContactUceCapability, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsFeature");
          if (param2RcsContactUceCapability != null) {
            parcel.writeInt(1);
            param2RcsContactUceCapability.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IImsRcsFeature.Stub.getDefaultImpl() != null) {
            IImsRcsFeature.Stub.getDefaultImpl().updateCapabilities(param2RcsContactUceCapability, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendCapabilityRequest(Uri param2Uri, RcsContactUceCapability param2RcsContactUceCapability, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsFeature");
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2RcsContactUceCapability != null) {
            parcel.writeInt(1);
            param2RcsContactUceCapability.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IImsRcsFeature.Stub.getDefaultImpl() != null) {
            IImsRcsFeature.Stub.getDefaultImpl().sendCapabilityRequest(param2Uri, param2RcsContactUceCapability, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void respondToCapabilityRequest(String param2String, RcsContactUceCapability param2RcsContactUceCapability, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsFeature");
          parcel.writeString(param2String);
          if (param2RcsContactUceCapability != null) {
            parcel.writeInt(1);
            param2RcsContactUceCapability.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IImsRcsFeature.Stub.getDefaultImpl() != null) {
            IImsRcsFeature.Stub.getDefaultImpl().respondToCapabilityRequest(param2String, param2RcsContactUceCapability, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void respondToCapabilityRequestWithError(Uri param2Uri, int param2Int1, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsFeature");
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int1);
          parcel.writeString(param2String);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(12, parcel, null, 1);
          if (!bool && IImsRcsFeature.Stub.getDefaultImpl() != null) {
            IImsRcsFeature.Stub.getDefaultImpl().respondToCapabilityRequestWithError(param2Uri, param2Int1, param2String, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsRcsFeature param1IImsRcsFeature) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsRcsFeature != null) {
          Proxy.sDefaultImpl = param1IImsRcsFeature;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsRcsFeature getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
