package android.telephony.ims.aidl;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.android.internal.telephony.IIntegerConsumer;
import java.util.ArrayList;
import java.util.List;

public interface IImsRcsController extends IInterface {
  void getImsRcsRegistrationState(int paramInt, IIntegerConsumer paramIIntegerConsumer) throws RemoteException;
  
  void getImsRcsRegistrationTransportType(int paramInt, IIntegerConsumer paramIIntegerConsumer) throws RemoteException;
  
  int getUcePublishState(int paramInt) throws RemoteException;
  
  boolean isAvailable(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean isCapable(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  boolean isUceSettingEnabled(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  void registerImsRegistrationCallback(int paramInt, IImsRegistrationCallback paramIImsRegistrationCallback) throws RemoteException;
  
  void registerRcsAvailabilityCallback(int paramInt, IImsCapabilityCallback paramIImsCapabilityCallback) throws RemoteException;
  
  void requestCapabilities(int paramInt, String paramString1, String paramString2, List<Uri> paramList, IRcsUceControllerCallback paramIRcsUceControllerCallback) throws RemoteException;
  
  void setUceSettingEnabled(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void unregisterImsRegistrationCallback(int paramInt, IImsRegistrationCallback paramIImsRegistrationCallback) throws RemoteException;
  
  void unregisterRcsAvailabilityCallback(int paramInt, IImsCapabilityCallback paramIImsCapabilityCallback) throws RemoteException;
  
  class Default implements IImsRcsController {
    public void registerImsRegistrationCallback(int param1Int, IImsRegistrationCallback param1IImsRegistrationCallback) throws RemoteException {}
    
    public void unregisterImsRegistrationCallback(int param1Int, IImsRegistrationCallback param1IImsRegistrationCallback) throws RemoteException {}
    
    public void getImsRcsRegistrationState(int param1Int, IIntegerConsumer param1IIntegerConsumer) throws RemoteException {}
    
    public void getImsRcsRegistrationTransportType(int param1Int, IIntegerConsumer param1IIntegerConsumer) throws RemoteException {}
    
    public void registerRcsAvailabilityCallback(int param1Int, IImsCapabilityCallback param1IImsCapabilityCallback) throws RemoteException {}
    
    public void unregisterRcsAvailabilityCallback(int param1Int, IImsCapabilityCallback param1IImsCapabilityCallback) throws RemoteException {}
    
    public boolean isCapable(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return false;
    }
    
    public boolean isAvailable(int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public void requestCapabilities(int param1Int, String param1String1, String param1String2, List<Uri> param1List, IRcsUceControllerCallback param1IRcsUceControllerCallback) throws RemoteException {}
    
    public int getUcePublishState(int param1Int) throws RemoteException {
      return 0;
    }
    
    public boolean isUceSettingEnabled(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public void setUceSettingEnabled(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsRcsController {
    private static final String DESCRIPTOR = "android.telephony.ims.aidl.IImsRcsController";
    
    static final int TRANSACTION_getImsRcsRegistrationState = 3;
    
    static final int TRANSACTION_getImsRcsRegistrationTransportType = 4;
    
    static final int TRANSACTION_getUcePublishState = 10;
    
    static final int TRANSACTION_isAvailable = 8;
    
    static final int TRANSACTION_isCapable = 7;
    
    static final int TRANSACTION_isUceSettingEnabled = 11;
    
    static final int TRANSACTION_registerImsRegistrationCallback = 1;
    
    static final int TRANSACTION_registerRcsAvailabilityCallback = 5;
    
    static final int TRANSACTION_requestCapabilities = 9;
    
    static final int TRANSACTION_setUceSettingEnabled = 12;
    
    static final int TRANSACTION_unregisterImsRegistrationCallback = 2;
    
    static final int TRANSACTION_unregisterRcsAvailabilityCallback = 6;
    
    public Stub() {
      attachInterface(this, "android.telephony.ims.aidl.IImsRcsController");
    }
    
    public static IImsRcsController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.ims.aidl.IImsRcsController");
      if (iInterface != null && iInterface instanceof IImsRcsController)
        return (IImsRcsController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 12:
          return "setUceSettingEnabled";
        case 11:
          return "isUceSettingEnabled";
        case 10:
          return "getUcePublishState";
        case 9:
          return "requestCapabilities";
        case 8:
          return "isAvailable";
        case 7:
          return "isCapable";
        case 6:
          return "unregisterRcsAvailabilityCallback";
        case 5:
          return "registerRcsAvailabilityCallback";
        case 4:
          return "getImsRcsRegistrationTransportType";
        case 3:
          return "getImsRcsRegistrationState";
        case 2:
          return "unregisterImsRegistrationCallback";
        case 1:
          break;
      } 
      return "registerImsRegistrationCallback";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        String str1;
        IRcsUceControllerCallback iRcsUceControllerCallback;
        IImsCapabilityCallback iImsCapabilityCallback;
        IIntegerConsumer iIntegerConsumer;
        boolean bool;
        String str2, str3;
        ArrayList<Uri> arrayList;
        int m;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 12:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsRcsController");
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            setUceSettingEnabled(param1Int1, bool);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            param1Parcel1.enforceInterface("android.telephony.ims.aidl.IImsRcsController");
            param1Int1 = param1Parcel1.readInt();
            str2 = param1Parcel1.readString();
            str1 = param1Parcel1.readString();
            bool3 = isUceSettingEnabled(param1Int1, str2, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 10:
            str1.enforceInterface("android.telephony.ims.aidl.IImsRcsController");
            k = str1.readInt();
            k = getUcePublishState(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 9:
            str1.enforceInterface("android.telephony.ims.aidl.IImsRcsController");
            k = str1.readInt();
            str2 = str1.readString();
            str3 = str1.readString();
            arrayList = str1.createTypedArrayList(Uri.CREATOR);
            iRcsUceControllerCallback = IRcsUceControllerCallback.Stub.asInterface(str1.readStrongBinder());
            requestCapabilities(k, str2, str3, arrayList, iRcsUceControllerCallback);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            iRcsUceControllerCallback.enforceInterface("android.telephony.ims.aidl.IImsRcsController");
            param1Int2 = iRcsUceControllerCallback.readInt();
            k = iRcsUceControllerCallback.readInt();
            bool2 = isAvailable(param1Int2, k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 7:
            iRcsUceControllerCallback.enforceInterface("android.telephony.ims.aidl.IImsRcsController");
            m = iRcsUceControllerCallback.readInt();
            j = iRcsUceControllerCallback.readInt();
            param1Int2 = iRcsUceControllerCallback.readInt();
            bool1 = isCapable(m, j, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 6:
            iRcsUceControllerCallback.enforceInterface("android.telephony.ims.aidl.IImsRcsController");
            i = iRcsUceControllerCallback.readInt();
            iImsCapabilityCallback = IImsCapabilityCallback.Stub.asInterface(iRcsUceControllerCallback.readStrongBinder());
            unregisterRcsAvailabilityCallback(i, iImsCapabilityCallback);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            iImsCapabilityCallback.enforceInterface("android.telephony.ims.aidl.IImsRcsController");
            i = iImsCapabilityCallback.readInt();
            iImsCapabilityCallback = IImsCapabilityCallback.Stub.asInterface(iImsCapabilityCallback.readStrongBinder());
            registerRcsAvailabilityCallback(i, iImsCapabilityCallback);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            iImsCapabilityCallback.enforceInterface("android.telephony.ims.aidl.IImsRcsController");
            i = iImsCapabilityCallback.readInt();
            iIntegerConsumer = IIntegerConsumer.Stub.asInterface(iImsCapabilityCallback.readStrongBinder());
            getImsRcsRegistrationTransportType(i, iIntegerConsumer);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            iIntegerConsumer.enforceInterface("android.telephony.ims.aidl.IImsRcsController");
            i = iIntegerConsumer.readInt();
            iIntegerConsumer = IIntegerConsumer.Stub.asInterface(iIntegerConsumer.readStrongBinder());
            getImsRcsRegistrationState(i, iIntegerConsumer);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            iIntegerConsumer.enforceInterface("android.telephony.ims.aidl.IImsRcsController");
            i = iIntegerConsumer.readInt();
            iImsRegistrationCallback = IImsRegistrationCallback.Stub.asInterface(iIntegerConsumer.readStrongBinder());
            unregisterImsRegistrationCallback(i, iImsRegistrationCallback);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        iImsRegistrationCallback.enforceInterface("android.telephony.ims.aidl.IImsRcsController");
        int i = iImsRegistrationCallback.readInt();
        IImsRegistrationCallback iImsRegistrationCallback = IImsRegistrationCallback.Stub.asInterface(iImsRegistrationCallback.readStrongBinder());
        registerImsRegistrationCallback(i, iImsRegistrationCallback);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.telephony.ims.aidl.IImsRcsController");
      return true;
    }
    
    private static class Proxy implements IImsRcsController {
      public static IImsRcsController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.ims.aidl.IImsRcsController";
      }
      
      public void registerImsRegistrationCallback(int param2Int, IImsRegistrationCallback param2IImsRegistrationCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsController");
          parcel1.writeInt(param2Int);
          if (param2IImsRegistrationCallback != null) {
            iBinder = param2IImsRegistrationCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IImsRcsController.Stub.getDefaultImpl() != null) {
            IImsRcsController.Stub.getDefaultImpl().registerImsRegistrationCallback(param2Int, param2IImsRegistrationCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterImsRegistrationCallback(int param2Int, IImsRegistrationCallback param2IImsRegistrationCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsController");
          parcel1.writeInt(param2Int);
          if (param2IImsRegistrationCallback != null) {
            iBinder = param2IImsRegistrationCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IImsRcsController.Stub.getDefaultImpl() != null) {
            IImsRcsController.Stub.getDefaultImpl().unregisterImsRegistrationCallback(param2Int, param2IImsRegistrationCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getImsRcsRegistrationState(int param2Int, IIntegerConsumer param2IIntegerConsumer) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsController");
          parcel1.writeInt(param2Int);
          if (param2IIntegerConsumer != null) {
            iBinder = param2IIntegerConsumer.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IImsRcsController.Stub.getDefaultImpl() != null) {
            IImsRcsController.Stub.getDefaultImpl().getImsRcsRegistrationState(param2Int, param2IIntegerConsumer);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getImsRcsRegistrationTransportType(int param2Int, IIntegerConsumer param2IIntegerConsumer) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsController");
          parcel1.writeInt(param2Int);
          if (param2IIntegerConsumer != null) {
            iBinder = param2IIntegerConsumer.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IImsRcsController.Stub.getDefaultImpl() != null) {
            IImsRcsController.Stub.getDefaultImpl().getImsRcsRegistrationTransportType(param2Int, param2IIntegerConsumer);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerRcsAvailabilityCallback(int param2Int, IImsCapabilityCallback param2IImsCapabilityCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsController");
          parcel1.writeInt(param2Int);
          if (param2IImsCapabilityCallback != null) {
            iBinder = param2IImsCapabilityCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IImsRcsController.Stub.getDefaultImpl() != null) {
            IImsRcsController.Stub.getDefaultImpl().registerRcsAvailabilityCallback(param2Int, param2IImsCapabilityCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterRcsAvailabilityCallback(int param2Int, IImsCapabilityCallback param2IImsCapabilityCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsController");
          parcel1.writeInt(param2Int);
          if (param2IImsCapabilityCallback != null) {
            iBinder = param2IImsCapabilityCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IImsRcsController.Stub.getDefaultImpl() != null) {
            IImsRcsController.Stub.getDefaultImpl().unregisterRcsAvailabilityCallback(param2Int, param2IImsCapabilityCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isCapable(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsController");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IImsRcsController.Stub.getDefaultImpl() != null) {
            bool1 = IImsRcsController.Stub.getDefaultImpl().isCapable(param2Int1, param2Int2, param2Int3);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAvailable(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsController");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(8, parcel1, parcel2, 0);
          if (!bool2 && IImsRcsController.Stub.getDefaultImpl() != null) {
            bool1 = IImsRcsController.Stub.getDefaultImpl().isAvailable(param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestCapabilities(int param2Int, String param2String1, String param2String2, List<Uri> param2List, IRcsUceControllerCallback param2IRcsUceControllerCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsController");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeTypedList(param2List);
          if (param2IRcsUceControllerCallback != null) {
            iBinder = param2IRcsUceControllerCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IImsRcsController.Stub.getDefaultImpl() != null) {
            IImsRcsController.Stub.getDefaultImpl().requestCapabilities(param2Int, param2String1, param2String2, param2List, param2IRcsUceControllerCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUcePublishState(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsController");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IImsRcsController.Stub.getDefaultImpl() != null) {
            param2Int = IImsRcsController.Stub.getDefaultImpl().getUcePublishState(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUceSettingEnabled(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsController");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(11, parcel1, parcel2, 0);
          if (!bool2 && IImsRcsController.Stub.getDefaultImpl() != null) {
            bool1 = IImsRcsController.Stub.getDefaultImpl().isUceSettingEnabled(param2Int, param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUceSettingEnabled(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.telephony.ims.aidl.IImsRcsController");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool1 && IImsRcsController.Stub.getDefaultImpl() != null) {
            IImsRcsController.Stub.getDefaultImpl().setUceSettingEnabled(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsRcsController param1IImsRcsController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsRcsController != null) {
          Proxy.sDefaultImpl = param1IImsRcsController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsRcsController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
