package android.telephony.ims;

import android.content.Context;
import android.telephony.SubscriptionManager;

public class ImsManager {
  public static final String ACTION_FORBIDDEN_NO_SERVICE_AUTHORIZATION = "com.android.internal.intent.action.ACTION_FORBIDDEN_NO_SERVICE_AUTHORIZATION";
  
  public static final String ACTION_WFC_IMS_REGISTRATION_ERROR = "android.telephony.ims.action.WFC_IMS_REGISTRATION_ERROR";
  
  public static final String EXTRA_WFC_REGISTRATION_FAILURE_MESSAGE = "android.telephony.ims.extra.WFC_REGISTRATION_FAILURE_MESSAGE";
  
  public static final String EXTRA_WFC_REGISTRATION_FAILURE_TITLE = "android.telephony.ims.extra.WFC_REGISTRATION_FAILURE_TITLE";
  
  private Context mContext;
  
  public ImsManager(Context paramContext) {
    this.mContext = paramContext;
  }
  
  public ImsRcsManager getImsRcsManager(int paramInt) {
    if (SubscriptionManager.isValidSubscriptionId(paramInt))
      return new ImsRcsManager(this.mContext, paramInt); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid subscription ID: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public ImsMmTelManager getImsMmTelManager(int paramInt) {
    if (SubscriptionManager.isValidSubscriptionId(paramInt))
      return new ImsMmTelManager(paramInt); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid subscription ID: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
}
