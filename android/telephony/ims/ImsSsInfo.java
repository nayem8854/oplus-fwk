package android.telephony.ims;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public final class ImsSsInfo implements Parcelable {
  public int mProvisionStatus = -1;
  
  private int mClirInterrogationStatus = 2;
  
  private int mClirOutgoingState = 0;
  
  @Retention(RetentionPolicy.SOURCE)
  class ServiceStatus implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ServiceProvisionStatus implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ClirOutgoingState implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ClirInterrogationStatus implements Annotation {}
  
  class Builder {
    private final ImsSsInfo mImsSsInfo;
    
    public Builder(ImsSsInfo this$0) {
      ImsSsInfo imsSsInfo = new ImsSsInfo();
      imsSsInfo.mStatus = this$0;
    }
    
    public Builder setIncomingCommunicationBarringNumber(String param1String) {
      this.mImsSsInfo.mIcbNum = param1String;
      return this;
    }
    
    public Builder setProvisionStatus(int param1Int) {
      this.mImsSsInfo.mProvisionStatus = param1Int;
      return this;
    }
    
    public Builder setClirInterrogationStatus(int param1Int) {
      ImsSsInfo.access$002(this.mImsSsInfo, param1Int);
      return this;
    }
    
    public Builder setClirOutgoingState(int param1Int) {
      ImsSsInfo.access$102(this.mImsSsInfo, param1Int);
      return this;
    }
    
    public ImsSsInfo build() {
      return this.mImsSsInfo;
    }
  }
  
  @Deprecated
  public ImsSsInfo(int paramInt, String paramString) {
    this.mStatus = paramInt;
    this.mIcbNum = paramString;
  }
  
  private ImsSsInfo(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mStatus);
    paramParcel.writeString(this.mIcbNum);
    paramParcel.writeInt(this.mProvisionStatus);
    paramParcel.writeInt(this.mClirInterrogationStatus);
    paramParcel.writeInt(this.mClirOutgoingState);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(super.toString());
    stringBuilder.append(", Status: ");
    if (this.mStatus == 0) {
      null = "disabled";
    } else {
      null = "enabled";
    } 
    stringBuilder.append(null);
    stringBuilder.append(", ProvisionStatus: ");
    int i = this.mProvisionStatus;
    stringBuilder.append(provisionStatusToString(i));
    return stringBuilder.toString();
  }
  
  private static String provisionStatusToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1)
        return "Service provisioning unknown"; 
      return "Service provisioned";
    } 
    return "Service not provisioned";
  }
  
  private void readFromParcel(Parcel paramParcel) {
    this.mStatus = paramParcel.readInt();
    this.mIcbNum = paramParcel.readString();
    this.mProvisionStatus = paramParcel.readInt();
    this.mClirInterrogationStatus = paramParcel.readInt();
    this.mClirOutgoingState = paramParcel.readInt();
  }
  
  public static final Parcelable.Creator<ImsSsInfo> CREATOR = new Parcelable.Creator<ImsSsInfo>() {
      public ImsSsInfo createFromParcel(Parcel param1Parcel) {
        return new ImsSsInfo(param1Parcel);
      }
      
      public ImsSsInfo[] newArray(int param1Int) {
        return new ImsSsInfo[param1Int];
      }
    };
  
  public static final int CLIR_OUTGOING_DEFAULT = 0;
  
  public static final int CLIR_OUTGOING_INVOCATION = 1;
  
  public static final int CLIR_OUTGOING_SUPPRESSION = 2;
  
  public static final int CLIR_STATUS_NOT_PROVISIONED = 0;
  
  public static final int CLIR_STATUS_PROVISIONED_PERMANENT = 1;
  
  public static final int CLIR_STATUS_TEMPORARILY_ALLOWED = 4;
  
  public static final int CLIR_STATUS_TEMPORARILY_RESTRICTED = 3;
  
  public static final int CLIR_STATUS_UNKNOWN = 2;
  
  public static final int DISABLED = 0;
  
  public static final int ENABLED = 1;
  
  public static final int NOT_REGISTERED = -1;
  
  public static final int SERVICE_NOT_PROVISIONED = 0;
  
  public static final int SERVICE_PROVISIONED = 1;
  
  public static final int SERVICE_PROVISIONING_UNKNOWN = -1;
  
  public String mIcbNum;
  
  public int mStatus;
  
  public int getStatus() {
    return this.mStatus;
  }
  
  @Deprecated
  public String getIcbNum() {
    return this.mIcbNum;
  }
  
  public String getIncomingCommunicationBarringNumber() {
    return this.mIcbNum;
  }
  
  public int getProvisionStatus() {
    return this.mProvisionStatus;
  }
  
  public int getClirOutgoingState() {
    return this.mClirOutgoingState;
  }
  
  public int getClirInterrogationStatus() {
    return this.mClirInterrogationStatus;
  }
  
  public int[] getCompatArray(int paramInt) {
    int[] arrayOfInt = new int[2];
    if (paramInt == 8) {
      arrayOfInt[0] = getClirOutgoingState();
      arrayOfInt[1] = getClirInterrogationStatus();
      return arrayOfInt;
    } 
    if (paramInt == 10)
      arrayOfInt[0] = getProvisionStatus(); 
    arrayOfInt[0] = getStatus();
    arrayOfInt[1] = getProvisionStatus();
    return arrayOfInt;
  }
  
  public ImsSsInfo() {}
}
