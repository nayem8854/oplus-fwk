package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public final class AvailableNetworkInfo implements Parcelable {
  public int getSubId() {
    return this.mSubId;
  }
  
  public int getPriority() {
    return this.mPriority;
  }
  
  public List<String> getMccMncs() {
    return (List<String>)this.mMccMncs.clone();
  }
  
  public List<Integer> getBands() {
    return (List<Integer>)this.mBands.clone();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mSubId);
    paramParcel.writeInt(this.mPriority);
    paramParcel.writeStringList(this.mMccMncs);
    paramParcel.writeList(this.mBands);
  }
  
  private AvailableNetworkInfo(Parcel paramParcel) {
    this.mSubId = paramParcel.readInt();
    this.mPriority = paramParcel.readInt();
    ArrayList<String> arrayList = new ArrayList();
    paramParcel.readStringList(arrayList);
    this.mBands = (ArrayList)(arrayList = new ArrayList<>());
    paramParcel.readList(arrayList, Integer.class.getClassLoader());
  }
  
  public AvailableNetworkInfo(int paramInt1, int paramInt2, List<String> paramList, List<Integer> paramList1) {
    this.mSubId = paramInt1;
    this.mPriority = paramInt2;
    this.mMccMncs = new ArrayList<>(paramList);
    this.mBands = new ArrayList<>(paramList1);
  }
  
  public boolean equals(Object<String> paramObject) {
    boolean bool = false;
    try {
      AvailableNetworkInfo availableNetworkInfo = (AvailableNetworkInfo)paramObject;
      if (paramObject == null)
        return false; 
      if (this.mSubId == availableNetworkInfo.mSubId && this.mPriority == availableNetworkInfo.mPriority) {
        paramObject = (Object<String>)this.mMccMncs;
        if (paramObject != null) {
          ArrayList<String> arrayList = availableNetworkInfo.mMccMncs;
          if (paramObject.equals(arrayList)) {
            ArrayList<Integer> arrayList1 = this.mBands, arrayList2 = availableNetworkInfo.mBands;
            if (arrayList1.equals(arrayList2))
              bool = true; 
          } 
        } 
      } 
      return bool;
    } catch (ClassCastException classCastException) {
      return false;
    } 
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mSubId), Integer.valueOf(this.mPriority), this.mMccMncs, this.mBands });
  }
  
  public static final Parcelable.Creator<AvailableNetworkInfo> CREATOR = new Parcelable.Creator<AvailableNetworkInfo>() {
      public AvailableNetworkInfo createFromParcel(Parcel param1Parcel) {
        return new AvailableNetworkInfo(param1Parcel);
      }
      
      public AvailableNetworkInfo[] newArray(int param1Int) {
        return new AvailableNetworkInfo[param1Int];
      }
    };
  
  public static final int PRIORITY_HIGH = 1;
  
  public static final int PRIORITY_LOW = 3;
  
  public static final int PRIORITY_MED = 2;
  
  private ArrayList<Integer> mBands;
  
  private ArrayList<String> mMccMncs;
  
  private int mPriority;
  
  private int mSubId;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("AvailableNetworkInfo: mSubId: ");
    stringBuilder.append(this.mSubId);
    stringBuilder.append(" mPriority: ");
    stringBuilder.append(this.mPriority);
    stringBuilder.append(" mMccMncs: ");
    ArrayList<String> arrayList1 = this.mMccMncs;
    stringBuilder.append(Arrays.toString(arrayList1.toArray()));
    stringBuilder.append(" mBands: ");
    ArrayList<Integer> arrayList = this.mBands;
    stringBuilder.append(Arrays.toString(arrayList.toArray()));
    return stringBuilder.toString();
  }
}
