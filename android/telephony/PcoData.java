package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;

public class PcoData implements Parcelable {
  public PcoData(int paramInt1, String paramString, int paramInt2, byte[] paramArrayOfbyte) {
    this.cid = paramInt1;
    this.bearerProto = paramString;
    this.pcoId = paramInt2;
    this.contents = paramArrayOfbyte;
  }
  
  public PcoData(Parcel paramParcel) {
    this.cid = paramParcel.readInt();
    this.bearerProto = paramParcel.readString();
    this.pcoId = paramParcel.readInt();
    this.contents = paramParcel.createByteArray();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.cid);
    paramParcel.writeString(this.bearerProto);
    paramParcel.writeInt(this.pcoId);
    paramParcel.writeByteArray(this.contents);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<PcoData> CREATOR = new Parcelable.Creator() {
      public PcoData createFromParcel(Parcel param1Parcel) {
        return new PcoData(param1Parcel);
      }
      
      public PcoData[] newArray(int param1Int) {
        return new PcoData[param1Int];
      }
    };
  
  public final String bearerProto;
  
  public final int cid;
  
  public final byte[] contents;
  
  public final int pcoId;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("PcoData(");
    stringBuilder.append(this.cid);
    stringBuilder.append(", ");
    stringBuilder.append(this.bearerProto);
    stringBuilder.append(", ");
    stringBuilder.append(this.pcoId);
    stringBuilder.append(", contents[");
    stringBuilder.append(this.contents.length);
    stringBuilder.append("])");
    return stringBuilder.toString();
  }
}
