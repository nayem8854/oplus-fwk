package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public class VoiceSpecificRegistrationInfo implements Parcelable {
  VoiceSpecificRegistrationInfo(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3) {
    this.cssSupported = paramBoolean;
    this.roamingIndicator = paramInt1;
    this.systemIsInPrl = paramInt2;
    this.defaultRoamingIndicator = paramInt3;
  }
  
  VoiceSpecificRegistrationInfo(VoiceSpecificRegistrationInfo paramVoiceSpecificRegistrationInfo) {
    this.cssSupported = paramVoiceSpecificRegistrationInfo.cssSupported;
    this.roamingIndicator = paramVoiceSpecificRegistrationInfo.roamingIndicator;
    this.systemIsInPrl = paramVoiceSpecificRegistrationInfo.systemIsInPrl;
    this.defaultRoamingIndicator = paramVoiceSpecificRegistrationInfo.defaultRoamingIndicator;
  }
  
  private VoiceSpecificRegistrationInfo(Parcel paramParcel) {
    this.cssSupported = paramParcel.readBoolean();
    this.roamingIndicator = paramParcel.readInt();
    this.systemIsInPrl = paramParcel.readInt();
    this.defaultRoamingIndicator = paramParcel.readInt();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeBoolean(this.cssSupported);
    paramParcel.writeInt(this.roamingIndicator);
    paramParcel.writeInt(this.systemIsInPrl);
    paramParcel.writeInt(this.defaultRoamingIndicator);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("VoiceSpecificRegistrationInfo { mCssSupported=");
    stringBuilder.append(this.cssSupported);
    stringBuilder.append(" mRoamingIndicator=");
    stringBuilder.append(this.roamingIndicator);
    stringBuilder.append(" mSystemIsInPrl=");
    stringBuilder.append(this.systemIsInPrl);
    stringBuilder.append(" mDefaultRoamingIndicator=");
    stringBuilder.append(this.defaultRoamingIndicator);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int hashCode() {
    boolean bool = this.cssSupported;
    int i = this.roamingIndicator, j = this.systemIsInPrl, k = this.defaultRoamingIndicator;
    return Objects.hash(new Object[] { Boolean.valueOf(bool), Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || !(paramObject instanceof VoiceSpecificRegistrationInfo))
      return false; 
    paramObject = paramObject;
    if (this.cssSupported != ((VoiceSpecificRegistrationInfo)paramObject).cssSupported || this.roamingIndicator != ((VoiceSpecificRegistrationInfo)paramObject).roamingIndicator || this.systemIsInPrl != ((VoiceSpecificRegistrationInfo)paramObject).systemIsInPrl || this.defaultRoamingIndicator != ((VoiceSpecificRegistrationInfo)paramObject).defaultRoamingIndicator)
      bool = false; 
    return bool;
  }
  
  public static final Parcelable.Creator<VoiceSpecificRegistrationInfo> CREATOR = new Parcelable.Creator<VoiceSpecificRegistrationInfo>() {
      public VoiceSpecificRegistrationInfo createFromParcel(Parcel param1Parcel) {
        return new VoiceSpecificRegistrationInfo(param1Parcel);
      }
      
      public VoiceSpecificRegistrationInfo[] newArray(int param1Int) {
        return new VoiceSpecificRegistrationInfo[param1Int];
      }
    };
  
  public final boolean cssSupported;
  
  public final int defaultRoamingIndicator;
  
  public final int roamingIndicator;
  
  public final int systemIsInPrl;
}
