package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Collections;
import java.util.List;

public final class VisualVoicemailSmsFilterSettings implements Parcelable {
  public static final Parcelable.Creator<VisualVoicemailSmsFilterSettings> CREATOR;
  
  public static final String DEFAULT_CLIENT_PREFIX = "//VVM";
  
  public static final int DEFAULT_DESTINATION_PORT = -1;
  
  public static final List<String> DEFAULT_ORIGINATING_NUMBERS = Collections.emptyList();
  
  public static final int DESTINATION_PORT_ANY = -1;
  
  public static final int DESTINATION_PORT_DATA_SMS = -2;
  
  public final String clientPrefix;
  
  public final int destinationPort;
  
  public final List<String> originatingNumbers;
  
  public final String packageName;
  
  class Builder {
    private String mClientPrefix = "//VVM";
    
    private List<String> mOriginatingNumbers = VisualVoicemailSmsFilterSettings.DEFAULT_ORIGINATING_NUMBERS;
    
    private int mDestinationPort = -1;
    
    private String mPackageName;
    
    public VisualVoicemailSmsFilterSettings build() {
      return new VisualVoicemailSmsFilterSettings(this);
    }
    
    public Builder setClientPrefix(String param1String) {
      if (param1String != null) {
        this.mClientPrefix = param1String;
        return this;
      } 
      throw new IllegalArgumentException("Client prefix cannot be null");
    }
    
    public Builder setOriginatingNumbers(List<String> param1List) {
      if (param1List != null) {
        this.mOriginatingNumbers = param1List;
        return this;
      } 
      throw new IllegalArgumentException("Originating numbers cannot be null");
    }
    
    public Builder setDestinationPort(int param1Int) {
      this.mDestinationPort = param1Int;
      return this;
    }
    
    public Builder setPackageName(String param1String) {
      this.mPackageName = param1String;
      return this;
    }
  }
  
  private VisualVoicemailSmsFilterSettings(Builder paramBuilder) {
    this.clientPrefix = paramBuilder.mClientPrefix;
    this.originatingNumbers = paramBuilder.mOriginatingNumbers;
    this.destinationPort = paramBuilder.mDestinationPort;
    this.packageName = paramBuilder.mPackageName;
  }
  
  static {
    CREATOR = new Parcelable.Creator<VisualVoicemailSmsFilterSettings>() {
        public VisualVoicemailSmsFilterSettings createFromParcel(Parcel param1Parcel) {
          VisualVoicemailSmsFilterSettings.Builder builder = new VisualVoicemailSmsFilterSettings.Builder();
          builder.setClientPrefix(param1Parcel.readString());
          builder.setOriginatingNumbers(param1Parcel.createStringArrayList());
          builder.setDestinationPort(param1Parcel.readInt());
          builder.setPackageName(param1Parcel.readString());
          return builder.build();
        }
        
        public VisualVoicemailSmsFilterSettings[] newArray(int param1Int) {
          return new VisualVoicemailSmsFilterSettings[param1Int];
        }
      };
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.clientPrefix);
    paramParcel.writeStringList(this.originatingNumbers);
    paramParcel.writeInt(this.destinationPort);
    paramParcel.writeString(this.packageName);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[VisualVoicemailSmsFilterSettings clientPrefix=");
    stringBuilder.append(this.clientPrefix);
    stringBuilder.append(", originatingNumbers=");
    stringBuilder.append(this.originatingNumbers);
    stringBuilder.append(", destinationPort=");
    stringBuilder.append(this.destinationPort);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
}
