package android.telephony;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.PersistableBundle;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.telecom.Log;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import com.android.internal.telecom.ITelecomService;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.SmsApplication;

public class OplusOSTelephonyManager implements IOplusOSTelephony {
  private static boolean isQcomGeminiSupport = false;
  
  static {
    isMtkGeminiSupport = false;
    vDescriptor = "com.android.internal.telephony.ITelephony";
  }
  
  private boolean mIsExpVersion = false;
  
  private boolean mIsDualLteSupported = true;
  
  private static final String TAG = "OplusOSTelephonyManager";
  
  private static boolean isMtkGeminiSupport;
  
  private static String vDescriptor;
  
  private CarrierConfigManager mCarrierConfigManager;
  
  private Context mContext;
  
  private TelephonyManager mTelephonyManager;
  
  public static OplusOSTelephonyManager getDefault(Context paramContext) {
    return new OplusOSTelephonyManager(paramContext);
  }
  
  public OplusOSTelephonyManager(Context paramContext) {
    this.mContext = paramContext;
    this.mTelephonyManager = TelephonyManager.from(paramContext);
    initRemoteService();
  }
  
  private void initRemoteService() {
    isQcomGeminiSupport = true;
    isMtkGeminiSupport = false;
    vDescriptor = "com.android.internal.telephony.ITelephony";
    Context context = this.mContext;
    this.mCarrierConfigManager = (CarrierConfigManager)context.getSystemService("carrier_config");
    PackageManager packageManager = this.mContext.getPackageManager();
    if (packageManager != null)
      this.mIsExpVersion = packageManager.hasSystemFeature("oplus.version.exp"); 
  }
  
  @Deprecated
  public int oplusGetQcomActiveSubscriptionsCount() {
    Context context = this.mContext;
    if (context != null)
      return SubscriptionManager.from(context).getActiveSubscriptionInfoCount(); 
    return 0;
  }
  
  public String getSubscriberIdGemini(int paramInt) {
    String str1 = null, str2 = null;
    if (isQcomGeminiSupport == true) {
      int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
      str1 = str2;
      if (arrayOfInt != null) {
        str1 = str2;
        if (arrayOfInt.length > 0)
          str1 = this.mTelephonyManager.getSubscriberId(arrayOfInt[0]); 
      } 
    } else if (isMtkGeminiSupport != true) {
      str1 = this.mTelephonyManager.getSubscriberId();
    } 
    return str1;
  }
  
  @Deprecated
  public int getCallStateGemini(int paramInt) {
    int i;
    byte b1 = 0, b2 = 0;
    if (isQcomGeminiSupport == true) {
      int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
      i = b2;
      if (arrayOfInt != null) {
        i = b2;
        if (arrayOfInt.length > 0)
          i = TelephonyManager.getDefault().getCallState(arrayOfInt[0]); 
      } 
    } else if (isMtkGeminiSupport == true) {
      i = b1;
    } else {
      i = b1;
      if (paramInt == 0) {
        int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
        i = b1;
        if (arrayOfInt != null) {
          i = b1;
          if (arrayOfInt.length > 0)
            i = TelephonyManager.getDefault().getCallState(arrayOfInt[0]); 
        } 
      } 
    } 
    return i;
  }
  
  public String getVoiceMailNumberGemini(int paramInt) {
    String str1 = null, str2 = null;
    if (isQcomGeminiSupport == true) {
      int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
      str1 = str2;
      if (arrayOfInt != null) {
        str1 = str2;
        if (arrayOfInt.length > 0)
          str1 = this.mTelephonyManager.getVoiceMailNumber(arrayOfInt[0]); 
      } 
    } else if (isMtkGeminiSupport != true) {
      str1 = this.mTelephonyManager.getVoiceMailNumber();
    } 
    return str1;
  }
  
  public String getLine1NumberGemini(int paramInt) {
    String str1 = null, str2 = null;
    if (isQcomGeminiSupport == true) {
      int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
      str1 = str2;
      if (arrayOfInt != null) {
        str1 = str2;
        if (arrayOfInt.length > 0)
          str1 = this.mTelephonyManager.getLine1Number(arrayOfInt[0]); 
      } 
    } else if (isMtkGeminiSupport != true) {
      str1 = this.mTelephonyManager.getLine1Number();
    } 
    return str1;
  }
  
  @Deprecated
  public int getSimStateGemini(int paramInt) {
    int i = -1;
    if (isQcomGeminiSupport == true) {
      i = TelephonyManager.getDefault().getSimState(paramInt);
    } else if (isMtkGeminiSupport != true) {
      if (paramInt == 0)
        i = TelephonyManager.getDefault().getSimState(); 
    } 
    return i;
  }
  
  public boolean hasIccCardGemini(int paramInt) {
    boolean bool = false;
    if (isQcomGeminiSupport == true) {
      bool = TelephonyManager.getDefault().hasIccCard(paramInt);
    } else if (isMtkGeminiSupport != true) {
      if (paramInt == 0)
        bool = TelephonyManager.getDefault().hasIccCard(); 
    } 
    return bool;
  }
  
  public int getNetworkTypeGemini(int paramInt) {
    boolean bool1 = false, bool2 = false;
    if (isQcomGeminiSupport == true) {
      int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
      paramInt = bool2;
      if (arrayOfInt != null) {
        paramInt = bool2;
        if (arrayOfInt.length > 0)
          paramInt = this.mTelephonyManager.getNetworkType(arrayOfInt[0]); 
      } 
    } else if (isMtkGeminiSupport == true) {
      paramInt = bool1;
    } else {
      paramInt = this.mTelephonyManager.getNetworkType();
    } 
    return paramInt;
  }
  
  public boolean isNetworkRoamingGemini(int paramInt) {
    boolean bool1 = false, bool2 = false;
    if (isQcomGeminiSupport == true) {
      int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
      bool1 = bool2;
      if (arrayOfInt != null) {
        bool1 = bool2;
        if (arrayOfInt.length > 0)
          bool1 = TelephonyManager.getDefault().isNetworkRoaming(arrayOfInt[0]); 
      } 
    } else if (isMtkGeminiSupport != true) {
      if (paramInt == 0)
        bool1 = TelephonyManager.getDefault().isNetworkRoaming(); 
    } 
    return bool1;
  }
  
  public String getDeviceIdGemini(int paramInt) {
    String str = null;
    if (isQcomGeminiSupport == true) {
      str = this.mTelephonyManager.getDeviceId(paramInt);
    } else if (isMtkGeminiSupport != true) {
      this.mTelephonyManager.getDeviceId();
    } 
    return str;
  }
  
  public void listenGemini(Context paramContext, PhoneStateListener paramPhoneStateListener, int paramInt1, int paramInt2) {
    TelephonyManager telephonyManager1 = null;
    int[] arrayOfInt = SubscriptionManager.getSubId(paramInt2);
    TelephonyManager telephonyManager2 = telephonyManager1;
    if (arrayOfInt != null) {
      telephonyManager2 = telephonyManager1;
      if (arrayOfInt.length > 0)
        telephonyManager2 = new TelephonyManager(paramContext, arrayOfInt[0]); 
    } 
    if (telephonyManager2 != null) {
      telephonyManager2.listen(paramPhoneStateListener, paramInt1);
    } else {
      log("listenGemini ERROR!");
    } 
  }
  
  @Deprecated
  public boolean isSimInsert(int paramInt) {
    boolean bool = false;
    if (isQcomGeminiSupport == true) {
      bool = hasIccCardGemini(paramInt);
    } else if (isMtkGeminiSupport != true) {
      if (paramInt == 0)
        bool = hasIccCardGemini(paramInt); 
    } 
    return bool;
  }
  
  @Deprecated
  public String oplusGetIccCardTypeGemini(int paramInt) {
    String str = "";
    if (isQcomGeminiSupport == true)
      try {
        String str1 = getIccCardTypeGemini(paramInt);
      } catch (Exception exception) {} 
    return str;
  }
  
  public String getNetworkOperatorGemini(int paramInt) {
    String str2, str1 = "";
    if (isQcomGeminiSupport == true) {
      int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
      str2 = str1;
      if (arrayOfInt != null) {
        str2 = str1;
        if (arrayOfInt.length > 0)
          str2 = TelephonyManager.getDefault().getNetworkOperator(arrayOfInt[0]); 
      } 
    } else if (isMtkGeminiSupport == true) {
      str2 = str1;
    } else {
      str2 = str1;
      if (paramInt == 0)
        str2 = TelephonyManager.getDefault().getNetworkOperator(); 
    } 
    return str2;
  }
  
  public String getSimOperatorGemini(int paramInt) {
    String str2, str1 = "";
    if (isQcomGeminiSupport == true) {
      int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
      str2 = str1;
      if (arrayOfInt != null) {
        str2 = str1;
        if (arrayOfInt.length > 0)
          str2 = TelephonyManager.getDefault().getSimOperator(arrayOfInt[0]); 
      } 
    } else if (isMtkGeminiSupport == true) {
      str2 = str1;
    } else {
      str2 = str1;
      if (paramInt == 0)
        str2 = TelephonyManager.getDefault().getSimOperator(); 
    } 
    return str2;
  }
  
  public int getVoiceNetworkTypeGemini(int paramInt) {
    boolean bool1 = false, bool2 = false;
    if (isQcomGeminiSupport == true) {
      int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
      paramInt = bool2;
      if (arrayOfInt != null) {
        paramInt = bool2;
        if (arrayOfInt.length > 0)
          paramInt = this.mTelephonyManager.getVoiceNetworkType(arrayOfInt[0]); 
      } 
    } else if (isMtkGeminiSupport == true) {
      paramInt = bool1;
    } else {
      paramInt = this.mTelephonyManager.getVoiceNetworkType();
    } 
    return paramInt;
  }
  
  public int getCurrentPhoneTypeGemini(int paramInt) {
    int i = 0;
    byte b = 0;
    if (isQcomGeminiSupport == true) {
      int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
      i = b;
      if (arrayOfInt != null) {
        i = b;
        if (arrayOfInt.length > 0)
          i = TelephonyManager.getDefault().getCurrentPhoneType(arrayOfInt[0]); 
      } 
    } else if (isMtkGeminiSupport != true) {
      if (paramInt == 0)
        i = TelephonyManager.getDefault().getCurrentPhoneType(); 
    } 
    return i;
  }
  
  public String getSimSerialNumberGemini(int paramInt) {
    String str2, str1 = "";
    if (isQcomGeminiSupport == true) {
      int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
      str2 = str1;
      if (arrayOfInt != null) {
        str2 = str1;
        if (arrayOfInt.length > 0)
          str2 = this.mTelephonyManager.getSimSerialNumber(arrayOfInt[0]); 
      } 
    } else if (isMtkGeminiSupport == true) {
      str2 = str1;
    } else {
      str2 = this.mTelephonyManager.getSimSerialNumber();
    } 
    return str2;
  }
  
  private ITelephony getITelephony() {
    return ITelephony.Stub.asInterface(ServiceManager.getService("phone"));
  }
  
  private ITelecomService getTelecomService() {
    return ITelecomService.Stub.asInterface(ServiceManager.getService("telecom"));
  }
  
  @Deprecated
  public boolean endCallGemini(int paramInt) {
    try {
      return getTelecomService().endCall(this.mContext.getPackageName());
    } catch (RemoteException remoteException) {
      return false;
    } catch (NullPointerException nullPointerException) {
      return false;
    } 
  }
  
  @Deprecated
  public void answerRingingCallGemini(int paramInt) {
    try {
      if (isQcomGeminiSupport == true) {
        getTelecomService().acceptRingingCall(this.mContext.getPackageName());
      } else if (isMtkGeminiSupport != true) {
        getTelecomService().acceptRingingCall(this.mContext.getPackageName());
      } 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
  }
  
  @Deprecated
  public boolean isRingingGemini(int paramInt, String paramString) {
    try {
      return this.mTelephonyManager.isRinging();
    } catch (NullPointerException nullPointerException) {
      return false;
    } catch (Exception exception) {
      return false;
    } 
  }
  
  @Deprecated
  public boolean isIdleGemini(int paramInt, String paramString) {
    try {
      return this.mTelephonyManager.isIdle();
    } catch (NullPointerException nullPointerException) {
      return true;
    } catch (Exception exception) {
      return true;
    } 
  }
  
  @Deprecated
  public boolean isOffhookGemini(int paramInt, String paramString) {
    try {
      return this.mTelephonyManager.isOffhook();
    } catch (NullPointerException nullPointerException) {
      return false;
    } catch (Exception exception) {
      return false;
    } 
  }
  
  @Deprecated
  public void silenceRingerGemini(int paramInt, String paramString) {
    try {
      getTelecomService().silenceRinger(paramString);
    } catch (RemoteException remoteException) {
      Log.w("OplusOSTelephonyManager", "Error calling ITelecomService#silenceRinger", new Object[] { remoteException });
    } catch (NullPointerException nullPointerException) {
      nullPointerException.printStackTrace();
    } 
  }
  
  public boolean showInCallScreenGemini(boolean paramBoolean, String paramString1, String paramString2) {
    boolean bool1 = false, bool2 = false;
    try {
      getTelecomService().showInCallScreen(paramBoolean, paramString1, paramString2);
      paramBoolean = true;
    } catch (RemoteException remoteException) {
      Log.w("OplusOSTelephonyManager", "Error calling ITelecomService#showInCallScreen", new Object[] { remoteException });
      paramBoolean = bool2;
    } catch (NullPointerException nullPointerException) {
      nullPointerException.printStackTrace();
      paramBoolean = bool1;
    } 
    return paramBoolean;
  }
  
  public boolean supplyPuk(String paramString1, String paramString2, int paramInt) {
    boolean bool = false;
    try {
      if (isQcomGeminiSupport == true) {
        int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
        boolean bool2 = bool;
        if (arrayOfInt != null) {
          bool2 = bool;
          if (arrayOfInt.length > 0)
            bool2 = getITelephony().supplyPukForSubscriber(arrayOfInt[0], paramString1, paramString2); 
        } 
        return bool2;
      } 
      boolean bool1 = isMtkGeminiSupport;
      if (bool1 == true)
        return false; 
      return false;
    } catch (RemoteException remoteException) {
      return false;
    } catch (NullPointerException nullPointerException) {
      return false;
    } 
  }
  
  public boolean supplyPin(String paramString, int paramInt) {
    boolean bool = false;
    try {
      if (isQcomGeminiSupport == true) {
        int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
        boolean bool2 = bool;
        if (arrayOfInt != null) {
          bool2 = bool;
          if (arrayOfInt.length > 0)
            bool2 = getITelephony().supplyPinForSubscriber(arrayOfInt[0], paramString); 
        } 
        return bool2;
      } 
      boolean bool1 = isMtkGeminiSupport;
      if (bool1 == true)
        return false; 
      return false;
    } catch (RemoteException remoteException) {
      return false;
    } catch (NullPointerException nullPointerException) {
      return false;
    } 
  }
  
  public int[] supplyPukReportResult(String paramString1, String paramString2, int paramInt) {
    try {
      if (isQcomGeminiSupport == true) {
        int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
        if (arrayOfInt != null)
          return getITelephony().supplyPukReportResultForSubscriber(arrayOfInt[0], paramString1, paramString2); 
        return null;
      } 
      boolean bool = isMtkGeminiSupport;
      if (bool == true)
        return null; 
      return null;
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  public int[] supplyPinReportResult(String paramString, int paramInt) {
    try {
      if (isQcomGeminiSupport == true) {
        int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
        if (arrayOfInt != null)
          return getITelephony().supplyPinReportResultForSubscriber(arrayOfInt[0], paramString); 
        return null;
      } 
      boolean bool = isMtkGeminiSupport;
      if (bool == true)
        return null; 
      return null;
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  public boolean oplusIsSubActive(int paramInt) {
    boolean bool;
    paramInt = getSimStateGemini(paramInt);
    if (paramInt == 5) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @Deprecated
  public int oplusGetActiveSubscriptionsCount(Context paramContext) {
    return SubscriptionManager.from(paramContext).getActiveSubscriptionInfoCount();
  }
  
  @Deprecated
  public int oplusGetDefaultSubscription() {
    return SubscriptionManager.getDefaultSubscriptionId();
  }
  
  public int oplusGetDataSubscription() {
    int i = SubscriptionManager.getDefaultDataSubscriptionId();
    i = SubscriptionManager.getSlotIndex(i);
    return i;
  }
  
  public void oplusSetDataSubscription(Context paramContext, int paramInt) {
    int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
    if (arrayOfInt != null && arrayOfInt.length > 0)
      SubscriptionManager.from(paramContext).setDefaultDataSubId(arrayOfInt[0]); 
  }
  
  private static IBinder getRemoteServiceBinder() {
    StringBuilder stringBuilder;
    IBinder iBinder = ServiceManager.getService("phone");
    if (iBinder == null) {
      log("***********************************");
      stringBuilder = new StringBuilder();
      stringBuilder.append("OplusOSTelephonyManager is NULL !!!");
      stringBuilder.append(1);
      log(stringBuilder.toString());
      log("***********************************");
      return null;
    } 
    return (IBinder)stringBuilder;
  }
  
  public int oplusGetSimIndicatorState(int paramInt) {
    if (!isMtkGeminiSupport)
      return 0; 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    IBinder iBinder = getRemoteServiceBinder();
    if (iBinder == null) {
      parcel2.recycle();
      parcel1.recycle();
      return 0;
    } 
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeInt(paramInt);
      iBinder.transact(10005, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("oplusGetSimIndicatorState ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      paramInt = -1;
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
    return paramInt;
  }
  
  public int oplusSetPreferredNetworkType(int paramInt1, int paramInt2) {
    boolean bool2, bool1 = false;
    boolean bool = false;
    try {
      int[] arrayOfInt = SubscriptionManager.getSubId(paramInt1);
      bool2 = bool;
      if (arrayOfInt != null) {
        bool2 = bool;
        if (arrayOfInt.length > 0)
          bool2 = getITelephony().setPreferredNetworkType(arrayOfInt[0], paramInt2); 
      } 
    } catch (RemoteException remoteException) {
      Log.w("OplusOSTelephonyManager", "setPreferredNetworkType RemoteException", new Object[] { remoteException });
      bool2 = bool;
    } catch (NullPointerException nullPointerException) {
      Log.w("OplusOSTelephonyManager", "setPreferredNetworkType NPE", new Object[] { nullPointerException });
      bool2 = bool1;
    } 
    if (bool2 == true) {
      paramInt1 = 0;
    } else {
      paramInt1 = -1;
    } 
    return paramInt1;
  }
  
  public boolean oplusSetLine1Number(int paramInt, String paramString) {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    IBinder iBinder = getRemoteServiceBinder();
    boolean bool = false;
    if (iBinder == null) {
      parcel2.recycle();
      parcel1.recycle();
      return false;
    } 
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeInt(paramInt);
      parcel1.writeString(paramString);
      iBinder.transact(10009, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      if (paramInt != 0)
        bool = true; 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("oplusSetLine1Number ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      bool = false;
    } finally {}
    parcel2.recycle();
    parcel1.recycle();
    return bool;
  }
  
  public boolean oplusGetIccLockEnabled(int paramInt) {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    IBinder iBinder = getRemoteServiceBinder();
    boolean bool = false;
    if (iBinder == null) {
      parcel2.recycle();
      parcel1.recycle();
      return false;
    } 
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeInt(paramInt);
      iBinder.transact(10010, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      if (paramInt != 0)
        bool = true; 
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("oplusGetIccLockEnabled ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      bool = false;
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
    return bool;
  }
  
  public String oplusGetScAddressGemini(int paramInt1, int paramInt2) {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    IBinder iBinder = getRemoteServiceBinder();
    if (iBinder == null) {
      parcel2.recycle();
      parcel1.recycle();
      return null;
    } 
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      iBinder.transact(10015, parcel1, parcel2, 0);
      parcel2.readException();
      String str = parcel2.readString();
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("oplusGetScAddressGemini ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      exception = null;
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
    return (String)iBinder;
  }
  
  public void oplusSetScAddressGemini(int paramInt1, String paramString, int paramInt2) {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    IBinder iBinder = getRemoteServiceBinder();
    if (iBinder == null) {
      parcel2.recycle();
      parcel1.recycle();
      return;
    } 
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeInt(paramInt1);
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt2);
      iBinder.transact(10016, parcel1, parcel2, 0);
      parcel2.readException();
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("oplusSetScAddressGemini ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
  }
  
  public String getIccCardTypeGemini(int paramInt) {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    IBinder iBinder = getRemoteServiceBinder();
    if (iBinder == null) {
      parcel2.recycle();
      parcel1.recycle();
      return null;
    } 
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeInt(paramInt);
      iBinder.transact(10038, parcel1, parcel2, 0);
      parcel2.readException();
      String str = parcel2.readString();
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("oplusSetPrioritySubscription ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      stringBuilder = null;
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
    return (String)iBinder;
  }
  
  @Deprecated
  public String oplusGetQcomImeiGemini(int paramInt) {
    return null;
  }
  
  @Deprecated
  public String[] oplusGetQcomLTECDMAImei(int paramInt) {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    IBinder iBinder = getRemoteServiceBinder();
    if (iBinder == null) {
      parcel2.recycle();
      parcel1.recycle();
      return null;
    } 
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeInt(paramInt);
      iBinder.transact(10042, parcel1, parcel2, 0);
      parcel2.readException();
      String[] arrayOfString = parcel2.createStringArray();
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("oplusGetQcomLTECDMAImei ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      exception = null;
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
    return (String[])iBinder;
  }
  
  public boolean oplusIsWhiteSIMCard(int paramInt) {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    IBinder iBinder = getRemoteServiceBinder();
    boolean bool = false;
    if (iBinder == null) {
      parcel2.recycle();
      parcel1.recycle();
      return false;
    } 
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeInt(paramInt);
      iBinder.transact(10047, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      if (paramInt != 0)
        bool = true; 
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("oplusIsWhiteSIMCard ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      bool = false;
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
    return bool;
  }
  
  @Deprecated
  public String oplusGetMeid(int paramInt) {
    return null;
  }
  
  public boolean isUriFileExist(String paramString) {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    IBinder iBinder = getRemoteServiceBinder();
    boolean bool = false;
    if (iBinder == null) {
      parcel2.recycle();
      parcel1.recycle();
      return false;
    } 
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeString(paramString);
      iBinder.transact(10049, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("isUriFileExist ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      bool = false;
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
    return bool;
  }
  
  public CellLocation getCellLocation(int paramInt) {
    CellLocation cellLocation;
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    null = null;
    IBinder iBinder = getRemoteServiceBinder();
    if (iBinder == null) {
      parcel2.recycle();
      parcel1.recycle();
      return null;
    } 
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeInt(paramInt);
      iBinder.transact(10051, parcel1, parcel2, 0);
      parcel2.readException();
      Bundle bundle = parcel2.readBundle();
    } catch (Exception exception) {
    
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
    iBinder = null;
    if (SYNTHETIC_LOCAL_VARIABLE_4 != null) {
      if (SYNTHETIC_LOCAL_VARIABLE_4.isEmpty())
        return null; 
      String str = getIccCardTypeGemini(paramInt);
      int i = SYNTHETIC_LOCAL_VARIABLE_4.getInt("type", 0);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getCellLocation-->");
      stringBuilder.append(str);
      stringBuilder.append(", vPhoneType-->");
      stringBuilder.append(i);
      stringBuilder.append(",  slotId-->");
      stringBuilder.append(paramInt);
      log(stringBuilder.toString());
      CellLocation cellLocation1 = newCellLocationFromBundle((Bundle)SYNTHETIC_LOCAL_VARIABLE_4, str);
      cellLocation = cellLocation1;
      if (cellLocation1 != null) {
        cellLocation = cellLocation1;
        if (cellLocation1.isEmpty()) {
          log("getCellLocationTT44 guix");
          GsmCellLocation gsmCellLocation = new GsmCellLocation((Bundle)SYNTHETIC_LOCAL_VARIABLE_4);
          if (!gsmCellLocation.isEmpty())
            return gsmCellLocation; 
          log("getCellLocationTT33");
          return null;
        } 
      } 
    } 
    return cellLocation;
  }
  
  private static CellLocation newCellLocationFromBundle(Bundle paramBundle, String paramString) {
    int i = paramBundle.getInt("type", 0);
    if ("CSIM".equals(paramString) || "RUIM".equals(paramString) || i == 2)
      return new CdmaCellLocation(paramBundle); 
    return new GsmCellLocation(paramBundle);
  }
  
  @Deprecated
  public static int oplusgetActiveSubInfoCount(Context paramContext) {
    return SubscriptionManager.from(paramContext).getActiveSubscriptionInfoCount();
  }
  
  @Deprecated
  public static int oplusgetPhoneId(Context paramContext, int paramInt) {
    return SubscriptionManager.getPhoneId(paramInt);
  }
  
  public static int oplusgetSubId(Context paramContext, int paramInt) {
    int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
    if (arrayOfInt != null && arrayOfInt.length > 0) {
      paramInt = arrayOfInt[0];
    } else {
      paramInt = -1000;
    } 
    return paramInt;
  }
  
  public static int oplusgetSlotId(Context paramContext, int paramInt) {
    return SubscriptionManager.getSlotIndex(paramInt);
  }
  
  @Deprecated
  public static int oplusgetOnDemandDataSubId(Context paramContext) {
    return -1;
  }
  
  @Deprecated
  public static int oplusgetSubState(Context paramContext, int paramInt) {
    return -1;
  }
  
  public static boolean oplusisValidPhoneId(Context paramContext, int paramInt) {
    return SubscriptionManager.isValidPhoneId(paramInt);
  }
  
  public static boolean oplusisValidSlotId(Context paramContext, int paramInt) {
    return SubscriptionManager.isValidSlotIndex(paramInt);
  }
  
  public static boolean oplusisValidSubId(Context paramContext, int paramInt) {
    return SubscriptionManager.isValidSubscriptionId(paramInt);
  }
  
  public static int oplusgetDefaultDataPhoneId(Context paramContext) {
    return SubscriptionManager.from(paramContext).getDefaultDataPhoneId();
  }
  
  public static int oplusgetDefaultDataSubId(Context paramContext) {
    return SubscriptionManager.getDefaultDataSubscriptionId();
  }
  
  public static int oplusgetDefaultSmsPhoneId(Context paramContext) {
    return SubscriptionManager.from(paramContext).getDefaultSmsPhoneId();
  }
  
  public static int oplusgetDefaultSmsSubId(Context paramContext) {
    return SubscriptionManager.getDefaultSmsSubscriptionId();
  }
  
  @Deprecated
  public static int oplusgetDefaultSubId(Context paramContext) {
    return SubscriptionManager.getDefaultSubscriptionId();
  }
  
  public String getNetworkCountryIso(int paramInt) {
    return TelephonyManager.getDefault().getNetworkCountryIso(paramInt);
  }
  
  public boolean handlePinMmiForSubscriber(int paramInt, String paramString) {
    try {
      return getITelephony().handlePinMmiForSubscriber(paramInt, paramString);
    } catch (RemoteException remoteException) {
      return false;
    } catch (NullPointerException nullPointerException) {
      return false;
    } 
  }
  
  public String getIccOperatorNumeric(int paramInt) {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    IBinder iBinder = getRemoteServiceBinder();
    if (iBinder == null) {
      parcel2.recycle();
      parcel1.recycle();
      return null;
    } 
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeInt(paramInt);
      iBinder.transact(10107, parcel1, parcel2, 0);
      parcel2.readException();
      String str = parcel2.readString();
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("getIccOperatorNumeric ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      stringBuilder = null;
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
    return (String)iBinder;
  }
  
  public static void setDefaultApplication(String paramString, Context paramContext) {
    SmsApplication.setDefaultApplication(paramString, paramContext);
  }
  
  private static void log(String paramString) {
    Log.d("OplusOSTelephonyManager", paramString, new Object[0]);
  }
  
  public boolean oplusIsQcomSubActive(int paramInt) {
    IBinder iBinder = getRemoteServiceBinder();
    boolean bool = false;
    if (iBinder == null) {
      log("oplusIsQcomSubActive remoteServiceBinder is null, return!");
      return false;
    } 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeInt(paramInt);
      iBinder.transact(10101, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      if (paramInt != 0)
        bool = true; 
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("oplusIsQcomSubActive ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      bool = false;
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
    return bool;
  }
  
  @Deprecated
  public boolean isRingingGemini(int paramInt) {
    Context context = this.mContext;
    if (context != null)
      return isRingingGemini(paramInt, context.getOpPackageName()); 
    return false;
  }
  
  @Deprecated
  public void silenceRingerGemini(int paramInt) {
    Context context = this.mContext;
    if (context != null)
      silenceRingerGemini(paramInt, context.getOpPackageName()); 
  }
  
  public void listenGemini(PhoneStateListener paramPhoneStateListener, int paramInt1, int paramInt2) {
    Context context = this.mContext;
    if (context != null)
      listenGemini(context, paramPhoneStateListener, paramInt1, paramInt2); 
  }
  
  @Deprecated
  public boolean isIdleGemini(int paramInt) {
    Context context = this.mContext;
    if (context != null)
      return isIdleGemini(paramInt, context.getOpPackageName()); 
    return true;
  }
  
  public boolean isOplusHasSoftSimCard() {
    IBinder iBinder = getRemoteServiceBinder();
    boolean bool = false;
    if (iBinder == null)
      return false; 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      iBinder.transact(10105, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("oplusIsImsRegistered ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      bool = false;
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
    return bool;
  }
  
  public int oplusGetSoftSimCardSlotId() {
    byte b2, b1 = -1;
    IBinder iBinder = getRemoteServiceBinder();
    if (iBinder == null) {
      log("getSubState remoteServiceBinder is null, return!");
      return -1;
    } 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      iBinder.transact(10106, parcel1, parcel2, 0);
      parcel2.readException();
      b2 = parcel2.readInt();
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("getSubState ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      b2 = b1;
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
    return b2;
  }
  
  public void oplusSetDataRoamingEnabled(int paramInt, boolean paramBoolean) {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    IBinder iBinder = getRemoteServiceBinder();
    if (iBinder == null) {
      parcel2.recycle();
      parcel1.recycle();
      return;
    } 
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeInt(paramInt);
      if (paramBoolean) {
        paramInt = 1;
      } else {
        paramInt = 0;
      } 
      parcel1.writeInt(paramInt);
      iBinder.transact(10053, parcel1, parcel2, 0);
      parcel2.readException();
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("oplusSetScAddressGemini ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
  }
  
  public boolean oplusIsImsRegistered(Context paramContext, int paramInt) {
    IBinder iBinder = getRemoteServiceBinder();
    boolean bool = false;
    if (iBinder == null)
      return false; 
    Parcel parcel2 = Parcel.obtain();
    Parcel parcel1 = Parcel.obtain();
    try {
      parcel2.writeInterfaceToken(vDescriptor);
      parcel2.writeInt(paramInt);
      iBinder.transact(10055, parcel2, parcel1, 0);
      parcel1.readException();
      paramInt = parcel1.readInt();
      if (paramInt != 0)
        bool = true; 
      parcel1.recycle();
      parcel2.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("oplusIsImsRegistered ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      bool = false;
      parcel1.recycle();
      parcel2.recycle();
    } finally {}
    return bool;
  }
  
  public String oplusGetPlmnOverride(String paramString, ServiceState paramServiceState) {
    return OplusTelephonyFunction.oplusGetPlmnOverride(this.mContext, paramString, paramServiceState);
  }
  
  public String oplusGetOemSpn(int paramInt) {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    IBinder iBinder = getRemoteServiceBinder();
    if (iBinder == null) {
      parcel2.recycle();
      parcel1.recycle();
      return null;
    } 
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeInt(paramInt);
      iBinder.transact(10056, parcel1, parcel2, 0);
      parcel2.readException();
      String str = parcel2.readString();
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("oplusGetOemSpn ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      stringBuilder = null;
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
    return (String)iBinder;
  }
  
  public boolean oplusMvnoMatches(int paramInt1, int paramInt2, String paramString1, String paramString2) {
    IBinder iBinder = getRemoteServiceBinder();
    boolean bool = false;
    if (iBinder == null)
      return false; 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      iBinder.transact(10057, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt1 = parcel2.readInt();
      if (paramInt1 != 0)
        bool = true; 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("oplusMvnoMatches ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      bool = false;
    } finally {}
    parcel2.recycle();
    parcel1.recycle();
    return bool;
  }
  
  public boolean isOplusSingleSimCard() {
    return OplusTelephonyFunction.oplusGetSingleSimCard();
  }
  
  public boolean oplusIsSimLockedEnabled() {
    return OplusTelephonyFunction.oplusIsSimLockedEnabled();
  }
  
  public boolean oplusGetSimLockedStatus(int paramInt) {
    return false;
  }
  
  public boolean oplusIsVolteEnabledByPlatform(Context paramContext, int paramInt) {
    IBinder iBinder = getRemoteServiceBinder();
    boolean bool = false;
    if (iBinder == null)
      return false; 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeInt(paramInt);
      iBinder.transact(10058, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      if (paramInt != 0)
        bool = true; 
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("oplusIsImsRegistered ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      bool = false;
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
    return bool;
  }
  
  public boolean oplusIsVtEnabledByPlatform(Context paramContext, int paramInt) {
    IBinder iBinder = getRemoteServiceBinder();
    boolean bool = false;
    if (iBinder == null)
      return false; 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeInt(paramInt);
      iBinder.transact(10059, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      if (paramInt != 0)
        bool = true; 
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("oplusIsImsRegistered ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      bool = false;
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
    return bool;
  }
  
  public boolean oplusIsWfcEnabledByPlatform(Context paramContext, int paramInt) {
    IBinder iBinder = getRemoteServiceBinder();
    boolean bool = false;
    if (iBinder == null)
      return false; 
    Parcel parcel2 = Parcel.obtain();
    Parcel parcel1 = Parcel.obtain();
    try {
      parcel2.writeInterfaceToken(vDescriptor);
      parcel2.writeInt(paramInt);
      iBinder.transact(10060, parcel2, parcel1, 0);
      parcel1.readException();
      paramInt = parcel1.readInt();
      if (paramInt != 0)
        bool = true; 
      parcel1.recycle();
      parcel2.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("oplusIsImsRegistered ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      bool = false;
      parcel1.recycle();
      parcel2.recycle();
    } finally {}
    return bool;
  }
  
  public String oplusGetIccId(int paramInt) {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    IBinder iBinder = getRemoteServiceBinder();
    if (iBinder == null) {
      parcel2.recycle();
      parcel1.recycle();
      return null;
    } 
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeInt(paramInt);
      iBinder.transact(10061, parcel1, parcel2, 0);
      parcel2.readException();
      String str = parcel2.readString();
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("oplusGetIccId ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      exception = null;
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
    return (String)iBinder;
  }
  
  public void setDualLteEnabled(boolean paramBoolean) {
    IBinder iBinder = getRemoteServiceBinder();
    if (iBinder == null) {
      log("setDualLteEnabled remoteServiceBinder is null, return!");
      return;
    } 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      boolean bool;
      parcel1.writeInterfaceToken(vDescriptor);
      if (paramBoolean) {
        bool = true;
      } else {
        bool = false;
      } 
      parcel1.writeInt(bool);
      iBinder.transact(10062, parcel1, parcel2, 0);
      parcel2.readException();
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("setDualLteEnabled ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
  }
  
  public boolean isDualLteEnabled() {
    IBinder iBinder = getRemoteServiceBinder();
    boolean bool = false;
    if (iBinder == null) {
      log("isDualLteEnabled remoteServiceBinder is null, return!");
      return false;
    } 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      iBinder.transact(10063, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("isDualLteEnabled ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      bool = false;
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
    return bool;
  }
  
  public boolean isDualLteSupportedByPlatform() {
    if (this.mIsExpVersion) {
      boolean bool = CarrierConfigManager.getDefaultConfig().getBoolean("config_oppo_dual_lte_available_bool");
      boolean bool1 = true, bool2 = true;
      if (bool) {
        if (!getBooleanCarrierConfig("config_oppo_dual_lte_available_bool", 0) || 
          !getBooleanCarrierConfig("config_oppo_dual_lte_available_bool", 1))
          bool2 = false; 
        this.mIsDualLteSupported = bool2;
      } else {
        if (getBooleanCarrierConfig("config_oppo_dual_lte_available_bool", 0) || 
          getBooleanCarrierConfig("config_oppo_dual_lte_available_bool", 1)) {
          bool2 = bool1;
        } else {
          bool2 = false;
        } 
        this.mIsDualLteSupported = bool2;
      } 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("isDualLteSupportedByPlatform mIsDualLteSupported = ");
    stringBuilder.append(this.mIsDualLteSupported);
    log(stringBuilder.toString());
    return this.mIsDualLteSupported;
  }
  
  public boolean getBooleanCarrierConfig(String paramString, int paramInt) {
    PersistableBundle persistableBundle;
    if (TextUtils.isEmpty(paramString)) {
      log("getBooleanCarrierConfig return false for key is null!");
      return false;
    } 
    int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
    if (arrayOfInt == null || arrayOfInt.length == 0)
      return false; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("getBooleanCarrierConfig: phoneId=");
    stringBuilder.append(paramInt);
    stringBuilder.append(" subId=");
    stringBuilder.append(arrayOfInt[0]);
    stringBuilder.append(" key = ");
    stringBuilder.append(paramString);
    log(stringBuilder.toString());
    stringBuilder = null;
    CarrierConfigManager carrierConfigManager = this.mCarrierConfigManager;
    if (carrierConfigManager != null)
      persistableBundle = carrierConfigManager.getConfigForSubId(arrayOfInt[0]); 
    if (persistableBundle != null)
      return persistableBundle.getBoolean(paramString); 
    return CarrierConfigManager.getDefaultConfig().getBoolean(paramString);
  }
  
  public static boolean getBooleanCarrierConfig(Context paramContext, String paramString, int paramInt) {
    PersistableBundle persistableBundle;
    if (paramContext == null || TextUtils.isEmpty(paramString)) {
      log("getBooleanCarrierConfig return false for context is null or key is null!");
      return false;
    } 
    CarrierConfigManager carrierConfigManager = (CarrierConfigManager)paramContext.getSystemService("carrier_config");
    int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
    if (arrayOfInt == null || arrayOfInt.length == 0)
      return false; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("getBooleanCarrierConfig: phoneId=");
    stringBuilder.append(paramInt);
    stringBuilder.append(" subId=");
    stringBuilder.append(arrayOfInt[0]);
    stringBuilder.append(" key = ");
    stringBuilder.append(paramString);
    log(stringBuilder.toString());
    stringBuilder = null;
    if (carrierConfigManager != null)
      persistableBundle = carrierConfigManager.getConfigForSubId(arrayOfInt[0]); 
    if (persistableBundle != null)
      return persistableBundle.getBoolean(paramString); 
    return CarrierConfigManager.getDefaultConfig().getBoolean(paramString);
  }
  
  public static void activateSubId(int paramInt) {
    IBinder iBinder = getRemoteServiceBinder();
    if (iBinder == null) {
      log("activateSubId remoteServiceBinder is null, return!");
      return;
    } 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeInt(paramInt);
      iBinder.transact(10102, parcel1, parcel2, 0);
      parcel2.readException();
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("activateSubId ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
  }
  
  public static void deactivateSubId(int paramInt) {
    IBinder iBinder = getRemoteServiceBinder();
    if (iBinder == null) {
      log("deactivateSubId remoteServiceBinder is null, return!");
      return;
    } 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeInt(paramInt);
      iBinder.transact(10103, parcel1, parcel2, 0);
      parcel2.readException();
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("deactivateSubId ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
  }
  
  public static int getSubState(int paramInt) {
    boolean bool = true;
    IBinder iBinder = getRemoteServiceBinder();
    if (iBinder == null) {
      log("getSubState remoteServiceBinder is null, return!");
      return 1;
    } 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeInt(paramInt);
      iBinder.transact(10104, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("getSubState ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      paramInt = bool;
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
    return paramInt;
  }
  
  public void oplusSimlockReq(String paramString) {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    IBinder iBinder = getRemoteServiceBinder();
    if (iBinder == null) {
      parcel2.recycle();
      parcel1.recycle();
      return;
    } 
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeString(paramString);
      iBinder.transact(10064, parcel1, parcel2, 0);
      parcel2.readException();
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append(" simlock request ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
  }
  
  public String oplusCommonReq(String paramString) {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    IBinder iBinder = getRemoteServiceBinder();
    if (iBinder == null) {
      parcel2.recycle();
      parcel1.recycle();
      return null;
    } 
    try {
      parcel1.writeInterfaceToken(vDescriptor);
      parcel1.writeString(paramString);
      iBinder.transact(10065, parcel1, parcel2, 0);
      parcel2.readException();
      paramString = parcel2.readString();
      parcel2.recycle();
      parcel1.recycle();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append(" common request ERROR !!! ");
      stringBuilder.append(exception);
      log(stringBuilder.toString());
      exception = null;
      parcel2.recycle();
      parcel1.recycle();
    } finally {}
    return paramString;
  }
}
