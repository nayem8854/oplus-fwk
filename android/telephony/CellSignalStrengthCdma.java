package android.telephony;

import android.hardware.radio.V1_0.CdmaSignalStrength;
import android.hardware.radio.V1_0.EvdoSignalStrength;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PersistableBundle;
import com.android.telephony.Rlog;
import java.util.Objects;

public final class CellSignalStrengthCdma extends CellSignalStrength implements Parcelable {
  public static final Parcelable.Creator<CellSignalStrengthCdma> CREATOR;
  
  private static final boolean DBG = false;
  
  private static final String LOG_TAG = "CellSignalStrengthCdma";
  
  public CellSignalStrengthCdma() {
    setDefaultValues();
  }
  
  public CellSignalStrengthCdma(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this.mCdmaDbm = inRangeOrUnavailable(paramInt1, -120, 0);
    this.mCdmaEcio = inRangeOrUnavailable(paramInt2, -160, 0);
    this.mEvdoDbm = inRangeOrUnavailable(paramInt3, -120, 0);
    this.mEvdoEcio = inRangeOrUnavailable(paramInt4, -160, 0);
    this.mEvdoSnr = inRangeOrUnavailable(paramInt5, 0, 8);
    updateLevel((PersistableBundle)null, (ServiceState)null);
  }
  
  public CellSignalStrengthCdma(CdmaSignalStrength paramCdmaSignalStrength, EvdoSignalStrength paramEvdoSignalStrength) {
    this(-paramCdmaSignalStrength.dbm, -paramCdmaSignalStrength.ecio, -paramEvdoSignalStrength.dbm, -paramEvdoSignalStrength.ecio, paramEvdoSignalStrength.signalNoiseRatio);
  }
  
  public CellSignalStrengthCdma(CellSignalStrengthCdma paramCellSignalStrengthCdma) {
    copyFrom(paramCellSignalStrengthCdma);
  }
  
  protected void copyFrom(CellSignalStrengthCdma paramCellSignalStrengthCdma) {
    this.mCdmaDbm = paramCellSignalStrengthCdma.mCdmaDbm;
    this.mCdmaEcio = paramCellSignalStrengthCdma.mCdmaEcio;
    this.mEvdoDbm = paramCellSignalStrengthCdma.mEvdoDbm;
    this.mEvdoEcio = paramCellSignalStrengthCdma.mEvdoEcio;
    this.mEvdoSnr = paramCellSignalStrengthCdma.mEvdoSnr;
    this.mLevel = paramCellSignalStrengthCdma.mLevel;
  }
  
  public CellSignalStrengthCdma copy() {
    return new CellSignalStrengthCdma(this);
  }
  
  public void setDefaultValues() {
    this.mCdmaDbm = Integer.MAX_VALUE;
    this.mCdmaEcio = Integer.MAX_VALUE;
    this.mEvdoDbm = Integer.MAX_VALUE;
    this.mEvdoEcio = Integer.MAX_VALUE;
    this.mEvdoSnr = Integer.MAX_VALUE;
    this.mLevel = 0;
  }
  
  public int getLevel() {
    return this.mLevel;
  }
  
  public void updateLevel(PersistableBundle paramPersistableBundle, ServiceState paramServiceState) {
    int i = getCdmaLevel();
    int j = getEvdoLevel();
    if (j == 0) {
      this.mLevel = getCdmaLevel();
    } else if (i == 0) {
      this.mLevel = getEvdoLevel();
    } else {
      if (i < j)
        j = i; 
      this.mLevel = j;
    } 
  }
  
  public int getAsuLevel() {
    int i = getCdmaDbm();
    int j = getCdmaEcio();
    if (i == Integer.MAX_VALUE) {
      i = 99;
    } else if (i >= -75) {
      i = 16;
    } else if (i >= -82) {
      i = 8;
    } else if (i >= -90) {
      i = 4;
    } else if (i >= -95) {
      i = 2;
    } else if (i >= -100) {
      i = 1;
    } else {
      i = 99;
    } 
    if (j == Integer.MAX_VALUE) {
      j = 99;
    } else if (j >= -90) {
      j = 16;
    } else if (j >= -100) {
      j = 8;
    } else if (j >= -115) {
      j = 4;
    } else if (j >= -130) {
      j = 2;
    } else if (j >= -150) {
      j = 1;
    } else {
      j = 99;
    } 
    if (i >= j)
      i = j; 
    return i;
  }
  
  public int getCdmaLevel() {
    int i = getCdmaDbm();
    int j = getCdmaEcio();
    if (i == Integer.MAX_VALUE) {
      i = 0;
    } else if (i >= -75) {
      i = 4;
    } else if (i >= -85) {
      i = 3;
    } else if (i >= -95) {
      i = 2;
    } else if (i >= -100) {
      i = 1;
    } else {
      i = 0;
    } 
    if (j == Integer.MAX_VALUE) {
      j = 0;
    } else if (j >= -90) {
      j = 4;
    } else if (j >= -110) {
      j = 3;
    } else if (j >= -130) {
      j = 2;
    } else if (j >= -150) {
      j = 1;
    } else {
      j = 0;
    } 
    if (i >= j)
      i = j; 
    return i;
  }
  
  public int getEvdoLevel() {
    int i = getEvdoDbm();
    int j = getEvdoSnr();
    if (i == Integer.MAX_VALUE) {
      i = 0;
    } else if (i >= -65) {
      i = 4;
    } else if (i >= -75) {
      i = 3;
    } else if (i >= -90) {
      i = 2;
    } else if (i >= -105) {
      i = 1;
    } else {
      i = 0;
    } 
    if (j == Integer.MAX_VALUE) {
      j = 0;
    } else if (j >= 7) {
      j = 4;
    } else if (j >= 5) {
      j = 3;
    } else if (j >= 3) {
      j = 2;
    } else if (j >= 1) {
      j = 1;
    } else {
      j = 0;
    } 
    if (i >= j)
      i = j; 
    return i;
  }
  
  public int getEvdoAsuLevel() {
    int i = getEvdoDbm();
    int j = getEvdoSnr();
    if (i >= -65) {
      i = 16;
    } else if (i >= -75) {
      i = 8;
    } else if (i >= -85) {
      i = 4;
    } else if (i >= -95) {
      i = 2;
    } else if (i >= -105) {
      i = 1;
    } else {
      i = 99;
    } 
    if (j >= 7) {
      j = 16;
    } else if (j >= 6) {
      j = 8;
    } else if (j >= 5) {
      j = 4;
    } else if (j >= 3) {
      j = 2;
    } else if (j >= 1) {
      j = 1;
    } else {
      j = 99;
    } 
    if (i >= j)
      i = j; 
    return i;
  }
  
  public int getDbm() {
    int i = getCdmaDbm();
    int j = getEvdoDbm();
    if (i < j)
      j = i; 
    return j;
  }
  
  public int getCdmaDbm() {
    return this.mCdmaDbm;
  }
  
  public void setCdmaDbm(int paramInt) {
    this.mCdmaDbm = paramInt;
  }
  
  public int getCdmaEcio() {
    return this.mCdmaEcio;
  }
  
  public void setCdmaEcio(int paramInt) {
    this.mCdmaEcio = paramInt;
  }
  
  public int getEvdoDbm() {
    return this.mEvdoDbm;
  }
  
  public void setEvdoDbm(int paramInt) {
    this.mEvdoDbm = paramInt;
  }
  
  public int getEvdoEcio() {
    return this.mEvdoEcio;
  }
  
  public void setEvdoEcio(int paramInt) {
    this.mEvdoEcio = paramInt;
  }
  
  public int getEvdoSnr() {
    return this.mEvdoSnr;
  }
  
  public void setEvdoSnr(int paramInt) {
    this.mEvdoSnr = paramInt;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mCdmaDbm), Integer.valueOf(this.mCdmaEcio), Integer.valueOf(this.mEvdoDbm), Integer.valueOf(this.mEvdoEcio), Integer.valueOf(this.mEvdoSnr), Integer.valueOf(this.mLevel) });
  }
  
  private static final CellSignalStrengthCdma sInvalid = new CellSignalStrengthCdma();
  
  private int mCdmaDbm;
  
  private int mCdmaEcio;
  
  private int mEvdoDbm;
  
  private int mEvdoEcio;
  
  private int mEvdoSnr;
  
  private int mLevel;
  
  public boolean isValid() {
    return equals(sInvalid) ^ true;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof CellSignalStrengthCdma;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    bool = bool1;
    if (this.mCdmaDbm == ((CellSignalStrengthCdma)paramObject).mCdmaDbm) {
      bool = bool1;
      if (this.mCdmaEcio == ((CellSignalStrengthCdma)paramObject).mCdmaEcio) {
        bool = bool1;
        if (this.mEvdoDbm == ((CellSignalStrengthCdma)paramObject).mEvdoDbm) {
          bool = bool1;
          if (this.mEvdoEcio == ((CellSignalStrengthCdma)paramObject).mEvdoEcio) {
            bool = bool1;
            if (this.mEvdoSnr == ((CellSignalStrengthCdma)paramObject).mEvdoSnr) {
              bool = bool1;
              if (this.mLevel == ((CellSignalStrengthCdma)paramObject).mLevel)
                bool = true; 
            } 
          } 
        } 
      } 
    } 
    return bool;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CellSignalStrengthCdma: cdmaDbm=");
    stringBuilder.append(this.mCdmaDbm);
    stringBuilder.append(" cdmaEcio=");
    stringBuilder.append(this.mCdmaEcio);
    stringBuilder.append(" evdoDbm=");
    stringBuilder.append(this.mEvdoDbm);
    stringBuilder.append(" evdoEcio=");
    stringBuilder.append(this.mEvdoEcio);
    stringBuilder.append(" evdoSnr=");
    stringBuilder.append(this.mEvdoSnr);
    stringBuilder.append(" level=");
    stringBuilder.append(this.mLevel);
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mCdmaDbm);
    paramParcel.writeInt(this.mCdmaEcio);
    paramParcel.writeInt(this.mEvdoDbm);
    paramParcel.writeInt(this.mEvdoEcio);
    paramParcel.writeInt(this.mEvdoSnr);
    paramParcel.writeInt(this.mLevel);
  }
  
  private CellSignalStrengthCdma(Parcel paramParcel) {
    this.mCdmaDbm = paramParcel.readInt();
    this.mCdmaEcio = paramParcel.readInt();
    this.mEvdoDbm = paramParcel.readInt();
    this.mEvdoEcio = paramParcel.readInt();
    this.mEvdoSnr = paramParcel.readInt();
    this.mLevel = paramParcel.readInt();
  }
  
  public int describeContents() {
    return 0;
  }
  
  static {
    CREATOR = (Parcelable.Creator<CellSignalStrengthCdma>)new Object();
  }
  
  private static void log(String paramString) {
    Rlog.w("CellSignalStrengthCdma", paramString);
  }
}
