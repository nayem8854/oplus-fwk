package android.telephony;

import android.annotation.SystemApi;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

@SystemApi
public final class SmsCbMessage implements Parcelable {
  public SmsCbMessage(int paramInt1, int paramInt2, int paramInt3, SmsCbLocation paramSmsCbLocation, int paramInt4, String paramString1, String paramString2, int paramInt5, SmsCbEtwsInfo paramSmsCbEtwsInfo, SmsCbCmasInfo paramSmsCbCmasInfo, int paramInt6, int paramInt7) {
    this(paramInt1, paramInt2, paramInt3, paramSmsCbLocation, paramInt4, paramString1, 0, paramString2, paramInt5, paramSmsCbEtwsInfo, paramSmsCbCmasInfo, 0, null, l, paramInt6, paramInt7);
  }
  
  public SmsCbMessage(int paramInt1, int paramInt2, int paramInt3, SmsCbLocation paramSmsCbLocation, int paramInt4, String paramString1, int paramInt5, String paramString2, int paramInt6, SmsCbEtwsInfo paramSmsCbEtwsInfo, SmsCbCmasInfo paramSmsCbCmasInfo, int paramInt7, List<CbGeoUtils.Geometry> paramList, long paramLong, int paramInt8, int paramInt9) {
    this.mMessageFormat = paramInt1;
    this.mGeographicalScope = paramInt2;
    this.mSerialNumber = paramInt3;
    this.mLocation = paramSmsCbLocation;
    this.mServiceCategory = paramInt4;
    this.mLanguage = paramString1;
    this.mDataCodingScheme = paramInt5;
    this.mBody = paramString2;
    this.mPriority = paramInt6;
    this.mEtwsWarningInfo = paramSmsCbEtwsInfo;
    this.mCmasWarningInfo = paramSmsCbCmasInfo;
    this.mReceivedTimeMillis = paramLong;
    this.mGeometries = paramList;
    this.mMaximumWaitTimeSec = paramInt7;
    this.mSlotIndex = paramInt8;
    this.mSubId = paramInt9;
  }
  
  public SmsCbMessage(Parcel paramParcel) {
    this.mMessageFormat = paramParcel.readInt();
    this.mGeographicalScope = paramParcel.readInt();
    this.mSerialNumber = paramParcel.readInt();
    this.mLocation = new SmsCbLocation(paramParcel);
    this.mServiceCategory = paramParcel.readInt();
    this.mLanguage = paramParcel.readString();
    this.mDataCodingScheme = paramParcel.readInt();
    this.mBody = paramParcel.readString();
    this.mPriority = paramParcel.readInt();
    int i = paramParcel.readInt();
    List<CbGeoUtils.Geometry> list = null;
    if (i != 67) {
      if (i != 69) {
        this.mEtwsWarningInfo = null;
        this.mCmasWarningInfo = null;
      } else {
        this.mEtwsWarningInfo = new SmsCbEtwsInfo(paramParcel);
        this.mCmasWarningInfo = null;
      } 
    } else {
      this.mEtwsWarningInfo = null;
      this.mCmasWarningInfo = new SmsCbCmasInfo(paramParcel);
    } 
    this.mReceivedTimeMillis = paramParcel.readLong();
    String str = paramParcel.readString();
    if (str != null)
      list = CbGeoUtils.parseGeometriesFromString(str); 
    this.mGeometries = list;
    this.mMaximumWaitTimeSec = paramParcel.readInt();
    this.mSlotIndex = paramParcel.readInt();
    this.mSubId = paramParcel.readInt();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mMessageFormat);
    paramParcel.writeInt(this.mGeographicalScope);
    paramParcel.writeInt(this.mSerialNumber);
    this.mLocation.writeToParcel(paramParcel, paramInt);
    paramParcel.writeInt(this.mServiceCategory);
    paramParcel.writeString(this.mLanguage);
    paramParcel.writeInt(this.mDataCodingScheme);
    paramParcel.writeString(this.mBody);
    paramParcel.writeInt(this.mPriority);
    if (this.mEtwsWarningInfo != null) {
      paramParcel.writeInt(69);
      this.mEtwsWarningInfo.writeToParcel(paramParcel, paramInt);
    } else if (this.mCmasWarningInfo != null) {
      paramParcel.writeInt(67);
      this.mCmasWarningInfo.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeInt(48);
    } 
    paramParcel.writeLong(this.mReceivedTimeMillis);
    List<CbGeoUtils.Geometry> list = this.mGeometries;
    if (list != null) {
      String str = CbGeoUtils.encodeGeometriesToString(list);
    } else {
      list = null;
    } 
    paramParcel.writeString((String)list);
    paramParcel.writeInt(this.mMaximumWaitTimeSec);
    paramParcel.writeInt(this.mSlotIndex);
    paramParcel.writeInt(this.mSubId);
  }
  
  public static final Parcelable.Creator<SmsCbMessage> CREATOR = new Parcelable.Creator<SmsCbMessage>() {
      public SmsCbMessage createFromParcel(Parcel param1Parcel) {
        return new SmsCbMessage(param1Parcel);
      }
      
      public SmsCbMessage[] newArray(int param1Int) {
        return new SmsCbMessage[param1Int];
      }
    };
  
  public static final int GEOGRAPHICAL_SCOPE_CELL_WIDE = 3;
  
  public static final int GEOGRAPHICAL_SCOPE_CELL_WIDE_IMMEDIATE = 0;
  
  public static final int GEOGRAPHICAL_SCOPE_LOCATION_AREA_WIDE = 2;
  
  public static final int GEOGRAPHICAL_SCOPE_PLMN_WIDE = 1;
  
  public static final String LOG_TAG = "SMSCB";
  
  public static final int MAXIMUM_WAIT_TIME_NOT_SET = 255;
  
  public static final int MESSAGE_FORMAT_3GPP = 1;
  
  public static final int MESSAGE_FORMAT_3GPP2 = 2;
  
  public static final int MESSAGE_PRIORITY_EMERGENCY = 3;
  
  public static final int MESSAGE_PRIORITY_INTERACTIVE = 1;
  
  public static final int MESSAGE_PRIORITY_NORMAL = 0;
  
  public static final int MESSAGE_PRIORITY_URGENT = 2;
  
  private final String mBody;
  
  private final SmsCbCmasInfo mCmasWarningInfo;
  
  private final int mDataCodingScheme;
  
  private final SmsCbEtwsInfo mEtwsWarningInfo;
  
  private final int mGeographicalScope;
  
  private final List<CbGeoUtils.Geometry> mGeometries;
  
  private final String mLanguage;
  
  private final SmsCbLocation mLocation;
  
  private final int mMaximumWaitTimeSec;
  
  private final int mMessageFormat;
  
  private final int mPriority;
  
  private final long mReceivedTimeMillis;
  
  private final int mSerialNumber;
  
  private final int mServiceCategory;
  
  private final int mSlotIndex;
  
  private final int mSubId;
  
  public int getGeographicalScope() {
    return this.mGeographicalScope;
  }
  
  public int getSerialNumber() {
    return this.mSerialNumber;
  }
  
  public SmsCbLocation getLocation() {
    return this.mLocation;
  }
  
  public int getServiceCategory() {
    return this.mServiceCategory;
  }
  
  public String getLanguageCode() {
    return this.mLanguage;
  }
  
  public int getDataCodingScheme() {
    return this.mDataCodingScheme;
  }
  
  public String getMessageBody() {
    return this.mBody;
  }
  
  @SystemApi
  public List<CbGeoUtils.Geometry> getGeometries() {
    List<CbGeoUtils.Geometry> list = this.mGeometries;
    if (list == null)
      return new ArrayList<>(); 
    return list;
  }
  
  public int getMaximumWaitingDuration() {
    return this.mMaximumWaitTimeSec;
  }
  
  public long getReceivedTime() {
    return this.mReceivedTimeMillis;
  }
  
  public int getSlotIndex() {
    return this.mSlotIndex;
  }
  
  public int getSubscriptionId() {
    return this.mSubId;
  }
  
  public int getMessageFormat() {
    return this.mMessageFormat;
  }
  
  public int getMessagePriority() {
    return this.mPriority;
  }
  
  public SmsCbEtwsInfo getEtwsWarningInfo() {
    return this.mEtwsWarningInfo;
  }
  
  public SmsCbCmasInfo getCmasWarningInfo() {
    return this.mCmasWarningInfo;
  }
  
  public boolean isEmergencyMessage() {
    boolean bool;
    if (this.mPriority == 3) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isEtwsMessage() {
    boolean bool;
    if (this.mEtwsWarningInfo != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isCmasMessage() {
    boolean bool;
    if (this.mCmasWarningInfo != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SmsCbMessage{geographicalScope=");
    stringBuilder.append(this.mGeographicalScope);
    stringBuilder.append(", serialNumber=");
    stringBuilder.append(this.mSerialNumber);
    stringBuilder.append(", location=");
    stringBuilder.append(this.mLocation);
    stringBuilder.append(", serviceCategory=");
    stringBuilder.append(this.mServiceCategory);
    stringBuilder.append(", language=");
    stringBuilder.append(this.mLanguage);
    stringBuilder.append(", body=");
    stringBuilder.append(this.mBody);
    stringBuilder.append(", priority=");
    stringBuilder.append(this.mPriority);
    SmsCbEtwsInfo smsCbEtwsInfo = this.mEtwsWarningInfo;
    String str2 = "";
    if (smsCbEtwsInfo != null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(", ");
      stringBuilder1.append(this.mEtwsWarningInfo.toString());
      str1 = stringBuilder1.toString();
    } else {
      str1 = "";
    } 
    stringBuilder.append(str1);
    String str1 = str2;
    if (this.mCmasWarningInfo != null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(", ");
      stringBuilder1.append(this.mCmasWarningInfo.toString());
      str1 = stringBuilder1.toString();
    } 
    stringBuilder.append(str1);
    stringBuilder.append(", maximumWaitingTime=");
    stringBuilder.append(this.mMaximumWaitTimeSec);
    stringBuilder.append(", received time=");
    stringBuilder.append(this.mReceivedTimeMillis);
    stringBuilder.append(", slotIndex = ");
    stringBuilder.append(this.mSlotIndex);
    stringBuilder.append(", geo=");
    List<CbGeoUtils.Geometry> list = this.mGeometries;
    if (list != null) {
      null = CbGeoUtils.encodeGeometriesToString(list);
    } else {
      null = "null";
    } 
    stringBuilder.append(null);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public ContentValues getContentValues() {
    ContentValues contentValues = new ContentValues(16);
    contentValues.put("slot_index", Integer.valueOf(this.mSlotIndex));
    contentValues.put("sub_id", Integer.valueOf(this.mSubId));
    contentValues.put("geo_scope", Integer.valueOf(this.mGeographicalScope));
    if (this.mLocation.getPlmn() != null)
      contentValues.put("plmn", this.mLocation.getPlmn()); 
    if (this.mLocation.getLac() != -1)
      contentValues.put("lac", Integer.valueOf(this.mLocation.getLac())); 
    if (this.mLocation.getCid() != -1)
      contentValues.put("cid", Integer.valueOf(this.mLocation.getCid())); 
    contentValues.put("serial_number", Integer.valueOf(getSerialNumber()));
    contentValues.put("service_category", Integer.valueOf(getServiceCategory()));
    contentValues.put("language", getLanguageCode());
    contentValues.put("dcs", Integer.valueOf(getDataCodingScheme()));
    contentValues.put("body", getMessageBody());
    contentValues.put("format", Integer.valueOf(getMessageFormat()));
    contentValues.put("priority", Integer.valueOf(getMessagePriority()));
    SmsCbEtwsInfo smsCbEtwsInfo = getEtwsWarningInfo();
    if (smsCbEtwsInfo != null) {
      contentValues.put("etws_warning_type", Integer.valueOf(smsCbEtwsInfo.getWarningType()));
      contentValues.put("etws_is_primary", Boolean.valueOf(smsCbEtwsInfo.isPrimary()));
    } 
    SmsCbCmasInfo smsCbCmasInfo = getCmasWarningInfo();
    if (smsCbCmasInfo != null) {
      contentValues.put("cmas_message_class", Integer.valueOf(smsCbCmasInfo.getMessageClass()));
      contentValues.put("cmas_category", Integer.valueOf(smsCbCmasInfo.getCategory()));
      contentValues.put("cmas_response_type", Integer.valueOf(smsCbCmasInfo.getResponseType()));
      contentValues.put("cmas_severity", Integer.valueOf(smsCbCmasInfo.getSeverity()));
      contentValues.put("cmas_urgency", Integer.valueOf(smsCbCmasInfo.getUrgency()));
      contentValues.put("cmas_certainty", Integer.valueOf(smsCbCmasInfo.getCertainty()));
    } 
    contentValues.put("received_time", Long.valueOf(this.mReceivedTimeMillis));
    List<CbGeoUtils.Geometry> list = this.mGeometries;
    if (list != null) {
      contentValues.put("geometries", CbGeoUtils.encodeGeometriesToString(list));
    } else {
      contentValues.put("geometries", (String)null);
    } 
    contentValues.put("maximum_wait_time", Integer.valueOf(this.mMaximumWaitTimeSec));
    return contentValues;
  }
  
  public static SmsCbMessage createFromCursor(Cursor paramCursor) {
    String str3;
    SmsCbCmasInfo smsCbCmasInfo;
    int i = paramCursor.getColumnIndexOrThrow("geo_scope");
    int j = paramCursor.getInt(i);
    int k = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("serial_number"));
    int m = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("service_category"));
    i = paramCursor.getColumnIndexOrThrow("language");
    String str1 = paramCursor.getString(i);
    String str2 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("body"));
    int n = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("format"));
    int i1 = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("priority"));
    int i2 = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("slot_index"));
    int i3 = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("sub_id"));
    i = paramCursor.getColumnIndex("plmn");
    if (i != -1 && !paramCursor.isNull(i)) {
      str3 = paramCursor.getString(i);
    } else {
      str3 = null;
    } 
    i = paramCursor.getColumnIndex("lac");
    if (i != -1 && !paramCursor.isNull(i)) {
      i = paramCursor.getInt(i);
    } else {
      i = -1;
    } 
    int i4 = paramCursor.getColumnIndex("cid");
    if (i4 != -1 && !paramCursor.isNull(i4)) {
      i4 = paramCursor.getInt(i4);
    } else {
      i4 = -1;
    } 
    SmsCbLocation smsCbLocation = new SmsCbLocation(str3, i, i4);
    i4 = paramCursor.getColumnIndex("etws_warning_type");
    i = paramCursor.getColumnIndex("etws_is_primary");
    if (i4 != -1 && !paramCursor.isNull(i4) && i != -1 && 
      !paramCursor.isNull(i)) {
      boolean bool;
      i4 = paramCursor.getInt(i4);
      if (paramCursor.getInt(i) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      SmsCbEtwsInfo smsCbEtwsInfo = new SmsCbEtwsInfo(i4, false, false, bool, null);
    } else {
      str3 = null;
    } 
    i = paramCursor.getColumnIndex("cmas_message_class");
    if (i != -1 && !paramCursor.isNull(i)) {
      int i5 = paramCursor.getInt(i);
      i = paramCursor.getColumnIndex("cmas_category");
      if (i != -1 && !paramCursor.isNull(i)) {
        i = paramCursor.getInt(i);
      } else {
        i = -1;
      } 
      i4 = paramCursor.getColumnIndex("cmas_response_type");
      if (i4 != -1 && !paramCursor.isNull(i4)) {
        i4 = paramCursor.getInt(i4);
      } else {
        i4 = -1;
      } 
      int i6 = paramCursor.getColumnIndex("cmas_severity");
      if (i6 != -1 && !paramCursor.isNull(i6)) {
        i6 = paramCursor.getInt(i6);
      } else {
        i6 = -1;
      } 
      int i7 = paramCursor.getColumnIndex("cmas_urgency");
      if (i7 != -1 && !paramCursor.isNull(i7)) {
        i7 = paramCursor.getInt(i7);
      } else {
        i7 = -1;
      } 
      int i8 = paramCursor.getColumnIndex("cmas_certainty");
      if (i8 != -1 && !paramCursor.isNull(i8)) {
        i8 = paramCursor.getInt(i8);
      } else {
        i8 = -1;
      } 
      smsCbCmasInfo = new SmsCbCmasInfo(i5, i, i4, i6, i7, i8);
    } else {
      smsCbCmasInfo = null;
    } 
    String str4 = paramCursor.getString(paramCursor.getColumnIndex("geometries"));
    if (str4 != null) {
      List<CbGeoUtils.Geometry> list = CbGeoUtils.parseGeometriesFromString(str4);
    } else {
      str4 = null;
    } 
    i = paramCursor.getColumnIndexOrThrow("received_time");
    long l = paramCursor.getLong(i);
    i = paramCursor.getColumnIndexOrThrow("maximum_wait_time");
    i = paramCursor.getInt(i);
    return new SmsCbMessage(n, j, k, smsCbLocation, m, str1, 0, str2, i1, (SmsCbEtwsInfo)str3, smsCbCmasInfo, i, (List<CbGeoUtils.Geometry>)str4, l, i2, i3);
  }
  
  public boolean needGeoFencingCheck() {
    if (this.mMaximumWaitTimeSec > 0) {
      List<CbGeoUtils.Geometry> list = this.mGeometries;
      if (list != null && !list.isEmpty())
        return true; 
    } 
    return false;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class GeographicalScope implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class MessageFormat implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class MessagePriority implements Annotation {}
}
