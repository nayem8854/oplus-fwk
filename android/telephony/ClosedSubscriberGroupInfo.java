package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public final class ClosedSubscriberGroupInfo implements Parcelable {
  public ClosedSubscriberGroupInfo(boolean paramBoolean, String paramString, int paramInt) {
    this.mCsgIndicator = paramBoolean;
    if (paramString == null)
      paramString = ""; 
    this.mHomeNodebName = paramString;
    this.mCsgIdentity = paramInt;
  }
  
  public ClosedSubscriberGroupInfo(android.hardware.radio.V1_5.ClosedSubscriberGroupInfo paramClosedSubscriberGroupInfo) {
    this(paramClosedSubscriberGroupInfo.csgIndication, paramClosedSubscriberGroupInfo.homeNodebName, paramClosedSubscriberGroupInfo.csgIdentity);
  }
  
  public boolean getCsgIndicator() {
    return this.mCsgIndicator;
  }
  
  public String getHomeNodebName() {
    return this.mHomeNodebName;
  }
  
  public int getCsgIdentity() {
    return this.mCsgIdentity;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Boolean.valueOf(this.mCsgIndicator), this.mHomeNodebName, Integer.valueOf(this.mCsgIdentity) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof ClosedSubscriberGroupInfo;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    bool = bool1;
    if (this.mCsgIndicator == ((ClosedSubscriberGroupInfo)paramObject).mCsgIndicator) {
      bool = bool1;
      if (((ClosedSubscriberGroupInfo)paramObject).mHomeNodebName.equals(this.mHomeNodebName)) {
        bool = bool1;
        if (this.mCsgIdentity == ((ClosedSubscriberGroupInfo)paramObject).mCsgIdentity)
          bool = true; 
      } 
    } 
    return bool;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("ClosedSubscriberGroupInfo:{");
    stringBuilder.append(" mCsgIndicator = ");
    stringBuilder.append(this.mCsgIndicator);
    stringBuilder.append(" mHomeNodebName = ");
    stringBuilder.append(this.mHomeNodebName);
    stringBuilder.append(" mCsgIdentity = ");
    stringBuilder.append(this.mCsgIdentity);
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeBoolean(this.mCsgIndicator);
    paramParcel.writeString(this.mHomeNodebName);
    paramParcel.writeInt(this.mCsgIdentity);
  }
  
  private ClosedSubscriberGroupInfo(Parcel paramParcel) {
    this(paramParcel.readBoolean(), paramParcel.readString(), paramParcel.readInt());
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<ClosedSubscriberGroupInfo> CREATOR = new Parcelable.Creator<ClosedSubscriberGroupInfo>() {
      public ClosedSubscriberGroupInfo createFromParcel(Parcel param1Parcel) {
        return ClosedSubscriberGroupInfo.createFromParcelBody(param1Parcel);
      }
      
      public ClosedSubscriberGroupInfo[] newArray(int param1Int) {
        return new ClosedSubscriberGroupInfo[param1Int];
      }
    };
  
  private static final String TAG = "ClosedSubscriberGroupInfo";
  
  private final int mCsgIdentity;
  
  private final boolean mCsgIndicator;
  
  private final String mHomeNodebName;
  
  protected static ClosedSubscriberGroupInfo createFromParcelBody(Parcel paramParcel) {
    return new ClosedSubscriberGroupInfo(paramParcel);
  }
}
