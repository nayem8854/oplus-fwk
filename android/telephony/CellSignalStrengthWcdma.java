package android.telephony;

import android.hardware.radio.V1_0.WcdmaSignalStrength;
import android.hardware.radio.V1_2.WcdmaSignalStrength;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PersistableBundle;
import com.android.telephony.Rlog;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;

public final class CellSignalStrengthWcdma extends CellSignalStrength implements Parcelable {
  public static final Parcelable.Creator<CellSignalStrengthWcdma> CREATOR;
  
  private static final boolean DBG = false;
  
  private static final String DEFAULT_LEVEL_CALCULATION_METHOD = "rssi";
  
  public static final String LEVEL_CALCULATION_METHOD_RSCP = "rscp";
  
  public static final String LEVEL_CALCULATION_METHOD_RSSI = "rssi";
  
  private static final String LOG_TAG = "CellSignalStrengthWcdma";
  
  private static final int WCDMA_RSCP_GOOD = -95;
  
  private static final int WCDMA_RSCP_GREAT = -85;
  
  private static final int WCDMA_RSCP_MAX = -24;
  
  private static final int WCDMA_RSCP_MIN = -120;
  
  private static final int WCDMA_RSCP_MODERATE = -105;
  
  private static final int WCDMA_RSCP_POOR = -115;
  
  private static final int WCDMA_RSSI_GOOD = -87;
  
  private static final int WCDMA_RSSI_GREAT = -77;
  
  private static final int WCDMA_RSSI_MAX = -51;
  
  private static final int WCDMA_RSSI_MIN = -113;
  
  private static final int WCDMA_RSSI_MODERATE = -97;
  
  private static final int WCDMA_RSSI_POOR = -107;
  
  private static final CellSignalStrengthWcdma sInvalid;
  
  private static final int[] sRscpThresholds;
  
  private static final int[] sRssiThresholds = new int[] { -107, -97, -87, -77 };
  
  private int mBitErrorRate;
  
  private int mEcNo;
  
  private int mLevel;
  
  private int mRscp;
  
  private int mRssi;
  
  static {
    sRscpThresholds = new int[] { -115, -105, -95, -85 };
    sInvalid = new CellSignalStrengthWcdma();
    CREATOR = new Parcelable.Creator<CellSignalStrengthWcdma>() {
        public CellSignalStrengthWcdma createFromParcel(Parcel param1Parcel) {
          return new CellSignalStrengthWcdma(param1Parcel);
        }
        
        public CellSignalStrengthWcdma[] newArray(int param1Int) {
          return new CellSignalStrengthWcdma[param1Int];
        }
      };
  }
  
  public CellSignalStrengthWcdma() {
    setDefaultValues();
  }
  
  public CellSignalStrengthWcdma(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mRssi = inRangeOrUnavailable(paramInt1, -113, -51);
    this.mBitErrorRate = inRangeOrUnavailable(paramInt2, 0, 7, 99);
    this.mRscp = inRangeOrUnavailable(paramInt3, -120, -24);
    this.mEcNo = inRangeOrUnavailable(paramInt4, -24, 1);
    updateLevel((PersistableBundle)null, (ServiceState)null);
  }
  
  public CellSignalStrengthWcdma(WcdmaSignalStrength paramWcdmaSignalStrength) {
    this(getRssiDbmFromAsu(paramWcdmaSignalStrength.signalStrength), paramWcdmaSignalStrength.bitErrorRate, 2147483647, 2147483647);
    if (this.mRssi == Integer.MAX_VALUE && this.mRscp == Integer.MAX_VALUE)
      setDefaultValues(); 
  }
  
  public CellSignalStrengthWcdma(WcdmaSignalStrength paramWcdmaSignalStrength) {
    this(i, j, k, m);
    if (this.mRssi == Integer.MAX_VALUE && this.mRscp == Integer.MAX_VALUE)
      setDefaultValues(); 
  }
  
  public CellSignalStrengthWcdma(CellSignalStrengthWcdma paramCellSignalStrengthWcdma) {
    copyFrom(paramCellSignalStrengthWcdma);
  }
  
  protected void copyFrom(CellSignalStrengthWcdma paramCellSignalStrengthWcdma) {
    this.mRssi = paramCellSignalStrengthWcdma.mRssi;
    this.mBitErrorRate = paramCellSignalStrengthWcdma.mBitErrorRate;
    this.mRscp = paramCellSignalStrengthWcdma.mRscp;
    this.mEcNo = paramCellSignalStrengthWcdma.mEcNo;
    this.mLevel = paramCellSignalStrengthWcdma.mLevel;
  }
  
  public CellSignalStrengthWcdma copy() {
    return new CellSignalStrengthWcdma(this);
  }
  
  public void setDefaultValues() {
    this.mRssi = Integer.MAX_VALUE;
    this.mBitErrorRate = Integer.MAX_VALUE;
    this.mRscp = Integer.MAX_VALUE;
    this.mEcNo = Integer.MAX_VALUE;
    this.mLevel = 0;
  }
  
  public int getLevel() {
    return this.mLevel;
  }
  
  public void updateLevel(PersistableBundle paramPersistableBundle, ServiceState paramServiceState) {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull -> 14
    //   4: ldc 'rssi'
    //   6: astore_3
    //   7: getstatic android/telephony/CellSignalStrengthWcdma.sRscpThresholds : [I
    //   10: astore_1
    //   11: goto -> 66
    //   14: aload_1
    //   15: ldc 'wcdma_default_signal_strength_measurement_string'
    //   17: ldc 'rssi'
    //   19: invokevirtual getString : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   22: astore_3
    //   23: aload_3
    //   24: astore_2
    //   25: aload_3
    //   26: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   29: ifeq -> 35
    //   32: ldc 'rssi'
    //   34: astore_2
    //   35: aload_1
    //   36: ldc 'wcdma_rscp_thresholds_int_array'
    //   38: invokevirtual getIntArray : (Ljava/lang/String;)[I
    //   41: astore #4
    //   43: aload #4
    //   45: ifnull -> 60
    //   48: aload_2
    //   49: astore_3
    //   50: aload #4
    //   52: astore_1
    //   53: aload #4
    //   55: arraylength
    //   56: iconst_4
    //   57: if_icmpeq -> 66
    //   60: getstatic android/telephony/CellSignalStrengthWcdma.sRscpThresholds : [I
    //   63: astore_1
    //   64: aload_2
    //   65: astore_3
    //   66: iconst_4
    //   67: istore #5
    //   69: aload_3
    //   70: invokevirtual hashCode : ()I
    //   73: istore #6
    //   75: iload #6
    //   77: ldc_w 3509870
    //   80: if_icmpeq -> 109
    //   83: iload #6
    //   85: ldc_w 3510359
    //   88: if_icmpeq -> 94
    //   91: goto -> 124
    //   94: aload_3
    //   95: ldc 'rssi'
    //   97: invokevirtual equals : (Ljava/lang/Object;)Z
    //   100: ifeq -> 91
    //   103: iconst_2
    //   104: istore #6
    //   106: goto -> 127
    //   109: aload_3
    //   110: ldc 'rscp'
    //   112: invokevirtual equals : (Ljava/lang/Object;)Z
    //   115: ifeq -> 91
    //   118: iconst_0
    //   119: istore #6
    //   121: goto -> 127
    //   124: iconst_m1
    //   125: istore #6
    //   127: iload #6
    //   129: ifeq -> 229
    //   132: iload #6
    //   134: iconst_2
    //   135: if_icmpeq -> 167
    //   138: new java/lang/StringBuilder
    //   141: dup
    //   142: invokespecial <init> : ()V
    //   145: astore_1
    //   146: aload_1
    //   147: ldc_w 'Invalid Level Calculation Method for CellSignalStrengthWcdma = '
    //   150: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   153: pop
    //   154: aload_1
    //   155: aload_3
    //   156: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   159: pop
    //   160: aload_1
    //   161: invokevirtual toString : ()Ljava/lang/String;
    //   164: invokestatic loge : (Ljava/lang/String;)V
    //   167: aload_0
    //   168: getfield mRssi : I
    //   171: istore #6
    //   173: iload #6
    //   175: bipush #-113
    //   177: if_icmplt -> 223
    //   180: iload #6
    //   182: bipush #-51
    //   184: if_icmple -> 190
    //   187: goto -> 223
    //   190: iload #5
    //   192: ifle -> 216
    //   195: aload_0
    //   196: getfield mRssi : I
    //   199: getstatic android/telephony/CellSignalStrengthWcdma.sRssiThresholds : [I
    //   202: iload #5
    //   204: iconst_1
    //   205: isub
    //   206: iaload
    //   207: if_icmpge -> 216
    //   210: iinc #5, -1
    //   213: goto -> 190
    //   216: aload_0
    //   217: iload #5
    //   219: putfield mLevel : I
    //   222: return
    //   223: aload_0
    //   224: iconst_0
    //   225: putfield mLevel : I
    //   228: return
    //   229: aload_0
    //   230: getfield mRscp : I
    //   233: istore #6
    //   235: iload #6
    //   237: bipush #-120
    //   239: if_icmplt -> 283
    //   242: iload #6
    //   244: bipush #-24
    //   246: if_icmple -> 252
    //   249: goto -> 283
    //   252: iload #5
    //   254: ifle -> 276
    //   257: aload_0
    //   258: getfield mRscp : I
    //   261: aload_1
    //   262: iload #5
    //   264: iconst_1
    //   265: isub
    //   266: iaload
    //   267: if_icmpge -> 276
    //   270: iinc #5, -1
    //   273: goto -> 252
    //   276: aload_0
    //   277: iload #5
    //   279: putfield mLevel : I
    //   282: return
    //   283: aload_0
    //   284: iconst_0
    //   285: putfield mLevel : I
    //   288: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #164	-> 0
    //   #165	-> 4
    //   #166	-> 7
    //   #169	-> 14
    //   #172	-> 23
    //   #173	-> 35
    //   #175	-> 43
    //   #176	-> 60
    //   #180	-> 66
    //   #181	-> 69
    //   #191	-> 138
    //   #195	-> 167
    //   #199	-> 190
    //   #200	-> 216
    //   #201	-> 222
    //   #196	-> 223
    //   #197	-> 228
    //   #183	-> 229
    //   #187	-> 252
    //   #188	-> 276
    //   #189	-> 282
    //   #184	-> 283
    //   #185	-> 288
  }
  
  public int getDbm() {
    int i = this.mRscp;
    if (i != Integer.MAX_VALUE)
      return i; 
    return this.mRssi;
  }
  
  public int getAsuLevel() {
    int i = this.mRscp;
    if (i != Integer.MAX_VALUE)
      return getAsuFromRscpDbm(i); 
    i = this.mRssi;
    if (i != Integer.MAX_VALUE)
      return getAsuFromRssiDbm(i); 
    return getAsuFromRscpDbm(2147483647);
  }
  
  public int getRssi() {
    return this.mRssi;
  }
  
  public int getRscp() {
    return this.mRscp;
  }
  
  public int getEcNo() {
    return this.mEcNo;
  }
  
  public int getBitErrorRate() {
    return this.mBitErrorRate;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mRssi), Integer.valueOf(this.mBitErrorRate), Integer.valueOf(this.mRscp), Integer.valueOf(this.mEcNo), Integer.valueOf(this.mLevel) });
  }
  
  public boolean isValid() {
    return equals(sInvalid) ^ true;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof CellSignalStrengthWcdma;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    bool = bool1;
    if (this.mRssi == ((CellSignalStrengthWcdma)paramObject).mRssi) {
      bool = bool1;
      if (this.mBitErrorRate == ((CellSignalStrengthWcdma)paramObject).mBitErrorRate) {
        bool = bool1;
        if (this.mRscp == ((CellSignalStrengthWcdma)paramObject).mRscp) {
          bool = bool1;
          if (this.mEcNo == ((CellSignalStrengthWcdma)paramObject).mEcNo) {
            bool = bool1;
            if (this.mLevel == ((CellSignalStrengthWcdma)paramObject).mLevel)
              bool = true; 
          } 
        } 
      } 
    } 
    return bool;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CellSignalStrengthWcdma: ss=");
    stringBuilder.append(this.mRssi);
    stringBuilder.append(" ber=");
    stringBuilder.append(this.mBitErrorRate);
    stringBuilder.append(" rscp=");
    stringBuilder.append(this.mRscp);
    stringBuilder.append(" ecno=");
    stringBuilder.append(this.mEcNo);
    stringBuilder.append(" level=");
    stringBuilder.append(this.mLevel);
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mRssi);
    paramParcel.writeInt(this.mBitErrorRate);
    paramParcel.writeInt(this.mRscp);
    paramParcel.writeInt(this.mEcNo);
    paramParcel.writeInt(this.mLevel);
  }
  
  private CellSignalStrengthWcdma(Parcel paramParcel) {
    this.mRssi = paramParcel.readInt();
    this.mBitErrorRate = paramParcel.readInt();
    this.mRscp = paramParcel.readInt();
    this.mEcNo = paramParcel.readInt();
    this.mLevel = paramParcel.readInt();
  }
  
  public int describeContents() {
    return 0;
  }
  
  private static void log(String paramString) {
    Rlog.w("CellSignalStrengthWcdma", paramString);
  }
  
  private static void loge(String paramString) {
    Rlog.e("CellSignalStrengthWcdma", paramString);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class LevelCalculationMethod implements Annotation {}
}
