package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public final class UssdResponse implements Parcelable {
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mUssdRequest);
    TextUtils.writeToParcel(this.mReturnMessage, paramParcel, 0);
  }
  
  public String getUssdRequest() {
    return this.mUssdRequest;
  }
  
  public CharSequence getReturnMessage() {
    return this.mReturnMessage;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public UssdResponse(String paramString, CharSequence paramCharSequence) {
    this.mUssdRequest = paramString;
    this.mReturnMessage = paramCharSequence;
  }
  
  public static final Parcelable.Creator<UssdResponse> CREATOR = new Parcelable.Creator<UssdResponse>() {
      public UssdResponse createFromParcel(Parcel param1Parcel) {
        String str = param1Parcel.readString();
        CharSequence charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel);
        return new UssdResponse(str, charSequence);
      }
      
      public UssdResponse[] newArray(int param1Int) {
        return new UssdResponse[param1Int];
      }
    };
  
  private CharSequence mReturnMessage;
  
  private String mUssdRequest;
}
