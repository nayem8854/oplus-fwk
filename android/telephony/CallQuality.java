package android.telephony;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;

@SystemApi
public final class CallQuality implements Parcelable {
  public static final int CALL_QUALITY_BAD = 4;
  
  public static final int CALL_QUALITY_EXCELLENT = 0;
  
  public static final int CALL_QUALITY_FAIR = 2;
  
  public static final int CALL_QUALITY_GOOD = 1;
  
  public static final int CALL_QUALITY_NOT_AVAILABLE = 5;
  
  public static final int CALL_QUALITY_POOR = 3;
  
  public CallQuality(Parcel paramParcel) {
    this.mDownlinkCallQualityLevel = paramParcel.readInt();
    this.mUplinkCallQualityLevel = paramParcel.readInt();
    this.mCallDuration = paramParcel.readInt();
    this.mNumRtpPacketsTransmitted = paramParcel.readInt();
    this.mNumRtpPacketsReceived = paramParcel.readInt();
    this.mNumRtpPacketsTransmittedLost = paramParcel.readInt();
    this.mNumRtpPacketsNotReceived = paramParcel.readInt();
    this.mAverageRelativeJitter = paramParcel.readInt();
    this.mMaxRelativeJitter = paramParcel.readInt();
    this.mAverageRoundTripTime = paramParcel.readInt();
    this.mCodecType = paramParcel.readInt();
    this.mRtpInactivityDetected = paramParcel.readBoolean();
    this.mRxSilenceDetected = paramParcel.readBoolean();
    this.mTxSilenceDetected = paramParcel.readBoolean();
  }
  
  public CallQuality() {}
  
  public CallQuality(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10, int paramInt11) {
    this(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, paramInt9, paramInt10, paramInt11, false, false, false);
  }
  
  public CallQuality(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10, int paramInt11, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    this.mDownlinkCallQualityLevel = paramInt1;
    this.mUplinkCallQualityLevel = paramInt2;
    this.mCallDuration = paramInt3;
    this.mNumRtpPacketsTransmitted = paramInt4;
    this.mNumRtpPacketsReceived = paramInt5;
    this.mNumRtpPacketsTransmittedLost = paramInt6;
    this.mNumRtpPacketsNotReceived = paramInt7;
    this.mAverageRelativeJitter = paramInt8;
    this.mMaxRelativeJitter = paramInt9;
    this.mAverageRoundTripTime = paramInt10;
    this.mCodecType = paramInt11;
    this.mRtpInactivityDetected = paramBoolean1;
    this.mRxSilenceDetected = paramBoolean2;
    this.mTxSilenceDetected = paramBoolean3;
  }
  
  public int getDownlinkCallQualityLevel() {
    return this.mDownlinkCallQualityLevel;
  }
  
  public int getUplinkCallQualityLevel() {
    return this.mUplinkCallQualityLevel;
  }
  
  public int getCallDuration() {
    return this.mCallDuration;
  }
  
  public int getNumRtpPacketsTransmitted() {
    return this.mNumRtpPacketsTransmitted;
  }
  
  public int getNumRtpPacketsReceived() {
    return this.mNumRtpPacketsReceived;
  }
  
  public int getNumRtpPacketsTransmittedLost() {
    return this.mNumRtpPacketsTransmittedLost;
  }
  
  public int getNumRtpPacketsNotReceived() {
    return this.mNumRtpPacketsNotReceived;
  }
  
  public int getAverageRelativeJitter() {
    return this.mAverageRelativeJitter;
  }
  
  public int getMaxRelativeJitter() {
    return this.mMaxRelativeJitter;
  }
  
  public int getAverageRoundTripTime() {
    return this.mAverageRoundTripTime;
  }
  
  public boolean isRtpInactivityDetected() {
    return this.mRtpInactivityDetected;
  }
  
  public boolean isIncomingSilenceDetectedAtCallSetup() {
    return this.mRxSilenceDetected;
  }
  
  public boolean isOutgoingSilenceDetectedAtCallSetup() {
    return this.mTxSilenceDetected;
  }
  
  public int getCodecType() {
    return this.mCodecType;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CallQuality: {downlinkCallQualityLevel=");
    stringBuilder.append(this.mDownlinkCallQualityLevel);
    stringBuilder.append(" uplinkCallQualityLevel=");
    stringBuilder.append(this.mUplinkCallQualityLevel);
    stringBuilder.append(" callDuration=");
    stringBuilder.append(this.mCallDuration);
    stringBuilder.append(" numRtpPacketsTransmitted=");
    stringBuilder.append(this.mNumRtpPacketsTransmitted);
    stringBuilder.append(" numRtpPacketsReceived=");
    stringBuilder.append(this.mNumRtpPacketsReceived);
    stringBuilder.append(" numRtpPacketsTransmittedLost=");
    stringBuilder.append(this.mNumRtpPacketsTransmittedLost);
    stringBuilder.append(" numRtpPacketsNotReceived=");
    stringBuilder.append(this.mNumRtpPacketsNotReceived);
    stringBuilder.append(" averageRelativeJitter=");
    stringBuilder.append(this.mAverageRelativeJitter);
    stringBuilder.append(" maxRelativeJitter=");
    stringBuilder.append(this.mMaxRelativeJitter);
    stringBuilder.append(" averageRoundTripTime=");
    stringBuilder.append(this.mAverageRoundTripTime);
    stringBuilder.append(" codecType=");
    stringBuilder.append(this.mCodecType);
    stringBuilder.append(" rtpInactivityDetected=");
    stringBuilder.append(this.mRtpInactivityDetected);
    stringBuilder.append(" txSilenceDetected=");
    stringBuilder.append(this.mRxSilenceDetected);
    stringBuilder.append(" rxSilenceDetected=");
    stringBuilder.append(this.mTxSilenceDetected);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int hashCode() {
    int i = this.mDownlinkCallQualityLevel;
    int j = this.mUplinkCallQualityLevel;
    int k = this.mCallDuration;
    int m = this.mNumRtpPacketsTransmitted;
    int n = this.mNumRtpPacketsReceived;
    int i1 = this.mNumRtpPacketsTransmittedLost;
    int i2 = this.mNumRtpPacketsNotReceived;
    int i3 = this.mAverageRelativeJitter;
    int i4 = this.mMaxRelativeJitter;
    int i5 = this.mAverageRoundTripTime;
    int i6 = this.mCodecType;
    boolean bool1 = this.mRtpInactivityDetected;
    boolean bool2 = this.mRxSilenceDetected;
    boolean bool3 = this.mTxSilenceDetected;
    return Objects.hash(new Object[] { 
          Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5), 
          Integer.valueOf(i6), Boolean.valueOf(bool1), Boolean.valueOf(bool2), Boolean.valueOf(bool3) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool1 = false;
    if (paramObject == null || !(paramObject instanceof CallQuality) || hashCode() != paramObject.hashCode())
      return false; 
    if (this == paramObject)
      return true; 
    paramObject = paramObject;
    boolean bool2 = bool1;
    if (this.mDownlinkCallQualityLevel == ((CallQuality)paramObject).mDownlinkCallQualityLevel) {
      bool2 = bool1;
      if (this.mUplinkCallQualityLevel == ((CallQuality)paramObject).mUplinkCallQualityLevel) {
        bool2 = bool1;
        if (this.mCallDuration == ((CallQuality)paramObject).mCallDuration) {
          bool2 = bool1;
          if (this.mNumRtpPacketsTransmitted == ((CallQuality)paramObject).mNumRtpPacketsTransmitted) {
            bool2 = bool1;
            if (this.mNumRtpPacketsReceived == ((CallQuality)paramObject).mNumRtpPacketsReceived) {
              bool2 = bool1;
              if (this.mNumRtpPacketsTransmittedLost == ((CallQuality)paramObject).mNumRtpPacketsTransmittedLost) {
                bool2 = bool1;
                if (this.mNumRtpPacketsNotReceived == ((CallQuality)paramObject).mNumRtpPacketsNotReceived) {
                  bool2 = bool1;
                  if (this.mAverageRelativeJitter == ((CallQuality)paramObject).mAverageRelativeJitter) {
                    bool2 = bool1;
                    if (this.mMaxRelativeJitter == ((CallQuality)paramObject).mMaxRelativeJitter) {
                      bool2 = bool1;
                      if (this.mAverageRoundTripTime == ((CallQuality)paramObject).mAverageRoundTripTime) {
                        bool2 = bool1;
                        if (this.mCodecType == ((CallQuality)paramObject).mCodecType) {
                          bool2 = bool1;
                          if (this.mRtpInactivityDetected == ((CallQuality)paramObject).mRtpInactivityDetected) {
                            bool2 = bool1;
                            if (this.mRxSilenceDetected == ((CallQuality)paramObject).mRxSilenceDetected) {
                              bool2 = bool1;
                              if (this.mTxSilenceDetected == ((CallQuality)paramObject).mTxSilenceDetected)
                                bool2 = true; 
                            } 
                          } 
                        } 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
    } 
    return bool2;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mDownlinkCallQualityLevel);
    paramParcel.writeInt(this.mUplinkCallQualityLevel);
    paramParcel.writeInt(this.mCallDuration);
    paramParcel.writeInt(this.mNumRtpPacketsTransmitted);
    paramParcel.writeInt(this.mNumRtpPacketsReceived);
    paramParcel.writeInt(this.mNumRtpPacketsTransmittedLost);
    paramParcel.writeInt(this.mNumRtpPacketsNotReceived);
    paramParcel.writeInt(this.mAverageRelativeJitter);
    paramParcel.writeInt(this.mMaxRelativeJitter);
    paramParcel.writeInt(this.mAverageRoundTripTime);
    paramParcel.writeInt(this.mCodecType);
    paramParcel.writeBoolean(this.mRtpInactivityDetected);
    paramParcel.writeBoolean(this.mRxSilenceDetected);
    paramParcel.writeBoolean(this.mTxSilenceDetected);
  }
  
  public static final Parcelable.Creator<CallQuality> CREATOR = new Parcelable.Creator() {
      public CallQuality createFromParcel(Parcel param1Parcel) {
        return new CallQuality(param1Parcel);
      }
      
      public CallQuality[] newArray(int param1Int) {
        return new CallQuality[param1Int];
      }
    };
  
  private int mAverageRelativeJitter;
  
  private int mAverageRoundTripTime;
  
  private int mCallDuration;
  
  private int mCodecType;
  
  private int mDownlinkCallQualityLevel;
  
  private int mMaxRelativeJitter;
  
  private int mNumRtpPacketsNotReceived;
  
  private int mNumRtpPacketsReceived;
  
  private int mNumRtpPacketsTransmitted;
  
  private int mNumRtpPacketsTransmittedLost;
  
  private boolean mRtpInactivityDetected;
  
  private boolean mRxSilenceDetected;
  
  private boolean mTxSilenceDetected;
  
  private int mUplinkCallQualityLevel;
  
  @Retention(RetentionPolicy.SOURCE)
  class CallQualityLevel implements Annotation {}
}
