package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.util.ArraySet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public final class CellIdentityWcdma extends CellIdentity {
  public static final Parcelable.Creator<CellIdentityWcdma> CREATOR;
  
  private static final boolean DBG = false;
  
  private static final int MAX_CID = 268435455;
  
  private static final int MAX_LAC = 65535;
  
  private static final int MAX_PSC = 511;
  
  private static final int MAX_UARFCN = 16383;
  
  private static final String TAG = CellIdentityWcdma.class.getSimpleName();
  
  private final ArraySet<String> mAdditionalPlmns;
  
  private final int mCid;
  
  private final ClosedSubscriberGroupInfo mCsgInfo;
  
  private final int mLac;
  
  private final int mPsc;
  
  private final int mUarfcn;
  
  public CellIdentityWcdma() {
    super(TAG, 4, null, null, null, null);
    this.mLac = Integer.MAX_VALUE;
    this.mCid = Integer.MAX_VALUE;
    this.mPsc = Integer.MAX_VALUE;
    this.mUarfcn = Integer.MAX_VALUE;
    this.mAdditionalPlmns = new ArraySet<>();
    this.mCsgInfo = null;
    this.mGlobalCellId = null;
  }
  
  public CellIdentityWcdma(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString1, String paramString2, String paramString3, String paramString4, Collection<String> paramCollection, ClosedSubscriberGroupInfo paramClosedSubscriberGroupInfo) {
    super(TAG, 4, paramString1, paramString2, paramString3, paramString4);
    this.mLac = inRangeOrUnavailable(paramInt1, 0, 65535);
    this.mCid = inRangeOrUnavailable(paramInt2, 0, 268435455);
    this.mPsc = inRangeOrUnavailable(paramInt3, 0, 511);
    this.mUarfcn = inRangeOrUnavailable(paramInt4, 0, 16383);
    this.mAdditionalPlmns = new ArraySet<>(paramCollection.size());
    for (String paramString1 : paramCollection) {
      if (isValidPlmn(paramString1))
        this.mAdditionalPlmns.add(paramString1); 
    } 
    this.mCsgInfo = paramClosedSubscriberGroupInfo;
    updateGlobalCellId();
  }
  
  public CellIdentityWcdma(android.hardware.radio.V1_0.CellIdentityWcdma paramCellIdentityWcdma) {
    this(paramCellIdentityWcdma.lac, paramCellIdentityWcdma.cid, paramCellIdentityWcdma.psc, paramCellIdentityWcdma.uarfcn, paramCellIdentityWcdma.mcc, paramCellIdentityWcdma.mnc, "", "", new ArraySet<>(), (ClosedSubscriberGroupInfo)null);
  }
  
  public CellIdentityWcdma(android.hardware.radio.V1_2.CellIdentityWcdma paramCellIdentityWcdma) {
    this(paramCellIdentityWcdma.base.lac, paramCellIdentityWcdma.base.cid, paramCellIdentityWcdma.base.psc, paramCellIdentityWcdma.base.uarfcn, paramCellIdentityWcdma.base.mcc, paramCellIdentityWcdma.base.mnc, paramCellIdentityWcdma.operatorNames.alphaLong, paramCellIdentityWcdma.operatorNames.alphaShort, new ArraySet<>(), (ClosedSubscriberGroupInfo)null);
  }
  
  public CellIdentityWcdma(android.hardware.radio.V1_5.CellIdentityWcdma paramCellIdentityWcdma) {
    this(i, j, k, m, str1, str2, str3, str4, arrayList, (ClosedSubscriberGroupInfo)paramCellIdentityWcdma);
  }
  
  private CellIdentityWcdma(CellIdentityWcdma paramCellIdentityWcdma) {
    this(paramCellIdentityWcdma.mLac, paramCellIdentityWcdma.mCid, paramCellIdentityWcdma.mPsc, paramCellIdentityWcdma.mUarfcn, paramCellIdentityWcdma.mMccStr, paramCellIdentityWcdma.mMncStr, paramCellIdentityWcdma.mAlphaLong, paramCellIdentityWcdma.mAlphaShort, paramCellIdentityWcdma.mAdditionalPlmns, paramCellIdentityWcdma.mCsgInfo);
  }
  
  public CellIdentityWcdma sanitizeLocationInfo() {
    return new CellIdentityWcdma(2147483647, 2147483647, 2147483647, 2147483647, this.mMccStr, this.mMncStr, this.mAlphaLong, this.mAlphaShort, this.mAdditionalPlmns, null);
  }
  
  CellIdentityWcdma copy() {
    return new CellIdentityWcdma(this);
  }
  
  protected void updateGlobalCellId() {
    this.mGlobalCellId = null;
    String str = getPlmn();
    if (str == null)
      return; 
    if (this.mLac == Integer.MAX_VALUE || this.mCid == Integer.MAX_VALUE)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str);
    stringBuilder.append(String.format("%04x%04x", new Object[] { Integer.valueOf(this.mLac), Integer.valueOf(this.mCid) }));
    this.mGlobalCellId = stringBuilder.toString();
  }
  
  @Deprecated
  public int getMcc() {
    int i;
    if (this.mMccStr != null) {
      i = Integer.valueOf(this.mMccStr).intValue();
    } else {
      i = Integer.MAX_VALUE;
    } 
    return i;
  }
  
  @Deprecated
  public int getMnc() {
    int i;
    if (this.mMncStr != null) {
      i = Integer.valueOf(this.mMncStr).intValue();
    } else {
      i = Integer.MAX_VALUE;
    } 
    return i;
  }
  
  public int getLac() {
    return this.mLac;
  }
  
  public int getCid() {
    return this.mCid;
  }
  
  public int getPsc() {
    return this.mPsc;
  }
  
  public String getMccString() {
    return this.mMccStr;
  }
  
  public String getMncString() {
    return this.mMncStr;
  }
  
  public String getMobileNetworkOperator() {
    if (this.mMccStr == null || this.mMncStr == null)
      return null; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.mMccStr);
    stringBuilder.append(this.mMncStr);
    return stringBuilder.toString();
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mLac), Integer.valueOf(this.mCid), Integer.valueOf(this.mPsc), Integer.valueOf(this.mAdditionalPlmns.hashCode()), Integer.valueOf(super.hashCode()) });
  }
  
  public int getUarfcn() {
    return this.mUarfcn;
  }
  
  public int getChannelNumber() {
    return this.mUarfcn;
  }
  
  public Set<String> getAdditionalPlmns() {
    return Collections.unmodifiableSet(this.mAdditionalPlmns);
  }
  
  public ClosedSubscriberGroupInfo getClosedSubscriberGroupInfo() {
    return this.mCsgInfo;
  }
  
  public GsmCellLocation asCellLocation() {
    GsmCellLocation gsmCellLocation = new GsmCellLocation();
    int i = this.mLac, j = -1;
    if (i == Integer.MAX_VALUE)
      i = -1; 
    int k = this.mCid;
    if (k == Integer.MAX_VALUE)
      k = -1; 
    int m = this.mPsc;
    if (m != Integer.MAX_VALUE)
      j = m; 
    gsmCellLocation.setLacAndCid(i, k);
    gsmCellLocation.setPsc(j);
    return gsmCellLocation;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof CellIdentityWcdma))
      return false; 
    CellIdentityWcdma cellIdentityWcdma = (CellIdentityWcdma)paramObject;
    if (this.mLac == cellIdentityWcdma.mLac && this.mCid == cellIdentityWcdma.mCid && this.mPsc == cellIdentityWcdma.mPsc && this.mUarfcn == cellIdentityWcdma.mUarfcn) {
      String str1 = this.mMccStr, str2 = cellIdentityWcdma.mMccStr;
      if (TextUtils.equals(str1, str2)) {
        str1 = this.mMncStr;
        str2 = cellIdentityWcdma.mMncStr;
        if (TextUtils.equals(str1, str2)) {
          ArraySet<String> arraySet1 = this.mAdditionalPlmns, arraySet2 = cellIdentityWcdma.mAdditionalPlmns;
          if (arraySet1.equals(arraySet2)) {
            ClosedSubscriberGroupInfo closedSubscriberGroupInfo2 = this.mCsgInfo, closedSubscriberGroupInfo1 = cellIdentityWcdma.mCsgInfo;
            if (Objects.equals(closedSubscriberGroupInfo2, closedSubscriberGroupInfo1) && 
              super.equals(paramObject))
              return null; 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(TAG);
    stringBuilder.append(":{ mLac=");
    stringBuilder.append(this.mLac);
    stringBuilder.append(" mCid=");
    stringBuilder.append(this.mCid);
    stringBuilder.append(" mPsc=");
    stringBuilder.append(this.mPsc);
    stringBuilder.append(" mUarfcn=");
    stringBuilder.append(this.mUarfcn);
    stringBuilder.append(" mMcc=");
    stringBuilder.append(this.mMccStr);
    stringBuilder.append(" mMnc=");
    stringBuilder.append(this.mMncStr);
    stringBuilder.append(" mAlphaLong=");
    stringBuilder.append(this.mAlphaLong);
    stringBuilder.append(" mAlphaShort=");
    stringBuilder.append(this.mAlphaShort);
    stringBuilder.append(" mAdditionalPlmns=");
    stringBuilder.append(this.mAdditionalPlmns);
    stringBuilder.append(" mCsgInfo=");
    stringBuilder.append(this.mCsgInfo);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    super.writeToParcel(paramParcel, 4);
    paramParcel.writeInt(this.mLac);
    paramParcel.writeInt(this.mCid);
    paramParcel.writeInt(this.mPsc);
    paramParcel.writeInt(this.mUarfcn);
    paramParcel.writeArraySet(this.mAdditionalPlmns);
    paramParcel.writeParcelable(this.mCsgInfo, paramInt);
  }
  
  private CellIdentityWcdma(Parcel paramParcel) {
    super(TAG, 4, paramParcel);
    this.mLac = paramParcel.readInt();
    this.mCid = paramParcel.readInt();
    this.mPsc = paramParcel.readInt();
    this.mUarfcn = paramParcel.readInt();
    this.mAdditionalPlmns = paramParcel.readArraySet(null);
    this.mCsgInfo = (ClosedSubscriberGroupInfo)paramParcel.readParcelable(null);
    updateGlobalCellId();
  }
  
  static {
    CREATOR = (Parcelable.Creator<CellIdentityWcdma>)new Object();
  }
  
  protected static CellIdentityWcdma createFromParcelBody(Parcel paramParcel) {
    return new CellIdentityWcdma(paramParcel);
  }
}
