package android.telephony;

import android.content.res.Resources;
import android.text.TextUtils;
import com.android.internal.telephony.GsmAlphabet;
import com.android.internal.telephony.OplusFeature;
import com.android.internal.telephony.Sms7BitEncodingTranslator;
import com.android.internal.telephony.SmsMessageBase;
import com.android.internal.telephony.util.ReflectionHelper;
import java.util.ArrayList;

public abstract class OplusSmsMessage implements IOplusSmsMessageEx {
  private static final String LOG_TAG = "OplusSmsMessage";
  
  private static final int NUMBER_10 = 10;
  
  public static ArrayList<String> oemFragmentText(String paramString, int paramInt) {
    return SmsMessage.fragmentText(paramString, paramInt);
  }
  
  public static ArrayList<String> oemFragmentText(String paramString, int paramInt1, int paramInt2) {
    boolean bool2;
    GsmAlphabet.TextEncodingDetails textEncodingDetails;
    Class<int> clazz = int.class;
    boolean bool1 = false;
    Object object1 = ReflectionHelper.callDeclaredMethodOrThrow(null, "android.telephony.SmsMessage", "useCdmaFormatForMoSms", new Class[] { clazz }, new Object[] { Integer.valueOf(paramInt1) });
    if (object1 != null) {
      bool2 = ((Boolean)object1).booleanValue();
    } else {
      bool2 = false;
    } 
    if (bool2) {
      textEncodingDetails = com.android.internal.telephony.cdma.OplusSmsMessage.calculateLengthOem(paramString, false, true, paramInt2);
    } else {
      if (paramInt2 == 1)
        bool1 = true; 
      textEncodingDetails = com.android.internal.telephony.gsm.OplusSmsMessage.calculateLengthOem(paramString, bool1, paramInt2);
    } 
    object1 = new StringBuilder();
    object1.append("ted.codeUnitSize=");
    object1.append(textEncodingDetails.codeUnitSize);
    object1.append(" isCdma=");
    object1.append(bool2);
    object1.append(" subid=");
    object1.append(paramInt1);
    Rlog.d("sms", object1.toString());
    if (textEncodingDetails.codeUnitSize == 1) {
      if (textEncodingDetails.languageTable != 0 && textEncodingDetails.languageShiftTable != 0) {
        paramInt1 = 7;
      } else if (textEncodingDetails.languageTable != 0 || textEncodingDetails.languageShiftTable != 0) {
        paramInt1 = 4;
      } else {
        paramInt1 = 0;
      } 
      int k = paramInt1;
      if (textEncodingDetails.msgCount > 1)
        k = paramInt1 + 6; 
      paramInt1 = k;
      if (k != 0)
        paramInt1 = k + 1; 
      paramInt1 = 160 - paramInt1;
    } else if (textEncodingDetails.msgCount > 1) {
      char c = '';
      paramInt1 = c;
      if (!SmsMessage.hasEmsSupport()) {
        paramInt1 = c;
        if (textEncodingDetails.msgCount < 10)
          paramInt1 = 134 - 2; 
      } 
    } else {
      paramInt1 = 140;
    } 
    object1 = null;
    Resources resources = Resources.getSystem();
    if (paramInt2 == 1 || resources.getBoolean(17891537))
      object1 = Sms7BitEncodingTranslator.translate(paramString, bool2); 
    Object object2 = object1;
    if (TextUtils.isEmpty((CharSequence)object1))
      object2 = paramString; 
    int i = 0;
    int j = object2.length();
    ArrayList<String> arrayList = new ArrayList(textEncodingDetails.msgCount);
    while (i < j) {
      if (textEncodingDetails.codeUnitSize == 1) {
        if (bool2 && textEncodingDetails.msgCount == 1) {
          paramInt2 = Math.min(paramInt1, j - i) + i;
        } else {
          paramInt2 = GsmAlphabet.findGsmSeptetLimitIndex((String)object2, i, paramInt1, textEncodingDetails.languageTable, textEncodingDetails.languageShiftTable);
        } 
      } else {
        paramInt2 = SmsMessageBase.findNextUnicodePosition(i, paramInt1, (CharSequence)object2);
      } 
      if (paramInt2 <= i || paramInt2 > j) {
        object1 = new StringBuilder();
        object1.append("fragmentText failed (");
        object1.append(i);
        object1.append(" >= ");
        object1.append(paramInt2);
        object1.append(" or ");
        object1.append(paramInt2);
        object1.append(" >= ");
        object1.append(j);
        object1.append(")");
        Rlog.e("OplusSmsMessage", object1.toString());
        break;
      } 
      arrayList.add(object2.substring(i, paramInt2));
      i = paramInt2;
    } 
    object1 = new StringBuilder();
    object1.append("size=");
    object1.append(arrayList.size());
    Rlog.d("OplusSmsMessage", object1.toString());
    return arrayList;
  }
  
  public static int[] calculateLengthOem(String paramString, boolean paramBoolean, int paramInt) {
    return calculateLengthOem(paramString, paramBoolean, paramInt);
  }
  
  public static int[] calculateLengthOem(String paramString, boolean paramBoolean, int paramInt1, int paramInt2) {
    return calculateLengthOem(paramString, paramBoolean, paramInt1, paramInt2);
  }
  
  public static int[] calculateLengthOem(CharSequence paramCharSequence, boolean paramBoolean, int paramInt) {
    GsmAlphabet.TextEncodingDetails textEncodingDetails;
    boolean bool;
    if (!OplusFeature.OPLUS_FEATURE_SMS_7BIT16BIT || (paramInt != 1 && paramInt != 3))
      return SmsMessage.calculateLength(paramCharSequence, paramBoolean); 
    Object object = ReflectionHelper.callDeclaredMethodOrThrow(null, "android.telephony.SmsMessage", "useCdmaFormatForMoSms", null, null);
    if (object != null) {
      bool = ((Boolean)object).booleanValue();
    } else {
      bool = false;
    } 
    if (bool) {
      textEncodingDetails = com.android.internal.telephony.cdma.OplusSmsMessage.calculateLengthOem(paramCharSequence, paramBoolean, true, paramInt);
    } else {
      textEncodingDetails = com.android.internal.telephony.gsm.OplusSmsMessage.calculateLengthOem((CharSequence)textEncodingDetails, paramBoolean, paramInt);
    } 
    int i = textEncodingDetails.msgCount;
    int j = textEncodingDetails.codeUnitCount;
    int k = textEncodingDetails.codeUnitsRemaining;
    int m = textEncodingDetails.codeUnitSize;
    int n = textEncodingDetails.languageTable;
    paramInt = textEncodingDetails.languageShiftTable;
    return new int[] { i, j, k, m, n, paramInt };
  }
  
  public static int[] calculateLengthOem(CharSequence paramCharSequence, boolean paramBoolean, int paramInt1, int paramInt2) {
    GsmAlphabet.TextEncodingDetails textEncodingDetails;
    boolean bool;
    if (!OplusFeature.OPLUS_FEATURE_SMS_7BIT16BIT || (paramInt2 != 1 && paramInt2 != 3))
      return SmsMessage.calculateLength(paramCharSequence, paramBoolean, paramInt1); 
    Class<int> clazz = int.class;
    Object object = ReflectionHelper.callDeclaredMethodOrThrow(null, "android.telephony.SmsMessage", "useCdmaFormatForMoSms", new Class[] { clazz }, new Object[] { Integer.valueOf(paramInt1) });
    if (object != null) {
      bool = ((Boolean)object).booleanValue();
    } else {
      bool = false;
    } 
    if (bool) {
      textEncodingDetails = com.android.internal.telephony.cdma.OplusSmsMessage.calculateLengthOem(paramCharSequence, paramBoolean, true, paramInt2);
    } else {
      textEncodingDetails = com.android.internal.telephony.gsm.OplusSmsMessage.calculateLengthOem((CharSequence)textEncodingDetails, paramBoolean, paramInt2);
    } 
    int i = textEncodingDetails.msgCount;
    paramInt1 = textEncodingDetails.codeUnitCount;
    int j = textEncodingDetails.codeUnitsRemaining;
    paramInt2 = textEncodingDetails.codeUnitSize;
    int k = textEncodingDetails.languageTable;
    int m = textEncodingDetails.languageShiftTable;
    return new int[] { i, paramInt1, j, paramInt2, k, m };
  }
  
  public String getDestinationAddress() {
    return getRecipientAddress();
  }
  
  public abstract int getEncodingType();
  
  public abstract String getRecipientAddress();
}
