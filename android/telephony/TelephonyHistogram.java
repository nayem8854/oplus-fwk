package android.telephony;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

@SystemApi
public final class TelephonyHistogram implements Parcelable {
  private static final int ABSENT = 0;
  
  public TelephonyHistogram(int paramInt1, int paramInt2, int paramInt3) {
    if (paramInt3 > 1) {
      this.mCategory = paramInt1;
      this.mId = paramInt2;
      this.mMinTimeMs = Integer.MAX_VALUE;
      this.mMaxTimeMs = 0;
      this.mAverageTimeMs = 0;
      this.mSampleCount = 0;
      this.mInitialTimings = new int[10];
      this.mBucketCount = paramInt3;
      this.mBucketEndPoints = new int[paramInt3 - 1];
      this.mBucketCounters = new int[paramInt3];
      return;
    } 
    throw new IllegalArgumentException("Invalid number of buckets");
  }
  
  public TelephonyHistogram(TelephonyHistogram paramTelephonyHistogram) {
    this.mCategory = paramTelephonyHistogram.getCategory();
    this.mId = paramTelephonyHistogram.getId();
    this.mMinTimeMs = paramTelephonyHistogram.getMinTime();
    this.mMaxTimeMs = paramTelephonyHistogram.getMaxTime();
    this.mAverageTimeMs = paramTelephonyHistogram.getAverageTime();
    this.mSampleCount = paramTelephonyHistogram.getSampleCount();
    this.mInitialTimings = paramTelephonyHistogram.getInitialTimings();
    this.mBucketCount = paramTelephonyHistogram.getBucketCount();
    this.mBucketEndPoints = paramTelephonyHistogram.getBucketEndPoints();
    this.mBucketCounters = paramTelephonyHistogram.getBucketCounters();
  }
  
  public int getCategory() {
    return this.mCategory;
  }
  
  public int getId() {
    return this.mId;
  }
  
  public int getMinTime() {
    return this.mMinTimeMs;
  }
  
  public int getMaxTime() {
    return this.mMaxTimeMs;
  }
  
  public int getAverageTime() {
    return this.mAverageTimeMs;
  }
  
  public int getSampleCount() {
    return this.mSampleCount;
  }
  
  private int[] getInitialTimings() {
    return this.mInitialTimings;
  }
  
  public int getBucketCount() {
    return this.mBucketCount;
  }
  
  public int[] getBucketEndPoints() {
    int i = this.mSampleCount;
    if (i > 1 && i < 10) {
      int[] arrayOfInt = new int[this.mBucketCount - 1];
      calculateBucketEndPoints(arrayOfInt);
      return arrayOfInt;
    } 
    return getDeepCopyOfArray(this.mBucketEndPoints);
  }
  
  public int[] getBucketCounters() {
    int i = this.mSampleCount;
    if (i > 1 && i < 10) {
      i = this.mBucketCount;
      int[] arrayOfInt1 = new int[i - 1];
      int[] arrayOfInt2 = new int[i];
      calculateBucketEndPoints(arrayOfInt1);
      for (i = 0; i < this.mSampleCount; i++)
        addToBucketCounter(arrayOfInt1, arrayOfInt2, this.mInitialTimings[i]); 
      return arrayOfInt2;
    } 
    return getDeepCopyOfArray(this.mBucketCounters);
  }
  
  private int[] getDeepCopyOfArray(int[] paramArrayOfint) {
    int[] arrayOfInt = new int[paramArrayOfint.length];
    System.arraycopy(paramArrayOfint, 0, arrayOfInt, 0, paramArrayOfint.length);
    return arrayOfInt;
  }
  
  private void addToBucketCounter(int[] paramArrayOfint1, int[] paramArrayOfint2, int paramInt) {
    byte b;
    for (b = 0; b < paramArrayOfint1.length; b++) {
      if (paramInt <= paramArrayOfint1[b]) {
        paramArrayOfint2[b] = paramArrayOfint2[b] + 1;
        return;
      } 
    } 
    paramArrayOfint2[b] = paramArrayOfint2[b] + 1;
  }
  
  private void calculateBucketEndPoints(int[] paramArrayOfint) {
    byte b = 1;
    while (true) {
      int i = this.mBucketCount;
      if (b < i) {
        int j = this.mMinTimeMs;
        i = (this.mMaxTimeMs - j) * b / i;
        paramArrayOfint[b - 1] = j + i;
        b++;
        continue;
      } 
      break;
    } 
  }
  
  public void addTimeTaken(int paramInt) {
    int i = this.mSampleCount;
    if (i == 0 || i == Integer.MAX_VALUE) {
      if (this.mSampleCount == 0) {
        this.mMinTimeMs = paramInt;
        this.mMaxTimeMs = paramInt;
        this.mAverageTimeMs = paramInt;
      } else {
        this.mInitialTimings = new int[10];
      } 
      this.mSampleCount = 1;
      Arrays.fill(this.mInitialTimings, 0);
      this.mInitialTimings[0] = paramInt;
      Arrays.fill(this.mBucketEndPoints, 0);
      Arrays.fill(this.mBucketCounters, 0);
      return;
    } 
    if (paramInt < this.mMinTimeMs)
      this.mMinTimeMs = paramInt; 
    if (paramInt > this.mMaxTimeMs)
      this.mMaxTimeMs = paramInt; 
    long l1 = this.mAverageTimeMs;
    i = this.mSampleCount;
    long l2 = i, l3 = paramInt;
    this.mSampleCount = ++i;
    this.mAverageTimeMs = (int)((l1 * l2 + l3) / i);
    if (i < 10) {
      this.mInitialTimings[i - 1] = paramInt;
    } else if (i == 10) {
      this.mInitialTimings[i - 1] = paramInt;
      calculateBucketEndPoints(this.mBucketEndPoints);
      for (paramInt = 0; paramInt < 10; paramInt++)
        addToBucketCounter(this.mBucketEndPoints, this.mBucketCounters, this.mInitialTimings[paramInt]); 
      this.mInitialTimings = null;
    } else {
      addToBucketCounter(this.mBucketEndPoints, this.mBucketCounters, paramInt);
    } 
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(" Histogram id = ");
    stringBuilder1.append(this.mId);
    stringBuilder1.append(" Time(ms): min = ");
    stringBuilder1.append(this.mMinTimeMs);
    stringBuilder1.append(" max = ");
    stringBuilder1.append(this.mMaxTimeMs);
    stringBuilder1.append(" avg = ");
    stringBuilder1.append(this.mAverageTimeMs);
    stringBuilder1.append(" Count = ");
    stringBuilder1.append(this.mSampleCount);
    String str = stringBuilder1.toString();
    if (this.mSampleCount < 10)
      return str; 
    StringBuffer stringBuffer = new StringBuffer(" Interval Endpoints:");
    byte b;
    for (b = 0; b < this.mBucketEndPoints.length; b++) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" ");
      stringBuilder.append(this.mBucketEndPoints[b]);
      stringBuffer.append(stringBuilder.toString());
    } 
    stringBuffer.append(" Interval counters:");
    for (b = 0; b < this.mBucketCounters.length; b++) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" ");
      stringBuilder.append(this.mBucketCounters[b]);
      stringBuffer.append(stringBuilder.toString());
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(str);
    stringBuilder2.append(stringBuffer);
    return stringBuilder2.toString();
  }
  
  public static final Parcelable.Creator<TelephonyHistogram> CREATOR = new Parcelable.Creator<TelephonyHistogram>() {
      public TelephonyHistogram createFromParcel(Parcel param1Parcel) {
        return new TelephonyHistogram(param1Parcel);
      }
      
      public TelephonyHistogram[] newArray(int param1Int) {
        return new TelephonyHistogram[param1Int];
      }
    };
  
  private static final int PRESENT = 1;
  
  private static final int RANGE_CALCULATION_COUNT = 10;
  
  public static final int TELEPHONY_CATEGORY_RIL = 1;
  
  private int mAverageTimeMs;
  
  private final int mBucketCount;
  
  private final int[] mBucketCounters;
  
  private final int[] mBucketEndPoints;
  
  private final int mCategory;
  
  private final int mId;
  
  private int[] mInitialTimings;
  
  private int mMaxTimeMs;
  
  private int mMinTimeMs;
  
  private int mSampleCount;
  
  public TelephonyHistogram(Parcel paramParcel) {
    this.mCategory = paramParcel.readInt();
    this.mId = paramParcel.readInt();
    this.mMinTimeMs = paramParcel.readInt();
    this.mMaxTimeMs = paramParcel.readInt();
    this.mAverageTimeMs = paramParcel.readInt();
    this.mSampleCount = paramParcel.readInt();
    if (paramParcel.readInt() == 1) {
      int[] arrayOfInt1 = new int[10];
      paramParcel.readIntArray(arrayOfInt1);
    } 
    int i = paramParcel.readInt();
    int[] arrayOfInt = new int[i - 1];
    paramParcel.readIntArray(arrayOfInt);
    this.mBucketCounters = arrayOfInt = new int[this.mBucketCount];
    paramParcel.readIntArray(arrayOfInt);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mCategory);
    paramParcel.writeInt(this.mId);
    paramParcel.writeInt(this.mMinTimeMs);
    paramParcel.writeInt(this.mMaxTimeMs);
    paramParcel.writeInt(this.mAverageTimeMs);
    paramParcel.writeInt(this.mSampleCount);
    if (this.mInitialTimings == null) {
      paramParcel.writeInt(0);
    } else {
      paramParcel.writeInt(1);
      paramParcel.writeIntArray(this.mInitialTimings);
    } 
    paramParcel.writeInt(this.mBucketCount);
    paramParcel.writeIntArray(this.mBucketEndPoints);
    paramParcel.writeIntArray(this.mBucketCounters);
  }
  
  public int describeContents() {
    return 0;
  }
}
