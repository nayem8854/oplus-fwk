package android.telephony;

import android.annotation.SystemApi;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.android.telephony.Rlog;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ServiceState implements Parcelable {
  public static final Parcelable.Creator<ServiceState> CREATOR;
  
  static final boolean DBG = false;
  
  public static final int DUPLEX_MODE_FDD = 1;
  
  public static final int DUPLEX_MODE_TDD = 2;
  
  public static final int DUPLEX_MODE_UNKNOWN = 0;
  
  private static final String EXTRA_SERVICE_STATE = "android.intent.extra.SERVICE_STATE";
  
  public static final int FREQUENCY_RANGE_HIGH = 3;
  
  public static final int FREQUENCY_RANGE_LOW = 1;
  
  public static final int FREQUENCY_RANGE_MID = 2;
  
  public static final int FREQUENCY_RANGE_MMWAVE = 4;
  
  private static final List<Integer> FREQUENCY_RANGE_ORDER = Arrays.asList(new Integer[] { Integer.valueOf(0), Integer.valueOf(1), Integer.valueOf(2), Integer.valueOf(3), Integer.valueOf(4) });
  
  public static final int FREQUENCY_RANGE_UNKNOWN = 0;
  
  static final String LOG_TAG = "PHONE";
  
  private static final int NEXT_RIL_RADIO_TECHNOLOGY = 21;
  
  public static final int RIL_RADIO_CDMA_TECHNOLOGY_BITMASK = 6392;
  
  private static final int RIL_RADIO_GSM_TECHNOLOGY_BITMASK = 1042183;
  
  public static final int RIL_RADIO_TECHNOLOGY_1xRTT = 6;
  
  public static final int RIL_RADIO_TECHNOLOGY_EDGE = 2;
  
  public static final int RIL_RADIO_TECHNOLOGY_EHRPD = 13;
  
  public static final int RIL_RADIO_TECHNOLOGY_EVDO_0 = 7;
  
  public static final int RIL_RADIO_TECHNOLOGY_EVDO_A = 8;
  
  public static final int RIL_RADIO_TECHNOLOGY_EVDO_B = 12;
  
  public static final int RIL_RADIO_TECHNOLOGY_GPRS = 1;
  
  public static final int RIL_RADIO_TECHNOLOGY_GSM = 16;
  
  public static final int RIL_RADIO_TECHNOLOGY_HSDPA = 9;
  
  public static final int RIL_RADIO_TECHNOLOGY_HSPA = 11;
  
  public static final int RIL_RADIO_TECHNOLOGY_HSPAP = 15;
  
  public static final int RIL_RADIO_TECHNOLOGY_HSUPA = 10;
  
  public static final int RIL_RADIO_TECHNOLOGY_IS95A = 4;
  
  public static final int RIL_RADIO_TECHNOLOGY_IS95B = 5;
  
  public static final int RIL_RADIO_TECHNOLOGY_IWLAN = 18;
  
  public static final int RIL_RADIO_TECHNOLOGY_LTE = 14;
  
  public static final int RIL_RADIO_TECHNOLOGY_LTE_CA = 19;
  
  public static final int RIL_RADIO_TECHNOLOGY_NR = 20;
  
  public static final int RIL_RADIO_TECHNOLOGY_TD_SCDMA = 17;
  
  public static final int RIL_RADIO_TECHNOLOGY_UMTS = 3;
  
  public static final int RIL_RADIO_TECHNOLOGY_UNKNOWN = 0;
  
  @SystemApi
  public static final int ROAMING_TYPE_DOMESTIC = 2;
  
  @SystemApi
  public static final int ROAMING_TYPE_INTERNATIONAL = 3;
  
  @SystemApi
  public static final int ROAMING_TYPE_NOT_ROAMING = 0;
  
  @SystemApi
  public static final int ROAMING_TYPE_UNKNOWN = 1;
  
  public static final int STATE_EMERGENCY_ONLY = 2;
  
  public static final int STATE_IN_SERVICE = 0;
  
  public static final int STATE_OUT_OF_SERVICE = 1;
  
  public static final int STATE_POWER_OFF = 3;
  
  public static final int UNKNOWN_ID = -1;
  
  static final boolean VDBG = false;
  
  private int mCdmaDefaultRoamingIndicator;
  
  private int mCdmaEriIconIndex;
  
  private int mCdmaEriIconMode;
  
  private int mCdmaRoamingIndicator;
  
  private int[] mCellBandwidths;
  
  private int mChannelNumber;
  
  private boolean mCssIndicator;
  
  private int mDataRegState;
  
  private boolean mIsDataRoamingFromRegistration;
  
  private boolean mIsEmergencyOnly;
  
  private boolean mIsIwlanPreferred;
  
  private boolean mIsManualNetworkSelection;
  
  private int mLteEarfcnRsrpBoost;
  
  private int mNetworkId;
  
  private final List<NetworkRegistrationInfo> mNetworkRegistrationInfos;
  
  private int mNrFrequencyRange;
  
  private String mOperatorAlphaLong;
  
  private String mOperatorAlphaLongRaw;
  
  private String mOperatorAlphaShort;
  
  private String mOperatorAlphaShortRaw;
  
  private String mOperatorNumeric;
  
  private int mSystemId;
  
  private int mVoiceRegState;
  
  public static final String getRoamingLogString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3)
            return "UNKNOWN"; 
          return "International Roaming";
        } 
        return "Domestic Roaming";
      } 
      return "roaming";
    } 
    return "home";
  }
  
  public static ServiceState newFromBundle(Bundle paramBundle) {
    ServiceState serviceState = new ServiceState();
    serviceState.setFromNotifierBundle(paramBundle);
    return serviceState;
  }
  
  public ServiceState() {
    this.mVoiceRegState = 1;
    this.mDataRegState = 1;
    this.mCellBandwidths = new int[0];
    this.mLteEarfcnRsrpBoost = 0;
    this.mNetworkRegistrationInfos = new ArrayList<>();
  }
  
  public ServiceState(ServiceState paramServiceState) {
    this.mVoiceRegState = 1;
    this.mDataRegState = 1;
    this.mCellBandwidths = new int[0];
    this.mLteEarfcnRsrpBoost = 0;
    this.mNetworkRegistrationInfos = new ArrayList<>();
    copyFrom(paramServiceState);
  }
  
  protected void copyFrom(ServiceState paramServiceState) {
    this.mVoiceRegState = paramServiceState.mVoiceRegState;
    this.mDataRegState = paramServiceState.mDataRegState;
    this.mOperatorAlphaLong = paramServiceState.mOperatorAlphaLong;
    this.mOperatorAlphaShort = paramServiceState.mOperatorAlphaShort;
    this.mOperatorNumeric = paramServiceState.mOperatorNumeric;
    this.mIsManualNetworkSelection = paramServiceState.mIsManualNetworkSelection;
    this.mCssIndicator = paramServiceState.mCssIndicator;
    this.mNetworkId = paramServiceState.mNetworkId;
    this.mSystemId = paramServiceState.mSystemId;
    this.mCdmaRoamingIndicator = paramServiceState.mCdmaRoamingIndicator;
    this.mCdmaDefaultRoamingIndicator = paramServiceState.mCdmaDefaultRoamingIndicator;
    this.mCdmaEriIconIndex = paramServiceState.mCdmaEriIconIndex;
    this.mCdmaEriIconMode = paramServiceState.mCdmaEriIconMode;
    this.mIsEmergencyOnly = paramServiceState.mIsEmergencyOnly;
    this.mChannelNumber = paramServiceState.mChannelNumber;
    int[] arrayOfInt = paramServiceState.mCellBandwidths;
    if (arrayOfInt == null) {
      arrayOfInt = null;
    } else {
      arrayOfInt = Arrays.copyOf(arrayOfInt, arrayOfInt.length);
    } 
    this.mCellBandwidths = arrayOfInt;
    this.mLteEarfcnRsrpBoost = paramServiceState.mLteEarfcnRsrpBoost;
    synchronized (this.mNetworkRegistrationInfos) {
      this.mNetworkRegistrationInfos.clear();
      this.mNetworkRegistrationInfos.addAll(paramServiceState.getNetworkRegistrationInfoList());
      this.mNrFrequencyRange = paramServiceState.mNrFrequencyRange;
      this.mOperatorAlphaLongRaw = paramServiceState.mOperatorAlphaLongRaw;
      this.mOperatorAlphaShortRaw = paramServiceState.mOperatorAlphaShortRaw;
      this.mIsDataRoamingFromRegistration = paramServiceState.mIsDataRoamingFromRegistration;
      this.mIsIwlanPreferred = paramServiceState.mIsIwlanPreferred;
      return;
    } 
  }
  
  @Deprecated
  public ServiceState(Parcel paramParcel) {
    boolean bool2, bool1 = true;
    this.mVoiceRegState = 1;
    this.mDataRegState = 1;
    this.mCellBandwidths = new int[0];
    this.mLteEarfcnRsrpBoost = 0;
    this.mNetworkRegistrationInfos = new ArrayList<>();
    this.mVoiceRegState = paramParcel.readInt();
    this.mDataRegState = paramParcel.readInt();
    this.mOperatorAlphaLong = paramParcel.readString();
    this.mOperatorAlphaShort = paramParcel.readString();
    this.mOperatorNumeric = paramParcel.readString();
    if (paramParcel.readInt() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mIsManualNetworkSelection = bool2;
    if (paramParcel.readInt() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mCssIndicator = bool2;
    this.mNetworkId = paramParcel.readInt();
    this.mSystemId = paramParcel.readInt();
    this.mCdmaRoamingIndicator = paramParcel.readInt();
    this.mCdmaDefaultRoamingIndicator = paramParcel.readInt();
    this.mCdmaEriIconIndex = paramParcel.readInt();
    this.mCdmaEriIconMode = paramParcel.readInt();
    if (paramParcel.readInt() != 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.mIsEmergencyOnly = bool2;
    this.mLteEarfcnRsrpBoost = paramParcel.readInt();
    synchronized (this.mNetworkRegistrationInfos) {
      paramParcel.readList(this.mNetworkRegistrationInfos, NetworkRegistrationInfo.class.getClassLoader());
      this.mChannelNumber = paramParcel.readInt();
      this.mCellBandwidths = paramParcel.createIntArray();
      this.mNrFrequencyRange = paramParcel.readInt();
      this.mOperatorAlphaLongRaw = paramParcel.readString();
      this.mOperatorAlphaShortRaw = paramParcel.readString();
      this.mIsDataRoamingFromRegistration = paramParcel.readBoolean();
      this.mIsIwlanPreferred = paramParcel.readBoolean();
      return;
    } 
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mVoiceRegState);
    paramParcel.writeInt(this.mDataRegState);
    paramParcel.writeString(this.mOperatorAlphaLong);
    paramParcel.writeString(this.mOperatorAlphaShort);
    paramParcel.writeString(this.mOperatorNumeric);
    paramParcel.writeInt(this.mIsManualNetworkSelection);
    paramParcel.writeInt(this.mCssIndicator);
    paramParcel.writeInt(this.mNetworkId);
    paramParcel.writeInt(this.mSystemId);
    paramParcel.writeInt(this.mCdmaRoamingIndicator);
    paramParcel.writeInt(this.mCdmaDefaultRoamingIndicator);
    paramParcel.writeInt(this.mCdmaEriIconIndex);
    paramParcel.writeInt(this.mCdmaEriIconMode);
    paramParcel.writeInt(this.mIsEmergencyOnly);
    paramParcel.writeInt(this.mLteEarfcnRsrpBoost);
    synchronized (this.mNetworkRegistrationInfos) {
      paramParcel.writeList(this.mNetworkRegistrationInfos);
      paramParcel.writeInt(this.mChannelNumber);
      paramParcel.writeIntArray(this.mCellBandwidths);
      paramParcel.writeInt(this.mNrFrequencyRange);
      paramParcel.writeString(this.mOperatorAlphaLongRaw);
      paramParcel.writeString(this.mOperatorAlphaShortRaw);
      paramParcel.writeBoolean(this.mIsDataRoamingFromRegistration);
      paramParcel.writeBoolean(this.mIsIwlanPreferred);
      return;
    } 
  }
  
  public int describeContents() {
    return 0;
  }
  
  static {
    CREATOR = new Parcelable.Creator<ServiceState>() {
        public ServiceState createFromParcel(Parcel param1Parcel) {
          return new ServiceState(param1Parcel);
        }
        
        public ServiceState[] newArray(int param1Int) {
          return new ServiceState[param1Int];
        }
      };
  }
  
  public int getState() {
    return getVoiceRegState();
  }
  
  public int getVoiceRegState() {
    return this.mVoiceRegState;
  }
  
  public int getDataRegState() {
    return this.mDataRegState;
  }
  
  public int getDataRegistrationState() {
    return getDataRegState();
  }
  
  public int getDuplexMode() {
    if (!isPsOnlyTech(getRilDataRadioTechnology()))
      return 0; 
    int i = AccessNetworkUtils.getOperatingBandForEarfcn(this.mChannelNumber);
    return AccessNetworkUtils.getDuplexModeForEutranBand(i);
  }
  
  public int getChannelNumber() {
    return this.mChannelNumber;
  }
  
  public int[] getCellBandwidths() {
    int[] arrayOfInt1 = this.mCellBandwidths, arrayOfInt2 = arrayOfInt1;
    if (arrayOfInt1 == null)
      arrayOfInt2 = new int[0]; 
    return arrayOfInt2;
  }
  
  public boolean getRoaming() {
    return (getVoiceRoaming() || getDataRoaming());
  }
  
  public boolean getVoiceRoaming() {
    boolean bool;
    if (getVoiceRoamingType() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getVoiceRoamingType() {
    NetworkRegistrationInfo networkRegistrationInfo = getNetworkRegistrationInfo(1, 1);
    if (networkRegistrationInfo != null)
      return networkRegistrationInfo.getRoamingType(); 
    return 0;
  }
  
  public boolean getDataRoaming() {
    boolean bool;
    if (getDataRoamingType() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setDataRoamingFromRegistration(boolean paramBoolean) {
    this.mIsDataRoamingFromRegistration = paramBoolean;
  }
  
  public boolean getDataRoamingFromRegistration() {
    return this.mIsDataRoamingFromRegistration;
  }
  
  public int getDataRoamingType() {
    NetworkRegistrationInfo networkRegistrationInfo = getNetworkRegistrationInfo(2, 1);
    if (networkRegistrationInfo != null)
      return networkRegistrationInfo.getRoamingType(); 
    return 0;
  }
  
  public boolean isEmergencyOnly() {
    return this.mIsEmergencyOnly;
  }
  
  public int getCdmaRoamingIndicator() {
    return this.mCdmaRoamingIndicator;
  }
  
  public int getCdmaDefaultRoamingIndicator() {
    return this.mCdmaDefaultRoamingIndicator;
  }
  
  public int getCdmaEriIconIndex() {
    return this.mCdmaEriIconIndex;
  }
  
  public int getCdmaEriIconMode() {
    return this.mCdmaEriIconMode;
  }
  
  public String getOperatorAlphaLong() {
    return this.mOperatorAlphaLong;
  }
  
  public String getVoiceOperatorAlphaLong() {
    return this.mOperatorAlphaLong;
  }
  
  public String getOperatorAlphaShort() {
    return this.mOperatorAlphaShort;
  }
  
  public String getVoiceOperatorAlphaShort() {
    return this.mOperatorAlphaShort;
  }
  
  public String getDataOperatorAlphaShort() {
    return this.mOperatorAlphaShort;
  }
  
  public String getOperatorAlpha() {
    if (TextUtils.isEmpty(this.mOperatorAlphaLong))
      return this.mOperatorAlphaShort; 
    return this.mOperatorAlphaLong;
  }
  
  public String getOperatorNumeric() {
    return this.mOperatorNumeric;
  }
  
  public String getVoiceOperatorNumeric() {
    return this.mOperatorNumeric;
  }
  
  public String getDataOperatorNumeric() {
    return this.mOperatorNumeric;
  }
  
  public boolean getIsManualSelection() {
    return this.mIsManualNetworkSelection;
  }
  
  public int hashCode() {
    synchronized (this.mNetworkRegistrationInfos) {
      int i = this.mVoiceRegState;
      int j = this.mDataRegState;
      int k = this.mChannelNumber;
      int[] arrayOfInt = this.mCellBandwidths;
      int m = Arrays.hashCode(arrayOfInt);
      String str1 = this.mOperatorAlphaLong, str2 = this.mOperatorAlphaShort, str3 = this.mOperatorNumeric;
      boolean bool1 = this.mIsManualNetworkSelection;
      boolean bool2 = this.mCssIndicator;
      int n = this.mNetworkId;
      int i1 = this.mSystemId;
      int i2 = this.mCdmaRoamingIndicator;
      int i3 = this.mCdmaDefaultRoamingIndicator;
      int i4 = this.mCdmaEriIconIndex;
      int i5 = this.mCdmaEriIconMode;
      boolean bool3 = this.mIsEmergencyOnly;
      int i6 = this.mLteEarfcnRsrpBoost;
      List<NetworkRegistrationInfo> list = this.mNetworkRegistrationInfos;
      int i7 = this.mNrFrequencyRange;
      String str4 = this.mOperatorAlphaLongRaw, str5 = this.mOperatorAlphaShortRaw;
      boolean bool4 = this.mIsDataRoamingFromRegistration;
      boolean bool5 = this.mIsIwlanPreferred;
      i2 = Objects.hash(new Object[] { 
            Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), str1, str2, str3, Boolean.valueOf(bool1), Boolean.valueOf(bool2), Integer.valueOf(n), 
            Integer.valueOf(i1), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5), Boolean.valueOf(bool3), Integer.valueOf(i6), list, Integer.valueOf(i7), str4, 
            str5, Boolean.valueOf(bool4), Boolean.valueOf(bool5) });
      return i2;
    } 
  }
  
  public boolean equals(Object<NetworkRegistrationInfo> paramObject) {
    boolean bool = paramObject instanceof ServiceState;
    boolean bool1 = false;
    if (!bool)
      return false; 
    null = (ServiceState)paramObject;
    synchronized (this.mNetworkRegistrationInfos) {
      if (this.mVoiceRegState == null.mVoiceRegState && this.mDataRegState == null.mDataRegState && this.mIsManualNetworkSelection == null.mIsManualNetworkSelection && this.mChannelNumber == null.mChannelNumber) {
        int[] arrayOfInt1 = this.mCellBandwidths, arrayOfInt2 = null.mCellBandwidths;
        if (Arrays.equals(arrayOfInt1, arrayOfInt2)) {
          String str1 = this.mOperatorAlphaLong, str2 = null.mOperatorAlphaLong;
          if (equalsHandlesNulls(str1, str2)) {
            str1 = this.mOperatorAlphaShort;
            str2 = null.mOperatorAlphaShort;
            if (equalsHandlesNulls(str1, str2)) {
              str2 = this.mOperatorNumeric;
              str1 = null.mOperatorNumeric;
              if (equalsHandlesNulls(str2, str1)) {
                bool = this.mCssIndicator;
                if (equalsHandlesNulls(Boolean.valueOf(bool), Boolean.valueOf(null.mCssIndicator))) {
                  int i = this.mNetworkId;
                  if (equalsHandlesNulls(Integer.valueOf(i), Integer.valueOf(null.mNetworkId))) {
                    i = this.mSystemId;
                    if (equalsHandlesNulls(Integer.valueOf(i), Integer.valueOf(null.mSystemId))) {
                      i = this.mCdmaRoamingIndicator;
                      if (equalsHandlesNulls(Integer.valueOf(i), Integer.valueOf(null.mCdmaRoamingIndicator))) {
                        i = this.mCdmaDefaultRoamingIndicator;
                        int j = null.mCdmaDefaultRoamingIndicator;
                        if (equalsHandlesNulls(Integer.valueOf(i), Integer.valueOf(j)) && this.mIsEmergencyOnly == null.mIsEmergencyOnly) {
                          str2 = this.mOperatorAlphaLongRaw;
                          str1 = null.mOperatorAlphaLongRaw;
                          if (equalsHandlesNulls(str2, str1)) {
                            str1 = this.mOperatorAlphaShortRaw;
                            str2 = null.mOperatorAlphaShortRaw;
                            if (equalsHandlesNulls(str1, str2)) {
                              List<NetworkRegistrationInfo> list = this.mNetworkRegistrationInfos;
                              if (list.size() == null.mNetworkRegistrationInfos.size()) {
                                List<NetworkRegistrationInfo> list1 = this.mNetworkRegistrationInfos;
                                list = null.mNetworkRegistrationInfos;
                                if (list1.containsAll(list) && this.mNrFrequencyRange == null.mNrFrequencyRange && this.mIsDataRoamingFromRegistration == null.mIsDataRoamingFromRegistration && this.mIsIwlanPreferred == null.mIsIwlanPreferred)
                                  bool1 = true; 
                              } 
                            } 
                          } 
                        } 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
      return bool1;
    } 
  }
  
  public static String roamingTypeToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unknown roaming type ");
            stringBuilder.append(paramInt);
            return stringBuilder.toString();
          } 
          return "INTERNATIONAL";
        } 
        return "DOMESTIC";
      } 
      return "UNKNOWN";
    } 
    return "NOT_ROAMING";
  }
  
  public static String rilRadioTechnologyToString(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        str = "Unexpected";
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unexpected radioTechnology=");
        stringBuilder.append(paramInt);
        Rlog.w("PHONE", stringBuilder.toString());
        return str;
      case 20:
        str = "NR_SA";
        return str;
      case 19:
        str = "LTE_CA";
        return str;
      case 18:
        str = "IWLAN";
        return str;
      case 17:
        str = "TD-SCDMA";
        return str;
      case 16:
        str = "GSM";
        return str;
      case 15:
        str = "HSPAP";
        return str;
      case 14:
        str = "LTE";
        return str;
      case 13:
        str = "eHRPD";
        return str;
      case 12:
        str = "EvDo-rev.B";
        return str;
      case 11:
        str = "HSPA";
        return str;
      case 10:
        str = "HSUPA";
        return str;
      case 9:
        str = "HSDPA";
        return str;
      case 8:
        str = "EvDo-rev.A";
        return str;
      case 7:
        str = "EvDo-rev.0";
        return str;
      case 6:
        str = "1xRTT";
        return str;
      case 5:
        str = "CDMA-IS95B";
        return str;
      case 4:
        str = "CDMA-IS95A";
        return str;
      case 3:
        str = "UMTS";
        return str;
      case 2:
        str = "EDGE";
        return str;
      case 1:
        str = "GPRS";
        return str;
      case 0:
        break;
    } 
    String str = "Unknown";
    return str;
  }
  
  public static String frequencyRangeToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt != 4)
              return Integer.toString(paramInt); 
            return "MMWAVE";
          } 
          return "HIGH";
        } 
        return "MID";
      } 
      return "LOW";
    } 
    return "UNKNOWN";
  }
  
  public static String rilServiceStateToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3)
            return "UNKNOWN"; 
          return "POWER_OFF";
        } 
        return "EMERGENCY_ONLY";
      } 
      return "OUT_OF_SERVICE";
    } 
    return "IN_SERVICE";
  }
  
  public String toString() {
    synchronized (this.mNetworkRegistrationInfos) {
      String str2;
      StringBuilder stringBuilder1 = new StringBuilder();
      this();
      stringBuilder1.append("{mVoiceRegState=");
      stringBuilder1.append(this.mVoiceRegState);
      StringBuilder stringBuilder3 = new StringBuilder();
      this();
      stringBuilder3.append("(");
      int i = this.mVoiceRegState;
      stringBuilder3.append(rilServiceStateToString(i));
      stringBuilder3.append(")");
      stringBuilder1.append(stringBuilder3.toString());
      stringBuilder1.append(", mDataRegState=");
      stringBuilder1.append(this.mDataRegState);
      stringBuilder3 = new StringBuilder();
      this();
      stringBuilder3.append("(");
      i = this.mDataRegState;
      stringBuilder3.append(rilServiceStateToString(i));
      stringBuilder3.append(")");
      stringBuilder1.append(stringBuilder3.toString());
      stringBuilder1.append(", mChannelNumber=");
      stringBuilder1.append(this.mChannelNumber);
      stringBuilder1.append(", duplexMode()=");
      stringBuilder1.append(getDuplexMode());
      stringBuilder1.append(", mCellBandwidths=");
      stringBuilder1.append(Arrays.toString(this.mCellBandwidths));
      stringBuilder1.append(", mOperatorAlphaLong=");
      stringBuilder1.append(this.mOperatorAlphaLong);
      stringBuilder1.append(", mOperatorAlphaShort=");
      stringBuilder1.append(this.mOperatorAlphaShort);
      stringBuilder1.append(", isManualNetworkSelection=");
      stringBuilder1.append(this.mIsManualNetworkSelection);
      if (this.mIsManualNetworkSelection) {
        str2 = "(manual)";
      } else {
        str2 = "(automatic)";
      } 
      stringBuilder1.append(str2);
      stringBuilder1.append(", getRilVoiceRadioTechnology=");
      stringBuilder1.append(getRilVoiceRadioTechnology());
      StringBuilder stringBuilder2 = new StringBuilder();
      this();
      stringBuilder2.append("(");
      stringBuilder2.append(rilRadioTechnologyToString(getRilVoiceRadioTechnology()));
      stringBuilder2.append(")");
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder1.append(", getRilDataRadioTechnology=");
      stringBuilder1.append(getRilDataRadioTechnology());
      stringBuilder2 = new StringBuilder();
      this();
      stringBuilder2.append("(");
      stringBuilder2.append(rilRadioTechnologyToString(getRilDataRadioTechnology()));
      stringBuilder2.append(")");
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder1.append(", mCssIndicator=");
      if (this.mCssIndicator) {
        str1 = "supported";
      } else {
        str1 = "unsupported";
      } 
      stringBuilder1.append(str1);
      stringBuilder1.append(", mNetworkId=");
      stringBuilder1.append(this.mNetworkId);
      stringBuilder1.append(", mSystemId=");
      stringBuilder1.append(this.mSystemId);
      stringBuilder1.append(", mCdmaRoamingIndicator=");
      stringBuilder1.append(this.mCdmaRoamingIndicator);
      stringBuilder1.append(", mCdmaDefaultRoamingIndicator=");
      stringBuilder1.append(this.mCdmaDefaultRoamingIndicator);
      stringBuilder1.append(", mIsEmergencyOnly=");
      stringBuilder1.append(this.mIsEmergencyOnly);
      stringBuilder1.append(", isUsingCarrierAggregation=");
      stringBuilder1.append(isUsingCarrierAggregation());
      stringBuilder1.append(", mLteEarfcnRsrpBoost=");
      stringBuilder1.append(this.mLteEarfcnRsrpBoost);
      stringBuilder1.append(", mNetworkRegistrationInfos=");
      stringBuilder1.append(this.mNetworkRegistrationInfos);
      stringBuilder1.append(", mNrFrequencyRange=");
      stringBuilder1.append(this.mNrFrequencyRange);
      stringBuilder1.append(", mOperatorAlphaLongRaw=");
      stringBuilder1.append(this.mOperatorAlphaLongRaw);
      stringBuilder1.append(", mOperatorAlphaShortRaw=");
      stringBuilder1.append(this.mOperatorAlphaShortRaw);
      stringBuilder1.append(", mIsDataRoamingFromRegistration=");
      boolean bool = this.mIsDataRoamingFromRegistration;
      stringBuilder1.append(bool);
      stringBuilder1.append(", mIsIwlanPreferred=");
      stringBuilder1.append(this.mIsIwlanPreferred);
      stringBuilder1.append("}");
      String str1 = stringBuilder1.toString();
      return str1;
    } 
  }
  
  private void init() {
    this.mVoiceRegState = 1;
    this.mDataRegState = 1;
    this.mChannelNumber = -1;
    this.mCellBandwidths = new int[0];
    this.mOperatorAlphaLong = null;
    this.mOperatorAlphaShort = null;
    this.mOperatorNumeric = null;
    this.mIsManualNetworkSelection = false;
    this.mCssIndicator = false;
    this.mNetworkId = -1;
    this.mSystemId = -1;
    this.mCdmaRoamingIndicator = -1;
    this.mCdmaDefaultRoamingIndicator = -1;
    this.mCdmaEriIconIndex = -1;
    this.mCdmaEriIconMode = -1;
    this.mIsEmergencyOnly = false;
    this.mLteEarfcnRsrpBoost = 0;
    this.mNrFrequencyRange = 0;
    synchronized (this.mNetworkRegistrationInfos) {
      this.mNetworkRegistrationInfos.clear();
      NetworkRegistrationInfo.Builder builder2 = new NetworkRegistrationInfo.Builder();
      this();
      builder2 = builder2.setDomain(1);
      builder2 = builder2.setTransportType(1);
      builder2 = builder2.setRegistrationState(4);
      NetworkRegistrationInfo networkRegistrationInfo2 = builder2.build();
      addNetworkRegistrationInfo(networkRegistrationInfo2);
      NetworkRegistrationInfo.Builder builder1 = new NetworkRegistrationInfo.Builder();
      this();
      builder1 = builder1.setDomain(2);
      builder1 = builder1.setTransportType(1);
      builder1 = builder1.setRegistrationState(4);
      NetworkRegistrationInfo networkRegistrationInfo1 = builder1.build();
      addNetworkRegistrationInfo(networkRegistrationInfo1);
      this.mOperatorAlphaLongRaw = null;
      this.mOperatorAlphaShortRaw = null;
      this.mIsDataRoamingFromRegistration = false;
      this.mIsIwlanPreferred = false;
      return;
    } 
  }
  
  public void setStateOutOfService() {
    init();
  }
  
  public void setStateOff() {
    init();
    this.mVoiceRegState = 3;
    this.mDataRegState = 3;
  }
  
  public void setState(int paramInt) {
    setVoiceRegState(paramInt);
  }
  
  public void setVoiceRegState(int paramInt) {
    this.mVoiceRegState = paramInt;
  }
  
  public void setDataRegState(int paramInt) {
    this.mDataRegState = paramInt;
  }
  
  public void setCellBandwidths(int[] paramArrayOfint) {
    this.mCellBandwidths = paramArrayOfint;
  }
  
  public void setChannelNumber(int paramInt) {
    this.mChannelNumber = paramInt;
  }
  
  public void setRoaming(boolean paramBoolean) {
    setVoiceRoaming(paramBoolean);
    setDataRoaming(paramBoolean);
  }
  
  public void setVoiceRoaming(boolean paramBoolean) {
    setVoiceRoamingType(paramBoolean);
  }
  
  public void setVoiceRoamingType(int paramInt) {
    NetworkRegistrationInfo networkRegistrationInfo1 = getNetworkRegistrationInfo(1, 1);
    NetworkRegistrationInfo networkRegistrationInfo2 = networkRegistrationInfo1;
    if (networkRegistrationInfo1 == null) {
      NetworkRegistrationInfo.Builder builder = new NetworkRegistrationInfo.Builder();
      builder = builder.setDomain(1);
      builder = builder.setTransportType(1);
      networkRegistrationInfo2 = builder.build();
    } 
    networkRegistrationInfo2.setRoamingType(paramInt);
    addNetworkRegistrationInfo(networkRegistrationInfo2);
  }
  
  public void setDataRoaming(boolean paramBoolean) {
    setDataRoamingType(paramBoolean);
  }
  
  public void setDataRoamingType(int paramInt) {
    NetworkRegistrationInfo networkRegistrationInfo1 = getNetworkRegistrationInfo(2, 1);
    NetworkRegistrationInfo networkRegistrationInfo2 = networkRegistrationInfo1;
    if (networkRegistrationInfo1 == null) {
      NetworkRegistrationInfo.Builder builder = new NetworkRegistrationInfo.Builder();
      builder = builder.setDomain(2);
      builder = builder.setTransportType(1);
      networkRegistrationInfo2 = builder.build();
    } 
    networkRegistrationInfo2.setRoamingType(paramInt);
    addNetworkRegistrationInfo(networkRegistrationInfo2);
  }
  
  public void setEmergencyOnly(boolean paramBoolean) {
    this.mIsEmergencyOnly = paramBoolean;
  }
  
  public void setCdmaRoamingIndicator(int paramInt) {
    this.mCdmaRoamingIndicator = paramInt;
  }
  
  public void setCdmaDefaultRoamingIndicator(int paramInt) {
    this.mCdmaDefaultRoamingIndicator = paramInt;
  }
  
  public void setCdmaEriIconIndex(int paramInt) {
    this.mCdmaEriIconIndex = paramInt;
  }
  
  public void setCdmaEriIconMode(int paramInt) {
    this.mCdmaEriIconMode = paramInt;
  }
  
  public void setOperatorName(String paramString1, String paramString2, String paramString3) {
    this.mOperatorAlphaLong = paramString1;
    this.mOperatorAlphaShort = paramString2;
    this.mOperatorNumeric = paramString3;
  }
  
  public void setOperatorAlphaLong(String paramString) {
    this.mOperatorAlphaLong = paramString;
  }
  
  public void setIsManualSelection(boolean paramBoolean) {
    this.mIsManualNetworkSelection = paramBoolean;
  }
  
  private static boolean equalsHandlesNulls(Object paramObject1, Object paramObject2) {
    boolean bool;
    if (paramObject1 == null) {
      if (paramObject2 == null) {
        bool = true;
      } else {
        bool = false;
      } 
    } else {
      bool = paramObject1.equals(paramObject2);
    } 
    return bool;
  }
  
  private void setFromNotifierBundle(Bundle paramBundle) {
    ServiceState serviceState = (ServiceState)paramBundle.getParcelable("android.intent.extra.SERVICE_STATE");
    if (serviceState != null)
      copyFrom(serviceState); 
  }
  
  public void fillInNotifierBundle(Bundle paramBundle) {
    paramBundle.putParcelable("android.intent.extra.SERVICE_STATE", this);
    paramBundle.putInt("voiceRegState", this.mVoiceRegState);
    paramBundle.putInt("dataRegState", this.mDataRegState);
    paramBundle.putInt("dataRoamingType", getDataRoamingType());
    paramBundle.putInt("voiceRoamingType", getVoiceRoamingType());
    paramBundle.putString("operator-alpha-long", this.mOperatorAlphaLong);
    paramBundle.putString("operator-alpha-short", this.mOperatorAlphaShort);
    paramBundle.putString("operator-numeric", this.mOperatorNumeric);
    paramBundle.putString("data-operator-alpha-long", this.mOperatorAlphaLong);
    paramBundle.putString("data-operator-alpha-short", this.mOperatorAlphaShort);
    paramBundle.putString("data-operator-numeric", this.mOperatorNumeric);
    paramBundle.putBoolean("manual", this.mIsManualNetworkSelection);
    paramBundle.putInt("radioTechnology", getRilVoiceRadioTechnology());
    paramBundle.putInt("dataRadioTechnology", getRadioTechnology());
    paramBundle.putBoolean("cssIndicator", this.mCssIndicator);
    paramBundle.putInt("networkId", this.mNetworkId);
    paramBundle.putInt("systemId", this.mSystemId);
    paramBundle.putInt("cdmaRoamingIndicator", this.mCdmaRoamingIndicator);
    paramBundle.putInt("cdmaDefaultRoamingIndicator", this.mCdmaDefaultRoamingIndicator);
    paramBundle.putBoolean("emergencyOnly", this.mIsEmergencyOnly);
    paramBundle.putBoolean("isDataRoamingFromRegistration", getDataRoamingFromRegistration());
    paramBundle.putBoolean("isUsingCarrierAggregation", isUsingCarrierAggregation());
    paramBundle.putInt("LteEarfcnRsrpBoost", this.mLteEarfcnRsrpBoost);
    paramBundle.putInt("ChannelNumber", this.mChannelNumber);
    paramBundle.putIntArray("CellBandwidths", this.mCellBandwidths);
    paramBundle.putInt("mNrFrequencyRange", this.mNrFrequencyRange);
    paramBundle.putString("operator-alpha-long-raw", this.mOperatorAlphaLongRaw);
    paramBundle.putString("operator-alpha-short-raw", this.mOperatorAlphaShortRaw);
  }
  
  public void setRilVoiceRadioTechnology(int paramInt) {
    Rlog.e("PHONE", "ServiceState.setRilVoiceRadioTechnology() called. It's encouraged to use addNetworkRegistrationInfo() instead *******");
    NetworkRegistrationInfo networkRegistrationInfo1 = getNetworkRegistrationInfo(1, 1);
    NetworkRegistrationInfo networkRegistrationInfo2 = networkRegistrationInfo1;
    if (networkRegistrationInfo1 == null) {
      NetworkRegistrationInfo.Builder builder = new NetworkRegistrationInfo.Builder();
      builder = builder.setDomain(1);
      builder = builder.setTransportType(1);
      networkRegistrationInfo2 = builder.build();
    } 
    networkRegistrationInfo2.setAccessNetworkTechnology(rilRadioTechnologyToNetworkType(paramInt));
    addNetworkRegistrationInfo(networkRegistrationInfo2);
  }
  
  public void setRilDataRadioTechnology(int paramInt) {
    Rlog.e("PHONE", "ServiceState.setRilDataRadioTechnology() called. It's encouraged to use addNetworkRegistrationInfo() instead *******");
    NetworkRegistrationInfo networkRegistrationInfo1 = getNetworkRegistrationInfo(2, 1);
    NetworkRegistrationInfo networkRegistrationInfo2 = networkRegistrationInfo1;
    if (networkRegistrationInfo1 == null) {
      NetworkRegistrationInfo.Builder builder = new NetworkRegistrationInfo.Builder();
      builder = builder.setDomain(2);
      builder = builder.setTransportType(1);
      networkRegistrationInfo2 = builder.build();
    } 
    networkRegistrationInfo2.setAccessNetworkTechnology(rilRadioTechnologyToNetworkType(paramInt));
    addNetworkRegistrationInfo(networkRegistrationInfo2);
  }
  
  public boolean isUsingCarrierAggregation() {
    boolean bool1 = false;
    boolean bool = true;
    NetworkRegistrationInfo networkRegistrationInfo = getNetworkRegistrationInfo(2, 1);
    boolean bool2 = bool1;
    if (networkRegistrationInfo != null) {
      DataSpecificRegistrationInfo dataSpecificRegistrationInfo = networkRegistrationInfo.getDataSpecificInfo();
      bool2 = bool1;
      if (dataSpecificRegistrationInfo != null)
        bool2 = dataSpecificRegistrationInfo.isUsingCarrierAggregation(); 
    } 
    bool1 = bool;
    if (!bool2)
      if ((getCellBandwidths()).length > 1) {
        bool1 = bool;
      } else {
        bool1 = false;
      }  
    return bool1;
  }
  
  public void setIsUsingCarrierAggregation(boolean paramBoolean) {
    NetworkRegistrationInfo networkRegistrationInfo = getNetworkRegistrationInfo(2, 1);
    if (networkRegistrationInfo != null) {
      DataSpecificRegistrationInfo dataSpecificRegistrationInfo = networkRegistrationInfo.getDataSpecificInfo();
      if (dataSpecificRegistrationInfo != null) {
        dataSpecificRegistrationInfo.setIsUsingCarrierAggregation(paramBoolean);
        addNetworkRegistrationInfo(networkRegistrationInfo);
      } 
    } 
  }
  
  public int getNrFrequencyRange() {
    return this.mNrFrequencyRange;
  }
  
  public int getNrState() {
    NetworkRegistrationInfo networkRegistrationInfo = getNetworkRegistrationInfo(2, 1);
    if (networkRegistrationInfo == null)
      return 0; 
    return networkRegistrationInfo.getNrState();
  }
  
  public void setNrFrequencyRange(int paramInt) {
    this.mNrFrequencyRange = paramInt;
  }
  
  public int getLteEarfcnRsrpBoost() {
    return this.mLteEarfcnRsrpBoost;
  }
  
  public void setLteEarfcnRsrpBoost(int paramInt) {
    this.mLteEarfcnRsrpBoost = paramInt;
  }
  
  public void setCssIndicator(int paramInt) {
    boolean bool;
    if (paramInt != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mCssIndicator = bool;
  }
  
  public void setCdmaSystemAndNetworkId(int paramInt1, int paramInt2) {
    this.mSystemId = paramInt1;
    this.mNetworkId = paramInt2;
  }
  
  public int getRilVoiceRadioTechnology() {
    NetworkRegistrationInfo networkRegistrationInfo = getNetworkRegistrationInfo(1, 1);
    if (networkRegistrationInfo != null)
      return networkTypeToRilRadioTechnology(networkRegistrationInfo.getAccessNetworkTechnology()); 
    return 0;
  }
  
  public int getRilDataRadioTechnology() {
    return networkTypeToRilRadioTechnology(getDataNetworkType());
  }
  
  public int getRadioTechnology() {
    Rlog.e("PHONE", "ServiceState.getRadioTechnology() DEPRECATED will be removed *******");
    return getRilDataRadioTechnology();
  }
  
  public static int rilRadioTechnologyToNetworkType(int paramInt) {
    switch (paramInt) {
      default:
        return 0;
      case 20:
        return 20;
      case 19:
        return 19;
      case 18:
        return 18;
      case 17:
        return 17;
      case 16:
        return 16;
      case 15:
        return 15;
      case 14:
        return 13;
      case 13:
        return 14;
      case 12:
        return 12;
      case 11:
        return 10;
      case 10:
        return 9;
      case 9:
        return 8;
      case 8:
        return 6;
      case 7:
        return 5;
      case 6:
        return 7;
      case 4:
      case 5:
        return 4;
      case 3:
        return 3;
      case 2:
        return 2;
      case 1:
        break;
    } 
    return 1;
  }
  
  public static int rilRadioTechnologyToAccessNetworkType(int paramInt) {
    switch (paramInt) {
      default:
        return 0;
      case 20:
        return 6;
      case 18:
        return 5;
      case 14:
      case 19:
        return 3;
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 12:
      case 13:
        return 4;
      case 3:
      case 9:
      case 10:
      case 11:
      case 15:
      case 17:
        return 2;
      case 1:
      case 2:
      case 16:
        break;
    } 
    return 1;
  }
  
  public static int networkTypeToRilRadioTechnology(int paramInt) {
    switch (paramInt) {
      default:
        return 0;
      case 20:
        return 20;
      case 19:
        return 19;
      case 18:
        return 18;
      case 17:
        return 17;
      case 16:
        return 16;
      case 15:
        return 15;
      case 14:
        return 13;
      case 13:
        return 14;
      case 12:
        return 12;
      case 10:
        return 11;
      case 9:
        return 10;
      case 8:
        return 9;
      case 7:
        return 6;
      case 6:
        return 8;
      case 5:
        return 7;
      case 4:
        return 4;
      case 3:
        return 3;
      case 2:
        return 2;
      case 1:
        break;
    } 
    return 1;
  }
  
  public int getDataNetworkType() {
    NetworkRegistrationInfo networkRegistrationInfo1 = getNetworkRegistrationInfo(2, 2);
    NetworkRegistrationInfo networkRegistrationInfo2 = getNetworkRegistrationInfo(2, 1);
    if (networkRegistrationInfo1 == null || !networkRegistrationInfo1.isInService()) {
      boolean bool;
      if (networkRegistrationInfo2 != null) {
        bool = networkRegistrationInfo2.getAccessNetworkTechnology();
      } else {
        bool = false;
      } 
      return bool;
    } 
    if (!networkRegistrationInfo2.isInService() || this.mIsIwlanPreferred)
      return networkRegistrationInfo1.getAccessNetworkTechnology(); 
    return networkRegistrationInfo2.getAccessNetworkTechnology();
  }
  
  public int getVoiceNetworkType() {
    NetworkRegistrationInfo networkRegistrationInfo = getNetworkRegistrationInfo(1, 1);
    if (networkRegistrationInfo != null)
      return networkRegistrationInfo.getAccessNetworkTechnology(); 
    return 0;
  }
  
  public int getCssIndicator() {
    return this.mCssIndicator;
  }
  
  public int getCdmaNetworkId() {
    return this.mNetworkId;
  }
  
  public int getCdmaSystemId() {
    return this.mSystemId;
  }
  
  public static boolean isGsm(int paramInt) {
    boolean bool1 = true, bool2 = bool1;
    if (paramInt != 1) {
      bool2 = bool1;
      if (paramInt != 2) {
        bool2 = bool1;
        if (paramInt != 3) {
          bool2 = bool1;
          if (paramInt != 9) {
            bool2 = bool1;
            if (paramInt != 10) {
              bool2 = bool1;
              if (paramInt != 11) {
                bool2 = bool1;
                if (paramInt != 14) {
                  bool2 = bool1;
                  if (paramInt != 15) {
                    bool2 = bool1;
                    if (paramInt != 16) {
                      bool2 = bool1;
                      if (paramInt != 17) {
                        bool2 = bool1;
                        if (paramInt != 18) {
                          bool2 = bool1;
                          if (paramInt != 19)
                            if (paramInt == 20) {
                              bool2 = bool1;
                            } else {
                              bool2 = false;
                            }  
                        } 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
    } 
    return bool2;
  }
  
  public static boolean isCdma(int paramInt) {
    return (paramInt == 4 || paramInt == 5 || paramInt == 6 || paramInt == 7 || paramInt == 8 || paramInt == 12 || paramInt == 13);
  }
  
  public static boolean isPsOnlyTech(int paramInt) {
    return (paramInt == 14 || paramInt == 19 || paramInt == 20);
  }
  
  public static boolean isPsTech(int paramInt) {
    return (paramInt == 14 || paramInt == 19 || paramInt == 20);
  }
  
  public static boolean bearerBitmapHasCdma(int paramInt) {
    boolean bool;
    if ((convertNetworkTypeBitmaskToBearerBitmask(paramInt) & 0x18F8) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean bitmaskHasTech(int paramInt1, int paramInt2) {
    boolean bool = true;
    if (paramInt1 == 0)
      return true; 
    if (paramInt2 >= 1) {
      if ((1 << paramInt2 - 1 & paramInt1) == 0)
        bool = false; 
      return bool;
    } 
    return false;
  }
  
  public static int getBitmaskForTech(int paramInt) {
    if (paramInt >= 1)
      return 1 << paramInt - 1; 
    return 0;
  }
  
  public static int getBitmaskFromString(String paramString) {
    String[] arrayOfString = paramString.split("\\|");
    int i = 0;
    int j;
    byte b;
    for (j = arrayOfString.length, b = 0; b < j; ) {
      paramString = arrayOfString[b];
      try {
        int k = Integer.parseInt(paramString.trim());
        if (k == 0)
          return 0; 
        i |= getBitmaskForTech(k);
        b++;
      } catch (NumberFormatException numberFormatException) {
        return 0;
      } 
    } 
    return i;
  }
  
  public static int convertNetworkTypeBitmaskToBearerBitmask(int paramInt) {
    if (paramInt == 0)
      return 0; 
    int i = 0;
    for (byte b = 0; b < 21; b++, i = j) {
      int j = i;
      if (bitmaskHasTech(paramInt, rilRadioTechnologyToNetworkType(b)))
        j = i | getBitmaskForTech(b); 
    } 
    return i;
  }
  
  public static int convertBearerBitmaskToNetworkTypeBitmask(int paramInt) {
    if (paramInt == 0)
      return 0; 
    int i = 0;
    for (byte b = 0; b < 21; b++, i = j) {
      int j = i;
      if (bitmaskHasTech(paramInt, b))
        j = i | getBitmaskForTech(rilRadioTechnologyToNetworkType(b)); 
    } 
    return i;
  }
  
  public static ServiceState mergeServiceStates(ServiceState paramServiceState1, ServiceState paramServiceState2) {
    if (paramServiceState2.mVoiceRegState != 0)
      return paramServiceState1; 
    paramServiceState1 = new ServiceState(paramServiceState1);
    paramServiceState1.mVoiceRegState = paramServiceState2.mVoiceRegState;
    paramServiceState1.mIsEmergencyOnly = false;
    return paramServiceState1;
  }
  
  public List<NetworkRegistrationInfo> getNetworkRegistrationInfoList() {
    synchronized (this.mNetworkRegistrationInfos) {
      ArrayList<NetworkRegistrationInfo> arrayList = new ArrayList();
      this();
      for (NetworkRegistrationInfo networkRegistrationInfo1 : this.mNetworkRegistrationInfos) {
        NetworkRegistrationInfo networkRegistrationInfo2 = new NetworkRegistrationInfo();
        this(networkRegistrationInfo1);
        arrayList.add(networkRegistrationInfo2);
      } 
      return arrayList;
    } 
  }
  
  @SystemApi
  public List<NetworkRegistrationInfo> getNetworkRegistrationInfoListForTransportType(int paramInt) {
    null = new ArrayList();
    synchronized (this.mNetworkRegistrationInfos) {
      for (NetworkRegistrationInfo networkRegistrationInfo : this.mNetworkRegistrationInfos) {
        if (networkRegistrationInfo.getTransportType() == paramInt) {
          NetworkRegistrationInfo networkRegistrationInfo1 = new NetworkRegistrationInfo();
          this(networkRegistrationInfo);
          null.add(networkRegistrationInfo1);
        } 
      } 
      return null;
    } 
  }
  
  @SystemApi
  public List<NetworkRegistrationInfo> getNetworkRegistrationInfoListForDomain(int paramInt) {
    null = new ArrayList();
    synchronized (this.mNetworkRegistrationInfos) {
      for (NetworkRegistrationInfo networkRegistrationInfo : this.mNetworkRegistrationInfos) {
        if ((networkRegistrationInfo.getDomain() & paramInt) != 0) {
          NetworkRegistrationInfo networkRegistrationInfo1 = new NetworkRegistrationInfo();
          this(networkRegistrationInfo);
          null.add(networkRegistrationInfo1);
        } 
      } 
      return null;
    } 
  }
  
  @SystemApi
  public NetworkRegistrationInfo getNetworkRegistrationInfo(int paramInt1, int paramInt2) {
    synchronized (this.mNetworkRegistrationInfos) {
      for (Iterator<NetworkRegistrationInfo> iterator = this.mNetworkRegistrationInfos.iterator(); iterator.hasNext(); ) {
        NetworkRegistrationInfo networkRegistrationInfo = iterator.next();
        if (networkRegistrationInfo.getTransportType() == paramInt2 && (
          networkRegistrationInfo.getDomain() & paramInt1) != 0) {
          NetworkRegistrationInfo networkRegistrationInfo1 = new NetworkRegistrationInfo();
          this(networkRegistrationInfo);
          return networkRegistrationInfo1;
        } 
      } 
      return null;
    } 
  }
  
  public void addNetworkRegistrationInfo(NetworkRegistrationInfo paramNetworkRegistrationInfo) {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull -> 5
    //   4: return
    //   5: aload_0
    //   6: getfield mNetworkRegistrationInfos : Ljava/util/List;
    //   9: astore_2
    //   10: aload_2
    //   11: monitorenter
    //   12: iconst_0
    //   13: istore_3
    //   14: iload_3
    //   15: aload_0
    //   16: getfield mNetworkRegistrationInfos : Ljava/util/List;
    //   19: invokeinterface size : ()I
    //   24: if_icmpge -> 86
    //   27: aload_0
    //   28: getfield mNetworkRegistrationInfos : Ljava/util/List;
    //   31: iload_3
    //   32: invokeinterface get : (I)Ljava/lang/Object;
    //   37: checkcast android/telephony/NetworkRegistrationInfo
    //   40: astore #4
    //   42: aload #4
    //   44: invokevirtual getTransportType : ()I
    //   47: aload_1
    //   48: invokevirtual getTransportType : ()I
    //   51: if_icmpne -> 80
    //   54: aload #4
    //   56: invokevirtual getDomain : ()I
    //   59: aload_1
    //   60: invokevirtual getDomain : ()I
    //   63: if_icmpne -> 80
    //   66: aload_0
    //   67: getfield mNetworkRegistrationInfos : Ljava/util/List;
    //   70: iload_3
    //   71: invokeinterface remove : (I)Ljava/lang/Object;
    //   76: pop
    //   77: goto -> 86
    //   80: iinc #3, 1
    //   83: goto -> 14
    //   86: aload_0
    //   87: getfield mNetworkRegistrationInfos : Ljava/util/List;
    //   90: astore #4
    //   92: new android/telephony/NetworkRegistrationInfo
    //   95: astore #5
    //   97: aload #5
    //   99: aload_1
    //   100: invokespecial <init> : (Landroid/telephony/NetworkRegistrationInfo;)V
    //   103: aload #4
    //   105: aload #5
    //   107: invokeinterface add : (Ljava/lang/Object;)Z
    //   112: pop
    //   113: aload_2
    //   114: monitorexit
    //   115: return
    //   116: astore_1
    //   117: aload_2
    //   118: monitorexit
    //   119: aload_1
    //   120: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1984	-> 0
    //   #1986	-> 5
    //   #1987	-> 12
    //   #1988	-> 27
    //   #1989	-> 42
    //   #1990	-> 54
    //   #1991	-> 66
    //   #1992	-> 77
    //   #1987	-> 80
    //   #1996	-> 86
    //   #1997	-> 113
    //   #1998	-> 115
    //   #1997	-> 116
    // Exception table:
    //   from	to	target	type
    //   14	27	116	finally
    //   27	42	116	finally
    //   42	54	116	finally
    //   54	66	116	finally
    //   66	77	116	finally
    //   86	113	116	finally
    //   113	115	116	finally
    //   117	119	116	finally
  }
  
  public static final int getBetterNRFrequencyRange(int paramInt1, int paramInt2) {
    if (FREQUENCY_RANGE_ORDER.indexOf(Integer.valueOf(paramInt1)) <= FREQUENCY_RANGE_ORDER.indexOf(Integer.valueOf(paramInt2)))
      paramInt1 = paramInt2; 
    return paramInt1;
  }
  
  public ServiceState createLocationInfoSanitizedCopy(boolean paramBoolean) {
    null = new ServiceState(this);
    synchronized (null.mNetworkRegistrationInfos) {
      List<NetworkRegistrationInfo> list1 = null.mNetworkRegistrationInfos;
      Stream<NetworkRegistrationInfo> stream = list1.stream();
      -$.Lambda.MLKtmRGKP3e0WU7x_KyS5-Vg8q4 mLKtmRGKP3e0WU7x_KyS5-Vg8q4 = _$$Lambda$MLKtmRGKP3e0WU7x_KyS5_Vg8q4.INSTANCE;
      stream = stream.map((Function<? super NetworkRegistrationInfo, ? extends NetworkRegistrationInfo>)mLKtmRGKP3e0WU7x_KyS5-Vg8q4);
      List<? extends NetworkRegistrationInfo> list = stream.collect((Collector)Collectors.toList());
      null.mNetworkRegistrationInfos.clear();
      null.mNetworkRegistrationInfos.addAll(list);
      if (!paramBoolean)
        return null; 
      null.mOperatorAlphaLong = null;
      null.mOperatorAlphaShort = null;
      null.mOperatorNumeric = null;
      return null;
    } 
  }
  
  public void setOperatorAlphaLongRaw(String paramString) {
    this.mOperatorAlphaLongRaw = paramString;
  }
  
  public String getOperatorAlphaLongRaw() {
    return this.mOperatorAlphaLongRaw;
  }
  
  public void setOperatorAlphaShortRaw(String paramString) {
    this.mOperatorAlphaShortRaw = paramString;
  }
  
  public String getOperatorAlphaShortRaw() {
    return this.mOperatorAlphaShortRaw;
  }
  
  public void setIwlanPreferred(boolean paramBoolean) {
    this.mIsIwlanPreferred = paramBoolean;
  }
  
  public boolean isIwlanPreferred() {
    return this.mIsIwlanPreferred;
  }
  
  public boolean isSearching() {
    NetworkRegistrationInfo networkRegistrationInfo = getNetworkRegistrationInfo(2, 1);
    if (networkRegistrationInfo != null && networkRegistrationInfo.getRegistrationState() == 2)
      return true; 
    networkRegistrationInfo = getNetworkRegistrationInfo(1, 1);
    if (networkRegistrationInfo != null && networkRegistrationInfo.getRegistrationState() == 2)
      return true; 
    return false;
  }
  
  public static boolean bearerBitmapHasGsm(int paramInt) {
    boolean bool;
    if ((convertNetworkTypeBitmaskToBearerBitmask(paramInt) & 0xFE707) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class DuplexMode implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class FrequencyRange implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class RegState implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class RilRadioTechnology implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class RoamingType implements Annotation {}
}
