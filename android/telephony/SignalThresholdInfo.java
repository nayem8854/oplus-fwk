package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;
import java.util.Objects;

public class SignalThresholdInfo implements Parcelable {
  public SignalThresholdInfo(int paramInt1, int paramInt2, int paramInt3, int[] paramArrayOfint, boolean paramBoolean) {
    int[] arrayOfInt = null;
    this.mThresholds = null;
    this.mIsEnabled = true;
    this.mSignalMeasurement = paramInt1;
    paramInt1 = 0;
    if (paramInt2 < 0)
      paramInt2 = 0; 
    this.mHysteresisMs = paramInt2;
    if (paramInt3 < 0)
      paramInt3 = paramInt1; 
    this.mHysteresisDb = paramInt3;
    if (paramArrayOfint == null) {
      paramArrayOfint = arrayOfInt;
    } else {
      paramArrayOfint = (int[])paramArrayOfint.clone();
    } 
    this.mThresholds = paramArrayOfint;
    this.mIsEnabled = paramBoolean;
  }
  
  public int getSignalMeasurement() {
    return this.mSignalMeasurement;
  }
  
  public int getHysteresisMs() {
    return this.mHysteresisMs;
  }
  
  public int getHysteresisDb() {
    return this.mHysteresisDb;
  }
  
  public boolean isEnabled() {
    return this.mIsEnabled;
  }
  
  public int[] getThresholds() {
    int[] arrayOfInt = this.mThresholds;
    if (arrayOfInt == null) {
      arrayOfInt = null;
    } else {
      arrayOfInt = (int[])arrayOfInt.clone();
    } 
    return arrayOfInt;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mSignalMeasurement);
    paramParcel.writeInt(this.mHysteresisMs);
    paramParcel.writeInt(this.mHysteresisDb);
    paramParcel.writeIntArray(this.mThresholds);
    paramParcel.writeBoolean(this.mIsEnabled);
  }
  
  private SignalThresholdInfo(Parcel paramParcel) {
    this.mThresholds = null;
    this.mIsEnabled = true;
    this.mSignalMeasurement = paramParcel.readInt();
    this.mHysteresisMs = paramParcel.readInt();
    this.mHysteresisDb = paramParcel.readInt();
    this.mThresholds = paramParcel.createIntArray();
    this.mIsEnabled = paramParcel.readBoolean();
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof SignalThresholdInfo))
      return false; 
    SignalThresholdInfo signalThresholdInfo = (SignalThresholdInfo)paramObject;
    if (this.mSignalMeasurement == signalThresholdInfo.mSignalMeasurement && this.mHysteresisMs == signalThresholdInfo.mHysteresisMs && this.mHysteresisDb == signalThresholdInfo.mHysteresisDb) {
      paramObject = this.mThresholds;
      int[] arrayOfInt = signalThresholdInfo.mThresholds;
      if (Arrays.equals((int[])paramObject, arrayOfInt) && this.mIsEnabled == signalThresholdInfo.mIsEnabled)
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = this.mSignalMeasurement;
    int j = this.mHysteresisMs, k = this.mHysteresisDb, arrayOfInt[] = this.mThresholds;
    boolean bool = this.mIsEnabled;
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), arrayOfInt, Boolean.valueOf(bool) });
  }
  
  public static final Parcelable.Creator<SignalThresholdInfo> CREATOR = new Parcelable.Creator<SignalThresholdInfo>() {
      public SignalThresholdInfo createFromParcel(Parcel param1Parcel) {
        return new SignalThresholdInfo(param1Parcel);
      }
      
      public SignalThresholdInfo[] newArray(int param1Int) {
        return new SignalThresholdInfo[param1Int];
      }
    };
  
  public static final int HYSTERESIS_DB_DISABLED = 0;
  
  public static final int HYSTERESIS_MS_DISABLED = 0;
  
  public static final int SIGNAL_RSCP = 2;
  
  public static final int SIGNAL_RSRP = 3;
  
  public static final int SIGNAL_RSRQ = 4;
  
  public static final int SIGNAL_RSSI = 1;
  
  public static final int SIGNAL_RSSNR = 5;
  
  public static final int SIGNAL_SSRSRP = 6;
  
  public static final int SIGNAL_SSRSRQ = 7;
  
  public static final int SIGNAL_SSSINR = 8;
  
  private int mHysteresisDb;
  
  private int mHysteresisMs;
  
  private boolean mIsEnabled;
  
  private int mSignalMeasurement;
  
  private int[] mThresholds;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("SignalThresholdInfo{");
    stringBuilder.append("mSignalMeasurement=");
    stringBuilder.append(this.mSignalMeasurement);
    stringBuilder.append("mHysteresisMs=");
    stringBuilder.append(this.mSignalMeasurement);
    stringBuilder.append("mHysteresisDb=");
    stringBuilder.append(this.mHysteresisDb);
    stringBuilder.append("mThresholds=");
    stringBuilder.append(Arrays.toString(this.mThresholds));
    stringBuilder.append("mIsEnabled=");
    stringBuilder.append(this.mIsEnabled);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class SignalMeasurementType implements Annotation {}
}
