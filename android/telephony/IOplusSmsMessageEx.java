package android.telephony;

public interface IOplusSmsMessageEx {
  String getDestinationAddress();
  
  int getEncodingType();
  
  String getRecipientAddress();
}
