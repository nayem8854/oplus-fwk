package android.telephony;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.telephony.Rlog;

@Deprecated
public final class VoLteServiceState implements Parcelable {
  public static VoLteServiceState newFromBundle(Bundle paramBundle) {
    VoLteServiceState voLteServiceState = new VoLteServiceState();
    voLteServiceState.setFromNotifierBundle(paramBundle);
    return voLteServiceState;
  }
  
  public VoLteServiceState() {
    initialize();
  }
  
  public VoLteServiceState(int paramInt) {
    initialize();
    this.mSrvccState = paramInt;
  }
  
  public VoLteServiceState(VoLteServiceState paramVoLteServiceState) {
    copyFrom(paramVoLteServiceState);
  }
  
  private void initialize() {
    this.mSrvccState = Integer.MAX_VALUE;
  }
  
  protected void copyFrom(VoLteServiceState paramVoLteServiceState) {
    this.mSrvccState = paramVoLteServiceState.mSrvccState;
  }
  
  public VoLteServiceState(Parcel paramParcel) {
    this.mSrvccState = paramParcel.readInt();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mSrvccState);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<VoLteServiceState> CREATOR = new Parcelable.Creator() {
      public VoLteServiceState createFromParcel(Parcel param1Parcel) {
        return new VoLteServiceState(param1Parcel);
      }
      
      public VoLteServiceState[] newArray(int param1Int) {
        return new VoLteServiceState[param1Int];
      }
    };
  
  private static final boolean DBG = false;
  
  public static final int HANDOVER_CANCELED = 3;
  
  public static final int HANDOVER_COMPLETED = 1;
  
  public static final int HANDOVER_FAILED = 2;
  
  public static final int HANDOVER_STARTED = 0;
  
  public static final int INVALID = 2147483647;
  
  private static final String LOG_TAG = "VoLteServiceState";
  
  public static final int NOT_SUPPORTED = 0;
  
  public static final int SUPPORTED = 1;
  
  private int mSrvccState;
  
  public void validateInput() {}
  
  public int hashCode() {
    return this.mSrvccState * 31;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    try {
      VoLteServiceState voLteServiceState = (VoLteServiceState)paramObject;
      if (paramObject == null)
        return false; 
      if (this.mSrvccState == voLteServiceState.mSrvccState)
        bool = true; 
      return bool;
    } catch (ClassCastException classCastException) {
      return false;
    } 
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("VoLteServiceState: ");
    stringBuilder.append(this.mSrvccState);
    return stringBuilder.toString();
  }
  
  private void setFromNotifierBundle(Bundle paramBundle) {
    this.mSrvccState = paramBundle.getInt("mSrvccState");
  }
  
  public void fillInNotifierBundle(Bundle paramBundle) {
    paramBundle.putInt("mSrvccState", this.mSrvccState);
  }
  
  public int getSrvccState() {
    return this.mSrvccState;
  }
  
  private static void log(String paramString) {
    Rlog.w("VoLteServiceState", paramString);
  }
}
