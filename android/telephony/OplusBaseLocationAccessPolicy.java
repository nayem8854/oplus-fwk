package android.telephony;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.UserHandle;

public class OplusBaseLocationAccessPolicy {
  public static final String[] mNLPPackages = new String[] { "com.baidu.map.location", "com.tencent.android.location", "com.amap.android.location", "com.amap.android.ams" };
  
  public static boolean isNLP(String paramString) {
    for (String str : mNLPPackages) {
      if (str.equals(paramString))
        return true; 
    } 
    return false;
  }
  
  public static boolean isAppAtLeastSdkVersion(Context paramContext, String paramString, int paramInt1, int paramInt2) {
    try {
      paramInt2 = (paramContext.getPackageManager().getApplicationInfoAsUser(paramString, 0, UserHandle.getUserId(paramInt2))).targetSdkVersion;
      if (paramInt2 >= paramInt1)
        return true; 
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {}
    return false;
  }
}
