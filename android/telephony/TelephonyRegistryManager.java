package android.telephony;

import android.compat.Compatibility;
import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.telephony.emergency.EmergencyNumber;
import android.telephony.ims.ImsReasonInfo;
import android.util.Log;
import com.android.internal.telephony.IOnSubscriptionsChangedListener;
import com.android.internal.telephony.ITelephonyRegistry;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

public class TelephonyRegistryManager {
  private final Map<SubscriptionManager.OnSubscriptionsChangedListener, IOnSubscriptionsChangedListener> mSubscriptionChangedListenerMap = new HashMap<>();
  
  private final Map<SubscriptionManager.OnOpportunisticSubscriptionsChangedListener, IOnSubscriptionsChangedListener> mOpportunisticSubscriptionChangedListenerMap = new HashMap<>();
  
  private final Context mContext;
  
  private static ITelephonyRegistry sRegistry;
  
  private static final String TAG = "TelephonyRegistryManager";
  
  public static final int SIM_ACTIVATION_TYPE_VOICE = 0;
  
  public static final int SIM_ACTIVATION_TYPE_DATA = 1;
  
  private static final long LISTEN_CODE_CHANGE = 147600208L;
  
  public TelephonyRegistryManager(Context paramContext) {
    this.mContext = paramContext;
    if (sRegistry == null) {
      IBinder iBinder = ServiceManager.getService("telephony.registry");
      sRegistry = ITelephonyRegistry.Stub.asInterface(iBinder);
    } 
  }
  
  public void addOnSubscriptionsChangedListener(SubscriptionManager.OnSubscriptionsChangedListener paramOnSubscriptionsChangedListener, Executor paramExecutor) {
    if (this.mSubscriptionChangedListenerMap.get(paramOnSubscriptionsChangedListener) != null) {
      Log.d("TelephonyRegistryManager", "addOnSubscriptionsChangedListener listener already present");
      return;
    } 
    Object object = new Object(this, paramExecutor, paramOnSubscriptionsChangedListener);
    this.mSubscriptionChangedListenerMap.put(paramOnSubscriptionsChangedListener, object);
    try {
      ITelephonyRegistry iTelephonyRegistry = sRegistry;
      String str1 = this.mContext.getOpPackageName();
      Context context = this.mContext;
      String str2 = context.getAttributionTag();
      iTelephonyRegistry.addOnSubscriptionsChangedListener(str1, str2, (IOnSubscriptionsChangedListener)object);
    } catch (RemoteException remoteException) {}
  }
  
  public void removeOnSubscriptionsChangedListener(SubscriptionManager.OnSubscriptionsChangedListener paramOnSubscriptionsChangedListener) {
    if (this.mSubscriptionChangedListenerMap.get(paramOnSubscriptionsChangedListener) == null)
      return; 
    try {
      ITelephonyRegistry iTelephonyRegistry = sRegistry;
      String str = this.mContext.getOpPackageName();
      Map<SubscriptionManager.OnSubscriptionsChangedListener, IOnSubscriptionsChangedListener> map = this.mSubscriptionChangedListenerMap;
      IOnSubscriptionsChangedListener iOnSubscriptionsChangedListener = map.get(paramOnSubscriptionsChangedListener);
      iTelephonyRegistry.removeOnSubscriptionsChangedListener(str, iOnSubscriptionsChangedListener);
      this.mSubscriptionChangedListenerMap.remove(paramOnSubscriptionsChangedListener);
    } catch (RemoteException remoteException) {}
  }
  
  public void addOnOpportunisticSubscriptionsChangedListener(SubscriptionManager.OnOpportunisticSubscriptionsChangedListener paramOnOpportunisticSubscriptionsChangedListener, Executor paramExecutor) {
    if (this.mOpportunisticSubscriptionChangedListenerMap.get(paramOnOpportunisticSubscriptionsChangedListener) != null) {
      Log.d("TelephonyRegistryManager", "addOnOpportunisticSubscriptionsChangedListener listener already present");
      return;
    } 
    Object object = new Object(this, paramExecutor, paramOnOpportunisticSubscriptionsChangedListener);
    this.mOpportunisticSubscriptionChangedListenerMap.put(paramOnOpportunisticSubscriptionsChangedListener, object);
    try {
      ITelephonyRegistry iTelephonyRegistry = sRegistry;
      String str1 = this.mContext.getOpPackageName();
      Context context = this.mContext;
      String str2 = context.getAttributionTag();
      iTelephonyRegistry.addOnOpportunisticSubscriptionsChangedListener(str1, str2, (IOnSubscriptionsChangedListener)object);
    } catch (RemoteException remoteException) {}
  }
  
  public void removeOnOpportunisticSubscriptionsChangedListener(SubscriptionManager.OnOpportunisticSubscriptionsChangedListener paramOnOpportunisticSubscriptionsChangedListener) {
    if (this.mOpportunisticSubscriptionChangedListenerMap.get(paramOnOpportunisticSubscriptionsChangedListener) == null)
      return; 
    try {
      ITelephonyRegistry iTelephonyRegistry = sRegistry;
      String str = this.mContext.getOpPackageName();
      Map<SubscriptionManager.OnOpportunisticSubscriptionsChangedListener, IOnSubscriptionsChangedListener> map = this.mOpportunisticSubscriptionChangedListenerMap;
      IOnSubscriptionsChangedListener iOnSubscriptionsChangedListener = map.get(paramOnOpportunisticSubscriptionsChangedListener);
      iTelephonyRegistry.removeOnSubscriptionsChangedListener(str, iOnSubscriptionsChangedListener);
      this.mOpportunisticSubscriptionChangedListenerMap.remove(paramOnOpportunisticSubscriptionsChangedListener);
    } catch (RemoteException remoteException) {}
  }
  
  public void listenForSubscriber(int paramInt1, String paramString1, String paramString2, PhoneStateListener paramPhoneStateListener, int paramInt2, boolean paramBoolean) {
    try {
      if (Compatibility.isChangeEnabled(147600208L)) {
        int i;
        if (paramInt2 == 0) {
          i = -1;
        } else {
          i = paramInt1;
        } 
        paramPhoneStateListener.mSubId = Integer.valueOf(i);
      } else if (paramPhoneStateListener.mSubId != null) {
        paramInt1 = paramPhoneStateListener.mSubId.intValue();
      } 
      sRegistry.listenForSubscriber(paramInt1, paramString1, paramString2, paramPhoneStateListener.callback, paramInt2, paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (NullPointerException nullPointerException) {
      Log.e("TelephonyRegistryManager", "listen: NullPointerException");
      return;
    } 
  }
  
  public void notifyCarrierNetworkChange(boolean paramBoolean) {
    try {
      sRegistry.notifyCarrierNetworkChange(paramBoolean);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyCallStateChanged(int paramInt1, int paramInt2, int paramInt3, String paramString) {
    try {
      sRegistry.notifyCallState(paramInt2, paramInt1, paramInt3, paramString);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyCallStateChangedForAllSubscriptions(int paramInt, String paramString) {
    try {
      sRegistry.notifyCallStateForAllSubs(paramInt, paramString);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifySubscriptionInfoChanged() {
    try {
      sRegistry.notifySubscriptionInfoChanged();
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyOpportunisticSubscriptionInfoChanged() {
    try {
      sRegistry.notifyOpportunisticSubscriptionInfoChanged();
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyServiceStateChanged(int paramInt1, int paramInt2, ServiceState paramServiceState) {
    try {
      sRegistry.notifyServiceStateForPhoneId(paramInt2, paramInt1, paramServiceState);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifySignalStrengthChanged(int paramInt1, int paramInt2, SignalStrength paramSignalStrength) {
    try {
      sRegistry.notifySignalStrengthForPhoneId(paramInt2, paramInt1, paramSignalStrength);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyMessageWaitingChanged(int paramInt1, int paramInt2, boolean paramBoolean) {
    try {
      sRegistry.notifyMessageWaitingChangedForPhoneId(paramInt2, paramInt1, paramBoolean);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyCallForwardingChanged(int paramInt, boolean paramBoolean) {
    try {
      sRegistry.notifyCallForwardingChangedForSubscriber(paramInt, paramBoolean);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyDataActivityChanged(int paramInt1, int paramInt2) {
    try {
      sRegistry.notifyDataActivityForSubscriber(paramInt1, paramInt2);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyDataConnectionForSubscriber(int paramInt1, int paramInt2, int paramInt3, PreciseDataConnectionState paramPreciseDataConnectionState) {
    try {
      sRegistry.notifyDataConnectionForSubscriber(paramInt1, paramInt2, paramInt3, paramPreciseDataConnectionState);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyCallQualityChanged(int paramInt1, int paramInt2, CallQuality paramCallQuality, int paramInt3) {
    try {
      sRegistry.notifyCallQualityChanged(paramCallQuality, paramInt2, paramInt1, paramInt3);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyEmergencyNumberList(int paramInt1, int paramInt2) {
    try {
      sRegistry.notifyEmergencyNumberList(paramInt2, paramInt1);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyOutgoingEmergencyCall(int paramInt1, int paramInt2, EmergencyNumber paramEmergencyNumber) {
    try {
      sRegistry.notifyOutgoingEmergencyCall(paramInt1, paramInt2, paramEmergencyNumber);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyOutgoingEmergencySms(int paramInt1, int paramInt2, EmergencyNumber paramEmergencyNumber) {
    try {
      sRegistry.notifyOutgoingEmergencySms(paramInt1, paramInt2, paramEmergencyNumber);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyRadioPowerStateChanged(int paramInt1, int paramInt2, int paramInt3) {
    try {
      sRegistry.notifyRadioPowerStateChanged(paramInt2, paramInt1, paramInt3);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyPhoneCapabilityChanged(PhoneCapability paramPhoneCapability) {
    try {
      sRegistry.notifyPhoneCapabilityChanged(paramPhoneCapability);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyDataActivationStateChanged(int paramInt1, int paramInt2, int paramInt3) {
    try {
      sRegistry.notifySimActivationStateChangedForPhoneId(paramInt2, paramInt1, 1, paramInt3);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyVoiceActivationStateChanged(int paramInt1, int paramInt2, int paramInt3) {
    try {
      sRegistry.notifySimActivationStateChangedForPhoneId(paramInt2, paramInt1, 0, paramInt3);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyUserMobileDataStateChanged(int paramInt1, int paramInt2, boolean paramBoolean) {
    try {
      sRegistry.notifyUserMobileDataStateChangedForPhoneId(paramInt1, paramInt2, paramBoolean);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyDisplayInfoChanged(int paramInt1, int paramInt2, TelephonyDisplayInfo paramTelephonyDisplayInfo) {
    try {
      sRegistry.notifyDisplayInfoChanged(paramInt1, paramInt2, paramTelephonyDisplayInfo);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyImsDisconnectCause(int paramInt, ImsReasonInfo paramImsReasonInfo) {
    try {
      sRegistry.notifyImsDisconnectCause(paramInt, paramImsReasonInfo);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyPreciseDataConnectionFailed(int paramInt1, int paramInt2, int paramInt3, String paramString, int paramInt4) {
    try {
      sRegistry.notifyPreciseDataConnectionFailed(paramInt2, paramInt1, paramInt3, paramString, paramInt4);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifySrvccStateChanged(int paramInt1, int paramInt2) {
    try {
      sRegistry.notifySrvccStateChanged(paramInt1, paramInt2);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyPreciseCallState(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    try {
      sRegistry.notifyPreciseCallState(paramInt2, paramInt1, paramInt3, paramInt4, paramInt5);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyDisconnectCause(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    try {
      sRegistry.notifyDisconnectCause(paramInt1, paramInt2, paramInt3, paramInt4);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyCellLocation(int paramInt, CellIdentity paramCellIdentity) {
    try {
      sRegistry.notifyCellLocationForSubscriber(paramInt, paramCellIdentity);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyCellInfoChanged(int paramInt, List<CellInfo> paramList) {
    try {
      sRegistry.notifyCellInfoForSubscriber(paramInt, paramList);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyActiveDataSubIdChanged(int paramInt) {
    try {
      sRegistry.notifyActiveDataSubIdChanged(paramInt);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyRegistrationFailed(int paramInt1, int paramInt2, CellIdentity paramCellIdentity, String paramString, int paramInt3, int paramInt4, int paramInt5) {
    try {
      sRegistry.notifyRegistrationFailed(paramInt1, paramInt2, paramCellIdentity, paramString, paramInt3, paramInt4, paramInt5);
    } catch (RemoteException remoteException) {}
  }
  
  public void notifyBarringInfoChanged(int paramInt1, int paramInt2, BarringInfo paramBarringInfo) {
    try {
      sRegistry.notifyBarringInfoChanged(paramInt1, paramInt2, paramBarringInfo);
    } catch (RemoteException remoteException) {}
  }
}
