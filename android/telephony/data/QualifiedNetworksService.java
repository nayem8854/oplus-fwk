package android.telephony.data;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.SparseArray;
import com.android.telephony.Rlog;
import java.util.List;
import java.util.function.ToIntFunction;

@SystemApi
public abstract class QualifiedNetworksService extends Service {
  private static final String TAG = QualifiedNetworksService.class.getSimpleName();
  
  private final SparseArray<NetworkAvailabilityProvider> mProviders = new SparseArray<>();
  
  public final IQualifiedNetworksServiceWrapper mBinder = new IQualifiedNetworksServiceWrapper();
  
  private static final int QNS_CREATE_NETWORK_AVAILABILITY_PROVIDER = 1;
  
  private static final int QNS_REMOVE_ALL_NETWORK_AVAILABILITY_PROVIDERS = 3;
  
  private static final int QNS_REMOVE_NETWORK_AVAILABILITY_PROVIDER = 2;
  
  private static final int QNS_UPDATE_QUALIFIED_NETWORKS = 4;
  
  public static final String QUALIFIED_NETWORKS_SERVICE_INTERFACE = "android.telephony.data.QualifiedNetworksService";
  
  private final QualifiedNetworksServiceHandler mHandler;
  
  private final HandlerThread mHandlerThread;
  
  class NetworkAvailabilityProvider implements AutoCloseable {
    private IQualifiedNetworksServiceCallback mCallback;
    
    private SparseArray<int[]> mQualifiedNetworkTypesList = (SparseArray)new SparseArray<>();
    
    private final int mSlotIndex;
    
    final QualifiedNetworksService this$0;
    
    public NetworkAvailabilityProvider(int param1Int) {
      this.mSlotIndex = param1Int;
    }
    
    public final int getSlotIndex() {
      return this.mSlotIndex;
    }
    
    private void registerForQualifiedNetworkTypesChanged(IQualifiedNetworksServiceCallback param1IQualifiedNetworksServiceCallback) {
      this.mCallback = param1IQualifiedNetworksServiceCallback;
      if (param1IQualifiedNetworksServiceCallback != null)
        for (byte b = 0; b < this.mQualifiedNetworkTypesList.size(); b++) {
          try {
            param1IQualifiedNetworksServiceCallback = this.mCallback;
            SparseArray<int[]> sparseArray = this.mQualifiedNetworkTypesList;
            int i = sparseArray.keyAt(b);
            sparseArray = this.mQualifiedNetworkTypesList;
            int[] arrayOfInt = sparseArray.valueAt(b);
            param1IQualifiedNetworksServiceCallback.onQualifiedNetworkTypesChanged(i, arrayOfInt);
          } catch (RemoteException remoteException) {
            QualifiedNetworksService qualifiedNetworksService = QualifiedNetworksService.this;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Failed to call onQualifiedNetworksChanged. ");
            stringBuilder.append(remoteException);
            qualifiedNetworksService.loge(stringBuilder.toString());
          } 
        }  
    }
    
    public final void updateQualifiedNetworkTypes(int param1Int, List<Integer> param1List) {
      int[] arrayOfInt = param1List.stream().mapToInt((ToIntFunction)_$$Lambda$QualifiedNetworksService$NetworkAvailabilityProvider$sNPqwkqArvqymBmHYmxAc4rF5Es.INSTANCE).toArray();
      Message message = QualifiedNetworksService.this.mHandler.obtainMessage(4, this.mSlotIndex, param1Int, arrayOfInt);
      message.sendToTarget();
    }
    
    private void onUpdateQualifiedNetworkTypes(int param1Int, int[] param1ArrayOfint) {
      this.mQualifiedNetworkTypesList.put(param1Int, param1ArrayOfint);
      IQualifiedNetworksServiceCallback iQualifiedNetworksServiceCallback = this.mCallback;
      if (iQualifiedNetworksServiceCallback != null)
        try {
          iQualifiedNetworksServiceCallback.onQualifiedNetworkTypesChanged(param1Int, param1ArrayOfint);
        } catch (RemoteException remoteException) {
          QualifiedNetworksService qualifiedNetworksService = QualifiedNetworksService.this;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Failed to call onQualifiedNetworksChanged. ");
          stringBuilder.append(remoteException);
          qualifiedNetworksService.loge(stringBuilder.toString());
        }  
    }
    
    public abstract void close();
  }
  
  class QualifiedNetworksServiceHandler extends Handler {
    final QualifiedNetworksService this$0;
    
    QualifiedNetworksServiceHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.arg1;
      QualifiedNetworksService.NetworkAvailabilityProvider networkAvailabilityProvider = QualifiedNetworksService.this.mProviders.get(i);
      int j = param1Message.what;
      if (j != 1) {
        if (j != 2) {
          if (j != 3) {
            if (j == 4)
              if (networkAvailabilityProvider != null)
                networkAvailabilityProvider.onUpdateQualifiedNetworkTypes(param1Message.arg2, (int[])param1Message.obj);  
          } else {
            for (j = 0; j < QualifiedNetworksService.this.mProviders.size(); j++) {
              QualifiedNetworksService.NetworkAvailabilityProvider networkAvailabilityProvider1 = QualifiedNetworksService.this.mProviders.get(j);
              if (networkAvailabilityProvider1 != null)
                networkAvailabilityProvider1.close(); 
            } 
            QualifiedNetworksService.this.mProviders.clear();
          } 
        } else if (networkAvailabilityProvider != null) {
          networkAvailabilityProvider.close();
          QualifiedNetworksService.this.mProviders.remove(i);
        } 
      } else {
        QualifiedNetworksService qualifiedNetworksService;
        if (QualifiedNetworksService.this.mProviders.get(i) != null) {
          qualifiedNetworksService = QualifiedNetworksService.this;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Network availability provider for slot ");
          stringBuilder.append(i);
          stringBuilder.append(" already existed.");
          qualifiedNetworksService.loge(stringBuilder.toString());
          return;
        } 
        networkAvailabilityProvider = QualifiedNetworksService.this.onCreateNetworkAvailabilityProvider(i);
        if (networkAvailabilityProvider != null) {
          QualifiedNetworksService.this.mProviders.put(i, networkAvailabilityProvider);
          IQualifiedNetworksServiceCallback iQualifiedNetworksServiceCallback = (IQualifiedNetworksServiceCallback)((Message)qualifiedNetworksService).obj;
          networkAvailabilityProvider.registerForQualifiedNetworkTypesChanged(iQualifiedNetworksServiceCallback);
        } else {
          qualifiedNetworksService = QualifiedNetworksService.this;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Failed to create network availability provider. slot index = ");
          stringBuilder.append(i);
          qualifiedNetworksService.loge(stringBuilder.toString());
        } 
      } 
    }
  }
  
  public QualifiedNetworksService() {
    HandlerThread handlerThread = new HandlerThread(TAG);
    handlerThread.start();
    this.mHandler = new QualifiedNetworksServiceHandler(this.mHandlerThread.getLooper());
    log("Qualified networks service created");
  }
  
  public IBinder onBind(Intent paramIntent) {
    if (paramIntent == null || !"android.telephony.data.QualifiedNetworksService".equals(paramIntent.getAction())) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected intent ");
      stringBuilder.append(paramIntent);
      loge(stringBuilder.toString());
      return null;
    } 
    return (IBinder)this.mBinder;
  }
  
  public boolean onUnbind(Intent paramIntent) {
    this.mHandler.obtainMessage(3).sendToTarget();
    return false;
  }
  
  public void onDestroy() {
    this.mHandlerThread.quit();
  }
  
  private class IQualifiedNetworksServiceWrapper extends IQualifiedNetworksService.Stub {
    final QualifiedNetworksService this$0;
    
    private IQualifiedNetworksServiceWrapper() {}
    
    public void createNetworkAvailabilityProvider(int param1Int, IQualifiedNetworksServiceCallback param1IQualifiedNetworksServiceCallback) {
      Message message = QualifiedNetworksService.this.mHandler.obtainMessage(1, param1Int, 0, param1IQualifiedNetworksServiceCallback);
      message.sendToTarget();
    }
    
    public void removeNetworkAvailabilityProvider(int param1Int) {
      Message message = QualifiedNetworksService.this.mHandler.obtainMessage(2, param1Int, 0);
      message.sendToTarget();
    }
  }
  
  private void log(String paramString) {
    Rlog.d(TAG, paramString);
  }
  
  private void loge(String paramString) {
    Rlog.e(TAG, paramString);
  }
  
  public abstract NetworkAvailabilityProvider onCreateNetworkAvailabilityProvider(int paramInt);
}
