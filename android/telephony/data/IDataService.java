package android.telephony.data;

import android.net.LinkProperties;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IDataService extends IInterface {
  void createDataServiceProvider(int paramInt) throws RemoteException;
  
  void deactivateDataCall(int paramInt1, int paramInt2, int paramInt3, IDataServiceCallback paramIDataServiceCallback) throws RemoteException;
  
  void registerForDataCallListChanged(int paramInt, IDataServiceCallback paramIDataServiceCallback) throws RemoteException;
  
  void removeDataServiceProvider(int paramInt) throws RemoteException;
  
  void requestDataCallList(int paramInt, IDataServiceCallback paramIDataServiceCallback) throws RemoteException;
  
  void setDataProfile(int paramInt, List<DataProfile> paramList, boolean paramBoolean, IDataServiceCallback paramIDataServiceCallback) throws RemoteException;
  
  void setInitialAttachApn(int paramInt, DataProfile paramDataProfile, boolean paramBoolean, IDataServiceCallback paramIDataServiceCallback) throws RemoteException;
  
  void setupDataCall(int paramInt1, int paramInt2, DataProfile paramDataProfile, boolean paramBoolean1, boolean paramBoolean2, int paramInt3, LinkProperties paramLinkProperties, IDataServiceCallback paramIDataServiceCallback) throws RemoteException;
  
  void unregisterForDataCallListChanged(int paramInt, IDataServiceCallback paramIDataServiceCallback) throws RemoteException;
  
  class Default implements IDataService {
    public void createDataServiceProvider(int param1Int) throws RemoteException {}
    
    public void removeDataServiceProvider(int param1Int) throws RemoteException {}
    
    public void setupDataCall(int param1Int1, int param1Int2, DataProfile param1DataProfile, boolean param1Boolean1, boolean param1Boolean2, int param1Int3, LinkProperties param1LinkProperties, IDataServiceCallback param1IDataServiceCallback) throws RemoteException {}
    
    public void deactivateDataCall(int param1Int1, int param1Int2, int param1Int3, IDataServiceCallback param1IDataServiceCallback) throws RemoteException {}
    
    public void setInitialAttachApn(int param1Int, DataProfile param1DataProfile, boolean param1Boolean, IDataServiceCallback param1IDataServiceCallback) throws RemoteException {}
    
    public void setDataProfile(int param1Int, List<DataProfile> param1List, boolean param1Boolean, IDataServiceCallback param1IDataServiceCallback) throws RemoteException {}
    
    public void requestDataCallList(int param1Int, IDataServiceCallback param1IDataServiceCallback) throws RemoteException {}
    
    public void registerForDataCallListChanged(int param1Int, IDataServiceCallback param1IDataServiceCallback) throws RemoteException {}
    
    public void unregisterForDataCallListChanged(int param1Int, IDataServiceCallback param1IDataServiceCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDataService {
    private static final String DESCRIPTOR = "android.telephony.data.IDataService";
    
    static final int TRANSACTION_createDataServiceProvider = 1;
    
    static final int TRANSACTION_deactivateDataCall = 4;
    
    static final int TRANSACTION_registerForDataCallListChanged = 8;
    
    static final int TRANSACTION_removeDataServiceProvider = 2;
    
    static final int TRANSACTION_requestDataCallList = 7;
    
    static final int TRANSACTION_setDataProfile = 6;
    
    static final int TRANSACTION_setInitialAttachApn = 5;
    
    static final int TRANSACTION_setupDataCall = 3;
    
    static final int TRANSACTION_unregisterForDataCallListChanged = 9;
    
    public Stub() {
      attachInterface(this, "android.telephony.data.IDataService");
    }
    
    public static IDataService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.data.IDataService");
      if (iInterface != null && iInterface instanceof IDataService)
        return (IDataService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 9:
          return "unregisterForDataCallListChanged";
        case 8:
          return "registerForDataCallListChanged";
        case 7:
          return "requestDataCallList";
        case 6:
          return "setDataProfile";
        case 5:
          return "setInitialAttachApn";
        case 4:
          return "deactivateDataCall";
        case 3:
          return "setupDataCall";
        case 2:
          return "removeDataServiceProvider";
        case 1:
          break;
      } 
      return "createDataServiceProvider";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ArrayList<DataProfile> arrayList;
      if (param1Int1 != 1598968902) {
        IDataServiceCallback iDataServiceCallback;
        int i;
        LinkProperties linkProperties;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 9:
            param1Parcel1.enforceInterface("android.telephony.data.IDataService");
            param1Int1 = param1Parcel1.readInt();
            iDataServiceCallback = IDataServiceCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            unregisterForDataCallListChanged(param1Int1, iDataServiceCallback);
            return true;
          case 8:
            iDataServiceCallback.enforceInterface("android.telephony.data.IDataService");
            param1Int1 = iDataServiceCallback.readInt();
            iDataServiceCallback = IDataServiceCallback.Stub.asInterface(iDataServiceCallback.readStrongBinder());
            registerForDataCallListChanged(param1Int1, iDataServiceCallback);
            return true;
          case 7:
            iDataServiceCallback.enforceInterface("android.telephony.data.IDataService");
            param1Int1 = iDataServiceCallback.readInt();
            iDataServiceCallback = IDataServiceCallback.Stub.asInterface(iDataServiceCallback.readStrongBinder());
            requestDataCallList(param1Int1, iDataServiceCallback);
            return true;
          case 6:
            iDataServiceCallback.enforceInterface("android.telephony.data.IDataService");
            param1Int1 = iDataServiceCallback.readInt();
            arrayList = iDataServiceCallback.createTypedArrayList(DataProfile.CREATOR);
            bool1 = bool2;
            if (iDataServiceCallback.readInt() != 0)
              bool1 = true; 
            iDataServiceCallback = IDataServiceCallback.Stub.asInterface(iDataServiceCallback.readStrongBinder());
            setDataProfile(param1Int1, arrayList, bool1, iDataServiceCallback);
            return true;
          case 5:
            iDataServiceCallback.enforceInterface("android.telephony.data.IDataService");
            param1Int1 = iDataServiceCallback.readInt();
            if (iDataServiceCallback.readInt() != 0) {
              DataProfile dataProfile = (DataProfile)DataProfile.CREATOR.createFromParcel((Parcel)iDataServiceCallback);
            } else {
              arrayList = null;
            } 
            if (iDataServiceCallback.readInt() != 0)
              bool1 = true; 
            iDataServiceCallback = IDataServiceCallback.Stub.asInterface(iDataServiceCallback.readStrongBinder());
            setInitialAttachApn(param1Int1, (DataProfile)arrayList, bool1, iDataServiceCallback);
            return true;
          case 4:
            iDataServiceCallback.enforceInterface("android.telephony.data.IDataService");
            i = iDataServiceCallback.readInt();
            param1Int2 = iDataServiceCallback.readInt();
            param1Int1 = iDataServiceCallback.readInt();
            iDataServiceCallback = IDataServiceCallback.Stub.asInterface(iDataServiceCallback.readStrongBinder());
            deactivateDataCall(i, param1Int2, param1Int1, iDataServiceCallback);
            return true;
          case 3:
            iDataServiceCallback.enforceInterface("android.telephony.data.IDataService");
            param1Int1 = iDataServiceCallback.readInt();
            i = iDataServiceCallback.readInt();
            if (iDataServiceCallback.readInt() != 0) {
              DataProfile dataProfile = (DataProfile)DataProfile.CREATOR.createFromParcel((Parcel)iDataServiceCallback);
            } else {
              arrayList = null;
            } 
            if (iDataServiceCallback.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            if (iDataServiceCallback.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            param1Int2 = iDataServiceCallback.readInt();
            if (iDataServiceCallback.readInt() != 0) {
              linkProperties = (LinkProperties)LinkProperties.CREATOR.createFromParcel((Parcel)iDataServiceCallback);
            } else {
              linkProperties = null;
            } 
            iDataServiceCallback = IDataServiceCallback.Stub.asInterface(iDataServiceCallback.readStrongBinder());
            setupDataCall(param1Int1, i, (DataProfile)arrayList, bool1, bool2, param1Int2, linkProperties, iDataServiceCallback);
            return true;
          case 2:
            iDataServiceCallback.enforceInterface("android.telephony.data.IDataService");
            param1Int1 = iDataServiceCallback.readInt();
            removeDataServiceProvider(param1Int1);
            return true;
          case 1:
            break;
        } 
        iDataServiceCallback.enforceInterface("android.telephony.data.IDataService");
        param1Int1 = iDataServiceCallback.readInt();
        createDataServiceProvider(param1Int1);
        return true;
      } 
      arrayList.writeString("android.telephony.data.IDataService");
      return true;
    }
    
    private static class Proxy implements IDataService {
      public static IDataService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.data.IDataService";
      }
      
      public void createDataServiceProvider(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.data.IDataService");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IDataService.Stub.getDefaultImpl() != null) {
            IDataService.Stub.getDefaultImpl().createDataServiceProvider(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeDataServiceProvider(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.data.IDataService");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IDataService.Stub.getDefaultImpl() != null) {
            IDataService.Stub.getDefaultImpl().removeDataServiceProvider(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setupDataCall(int param2Int1, int param2Int2, DataProfile param2DataProfile, boolean param2Boolean1, boolean param2Boolean2, int param2Int3, LinkProperties param2LinkProperties, IDataServiceCallback param2IDataServiceCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.data.IDataService");
          try {
            parcel.writeInt(param2Int1);
            try {
              boolean bool;
              parcel.writeInt(param2Int2);
              if (param2DataProfile != null) {
                parcel.writeInt(1);
                param2DataProfile.writeToParcel(parcel, 0);
              } else {
                parcel.writeInt(0);
              } 
              if (param2Boolean1) {
                bool = true;
              } else {
                bool = false;
              } 
              parcel.writeInt(bool);
              if (param2Boolean2) {
                bool = true;
              } else {
                bool = false;
              } 
              parcel.writeInt(bool);
              try {
                IBinder iBinder;
                parcel.writeInt(param2Int3);
                if (param2LinkProperties != null) {
                  parcel.writeInt(1);
                  param2LinkProperties.writeToParcel(parcel, 0);
                } else {
                  parcel.writeInt(0);
                } 
                if (param2IDataServiceCallback != null) {
                  iBinder = param2IDataServiceCallback.asBinder();
                } else {
                  iBinder = null;
                } 
                parcel.writeStrongBinder(iBinder);
                boolean bool1 = this.mRemote.transact(3, parcel, null, 1);
                if (!bool1 && IDataService.Stub.getDefaultImpl() != null) {
                  IDataService.Stub.getDefaultImpl().setupDataCall(param2Int1, param2Int2, param2DataProfile, param2Boolean1, param2Boolean2, param2Int3, param2LinkProperties, param2IDataServiceCallback);
                  parcel.recycle();
                  return;
                } 
                parcel.recycle();
                return;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2DataProfile;
      }
      
      public void deactivateDataCall(int param2Int1, int param2Int2, int param2Int3, IDataServiceCallback param2IDataServiceCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.data.IDataService");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          if (param2IDataServiceCallback != null) {
            iBinder = param2IDataServiceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IDataService.Stub.getDefaultImpl() != null) {
            IDataService.Stub.getDefaultImpl().deactivateDataCall(param2Int1, param2Int2, param2Int3, param2IDataServiceCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setInitialAttachApn(int param2Int, DataProfile param2DataProfile, boolean param2Boolean, IDataServiceCallback param2IDataServiceCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.data.IDataService");
          parcel.writeInt(param2Int);
          boolean bool = false;
          if (param2DataProfile != null) {
            parcel.writeInt(1);
            param2DataProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          if (param2IDataServiceCallback != null) {
            iBinder = param2IDataServiceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool1 = this.mRemote.transact(5, parcel, null, 1);
          if (!bool1 && IDataService.Stub.getDefaultImpl() != null) {
            IDataService.Stub.getDefaultImpl().setInitialAttachApn(param2Int, param2DataProfile, param2Boolean, param2IDataServiceCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setDataProfile(int param2Int, List<DataProfile> param2List, boolean param2Boolean, IDataServiceCallback param2IDataServiceCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.data.IDataService");
          parcel.writeInt(param2Int);
          parcel.writeTypedList(param2List);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2IDataServiceCallback != null) {
            iBinder = param2IDataServiceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool1 = this.mRemote.transact(6, parcel, null, 1);
          if (!bool1 && IDataService.Stub.getDefaultImpl() != null) {
            IDataService.Stub.getDefaultImpl().setDataProfile(param2Int, param2List, param2Boolean, param2IDataServiceCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestDataCallList(int param2Int, IDataServiceCallback param2IDataServiceCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.data.IDataService");
          parcel.writeInt(param2Int);
          if (param2IDataServiceCallback != null) {
            iBinder = param2IDataServiceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IDataService.Stub.getDefaultImpl() != null) {
            IDataService.Stub.getDefaultImpl().requestDataCallList(param2Int, param2IDataServiceCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void registerForDataCallListChanged(int param2Int, IDataServiceCallback param2IDataServiceCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.data.IDataService");
          parcel.writeInt(param2Int);
          if (param2IDataServiceCallback != null) {
            iBinder = param2IDataServiceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IDataService.Stub.getDefaultImpl() != null) {
            IDataService.Stub.getDefaultImpl().registerForDataCallListChanged(param2Int, param2IDataServiceCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void unregisterForDataCallListChanged(int param2Int, IDataServiceCallback param2IDataServiceCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.data.IDataService");
          parcel.writeInt(param2Int);
          if (param2IDataServiceCallback != null) {
            iBinder = param2IDataServiceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IDataService.Stub.getDefaultImpl() != null) {
            IDataService.Stub.getDefaultImpl().unregisterForDataCallListChanged(param2Int, param2IDataServiceCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDataService param1IDataService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDataService != null) {
          Proxy.sDefaultImpl = param1IDataService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDataService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
