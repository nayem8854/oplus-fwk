package android.telephony.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.OplusApnSetting;
import android.telephony.ServiceState;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ApnSetting implements Parcelable {
  private static final Map<Integer, String> APN_TYPE_INT_MAP;
  
  private static final Map<String, Integer> APN_TYPE_STRING_MAP;
  
  public static final int AUTH_TYPE_CHAP = 2;
  
  public static final int AUTH_TYPE_NONE = 0;
  
  public static final int AUTH_TYPE_PAP = 1;
  
  public static final int AUTH_TYPE_PAP_OR_CHAP = 3;
  
  public static final Parcelable.Creator<ApnSetting> CREATOR;
  
  private static final String LOG_TAG = "ApnSetting";
  
  public static final int MVNO_TYPE_GID = 2;
  
  public static final int MVNO_TYPE_ICCID = 3;
  
  public static final int MVNO_TYPE_IMSI = 1;
  
  private static final Map<Integer, String> MVNO_TYPE_INT_MAP;
  
  public static final int MVNO_TYPE_SPN = 0;
  
  private static final Map<String, Integer> MVNO_TYPE_STRING_MAP;
  
  private static final Map<Integer, String> PROTOCOL_INT_MAP;
  
  public static final int PROTOCOL_IP = 0;
  
  public static final int PROTOCOL_IPV4V6 = 2;
  
  public static final int PROTOCOL_IPV6 = 1;
  
  public static final int PROTOCOL_NON_IP = 4;
  
  public static final int PROTOCOL_PPP = 3;
  
  private static final Map<String, Integer> PROTOCOL_STRING_MAP;
  
  public static final int PROTOCOL_UNSTRUCTURED = 5;
  
  public static final int TYPE_ALL = 255;
  
  public static final String TYPE_ALL_STRING = "*";
  
  public static final int TYPE_CBS = 128;
  
  public static final String TYPE_CBS_STRING = "cbs";
  
  public static final int TYPE_DEFAULT = 17;
  
  public static final String TYPE_DEFAULT_STRING = "default";
  
  public static final int TYPE_DUN = 8;
  
  public static final String TYPE_DUN_STRING = "dun";
  
  public static final int TYPE_EMERGENCY = 512;
  
  public static final String TYPE_EMERGENCY_STRING = "emergency";
  
  public static final int TYPE_FOTA = 32;
  
  public static final String TYPE_FOTA_STRING = "fota";
  
  public static final int TYPE_HIPRI = 16;
  
  public static final String TYPE_HIPRI_STRING = "hipri";
  
  public static final int TYPE_IA = 256;
  
  public static final String TYPE_IA_STRING = "ia";
  
  public static final int TYPE_IMS = 64;
  
  public static final String TYPE_IMS_STRING = "ims";
  
  public static final int TYPE_MCX = 1024;
  
  public static final String TYPE_MCX_STRING = "mcx";
  
  public static final int TYPE_MMS = 2;
  
  public static final String TYPE_MMS_STRING = "mms";
  
  public static final int TYPE_NONE = 0;
  
  public static final int TYPE_SUPL = 4;
  
  public static final String TYPE_SUPL_STRING = "supl";
  
  public static final int TYPE_XCAP = 2048;
  
  public static final String TYPE_XCAP_STRING = "xcap";
  
  public static final int UNSET_MTU = 0;
  
  private static final int UNSPECIFIED_INT = -1;
  
  private static final String UNSPECIFIED_STRING = "";
  
  private static final String V2_FORMAT_REGEX = "^\\[ApnSettingV2\\]\\s*";
  
  private static final String V3_FORMAT_REGEX = "^\\[ApnSettingV3\\]\\s*";
  
  private static final String V4_FORMAT_REGEX = "^\\[ApnSettingV4\\]\\s*";
  
  private static final String V5_FORMAT_REGEX = "^\\[ApnSettingV5\\]\\s*";
  
  private static final String V6_FORMAT_REGEX = "^\\[ApnSettingV6\\]\\s*";
  
  private static final String V7_FORMAT_REGEX = "^\\[ApnSettingV7\\]\\s*";
  
  private static final boolean VDBG = false;
  
  private final String mApnName;
  
  private final int mApnSetId;
  
  private final int mApnTypeBitmask;
  
  private final int mAuthType;
  
  private final boolean mCarrierEnabled;
  
  private final int mCarrierId;
  
  private final String mEntryName;
  
  private final int mId;
  
  private final int mMaxConns;
  
  private final int mMaxConnsTime;
  
  private final String mMmsProxyAddress;
  
  private final int mMmsProxyPort;
  
  private final Uri mMmsc;
  
  private final int mMtu;
  
  private final String mMvnoMatchData;
  
  private final int mMvnoType;
  
  private final int mNetworkTypeBitmask;
  
  private final String mOperatorNumeric;
  
  private final String mPassword;
  
  static {
    ArrayMap<Object, Object> arrayMap1 = new ArrayMap<>();
    arrayMap1.put("*", Integer.valueOf(255));
    Map<String, Integer> map1 = APN_TYPE_STRING_MAP;
    Integer integer2 = Integer.valueOf(17);
    map1.put("default", integer2);
    Map<String, Integer> map2 = APN_TYPE_STRING_MAP;
    Integer integer1 = Integer.valueOf(2);
    map2.put("mms", integer1);
    Map<String, Integer> map3 = APN_TYPE_STRING_MAP;
    Integer integer3 = Integer.valueOf(4);
    map3.put("supl", integer3);
    Map<String, Integer> map5 = APN_TYPE_STRING_MAP;
    Integer integer5 = Integer.valueOf(8);
    map5.put("dun", integer5);
    Map<String, Integer> map7 = APN_TYPE_STRING_MAP;
    Integer integer7 = Integer.valueOf(16);
    map7.put("hipri", integer7);
    Map<String, Integer> map8 = APN_TYPE_STRING_MAP;
    Integer integer8 = Integer.valueOf(32);
    map8.put("fota", integer8);
    Map<String, Integer> map9 = APN_TYPE_STRING_MAP;
    Integer integer9 = Integer.valueOf(64);
    map9.put("ims", integer9);
    Map<String, Integer> map10 = APN_TYPE_STRING_MAP;
    Integer integer10 = Integer.valueOf(128);
    map10.put("cbs", integer10);
    Map<String, Integer> map11 = APN_TYPE_STRING_MAP;
    Integer integer11 = Integer.valueOf(256);
    map11.put("ia", integer11);
    APN_TYPE_STRING_MAP.put("emergency", Integer.valueOf(512));
    APN_TYPE_STRING_MAP.put("mcx", Integer.valueOf(1024));
    APN_TYPE_STRING_MAP.put("xcap", Integer.valueOf(2048));
    APN_TYPE_INT_MAP = (Map)(map11 = new ArrayMap<>());
    map11.put(integer2, "default");
    APN_TYPE_INT_MAP.put(integer1, "mms");
    APN_TYPE_INT_MAP.put(integer3, "supl");
    APN_TYPE_INT_MAP.put(integer5, "dun");
    APN_TYPE_INT_MAP.put(integer7, "hipri");
    APN_TYPE_INT_MAP.put(integer8, "fota");
    APN_TYPE_INT_MAP.put(integer9, "ims");
    APN_TYPE_INT_MAP.put(integer10, "cbs");
    APN_TYPE_INT_MAP.put(integer11, "ia");
    APN_TYPE_INT_MAP.put(Integer.valueOf(512), "emergency");
    APN_TYPE_INT_MAP.put(Integer.valueOf(1024), "mcx");
    APN_TYPE_INT_MAP.put(Integer.valueOf(2048), "xcap");
    ArrayMap<Object, Object> arrayMap3 = new ArrayMap<>();
    integer2 = Integer.valueOf(0);
    arrayMap3.put("IP", integer2);
    Map<String, Integer> map4 = PROTOCOL_STRING_MAP;
    Integer integer4 = Integer.valueOf(1);
    map4.put("IPV6", integer4);
    PROTOCOL_STRING_MAP.put("IPV4V6", integer1);
    Map<String, Integer> map6 = PROTOCOL_STRING_MAP;
    Integer integer6 = Integer.valueOf(3);
    map6.put("PPP", integer6);
    PROTOCOL_STRING_MAP.put("NON-IP", integer3);
    PROTOCOL_STRING_MAP.put("UNSTRUCTURED", Integer.valueOf(5));
    PROTOCOL_INT_MAP = (Map)(map6 = new ArrayMap<>());
    map6.put(integer2, "IP");
    PROTOCOL_INT_MAP.put(integer4, "IPV6");
    PROTOCOL_INT_MAP.put(integer1, "IPV4V6");
    PROTOCOL_INT_MAP.put(integer6, "PPP");
    PROTOCOL_INT_MAP.put(integer3, "NON-IP");
    PROTOCOL_INT_MAP.put(Integer.valueOf(5), "UNSTRUCTURED");
    ArrayMap<Object, Object> arrayMap2 = new ArrayMap<>();
    arrayMap2.put("spn", integer2);
    MVNO_TYPE_STRING_MAP.put("imsi", integer4);
    MVNO_TYPE_STRING_MAP.put("gid", integer1);
    MVNO_TYPE_STRING_MAP.put("iccid", integer6);
    MVNO_TYPE_INT_MAP = (Map)(arrayMap2 = new ArrayMap<>());
    arrayMap2.put(integer2, "spn");
    MVNO_TYPE_INT_MAP.put(integer4, "imsi");
    MVNO_TYPE_INT_MAP.put(integer1, "gid");
    MVNO_TYPE_INT_MAP.put(integer6, "iccid");
    CREATOR = new Parcelable.Creator<ApnSetting>() {
        public ApnSetting createFromParcel(Parcel param1Parcel) {
          return ApnSetting.readFromParcel(param1Parcel);
        }
        
        public ApnSetting[] newArray(int param1Int) {
          return new ApnSetting[param1Int];
        }
      };
  }
  
  private boolean mPermanentFailed = false;
  
  private final boolean mPersistent;
  
  private final int mProfileId;
  
  private final int mProtocol;
  
  private final String mProxyAddress;
  
  private final int mProxyPort;
  
  private final int mRoamingProtocol;
  
  private final int mSkip464Xlat;
  
  private final String mUser;
  
  private final int mWaitTime;
  
  public int getMtu() {
    return this.mMtu;
  }
  
  public int getProfileId() {
    return this.mProfileId;
  }
  
  public boolean isPersistent() {
    return this.mPersistent;
  }
  
  public int getMaxConns() {
    return this.mMaxConns;
  }
  
  public int getWaitTime() {
    return this.mWaitTime;
  }
  
  public int getMaxConnsTime() {
    return this.mMaxConnsTime;
  }
  
  public String getMvnoMatchData() {
    return this.mMvnoMatchData;
  }
  
  public int getApnSetId() {
    return this.mApnSetId;
  }
  
  public boolean getPermanentFailed() {
    return this.mPermanentFailed;
  }
  
  public void setPermanentFailed(boolean paramBoolean) {
    this.mPermanentFailed = paramBoolean;
  }
  
  public String getEntryName() {
    return this.mEntryName;
  }
  
  public String getApnName() {
    return this.mApnName;
  }
  
  @Deprecated
  public InetAddress getProxyAddress() {
    return inetAddressFromString(this.mProxyAddress);
  }
  
  public String getProxyAddressAsString() {
    return this.mProxyAddress;
  }
  
  public int getProxyPort() {
    return this.mProxyPort;
  }
  
  public Uri getMmsc() {
    return this.mMmsc;
  }
  
  @Deprecated
  public InetAddress getMmsProxyAddress() {
    return inetAddressFromString(this.mMmsProxyAddress);
  }
  
  public String getMmsProxyAddressAsString() {
    return this.mMmsProxyAddress;
  }
  
  public int getMmsProxyPort() {
    return this.mMmsProxyPort;
  }
  
  public String getUser() {
    return this.mUser;
  }
  
  public String getPassword() {
    return this.mPassword;
  }
  
  public int getAuthType() {
    return this.mAuthType;
  }
  
  public int getApnTypeBitmask() {
    return this.mApnTypeBitmask;
  }
  
  public int getId() {
    return this.mId;
  }
  
  public String getOperatorNumeric() {
    return this.mOperatorNumeric;
  }
  
  public int getProtocol() {
    return this.mProtocol;
  }
  
  public int getRoamingProtocol() {
    return this.mRoamingProtocol;
  }
  
  public boolean isEnabled() {
    return this.mCarrierEnabled;
  }
  
  public int getNetworkTypeBitmask() {
    return this.mNetworkTypeBitmask;
  }
  
  public int getMvnoType() {
    return this.mMvnoType;
  }
  
  public int getCarrierId() {
    return this.mCarrierId;
  }
  
  public int getSkip464Xlat() {
    return this.mSkip464Xlat;
  }
  
  private ApnSetting(Builder paramBuilder) {
    this.mEntryName = paramBuilder.mEntryName;
    this.mApnName = paramBuilder.mApnName;
    this.mProxyAddress = paramBuilder.mProxyAddress;
    this.mProxyPort = paramBuilder.mProxyPort;
    this.mMmsc = paramBuilder.mMmsc;
    this.mMmsProxyAddress = paramBuilder.mMmsProxyAddress;
    this.mMmsProxyPort = paramBuilder.mMmsProxyPort;
    this.mUser = paramBuilder.mUser;
    this.mPassword = paramBuilder.mPassword;
    this.mAuthType = paramBuilder.mAuthType;
    this.mApnTypeBitmask = paramBuilder.mApnTypeBitmask;
    this.mId = paramBuilder.mId;
    this.mOperatorNumeric = paramBuilder.mOperatorNumeric;
    this.mProtocol = paramBuilder.mProtocol;
    this.mRoamingProtocol = paramBuilder.mRoamingProtocol;
    this.mMtu = paramBuilder.mMtu;
    this.mCarrierEnabled = paramBuilder.mCarrierEnabled;
    this.mNetworkTypeBitmask = paramBuilder.mNetworkTypeBitmask;
    this.mProfileId = paramBuilder.mProfileId;
    this.mPersistent = paramBuilder.mModemCognitive;
    this.mMaxConns = paramBuilder.mMaxConns;
    this.mWaitTime = paramBuilder.mWaitTime;
    this.mMaxConnsTime = paramBuilder.mMaxConnsTime;
    this.mMvnoType = paramBuilder.mMvnoType;
    this.mMvnoMatchData = paramBuilder.mMvnoMatchData;
    this.mApnSetId = paramBuilder.mApnSetId;
    this.mCarrierId = paramBuilder.mCarrierId;
    this.mSkip464Xlat = paramBuilder.mSkip464Xlat;
  }
  
  public static ApnSetting makeApnSetting(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, int paramInt2, Uri paramUri, String paramString5, int paramInt3, String paramString6, String paramString7, int paramInt4, int paramInt5, int paramInt6, int paramInt7, boolean paramBoolean1, int paramInt8, int paramInt9, boolean paramBoolean2, int paramInt10, int paramInt11, int paramInt12, int paramInt13, int paramInt14, String paramString8, int paramInt15, int paramInt16, int paramInt17) {
    Builder builder2 = new Builder();
    builder2 = builder2.setId(paramInt1);
    Builder builder1 = builder2.setOperatorNumeric(paramString1);
    builder1 = builder1.setEntryName(paramString2);
    builder1 = builder1.setApnName(paramString3);
    builder1 = builder1.setProxyAddress(paramString4);
    builder1 = builder1.setProxyPort(paramInt2);
    builder1 = builder1.setMmsc(paramUri);
    builder1 = builder1.setMmsProxyAddress(paramString5);
    builder1 = builder1.setMmsProxyPort(paramInt3);
    builder1 = builder1.setUser(paramString6);
    builder1 = builder1.setPassword(paramString7);
    builder1 = builder1.setAuthType(paramInt4);
    builder1 = builder1.setApnTypeBitmask(paramInt5);
    builder1 = builder1.setProtocol(paramInt6);
    builder1 = builder1.setRoamingProtocol(paramInt7);
    builder1 = builder1.setCarrierEnabled(paramBoolean1);
    builder1 = builder1.setNetworkTypeBitmask(paramInt8);
    builder1 = builder1.setProfileId(paramInt9);
    builder1 = builder1.setModemCognitive(paramBoolean2);
    builder1 = builder1.setMaxConns(paramInt10);
    builder1 = builder1.setWaitTime(paramInt11);
    builder1 = builder1.setMaxConnsTime(paramInt12);
    builder1 = builder1.setMtu(paramInt13);
    builder1 = builder1.setMvnoType(paramInt14);
    builder1 = builder1.setMvnoMatchData(paramString8);
    builder1 = builder1.setApnSetId(paramInt15);
    builder1 = builder1.setCarrierId(paramInt16);
    builder1 = builder1.setSkip464Xlat(paramInt17);
    return builder1.buildWithoutCheck();
  }
  
  public static ApnSetting makeApnSetting(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, int paramInt2, Uri paramUri, String paramString5, int paramInt3, String paramString6, String paramString7, int paramInt4, int paramInt5, int paramInt6, int paramInt7, boolean paramBoolean1, int paramInt8, int paramInt9, boolean paramBoolean2, int paramInt10, int paramInt11, int paramInt12, int paramInt13, int paramInt14, String paramString8) {
    return makeApnSetting(paramInt1, paramString1, paramString2, paramString3, paramString4, paramInt2, paramUri, paramString5, paramInt3, paramString6, paramString7, paramInt4, paramInt5, paramInt6, paramInt7, paramBoolean1, paramInt8, paramInt9, paramBoolean2, paramInt10, paramInt11, paramInt12, paramInt13, paramInt14, paramString8, 0, -1, -1);
  }
  
  public static ApnSetting makeApnSetting(Cursor paramCursor) {
    boolean bool1, bool2;
    String str1 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("numeric"));
    String str2 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("type"));
    int i = OplusApnSetting.oemGetApnTypesBitmaskFromString(str2, str1);
    int j = paramCursor.getColumnIndexOrThrow("network_type_bitmask");
    j = paramCursor.getInt(j);
    if (j == 0) {
      j = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("bearer_bitmask"));
      j = ServiceState.convertBearerBitmaskToNetworkTypeBitmask(j);
    } 
    int k = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("_id"));
    String str3 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("numeric"));
    String str4 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("name"));
    str1 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("apn"));
    int m = paramCursor.getColumnIndexOrThrow("proxy");
    String str5 = paramCursor.getString(m);
    m = paramCursor.getColumnIndexOrThrow("port");
    m = portFromString(paramCursor.getString(m));
    int n = paramCursor.getColumnIndexOrThrow("mmsc");
    Uri uri = UriFromString(paramCursor.getString(n));
    n = paramCursor.getColumnIndexOrThrow("mmsproxy");
    String str6 = paramCursor.getString(n);
    n = paramCursor.getColumnIndexOrThrow("mmsport");
    int i1 = portFromString(paramCursor.getString(n));
    String str7 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("user"));
    String str8 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("password"));
    int i2 = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("authtype"));
    String str9 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("protocol"));
    int i3 = getProtocolIntFromString(str9);
    str9 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("roaming_protocol"));
    int i4 = getProtocolIntFromString(str9);
    if (paramCursor.getInt(paramCursor.getColumnIndexOrThrow("carrier_enabled")) == 1) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    int i5 = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("profile_id"));
    if (paramCursor.getInt(paramCursor.getColumnIndexOrThrow("modem_cognitive")) == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    int i6 = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("max_conns"));
    int i7 = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("wait_time"));
    int i8 = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("max_conns_time"));
    int i9 = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("mtu"));
    str9 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("mvno_type"));
    int i10 = getMvnoTypeIntFromString(str9);
    str9 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("mvno_match_data"));
    int i11 = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("apn_set_id"));
    int i12 = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("carrier_id"));
    n = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("skip_464xlat"));
    return makeApnSetting(k, str3, str4, str1, str5, m, uri, str6, i1, str7, str8, i2, i, i3, i4, bool1, j, i5, bool2, i6, i7, i8, i9, i10, str9, i11, i12, n);
  }
  
  public static ApnSetting makeApnSetting(ApnSetting paramApnSetting) {
    return makeApnSetting(paramApnSetting.mId, paramApnSetting.mOperatorNumeric, paramApnSetting.mEntryName, paramApnSetting.mApnName, paramApnSetting.mProxyAddress, paramApnSetting.mProxyPort, paramApnSetting.mMmsc, paramApnSetting.mMmsProxyAddress, paramApnSetting.mMmsProxyPort, paramApnSetting.mUser, paramApnSetting.mPassword, paramApnSetting.mAuthType, paramApnSetting.mApnTypeBitmask, paramApnSetting.mProtocol, paramApnSetting.mRoamingProtocol, paramApnSetting.mCarrierEnabled, paramApnSetting.mNetworkTypeBitmask, paramApnSetting.mProfileId, paramApnSetting.mPersistent, paramApnSetting.mMaxConns, paramApnSetting.mWaitTime, paramApnSetting.mMaxConnsTime, paramApnSetting.mMtu, paramApnSetting.mMvnoType, paramApnSetting.mMvnoMatchData, paramApnSetting.mApnSetId, paramApnSetting.mCarrierId, paramApnSetting.mSkip464Xlat);
  }
  
  public static ApnSetting fromString(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: ifnonnull -> 6
    //   4: aconst_null
    //   5: areturn
    //   6: aload_0
    //   7: ldc_w '^\[ApnSettingV7\]\s*.*'
    //   10: invokevirtual matches : (Ljava/lang/String;)Z
    //   13: ifeq -> 31
    //   16: aload_0
    //   17: ldc '^\[ApnSettingV7\]\s*'
    //   19: ldc ''
    //   21: invokevirtual replaceFirst : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   24: astore_0
    //   25: bipush #7
    //   27: istore_1
    //   28: goto -> 154
    //   31: aload_0
    //   32: ldc_w '^\[ApnSettingV6\]\s*.*'
    //   35: invokevirtual matches : (Ljava/lang/String;)Z
    //   38: ifeq -> 56
    //   41: aload_0
    //   42: ldc '^\[ApnSettingV6\]\s*'
    //   44: ldc ''
    //   46: invokevirtual replaceFirst : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   49: astore_0
    //   50: bipush #6
    //   52: istore_1
    //   53: goto -> 154
    //   56: aload_0
    //   57: ldc_w '^\[ApnSettingV5\]\s*.*'
    //   60: invokevirtual matches : (Ljava/lang/String;)Z
    //   63: ifeq -> 80
    //   66: aload_0
    //   67: ldc '^\[ApnSettingV5\]\s*'
    //   69: ldc ''
    //   71: invokevirtual replaceFirst : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   74: astore_0
    //   75: iconst_5
    //   76: istore_1
    //   77: goto -> 154
    //   80: aload_0
    //   81: ldc_w '^\[ApnSettingV4\]\s*.*'
    //   84: invokevirtual matches : (Ljava/lang/String;)Z
    //   87: ifeq -> 104
    //   90: aload_0
    //   91: ldc '^\[ApnSettingV4\]\s*'
    //   93: ldc ''
    //   95: invokevirtual replaceFirst : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   98: astore_0
    //   99: iconst_4
    //   100: istore_1
    //   101: goto -> 154
    //   104: aload_0
    //   105: ldc_w '^\[ApnSettingV3\]\s*.*'
    //   108: invokevirtual matches : (Ljava/lang/String;)Z
    //   111: ifeq -> 128
    //   114: aload_0
    //   115: ldc '^\[ApnSettingV3\]\s*'
    //   117: ldc ''
    //   119: invokevirtual replaceFirst : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   122: astore_0
    //   123: iconst_3
    //   124: istore_1
    //   125: goto -> 154
    //   128: aload_0
    //   129: ldc_w '^\[ApnSettingV2\]\s*.*'
    //   132: invokevirtual matches : (Ljava/lang/String;)Z
    //   135: ifeq -> 152
    //   138: aload_0
    //   139: ldc '^\[ApnSettingV2\]\s*'
    //   141: ldc ''
    //   143: invokevirtual replaceFirst : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   146: astore_0
    //   147: iconst_2
    //   148: istore_1
    //   149: goto -> 154
    //   152: iconst_1
    //   153: istore_1
    //   154: aload_0
    //   155: ldc_w '\s*,\s*'
    //   158: iconst_m1
    //   159: invokevirtual split : (Ljava/lang/String;I)[Ljava/lang/String;
    //   162: astore_2
    //   163: aload_2
    //   164: arraylength
    //   165: bipush #14
    //   167: if_icmpge -> 172
    //   170: aconst_null
    //   171: areturn
    //   172: aload_2
    //   173: bipush #12
    //   175: aaload
    //   176: invokestatic parseInt : (Ljava/lang/String;)I
    //   179: istore_3
    //   180: goto -> 186
    //   183: astore_0
    //   184: iconst_0
    //   185: istore_3
    //   186: iconst_0
    //   187: istore #4
    //   189: iconst_0
    //   190: istore #5
    //   192: iconst_0
    //   193: istore #6
    //   195: iconst_0
    //   196: istore #7
    //   198: iconst_0
    //   199: istore #8
    //   201: iconst_0
    //   202: istore #9
    //   204: iconst_0
    //   205: istore #10
    //   207: iconst_0
    //   208: istore #11
    //   210: iconst_0
    //   211: istore #12
    //   213: ldc ''
    //   215: astore #13
    //   217: ldc ''
    //   219: astore_0
    //   220: iconst_0
    //   221: istore #14
    //   223: iconst_m1
    //   224: istore #15
    //   226: iload_1
    //   227: iconst_1
    //   228: if_icmpne -> 334
    //   231: aload_2
    //   232: arraylength
    //   233: bipush #13
    //   235: isub
    //   236: anewarray java/lang/String
    //   239: astore #16
    //   241: aload_2
    //   242: bipush #13
    //   244: aload #16
    //   246: iconst_0
    //   247: aload_2
    //   248: arraylength
    //   249: bipush #13
    //   251: isub
    //   252: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   255: getstatic android/telephony/data/ApnSetting.PROTOCOL_INT_MAP : Ljava/util/Map;
    //   258: iconst_0
    //   259: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   262: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   267: checkcast java/lang/String
    //   270: astore_0
    //   271: getstatic android/telephony/data/ApnSetting.PROTOCOL_INT_MAP : Ljava/util/Map;
    //   274: iconst_0
    //   275: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   278: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   283: checkcast java/lang/String
    //   286: astore #13
    //   288: iconst_1
    //   289: istore #6
    //   291: iconst_0
    //   292: istore_1
    //   293: iconst_0
    //   294: istore #17
    //   296: iconst_0
    //   297: istore #7
    //   299: iconst_0
    //   300: istore #4
    //   302: iconst_0
    //   303: istore #9
    //   305: iconst_0
    //   306: istore #8
    //   308: ldc ''
    //   310: astore #18
    //   312: ldc ''
    //   314: astore #19
    //   316: iconst_0
    //   317: istore #10
    //   319: iconst_m1
    //   320: istore #11
    //   322: iconst_m1
    //   323: istore #20
    //   325: iconst_0
    //   326: istore #15
    //   328: iconst_0
    //   329: istore #12
    //   331: goto -> 769
    //   334: aload_2
    //   335: arraylength
    //   336: bipush #18
    //   338: if_icmpge -> 343
    //   341: aconst_null
    //   342: areturn
    //   343: aload_2
    //   344: bipush #13
    //   346: aaload
    //   347: ldc_w '\s*\|\s*'
    //   350: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   353: astore #18
    //   355: aload_2
    //   356: bipush #14
    //   358: aaload
    //   359: astore #16
    //   361: aload_2
    //   362: bipush #15
    //   364: aaload
    //   365: astore #19
    //   367: aload_2
    //   368: bipush #16
    //   370: aaload
    //   371: invokestatic parseBoolean : (Ljava/lang/String;)Z
    //   374: istore #17
    //   376: aload_2
    //   377: bipush #17
    //   379: aaload
    //   380: invokestatic getBitmaskFromString : (Ljava/lang/String;)I
    //   383: istore #20
    //   385: iload #9
    //   387: istore_1
    //   388: iload #11
    //   390: istore #9
    //   392: aload_2
    //   393: arraylength
    //   394: bipush #22
    //   396: if_icmple -> 516
    //   399: aload_2
    //   400: bipush #19
    //   402: aaload
    //   403: invokestatic parseBoolean : (Ljava/lang/String;)Z
    //   406: istore #6
    //   408: iload #5
    //   410: istore #4
    //   412: iload #8
    //   414: istore #7
    //   416: iload #10
    //   418: istore_1
    //   419: aload_2
    //   420: bipush #18
    //   422: aaload
    //   423: invokestatic parseInt : (Ljava/lang/String;)I
    //   426: istore #9
    //   428: iload #9
    //   430: istore #4
    //   432: iload #8
    //   434: istore #7
    //   436: iload #10
    //   438: istore_1
    //   439: aload_2
    //   440: bipush #20
    //   442: aaload
    //   443: invokestatic parseInt : (Ljava/lang/String;)I
    //   446: istore #8
    //   448: iload #9
    //   450: istore #4
    //   452: iload #8
    //   454: istore #7
    //   456: iload #10
    //   458: istore_1
    //   459: aload_2
    //   460: bipush #21
    //   462: aaload
    //   463: invokestatic parseInt : (Ljava/lang/String;)I
    //   466: istore #10
    //   468: iload #9
    //   470: istore #4
    //   472: iload #8
    //   474: istore #7
    //   476: iload #10
    //   478: istore_1
    //   479: aload_2
    //   480: bipush #22
    //   482: aaload
    //   483: invokestatic parseInt : (Ljava/lang/String;)I
    //   486: istore #5
    //   488: iload #5
    //   490: istore #11
    //   492: iload #9
    //   494: istore #4
    //   496: iload #8
    //   498: istore #7
    //   500: iload #10
    //   502: istore_1
    //   503: iload #11
    //   505: istore #9
    //   507: goto -> 516
    //   510: astore #21
    //   512: iload #11
    //   514: istore #9
    //   516: iload #12
    //   518: istore #8
    //   520: aload_2
    //   521: arraylength
    //   522: bipush #23
    //   524: if_icmple -> 545
    //   527: aload_2
    //   528: bipush #23
    //   530: aaload
    //   531: invokestatic parseInt : (Ljava/lang/String;)I
    //   534: istore #8
    //   536: goto -> 545
    //   539: astore #21
    //   541: iload #12
    //   543: istore #8
    //   545: aload_2
    //   546: arraylength
    //   547: bipush #25
    //   549: if_icmple -> 563
    //   552: aload_2
    //   553: bipush #24
    //   555: aaload
    //   556: astore #13
    //   558: aload_2
    //   559: bipush #25
    //   561: aaload
    //   562: astore_0
    //   563: aload_2
    //   564: arraylength
    //   565: bipush #26
    //   567: if_icmple -> 582
    //   570: aload_2
    //   571: bipush #26
    //   573: aaload
    //   574: invokestatic getBitmaskFromString : (Ljava/lang/String;)I
    //   577: istore #12
    //   579: goto -> 585
    //   582: iconst_0
    //   583: istore #12
    //   585: iload #14
    //   587: istore #10
    //   589: aload_2
    //   590: arraylength
    //   591: bipush #27
    //   593: if_icmple -> 605
    //   596: aload_2
    //   597: bipush #27
    //   599: aaload
    //   600: invokestatic parseInt : (Ljava/lang/String;)I
    //   603: istore #10
    //   605: iload #15
    //   607: istore #11
    //   609: aload_2
    //   610: arraylength
    //   611: bipush #28
    //   613: if_icmple -> 625
    //   616: aload_2
    //   617: bipush #28
    //   619: aaload
    //   620: invokestatic parseInt : (Ljava/lang/String;)I
    //   623: istore #11
    //   625: aload_2
    //   626: arraylength
    //   627: bipush #29
    //   629: if_icmple -> 702
    //   632: aload_2
    //   633: bipush #29
    //   635: aaload
    //   636: invokestatic parseInt : (Ljava/lang/String;)I
    //   639: istore #5
    //   641: iload #6
    //   643: istore #22
    //   645: iload_1
    //   646: istore #14
    //   648: aload #13
    //   650: astore #23
    //   652: aload_0
    //   653: astore #21
    //   655: aload #19
    //   657: astore #13
    //   659: iload #17
    //   661: istore #6
    //   663: aload #16
    //   665: astore_0
    //   666: iload #20
    //   668: istore #15
    //   670: aload #18
    //   672: astore #16
    //   674: iload #4
    //   676: istore_1
    //   677: iload #22
    //   679: istore #17
    //   681: iload #14
    //   683: istore #4
    //   685: aload #23
    //   687: astore #18
    //   689: aload #21
    //   691: astore #19
    //   693: iload #5
    //   695: istore #20
    //   697: goto -> 769
    //   700: astore #21
    //   702: iload #4
    //   704: istore #15
    //   706: aload #13
    //   708: astore #21
    //   710: aload_0
    //   711: astore #24
    //   713: iconst_m1
    //   714: istore #4
    //   716: aload #19
    //   718: astore #13
    //   720: iload #20
    //   722: istore #14
    //   724: iload #17
    //   726: istore #22
    //   728: aload #18
    //   730: astore #23
    //   732: aload #16
    //   734: astore_0
    //   735: iload #4
    //   737: istore #20
    //   739: aload #24
    //   741: astore #19
    //   743: aload #21
    //   745: astore #18
    //   747: iload_1
    //   748: istore #4
    //   750: iload #6
    //   752: istore #17
    //   754: iload #15
    //   756: istore_1
    //   757: iload #22
    //   759: istore #6
    //   761: aload #23
    //   763: astore #16
    //   765: iload #14
    //   767: istore #15
    //   769: iload #12
    //   771: ifne -> 784
    //   774: iload #15
    //   776: invokestatic convertBearerBitmaskToNetworkTypeBitmask : (I)I
    //   779: istore #12
    //   781: goto -> 784
    //   784: new java/lang/StringBuilder
    //   787: dup
    //   788: invokespecial <init> : ()V
    //   791: astore #21
    //   793: aload #21
    //   795: aload_2
    //   796: bipush #10
    //   798: aaload
    //   799: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   802: pop
    //   803: aload #21
    //   805: aload_2
    //   806: bipush #11
    //   808: aaload
    //   809: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   812: pop
    //   813: aload #21
    //   815: invokevirtual toString : ()Ljava/lang/String;
    //   818: astore #24
    //   820: aload_2
    //   821: iconst_0
    //   822: aaload
    //   823: astore #25
    //   825: aload_2
    //   826: iconst_1
    //   827: aaload
    //   828: astore #21
    //   830: aload_2
    //   831: iconst_2
    //   832: aaload
    //   833: astore #23
    //   835: aload_2
    //   836: iconst_3
    //   837: aaload
    //   838: astore #26
    //   840: aload #26
    //   842: invokestatic portFromString : (Ljava/lang/String;)I
    //   845: istore #14
    //   847: aload_2
    //   848: bipush #7
    //   850: aaload
    //   851: invokestatic UriFromString : (Ljava/lang/String;)Landroid/net/Uri;
    //   854: astore #27
    //   856: aload_2
    //   857: bipush #8
    //   859: aaload
    //   860: astore #26
    //   862: aload_2
    //   863: bipush #9
    //   865: aaload
    //   866: astore #28
    //   868: aload #28
    //   870: invokestatic portFromString : (Ljava/lang/String;)I
    //   873: istore #29
    //   875: aload_2
    //   876: iconst_4
    //   877: aaload
    //   878: astore #28
    //   880: aload_2
    //   881: iconst_5
    //   882: aaload
    //   883: astore_2
    //   884: ldc_w ','
    //   887: aload #16
    //   889: invokestatic join : (Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;
    //   892: invokestatic getApnTypesBitmaskFromString : (Ljava/lang/String;)I
    //   895: istore #30
    //   897: aload_0
    //   898: invokestatic getProtocolIntFromString : (Ljava/lang/String;)I
    //   901: istore #31
    //   903: aload #13
    //   905: invokestatic getProtocolIntFromString : (Ljava/lang/String;)I
    //   908: istore #15
    //   910: aload #18
    //   912: invokestatic getMvnoTypeIntFromString : (Ljava/lang/String;)I
    //   915: istore #5
    //   917: iconst_m1
    //   918: aload #24
    //   920: aload #25
    //   922: aload #21
    //   924: aload #23
    //   926: iload #14
    //   928: aload #27
    //   930: aload #26
    //   932: iload #29
    //   934: aload #28
    //   936: aload_2
    //   937: iload_3
    //   938: iload #30
    //   940: iload #31
    //   942: iload #15
    //   944: iload #6
    //   946: iload #12
    //   948: iload_1
    //   949: iload #17
    //   951: iload #7
    //   953: iload #4
    //   955: iload #9
    //   957: iload #8
    //   959: iload #5
    //   961: aload #19
    //   963: iload #10
    //   965: iload #11
    //   967: iload #20
    //   969: invokestatic makeApnSetting : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/net/Uri;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIIIZIIZIIIIILjava/lang/String;III)Landroid/telephony/data/ApnSetting;
    //   972: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #984	-> 0
    //   #988	-> 6
    //   #989	-> 16
    //   #990	-> 16
    //   #991	-> 31
    //   #992	-> 41
    //   #993	-> 41
    //   #994	-> 56
    //   #995	-> 66
    //   #996	-> 66
    //   #997	-> 80
    //   #998	-> 90
    //   #999	-> 90
    //   #1000	-> 104
    //   #1001	-> 114
    //   #1002	-> 114
    //   #1003	-> 128
    //   #1004	-> 138
    //   #1005	-> 138
    //   #1007	-> 152
    //   #1010	-> 154
    //   #1011	-> 163
    //   #1012	-> 170
    //   #1017	-> 172
    //   #1020	-> 180
    //   #1018	-> 183
    //   #1019	-> 184
    //   #1025	-> 186
    //   #1026	-> 186
    //   #1027	-> 186
    //   #1028	-> 192
    //   #1029	-> 195
    //   #1030	-> 201
    //   #1031	-> 207
    //   #1032	-> 210
    //   #1033	-> 213
    //   #1034	-> 217
    //   #1035	-> 220
    //   #1036	-> 223
    //   #1037	-> 226
    //   #1038	-> 226
    //   #1039	-> 231
    //   #1040	-> 241
    //   #1041	-> 255
    //   #1042	-> 271
    //   #1043	-> 288
    //   #1045	-> 334
    //   #1046	-> 341
    //   #1048	-> 343
    //   #1049	-> 355
    //   #1050	-> 361
    //   #1051	-> 367
    //   #1053	-> 376
    //   #1055	-> 385
    //   #1056	-> 399
    //   #1058	-> 408
    //   #1059	-> 428
    //   #1060	-> 448
    //   #1061	-> 468
    //   #1063	-> 488
    //   #1062	-> 510
    //   #1065	-> 516
    //   #1067	-> 527
    //   #1069	-> 536
    //   #1068	-> 539
    //   #1071	-> 545
    //   #1072	-> 552
    //   #1073	-> 558
    //   #1075	-> 563
    //   #1076	-> 570
    //   #1075	-> 582
    //   #1078	-> 585
    //   #1079	-> 596
    //   #1081	-> 605
    //   #1082	-> 616
    //   #1084	-> 625
    //   #1086	-> 632
    //   #1088	-> 641
    //   #1087	-> 700
    //   #1094	-> 702
    //   #1095	-> 774
    //   #1096	-> 774
    //   #1094	-> 784
    //   #1098	-> 784
    //   #1099	-> 840
    //   #1100	-> 868
    //   #1101	-> 884
    //   #1102	-> 897
    //   #1104	-> 910
    //   #1098	-> 917
    // Exception table:
    //   from	to	target	type
    //   172	180	183	java/lang/NumberFormatException
    //   419	428	510	java/lang/NumberFormatException
    //   439	448	510	java/lang/NumberFormatException
    //   459	468	510	java/lang/NumberFormatException
    //   479	488	510	java/lang/NumberFormatException
    //   527	536	539	java/lang/NumberFormatException
    //   632	641	700	java/lang/NumberFormatException
  }
  
  public static List<ApnSetting> arrayFromString(String paramString) {
    ArrayList<ApnSetting> arrayList = new ArrayList();
    if (TextUtils.isEmpty(paramString))
      return arrayList; 
    for (String str : paramString.split("\\s*;\\s*")) {
      ApnSetting apnSetting = fromString(str);
      if (apnSetting != null)
        arrayList.add(apnSetting); 
    } 
    return arrayList;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[ApnSettingV7] ");
    String str = this.mEntryName;
    stringBuilder.append(str);
    stringBuilder.append(", ");
    stringBuilder.append(this.mId);
    stringBuilder.append(", ");
    stringBuilder.append(this.mOperatorNumeric);
    stringBuilder.append(", ");
    stringBuilder.append(this.mApnName);
    stringBuilder.append(", ");
    stringBuilder.append(this.mProxyAddress);
    stringBuilder.append(", ");
    stringBuilder.append(UriToString(this.mMmsc));
    stringBuilder.append(", ");
    stringBuilder.append(this.mMmsProxyAddress);
    stringBuilder.append(", ");
    stringBuilder.append(portToString(this.mMmsProxyPort));
    stringBuilder.append(", ");
    stringBuilder.append(portToString(this.mProxyPort));
    stringBuilder.append(", ");
    stringBuilder.append(this.mAuthType);
    stringBuilder.append(", ");
    String[] arrayOfString = getApnTypesStringFromBitmask(this.mApnTypeBitmask).split(",");
    stringBuilder.append(TextUtils.join(" | ", (Object[])arrayOfString));
    stringBuilder.append(", ");
    stringBuilder.append(PROTOCOL_INT_MAP.get(Integer.valueOf(this.mProtocol)));
    stringBuilder.append(", ");
    stringBuilder.append(PROTOCOL_INT_MAP.get(Integer.valueOf(this.mRoamingProtocol)));
    stringBuilder.append(", ");
    stringBuilder.append(this.mCarrierEnabled);
    stringBuilder.append(", ");
    stringBuilder.append(this.mProfileId);
    stringBuilder.append(", ");
    stringBuilder.append(this.mPersistent);
    stringBuilder.append(", ");
    stringBuilder.append(this.mMaxConns);
    stringBuilder.append(", ");
    stringBuilder.append(this.mWaitTime);
    stringBuilder.append(", ");
    stringBuilder.append(this.mMaxConnsTime);
    stringBuilder.append(", ");
    stringBuilder.append(this.mMtu);
    stringBuilder.append(", ");
    stringBuilder.append(MVNO_TYPE_INT_MAP.get(Integer.valueOf(this.mMvnoType)));
    stringBuilder.append(", ");
    stringBuilder.append(this.mMvnoMatchData);
    stringBuilder.append(", ");
    stringBuilder.append(this.mPermanentFailed);
    stringBuilder.append(", ");
    stringBuilder.append(this.mNetworkTypeBitmask);
    stringBuilder.append(", ");
    stringBuilder.append(this.mApnSetId);
    stringBuilder.append(", ");
    stringBuilder.append(this.mCarrierId);
    stringBuilder.append(", ");
    stringBuilder.append(this.mSkip464Xlat);
    return stringBuilder.toString();
  }
  
  public boolean hasMvnoParams() {
    if (!TextUtils.isEmpty(getMvnoTypeStringFromInt(this.mMvnoType))) {
      String str = this.mMvnoMatchData;
      if (!TextUtils.isEmpty(str))
        return true; 
    } 
    return false;
  }
  
  private boolean hasApnType(int paramInt) {
    boolean bool;
    if ((this.mApnTypeBitmask & paramInt) == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isEmergencyApn() {
    return hasApnType(512);
  }
  
  public boolean canHandleType(int paramInt) {
    if (!this.mCarrierEnabled)
      return false; 
    if (hasApnType(paramInt))
      return true; 
    return false;
  }
  
  private boolean typeSameAny(ApnSetting paramApnSetting1, ApnSetting paramApnSetting2) {
    if ((paramApnSetting1.mApnTypeBitmask & paramApnSetting2.mApnTypeBitmask) != 0)
      return true; 
    return false;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof ApnSetting;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    if (this.mEntryName.equals(((ApnSetting)paramObject).mEntryName)) {
      int i = this.mId;
      if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((ApnSetting)paramObject).mId))) {
        String str1 = this.mOperatorNumeric, str2 = ((ApnSetting)paramObject).mOperatorNumeric;
        if (Objects.equals(str1, str2)) {
          str2 = this.mApnName;
          str1 = ((ApnSetting)paramObject).mApnName;
          if (Objects.equals(str2, str1)) {
            str1 = this.mProxyAddress;
            str2 = ((ApnSetting)paramObject).mProxyAddress;
            if (Objects.equals(str1, str2)) {
              Uri uri1 = this.mMmsc, uri2 = ((ApnSetting)paramObject).mMmsc;
              if (Objects.equals(uri1, uri2)) {
                String str4 = this.mMmsProxyAddress, str3 = ((ApnSetting)paramObject).mMmsProxyAddress;
                if (Objects.equals(str4, str3)) {
                  i = this.mMmsProxyPort;
                  if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((ApnSetting)paramObject).mMmsProxyPort))) {
                    i = this.mProxyPort;
                    if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((ApnSetting)paramObject).mProxyPort))) {
                      str3 = this.mUser;
                      str4 = ((ApnSetting)paramObject).mUser;
                      if (Objects.equals(str3, str4)) {
                        str3 = this.mPassword;
                        str4 = ((ApnSetting)paramObject).mPassword;
                        if (Objects.equals(str3, str4)) {
                          i = this.mAuthType;
                          if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((ApnSetting)paramObject).mAuthType))) {
                            i = this.mApnTypeBitmask;
                            if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((ApnSetting)paramObject).mApnTypeBitmask))) {
                              i = this.mProtocol;
                              if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((ApnSetting)paramObject).mProtocol))) {
                                i = this.mRoamingProtocol;
                                if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((ApnSetting)paramObject).mRoamingProtocol))) {
                                  bool = this.mCarrierEnabled;
                                  if (Objects.equals(Boolean.valueOf(bool), Boolean.valueOf(((ApnSetting)paramObject).mCarrierEnabled))) {
                                    i = this.mProfileId;
                                    if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((ApnSetting)paramObject).mProfileId))) {
                                      bool = this.mPersistent;
                                      if (Objects.equals(Boolean.valueOf(bool), Boolean.valueOf(((ApnSetting)paramObject).mPersistent))) {
                                        i = this.mMaxConns;
                                        if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((ApnSetting)paramObject).mMaxConns))) {
                                          i = this.mWaitTime;
                                          if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((ApnSetting)paramObject).mWaitTime))) {
                                            i = this.mMaxConnsTime;
                                            if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((ApnSetting)paramObject).mMaxConnsTime))) {
                                              i = this.mMtu;
                                              if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((ApnSetting)paramObject).mMtu))) {
                                                i = this.mMvnoType;
                                                if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((ApnSetting)paramObject).mMvnoType))) {
                                                  str4 = this.mMvnoMatchData;
                                                  str3 = ((ApnSetting)paramObject).mMvnoMatchData;
                                                  if (Objects.equals(str4, str3)) {
                                                    i = this.mNetworkTypeBitmask;
                                                    if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((ApnSetting)paramObject).mNetworkTypeBitmask))) {
                                                      i = this.mApnSetId;
                                                      if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((ApnSetting)paramObject).mApnSetId))) {
                                                        i = this.mCarrierId;
                                                        if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((ApnSetting)paramObject).mCarrierId))) {
                                                          i = this.mSkip464Xlat;
                                                          if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((ApnSetting)paramObject).mSkip464Xlat)))
                                                            bool1 = true; 
                                                        } 
                                                      } 
                                                    } 
                                                  } 
                                                } 
                                              } 
                                            } 
                                          } 
                                        } 
                                      } 
                                    } 
                                  } 
                                } 
                              } 
                            } 
                          } 
                        } 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
    } 
    return bool1;
  }
  
  public boolean equals(Object paramObject, boolean paramBoolean) {
    // Byte code:
    //   0: aload_1
    //   1: instanceof android/telephony/data/ApnSetting
    //   4: istore_3
    //   5: iconst_0
    //   6: istore #4
    //   8: iload_3
    //   9: ifne -> 14
    //   12: iconst_0
    //   13: ireturn
    //   14: aload_1
    //   15: checkcast android/telephony/data/ApnSetting
    //   18: astore_1
    //   19: aload_0
    //   20: getfield mEntryName : Ljava/lang/String;
    //   23: aload_1
    //   24: getfield mEntryName : Ljava/lang/String;
    //   27: invokevirtual equals : (Ljava/lang/Object;)Z
    //   30: ifeq -> 626
    //   33: aload_0
    //   34: getfield mOperatorNumeric : Ljava/lang/String;
    //   37: astore #5
    //   39: aload_1
    //   40: getfield mOperatorNumeric : Ljava/lang/String;
    //   43: astore #6
    //   45: aload #5
    //   47: aload #6
    //   49: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   52: ifeq -> 626
    //   55: aload_0
    //   56: getfield mApnName : Ljava/lang/String;
    //   59: astore #6
    //   61: aload_1
    //   62: getfield mApnName : Ljava/lang/String;
    //   65: astore #5
    //   67: aload #6
    //   69: aload #5
    //   71: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   74: ifeq -> 626
    //   77: aload_0
    //   78: getfield mProxyAddress : Ljava/lang/String;
    //   81: astore #5
    //   83: aload_1
    //   84: getfield mProxyAddress : Ljava/lang/String;
    //   87: astore #6
    //   89: aload #5
    //   91: aload #6
    //   93: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   96: ifeq -> 626
    //   99: aload_0
    //   100: getfield mMmsc : Landroid/net/Uri;
    //   103: astore #6
    //   105: aload_1
    //   106: getfield mMmsc : Landroid/net/Uri;
    //   109: astore #5
    //   111: aload #6
    //   113: aload #5
    //   115: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   118: ifeq -> 626
    //   121: aload_0
    //   122: getfield mMmsProxyAddress : Ljava/lang/String;
    //   125: astore #6
    //   127: aload_1
    //   128: getfield mMmsProxyAddress : Ljava/lang/String;
    //   131: astore #5
    //   133: aload #6
    //   135: aload #5
    //   137: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   140: ifeq -> 626
    //   143: aload_0
    //   144: getfield mMmsProxyPort : I
    //   147: istore #7
    //   149: iload #7
    //   151: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   154: aload_1
    //   155: getfield mMmsProxyPort : I
    //   158: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   161: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   164: ifeq -> 626
    //   167: aload_0
    //   168: getfield mProxyPort : I
    //   171: istore #7
    //   173: iload #7
    //   175: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   178: aload_1
    //   179: getfield mProxyPort : I
    //   182: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   185: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   188: ifeq -> 626
    //   191: aload_0
    //   192: getfield mUser : Ljava/lang/String;
    //   195: astore #6
    //   197: aload_1
    //   198: getfield mUser : Ljava/lang/String;
    //   201: astore #5
    //   203: aload #6
    //   205: aload #5
    //   207: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   210: ifeq -> 626
    //   213: aload_0
    //   214: getfield mPassword : Ljava/lang/String;
    //   217: astore #5
    //   219: aload_1
    //   220: getfield mPassword : Ljava/lang/String;
    //   223: astore #6
    //   225: aload #5
    //   227: aload #6
    //   229: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   232: ifeq -> 626
    //   235: aload_0
    //   236: getfield mAuthType : I
    //   239: istore #7
    //   241: iload #7
    //   243: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   246: aload_1
    //   247: getfield mAuthType : I
    //   250: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   253: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   256: ifeq -> 626
    //   259: aload_0
    //   260: getfield mApnTypeBitmask : I
    //   263: istore #7
    //   265: iload #7
    //   267: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   270: aload_1
    //   271: getfield mApnTypeBitmask : I
    //   274: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   277: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   280: ifeq -> 626
    //   283: iload_2
    //   284: ifne -> 311
    //   287: aload_0
    //   288: getfield mProtocol : I
    //   291: istore #7
    //   293: iload #7
    //   295: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   298: aload_1
    //   299: getfield mProtocol : I
    //   302: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   305: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   308: ifeq -> 626
    //   311: iload_2
    //   312: ifeq -> 339
    //   315: aload_0
    //   316: getfield mRoamingProtocol : I
    //   319: istore #7
    //   321: iload #7
    //   323: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   326: aload_1
    //   327: getfield mRoamingProtocol : I
    //   330: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   333: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   336: ifeq -> 626
    //   339: aload_0
    //   340: getfield mCarrierEnabled : Z
    //   343: istore_2
    //   344: iload_2
    //   345: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   348: aload_1
    //   349: getfield mCarrierEnabled : Z
    //   352: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   355: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   358: ifeq -> 626
    //   361: aload_0
    //   362: getfield mProfileId : I
    //   365: istore #7
    //   367: iload #7
    //   369: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   372: aload_1
    //   373: getfield mProfileId : I
    //   376: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   379: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   382: ifeq -> 626
    //   385: aload_0
    //   386: getfield mPersistent : Z
    //   389: istore_2
    //   390: iload_2
    //   391: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   394: aload_1
    //   395: getfield mPersistent : Z
    //   398: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   401: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   404: ifeq -> 626
    //   407: aload_0
    //   408: getfield mMaxConns : I
    //   411: istore #7
    //   413: iload #7
    //   415: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   418: aload_1
    //   419: getfield mMaxConns : I
    //   422: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   425: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   428: ifeq -> 626
    //   431: aload_0
    //   432: getfield mWaitTime : I
    //   435: istore #7
    //   437: iload #7
    //   439: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   442: aload_1
    //   443: getfield mWaitTime : I
    //   446: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   449: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   452: ifeq -> 626
    //   455: aload_0
    //   456: getfield mMaxConnsTime : I
    //   459: istore #7
    //   461: iload #7
    //   463: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   466: aload_1
    //   467: getfield mMaxConnsTime : I
    //   470: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   473: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   476: ifeq -> 626
    //   479: aload_0
    //   480: getfield mMtu : I
    //   483: istore #7
    //   485: iload #7
    //   487: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   490: aload_1
    //   491: getfield mMtu : I
    //   494: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   497: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   500: ifeq -> 626
    //   503: aload_0
    //   504: getfield mMvnoType : I
    //   507: istore #7
    //   509: iload #7
    //   511: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   514: aload_1
    //   515: getfield mMvnoType : I
    //   518: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   521: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   524: ifeq -> 626
    //   527: aload_0
    //   528: getfield mMvnoMatchData : Ljava/lang/String;
    //   531: astore #5
    //   533: aload_1
    //   534: getfield mMvnoMatchData : Ljava/lang/String;
    //   537: astore #6
    //   539: aload #5
    //   541: aload #6
    //   543: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   546: ifeq -> 626
    //   549: aload_0
    //   550: getfield mApnSetId : I
    //   553: istore #7
    //   555: iload #7
    //   557: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   560: aload_1
    //   561: getfield mApnSetId : I
    //   564: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   567: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   570: ifeq -> 626
    //   573: aload_0
    //   574: getfield mCarrierId : I
    //   577: istore #7
    //   579: iload #7
    //   581: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   584: aload_1
    //   585: getfield mCarrierId : I
    //   588: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   591: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   594: ifeq -> 626
    //   597: aload_0
    //   598: getfield mSkip464Xlat : I
    //   601: istore #7
    //   603: iload #7
    //   605: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   608: aload_1
    //   609: getfield mSkip464Xlat : I
    //   612: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   615: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   618: ifeq -> 626
    //   621: iconst_1
    //   622: istore_2
    //   623: goto -> 629
    //   626: iload #4
    //   628: istore_2
    //   629: iload_2
    //   630: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1285	-> 0
    //   #1286	-> 12
    //   #1289	-> 14
    //   #1291	-> 19
    //   #1292	-> 45
    //   #1293	-> 67
    //   #1294	-> 89
    //   #1295	-> 111
    //   #1296	-> 133
    //   #1297	-> 149
    //   #1298	-> 173
    //   #1299	-> 203
    //   #1300	-> 225
    //   #1301	-> 241
    //   #1302	-> 265
    //   #1303	-> 293
    //   #1304	-> 321
    //   #1305	-> 344
    //   #1306	-> 367
    //   #1307	-> 390
    //   #1308	-> 413
    //   #1309	-> 437
    //   #1310	-> 461
    //   #1311	-> 485
    //   #1312	-> 509
    //   #1313	-> 539
    //   #1314	-> 555
    //   #1315	-> 579
    //   #1316	-> 603
    //   #1291	-> 629
  }
  
  public boolean similar(ApnSetting paramApnSetting) {
    if (!canHandleType(8) && !paramApnSetting.canHandleType(8)) {
      String str1 = this.mApnName, str2 = paramApnSetting.mApnName;
      if (Objects.equals(str1, str2) && !typeSameAny(this, paramApnSetting)) {
        str2 = this.mProxyAddress;
        str1 = paramApnSetting.mProxyAddress;
        if (xorEqualsString(str2, str1)) {
          int i = this.mProxyPort, j = paramApnSetting.mProxyPort;
          if (xorEqualsInt(i, j))
            if (OplusApnSetting.oemMergeApnIgnoreProtocolType(this, paramApnSetting)) {
              boolean bool = this.mCarrierEnabled;
              if (Objects.equals(Boolean.valueOf(bool), Boolean.valueOf(paramApnSetting.mCarrierEnabled))) {
                i = this.mProfileId;
                if (Objects.equals(Integer.valueOf(i), Integer.valueOf(paramApnSetting.mProfileId))) {
                  i = this.mMvnoType;
                  if (Objects.equals(Integer.valueOf(i), Integer.valueOf(paramApnSetting.mMvnoType))) {
                    str1 = this.mMvnoMatchData;
                    str2 = paramApnSetting.mMvnoMatchData;
                    if (Objects.equals(str1, str2)) {
                      Uri uri2 = this.mMmsc, uri1 = paramApnSetting.mMmsc;
                      if (xorEquals(uri2, uri1)) {
                        String str3 = this.mMmsProxyAddress, str4 = paramApnSetting.mMmsProxyAddress;
                        if (xorEqualsString(str3, str4)) {
                          i = this.mMmsProxyPort;
                          j = paramApnSetting.mMmsProxyPort;
                          if (xorEqualsInt(i, j)) {
                            i = this.mNetworkTypeBitmask;
                            if (Objects.equals(Integer.valueOf(i), Integer.valueOf(paramApnSetting.mNetworkTypeBitmask))) {
                              i = this.mApnSetId;
                              if (Objects.equals(Integer.valueOf(i), Integer.valueOf(paramApnSetting.mApnSetId))) {
                                i = this.mCarrierId;
                                if (Objects.equals(Integer.valueOf(i), Integer.valueOf(paramApnSetting.mCarrierId))) {
                                  i = this.mSkip464Xlat;
                                  if (Objects.equals(Integer.valueOf(i), Integer.valueOf(paramApnSetting.mSkip464Xlat)))
                                    return true; 
                                } 
                              } 
                            } 
                          } 
                        } 
                      } 
                    } 
                  } 
                } 
              } 
            }  
        } 
      } 
    } 
    return false;
  }
  
  private boolean xorEquals(Object paramObject1, Object paramObject2) {
    return (paramObject1 == null || paramObject2 == null || paramObject1.equals(paramObject2));
  }
  
  private boolean xorEqualsString(String paramString1, String paramString2) {
    return (TextUtils.isEmpty(paramString1) || TextUtils.isEmpty(paramString2) || paramString1.equals(paramString2));
  }
  
  private boolean xorEqualsInt(int paramInt1, int paramInt2) {
    return (paramInt1 == -1 || paramInt2 == -1 || Objects.equals(Integer.valueOf(paramInt1), Integer.valueOf(paramInt2)));
  }
  
  private String nullToEmpty(String paramString) {
    if (paramString == null)
      paramString = ""; 
    return paramString;
  }
  
  public ContentValues toContentValues() {
    ContentValues contentValues = new ContentValues();
    contentValues.put("numeric", nullToEmpty(this.mOperatorNumeric));
    contentValues.put("name", nullToEmpty(this.mEntryName));
    contentValues.put("apn", nullToEmpty(this.mApnName));
    contentValues.put("proxy", nullToEmpty(this.mProxyAddress));
    contentValues.put("port", nullToEmpty(portToString(this.mProxyPort)));
    contentValues.put("mmsc", nullToEmpty(UriToString(this.mMmsc)));
    contentValues.put("mmsport", nullToEmpty(portToString(this.mMmsProxyPort)));
    contentValues.put("mmsproxy", nullToEmpty(this.mMmsProxyAddress));
    contentValues.put("user", nullToEmpty(this.mUser));
    contentValues.put("password", nullToEmpty(this.mPassword));
    contentValues.put("authtype", Integer.valueOf(this.mAuthType));
    String str = getApnTypesStringFromBitmask(this.mApnTypeBitmask);
    contentValues.put("type", nullToEmpty(str));
    int i = this.mProtocol;
    str = getProtocolStringFromInt(i);
    contentValues.put("protocol", str);
    i = this.mRoamingProtocol;
    str = getProtocolStringFromInt(i);
    contentValues.put("roaming_protocol", str);
    contentValues.put("carrier_enabled", Boolean.valueOf(this.mCarrierEnabled));
    contentValues.put("mvno_type", getMvnoTypeStringFromInt(this.mMvnoType));
    contentValues.put("network_type_bitmask", Integer.valueOf(this.mNetworkTypeBitmask));
    contentValues.put("carrier_id", Integer.valueOf(this.mCarrierId));
    contentValues.put("skip_464xlat", Integer.valueOf(this.mSkip464Xlat));
    return contentValues;
  }
  
  public List<Integer> getApnTypes() {
    ArrayList<Integer> arrayList = new ArrayList();
    for (Integer integer : APN_TYPE_INT_MAP.keySet()) {
      if ((this.mApnTypeBitmask & integer.intValue()) == integer.intValue())
        arrayList.add(integer); 
    } 
    return arrayList;
  }
  
  public static String getApnTypesStringFromBitmask(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    for (Integer integer : APN_TYPE_INT_MAP.keySet()) {
      if ((integer.intValue() & paramInt) == integer.intValue())
        arrayList.add(APN_TYPE_INT_MAP.get(integer)); 
    } 
    return TextUtils.join(",", arrayList);
  }
  
  public static String getApnTypeString(int paramInt) {
    if (paramInt == 255)
      return "*"; 
    String str = APN_TYPE_INT_MAP.get(Integer.valueOf(paramInt));
    if (str == null)
      str = "Unknown"; 
    return str;
  }
  
  public static int getApnTypesBitmaskFromString(String paramString) {
    if (TextUtils.isEmpty(paramString))
      return 255; 
    int i = 0;
    String[] arrayOfString;
    int j;
    byte b;
    for (arrayOfString = paramString.split(","), j = arrayOfString.length, b = 0; b < j; ) {
      String str = arrayOfString[b];
      Integer integer = APN_TYPE_STRING_MAP.get(str.toLowerCase());
      int k = i;
      if (integer != null)
        k = i | integer.intValue(); 
      b++;
      i = k;
    } 
    return i;
  }
  
  public static int getMvnoTypeIntFromString(String paramString) {
    int i;
    if (!TextUtils.isEmpty(paramString))
      paramString = paramString.toLowerCase(); 
    Integer integer = MVNO_TYPE_STRING_MAP.get(paramString);
    if (integer == null) {
      i = -1;
    } else {
      i = integer.intValue();
    } 
    return i;
  }
  
  public static String getMvnoTypeStringFromInt(int paramInt) {
    String str = MVNO_TYPE_INT_MAP.get(Integer.valueOf(paramInt));
    if (str == null)
      str = ""; 
    return str;
  }
  
  public static int getProtocolIntFromString(String paramString) {
    int i;
    Integer integer = PROTOCOL_STRING_MAP.get(paramString);
    if (integer == null) {
      i = -1;
    } else {
      i = integer.intValue();
    } 
    return i;
  }
  
  public static String getProtocolStringFromInt(int paramInt) {
    String str = PROTOCOL_INT_MAP.get(Integer.valueOf(paramInt));
    if (str == null)
      str = ""; 
    return str;
  }
  
  private static Uri UriFromString(String paramString) {
    Uri uri;
    if (TextUtils.isEmpty(paramString)) {
      paramString = null;
    } else {
      uri = Uri.parse(paramString);
    } 
    return uri;
  }
  
  private static String UriToString(Uri paramUri) {
    String str;
    if (paramUri == null) {
      paramUri = null;
    } else {
      str = paramUri.toString();
    } 
    return str;
  }
  
  public static InetAddress inetAddressFromString(String paramString) {
    if (TextUtils.isEmpty(paramString))
      return null; 
    try {
      return InetAddress.getByName(paramString);
    } catch (UnknownHostException unknownHostException) {
      Log.e("ApnSetting", "Can't parse InetAddress from string: unknown host.");
      return null;
    } 
  }
  
  public static String inetAddressToString(InetAddress paramInetAddress) {
    if (paramInetAddress == null)
      return null; 
    String str2 = paramInetAddress.toString();
    if (TextUtils.isEmpty(str2))
      return null; 
    String str1 = str2.substring(0, str2.indexOf("/"));
    str2 = str2.substring(str2.indexOf("/") + 1);
    if (TextUtils.isEmpty(str1) && TextUtils.isEmpty(str2))
      return null; 
    if (TextUtils.isEmpty(str1))
      str1 = str2; 
    return str1;
  }
  
  private static int portFromString(String paramString) {
    byte b = -1;
    int i = b;
    if (!TextUtils.isEmpty(paramString))
      try {
        i = Integer.parseInt(paramString);
      } catch (NumberFormatException numberFormatException) {
        Log.e("ApnSetting", "Can't parse port from String");
        i = b;
      }  
    return i;
  }
  
  private static String portToString(int paramInt) {
    String str;
    if (paramInt == -1) {
      str = null;
    } else {
      str = Integer.toString(paramInt);
    } 
    return str;
  }
  
  public boolean canSupportNetworkType(int paramInt) {
    if (paramInt == 16 && (this.mNetworkTypeBitmask & 0x3L) != 0L)
      return true; 
    return ServiceState.bitmaskHasTech(this.mNetworkTypeBitmask, paramInt);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mId);
    paramParcel.writeString(this.mOperatorNumeric);
    paramParcel.writeString(this.mEntryName);
    paramParcel.writeString(this.mApnName);
    paramParcel.writeString(this.mProxyAddress);
    paramParcel.writeInt(this.mProxyPort);
    paramParcel.writeValue(this.mMmsc);
    paramParcel.writeString(this.mMmsProxyAddress);
    paramParcel.writeInt(this.mMmsProxyPort);
    paramParcel.writeString(this.mUser);
    paramParcel.writeString(this.mPassword);
    paramParcel.writeInt(this.mAuthType);
    paramParcel.writeInt(this.mApnTypeBitmask);
    paramParcel.writeInt(this.mProtocol);
    paramParcel.writeInt(this.mRoamingProtocol);
    paramParcel.writeBoolean(this.mCarrierEnabled);
    paramParcel.writeInt(this.mMvnoType);
    paramParcel.writeInt(this.mNetworkTypeBitmask);
    paramParcel.writeInt(this.mApnSetId);
    paramParcel.writeInt(this.mCarrierId);
    paramParcel.writeInt(this.mSkip464Xlat);
  }
  
  private static ApnSetting readFromParcel(Parcel paramParcel) {
    int i = paramParcel.readInt();
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    String str3 = paramParcel.readString();
    String str4 = paramParcel.readString();
    int j = paramParcel.readInt();
    Uri uri = (Uri)paramParcel.readValue(Uri.class.getClassLoader());
    String str5 = paramParcel.readString();
    int k = paramParcel.readInt();
    String str6 = paramParcel.readString();
    String str7 = paramParcel.readString();
    int m = paramParcel.readInt();
    int n = paramParcel.readInt();
    int i1 = paramParcel.readInt();
    int i2 = paramParcel.readInt();
    boolean bool = paramParcel.readBoolean();
    int i3 = paramParcel.readInt();
    int i4 = paramParcel.readInt();
    int i5 = paramParcel.readInt();
    int i6 = paramParcel.readInt();
    int i7 = paramParcel.readInt();
    return makeApnSetting(i, str1, str2, str3, str4, j, uri, str5, k, str6, str7, m, n, i1, i2, bool, i4, 0, false, 0, 0, 0, 0, i3, null, i5, i6, i7);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class AuthType implements Annotation {}
  
  class Builder {
    private int mProxyPort = -1;
    
    private int mMmsProxyPort = -1;
    
    private int mProtocol = -1;
    
    private int mRoamingProtocol = -1;
    
    private int mMvnoType = -1;
    
    private int mCarrierId = -1;
    
    private int mSkip464Xlat = -1;
    
    private String mApnName;
    
    private int mApnSetId;
    
    private int mApnTypeBitmask;
    
    private int mAuthType;
    
    private boolean mCarrierEnabled;
    
    private String mEntryName;
    
    private int mId;
    
    private int mMaxConns;
    
    private int mMaxConnsTime;
    
    private String mMmsProxyAddress;
    
    private Uri mMmsc;
    
    private boolean mModemCognitive;
    
    private int mMtu;
    
    private String mMvnoMatchData;
    
    private int mNetworkTypeBitmask;
    
    private String mOperatorNumeric;
    
    private String mPassword;
    
    private int mProfileId;
    
    private String mProxyAddress;
    
    private String mUser;
    
    private int mWaitTime;
    
    private Builder setId(int param1Int) {
      this.mId = param1Int;
      return this;
    }
    
    public Builder setMtu(int param1Int) {
      this.mMtu = param1Int;
      return this;
    }
    
    public Builder setProfileId(int param1Int) {
      this.mProfileId = param1Int;
      return this;
    }
    
    public Builder setModemCognitive(boolean param1Boolean) {
      this.mModemCognitive = param1Boolean;
      return this;
    }
    
    public Builder setMaxConns(int param1Int) {
      this.mMaxConns = param1Int;
      return this;
    }
    
    public Builder setWaitTime(int param1Int) {
      this.mWaitTime = param1Int;
      return this;
    }
    
    public Builder setMaxConnsTime(int param1Int) {
      this.mMaxConnsTime = param1Int;
      return this;
    }
    
    public Builder setMvnoMatchData(String param1String) {
      this.mMvnoMatchData = param1String;
      return this;
    }
    
    public Builder setApnSetId(int param1Int) {
      this.mApnSetId = param1Int;
      return this;
    }
    
    public Builder setEntryName(String param1String) {
      this.mEntryName = param1String;
      return this;
    }
    
    public Builder setApnName(String param1String) {
      this.mApnName = param1String;
      return this;
    }
    
    @Deprecated
    public Builder setProxyAddress(InetAddress param1InetAddress) {
      this.mProxyAddress = ApnSetting.inetAddressToString(param1InetAddress);
      return this;
    }
    
    public Builder setProxyAddress(String param1String) {
      this.mProxyAddress = param1String;
      return this;
    }
    
    public Builder setProxyPort(int param1Int) {
      this.mProxyPort = param1Int;
      return this;
    }
    
    public Builder setMmsc(Uri param1Uri) {
      this.mMmsc = param1Uri;
      return this;
    }
    
    @Deprecated
    public Builder setMmsProxyAddress(InetAddress param1InetAddress) {
      this.mMmsProxyAddress = ApnSetting.inetAddressToString(param1InetAddress);
      return this;
    }
    
    public Builder setMmsProxyAddress(String param1String) {
      this.mMmsProxyAddress = param1String;
      return this;
    }
    
    public Builder setMmsProxyPort(int param1Int) {
      this.mMmsProxyPort = param1Int;
      return this;
    }
    
    public Builder setUser(String param1String) {
      this.mUser = param1String;
      return this;
    }
    
    public Builder setPassword(String param1String) {
      this.mPassword = param1String;
      return this;
    }
    
    public Builder setAuthType(int param1Int) {
      this.mAuthType = param1Int;
      return this;
    }
    
    public Builder setApnTypeBitmask(int param1Int) {
      this.mApnTypeBitmask = param1Int;
      return this;
    }
    
    public Builder setOperatorNumeric(String param1String) {
      this.mOperatorNumeric = param1String;
      return this;
    }
    
    public Builder setProtocol(int param1Int) {
      this.mProtocol = param1Int;
      return this;
    }
    
    public Builder setRoamingProtocol(int param1Int) {
      this.mRoamingProtocol = param1Int;
      return this;
    }
    
    public Builder setCarrierEnabled(boolean param1Boolean) {
      this.mCarrierEnabled = param1Boolean;
      return this;
    }
    
    public Builder setNetworkTypeBitmask(int param1Int) {
      this.mNetworkTypeBitmask = param1Int;
      return this;
    }
    
    public Builder setMvnoType(int param1Int) {
      this.mMvnoType = param1Int;
      return this;
    }
    
    public Builder setCarrierId(int param1Int) {
      this.mCarrierId = param1Int;
      return this;
    }
    
    public Builder setSkip464Xlat(int param1Int) {
      this.mSkip464Xlat = param1Int;
      return this;
    }
    
    public ApnSetting build() {
      if ((this.mApnTypeBitmask & 0xFFF) != 0) {
        String str = this.mApnName;
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(this.mEntryName))
          return new ApnSetting(this); 
      } 
      return null;
    }
    
    public ApnSetting buildWithoutCheck() {
      return new ApnSetting(this);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class MvnoType implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ProtocolType implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Skip464XlatStatus implements Annotation {}
}
