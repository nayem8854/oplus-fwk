package android.telephony;

import android.os.IBinder;
import android.os.RemoteException;
import android.os.TelephonyServiceManager;
import com.android.internal.telephony.ITelephony;
import com.android.telephony.Rlog;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class NetworkScan {
  public static final int ERROR_INTERRUPTED = 10002;
  
  public static final int ERROR_INVALID_SCAN = 2;
  
  public static final int ERROR_INVALID_SCANID = 10001;
  
  public static final int ERROR_MODEM_ERROR = 1;
  
  public static final int ERROR_MODEM_UNAVAILABLE = 3;
  
  public static final int ERROR_RADIO_INTERFACE_ERROR = 10000;
  
  public static final int ERROR_UNSUPPORTED = 4;
  
  public static final int SUCCESS = 0;
  
  private static final String TAG = "NetworkScan";
  
  private final int mScanId;
  
  private final int mSubId;
  
  public void stopScan() {
    ITelephony iTelephony = getITelephony();
    if (iTelephony == null)
      Rlog.e("NetworkScan", "Failed to get the ITelephony instance."); 
    try {
      iTelephony.stopNetworkScan(this.mSubId, this.mScanId);
    } catch (IllegalArgumentException illegalArgumentException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("stopNetworkScan - no active scan for ScanID=");
      stringBuilder.append(this.mScanId);
      Rlog.d("NetworkScan", stringBuilder.toString());
    } catch (RemoteException remoteException) {
      Rlog.e("NetworkScan", "stopNetworkScan  RemoteException", (Throwable)remoteException);
    } catch (RuntimeException runtimeException) {
      Rlog.e("NetworkScan", "stopNetworkScan  RuntimeException", runtimeException);
    } 
  }
  
  @Deprecated
  public void stop() throws RemoteException {
    try {
      stopScan();
      return;
    } catch (RuntimeException runtimeException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to stop the network scan with id ");
      stringBuilder.append(this.mScanId);
      throw new RemoteException(stringBuilder.toString());
    } 
  }
  
  public NetworkScan(int paramInt1, int paramInt2) {
    this.mScanId = paramInt1;
    this.mSubId = paramInt2;
  }
  
  private ITelephony getITelephony() {
    TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
    TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getTelephonyServiceRegisterer();
    IBinder iBinder = serviceRegisterer.get();
    return ITelephony.Stub.asInterface(iBinder);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ScanErrorCode {}
}
