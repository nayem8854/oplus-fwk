package android.telephony.emergency;

import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.PhoneNumberUtils;
import com.android.telephony.Rlog;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public final class EmergencyNumber implements Parcelable, Comparable<EmergencyNumber> {
  public static final Parcelable.Creator<EmergencyNumber> CREATOR;
  
  public static final int EMERGENCY_CALL_ROUTING_EMERGENCY = 1;
  
  public static final int EMERGENCY_CALL_ROUTING_NORMAL = 2;
  
  public static final int EMERGENCY_CALL_ROUTING_UNKNOWN = 0;
  
  public static final int EMERGENCY_NUMBER_SOURCE_DATABASE = 16;
  
  public static final int EMERGENCY_NUMBER_SOURCE_DEFAULT = 8;
  
  public static final int EMERGENCY_NUMBER_SOURCE_MODEM_CONFIG = 4;
  
  public static final int EMERGENCY_NUMBER_SOURCE_NETWORK_SIGNALING = 1;
  
  private static final Set<Integer> EMERGENCY_NUMBER_SOURCE_SET;
  
  public static final int EMERGENCY_NUMBER_SOURCE_SIM = 2;
  
  public static final int EMERGENCY_NUMBER_SOURCE_TEST = 32;
  
  public static final int EMERGENCY_SERVICE_CATEGORY_AIEC = 64;
  
  public static final int EMERGENCY_SERVICE_CATEGORY_AMBULANCE = 2;
  
  public static final int EMERGENCY_SERVICE_CATEGORY_FIRE_BRIGADE = 4;
  
  public static final int EMERGENCY_SERVICE_CATEGORY_MARINE_GUARD = 8;
  
  public static final int EMERGENCY_SERVICE_CATEGORY_MIEC = 32;
  
  public static final int EMERGENCY_SERVICE_CATEGORY_MOUNTAIN_RESCUE = 16;
  
  public static final int EMERGENCY_SERVICE_CATEGORY_POLICE = 1;
  
  private static final Set<Integer> EMERGENCY_SERVICE_CATEGORY_SET;
  
  public static final int EMERGENCY_SERVICE_CATEGORY_UNSPECIFIED = 0;
  
  private static final String LOG_TAG = "EmergencyNumber";
  
  private final String mCountryIso;
  
  private final int mEmergencyCallRouting;
  
  private final int mEmergencyNumberSourceBitmask;
  
  private final int mEmergencyServiceCategoryBitmask;
  
  private final List<String> mEmergencyUrns;
  
  private final String mMnc;
  
  private final String mNumber;
  
  static {
    HashSet<Integer> hashSet = new HashSet();
    Integer integer2 = Integer.valueOf(1);
    hashSet.add(integer2);
    Set<Integer> set1 = EMERGENCY_SERVICE_CATEGORY_SET;
    Integer integer1 = Integer.valueOf(2);
    set1.add(integer1);
    Set<Integer> set2 = EMERGENCY_SERVICE_CATEGORY_SET;
    Integer integer3 = Integer.valueOf(4);
    set2.add(integer3);
    Set<Integer> set3 = EMERGENCY_SERVICE_CATEGORY_SET;
    Integer integer4 = Integer.valueOf(8);
    set3.add(integer4);
    Set<Integer> set4 = EMERGENCY_SERVICE_CATEGORY_SET;
    Integer integer5 = Integer.valueOf(16);
    set4.add(integer5);
    EMERGENCY_SERVICE_CATEGORY_SET.add(Integer.valueOf(32));
    EMERGENCY_SERVICE_CATEGORY_SET.add(Integer.valueOf(64));
    EMERGENCY_NUMBER_SOURCE_SET = set4 = new HashSet<>();
    set4.add(integer2);
    EMERGENCY_NUMBER_SOURCE_SET.add(integer1);
    EMERGENCY_NUMBER_SOURCE_SET.add(integer5);
    EMERGENCY_NUMBER_SOURCE_SET.add(integer3);
    EMERGENCY_NUMBER_SOURCE_SET.add(integer4);
    CREATOR = new Parcelable.Creator<EmergencyNumber>() {
        public EmergencyNumber createFromParcel(Parcel param1Parcel) {
          return new EmergencyNumber(param1Parcel);
        }
        
        public EmergencyNumber[] newArray(int param1Int) {
          return new EmergencyNumber[param1Int];
        }
      };
  }
  
  public EmergencyNumber(String paramString1, String paramString2, String paramString3, int paramInt1, List<String> paramList, int paramInt2, int paramInt3) {
    this.mNumber = paramString1;
    this.mCountryIso = paramString2;
    this.mMnc = paramString3;
    this.mEmergencyServiceCategoryBitmask = paramInt1;
    this.mEmergencyUrns = paramList;
    this.mEmergencyNumberSourceBitmask = paramInt2;
    this.mEmergencyCallRouting = paramInt3;
  }
  
  public EmergencyNumber(Parcel paramParcel) {
    this.mNumber = paramParcel.readString();
    this.mCountryIso = paramParcel.readString();
    this.mMnc = paramParcel.readString();
    this.mEmergencyServiceCategoryBitmask = paramParcel.readInt();
    this.mEmergencyUrns = paramParcel.createStringArrayList();
    this.mEmergencyNumberSourceBitmask = paramParcel.readInt();
    this.mEmergencyCallRouting = paramParcel.readInt();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mNumber);
    paramParcel.writeString(this.mCountryIso);
    paramParcel.writeString(this.mMnc);
    paramParcel.writeInt(this.mEmergencyServiceCategoryBitmask);
    paramParcel.writeStringList(this.mEmergencyUrns);
    paramParcel.writeInt(this.mEmergencyNumberSourceBitmask);
    paramParcel.writeInt(this.mEmergencyCallRouting);
  }
  
  public String getNumber() {
    return this.mNumber;
  }
  
  public String getCountryIso() {
    return this.mCountryIso;
  }
  
  public String getMnc() {
    return this.mMnc;
  }
  
  public int getEmergencyServiceCategoryBitmask() {
    return this.mEmergencyServiceCategoryBitmask;
  }
  
  public int getEmergencyServiceCategoryBitmaskInternalDial() {
    if (this.mEmergencyNumberSourceBitmask == 16)
      return 0; 
    return this.mEmergencyServiceCategoryBitmask;
  }
  
  public List<Integer> getEmergencyServiceCategories() {
    ArrayList<Integer> arrayList = new ArrayList();
    if (serviceUnspecified()) {
      arrayList.add(Integer.valueOf(0));
      return arrayList;
    } 
    for (Integer integer : EMERGENCY_SERVICE_CATEGORY_SET) {
      if (isInEmergencyServiceCategories(integer.intValue()))
        arrayList.add(integer); 
    } 
    return arrayList;
  }
  
  public List<String> getEmergencyUrns() {
    return Collections.unmodifiableList(this.mEmergencyUrns);
  }
  
  private boolean serviceUnspecified() {
    boolean bool;
    if (this.mEmergencyServiceCategoryBitmask == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isInEmergencyServiceCategories(int paramInt) {
    if (paramInt == 0)
      return serviceUnspecified(); 
    boolean bool = serviceUnspecified();
    boolean bool1 = true;
    if (bool)
      return true; 
    if ((this.mEmergencyServiceCategoryBitmask & paramInt) != paramInt)
      bool1 = false; 
    return bool1;
  }
  
  public int getEmergencyNumberSourceBitmask() {
    return this.mEmergencyNumberSourceBitmask;
  }
  
  public List<Integer> getEmergencyNumberSources() {
    ArrayList<Integer> arrayList = new ArrayList();
    for (Integer integer : EMERGENCY_NUMBER_SOURCE_SET) {
      if ((this.mEmergencyNumberSourceBitmask & integer.intValue()) == integer.intValue())
        arrayList.add(integer); 
    } 
    return arrayList;
  }
  
  public boolean isFromSources(int paramInt) {
    boolean bool;
    if ((this.mEmergencyNumberSourceBitmask & paramInt) == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getEmergencyCallRouting() {
    return this.mEmergencyCallRouting;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("EmergencyNumber:Number-");
    stringBuilder.append(this.mNumber);
    stringBuilder.append("|CountryIso-");
    stringBuilder.append(this.mCountryIso);
    stringBuilder.append("|Mnc-");
    stringBuilder.append(this.mMnc);
    stringBuilder.append("|ServiceCategories-");
    int i = this.mEmergencyServiceCategoryBitmask;
    stringBuilder.append(Integer.toBinaryString(i));
    stringBuilder.append("|Urns-");
    stringBuilder.append(this.mEmergencyUrns);
    stringBuilder.append("|Sources-");
    i = this.mEmergencyNumberSourceBitmask;
    stringBuilder.append(Integer.toBinaryString(i));
    stringBuilder.append("|Routing-");
    i = this.mEmergencyCallRouting;
    stringBuilder.append(Integer.toBinaryString(i));
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = EmergencyNumber.class.isInstance(paramObject);
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    if (this.mNumber.equals(((EmergencyNumber)paramObject).mNumber)) {
      String str1 = this.mCountryIso, str2 = ((EmergencyNumber)paramObject).mCountryIso;
      if (str1.equals(str2)) {
        str2 = this.mMnc;
        str1 = ((EmergencyNumber)paramObject).mMnc;
        if (str2.equals(str1) && this.mEmergencyServiceCategoryBitmask == ((EmergencyNumber)paramObject).mEmergencyServiceCategoryBitmask) {
          List<String> list2 = this.mEmergencyUrns, list1 = ((EmergencyNumber)paramObject).mEmergencyUrns;
          if (list2.equals(list1) && this.mEmergencyNumberSourceBitmask == ((EmergencyNumber)paramObject).mEmergencyNumberSourceBitmask && this.mEmergencyCallRouting == ((EmergencyNumber)paramObject).mEmergencyCallRouting)
            bool1 = true; 
        } 
      } 
    } 
    return bool1;
  }
  
  public int hashCode() {
    String str1 = this.mNumber, str2 = this.mCountryIso, str3 = this.mMnc;
    int i = this.mEmergencyServiceCategoryBitmask;
    List<String> list = this.mEmergencyUrns;
    int j = this.mEmergencyNumberSourceBitmask;
    int k = this.mEmergencyCallRouting;
    return Objects.hash(new Object[] { str1, str2, str3, Integer.valueOf(i), list, Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  private int getDisplayPriorityScore() {
    int i = 0;
    if (isFromSources(1))
      i = 0 + 16; 
    int j = i;
    if (isFromSources(2))
      j = i + 8; 
    i = j;
    if (isFromSources(16))
      i = j + 4; 
    j = i;
    if (isFromSources(8))
      j = i + 2; 
    i = j;
    if (isFromSources(4))
      i = j + 1; 
    return i;
  }
  
  public int compareTo(EmergencyNumber paramEmergencyNumber) {
    String str1;
    int i = getDisplayPriorityScore();
    int j = paramEmergencyNumber.getDisplayPriorityScore();
    byte b = -1;
    if (i > j)
      return -1; 
    j = getDisplayPriorityScore();
    if (j < paramEmergencyNumber.getDisplayPriorityScore())
      return 1; 
    if (getNumber().compareTo(paramEmergencyNumber.getNumber()) != 0)
      return getNumber().compareTo(paramEmergencyNumber.getNumber()); 
    if (getCountryIso().compareTo(paramEmergencyNumber.getCountryIso()) != 0)
      return getCountryIso().compareTo(paramEmergencyNumber.getCountryIso()); 
    if (getMnc().compareTo(paramEmergencyNumber.getMnc()) != 0)
      return getMnc().compareTo(paramEmergencyNumber.getMnc()); 
    j = getEmergencyServiceCategoryBitmask();
    if (j != paramEmergencyNumber.getEmergencyServiceCategoryBitmask()) {
      j = getEmergencyServiceCategoryBitmask();
      if (j <= paramEmergencyNumber.getEmergencyServiceCategoryBitmask())
        b = 1; 
      return b;
    } 
    String str2 = getEmergencyUrns().toString();
    String str3 = paramEmergencyNumber.getEmergencyUrns().toString();
    if (str2.compareTo(str3) != 0) {
      str3 = getEmergencyUrns().toString();
      str1 = paramEmergencyNumber.getEmergencyUrns().toString();
      return str3.compareTo(str1);
    } 
    j = getEmergencyCallRouting();
    if (j != str1.getEmergencyCallRouting()) {
      j = getEmergencyCallRouting();
      if (j <= str1.getEmergencyCallRouting())
        b = 1; 
      return b;
    } 
    return 0;
  }
  
  public static void mergeSameNumbersInEmergencyNumberList(List<EmergencyNumber> paramList) {
    if (paramList == null)
      return; 
    HashSet<Integer> hashSet = new HashSet();
    int i;
    for (i = 0; i < paramList.size(); i++) {
      for (byte b = 0; b < i; b++) {
        EmergencyNumber emergencyNumber1 = paramList.get(i), emergencyNumber2 = paramList.get(b);
        if (areSameEmergencyNumbers(emergencyNumber1, emergencyNumber2)) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Found unexpected duplicate numbers: ");
          stringBuilder.append(paramList.get(i));
          stringBuilder.append(" vs ");
          stringBuilder.append(paramList.get(b));
          String str = stringBuilder.toString();
          Rlog.e("EmergencyNumber", str);
          EmergencyNumber emergencyNumber = paramList.get(i);
          emergencyNumber1 = paramList.get(b);
          paramList.set(i, mergeSameEmergencyNumbers(emergencyNumber, emergencyNumber1));
          hashSet.add(Integer.valueOf(b));
        } 
      } 
    } 
    for (i = paramList.size() - 1; i >= 0; i--) {
      if (hashSet.contains(Integer.valueOf(i)))
        paramList.remove(i); 
    } 
    Collections.sort(paramList);
  }
  
  public static boolean areSameEmergencyNumbers(EmergencyNumber paramEmergencyNumber1, EmergencyNumber paramEmergencyNumber2) {
    if (!paramEmergencyNumber1.getNumber().equals(paramEmergencyNumber2.getNumber()))
      return false; 
    if (!paramEmergencyNumber1.getCountryIso().equals(paramEmergencyNumber2.getCountryIso()))
      return false; 
    if (!paramEmergencyNumber1.getMnc().equals(paramEmergencyNumber2.getMnc()))
      return false; 
    int i = paramEmergencyNumber1.getEmergencyServiceCategoryBitmask();
    if (i != paramEmergencyNumber2.getEmergencyServiceCategoryBitmask())
      return false; 
    if (!paramEmergencyNumber1.getEmergencyUrns().equals(paramEmergencyNumber2.getEmergencyUrns()))
      return false; 
    if (paramEmergencyNumber1.getEmergencyCallRouting() != paramEmergencyNumber2.getEmergencyCallRouting())
      return false; 
    boolean bool = paramEmergencyNumber1.isFromSources(32);
    if (paramEmergencyNumber2.isFromSources(32) ^ bool)
      return false; 
    return true;
  }
  
  public static EmergencyNumber mergeSameEmergencyNumbers(EmergencyNumber paramEmergencyNumber1, EmergencyNumber paramEmergencyNumber2) {
    if (areSameEmergencyNumbers(paramEmergencyNumber1, paramEmergencyNumber2)) {
      String str1 = paramEmergencyNumber1.getNumber(), str2 = paramEmergencyNumber1.getCountryIso(), str3 = paramEmergencyNumber1.getMnc();
      int i = paramEmergencyNumber1.getEmergencyServiceCategoryBitmask();
      List<String> list = paramEmergencyNumber1.getEmergencyUrns();
      int j = paramEmergencyNumber1.getEmergencyNumberSourceBitmask();
      int k = paramEmergencyNumber2.getEmergencyNumberSourceBitmask();
      return new EmergencyNumber(str1, str2, str3, i, list, k | j, paramEmergencyNumber1.getEmergencyCallRouting());
    } 
    return null;
  }
  
  public static boolean validateEmergencyNumberAddress(String paramString) {
    if (paramString == null)
      return false; 
    for (char c : paramString.toCharArray()) {
      if (!PhoneNumberUtils.isDialable(c))
        return false; 
    } 
    return true;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class EmergencyCallRouting implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class EmergencyNumberSources implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class EmergencyServiceCategories implements Annotation {}
}
