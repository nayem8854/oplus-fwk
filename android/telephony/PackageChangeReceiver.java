package android.telephony;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.UserHandle;

public abstract class PackageChangeReceiver extends BroadcastReceiver {
  private static HandlerThread sHandlerThread;
  
  static final IntentFilter sPackageIntentFilter;
  
  Context mRegisteredContext;
  
  static {
    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
    sPackageIntentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
    sPackageIntentFilter.addAction("android.intent.action.PACKAGE_CHANGED");
    sPackageIntentFilter.addAction("android.intent.action.QUERY_PACKAGE_RESTART");
    sPackageIntentFilter.addAction("android.intent.action.PACKAGE_RESTARTED");
    sPackageIntentFilter.addDataScheme("package");
  }
  
  public void register(Context paramContext, Looper paramLooper, UserHandle paramUserHandle) {
    if (this.mRegisteredContext == null) {
      if (paramLooper == null)
        paramLooper = getStaticLooper(); 
      Handler handler = new Handler(paramLooper);
      if (paramUserHandle != null)
        paramContext = paramContext.createContextAsUser(paramUserHandle, 0); 
      this.mRegisteredContext = paramContext;
      paramContext.registerReceiver(this, sPackageIntentFilter, null, handler);
      return;
    } 
    throw new IllegalStateException("Already registered");
  }
  
  public void unregister() {
    Context context = this.mRegisteredContext;
    if (context != null) {
      context.unregisterReceiver(this);
      this.mRegisteredContext = null;
      return;
    } 
    throw new IllegalStateException("Not registered");
  }
  
  private static Looper getStaticLooper() {
    // Byte code:
    //   0: ldc android/telephony/PackageChangeReceiver
    //   2: monitorenter
    //   3: getstatic android/telephony/PackageChangeReceiver.sHandlerThread : Landroid/os/HandlerThread;
    //   6: ifnonnull -> 30
    //   9: new android/os/HandlerThread
    //   12: astore_0
    //   13: aload_0
    //   14: ldc android/telephony/PackageChangeReceiver
    //   16: invokevirtual getSimpleName : ()Ljava/lang/String;
    //   19: invokespecial <init> : (Ljava/lang/String;)V
    //   22: aload_0
    //   23: putstatic android/telephony/PackageChangeReceiver.sHandlerThread : Landroid/os/HandlerThread;
    //   26: aload_0
    //   27: invokevirtual start : ()V
    //   30: getstatic android/telephony/PackageChangeReceiver.sHandlerThread : Landroid/os/HandlerThread;
    //   33: invokevirtual getLooper : ()Landroid/os/Looper;
    //   36: astore_0
    //   37: ldc android/telephony/PackageChangeReceiver
    //   39: monitorexit
    //   40: aload_0
    //   41: areturn
    //   42: astore_0
    //   43: ldc android/telephony/PackageChangeReceiver
    //   45: monitorexit
    //   46: aload_0
    //   47: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #78	-> 3
    //   #79	-> 9
    //   #80	-> 26
    //   #82	-> 30
    //   #77	-> 42
    // Exception table:
    //   from	to	target	type
    //   3	9	42	finally
    //   9	26	42	finally
    //   26	30	42	finally
    //   30	37	42	finally
  }
  
  public void onPackageAdded(String paramString) {}
  
  public void onPackageRemoved(String paramString) {}
  
  public void onPackageUpdateFinished(String paramString) {}
  
  public void onPackageModified(String paramString) {}
  
  public void onHandleForceStop(String[] paramArrayOfString, boolean paramBoolean) {}
  
  public void onPackageDisappeared() {}
  
  public void onPackageAppeared() {}
  
  public void onReceive(Context paramContext, Intent paramIntent) {
    String str = paramIntent.getAction();
    if ("android.intent.action.PACKAGE_ADDED".equals(str)) {
      str = getPackageName(paramIntent);
      if (str != null) {
        if (paramIntent.getBooleanExtra("android.intent.extra.REPLACING", false)) {
          onPackageUpdateFinished(str);
          onPackageModified(str);
        } else {
          onPackageAdded(str);
        } 
        onPackageAppeared();
      } 
    } else if ("android.intent.action.PACKAGE_REMOVED".equals(str)) {
      str = getPackageName(paramIntent);
      if (str != null) {
        if (!paramIntent.getBooleanExtra("android.intent.extra.REPLACING", false))
          onPackageRemoved(str); 
        onPackageDisappeared();
      } 
    } else if ("android.intent.action.PACKAGE_CHANGED".equals(str)) {
      str = getPackageName(paramIntent);
      if (str != null)
        onPackageModified(str); 
    } else {
      String[] arrayOfString;
      if ("android.intent.action.QUERY_PACKAGE_RESTART".equals(str)) {
        arrayOfString = paramIntent.getStringArrayExtra("android.intent.extra.PACKAGES");
        onHandleForceStop(arrayOfString, false);
      } else if ("android.intent.action.PACKAGE_RESTARTED".equals(arrayOfString)) {
        String str1 = getPackageName(paramIntent);
        onHandleForceStop(new String[] { str1 }, true);
      } 
    } 
  }
  
  String getPackageName(Intent paramIntent) {
    Uri uri = paramIntent.getData();
    if (uri != null) {
      String str = uri.getSchemeSpecificPart();
    } else {
      uri = null;
    } 
    return (String)uri;
  }
}
