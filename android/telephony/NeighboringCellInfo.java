package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;

@Deprecated
public class NeighboringCellInfo implements Parcelable {
  @Deprecated
  public NeighboringCellInfo() {
    this.mRssi = 99;
    this.mLac = -1;
    this.mCid = -1;
    this.mPsc = -1;
    this.mNetworkType = 0;
  }
  
  @Deprecated
  public NeighboringCellInfo(int paramInt1, int paramInt2) {
    this.mRssi = paramInt1;
    this.mCid = paramInt2;
  }
  
  public NeighboringCellInfo(CellInfoGsm paramCellInfoGsm) {
    this.mNetworkType = 1;
    int i = paramCellInfoGsm.getCellSignalStrength().getAsuLevel();
    if (i == Integer.MAX_VALUE)
      this.mRssi = 99; 
    this.mLac = i = paramCellInfoGsm.getCellIdentity().getLac();
    if (i == Integer.MAX_VALUE)
      this.mLac = -1; 
    this.mCid = i = paramCellInfoGsm.getCellIdentity().getCid();
    if (i == Integer.MAX_VALUE)
      this.mCid = -1; 
    this.mPsc = -1;
  }
  
  public NeighboringCellInfo(CellInfoWcdma paramCellInfoWcdma) {
    this.mNetworkType = 3;
    int i = paramCellInfoWcdma.getCellSignalStrength().getAsuLevel();
    if (i == Integer.MAX_VALUE)
      this.mRssi = 99; 
    this.mLac = i = paramCellInfoWcdma.getCellIdentity().getLac();
    if (i == Integer.MAX_VALUE)
      this.mLac = -1; 
    this.mCid = i = paramCellInfoWcdma.getCellIdentity().getCid();
    if (i == Integer.MAX_VALUE)
      this.mCid = -1; 
    this.mPsc = i = paramCellInfoWcdma.getCellIdentity().getPsc();
    if (i == Integer.MAX_VALUE)
      this.mPsc = -1; 
  }
  
  public NeighboringCellInfo(int paramInt1, String paramString, int paramInt2) {
    StringBuilder stringBuilder;
    this.mRssi = paramInt1;
    this.mNetworkType = 0;
    this.mPsc = -1;
    this.mLac = -1;
    this.mCid = -1;
    int i = paramString.length();
    if (i > 8)
      return; 
    String str = paramString;
    if (i < 8) {
      paramInt1 = 0;
      while (true) {
        str = paramString;
        if (paramInt1 < 8 - i) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("0");
          stringBuilder.append(paramString);
          paramString = stringBuilder.toString();
          paramInt1++;
          continue;
        } 
        break;
      } 
    } 
    if (paramInt2 != 1 && paramInt2 != 2) {
      if (paramInt2 != 3)
        switch (paramInt2) {
          default:
            return;
          case 8:
          case 9:
          case 10:
            break;
        }  
      try {
        this.mNetworkType = paramInt2;
        this.mPsc = Integer.parseInt((String)stringBuilder, 16);
      } catch (NumberFormatException numberFormatException) {
        this.mPsc = -1;
        this.mLac = -1;
        this.mCid = -1;
        this.mNetworkType = 0;
      } 
    } 
    this.mNetworkType = paramInt2;
    if (!stringBuilder.equalsIgnoreCase("FFFFFFFF")) {
      this.mCid = Integer.parseInt(stringBuilder.substring(4), 16);
      this.mLac = Integer.parseInt(stringBuilder.substring(0, 4), 16);
    } 
  }
  
  public NeighboringCellInfo(Parcel paramParcel) {
    this.mRssi = paramParcel.readInt();
    this.mLac = paramParcel.readInt();
    this.mCid = paramParcel.readInt();
    this.mPsc = paramParcel.readInt();
    this.mNetworkType = paramParcel.readInt();
  }
  
  public int getRssi() {
    return this.mRssi;
  }
  
  public int getLac() {
    return this.mLac;
  }
  
  public int getCid() {
    return this.mCid;
  }
  
  public int getPsc() {
    return this.mPsc;
  }
  
  public int getNetworkType() {
    return this.mNetworkType;
  }
  
  @Deprecated
  public void setCid(int paramInt) {
    this.mCid = paramInt;
  }
  
  @Deprecated
  public void setRssi(int paramInt) {
    this.mRssi = paramInt;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[");
    int i = this.mPsc;
    String str = "-";
    if (i != -1) {
      Integer integer;
      stringBuilder.append(Integer.toHexString(i));
      stringBuilder.append("@");
      i = this.mRssi;
      if (i != 99)
        integer = Integer.valueOf(i); 
      stringBuilder.append(integer);
    } else {
      i = this.mLac;
      if (i != -1 && this.mCid != -1) {
        Integer integer;
        stringBuilder.append(Integer.toHexString(i));
        i = this.mCid;
        stringBuilder.append(Integer.toHexString(i));
        stringBuilder.append("@");
        i = this.mRssi;
        if (i != 99)
          integer = Integer.valueOf(i); 
        stringBuilder.append(integer);
      } 
    } 
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mRssi);
    paramParcel.writeInt(this.mLac);
    paramParcel.writeInt(this.mCid);
    paramParcel.writeInt(this.mPsc);
    paramParcel.writeInt(this.mNetworkType);
  }
  
  public static final Parcelable.Creator<NeighboringCellInfo> CREATOR = new Parcelable.Creator<NeighboringCellInfo>() {
      public NeighboringCellInfo createFromParcel(Parcel param1Parcel) {
        return new NeighboringCellInfo(param1Parcel);
      }
      
      public NeighboringCellInfo[] newArray(int param1Int) {
        return new NeighboringCellInfo[param1Int];
      }
    };
  
  public static final int UNKNOWN_CID = -1;
  
  public static final int UNKNOWN_RSSI = 99;
  
  private int mCid;
  
  private int mLac;
  
  private int mNetworkType;
  
  private int mPsc;
  
  private int mRssi;
}
