package android.telephony;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;

@SystemApi
public final class ImsiEncryptionInfo implements Parcelable {
  public ImsiEncryptionInfo(String paramString1, String paramString2, int paramInt, String paramString3, byte[] paramArrayOfbyte, Date paramDate) {
    this(paramString1, paramString2, paramInt, paramString3, makeKeyObject(paramArrayOfbyte), paramDate);
  }
  
  public ImsiEncryptionInfo(String paramString1, String paramString2, int paramInt, String paramString3, PublicKey paramPublicKey, Date paramDate) {
    this.mcc = paramString1;
    this.mnc = paramString2;
    this.keyType = paramInt;
    this.publicKey = paramPublicKey;
    this.keyIdentifier = paramString3;
    this.expirationTime = paramDate;
  }
  
  public ImsiEncryptionInfo(Parcel paramParcel) {
    int i = paramParcel.readInt();
    byte[] arrayOfByte = new byte[i];
    paramParcel.readByteArray(arrayOfByte);
    this.publicKey = makeKeyObject(arrayOfByte);
    this.mcc = paramParcel.readString();
    this.mnc = paramParcel.readString();
    this.keyIdentifier = paramParcel.readString();
    this.keyType = paramParcel.readInt();
    this.expirationTime = new Date(paramParcel.readLong());
  }
  
  public String getMnc() {
    return this.mnc;
  }
  
  public String getMcc() {
    return this.mcc;
  }
  
  public String getKeyIdentifier() {
    return this.keyIdentifier;
  }
  
  public int getKeyType() {
    return this.keyType;
  }
  
  public PublicKey getPublicKey() {
    return this.publicKey;
  }
  
  public Date getExpirationTime() {
    return this.expirationTime;
  }
  
  private static PublicKey makeKeyObject(byte[] paramArrayOfbyte) {
    try {
      X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec();
      this(paramArrayOfbyte);
      return KeyFactory.getInstance("RSA").generatePublic(x509EncodedKeySpec);
    } catch (InvalidKeySpecException|java.security.NoSuchAlgorithmException invalidKeySpecException) {
      Log.e("ImsiEncryptionInfo", "Error makeKeyObject: unable to convert into PublicKey", invalidKeySpecException);
      throw new IllegalArgumentException();
    } 
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<ImsiEncryptionInfo> CREATOR = new Parcelable.Creator<ImsiEncryptionInfo>() {
      public ImsiEncryptionInfo createFromParcel(Parcel param1Parcel) {
        return new ImsiEncryptionInfo(param1Parcel);
      }
      
      public ImsiEncryptionInfo[] newArray(int param1Int) {
        return new ImsiEncryptionInfo[param1Int];
      }
    };
  
  private static final String LOG_TAG = "ImsiEncryptionInfo";
  
  private final Date expirationTime;
  
  private final String keyIdentifier;
  
  private final int keyType;
  
  private final String mcc;
  
  private final String mnc;
  
  private final PublicKey publicKey;
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    byte[] arrayOfByte = this.publicKey.getEncoded();
    paramParcel.writeInt(arrayOfByte.length);
    paramParcel.writeByteArray(arrayOfByte);
    paramParcel.writeString(this.mcc);
    paramParcel.writeString(this.mnc);
    paramParcel.writeString(this.keyIdentifier);
    paramParcel.writeInt(this.keyType);
    paramParcel.writeLong(this.expirationTime.getTime());
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[ImsiEncryptionInfo mcc=");
    stringBuilder.append(this.mcc);
    stringBuilder.append("mnc=");
    stringBuilder.append(this.mnc);
    stringBuilder.append("publicKey=");
    stringBuilder.append(this.publicKey);
    stringBuilder.append(", keyIdentifier=");
    stringBuilder.append(this.keyIdentifier);
    stringBuilder.append(", keyType=");
    stringBuilder.append(this.keyType);
    stringBuilder.append(", expirationTime=");
    stringBuilder.append(this.expirationTime);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
}
