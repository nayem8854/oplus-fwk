package android.telephony;

public abstract class OplusSignalStrength {
  public int mOEMLevel_0 = 0;
  
  public int mOEMLevel_1 = 0;
  
  public boolean mIsFake = false;
  
  public int[] getOplusOSLevel() {
    return new int[] { this.mOEMLevel_0, this.mOEMLevel_1 };
  }
  
  public int getOEMLevel_0() {
    return this.mOEMLevel_0;
  }
  
  public int getOEMLevel_1() {
    return this.mOEMLevel_1;
  }
  
  public void setOEMLevel(int paramInt1, int paramInt2) {
    if (-1 != paramInt1)
      this.mOEMLevel_0 = paramInt1; 
    if (-1 != paramInt2)
      this.mOEMLevel_1 = paramInt2; 
  }
  
  public void setFake(boolean paramBoolean) {
    this.mIsFake = paramBoolean;
  }
  
  public boolean isFake() {
    return this.mIsFake;
  }
}
