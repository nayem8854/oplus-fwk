package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Range;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class ModemActivityInfo implements Parcelable {
  public static final Parcelable.Creator<ModemActivityInfo> CREATOR;
  
  public static final int TX_POWER_LEVELS = 5;
  
  public static final int TX_POWER_LEVEL_0 = 0;
  
  public static final int TX_POWER_LEVEL_1 = 1;
  
  public static final int TX_POWER_LEVEL_2 = 2;
  
  public static final int TX_POWER_LEVEL_3 = 3;
  
  public static final int TX_POWER_LEVEL_4 = 4;
  
  private static final Range<Integer>[] TX_POWER_RANGES;
  
  private int mIdleTimeMs;
  
  private int mRxTimeMs;
  
  private int mSleepTimeMs;
  
  private long mTimestamp;
  
  static {
    Integer integer1 = Integer.valueOf(5);
    Integer integer2 = Integer.valueOf(0);
    Range<Integer> range3 = new Range<>(Integer.valueOf(-2147483648), integer2);
    Range<Integer> range2 = new Range<>(integer2, integer1);
    Integer integer3 = Integer.valueOf(15);
    Range<Integer> range1 = new Range<>(integer1, integer3);
    Integer integer4 = Integer.valueOf(20);
    Range<Integer> range4 = new Range<>(integer3, integer4);
    TX_POWER_RANGES = (Range<Integer>[])new Range[] { range3, range2, range1, range4, new Range<>(integer4, Integer.valueOf(2147483647)) };
    CREATOR = new Parcelable.Creator<ModemActivityInfo>() {
        public ModemActivityInfo createFromParcel(Parcel param1Parcel) {
          long l = param1Parcel.readLong();
          int i = param1Parcel.readInt();
          int j = param1Parcel.readInt();
          int[] arrayOfInt = new int[5];
          int k;
          for (k = 0; k < 5; k++)
            arrayOfInt[k] = param1Parcel.readInt(); 
          k = param1Parcel.readInt();
          return new ModemActivityInfo(l, i, j, arrayOfInt, k);
        }
        
        public ModemActivityInfo[] newArray(int param1Int) {
          return new ModemActivityInfo[param1Int];
        }
      };
  }
  
  private List<TransmitPower> mTransmitPowerInfo = new ArrayList<>(5);
  
  public ModemActivityInfo(long paramLong, int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    this.mTimestamp = paramLong;
    this.mSleepTimeMs = paramInt1;
    this.mIdleTimeMs = paramInt2;
    populateTransmitPowerRange(paramArrayOfint);
    this.mRxTimeMs = paramInt3;
  }
  
  private void populateTransmitPowerRange(int[] paramArrayOfint) {
    byte b2, b1 = 0;
    while (true) {
      b2 = b1;
      if (b1 < Math.min(paramArrayOfint.length, 5)) {
        this.mTransmitPowerInfo.add(b1, new TransmitPower(TX_POWER_RANGES[b1], paramArrayOfint[b1]));
        b1++;
        continue;
      } 
      break;
    } 
    for (; b2 < 5; b2++)
      this.mTransmitPowerInfo.add(b2, new TransmitPower(TX_POWER_RANGES[b2], 0)); 
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ModemActivityInfo{ mTimestamp=");
    stringBuilder.append(this.mTimestamp);
    stringBuilder.append(" mSleepTimeMs=");
    stringBuilder.append(this.mSleepTimeMs);
    stringBuilder.append(" mIdleTimeMs=");
    stringBuilder.append(this.mIdleTimeMs);
    stringBuilder.append(" mTransmitPowerInfo[]=");
    List<TransmitPower> list = this.mTransmitPowerInfo;
    stringBuilder.append(list.toString());
    stringBuilder.append(" mRxTimeMs=");
    stringBuilder.append(this.mRxTimeMs);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mTimestamp);
    paramParcel.writeInt(this.mSleepTimeMs);
    paramParcel.writeInt(this.mIdleTimeMs);
    for (paramInt = 0; paramInt < 5; paramInt++)
      paramParcel.writeInt(((TransmitPower)this.mTransmitPowerInfo.get(paramInt)).getTimeInMillis()); 
    paramParcel.writeInt(this.mRxTimeMs);
  }
  
  public long getTimestamp() {
    return this.mTimestamp;
  }
  
  public void setTimestamp(long paramLong) {
    this.mTimestamp = paramLong;
  }
  
  public List<TransmitPower> getTransmitPowerInfo() {
    return this.mTransmitPowerInfo;
  }
  
  public void setTransmitTimeMillis(int[] paramArrayOfint) {
    populateTransmitPowerRange(paramArrayOfint);
  }
  
  public int[] getTransmitTimeMillis() {
    int[] arrayOfInt = new int[5];
    for (byte b = 0; b < arrayOfInt.length; b++)
      arrayOfInt[b] = ((TransmitPower)this.mTransmitPowerInfo.get(b)).getTimeInMillis(); 
    return arrayOfInt;
  }
  
  public int getSleepTimeMillis() {
    return this.mSleepTimeMs;
  }
  
  public void setSleepTimeMillis(int paramInt) {
    this.mSleepTimeMs = paramInt;
  }
  
  public int getIdleTimeMillis() {
    return this.mIdleTimeMs;
  }
  
  public void setIdleTimeMillis(int paramInt) {
    this.mIdleTimeMs = paramInt;
  }
  
  public int getReceiveTimeMillis() {
    return this.mRxTimeMs;
  }
  
  public void setReceiveTimeMillis(int paramInt) {
    this.mRxTimeMs = paramInt;
  }
  
  public boolean isValid() {
    boolean bool;
    Iterator<TransmitPower> iterator = getTransmitPowerInfo().iterator();
    while (true) {
      boolean bool1 = iterator.hasNext();
      bool = false;
      if (bool1) {
        TransmitPower transmitPower = iterator.next();
        if (transmitPower.getTimeInMillis() < 0)
          return false; 
        continue;
      } 
      break;
    } 
    if (getIdleTimeMillis() >= 0 && getSleepTimeMillis() >= 0 && 
      getReceiveTimeMillis() >= 0 && !isEmpty())
      bool = true; 
    return bool;
  }
  
  private boolean isEmpty() {
    boolean bool;
    Iterator<TransmitPower> iterator = getTransmitPowerInfo().iterator();
    while (true) {
      boolean bool1 = iterator.hasNext();
      bool = false;
      if (bool1) {
        TransmitPower transmitPower = iterator.next();
        if (transmitPower.getTimeInMillis() != 0)
          return false; 
        continue;
      } 
      break;
    } 
    if (getIdleTimeMillis() == 0 && getSleepTimeMillis() == 0 && 
      getReceiveTimeMillis() == 0)
      bool = true; 
    return bool;
  }
  
  class TransmitPower {
    private Range<Integer> mPowerRangeInDbm;
    
    private int mTimeInMillis;
    
    final ModemActivityInfo this$0;
    
    public TransmitPower(Range<Integer> param1Range, int param1Int) {
      this.mTimeInMillis = param1Int;
      this.mPowerRangeInDbm = param1Range;
    }
    
    public int getTimeInMillis() {
      return this.mTimeInMillis;
    }
    
    public Range<Integer> getPowerRangeInDbm() {
      return this.mPowerRangeInDbm;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("TransmitPower{ mTimeInMillis=");
      stringBuilder.append(this.mTimeInMillis);
      stringBuilder.append(" mPowerRangeInDbm={");
      Range<Integer> range = this.mPowerRangeInDbm;
      stringBuilder.append(range.getLower());
      stringBuilder.append(",");
      range = this.mPowerRangeInDbm;
      stringBuilder.append(range.getUpper());
      stringBuilder.append("}}");
      return stringBuilder.toString();
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class TxPowerLevel implements Annotation {}
}
