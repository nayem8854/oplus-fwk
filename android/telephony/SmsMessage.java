package android.telephony;

import android.annotation.SystemApi;
import android.content.res.Resources;
import android.os.Binder;
import android.text.TextUtils;
import com.android.internal.telephony.GsmAlphabet;
import com.android.internal.telephony.Sms7BitEncodingTranslator;
import com.android.internal.telephony.SmsHeader;
import com.android.internal.telephony.SmsMessageBase;
import com.android.internal.telephony.cdma.sms.UserData;
import com.android.telephony.Rlog;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;

public class SmsMessage extends OplusSmsMessage {
  public SmsMessageBase mWrappedSmsMessage;
  
  class MessageClass extends Enum<MessageClass> {
    private static final MessageClass[] $VALUES;
    
    public static final MessageClass CLASS_0 = new MessageClass("CLASS_0", 1);
    
    public static final MessageClass CLASS_1 = new MessageClass("CLASS_1", 2);
    
    public static final MessageClass CLASS_2 = new MessageClass("CLASS_2", 3);
    
    public static final MessageClass CLASS_3;
    
    public static MessageClass[] values() {
      return (MessageClass[])$VALUES.clone();
    }
    
    public static MessageClass valueOf(String param1String) {
      return Enum.<MessageClass>valueOf(MessageClass.class, param1String);
    }
    
    private MessageClass(SmsMessage this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static final MessageClass UNKNOWN = new MessageClass("UNKNOWN", 0);
    
    static {
      MessageClass messageClass = new MessageClass("CLASS_3", 4);
      $VALUES = new MessageClass[] { UNKNOWN, CLASS_0, CLASS_1, CLASS_2, messageClass };
    }
  }
  
  private int mSubId = 0;
  
  public void setSubId(int paramInt) {
    this.mSubId = paramInt;
  }
  
  public int getSubId() {
    return this.mSubId;
  }
  
  class SubmitPdu {
    public byte[] encodedMessage;
    
    public byte[] encodedScAddress;
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("SubmitPdu: encodedScAddress = ");
      byte[] arrayOfByte = this.encodedScAddress;
      stringBuilder.append(Arrays.toString(arrayOfByte));
      stringBuilder.append(", encodedMessage = ");
      arrayOfByte = this.encodedMessage;
      stringBuilder.append(Arrays.toString(arrayOfByte));
      return stringBuilder.toString();
    }
    
    protected SubmitPdu(SmsMessage this$0) {
      this.encodedMessage = ((SmsMessageBase.SubmitPduBase)this$0).encodedMessage;
      this.encodedScAddress = ((SmsMessageBase.SubmitPduBase)this$0).encodedScAddress;
    }
  }
  
  public SmsMessage(SmsMessageBase paramSmsMessageBase) {
    this.mWrappedSmsMessage = paramSmsMessageBase;
  }
  
  @Deprecated
  public static SmsMessage createFromPdu(byte[] paramArrayOfbyte) {
    String str;
    int i = TelephonyManager.getDefault().getCurrentPhoneType();
    if (2 == i) {
      str = "3gpp2";
    } else {
      str = "3gpp";
    } 
    return createFromPdu(paramArrayOfbyte, str);
  }
  
  public static SmsMessage createFromPdu(byte[] paramArrayOfbyte, String paramString) {
    return createFromPdu(paramArrayOfbyte, paramString, true);
  }
  
  private static SmsMessage createFromPdu(byte[] paramArrayOfbyte, String paramString, boolean paramBoolean) {
    StringBuilder stringBuilder;
    com.android.internal.telephony.cdma.SmsMessage smsMessage1;
    com.android.internal.telephony.gsm.SmsMessage smsMessage;
    String str;
    if (paramArrayOfbyte == null) {
      Rlog.i("SmsMessage", "createFromPdu(): pdu is null");
      return null;
    } 
    if ("3gpp2".equals(paramString)) {
      str = "3gpp";
    } else {
      str = "3gpp2";
    } 
    if ("3gpp2".equals(paramString)) {
      smsMessage1 = com.android.internal.telephony.cdma.SmsMessage.createFromPdu(paramArrayOfbyte);
    } else if ("3gpp".equals(smsMessage1)) {
      smsMessage = com.android.internal.telephony.gsm.SmsMessage.createFromPdu(paramArrayOfbyte);
    } else {
      stringBuilder = new StringBuilder();
      stringBuilder.append("createFromPdu(): unsupported message format ");
      stringBuilder.append((String)smsMessage);
      Rlog.e("SmsMessage", stringBuilder.toString());
      return null;
    } 
    if (smsMessage != null)
      return new SmsMessage((SmsMessageBase)smsMessage); 
    if (paramBoolean)
      return createFromPdu((byte[])stringBuilder, str, false); 
    Rlog.e("SmsMessage", "createFromPdu(): wrappedMessage is null");
    return null;
  }
  
  public static SmsMessage newFromCMT(byte[] paramArrayOfbyte) {
    com.android.internal.telephony.gsm.SmsMessage smsMessage = com.android.internal.telephony.gsm.SmsMessage.newFromCMT(paramArrayOfbyte);
    if (smsMessage != null)
      return new SmsMessage((SmsMessageBase)smsMessage); 
    Rlog.e("SmsMessage", "newFromCMT(): wrappedMessage is null");
    return null;
  }
  
  public static SmsMessage createFromEfRecord(int paramInt, byte[] paramArrayOfbyte) {
    return createFromEfRecord(paramInt, paramArrayOfbyte, SmsManager.getDefaultSmsSubscriptionId());
  }
  
  public static SmsMessage createFromEfRecord(int paramInt1, byte[] paramArrayOfbyte, int paramInt2) {
    com.android.internal.telephony.cdma.SmsMessage smsMessage1;
    com.android.internal.telephony.gsm.SmsMessage smsMessage;
    if (isCdmaVoice(paramInt2)) {
      smsMessage1 = com.android.internal.telephony.cdma.SmsMessage.createFromEfRecord(paramInt1, paramArrayOfbyte);
    } else {
      smsMessage = com.android.internal.telephony.gsm.SmsMessage.createFromEfRecord(paramInt1, (byte[])smsMessage1);
    } 
    if (smsMessage != null) {
      SmsMessage smsMessage2 = new SmsMessage((SmsMessageBase)smsMessage);
    } else {
      smsMessage = null;
    } 
    return (SmsMessage)smsMessage;
  }
  
  @SystemApi
  public static SmsMessage createFromNativeSmsSubmitPdu(byte[] paramArrayOfbyte, boolean paramBoolean) {
    com.android.internal.telephony.cdma.SmsMessage smsMessage1;
    com.android.internal.telephony.gsm.SmsMessage smsMessage;
    if (paramBoolean) {
      smsMessage1 = com.android.internal.telephony.cdma.SmsMessage.createFromEfRecord(0, paramArrayOfbyte);
    } else {
      smsMessage = com.android.internal.telephony.gsm.SmsMessage.createFromEfRecord(0, (byte[])smsMessage1);
    } 
    if (smsMessage != null) {
      SmsMessage smsMessage2 = new SmsMessage((SmsMessageBase)smsMessage);
    } else {
      smsMessage = null;
    } 
    return (SmsMessage)smsMessage;
  }
  
  public static int getTPLayerLengthForPDU(String paramString) {
    if (isCdmaVoice())
      return com.android.internal.telephony.cdma.SmsMessage.getTPLayerLengthForPDU(paramString); 
    return com.android.internal.telephony.gsm.SmsMessage.getTPLayerLengthForPDU(paramString);
  }
  
  public static int[] calculateLength(CharSequence paramCharSequence, boolean paramBoolean) {
    return calculateLength(paramCharSequence, paramBoolean, SmsManager.getDefaultSmsSubscriptionId());
  }
  
  public static int[] calculateLength(CharSequence paramCharSequence, boolean paramBoolean, int paramInt) {
    GsmAlphabet.TextEncodingDetails textEncodingDetails;
    if (useCdmaFormatForMoSms(paramInt)) {
      textEncodingDetails = com.android.internal.telephony.cdma.SmsMessage.calculateLength(paramCharSequence, paramBoolean, true);
    } else {
      textEncodingDetails = com.android.internal.telephony.gsm.SmsMessage.calculateLength((CharSequence)textEncodingDetails, paramBoolean);
    } 
    int i = textEncodingDetails.msgCount;
    paramInt = textEncodingDetails.codeUnitCount;
    int j = textEncodingDetails.codeUnitsRemaining;
    int k = textEncodingDetails.codeUnitSize;
    int m = textEncodingDetails.languageTable;
    int n = textEncodingDetails.languageShiftTable;
    return new int[] { i, paramInt, j, k, m, n };
  }
  
  public static ArrayList<String> fragmentText(String paramString) {
    return fragmentText(paramString, SmsManager.getDefaultSmsSubscriptionId());
  }
  
  public static ArrayList<String> fragmentText(String paramString, int paramInt) {
    GsmAlphabet.TextEncodingDetails textEncodingDetails;
    boolean bool = useCdmaFormatForMoSms(paramInt);
    if (bool) {
      textEncodingDetails = com.android.internal.telephony.cdma.SmsMessage.calculateLength(paramString, false, true);
    } else {
      textEncodingDetails = com.android.internal.telephony.gsm.SmsMessage.calculateLength(paramString, false);
    } 
    if (textEncodingDetails != null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("subId=");
      stringBuilder1.append(paramInt);
      stringBuilder1.append(" isCdma=");
      stringBuilder1.append(bool);
      stringBuilder1.append(" codeUnitSize=");
      stringBuilder1.append(textEncodingDetails.codeUnitSize);
      Rlog.d("SmsMessage", stringBuilder1.toString());
    } 
    if (textEncodingDetails.codeUnitSize == 1) {
      if (textEncodingDetails.languageTable != 0 && textEncodingDetails.languageShiftTable != 0) {
        paramInt = 7;
      } else if (textEncodingDetails.languageTable != 0 || textEncodingDetails.languageShiftTable != 0) {
        paramInt = 4;
      } else {
        paramInt = 0;
      } 
      int k = paramInt;
      if (textEncodingDetails.msgCount > 1)
        k = paramInt + 6; 
      paramInt = k;
      if (k != 0)
        paramInt = k + 1; 
      paramInt = 160 - paramInt;
    } else if (textEncodingDetails.msgCount > 1) {
      char c = '';
      paramInt = c;
      if (!hasEmsSupport()) {
        paramInt = c;
        if (textEncodingDetails.msgCount < 10)
          paramInt = 134 - 2; 
      } 
    } else {
      paramInt = 140;
    } 
    String str1 = null;
    Resources resources = Resources.getSystem();
    if (resources.getBoolean(17891537))
      str1 = Sms7BitEncodingTranslator.translate(paramString, bool); 
    String str2 = str1;
    if (TextUtils.isEmpty(str1))
      str2 = paramString; 
    int i = 0;
    int j = str2.length();
    ArrayList<String> arrayList = new ArrayList(textEncodingDetails.msgCount);
    while (i < j) {
      int k;
      if (textEncodingDetails.codeUnitSize == 1) {
        if (bool && textEncodingDetails.msgCount == 1) {
          k = Math.min(paramInt, j - i) + i;
        } else {
          k = GsmAlphabet.findGsmSeptetLimitIndex(str2, i, paramInt, textEncodingDetails.languageTable, textEncodingDetails.languageShiftTable);
        } 
      } else {
        k = SmsMessageBase.findNextUnicodePosition(i, paramInt, str2);
      } 
      if (k <= i || k > j) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("fragmentText failed (");
        stringBuilder1.append(i);
        stringBuilder1.append(" >= ");
        stringBuilder1.append(k);
        stringBuilder1.append(" or ");
        stringBuilder1.append(k);
        stringBuilder1.append(" >= ");
        stringBuilder1.append(j);
        stringBuilder1.append(")");
        Rlog.e("SmsMessage", stringBuilder1.toString());
        break;
      } 
      arrayList.add(str2.substring(i, k));
      i = k;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("size=");
    stringBuilder.append(arrayList.size());
    Rlog.d("SmsMessage", stringBuilder.toString());
    return arrayList;
  }
  
  public static int[] calculateLength(String paramString, boolean paramBoolean) {
    return calculateLength(paramString, paramBoolean);
  }
  
  public static int[] calculateLength(String paramString, boolean paramBoolean, int paramInt) {
    return calculateLength(paramString, paramBoolean, paramInt);
  }
  
  public static SubmitPdu getSubmitPdu(String paramString1, String paramString2, String paramString3, boolean paramBoolean) {
    int i = SmsManager.getDefaultSmsSubscriptionId();
    return getSubmitPdu(paramString1, paramString2, paramString3, paramBoolean, i);
  }
  
  public static SubmitPdu getSubmitPdu(String paramString1, String paramString2, String paramString3, boolean paramBoolean, int paramInt) {
    com.android.internal.telephony.cdma.SmsMessage.SubmitPdu submitPdu1;
    com.android.internal.telephony.gsm.SmsMessage.SubmitPdu submitPdu;
    SubmitPdu submitPdu2;
    boolean bool = useCdmaFormatForMoSms(paramInt);
    String str = null;
    if (bool) {
      submitPdu1 = com.android.internal.telephony.cdma.SmsMessage.getSubmitPdu(paramString1, paramString2, paramString3, paramBoolean, null);
    } else {
      submitPdu = com.android.internal.telephony.gsm.SmsMessage.getSubmitPdu((String)submitPdu1, paramString2, paramString3, paramBoolean);
    } 
    paramString2 = str;
    if (submitPdu != null)
      submitPdu2 = new SubmitPdu((SmsMessageBase.SubmitPduBase)submitPdu); 
    return submitPdu2;
  }
  
  public static SubmitPdu getSubmitPdu(String paramString1, String paramString2, short paramShort, byte[] paramArrayOfbyte, boolean paramBoolean) {
    com.android.internal.telephony.cdma.SmsMessage.SubmitPdu submitPdu1;
    com.android.internal.telephony.gsm.SmsMessage.SubmitPdu submitPdu;
    if (useCdmaFormatForMoSms()) {
      submitPdu1 = com.android.internal.telephony.cdma.SmsMessage.getSubmitPdu(paramString1, paramString2, paramShort, paramArrayOfbyte, paramBoolean);
    } else {
      submitPdu = com.android.internal.telephony.gsm.SmsMessage.getSubmitPdu((String)submitPdu1, paramString2, paramShort, paramArrayOfbyte, paramBoolean);
    } 
    if (submitPdu != null) {
      SubmitPdu submitPdu2 = new SubmitPdu((SmsMessageBase.SubmitPduBase)submitPdu);
    } else {
      submitPdu = null;
    } 
    return (SubmitPdu)submitPdu;
  }
  
  @SystemApi
  public static SubmitPdu getSmsPdu(int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3, long paramLong) {
    com.android.internal.telephony.cdma.SmsMessage.SubmitPdu submitPdu1;
    com.android.internal.telephony.gsm.SmsMessage.SubmitPdu submitPdu;
    SubmitPdu submitPdu2;
    boolean bool = isCdmaVoice(paramInt1);
    String str = null;
    if (bool) {
      if (paramInt2 == 1 || paramInt2 == 3) {
        submitPdu1 = com.android.internal.telephony.cdma.SmsMessage.getDeliverPdu(paramString2, paramString3, paramLong);
      } else {
        submitPdu1 = com.android.internal.telephony.cdma.SmsMessage.getSubmitPdu((String)submitPdu1, paramString2, paramString3, false, null);
      } 
    } else if (paramInt2 == 1 || paramInt2 == 3) {
      submitPdu = com.android.internal.telephony.gsm.SmsMessage.getDeliverPdu((String)submitPdu1, paramString2, paramString3, paramLong);
    } else {
      submitPdu = com.android.internal.telephony.gsm.SmsMessage.getSubmitPdu((String)submitPdu, paramString2, paramString3, false, null);
    } 
    paramString2 = str;
    if (submitPdu != null)
      submitPdu2 = new SubmitPdu((SmsMessageBase.SubmitPduBase)submitPdu); 
    return submitPdu2;
  }
  
  @SystemApi
  public static byte[] getSubmitPduEncodedMessage(boolean paramBoolean, String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    byte[] arrayOfByte1, arrayOfByte2;
    SmsHeader.ConcatRef concatRef = new SmsHeader.ConcatRef();
    concatRef.refNumber = paramInt4;
    concatRef.seqNumber = paramInt5;
    concatRef.msgCount = paramInt6;
    concatRef.isEightBits = true;
    SmsHeader smsHeader = new SmsHeader();
    smsHeader.concatRef = concatRef;
    if (paramInt1 == 1) {
      smsHeader.languageTable = paramInt2;
      smsHeader.languageShiftTable = paramInt3;
    } 
    if (paramBoolean) {
      arrayOfByte2 = SmsHeader.toByteArray(smsHeader);
      arrayOfByte1 = (com.android.internal.telephony.gsm.SmsMessage.getSubmitPdu(null, paramString1, paramString2, false, arrayOfByte2, paramInt1, paramInt2, paramInt3)).encodedMessage;
    } else {
      UserData userData = new UserData();
      userData.payloadStr = paramString2;
      userData.userDataHeader = (SmsHeader)arrayOfByte2;
      if (paramInt1 == 1) {
        userData.msgEncoding = 9;
      } else {
        userData.msgEncoding = 4;
      } 
      userData.msgEncodingSet = true;
      arrayOfByte1 = (com.android.internal.telephony.cdma.SmsMessage.getSubmitPdu((String)arrayOfByte1, userData, false)).encodedMessage;
    } 
    if (arrayOfByte1 == null)
      return new byte[0]; 
    return arrayOfByte1;
  }
  
  public String getServiceCenterAddress() {
    return this.mWrappedSmsMessage.getServiceCenterAddress();
  }
  
  public String getOriginatingAddress() {
    return this.mWrappedSmsMessage.getOriginatingAddress();
  }
  
  public String getDisplayOriginatingAddress() {
    return this.mWrappedSmsMessage.getDisplayOriginatingAddress();
  }
  
  public String getMessageBody() {
    return this.mWrappedSmsMessage.getMessageBody();
  }
  
  public MessageClass getMessageClass() {
    int i = null.$SwitchMap$com$android$internal$telephony$SmsConstants$MessageClass[this.mWrappedSmsMessage.getMessageClass().ordinal()];
    if (i != 1) {
      if (i != 2) {
        if (i != 3) {
          if (i != 4)
            return MessageClass.UNKNOWN; 
          return MessageClass.CLASS_3;
        } 
        return MessageClass.CLASS_2;
      } 
      return MessageClass.CLASS_1;
    } 
    return MessageClass.CLASS_0;
  }
  
  public String getDisplayMessageBody() {
    return this.mWrappedSmsMessage.getDisplayMessageBody();
  }
  
  public String getPseudoSubject() {
    return this.mWrappedSmsMessage.getPseudoSubject();
  }
  
  public long getTimestampMillis() {
    return this.mWrappedSmsMessage.getTimestampMillis();
  }
  
  public boolean isEmail() {
    return this.mWrappedSmsMessage.isEmail();
  }
  
  public String getEmailBody() {
    return this.mWrappedSmsMessage.getEmailBody();
  }
  
  public String getEmailFrom() {
    return this.mWrappedSmsMessage.getEmailFrom();
  }
  
  public int getProtocolIdentifier() {
    return this.mWrappedSmsMessage.getProtocolIdentifier();
  }
  
  public boolean isReplace() {
    return this.mWrappedSmsMessage.isReplace();
  }
  
  public boolean isCphsMwiMessage() {
    return this.mWrappedSmsMessage.isCphsMwiMessage();
  }
  
  public boolean isMWIClearMessage() {
    return this.mWrappedSmsMessage.isMWIClearMessage();
  }
  
  public boolean isMWISetMessage() {
    return this.mWrappedSmsMessage.isMWISetMessage();
  }
  
  public boolean isMwiDontStore() {
    return this.mWrappedSmsMessage.isMwiDontStore();
  }
  
  public byte[] getUserData() {
    return this.mWrappedSmsMessage.getUserData();
  }
  
  public byte[] getPdu() {
    return this.mWrappedSmsMessage.getPdu();
  }
  
  @Deprecated
  public int getStatusOnSim() {
    return this.mWrappedSmsMessage.getStatusOnIcc();
  }
  
  public int getStatusOnIcc() {
    return this.mWrappedSmsMessage.getStatusOnIcc();
  }
  
  @Deprecated
  public int getIndexOnSim() {
    return this.mWrappedSmsMessage.getIndexOnIcc();
  }
  
  public int getIndexOnIcc() {
    return this.mWrappedSmsMessage.getIndexOnIcc();
  }
  
  public int getStatus() {
    return this.mWrappedSmsMessage.getStatus();
  }
  
  public boolean isStatusReportMessage() {
    return this.mWrappedSmsMessage.isStatusReportMessage();
  }
  
  public boolean isReplyPathPresent() {
    return this.mWrappedSmsMessage.isReplyPathPresent();
  }
  
  private static boolean useCdmaFormatForMoSms() {
    return useCdmaFormatForMoSms(SmsManager.getDefaultSmsSubscriptionId());
  }
  
  private static boolean useCdmaFormatForMoSms(int paramInt) {
    SmsManager smsManager = SmsManager.getSmsManagerForSubscriptionId(paramInt);
    if (!smsManager.isImsSmsSupported())
      return isCdmaVoice(paramInt); 
    return "3gpp2".equals(smsManager.getImsSmsFormat());
  }
  
  private static boolean isCdmaVoice() {
    return isCdmaVoice(SmsManager.getDefaultSmsSubscriptionId());
  }
  
  private static boolean isCdmaVoice(int paramInt) {
    boolean bool;
    paramInt = TelephonyManager.getDefault().getCurrentPhoneType(paramInt);
    if (2 == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean hasEmsSupport() {
    if (!isNoEmsSupportConfigListExisted())
      return true; 
    long l = Binder.clearCallingIdentity();
    try {
      String str1 = TelephonyManager.getDefault().getSimOperatorNumeric();
      String str2 = TelephonyManager.getDefault().getGroupIdLevel1();
      Binder.restoreCallingIdentity(l);
      return true;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public static boolean shouldAppendPageNumberAsPrefix() {
    if (!isNoEmsSupportConfigListExisted())
      return false; 
    long l = Binder.clearCallingIdentity();
    try {
      String str1 = TelephonyManager.getDefault().getSimOperatorNumeric();
      String str2 = TelephonyManager.getDefault().getGroupIdLevel1();
      Binder.restoreCallingIdentity(l);
      return false;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  class NoEmsSupportConfig {
    String mGid1;
    
    boolean mIsPrefix;
    
    String mOperatorNumber;
    
    public NoEmsSupportConfig(SmsMessage this$0) {
      this.mOperatorNumber = (String)this$0[0];
      this.mIsPrefix = "prefix".equals(this$0[1]);
      if (this$0.length > 2) {
        this$0 = this$0[2];
      } else {
        this$0 = null;
      } 
      this.mGid1 = (String)this$0;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("NoEmsSupportConfig { mOperatorNumber = ");
      stringBuilder.append(this.mOperatorNumber);
      stringBuilder.append(", mIsPrefix = ");
      stringBuilder.append(this.mIsPrefix);
      stringBuilder.append(", mGid1 = ");
      stringBuilder.append(this.mGid1);
      stringBuilder.append(" }");
      return stringBuilder.toString();
    }
  }
  
  private static NoEmsSupportConfig[] mNoEmsSupportConfigList = null;
  
  private static boolean mIsNoEmsSupportConfigListLoaded;
  
  public static final int MAX_USER_DATA_SEPTETS_WITH_HEADER = 153;
  
  public static final int MAX_USER_DATA_SEPTETS = 160;
  
  public static final int MAX_USER_DATA_BYTES_WITH_HEADER = 134;
  
  public static final int MAX_USER_DATA_BYTES = 140;
  
  private static final String LOG_TAG = "SmsMessage";
  
  public static final String FORMAT_3GPP2 = "3gpp2";
  
  public static final String FORMAT_3GPP = "3gpp";
  
  public static final int ENCODING_UNKNOWN = 0;
  
  public static final int ENCODING_KSC5601 = 4;
  
  public static final int ENCODING_8BIT = 2;
  
  public static final int ENCODING_7BIT = 1;
  
  public static final int ENCODING_16BIT = 3;
  
  static {
    mIsNoEmsSupportConfigListLoaded = false;
  }
  
  private static boolean isNoEmsSupportConfigListExisted() {
    if (!mIsNoEmsSupportConfigListLoaded) {
      Resources resources = Resources.getSystem();
      if (resources != null) {
        String[] arrayOfString = resources.getStringArray(17236112);
        if (arrayOfString != null && arrayOfString.length > 0) {
          mNoEmsSupportConfigList = new NoEmsSupportConfig[arrayOfString.length];
          for (byte b = 0; b < arrayOfString.length; b++)
            mNoEmsSupportConfigList[b] = new NoEmsSupportConfig(arrayOfString[b].split(";")); 
        } 
        mIsNoEmsSupportConfigListLoaded = true;
      } 
    } 
    NoEmsSupportConfig[] arrayOfNoEmsSupportConfig = mNoEmsSupportConfigList;
    if (arrayOfNoEmsSupportConfig != null && arrayOfNoEmsSupportConfig.length != 0)
      return true; 
    return false;
  }
  
  public String getRecipientAddress() {
    return this.mWrappedSmsMessage.getRecipientAddress();
  }
  
  public int getEncodingType() {
    SmsMessageBase smsMessageBase = this.mWrappedSmsMessage;
    if (smsMessageBase != null)
      return smsMessageBase.getEncodingType(); 
    return 0;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class EncodingSize implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Format implements Annotation {}
}
