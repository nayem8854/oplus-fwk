package android.telephony;

import android.annotation.SystemApi;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Parcel;
import android.os.ParcelUuid;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import com.android.internal.telephony.util.TelephonyUtils;
import com.android.telephony.Rlog;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class SubscriptionInfo implements Parcelable {
  public SubscriptionInfo(SubscriptionInfo paramSubscriptionInfo) {
    this(i, str1, j, charSequence1, charSequence2, k, m, str2, n, bitmap, str3, str4, str5, bool1, arrayOfUiccAccessRule1, str6, i1, bool2, str7, bool3, i2, i3, i4, str8, arrayOfUiccAccessRule2, bool4);
  }
  
  public SubscriptionInfo(int paramInt1, String paramString1, int paramInt2, CharSequence paramCharSequence1, CharSequence paramCharSequence2, int paramInt3, int paramInt4, String paramString2, int paramInt5, Bitmap paramBitmap, String paramString3, String paramString4, String paramString5, boolean paramBoolean, UiccAccessRule[] paramArrayOfUiccAccessRule, String paramString6) {
    this(paramInt1, paramString1, paramInt2, paramCharSequence1, paramCharSequence2, paramInt3, paramInt4, paramString2, paramInt5, paramBitmap, paramString3, paramString4, paramString5, paramBoolean, paramArrayOfUiccAccessRule, paramString6, -1, false, null, false, -1, -1, 0, null, null, true);
  }
  
  public SubscriptionInfo(int paramInt1, String paramString1, int paramInt2, CharSequence paramCharSequence1, CharSequence paramCharSequence2, int paramInt3, int paramInt4, String paramString2, int paramInt5, Bitmap paramBitmap, String paramString3, String paramString4, String paramString5, boolean paramBoolean1, UiccAccessRule[] paramArrayOfUiccAccessRule, String paramString6, boolean paramBoolean2, String paramString7, int paramInt6, int paramInt7) {
    this(paramInt1, paramString1, paramInt2, paramCharSequence1, paramCharSequence2, paramInt3, paramInt4, paramString2, paramInt5, paramBitmap, paramString3, paramString4, paramString5, paramBoolean1, paramArrayOfUiccAccessRule, paramString6, -1, paramBoolean2, paramString7, false, paramInt6, paramInt7, 0, null, null, true);
  }
  
  public SubscriptionInfo(int paramInt1, String paramString1, int paramInt2, CharSequence paramCharSequence1, CharSequence paramCharSequence2, int paramInt3, int paramInt4, String paramString2, int paramInt5, Bitmap paramBitmap, String paramString3, String paramString4, String paramString5, boolean paramBoolean1, UiccAccessRule[] paramArrayOfUiccAccessRule1, String paramString6, int paramInt6, boolean paramBoolean2, String paramString7, boolean paramBoolean3, int paramInt7, int paramInt8, int paramInt9, String paramString8, UiccAccessRule[] paramArrayOfUiccAccessRule2, boolean paramBoolean4) {
    ParcelUuid parcelUuid;
    this.mIsGroupDisabled = false;
    this.mAreUiccApplicationsEnabled = true;
    this.mId = paramInt1;
    this.mIccId = paramString1;
    this.mSimSlotIndex = paramInt2;
    this.mDisplayName = paramCharSequence1;
    this.mCarrierName = paramCharSequence2;
    this.mNameSource = paramInt3;
    this.mIconTint = paramInt4;
    this.mNumber = paramString2;
    this.mDataRoaming = paramInt5;
    this.mIconBitmap = paramBitmap;
    this.mMcc = paramString3;
    this.mMnc = paramString4;
    this.mCountryIso = paramString5;
    this.mIsEmbedded = paramBoolean1;
    this.mNativeAccessRules = paramArrayOfUiccAccessRule1;
    this.mCardString = paramString6;
    this.mCardId = paramInt6;
    this.mIsOpportunistic = paramBoolean2;
    if (paramString7 == null) {
      paramString1 = null;
    } else {
      parcelUuid = ParcelUuid.fromString(paramString7);
    } 
    this.mGroupUUID = parcelUuid;
    this.mIsGroupDisabled = paramBoolean3;
    this.mCarrierId = paramInt7;
    this.mProfileClass = paramInt8;
    this.mSubscriptionType = paramInt9;
    this.mGroupOwner = paramString8;
    this.mCarrierConfigAccessRules = paramArrayOfUiccAccessRule2;
    this.mAreUiccApplicationsEnabled = paramBoolean4;
  }
  
  public int getSubscriptionId() {
    return this.mId;
  }
  
  public String getIccId() {
    return this.mIccId;
  }
  
  public void clearIccId() {
    this.mIccId = "";
  }
  
  public int getSimSlotIndex() {
    return this.mSimSlotIndex;
  }
  
  public int getCarrierId() {
    return this.mCarrierId;
  }
  
  public CharSequence getDisplayName() {
    return this.mDisplayName;
  }
  
  public void setDisplayName(CharSequence paramCharSequence) {
    this.mDisplayName = paramCharSequence;
  }
  
  public CharSequence getCarrierName() {
    return this.mCarrierName;
  }
  
  public void setCarrierName(CharSequence paramCharSequence) {
    this.mCarrierName = paramCharSequence;
  }
  
  public int getNameSource() {
    return this.mNameSource;
  }
  
  public void setAssociatedPlmns(String[] paramArrayOfString1, String[] paramArrayOfString2) {
    this.mEhplmns = paramArrayOfString1;
    this.mHplmns = paramArrayOfString2;
  }
  
  public Bitmap createIconBitmap(Context paramContext) {
    int i = this.mIconBitmap.getWidth();
    int j = this.mIconBitmap.getHeight();
    DisplayMetrics displayMetrics = paramContext.getResources().getDisplayMetrics();
    Bitmap bitmap = Bitmap.createBitmap(displayMetrics, i, j, this.mIconBitmap.getConfig());
    Canvas canvas = new Canvas(bitmap);
    Paint paint = new Paint();
    paint.setColorFilter((ColorFilter)new PorterDuffColorFilter(this.mIconTint, PorterDuff.Mode.SRC_ATOP));
    canvas.drawBitmap(this.mIconBitmap, 0.0F, 0.0F, paint);
    paint.setColorFilter(null);
    paint.setAntiAlias(true);
    paint.setTypeface(Typeface.create("sans-serif", 0));
    paint.setColor(-1);
    paint.setTextSize(displayMetrics.density * 16.0F);
    String str = String.format("%d", new Object[] { Integer.valueOf(this.mSimSlotIndex + 1) });
    Rect rect = new Rect();
    paint.getTextBounds(str, 0, 1, rect);
    float f1 = i / 2.0F, f2 = rect.centerX();
    float f3 = j / 2.0F, f4 = rect.centerY();
    canvas.drawText(str, f1 - f2, f3 - f4, paint);
    return bitmap;
  }
  
  public int getIconTint() {
    return this.mIconTint;
  }
  
  public void setIconTint(int paramInt) {
    this.mIconTint = paramInt;
  }
  
  public String getNumber() {
    return this.mNumber;
  }
  
  public void clearNumber() {
    this.mNumber = "";
  }
  
  public int getDataRoaming() {
    return this.mDataRoaming;
  }
  
  @Deprecated
  public int getMcc() {
    int i = 0;
    try {
      if (this.mMcc != null)
        i = Integer.valueOf(this.mMcc).intValue(); 
      return i;
    } catch (NumberFormatException numberFormatException) {
      Log.w(SubscriptionInfo.class.getSimpleName(), "MCC string is not a number");
      return 0;
    } 
  }
  
  @Deprecated
  public int getMnc() {
    int i = 0;
    try {
      if (this.mMnc != null)
        i = Integer.valueOf(this.mMnc).intValue(); 
      return i;
    } catch (NumberFormatException numberFormatException) {
      Log.w(SubscriptionInfo.class.getSimpleName(), "MNC string is not a number");
      return 0;
    } 
  }
  
  public String getMccString() {
    return this.mMcc;
  }
  
  public String getMncString() {
    return this.mMnc;
  }
  
  public String getCountryIso() {
    return this.mCountryIso;
  }
  
  public boolean isEmbedded() {
    return this.mIsEmbedded;
  }
  
  public boolean isOpportunistic() {
    return this.mIsOpportunistic;
  }
  
  public ParcelUuid getGroupUuid() {
    return this.mGroupUUID;
  }
  
  public void clearGroupUuid() {
    this.mGroupUUID = null;
  }
  
  public List<String> getEhplmns() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mEhplmns : [Ljava/lang/String;
    //   4: astore_1
    //   5: aload_1
    //   6: ifnonnull -> 16
    //   9: invokestatic emptyList : ()Ljava/util/List;
    //   12: astore_1
    //   13: goto -> 21
    //   16: aload_1
    //   17: invokestatic asList : ([Ljava/lang/Object;)Ljava/util/List;
    //   20: astore_1
    //   21: aload_1
    //   22: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #579	-> 0
  }
  
  public List<String> getHplmns() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mHplmns : [Ljava/lang/String;
    //   4: astore_1
    //   5: aload_1
    //   6: ifnonnull -> 16
    //   9: invokestatic emptyList : ()Ljava/util/List;
    //   12: astore_1
    //   13: goto -> 21
    //   16: aload_1
    //   17: invokestatic asList : ([Ljava/lang/Object;)Ljava/util/List;
    //   20: astore_1
    //   21: aload_1
    //   22: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #586	-> 0
  }
  
  public String getGroupOwner() {
    return this.mGroupOwner;
  }
  
  @SystemApi
  public int getProfileClass() {
    return this.mProfileClass;
  }
  
  public int getSubscriptionType() {
    return this.mSubscriptionType;
  }
  
  @Deprecated
  public boolean canManageSubscription(Context paramContext) {
    return canManageSubscription(paramContext, paramContext.getPackageName());
  }
  
  @Deprecated
  public boolean canManageSubscription(Context paramContext, String paramString) {
    List<UiccAccessRule> list = getAllAccessRules();
    if (list == null)
      return false; 
    PackageManager packageManager = paramContext.getPackageManager();
    try {
      PackageInfo packageInfo = packageManager.getPackageInfo(paramString, 134217728);
      for (UiccAccessRule uiccAccessRule : list) {
        if (uiccAccessRule.getCarrierPrivilegeStatus(packageInfo) == 1)
          return true; 
      } 
      return false;
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("canManageSubscription: Unknown package: ");
      stringBuilder.append((String)uiccAccessRule);
      Log.d("SubscriptionInfo", stringBuilder.toString(), (Throwable)nameNotFoundException);
      return false;
    } 
  }
  
  @SystemApi
  public List<UiccAccessRule> getAccessRules() {
    UiccAccessRule[] arrayOfUiccAccessRule = this.mNativeAccessRules;
    if (arrayOfUiccAccessRule == null)
      return null; 
    return Arrays.asList(arrayOfUiccAccessRule);
  }
  
  public List<UiccAccessRule> getAllAccessRules() {
    ArrayList<UiccAccessRule> arrayList = new ArrayList();
    if (this.mNativeAccessRules != null)
      arrayList.addAll(getAccessRules()); 
    UiccAccessRule[] arrayOfUiccAccessRule = this.mCarrierConfigAccessRules;
    if (arrayOfUiccAccessRule != null)
      arrayList.addAll(Arrays.asList(arrayOfUiccAccessRule)); 
    if (arrayList.isEmpty())
      arrayList = null; 
    return arrayList;
  }
  
  public String getCardString() {
    return this.mCardString;
  }
  
  public void clearCardString() {
    this.mCardString = "";
  }
  
  public int getCardId() {
    return this.mCardId;
  }
  
  public void setGroupDisabled(boolean paramBoolean) {
    this.mIsGroupDisabled = paramBoolean;
  }
  
  @SystemApi
  public boolean isGroupDisabled() {
    return this.mIsGroupDisabled;
  }
  
  @SystemApi
  public boolean areUiccApplicationsEnabled() {
    return this.mAreUiccApplicationsEnabled;
  }
  
  public static final Parcelable.Creator<SubscriptionInfo> CREATOR = new Parcelable.Creator<SubscriptionInfo>() {
      public SubscriptionInfo createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        String str1 = param1Parcel.readString();
        int j = param1Parcel.readInt();
        CharSequence charSequence1 = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel);
        CharSequence charSequence2 = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel);
        int k = param1Parcel.readInt();
        int m = param1Parcel.readInt();
        String str2 = param1Parcel.readString();
        int n = param1Parcel.readInt();
        String str3 = param1Parcel.readString();
        String str4 = param1Parcel.readString();
        String str5 = param1Parcel.readString();
        Bitmap bitmap = (Bitmap)param1Parcel.readParcelable(Bitmap.class.getClassLoader());
        boolean bool1 = param1Parcel.readBoolean();
        UiccAccessRule[] arrayOfUiccAccessRule1 = (UiccAccessRule[])param1Parcel.createTypedArray(UiccAccessRule.CREATOR);
        String str6 = param1Parcel.readString();
        int i1 = param1Parcel.readInt();
        boolean bool2 = param1Parcel.readBoolean();
        String str7 = param1Parcel.readString();
        boolean bool3 = param1Parcel.readBoolean();
        int i2 = param1Parcel.readInt();
        int i3 = param1Parcel.readInt();
        int i4 = param1Parcel.readInt();
        String[] arrayOfString1 = param1Parcel.createStringArray();
        String[] arrayOfString2 = param1Parcel.createStringArray();
        String str8 = param1Parcel.readString();
        UiccAccessRule[] arrayOfUiccAccessRule2 = (UiccAccessRule[])param1Parcel.createTypedArray(UiccAccessRule.CREATOR);
        boolean bool4 = param1Parcel.readBoolean();
        SubscriptionInfo subscriptionInfo = new SubscriptionInfo(i, str1, j, charSequence1, charSequence2, k, m, str2, n, bitmap, str3, str4, str5, bool1, arrayOfUiccAccessRule1, str6, i1, bool2, str7, bool3, i2, i3, i4, str8, arrayOfUiccAccessRule2, bool4);
        subscriptionInfo.setAssociatedPlmns(arrayOfString1, arrayOfString2);
        return subscriptionInfo;
      }
      
      public SubscriptionInfo[] newArray(int param1Int) {
        return new SubscriptionInfo[param1Int];
      }
    };
  
  private static final int TEXT_SIZE = 16;
  
  private boolean mAreUiccApplicationsEnabled;
  
  private int mCardId;
  
  private String mCardString;
  
  private UiccAccessRule[] mCarrierConfigAccessRules;
  
  private int mCarrierId;
  
  private CharSequence mCarrierName;
  
  private String mCountryIso;
  
  private int mDataRoaming;
  
  private CharSequence mDisplayName;
  
  private String[] mEhplmns;
  
  private String mGroupOwner;
  
  private ParcelUuid mGroupUUID;
  
  private String[] mHplmns;
  
  private String mIccId;
  
  private Bitmap mIconBitmap;
  
  private int mIconTint;
  
  private int mId;
  
  private boolean mIsEmbedded;
  
  private boolean mIsGroupDisabled;
  
  private boolean mIsOpportunistic;
  
  private String mMcc;
  
  private String mMnc;
  
  private int mNameSource;
  
  private UiccAccessRule[] mNativeAccessRules;
  
  private String mNumber;
  
  private int mProfileClass;
  
  private int mSimSlotIndex;
  
  private int mSubscriptionType;
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    String str;
    paramParcel.writeInt(this.mId);
    paramParcel.writeString(this.mIccId);
    paramParcel.writeInt(this.mSimSlotIndex);
    TextUtils.writeToParcel(this.mDisplayName, paramParcel, 0);
    TextUtils.writeToParcel(this.mCarrierName, paramParcel, 0);
    paramParcel.writeInt(this.mNameSource);
    paramParcel.writeInt(this.mIconTint);
    paramParcel.writeString(this.mNumber);
    paramParcel.writeInt(this.mDataRoaming);
    paramParcel.writeString(this.mMcc);
    paramParcel.writeString(this.mMnc);
    paramParcel.writeString(this.mCountryIso);
    paramParcel.writeParcelable((Parcelable)this.mIconBitmap, paramInt);
    paramParcel.writeBoolean(this.mIsEmbedded);
    paramParcel.writeTypedArray((Parcelable[])this.mNativeAccessRules, paramInt);
    paramParcel.writeString(this.mCardString);
    paramParcel.writeInt(this.mCardId);
    paramParcel.writeBoolean(this.mIsOpportunistic);
    ParcelUuid parcelUuid = this.mGroupUUID;
    if (parcelUuid == null) {
      parcelUuid = null;
    } else {
      str = parcelUuid.toString();
    } 
    paramParcel.writeString(str);
    paramParcel.writeBoolean(this.mIsGroupDisabled);
    paramParcel.writeInt(this.mCarrierId);
    paramParcel.writeInt(this.mProfileClass);
    paramParcel.writeInt(this.mSubscriptionType);
    paramParcel.writeStringArray(this.mEhplmns);
    paramParcel.writeStringArray(this.mHplmns);
    paramParcel.writeString(this.mGroupOwner);
    paramParcel.writeTypedArray((Parcelable[])this.mCarrierConfigAccessRules, paramInt);
    paramParcel.writeBoolean(this.mAreUiccApplicationsEnabled);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static String givePrintableIccid(String paramString) {
    String str;
    StringBuilder stringBuilder = null;
    if (paramString != null)
      if (paramString.length() > 9 && !TelephonyUtils.IS_DEBUGGABLE) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(paramString.substring(0, 9));
        stringBuilder.append(Rlog.pii(false, paramString.substring(9)));
        str = stringBuilder.toString();
      } else {
        str = paramString;
      }  
    return str;
  }
  
  public String toString() {
    String str1 = givePrintableIccid(this.mIccId);
    String str2 = givePrintableIccid(this.mCardString);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{id=");
    stringBuilder.append(this.mId);
    stringBuilder.append(" iccId=");
    stringBuilder.append(str1);
    stringBuilder.append(" simSlotIndex=");
    stringBuilder.append(this.mSimSlotIndex);
    stringBuilder.append(" carrierId=");
    stringBuilder.append(this.mCarrierId);
    stringBuilder.append(" displayName=");
    stringBuilder.append(this.mDisplayName);
    stringBuilder.append(" carrierName=");
    stringBuilder.append(this.mCarrierName);
    stringBuilder.append(" nameSource=");
    stringBuilder.append(this.mNameSource);
    stringBuilder.append(" iconTint=");
    stringBuilder.append(this.mIconTint);
    stringBuilder.append(" dataRoaming=");
    stringBuilder.append(this.mDataRoaming);
    stringBuilder.append(" iconBitmap=");
    stringBuilder.append(this.mIconBitmap);
    stringBuilder.append(" mcc=");
    stringBuilder.append(this.mMcc);
    stringBuilder.append(" mnc=");
    stringBuilder.append(this.mMnc);
    stringBuilder.append(" countryIso=");
    stringBuilder.append(this.mCountryIso);
    stringBuilder.append(" isEmbedded=");
    stringBuilder.append(this.mIsEmbedded);
    stringBuilder.append(" nativeAccessRules=");
    UiccAccessRule[] arrayOfUiccAccessRule1 = this.mNativeAccessRules;
    stringBuilder.append(Arrays.toString((Object[])arrayOfUiccAccessRule1));
    stringBuilder.append(" cardString=");
    stringBuilder.append(str2);
    stringBuilder.append(" cardId=");
    stringBuilder.append(this.mCardId);
    stringBuilder.append(" isOpportunistic=");
    stringBuilder.append(this.mIsOpportunistic);
    stringBuilder.append(" groupUUID=");
    stringBuilder.append(this.mGroupUUID);
    stringBuilder.append(" isGroupDisabled=");
    stringBuilder.append(this.mIsGroupDisabled);
    stringBuilder.append(" profileClass=");
    stringBuilder.append(this.mProfileClass);
    stringBuilder.append(" ehplmns=");
    String[] arrayOfString = this.mEhplmns;
    stringBuilder.append(Arrays.toString((Object[])arrayOfString));
    stringBuilder.append(" hplmns=");
    arrayOfString = this.mHplmns;
    stringBuilder.append(Arrays.toString((Object[])arrayOfString));
    stringBuilder.append(" subscriptionType=");
    stringBuilder.append(this.mSubscriptionType);
    stringBuilder.append(" groupOwner=");
    stringBuilder.append(this.mGroupOwner);
    stringBuilder.append(" carrierConfigAccessRules=");
    UiccAccessRule[] arrayOfUiccAccessRule2 = this.mCarrierConfigAccessRules;
    stringBuilder.append(Arrays.toString((Object[])arrayOfUiccAccessRule2));
    stringBuilder.append(" areUiccApplicationsEnabled=");
    stringBuilder.append(this.mAreUiccApplicationsEnabled);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int hashCode() {
    int i = this.mId, j = this.mSimSlotIndex, k = this.mNameSource, m = this.mIconTint, n = this.mDataRoaming;
    boolean bool1 = this.mIsEmbedded, bool2 = this.mIsOpportunistic;
    ParcelUuid parcelUuid = this.mGroupUUID;
    String str1 = this.mIccId, str2 = this.mNumber, str3 = this.mMcc, str4 = this.mMnc, str5 = this.mCountryIso, str6 = this.mCardString;
    int i1 = this.mCardId;
    CharSequence charSequence1 = this.mDisplayName, charSequence2 = this.mCarrierName;
    UiccAccessRule[] arrayOfUiccAccessRule = this.mNativeAccessRules;
    boolean bool3 = this.mIsGroupDisabled;
    int i2 = this.mCarrierId;
    int i3 = this.mProfileClass;
    String str7 = this.mGroupOwner;
    boolean bool4 = this.mAreUiccApplicationsEnabled;
    return Objects.hash(new Object[] { 
          Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Boolean.valueOf(bool1), Boolean.valueOf(bool2), parcelUuid, str1, str2, 
          str3, str4, str5, str6, Integer.valueOf(i1), charSequence1, charSequence2, arrayOfUiccAccessRule, Boolean.valueOf(bool3), Integer.valueOf(i2), 
          Integer.valueOf(i3), str7, Boolean.valueOf(bool4) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    if (paramObject == null)
      return false; 
    if (paramObject == this)
      return true; 
    try {
      paramObject = paramObject;
      if (this.mId == ((SubscriptionInfo)paramObject).mId && this.mSimSlotIndex == ((SubscriptionInfo)paramObject).mSimSlotIndex && this.mNameSource == ((SubscriptionInfo)paramObject).mNameSource && this.mIconTint == ((SubscriptionInfo)paramObject).mIconTint && this.mDataRoaming == ((SubscriptionInfo)paramObject).mDataRoaming && this.mIsEmbedded == ((SubscriptionInfo)paramObject).mIsEmbedded && this.mIsOpportunistic == ((SubscriptionInfo)paramObject).mIsOpportunistic && this.mIsGroupDisabled == ((SubscriptionInfo)paramObject).mIsGroupDisabled && this.mAreUiccApplicationsEnabled == ((SubscriptionInfo)paramObject).mAreUiccApplicationsEnabled && this.mCarrierId == ((SubscriptionInfo)paramObject).mCarrierId) {
        ParcelUuid parcelUuid1 = this.mGroupUUID, parcelUuid2 = ((SubscriptionInfo)paramObject).mGroupUUID;
        if (Objects.equals(parcelUuid1, parcelUuid2)) {
          String str2 = this.mIccId, str1 = ((SubscriptionInfo)paramObject).mIccId;
          if (Objects.equals(str2, str1)) {
            str2 = this.mNumber;
            str1 = ((SubscriptionInfo)paramObject).mNumber;
            if (Objects.equals(str2, str1)) {
              str2 = this.mMcc;
              str1 = ((SubscriptionInfo)paramObject).mMcc;
              if (Objects.equals(str2, str1)) {
                str2 = this.mMnc;
                str1 = ((SubscriptionInfo)paramObject).mMnc;
                if (Objects.equals(str2, str1)) {
                  str2 = this.mCountryIso;
                  str1 = ((SubscriptionInfo)paramObject).mCountryIso;
                  if (Objects.equals(str2, str1)) {
                    str2 = this.mCardString;
                    str1 = ((SubscriptionInfo)paramObject).mCardString;
                    if (Objects.equals(str2, str1)) {
                      int i = this.mCardId;
                      if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((SubscriptionInfo)paramObject).mCardId))) {
                        str2 = this.mGroupOwner;
                        str1 = ((SubscriptionInfo)paramObject).mGroupOwner;
                        if (Objects.equals(str2, str1)) {
                          CharSequence charSequence1 = this.mDisplayName, charSequence2 = ((SubscriptionInfo)paramObject).mDisplayName;
                          if (TextUtils.equals(charSequence1, charSequence2)) {
                            charSequence2 = this.mCarrierName;
                            charSequence1 = ((SubscriptionInfo)paramObject).mCarrierName;
                            if (TextUtils.equals(charSequence2, charSequence1)) {
                              UiccAccessRule[] arrayOfUiccAccessRule1 = this.mNativeAccessRules, arrayOfUiccAccessRule2 = ((SubscriptionInfo)paramObject).mNativeAccessRules;
                              if (Arrays.equals((Object[])arrayOfUiccAccessRule1, (Object[])arrayOfUiccAccessRule2) && this.mProfileClass == ((SubscriptionInfo)paramObject).mProfileClass) {
                                String[] arrayOfString1 = this.mEhplmns, arrayOfString2 = ((SubscriptionInfo)paramObject).mEhplmns;
                                if (Arrays.equals((Object[])arrayOfString1, (Object[])arrayOfString2)) {
                                  arrayOfString1 = this.mHplmns;
                                  paramObject = ((SubscriptionInfo)paramObject).mHplmns;
                                  if (Arrays.equals((Object[])arrayOfString1, (Object[])paramObject))
                                    bool = true; 
                                } 
                              } 
                            } 
                          } 
                        } 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
      return bool;
    } catch (ClassCastException classCastException) {
      return false;
    } 
  }
}
