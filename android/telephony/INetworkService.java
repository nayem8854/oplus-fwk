package android.telephony;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INetworkService extends IInterface {
  void createNetworkServiceProvider(int paramInt) throws RemoteException;
  
  void registerForNetworkRegistrationInfoChanged(int paramInt, INetworkServiceCallback paramINetworkServiceCallback) throws RemoteException;
  
  void removeNetworkServiceProvider(int paramInt) throws RemoteException;
  
  void requestNetworkRegistrationInfo(int paramInt1, int paramInt2, INetworkServiceCallback paramINetworkServiceCallback) throws RemoteException;
  
  void unregisterForNetworkRegistrationInfoChanged(int paramInt, INetworkServiceCallback paramINetworkServiceCallback) throws RemoteException;
  
  class Default implements INetworkService {
    public void createNetworkServiceProvider(int param1Int) throws RemoteException {}
    
    public void removeNetworkServiceProvider(int param1Int) throws RemoteException {}
    
    public void requestNetworkRegistrationInfo(int param1Int1, int param1Int2, INetworkServiceCallback param1INetworkServiceCallback) throws RemoteException {}
    
    public void registerForNetworkRegistrationInfoChanged(int param1Int, INetworkServiceCallback param1INetworkServiceCallback) throws RemoteException {}
    
    public void unregisterForNetworkRegistrationInfoChanged(int param1Int, INetworkServiceCallback param1INetworkServiceCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INetworkService {
    private static final String DESCRIPTOR = "android.telephony.INetworkService";
    
    static final int TRANSACTION_createNetworkServiceProvider = 1;
    
    static final int TRANSACTION_registerForNetworkRegistrationInfoChanged = 4;
    
    static final int TRANSACTION_removeNetworkServiceProvider = 2;
    
    static final int TRANSACTION_requestNetworkRegistrationInfo = 3;
    
    static final int TRANSACTION_unregisterForNetworkRegistrationInfoChanged = 5;
    
    public Stub() {
      attachInterface(this, "android.telephony.INetworkService");
    }
    
    public static INetworkService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.INetworkService");
      if (iInterface != null && iInterface instanceof INetworkService)
        return (INetworkService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "unregisterForNetworkRegistrationInfoChanged";
            } 
            return "registerForNetworkRegistrationInfoChanged";
          } 
          return "requestNetworkRegistrationInfo";
        } 
        return "removeNetworkServiceProvider";
      } 
      return "createNetworkServiceProvider";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      INetworkServiceCallback iNetworkServiceCallback;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.telephony.INetworkService");
                return true;
              } 
              param1Parcel1.enforceInterface("android.telephony.INetworkService");
              param1Int1 = param1Parcel1.readInt();
              iNetworkServiceCallback = INetworkServiceCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
              unregisterForNetworkRegistrationInfoChanged(param1Int1, iNetworkServiceCallback);
              return true;
            } 
            iNetworkServiceCallback.enforceInterface("android.telephony.INetworkService");
            param1Int1 = iNetworkServiceCallback.readInt();
            iNetworkServiceCallback = INetworkServiceCallback.Stub.asInterface(iNetworkServiceCallback.readStrongBinder());
            registerForNetworkRegistrationInfoChanged(param1Int1, iNetworkServiceCallback);
            return true;
          } 
          iNetworkServiceCallback.enforceInterface("android.telephony.INetworkService");
          param1Int1 = iNetworkServiceCallback.readInt();
          param1Int2 = iNetworkServiceCallback.readInt();
          iNetworkServiceCallback = INetworkServiceCallback.Stub.asInterface(iNetworkServiceCallback.readStrongBinder());
          requestNetworkRegistrationInfo(param1Int1, param1Int2, iNetworkServiceCallback);
          return true;
        } 
        iNetworkServiceCallback.enforceInterface("android.telephony.INetworkService");
        param1Int1 = iNetworkServiceCallback.readInt();
        removeNetworkServiceProvider(param1Int1);
        return true;
      } 
      iNetworkServiceCallback.enforceInterface("android.telephony.INetworkService");
      param1Int1 = iNetworkServiceCallback.readInt();
      createNetworkServiceProvider(param1Int1);
      return true;
    }
    
    private static class Proxy implements INetworkService {
      public static INetworkService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.INetworkService";
      }
      
      public void createNetworkServiceProvider(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.INetworkService");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && INetworkService.Stub.getDefaultImpl() != null) {
            INetworkService.Stub.getDefaultImpl().createNetworkServiceProvider(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeNetworkServiceProvider(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.INetworkService");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && INetworkService.Stub.getDefaultImpl() != null) {
            INetworkService.Stub.getDefaultImpl().removeNetworkServiceProvider(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestNetworkRegistrationInfo(int param2Int1, int param2Int2, INetworkServiceCallback param2INetworkServiceCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.INetworkService");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2INetworkServiceCallback != null) {
            iBinder = param2INetworkServiceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && INetworkService.Stub.getDefaultImpl() != null) {
            INetworkService.Stub.getDefaultImpl().requestNetworkRegistrationInfo(param2Int1, param2Int2, param2INetworkServiceCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void registerForNetworkRegistrationInfoChanged(int param2Int, INetworkServiceCallback param2INetworkServiceCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.INetworkService");
          parcel.writeInt(param2Int);
          if (param2INetworkServiceCallback != null) {
            iBinder = param2INetworkServiceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && INetworkService.Stub.getDefaultImpl() != null) {
            INetworkService.Stub.getDefaultImpl().registerForNetworkRegistrationInfoChanged(param2Int, param2INetworkServiceCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void unregisterForNetworkRegistrationInfoChanged(int param2Int, INetworkServiceCallback param2INetworkServiceCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.telephony.INetworkService");
          parcel.writeInt(param2Int);
          if (param2INetworkServiceCallback != null) {
            iBinder = param2INetworkServiceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && INetworkService.Stub.getDefaultImpl() != null) {
            INetworkService.Stub.getDefaultImpl().unregisterForNetworkRegistrationInfoChanged(param2Int, param2INetworkServiceCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INetworkService param1INetworkService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INetworkService != null) {
          Proxy.sDefaultImpl = param1INetworkService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INetworkService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
