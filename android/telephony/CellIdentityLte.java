package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.util.ArraySet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public final class CellIdentityLte extends CellIdentity {
  public static final Parcelable.Creator<CellIdentityLte> CREATOR;
  
  private static final boolean DBG = false;
  
  private static final int MAX_BANDWIDTH = 20000;
  
  private static final int MAX_CI = 268435455;
  
  private static final int MAX_EARFCN = 262143;
  
  private static final int MAX_PCI = 503;
  
  private static final int MAX_TAC = 65535;
  
  private static final String TAG = CellIdentityLte.class.getSimpleName();
  
  private final ArraySet<String> mAdditionalPlmns;
  
  private final int[] mBands;
  
  private final int mBandwidth;
  
  private final int mCi;
  
  private ClosedSubscriberGroupInfo mCsgInfo;
  
  private final int mEarfcn;
  
  private final int mPci;
  
  private final int mTac;
  
  public CellIdentityLte() {
    super(TAG, 3, null, null, null, null);
    this.mCi = Integer.MAX_VALUE;
    this.mPci = Integer.MAX_VALUE;
    this.mTac = Integer.MAX_VALUE;
    this.mEarfcn = Integer.MAX_VALUE;
    this.mBands = new int[0];
    this.mBandwidth = Integer.MAX_VALUE;
    this.mAdditionalPlmns = new ArraySet<>();
    this.mCsgInfo = null;
    this.mGlobalCellId = null;
  }
  
  public CellIdentityLte(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this(paramInt3, paramInt4, paramInt5, 2147483647, new int[0], 2147483647, String.valueOf(paramInt1), String.valueOf(paramInt2), (String)null, (String)null, arraySet, (ClosedSubscriberGroupInfo)null);
  }
  
  public CellIdentityLte(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfint, int paramInt5, String paramString1, String paramString2, String paramString3, String paramString4, Collection<String> paramCollection, ClosedSubscriberGroupInfo paramClosedSubscriberGroupInfo) {
    super(TAG, 3, paramString1, paramString2, paramString3, paramString4);
    this.mCi = inRangeOrUnavailable(paramInt1, 0, 268435455);
    this.mPci = inRangeOrUnavailable(paramInt2, 0, 503);
    this.mTac = inRangeOrUnavailable(paramInt3, 0, 65535);
    this.mEarfcn = inRangeOrUnavailable(paramInt4, 0, 262143);
    this.mBands = paramArrayOfint;
    this.mBandwidth = inRangeOrUnavailable(paramInt5, 0, 20000);
    this.mAdditionalPlmns = new ArraySet<>(paramCollection.size());
    for (String str : paramCollection) {
      if (isValidPlmn(str))
        this.mAdditionalPlmns.add(str); 
    } 
    this.mCsgInfo = paramClosedSubscriberGroupInfo;
    updateGlobalCellId();
  }
  
  public CellIdentityLte(android.hardware.radio.V1_0.CellIdentityLte paramCellIdentityLte) {
    this(i, j, k, m, new int[0], 2147483647, str1, str2, "", "", arraySet, (ClosedSubscriberGroupInfo)null);
  }
  
  public CellIdentityLte(android.hardware.radio.V1_2.CellIdentityLte paramCellIdentityLte) {
    this(i, j, k, m, new int[0], n, str2, str3, str4, str1, arraySet, (ClosedSubscriberGroupInfo)null);
  }
  
  public CellIdentityLte(android.hardware.radio.V1_5.CellIdentityLte paramCellIdentityLte) {
    this(i, j, k, m, arrayOfInt, n, str1, str2, str3, str4, arrayList1, (ClosedSubscriberGroupInfo)paramCellIdentityLte);
  }
  
  private CellIdentityLte(CellIdentityLte paramCellIdentityLte) {
    this(paramCellIdentityLte.mCi, paramCellIdentityLte.mPci, paramCellIdentityLte.mTac, paramCellIdentityLte.mEarfcn, paramCellIdentityLte.mBands, paramCellIdentityLte.mBandwidth, paramCellIdentityLte.mMccStr, paramCellIdentityLte.mMncStr, paramCellIdentityLte.mAlphaLong, paramCellIdentityLte.mAlphaShort, paramCellIdentityLte.mAdditionalPlmns, paramCellIdentityLte.mCsgInfo);
  }
  
  public CellIdentityLte sanitizeLocationInfo() {
    return new CellIdentityLte(2147483647, 2147483647, 2147483647, 2147483647, this.mBands, 2147483647, this.mMccStr, this.mMncStr, this.mAlphaLong, this.mAlphaShort, this.mAdditionalPlmns, null);
  }
  
  CellIdentityLte copy() {
    return new CellIdentityLte(this);
  }
  
  protected void updateGlobalCellId() {
    this.mGlobalCellId = null;
    String str = getPlmn();
    if (str == null)
      return; 
    if (this.mCi == Integer.MAX_VALUE)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str);
    stringBuilder.append(String.format("%07x", new Object[] { Integer.valueOf(this.mCi) }));
    this.mGlobalCellId = stringBuilder.toString();
  }
  
  @Deprecated
  public int getMcc() {
    int i;
    if (this.mMccStr != null) {
      i = Integer.valueOf(this.mMccStr).intValue();
    } else {
      i = Integer.MAX_VALUE;
    } 
    return i;
  }
  
  @Deprecated
  public int getMnc() {
    int i;
    if (this.mMncStr != null) {
      i = Integer.valueOf(this.mMncStr).intValue();
    } else {
      i = Integer.MAX_VALUE;
    } 
    return i;
  }
  
  public int getCi() {
    return this.mCi;
  }
  
  public int getPci() {
    return this.mPci;
  }
  
  public int getTac() {
    return this.mTac;
  }
  
  public int getEarfcn() {
    return this.mEarfcn;
  }
  
  public int[] getBands() {
    int[] arrayOfInt = this.mBands;
    return Arrays.copyOf(arrayOfInt, arrayOfInt.length);
  }
  
  public int getBandwidth() {
    return this.mBandwidth;
  }
  
  public String getMccString() {
    return this.mMccStr;
  }
  
  public String getMncString() {
    return this.mMncStr;
  }
  
  public String getMobileNetworkOperator() {
    if (this.mMccStr == null || this.mMncStr == null)
      return null; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.mMccStr);
    stringBuilder.append(this.mMncStr);
    return stringBuilder.toString();
  }
  
  public int getChannelNumber() {
    return this.mEarfcn;
  }
  
  public Set<String> getAdditionalPlmns() {
    return Collections.unmodifiableSet(this.mAdditionalPlmns);
  }
  
  public ClosedSubscriberGroupInfo getClosedSubscriberGroupInfo() {
    return this.mCsgInfo;
  }
  
  public GsmCellLocation asCellLocation() {
    GsmCellLocation gsmCellLocation = new GsmCellLocation();
    int i = this.mTac, j = -1;
    if (i == Integer.MAX_VALUE)
      i = -1; 
    int k = this.mCi;
    if (k != Integer.MAX_VALUE)
      j = k; 
    gsmCellLocation.setLacAndCid(i, j);
    gsmCellLocation.setPsc(0);
    return gsmCellLocation;
  }
  
  public int hashCode() {
    int i = this.mCi, j = this.mPci, k = this.mTac, m = this.mEarfcn, n = Arrays.hashCode(this.mBands), i1 = this.mBandwidth;
    int i2 = this.mAdditionalPlmns.hashCode();
    ClosedSubscriberGroupInfo closedSubscriberGroupInfo = this.mCsgInfo;
    int i3 = super.hashCode();
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), Integer.valueOf(i2), closedSubscriberGroupInfo, Integer.valueOf(i3) });
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof CellIdentityLte))
      return false; 
    CellIdentityLte cellIdentityLte = (CellIdentityLte)paramObject;
    if (this.mCi == cellIdentityLte.mCi && this.mPci == cellIdentityLte.mPci && this.mTac == cellIdentityLte.mTac && this.mEarfcn == cellIdentityLte.mEarfcn) {
      int[] arrayOfInt1 = this.mBands, arrayOfInt2 = cellIdentityLte.mBands;
      if (Arrays.equals(arrayOfInt1, arrayOfInt2) && this.mBandwidth == cellIdentityLte.mBandwidth) {
        String str1 = this.mMccStr, str2 = cellIdentityLte.mMccStr;
        if (TextUtils.equals(str1, str2)) {
          str2 = this.mMncStr;
          str1 = cellIdentityLte.mMncStr;
          if (TextUtils.equals(str2, str1)) {
            ArraySet<String> arraySet1 = this.mAdditionalPlmns, arraySet2 = cellIdentityLte.mAdditionalPlmns;
            if (arraySet1.equals(arraySet2)) {
              ClosedSubscriberGroupInfo closedSubscriberGroupInfo2 = this.mCsgInfo, closedSubscriberGroupInfo1 = cellIdentityLte.mCsgInfo;
              if (Objects.equals(closedSubscriberGroupInfo2, closedSubscriberGroupInfo1) && 
                super.equals(paramObject))
                return null; 
            } 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(TAG);
    stringBuilder.append(":{ mCi=");
    stringBuilder.append(this.mCi);
    stringBuilder.append(" mPci=");
    stringBuilder.append(this.mPci);
    stringBuilder.append(" mTac=");
    stringBuilder.append(this.mTac);
    stringBuilder.append(" mEarfcn=");
    stringBuilder.append(this.mEarfcn);
    stringBuilder.append(" mBands=");
    stringBuilder.append(Arrays.toString(this.mBands));
    stringBuilder.append(" mBandwidth=");
    stringBuilder.append(this.mBandwidth);
    stringBuilder.append(" mMcc=");
    stringBuilder.append(this.mMccStr);
    stringBuilder.append(" mMnc=");
    stringBuilder.append(this.mMncStr);
    stringBuilder.append(" mAlphaLong=");
    stringBuilder.append(this.mAlphaLong);
    stringBuilder.append(" mAlphaShort=");
    stringBuilder.append(this.mAlphaShort);
    stringBuilder.append(" mAdditionalPlmns=");
    stringBuilder.append(this.mAdditionalPlmns);
    stringBuilder.append(" mCsgInfo=");
    stringBuilder.append(this.mCsgInfo);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    super.writeToParcel(paramParcel, 3);
    paramParcel.writeInt(this.mCi);
    paramParcel.writeInt(this.mPci);
    paramParcel.writeInt(this.mTac);
    paramParcel.writeInt(this.mEarfcn);
    paramParcel.writeIntArray(this.mBands);
    paramParcel.writeInt(this.mBandwidth);
    paramParcel.writeArraySet(this.mAdditionalPlmns);
    paramParcel.writeParcelable(this.mCsgInfo, paramInt);
  }
  
  private CellIdentityLte(Parcel paramParcel) {
    super(TAG, 3, paramParcel);
    this.mCi = paramParcel.readInt();
    this.mPci = paramParcel.readInt();
    this.mTac = paramParcel.readInt();
    this.mEarfcn = paramParcel.readInt();
    this.mBands = paramParcel.createIntArray();
    this.mBandwidth = paramParcel.readInt();
    this.mAdditionalPlmns = paramParcel.readArraySet(null);
    this.mCsgInfo = (ClosedSubscriberGroupInfo)paramParcel.readParcelable(null);
    updateGlobalCellId();
  }
  
  static {
    CREATOR = (Parcelable.Creator<CellIdentityLte>)new Object();
  }
  
  protected static CellIdentityLte createFromParcelBody(Parcel paramParcel) {
    return new CellIdentityLte(paramParcel);
  }
}
