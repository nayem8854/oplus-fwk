package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;

public final class NetworkScanRequest implements Parcelable {
  public NetworkScanRequest(int paramInt1, RadioAccessSpecifier[] paramArrayOfRadioAccessSpecifier, int paramInt2, int paramInt3, boolean paramBoolean, int paramInt4, ArrayList<String> paramArrayList) {
    this.mScanType = paramInt1;
    if (paramArrayOfRadioAccessSpecifier != null) {
      this.mSpecifiers = (RadioAccessSpecifier[])paramArrayOfRadioAccessSpecifier.clone();
    } else {
      this.mSpecifiers = null;
    } 
    this.mSearchPeriodicity = paramInt2;
    this.mMaxSearchTime = paramInt3;
    this.mIncrementalResults = paramBoolean;
    this.mIncrementalResultsPeriodicity = paramInt4;
    if (paramArrayList != null) {
      this.mMccMncs = (ArrayList<String>)paramArrayList.clone();
    } else {
      this.mMccMncs = new ArrayList<>();
    } 
  }
  
  public int getScanType() {
    return this.mScanType;
  }
  
  public int getSearchPeriodicity() {
    return this.mSearchPeriodicity;
  }
  
  public int getMaxSearchTime() {
    return this.mMaxSearchTime;
  }
  
  public boolean getIncrementalResults() {
    return this.mIncrementalResults;
  }
  
  public int getIncrementalResultsPeriodicity() {
    return this.mIncrementalResultsPeriodicity;
  }
  
  public RadioAccessSpecifier[] getSpecifiers() {
    RadioAccessSpecifier[] arrayOfRadioAccessSpecifier = this.mSpecifiers;
    if (arrayOfRadioAccessSpecifier == null) {
      arrayOfRadioAccessSpecifier = null;
    } else {
      arrayOfRadioAccessSpecifier = (RadioAccessSpecifier[])arrayOfRadioAccessSpecifier.clone();
    } 
    return arrayOfRadioAccessSpecifier;
  }
  
  public ArrayList<String> getPlmns() {
    return (ArrayList<String>)this.mMccMncs.clone();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mScanType);
    paramParcel.writeParcelableArray((Parcelable[])this.mSpecifiers, paramInt);
    paramParcel.writeInt(this.mSearchPeriodicity);
    paramParcel.writeInt(this.mMaxSearchTime);
    paramParcel.writeBoolean(this.mIncrementalResults);
    paramParcel.writeInt(this.mIncrementalResultsPeriodicity);
    paramParcel.writeStringList(this.mMccMncs);
  }
  
  private NetworkScanRequest(Parcel paramParcel) {
    this.mScanType = paramParcel.readInt();
    Parcelable[] arrayOfParcelable = paramParcel.readParcelableArray(Object.class.getClassLoader());
    if (arrayOfParcelable != null) {
      this.mSpecifiers = new RadioAccessSpecifier[arrayOfParcelable.length];
      for (byte b = 0; b < arrayOfParcelable.length; b++)
        this.mSpecifiers[b] = (RadioAccessSpecifier)arrayOfParcelable[b]; 
    } else {
      this.mSpecifiers = null;
    } 
    this.mSearchPeriodicity = paramParcel.readInt();
    this.mMaxSearchTime = paramParcel.readInt();
    this.mIncrementalResults = paramParcel.readBoolean();
    this.mIncrementalResultsPeriodicity = paramParcel.readInt();
    ArrayList<String> arrayList = new ArrayList();
    paramParcel.readStringList(arrayList);
  }
  
  public boolean equals(Object<String> paramObject) {
    boolean bool = false;
    try {
      NetworkScanRequest networkScanRequest = (NetworkScanRequest)paramObject;
      if (paramObject == null)
        return false; 
      if (this.mScanType == networkScanRequest.mScanType) {
        paramObject = (Object<String>)this.mSpecifiers;
        RadioAccessSpecifier[] arrayOfRadioAccessSpecifier = networkScanRequest.mSpecifiers;
        if (Arrays.equals((Object[])paramObject, (Object[])arrayOfRadioAccessSpecifier) && this.mSearchPeriodicity == networkScanRequest.mSearchPeriodicity && this.mMaxSearchTime == networkScanRequest.mMaxSearchTime && this.mIncrementalResults == networkScanRequest.mIncrementalResults && this.mIncrementalResultsPeriodicity == networkScanRequest.mIncrementalResultsPeriodicity) {
          paramObject = (Object<String>)this.mMccMncs;
          if (paramObject != null) {
            ArrayList<String> arrayList = networkScanRequest.mMccMncs;
            if (paramObject.equals(arrayList))
              bool = true; 
          } 
        } 
      } 
      return bool;
    } catch (ClassCastException classCastException) {
      return false;
    } 
  }
  
  public int hashCode() {
    int i = this.mScanType;
    RadioAccessSpecifier[] arrayOfRadioAccessSpecifier = this.mSpecifiers;
    int j = Arrays.hashCode((Object[])arrayOfRadioAccessSpecifier), k = this.mSearchPeriodicity, m = this.mMaxSearchTime;
    boolean bool = this.mIncrementalResults;
    byte b = 1;
    if (bool != true)
      b = 0; 
    int n = this.mIncrementalResultsPeriodicity;
    ArrayList<String> arrayList = this.mMccMncs;
    int i1 = arrayList.hashCode();
    return i * 31 + j * 37 + k * 41 + m * 43 + b * 47 + n * 53 + i1 * 59;
  }
  
  public static final Parcelable.Creator<NetworkScanRequest> CREATOR = new Parcelable.Creator<NetworkScanRequest>() {
      public NetworkScanRequest createFromParcel(Parcel param1Parcel) {
        return new NetworkScanRequest(param1Parcel);
      }
      
      public NetworkScanRequest[] newArray(int param1Int) {
        return new NetworkScanRequest[param1Int];
      }
    };
  
  public static final int MAX_BANDS = 8;
  
  public static final int MAX_CHANNELS = 32;
  
  public static final int MAX_INCREMENTAL_PERIODICITY_SEC = 10;
  
  public static final int MAX_MCC_MNC_LIST_SIZE = 20;
  
  public static final int MAX_RADIO_ACCESS_NETWORKS = 8;
  
  public static final int MAX_SEARCH_MAX_SEC = 3600;
  
  public static final int MAX_SEARCH_PERIODICITY_SEC = 300;
  
  public static final int MIN_INCREMENTAL_PERIODICITY_SEC = 1;
  
  public static final int MIN_SEARCH_MAX_SEC = 60;
  
  public static final int MIN_SEARCH_PERIODICITY_SEC = 5;
  
  public static final int SCAN_TYPE_ONE_SHOT = 0;
  
  public static final int SCAN_TYPE_PERIODIC = 1;
  
  private boolean mIncrementalResults;
  
  private int mIncrementalResultsPeriodicity;
  
  private int mMaxSearchTime;
  
  private ArrayList<String> mMccMncs;
  
  private int mScanType;
  
  private int mSearchPeriodicity;
  
  private RadioAccessSpecifier[] mSpecifiers;
  
  @Retention(RetentionPolicy.SOURCE)
  class ScanType implements Annotation {}
}
