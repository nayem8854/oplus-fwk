package android.telephony;

import android.os.PersistableBundle;
import android.util.Log;

public class OplusCarrierConfigManager {
  public static final String KEY_FR1_SIG_INT_ARRAY = "carrier_volte_fr1_int_array";
  
  public static final String KEY_FR1_SWITCH_BOOL = "carrier_volte_fr1_bool";
  
  public static final String KEY_SUPPORT_1X_INCALL_MMI = "support_1x_incall_mmi";
  
  private static final String LOG_TAG = "OplusCarrierConfigManager";
  
  public static final String OPLUS_DUAL_LTE_AVAILABLE = "config_oppo_dual_lte_available_bool";
  
  public static void putDefault(PersistableBundle paramPersistableBundle) {
    if (paramPersistableBundle == null) {
      Log.d("OplusCarrierConfigManager", "setPersistableBundleDefault null");
      return;
    } 
    paramPersistableBundle.putBoolean("carrier_volte_fr1_bool", false);
    paramPersistableBundle.putIntArray("carrier_volte_fr1_int_array", new int[] { -999, -999, -999 });
    paramPersistableBundle.putBoolean("support_1x_incall_mmi", false);
    paramPersistableBundle.putBoolean("config_oppo_dual_lte_available_bool", true);
    paramPersistableBundle.putStringArray("carrier_metered_apn_types_strings", new String[] { "default", "dun", "supl" });
  }
}
