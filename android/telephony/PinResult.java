package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.util.Objects;

public final class PinResult implements Parcelable {
  public static final Parcelable.Creator<PinResult> CREATOR;
  
  public static final int PIN_RESULT_TYPE_FAILURE = 2;
  
  public static final int PIN_RESULT_TYPE_INCORRECT = 1;
  
  public static final int PIN_RESULT_TYPE_SUCCESS = 0;
  
  private static final PinResult sFailedResult = new PinResult(2, -1);
  
  private final int mAttemptsRemaining;
  
  private final int mType;
  
  public int getType() {
    return this.mType;
  }
  
  public int getAttemptsRemaining() {
    return this.mAttemptsRemaining;
  }
  
  public static PinResult getDefaultFailedResult() {
    return sFailedResult;
  }
  
  public PinResult(int paramInt1, int paramInt2) {
    this.mType = paramInt1;
    this.mAttemptsRemaining = paramInt2;
  }
  
  private PinResult(Parcel paramParcel) {
    this.mType = paramParcel.readInt();
    this.mAttemptsRemaining = paramParcel.readInt();
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("type: ");
    stringBuilder.append(getType());
    stringBuilder.append(", attempts remaining: ");
    stringBuilder.append(getAttemptsRemaining());
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mType);
    paramParcel.writeInt(this.mAttemptsRemaining);
  }
  
  static {
    CREATOR = new Parcelable.Creator<PinResult>() {
        public PinResult createFromParcel(Parcel param1Parcel) {
          return new PinResult(param1Parcel);
        }
        
        public PinResult[] newArray(int param1Int) {
          return new PinResult[param1Int];
        }
      };
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mAttemptsRemaining), Integer.valueOf(this.mType) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mType != ((PinResult)paramObject).mType || this.mAttemptsRemaining != ((PinResult)paramObject).mAttemptsRemaining)
      bool = false; 
    return bool;
  }
  
  class PinResultType implements Annotation {}
}
