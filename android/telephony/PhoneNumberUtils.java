package android.telephony;

import android.annotation.SystemApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.sysprop.TelephonyProperties;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.TtsSpan;
import android.util.SparseIntArray;
import com.android.i18n.phonenumbers.NumberParseException;
import com.android.i18n.phonenumbers.PhoneNumberUtil;
import com.android.i18n.phonenumbers.Phonenumber;
import com.android.telephony.Rlog;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumberUtils {
  private static final String BCD_CALLED_PARTY_EXTENDED = "*#abc";
  
  private static final String BCD_EF_ADN_EXTENDED = "*#,N;";
  
  public static final int BCD_EXTENDED_TYPE_CALLED_PARTY = 2;
  
  public static final int BCD_EXTENDED_TYPE_EF_ADN = 1;
  
  private static final int CCC_LENGTH;
  
  private static final String CLIR_OFF = "#31#";
  
  private static final String CLIR_ON = "*31#";
  
  private static final boolean[] COUNTRY_CALLING_CALL;
  
  private static final boolean DBG = false;
  
  public static final int FORMAT_JAPAN = 2;
  
  public static final int FORMAT_NANP = 1;
  
  public static final int FORMAT_UNKNOWN = 0;
  
  private static final Pattern GLOBAL_PHONE_NUMBER_PATTERN = Pattern.compile("[\\+]?[0-9.-]+");
  
  private static final String JAPAN_ISO_COUNTRY_CODE = "JP";
  
  private static final SparseIntArray KEYPAD_MAP;
  
  private static final String KOREA_ISO_COUNTRY_CODE = "KR";
  
  static final String LOG_TAG = "PhoneNumberUtils";
  
  private static final String[] NANP_COUNTRIES;
  
  private static final String NANP_IDP_STRING = "011";
  
  private static final int NANP_LENGTH = 10;
  
  private static final int NANP_STATE_DASH = 4;
  
  private static final int NANP_STATE_DIGIT = 1;
  
  private static final int NANP_STATE_ONE = 3;
  
  private static final int NANP_STATE_PLUS = 2;
  
  public static final char PAUSE = ',';
  
  private static final char PLUS_SIGN_CHAR = '+';
  
  private static final String PLUS_SIGN_STRING = "+";
  
  public static final int TOA_International = 145;
  
  public static final int TOA_Unknown = 129;
  
  public static final char WAIT = ';';
  
  public static final char WILD = 'N';
  
  private static String[] sConvertToEmergencyMap;
  
  public static boolean isISODigit(char paramChar) {
    boolean bool;
    if (paramChar >= '0' && paramChar <= '9') {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static final boolean is12Key(char paramChar) {
    return ((paramChar >= '0' && paramChar <= '9') || paramChar == '*' || paramChar == '#');
  }
  
  public static final boolean isDialable(char paramChar) {
    return ((paramChar >= '0' && paramChar <= '9') || paramChar == '*' || paramChar == '#' || paramChar == '+' || paramChar == 'N');
  }
  
  public static final boolean isReallyDialable(char paramChar) {
    return ((paramChar >= '0' && paramChar <= '9') || paramChar == '*' || paramChar == '#' || paramChar == '+');
  }
  
  public static final boolean isNonSeparator(char paramChar) {
    return ((paramChar >= '0' && paramChar <= '9') || paramChar == '*' || paramChar == '#' || paramChar == '+' || paramChar == 'N' || paramChar == ';' || paramChar == ',');
  }
  
  public static final boolean isStartsPostDial(char paramChar) {
    return (paramChar == ',' || paramChar == ';');
  }
  
  private static boolean isPause(char paramChar) {
    return (paramChar == 'p' || paramChar == 'P');
  }
  
  private static boolean isToneWait(char paramChar) {
    return (paramChar == 'w' || paramChar == 'W');
  }
  
  private static int sMinMatch = 0;
  
  private static int getMinMatch() {
    if (sMinMatch == 0)
      sMinMatch = Resources.getSystem().getInteger(17694878); 
    return sMinMatch;
  }
  
  public static int getMinMatchForTest() {
    return getMinMatch();
  }
  
  public static void setMinMatchForTest(int paramInt) {
    sMinMatch = paramInt;
  }
  
  private static boolean isSeparator(char paramChar) {
    boolean bool;
    if (!isDialable(paramChar) && ('a' > paramChar || paramChar > 'z') && ('A' > paramChar || paramChar > 'Z')) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static String getNumberFromIntent(Intent paramIntent, Context paramContext) {
    Cursor cursor;
    String str4, str2 = OplusOSPhoneNumberUtils.getNumberFromIntent(paramIntent, paramContext);
    if (str2 != null)
      return str2; 
    Uri uri = paramIntent.getData();
    if (uri == null)
      return null; 
    String str3 = uri.getScheme();
    if (str3 == null)
      return null; 
    if (str3.equals("tel") || str3.equals("sip"))
      return uri.getSchemeSpecificPart(); 
    if (paramContext == null)
      return null; 
    paramIntent.resolveType(paramContext);
    String str1 = uri.getAuthority();
    if ("contacts".equals(str1)) {
      str4 = "number";
    } else if ("com.android.contacts".equals(str1)) {
      str4 = "data1";
    } else {
      str4 = null;
    } 
    str1 = null;
    str3 = null;
    try {
      Cursor cursor1 = paramContext.getContentResolver().query(uri, new String[] { str4 }, null, null, null);
      String str = str2;
      if (cursor1 != null) {
        str = str2;
        Cursor cursor2 = cursor1;
        cursor = cursor1;
        if (cursor1.moveToFirst()) {
          cursor2 = cursor1;
          cursor = cursor1;
          str = cursor1.getString(cursor1.getColumnIndex(str4));
        } 
      } 
      str3 = str;
      if (cursor1 != null) {
        cursor = cursor1;
      } else {
        return str3;
      } 
      cursor.close();
      str3 = str;
    } catch (RuntimeException runtimeException) {
      String str;
      Cursor cursor1 = cursor;
      Rlog.e("PhoneNumberUtils", "Error getting phone number.", runtimeException);
      str3 = str2;
      if (cursor != null) {
        str = str2;
      } else {
        return str3;
      } 
      cursor.close();
      str3 = str;
    } finally {}
    return str3;
  }
  
  public static String extractNetworkPortion(String paramString) {
    if (paramString == null)
      return null; 
    int i = paramString.length();
    StringBuilder stringBuilder = new StringBuilder(i);
    for (byte b = 0; b < i; b++) {
      char c = paramString.charAt(b);
      int j = Character.digit(c, 10);
      if (j != -1) {
        stringBuilder.append(j);
      } else if (c == '+') {
        String str = stringBuilder.toString();
        if (str.length() == 0 || str.equals("*31#") || str.equals("#31#"))
          stringBuilder.append(c); 
      } else if (isDialable(c)) {
        stringBuilder.append(c);
      } else if (isStartsPostDial(c)) {
        break;
      } 
    } 
    return stringBuilder.toString();
  }
  
  public static String extractNetworkPortionAlt(String paramString) {
    if (paramString == null)
      return null; 
    int i = paramString.length();
    StringBuilder stringBuilder = new StringBuilder(i);
    boolean bool = false;
    for (byte b = 0; b < i; b++) {
      char c = paramString.charAt(b);
      boolean bool1 = bool;
      if (c == '+') {
        if (bool)
          continue; 
        bool1 = true;
      } 
      if (isDialable(c)) {
        stringBuilder.append(c);
        bool = bool1;
      } else {
        bool = bool1;
        if (isStartsPostDial(c))
          break; 
      } 
      continue;
    } 
    return stringBuilder.toString();
  }
  
  public static String stripSeparators(String paramString) {
    if (paramString == null)
      return null; 
    int i = paramString.length();
    StringBuilder stringBuilder = new StringBuilder(i);
    for (byte b = 0; b < i; b++) {
      char c = paramString.charAt(b);
      int j = Character.digit(c, 10);
      if (j != -1) {
        stringBuilder.append(j);
      } else if (isNonSeparator(c)) {
        stringBuilder.append(c);
      } 
    } 
    return stringBuilder.toString();
  }
  
  public static String convertAndStrip(String paramString) {
    return stripSeparators(convertKeypadLettersToDigits(paramString));
  }
  
  public static String convertPreDial(String paramString) {
    if (paramString == null)
      return null; 
    int i = paramString.length();
    StringBuilder stringBuilder = new StringBuilder(i);
    for (byte b = 0; b < i; b++) {
      char c1;
      char c = paramString.charAt(b);
      if (isPause(c)) {
        c1 = ',';
      } else {
        c1 = c;
        if (isToneWait(c))
          c1 = ';'; 
      } 
      stringBuilder.append(c1);
    } 
    return stringBuilder.toString();
  }
  
  private static int minPositive(int paramInt1, int paramInt2) {
    if (paramInt1 >= 0 && paramInt2 >= 0) {
      if (paramInt1 >= paramInt2)
        paramInt1 = paramInt2; 
      return paramInt1;
    } 
    if (paramInt1 >= 0)
      return paramInt1; 
    if (paramInt2 >= 0)
      return paramInt2; 
    return -1;
  }
  
  private static void log(String paramString) {
    Rlog.d("PhoneNumberUtils", paramString);
  }
  
  private static int indexOfLastNetworkChar(String paramString) {
    int i = paramString.length();
    int j = paramString.indexOf(',');
    int k = paramString.indexOf(';');
    j = minPositive(j, k);
    if (j < 0)
      return i - 1; 
    return j - 1;
  }
  
  public static String extractPostDialPortion(String paramString) {
    if (paramString == null)
      return null; 
    StringBuilder stringBuilder = new StringBuilder();
    int i = indexOfLastNetworkChar(paramString);
    i++;
    int j = paramString.length();
    for (; i < j; i++) {
      char c = paramString.charAt(i);
      if (isNonSeparator(c))
        stringBuilder.append(c); 
    } 
    return stringBuilder.toString();
  }
  
  public static boolean compare(String paramString1, String paramString2) {
    return compare(paramString1, paramString2, false);
  }
  
  public static boolean compare(Context paramContext, String paramString1, String paramString2) {
    boolean bool = paramContext.getResources().getBoolean(17891581);
    return compare(paramString1, paramString2, bool);
  }
  
  public static boolean compare(String paramString1, String paramString2, boolean paramBoolean) {
    if (paramBoolean) {
      paramBoolean = compareStrictly(paramString1, paramString2);
    } else {
      paramBoolean = compareLoosely(paramString1, paramString2);
    } 
    return paramBoolean;
  }
  
  public static boolean compareLoosely(String paramString1, String paramString2) {
    int i2, i3, i4, i5, i = 0;
    int j = 0;
    int k = getMinMatch();
    boolean bool = false;
    if (paramString1 == null || paramString2 == null) {
      if (paramString1 == paramString2)
        bool = true; 
      return bool;
    } 
    if (paramString1.length() == 0 || paramString2.length() == 0)
      return false; 
    int m = indexOfLastNetworkChar(paramString1);
    int n = indexOfLastNetworkChar(paramString2);
    int i1 = 0;
    while (true) {
      i2 = i;
      i3 = j;
      i4 = m;
      i5 = n;
      if (m >= 0) {
        i2 = i;
        i3 = j;
        i4 = m;
        i5 = n;
        if (n >= 0) {
          i5 = 0;
          char c1 = paramString1.charAt(m);
          i2 = i;
          i4 = m;
          if (!isDialable(c1)) {
            i4 = m - 1;
            i5 = 1;
            i2 = i + 1;
          } 
          char c2 = paramString2.charAt(n);
          i3 = j;
          m = n;
          i = i5;
          if (!isDialable(c2)) {
            m = n - 1;
            i = 1;
            i3 = j + 1;
          } 
          i5 = i4;
          n = m;
          int i6 = i1;
          if (i == 0) {
            if (c2 != c1 && c1 != 'N' && c2 != 'N') {
              i5 = m;
              break;
            } 
            i5 = i4 - 1;
            n = m - 1;
            i6 = i1 + 1;
          } 
          i = i2;
          j = i3;
          m = i5;
          i1 = i6;
          continue;
        } 
      } 
      break;
    } 
    if (i1 < k) {
      n = paramString1.length() - i2;
      i4 = paramString2.length();
      if (n == i4 - i3 && n == i1)
        return true; 
      return false;
    } 
    if (i1 >= k && (i4 < 0 || i5 < 0))
      return true; 
    if (matchIntlPrefix(paramString1, i4 + 1) && 
      matchIntlPrefix(paramString2, i5 + 1))
      return true; 
    if (matchTrunkPrefix(paramString1, i4 + 1) && 
      matchIntlPrefixAndCC(paramString2, i5 + 1))
      return true; 
    if (matchTrunkPrefix(paramString2, i5 + 1) && 
      matchIntlPrefixAndCC(paramString1, i4 + 1))
      return true; 
    return false;
  }
  
  public static boolean compareStrictly(String paramString1, String paramString2) {
    return compareStrictly(paramString1, paramString2, true);
  }
  
  public static boolean compareStrictly(String paramString1, String paramString2, boolean paramBoolean) {
    int i3, i4;
    if (paramString1 == null || paramString2 == null) {
      if (paramString1 == paramString2) {
        paramBoolean = true;
      } else {
        paramBoolean = false;
      } 
      return paramBoolean;
    } 
    if (paramString1.length() == 0 && paramString2.length() == 0)
      return false; 
    int i = 0, j = 0;
    int k = 0;
    CountryCallingCodeAndNewIndex countryCallingCodeAndNewIndex1 = tryGetCountryCallingCodeAndNewIndex(paramString1, paramBoolean);
    CountryCallingCodeAndNewIndex countryCallingCodeAndNewIndex2 = tryGetCountryCallingCodeAndNewIndex(paramString2, paramBoolean);
    int m = 0;
    int n = 1;
    int i1 = 0, i2 = 0;
    boolean bool = false;
    if (countryCallingCodeAndNewIndex1 != null && countryCallingCodeAndNewIndex2 != null) {
      if (countryCallingCodeAndNewIndex1.countryCallingCode != countryCallingCodeAndNewIndex2.countryCallingCode)
        return false; 
      i3 = 0;
      i4 = 1;
      i = countryCallingCodeAndNewIndex1.newIndex;
      k = countryCallingCodeAndNewIndex2.newIndex;
    } else if (countryCallingCodeAndNewIndex1 == null && countryCallingCodeAndNewIndex2 == null) {
      i3 = 0;
      i4 = m;
    } else {
      if (countryCallingCodeAndNewIndex1 != null) {
        j = countryCallingCodeAndNewIndex1.newIndex;
      } else {
        i = tryGetTrunkPrefixOmittedIndex(paramString2, 0);
        if (i >= 0) {
          j = i;
          i2 = 1;
        } 
      } 
      if (countryCallingCodeAndNewIndex2 != null) {
        k = countryCallingCodeAndNewIndex2.newIndex;
        i = j;
        i4 = m;
        i3 = n;
        i1 = i2;
      } else {
        int i5 = tryGetTrunkPrefixOmittedIndex(paramString2, 0);
        i = j;
        i4 = m;
        i3 = n;
        i1 = i2;
        if (i5 >= 0) {
          k = i5;
          bool = true;
          i1 = i2;
          i3 = n;
          i4 = m;
          i = j;
        } 
      } 
    } 
    i2 = paramString1.length() - 1;
    j = paramString2.length() - 1;
    while (i2 >= i && j >= k) {
      n = 0;
      char c1 = paramString1.charAt(i2);
      char c2 = paramString2.charAt(j);
      m = i2;
      if (isSeparator(c1)) {
        m = i2 - 1;
        n = 1;
      } 
      i2 = j;
      int i5 = n;
      if (isSeparator(c2)) {
        i2 = j - 1;
        i5 = 1;
      } 
      n = m;
      j = i2;
      if (i5 == 0) {
        if (c1 != c2)
          return false; 
        n = m - 1;
        j = i2 - 1;
      } 
      i2 = n;
    } 
    if (i3 != 0) {
      if ((i1 != 0 && i <= i2) || 
        !checkPrefixIsIgnorable(paramString1, i, i2)) {
        if (paramBoolean)
          return compare(paramString1, paramString2, false); 
        return false;
      } 
      if ((bool && k <= j) || 
        !checkPrefixIsIgnorable(paramString2, i, j)) {
        if (paramBoolean)
          return compare(paramString1, paramString2, false); 
        return false;
      } 
    } else {
      m = i4 ^ 0x1;
      i3 = i2;
      i2 = m;
      while (true) {
        n = i2;
        m = j;
        if (i3 >= i) {
          char c = paramString1.charAt(i3);
          m = i2;
          if (isDialable(c))
            if (i2 != 0 && tryGetISODigit(c) == 1) {
              m = 0;
            } else {
              return false;
            }  
          i3--;
          i2 = m;
          continue;
        } 
        break;
      } 
      while (m >= k) {
        char c = paramString2.charAt(m);
        if (isDialable(c))
          if (n != 0 && tryGetISODigit(c) == 1) {
            n = 0;
          } else {
            return false;
          }  
        m--;
      } 
    } 
    return true;
  }
  
  public static String toCallerIDMinMatch(String paramString) {
    paramString = extractNetworkPortionAlt(paramString);
    return internalGetStrippedReversed(paramString, getMinMatch());
  }
  
  public static String getStrippedReversed(String paramString) {
    paramString = extractNetworkPortionAlt(paramString);
    if (paramString == null)
      return null; 
    return internalGetStrippedReversed(paramString, paramString.length());
  }
  
  private static String internalGetStrippedReversed(String paramString, int paramInt) {
    if (paramString == null)
      return null; 
    StringBuilder stringBuilder = new StringBuilder(paramInt);
    int i = paramString.length();
    int j = i - 1;
    for (; j >= 0 && i - j <= paramInt; j--) {
      char c = paramString.charAt(j);
      stringBuilder.append(c);
    } 
    return stringBuilder.toString();
  }
  
  public static String stringFromStringAndTOA(String paramString, int paramInt) {
    if (paramString == null)
      return null; 
    if (paramInt == 145 && paramString.length() > 0 && paramString.charAt(0) != '+') {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("+");
      stringBuilder.append(paramString);
      return stringBuilder.toString();
    } 
    return paramString;
  }
  
  public static int toaFromString(String paramString) {
    if (paramString != null && paramString.length() > 0 && paramString.charAt(0) == '+')
      return 145; 
    return 129;
  }
  
  @Deprecated
  public static String calledPartyBCDToString(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    return calledPartyBCDToString(paramArrayOfbyte, paramInt1, paramInt2, 1);
  }
  
  public static String calledPartyBCDToString(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3) {
    boolean bool = false;
    StringBuilder stringBuilder2 = new StringBuilder(paramInt2 * 2 + 1);
    if (paramInt2 < 2)
      return ""; 
    if ((paramArrayOfbyte[paramInt1] & 0xF0) == 144)
      bool = true; 
    internalCalledPartyBCDFragmentToString(stringBuilder2, paramArrayOfbyte, paramInt1 + 1, paramInt2 - 1, paramInt3);
    if (bool && stringBuilder2.length() == 0)
      return ""; 
    StringBuilder stringBuilder1 = stringBuilder2;
    if (bool) {
      String str = stringBuilder2.toString();
      Pattern pattern = Pattern.compile("(^[#*])(.*)([#*])(.*)(#)$");
      Matcher matcher = pattern.matcher(str);
      if (matcher.matches()) {
        if ("".equals(matcher.group(2))) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(matcher.group(1));
          stringBuilder.append(matcher.group(3));
          stringBuilder.append(matcher.group(4));
          stringBuilder.append(matcher.group(5));
          stringBuilder.append("+");
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(matcher.group(1));
          stringBuilder.append(matcher.group(2));
          stringBuilder.append(matcher.group(3));
          stringBuilder.append("+");
          stringBuilder.append(matcher.group(4));
          stringBuilder.append(matcher.group(5));
        } 
      } else {
        pattern = Pattern.compile("(^[#*])(.*)([#*])(.*)");
        matcher = pattern.matcher(str);
        if (matcher.matches()) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(matcher.group(1));
          stringBuilder.append(matcher.group(2));
          stringBuilder.append(matcher.group(3));
          stringBuilder.append("+");
          stringBuilder.append(matcher.group(4));
        } else {
          stringBuilder1 = new StringBuilder();
          stringBuilder1.append('+');
          stringBuilder1.append(str);
        } 
      } 
    } 
    return stringBuilder1.toString();
  }
  
  private static void internalCalledPartyBCDFragmentToString(StringBuilder paramStringBuilder, byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3) {
    for (int i = paramInt1; i < paramInt2 + paramInt1; i++) {
      char c = bcdToChar((byte)(paramArrayOfbyte[i] & 0xF), paramInt3);
      if (c == '\000')
        return; 
      paramStringBuilder.append(c);
      byte b = (byte)(paramArrayOfbyte[i] >> 4 & 0xF);
      if (b == 15 && i + 1 == paramInt2 + paramInt1)
        break; 
      c = bcdToChar(b, paramInt3);
      if (c == '\000')
        return; 
      paramStringBuilder.append(c);
    } 
  }
  
  @Deprecated
  public static String calledPartyBCDFragmentToString(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    return calledPartyBCDFragmentToString(paramArrayOfbyte, paramInt1, paramInt2, 1);
  }
  
  public static String calledPartyBCDFragmentToString(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3) {
    StringBuilder stringBuilder = new StringBuilder(paramInt2 * 2);
    internalCalledPartyBCDFragmentToString(stringBuilder, paramArrayOfbyte, paramInt1, paramInt2, paramInt3);
    return stringBuilder.toString();
  }
  
  private static char bcdToChar(byte paramByte, int paramInt) {
    if (paramByte < 10)
      return (char)(paramByte + 48); 
    String str = null;
    if (1 == paramInt) {
      str = "*#,N;";
    } else if (2 == paramInt) {
      str = "*#abc";
    } 
    if (str == null || paramByte - 10 >= str.length())
      return Character.MIN_VALUE; 
    return str.charAt(paramByte - 10);
  }
  
  private static int charToBCD(char paramChar, int paramInt) {
    if ('0' <= paramChar && paramChar <= '9')
      return paramChar - 48; 
    String str = null;
    if (1 == paramInt) {
      str = "*#,N;";
    } else if (2 == paramInt) {
      str = "*#abc";
    } 
    if (str != null && str.indexOf(paramChar) != -1)
      return str.indexOf(paramChar) + 10; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("invalid char for BCD ");
    stringBuilder.append(paramChar);
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public static boolean isWellFormedSmsAddress(String paramString) {
    boolean bool;
    paramString = extractNetworkPortion(paramString);
    if (!paramString.equals("+") && 
      !TextUtils.isEmpty(paramString) && 
      isDialable(paramString)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isGlobalPhoneNumber(String paramString) {
    if (TextUtils.isEmpty(paramString))
      return false; 
    Matcher matcher = GLOBAL_PHONE_NUMBER_PATTERN.matcher(paramString);
    return matcher.matches();
  }
  
  private static boolean isDialable(String paramString) {
    byte b;
    int i;
    for (b = 0, i = paramString.length(); b < i; b++) {
      if (!isDialable(paramString.charAt(b)))
        return false; 
    } 
    return true;
  }
  
  private static boolean isNonSeparator(String paramString) {
    byte b;
    int i;
    for (b = 0, i = paramString.length(); b < i; b++) {
      if (!isNonSeparator(paramString.charAt(b)))
        return false; 
    } 
    return true;
  }
  
  public static byte[] networkPortionToCalledPartyBCD(String paramString) {
    paramString = extractNetworkPortion(paramString);
    return numberToCalledPartyBCDHelper(paramString, false, 1);
  }
  
  public static byte[] networkPortionToCalledPartyBCDWithLength(String paramString) {
    paramString = extractNetworkPortion(paramString);
    return numberToCalledPartyBCDHelper(paramString, true, 1);
  }
  
  @Deprecated
  public static byte[] numberToCalledPartyBCD(String paramString) {
    return numberToCalledPartyBCD(paramString, 1);
  }
  
  public static byte[] numberToCalledPartyBCD(String paramString, int paramInt) {
    paramString = OplusTelephonyFunction.stripSeparators(paramString);
    if (paramString == null)
      return null; 
    return numberToCalledPartyBCDHelper(paramString, false, paramInt);
  }
  
  private static byte[] numberToCalledPartyBCDHelper(String paramString, boolean paramBoolean, int paramInt) {
    char c;
    int i = paramString.length();
    int j = i;
    if (paramString.indexOf('+') != -1) {
      c = '\001';
    } else {
      c = Character.MIN_VALUE;
    } 
    int k = j;
    if (c)
      k = j - 1; 
    if (k == 0)
      return null; 
    k = (k + 1) / 2;
    j = 1;
    if (paramBoolean)
      j = 1 + 1; 
    int m = k + j;
    byte[] arrayOfByte = new byte[m];
    byte b = 0;
    for (k = 0; k < i; k++) {
      char c1 = paramString.charAt(k);
      if (c1 != '+') {
        byte b1;
        if ((b & 0x1) == 1) {
          b1 = 4;
        } else {
          b1 = 0;
        } 
        int n = (b >> 1) + j;
        byte b2 = arrayOfByte[n];
        arrayOfByte[n] = (byte)((byte)((charToBCD(c1, paramInt) & 0xF) << b1) | b2);
        b++;
      } 
    } 
    if ((b & 0x1) == 1) {
      paramInt = (b >> 1) + j;
      arrayOfByte[paramInt] = (byte)(arrayOfByte[paramInt] | 0xF0);
    } 
    paramInt = 0;
    if (paramBoolean) {
      arrayOfByte[0] = (byte)(m - 1);
      paramInt = 0 + 1;
    } 
    if (c) {
      c = '';
    } else {
      c = '';
    } 
    arrayOfByte[paramInt] = (byte)c;
    return arrayOfByte;
  }
  
  static {
    NANP_COUNTRIES = new String[] { 
        "US", "CA", "AS", "AI", "AG", "BS", "BB", "BM", "VG", "KY", 
        "DM", "DO", "GD", "GU", "JM", "PR", "MS", "MP", "KN", "LC", 
        "VC", "TT", "TC", "VI" };
    SparseIntArray sparseIntArray = new SparseIntArray();
    sparseIntArray.put(97, 50);
    KEYPAD_MAP.put(98, 50);
    KEYPAD_MAP.put(99, 50);
    KEYPAD_MAP.put(65, 50);
    KEYPAD_MAP.put(66, 50);
    KEYPAD_MAP.put(67, 50);
    KEYPAD_MAP.put(100, 51);
    KEYPAD_MAP.put(101, 51);
    KEYPAD_MAP.put(102, 51);
    KEYPAD_MAP.put(68, 51);
    KEYPAD_MAP.put(69, 51);
    KEYPAD_MAP.put(70, 51);
    KEYPAD_MAP.put(103, 52);
    KEYPAD_MAP.put(104, 52);
    KEYPAD_MAP.put(105, 52);
    KEYPAD_MAP.put(71, 52);
    KEYPAD_MAP.put(72, 52);
    KEYPAD_MAP.put(73, 52);
    KEYPAD_MAP.put(106, 53);
    KEYPAD_MAP.put(107, 53);
    KEYPAD_MAP.put(108, 53);
    KEYPAD_MAP.put(74, 53);
    KEYPAD_MAP.put(75, 53);
    KEYPAD_MAP.put(76, 53);
    KEYPAD_MAP.put(109, 54);
    KEYPAD_MAP.put(110, 54);
    KEYPAD_MAP.put(111, 54);
    KEYPAD_MAP.put(77, 54);
    KEYPAD_MAP.put(78, 54);
    KEYPAD_MAP.put(79, 54);
    KEYPAD_MAP.put(112, 55);
    KEYPAD_MAP.put(113, 55);
    KEYPAD_MAP.put(114, 55);
    KEYPAD_MAP.put(115, 55);
    KEYPAD_MAP.put(80, 55);
    KEYPAD_MAP.put(81, 55);
    KEYPAD_MAP.put(82, 55);
    KEYPAD_MAP.put(83, 55);
    KEYPAD_MAP.put(116, 56);
    KEYPAD_MAP.put(117, 56);
    KEYPAD_MAP.put(118, 56);
    KEYPAD_MAP.put(84, 56);
    KEYPAD_MAP.put(85, 56);
    KEYPAD_MAP.put(86, 56);
    KEYPAD_MAP.put(119, 57);
    KEYPAD_MAP.put(120, 57);
    KEYPAD_MAP.put(121, 57);
    KEYPAD_MAP.put(122, 57);
    KEYPAD_MAP.put(87, 57);
    KEYPAD_MAP.put(88, 57);
    KEYPAD_MAP.put(89, 57);
    KEYPAD_MAP.put(90, 57);
    boolean[] arrayOfBoolean = new boolean[100];
    arrayOfBoolean[0] = true;
    arrayOfBoolean[1] = true;
    arrayOfBoolean[2] = false;
    arrayOfBoolean[3] = false;
    arrayOfBoolean[4] = false;
    arrayOfBoolean[5] = false;
    arrayOfBoolean[6] = false;
    arrayOfBoolean[7] = true;
    arrayOfBoolean[8] = false;
    arrayOfBoolean[9] = false;
    arrayOfBoolean[10] = false;
    arrayOfBoolean[11] = false;
    arrayOfBoolean[12] = false;
    arrayOfBoolean[13] = false;
    arrayOfBoolean[14] = false;
    arrayOfBoolean[15] = false;
    arrayOfBoolean[16] = false;
    arrayOfBoolean[17] = false;
    arrayOfBoolean[18] = false;
    arrayOfBoolean[19] = false;
    arrayOfBoolean[20] = true;
    arrayOfBoolean[21] = false;
    arrayOfBoolean[22] = false;
    arrayOfBoolean[23] = false;
    arrayOfBoolean[24] = false;
    arrayOfBoolean[25] = false;
    arrayOfBoolean[26] = false;
    arrayOfBoolean[27] = true;
    arrayOfBoolean[28] = true;
    arrayOfBoolean[29] = false;
    arrayOfBoolean[30] = true;
    arrayOfBoolean[31] = true;
    arrayOfBoolean[32] = true;
    arrayOfBoolean[33] = true;
    arrayOfBoolean[34] = true;
    arrayOfBoolean[35] = false;
    arrayOfBoolean[36] = true;
    arrayOfBoolean[37] = false;
    arrayOfBoolean[38] = false;
    arrayOfBoolean[39] = true;
    arrayOfBoolean[40] = true;
    arrayOfBoolean[41] = false;
    arrayOfBoolean[42] = false;
    arrayOfBoolean[43] = true;
    arrayOfBoolean[44] = true;
    arrayOfBoolean[45] = true;
    arrayOfBoolean[46] = true;
    arrayOfBoolean[47] = true;
    arrayOfBoolean[48] = true;
    arrayOfBoolean[49] = true;
    arrayOfBoolean[50] = false;
    arrayOfBoolean[51] = true;
    arrayOfBoolean[52] = true;
    arrayOfBoolean[53] = true;
    arrayOfBoolean[54] = true;
    arrayOfBoolean[55] = true;
    arrayOfBoolean[56] = true;
    arrayOfBoolean[57] = true;
    arrayOfBoolean[58] = true;
    arrayOfBoolean[59] = false;
    arrayOfBoolean[60] = true;
    arrayOfBoolean[61] = true;
    arrayOfBoolean[62] = true;
    arrayOfBoolean[63] = true;
    arrayOfBoolean[64] = true;
    arrayOfBoolean[65] = true;
    arrayOfBoolean[66] = true;
    arrayOfBoolean[67] = false;
    arrayOfBoolean[68] = false;
    arrayOfBoolean[69] = false;
    arrayOfBoolean[70] = false;
    arrayOfBoolean[71] = false;
    arrayOfBoolean[72] = false;
    arrayOfBoolean[73] = false;
    arrayOfBoolean[74] = false;
    arrayOfBoolean[75] = false;
    arrayOfBoolean[76] = false;
    arrayOfBoolean[77] = false;
    arrayOfBoolean[78] = false;
    arrayOfBoolean[79] = false;
    arrayOfBoolean[80] = false;
    arrayOfBoolean[81] = true;
    arrayOfBoolean[82] = true;
    arrayOfBoolean[83] = true;
    arrayOfBoolean[84] = true;
    arrayOfBoolean[85] = false;
    arrayOfBoolean[86] = true;
    arrayOfBoolean[87] = false;
    arrayOfBoolean[88] = false;
    arrayOfBoolean[89] = true;
    arrayOfBoolean[90] = true;
    arrayOfBoolean[91] = true;
    arrayOfBoolean[92] = true;
    arrayOfBoolean[93] = true;
    arrayOfBoolean[94] = true;
    arrayOfBoolean[95] = true;
    arrayOfBoolean[96] = false;
    arrayOfBoolean[97] = false;
    arrayOfBoolean[98] = true;
    arrayOfBoolean[99] = false;
    COUNTRY_CALLING_CALL = arrayOfBoolean;
    CCC_LENGTH = arrayOfBoolean.length;
    sConvertToEmergencyMap = null;
  }
  
  @Deprecated
  public static String formatNumber(String paramString) {
    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(paramString);
    formatNumber(spannableStringBuilder, getFormatTypeForLocale(Locale.getDefault()));
    return spannableStringBuilder.toString();
  }
  
  @Deprecated
  public static String formatNumber(String paramString, int paramInt) {
    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(paramString);
    formatNumber(spannableStringBuilder, paramInt);
    return spannableStringBuilder.toString();
  }
  
  @Deprecated
  public static int getFormatTypeForLocale(Locale paramLocale) {
    String str = paramLocale.getCountry();
    return getFormatTypeFromCountryCode(str);
  }
  
  @Deprecated
  public static void formatNumber(Editable paramEditable, int paramInt) {
    int i = paramInt;
    paramInt = i;
    if (paramEditable.length() > 2) {
      paramInt = i;
      if (paramEditable.charAt(0) == '+')
        if (paramEditable.charAt(1) == '1') {
          paramInt = 1;
        } else if (paramEditable.length() >= 3 && paramEditable.charAt(1) == '8' && paramEditable.charAt(2) == '1') {
          paramInt = 2;
        } else {
          paramInt = 0;
        }  
    } 
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2)
          return; 
        formatJapaneseNumber(paramEditable);
        return;
      } 
      formatNanpNumber(paramEditable);
      return;
    } 
    removeDashes(paramEditable);
  }
  
  @Deprecated
  public static void formatNanpNumber(Editable paramEditable) {
    int i = paramEditable.length();
    if (i > "+1-nnn-nnn-nnnn".length())
      return; 
    if (i <= 5)
      return; 
    CharSequence charSequence = paramEditable.subSequence(0, i);
    removeDashes(paramEditable);
    int j = paramEditable.length();
    int[] arrayOfInt = new int[3];
    byte b = 0;
    int k = 1;
    boolean bool = false;
    int m;
    for (m = 0; m < j; m++, k = i) {
      i = paramEditable.charAt(m);
      if (i != 43) {
        if (i != 45) {
          switch (i) {
            case 49:
            
            case 48:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            
          } 
        } else {
          i = 4;
          continue;
        } 
      } else if (m == 0) {
        i = 2;
        continue;
      } 
      paramEditable.replace(0, j, charSequence);
      return;
    } 
    i = b;
    if (bool == 7)
      i = b - 1; 
    for (b = 0; b < i; b++) {
      m = arrayOfInt[b];
      paramEditable.replace(m + b, m + b, "-");
    } 
    i = paramEditable.length();
    while (i > 0 && paramEditable.charAt(i - 1) == '-') {
      paramEditable.delete(i - 1, i);
      i--;
    } 
  }
  
  @Deprecated
  public static void formatJapaneseNumber(Editable paramEditable) {
    JapanesePhoneNumberFormatter.format(paramEditable);
  }
  
  private static void removeDashes(Editable paramEditable) {
    byte b = 0;
    while (b < paramEditable.length()) {
      if (paramEditable.charAt(b) == '-') {
        paramEditable.delete(b, b + 1);
        continue;
      } 
      b++;
    } 
  }
  
  public static String formatNumberToE164(String paramString1, String paramString2) {
    return formatNumberInternal(paramString1, paramString2, PhoneNumberUtil.PhoneNumberFormat.E164);
  }
  
  public static String formatNumberToRFC3966(String paramString1, String paramString2) {
    return formatNumberInternal(paramString1, paramString2, PhoneNumberUtil.PhoneNumberFormat.RFC3966);
  }
  
  private static String formatNumberInternal(String paramString1, String paramString2, PhoneNumberUtil.PhoneNumberFormat paramPhoneNumberFormat) {
    PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
    try {
      Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(paramString1, paramString2);
      if (phoneNumberUtil.isValidNumber(phoneNumber))
        return phoneNumberUtil.format(phoneNumber, paramPhoneNumberFormat); 
    } catch (NumberParseException numberParseException) {}
    return null;
  }
  
  public static boolean isInternationalNumber(String paramString1, String paramString2) {
    boolean bool = TextUtils.isEmpty(paramString1);
    boolean bool1 = false;
    if (bool)
      return false; 
    if (paramString1.startsWith("#") || paramString1.startsWith("*"))
      return false; 
    PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
    try {
      Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parseAndKeepRawInput(paramString1, paramString2);
      int i = phoneNumber.getCountryCode(), j = phoneNumberUtil.getCountryCodeForRegion(paramString2);
      if (i != j)
        bool1 = true; 
      return bool1;
    } catch (NumberParseException numberParseException) {
      return false;
    } 
  }
  
  public static String formatNumber(String paramString1, String paramString2) {
    if (paramString1.startsWith("#") || paramString1.startsWith("*"))
      return paramString1; 
    PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
    NumberParseException numberParseException2 = null;
    try {
      String str;
      Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parseAndKeepRawInput(paramString1, paramString2);
      if ("KR".equalsIgnoreCase(paramString2) && phoneNumber.getCountryCode() == phoneNumberUtil.getCountryCodeForRegion("KR") && phoneNumber.getCountryCodeSource() == Phonenumber.PhoneNumber.CountryCodeSource.FROM_NUMBER_WITH_PLUS_SIGN) {
        str = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.NATIONAL);
      } else if ("JP".equalsIgnoreCase(paramString2) && str.getCountryCode() == phoneNumberUtil.getCountryCodeForRegion("JP") && str.getCountryCodeSource() == Phonenumber.PhoneNumber.CountryCodeSource.FROM_NUMBER_WITH_PLUS_SIGN) {
        str = phoneNumberUtil.format((Phonenumber.PhoneNumber)str, PhoneNumberUtil.PhoneNumberFormat.NATIONAL);
      } else {
        str = phoneNumberUtil.formatInOriginalFormat((Phonenumber.PhoneNumber)str, paramString2);
      } 
    } catch (NumberParseException numberParseException1) {
      numberParseException1 = numberParseException2;
    } 
    return (String)numberParseException1;
  }
  
  public static String formatNumber(String paramString1, String paramString2, String paramString3) {
    if (paramString1 == null)
      return null; 
    int i = paramString1.length();
    int j;
    for (j = 0; j < i; j++) {
      if (!isDialable(paramString1.charAt(j)))
        return paramString1; 
    } 
    PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
    String str = paramString3;
    if (paramString2 != null) {
      str = paramString3;
      if (paramString2.length() >= 2) {
        str = paramString3;
        if (paramString2.charAt(0) == '+')
          try {
            Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(paramString2, "ZZ");
            String str1 = phoneNumberUtil.getRegionCodeForNumber(phoneNumber);
            String str2 = paramString3;
            if (!TextUtils.isEmpty(str1)) {
              j = normalizeNumber(paramString1).indexOf(paramString2.substring(1));
              str2 = paramString3;
              if (j <= 0)
                str2 = str1; 
            } 
          } catch (NumberParseException numberParseException) {
            str = paramString3;
          }  
      } 
    } 
    paramString2 = formatNumber(paramString1, str);
    if (paramString2 != null)
      paramString1 = paramString2; 
    return paramString1;
  }
  
  public static String normalizeNumber(String paramString) {
    if (TextUtils.isEmpty(paramString))
      return ""; 
    StringBuilder stringBuilder = new StringBuilder();
    int i = paramString.length();
    for (byte b = 0; b < i; b++) {
      char c = paramString.charAt(b);
      int j = Character.digit(c, 10);
      if (j != -1) {
        stringBuilder.append(j);
      } else if (stringBuilder.length() == 0 && c == '+') {
        stringBuilder.append(c);
      } else if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
        return normalizeNumber(convertKeypadLettersToDigits(paramString));
      } 
    } 
    return stringBuilder.toString();
  }
  
  public static String replaceUnicodeDigits(String paramString) {
    StringBuilder stringBuilder = new StringBuilder(paramString.length());
    for (char c : paramString.toCharArray()) {
      int i = Character.digit(c, 10);
      if (i != -1) {
        stringBuilder.append(i);
      } else {
        stringBuilder.append(c);
      } 
    } 
    return stringBuilder.toString();
  }
  
  @Deprecated
  public static boolean isEmergencyNumber(String paramString) {
    return isEmergencyNumber(getDefaultVoiceSubId(), paramString);
  }
  
  @Deprecated
  public static boolean isEmergencyNumber(int paramInt, String paramString) {
    return isEmergencyNumberInternal(paramInt, paramString, true);
  }
  
  @Deprecated
  public static boolean isPotentialEmergencyNumber(String paramString) {
    return isPotentialEmergencyNumber(getDefaultVoiceSubId(), paramString);
  }
  
  @Deprecated
  public static boolean isPotentialEmergencyNumber(int paramInt, String paramString) {
    return isEmergencyNumberInternal(paramInt, paramString, false);
  }
  
  private static boolean isEmergencyNumberInternal(String paramString, boolean paramBoolean) {
    return isEmergencyNumberInternal(getDefaultVoiceSubId(), paramString, paramBoolean);
  }
  
  private static boolean isEmergencyNumberInternal(int paramInt, String paramString, boolean paramBoolean) {
    return isEmergencyNumberInternal(paramInt, paramString, null, paramBoolean);
  }
  
  @Deprecated
  public static boolean isEmergencyNumber(String paramString1, String paramString2) {
    return isEmergencyNumber(getDefaultVoiceSubId(), paramString1, paramString2);
  }
  
  @Deprecated
  public static boolean isEmergencyNumber(int paramInt, String paramString1, String paramString2) {
    return isEmergencyNumberInternal(paramInt, paramString1, paramString2, true);
  }
  
  @Deprecated
  public static boolean isPotentialEmergencyNumber(String paramString1, String paramString2) {
    return isPotentialEmergencyNumber(getDefaultVoiceSubId(), paramString1, paramString2);
  }
  
  @Deprecated
  public static boolean isPotentialEmergencyNumber(int paramInt, String paramString1, String paramString2) {
    return isEmergencyNumberInternal(paramInt, paramString1, paramString2, false);
  }
  
  private static boolean isEmergencyNumberInternal(String paramString1, String paramString2, boolean paramBoolean) {
    return isEmergencyNumberInternal(getDefaultVoiceSubId(), paramString1, paramString2, paramBoolean);
  }
  
  private static boolean isEmergencyNumberInternal(int paramInt, String paramString1, String paramString2, boolean paramBoolean) {
    if (paramBoolean)
      try {
        return TelephonyManager.getDefault().isEmergencyNumber(paramString1);
      } catch (RuntimeException runtimeException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("isEmergencyNumberInternal: RuntimeException: ");
        stringBuilder.append(runtimeException);
        Rlog.e("PhoneNumberUtils", stringBuilder.toString());
        return false;
      }  
    return TelephonyManager.getDefault().isPotentialEmergencyNumber((String)runtimeException);
  }
  
  @Deprecated
  public static boolean isLocalEmergencyNumber(Context paramContext, String paramString) {
    return isLocalEmergencyNumber(paramContext, getDefaultVoiceSubId(), paramString);
  }
  
  @Deprecated
  public static boolean isLocalEmergencyNumber(Context paramContext, int paramInt, String paramString) {
    return isLocalEmergencyNumberInternal(paramInt, paramString, paramContext, true);
  }
  
  @Deprecated
  public static boolean isPotentialLocalEmergencyNumber(Context paramContext, String paramString) {
    return isPotentialLocalEmergencyNumber(paramContext, getDefaultVoiceSubId(), paramString);
  }
  
  @Deprecated
  public static boolean isPotentialLocalEmergencyNumber(Context paramContext, int paramInt, String paramString) {
    return isLocalEmergencyNumberInternal(paramInt, paramString, paramContext, false);
  }
  
  private static boolean isLocalEmergencyNumberInternal(String paramString, Context paramContext, boolean paramBoolean) {
    return isLocalEmergencyNumberInternal(getDefaultVoiceSubId(), paramString, paramContext, paramBoolean);
  }
  
  private static boolean isLocalEmergencyNumberInternal(int paramInt, String paramString, Context paramContext, boolean paramBoolean) {
    return isEmergencyNumberInternal(paramInt, paramString, null, paramBoolean);
  }
  
  public static boolean isVoiceMailNumber(String paramString) {
    return isVoiceMailNumber(SubscriptionManager.getDefaultSubscriptionId(), paramString);
  }
  
  public static boolean isVoiceMailNumber(int paramInt, String paramString) {
    return isVoiceMailNumber(null, paramInt, paramString);
  }
  
  @SystemApi
  public static boolean isVoiceMailNumber(Context paramContext, int paramInt, String paramString) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_3
    //   2: aload_0
    //   3: ifnonnull -> 14
    //   6: invokestatic getDefault : ()Landroid/telephony/TelephonyManager;
    //   9: astore #4
    //   11: goto -> 20
    //   14: aload_0
    //   15: invokestatic from : (Landroid/content/Context;)Landroid/telephony/TelephonyManager;
    //   18: astore #4
    //   20: aload #4
    //   22: iload_1
    //   23: invokevirtual getVoiceMailNumber : (I)Ljava/lang/String;
    //   26: astore #5
    //   28: aload #4
    //   30: iload_1
    //   31: invokevirtual getLine1Number : (I)Ljava/lang/String;
    //   34: astore #4
    //   36: aload_2
    //   37: invokestatic extractNetworkPortionAlt : (Ljava/lang/String;)Ljava/lang/String;
    //   40: astore_2
    //   41: aload_2
    //   42: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   45: ifeq -> 50
    //   48: iconst_0
    //   49: ireturn
    //   50: iconst_0
    //   51: istore #6
    //   53: iload #6
    //   55: istore #7
    //   57: aload_0
    //   58: ifnull -> 103
    //   61: aload_0
    //   62: ldc_w 'carrier_config'
    //   65: invokevirtual getSystemService : (Ljava/lang/String;)Ljava/lang/Object;
    //   68: checkcast android/telephony/CarrierConfigManager
    //   71: astore_0
    //   72: iload #6
    //   74: istore #7
    //   76: aload_0
    //   77: ifnull -> 103
    //   80: aload_0
    //   81: iload_1
    //   82: invokevirtual getConfigForSubId : (I)Landroid/os/PersistableBundle;
    //   85: astore_0
    //   86: iload #6
    //   88: istore #7
    //   90: aload_0
    //   91: ifnull -> 103
    //   94: aload_0
    //   95: ldc_w 'mdn_is_additional_voicemail_number_bool'
    //   98: invokevirtual getBoolean : (Ljava/lang/String;)Z
    //   101: istore #7
    //   103: iload #7
    //   105: ifeq -> 135
    //   108: aload_2
    //   109: aload #5
    //   111: invokestatic compare : (Ljava/lang/String;Ljava/lang/String;)Z
    //   114: ifne -> 129
    //   117: iload_3
    //   118: istore #7
    //   120: aload_2
    //   121: aload #4
    //   123: invokestatic compare : (Ljava/lang/String;Ljava/lang/String;)Z
    //   126: ifeq -> 132
    //   129: iconst_1
    //   130: istore #7
    //   132: iload #7
    //   134: ireturn
    //   135: aload_2
    //   136: aload #5
    //   138: invokestatic compare : (Ljava/lang/String;Ljava/lang/String;)Z
    //   141: ireturn
    //   142: astore_0
    //   143: iconst_0
    //   144: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2268	-> 0
    //   #2269	-> 6
    //   #2272	-> 14
    //   #2275	-> 20
    //   #2276	-> 28
    //   #2282	-> 36
    //   #2285	-> 36
    //   #2286	-> 41
    //   #2288	-> 48
    //   #2292	-> 50
    //   #2293	-> 53
    //   #2294	-> 61
    //   #2295	-> 61
    //   #2296	-> 72
    //   #2297	-> 80
    //   #2298	-> 86
    //   #2299	-> 94
    //   #2306	-> 103
    //   #2308	-> 108
    //   #2311	-> 135
    //   #2279	-> 142
    //   #2281	-> 143
    // Exception table:
    //   from	to	target	type
    //   6	11	142	java/lang/SecurityException
    //   14	20	142	java/lang/SecurityException
    //   20	28	142	java/lang/SecurityException
    //   28	36	142	java/lang/SecurityException
  }
  
  public static String convertKeypadLettersToDigits(String paramString) {
    if (paramString == null)
      return paramString; 
    int i = paramString.length();
    if (i == 0)
      return paramString; 
    char[] arrayOfChar = paramString.toCharArray();
    for (byte b = 0; b < i; b++) {
      char c = arrayOfChar[b];
      arrayOfChar[b] = (char)KEYPAD_MAP.get(c, c);
    } 
    return new String(arrayOfChar);
  }
  
  public static String cdmaCheckAndProcessPlusCode(String paramString) {
    if (!TextUtils.isEmpty(paramString) && isReallyDialable(paramString.charAt(0)) && isNonSeparator(paramString)) {
      String str1 = TelephonyManager.getDefault().getNetworkCountryIso();
      String str2 = TelephonyManager.getDefault().getSimCountryIso();
      if (!TextUtils.isEmpty(str1) && !TextUtils.isEmpty(str2)) {
        int i = getFormatTypeFromCountryCode(str1);
        int j = getFormatTypeFromCountryCode(str2);
        return cdmaCheckAndProcessPlusCodeByNumberFormat(paramString, i, j);
      } 
    } 
    return paramString;
  }
  
  public static String cdmaCheckAndProcessPlusCodeForSms(String paramString) {
    if (!TextUtils.isEmpty(paramString) && isReallyDialable(paramString.charAt(0)) && isNonSeparator(paramString)) {
      String str = TelephonyManager.getDefault().getSimCountryIso();
      if (!TextUtils.isEmpty(str)) {
        int i = getFormatTypeFromCountryCode(str);
        return cdmaCheckAndProcessPlusCodeByNumberFormat(paramString, i, i);
      } 
    } 
    return paramString;
  }
  
  public static String cdmaCheckAndProcessPlusCodeByNumberFormat(String paramString, int paramInt1, int paramInt2) {
    boolean bool;
    String str1 = paramString;
    if (paramInt1 == paramInt2 && paramInt1 == 1) {
      bool = true;
    } else {
      bool = false;
    } 
    String str2 = str1;
    if (paramString != null) {
      str2 = str1;
      if (paramString.lastIndexOf("+") != -1) {
        String str = paramString;
        str2 = null;
        while (true) {
          if (bool) {
            str1 = extractNetworkPortion(str);
          } else {
            str1 = extractNetworkPortionAlt(str);
          } 
          str1 = processPlusCode(str1, bool);
          if (!TextUtils.isEmpty(str1)) {
            if (str2 == null) {
              str2 = str1;
            } else {
              str2 = str2.concat(str1);
            } 
            String str3 = extractPostDialPortion(str);
            str1 = str2;
            String str4 = str3, str5 = str;
            if (!TextUtils.isEmpty(str3)) {
              paramInt1 = findDialableIndexFromPostDialStr(str3);
              if (paramInt1 >= 1) {
                str1 = appendPwCharBackToOrigDialStr(paramInt1, str2, str3);
                str5 = str3.substring(paramInt1);
                str4 = str3;
              } else {
                str1 = str3;
                if (paramInt1 < 0)
                  str1 = ""; 
                Rlog.e("wrong postDialStr=", str1);
                str5 = str;
                str4 = str1;
                str1 = str2;
              } 
            } 
            str2 = str1;
            if (!TextUtils.isEmpty(str4)) {
              str2 = str1;
              str = str5;
              if (TextUtils.isEmpty(str5)) {
                str2 = str1;
                break;
              } 
              continue;
            } 
            break;
          } 
          Rlog.e("checkAndProcessPlusCode: null newDialStr", str1);
          return paramString;
        } 
      } 
    } 
    return str2;
  }
  
  public static CharSequence createTtsSpannable(CharSequence paramCharSequence) {
    if (paramCharSequence == null)
      return null; 
    paramCharSequence = Spannable.Factory.getInstance().newSpannable(paramCharSequence);
    addTtsSpan((Spannable)paramCharSequence, 0, paramCharSequence.length());
    return paramCharSequence;
  }
  
  public static void addTtsSpan(Spannable paramSpannable, int paramInt1, int paramInt2) {
    paramSpannable.setSpan(createTtsSpan(paramSpannable.subSequence(paramInt1, paramInt2).toString()), paramInt1, paramInt2, 33);
  }
  
  @Deprecated
  public static CharSequence ttsSpanAsPhoneNumber(CharSequence paramCharSequence) {
    return createTtsSpannable(paramCharSequence);
  }
  
  @Deprecated
  public static void ttsSpanAsPhoneNumber(Spannable paramSpannable, int paramInt1, int paramInt2) {
    addTtsSpan(paramSpannable, paramInt1, paramInt2);
  }
  
  public static TtsSpan createTtsSpan(String paramString) {
    if (paramString == null)
      return null; 
    PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
    Phonenumber.PhoneNumber phoneNumber = null;
    try {
      Phonenumber.PhoneNumber phoneNumber1 = phoneNumberUtil.parse(paramString, null);
    } catch (NumberParseException numberParseException) {}
    TtsSpan.TelephoneBuilder telephoneBuilder = new TtsSpan.TelephoneBuilder();
    if (phoneNumber == null) {
      telephoneBuilder.setNumberParts(splitAtNonNumerics(paramString));
    } else {
      if (phoneNumber.hasCountryCode())
        telephoneBuilder.setCountryCode(Integer.toString(phoneNumber.getCountryCode())); 
      telephoneBuilder.setNumberParts(Long.toString(phoneNumber.getNationalNumber()));
    } 
    return telephoneBuilder.build();
  }
  
  private static String splitAtNonNumerics(CharSequence paramCharSequence) {
    StringBuilder stringBuilder = new StringBuilder(paramCharSequence.length());
    byte b = 0;
    while (true) {
      int i = paramCharSequence.length();
      String str = " ";
      if (b < i) {
        Character character;
        if (is12Key(paramCharSequence.charAt(b)))
          character = Character.valueOf(paramCharSequence.charAt(b)); 
        stringBuilder.append(character);
        b++;
        continue;
      } 
      break;
    } 
    return stringBuilder.toString().replaceAll(" +", " ").trim();
  }
  
  private static String getCurrentIdp(boolean paramBoolean) {
    String str;
    if (paramBoolean) {
      str = "011";
    } else {
      str = TelephonyProperties.operator_idp_string().orElse("+");
    } 
    return str;
  }
  
  private static boolean isTwoToNine(char paramChar) {
    if (paramChar >= '2' && paramChar <= '9')
      return true; 
    return false;
  }
  
  private static int getFormatTypeFromCountryCode(String paramString) {
    int i = NANP_COUNTRIES.length;
    for (byte b = 0; b < i; b++) {
      if (NANP_COUNTRIES[b].compareToIgnoreCase(paramString) == 0)
        return 1; 
    } 
    if ("jp".compareToIgnoreCase(paramString) == 0)
      return 2; 
    return 0;
  }
  
  public static boolean isNanp(String paramString) {
    boolean bool2, bool1 = false;
    if (paramString != null) {
      bool2 = bool1;
      if (paramString.length() == 10) {
        bool2 = bool1;
        if (isTwoToNine(paramString.charAt(0))) {
          bool2 = bool1;
          if (isTwoToNine(paramString.charAt(3))) {
            bool1 = true;
            byte b = 1;
            while (true) {
              bool2 = bool1;
              if (b < 10) {
                char c = paramString.charAt(b);
                if (!isISODigit(c)) {
                  bool2 = false;
                  break;
                } 
                b++;
                continue;
              } 
              break;
            } 
          } 
        } 
      } 
    } else {
      Rlog.e("isNanp: null dialStr passed in", paramString);
      bool2 = bool1;
    } 
    return bool2;
  }
  
  private static boolean isOneNanp(String paramString) {
    boolean bool1 = false, bool2 = false;
    if (paramString != null) {
      String str = paramString.substring(1);
      bool1 = bool2;
      if (paramString.charAt(0) == '1') {
        bool1 = bool2;
        if (isNanp(str))
          bool1 = true; 
      } 
    } else {
      Rlog.e("isOneNanp: null dialStr passed in", paramString);
    } 
    return bool1;
  }
  
  @SystemApi
  public static boolean isUriNumber(String paramString) {
    boolean bool;
    if (paramString != null && (paramString.contains("@") || paramString.contains("%40"))) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @SystemApi
  public static String getUsernameFromUriNumber(String paramString) {
    int i = paramString.indexOf('@');
    int j = i;
    if (i < 0)
      j = paramString.indexOf("%40"); 
    i = j;
    if (j < 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getUsernameFromUriNumber: no delimiter found in SIP addr '");
      stringBuilder.append(paramString);
      stringBuilder.append("'");
      Rlog.w("PhoneNumberUtils", stringBuilder.toString());
      i = paramString.length();
    } 
    return paramString.substring(0, i);
  }
  
  public static Uri convertSipUriToTelUri(Uri paramUri) {
    String str2 = paramUri.getScheme();
    if (!"sip".equals(str2))
      return paramUri; 
    str2 = paramUri.getSchemeSpecificPart();
    String[] arrayOfString = str2.split("[@;:]");
    if (arrayOfString.length == 0)
      return paramUri; 
    String str1 = arrayOfString[0];
    return Uri.fromParts("tel", str1, null);
  }
  
  private static String processPlusCode(String paramString, boolean paramBoolean) {
    String str1 = paramString;
    String str2 = str1;
    if (paramString != null) {
      str2 = str1;
      if (paramString.charAt(0) == '+') {
        str2 = str1;
        if (paramString.length() > 1) {
          str2 = paramString.substring(1);
          if (!paramBoolean || !isOneNanp(str2))
            str2 = paramString.replaceFirst("[+]", getCurrentIdp(paramBoolean)); 
        } 
      } 
    } 
    return str2;
  }
  
  private static int findDialableIndexFromPostDialStr(String paramString) {
    for (byte b = 0; b < paramString.length(); b++) {
      char c = paramString.charAt(b);
      if (isReallyDialable(c))
        return b; 
    } 
    return -1;
  }
  
  private static String appendPwCharBackToOrigDialStr(int paramInt, String paramString1, String paramString2) {
    String str;
    if (paramInt == 1) {
      StringBuilder stringBuilder = new StringBuilder(paramString1);
      stringBuilder = stringBuilder.append(paramString2.charAt(0));
      str = stringBuilder.toString();
    } else {
      paramString2 = paramString2.substring(0, paramInt);
      str = str.concat(paramString2);
    } 
    return str;
  }
  
  private static boolean matchIntlPrefix(String paramString, int paramInt) {
    boolean bool;
    byte b1 = 0;
    byte b2 = 0;
    while (true) {
      bool = false;
      if (b2 < paramInt) {
        char c = paramString.charAt(b2);
        if (b1) {
          if (b1 != 2) {
            if (b1 != 4) {
              if (isNonSeparator(c))
                return false; 
            } else if (c == '1') {
              b1 = 5;
            } else if (isNonSeparator(c)) {
              return false;
            } 
          } else if (c == '0') {
            b1 = 3;
          } else if (c == '1') {
            b1 = 4;
          } else if (isNonSeparator(c)) {
            return false;
          } 
        } else if (c == '+') {
          b1 = 1;
        } else if (c == '0') {
          b1 = 2;
        } else if (isNonSeparator(c)) {
          return false;
        } 
        b2++;
        continue;
      } 
      break;
    } 
    if (b1 == 1 || b1 == 3 || b1 == 5)
      bool = true; 
    return bool;
  }
  
  private static boolean matchIntlPrefixAndCC(String paramString, int paramInt) {
    boolean bool;
    byte b1 = 0;
    byte b2 = 0;
    while (true) {
      bool = false;
      if (b2 < paramInt) {
        char c = paramString.charAt(b2);
        switch (b1) {
          default:
            if (isNonSeparator(c))
              return false; 
            break;
          case true:
          case true:
            if (isISODigit(c)) {
              b1++;
              break;
            } 
            if (isNonSeparator(c))
              return false; 
            break;
          case true:
            if (c == '1') {
              b1 = 5;
              break;
            } 
            if (isNonSeparator(c))
              return false; 
            break;
          case true:
            if (c == '0') {
              b1 = 3;
              break;
            } 
            if (c == '1') {
              b1 = 4;
              break;
            } 
            if (isNonSeparator(c))
              return false; 
            break;
          case true:
          case true:
          case true:
            if (isISODigit(c)) {
              b1 = 6;
              break;
            } 
            if (isNonSeparator(c))
              return false; 
            break;
          case false:
            if (c == '+') {
              b1 = 1;
              break;
            } 
            if (c == '0') {
              b1 = 2;
              break;
            } 
            if (isNonSeparator(c))
              return false; 
            break;
        } 
        b2++;
        continue;
      } 
      break;
    } 
    if (b1 == 6 || b1 == 7 || b1 == 8)
      bool = true; 
    return bool;
  }
  
  private static boolean matchTrunkPrefix(String paramString, int paramInt) {
    boolean bool = false;
    for (byte b = 0; b < paramInt; b++) {
      char c = paramString.charAt(b);
      if (c == '0' && !bool) {
        bool = true;
      } else if (isNonSeparator(c)) {
        return false;
      } 
    } 
    return bool;
  }
  
  private static boolean isCountryCallingCode(int paramInt) {
    boolean bool;
    if (paramInt > 0 && paramInt < CCC_LENGTH && COUNTRY_CALLING_CALL[paramInt]) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static int tryGetISODigit(char paramChar) {
    if ('0' <= paramChar && paramChar <= '9')
      return paramChar - 48; 
    return -1;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface BcdExtendType {}
  
  private static class CountryCallingCodeAndNewIndex {
    public final int countryCallingCode;
    
    public final int newIndex;
    
    public CountryCallingCodeAndNewIndex(int param1Int1, int param1Int2) {
      this.countryCallingCode = param1Int1;
      this.newIndex = param1Int2;
    }
  }
  
  private static CountryCallingCodeAndNewIndex tryGetCountryCallingCodeAndNewIndex(String paramString, boolean paramBoolean) {
    byte b1 = 0;
    int i = 0;
    int j = paramString.length();
    for (byte b2 = 0; b2 < j; b2++) {
      int k;
      char c = paramString.charAt(b2);
      switch (b1) {
        default:
          return null;
        case true:
          if (c == '6')
            return new CountryCallingCodeAndNewIndex(66, b2 + 1); 
          return null;
        case true:
          if (c == '6') {
            b1 = 9;
            break;
          } 
          if (isDialable(c))
            return null; 
          break;
        case true:
          if (c == '1') {
            b1 = 5;
            break;
          } 
          if (isDialable(c))
            return null; 
          break;
        case true:
          if (c == '0') {
            b1 = 3;
            break;
          } 
          if (c == '1') {
            b1 = 4;
            break;
          } 
          if (isDialable(c))
            return null; 
          break;
        case true:
        case true:
        case true:
        case true:
        case true:
          k = tryGetISODigit(c);
          if (k > 0) {
            i = i * 10 + k;
            if (i >= 100 || isCountryCallingCode(i))
              return new CountryCallingCodeAndNewIndex(i, b2 + 1); 
            if (b1 == 1 || b1 == 3 || b1 == 5) {
              b1 = 6;
              break;
            } 
            b1++;
            break;
          } 
          if (isDialable(c))
            return null; 
          break;
        case false:
          if (c == '+') {
            b1 = 1;
            break;
          } 
          if (c == '0') {
            b1 = 2;
            break;
          } 
          if (c == '1') {
            if (paramBoolean) {
              b1 = 8;
              break;
            } 
            return null;
          } 
          if (isDialable(c))
            return null; 
          break;
      } 
    } 
    return null;
  }
  
  private static int tryGetTrunkPrefixOmittedIndex(String paramString, int paramInt) {
    int i = paramString.length();
    for (; paramInt < i; paramInt++) {
      char c = paramString.charAt(paramInt);
      if (tryGetISODigit(c) >= 0)
        return paramInt + 1; 
      if (isDialable(c))
        return -1; 
    } 
    return -1;
  }
  
  private static boolean checkPrefixIsIgnorable(String paramString, int paramInt1, int paramInt2) {
    boolean bool = false;
    while (paramInt2 >= paramInt1) {
      if (tryGetISODigit(paramString.charAt(paramInt2)) >= 0) {
        if (bool)
          return false; 
        bool = true;
      } else if (isDialable(paramString.charAt(paramInt2))) {
        return false;
      } 
      paramInt2--;
    } 
    return true;
  }
  
  private static int getDefaultVoiceSubId() {
    return SubscriptionManager.getDefaultVoiceSubscriptionId();
  }
  
  public static String convertToEmergencyNumber(Context paramContext, String paramString) {
    if (paramContext == null || TextUtils.isEmpty(paramString))
      return paramString; 
    String str = normalizeNumber(paramString);
    if (isEmergencyNumber(str))
      return paramString; 
    if (sConvertToEmergencyMap == null)
      sConvertToEmergencyMap = paramContext.getResources().getStringArray(17236004); 
    String[] arrayOfString = sConvertToEmergencyMap;
    if (arrayOfString == null || arrayOfString.length == 0)
      return paramString; 
    int i;
    byte b;
    for (i = arrayOfString.length, b = 0; b < i; ) {
      String str1 = arrayOfString[b];
      String[] arrayOfString1 = null;
      String[] arrayOfString2 = null;
      String str2 = null;
      if (!TextUtils.isEmpty(str1))
        arrayOfString1 = str1.split(":"); 
      String[] arrayOfString3 = arrayOfString2;
      str1 = str2;
      if (arrayOfString1 != null) {
        arrayOfString3 = arrayOfString2;
        str1 = str2;
        if (arrayOfString1.length == 2) {
          str2 = arrayOfString1[1];
          arrayOfString3 = arrayOfString2;
          str1 = str2;
          if (!TextUtils.isEmpty(arrayOfString1[0])) {
            arrayOfString3 = arrayOfString1[0].split(",");
            str1 = str2;
          } 
        } 
      } 
      if (!TextUtils.isEmpty(str1) && arrayOfString3 != null && arrayOfString3.length != 0) {
        int j;
        byte b1;
        for (j = arrayOfString3.length, b1 = 0; b1 < j; ) {
          String str3 = arrayOfString3[b1];
          if (!TextUtils.isEmpty(str3) && str3.equals(str))
            return str1; 
          b1++;
        } 
      } 
      b++;
    } 
    return paramString;
  }
}
