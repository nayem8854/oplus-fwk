package android.telephony;

import android.hardware.radio.V1_0.CellInfo;
import android.hardware.radio.V1_2.CellInfo;
import android.hardware.radio.V1_4.CellInfo;
import android.hardware.radio.V1_5.CellInfo;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.telephony.Rlog;
import java.util.Objects;

public final class CellInfoLte extends CellInfo implements Parcelable {
  public CellInfoLte() {
    this.mCellIdentityLte = new CellIdentityLte();
    this.mCellSignalStrengthLte = new CellSignalStrengthLte();
    this.mCellConfig = new CellConfigLte();
  }
  
  public CellInfoLte(CellInfoLte paramCellInfoLte) {
    super(paramCellInfoLte);
    this.mCellIdentityLte = paramCellInfoLte.mCellIdentityLte.copy();
    this.mCellSignalStrengthLte = paramCellInfoLte.mCellSignalStrengthLte.copy();
    this.mCellConfig = new CellConfigLte(paramCellInfoLte.mCellConfig);
  }
  
  public CellInfoLte(CellInfo paramCellInfo) {
    super(paramCellInfo);
    android.hardware.radio.V1_0.CellInfoLte cellInfoLte = paramCellInfo.lte.get(0);
    this.mCellIdentityLte = new CellIdentityLte(cellInfoLte.cellIdentityLte);
    this.mCellSignalStrengthLte = new CellSignalStrengthLte(cellInfoLte.signalStrengthLte);
    this.mCellConfig = new CellConfigLte();
  }
  
  public CellInfoLte(CellInfo paramCellInfo) {
    super(paramCellInfo);
    android.hardware.radio.V1_2.CellInfoLte cellInfoLte = paramCellInfo.lte.get(0);
    this.mCellIdentityLte = new CellIdentityLte(cellInfoLte.cellIdentityLte);
    this.mCellSignalStrengthLte = new CellSignalStrengthLte(cellInfoLte.signalStrengthLte);
    this.mCellConfig = new CellConfigLte();
  }
  
  public CellInfoLte(CellInfo paramCellInfo, long paramLong) {
    super(paramCellInfo, paramLong);
    android.hardware.radio.V1_4.CellInfoLte cellInfoLte = paramCellInfo.info.lte();
    this.mCellIdentityLte = new CellIdentityLte(cellInfoLte.base.cellIdentityLte);
    this.mCellSignalStrengthLte = new CellSignalStrengthLte(cellInfoLte.base.signalStrengthLte);
    this.mCellConfig = new CellConfigLte(cellInfoLte.cellConfig);
  }
  
  public CellInfoLte(CellInfo paramCellInfo, long paramLong) {
    super(paramCellInfo, paramLong);
    android.hardware.radio.V1_5.CellInfoLte cellInfoLte = paramCellInfo.ratSpecificInfo.lte();
    this.mCellIdentityLte = new CellIdentityLte(cellInfoLte.cellIdentityLte);
    this.mCellSignalStrengthLte = new CellSignalStrengthLte(cellInfoLte.signalStrengthLte);
    this.mCellConfig = new CellConfigLte();
  }
  
  public CellIdentityLte getCellIdentity() {
    return this.mCellIdentityLte;
  }
  
  public void setCellIdentity(CellIdentityLte paramCellIdentityLte) {
    this.mCellIdentityLte = paramCellIdentityLte;
  }
  
  public CellSignalStrengthLte getCellSignalStrength() {
    return this.mCellSignalStrengthLte;
  }
  
  public CellInfo sanitizeLocationInfo() {
    CellInfoLte cellInfoLte = new CellInfoLte(this);
    cellInfoLte.mCellIdentityLte = this.mCellIdentityLte.sanitizeLocationInfo();
    return cellInfoLte;
  }
  
  public void setCellSignalStrength(CellSignalStrengthLte paramCellSignalStrengthLte) {
    this.mCellSignalStrengthLte = paramCellSignalStrengthLte;
  }
  
  public void setCellConfig(CellConfigLte paramCellConfigLte) {
    this.mCellConfig = paramCellConfigLte;
  }
  
  public CellConfigLte getCellConfig() {
    return this.mCellConfig;
  }
  
  public int hashCode() {
    int i = super.hashCode();
    CellIdentityLte cellIdentityLte = this.mCellIdentityLte;
    int j = cellIdentityLte.hashCode();
    CellSignalStrengthLte cellSignalStrengthLte = this.mCellSignalStrengthLte;
    int k = cellSignalStrengthLte.hashCode();
    CellConfigLte cellConfigLte = this.mCellConfig;
    int m = cellConfigLte.hashCode();
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof CellInfoLte;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    if (super.equals(paramObject) && this.mCellIdentityLte.equals(((CellInfoLte)paramObject).mCellIdentityLte)) {
      CellSignalStrengthLte cellSignalStrengthLte1 = this.mCellSignalStrengthLte, cellSignalStrengthLte2 = ((CellInfoLte)paramObject).mCellSignalStrengthLte;
      if (cellSignalStrengthLte1.equals(cellSignalStrengthLte2)) {
        CellConfigLte cellConfigLte = this.mCellConfig;
        paramObject = ((CellInfoLte)paramObject).mCellConfig;
        if (cellConfigLte.equals(paramObject))
          bool1 = true; 
      } 
    } 
    return bool1;
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append("CellInfoLte:{");
    stringBuffer.append(super.toString());
    stringBuffer.append(" ");
    stringBuffer.append(this.mCellIdentityLte);
    stringBuffer.append(" ");
    stringBuffer.append(this.mCellSignalStrengthLte);
    stringBuffer.append(" ");
    stringBuffer.append(this.mCellConfig);
    stringBuffer.append("}");
    return stringBuffer.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcel(paramParcel, paramInt, 3);
    this.mCellIdentityLte.writeToParcel(paramParcel, paramInt);
    this.mCellSignalStrengthLte.writeToParcel(paramParcel, paramInt);
    this.mCellConfig.writeToParcel(paramParcel, paramInt);
  }
  
  private CellInfoLte(Parcel paramParcel) {
    super(paramParcel);
    this.mCellIdentityLte = (CellIdentityLte)CellIdentityLte.CREATOR.createFromParcel(paramParcel);
    this.mCellSignalStrengthLte = (CellSignalStrengthLte)CellSignalStrengthLte.CREATOR.createFromParcel(paramParcel);
    this.mCellConfig = (CellConfigLte)CellConfigLte.CREATOR.createFromParcel(paramParcel);
  }
  
  public static final Parcelable.Creator<CellInfoLte> CREATOR = (Parcelable.Creator<CellInfoLte>)new Object();
  
  private static final boolean DBG = false;
  
  private static final String LOG_TAG = "CellInfoLte";
  
  private CellConfigLte mCellConfig;
  
  private CellIdentityLte mCellIdentityLte;
  
  private CellSignalStrengthLte mCellSignalStrengthLte;
  
  protected static CellInfoLte createFromParcelBody(Parcel paramParcel) {
    return new CellInfoLte(paramParcel);
  }
  
  private static void log(String paramString) {
    Rlog.w("CellInfoLte", paramString);
  }
}
