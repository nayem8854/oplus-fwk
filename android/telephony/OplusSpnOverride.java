package android.telephony;

import android.util.Log;
import com.android.internal.util.XmlUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class OplusSpnOverride {
  private HashMap<String, String> mCarrierSpnMap = new HashMap<>();
  
  static final Object sInstSync = new Object();
  
  private String citylan_name = null;
  
  private static final String SPN_DATAPATH = "/data/data/com.android.phone/";
  
  private static final String TAG = "OplusSpnOverride";
  
  private static final String cityLan = "lan_";
  
  private static OplusSpnOverride sInstance;
  
  private static final String spnname_postfix = ".xml";
  
  private static final String spnname_substr = "spn_";
  
  private OplusSpnOverride(String paramString) {
    this.citylan_name = paramString;
    loadSpnOverrides(paramString);
  }
  
  public static OplusSpnOverride getInstance(String paramString) {
    synchronized (sInstSync) {
      if (sInstance == null || !paramString.equals(sInstance.getLanName())) {
        OplusSpnOverride oplusSpnOverride = new OplusSpnOverride();
        this(paramString);
        sInstance = oplusSpnOverride;
      } 
      return sInstance;
    } 
  }
  
  public boolean containsCarrier(String paramString) {
    return this.mCarrierSpnMap.containsKey(paramString);
  }
  
  public String getSpn(String paramString) {
    return this.mCarrierSpnMap.get(paramString);
  }
  
  private String getLanName() {
    return this.citylan_name;
  }
  
  private void loadSpnOverrides(String paramString) {
    Log.d("OplusSpnOverride", "getXmlFile");
    FileReader fileReader1 = null, fileReader2 = null;
    StringBuilder stringBuilder1 = null;
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("/data/data/com.android.phone/spn_");
    stringBuilder2.append(paramString);
    stringBuilder2.append(".xml");
    File file = new File(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("mSpnInfoDataPath=/data/data/com.android.phone/spn_");
    stringBuilder2.append(paramString);
    stringBuilder2.append(".xml");
    Log.d("OplusSpnOverride", stringBuilder2.toString());
    if (file.exists()) {
      stringBuilder2 = stringBuilder1;
      FileReader fileReader3 = fileReader1, fileReader4 = fileReader2;
      try {
        FileReader fileReader7 = new FileReader();
        stringBuilder2 = stringBuilder1;
        fileReader3 = fileReader1;
        fileReader4 = fileReader2;
        this(file);
        FileReader fileReader5 = fileReader7;
        FileReader fileReader6 = fileReader5;
        fileReader3 = fileReader5;
        fileReader4 = fileReader5;
        XmlPullParserFactory xmlPullParserFactory = XmlPullParserFactory.newInstance();
        fileReader6 = fileReader5;
        fileReader3 = fileReader5;
        fileReader4 = fileReader5;
        XmlPullParser xmlPullParser = xmlPullParserFactory.newPullParser();
        fileReader6 = fileReader5;
        fileReader3 = fileReader5;
        fileReader4 = fileReader5;
        xmlPullParser.setInput(fileReader5);
        if (xmlPullParser != null) {
          fileReader6 = fileReader5;
          fileReader3 = fileReader5;
          fileReader4 = fileReader5;
          StringBuilder stringBuilder = new StringBuilder();
          fileReader6 = fileReader5;
          fileReader3 = fileReader5;
          fileReader4 = fileReader5;
          this();
          fileReader6 = fileReader5;
          fileReader3 = fileReader5;
          fileReader4 = fileReader5;
          stringBuilder.append("lan_");
          fileReader6 = fileReader5;
          fileReader3 = fileReader5;
          fileReader4 = fileReader5;
          stringBuilder.append(paramString);
          fileReader6 = fileReader5;
          fileReader3 = fileReader5;
          fileReader4 = fileReader5;
          XmlUtils.beginDocument(xmlPullParser, stringBuilder.toString());
          fileReader6 = fileReader5;
          fileReader3 = fileReader5;
          fileReader4 = fileReader5;
          XmlUtils.nextElement(xmlPullParser);
          while (true) {
            fileReader6 = fileReader5;
            fileReader3 = fileReader5;
            fileReader4 = fileReader5;
            if (xmlPullParser.getEventType() != 1) {
              fileReader6 = fileReader5;
              fileReader3 = fileReader5;
              fileReader4 = fileReader5;
              getRow(xmlPullParser);
              fileReader6 = fileReader5;
              fileReader3 = fileReader5;
              fileReader4 = fileReader5;
              if (this.mCarrierSpnMap != null) {
                fileReader6 = fileReader5;
                fileReader3 = fileReader5;
                fileReader4 = fileReader5;
                XmlUtils.nextElement(xmlPullParser);
                continue;
              } 
              fileReader6 = fileReader5;
              fileReader3 = fileReader5;
              fileReader4 = fileReader5;
              XmlPullParserException xmlPullParserException = new XmlPullParserException();
              fileReader6 = fileReader5;
              fileReader3 = fileReader5;
              fileReader4 = fileReader5;
              this("Expected 'spn' tag", xmlPullParser, null);
              fileReader6 = fileReader5;
              fileReader3 = fileReader5;
              fileReader4 = fileReader5;
              throw xmlPullParserException;
            } 
            break;
          } 
        } else {
          fileReader6 = fileReader5;
          fileReader3 = fileReader5;
          fileReader4 = fileReader5;
          Log.d("OplusSpnOverride", "confparser==null");
        } 
        try {
          fileReader5.close();
        } catch (IOException iOException) {}
      } catch (FileNotFoundException fileNotFoundException) {
        FileReader fileReader = fileReader4;
        Log.e("OplusSpnOverride", "getXmlFile file not found", fileNotFoundException);
        if (fileReader4 != null)
          fileReader4.close(); 
      } catch (Exception exception) {
        FileReader fileReader = fileReader3;
        Log.e("OplusSpnOverride", "getXmlFile Exception while parsing", exception);
        if (fileReader3 != null)
          fileReader3.close(); 
      } finally {}
    } 
  }
  
  private void getRow(XmlPullParser paramXmlPullParser) {
    if (!"spnOverride".equals(paramXmlPullParser.getName()))
      Log.d("OplusSpnOverride", "spnOverride is not matched"); 
    String str2 = paramXmlPullParser.getAttributeValue(null, "numeric");
    String str1 = paramXmlPullParser.getAttributeValue(null, "spn");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("numeric=");
    stringBuilder.append(str2);
    stringBuilder.append("spn=");
    stringBuilder.append(str1);
    Log.d("OplusSpnOverride", stringBuilder.toString());
    this.mCarrierSpnMap.put(str2, str1);
  }
}
