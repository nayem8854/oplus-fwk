package android.telephony;

import android.annotation.SystemApi;
import android.os.RemoteException;
import com.android.telephony.Rlog;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public class NetworkServiceCallback {
  public static final int RESULT_ERROR_BUSY = 3;
  
  public static final int RESULT_ERROR_FAILED = 5;
  
  public static final int RESULT_ERROR_ILLEGAL_STATE = 4;
  
  public static final int RESULT_ERROR_INVALID_ARG = 2;
  
  public static final int RESULT_ERROR_UNSUPPORTED = 1;
  
  public static final int RESULT_SUCCESS = 0;
  
  private static final String mTag = NetworkServiceCallback.class.getSimpleName();
  
  private final INetworkServiceCallback mCallback;
  
  public NetworkServiceCallback(INetworkServiceCallback paramINetworkServiceCallback) {
    this.mCallback = paramINetworkServiceCallback;
  }
  
  public void onRequestNetworkRegistrationInfoComplete(int paramInt, NetworkRegistrationInfo paramNetworkRegistrationInfo) {
    INetworkServiceCallback iNetworkServiceCallback = this.mCallback;
    if (iNetworkServiceCallback != null) {
      try {
        iNetworkServiceCallback.onRequestNetworkRegistrationInfoComplete(paramInt, paramNetworkRegistrationInfo);
      } catch (RemoteException remoteException) {
        Rlog.e(mTag, "Failed to onRequestNetworkRegistrationInfoComplete on the remote");
      } 
    } else {
      Rlog.e(mTag, "onRequestNetworkRegistrationInfoComplete callback is null.");
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Result {}
}
