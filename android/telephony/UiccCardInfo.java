package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public final class UiccCardInfo implements Parcelable {
  public static final Parcelable.Creator<UiccCardInfo> CREATOR = new Parcelable.Creator<UiccCardInfo>() {
      public UiccCardInfo createFromParcel(Parcel param1Parcel) {
        return new UiccCardInfo(param1Parcel);
      }
      
      public UiccCardInfo[] newArray(int param1Int) {
        return new UiccCardInfo[param1Int];
      }
    };
  
  private final int mCardId;
  
  private final String mEid;
  
  private final String mIccId;
  
  private final boolean mIsEuicc;
  
  private final boolean mIsRemovable;
  
  private final int mSlotIndex;
  
  private UiccCardInfo(Parcel paramParcel) {
    boolean bool2;
    byte b = paramParcel.readByte();
    boolean bool1 = true;
    if (b != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mIsEuicc = bool2;
    this.mCardId = paramParcel.readInt();
    this.mEid = paramParcel.readString();
    this.mIccId = paramParcel.readString();
    this.mSlotIndex = paramParcel.readInt();
    if (paramParcel.readByte() != 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.mIsRemovable = bool2;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByte((byte)this.mIsEuicc);
    paramParcel.writeInt(this.mCardId);
    paramParcel.writeString(this.mEid);
    paramParcel.writeString(this.mIccId);
    paramParcel.writeInt(this.mSlotIndex);
    paramParcel.writeByte((byte)this.mIsRemovable);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public UiccCardInfo(boolean paramBoolean1, int paramInt1, String paramString1, String paramString2, int paramInt2, boolean paramBoolean2) {
    this.mIsEuicc = paramBoolean1;
    this.mCardId = paramInt1;
    this.mEid = paramString1;
    this.mIccId = paramString2;
    this.mSlotIndex = paramInt2;
    this.mIsRemovable = paramBoolean2;
  }
  
  public boolean isEuicc() {
    return this.mIsEuicc;
  }
  
  public int getCardId() {
    return this.mCardId;
  }
  
  public String getEid() {
    if (!this.mIsEuicc)
      return null; 
    return this.mEid;
  }
  
  public String getIccId() {
    return this.mIccId;
  }
  
  public int getSlotIndex() {
    return this.mSlotIndex;
  }
  
  public UiccCardInfo getUnprivileged() {
    return new UiccCardInfo(this.mIsEuicc, this.mCardId, null, null, this.mSlotIndex, this.mIsRemovable);
  }
  
  public boolean isRemovable() {
    return this.mIsRemovable;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mIsEuicc == ((UiccCardInfo)paramObject).mIsEuicc && this.mCardId == ((UiccCardInfo)paramObject).mCardId) {
      String str1 = this.mEid, str2 = ((UiccCardInfo)paramObject).mEid;
      if (Objects.equals(str1, str2)) {
        str1 = this.mIccId;
        str2 = ((UiccCardInfo)paramObject).mIccId;
        if (Objects.equals(str1, str2) && this.mSlotIndex == ((UiccCardInfo)paramObject).mSlotIndex && this.mIsRemovable == ((UiccCardInfo)paramObject).mIsRemovable)
          return null; 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Boolean.valueOf(this.mIsEuicc), Integer.valueOf(this.mCardId), this.mEid, this.mIccId, Integer.valueOf(this.mSlotIndex), Boolean.valueOf(this.mIsRemovable) });
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UiccCardInfo (mIsEuicc=");
    stringBuilder.append(this.mIsEuicc);
    stringBuilder.append(", mCardId=");
    stringBuilder.append(this.mCardId);
    stringBuilder.append(", mEid=");
    stringBuilder.append(this.mEid);
    stringBuilder.append(", mIccId=");
    stringBuilder.append(this.mIccId);
    stringBuilder.append(", mSlotIndex=");
    stringBuilder.append(this.mSlotIndex);
    stringBuilder.append(", mIsRemovable=");
    stringBuilder.append(this.mIsRemovable);
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
}
