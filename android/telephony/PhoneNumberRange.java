package android.telephony;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.util.Objects;
import java.util.regex.Pattern;

@SystemApi
public final class PhoneNumberRange implements Parcelable {
  public static final Parcelable.Creator<PhoneNumberRange> CREATOR = new Parcelable.Creator<PhoneNumberRange>() {
      public PhoneNumberRange createFromParcel(Parcel param1Parcel) {
        return new PhoneNumberRange(param1Parcel);
      }
      
      public PhoneNumberRange[] newArray(int param1Int) {
        return new PhoneNumberRange[param1Int];
      }
    };
  
  private final String mCountryCode;
  
  private final String mLowerBound;
  
  private final String mPrefix;
  
  private final String mUpperBound;
  
  public PhoneNumberRange(String paramString1, String paramString2, String paramString3, String paramString4) {
    validateLowerAndUpperBounds(paramString3, paramString4);
    if (Pattern.matches("[0-9]*", paramString1)) {
      if (Pattern.matches("[0-9]*", paramString2)) {
        this.mCountryCode = paramString1;
        this.mPrefix = paramString2;
        this.mLowerBound = paramString3;
        this.mUpperBound = paramString4;
        return;
      } 
      throw new IllegalArgumentException("Prefix must be all numeric");
    } 
    throw new IllegalArgumentException("Country code must be all numeric");
  }
  
  private PhoneNumberRange(Parcel paramParcel) {
    this.mCountryCode = paramParcel.readString();
    this.mPrefix = paramParcel.readString();
    this.mLowerBound = paramParcel.readString();
    this.mUpperBound = paramParcel.readString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mCountryCode);
    paramParcel.writeString(this.mPrefix);
    paramParcel.writeString(this.mLowerBound);
    paramParcel.writeString(this.mUpperBound);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (Objects.equals(this.mCountryCode, ((PhoneNumberRange)paramObject).mCountryCode)) {
      String str1 = this.mPrefix, str2 = ((PhoneNumberRange)paramObject).mPrefix;
      if (Objects.equals(str1, str2)) {
        str2 = this.mLowerBound;
        str1 = ((PhoneNumberRange)paramObject).mLowerBound;
        if (Objects.equals(str2, str1)) {
          str2 = this.mUpperBound;
          paramObject = ((PhoneNumberRange)paramObject).mUpperBound;
          if (Objects.equals(str2, paramObject))
            return null; 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mCountryCode, this.mPrefix, this.mLowerBound, this.mUpperBound });
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("PhoneNumberRange{mCountryCode='");
    stringBuilder.append(this.mCountryCode);
    stringBuilder.append('\'');
    stringBuilder.append(", mPrefix='");
    stringBuilder.append(this.mPrefix);
    stringBuilder.append('\'');
    stringBuilder.append(", mLowerBound='");
    stringBuilder.append(this.mLowerBound);
    stringBuilder.append('\'');
    stringBuilder.append(", mUpperBound='");
    stringBuilder.append(this.mUpperBound);
    stringBuilder.append('\'');
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  private void validateLowerAndUpperBounds(String paramString1, String paramString2) {
    if (paramString1.length() == paramString2.length()) {
      if (Pattern.matches("[0-9]*", paramString1)) {
        if (Pattern.matches("[0-9]*", paramString2)) {
          if (Integer.parseInt(paramString1) <= Integer.parseInt(paramString2))
            return; 
          throw new IllegalArgumentException("Lower bound must be lower than upper bound");
        } 
        throw new IllegalArgumentException("Upper bound must be all numeric");
      } 
      throw new IllegalArgumentException("Lower bound must be all numeric");
    } 
    throw new IllegalArgumentException("Lower and upper bounds must have the same length");
  }
  
  public boolean matches(String paramString) {
    paramString = paramString.replaceAll("[^0-9]", "");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.mCountryCode);
    stringBuilder.append(this.mPrefix);
    String str = stringBuilder.toString();
    boolean bool = paramString.startsWith(str);
    boolean bool1 = false;
    if (bool) {
      paramString = paramString.substring(str.length());
    } else if (paramString.startsWith(this.mPrefix)) {
      paramString = paramString.substring(this.mPrefix.length());
    } else {
      return false;
    } 
    try {
      int i = Integer.parseInt(this.mLowerBound);
      int j = Integer.parseInt(this.mUpperBound);
      int k = Integer.parseInt(paramString);
      bool = bool1;
      if (k <= j) {
        bool = bool1;
        if (k >= i)
          bool = true; 
      } 
      return bool;
    } catch (NumberFormatException numberFormatException) {
      Log.e(PhoneNumberRange.class.getSimpleName(), "Invalid bounds or number.", numberFormatException);
      return false;
    } 
  }
}
