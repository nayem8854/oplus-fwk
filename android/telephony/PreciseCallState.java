package android.telephony;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

@SystemApi
public final class PreciseCallState implements Parcelable {
  private int mRingingCallState = -1;
  
  private int mForegroundCallState = -1;
  
  private int mBackgroundCallState = -1;
  
  private int mDisconnectCause = -1;
  
  private int mPreciseDisconnectCause = -1;
  
  @SystemApi
  public PreciseCallState(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this.mRingingCallState = paramInt1;
    this.mForegroundCallState = paramInt2;
    this.mBackgroundCallState = paramInt3;
    this.mDisconnectCause = paramInt4;
    this.mPreciseDisconnectCause = paramInt5;
  }
  
  private PreciseCallState(Parcel paramParcel) {
    this.mRingingCallState = paramParcel.readInt();
    this.mForegroundCallState = paramParcel.readInt();
    this.mBackgroundCallState = paramParcel.readInt();
    this.mDisconnectCause = paramParcel.readInt();
    this.mPreciseDisconnectCause = paramParcel.readInt();
  }
  
  public int getRingingCallState() {
    return this.mRingingCallState;
  }
  
  public int getForegroundCallState() {
    return this.mForegroundCallState;
  }
  
  public int getBackgroundCallState() {
    return this.mBackgroundCallState;
  }
  
  public int getDisconnectCause() {
    return this.mDisconnectCause;
  }
  
  public int getPreciseDisconnectCause() {
    return this.mPreciseDisconnectCause;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mRingingCallState);
    paramParcel.writeInt(this.mForegroundCallState);
    paramParcel.writeInt(this.mBackgroundCallState);
    paramParcel.writeInt(this.mDisconnectCause);
    paramParcel.writeInt(this.mPreciseDisconnectCause);
  }
  
  public static final Parcelable.Creator<PreciseCallState> CREATOR = new Parcelable.Creator<PreciseCallState>() {
      public PreciseCallState createFromParcel(Parcel param1Parcel) {
        return new PreciseCallState(param1Parcel);
      }
      
      public PreciseCallState[] newArray(int param1Int) {
        return new PreciseCallState[param1Int];
      }
    };
  
  public static final int PRECISE_CALL_STATE_ACTIVE = 1;
  
  public static final int PRECISE_CALL_STATE_ALERTING = 4;
  
  public static final int PRECISE_CALL_STATE_DIALING = 3;
  
  public static final int PRECISE_CALL_STATE_DISCONNECTED = 7;
  
  public static final int PRECISE_CALL_STATE_DISCONNECTING = 8;
  
  public static final int PRECISE_CALL_STATE_HOLDING = 2;
  
  public static final int PRECISE_CALL_STATE_IDLE = 0;
  
  public static final int PRECISE_CALL_STATE_INCOMING = 5;
  
  public static final int PRECISE_CALL_STATE_NOT_VALID = -1;
  
  public static final int PRECISE_CALL_STATE_WAITING = 6;
  
  public int hashCode() {
    int i = this.mRingingCallState, j = this.mForegroundCallState, k = this.mForegroundCallState, m = this.mDisconnectCause;
    int n = this.mPreciseDisconnectCause;
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mRingingCallState != ((PreciseCallState)paramObject).mRingingCallState || this.mForegroundCallState != ((PreciseCallState)paramObject).mForegroundCallState || this.mBackgroundCallState != ((PreciseCallState)paramObject).mBackgroundCallState || this.mDisconnectCause != ((PreciseCallState)paramObject).mDisconnectCause || this.mPreciseDisconnectCause != ((PreciseCallState)paramObject).mPreciseDisconnectCause)
      bool = false; 
    return bool;
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Ringing call state: ");
    stringBuilder.append(this.mRingingCallState);
    stringBuffer.append(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append(", Foreground call state: ");
    stringBuilder.append(this.mForegroundCallState);
    stringBuffer.append(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append(", Background call state: ");
    stringBuilder.append(this.mBackgroundCallState);
    stringBuffer.append(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append(", Disconnect cause: ");
    stringBuilder.append(this.mDisconnectCause);
    stringBuffer.append(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append(", Precise disconnect cause: ");
    stringBuilder.append(this.mPreciseDisconnectCause);
    stringBuffer.append(stringBuilder.toString());
    return stringBuffer.toString();
  }
  
  public PreciseCallState() {}
}
