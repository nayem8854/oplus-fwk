package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.util.ArraySet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public final class CellIdentityGsm extends CellIdentity {
  public static final Parcelable.Creator<CellIdentityGsm> CREATOR;
  
  private static final boolean DBG = false;
  
  private static final int MAX_ARFCN = 65535;
  
  private static final int MAX_BSIC = 63;
  
  private static final int MAX_CID = 65535;
  
  private static final int MAX_LAC = 65535;
  
  private static final String TAG = CellIdentityGsm.class.getSimpleName();
  
  private final ArraySet<String> mAdditionalPlmns;
  
  private final int mArfcn;
  
  private final int mBsic;
  
  private final int mCid;
  
  private final int mLac;
  
  public CellIdentityGsm() {
    super(TAG, 1, null, null, null, null);
    this.mLac = Integer.MAX_VALUE;
    this.mCid = Integer.MAX_VALUE;
    this.mArfcn = Integer.MAX_VALUE;
    this.mBsic = Integer.MAX_VALUE;
    this.mAdditionalPlmns = new ArraySet<>();
    this.mGlobalCellId = null;
  }
  
  public CellIdentityGsm(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString1, String paramString2, String paramString3, String paramString4, Collection<String> paramCollection) {
    super(TAG, 1, paramString1, paramString2, paramString3, paramString4);
    this.mLac = inRangeOrUnavailable(paramInt1, 0, 65535);
    this.mCid = inRangeOrUnavailable(paramInt2, 0, 65535);
    this.mArfcn = inRangeOrUnavailable(paramInt3, 0, 65535);
    this.mBsic = inRangeOrUnavailable(paramInt4, 0, 63);
    this.mAdditionalPlmns = new ArraySet<>(paramCollection.size());
    for (String paramString2 : paramCollection) {
      if (isValidPlmn(paramString2))
        this.mAdditionalPlmns.add(paramString2); 
    } 
    updateGlobalCellId();
  }
  
  public CellIdentityGsm(android.hardware.radio.V1_0.CellIdentityGsm paramCellIdentityGsm) {
    this(i, j, k, m, str2, str1, "", "", arraySet);
  }
  
  public CellIdentityGsm(android.hardware.radio.V1_2.CellIdentityGsm paramCellIdentityGsm) {
    this(i, j, k, m, str2, str3, str4, str1, arraySet);
  }
  
  public CellIdentityGsm(android.hardware.radio.V1_5.CellIdentityGsm paramCellIdentityGsm) {
    this(i, j, k, m, str1, str2, str3, str4, arrayList);
  }
  
  private CellIdentityGsm(CellIdentityGsm paramCellIdentityGsm) {
    this(paramCellIdentityGsm.mLac, paramCellIdentityGsm.mCid, paramCellIdentityGsm.mArfcn, paramCellIdentityGsm.mBsic, paramCellIdentityGsm.mMccStr, paramCellIdentityGsm.mMncStr, paramCellIdentityGsm.mAlphaLong, paramCellIdentityGsm.mAlphaShort, paramCellIdentityGsm.mAdditionalPlmns);
  }
  
  CellIdentityGsm copy() {
    return new CellIdentityGsm(this);
  }
  
  public CellIdentityGsm sanitizeLocationInfo() {
    return new CellIdentityGsm(2147483647, 2147483647, 2147483647, 2147483647, this.mMccStr, this.mMncStr, this.mAlphaLong, this.mAlphaShort, this.mAdditionalPlmns);
  }
  
  protected void updateGlobalCellId() {
    this.mGlobalCellId = null;
    String str = getPlmn();
    if (str == null)
      return; 
    if (this.mLac == Integer.MAX_VALUE || this.mCid == Integer.MAX_VALUE)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str);
    stringBuilder.append(String.format("%04x%04x", new Object[] { Integer.valueOf(this.mLac), Integer.valueOf(this.mCid) }));
    this.mGlobalCellId = stringBuilder.toString();
  }
  
  @Deprecated
  public int getMcc() {
    int i;
    if (this.mMccStr != null) {
      i = Integer.valueOf(this.mMccStr).intValue();
    } else {
      i = Integer.MAX_VALUE;
    } 
    return i;
  }
  
  @Deprecated
  public int getMnc() {
    int i;
    if (this.mMncStr != null) {
      i = Integer.valueOf(this.mMncStr).intValue();
    } else {
      i = Integer.MAX_VALUE;
    } 
    return i;
  }
  
  public int getLac() {
    return this.mLac;
  }
  
  public int getCid() {
    return this.mCid;
  }
  
  public int getArfcn() {
    return this.mArfcn;
  }
  
  public int getBsic() {
    return this.mBsic;
  }
  
  public String getMobileNetworkOperator() {
    if (this.mMccStr == null || this.mMncStr == null)
      return null; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.mMccStr);
    stringBuilder.append(this.mMncStr);
    return stringBuilder.toString();
  }
  
  public String getMccString() {
    return this.mMccStr;
  }
  
  public String getMncString() {
    return this.mMncStr;
  }
  
  public int getChannelNumber() {
    return this.mArfcn;
  }
  
  public Set<String> getAdditionalPlmns() {
    return Collections.unmodifiableSet(this.mAdditionalPlmns);
  }
  
  @Deprecated
  public int getPsc() {
    return Integer.MAX_VALUE;
  }
  
  public GsmCellLocation asCellLocation() {
    GsmCellLocation gsmCellLocation = new GsmCellLocation();
    int i = this.mLac;
    if (i == Integer.MAX_VALUE)
      i = -1; 
    int j = this.mCid;
    if (j == Integer.MAX_VALUE)
      j = -1; 
    gsmCellLocation.setLacAndCid(i, j);
    gsmCellLocation.setPsc(-1);
    return gsmCellLocation;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mLac), Integer.valueOf(this.mCid), Integer.valueOf(this.mAdditionalPlmns.hashCode()), Integer.valueOf(super.hashCode()) });
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof CellIdentityGsm))
      return false; 
    CellIdentityGsm cellIdentityGsm = (CellIdentityGsm)paramObject;
    if (this.mLac == cellIdentityGsm.mLac && this.mCid == cellIdentityGsm.mCid && this.mArfcn == cellIdentityGsm.mArfcn && this.mBsic == cellIdentityGsm.mBsic) {
      String str1 = this.mMccStr, str2 = cellIdentityGsm.mMccStr;
      if (TextUtils.equals(str1, str2)) {
        str1 = this.mMncStr;
        str2 = cellIdentityGsm.mMncStr;
        if (TextUtils.equals(str1, str2)) {
          ArraySet<String> arraySet2 = this.mAdditionalPlmns, arraySet1 = cellIdentityGsm.mAdditionalPlmns;
          if (arraySet2.equals(arraySet1) && 
            super.equals(paramObject))
            return null; 
        } 
      } 
    } 
    return false;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(TAG);
    stringBuilder.append(":{ mLac=");
    stringBuilder.append(this.mLac);
    stringBuilder.append(" mCid=");
    stringBuilder.append(this.mCid);
    stringBuilder.append(" mArfcn=");
    stringBuilder.append(this.mArfcn);
    stringBuilder.append(" mBsic=");
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(this.mBsic));
    stringBuilder.append(" mMcc=");
    stringBuilder.append(this.mMccStr);
    stringBuilder.append(" mMnc=");
    stringBuilder.append(this.mMncStr);
    stringBuilder.append(" mAlphaLong=");
    stringBuilder.append(this.mAlphaLong);
    stringBuilder.append(" mAlphaShort=");
    stringBuilder.append(this.mAlphaShort);
    stringBuilder.append(" mAdditionalPlmns=");
    stringBuilder.append(this.mAdditionalPlmns);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    super.writeToParcel(paramParcel, 1);
    paramParcel.writeInt(this.mLac);
    paramParcel.writeInt(this.mCid);
    paramParcel.writeInt(this.mArfcn);
    paramParcel.writeInt(this.mBsic);
    paramParcel.writeArraySet(this.mAdditionalPlmns);
  }
  
  private CellIdentityGsm(Parcel paramParcel) {
    super(TAG, 1, paramParcel);
    this.mLac = paramParcel.readInt();
    this.mCid = paramParcel.readInt();
    this.mArfcn = paramParcel.readInt();
    this.mBsic = paramParcel.readInt();
    this.mAdditionalPlmns = paramParcel.readArraySet(null);
    updateGlobalCellId();
  }
  
  static {
    CREATOR = (Parcelable.Creator<CellIdentityGsm>)new Object();
  }
  
  protected static CellIdentityGsm createFromParcelBody(Parcel paramParcel) {
    return new CellIdentityGsm(paramParcel);
  }
}
