package android.telephony;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.telephony.uicc.IccUtils;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;

@SystemApi
public final class SmsCbEtwsInfo implements Parcelable {
  public SmsCbEtwsInfo(int paramInt, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, byte[] paramArrayOfbyte) {
    this.mWarningType = paramInt;
    this.mIsEmergencyUserAlert = paramBoolean1;
    this.mIsPopupAlert = paramBoolean2;
    this.mIsPrimary = paramBoolean3;
    this.mWarningSecurityInformation = paramArrayOfbyte;
  }
  
  SmsCbEtwsInfo(Parcel paramParcel) {
    boolean bool2;
    this.mWarningType = paramParcel.readInt();
    int i = paramParcel.readInt();
    boolean bool1 = true;
    if (i != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mIsEmergencyUserAlert = bool2;
    if (paramParcel.readInt() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mIsPopupAlert = bool2;
    if (paramParcel.readInt() != 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.mIsPrimary = bool2;
    this.mWarningSecurityInformation = paramParcel.createByteArray();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mWarningType);
    paramParcel.writeInt(this.mIsEmergencyUserAlert);
    paramParcel.writeInt(this.mIsPopupAlert);
    paramParcel.writeInt(this.mIsPrimary);
    paramParcel.writeByteArray(this.mWarningSecurityInformation);
  }
  
  public int getWarningType() {
    return this.mWarningType;
  }
  
  public boolean isEmergencyUserAlert() {
    return this.mIsEmergencyUserAlert;
  }
  
  public boolean isPopupAlert() {
    return this.mIsPopupAlert;
  }
  
  public boolean isPrimary() {
    return this.mIsPrimary;
  }
  
  public long getPrimaryNotificationTimestamp() {
    byte[] arrayOfByte = this.mWarningSecurityInformation;
    if (arrayOfByte == null || arrayOfByte.length < 7)
      return 0L; 
    int i = IccUtils.gsmBcdByteToInt(arrayOfByte[0]);
    int j = IccUtils.gsmBcdByteToInt(this.mWarningSecurityInformation[1]);
    int k = IccUtils.gsmBcdByteToInt(this.mWarningSecurityInformation[2]);
    int m = IccUtils.gsmBcdByteToInt(this.mWarningSecurityInformation[3]);
    int n = IccUtils.gsmBcdByteToInt(this.mWarningSecurityInformation[4]);
    int i1 = IccUtils.gsmBcdByteToInt(this.mWarningSecurityInformation[5]);
    byte b = this.mWarningSecurityInformation[6];
    int i2 = IccUtils.gsmBcdByteToInt((byte)(b & 0xFFFFFFF7));
    if ((b & 0x8) != 0)
      i2 = -i2; 
    LocalDateTime localDateTime = LocalDateTime.of(i + 2000, j, k, m, n, i1);
    long l1 = localDateTime.toEpochSecond(ZoneOffset.UTC), l2 = (i2 * 15 * 60);
    return 1000L * (l1 - l2);
  }
  
  public byte[] getPrimaryNotificationSignature() {
    byte[] arrayOfByte = this.mWarningSecurityInformation;
    if (arrayOfByte == null || arrayOfByte.length < 50)
      return null; 
    return Arrays.copyOfRange(arrayOfByte, 7, 50);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SmsCbEtwsInfo{warningType=");
    stringBuilder.append(this.mWarningType);
    stringBuilder.append(", emergencyUserAlert=");
    stringBuilder.append(this.mIsEmergencyUserAlert);
    stringBuilder.append(", activatePopup=");
    stringBuilder.append(this.mIsPopupAlert);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<SmsCbEtwsInfo> CREATOR = new Parcelable.Creator<SmsCbEtwsInfo>() {
      public SmsCbEtwsInfo createFromParcel(Parcel param1Parcel) {
        return new SmsCbEtwsInfo(param1Parcel);
      }
      
      public SmsCbEtwsInfo[] newArray(int param1Int) {
        return new SmsCbEtwsInfo[param1Int];
      }
    };
  
  public static final int ETWS_WARNING_TYPE_EARTHQUAKE = 0;
  
  public static final int ETWS_WARNING_TYPE_EARTHQUAKE_AND_TSUNAMI = 2;
  
  public static final int ETWS_WARNING_TYPE_OTHER_EMERGENCY = 4;
  
  public static final int ETWS_WARNING_TYPE_TEST_MESSAGE = 3;
  
  public static final int ETWS_WARNING_TYPE_TSUNAMI = 1;
  
  public static final int ETWS_WARNING_TYPE_UNKNOWN = -1;
  
  private final boolean mIsEmergencyUserAlert;
  
  private final boolean mIsPopupAlert;
  
  private final boolean mIsPrimary;
  
  private final byte[] mWarningSecurityInformation;
  
  private final int mWarningType;
  
  @Retention(RetentionPolicy.SOURCE)
  class WarningType implements Annotation {}
}
