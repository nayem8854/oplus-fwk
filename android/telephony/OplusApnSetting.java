package android.telephony;

import android.telephony.data.ApnSetting;
import android.text.TextUtils;
import android.util.ArrayMap;
import java.util.Map;
import java.util.Objects;

public class OplusApnSetting {
  private static final Map<String, Integer> APN_TYPE_STRING_MAP;
  
  private static final String FASTWEB = "apn.fastweb.it";
  
  private static final String LOG_TAG = "OplusApnSetting";
  
  private static final int UNSPECIFIED_INT = -1;
  
  static {
    ArrayMap<Object, Object> arrayMap = new ArrayMap<>();
    arrayMap.put("*", Integer.valueOf(255));
    APN_TYPE_STRING_MAP.put("default", Integer.valueOf(17));
    APN_TYPE_STRING_MAP.put("mms", Integer.valueOf(2));
    APN_TYPE_STRING_MAP.put("supl", Integer.valueOf(4));
    APN_TYPE_STRING_MAP.put("dun", Integer.valueOf(8));
    APN_TYPE_STRING_MAP.put("hipri", Integer.valueOf(16));
    APN_TYPE_STRING_MAP.put("fota", Integer.valueOf(32));
    APN_TYPE_STRING_MAP.put("ims", Integer.valueOf(64));
    APN_TYPE_STRING_MAP.put("cbs", Integer.valueOf(128));
    APN_TYPE_STRING_MAP.put("ia", Integer.valueOf(256));
    APN_TYPE_STRING_MAP.put("emergency", Integer.valueOf(512));
    APN_TYPE_STRING_MAP.put("mcx", Integer.valueOf(1024));
  }
  
  public static int oemGetApnTypesBitmaskFromString(String paramString1, String paramString2) {
    boolean bool = TextUtils.isEmpty(paramString1);
    int i = 0, j = 0;
    if (bool) {
      if (paramString2 != null && paramString2.equals("44010")) {
        Rlog.d("OplusApnSetting", "Add additional APN types for Rakuten MVNO.");
        i = 0;
        int n;
        for (arrayOfString = "default,mms,supl,dun,fota,cbs,hipri".split(","), n = arrayOfString.length; j < n; ) {
          paramString2 = arrayOfString[j];
          Integer integer = APN_TYPE_STRING_MAP.get(paramString2.toLowerCase());
          int i1 = i;
          if (integer != null)
            i1 = i | integer.intValue(); 
          j++;
          i = i1;
        } 
        return i;
      } 
      return 255;
    } 
    int m = 0;
    String[] arrayOfString;
    int k;
    for (arrayOfString = arrayOfString.split(","), k = arrayOfString.length, j = i; j < k; ) {
      paramString2 = arrayOfString[j];
      Integer integer = APN_TYPE_STRING_MAP.get(paramString2.toLowerCase());
      i = m;
      if (integer != null)
        i = m | integer.intValue(); 
      j++;
      m = i;
    } 
    return m;
  }
  
  public static boolean oemMergeApnIgnoreProtocolType(ApnSetting paramApnSetting1, ApnSetting paramApnSetting2) {
    boolean bool = Objects.equals(paramApnSetting1.getApnName(), "apn.fastweb.it");
    boolean bool1 = true;
    if (bool && Objects.equals(paramApnSetting2.getApnName(), "apn.fastweb.it")) {
      Rlog.d("OplusApnSetting", "Merge APN for Fastweb.");
      return true;
    } 
    if (!xorEqualsInt(paramApnSetting1.getProtocol(), paramApnSetting2.getProtocol()) || !xorEqualsInt(paramApnSetting1.getRoamingProtocol(), paramApnSetting2.getRoamingProtocol()))
      bool1 = false; 
    return bool1;
  }
  
  private static boolean xorEqualsInt(int paramInt1, int paramInt2) {
    return (paramInt1 == -1 || paramInt2 == -1 || 
      Objects.equals(Integer.valueOf(paramInt1), Integer.valueOf(paramInt2)));
  }
}
