package android.telephony;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INetworkServiceCallback extends IInterface {
  void onNetworkStateChanged() throws RemoteException;
  
  void onRequestNetworkRegistrationInfoComplete(int paramInt, NetworkRegistrationInfo paramNetworkRegistrationInfo) throws RemoteException;
  
  class Default implements INetworkServiceCallback {
    public void onRequestNetworkRegistrationInfoComplete(int param1Int, NetworkRegistrationInfo param1NetworkRegistrationInfo) throws RemoteException {}
    
    public void onNetworkStateChanged() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INetworkServiceCallback {
    private static final String DESCRIPTOR = "android.telephony.INetworkServiceCallback";
    
    static final int TRANSACTION_onNetworkStateChanged = 2;
    
    static final int TRANSACTION_onRequestNetworkRegistrationInfoComplete = 1;
    
    public Stub() {
      attachInterface(this, "android.telephony.INetworkServiceCallback");
    }
    
    public static INetworkServiceCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.INetworkServiceCallback");
      if (iInterface != null && iInterface instanceof INetworkServiceCallback)
        return (INetworkServiceCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onNetworkStateChanged";
      } 
      return "onRequestNetworkRegistrationInfoComplete";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.telephony.INetworkServiceCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("android.telephony.INetworkServiceCallback");
        onNetworkStateChanged();
        return true;
      } 
      param1Parcel1.enforceInterface("android.telephony.INetworkServiceCallback");
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        NetworkRegistrationInfo networkRegistrationInfo = (NetworkRegistrationInfo)NetworkRegistrationInfo.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onRequestNetworkRegistrationInfoComplete(param1Int1, (NetworkRegistrationInfo)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements INetworkServiceCallback {
      public static INetworkServiceCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.INetworkServiceCallback";
      }
      
      public void onRequestNetworkRegistrationInfoComplete(int param2Int, NetworkRegistrationInfo param2NetworkRegistrationInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.INetworkServiceCallback");
          parcel.writeInt(param2Int);
          if (param2NetworkRegistrationInfo != null) {
            parcel.writeInt(1);
            param2NetworkRegistrationInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && INetworkServiceCallback.Stub.getDefaultImpl() != null) {
            INetworkServiceCallback.Stub.getDefaultImpl().onRequestNetworkRegistrationInfoComplete(param2Int, param2NetworkRegistrationInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onNetworkStateChanged() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.INetworkServiceCallback");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && INetworkServiceCallback.Stub.getDefaultImpl() != null) {
            INetworkServiceCallback.Stub.getDefaultImpl().onNetworkStateChanged();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INetworkServiceCallback param1INetworkServiceCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INetworkServiceCallback != null) {
          Proxy.sDefaultImpl = param1INetworkServiceCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INetworkServiceCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
