package android.telephony.gsm;

import android.os.Bundle;
import android.telephony.CellLocation;

public class GsmCellLocation extends CellLocation {
  private int mCid;
  
  private int mLac;
  
  private int mPsc;
  
  public GsmCellLocation() {
    this.mLac = -1;
    this.mCid = -1;
    this.mPsc = -1;
  }
  
  public GsmCellLocation(Bundle paramBundle) {
    this.mLac = paramBundle.getInt("lac", -1);
    this.mCid = paramBundle.getInt("cid", -1);
    this.mPsc = paramBundle.getInt("psc", -1);
  }
  
  public int getLac() {
    return this.mLac;
  }
  
  public int getCid() {
    return this.mCid;
  }
  
  public int getPsc() {
    return this.mPsc;
  }
  
  public void setStateInvalid() {
    this.mLac = -1;
    this.mCid = -1;
    this.mPsc = -1;
  }
  
  public void setLacAndCid(int paramInt1, int paramInt2) {
    this.mLac = paramInt1;
    this.mCid = paramInt2;
  }
  
  public void setPsc(int paramInt) {
    this.mPsc = paramInt;
  }
  
  public int hashCode() {
    return this.mLac ^ this.mCid;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    try {
      GsmCellLocation gsmCellLocation = (GsmCellLocation)paramObject;
      if (paramObject == null)
        return false; 
      if (equalsHandlesNulls(Integer.valueOf(this.mLac), Integer.valueOf(gsmCellLocation.mLac)) && equalsHandlesNulls(Integer.valueOf(this.mCid), Integer.valueOf(gsmCellLocation.mCid))) {
        int i = this.mPsc;
        if (equalsHandlesNulls(Integer.valueOf(i), Integer.valueOf(gsmCellLocation.mPsc)))
          bool = true; 
      } 
      return bool;
    } catch (ClassCastException classCastException) {
      return false;
    } 
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[");
    stringBuilder.append(this.mLac);
    stringBuilder.append(",");
    stringBuilder.append(this.mCid);
    stringBuilder.append(",");
    stringBuilder.append(this.mPsc);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  private static boolean equalsHandlesNulls(Object paramObject1, Object paramObject2) {
    boolean bool;
    if (paramObject1 == null) {
      if (paramObject2 == null) {
        bool = true;
      } else {
        bool = false;
      } 
    } else {
      bool = paramObject1.equals(paramObject2);
    } 
    return bool;
  }
  
  public void fillInNotifierBundle(Bundle paramBundle) {
    paramBundle.putInt("lac", this.mLac);
    paramBundle.putInt("cid", this.mCid);
    paramBundle.putInt("psc", this.mPsc);
  }
  
  public boolean isEmpty() {
    boolean bool;
    if (this.mLac == -1 && this.mCid == -1 && this.mPsc == -1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
