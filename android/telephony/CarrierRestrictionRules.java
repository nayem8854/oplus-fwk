package android.telephony;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.service.carrier.CarrierIdentifier;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.UnaryOperator;

@SystemApi
public final class CarrierRestrictionRules implements Parcelable {
  public static final int CARRIER_RESTRICTION_DEFAULT_ALLOWED = 1;
  
  public static final int CARRIER_RESTRICTION_DEFAULT_NOT_ALLOWED = 0;
  
  private CarrierRestrictionRules() {
    this.mAllowedCarriers = new ArrayList<>();
    this.mExcludedCarriers = new ArrayList<>();
    this.mCarrierRestrictionDefault = 0;
    this.mMultiSimPolicy = 0;
  }
  
  private CarrierRestrictionRules(Parcel paramParcel) {
    this.mAllowedCarriers = new ArrayList<>();
    this.mExcludedCarriers = new ArrayList<>();
    paramParcel.readTypedList(this.mAllowedCarriers, CarrierIdentifier.CREATOR);
    paramParcel.readTypedList(this.mExcludedCarriers, CarrierIdentifier.CREATOR);
    this.mCarrierRestrictionDefault = paramParcel.readInt();
    this.mMultiSimPolicy = paramParcel.readInt();
  }
  
  public static Builder newBuilder() {
    return new Builder();
  }
  
  public boolean isAllCarriersAllowed() {
    boolean bool = this.mAllowedCarriers.isEmpty();
    boolean bool1 = true;
    if (!bool || !this.mExcludedCarriers.isEmpty() || this.mCarrierRestrictionDefault != 1)
      bool1 = false; 
    return bool1;
  }
  
  public List<CarrierIdentifier> getAllowedCarriers() {
    return this.mAllowedCarriers;
  }
  
  public List<CarrierIdentifier> getExcludedCarriers() {
    return this.mExcludedCarriers;
  }
  
  public int getDefaultCarrierRestriction() {
    return this.mCarrierRestrictionDefault;
  }
  
  public int getMultiSimPolicy() {
    return this.mMultiSimPolicy;
  }
  
  public List<Boolean> areCarrierIdentifiersAllowed(List<CarrierIdentifier> paramList) {
    ArrayList<Boolean> arrayList = new ArrayList(paramList.size());
    byte b = 0;
    while (true) {
      int i = paramList.size();
      boolean bool1 = true, bool2 = true;
      if (b < i) {
        boolean bool3 = isCarrierIdInList(paramList.get(b), this.mAllowedCarriers);
        boolean bool4 = isCarrierIdInList(paramList.get(b), this.mExcludedCarriers);
        if (this.mCarrierRestrictionDefault == 0) {
          if (!bool3 || bool4)
            bool2 = false; 
          arrayList.add(Boolean.valueOf(bool2));
        } else {
          bool2 = bool1;
          if (bool4) {
            bool2 = bool1;
            if (!bool3)
              bool2 = false; 
          } 
          arrayList.add(Boolean.valueOf(bool2));
        } 
        b++;
        continue;
      } 
      break;
    } 
    if (this.mMultiSimPolicy == 1)
      for (Iterator<Boolean> iterator = arrayList.iterator(); iterator.hasNext(); ) {
        boolean bool = ((Boolean)iterator.next()).booleanValue();
        if (bool) {
          arrayList.replaceAll((UnaryOperator<Boolean>)_$$Lambda$CarrierRestrictionRules$LmZXhiwgp1w_MAHEuZsMgdCVMiU.INSTANCE);
          break;
        } 
      }  
    return arrayList;
  }
  
  private static boolean isCarrierIdInList(CarrierIdentifier paramCarrierIdentifier, List<CarrierIdentifier> paramList) {
    for (CarrierIdentifier carrierIdentifier : paramList) {
      if (!patternMatch(paramCarrierIdentifier.getMcc(), carrierIdentifier.getMcc()) || 
        !patternMatch(paramCarrierIdentifier.getMnc(), carrierIdentifier.getMnc()))
        continue; 
      String str2 = convertNullToEmpty(carrierIdentifier.getSpn());
      String str3 = convertNullToEmpty(paramCarrierIdentifier.getSpn());
      if (!str2.isEmpty() && 
        !patternMatch(str3, str2))
        continue; 
      str2 = convertNullToEmpty(carrierIdentifier.getImsi());
      str3 = convertNullToEmpty(paramCarrierIdentifier.getImsi());
      str3 = str3.substring(0, Math.min(str3.length(), str2.length()));
      if (!patternMatch(str3, str2))
        continue; 
      str2 = convertNullToEmpty(carrierIdentifier.getGid1());
      str3 = convertNullToEmpty(paramCarrierIdentifier.getGid1());
      str3 = str3.substring(0, Math.min(str3.length(), str2.length()));
      if (!patternMatch(str3, str2))
        continue; 
      String str1 = convertNullToEmpty(carrierIdentifier.getGid2());
      str2 = convertNullToEmpty(paramCarrierIdentifier.getGid2());
      str2 = str2.substring(0, Math.min(str2.length(), str1.length()));
      if (!patternMatch(str2, str1))
        continue; 
      return true;
    } 
    return false;
  }
  
  private static String convertNullToEmpty(String paramString) {
    return Objects.toString(paramString, "");
  }
  
  private static boolean patternMatch(String paramString1, String paramString2) {
    if (paramString1.length() != paramString2.length())
      return false; 
    paramString1 = paramString1.toLowerCase();
    paramString2 = paramString2.toLowerCase();
    for (byte b = 0; b < paramString2.length(); b++) {
      if (paramString2.charAt(b) != paramString1.charAt(b) && 
        paramString2.charAt(b) != '?')
        return false; 
    } 
    return true;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeTypedList(this.mAllowedCarriers);
    paramParcel.writeTypedList(this.mExcludedCarriers);
    paramParcel.writeInt(this.mCarrierRestrictionDefault);
    paramParcel.writeInt(this.mMultiSimPolicy);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<CarrierRestrictionRules> CREATOR = new Parcelable.Creator<CarrierRestrictionRules>() {
      public CarrierRestrictionRules createFromParcel(Parcel param1Parcel) {
        return new CarrierRestrictionRules(param1Parcel);
      }
      
      public CarrierRestrictionRules[] newArray(int param1Int) {
        return new CarrierRestrictionRules[param1Int];
      }
    };
  
  public static final int MULTISIM_POLICY_NONE = 0;
  
  public static final int MULTISIM_POLICY_ONE_VALID_SIM_MUST_BE_PRESENT = 1;
  
  private static final char WILD_CHARACTER = '?';
  
  private List<CarrierIdentifier> mAllowedCarriers;
  
  private int mCarrierRestrictionDefault;
  
  private List<CarrierIdentifier> mExcludedCarriers;
  
  private int mMultiSimPolicy;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CarrierRestrictionRules(allowed:");
    stringBuilder.append(this.mAllowedCarriers);
    stringBuilder.append(", excluded:");
    stringBuilder.append(this.mExcludedCarriers);
    stringBuilder.append(", default:");
    stringBuilder.append(this.mCarrierRestrictionDefault);
    stringBuilder.append(", multisim policy:");
    stringBuilder.append(this.mMultiSimPolicy);
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
  
  class Builder {
    private final CarrierRestrictionRules mRules;
    
    public Builder() {
      this.mRules = new CarrierRestrictionRules();
    }
    
    public CarrierRestrictionRules build() {
      return this.mRules;
    }
    
    public Builder setAllCarriersAllowed() {
      this.mRules.mAllowedCarriers.clear();
      this.mRules.mExcludedCarriers.clear();
      CarrierRestrictionRules.access$402(this.mRules, 1);
      return this;
    }
    
    public Builder setAllowedCarriers(List<CarrierIdentifier> param1List) {
      CarrierRestrictionRules.access$202(this.mRules, new ArrayList<>(param1List));
      return this;
    }
    
    public Builder setExcludedCarriers(List<CarrierIdentifier> param1List) {
      CarrierRestrictionRules.access$302(this.mRules, new ArrayList<>(param1List));
      return this;
    }
    
    public Builder setDefaultCarrierRestriction(int param1Int) {
      CarrierRestrictionRules.access$402(this.mRules, param1Int);
      return this;
    }
    
    public Builder setMultiSimPolicy(int param1Int) {
      CarrierRestrictionRules.access$502(this.mRules, param1Int);
      return this;
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class CarrierRestrictionDefault implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class MultiSimPolicy implements Annotation {}
}
