package android.telephony;

import android.annotation.SystemApi;
import android.content.Context;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.RemoteException;
import android.telephony.mbms.DownloadProgressListener;
import android.telephony.mbms.DownloadRequest;
import android.telephony.mbms.DownloadStatusListener;
import android.telephony.mbms.FileInfo;
import android.telephony.mbms.InternalDownloadProgressListener;
import android.telephony.mbms.InternalDownloadSessionCallback;
import android.telephony.mbms.InternalDownloadStatusListener;
import android.telephony.mbms.MbmsDownloadSessionCallback;
import android.telephony.mbms.MbmsTempFileProvider;
import android.telephony.mbms.MbmsUtils;
import android.telephony.mbms.vendor.IMbmsDownloadService;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class MbmsDownloadSession implements AutoCloseable {
  private static final String LOG_TAG = MbmsDownloadSession.class.getSimpleName();
  
  private static AtomicBoolean sIsInitialized = new AtomicBoolean(false);
  
  private int mSubscriptionId = -1;
  
  private IBinder.DeathRecipient mDeathRecipient = (IBinder.DeathRecipient)new Object(this);
  
  private AtomicReference<IMbmsDownloadService> mService = new AtomicReference<>(null);
  
  private final Map<DownloadStatusListener, InternalDownloadStatusListener> mInternalDownloadStatusListeners = new HashMap<>();
  
  private final Map<DownloadProgressListener, InternalDownloadProgressListener> mInternalDownloadProgressListeners = new HashMap<>();
  
  public static final String DEFAULT_TOP_LEVEL_TEMP_DIRECTORY = "androidMbmsTempFileRoot";
  
  private static final String DESTINATION_SANITY_CHECK_FILE_NAME = "destinationSanityCheckFile";
  
  public static final String EXTRA_MBMS_COMPLETED_FILE_URI = "android.telephony.extra.MBMS_COMPLETED_FILE_URI";
  
  public static final String EXTRA_MBMS_DOWNLOAD_REQUEST = "android.telephony.extra.MBMS_DOWNLOAD_REQUEST";
  
  public static final String EXTRA_MBMS_DOWNLOAD_RESULT = "android.telephony.extra.MBMS_DOWNLOAD_RESULT";
  
  public static final String EXTRA_MBMS_FILE_INFO = "android.telephony.extra.MBMS_FILE_INFO";
  
  @SystemApi
  public static final String MBMS_DOWNLOAD_SERVICE_ACTION = "android.telephony.action.EmbmsDownload";
  
  public static final String MBMS_DOWNLOAD_SERVICE_OVERRIDE_METADATA = "mbms-download-service-override";
  
  public static final int RESULT_CANCELLED = 2;
  
  public static final int RESULT_DOWNLOAD_FAILURE = 6;
  
  public static final int RESULT_EXPIRED = 3;
  
  public static final int RESULT_FILE_ROOT_UNREACHABLE = 8;
  
  public static final int RESULT_IO_ERROR = 4;
  
  public static final int RESULT_OUT_OF_STORAGE = 7;
  
  public static final int RESULT_SERVICE_ID_NOT_DEFINED = 5;
  
  public static final int RESULT_SUCCESSFUL = 1;
  
  public static final int STATUS_ACTIVELY_DOWNLOADING = 1;
  
  public static final int STATUS_PENDING_DOWNLOAD = 2;
  
  public static final int STATUS_PENDING_DOWNLOAD_WINDOW = 4;
  
  public static final int STATUS_PENDING_REPAIR = 3;
  
  public static final int STATUS_UNKNOWN = 0;
  
  private final Context mContext;
  
  private final InternalDownloadSessionCallback mInternalCallback;
  
  private ServiceConnection mServiceConnection;
  
  private MbmsDownloadSession(Context paramContext, Executor paramExecutor, int paramInt, MbmsDownloadSessionCallback paramMbmsDownloadSessionCallback) {
    this.mContext = paramContext;
    this.mSubscriptionId = paramInt;
    this.mInternalCallback = new InternalDownloadSessionCallback(paramMbmsDownloadSessionCallback, paramExecutor);
  }
  
  public static MbmsDownloadSession create(Context paramContext, Executor paramExecutor, MbmsDownloadSessionCallback paramMbmsDownloadSessionCallback) {
    return create(paramContext, paramExecutor, SubscriptionManager.getDefaultSubscriptionId(), paramMbmsDownloadSessionCallback);
  }
  
  public static MbmsDownloadSession create(Context paramContext, Executor paramExecutor, final int result, final MbmsDownloadSessionCallback callback) {
    if (sIsInitialized.compareAndSet(false, true)) {
      MbmsDownloadSession mbmsDownloadSession = new MbmsDownloadSession(paramContext, paramExecutor, result, callback);
      result = mbmsDownloadSession.bindAndInitialize();
      if (result != 0) {
        sIsInitialized.set(false);
        paramExecutor.execute(new Runnable() {
              final MbmsDownloadSessionCallback val$callback;
              
              final int val$result;
              
              public void run() {
                callback.onError(result, null);
              }
            });
        return null;
      } 
      return mbmsDownloadSession;
    } 
    throw new IllegalStateException("Cannot have two active instances");
  }
  
  private int bindAndInitialize() {
    Object object = new Object(this);
    return MbmsUtils.startBinding(this.mContext, "android.telephony.action.EmbmsDownload", (ServiceConnection)object);
  }
  
  public void requestUpdateFileServices(List<String> paramList) {
    IMbmsDownloadService iMbmsDownloadService = this.mService.get();
    if (iMbmsDownloadService != null) {
      try {
        int i = iMbmsDownloadService.requestUpdateFileServices(this.mSubscriptionId, paramList);
        if (i != -1) {
          if (i != 0)
            sendErrorToApp(i, null); 
        } else {
          close();
          IllegalStateException illegalStateException = new IllegalStateException();
          this("Middleware must not return an unknown error code");
          throw illegalStateException;
        } 
      } catch (RemoteException remoteException) {
        Log.w(LOG_TAG, "Remote process died");
        this.mService.set(null);
        sIsInitialized.set(false);
        sendErrorToApp(3, null);
      } 
      return;
    } 
    throw new IllegalStateException("Middleware not yet bound");
  }
  
  public void setTempFileRootDirectory(File paramFile) {
    IMbmsDownloadService iMbmsDownloadService = this.mService.get();
    if (iMbmsDownloadService != null)
      try {
        validateTempFileRootSanity(paramFile);
        try {
          String str = paramFile.getCanonicalPath();
          try {
            int i = iMbmsDownloadService.setTempFileRootDirectory(this.mSubscriptionId, str);
            if (i != -1) {
              if (i != 0) {
                sendErrorToApp(i, null);
                return;
              } 
              SharedPreferences sharedPreferences = this.mContext.getSharedPreferences("MbmsTempFileRootPrefs", 0);
              sharedPreferences.edit().putString("mbms_temp_file_root", str).apply();
              return;
            } 
            close();
            IllegalStateException illegalStateException = new IllegalStateException();
            this("Middleware must not return an unknown error code");
            throw illegalStateException;
          } catch (RemoteException remoteException) {
            this.mService.set(null);
            sIsInitialized.set(false);
            sendErrorToApp(3, null);
            return;
          } 
        } catch (IOException iOException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unable to canonicalize the provided path: ");
          stringBuilder.append(iOException);
          throw new IllegalArgumentException(stringBuilder.toString());
        } 
      } catch (IOException iOException) {
        throw new IllegalStateException("Got IOException checking directory sanity");
      }  
    throw new IllegalStateException("Middleware not yet bound");
  }
  
  private void validateTempFileRootSanity(File paramFile) throws IOException {
    if (paramFile.exists()) {
      if (paramFile.isDirectory()) {
        String str = paramFile.getCanonicalPath();
        if (!this.mContext.getDataDir().getCanonicalPath().equals(str)) {
          if (!this.mContext.getCacheDir().getCanonicalPath().equals(str)) {
            if (!this.mContext.getFilesDir().getCanonicalPath().equals(str))
              return; 
            throw new IllegalArgumentException("Temp file root cannot be your files dir");
          } 
          throw new IllegalArgumentException("Temp file root cannot be your cache dir");
        } 
        throw new IllegalArgumentException("Temp file root cannot be your data dir");
      } 
      throw new IllegalArgumentException("Provided File is not a directory");
    } 
    throw new IllegalArgumentException("Provided directory does not exist");
  }
  
  public File getTempFileRootDirectory() {
    SharedPreferences sharedPreferences = this.mContext.getSharedPreferences("MbmsTempFileRootPrefs", 0);
    String str = sharedPreferences.getString("mbms_temp_file_root", null);
    if (str != null)
      return new File(str); 
    return null;
  }
  
  public void download(DownloadRequest paramDownloadRequest) {
    IMbmsDownloadService iMbmsDownloadService = this.mService.get();
    if (iMbmsDownloadService != null) {
      SharedPreferences sharedPreferences = this.mContext.getSharedPreferences("MbmsTempFileRootPrefs", 0);
      if (sharedPreferences.getString("mbms_temp_file_root", null) == null) {
        File file = new File(this.mContext.getFilesDir(), "androidMbmsTempFileRoot");
        file.mkdirs();
        setTempFileRootDirectory(file);
      } 
      checkDownloadRequestDestination(paramDownloadRequest);
      try {
        int i = iMbmsDownloadService.download(paramDownloadRequest);
        if (i == 0) {
          writeDownloadRequestToken(paramDownloadRequest);
        } else if (i != -1) {
          sendErrorToApp(i, null);
        } else {
          close();
          IllegalStateException illegalStateException = new IllegalStateException();
          this("Middleware must not return an unknown error code");
          throw illegalStateException;
        } 
      } catch (RemoteException remoteException) {
        this.mService.set(null);
        sIsInitialized.set(false);
        sendErrorToApp(3, null);
      } 
      return;
    } 
    throw new IllegalStateException("Middleware not yet bound");
  }
  
  public List<DownloadRequest> listPendingDownloads() {
    IMbmsDownloadService iMbmsDownloadService = this.mService.get();
    if (iMbmsDownloadService != null)
      try {
        return iMbmsDownloadService.listPendingDownloads(this.mSubscriptionId);
      } catch (RemoteException remoteException) {
        this.mService.set(null);
        sIsInitialized.set(false);
        sendErrorToApp(3, null);
        return Collections.emptyList();
      }  
    throw new IllegalStateException("Middleware not yet bound");
  }
  
  public void addStatusListener(DownloadRequest paramDownloadRequest, Executor paramExecutor, DownloadStatusListener paramDownloadStatusListener) {
    IMbmsDownloadService iMbmsDownloadService = this.mService.get();
    if (iMbmsDownloadService != null) {
      InternalDownloadStatusListener internalDownloadStatusListener = new InternalDownloadStatusListener(paramDownloadStatusListener, paramExecutor);
      try {
        int i = iMbmsDownloadService.addStatusListener(paramDownloadRequest, internalDownloadStatusListener);
        if (i != -1) {
          if (i != 0) {
            if (i != 402) {
              sendErrorToApp(i, null);
              return;
            } 
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
            this("Unknown download request.");
            throw illegalArgumentException;
          } 
          this.mInternalDownloadStatusListeners.put(paramDownloadStatusListener, internalDownloadStatusListener);
          return;
        } 
        close();
        IllegalStateException illegalStateException = new IllegalStateException();
        this("Middleware must not return an unknown error code");
        throw illegalStateException;
      } catch (RemoteException remoteException) {
        this.mService.set(null);
        sIsInitialized.set(false);
        sendErrorToApp(3, null);
        return;
      } 
    } 
    throw new IllegalStateException("Middleware not yet bound");
  }
  
  public void removeStatusListener(DownloadRequest paramDownloadRequest, DownloadStatusListener paramDownloadStatusListener) {
    try {
      IMbmsDownloadService iMbmsDownloadService = this.mService.get();
      if (iMbmsDownloadService != null) {
        Map<DownloadStatusListener, InternalDownloadStatusListener> map = this.mInternalDownloadStatusListeners;
        InternalDownloadStatusListener internalDownloadStatusListener = map.get(paramDownloadStatusListener);
        if (internalDownloadStatusListener != null)
          try {
            int i = iMbmsDownloadService.removeStatusListener(paramDownloadRequest, internalDownloadStatusListener);
            if (i != -1) {
              if (i != 0) {
                if (i != 402) {
                  sendErrorToApp(i, null);
                  return;
                } 
                IllegalArgumentException illegalArgumentException1 = new IllegalArgumentException();
                this("Unknown download request.");
                throw illegalArgumentException1;
              } 
              return;
            } 
            close();
            IllegalStateException illegalStateException1 = new IllegalStateException();
            this("Middleware must not return an unknown error code");
            throw illegalStateException1;
          } catch (RemoteException remoteException) {
            this.mService.set(null);
            sIsInitialized.set(false);
            sendErrorToApp(3, null);
            return;
          }  
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        this("Provided listener was never registered");
        throw illegalArgumentException;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Middleware not yet bound");
      throw illegalStateException;
    } finally {
      Map<DownloadStatusListener, InternalDownloadStatusListener> map = this.mInternalDownloadStatusListeners;
      InternalDownloadStatusListener internalDownloadStatusListener = map.remove(paramDownloadStatusListener);
      if (internalDownloadStatusListener != null)
        internalDownloadStatusListener.stop(); 
    } 
  }
  
  public void addProgressListener(DownloadRequest paramDownloadRequest, Executor paramExecutor, DownloadProgressListener paramDownloadProgressListener) {
    IMbmsDownloadService iMbmsDownloadService = this.mService.get();
    if (iMbmsDownloadService != null) {
      InternalDownloadProgressListener internalDownloadProgressListener = new InternalDownloadProgressListener(paramDownloadProgressListener, paramExecutor);
      try {
        int i = iMbmsDownloadService.addProgressListener(paramDownloadRequest, internalDownloadProgressListener);
        if (i != -1) {
          if (i != 0) {
            if (i != 402) {
              sendErrorToApp(i, null);
              return;
            } 
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
            this("Unknown download request.");
            throw illegalArgumentException;
          } 
          this.mInternalDownloadProgressListeners.put(paramDownloadProgressListener, internalDownloadProgressListener);
          return;
        } 
        close();
        IllegalStateException illegalStateException = new IllegalStateException();
        this("Middleware must not return an unknown error code");
        throw illegalStateException;
      } catch (RemoteException remoteException) {
        this.mService.set(null);
        sIsInitialized.set(false);
        sendErrorToApp(3, null);
        return;
      } 
    } 
    throw new IllegalStateException("Middleware not yet bound");
  }
  
  public void removeProgressListener(DownloadRequest paramDownloadRequest, DownloadProgressListener paramDownloadProgressListener) {
    try {
      IMbmsDownloadService iMbmsDownloadService = this.mService.get();
      if (iMbmsDownloadService != null) {
        Map<DownloadProgressListener, InternalDownloadProgressListener> map = this.mInternalDownloadProgressListeners;
        InternalDownloadProgressListener internalDownloadProgressListener = map.get(paramDownloadProgressListener);
        if (internalDownloadProgressListener != null)
          try {
            int i = iMbmsDownloadService.removeProgressListener(paramDownloadRequest, internalDownloadProgressListener);
            if (i != -1) {
              if (i != 0) {
                if (i != 402) {
                  sendErrorToApp(i, null);
                  return;
                } 
                IllegalArgumentException illegalArgumentException1 = new IllegalArgumentException();
                this("Unknown download request.");
                throw illegalArgumentException1;
              } 
              return;
            } 
            close();
            IllegalStateException illegalStateException1 = new IllegalStateException();
            this("Middleware must not return an unknown error code");
            throw illegalStateException1;
          } catch (RemoteException remoteException) {
            this.mService.set(null);
            sIsInitialized.set(false);
            sendErrorToApp(3, null);
            return;
          }  
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        this("Provided listener was never registered");
        throw illegalArgumentException;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Middleware not yet bound");
      throw illegalStateException;
    } finally {
      Map<DownloadProgressListener, InternalDownloadProgressListener> map = this.mInternalDownloadProgressListeners;
      InternalDownloadProgressListener internalDownloadProgressListener = map.remove(paramDownloadProgressListener);
      if (internalDownloadProgressListener != null)
        internalDownloadProgressListener.stop(); 
    } 
  }
  
  public void cancelDownload(DownloadRequest paramDownloadRequest) {
    IMbmsDownloadService iMbmsDownloadService = this.mService.get();
    if (iMbmsDownloadService != null) {
      try {
        int i = iMbmsDownloadService.cancelDownload(paramDownloadRequest);
        if (i != -1) {
          if (i != 0) {
            sendErrorToApp(i, null);
          } else {
            deleteDownloadRequestToken(paramDownloadRequest);
          } 
        } else {
          close();
          IllegalStateException illegalStateException = new IllegalStateException();
          this("Middleware must not return an unknown error code");
          throw illegalStateException;
        } 
      } catch (RemoteException remoteException) {
        this.mService.set(null);
        sIsInitialized.set(false);
        sendErrorToApp(3, null);
      } 
      return;
    } 
    throw new IllegalStateException("Middleware not yet bound");
  }
  
  public void requestDownloadState(DownloadRequest paramDownloadRequest, FileInfo paramFileInfo) {
    IMbmsDownloadService iMbmsDownloadService = this.mService.get();
    if (iMbmsDownloadService != null) {
      try {
        int i = iMbmsDownloadService.requestDownloadState(paramDownloadRequest, paramFileInfo);
        if (i != -1) {
          if (i != 0)
            if (i != 402) {
              if (i != 403) {
                sendErrorToApp(i, null);
              } else {
                IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
                this("Unknown file.");
                throw illegalArgumentException;
              } 
            } else {
              IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
              this("Unknown download request.");
              throw illegalArgumentException;
            }  
        } else {
          close();
          IllegalStateException illegalStateException = new IllegalStateException();
          this("Middleware must not return an unknown error code");
          throw illegalStateException;
        } 
      } catch (RemoteException remoteException) {
        this.mService.set(null);
        sIsInitialized.set(false);
        sendErrorToApp(3, null);
      } 
      return;
    } 
    throw new IllegalStateException("Middleware not yet bound");
  }
  
  public void resetDownloadKnowledge(DownloadRequest paramDownloadRequest) {
    IMbmsDownloadService iMbmsDownloadService = this.mService.get();
    if (iMbmsDownloadService != null) {
      try {
        int i = iMbmsDownloadService.resetDownloadKnowledge(paramDownloadRequest);
        if (i != -1) {
          if (i != 0)
            if (i != 402) {
              sendErrorToApp(i, null);
            } else {
              IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
              this("Unknown download request.");
              throw illegalArgumentException;
            }  
        } else {
          close();
          IllegalStateException illegalStateException = new IllegalStateException();
          this("Middleware must not return an unknown error code");
          throw illegalStateException;
        } 
      } catch (RemoteException remoteException) {
        this.mService.set(null);
        sIsInitialized.set(false);
        sendErrorToApp(3, null);
      } 
      return;
    } 
    throw new IllegalStateException("Middleware not yet bound");
  }
  
  public void close() {
    try {
      IMbmsDownloadService iMbmsDownloadService = this.mService.get();
      if (iMbmsDownloadService == null || this.mServiceConnection == null) {
        Log.i(LOG_TAG, "Service already dead");
        this.mService.set(null);
        sIsInitialized.set(false);
        this.mServiceConnection = null;
        this.mInternalCallback.stop();
        return;
      } 
      iMbmsDownloadService.dispose(this.mSubscriptionId);
      this.mContext.unbindService(this.mServiceConnection);
    } catch (RemoteException remoteException) {
      Log.i(LOG_TAG, "Remote exception while disposing of service");
    } finally {
      Exception exception;
    } 
    this.mService.set(null);
    sIsInitialized.set(false);
    this.mServiceConnection = null;
    this.mInternalCallback.stop();
  }
  
  private void writeDownloadRequestToken(DownloadRequest paramDownloadRequest) {
    StringBuilder stringBuilder;
    File file = getDownloadRequestTokenPath(paramDownloadRequest);
    if (!file.getParentFile().exists())
      file.getParentFile().mkdirs(); 
    if (file.exists()) {
      String str = LOG_TAG;
      stringBuilder = new StringBuilder();
      stringBuilder.append("Download token ");
      stringBuilder.append(file.getName());
      stringBuilder.append(" already exists");
      Log.w(str, stringBuilder.toString());
      return;
    } 
    try {
      if (file.createNewFile())
        return; 
      RuntimeException runtimeException = new RuntimeException();
      StringBuilder stringBuilder1 = new StringBuilder();
      this();
      stringBuilder1.append("Failed to create download token for request ");
      stringBuilder1.append(stringBuilder);
      stringBuilder1.append(". Token location is ");
      stringBuilder1.append(file.getPath());
      this(stringBuilder1.toString());
      throw runtimeException;
    } catch (IOException iOException) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Failed to create download token for request ");
      stringBuilder1.append(stringBuilder);
      stringBuilder1.append(" due to IOException ");
      stringBuilder1.append(iOException);
      stringBuilder1.append(". Attempted to write to ");
      stringBuilder1.append(file.getPath());
      throw new RuntimeException(stringBuilder1.toString());
    } 
  }
  
  private void deleteDownloadRequestToken(DownloadRequest paramDownloadRequest) {
    File file = getDownloadRequestTokenPath(paramDownloadRequest);
    if (!file.isFile()) {
      String str = LOG_TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Attempting to delete non-existent download token at ");
      stringBuilder.append(file);
      Log.w(str, stringBuilder.toString());
      return;
    } 
    if (!file.delete()) {
      String str = LOG_TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Couldn't delete download token at ");
      stringBuilder.append(file);
      Log.w(str, stringBuilder.toString());
    } 
  }
  
  private void checkDownloadRequestDestination(DownloadRequest paramDownloadRequest) {
    File file = new File(paramDownloadRequest.getDestinationUri().getPath());
    if (file.isDirectory()) {
      Exception exception;
      File file1 = new File(MbmsTempFileProvider.getEmbmsTempFileDir(this.mContext), "destinationSanityCheckFile");
      file = new File(file, "destinationSanityCheckFile");
      try {
        if (!file1.exists())
          file1.createNewFile(); 
        boolean bool = file1.renameTo(file);
        if (bool) {
          file1.delete();
          file.delete();
          return;
        } 
        exception = new IllegalArgumentException();
        this("Destination provided in the download request is invalid -- files in the temp file directory cannot be directly moved there.");
        throw exception;
      } catch (IOException null) {
        IllegalStateException illegalStateException = new IllegalStateException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Got IOException while testing out the destination: ");
        stringBuilder.append(exception);
        this(stringBuilder.toString());
        throw illegalStateException;
      } finally {}
      file1.delete();
      file.delete();
      throw exception;
    } 
    throw new IllegalArgumentException("The destination path must be a directory");
  }
  
  private File getDownloadRequestTokenPath(DownloadRequest paramDownloadRequest) {
    Context context = this.mContext;
    String str2 = paramDownloadRequest.getFileServiceId();
    File file = MbmsUtils.getEmbmsTempFileDirForService(context, str2);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramDownloadRequest.getHash());
    stringBuilder.append(".download_token");
    String str1 = stringBuilder.toString();
    return new File(file, str1);
  }
  
  private void sendErrorToApp(int paramInt, String paramString) {
    this.mInternalCallback.onError(paramInt, paramString);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface DownloadResultCode {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface DownloadStatus {}
}
