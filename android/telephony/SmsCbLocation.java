package android.telephony;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class SmsCbLocation implements Parcelable {
  public SmsCbLocation() {
    this.mPlmn = "";
    this.mLac = -1;
    this.mCid = -1;
  }
  
  public SmsCbLocation(String paramString) {
    this.mPlmn = paramString;
    this.mLac = -1;
    this.mCid = -1;
  }
  
  public SmsCbLocation(String paramString, int paramInt1, int paramInt2) {
    this.mPlmn = paramString;
    this.mLac = paramInt1;
    this.mCid = paramInt2;
  }
  
  public SmsCbLocation(Parcel paramParcel) {
    this.mPlmn = paramParcel.readString();
    this.mLac = paramParcel.readInt();
    this.mCid = paramParcel.readInt();
  }
  
  public String getPlmn() {
    return this.mPlmn;
  }
  
  public int getLac() {
    return this.mLac;
  }
  
  public int getCid() {
    return this.mCid;
  }
  
  public int hashCode() {
    int i = this.mPlmn.hashCode();
    int j = this.mLac;
    int k = this.mCid;
    return (i * 31 + j) * 31 + k;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (paramObject == this)
      return true; 
    if (paramObject == null || !(paramObject instanceof SmsCbLocation))
      return false; 
    paramObject = paramObject;
    if (!this.mPlmn.equals(((SmsCbLocation)paramObject).mPlmn) || this.mLac != ((SmsCbLocation)paramObject).mLac || this.mCid != ((SmsCbLocation)paramObject).mCid)
      bool = false; 
    return bool;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append('[');
    stringBuilder.append(this.mPlmn);
    stringBuilder.append(',');
    stringBuilder.append(this.mLac);
    stringBuilder.append(',');
    stringBuilder.append(this.mCid);
    stringBuilder.append(']');
    return stringBuilder.toString();
  }
  
  public boolean isInLocationArea(SmsCbLocation paramSmsCbLocation) {
    int i = this.mCid;
    if (i != -1 && i != paramSmsCbLocation.mCid)
      return false; 
    i = this.mLac;
    if (i != -1 && i != paramSmsCbLocation.mLac)
      return false; 
    return this.mPlmn.equals(paramSmsCbLocation.mPlmn);
  }
  
  public boolean isInLocationArea(String paramString, int paramInt1, int paramInt2) {
    if (!this.mPlmn.equals(paramString))
      return false; 
    int i = this.mLac;
    if (i != -1 && i != paramInt1)
      return false; 
    paramInt1 = this.mCid;
    if (paramInt1 != -1 && paramInt1 != paramInt2)
      return false; 
    return true;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mPlmn);
    paramParcel.writeInt(this.mLac);
    paramParcel.writeInt(this.mCid);
  }
  
  public static final Parcelable.Creator<SmsCbLocation> CREATOR = new Parcelable.Creator<SmsCbLocation>() {
      public SmsCbLocation createFromParcel(Parcel param1Parcel) {
        return new SmsCbLocation(param1Parcel);
      }
      
      public SmsCbLocation[] newArray(int param1Int) {
        return new SmsCbLocation[param1Int];
      }
    };
  
  private final int mCid;
  
  private final int mLac;
  
  private final String mPlmn;
  
  public int describeContents() {
    return 0;
  }
}
