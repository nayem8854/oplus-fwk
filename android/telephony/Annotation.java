package android.telephony;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Annotation {
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ApnType {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface CallState {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface CarrierPrivilegeStatus {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface DataActivityType {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface DataFailureCause {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface DataState {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface DisconnectCauses {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ImsAudioCodec {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface NetworkType {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface OverrideNetworkType {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface PreciseCallStates {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface PreciseDisconnectCauses {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface RadioPowerState {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SimActivationState {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SrvccState {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface UiccAppType {}
}
