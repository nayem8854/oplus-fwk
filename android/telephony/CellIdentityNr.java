package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.gsm.GsmCellLocation;
import android.util.ArraySet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public final class CellIdentityNr extends CellIdentity {
  public CellIdentityNr() {
    super("CellIdentityNr", 6, null, null, null, null);
    this.mNrArfcn = Integer.MAX_VALUE;
    this.mPci = Integer.MAX_VALUE;
    this.mTac = Integer.MAX_VALUE;
    this.mNci = 2147483647L;
    this.mBands = new int[0];
    this.mAdditionalPlmns = new ArraySet<>();
  }
  
  public CellIdentityNr(int paramInt1, int paramInt2, int paramInt3, int[] paramArrayOfint, String paramString1, String paramString2, long paramLong, String paramString3, String paramString4, Collection<String> paramCollection) {
    super("CellIdentityNr", 6, paramString1, paramString2, paramString3, paramString4);
    this.mPci = inRangeOrUnavailable(paramInt1, 0, 1007);
    this.mTac = inRangeOrUnavailable(paramInt2, 0, 65535);
    this.mNrArfcn = inRangeOrUnavailable(paramInt3, 0, 3279165);
    this.mBands = paramArrayOfint;
    this.mNci = inRangeOrUnavailable(paramLong, 0L, 68719476735L);
    this.mAdditionalPlmns = new ArraySet<>(paramCollection.size());
    for (String paramString1 : paramCollection) {
      if (isValidPlmn(paramString1))
        this.mAdditionalPlmns.add(paramString1); 
    } 
    updateGlobalCellId();
  }
  
  public CellIdentityNr(android.hardware.radio.V1_4.CellIdentityNr paramCellIdentityNr) {
    this(i, j, k, new int[0], str2, str3, l, str4, str1, arraySet);
  }
  
  public CellIdentityNr(android.hardware.radio.V1_5.CellIdentityNr paramCellIdentityNr) {
    this(i, j, k, arrayOfInt, str1, str2, l, str3, str4, arrayList);
  }
  
  public CellIdentityNr sanitizeLocationInfo() {
    return new CellIdentityNr(2147483647, 2147483647, this.mNrArfcn, this.mBands, this.mMccStr, this.mMncStr, 2147483647L, this.mAlphaLong, this.mAlphaShort, this.mAdditionalPlmns);
  }
  
  protected void updateGlobalCellId() {
    this.mGlobalCellId = null;
    String str = getPlmn();
    if (str == null)
      return; 
    if (this.mNci == Long.MAX_VALUE)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str);
    stringBuilder.append(String.format("%09x", new Object[] { Long.valueOf(this.mNci) }));
    this.mGlobalCellId = stringBuilder.toString();
  }
  
  public CellLocation asCellLocation() {
    return new GsmCellLocation();
  }
  
  public int hashCode() {
    int i = super.hashCode(), j = this.mPci, k = this.mTac, m = this.mNrArfcn;
    int n = Arrays.hashCode(this.mBands);
    long l = this.mNci;
    int i1 = this.mAdditionalPlmns.hashCode();
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Long.valueOf(l), Integer.valueOf(i1) });
  }
  
  public boolean equals(Object<String> paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof CellIdentityNr))
      return false; 
    paramObject = paramObject;
    if (super.equals(paramObject) && this.mPci == ((CellIdentityNr)paramObject).mPci && this.mTac == ((CellIdentityNr)paramObject).mTac && this.mNrArfcn == ((CellIdentityNr)paramObject).mNrArfcn) {
      int[] arrayOfInt1 = this.mBands, arrayOfInt2 = ((CellIdentityNr)paramObject).mBands;
      if (Arrays.equals(arrayOfInt1, arrayOfInt2) && this.mNci == ((CellIdentityNr)paramObject).mNci) {
        ArraySet<String> arraySet = this.mAdditionalPlmns;
        paramObject = (Object<String>)((CellIdentityNr)paramObject).mAdditionalPlmns;
        if (arraySet.equals(paramObject))
          return null; 
      } 
    } 
    return false;
  }
  
  public long getNci() {
    return this.mNci;
  }
  
  public int getNrarfcn() {
    return this.mNrArfcn;
  }
  
  public int[] getBands() {
    int[] arrayOfInt = this.mBands;
    return Arrays.copyOf(arrayOfInt, arrayOfInt.length);
  }
  
  public int getPci() {
    return this.mPci;
  }
  
  public int getTac() {
    return this.mTac;
  }
  
  public String getMccString() {
    return this.mMccStr;
  }
  
  public String getMncString() {
    return this.mMncStr;
  }
  
  public int getChannelNumber() {
    return this.mNrArfcn;
  }
  
  public Set<String> getAdditionalPlmns() {
    return Collections.unmodifiableSet(this.mAdditionalPlmns);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("CellIdentityNr:{");
    stringBuilder.append(" mPci = ");
    stringBuilder.append(this.mPci);
    stringBuilder.append(" mTac = ");
    stringBuilder.append(this.mTac);
    stringBuilder.append(" mNrArfcn = ");
    stringBuilder.append(this.mNrArfcn);
    stringBuilder.append(" mBands = ");
    stringBuilder.append(Arrays.toString(this.mBands));
    stringBuilder.append(" mMcc = ");
    stringBuilder.append(this.mMccStr);
    stringBuilder.append(" mMnc = ");
    stringBuilder.append(this.mMncStr);
    stringBuilder.append(" mNci = ");
    stringBuilder.append(this.mNci);
    stringBuilder.append(" mAlphaLong = ");
    stringBuilder.append(this.mAlphaLong);
    stringBuilder.append(" mAlphaShort = ");
    stringBuilder.append(this.mAlphaShort);
    stringBuilder.append(" mAdditionalPlmns = ");
    stringBuilder.append(this.mAdditionalPlmns);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    super.writeToParcel(paramParcel, 6);
    paramParcel.writeInt(this.mPci);
    paramParcel.writeInt(this.mTac);
    paramParcel.writeInt(this.mNrArfcn);
    paramParcel.writeIntArray(this.mBands);
    paramParcel.writeLong(this.mNci);
    paramParcel.writeArraySet(this.mAdditionalPlmns);
  }
  
  private CellIdentityNr(Parcel paramParcel) {
    super("CellIdentityNr", 6, paramParcel);
    this.mPci = paramParcel.readInt();
    this.mTac = paramParcel.readInt();
    this.mNrArfcn = paramParcel.readInt();
    this.mBands = paramParcel.createIntArray();
    this.mNci = paramParcel.readLong();
    this.mAdditionalPlmns = paramParcel.readArraySet(null);
    updateGlobalCellId();
  }
  
  public static final Parcelable.Creator<CellIdentityNr> CREATOR = (Parcelable.Creator<CellIdentityNr>)new Object();
  
  private static final long MAX_NCI = 68719476735L;
  
  private static final int MAX_NRARFCN = 3279165;
  
  private static final int MAX_PCI = 1007;
  
  private static final int MAX_TAC = 65535;
  
  private static final String TAG = "CellIdentityNr";
  
  private final ArraySet<String> mAdditionalPlmns;
  
  private final int[] mBands;
  
  private final long mNci;
  
  private final int mNrArfcn;
  
  private final int mPci;
  
  private final int mTac;
  
  protected static CellIdentityNr createFromParcelBody(Parcel paramParcel) {
    return new CellIdentityNr(paramParcel);
  }
}
