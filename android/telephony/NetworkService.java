package android.telephony;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.SparseArray;
import com.android.telephony.Rlog;
import java.util.ArrayList;
import java.util.List;

@SystemApi
public abstract class NetworkService extends Service {
  private final String TAG = NetworkService.class.getSimpleName();
  
  private final SparseArray<NetworkServiceProvider> mServiceMap = new SparseArray<>();
  
  public final INetworkServiceWrapper mBinder = new INetworkServiceWrapper();
  
  private static final int NETWORK_SERVICE_CREATE_NETWORK_SERVICE_PROVIDER = 1;
  
  private static final int NETWORK_SERVICE_GET_REGISTRATION_INFO = 4;
  
  private static final int NETWORK_SERVICE_INDICATION_NETWORK_INFO_CHANGED = 7;
  
  private static final int NETWORK_SERVICE_REGISTER_FOR_INFO_CHANGE = 5;
  
  private static final int NETWORK_SERVICE_REMOVE_ALL_NETWORK_SERVICE_PROVIDERS = 3;
  
  private static final int NETWORK_SERVICE_REMOVE_NETWORK_SERVICE_PROVIDER = 2;
  
  private static final int NETWORK_SERVICE_UNREGISTER_FOR_INFO_CHANGE = 6;
  
  public static final String SERVICE_INTERFACE = "android.telephony.NetworkService";
  
  private final NetworkServiceHandler mHandler;
  
  private final HandlerThread mHandlerThread;
  
  class NetworkServiceProvider implements AutoCloseable {
    private final List<INetworkServiceCallback> mNetworkRegistrationInfoChangedCallbacks = new ArrayList<>();
    
    private final int mSlotIndex;
    
    final NetworkService this$0;
    
    public NetworkServiceProvider(int param1Int) {
      this.mSlotIndex = param1Int;
    }
    
    public final int getSlotIndex() {
      return this.mSlotIndex;
    }
    
    public void requestNetworkRegistrationInfo(int param1Int, NetworkServiceCallback param1NetworkServiceCallback) {
      param1NetworkServiceCallback.onRequestNetworkRegistrationInfoComplete(1, null);
    }
    
    public final void notifyNetworkRegistrationInfoChanged() {
      Message message = NetworkService.this.mHandler.obtainMessage(7, this.mSlotIndex, 0, null);
      message.sendToTarget();
    }
    
    private void registerForInfoChanged(INetworkServiceCallback param1INetworkServiceCallback) {
      synchronized (this.mNetworkRegistrationInfoChangedCallbacks) {
        this.mNetworkRegistrationInfoChangedCallbacks.add(param1INetworkServiceCallback);
        return;
      } 
    }
    
    private void unregisterForInfoChanged(INetworkServiceCallback param1INetworkServiceCallback) {
      synchronized (this.mNetworkRegistrationInfoChangedCallbacks) {
        this.mNetworkRegistrationInfoChangedCallbacks.remove(param1INetworkServiceCallback);
        return;
      } 
    }
    
    private void notifyInfoChangedToCallbacks() {
      for (INetworkServiceCallback iNetworkServiceCallback : this.mNetworkRegistrationInfoChangedCallbacks) {
        try {
          iNetworkServiceCallback.onNetworkStateChanged();
        } catch (RemoteException remoteException) {}
      } 
    }
    
    public abstract void close();
  }
  
  class NetworkServiceHandler extends Handler {
    final NetworkService this$0;
    
    NetworkServiceHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.arg1;
      INetworkServiceCallback iNetworkServiceCallback = (INetworkServiceCallback)param1Message.obj;
      NetworkService.NetworkServiceProvider networkServiceProvider = NetworkService.this.mServiceMap.get(i);
      switch (param1Message.what) {
        default:
          return;
        case 7:
          if (networkServiceProvider != null)
            networkServiceProvider.notifyInfoChangedToCallbacks(); 
        case 6:
          if (networkServiceProvider != null)
            networkServiceProvider.unregisterForInfoChanged(iNetworkServiceCallback); 
        case 5:
          if (networkServiceProvider != null)
            networkServiceProvider.registerForInfoChanged(iNetworkServiceCallback); 
        case 4:
          if (networkServiceProvider != null) {
            i = param1Message.arg2;
            networkServiceProvider.requestNetworkRegistrationInfo(i, new NetworkServiceCallback(iNetworkServiceCallback));
          } 
        case 3:
          for (i = 0; i < NetworkService.this.mServiceMap.size(); i++) {
            NetworkService.NetworkServiceProvider networkServiceProvider1 = NetworkService.this.mServiceMap.get(i);
            if (networkServiceProvider1 != null)
              networkServiceProvider1.close(); 
          } 
          NetworkService.this.mServiceMap.clear();
        case 2:
          if (networkServiceProvider != null) {
            networkServiceProvider.close();
            NetworkService.this.mServiceMap.remove(i);
          } 
        case 1:
          break;
      } 
      if (networkServiceProvider == null)
        NetworkService.this.mServiceMap.put(i, NetworkService.this.onCreateNetworkServiceProvider(i)); 
    }
  }
  
  public NetworkService() {
    HandlerThread handlerThread = new HandlerThread(this.TAG);
    handlerThread.start();
    this.mHandler = new NetworkServiceHandler(this.mHandlerThread.getLooper());
    log("network service created");
  }
  
  public IBinder onBind(Intent paramIntent) {
    if (paramIntent == null || !"android.telephony.NetworkService".equals(paramIntent.getAction())) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected intent ");
      stringBuilder.append(paramIntent);
      loge(stringBuilder.toString());
      return null;
    } 
    return (IBinder)this.mBinder;
  }
  
  public boolean onUnbind(Intent paramIntent) {
    Message message = this.mHandler.obtainMessage(3, 0, 0, null);
    message.sendToTarget();
    return false;
  }
  
  public void onDestroy() {
    this.mHandlerThread.quit();
    super.onDestroy();
  }
  
  private class INetworkServiceWrapper extends INetworkService.Stub {
    final NetworkService this$0;
    
    private INetworkServiceWrapper() {}
    
    public void createNetworkServiceProvider(int param1Int) {
      Message message = NetworkService.this.mHandler.obtainMessage(1, param1Int, 0, null);
      message.sendToTarget();
    }
    
    public void removeNetworkServiceProvider(int param1Int) {
      Message message = NetworkService.this.mHandler.obtainMessage(2, param1Int, 0, null);
      message.sendToTarget();
    }
    
    public void requestNetworkRegistrationInfo(int param1Int1, int param1Int2, INetworkServiceCallback param1INetworkServiceCallback) {
      Message message = NetworkService.this.mHandler.obtainMessage(4, param1Int1, param1Int2, param1INetworkServiceCallback);
      message.sendToTarget();
    }
    
    public void registerForNetworkRegistrationInfoChanged(int param1Int, INetworkServiceCallback param1INetworkServiceCallback) {
      Message message = NetworkService.this.mHandler.obtainMessage(5, param1Int, 0, param1INetworkServiceCallback);
      message.sendToTarget();
    }
    
    public void unregisterForNetworkRegistrationInfoChanged(int param1Int, INetworkServiceCallback param1INetworkServiceCallback) {
      Message message = NetworkService.this.mHandler.obtainMessage(6, param1Int, 0, param1INetworkServiceCallback);
      message.sendToTarget();
    }
  }
  
  private final void log(String paramString) {
    Rlog.d(this.TAG, paramString);
  }
  
  private final void loge(String paramString) {
    Rlog.e(this.TAG, paramString);
  }
  
  public abstract NetworkServiceProvider onCreateNetworkServiceProvider(int paramInt);
}
