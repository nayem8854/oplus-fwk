package android.telephony;

import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Binder;
import android.os.UserHandle;
import android.util.Log;
import android.widget.Toast;
import com.android.internal.telephony.util.TelephonyUtils;
import com.oplus.multiapp.OplusMultiAppManager;

public final class LocationAccessPolicy {
  private static final boolean DBG = false;
  
  public static final int MAX_SDK_FOR_ANY_ENFORCEMENT = 10000;
  
  private static final String TAG = "LocationAccessPolicy";
  
  public enum LocationPermissionResult {
    ALLOWED, DENIED_HARD, DENIED_SOFT;
    
    private static final LocationPermissionResult[] $VALUES;
    
    static {
      LocationPermissionResult locationPermissionResult = new LocationPermissionResult("DENIED_HARD", 2);
      $VALUES = new LocationPermissionResult[] { ALLOWED, DENIED_SOFT, locationPermissionResult };
    }
  }
  
  public static class LocationPermissionQuery {
    public final String callingFeatureId;
    
    public final String callingPackage;
    
    public final int callingPid;
    
    public final int callingUid;
    
    public final boolean logAsInfo;
    
    public final String method;
    
    public final int minSdkVersionForCoarse;
    
    public final int minSdkVersionForFine;
    
    private LocationPermissionQuery(String param1String1, String param1String2, int param1Int1, int param1Int2, int param1Int3, int param1Int4, boolean param1Boolean, String param1String3) {
      this.callingPackage = param1String1;
      this.callingFeatureId = param1String2;
      this.callingUid = param1Int1;
      this.callingPid = param1Int2;
      this.minSdkVersionForCoarse = param1Int3;
      this.minSdkVersionForFine = param1Int4;
      this.logAsInfo = param1Boolean;
      this.method = param1String3;
    }
    
    public static class Builder {
      private int mMinSdkVersionForCoarse = Integer.MAX_VALUE;
      
      private int mMinSdkVersionForFine = Integer.MAX_VALUE;
      
      private boolean mLogAsInfo = false;
      
      private String mCallingFeatureId;
      
      private String mCallingPackage;
      
      private int mCallingPid;
      
      private int mCallingUid;
      
      private String mMethod;
      
      public Builder setCallingPackage(String param2String) {
        this.mCallingPackage = param2String;
        return this;
      }
      
      public Builder setCallingFeatureId(String param2String) {
        this.mCallingFeatureId = param2String;
        return this;
      }
      
      public Builder setCallingUid(int param2Int) {
        this.mCallingUid = param2Int;
        return this;
      }
      
      public Builder setCallingPid(int param2Int) {
        this.mCallingPid = param2Int;
        return this;
      }
      
      public Builder setMinSdkVersionForCoarse(int param2Int) {
        this.mMinSdkVersionForCoarse = param2Int;
        return this;
      }
      
      public Builder setMinSdkVersionForFine(int param2Int) {
        this.mMinSdkVersionForFine = param2Int;
        return this;
      }
      
      public Builder setMethod(String param2String) {
        this.mMethod = param2String;
        return this;
      }
      
      public Builder setLogAsInfo(boolean param2Boolean) {
        this.mLogAsInfo = param2Boolean;
        return this;
      }
      
      public LocationAccessPolicy.LocationPermissionQuery build() {
        return new LocationAccessPolicy.LocationPermissionQuery(this.mCallingPackage, this.mCallingFeatureId, this.mCallingUid, this.mCallingPid, this.mMinSdkVersionForCoarse, this.mMinSdkVersionForFine, this.mLogAsInfo, this.mMethod);
      }
    }
  }
  
  public static class Builder {
    private int mMinSdkVersionForCoarse = Integer.MAX_VALUE;
    
    private int mMinSdkVersionForFine = Integer.MAX_VALUE;
    
    private boolean mLogAsInfo = false;
    
    private String mCallingFeatureId;
    
    private String mCallingPackage;
    
    private int mCallingPid;
    
    private int mCallingUid;
    
    private String mMethod;
    
    public Builder setCallingPackage(String param1String) {
      this.mCallingPackage = param1String;
      return this;
    }
    
    public Builder setCallingFeatureId(String param1String) {
      this.mCallingFeatureId = param1String;
      return this;
    }
    
    public Builder setCallingUid(int param1Int) {
      this.mCallingUid = param1Int;
      return this;
    }
    
    public Builder setCallingPid(int param1Int) {
      this.mCallingPid = param1Int;
      return this;
    }
    
    public Builder setMinSdkVersionForCoarse(int param1Int) {
      this.mMinSdkVersionForCoarse = param1Int;
      return this;
    }
    
    public Builder setMinSdkVersionForFine(int param1Int) {
      this.mMinSdkVersionForFine = param1Int;
      return this;
    }
    
    public Builder setMethod(String param1String) {
      this.mMethod = param1String;
      return this;
    }
    
    public Builder setLogAsInfo(boolean param1Boolean) {
      this.mLogAsInfo = param1Boolean;
      return this;
    }
    
    public LocationAccessPolicy.LocationPermissionQuery build() {
      return new LocationAccessPolicy.LocationPermissionQuery(this.mCallingPackage, this.mCallingFeatureId, this.mCallingUid, this.mCallingPid, this.mMinSdkVersionForCoarse, this.mMinSdkVersionForFine, this.mLogAsInfo, this.mMethod);
    }
  }
  
  private static void logError(Context paramContext, LocationPermissionQuery paramLocationPermissionQuery, String paramString) {
    if (paramLocationPermissionQuery.logAsInfo) {
      Log.i("LocationAccessPolicy", paramString);
      return;
    } 
    Log.e("LocationAccessPolicy", paramString);
    try {
      if (TelephonyUtils.IS_DEBUGGABLE)
        Toast.makeText(paramContext, paramString, 0).show(); 
    } finally {}
  }
  
  private static LocationPermissionResult appOpsModeToPermissionResult(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 2)
        return LocationPermissionResult.DENIED_SOFT; 
      return LocationPermissionResult.DENIED_HARD;
    } 
    return LocationPermissionResult.ALLOWED;
  }
  
  private static String getAppOpsString(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual hashCode : ()I
    //   4: istore_1
    //   5: iload_1
    //   6: ldc -1888586689
    //   8: if_icmpeq -> 34
    //   11: iload_1
    //   12: ldc -63024214
    //   14: if_icmpeq -> 20
    //   17: goto -> 48
    //   20: aload_0
    //   21: ldc 'android.permission.ACCESS_COARSE_LOCATION'
    //   23: invokevirtual equals : (Ljava/lang/Object;)Z
    //   26: ifeq -> 17
    //   29: iconst_1
    //   30: istore_1
    //   31: goto -> 50
    //   34: aload_0
    //   35: ldc 'android.permission.ACCESS_FINE_LOCATION'
    //   37: invokevirtual equals : (Ljava/lang/Object;)Z
    //   40: ifeq -> 17
    //   43: iconst_0
    //   44: istore_1
    //   45: goto -> 50
    //   48: iconst_m1
    //   49: istore_1
    //   50: iload_1
    //   51: ifeq -> 64
    //   54: iload_1
    //   55: iconst_1
    //   56: if_icmpeq -> 61
    //   59: aconst_null
    //   60: areturn
    //   61: ldc 'android:coarse_location'
    //   63: areturn
    //   64: ldc 'android:fine_location'
    //   66: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #203	-> 0
    //   #209	-> 59
    //   #207	-> 61
    //   #205	-> 64
  }
  
  private static LocationPermissionResult checkAppLocationPermissionHelper(Context paramContext, LocationPermissionQuery paramLocationPermissionQuery, String paramString) {
    StringBuilder stringBuilder;
    String str;
    int i;
    if ("android.permission.ACCESS_FINE_LOCATION".equals(paramString)) {
      str = "fine";
    } else {
      str = "coarse";
    } 
    boolean bool = checkManifestPermission(paramContext, paramLocationPermissionQuery.callingPid, paramLocationPermissionQuery.callingUid, paramString);
    if (bool) {
      AppOpsManager appOpsManager = (AppOpsManager)paramContext.getSystemService(AppOpsManager.class);
      i = appOpsManager.noteOpNoThrow(getAppOpsString(paramString), paramLocationPermissionQuery.callingUid, paramLocationPermissionQuery.callingPackage, paramLocationPermissionQuery.callingFeatureId, null);
      if (i == 0)
        return LocationPermissionResult.ALLOWED; 
      stringBuilder = new StringBuilder();
      stringBuilder.append(paramLocationPermissionQuery.callingPackage);
      stringBuilder.append(" is aware of ");
      stringBuilder.append(str);
      stringBuilder.append(" but the app-ops permission is specifically denied.");
      Log.i("LocationAccessPolicy", stringBuilder.toString());
      return appOpsModeToPermissionResult(i);
    } 
    if ("android.permission.ACCESS_FINE_LOCATION".equals(paramString)) {
      i = paramLocationPermissionQuery.minSdkVersionForFine;
    } else {
      i = paramLocationPermissionQuery.minSdkVersionForCoarse;
    } 
    if (i > 10000) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Allowing ");
      stringBuilder1.append(paramLocationPermissionQuery.callingPackage);
      stringBuilder1.append(" ");
      stringBuilder1.append(str);
      stringBuilder1.append(" because we're not enforcing API ");
      stringBuilder1.append(i);
      stringBuilder1.append(" yet. Please fix this app because it will break in the future. Called from ");
      stringBuilder1.append(paramLocationPermissionQuery.method);
      String str1 = stringBuilder1.toString();
      logError((Context)stringBuilder, paramLocationPermissionQuery, str1);
      return null;
    } 
    if (!isAppAtLeastSdkVersion((Context)stringBuilder, paramLocationPermissionQuery.callingPackage, i, paramLocationPermissionQuery.callingUid)) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Allowing ");
      stringBuilder1.append(paramLocationPermissionQuery.callingPackage);
      stringBuilder1.append(" ");
      stringBuilder1.append(str);
      stringBuilder1.append(" because it doesn't target API ");
      stringBuilder1.append(i);
      stringBuilder1.append(" yet. Please fix this app. Called from ");
      stringBuilder1.append(paramLocationPermissionQuery.method);
      String str1 = stringBuilder1.toString();
      logError((Context)stringBuilder, paramLocationPermissionQuery, str1);
      return null;
    } 
    return LocationPermissionResult.DENIED_HARD;
  }
  
  public static LocationPermissionResult checkLocationPermission(Context paramContext, LocationPermissionQuery paramLocationPermissionQuery) {
    if (paramLocationPermissionQuery.callingUid == 1001 || paramLocationPermissionQuery.callingUid == 1000 || paramLocationPermissionQuery.callingUid == 1073 || paramLocationPermissionQuery.callingUid == 0)
      return LocationPermissionResult.ALLOWED; 
    if (!checkSystemLocationAccess(paramContext, paramLocationPermissionQuery.callingUid, paramLocationPermissionQuery.callingPid))
      return LocationPermissionResult.DENIED_SOFT; 
    if (paramLocationPermissionQuery.minSdkVersionForFine < Integer.MAX_VALUE) {
      LocationPermissionResult locationPermissionResult = checkAppLocationPermissionHelper(paramContext, paramLocationPermissionQuery, "android.permission.ACCESS_FINE_LOCATION");
      if (locationPermissionResult != null)
        return locationPermissionResult; 
    } 
    if (paramLocationPermissionQuery.minSdkVersionForCoarse < Integer.MAX_VALUE) {
      LocationPermissionResult locationPermissionResult = checkAppLocationPermissionHelper(paramContext, paramLocationPermissionQuery, "android.permission.ACCESS_COARSE_LOCATION");
      if (locationPermissionResult != null)
        return locationPermissionResult; 
    } 
    return LocationPermissionResult.ALLOWED;
  }
  
  private static boolean checkManifestPermission(Context paramContext, int paramInt1, int paramInt2, String paramString) {
    boolean bool;
    if (paramContext.checkPermission(paramString, paramInt1, paramInt2) == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static boolean checkSystemLocationAccess(Context paramContext, int paramInt1, int paramInt2) {
    boolean bool = isLocationModeEnabled(paramContext, UserHandle.getUserHandleForUid(paramInt1).getIdentifier());
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (isCurrentProfile(paramContext, paramInt1) || checkInteractAcrossUsersFull(paramContext, paramInt2, paramInt1))
      bool1 = true; 
    return bool1;
  }
  
  private static boolean isLocationModeEnabled(Context paramContext, int paramInt) {
    LocationManager locationManager = (LocationManager)paramContext.getSystemService(LocationManager.class);
    if (locationManager == null) {
      Log.w("LocationAccessPolicy", "Couldn't get location manager, denying location access");
      return false;
    } 
    return locationManager.isLocationEnabledForUser(UserHandle.of(paramInt));
  }
  
  private static boolean checkInteractAcrossUsersFull(Context paramContext, int paramInt1, int paramInt2) {
    return checkManifestPermission(paramContext, paramInt1, paramInt2, "android.permission.INTERACT_ACROSS_USERS_FULL");
  }
  
  private static boolean isCurrentProfile(Context paramContext, int paramInt) {
    long l = Binder.clearCallingIdentity();
    try {
      int i = UserHandle.getUserId(paramInt);
      if (ActivityManager.getCurrentUser() == 0) {
        boolean bool = OplusMultiAppManager.getInstance().isMultiAppUserId(i);
        if (bool)
          return true; 
      } 
      i = UserHandle.getUserHandleForUid(paramInt).getIdentifier();
      paramInt = ActivityManager.getCurrentUser();
      if (i == paramInt)
        return true; 
      ActivityManager activityManager = (ActivityManager)paramContext.getSystemService(ActivityManager.class);
      if (activityManager != null) {
        UserHandle userHandle = UserHandle.getUserHandleForUid(ActivityManager.getCurrentUser());
        return activityManager.isProfileForeground(userHandle);
      } 
      return false;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  private static boolean isAppAtLeastSdkVersion(Context paramContext, String paramString, int paramInt1, int paramInt2) {
    try {
      paramInt2 = (paramContext.getPackageManager().getApplicationInfoAsUser(paramString, 0, UserHandle.getUserId(paramInt2))).targetSdkVersion;
      if (paramInt2 >= paramInt1)
        return true; 
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {}
    return false;
  }
}
