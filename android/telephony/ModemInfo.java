package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public class ModemInfo implements Parcelable {
  public ModemInfo(int paramInt) {
    this(paramInt, 0, true, true);
  }
  
  public ModemInfo(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2) {
    this.modemId = paramInt1;
    this.rat = paramInt2;
    this.isVoiceSupported = paramBoolean1;
    this.isDataSupported = paramBoolean2;
  }
  
  public ModemInfo(Parcel paramParcel) {
    this.modemId = paramParcel.readInt();
    this.rat = paramParcel.readInt();
    this.isVoiceSupported = paramParcel.readBoolean();
    this.isDataSupported = paramParcel.readBoolean();
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("modemId=");
    stringBuilder.append(this.modemId);
    stringBuilder.append(" rat=");
    stringBuilder.append(this.rat);
    stringBuilder.append(" isVoiceSupported:");
    stringBuilder.append(this.isVoiceSupported);
    stringBuilder.append(" isDataSupported:");
    stringBuilder.append(this.isDataSupported);
    return stringBuilder.toString();
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.modemId), Integer.valueOf(this.rat), Boolean.valueOf(this.isVoiceSupported), Boolean.valueOf(this.isDataSupported) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool1 = false;
    if (paramObject == null || !(paramObject instanceof ModemInfo) || hashCode() != paramObject.hashCode())
      return false; 
    if (this == paramObject)
      return true; 
    paramObject = paramObject;
    boolean bool2 = bool1;
    if (this.modemId == ((ModemInfo)paramObject).modemId) {
      bool2 = bool1;
      if (this.rat == ((ModemInfo)paramObject).rat) {
        bool2 = bool1;
        if (this.isVoiceSupported == ((ModemInfo)paramObject).isVoiceSupported) {
          bool2 = bool1;
          if (this.isDataSupported == ((ModemInfo)paramObject).isDataSupported)
            bool2 = true; 
        } 
      } 
    } 
    return bool2;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.modemId);
    paramParcel.writeInt(this.rat);
    paramParcel.writeBoolean(this.isVoiceSupported);
    paramParcel.writeBoolean(this.isDataSupported);
  }
  
  public static final Parcelable.Creator<ModemInfo> CREATOR = new Parcelable.Creator() {
      public ModemInfo createFromParcel(Parcel param1Parcel) {
        return new ModemInfo(param1Parcel);
      }
      
      public ModemInfo[] newArray(int param1Int) {
        return new ModemInfo[param1Int];
      }
    };
  
  public final boolean isDataSupported;
  
  public final boolean isVoiceSupported;
  
  public final int modemId;
  
  public final int rat;
}
