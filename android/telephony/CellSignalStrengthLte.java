package android.telephony;

import android.hardware.radio.V1_0.LteSignalStrength;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PersistableBundle;
import com.android.telephony.Rlog;
import java.util.Objects;

public final class CellSignalStrengthLte extends CellSignalStrength implements Parcelable {
  public static final Parcelable.Creator<CellSignalStrengthLte> CREATOR;
  
  private static final boolean DBG = false;
  
  private static final String LOG_TAG = "CellSignalStrengthLte";
  
  private static final int MAX_LTE_RSRP = -44;
  
  private static final int MIN_LTE_RSRP = -140;
  
  private static final int SIGNAL_STRENGTH_LTE_RSSI_ASU_UNKNOWN = 99;
  
  private static final int SIGNAL_STRENGTH_LTE_RSSI_VALID_ASU_MAX_VALUE = 31;
  
  private static final int SIGNAL_STRENGTH_LTE_RSSI_VALID_ASU_MIN_VALUE = 0;
  
  public static final int USE_RSRP = 1;
  
  public static final int USE_RSRQ = 2;
  
  public static final int USE_RSSNR = 4;
  
  private static final CellSignalStrengthLte sInvalid;
  
  private static final int sRsrpBoost = 0;
  
  public CellSignalStrengthLte() {
    setDefaultValues();
  }
  
  public CellSignalStrengthLte(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    this.mRssi = paramInt1 = inRangeOrUnavailable(paramInt1, -113, -51);
    this.mSignalStrength = paramInt1;
    this.mRsrp = inRangeOrUnavailable(paramInt2, -140, -43);
    this.mRsrq = inRangeOrUnavailable(paramInt3, -34, 3);
    this.mRssnr = inRangeOrUnavailable(paramInt4, -20, 30);
    this.mCqi = inRangeOrUnavailable(paramInt5, 0, 15);
    this.mTimingAdvance = inRangeOrUnavailable(paramInt6, 0, 1282);
    updateLevel((PersistableBundle)null, (ServiceState)null);
  }
  
  public CellSignalStrengthLte(LteSignalStrength paramLteSignalStrength) {
    this(i, j, k, n, i1, m);
  }
  
  public CellSignalStrengthLte(CellSignalStrengthLte paramCellSignalStrengthLte) {
    copyFrom(paramCellSignalStrengthLte);
  }
  
  protected void copyFrom(CellSignalStrengthLte paramCellSignalStrengthLte) {
    this.mSignalStrength = paramCellSignalStrengthLte.mSignalStrength;
    this.mRssi = paramCellSignalStrengthLte.mRssi;
    this.mRsrp = paramCellSignalStrengthLte.mRsrp;
    this.mRsrq = paramCellSignalStrengthLte.mRsrq;
    this.mRssnr = paramCellSignalStrengthLte.mRssnr;
    this.mCqi = paramCellSignalStrengthLte.mCqi;
    this.mTimingAdvance = paramCellSignalStrengthLte.mTimingAdvance;
    this.mLevel = paramCellSignalStrengthLte.mLevel;
    this.mParametersUseForLevel = paramCellSignalStrengthLte.mParametersUseForLevel;
  }
  
  public CellSignalStrengthLte copy() {
    return new CellSignalStrengthLte(this);
  }
  
  public void setDefaultValues() {
    this.mSignalStrength = Integer.MAX_VALUE;
    this.mRssi = Integer.MAX_VALUE;
    this.mRsrp = Integer.MAX_VALUE;
    this.mRsrq = Integer.MAX_VALUE;
    this.mRssnr = Integer.MAX_VALUE;
    this.mCqi = Integer.MAX_VALUE;
    this.mTimingAdvance = Integer.MAX_VALUE;
    this.mLevel = 0;
    this.mParametersUseForLevel = 1;
  }
  
  public int getLevel() {
    return this.mLevel;
  }
  
  private static final int[] sRsrpThresholds = new int[] { -115, -105, -95, -85 };
  
  private static final int[] sRsrqThresholds = new int[] { -19, -17, -14, -12 };
  
  private static final int[] sRssnrThresholds = new int[] { -3, 1, 5, 13 };
  
  private int mCqi;
  
  private int mLevel;
  
  private int mParametersUseForLevel;
  
  private int mRsrp;
  
  private int mRsrq;
  
  private int mRssi;
  
  private int mRssnr;
  
  private int mSignalStrength;
  
  private int mTimingAdvance;
  
  private boolean isLevelForParameter(int paramInt) {
    boolean bool;
    if ((this.mParametersUseForLevel & paramInt) == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void updateLevel(PersistableBundle paramPersistableBundle, ServiceState paramServiceState) {
    int[] arrayOfInt1, arrayOfInt2, arrayOfInt3;
    boolean bool;
    if (paramPersistableBundle == null) {
      this.mParametersUseForLevel = 1;
      arrayOfInt1 = sRsrpThresholds;
      arrayOfInt2 = sRsrqThresholds;
      arrayOfInt3 = sRssnrThresholds;
      bool = false;
    } else {
      this.mParametersUseForLevel = paramPersistableBundle.getInt("parameters_used_for_lte_signal_bar_int");
      arrayOfInt2 = paramPersistableBundle.getIntArray("lte_rsrp_thresholds_int_array");
      arrayOfInt1 = arrayOfInt2;
      if (arrayOfInt2 == null)
        arrayOfInt1 = sRsrpThresholds; 
      arrayOfInt3 = paramPersistableBundle.getIntArray("lte_rsrq_thresholds_int_array");
      arrayOfInt2 = arrayOfInt3;
      if (arrayOfInt3 == null)
        arrayOfInt2 = sRsrqThresholds; 
      int[] arrayOfInt = paramPersistableBundle.getIntArray("lte_rssnr_thresholds_int_array");
      arrayOfInt3 = arrayOfInt;
      if (arrayOfInt == null)
        arrayOfInt3 = sRssnrThresholds; 
      bool = paramPersistableBundle.getBoolean("use_only_rsrp_for_lte_signal_bar_bool", false);
    } 
    int i = 0;
    if (paramServiceState != null)
      i = paramServiceState.getLteEarfcnRsrpBoost(); 
    int j = inRangeOrUnavailable(this.mRsrp + i, -140, -44);
    if (bool) {
      i = updateLevelWithMeasure(j, arrayOfInt1);
      if (i != Integer.MAX_VALUE) {
        this.mLevel = i;
        return;
      } 
    } 
    i = Integer.MAX_VALUE;
    int k = Integer.MAX_VALUE;
    int m = Integer.MAX_VALUE;
    if (isLevelForParameter(1))
      i = updateLevelWithMeasure(j, arrayOfInt1); 
    if (isLevelForParameter(2))
      k = updateLevelWithMeasure(this.mRsrq, arrayOfInt2); 
    if (isLevelForParameter(4))
      m = updateLevelWithMeasure(this.mRssnr, arrayOfInt3); 
    this.mLevel = i = Math.min(Math.min(i, k), m);
    if (i == Integer.MAX_VALUE) {
      i = this.mRssi;
      if (i > -51) {
        i = 0;
      } else if (i >= -89) {
        i = 4;
      } else if (i >= -97) {
        i = 3;
      } else if (i >= -103) {
        i = 2;
      } else if (i >= -113) {
        i = 1;
      } else {
        i = 0;
      } 
      this.mLevel = i;
    } 
  }
  
  private int updateLevelWithMeasure(int paramInt, int[] paramArrayOfint) {
    if (paramInt == Integer.MAX_VALUE) {
      paramInt = Integer.MAX_VALUE;
    } else if (paramInt >= paramArrayOfint[3]) {
      paramInt = 4;
    } else if (paramInt >= paramArrayOfint[2]) {
      paramInt = 3;
    } else if (paramInt >= paramArrayOfint[1]) {
      paramInt = 2;
    } else if (paramInt >= paramArrayOfint[0]) {
      paramInt = 1;
    } else {
      paramInt = 0;
    } 
    return paramInt;
  }
  
  public int getRsrq() {
    return this.mRsrq;
  }
  
  public int getRssi() {
    return this.mRssi;
  }
  
  public int getRssnr() {
    return this.mRssnr;
  }
  
  public int getRsrp() {
    return this.mRsrp;
  }
  
  public int getCqi() {
    return this.mCqi;
  }
  
  public int getDbm() {
    return this.mRsrp;
  }
  
  public int getAsuLevel() {
    int i = this.mRsrp;
    if (i == Integer.MAX_VALUE) {
      i = 99;
    } else if (i <= -140) {
      i = 0;
    } else if (i >= -43) {
      i = 97;
    } else {
      i += 140;
    } 
    return i;
  }
  
  public int getTimingAdvance() {
    return this.mTimingAdvance;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mRssi), Integer.valueOf(this.mRsrp), Integer.valueOf(this.mRsrq), Integer.valueOf(this.mRssnr), Integer.valueOf(this.mCqi), Integer.valueOf(this.mTimingAdvance), Integer.valueOf(this.mLevel) });
  }
  
  static {
    sInvalid = new CellSignalStrengthLte();
    CREATOR = new Parcelable.Creator<CellSignalStrengthLte>() {
        public CellSignalStrengthLte createFromParcel(Parcel param1Parcel) {
          return new CellSignalStrengthLte(param1Parcel);
        }
        
        public CellSignalStrengthLte[] newArray(int param1Int) {
          return new CellSignalStrengthLte[param1Int];
        }
      };
  }
  
  public boolean isValid() {
    return equals(sInvalid) ^ true;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof CellSignalStrengthLte;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    bool = bool1;
    if (this.mRssi == ((CellSignalStrengthLte)paramObject).mRssi) {
      bool = bool1;
      if (this.mRsrp == ((CellSignalStrengthLte)paramObject).mRsrp) {
        bool = bool1;
        if (this.mRsrq == ((CellSignalStrengthLte)paramObject).mRsrq) {
          bool = bool1;
          if (this.mRssnr == ((CellSignalStrengthLte)paramObject).mRssnr) {
            bool = bool1;
            if (this.mCqi == ((CellSignalStrengthLte)paramObject).mCqi) {
              bool = bool1;
              if (this.mTimingAdvance == ((CellSignalStrengthLte)paramObject).mTimingAdvance) {
                bool = bool1;
                if (this.mLevel == ((CellSignalStrengthLte)paramObject).mLevel)
                  bool = true; 
              } 
            } 
          } 
        } 
      } 
    } 
    return bool;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CellSignalStrengthLte: rssi=");
    stringBuilder.append(this.mRssi);
    stringBuilder.append(" rsrp=");
    stringBuilder.append(this.mRsrp);
    stringBuilder.append(" rsrq=");
    stringBuilder.append(this.mRsrq);
    stringBuilder.append(" rssnr=");
    stringBuilder.append(this.mRssnr);
    stringBuilder.append(" cqi=");
    stringBuilder.append(this.mCqi);
    stringBuilder.append(" ta=");
    stringBuilder.append(this.mTimingAdvance);
    stringBuilder.append(" level=");
    stringBuilder.append(this.mLevel);
    stringBuilder.append(" parametersUseForLevel=");
    stringBuilder.append(this.mParametersUseForLevel);
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mRssi);
    paramParcel.writeInt(this.mRsrp);
    paramParcel.writeInt(this.mRsrq);
    paramParcel.writeInt(this.mRssnr);
    paramParcel.writeInt(this.mCqi);
    paramParcel.writeInt(this.mTimingAdvance);
    paramParcel.writeInt(this.mLevel);
  }
  
  private CellSignalStrengthLte(Parcel paramParcel) {
    int i = paramParcel.readInt();
    this.mSignalStrength = i;
    this.mRsrp = paramParcel.readInt();
    this.mRsrq = paramParcel.readInt();
    this.mRssnr = paramParcel.readInt();
    this.mCqi = paramParcel.readInt();
    this.mTimingAdvance = paramParcel.readInt();
    this.mLevel = paramParcel.readInt();
  }
  
  public int describeContents() {
    return 0;
  }
  
  private static void log(String paramString) {
    Rlog.w("CellSignalStrengthLte", paramString);
  }
  
  private static int convertRssnrUnitFromTenDbToDB(int paramInt) {
    return paramInt / 10;
  }
  
  private static int convertRssiAsuToDBm(int paramInt) {
    if (paramInt == 99)
      return Integer.MAX_VALUE; 
    if (paramInt < 0 || paramInt > 31) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("convertRssiAsuToDBm: invalid RSSI in ASU=");
      stringBuilder.append(paramInt);
      Rlog.e("CellSignalStrengthLte", stringBuilder.toString());
      return Integer.MAX_VALUE;
    } 
    return paramInt * 2 - 113;
  }
}
