package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;

public class DataConnectionRealTimeInfo implements Parcelable {
  public DataConnectionRealTimeInfo(long paramLong, int paramInt) {
    this.mTime = paramLong;
    this.mDcPowerState = paramInt;
  }
  
  public DataConnectionRealTimeInfo() {
    this.mTime = Long.MAX_VALUE;
    this.mDcPowerState = Integer.MAX_VALUE;
  }
  
  private DataConnectionRealTimeInfo(Parcel paramParcel) {
    this.mTime = paramParcel.readLong();
    this.mDcPowerState = paramParcel.readInt();
  }
  
  public long getTime() {
    return this.mTime;
  }
  
  public int getDcPowerState() {
    return this.mDcPowerState;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mTime);
    paramParcel.writeInt(this.mDcPowerState);
  }
  
  public static final Parcelable.Creator<DataConnectionRealTimeInfo> CREATOR = new Parcelable.Creator<DataConnectionRealTimeInfo>() {
      public DataConnectionRealTimeInfo createFromParcel(Parcel param1Parcel) {
        return new DataConnectionRealTimeInfo(param1Parcel);
      }
      
      public DataConnectionRealTimeInfo[] newArray(int param1Int) {
        return new DataConnectionRealTimeInfo[param1Int];
      }
    };
  
  public static final int DC_POWER_STATE_HIGH = 3;
  
  public static final int DC_POWER_STATE_LOW = 1;
  
  public static final int DC_POWER_STATE_MEDIUM = 2;
  
  public static final int DC_POWER_STATE_UNKNOWN = 2147483647;
  
  private int mDcPowerState;
  
  private long mTime;
  
  public int hashCode() {
    long l1 = 1L * 17L + this.mTime;
    long l2 = this.mDcPowerState;
    return (int)(l1 + 17L * l1 + l2);
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mTime != ((DataConnectionRealTimeInfo)paramObject).mTime || this.mDcPowerState != ((DataConnectionRealTimeInfo)paramObject).mDcPowerState)
      bool = false; 
    return bool;
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append("mTime=");
    stringBuffer.append(this.mTime);
    stringBuffer.append(" mDcPowerState=");
    stringBuffer.append(this.mDcPowerState);
    return stringBuffer.toString();
  }
}
