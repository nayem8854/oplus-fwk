package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;

public class IccOpenLogicalChannelResponse implements Parcelable {
  public IccOpenLogicalChannelResponse(int paramInt1, int paramInt2, byte[] paramArrayOfbyte) {
    this.mChannel = paramInt1;
    this.mStatus = paramInt2;
    this.mSelectResponse = paramArrayOfbyte;
  }
  
  private IccOpenLogicalChannelResponse(Parcel paramParcel) {
    this.mChannel = paramParcel.readInt();
    this.mStatus = paramParcel.readInt();
    int i = paramParcel.readInt();
    if (i > 0) {
      byte[] arrayOfByte = new byte[i];
      paramParcel.readByteArray(arrayOfByte);
    } else {
      this.mSelectResponse = null;
    } 
  }
  
  public int getChannel() {
    return this.mChannel;
  }
  
  public int getStatus() {
    return this.mStatus;
  }
  
  public byte[] getSelectResponse() {
    return this.mSelectResponse;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mChannel);
    paramParcel.writeInt(this.mStatus);
    byte[] arrayOfByte = this.mSelectResponse;
    if (arrayOfByte != null && arrayOfByte.length > 0) {
      paramParcel.writeInt(arrayOfByte.length);
      paramParcel.writeByteArray(this.mSelectResponse);
    } else {
      paramParcel.writeInt(0);
    } 
  }
  
  public static final Parcelable.Creator<IccOpenLogicalChannelResponse> CREATOR = new Parcelable.Creator<IccOpenLogicalChannelResponse>() {
      public IccOpenLogicalChannelResponse createFromParcel(Parcel param1Parcel) {
        return new IccOpenLogicalChannelResponse(param1Parcel);
      }
      
      public IccOpenLogicalChannelResponse[] newArray(int param1Int) {
        return new IccOpenLogicalChannelResponse[param1Int];
      }
    };
  
  public static final int INVALID_CHANNEL = -1;
  
  public static final int STATUS_MISSING_RESOURCE = 2;
  
  public static final int STATUS_NO_ERROR = 1;
  
  public static final int STATUS_NO_SUCH_ELEMENT = 3;
  
  public static final int STATUS_UNKNOWN_ERROR = 4;
  
  private final int mChannel;
  
  private final byte[] mSelectResponse;
  
  private final int mStatus;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Channel: ");
    stringBuilder.append(this.mChannel);
    stringBuilder.append(" Status: ");
    stringBuilder.append(this.mStatus);
    return stringBuilder.toString();
  }
}
