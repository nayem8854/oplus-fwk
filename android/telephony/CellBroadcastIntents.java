package android.telephony;

import android.annotation.SystemApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.UserHandle;

@SystemApi
public class CellBroadcastIntents {
  public static final String ACTION_AREA_INFO_UPDATED = "android.telephony.action.AREA_INFO_UPDATED";
  
  private static final String EXTRA_MESSAGE = "message";
  
  private static final String LOG_TAG = "CellBroadcastIntents";
  
  public static void sendSmsCbReceivedBroadcast(Context paramContext, UserHandle paramUserHandle, SmsCbMessage paramSmsCbMessage, BroadcastReceiver paramBroadcastReceiver, Handler paramHandler, int paramInt1, int paramInt2) {
    Intent intent = new Intent("android.provider.Telephony.SMS_CB_RECEIVED");
    intent.putExtra("message", paramSmsCbMessage);
    putPhoneIdAndSubIdExtra(paramContext, intent, paramInt2);
    if (paramUserHandle != null) {
      paramContext.createContextAsUser(paramUserHandle, 0).sendOrderedBroadcast(intent, "android.permission.RECEIVE_SMS", "android:receive_sms", paramBroadcastReceiver, paramHandler, paramInt1, null, null);
    } else {
      paramContext.sendOrderedBroadcast(intent, "android.permission.RECEIVE_SMS", "android:receive_sms", paramBroadcastReceiver, paramHandler, paramInt1, null, null);
    } 
  }
  
  private static void putPhoneIdAndSubIdExtra(Context paramContext, Intent paramIntent, int paramInt) {
    int i = getSubIdForPhone(paramContext, paramInt);
    if (i != -1) {
      paramIntent.putExtra("subscription", i);
      paramIntent.putExtra("android.telephony.extra.SUBSCRIPTION_INDEX", i);
    } 
    paramIntent.putExtra("phone", paramInt);
    paramIntent.putExtra("android.telephony.extra.SLOT_INDEX", paramInt);
  }
  
  private static int getSubIdForPhone(Context paramContext, int paramInt) {
    SubscriptionManager subscriptionManager = (SubscriptionManager)paramContext.getSystemService("telephony_subscription_service");
    int[] arrayOfInt = subscriptionManager.getSubscriptionIds(paramInt);
    if (arrayOfInt != null)
      return arrayOfInt[0]; 
    return -1;
  }
}
