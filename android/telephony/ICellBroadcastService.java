package android.telephony;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteCallback;
import android.os.RemoteException;
import android.telephony.cdma.CdmaSmsCbProgramData;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

public interface ICellBroadcastService extends IInterface {
  CharSequence getCellBroadcastAreaInfo(int paramInt) throws RemoteException;
  
  void handleCdmaCellBroadcastSms(int paramInt1, byte[] paramArrayOfbyte, int paramInt2) throws RemoteException;
  
  void handleCdmaScpMessage(int paramInt, List<CdmaSmsCbProgramData> paramList, String paramString, RemoteCallback paramRemoteCallback) throws RemoteException;
  
  void handleGsmCellBroadcastSms(int paramInt, byte[] paramArrayOfbyte) throws RemoteException;
  
  class Default implements ICellBroadcastService {
    public void handleGsmCellBroadcastSms(int param1Int, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void handleCdmaCellBroadcastSms(int param1Int1, byte[] param1ArrayOfbyte, int param1Int2) throws RemoteException {}
    
    public void handleCdmaScpMessage(int param1Int, List<CdmaSmsCbProgramData> param1List, String param1String, RemoteCallback param1RemoteCallback) throws RemoteException {}
    
    public CharSequence getCellBroadcastAreaInfo(int param1Int) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICellBroadcastService {
    private static final String DESCRIPTOR = "android.telephony.ICellBroadcastService";
    
    static final int TRANSACTION_getCellBroadcastAreaInfo = 4;
    
    static final int TRANSACTION_handleCdmaCellBroadcastSms = 2;
    
    static final int TRANSACTION_handleCdmaScpMessage = 3;
    
    static final int TRANSACTION_handleGsmCellBroadcastSms = 1;
    
    public Stub() {
      attachInterface(this, "android.telephony.ICellBroadcastService");
    }
    
    public static ICellBroadcastService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.ICellBroadcastService");
      if (iInterface != null && iInterface instanceof ICellBroadcastService)
        return (ICellBroadcastService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "getCellBroadcastAreaInfo";
          } 
          return "handleCdmaScpMessage";
        } 
        return "handleCdmaCellBroadcastSms";
      } 
      return "handleGsmCellBroadcastSms";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      CharSequence charSequence;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.telephony.ICellBroadcastService");
              return true;
            } 
            param1Parcel1.enforceInterface("android.telephony.ICellBroadcastService");
            param1Int1 = param1Parcel1.readInt();
            charSequence = getCellBroadcastAreaInfo(param1Int1);
            param1Parcel2.writeNoException();
            if (charSequence != null) {
              param1Parcel2.writeInt(1);
              TextUtils.writeToParcel(charSequence, param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          } 
          charSequence.enforceInterface("android.telephony.ICellBroadcastService");
          param1Int1 = charSequence.readInt();
          ArrayList<CdmaSmsCbProgramData> arrayList = charSequence.createTypedArrayList(CdmaSmsCbProgramData.CREATOR);
          String str = charSequence.readString();
          if (charSequence.readInt() != 0) {
            RemoteCallback remoteCallback = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel((Parcel)charSequence);
          } else {
            charSequence = null;
          } 
          handleCdmaScpMessage(param1Int1, arrayList, str, (RemoteCallback)charSequence);
          return true;
        } 
        charSequence.enforceInterface("android.telephony.ICellBroadcastService");
        param1Int1 = charSequence.readInt();
        byte[] arrayOfByte1 = charSequence.createByteArray();
        param1Int2 = charSequence.readInt();
        handleCdmaCellBroadcastSms(param1Int1, arrayOfByte1, param1Int2);
        return true;
      } 
      charSequence.enforceInterface("android.telephony.ICellBroadcastService");
      param1Int1 = charSequence.readInt();
      byte[] arrayOfByte = charSequence.createByteArray();
      handleGsmCellBroadcastSms(param1Int1, arrayOfByte);
      return true;
    }
    
    private static class Proxy implements ICellBroadcastService {
      public static ICellBroadcastService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.ICellBroadcastService";
      }
      
      public void handleGsmCellBroadcastSms(int param2Int, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ICellBroadcastService");
          parcel.writeInt(param2Int);
          parcel.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ICellBroadcastService.Stub.getDefaultImpl() != null) {
            ICellBroadcastService.Stub.getDefaultImpl().handleGsmCellBroadcastSms(param2Int, param2ArrayOfbyte);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void handleCdmaCellBroadcastSms(int param2Int1, byte[] param2ArrayOfbyte, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ICellBroadcastService");
          parcel.writeInt(param2Int1);
          parcel.writeByteArray(param2ArrayOfbyte);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && ICellBroadcastService.Stub.getDefaultImpl() != null) {
            ICellBroadcastService.Stub.getDefaultImpl().handleCdmaCellBroadcastSms(param2Int1, param2ArrayOfbyte, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void handleCdmaScpMessage(int param2Int, List<CdmaSmsCbProgramData> param2List, String param2String, RemoteCallback param2RemoteCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.ICellBroadcastService");
          parcel.writeInt(param2Int);
          parcel.writeTypedList(param2List);
          parcel.writeString(param2String);
          if (param2RemoteCallback != null) {
            parcel.writeInt(1);
            param2RemoteCallback.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && ICellBroadcastService.Stub.getDefaultImpl() != null) {
            ICellBroadcastService.Stub.getDefaultImpl().handleCdmaScpMessage(param2Int, param2List, param2String, param2RemoteCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public CharSequence getCellBroadcastAreaInfo(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          CharSequence charSequence;
          parcel1.writeInterfaceToken("android.telephony.ICellBroadcastService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ICellBroadcastService.Stub.getDefaultImpl() != null) {
            charSequence = ICellBroadcastService.Stub.getDefaultImpl().getCellBroadcastAreaInfo(param2Int);
            return charSequence;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel2);
          } else {
            charSequence = null;
          } 
          return charSequence;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICellBroadcastService param1ICellBroadcastService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICellBroadcastService != null) {
          Proxy.sDefaultImpl = param1ICellBroadcastService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICellBroadcastService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
