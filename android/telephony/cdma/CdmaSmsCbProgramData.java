package android.telephony.cdma;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public final class CdmaSmsCbProgramData implements Parcelable {
  public static final int ALERT_OPTION_DEFAULT_ALERT = 1;
  
  public static final int ALERT_OPTION_HIGH_PRIORITY_ONCE = 10;
  
  public static final int ALERT_OPTION_HIGH_PRIORITY_REPEAT = 11;
  
  public static final int ALERT_OPTION_LOW_PRIORITY_ONCE = 6;
  
  public static final int ALERT_OPTION_LOW_PRIORITY_REPEAT = 7;
  
  public static final int ALERT_OPTION_MED_PRIORITY_ONCE = 8;
  
  public static final int ALERT_OPTION_MED_PRIORITY_REPEAT = 9;
  
  public static final int ALERT_OPTION_NO_ALERT = 0;
  
  public static final int ALERT_OPTION_VIBRATE_ONCE = 2;
  
  public static final int ALERT_OPTION_VIBRATE_REPEAT = 3;
  
  public static final int ALERT_OPTION_VISUAL_ONCE = 4;
  
  public static final int ALERT_OPTION_VISUAL_REPEAT = 5;
  
  public static final int CATEGORY_CMAS_CHILD_ABDUCTION_EMERGENCY = 4099;
  
  public static final int CATEGORY_CMAS_EXTREME_THREAT = 4097;
  
  public static final int CATEGORY_CMAS_LAST_RESERVED_VALUE = 4351;
  
  public static final int CATEGORY_CMAS_PRESIDENTIAL_LEVEL_ALERT = 4096;
  
  public static final int CATEGORY_CMAS_SEVERE_THREAT = 4098;
  
  public static final int CATEGORY_CMAS_TEST_MESSAGE = 4100;
  
  public CdmaSmsCbProgramData(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, String paramString) {
    this.mOperation = paramInt1;
    this.mCategory = paramInt2;
    this.mLanguage = paramInt3;
    this.mMaxMessages = paramInt4;
    this.mAlertOption = paramInt5;
    this.mCategoryName = paramString;
  }
  
  CdmaSmsCbProgramData(Parcel paramParcel) {
    this.mOperation = paramParcel.readInt();
    this.mCategory = paramParcel.readInt();
    this.mLanguage = paramParcel.readInt();
    this.mMaxMessages = paramParcel.readInt();
    this.mAlertOption = paramParcel.readInt();
    this.mCategoryName = paramParcel.readString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mOperation);
    paramParcel.writeInt(this.mCategory);
    paramParcel.writeInt(this.mLanguage);
    paramParcel.writeInt(this.mMaxMessages);
    paramParcel.writeInt(this.mAlertOption);
    paramParcel.writeString(this.mCategoryName);
  }
  
  public int getOperation() {
    return this.mOperation;
  }
  
  public int getCategory() {
    return this.mCategory;
  }
  
  public int getLanguage() {
    return this.mLanguage;
  }
  
  public int getMaxMessages() {
    return this.mMaxMessages;
  }
  
  public int getAlertOption() {
    return this.mAlertOption;
  }
  
  public String getCategoryName() {
    return this.mCategoryName;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CdmaSmsCbProgramData{operation=");
    stringBuilder.append(this.mOperation);
    stringBuilder.append(", category=");
    stringBuilder.append(this.mCategory);
    stringBuilder.append(", language=");
    stringBuilder.append(this.mLanguage);
    stringBuilder.append(", max messages=");
    stringBuilder.append(this.mMaxMessages);
    stringBuilder.append(", alert option=");
    stringBuilder.append(this.mAlertOption);
    stringBuilder.append(", category name=");
    stringBuilder.append(this.mCategoryName);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<CdmaSmsCbProgramData> CREATOR = new Parcelable.Creator<CdmaSmsCbProgramData>() {
      public CdmaSmsCbProgramData createFromParcel(Parcel param1Parcel) {
        return new CdmaSmsCbProgramData(param1Parcel);
      }
      
      public CdmaSmsCbProgramData[] newArray(int param1Int) {
        return new CdmaSmsCbProgramData[param1Int];
      }
    };
  
  public static final int OPERATION_ADD_CATEGORY = 1;
  
  public static final int OPERATION_CLEAR_CATEGORIES = 2;
  
  public static final int OPERATION_DELETE_CATEGORY = 0;
  
  private final int mAlertOption;
  
  private final int mCategory;
  
  private final String mCategoryName;
  
  private final int mLanguage;
  
  private final int mMaxMessages;
  
  private final int mOperation;
  
  @Retention(RetentionPolicy.SOURCE)
  class Category implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Operation implements Annotation {}
}
