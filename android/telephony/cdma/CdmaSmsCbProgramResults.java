package android.telephony.cdma;

import android.os.Parcel;
import android.os.Parcelable;

public class CdmaSmsCbProgramResults implements Parcelable {
  public CdmaSmsCbProgramResults(int paramInt1, int paramInt2, int paramInt3) {
    this.mCategory = paramInt1;
    this.mLanguage = paramInt2;
    this.mCategoryResult = paramInt3;
  }
  
  CdmaSmsCbProgramResults(Parcel paramParcel) {
    this.mCategory = paramParcel.readInt();
    this.mLanguage = paramParcel.readInt();
    this.mCategoryResult = paramParcel.readInt();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mCategory);
    paramParcel.writeInt(this.mLanguage);
    paramParcel.writeInt(this.mCategoryResult);
  }
  
  public int getCategory() {
    return this.mCategory;
  }
  
  public int getLanguage() {
    return this.mLanguage;
  }
  
  public int getCategoryResult() {
    return this.mCategoryResult;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CdmaSmsCbProgramResults{category=");
    stringBuilder.append(this.mCategory);
    stringBuilder.append(", language=");
    stringBuilder.append(this.mLanguage);
    stringBuilder.append(", result=");
    stringBuilder.append(this.mCategoryResult);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<CdmaSmsCbProgramResults> CREATOR = new Parcelable.Creator<CdmaSmsCbProgramResults>() {
      public CdmaSmsCbProgramResults createFromParcel(Parcel param1Parcel) {
        return new CdmaSmsCbProgramResults(param1Parcel);
      }
      
      public CdmaSmsCbProgramResults[] newArray(int param1Int) {
        return new CdmaSmsCbProgramResults[param1Int];
      }
    };
  
  public static final int RESULT_CATEGORY_ALREADY_ADDED = 3;
  
  public static final int RESULT_CATEGORY_ALREADY_DELETED = 4;
  
  public static final int RESULT_CATEGORY_LIMIT_EXCEEDED = 2;
  
  public static final int RESULT_INVALID_ALERT_OPTION = 6;
  
  public static final int RESULT_INVALID_CATEGORY_NAME = 7;
  
  public static final int RESULT_INVALID_MAX_MESSAGES = 5;
  
  public static final int RESULT_MEMORY_LIMIT_EXCEEDED = 1;
  
  public static final int RESULT_SUCCESS = 0;
  
  public static final int RESULT_UNSPECIFIED_FAILURE = 8;
  
  private final int mCategory;
  
  private final int mCategoryResult;
  
  private final int mLanguage;
}
