package android.telephony.cdma;

import android.os.Bundle;
import android.telephony.CellLocation;

public class CdmaCellLocation extends CellLocation {
  private int mBaseStationId = -1;
  
  private int mBaseStationLatitude = Integer.MAX_VALUE;
  
  private int mBaseStationLongitude = Integer.MAX_VALUE;
  
  private int mSystemId = -1;
  
  private int mNetworkId = -1;
  
  public static final int INVALID_LAT_LONG = 2147483647;
  
  public CdmaCellLocation() {
    this.mBaseStationId = -1;
    this.mBaseStationLatitude = Integer.MAX_VALUE;
    this.mBaseStationLongitude = Integer.MAX_VALUE;
    this.mSystemId = -1;
    this.mNetworkId = -1;
  }
  
  public CdmaCellLocation(Bundle paramBundle) {
    this.mBaseStationId = paramBundle.getInt("baseStationId", -1);
    this.mBaseStationLatitude = paramBundle.getInt("baseStationLatitude", this.mBaseStationLatitude);
    this.mBaseStationLongitude = paramBundle.getInt("baseStationLongitude", this.mBaseStationLongitude);
    this.mSystemId = paramBundle.getInt("systemId", this.mSystemId);
    this.mNetworkId = paramBundle.getInt("networkId", this.mNetworkId);
  }
  
  public int getBaseStationId() {
    return this.mBaseStationId;
  }
  
  public int getBaseStationLatitude() {
    return this.mBaseStationLatitude;
  }
  
  public int getBaseStationLongitude() {
    return this.mBaseStationLongitude;
  }
  
  public int getSystemId() {
    return this.mSystemId;
  }
  
  public int getNetworkId() {
    return this.mNetworkId;
  }
  
  public void setStateInvalid() {
    this.mBaseStationId = -1;
    this.mBaseStationLatitude = Integer.MAX_VALUE;
    this.mBaseStationLongitude = Integer.MAX_VALUE;
    this.mSystemId = -1;
    this.mNetworkId = -1;
  }
  
  public void setCellLocationData(int paramInt1, int paramInt2, int paramInt3) {
    this.mBaseStationId = paramInt1;
    this.mBaseStationLatitude = paramInt2;
    this.mBaseStationLongitude = paramInt3;
  }
  
  public void setCellLocationData(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this.mBaseStationId = paramInt1;
    this.mBaseStationLatitude = paramInt2;
    this.mBaseStationLongitude = paramInt3;
    this.mSystemId = paramInt4;
    this.mNetworkId = paramInt5;
  }
  
  public int hashCode() {
    return this.mBaseStationId ^ this.mBaseStationLatitude ^ this.mBaseStationLongitude ^ this.mSystemId ^ this.mNetworkId;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    try {
      CdmaCellLocation cdmaCellLocation = (CdmaCellLocation)paramObject;
      if (paramObject == null)
        return false; 
      if (equalsHandlesNulls(Integer.valueOf(this.mBaseStationId), Integer.valueOf(cdmaCellLocation.mBaseStationId))) {
        int i = this.mBaseStationLatitude;
        if (equalsHandlesNulls(Integer.valueOf(i), Integer.valueOf(cdmaCellLocation.mBaseStationLatitude))) {
          i = this.mBaseStationLongitude;
          if (equalsHandlesNulls(Integer.valueOf(i), Integer.valueOf(cdmaCellLocation.mBaseStationLongitude))) {
            i = this.mSystemId;
            if (equalsHandlesNulls(Integer.valueOf(i), Integer.valueOf(cdmaCellLocation.mSystemId))) {
              i = this.mNetworkId;
              if (equalsHandlesNulls(Integer.valueOf(i), Integer.valueOf(cdmaCellLocation.mNetworkId)))
                bool = true; 
            } 
          } 
        } 
      } 
      return bool;
    } catch (ClassCastException classCastException) {
      return false;
    } 
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[");
    stringBuilder.append(this.mBaseStationId);
    stringBuilder.append(",");
    stringBuilder.append(this.mBaseStationLatitude);
    stringBuilder.append(",");
    stringBuilder.append(this.mBaseStationLongitude);
    stringBuilder.append(",");
    stringBuilder.append(this.mSystemId);
    stringBuilder.append(",");
    stringBuilder.append(this.mNetworkId);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  private static boolean equalsHandlesNulls(Object paramObject1, Object paramObject2) {
    boolean bool;
    if (paramObject1 == null) {
      if (paramObject2 == null) {
        bool = true;
      } else {
        bool = false;
      } 
    } else {
      bool = paramObject1.equals(paramObject2);
    } 
    return bool;
  }
  
  public void fillInNotifierBundle(Bundle paramBundle) {
    paramBundle.putInt("baseStationId", this.mBaseStationId);
    paramBundle.putInt("baseStationLatitude", this.mBaseStationLatitude);
    paramBundle.putInt("baseStationLongitude", this.mBaseStationLongitude);
    paramBundle.putInt("systemId", this.mSystemId);
    paramBundle.putInt("networkId", this.mNetworkId);
  }
  
  public boolean isEmpty() {
    boolean bool;
    if (this.mBaseStationId == -1 && this.mBaseStationLatitude == Integer.MAX_VALUE && this.mBaseStationLongitude == Integer.MAX_VALUE && this.mSystemId == -1 && this.mNetworkId == -1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static double convertQuartSecToDecDegrees(int paramInt) {
    if (!Double.isNaN(paramInt) && paramInt >= -2592000 && paramInt <= 2592000)
      return paramInt / 14400.0D; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid coordiante value:");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
}
