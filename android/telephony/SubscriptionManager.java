package android.telephony;

import android.annotation.SystemApi;
import android.app.PendingIntent;
import android.app.PropertyInvalidatedCache;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.NetworkPolicyManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.ParcelUuid;
import android.os.Process;
import android.os.RemoteException;
import android.os.TelephonyServiceManager;
import android.provider.Telephony;
import android.telephony.euicc.EuiccManager;
import android.util.Log;
import android.util.Pair;
import com.android.internal.telephony.ISetOpportunisticDataCallback;
import com.android.internal.telephony.ISub;
import com.android.internal.telephony.util.HandlerExecutor;
import com.android.internal.util.FunctionalUtils;
import com.android.internal.util.Preconditions;
import com.android.telephony.Rlog;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.function.Consumer;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SubscriptionManager {
  public static final String ACCESS_RULES = "access_rules";
  
  public static final String ACCESS_RULES_FROM_CARRIER_CONFIGS = "access_rules_from_carrier_configs";
  
  public static final String ACTION_DEFAULT_SMS_SUBSCRIPTION_CHANGED = "android.telephony.action.DEFAULT_SMS_SUBSCRIPTION_CHANGED";
  
  public static final String ACTION_DEFAULT_SUBSCRIPTION_CHANGED = "android.telephony.action.DEFAULT_SUBSCRIPTION_CHANGED";
  
  public static final String ACTION_MANAGE_SUBSCRIPTION_PLANS = "android.telephony.action.MANAGE_SUBSCRIPTION_PLANS";
  
  public static final String ACTION_REFRESH_SUBSCRIPTION_PLANS = "android.telephony.action.REFRESH_SUBSCRIPTION_PLANS";
  
  @SystemApi
  public static final String ACTION_SUBSCRIPTION_PLANS_CHANGED = "android.telephony.action.SUBSCRIPTION_PLANS_CHANGED";
  
  @SystemApi
  public static final Uri ADVANCED_CALLING_ENABLED_CONTENT_URI;
  
  public static final String ALLOWED_NETWORK_TYPES = "allowed_network_types";
  
  public static final String CACHE_KEY_ACTIVE_DATA_SUB_ID_PROPERTY = "cache_key.telephony.get_active_data_sub_id";
  
  public static final String CACHE_KEY_DEFAULT_DATA_SUB_ID_PROPERTY = "cache_key.telephony.get_default_data_sub_id";
  
  public static final String CACHE_KEY_DEFAULT_SMS_SUB_ID_PROPERTY = "cache_key.telephony.get_default_sms_sub_id";
  
  public static final String CACHE_KEY_DEFAULT_SUB_ID_PROPERTY = "cache_key.telephony.get_default_sub_id";
  
  public static final String CACHE_KEY_SLOT_INDEX_PROPERTY = "cache_key.telephony.get_slot_index";
  
  public static final String CARD_ID = "card_id";
  
  public static final String CARRIER_ID = "carrier_id";
  
  public static final String CARRIER_NAME = "carrier_name";
  
  public static final String CB_ALERT_REMINDER_INTERVAL = "alert_reminder_interval";
  
  public static final String CB_ALERT_SOUND_DURATION = "alert_sound_duration";
  
  public static final String CB_ALERT_SPEECH = "enable_alert_speech";
  
  public static final String CB_ALERT_VIBRATE = "enable_alert_vibrate";
  
  public static final String CB_AMBER_ALERT = "enable_cmas_amber_alerts";
  
  public static final String CB_CHANNEL_50_ALERT = "enable_channel_50_alerts";
  
  public static final String CB_CMAS_TEST_ALERT = "enable_cmas_test_alerts";
  
  public static final String CB_EMERGENCY_ALERT = "enable_emergency_alerts";
  
  public static final String CB_ETWS_TEST_ALERT = "enable_etws_test_alerts";
  
  public static final String CB_EXTREME_THREAT_ALERT = "enable_cmas_extreme_threat_alerts";
  
  public static final String CB_OPT_OUT_DIALOG = "show_cmas_opt_out_dialog";
  
  public static final String CB_SEVERE_THREAT_ALERT = "enable_cmas_severe_threat_alerts";
  
  public static final Uri CONTENT_URI = Telephony.SimInfo.CONTENT_URI;
  
  public static final String DATA_ENABLED_OVERRIDE_RULES = "data_enabled_override_rules";
  
  public static final String DATA_ROAMING = "data_roaming";
  
  public static final int DATA_ROAMING_DISABLE = 0;
  
  public static final int DATA_ROAMING_ENABLE = 1;
  
  private static final boolean DBG = false;
  
  public static final int DEFAULT_NAME_RES = 17039374;
  
  public static final int DEFAULT_PHONE_INDEX = 2147483647;
  
  public static final int DEFAULT_SIM_SLOT_INDEX = 2147483647;
  
  public static final int DEFAULT_SUBSCRIPTION_ID = 2147483647;
  
  public static final String DISPLAY_NAME = "display_name";
  
  public static final int DUMMY_SUBSCRIPTION_ID_BASE = -2;
  
  public static final String EHPLMNS = "ehplmns";
  
  public static final String ENHANCED_4G_MODE_ENABLED = "volte_vt_enabled";
  
  public static final String EXTRA_SLOT_INDEX = "android.telephony.extra.SLOT_INDEX";
  
  public static final String EXTRA_SUBSCRIPTION_INDEX = "android.telephony.extra.SUBSCRIPTION_INDEX";
  
  public static final String GROUP_OWNER = "group_owner";
  
  public static final String GROUP_UUID = "group_uuid";
  
  public static final String HPLMNS = "hplmns";
  
  public static final String HUE = "color";
  
  public static final String ICC_ID = "icc_id";
  
  public static final String IMSI = "imsi";
  
  public static final String IMS_RCS_UCE_ENABLED = "ims_rcs_uce_enabled";
  
  public static final int INVALID_PHONE_INDEX = -1;
  
  public static final int INVALID_SIM_SLOT_INDEX = -1;
  
  public static final int INVALID_SUBSCRIPTION_ID = -1;
  
  public static final String ISO_COUNTRY_CODE = "iso_country_code";
  
  public static final String IS_EMBEDDED = "is_embedded";
  
  public static final String IS_OPPORTUNISTIC = "is_opportunistic";
  
  public static final String IS_REMOVABLE = "is_removable";
  
  private static final String LOG_TAG = "SubscriptionManager";
  
  private static final int MAX_CACHE_SIZE = 4;
  
  public static final int MAX_SUBSCRIPTION_ID_VALUE = 2147483646;
  
  public static final String MCC = "mcc";
  
  public static final String MCC_STRING = "mcc_string";
  
  public static final int MIN_SUBSCRIPTION_ID_VALUE = 0;
  
  public static final String MNC = "mnc";
  
  public static final String MNC_STRING = "mnc_string";
  
  public static final String NAME_SOURCE = "name_source";
  
  public static final int NAME_SOURCE_CARRIER = 3;
  
  public static final int NAME_SOURCE_CARRIER_ID = 0;
  
  public static final int NAME_SOURCE_SIM_PNN = 4;
  
  public static final int NAME_SOURCE_SIM_SPN = 1;
  
  public static final int NAME_SOURCE_USER_INPUT = 2;
  
  public static final String NUMBER = "number";
  
  public static final String PROFILE_CLASS = "profile_class";
  
  @SystemApi
  @Deprecated
  public static final int PROFILE_CLASS_DEFAULT = -1;
  
  @SystemApi
  public static final int PROFILE_CLASS_OPERATIONAL = 2;
  
  @SystemApi
  public static final int PROFILE_CLASS_PROVISIONING = 1;
  
  @SystemApi
  public static final int PROFILE_CLASS_TESTING = 0;
  
  @SystemApi
  public static final int PROFILE_CLASS_UNSET = -1;
  
  public static final int SIM_NOT_INSERTED = -1;
  
  public static final String SIM_SLOT_INDEX = "sim_id";
  
  public static final int SLOT_INDEX_FOR_REMOTE_SIM_SUB = -1;
  
  public static final String SUBSCRIPTION_TYPE = "subscription_type";
  
  public static final int SUBSCRIPTION_TYPE_LOCAL_SIM = 0;
  
  public static final int SUBSCRIPTION_TYPE_REMOTE_SIM = 1;
  
  public static final String SUB_DEFAULT_CHANGED_ACTION = "android.intent.action.SUB_DEFAULT_CHANGED";
  
  public static final String UICC_APPLICATIONS_ENABLED = "uicc_applications_enabled";
  
  public static final String UNIQUE_KEY_SUBSCRIPTION_ID = "_id";
  
  private static final boolean VDBG = false;
  
  @SystemApi
  public static final Uri VT_ENABLED_CONTENT_URI;
  
  public static final String VT_IMS_ENABLED = "vt_ims_enabled";
  
  @SystemApi
  public static final Uri WFC_ENABLED_CONTENT_URI;
  
  public static final String WFC_IMS_ENABLED = "wfc_ims_enabled";
  
  public static final String WFC_IMS_MODE = "wfc_ims_mode";
  
  public static final String WFC_IMS_ROAMING_ENABLED = "wfc_ims_roaming_enabled";
  
  public static final String WFC_IMS_ROAMING_MODE = "wfc_ims_roaming_mode";
  
  @SystemApi
  public static final Uri WFC_MODE_CONTENT_URI;
  
  @SystemApi
  public static final Uri WFC_ROAMING_ENABLED_CONTENT_URI;
  
  @SystemApi
  public static final Uri WFC_ROAMING_MODE_CONTENT_URI;
  
  private static VoidPropertyInvalidatedCache<Integer> sActiveDataSubIdCache;
  
  private static VoidPropertyInvalidatedCache<Integer> sDefaultDataSubIdCache;
  
  private static VoidPropertyInvalidatedCache<Integer> sDefaultSmsSubIdCache;
  
  private static VoidPropertyInvalidatedCache<Integer> sDefaultSubIdCache;
  
  private static IntegerPropertyInvalidatedCache<Integer> sPhoneIdCache;
  
  private static final Map<Pair<Context, Integer>, Resources> sResourcesCache;
  
  private static IntegerPropertyInvalidatedCache<Integer> sSlotIndexCache;
  
  private final Context mContext;
  
  class VoidPropertyInvalidatedCache<T> extends PropertyInvalidatedCache<Void, T> {
    private final String mCacheKeyProperty;
    
    private final T mDefaultValue;
    
    private final FunctionalUtils.ThrowingFunction<ISub, T> mInterfaceMethod;
    
    VoidPropertyInvalidatedCache(SubscriptionManager this$0, String param1String, T param1T) {
      super(4, param1String);
      this.mInterfaceMethod = (FunctionalUtils.ThrowingFunction<ISub, T>)this$0;
      this.mCacheKeyProperty = param1String;
      this.mDefaultValue = param1T;
    }
    
    protected T recompute(Void param1Void) {
      T t1, t2 = this.mDefaultValue;
      try {
        ISub iSub = TelephonyManager.getSubscriptionService();
        t1 = t2;
        if (iSub != null)
          t1 = (T)this.mInterfaceMethod.applyOrThrow(iSub); 
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to recompute cache key for ");
        stringBuilder.append(this.mCacheKeyProperty);
        Rlog.w("SubscriptionManager", stringBuilder.toString());
        t1 = t2;
      } 
      return t1;
    }
  }
  
  class IntegerPropertyInvalidatedCache<T> extends PropertyInvalidatedCache<Integer, T> {
    private final String mCacheKeyProperty;
    
    private final T mDefaultValue;
    
    private final FunctionalUtils.ThrowingBiFunction<ISub, Integer, T> mInterfaceMethod;
    
    IntegerPropertyInvalidatedCache(SubscriptionManager this$0, String param1String, T param1T) {
      super(4, param1String);
      this.mInterfaceMethod = (FunctionalUtils.ThrowingBiFunction<ISub, Integer, T>)this$0;
      this.mCacheKeyProperty = param1String;
      this.mDefaultValue = param1T;
    }
    
    protected T recompute(Integer param1Integer) {
      T t2, t1 = this.mDefaultValue;
      try {
        ISub iSub = TelephonyManager.getSubscriptionService();
        t2 = t1;
        if (iSub != null)
          t2 = (T)this.mInterfaceMethod.applyOrThrow(iSub, param1Integer); 
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to recompute cache key for ");
        stringBuilder.append(this.mCacheKeyProperty);
        Rlog.w("SubscriptionManager", stringBuilder.toString());
        t2 = t1;
      } 
      return t2;
    }
  }
  
  static {
    -$.Lambda.VtfSvbW0tRP_qFDYPVM9jEdZHj0 vtfSvbW0tRP_qFDYPVM9jEdZHj0 = _$$Lambda$VtfSvbW0tRP_qFDYPVM9jEdZHj0.INSTANCE;
    Integer integer = Integer.valueOf(-1);
    sDefaultSubIdCache = new VoidPropertyInvalidatedCache<>((FunctionalUtils.ThrowingFunction<ISub, Integer>)vtfSvbW0tRP_qFDYPVM9jEdZHj0, "cache_key.telephony.get_default_sub_id", integer);
    -$.Lambda.Rj1EhkciYpNb4BkVxAk-tibQjhM rj1EhkciYpNb4BkVxAk-tibQjhM = _$$Lambda$Rj1EhkciYpNb4BkVxAk_tibQjhM.INSTANCE;
    sDefaultDataSubIdCache = new VoidPropertyInvalidatedCache<>((FunctionalUtils.ThrowingFunction<ISub, Integer>)rj1EhkciYpNb4BkVxAk-tibQjhM, "cache_key.telephony.get_default_data_sub_id", integer);
    -$.Lambda.VoWbarPy40APZWYZ2AqZZxi_Jm8 voWbarPy40APZWYZ2AqZZxi_Jm8 = _$$Lambda$VoWbarPy40APZWYZ2AqZZxi_Jm8.INSTANCE;
    sDefaultSmsSubIdCache = new VoidPropertyInvalidatedCache<>((FunctionalUtils.ThrowingFunction<ISub, Integer>)voWbarPy40APZWYZ2AqZZxi_Jm8, "cache_key.telephony.get_default_sms_sub_id", integer);
    -$.Lambda.NbX5ZB4Wdogc_DUyrSlzFoDHvU nbX5ZB4Wdogc_DUyrSlzFoDHvU = _$$Lambda$0NbX5ZB4Wdogc_DUyrSlzFoDHvU.INSTANCE;
    sActiveDataSubIdCache = new VoidPropertyInvalidatedCache<>((FunctionalUtils.ThrowingFunction<ISub, Integer>)nbX5ZB4Wdogc_DUyrSlzFoDHvU, "cache_key.telephony.get_active_data_sub_id", integer);
    -$.Lambda.Vaai8Sbs2IpNs9Mr8tx6u3YoWp4 vaai8Sbs2IpNs9Mr8tx6u3YoWp4 = _$$Lambda$Vaai8Sbs2IpNs9Mr8tx6u3YoWp4.INSTANCE;
    sSlotIndexCache = new IntegerPropertyInvalidatedCache<>((FunctionalUtils.ThrowingBiFunction<ISub, Integer, Integer>)vaai8Sbs2IpNs9Mr8tx6u3YoWp4, "cache_key.telephony.get_slot_index", integer);
    -$.Lambda.U5dt9Oz29BpLzJ19WIl50whqAGs u5dt9Oz29BpLzJ19WIl50whqAGs = _$$Lambda$U5dt9Oz29BpLzJ19WIl50whqAGs.INSTANCE;
    sPhoneIdCache = new IntegerPropertyInvalidatedCache<>((FunctionalUtils.ThrowingBiFunction<ISub, Integer, Integer>)u5dt9Oz29BpLzJ19WIl50whqAGs, "cache_key.telephony.get_default_sub_id", integer);
    WFC_ENABLED_CONTENT_URI = Uri.withAppendedPath(CONTENT_URI, "wfc");
    ADVANCED_CALLING_ENABLED_CONTENT_URI = Uri.withAppendedPath(CONTENT_URI, "advanced_calling");
    WFC_MODE_CONTENT_URI = Uri.withAppendedPath(CONTENT_URI, "wfc_mode");
    WFC_ROAMING_MODE_CONTENT_URI = Uri.withAppendedPath(CONTENT_URI, "wfc_roaming_mode");
    VT_ENABLED_CONTENT_URI = Uri.withAppendedPath(CONTENT_URI, "vt_enabled");
    WFC_ROAMING_ENABLED_CONTENT_URI = Uri.withAppendedPath(CONTENT_URI, "wfc_roaming_enabled");
    sResourcesCache = new ConcurrentHashMap<>();
  }
  
  public static Uri getUriForSubscriptionId(int paramInt) {
    return Uri.withAppendedPath(CONTENT_URI, String.valueOf(paramInt));
  }
  
  public static class OnSubscriptionsChangedListener {
    private final HandlerExecutor mExecutor;
    
    class OnSubscriptionsChangedListenerHandler extends Handler {
      final SubscriptionManager.OnSubscriptionsChangedListener this$0;
      
      OnSubscriptionsChangedListenerHandler() {}
      
      OnSubscriptionsChangedListenerHandler(Looper param2Looper) {
        super(param2Looper);
      }
    }
    
    public HandlerExecutor getHandlerExecutor() {
      return this.mExecutor;
    }
    
    public OnSubscriptionsChangedListener() {
      this.mExecutor = new HandlerExecutor(new OnSubscriptionsChangedListenerHandler());
    }
    
    public OnSubscriptionsChangedListener(Looper param1Looper) {
      this.mExecutor = new HandlerExecutor(new OnSubscriptionsChangedListenerHandler(param1Looper));
    }
    
    public void onSubscriptionsChanged() {}
    
    public void onAddListenerFailed() {
      Rlog.w("SubscriptionManager", "onAddListenerFailed not overridden");
    }
    
    private void log(String param1String) {
      Rlog.d("SubscriptionManager", param1String);
    }
  }
  
  class OnSubscriptionsChangedListenerHandler extends Handler {
    final SubscriptionManager.OnSubscriptionsChangedListener this$0;
    
    OnSubscriptionsChangedListenerHandler() {}
    
    OnSubscriptionsChangedListenerHandler(Looper param1Looper) {
      super(param1Looper);
    }
  }
  
  public SubscriptionManager(Context paramContext) {
    this.mContext = paramContext;
  }
  
  private NetworkPolicyManager getNetworkPolicyManager() {
    Context context = this.mContext;
    return 
      (NetworkPolicyManager)context.getSystemService("netpolicy");
  }
  
  @Deprecated
  public static SubscriptionManager from(Context paramContext) {
    return 
      (SubscriptionManager)paramContext.getSystemService("telephony_subscription_service");
  }
  
  public void addOnSubscriptionsChangedListener(OnSubscriptionsChangedListener paramOnSubscriptionsChangedListener) {
    if (paramOnSubscriptionsChangedListener == null)
      return; 
    addOnSubscriptionsChangedListener((Executor)paramOnSubscriptionsChangedListener.mExecutor, paramOnSubscriptionsChangedListener);
  }
  
  public void addOnSubscriptionsChangedListener(Executor paramExecutor, OnSubscriptionsChangedListener paramOnSubscriptionsChangedListener) {
    String str;
    Context context1 = this.mContext;
    if (context1 != null) {
      str = context1.getOpPackageName();
    } else {
      str = "<unknown>";
    } 
    Context context2 = this.mContext;
    TelephonyRegistryManager telephonyRegistryManager = (TelephonyRegistryManager)context2.getSystemService("telephony_registry");
    if (telephonyRegistryManager != null) {
      telephonyRegistryManager.addOnSubscriptionsChangedListener(paramOnSubscriptionsChangedListener, paramExecutor);
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("addOnSubscriptionsChangedListener: pkgname=");
      stringBuilder.append(str);
      stringBuilder.append(" failed to be added  due to TELEPHONY_REGISTRY_SERVICE being unavailable.");
      loge(stringBuilder.toString());
      paramExecutor.execute(new _$$Lambda$SubscriptionManager$TVQ_FjyYRlVRKpgsmPOQsZrBDJs(paramOnSubscriptionsChangedListener));
    } 
  }
  
  public void removeOnSubscriptionsChangedListener(OnSubscriptionsChangedListener paramOnSubscriptionsChangedListener) {
    if (paramOnSubscriptionsChangedListener == null)
      return; 
    Context context = this.mContext;
    if (context != null)
      context.getOpPackageName(); 
    context = this.mContext;
    TelephonyRegistryManager telephonyRegistryManager = (TelephonyRegistryManager)context.getSystemService("telephony_registry");
    if (telephonyRegistryManager != null)
      telephonyRegistryManager.removeOnSubscriptionsChangedListener(paramOnSubscriptionsChangedListener); 
  }
  
  public static class OnOpportunisticSubscriptionsChangedListener {
    public void onOpportunisticSubscriptionsChanged() {}
    
    private void log(String param1String) {
      Rlog.d("SubscriptionManager", param1String);
    }
  }
  
  public void addOnOpportunisticSubscriptionsChangedListener(Executor paramExecutor, OnOpportunisticSubscriptionsChangedListener paramOnOpportunisticSubscriptionsChangedListener) {
    if (paramExecutor == null || paramOnOpportunisticSubscriptionsChangedListener == null)
      return; 
    Context context = this.mContext;
    if (context != null)
      context.getOpPackageName(); 
    context = this.mContext;
    TelephonyRegistryManager telephonyRegistryManager = (TelephonyRegistryManager)context.getSystemService("telephony_registry");
    if (telephonyRegistryManager != null)
      telephonyRegistryManager.addOnOpportunisticSubscriptionsChangedListener(paramOnOpportunisticSubscriptionsChangedListener, paramExecutor); 
  }
  
  public void removeOnOpportunisticSubscriptionsChangedListener(OnOpportunisticSubscriptionsChangedListener paramOnOpportunisticSubscriptionsChangedListener) {
    Preconditions.checkNotNull(paramOnOpportunisticSubscriptionsChangedListener, "listener cannot be null");
    Context context = this.mContext;
    if (context != null)
      context.getOpPackageName(); 
    context = this.mContext;
    TelephonyRegistryManager telephonyRegistryManager = (TelephonyRegistryManager)context.getSystemService("telephony_registry");
    if (telephonyRegistryManager != null)
      telephonyRegistryManager.removeOnOpportunisticSubscriptionsChangedListener(paramOnOpportunisticSubscriptionsChangedListener); 
  }
  
  public SubscriptionInfo getActiveSubscriptionInfo(int paramInt) {
    if (!isValidSubscriptionId(paramInt))
      return null; 
    RemoteException remoteException1 = null;
    String str = null;
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null) {
        str = this.mContext.getOpPackageName();
        Context context = this.mContext;
        String str1 = context.getAttributionTag();
        SubscriptionInfo subscriptionInfo = iSub.getActiveSubscriptionInfo(paramInt, str, str1);
      } 
    } catch (RemoteException remoteException2) {
      remoteException2 = remoteException1;
    } 
    return (SubscriptionInfo)remoteException2;
  }
  
  @SystemApi
  public SubscriptionInfo getActiveSubscriptionInfoForIcc(String paramString) {
    if (paramString == null) {
      logd("[getActiveSubscriptionInfoForIccIndex]- null iccid");
      return null;
    } 
    String str1 = null, str2 = null;
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null) {
        str2 = this.mContext.getOpPackageName();
        Context context = this.mContext;
        String str = context.getAttributionTag();
        SubscriptionInfo subscriptionInfo = iSub.getActiveSubscriptionInfoForIccId(paramString, str2, str);
      } 
    } catch (RemoteException remoteException) {
      str2 = str1;
    } 
    return (SubscriptionInfo)str2;
  }
  
  public SubscriptionInfo getActiveSubscriptionInfoForSimSlotIndex(int paramInt) {
    if (!isValidSlotIndex(paramInt)) {
      logd("[getActiveSubscriptionInfoForSimSlotIndex]- invalid slotIndex");
      return null;
    } 
    RemoteException remoteException1 = null;
    Context context = null;
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null) {
        context = this.mContext;
        String str2 = context.getOpPackageName(), str1 = this.mContext.getAttributionTag();
        SubscriptionInfo subscriptionInfo = iSub.getActiveSubscriptionInfoForSimSlotIndex(paramInt, str2, str1);
      } 
    } catch (RemoteException remoteException2) {
      remoteException2 = remoteException1;
    } 
    return (SubscriptionInfo)remoteException2;
  }
  
  public List<SubscriptionInfo> getAllSubscriptionInfoList() {
    ArrayList<SubscriptionInfo> arrayList;
    RemoteException remoteException1 = null;
    String str = null;
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null) {
        str = this.mContext.getOpPackageName();
        Context context = this.mContext;
        String str1 = context.getAttributionTag();
        List list = iSub.getAllSubInfoList(str, str1);
      } 
    } catch (RemoteException remoteException2) {
      remoteException2 = remoteException1;
    } 
    remoteException1 = remoteException2;
    if (remoteException2 == null)
      arrayList = new ArrayList(); 
    return arrayList;
  }
  
  public List<SubscriptionInfo> getActiveSubscriptionInfoList() {
    return getActiveSubscriptionInfoList(true);
  }
  
  public List<SubscriptionInfo> getCompleteActiveSubscriptionInfoList() {
    List<SubscriptionInfo> list1 = getActiveSubscriptionInfoList(false);
    List<SubscriptionInfo> list2 = list1;
    if (list1 == null)
      list2 = new ArrayList<>(); 
    return list2;
  }
  
  public List<SubscriptionInfo> getActiveSubscriptionInfoList(boolean paramBoolean) {
    RemoteException remoteException1 = null;
    String str = null;
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null) {
        str = this.mContext.getOpPackageName();
        Context context = this.mContext;
        String str1 = context.getAttributionTag();
        List list = iSub.getActiveSubscriptionInfoList(str, str1);
      } 
    } catch (RemoteException remoteException2) {
      remoteException2 = remoteException1;
    } 
    if (!paramBoolean || remoteException2 == null)
      return (List<SubscriptionInfo>)remoteException2; 
    Stream stream = remoteException2.stream().filter(new _$$Lambda$SubscriptionManager$BFE6hex1480LcW4ZjtlaBEqYbEs(this));
    return 
      (List)stream.collect(Collectors.toList());
  }
  
  @SystemApi
  public List<SubscriptionInfo> getAvailableSubscriptionInfoList() {
    RemoteException remoteException1 = null;
    String str = null;
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null) {
        str = this.mContext.getOpPackageName();
        Context context = this.mContext;
        String str1 = context.getAttributionTag();
        List list = iSub.getAvailableSubscriptionInfoList(str, str1);
      } 
    } catch (RemoteException remoteException2) {
      remoteException2 = remoteException1;
    } 
    return (List<SubscriptionInfo>)remoteException2;
  }
  
  public List<SubscriptionInfo> getAccessibleSubscriptionInfoList() {
    RemoteException remoteException1 = null;
    List list = null;
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null)
        list = iSub.getAccessibleSubscriptionInfoList(this.mContext.getOpPackageName()); 
    } catch (RemoteException remoteException2) {
      remoteException2 = remoteException1;
    } 
    return (List<SubscriptionInfo>)remoteException2;
  }
  
  @SystemApi
  public void requestEmbeddedSubscriptionInfoListRefresh() {
    int i = TelephonyManager.from(this.mContext).getCardIdForDefaultEuicc();
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null)
        iSub.requestEmbeddedSubscriptionInfoListRefresh(i); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("requestEmbeddedSubscriptionInfoListFresh for card = ");
      stringBuilder.append(i);
      stringBuilder.append(" failed.");
      logd(stringBuilder.toString());
    } 
  }
  
  @SystemApi
  public void requestEmbeddedSubscriptionInfoListRefresh(int paramInt) {
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null)
        iSub.requestEmbeddedSubscriptionInfoListRefresh(paramInt); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("requestEmbeddedSubscriptionInfoListFresh for card = ");
      stringBuilder.append(paramInt);
      stringBuilder.append(" failed.");
      logd(stringBuilder.toString());
    } 
  }
  
  public int getAllSubscriptionInfoCount() {
    boolean bool = false;
    int i = 0;
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null) {
        String str1 = this.mContext.getOpPackageName();
        Context context = this.mContext;
        String str2 = context.getAttributionTag();
        i = iSub.getAllSubInfoCount(str1, str2);
      } 
    } catch (RemoteException remoteException) {
      i = bool;
    } 
    return i;
  }
  
  public int getActiveSubscriptionInfoCount() {
    boolean bool = false;
    int i = 0;
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null) {
        String str1 = this.mContext.getOpPackageName();
        Context context = this.mContext;
        String str2 = context.getAttributionTag();
        i = iSub.getActiveSubInfoCount(str1, str2);
      } 
    } catch (RemoteException remoteException) {
      i = bool;
    } 
    return i;
  }
  
  public int getActiveSubscriptionInfoCountMax() {
    boolean bool = false;
    int i = 0;
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null)
        i = iSub.getActiveSubInfoCountMax(); 
    } catch (RemoteException remoteException) {
      i = bool;
    } 
    return i;
  }
  
  public Uri addSubscriptionInfoRecord(String paramString, int paramInt) {
    if (paramString == null)
      logd("[addSubscriptionInfoRecord]- null iccId"); 
    if (!isValidSlotIndex(paramInt))
      logd("[addSubscriptionInfoRecord]- invalid slotIndex"); 
    addSubscriptionInfoRecord(paramString, null, paramInt, 0);
    return null;
  }
  
  public void addSubscriptionInfoRecord(String paramString1, String paramString2, int paramInt1, int paramInt2) {
    if (paramString1 == null) {
      Log.e("SubscriptionManager", "[addSubscriptionInfoRecord]- uniqueId is null");
      return;
    } 
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub == null) {
        Log.e("SubscriptionManager", "[addSubscriptionInfoRecord]- ISub service is null");
        return;
      } 
      paramInt1 = iSub.addSubInfo(paramString1, paramString2, paramInt1, paramInt2);
      if (paramInt1 < 0) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Adding of subscription didn't succeed: error = ");
        stringBuilder.append(paramInt1);
        Log.e("SubscriptionManager", stringBuilder.toString());
      } else {
        logd("successfully added new subscription");
      } 
    } catch (RemoteException remoteException) {}
  }
  
  public void removeSubscriptionInfoRecord(String paramString, int paramInt) {
    if (paramString == null) {
      Log.e("SubscriptionManager", "[addSubscriptionInfoRecord]- uniqueId is null");
      return;
    } 
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub == null) {
        Log.e("SubscriptionManager", "[removeSubscriptionInfoRecord]- ISub service is null");
        return;
      } 
      paramInt = iSub.removeSubInfo(paramString, paramInt);
      if (paramInt < 0) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Removal of subscription didn't succeed: error = ");
        stringBuilder.append(paramInt);
        Log.e("SubscriptionManager", stringBuilder.toString());
      } else {
        logd("successfully removed subscription");
      } 
    } catch (RemoteException remoteException) {}
  }
  
  public int setIconTint(int paramInt1, int paramInt2) {
    return setSubscriptionPropertyHelper(paramInt2, "setIconTint", new _$$Lambda$SubscriptionManager$YBVyClIRQJdWREquhRoX1Ha8_yw(paramInt1, paramInt2));
  }
  
  public int setDisplayName(String paramString, int paramInt1, int paramInt2) {
    return setSubscriptionPropertyHelper(paramInt1, "setDisplayName", new _$$Lambda$SubscriptionManager$vq2TobVWeB6FgTmZtD6jJ3grtk0(paramString, paramInt1, paramInt2));
  }
  
  public int setDisplayNumber(String paramString, int paramInt) {
    if (paramString == null) {
      logd("[setDisplayNumber]- fail");
      return -1;
    } 
    return setSubscriptionPropertyHelper(paramInt, "setDisplayNumber", new _$$Lambda$SubscriptionManager$_V9Pitohq6YI8ab2G44kCDK4K4M(paramString, paramInt));
  }
  
  public int setDataRoaming(int paramInt1, int paramInt2) {
    return setSubscriptionPropertyHelper(paramInt2, "setDataRoaming", new _$$Lambda$SubscriptionManager$3xL3VvVeihgpLYlVgAuAtdlfmlo(paramInt1, paramInt2));
  }
  
  public static int getSlotIndex(int paramInt) {
    return ((Integer)sSlotIndexCache.query(Integer.valueOf(paramInt))).intValue();
  }
  
  public int[] getSubscriptionIds(int paramInt) {
    return getSubId(paramInt);
  }
  
  public static int[] getSubId(int paramInt) {
    if (!isValidSlotIndex(paramInt)) {
      logd("[getSubId]- fail");
      return null;
    } 
    RemoteException remoteException1 = null;
    int[] arrayOfInt = null;
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null)
        arrayOfInt = iSub.getSubId(paramInt); 
    } catch (RemoteException remoteException2) {
      remoteException2 = remoteException1;
    } 
    return (int[])remoteException2;
  }
  
  public static int getPhoneId(int paramInt) {
    return ((Integer)sPhoneIdCache.query(Integer.valueOf(paramInt))).intValue();
  }
  
  private static void logd(String paramString) {
    Rlog.d("SubscriptionManager", paramString);
  }
  
  private static void loge(String paramString) {
    Rlog.e("SubscriptionManager", paramString);
  }
  
  public static int getDefaultSubscriptionId() {
    return ((Integer)sDefaultSubIdCache.query(null)).intValue();
  }
  
  public static int getDefaultVoiceSubscriptionId() {
    byte b2, b1 = -1;
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      b2 = b1;
      if (iSub != null)
        b2 = iSub.getDefaultVoiceSubId(); 
    } catch (RemoteException remoteException) {
      b2 = b1;
    } 
    return b2;
  }
  
  @SystemApi
  public void setDefaultVoiceSubscriptionId(int paramInt) {
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null)
        iSub.setDefaultVoiceSubId(paramInt); 
    } catch (RemoteException remoteException) {}
  }
  
  public void setDefaultVoiceSubId(int paramInt) {
    setDefaultVoiceSubscriptionId(paramInt);
  }
  
  public SubscriptionInfo getDefaultVoiceSubscriptionInfo() {
    return getActiveSubscriptionInfo(getDefaultVoiceSubscriptionId());
  }
  
  public static int getDefaultVoicePhoneId() {
    return getPhoneId(getDefaultVoiceSubscriptionId());
  }
  
  public static int getDefaultSmsSubscriptionId() {
    return ((Integer)sDefaultSmsSubIdCache.query(null)).intValue();
  }
  
  @SystemApi
  public void setDefaultSmsSubId(int paramInt) {
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null)
        iSub.setDefaultSmsSubId(paramInt); 
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public SubscriptionInfo getDefaultSmsSubscriptionInfo() {
    return getActiveSubscriptionInfo(getDefaultSmsSubscriptionId());
  }
  
  public int getDefaultSmsPhoneId() {
    return getPhoneId(getDefaultSmsSubscriptionId());
  }
  
  public static int getDefaultDataSubscriptionId() {
    return ((Integer)sDefaultDataSubIdCache.query(null)).intValue();
  }
  
  @SystemApi
  public void setDefaultDataSubId(int paramInt) {
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null)
        iSub.setDefaultDataSubId(paramInt); 
    } catch (RemoteException remoteException) {}
  }
  
  public SubscriptionInfo getDefaultDataSubscriptionInfo() {
    return getActiveSubscriptionInfo(getDefaultDataSubscriptionId());
  }
  
  public int getDefaultDataPhoneId() {
    return getPhoneId(getDefaultDataSubscriptionId());
  }
  
  public void clearSubscriptionInfo() {
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null)
        iSub.clearSubInfo(); 
    } catch (RemoteException remoteException) {}
  }
  
  public boolean allDefaultsSelected() {
    if (!isValidSubscriptionId(getDefaultDataSubscriptionId()))
      return false; 
    if (!isValidSubscriptionId(getDefaultSmsSubscriptionId()))
      return false; 
    if (!isValidSubscriptionId(getDefaultVoiceSubscriptionId()))
      return false; 
    return true;
  }
  
  public static boolean isValidSubscriptionId(int paramInt) {
    boolean bool;
    if (paramInt > -1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isUsableSubscriptionId(int paramInt) {
    return isUsableSubIdValue(paramInt);
  }
  
  public static boolean isUsableSubIdValue(int paramInt) {
    boolean bool;
    if (paramInt >= 0 && paramInt <= 2147483646) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isValidSlotIndex(int paramInt) {
    boolean bool;
    if (paramInt >= 0 && paramInt < TelephonyManager.getDefault().getActiveModemCount()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isValidPhoneId(int paramInt) {
    boolean bool;
    if (paramInt >= 0 && paramInt < TelephonyManager.getDefault().getActiveModemCount()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static void putPhoneIdAndSubIdExtra(Intent paramIntent, int paramInt) {
    int[] arrayOfInt = getSubId(paramInt);
    if (arrayOfInt != null && arrayOfInt.length > 0) {
      putPhoneIdAndSubIdExtra(paramIntent, paramInt, arrayOfInt[0]);
    } else {
      logd("putPhoneIdAndSubIdExtra: no valid subs");
      paramIntent.putExtra("phone", paramInt);
      paramIntent.putExtra("android.telephony.extra.SLOT_INDEX", paramInt);
    } 
  }
  
  public static void putPhoneIdAndSubIdExtra(Intent paramIntent, int paramInt1, int paramInt2) {
    paramIntent.putExtra("android.telephony.extra.SLOT_INDEX", paramInt1);
    paramIntent.putExtra("phone", paramInt1);
    putSubscriptionIdExtra(paramIntent, paramInt2);
  }
  
  @SystemApi
  public int[] getActiveSubscriptionIdList() {
    return getActiveSubscriptionIdList(true);
  }
  
  @SystemApi
  public int[] getCompleteActiveSubscriptionIdList() {
    return getActiveSubscriptionIdList(false);
  }
  
  public int[] getActiveSubscriptionIdList(boolean paramBoolean) {
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null) {
        int[] arrayOfInt = iSub.getActiveSubIdList(paramBoolean);
        if (arrayOfInt != null)
          return arrayOfInt; 
      } 
    } catch (RemoteException remoteException) {}
    return new int[0];
  }
  
  public boolean isNetworkRoaming(int paramInt) {
    int i = getPhoneId(paramInt);
    if (i < 0)
      return false; 
    return TelephonyManager.getDefault().isNetworkRoaming(paramInt);
  }
  
  public static int getSimStateForSlotIndex(int paramInt) {
    boolean bool = false;
    int i = 0;
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null)
        i = iSub.getSimStateForSlotIndex(paramInt); 
    } catch (RemoteException remoteException) {
      i = bool;
    } 
    return i;
  }
  
  public static void setSubscriptionProperty(int paramInt, String paramString1, String paramString2) {
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null)
        iSub.setSubscriptionProperty(paramInt, paramString1, paramString2); 
    } catch (RemoteException remoteException) {}
  }
  
  private static String getSubscriptionProperty(int paramInt, String paramString, Context paramContext) {
    String str1 = null, str2 = null;
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null) {
        str2 = paramContext.getOpPackageName();
        String str = paramContext.getAttributionTag();
        str2 = iSub.getSubscriptionProperty(paramInt, paramString, str2, str);
      } 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getSubscriptionProperty(");
      stringBuilder.append(paramInt);
      stringBuilder.append(",");
      stringBuilder.append(paramString);
      stringBuilder.append(") RemoteException, return null!");
      loge(stringBuilder.toString());
      str2 = str1;
    } 
    return str2;
  }
  
  public static boolean getBooleanSubscriptionProperty(int paramInt, String paramString, boolean paramBoolean, Context paramContext) {
    paramString = getSubscriptionProperty(paramInt, paramString, paramContext);
    if (paramString != null)
      try {
        paramInt = Integer.parseInt(paramString);
        paramBoolean = true;
        if (paramInt != 1)
          paramBoolean = false; 
        return paramBoolean;
      } catch (NumberFormatException numberFormatException) {
        logd("getBooleanSubscriptionProperty NumberFormat exception");
      }  
    return paramBoolean;
  }
  
  public static int getIntegerSubscriptionProperty(int paramInt1, String paramString, int paramInt2, Context paramContext) {
    paramString = getSubscriptionProperty(paramInt1, paramString, paramContext);
    if (paramString != null)
      try {
        return Integer.parseInt(paramString);
      } catch (NumberFormatException numberFormatException) {
        logd("getIntegerSubscriptionProperty NumberFormat exception");
      }  
    return paramInt2;
  }
  
  public static long getLongSubscriptionProperty(int paramInt, String paramString, long paramLong, Context paramContext) {
    paramString = getSubscriptionProperty(paramInt, paramString, paramContext);
    if (paramString != null)
      try {
        return Long.parseLong(paramString);
      } catch (NumberFormatException numberFormatException) {
        logd("getLongSubscriptionProperty NumberFormat exception");
      }  
    return paramLong;
  }
  
  @SystemApi
  public static Resources getResourcesForSubId(Context paramContext, int paramInt) {
    return getResourcesForSubId(paramContext, paramInt, false);
  }
  
  public static Resources getResourcesForSubId(Context paramContext, int paramInt, boolean paramBoolean) {
    Pair<Context, Integer> pair1 = null;
    Pair<Context, Integer> pair2 = pair1;
    if (isValidSubscriptionId(paramInt)) {
      pair2 = pair1;
      if (!paramBoolean) {
        pair1 = Pair.create(paramContext, Integer.valueOf(paramInt));
        pair2 = pair1;
        if (sResourcesCache.containsKey(pair1))
          return sResourcesCache.get(pair1); 
      } 
    } 
    SubscriptionInfo subscriptionInfo = from(paramContext).getActiveSubscriptionInfo(paramInt);
    Configuration configuration = new Configuration();
    if (subscriptionInfo != null) {
      configuration.mcc = subscriptionInfo.getMcc();
      configuration.mnc = subscriptionInfo.getMnc();
      if (configuration.mnc == 0)
        configuration.mnc = 65535; 
    } 
    if (paramBoolean)
      configuration.setLocale(Locale.ROOT); 
    paramContext = paramContext.createConfigurationContext(configuration);
    Resources resources = paramContext.getResources();
    if (pair2 != null)
      sResourcesCache.put(pair2, resources); 
    return resources;
  }
  
  public boolean isActiveSubscriptionId(int paramInt) {
    return isActiveSubId(paramInt);
  }
  
  public boolean isActiveSubId(int paramInt) {
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null) {
        String str1 = this.mContext.getOpPackageName();
        Context context = this.mContext;
        String str2 = context.getAttributionTag();
        return iSub.isActiveSubId(paramInt, str1, str2);
      } 
    } catch (RemoteException remoteException) {}
    return false;
  }
  
  public List<SubscriptionPlan> getSubscriptionPlans(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial getNetworkPolicyManager : ()Landroid/net/NetworkPolicyManager;
    //   4: iload_1
    //   5: aload_0
    //   6: getfield mContext : Landroid/content/Context;
    //   9: invokevirtual getOpPackageName : ()Ljava/lang/String;
    //   12: invokevirtual getSubscriptionPlans : (ILjava/lang/String;)[Landroid/telephony/SubscriptionPlan;
    //   15: astore_2
    //   16: aload_2
    //   17: ifnonnull -> 27
    //   20: invokestatic emptyList : ()Ljava/util/List;
    //   23: astore_2
    //   24: goto -> 32
    //   27: aload_2
    //   28: invokestatic asList : ([Ljava/lang/Object;)Ljava/util/List;
    //   31: astore_2
    //   32: aload_2
    //   33: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2539	-> 0
    //   #2540	-> 0
    //   #2541	-> 16
    //   #2542	-> 20
    //   #2541	-> 32
  }
  
  public void setSubscriptionPlans(int paramInt, List<SubscriptionPlan> paramList) {
    NetworkPolicyManager networkPolicyManager = getNetworkPolicyManager();
    SubscriptionPlan[] arrayOfSubscriptionPlan = paramList.<SubscriptionPlan>toArray(new SubscriptionPlan[paramList.size()]);
    String str = this.mContext.getOpPackageName();
    networkPolicyManager.setSubscriptionPlans(paramInt, arrayOfSubscriptionPlan, str);
  }
  
  public void setSubscriptionOverrideUnmetered(int paramInt, boolean paramBoolean, long paramLong) {
    NetworkPolicyManager networkPolicyManager = getNetworkPolicyManager();
    Context context = this.mContext;
    String str = context.getOpPackageName();
    networkPolicyManager.setSubscriptionOverride(paramInt, 1, paramBoolean, paramLong, str);
  }
  
  public void setSubscriptionOverrideCongested(int paramInt, boolean paramBoolean, long paramLong) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    NetworkPolicyManager networkPolicyManager = getNetworkPolicyManager();
    Context context = this.mContext;
    String str = context.getOpPackageName();
    networkPolicyManager.setSubscriptionOverride(paramInt, 2, bool, paramLong, str);
  }
  
  public boolean canManageSubscription(SubscriptionInfo paramSubscriptionInfo) {
    return canManageSubscription(paramSubscriptionInfo, this.mContext.getPackageName());
  }
  
  @SystemApi
  public boolean canManageSubscription(SubscriptionInfo paramSubscriptionInfo, String paramString) {
    if (paramSubscriptionInfo == null || paramSubscriptionInfo.getAllAccessRules() == null || paramString == null)
      return false; 
    PackageManager packageManager = this.mContext.getPackageManager();
    try {
      PackageInfo packageInfo = packageManager.getPackageInfo(paramString, 134217728);
      for (UiccAccessRule uiccAccessRule : paramSubscriptionInfo.getAllAccessRules()) {
        if (uiccAccessRule.getCarrierPrivilegeStatus(packageInfo) == 1)
          return true; 
      } 
      return false;
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown package: ");
      stringBuilder.append(paramString);
      logd(stringBuilder.toString());
      return false;
    } 
  }
  
  @SystemApi
  public void setPreferredDataSubscriptionId(int paramInt, boolean paramBoolean, Executor paramExecutor, Consumer<Integer> paramConsumer) {
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub == null)
        return; 
      Object object = new Object();
      super(this, paramExecutor, paramConsumer);
      iSub.setPreferredDataSubscriptionId(paramInt, paramBoolean, (ISetOpportunisticDataCallback)object);
    } catch (RemoteException remoteException) {}
  }
  
  public int getPreferredDataSubscriptionId() {
    int j, i = Integer.MAX_VALUE;
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      j = i;
      if (iSub != null)
        j = iSub.getPreferredDataSubscriptionId(); 
    } catch (RemoteException remoteException) {
      j = i;
    } 
    return j;
  }
  
  public List<SubscriptionInfo> getOpportunisticSubscriptions() {
    String str1;
    ArrayList<SubscriptionInfo> arrayList;
    String str2;
    Context context = this.mContext;
    if (context != null) {
      str1 = context.getOpPackageName();
    } else {
      str1 = "<unknown>";
    } 
    context = this.mContext;
    if (context != null) {
      str2 = context.getAttributionTag();
    } else {
      str2 = null;
    } 
    RemoteException remoteException3 = null;
    context = null;
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null)
        List list = iSub.getOpportunisticSubscriptions(str1, str2); 
    } catch (RemoteException remoteException1) {
      remoteException1 = remoteException3;
    } 
    RemoteException remoteException2 = remoteException1;
    if (remoteException1 == null)
      arrayList = new ArrayList(); 
    return arrayList;
  }
  
  public void switchToSubscription(int paramInt, PendingIntent paramPendingIntent) {
    Preconditions.checkNotNull(paramPendingIntent, "callbackIntent cannot be null");
    EuiccManager euiccManager = new EuiccManager(this.mContext);
    euiccManager.switchToSubscription(paramInt, paramPendingIntent);
  }
  
  public boolean setOpportunistic(boolean paramBoolean, int paramInt) {
    paramInt = setSubscriptionPropertyHelper(paramInt, "setOpportunistic", new _$$Lambda$SubscriptionManager$vEkhQSELvddUPjZ7BQuik8uCACE(this, paramBoolean, paramInt));
    paramBoolean = true;
    if (paramInt != 1)
      paramBoolean = false; 
    return paramBoolean;
  }
  
  public ParcelUuid createSubscriptionGroup(List<Integer> paramList) {
    StringBuilder stringBuilder1;
    String str;
    Preconditions.checkNotNull(paramList, "can't create group for null subId list");
    Context context = this.mContext;
    if (context != null) {
      str = context.getOpPackageName();
    } else {
      str = "<unknown>";
    } 
    StringBuilder stringBuilder2 = null;
    ISub iSub = null;
    int[] arrayOfInt = paramList.stream().mapToInt((ToIntFunction)_$$Lambda$SubscriptionManager$XMBKvTqAZ_tfr6YV6zRf1verEog.INSTANCE).toArray();
    try {
      ISub iSub1 = TelephonyManager.getSubscriptionService();
      if (iSub1 != null) {
        ParcelUuid parcelUuid = iSub1.createSubscriptionGroup(arrayOfInt, str);
      } else if (isSystemProcess()) {
        iSub1 = iSub;
      } else {
        IllegalStateException illegalStateException = new IllegalStateException();
        this("telephony service is null.");
        throw illegalStateException;
      } 
    } catch (RemoteException remoteException) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("createSubscriptionGroup RemoteException ");
      stringBuilder1.append(remoteException);
      loge(stringBuilder1.toString());
      stringBuilder1 = stringBuilder2;
      if (!isSystemProcess()) {
        remoteException.rethrowAsRuntimeException();
        stringBuilder1 = stringBuilder2;
      } 
    } 
    return (ParcelUuid)stringBuilder1;
  }
  
  public void addSubscriptionsIntoGroup(List<Integer> paramList, ParcelUuid paramParcelUuid) {
    String str;
    Preconditions.checkNotNull(paramList, "subIdList can't be null.");
    Preconditions.checkNotNull(paramParcelUuid, "groupUuid can't be null.");
    Context context = this.mContext;
    if (context != null) {
      str = context.getOpPackageName();
    } else {
      str = "<unknown>";
    } 
    int[] arrayOfInt = paramList.stream().mapToInt((ToIntFunction)_$$Lambda$SubscriptionManager$1QOdrF5xwHkpZ5lglQiEDxcF7RA.INSTANCE).toArray();
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null) {
        iSub.addSubscriptionsIntoGroup(arrayOfInt, paramParcelUuid, str);
      } else if (!isSystemProcess()) {
        IllegalStateException illegalStateException = new IllegalStateException();
        this("telephony service is null.");
        throw illegalStateException;
      } 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("addSubscriptionsIntoGroup RemoteException ");
      stringBuilder.append(remoteException);
      loge(stringBuilder.toString());
      if (!isSystemProcess())
        remoteException.rethrowAsRuntimeException(); 
    } 
  }
  
  private boolean isSystemProcess() {
    boolean bool;
    if (Process.myUid() == 1000) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void removeSubscriptionsFromGroup(List<Integer> paramList, ParcelUuid paramParcelUuid) {
    String str;
    Preconditions.checkNotNull(paramList, "subIdList can't be null.");
    Preconditions.checkNotNull(paramParcelUuid, "groupUuid can't be null.");
    Context context = this.mContext;
    if (context != null) {
      str = context.getOpPackageName();
    } else {
      str = "<unknown>";
    } 
    int[] arrayOfInt = paramList.stream().mapToInt((ToIntFunction)_$$Lambda$SubscriptionManager$wql9r4zMYyVpqqNWW9Wt35fzC_w.INSTANCE).toArray();
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null) {
        iSub.removeSubscriptionsFromGroup(arrayOfInt, paramParcelUuid, str);
      } else if (!isSystemProcess()) {
        IllegalStateException illegalStateException = new IllegalStateException();
        this("telephony service is null.");
        throw illegalStateException;
      } 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removeSubscriptionsFromGroup RemoteException ");
      stringBuilder.append(remoteException);
      loge(stringBuilder.toString());
      if (!isSystemProcess())
        remoteException.rethrowAsRuntimeException(); 
    } 
  }
  
  public List<SubscriptionInfo> getSubscriptionsInGroup(ParcelUuid paramParcelUuid) {
    StringBuilder stringBuilder1;
    String str;
    Preconditions.checkNotNull(paramParcelUuid, "groupUuid can't be null");
    Context context1 = this.mContext;
    if (context1 != null) {
      str = context1.getOpPackageName();
    } else {
      str = "<unknown>";
    } 
    Context context2 = this.mContext;
    if (context2 != null) {
      String str1 = context2.getAttributionTag();
    } else {
      context2 = null;
    } 
    StringBuilder stringBuilder2 = null;
    ParcelUuid parcelUuid = null;
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null) {
        List list = iSub.getSubscriptionsInGroup(paramParcelUuid, str, (String)context2);
      } else if (isSystemProcess()) {
        paramParcelUuid = parcelUuid;
      } else {
        IllegalStateException illegalStateException = new IllegalStateException();
        this("telephony service is null.");
        throw illegalStateException;
      } 
    } catch (RemoteException remoteException) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("removeSubscriptionsFromGroup RemoteException ");
      stringBuilder1.append(remoteException);
      loge(stringBuilder1.toString());
      stringBuilder1 = stringBuilder2;
      if (!isSystemProcess()) {
        remoteException.rethrowAsRuntimeException();
        stringBuilder1 = stringBuilder2;
      } 
    } 
    return (List<SubscriptionInfo>)stringBuilder1;
  }
  
  public boolean isSubscriptionVisible(SubscriptionInfo paramSubscriptionInfo) {
    boolean bool = false;
    if (paramSubscriptionInfo == null)
      return false; 
    if (paramSubscriptionInfo.getGroupUuid() == null || !paramSubscriptionInfo.isOpportunistic())
      return true; 
    TelephonyManager telephonyManager = TelephonyManager.from(this.mContext);
    if (telephonyManager.hasCarrierPrivileges(paramSubscriptionInfo.getSubscriptionId()) || 
      canManageSubscription(paramSubscriptionInfo))
      bool = true; 
    return bool;
  }
  
  public List<SubscriptionInfo> getSelectableSubscriptionInfoList() {
    List<SubscriptionInfo> list = getAvailableSubscriptionInfoList();
    if (list == null)
      return null; 
    ArrayList<SubscriptionInfo> arrayList = new ArrayList();
    HashMap<Object, Object> hashMap = new HashMap<>();
    for (SubscriptionInfo subscriptionInfo : list) {
      if (!isSubscriptionVisible(subscriptionInfo))
        continue; 
      ParcelUuid parcelUuid = subscriptionInfo.getGroupUuid();
      if (parcelUuid == null) {
        arrayList.add(subscriptionInfo);
        continue;
      } 
      if (!hashMap.containsKey(parcelUuid) || ((
        (SubscriptionInfo)hashMap.get(parcelUuid)).getSimSlotIndex() == -1 && 
        subscriptionInfo.getSimSlotIndex() != -1)) {
        arrayList.remove(hashMap.get(parcelUuid));
        arrayList.add(subscriptionInfo);
        hashMap.put(parcelUuid, subscriptionInfo);
      } 
    } 
    return arrayList;
  }
  
  @SystemApi
  public boolean setSubscriptionEnabled(int paramInt, boolean paramBoolean) {
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null)
        return iSub.setSubscriptionEnabled(paramBoolean, paramInt); 
    } catch (RemoteException remoteException) {}
    return false;
  }
  
  @SystemApi
  public void setUiccApplicationsEnabled(int paramInt, boolean paramBoolean) {
    try {
      TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
      TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getSubscriptionServiceRegisterer();
      IBinder iBinder = serviceRegisterer.get();
      ISub iSub = ISub.Stub.asInterface(iBinder);
      if (iSub != null)
        iSub.setUiccApplicationsEnabled(paramBoolean, paramInt); 
    } catch (RemoteException remoteException) {}
  }
  
  @SystemApi
  public boolean canDisablePhysicalSubscription() {
    try {
      TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
      TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getSubscriptionServiceRegisterer();
      IBinder iBinder = serviceRegisterer.get();
      ISub iSub = ISub.Stub.asInterface(iBinder);
      if (iSub != null)
        return iSub.canDisablePhysicalSubscription(); 
    } catch (RemoteException remoteException) {}
    return false;
  }
  
  @SystemApi
  public boolean isSubscriptionEnabled(int paramInt) {
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null)
        return iSub.isSubscriptionEnabled(paramInt); 
    } catch (RemoteException remoteException) {}
    return false;
  }
  
  @SystemApi
  public int getEnabledSubscriptionId(int paramInt) {
    byte b2, b1 = -1;
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      b2 = b1;
      if (iSub != null)
        b2 = iSub.getEnabledSubscriptionId(paramInt); 
    } catch (RemoteException remoteException) {
      b2 = b1;
    } 
    return b2;
  }
  
  private int setSubscriptionPropertyHelper(int paramInt, String paramString, CallISubMethodHelper paramCallISubMethodHelper) {
    StringBuilder stringBuilder;
    if (!isValidSubscriptionId(paramInt)) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("[");
      stringBuilder.append(paramString);
      stringBuilder.append("]- fail");
      logd(stringBuilder.toString());
      return -1;
    } 
    boolean bool = false;
    paramInt = 0;
    try {
      ISub iSub = TelephonyManager.getSubscriptionService();
      if (iSub != null)
        paramInt = stringBuilder.callMethod(iSub); 
    } catch (RemoteException remoteException) {
      paramInt = bool;
    } 
    return paramInt;
  }
  
  public static int getActiveDataSubscriptionId() {
    return ((Integer)sActiveDataSubIdCache.query(null)).intValue();
  }
  
  public static void putSubscriptionIdExtra(Intent paramIntent, int paramInt) {
    paramIntent.putExtra("android.telephony.extra.SUBSCRIPTION_INDEX", paramInt);
    paramIntent.putExtra("subscription", paramInt);
  }
  
  public static void invalidateDefaultSubIdCaches() {
    PropertyInvalidatedCache.invalidateCache("cache_key.telephony.get_default_sub_id");
  }
  
  public static void invalidateDefaultDataSubIdCaches() {
    PropertyInvalidatedCache.invalidateCache("cache_key.telephony.get_default_data_sub_id");
  }
  
  public static void invalidateDefaultSmsSubIdCaches() {
    PropertyInvalidatedCache.invalidateCache("cache_key.telephony.get_default_sms_sub_id");
  }
  
  public static void invalidateActiveDataSubIdCaches() {
    PropertyInvalidatedCache.invalidateCache("cache_key.telephony.get_active_data_sub_id");
  }
  
  public static void invalidateSlotIndexCaches() {
    PropertyInvalidatedCache.invalidateCache("cache_key.telephony.get_slot_index");
  }
  
  public static void disableCaching() {
    sDefaultSubIdCache.disableLocal();
    sDefaultDataSubIdCache.disableLocal();
    sActiveDataSubIdCache.disableLocal();
    sDefaultSmsSubIdCache.disableLocal();
    sSlotIndexCache.disableLocal();
    sPhoneIdCache.disableLocal();
  }
  
  public static void clearCaches() {
    sDefaultSubIdCache.clear();
    sDefaultDataSubIdCache.clear();
    sActiveDataSubIdCache.clear();
    sDefaultSmsSubIdCache.clear();
    sSlotIndexCache.clear();
    sPhoneIdCache.clear();
  }
  
  private static interface CallISubMethodHelper {
    int callMethod(ISub param1ISub) throws RemoteException;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ProfileClass {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SimDisplayNameSource {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SubscriptionType {}
}
