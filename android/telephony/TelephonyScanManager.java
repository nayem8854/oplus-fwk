package android.telephony;

import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.TelephonyServiceManager;
import android.util.SparseArray;
import com.android.internal.telephony.ITelephony;
import com.android.telephony.Rlog;
import java.util.List;
import java.util.concurrent.Executor;

public final class TelephonyScanManager {
  public static final int CALLBACK_RESTRICTED_SCAN_RESULTS = 4;
  
  public static final int CALLBACK_SCAN_COMPLETE = 3;
  
  public static final int CALLBACK_SCAN_ERROR = 2;
  
  public static final int CALLBACK_SCAN_RESULTS = 1;
  
  public static final int CALLBACK_TELEPHONY_DIED = 5;
  
  public static final int INVALID_SCAN_ID = -1;
  
  public static final String SCAN_RESULT_KEY = "scanResult";
  
  private static final String TAG = "TelephonyScanManager";
  
  private final IBinder.DeathRecipient mDeathRecipient;
  
  private final Handler mHandler;
  
  private final Looper mLooper;
  
  private final Messenger mMessenger;
  
  public static abstract class NetworkScanCallback {
    public void onResults(List<CellInfo> param1List) {}
    
    public void onComplete() {}
    
    public void onError(int param1Int) {}
  }
  
  private static class NetworkScanInfo {
    private final TelephonyScanManager.NetworkScanCallback mCallback;
    
    private final Executor mExecutor;
    
    private final NetworkScanRequest mRequest;
    
    NetworkScanInfo(NetworkScanRequest param1NetworkScanRequest, Executor param1Executor, TelephonyScanManager.NetworkScanCallback param1NetworkScanCallback) {
      this.mRequest = param1NetworkScanRequest;
      this.mExecutor = param1Executor;
      this.mCallback = param1NetworkScanCallback;
    }
  }
  
  private final SparseArray<NetworkScanInfo> mScanInfo = new SparseArray<>();
  
  public TelephonyScanManager() {
    HandlerThread handlerThread = new HandlerThread("TelephonyScanManager");
    handlerThread.start();
    this.mLooper = handlerThread.getLooper();
    this.mHandler = (Handler)new Object(this, this.mLooper);
    this.mMessenger = new Messenger(this.mHandler);
    this.mDeathRecipient = (IBinder.DeathRecipient)new Object(this);
  }
  
  public NetworkScan requestNetworkScan(int paramInt, NetworkScanRequest paramNetworkScanRequest, Executor paramExecutor, NetworkScanCallback paramNetworkScanCallback, String paramString1, String paramString2) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return null; 
      Messenger messenger = this.mMessenger;
      Binder binder = new Binder();
      this();
      int i = iTelephony.requestNetworkScan(paramInt, paramNetworkScanRequest, messenger, (IBinder)binder, paramString1, paramString2);
      if (i == -1) {
        Rlog.e("TelephonyScanManager", "Failed to initiate network scan");
        return null;
      } 
      synchronized (this.mScanInfo) {
        iTelephony.asBinder().linkToDeath(this.mDeathRecipient, 0);
        saveScanInfo(i, paramNetworkScanRequest, paramExecutor, paramNetworkScanCallback);
        NetworkScan networkScan = new NetworkScan();
        this(i, paramInt);
        return networkScan;
      } 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyScanManager", "requestNetworkScan RemoteException", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Rlog.e("TelephonyScanManager", "requestNetworkScan NPE", nullPointerException);
    } 
    return null;
  }
  
  private void saveScanInfo(int paramInt, NetworkScanRequest paramNetworkScanRequest, Executor paramExecutor, NetworkScanCallback paramNetworkScanCallback) {
    this.mScanInfo.put(paramInt, new NetworkScanInfo(paramNetworkScanRequest, paramExecutor, paramNetworkScanCallback));
  }
  
  private ITelephony getITelephony() {
    TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
    TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getTelephonyServiceRegisterer();
    IBinder iBinder = serviceRegisterer.get();
    return ITelephony.Stub.asInterface(iBinder);
  }
}
