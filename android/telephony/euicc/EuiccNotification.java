package android.telephony.euicc;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;
import java.util.Objects;

@SystemApi
public final class EuiccNotification implements Parcelable {
  public static final int ALL_EVENTS = 15;
  
  public EuiccNotification(int paramInt1, String paramString, int paramInt2, byte[] paramArrayOfbyte) {
    this.mSeq = paramInt1;
    this.mTargetAddr = paramString;
    this.mEvent = paramInt2;
    this.mData = paramArrayOfbyte;
  }
  
  public int getSeq() {
    return this.mSeq;
  }
  
  public String getTargetAddr() {
    return this.mTargetAddr;
  }
  
  public int getEvent() {
    return this.mEvent;
  }
  
  public byte[] getData() {
    return this.mData;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mSeq == ((EuiccNotification)paramObject).mSeq) {
      String str1 = this.mTargetAddr, str2 = ((EuiccNotification)paramObject).mTargetAddr;
      if (Objects.equals(str1, str2) && this.mEvent == ((EuiccNotification)paramObject).mEvent) {
        byte[] arrayOfByte = this.mData;
        paramObject = ((EuiccNotification)paramObject).mData;
        if (Arrays.equals(arrayOfByte, (byte[])paramObject))
          return null; 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = this.mSeq;
    int j = Objects.hashCode(this.mTargetAddr);
    int k = this.mEvent;
    int m = Arrays.hashCode(this.mData);
    return (((1 * 31 + i) * 31 + j) * 31 + k) * 31 + m;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("EuiccNotification (seq=");
    stringBuilder.append(this.mSeq);
    stringBuilder.append(", targetAddr=");
    stringBuilder.append(this.mTargetAddr);
    stringBuilder.append(", event=");
    stringBuilder.append(this.mEvent);
    stringBuilder.append(", data=");
    if (this.mData == null) {
      null = "null";
    } else {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("byte[");
      stringBuilder1.append(this.mData.length);
      stringBuilder1.append("]");
      null = stringBuilder1.toString();
    } 
    stringBuilder.append(null);
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mSeq);
    paramParcel.writeString(this.mTargetAddr);
    paramParcel.writeInt(this.mEvent);
    paramParcel.writeByteArray(this.mData);
  }
  
  private EuiccNotification(Parcel paramParcel) {
    this.mSeq = paramParcel.readInt();
    this.mTargetAddr = paramParcel.readString();
    this.mEvent = paramParcel.readInt();
    this.mData = paramParcel.createByteArray();
  }
  
  public static final Parcelable.Creator<EuiccNotification> CREATOR = new Parcelable.Creator<EuiccNotification>() {
      public EuiccNotification createFromParcel(Parcel param1Parcel) {
        return new EuiccNotification(param1Parcel);
      }
      
      public EuiccNotification[] newArray(int param1Int) {
        return new EuiccNotification[param1Int];
      }
    };
  
  public static final int EVENT_DELETE = 8;
  
  public static final int EVENT_DISABLE = 4;
  
  public static final int EVENT_ENABLE = 2;
  
  public static final int EVENT_INSTALL = 1;
  
  private final byte[] mData;
  
  private final int mEvent;
  
  private final int mSeq;
  
  private final String mTargetAddr;
  
  @Retention(RetentionPolicy.SOURCE)
  class Event implements Annotation {}
}
