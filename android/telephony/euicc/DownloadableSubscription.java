package android.telephony.euicc;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.UiccAccessRule;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class DownloadableSubscription implements Parcelable {
  public static final Parcelable.Creator<DownloadableSubscription> CREATOR = new Parcelable.Creator<DownloadableSubscription>() {
      public DownloadableSubscription createFromParcel(Parcel param1Parcel) {
        return new DownloadableSubscription(param1Parcel);
      }
      
      public DownloadableSubscription[] newArray(int param1Int) {
        return new DownloadableSubscription[param1Int];
      }
    };
  
  private List<UiccAccessRule> accessRules;
  
  private String carrierName;
  
  private String confirmationCode;
  
  @Deprecated
  public final String encodedActivationCode;
  
  public String getEncodedActivationCode() {
    return this.encodedActivationCode;
  }
  
  private DownloadableSubscription(String paramString) {
    this.encodedActivationCode = paramString;
  }
  
  private DownloadableSubscription(Parcel paramParcel) {
    this.encodedActivationCode = paramParcel.readString();
    this.confirmationCode = paramParcel.readString();
    this.carrierName = paramParcel.readString();
    ArrayList<UiccAccessRule> arrayList = new ArrayList();
    paramParcel.readTypedList(arrayList, UiccAccessRule.CREATOR);
  }
  
  private DownloadableSubscription(String paramString1, String paramString2, String paramString3, List<UiccAccessRule> paramList) {
    this.encodedActivationCode = paramString1;
    this.confirmationCode = paramString2;
    this.carrierName = paramString3;
    this.accessRules = paramList;
  }
  
  @SystemApi
  class Builder {
    List<UiccAccessRule> accessRules;
    
    private String carrierName;
    
    private String confirmationCode;
    
    private String encodedActivationCode;
    
    public Builder() {}
    
    public Builder(DownloadableSubscription this$0) {
      this.encodedActivationCode = this$0.getEncodedActivationCode();
      this.confirmationCode = this$0.getConfirmationCode();
      this.carrierName = this$0.getCarrierName();
      this.accessRules = this$0.getAccessRules();
    }
    
    public DownloadableSubscription build() {
      return new DownloadableSubscription(this.encodedActivationCode, this.confirmationCode, this.carrierName, this.accessRules);
    }
    
    public Builder setEncodedActivationCode(String param1String) {
      this.encodedActivationCode = param1String;
      return this;
    }
    
    public Builder setConfirmationCode(String param1String) {
      this.confirmationCode = param1String;
      return this;
    }
    
    public Builder setCarrierName(String param1String) {
      this.carrierName = param1String;
      return this;
    }
    
    public Builder setAccessRules(List<UiccAccessRule> param1List) {
      this.accessRules = param1List;
      return this;
    }
  }
  
  public static DownloadableSubscription forActivationCode(String paramString) {
    Preconditions.checkNotNull(paramString, "Activation code may not be null");
    return new DownloadableSubscription(paramString);
  }
  
  @Deprecated
  public void setConfirmationCode(String paramString) {
    this.confirmationCode = paramString;
  }
  
  public String getConfirmationCode() {
    return this.confirmationCode;
  }
  
  @Deprecated
  public void setCarrierName(String paramString) {
    this.carrierName = paramString;
  }
  
  @SystemApi
  public String getCarrierName() {
    return this.carrierName;
  }
  
  @SystemApi
  public List<UiccAccessRule> getAccessRules() {
    return this.accessRules;
  }
  
  @Deprecated
  public void setAccessRules(List<UiccAccessRule> paramList) {
    this.accessRules = paramList;
  }
  
  @Deprecated
  public void setAccessRules(UiccAccessRule[] paramArrayOfUiccAccessRule) {
    this.accessRules = Arrays.asList(paramArrayOfUiccAccessRule);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.encodedActivationCode);
    paramParcel.writeString(this.confirmationCode);
    paramParcel.writeString(this.carrierName);
    paramParcel.writeTypedList(this.accessRules);
  }
  
  public int describeContents() {
    return 0;
  }
}
