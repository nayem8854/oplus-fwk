package android.telephony.euicc;

import android.annotation.SystemApi;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.TelephonyServiceManager;
import android.telephony.TelephonyFrameworkInitializer;
import android.telephony.TelephonyManager;
import com.android.internal.telephony.euicc.IEuiccController;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EuiccManager {
  @SystemApi
  public static final String ACTION_DELETE_SUBSCRIPTION_PRIVILEGED = "android.telephony.euicc.action.DELETE_SUBSCRIPTION_PRIVILEGED";
  
  public static final String ACTION_MANAGE_EMBEDDED_SUBSCRIPTIONS = "android.telephony.euicc.action.MANAGE_EMBEDDED_SUBSCRIPTIONS";
  
  public static final String ACTION_NOTIFY_CARRIER_SETUP_INCOMPLETE = "android.telephony.euicc.action.NOTIFY_CARRIER_SETUP_INCOMPLETE";
  
  @SystemApi
  public static final String ACTION_OTA_STATUS_CHANGED = "android.telephony.euicc.action.OTA_STATUS_CHANGED";
  
  @SystemApi
  public static final String ACTION_PROVISION_EMBEDDED_SUBSCRIPTION = "android.telephony.euicc.action.PROVISION_EMBEDDED_SUBSCRIPTION";
  
  @SystemApi
  public static final String ACTION_RENAME_SUBSCRIPTION_PRIVILEGED = "android.telephony.euicc.action.RENAME_SUBSCRIPTION_PRIVILEGED";
  
  public static final String ACTION_RESOLVE_ERROR = "android.telephony.euicc.action.RESOLVE_ERROR";
  
  public static final String ACTION_START_EUICC_ACTIVATION = "android.telephony.euicc.action.START_EUICC_ACTIVATION";
  
  @SystemApi
  public static final String ACTION_TOGGLE_SUBSCRIPTION_PRIVILEGED = "android.telephony.euicc.action.TOGGLE_SUBSCRIPTION_PRIVILEGED";
  
  public static final int EMBEDDED_SUBSCRIPTION_RESULT_ERROR = 2;
  
  public static final int EMBEDDED_SUBSCRIPTION_RESULT_OK = 0;
  
  public static final int EMBEDDED_SUBSCRIPTION_RESULT_RESOLVABLE_ERROR = 1;
  
  public static final int ERROR_ADDRESS_MISSING = 10011;
  
  public static final int ERROR_CARRIER_LOCKED = 10000;
  
  public static final int ERROR_CERTIFICATE_ERROR = 10012;
  
  public static final int ERROR_CONNECTION_ERROR = 10014;
  
  public static final int ERROR_DISALLOWED_BY_PPR = 10010;
  
  public static final int ERROR_EUICC_INSUFFICIENT_MEMORY = 10004;
  
  public static final int ERROR_EUICC_MISSING = 10006;
  
  public static final int ERROR_INCOMPATIBLE_CARRIER = 10003;
  
  public static final int ERROR_INSTALL_PROFILE = 10009;
  
  public static final int ERROR_INVALID_ACTIVATION_CODE = 10001;
  
  public static final int ERROR_INVALID_CONFIRMATION_CODE = 10002;
  
  public static final int ERROR_INVALID_RESPONSE = 10015;
  
  public static final int ERROR_NO_PROFILES_AVAILABLE = 10013;
  
  public static final int ERROR_OPERATION_BUSY = 10016;
  
  public static final int ERROR_SIM_MISSING = 10008;
  
  public static final int ERROR_TIME_OUT = 10005;
  
  public static final int ERROR_UNSUPPORTED_VERSION = 10007;
  
  @SystemApi
  public static final int EUICC_ACTIVATION_TYPE_ACCOUNT_REQUIRED = 4;
  
  @SystemApi
  public static final int EUICC_ACTIVATION_TYPE_BACKUP = 2;
  
  @SystemApi
  public static final int EUICC_ACTIVATION_TYPE_DEFAULT = 1;
  
  @SystemApi
  public static final int EUICC_ACTIVATION_TYPE_TRANSFER = 3;
  
  @SystemApi
  public static final int EUICC_OTA_FAILED = 2;
  
  @SystemApi
  public static final int EUICC_OTA_IN_PROGRESS = 1;
  
  @SystemApi
  public static final int EUICC_OTA_NOT_NEEDED = 4;
  
  @SystemApi
  public static final int EUICC_OTA_STATUS_UNAVAILABLE = 5;
  
  @SystemApi
  public static final int EUICC_OTA_SUCCEEDED = 3;
  
  @SystemApi
  public static final String EXTRA_ACTIVATION_TYPE = "android.telephony.euicc.extra.ACTIVATION_TYPE";
  
  public static final String EXTRA_EMBEDDED_SUBSCRIPTION_DETAILED_CODE = "android.telephony.euicc.extra.EMBEDDED_SUBSCRIPTION_DETAILED_CODE";
  
  public static final String EXTRA_EMBEDDED_SUBSCRIPTION_DOWNLOADABLE_SUBSCRIPTION = "android.telephony.euicc.extra.EMBEDDED_SUBSCRIPTION_DOWNLOADABLE_SUBSCRIPTION";
  
  @SystemApi
  public static final String EXTRA_EMBEDDED_SUBSCRIPTION_DOWNLOADABLE_SUBSCRIPTIONS = "android.telephony.euicc.extra.EMBEDDED_SUBSCRIPTION_DOWNLOADABLE_SUBSCRIPTIONS";
  
  public static final String EXTRA_EMBEDDED_SUBSCRIPTION_ERROR_CODE = "android.telephony.euicc.extra.EMBEDDED_SUBSCRIPTION_ERROR_CODE";
  
  public static final String EXTRA_EMBEDDED_SUBSCRIPTION_OPERATION_CODE = "android.telephony.euicc.extra.EMBEDDED_SUBSCRIPTION_OPERATION_CODE";
  
  public static final String EXTRA_EMBEDDED_SUBSCRIPTION_RESOLUTION_ACTION = "android.telephony.euicc.extra.EMBEDDED_SUBSCRIPTION_RESOLUTION_ACTION";
  
  public static final String EXTRA_EMBEDDED_SUBSCRIPTION_RESOLUTION_CALLBACK_INTENT = "android.telephony.euicc.extra.EMBEDDED_SUBSCRIPTION_RESOLUTION_CALLBACK_INTENT";
  
  public static final String EXTRA_EMBEDDED_SUBSCRIPTION_RESOLUTION_INTENT = "android.telephony.euicc.extra.EMBEDDED_SUBSCRIPTION_RESOLUTION_INTENT";
  
  public static final String EXTRA_EMBEDDED_SUBSCRIPTION_SMDX_REASON_CODE = "android.telephony.euicc.extra.EMBEDDED_SUBSCRIPTION_SMDX_REASON_CODE";
  
  public static final String EXTRA_EMBEDDED_SUBSCRIPTION_SMDX_SUBJECT_CODE = "android.telephony.euicc.extra.EMBEDDED_SUBSCRIPTION_SMDX_SUBJECT_CODE";
  
  @SystemApi
  public static final String EXTRA_ENABLE_SUBSCRIPTION = "android.telephony.euicc.extra.ENABLE_SUBSCRIPTION";
  
  @SystemApi
  public static final String EXTRA_FORCE_PROVISION = "android.telephony.euicc.extra.FORCE_PROVISION";
  
  @SystemApi
  public static final String EXTRA_FROM_SUBSCRIPTION_ID = "android.telephony.euicc.extra.FROM_SUBSCRIPTION_ID";
  
  @SystemApi
  public static final String EXTRA_PHYSICAL_SLOT_ID = "android.telephony.euicc.extra.PHYSICAL_SLOT_ID";
  
  @SystemApi
  public static final String EXTRA_SUBSCRIPTION_ID = "android.telephony.euicc.extra.SUBSCRIPTION_ID";
  
  @SystemApi
  public static final String EXTRA_SUBSCRIPTION_NICKNAME = "android.telephony.euicc.extra.SUBSCRIPTION_NICKNAME";
  
  public static final String EXTRA_USE_QR_SCANNER = "android.telephony.euicc.extra.USE_QR_SCANNER";
  
  public static final String META_DATA_CARRIER_ICON = "android.telephony.euicc.carriericon";
  
  public static final int OPERATION_APDU = 8;
  
  public static final int OPERATION_DOWNLOAD = 5;
  
  public static final int OPERATION_EUICC_CARD = 3;
  
  public static final int OPERATION_EUICC_GSMA = 7;
  
  public static final int OPERATION_HTTP = 11;
  
  public static final int OPERATION_METADATA = 6;
  
  public static final int OPERATION_SIM_SLOT = 2;
  
  public static final int OPERATION_SMDX = 9;
  
  public static final int OPERATION_SMDX_SUBJECT_REASON_CODE = 10;
  
  public static final int OPERATION_SWITCH = 4;
  
  public static final int OPERATION_SYSTEM = 1;
  
  private int mCardId;
  
  private final Context mContext;
  
  public EuiccManager(Context paramContext) {
    this.mContext = paramContext;
    TelephonyManager telephonyManager = (TelephonyManager)paramContext.getSystemService("phone");
    this.mCardId = telephonyManager.getCardIdForDefaultEuicc();
  }
  
  private EuiccManager(Context paramContext, int paramInt) {
    this.mContext = paramContext;
    this.mCardId = paramInt;
  }
  
  public EuiccManager createForCardId(int paramInt) {
    return new EuiccManager(this.mContext, paramInt);
  }
  
  public boolean isEnabled() {
    boolean bool;
    if (getIEuiccController() != null && refreshCardIdIfUninitialized()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String getEid() {
    if (!isEnabled())
      return null; 
    try {
      return getIEuiccController().getEid(this.mCardId, this.mContext.getOpPackageName());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public int getOtaStatus() {
    if (!isEnabled())
      return 5; 
    try {
      return getIEuiccController().getOtaStatus(this.mCardId);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void downloadSubscription(DownloadableSubscription paramDownloadableSubscription, boolean paramBoolean, PendingIntent paramPendingIntent) {
    if (!isEnabled()) {
      sendUnavailableError(paramPendingIntent);
      return;
    } 
    try {
      IEuiccController iEuiccController = getIEuiccController();
      int i = this.mCardId;
      Context context = this.mContext;
      String str = context.getOpPackageName();
      iEuiccController.downloadSubscription(i, paramDownloadableSubscription, paramBoolean, str, null, paramPendingIntent);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void startResolutionActivity(Activity paramActivity, int paramInt, Intent paramIntent, PendingIntent paramPendingIntent) throws IntentSender.SendIntentException {
    PendingIntent pendingIntent = (PendingIntent)paramIntent.getParcelableExtra("android.telephony.euicc.extra.EMBEDDED_SUBSCRIPTION_RESOLUTION_INTENT");
    if (pendingIntent != null) {
      Intent intent = new Intent();
      intent.putExtra("android.telephony.euicc.extra.EMBEDDED_SUBSCRIPTION_RESOLUTION_CALLBACK_INTENT", (Parcelable)paramPendingIntent);
      paramActivity.startIntentSenderForResult(pendingIntent.getIntentSender(), paramInt, intent, 0, 0, 0);
      return;
    } 
    throw new IllegalArgumentException("Invalid result intent");
  }
  
  @SystemApi
  public void continueOperation(Intent paramIntent, Bundle paramBundle) {
    PendingIntent pendingIntent;
    if (!isEnabled()) {
      pendingIntent = (PendingIntent)paramIntent.getParcelableExtra("android.telephony.euicc.extra.EMBEDDED_SUBSCRIPTION_RESOLUTION_CALLBACK_INTENT");
      if (pendingIntent != null)
        sendUnavailableError(pendingIntent); 
      return;
    } 
    try {
      getIEuiccController().continueOperation(this.mCardId, (Intent)pendingIntent, paramBundle);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void getDownloadableSubscriptionMetadata(DownloadableSubscription paramDownloadableSubscription, PendingIntent paramPendingIntent) {
    if (!isEnabled()) {
      sendUnavailableError(paramPendingIntent);
      return;
    } 
    try {
      IEuiccController iEuiccController = getIEuiccController();
      int i = this.mCardId;
      Context context = this.mContext;
      String str = context.getOpPackageName();
      iEuiccController.getDownloadableSubscriptionMetadata(i, paramDownloadableSubscription, str, paramPendingIntent);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void getDefaultDownloadableSubscriptionList(PendingIntent paramPendingIntent) {
    if (!isEnabled()) {
      sendUnavailableError(paramPendingIntent);
      return;
    } 
    try {
      IEuiccController iEuiccController = getIEuiccController();
      int i = this.mCardId;
      Context context = this.mContext;
      String str = context.getOpPackageName();
      iEuiccController.getDefaultDownloadableSubscriptionList(i, str, paramPendingIntent);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public EuiccInfo getEuiccInfo() {
    if (!isEnabled())
      return null; 
    try {
      return getIEuiccController().getEuiccInfo(this.mCardId);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void deleteSubscription(int paramInt, PendingIntent paramPendingIntent) {
    if (!isEnabled()) {
      sendUnavailableError(paramPendingIntent);
      return;
    } 
    try {
      IEuiccController iEuiccController = getIEuiccController();
      int i = this.mCardId;
      Context context = this.mContext;
      String str = context.getOpPackageName();
      iEuiccController.deleteSubscription(i, paramInt, str, paramPendingIntent);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void switchToSubscription(int paramInt, PendingIntent paramPendingIntent) {
    if (!isEnabled()) {
      sendUnavailableError(paramPendingIntent);
      return;
    } 
    try {
      IEuiccController iEuiccController = getIEuiccController();
      int i = this.mCardId;
      Context context = this.mContext;
      String str = context.getOpPackageName();
      iEuiccController.switchToSubscription(i, paramInt, str, paramPendingIntent);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void updateSubscriptionNickname(int paramInt, String paramString, PendingIntent paramPendingIntent) {
    if (!isEnabled()) {
      sendUnavailableError(paramPendingIntent);
      return;
    } 
    try {
      IEuiccController iEuiccController = getIEuiccController();
      int i = this.mCardId;
      Context context = this.mContext;
      String str = context.getOpPackageName();
      iEuiccController.updateSubscriptionNickname(i, paramInt, paramString, str, paramPendingIntent);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  @Deprecated
  public void eraseSubscriptions(PendingIntent paramPendingIntent) {
    if (!isEnabled()) {
      sendUnavailableError(paramPendingIntent);
      return;
    } 
    try {
      getIEuiccController().eraseSubscriptions(this.mCardId, paramPendingIntent);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void eraseSubscriptions(int paramInt, PendingIntent paramPendingIntent) {
    if (!isEnabled()) {
      sendUnavailableError(paramPendingIntent);
      return;
    } 
    try {
      getIEuiccController().eraseSubscriptionsWithOptions(this.mCardId, paramInt, paramPendingIntent);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void retainSubscriptionsForFactoryReset(PendingIntent paramPendingIntent) {
    if (!isEnabled()) {
      sendUnavailableError(paramPendingIntent);
      return;
    } 
    try {
      getIEuiccController().retainSubscriptionsForFactoryReset(this.mCardId, paramPendingIntent);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void setSupportedCountries(List<String> paramList) {
    if (!isEnabled())
      return; 
    try {
      IEuiccController iEuiccController = getIEuiccController();
      Stream<String> stream = paramList.stream();
      -$.Lambda.QszM0TfuLNTNlqlb2YFU7MVLozs qszM0TfuLNTNlqlb2YFU7MVLozs = _$$Lambda$QszM0TfuLNTNlqlb2YFU7MVLozs.INSTANCE;
      List list = (List)stream.map((Function<? super String, ?>)qszM0TfuLNTNlqlb2YFU7MVLozs).collect(Collectors.toList());
      iEuiccController.setSupportedCountries(true, list);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void setUnsupportedCountries(List<String> paramList) {
    if (!isEnabled())
      return; 
    try {
      IEuiccController iEuiccController = getIEuiccController();
      Stream<String> stream = paramList.stream();
      -$.Lambda.QszM0TfuLNTNlqlb2YFU7MVLozs qszM0TfuLNTNlqlb2YFU7MVLozs = _$$Lambda$QszM0TfuLNTNlqlb2YFU7MVLozs.INSTANCE;
      List list = (List)stream.map((Function<? super String, ?>)qszM0TfuLNTNlqlb2YFU7MVLozs).collect(Collectors.toList());
      iEuiccController.setSupportedCountries(false, list);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public List<String> getSupportedCountries() {
    if (!isEnabled())
      return Collections.emptyList(); 
    try {
      return getIEuiccController().getSupportedCountries(true);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public List<String> getUnsupportedCountries() {
    if (!isEnabled())
      return Collections.emptyList(); 
    try {
      return getIEuiccController().getSupportedCountries(false);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean isSupportedCountry(String paramString) {
    if (!isEnabled())
      return false; 
    try {
      return getIEuiccController().isSupportedCountry(paramString.toUpperCase());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private boolean refreshCardIdIfUninitialized() {
    if (this.mCardId == -2) {
      Context context = this.mContext;
      TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService("phone");
      this.mCardId = telephonyManager.getCardIdForDefaultEuicc();
    } 
    if (this.mCardId == -2)
      return false; 
    return true;
  }
  
  private static void sendUnavailableError(PendingIntent paramPendingIntent) {
    try {
      paramPendingIntent.send(2);
    } catch (android.app.PendingIntent.CanceledException canceledException) {}
  }
  
  private static IEuiccController getIEuiccController() {
    TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
    TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getEuiccControllerService();
    IBinder iBinder = serviceRegisterer.get();
    return IEuiccController.Stub.asInterface(iBinder);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ErrorCode {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface EuiccActivationType {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface OperationCode {}
  
  @SystemApi
  @Retention(RetentionPolicy.SOURCE)
  public static @interface OtaStatus {}
}
