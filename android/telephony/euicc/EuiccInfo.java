package android.telephony.euicc;

import android.os.Parcel;
import android.os.Parcelable;

public final class EuiccInfo implements Parcelable {
  public static final Parcelable.Creator<EuiccInfo> CREATOR = new Parcelable.Creator<EuiccInfo>() {
      public EuiccInfo createFromParcel(Parcel param1Parcel) {
        return new EuiccInfo(param1Parcel);
      }
      
      public EuiccInfo[] newArray(int param1Int) {
        return new EuiccInfo[param1Int];
      }
    };
  
  private final String osVersion;
  
  public String getOsVersion() {
    return this.osVersion;
  }
  
  public EuiccInfo(String paramString) {
    this.osVersion = paramString;
  }
  
  private EuiccInfo(Parcel paramParcel) {
    this.osVersion = paramParcel.readString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.osVersion);
  }
  
  public int describeContents() {
    return 0;
  }
}
