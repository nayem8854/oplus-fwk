package android.telephony.euicc;

import android.annotation.SystemApi;
import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.TelephonyServiceManager;
import android.service.euicc.EuiccProfileInfo;
import android.telephony.TelephonyFrameworkInitializer;
import android.util.Log;
import com.android.internal.telephony.euicc.IAuthenticateServerCallback;
import com.android.internal.telephony.euicc.ICancelSessionCallback;
import com.android.internal.telephony.euicc.IDeleteProfileCallback;
import com.android.internal.telephony.euicc.IDisableProfileCallback;
import com.android.internal.telephony.euicc.IEuiccCardController;
import com.android.internal.telephony.euicc.IGetAllProfilesCallback;
import com.android.internal.telephony.euicc.IGetDefaultSmdpAddressCallback;
import com.android.internal.telephony.euicc.IGetEuiccChallengeCallback;
import com.android.internal.telephony.euicc.IGetEuiccInfo1Callback;
import com.android.internal.telephony.euicc.IGetEuiccInfo2Callback;
import com.android.internal.telephony.euicc.IGetProfileCallback;
import com.android.internal.telephony.euicc.IGetRulesAuthTableCallback;
import com.android.internal.telephony.euicc.IGetSmdsAddressCallback;
import com.android.internal.telephony.euicc.IListNotificationsCallback;
import com.android.internal.telephony.euicc.ILoadBoundProfilePackageCallback;
import com.android.internal.telephony.euicc.IPrepareDownloadCallback;
import com.android.internal.telephony.euicc.IRemoveNotificationFromListCallback;
import com.android.internal.telephony.euicc.IResetMemoryCallback;
import com.android.internal.telephony.euicc.IRetrieveNotificationCallback;
import com.android.internal.telephony.euicc.IRetrieveNotificationListCallback;
import com.android.internal.telephony.euicc.ISetDefaultSmdpAddressCallback;
import com.android.internal.telephony.euicc.ISetNicknameCallback;
import com.android.internal.telephony.euicc.ISwitchToProfileCallback;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.Executor;

@SystemApi
public class EuiccCardManager {
  public static final int CANCEL_REASON_END_USER_REJECTED = 0;
  
  public static final int CANCEL_REASON_POSTPONED = 1;
  
  public static final int CANCEL_REASON_PPR_NOT_ALLOWED = 3;
  
  public static final int CANCEL_REASON_TIMEOUT = 2;
  
  public static final int RESET_OPTION_DELETE_FIELD_LOADED_TEST_PROFILES = 2;
  
  public static final int RESET_OPTION_DELETE_OPERATIONAL_PROFILES = 1;
  
  public static final int RESET_OPTION_RESET_DEFAULT_SMDP_ADDRESS = 4;
  
  public static final int RESULT_CALLER_NOT_ALLOWED = -3;
  
  public static final int RESULT_EUICC_NOT_FOUND = -2;
  
  public static final int RESULT_OK = 0;
  
  public static final int RESULT_UNKNOWN_ERROR = -1;
  
  private static final String TAG = "EuiccCardManager";
  
  private final Context mContext;
  
  public EuiccCardManager(Context paramContext) {
    this.mContext = paramContext;
  }
  
  private IEuiccCardController getIEuiccCardController() {
    TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
    TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getEuiccCardControllerServiceRegisterer();
    IBinder iBinder = serviceRegisterer.get();
    return IEuiccCardController.Stub.asInterface(iBinder);
  }
  
  public void requestAllProfiles(String paramString, Executor paramExecutor, ResultCallback<EuiccProfileInfo[]> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      String str = this.mContext.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.getAllProfiles(str, paramString, (IGetAllProfilesCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling getAllProfiles", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void requestProfile(String paramString1, String paramString2, Executor paramExecutor, ResultCallback<EuiccProfileInfo> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      String str = this.mContext.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.getProfile(str, paramString1, paramString2, (IGetProfileCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling getProfile", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void disableProfile(String paramString1, String paramString2, boolean paramBoolean, Executor paramExecutor, ResultCallback<Void> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      String str = this.mContext.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.disableProfile(str, paramString1, paramString2, paramBoolean, (IDisableProfileCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling disableProfile", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void switchToProfile(String paramString1, String paramString2, boolean paramBoolean, Executor paramExecutor, ResultCallback<EuiccProfileInfo> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      String str = this.mContext.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.switchToProfile(str, paramString1, paramString2, paramBoolean, (ISwitchToProfileCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling switchToProfile", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setNickname(String paramString1, String paramString2, String paramString3, Executor paramExecutor, ResultCallback<Void> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      String str = this.mContext.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.setNickname(str, paramString1, paramString2, paramString3, (ISetNicknameCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling setNickname", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void deleteProfile(String paramString1, String paramString2, Executor paramExecutor, ResultCallback<Void> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      String str = this.mContext.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.deleteProfile(str, paramString1, paramString2, (IDeleteProfileCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling deleteProfile", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void resetMemory(String paramString, int paramInt, Executor paramExecutor, ResultCallback<Void> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      String str = this.mContext.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.resetMemory(str, paramString, paramInt, (IResetMemoryCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling resetMemory", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void requestDefaultSmdpAddress(String paramString, Executor paramExecutor, ResultCallback<String> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      String str = this.mContext.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.getDefaultSmdpAddress(str, paramString, (IGetDefaultSmdpAddressCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling getDefaultSmdpAddress", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void requestSmdsAddress(String paramString, Executor paramExecutor, ResultCallback<String> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      String str = this.mContext.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.getSmdsAddress(str, paramString, (IGetSmdsAddressCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling getSmdsAddress", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setDefaultSmdpAddress(String paramString1, String paramString2, Executor paramExecutor, ResultCallback<Void> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      String str = this.mContext.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.setDefaultSmdpAddress(str, paramString1, paramString2, (ISetDefaultSmdpAddressCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling setDefaultSmdpAddress", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void requestRulesAuthTable(String paramString, Executor paramExecutor, ResultCallback<EuiccRulesAuthTable> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      String str = this.mContext.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.getRulesAuthTable(str, paramString, (IGetRulesAuthTableCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling getRulesAuthTable", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void requestEuiccChallenge(String paramString, Executor paramExecutor, ResultCallback<byte[]> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      String str = this.mContext.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.getEuiccChallenge(str, paramString, (IGetEuiccChallengeCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling getEuiccChallenge", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void requestEuiccInfo1(String paramString, Executor paramExecutor, ResultCallback<byte[]> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      String str = this.mContext.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.getEuiccInfo1(str, paramString, (IGetEuiccInfo1Callback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling getEuiccInfo1", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void requestEuiccInfo2(String paramString, Executor paramExecutor, ResultCallback<byte[]> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      String str = this.mContext.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.getEuiccInfo2(str, paramString, (IGetEuiccInfo2Callback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling getEuiccInfo2", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void authenticateServer(String paramString1, String paramString2, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3, byte[] paramArrayOfbyte4, Executor paramExecutor, ResultCallback<byte[]> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      Context context = this.mContext;
      String str = context.getOpPackageName();
      Object object = new Object();
      try {
        super(this, paramExecutor, paramResultCallback);
        iEuiccCardController.authenticateServer(str, paramString1, paramString2, paramArrayOfbyte1, paramArrayOfbyte2, paramArrayOfbyte3, paramArrayOfbyte4, (IAuthenticateServerCallback)object);
        return;
      } catch (RemoteException null) {}
    } catch (RemoteException remoteException) {}
    Log.e("EuiccCardManager", "Error calling authenticateServer", (Throwable)remoteException);
    throw remoteException.rethrowFromSystemServer();
  }
  
  public void prepareDownload(String paramString, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3, byte[] paramArrayOfbyte4, Executor paramExecutor, ResultCallback<byte[]> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      Context context = this.mContext;
      String str = context.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.prepareDownload(str, paramString, paramArrayOfbyte1, paramArrayOfbyte2, paramArrayOfbyte3, paramArrayOfbyte4, (IPrepareDownloadCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling prepareDownload", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void loadBoundProfilePackage(String paramString, byte[] paramArrayOfbyte, Executor paramExecutor, ResultCallback<byte[]> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      Context context = this.mContext;
      String str = context.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.loadBoundProfilePackage(str, paramString, paramArrayOfbyte, (ILoadBoundProfilePackageCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling loadBoundProfilePackage", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void cancelSession(String paramString, byte[] paramArrayOfbyte, int paramInt, Executor paramExecutor, ResultCallback<byte[]> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      Context context = this.mContext;
      String str = context.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.cancelSession(str, paramString, paramArrayOfbyte, paramInt, (ICancelSessionCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling cancelSession", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void listNotifications(String paramString, int paramInt, Executor paramExecutor, ResultCallback<EuiccNotification[]> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      String str = this.mContext.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.listNotifications(str, paramString, paramInt, (IListNotificationsCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling listNotifications", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void retrieveNotificationList(String paramString, int paramInt, Executor paramExecutor, ResultCallback<EuiccNotification[]> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      String str = this.mContext.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.retrieveNotificationList(str, paramString, paramInt, (IRetrieveNotificationListCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling retrieveNotificationList", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void retrieveNotification(String paramString, int paramInt, Executor paramExecutor, ResultCallback<EuiccNotification> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      String str = this.mContext.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.retrieveNotification(str, paramString, paramInt, (IRetrieveNotificationCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling retrieveNotification", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void removeNotificationFromList(String paramString, int paramInt, Executor paramExecutor, ResultCallback<Void> paramResultCallback) {
    try {
      IEuiccCardController iEuiccCardController = getIEuiccCardController();
      Context context = this.mContext;
      String str = context.getOpPackageName();
      Object object = new Object();
      super(this, paramExecutor, paramResultCallback);
      iEuiccCardController.removeNotificationFromList(str, paramString, paramInt, (IRemoveNotificationFromListCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("EuiccCardManager", "Error calling removeNotificationFromList", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface CancelReason {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ResetOption {}
  
  public static interface ResultCallback<T> {
    void onComplete(int param1Int, T param1T);
  }
}
