package android.telephony.euicc;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.service.carrier.CarrierIdentifier;
import android.text.TextUtils;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;
import java.util.List;

@SystemApi
public final class EuiccRulesAuthTable implements Parcelable {
  @Retention(RetentionPolicy.SOURCE)
  class PolicyRuleFlag implements Annotation {}
  
  class Builder {
    private CarrierIdentifier[][] mCarrierIds;
    
    private int[] mPolicyRuleFlags;
    
    private int[] mPolicyRules;
    
    private int mPosition;
    
    public Builder(EuiccRulesAuthTable this$0) {
      this.mPolicyRules = new int[this$0];
      this.mCarrierIds = new CarrierIdentifier[this$0][];
      this.mPolicyRuleFlags = new int[this$0];
    }
    
    public EuiccRulesAuthTable build() {
      if (this.mPosition == this.mPolicyRules.length)
        return new EuiccRulesAuthTable(this.mPolicyRules, this.mCarrierIds, this.mPolicyRuleFlags); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Not enough rules are added, expected: ");
      stringBuilder.append(this.mPolicyRules.length);
      stringBuilder.append(", added: ");
      stringBuilder.append(this.mPosition);
      throw new IllegalStateException(stringBuilder.toString());
    }
    
    public Builder add(int param1Int1, List<CarrierIdentifier> param1List, int param1Int2) {
      int i = this.mPosition, arrayOfInt[] = this.mPolicyRules;
      if (i < arrayOfInt.length) {
        arrayOfInt[i] = param1Int1;
        if (param1List != null && param1List.size() > 0)
          this.mCarrierIds[this.mPosition] = param1List.<CarrierIdentifier>toArray(new CarrierIdentifier[param1List.size()]); 
        int[] arrayOfInt1 = this.mPolicyRuleFlags;
        param1Int1 = this.mPosition;
        arrayOfInt1[param1Int1] = param1Int2;
        this.mPosition = param1Int1 + 1;
        return this;
      } 
      throw new ArrayIndexOutOfBoundsException(this.mPosition);
    }
  }
  
  public static boolean match(String paramString1, String paramString2) {
    if (paramString1.length() < paramString2.length())
      return false; 
    for (byte b = 0; b < paramString1.length(); ) {
      if (paramString1.charAt(b) == 'E' || (
        b < paramString2.length() && paramString1.charAt(b) == paramString2.charAt(b))) {
        b++;
        continue;
      } 
      return false;
    } 
    return true;
  }
  
  private EuiccRulesAuthTable(int[] paramArrayOfint1, CarrierIdentifier[][] paramArrayOfCarrierIdentifier, int[] paramArrayOfint2) {
    this.mPolicyRules = paramArrayOfint1;
    this.mCarrierIds = paramArrayOfCarrierIdentifier;
    this.mPolicyRuleFlags = paramArrayOfint2;
  }
  
  public int findIndex(int paramInt, CarrierIdentifier paramCarrierIdentifier) {
    byte b = 0;
    while (true) {
      int[] arrayOfInt = this.mPolicyRules;
      if (b < arrayOfInt.length) {
        if ((arrayOfInt[b] & paramInt) != 0) {
          CarrierIdentifier[] arrayOfCarrierIdentifier = this.mCarrierIds[b];
          if (arrayOfCarrierIdentifier != null && arrayOfCarrierIdentifier.length != 0)
            for (byte b1 = 0; b1 < arrayOfCarrierIdentifier.length; b1++) {
              CarrierIdentifier carrierIdentifier = arrayOfCarrierIdentifier[b1];
              if (match(carrierIdentifier.getMcc(), paramCarrierIdentifier.getMcc()) && 
                match(carrierIdentifier.getMnc(), paramCarrierIdentifier.getMnc())) {
                String str = carrierIdentifier.getGid1();
                if (TextUtils.isEmpty(str) || str.equals(paramCarrierIdentifier.getGid1())) {
                  String str1 = carrierIdentifier.getGid2();
                  if (TextUtils.isEmpty(str1) || str1.equals(paramCarrierIdentifier.getGid2()))
                    return b; 
                } 
              } 
            }  
        } 
        b++;
        continue;
      } 
      break;
    } 
    return -1;
  }
  
  public boolean hasPolicyRuleFlag(int paramInt1, int paramInt2) {
    if (paramInt1 >= 0 && paramInt1 < this.mPolicyRules.length) {
      boolean bool;
      if ((this.mPolicyRuleFlags[paramInt1] & paramInt2) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
    throw new ArrayIndexOutOfBoundsException(paramInt1);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeIntArray(this.mPolicyRules);
    for (CarrierIdentifier[] arrayOfCarrierIdentifier : this.mCarrierIds)
      paramParcel.writeTypedArray((Parcelable[])arrayOfCarrierIdentifier, paramInt); 
    paramParcel.writeIntArray(this.mPolicyRuleFlags);
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mCarrierIds.length != ((EuiccRulesAuthTable)paramObject).mCarrierIds.length)
      return false; 
    byte b = 0;
    while (true) {
      CarrierIdentifier[][] arrayOfCarrierIdentifier = this.mCarrierIds;
      if (b < arrayOfCarrierIdentifier.length) {
        CarrierIdentifier[] arrayOfCarrierIdentifier1 = arrayOfCarrierIdentifier[b];
        CarrierIdentifier[] arrayOfCarrierIdentifier2 = ((EuiccRulesAuthTable)paramObject).mCarrierIds[b];
        if (arrayOfCarrierIdentifier1 != null && arrayOfCarrierIdentifier2 != null) {
          if (arrayOfCarrierIdentifier1.length != arrayOfCarrierIdentifier2.length)
            return false; 
          for (byte b1 = 0; b1 < arrayOfCarrierIdentifier1.length; b1++) {
            if (!arrayOfCarrierIdentifier1[b1].equals(arrayOfCarrierIdentifier2[b1]))
              return false; 
          } 
        } else if (arrayOfCarrierIdentifier1 != null || arrayOfCarrierIdentifier2 != null) {
          return false;
        } 
        b++;
        continue;
      } 
      break;
    } 
    if (Arrays.equals(this.mPolicyRules, ((EuiccRulesAuthTable)paramObject).mPolicyRules)) {
      int[] arrayOfInt = this.mPolicyRuleFlags;
      paramObject = ((EuiccRulesAuthTable)paramObject).mPolicyRuleFlags;
      if (Arrays.equals(arrayOfInt, (int[])paramObject))
        return null; 
    } 
    return false;
  }
  
  private EuiccRulesAuthTable(Parcel paramParcel) {
    int[] arrayOfInt = paramParcel.createIntArray();
    int i = arrayOfInt.length;
    this.mCarrierIds = new CarrierIdentifier[i][];
    for (byte b = 0; b < i; b++)
      this.mCarrierIds[b] = (CarrierIdentifier[])paramParcel.createTypedArray(CarrierIdentifier.CREATOR); 
    this.mPolicyRuleFlags = paramParcel.createIntArray();
  }
  
  public static final Parcelable.Creator<EuiccRulesAuthTable> CREATOR = new Parcelable.Creator<EuiccRulesAuthTable>() {
      public EuiccRulesAuthTable createFromParcel(Parcel param1Parcel) {
        return new EuiccRulesAuthTable(param1Parcel);
      }
      
      public EuiccRulesAuthTable[] newArray(int param1Int) {
        return new EuiccRulesAuthTable[param1Int];
      }
    };
  
  public static final int POLICY_RULE_FLAG_CONSENT_REQUIRED = 1;
  
  private final CarrierIdentifier[][] mCarrierIds;
  
  private final int[] mPolicyRuleFlags;
  
  private final int[] mPolicyRules;
}
