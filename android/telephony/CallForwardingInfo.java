package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.telephony.Rlog;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;

public final class CallForwardingInfo implements Parcelable {
  public CallForwardingInfo(int paramInt1, int paramInt2, String paramString, int paramInt3) {
    this.mStatus = paramInt1;
    this.mReason = paramInt2;
    this.mNumber = paramString;
    this.mTimeSeconds = paramInt3;
  }
  
  public int getStatus() {
    return this.mStatus;
  }
  
  public int getReason() {
    return this.mReason;
  }
  
  public String getNumber() {
    return this.mNumber;
  }
  
  public int getTimeoutSeconds() {
    return this.mTimeSeconds;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mNumber);
    paramParcel.writeInt(this.mStatus);
    paramParcel.writeInt(this.mReason);
    paramParcel.writeInt(this.mTimeSeconds);
  }
  
  private CallForwardingInfo(Parcel paramParcel) {
    this.mNumber = paramParcel.readString();
    this.mStatus = paramParcel.readInt();
    this.mReason = paramParcel.readInt();
    this.mTimeSeconds = paramParcel.readInt();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof CallForwardingInfo))
      return false; 
    paramObject = paramObject;
    if (this.mStatus != ((CallForwardingInfo)paramObject).mStatus || this.mNumber != ((CallForwardingInfo)paramObject).mNumber || this.mReason != ((CallForwardingInfo)paramObject).mReason || this.mTimeSeconds != ((CallForwardingInfo)paramObject).mTimeSeconds)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mStatus), this.mNumber, Integer.valueOf(this.mReason), Integer.valueOf(this.mTimeSeconds) });
  }
  
  public static final Parcelable.Creator<CallForwardingInfo> CREATOR = new Parcelable.Creator<CallForwardingInfo>() {
      public CallForwardingInfo createFromParcel(Parcel param1Parcel) {
        return new CallForwardingInfo(param1Parcel);
      }
      
      public CallForwardingInfo[] newArray(int param1Int) {
        return new CallForwardingInfo[param1Int];
      }
    };
  
  public static final int REASON_ALL = 4;
  
  public static final int REASON_ALL_CONDITIONAL = 5;
  
  public static final int REASON_BUSY = 1;
  
  public static final int REASON_NOT_REACHABLE = 3;
  
  public static final int REASON_NO_REPLY = 2;
  
  public static final int REASON_UNCONDITIONAL = 0;
  
  public static final int STATUS_ACTIVE = 1;
  
  public static final int STATUS_FDN_CHECK_FAILURE = 2;
  
  public static final int STATUS_INACTIVE = 0;
  
  public static final int STATUS_NOT_SUPPORTED = 4;
  
  public static final int STATUS_UNKNOWN_ERROR = 3;
  
  private static final String TAG = "CallForwardingInfo";
  
  private String mNumber;
  
  private int mReason;
  
  private int mStatus;
  
  private int mTimeSeconds;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[CallForwardingInfo: status=");
    stringBuilder.append(this.mStatus);
    stringBuilder.append(", reason= ");
    stringBuilder.append(this.mReason);
    stringBuilder.append(", timeSec= ");
    stringBuilder.append(this.mTimeSeconds);
    stringBuilder.append(" seconds, number=");
    null = this.mNumber;
    stringBuilder.append(Rlog.pii("CallForwardingInfo", null));
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class CallForwardingReason implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class CallForwardingStatus implements Annotation {}
}
