package android.telephony;

import android.annotation.SystemApi;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.util.Log;

public abstract class VisualVoicemailService extends Service {
  public static final String DATA_PHONE_ACCOUNT_HANDLE = "data_phone_account_handle";
  
  public static final String DATA_SMS = "data_sms";
  
  public static final int MSG_ON_CELL_SERVICE_CONNECTED = 1;
  
  public static final int MSG_ON_SIM_REMOVED = 3;
  
  public static final int MSG_ON_SMS_RECEIVED = 2;
  
  public static final int MSG_TASK_ENDED = 4;
  
  public static final int MSG_TASK_STOPPED = 5;
  
  public static final String SERVICE_INTERFACE = "android.telephony.VisualVoicemailService";
  
  private static final String TAG = "VvmService";
  
  class VisualVoicemailTask {
    private final Messenger mReplyTo;
    
    private final int mTaskId;
    
    private VisualVoicemailTask(VisualVoicemailService this$0, int param1Int) {
      this.mTaskId = param1Int;
      this.mReplyTo = (Messenger)this$0;
    }
    
    public final void finish() {
      Message message = Message.obtain();
      try {
        message.what = 4;
        message.arg1 = this.mTaskId;
        this.mReplyTo.send(message);
      } catch (RemoteException remoteException) {
        Log.e("VvmService", "Cannot send MSG_TASK_ENDED, remote handler no longer exist");
      } 
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof VisualVoicemailTask;
      boolean bool1 = false;
      if (!bool)
        return false; 
      if (this.mTaskId == ((VisualVoicemailTask)param1Object).mTaskId)
        bool1 = true; 
      return bool1;
    }
    
    public int hashCode() {
      return this.mTaskId;
    }
  }
  
  private final Messenger mMessenger = new Messenger((Handler)new Object(this));
  
  public abstract void onStopped(VisualVoicemailTask paramVisualVoicemailTask);
  
  public abstract void onSmsReceived(VisualVoicemailTask paramVisualVoicemailTask, VisualVoicemailSms paramVisualVoicemailSms);
  
  public abstract void onSimRemoved(VisualVoicemailTask paramVisualVoicemailTask, PhoneAccountHandle paramPhoneAccountHandle);
  
  public abstract void onCellServiceConnected(VisualVoicemailTask paramVisualVoicemailTask, PhoneAccountHandle paramPhoneAccountHandle);
  
  public IBinder onBind(Intent paramIntent) {
    return this.mMessenger.getBinder();
  }
  
  @SystemApi
  public static final void setSmsFilterSettings(Context paramContext, PhoneAccountHandle paramPhoneAccountHandle, VisualVoicemailSmsFilterSettings paramVisualVoicemailSmsFilterSettings) {
    TelephonyManager telephonyManager = (TelephonyManager)paramContext.getSystemService(TelephonyManager.class);
    int i = getSubId(paramContext, paramPhoneAccountHandle);
    if (paramVisualVoicemailSmsFilterSettings == null) {
      telephonyManager.disableVisualVoicemailSmsFilter(i);
    } else {
      telephonyManager.enableVisualVoicemailSmsFilter(i, paramVisualVoicemailSmsFilterSettings);
    } 
  }
  
  @SystemApi
  public static final void sendVisualVoicemailSms(Context paramContext, PhoneAccountHandle paramPhoneAccountHandle, String paramString1, short paramShort, String paramString2, PendingIntent paramPendingIntent) {
    TelephonyManager telephonyManager = (TelephonyManager)paramContext.getSystemService(TelephonyManager.class);
    telephonyManager.sendVisualVoicemailSmsForSubscriber(getSubId(paramContext, paramPhoneAccountHandle), paramString1, paramShort, paramString2, paramPendingIntent);
  }
  
  private static int getSubId(Context paramContext, PhoneAccountHandle paramPhoneAccountHandle) {
    TelephonyManager telephonyManager = (TelephonyManager)paramContext.getSystemService(TelephonyManager.class);
    TelecomManager telecomManager = (TelecomManager)paramContext.getSystemService(TelecomManager.class);
    return 
      telephonyManager.getSubIdForPhoneAccount(telecomManager.getPhoneAccount(paramPhoneAccountHandle));
  }
}
