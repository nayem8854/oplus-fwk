package android.telephony;

import android.app.SystemServiceRegistry;
import android.content.Context;
import android.os.TelephonyServiceManager;
import android.telephony.euicc.EuiccCardManager;
import android.telephony.euicc.EuiccManager;
import android.telephony.ims.ImsManager;
import com.android.internal.util.Preconditions;

public class TelephonyFrameworkInitializer {
  private static volatile TelephonyServiceManager sTelephonyServiceManager;
  
  public static void setTelephonyServiceManager(TelephonyServiceManager paramTelephonyServiceManager) {
    boolean bool;
    if (sTelephonyServiceManager == null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkState(bool, "setTelephonyServiceManager called twice!");
    sTelephonyServiceManager = (TelephonyServiceManager)Preconditions.checkNotNull(paramTelephonyServiceManager);
  }
  
  public static void registerServiceWrappers() {
    SystemServiceRegistry.registerContextAwareService("phone", TelephonyManager.class, (SystemServiceRegistry.ContextAwareServiceProducerWithoutBinder)_$$Lambda$TelephonyFrameworkInitializer$sQClc4rjc9ydh0nXpY79gr33av4.INSTANCE);
    SystemServiceRegistry.registerContextAwareService("telephony_subscription_service", SubscriptionManager.class, (SystemServiceRegistry.ContextAwareServiceProducerWithoutBinder)_$$Lambda$TelephonyFrameworkInitializer$b_92_3ZijRrdEa9yLyFA5xu19OM.INSTANCE);
    SystemServiceRegistry.registerContextAwareService("carrier_config", CarrierConfigManager.class, (SystemServiceRegistry.ContextAwareServiceProducerWithoutBinder)_$$Lambda$TelephonyFrameworkInitializer$3Kis6wL1IbLustWe9A2o4_2YpGo.INSTANCE);
    SystemServiceRegistry.registerContextAwareService("euicc", EuiccManager.class, (SystemServiceRegistry.ContextAwareServiceProducerWithoutBinder)_$$Lambda$TelephonyFrameworkInitializer$mpe0Kh92VEQmEtmo60oqykdvnBE.INSTANCE);
    SystemServiceRegistry.registerContextAwareService("euicc_card", EuiccCardManager.class, (SystemServiceRegistry.ContextAwareServiceProducerWithoutBinder)_$$Lambda$TelephonyFrameworkInitializer$MLDtRnX1dj1RKFdjgIsOvcQxhA0.INSTANCE);
    SystemServiceRegistry.registerContextAwareService("telephony_ims", ImsManager.class, (SystemServiceRegistry.ContextAwareServiceProducerWithoutBinder)_$$Lambda$TelephonyFrameworkInitializer$o3geRfUaRT9tnqKKZbu1EbUxw4Q.INSTANCE);
  }
  
  public static TelephonyServiceManager getTelephonyServiceManager() {
    return sTelephonyServiceManager;
  }
}
