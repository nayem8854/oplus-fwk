package android.telephony;

import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.TelephonyServiceManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import com.android.internal.telephony.ITelephony;

public abstract class CellLocation {
  public abstract void setStateInvalid();
  
  public abstract boolean isEmpty();
  
  public abstract void fillInNotifierBundle(Bundle paramBundle);
  
  public static void requestLocationUpdate() {
    try {
      TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
      TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getTelephonyServiceRegisterer();
      IBinder iBinder = serviceRegisterer.get();
      ITelephony iTelephony = ITelephony.Stub.asInterface(iBinder);
      if (iTelephony != null)
        iTelephony.updateServiceLocation(); 
    } catch (RemoteException remoteException) {}
  }
  
  public static CellLocation newFromBundle(Bundle paramBundle) {
    int i = TelephonyManager.getDefault().getCurrentPhoneType();
    if (i != 1) {
      if (i != 2)
        return null; 
      return new CdmaCellLocation(paramBundle);
    } 
    return new GsmCellLocation(paramBundle);
  }
  
  public static CellLocation getEmpty() {
    int i = TelephonyManager.getDefault().getCurrentPhoneType();
    if (i != 1) {
      if (i != 2)
        return null; 
      return new CdmaCellLocation();
    } 
    return new GsmCellLocation();
  }
}
