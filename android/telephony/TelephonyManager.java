package android.telephony;

import android.annotation.SystemApi;
import android.app.PendingIntent;
import android.compat.Compatibility;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.os.PersistableBundle;
import android.os.Process;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.os.SystemProperties;
import android.os.TelephonyServiceManager;
import android.os.WorkSource;
import android.provider.Settings;
import android.provider.Telephony;
import android.service.carrier.CarrierIdentifier;
import android.sysprop.TelephonyProperties;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.data.ApnSetting;
import android.telephony.emergency.EmergencyNumber;
import android.telephony.ims.aidl.IImsConfig;
import android.telephony.ims.aidl.IImsMmTelFeature;
import android.telephony.ims.aidl.IImsRcsFeature;
import android.telephony.ims.aidl.IImsRegistration;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.util.SeempLog;
import com.android.ims.internal.IImsServiceFeatureCallback;
import com.android.internal.telephony.CellNetworkScanResult;
import com.android.internal.telephony.IBooleanConsumer;
import com.android.internal.telephony.INumberVerificationCallback;
import com.android.internal.telephony.IOns;
import com.android.internal.telephony.IPhoneSubInfo;
import com.android.internal.telephony.ISetOpportunisticDataCallback;
import com.android.internal.telephony.ISms;
import com.android.internal.telephony.ISub;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.IUpdateAvailableNetworksCallback;
import com.android.internal.telephony.OperatorInfo;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.RILConstants;
import com.android.internal.telephony.SmsApplication;
import com.android.internal.util.Preconditions;
import com.android.telephony.Rlog;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TelephonyManager {
  @SystemApi
  public static final String ACTION_ANOMALY_REPORTED = "android.telephony.action.ANOMALY_REPORTED";
  
  public static final String ACTION_CALL_DISCONNECT_CAUSE_CHANGED = "android.intent.action.CALL_DISCONNECT_CAUSE";
  
  public static final String ACTION_CARRIER_MESSAGING_CLIENT_SERVICE = "android.telephony.action.CARRIER_MESSAGING_CLIENT_SERVICE";
  
  public static final String ACTION_CARRIER_SIGNAL_DEFAULT_NETWORK_AVAILABLE = "com.android.internal.telephony.CARRIER_SIGNAL_DEFAULT_NETWORK_AVAILABLE";
  
  public static final String ACTION_CARRIER_SIGNAL_PCO_VALUE = "com.android.internal.telephony.CARRIER_SIGNAL_PCO_VALUE";
  
  public static final String ACTION_CARRIER_SIGNAL_REDIRECTED = "com.android.internal.telephony.CARRIER_SIGNAL_REDIRECTED";
  
  public static final String ACTION_CARRIER_SIGNAL_REQUEST_NETWORK_FAILED = "com.android.internal.telephony.CARRIER_SIGNAL_REQUEST_NETWORK_FAILED";
  
  public static final String ACTION_CARRIER_SIGNAL_RESET = "com.android.internal.telephony.CARRIER_SIGNAL_RESET";
  
  public static final String ACTION_CONFIGURE_VOICEMAIL = "android.telephony.action.CONFIGURE_VOICEMAIL";
  
  public static final String ACTION_DATA_STALL_DETECTED = "android.intent.action.DATA_STALL_DETECTED";
  
  @SystemApi
  public static final String ACTION_DEFAULT_DATA_SUBSCRIPTION_CHANGED = "android.intent.action.ACTION_DEFAULT_DATA_SUBSCRIPTION_CHANGED";
  
  @SystemApi
  public static final String ACTION_DEFAULT_VOICE_SUBSCRIPTION_CHANGED = "android.intent.action.ACTION_DEFAULT_VOICE_SUBSCRIPTION_CHANGED";
  
  @SystemApi
  public static final String ACTION_EMERGENCY_ASSISTANCE = "android.telephony.action.EMERGENCY_ASSISTANCE";
  
  @SystemApi
  public static final String ACTION_EMERGENCY_CALLBACK_MODE_CHANGED = "android.intent.action.EMERGENCY_CALLBACK_MODE_CHANGED";
  
  @SystemApi
  public static final String ACTION_EMERGENCY_CALL_STATE_CHANGED = "android.intent.action.EMERGENCY_CALL_STATE_CHANGED";
  
  public static final String ACTION_MULTI_SIM_CONFIG_CHANGED = "android.telephony.action.MULTI_SIM_CONFIG_CHANGED";
  
  public static final String ACTION_NETWORK_COUNTRY_CHANGED = "android.telephony.action.NETWORK_COUNTRY_CHANGED";
  
  public static final String ACTION_PHONE_STATE_CHANGED = "android.intent.action.PHONE_STATE";
  
  public static final String ACTION_PRIMARY_SUBSCRIPTION_LIST_CHANGED = "android.telephony.action.PRIMARY_SUBSCRIPTION_LIST_CHANGED";
  
  @SystemApi
  public static final String ACTION_REQUEST_OMADM_CONFIGURATION_UPDATE = "com.android.omadm.service.CONFIGURATION_UPDATE";
  
  public static final String ACTION_RESPOND_VIA_MESSAGE = "android.intent.action.RESPOND_VIA_MESSAGE";
  
  public static final String ACTION_SECRET_CODE = "android.telephony.action.SECRET_CODE";
  
  public static final String ACTION_SERVICE_PROVIDERS_UPDATED = "android.telephony.action.SERVICE_PROVIDERS_UPDATED";
  
  @SystemApi
  public static final String ACTION_SHOW_NOTICE_ECM_BLOCK_OTHERS = "android.telephony.action.SHOW_NOTICE_ECM_BLOCK_OTHERS";
  
  public static final String ACTION_SHOW_VOICEMAIL_NOTIFICATION = "android.telephony.action.SHOW_VOICEMAIL_NOTIFICATION";
  
  @SystemApi
  public static final String ACTION_SIM_APPLICATION_STATE_CHANGED = "android.telephony.action.SIM_APPLICATION_STATE_CHANGED";
  
  @SystemApi
  public static final String ACTION_SIM_CARD_STATE_CHANGED = "android.telephony.action.SIM_CARD_STATE_CHANGED";
  
  @SystemApi
  public static final String ACTION_SIM_SLOT_STATUS_CHANGED = "android.telephony.action.SIM_SLOT_STATUS_CHANGED";
  
  public static final String ACTION_SUBSCRIPTION_CARRIER_IDENTITY_CHANGED = "android.telephony.action.SUBSCRIPTION_CARRIER_IDENTITY_CHANGED";
  
  public static final String ACTION_SUBSCRIPTION_SPECIFIC_CARRIER_IDENTITY_CHANGED = "android.telephony.action.SUBSCRIPTION_SPECIFIC_CARRIER_IDENTITY_CHANGED";
  
  public static final int ALLOWED_NETWORK_TYPES_REASON_POWER = 0;
  
  public static final int APPTYPE_CSIM = 4;
  
  public static final int APPTYPE_ISIM = 5;
  
  public static final int APPTYPE_RUIM = 3;
  
  public static final int APPTYPE_SIM = 1;
  
  public static final int APPTYPE_USIM = 2;
  
  public static final int AUTHTYPE_EAP_AKA = 129;
  
  public static final int AUTHTYPE_EAP_SIM = 128;
  
  private static final long CALLBACK_ON_MORE_ERROR_CODE_CHANGE = 130595455L;
  
  public static final int CALL_STATE_IDLE = 0;
  
  public static final int CALL_STATE_OFFHOOK = 2;
  
  public static final int CALL_STATE_RINGING = 1;
  
  public static final int CALL_WAITING_STATUS_ACTIVE = 1;
  
  public static final int CALL_WAITING_STATUS_INACTIVE = 2;
  
  public static final int CALL_WAITING_STATUS_NOT_SUPPORTED = 4;
  
  public static final int CALL_WAITING_STATUS_UNKNOWN_ERROR = 3;
  
  public static final int CARD_POWER_DOWN = 0;
  
  public static final int CARD_POWER_UP = 1;
  
  public static final int CARD_POWER_UP_PASS_THROUGH = 2;
  
  @SystemApi
  public static final int CARRIER_PRIVILEGE_STATUS_ERROR_LOADING_RULES = -2;
  
  @SystemApi
  public static final int CARRIER_PRIVILEGE_STATUS_HAS_ACCESS = 1;
  
  @SystemApi
  public static final int CARRIER_PRIVILEGE_STATUS_NO_ACCESS = 0;
  
  @SystemApi
  public static final int CARRIER_PRIVILEGE_STATUS_RULES_NOT_LOADED = -1;
  
  public static final int CDMA_ROAMING_MODE_AFFILIATED = 1;
  
  public static final int CDMA_ROAMING_MODE_ANY = 2;
  
  public static final int CDMA_ROAMING_MODE_HOME = 0;
  
  public static final int CDMA_ROAMING_MODE_RADIO_DEFAULT = -1;
  
  public static final int CDMA_SUBSCRIPTION_NV = 1;
  
  public static final int CDMA_SUBSCRIPTION_RUIM_SIM = 0;
  
  public static final int CDMA_SUBSCRIPTION_UNKNOWN = -1;
  
  public static final int CHANGE_ICC_LOCK_SUCCESS = 2147483647;
  
  public static final int DATA_ACTIVITY_DORMANT = 4;
  
  public static final int DATA_ACTIVITY_IN = 1;
  
  public static final int DATA_ACTIVITY_INOUT = 3;
  
  public static final int DATA_ACTIVITY_NONE = 0;
  
  public static final int DATA_ACTIVITY_OUT = 2;
  
  public static final int DATA_CONNECTED = 2;
  
  public static final int DATA_CONNECTING = 1;
  
  public static final int DATA_DISCONNECTED = 0;
  
  public static final int DATA_DISCONNECTING = 4;
  
  public static final int DATA_SUSPENDED = 3;
  
  public static final int DATA_UNKNOWN = -1;
  
  public static final int DEFAULT_PREFERRED_NETWORK_MODE;
  
  public static final boolean EMERGENCY_ASSISTANCE_ENABLED = true;
  
  public static final String EVENT_CALL_FORWARDED = "android.telephony.event.EVENT_CALL_FORWARDED";
  
  public static final String EVENT_DOWNGRADE_DATA_DISABLED = "android.telephony.event.EVENT_DOWNGRADE_DATA_DISABLED";
  
  public static final String EVENT_DOWNGRADE_DATA_LIMIT_REACHED = "android.telephony.event.EVENT_DOWNGRADE_DATA_LIMIT_REACHED";
  
  public static final String EVENT_HANDOVER_TO_WIFI_FAILED = "android.telephony.event.EVENT_HANDOVER_TO_WIFI_FAILED";
  
  public static final String EVENT_HANDOVER_VIDEO_FROM_LTE_TO_WIFI = "android.telephony.event.EVENT_HANDOVER_VIDEO_FROM_LTE_TO_WIFI";
  
  public static final String EVENT_HANDOVER_VIDEO_FROM_WIFI_TO_LTE = "android.telephony.event.EVENT_HANDOVER_VIDEO_FROM_WIFI_TO_LTE";
  
  public static final String EVENT_NOTIFY_INTERNATIONAL_CALL_ON_WFC = "android.telephony.event.EVENT_NOTIFY_INTERNATIONAL_CALL_ON_WFC";
  
  public static final String EVENT_SUPPLEMENTARY_SERVICE_NOTIFICATION = "android.telephony.event.EVENT_SUPPLEMENTARY_SERVICE_NOTIFICATION";
  
  public static final String EXTRA_ACTIVE_SIM_SUPPORTED_COUNT = "android.telephony.extra.ACTIVE_SIM_SUPPORTED_COUNT";
  
  @SystemApi
  public static final String EXTRA_ANOMALY_DESCRIPTION = "android.telephony.extra.ANOMALY_DESCRIPTION";
  
  @SystemApi
  public static final String EXTRA_ANOMALY_ID = "android.telephony.extra.ANOMALY_ID";
  
  @Deprecated
  public static final String EXTRA_APN_PROTOCOL = "apnProto";
  
  public static final String EXTRA_APN_PROTOCOL_INT = "apnProtoInt";
  
  @Deprecated
  public static final String EXTRA_APN_TYPE = "apnType";
  
  public static final String EXTRA_APN_TYPE_INT = "apnTypeInt";
  
  public static final String EXTRA_CALL_VOICEMAIL_INTENT = "android.telephony.extra.CALL_VOICEMAIL_INTENT";
  
  public static final String EXTRA_CARRIER_ID = "android.telephony.extra.CARRIER_ID";
  
  public static final String EXTRA_CARRIER_NAME = "android.telephony.extra.CARRIER_NAME";
  
  public static final String EXTRA_DATA_SPN = "android.telephony.extra.DATA_SPN";
  
  public static final String EXTRA_DEFAULT_NETWORK_AVAILABLE = "defaultNetworkAvailable";
  
  public static final String EXTRA_DEFAULT_SUBSCRIPTION_SELECT_TYPE = "android.telephony.extra.DEFAULT_SUBSCRIPTION_SELECT_TYPE";
  
  public static final int EXTRA_DEFAULT_SUBSCRIPTION_SELECT_TYPE_ALL = 4;
  
  public static final int EXTRA_DEFAULT_SUBSCRIPTION_SELECT_TYPE_DATA = 1;
  
  public static final int EXTRA_DEFAULT_SUBSCRIPTION_SELECT_TYPE_NONE = 0;
  
  public static final int EXTRA_DEFAULT_SUBSCRIPTION_SELECT_TYPE_SMS = 3;
  
  public static final int EXTRA_DEFAULT_SUBSCRIPTION_SELECT_TYPE_VOICE = 2;
  
  @Deprecated
  public static final String EXTRA_DISCONNECT_CAUSE = "disconnect_cause";
  
  public static final String EXTRA_ERROR_CODE = "errorCode";
  
  public static final String EXTRA_HIDE_PUBLIC_SETTINGS = "android.telephony.extra.HIDE_PUBLIC_SETTINGS";
  
  @Deprecated
  public static final String EXTRA_INCOMING_NUMBER = "incoming_number";
  
  public static final String EXTRA_IS_REFRESH = "android.telephony.extra.IS_REFRESH";
  
  public static final String EXTRA_LAST_KNOWN_NETWORK_COUNTRY = "android.telephony.extra.LAST_KNOWN_NETWORK_COUNTRY";
  
  public static final String EXTRA_LAUNCH_VOICEMAIL_SETTINGS_INTENT = "android.telephony.extra.LAUNCH_VOICEMAIL_SETTINGS_INTENT";
  
  public static final String EXTRA_NETWORK_COUNTRY = "android.telephony.extra.NETWORK_COUNTRY";
  
  public static final String EXTRA_NOTIFICATION_CODE = "android.telephony.extra.NOTIFICATION_CODE";
  
  public static final String EXTRA_NOTIFICATION_COUNT = "android.telephony.extra.NOTIFICATION_COUNT";
  
  public static final String EXTRA_NOTIFICATION_MESSAGE = "android.telephony.extra.NOTIFICATION_MESSAGE";
  
  public static final String EXTRA_NOTIFICATION_TYPE = "android.telephony.extra.NOTIFICATION_TYPE";
  
  public static final String EXTRA_PCO_ID = "pcoId";
  
  public static final String EXTRA_PCO_VALUE = "pcoValue";
  
  public static final String EXTRA_PHONE_ACCOUNT_HANDLE = "android.telephony.extra.PHONE_ACCOUNT_HANDLE";
  
  @SystemApi
  public static final String EXTRA_PHONE_IN_ECM_STATE = "android.telephony.extra.PHONE_IN_ECM_STATE";
  
  @SystemApi
  public static final String EXTRA_PHONE_IN_EMERGENCY_CALL = "android.telephony.extra.PHONE_IN_EMERGENCY_CALL";
  
  public static final String EXTRA_PLMN = "android.telephony.extra.PLMN";
  
  public static final String EXTRA_PRECISE_DISCONNECT_CAUSE = "precise_disconnect_cause";
  
  public static final String EXTRA_RECOVERY_ACTION = "recoveryAction";
  
  public static final String EXTRA_REDIRECTION_URL = "redirectionUrl";
  
  public static final String EXTRA_SHOW_PLMN = "android.telephony.extra.SHOW_PLMN";
  
  public static final String EXTRA_SHOW_SPN = "android.telephony.extra.SHOW_SPN";
  
  public static final String EXTRA_SIM_COMBINATION_NAMES = "android.telephony.extra.SIM_COMBINATION_NAMES";
  
  public static final String EXTRA_SIM_COMBINATION_WARNING_TYPE = "android.telephony.extra.SIM_COMBINATION_WARNING_TYPE";
  
  public static final int EXTRA_SIM_COMBINATION_WARNING_TYPE_DUAL_CDMA = 1;
  
  public static final int EXTRA_SIM_COMBINATION_WARNING_TYPE_NONE = 0;
  
  @SystemApi
  public static final String EXTRA_SIM_STATE = "android.telephony.extra.SIM_STATE";
  
  public static final String EXTRA_SPECIFIC_CARRIER_ID = "android.telephony.extra.SPECIFIC_CARRIER_ID";
  
  public static final String EXTRA_SPECIFIC_CARRIER_NAME = "android.telephony.extra.SPECIFIC_CARRIER_NAME";
  
  public static final String EXTRA_SPN = "android.telephony.extra.SPN";
  
  public static final String EXTRA_STATE = "state";
  
  public static final String EXTRA_STATE_IDLE;
  
  public static final String EXTRA_STATE_OFFHOOK;
  
  public static final String EXTRA_STATE_RINGING;
  
  public static final String EXTRA_SUBSCRIPTION_ID = "android.telephony.extra.SUBSCRIPTION_ID";
  
  @SystemApi
  public static final String EXTRA_VISUAL_VOICEMAIL_ENABLED_BY_USER_BOOL = "android.telephony.extra.VISUAL_VOICEMAIL_ENABLED_BY_USER_BOOL";
  
  public static final String EXTRA_VOICEMAIL_NUMBER = "android.telephony.extra.VOICEMAIL_NUMBER";
  
  @SystemApi
  public static final String EXTRA_VOICEMAIL_SCRAMBLED_PIN_STRING = "android.telephony.extra.VOICEMAIL_SCRAMBLED_PIN_STRING";
  
  private static final long GET_DATA_STATE_R_VERSION = 148534348L;
  
  public static final int INDICATION_FILTER_DATA_CALL_DORMANCY_CHANGED = 4;
  
  public static final int INDICATION_FILTER_FULL_NETWORK_STATE = 2;
  
  public static final int INDICATION_FILTER_LINK_CAPACITY_ESTIMATE = 8;
  
  public static final int INDICATION_FILTER_PHYSICAL_CHANNEL_CONFIG = 16;
  
  public static final int INDICATION_FILTER_SIGNAL_STRENGTH = 1;
  
  public static final int INDICATION_UPDATE_MODE_IGNORE_SCREEN_OFF = 2;
  
  public static final int INDICATION_UPDATE_MODE_NORMAL = 1;
  
  @SystemApi
  public static final int INVALID_EMERGENCY_NUMBER_DB_VERSION = -1;
  
  @SystemApi
  public static final int KEY_TYPE_EPDG = 1;
  
  @SystemApi
  public static final int KEY_TYPE_WLAN = 2;
  
  private static final long MAX_NUMBER_VERIFICATION_TIMEOUT_MILLIS = 60000L;
  
  public static final String METADATA_HIDE_VOICEMAIL_SETTINGS_MENU = "android.telephony.HIDE_VOICEMAIL_SETTINGS_MENU";
  
  public static final String MODEM_ACTIVITY_RESULT_KEY = "controller_activity";
  
  public static final int MULTISIM_ALLOWED = 0;
  
  public static final int MULTISIM_NOT_SUPPORTED_BY_CARRIER = 2;
  
  public static final int MULTISIM_NOT_SUPPORTED_BY_HARDWARE = 1;
  
  public static final int NETWORK_CLASS_2_G = 1;
  
  public static final int NETWORK_CLASS_3_G = 2;
  
  public static final int NETWORK_CLASS_4_G = 3;
  
  public static final int NETWORK_CLASS_5_G = 4;
  
  public static final long NETWORK_CLASS_BITMASK_2G = 32843L;
  
  public static final long NETWORK_CLASS_BITMASK_3G = 93108L;
  
  public static final long NETWORK_CLASS_BITMASK_4G = 397312L;
  
  public static final long NETWORK_CLASS_BITMASK_5G = 524288L;
  
  public static final int NETWORK_CLASS_UNKNOWN = 0;
  
  public static final int NETWORK_MODE_CDMA_EVDO = 4;
  
  public static final int NETWORK_MODE_CDMA_NO_EVDO = 5;
  
  public static final int NETWORK_MODE_EVDO_NO_CDMA = 6;
  
  public static final int NETWORK_MODE_GLOBAL = 7;
  
  public static final int NETWORK_MODE_GSM_ONLY = 1;
  
  public static final int NETWORK_MODE_GSM_UMTS = 3;
  
  public static final int NETWORK_MODE_LTE_CDMA_EVDO = 8;
  
  public static final int NETWORK_MODE_LTE_CDMA_EVDO_GSM_WCDMA = 10;
  
  public static final int NETWORK_MODE_LTE_GSM_WCDMA = 9;
  
  public static final int NETWORK_MODE_LTE_ONLY = 11;
  
  public static final int NETWORK_MODE_LTE_TDSCDMA = 15;
  
  public static final int NETWORK_MODE_LTE_TDSCDMA_CDMA_EVDO_GSM_WCDMA = 22;
  
  public static final int NETWORK_MODE_LTE_TDSCDMA_GSM = 17;
  
  public static final int NETWORK_MODE_LTE_TDSCDMA_GSM_WCDMA = 20;
  
  public static final int NETWORK_MODE_LTE_TDSCDMA_WCDMA = 19;
  
  public static final int NETWORK_MODE_LTE_WCDMA = 12;
  
  public static final int NETWORK_MODE_NR_LTE = 24;
  
  public static final int NETWORK_MODE_NR_LTE_CDMA_EVDO = 25;
  
  public static final int NETWORK_MODE_NR_LTE_CDMA_EVDO_GSM_WCDMA = 27;
  
  public static final int NETWORK_MODE_NR_LTE_GSM_WCDMA = 26;
  
  public static final int NETWORK_MODE_NR_LTE_TDSCDMA = 29;
  
  public static final int NETWORK_MODE_NR_LTE_TDSCDMA_CDMA_EVDO_GSM_WCDMA = 33;
  
  public static final int NETWORK_MODE_NR_LTE_TDSCDMA_GSM = 30;
  
  public static final int NETWORK_MODE_NR_LTE_TDSCDMA_GSM_WCDMA = 32;
  
  public static final int NETWORK_MODE_NR_LTE_TDSCDMA_WCDMA = 31;
  
  public static final int NETWORK_MODE_NR_LTE_WCDMA = 28;
  
  public static final int NETWORK_MODE_NR_ONLY = 23;
  
  public static final int NETWORK_MODE_TDSCDMA_CDMA_EVDO_GSM_WCDMA = 21;
  
  public static final int NETWORK_MODE_TDSCDMA_GSM = 16;
  
  public static final int NETWORK_MODE_TDSCDMA_GSM_WCDMA = 18;
  
  public static final int NETWORK_MODE_TDSCDMA_ONLY = 13;
  
  public static final int NETWORK_MODE_TDSCDMA_WCDMA = 14;
  
  public static final int NETWORK_MODE_WCDMA_ONLY = 2;
  
  public static final int NETWORK_MODE_WCDMA_PREF = 0;
  
  public static final int NETWORK_SELECTION_MODE_AUTO = 1;
  
  public static final int NETWORK_SELECTION_MODE_MANUAL = 2;
  
  public static final int NETWORK_SELECTION_MODE_UNKNOWN = 0;
  
  public static final long NETWORK_STANDARDS_FAMILY_BITMASK_3GPP = 906119L;
  
  public static final long NETWORK_STANDARDS_FAMILY_BITMASK_3GPP2 = 10360L;
  
  private static final int[] NETWORK_TYPES;
  
  public static final int NETWORK_TYPE_1xRTT = 7;
  
  @SystemApi
  public static final long NETWORK_TYPE_BITMASK_1xRTT = 64L;
  
  @SystemApi
  public static final long NETWORK_TYPE_BITMASK_CDMA = 8L;
  
  @SystemApi
  public static final long NETWORK_TYPE_BITMASK_EDGE = 2L;
  
  @SystemApi
  public static final long NETWORK_TYPE_BITMASK_EHRPD = 8192L;
  
  @SystemApi
  public static final long NETWORK_TYPE_BITMASK_EVDO_0 = 16L;
  
  @SystemApi
  public static final long NETWORK_TYPE_BITMASK_EVDO_A = 32L;
  
  @SystemApi
  public static final long NETWORK_TYPE_BITMASK_EVDO_B = 2048L;
  
  @SystemApi
  public static final long NETWORK_TYPE_BITMASK_GPRS = 1L;
  
  @SystemApi
  public static final long NETWORK_TYPE_BITMASK_GSM = 32768L;
  
  @SystemApi
  public static final long NETWORK_TYPE_BITMASK_HSDPA = 128L;
  
  @SystemApi
  public static final long NETWORK_TYPE_BITMASK_HSPA = 512L;
  
  @SystemApi
  public static final long NETWORK_TYPE_BITMASK_HSPAP = 16384L;
  
  @SystemApi
  public static final long NETWORK_TYPE_BITMASK_HSUPA = 256L;
  
  @SystemApi
  public static final long NETWORK_TYPE_BITMASK_IWLAN = 131072L;
  
  @SystemApi
  public static final long NETWORK_TYPE_BITMASK_LTE = 4096L;
  
  @SystemApi
  public static final long NETWORK_TYPE_BITMASK_LTE_CA = 262144L;
  
  @SystemApi
  public static final long NETWORK_TYPE_BITMASK_NR = 524288L;
  
  @SystemApi
  public static final long NETWORK_TYPE_BITMASK_TD_SCDMA = 65536L;
  
  @SystemApi
  public static final long NETWORK_TYPE_BITMASK_UMTS = 4L;
  
  @SystemApi
  public static final long NETWORK_TYPE_BITMASK_UNKNOWN = 0L;
  
  public static final int NETWORK_TYPE_CDMA = 4;
  
  public static final int NETWORK_TYPE_EDGE = 2;
  
  public static final int NETWORK_TYPE_EHRPD = 14;
  
  public static final int NETWORK_TYPE_EVDO_0 = 5;
  
  public static final int NETWORK_TYPE_EVDO_A = 6;
  
  public static final int NETWORK_TYPE_EVDO_B = 12;
  
  public static final int NETWORK_TYPE_GPRS = 1;
  
  public static final int NETWORK_TYPE_GSM = 16;
  
  public static final int NETWORK_TYPE_HSDPA = 8;
  
  public static final int NETWORK_TYPE_HSPA = 10;
  
  public static final int NETWORK_TYPE_HSPAP = 15;
  
  public static final int NETWORK_TYPE_HSUPA = 9;
  
  public static final int NETWORK_TYPE_IDEN = 11;
  
  public static final int NETWORK_TYPE_IWLAN = 18;
  
  public static final int NETWORK_TYPE_LTE = 13;
  
  public static final int NETWORK_TYPE_LTE_CA = 19;
  
  public static final int NETWORK_TYPE_NR = 20;
  
  public static final int NETWORK_TYPE_TD_SCDMA = 17;
  
  public static final int NETWORK_TYPE_UMTS = 3;
  
  public static final int NETWORK_TYPE_UNKNOWN = 0;
  
  public static final int OTASP_NEEDED = 2;
  
  public static final int OTASP_NOT_NEEDED = 3;
  
  public static final int OTASP_SIM_UNPROVISIONED = 5;
  
  public static final int OTASP_UNINITIALIZED = 0;
  
  public static final int OTASP_UNKNOWN = 1;
  
  public static final String PHONE_PROCESS_NAME = "com.android.phone";
  
  public static final int PHONE_TYPE_CDMA = 2;
  
  public static final int PHONE_TYPE_GSM = 1;
  
  public static final int PHONE_TYPE_IMS = 5;
  
  public static final int PHONE_TYPE_NONE = 0;
  
  public static final int PHONE_TYPE_SIP = 3;
  
  public static final int PHONE_TYPE_THIRD_PARTY = 4;
  
  public static final int PREFERRED_CDMA_SUBSCRIPTION = 0;
  
  @SystemApi
  public static final int RADIO_POWER_OFF = 0;
  
  @SystemApi
  public static final int RADIO_POWER_ON = 1;
  
  @SystemApi
  public static final int RADIO_POWER_UNAVAILABLE = 2;
  
  @SystemApi
  public static final int SET_CARRIER_RESTRICTION_ERROR = 2;
  
  @SystemApi
  public static final int SET_CARRIER_RESTRICTION_NOT_SUPPORTED = 1;
  
  @SystemApi
  public static final int SET_CARRIER_RESTRICTION_SUCCESS = 0;
  
  public static final int SET_OPPORTUNISTIC_SUB_INACTIVE_SUBSCRIPTION = 2;
  
  public static final int SET_OPPORTUNISTIC_SUB_NO_OPPORTUNISTIC_SUB_AVAILABLE = 3;
  
  public static final int SET_OPPORTUNISTIC_SUB_REMOTE_SERVICE_EXCEPTION = 4;
  
  public static final int SET_OPPORTUNISTIC_SUB_SUCCESS = 0;
  
  public static final int SET_OPPORTUNISTIC_SUB_VALIDATION_FAILED = 1;
  
  @SystemApi
  public static final int SIM_ACTIVATION_STATE_ACTIVATED = 2;
  
  @SystemApi
  public static final int SIM_ACTIVATION_STATE_ACTIVATING = 1;
  
  @SystemApi
  public static final int SIM_ACTIVATION_STATE_DEACTIVATED = 3;
  
  @SystemApi
  public static final int SIM_ACTIVATION_STATE_RESTRICTED = 4;
  
  @SystemApi
  public static final int SIM_ACTIVATION_STATE_UNKNOWN = 0;
  
  public static final int SIM_STATE_ABSENT = 1;
  
  public static final int SIM_STATE_CARD_IO_ERROR = 8;
  
  public static final int SIM_STATE_CARD_RESTRICTED = 9;
  
  @SystemApi
  public static final int SIM_STATE_LOADED = 10;
  
  public static final int SIM_STATE_NETWORK_LOCKED = 4;
  
  public static final int SIM_STATE_NOT_READY = 6;
  
  public static final int SIM_STATE_PERM_DISABLED = 7;
  
  public static final int SIM_STATE_PIN_REQUIRED = 2;
  
  @SystemApi
  public static final int SIM_STATE_PRESENT = 11;
  
  public static final int SIM_STATE_PUK_REQUIRED = 3;
  
  public static final int SIM_STATE_READY = 5;
  
  public static final int SIM_STATE_UNKNOWN = 0;
  
  @SystemApi
  public static final int SRVCC_STATE_HANDOVER_CANCELED = 3;
  
  @SystemApi
  public static final int SRVCC_STATE_HANDOVER_COMPLETED = 1;
  
  @SystemApi
  public static final int SRVCC_STATE_HANDOVER_FAILED = 2;
  
  @SystemApi
  public static final int SRVCC_STATE_HANDOVER_NONE = -1;
  
  @SystemApi
  public static final int SRVCC_STATE_HANDOVER_STARTED = 0;
  
  private static final String TAG = "TelephonyManager";
  
  public static final int UNINITIALIZED_CARD_ID = -2;
  
  public static final int UNKNOWN_CARRIER_ID = -1;
  
  public static final int UNKNOWN_CARRIER_ID_LIST_VERSION = -1;
  
  public static final int UNSUPPORTED_CARD_ID = -1;
  
  public static final int UPDATE_AVAILABLE_NETWORKS_ABORTED = 2;
  
  public static final int UPDATE_AVAILABLE_NETWORKS_DISABLE_MODEM_FAIL = 5;
  
  public static final int UPDATE_AVAILABLE_NETWORKS_ENABLE_MODEM_FAIL = 6;
  
  public static final int UPDATE_AVAILABLE_NETWORKS_INVALID_ARGUMENTS = 3;
  
  public static final int UPDATE_AVAILABLE_NETWORKS_MULTIPLE_NETWORKS_NOT_SUPPORTED = 7;
  
  public static final int UPDATE_AVAILABLE_NETWORKS_NO_CARRIER_PRIVILEGE = 4;
  
  public static final int UPDATE_AVAILABLE_NETWORKS_NO_OPPORTUNISTIC_SUB_AVAILABLE = 8;
  
  public static final int UPDATE_AVAILABLE_NETWORKS_REMOTE_SERVICE_EXCEPTION = 9;
  
  public static final int UPDATE_AVAILABLE_NETWORKS_SERVICE_IS_DISABLED = 10;
  
  public static final int UPDATE_AVAILABLE_NETWORKS_SUCCESS = 0;
  
  public static final int UPDATE_AVAILABLE_NETWORKS_UNKNOWN_FAILURE = 1;
  
  public static final int USSD_ERROR_SERVICE_UNAVAIL = -2;
  
  public static final String USSD_RESPONSE = "USSD_RESPONSE";
  
  public static final int USSD_RETURN_FAILURE = -1;
  
  public static final int USSD_RETURN_SUCCESS = 100;
  
  public static final String VVM_TYPE_CVVM = "vvm_type_cvvm";
  
  public static final String VVM_TYPE_OMTP = "vvm_type_omtp";
  
  private static final Object sCacheLock = new Object();
  
  private static IPhoneSubInfo sIPhoneSubInfo;
  
  private static ISms sISms;
  
  private static ISub sISub;
  
  private static TelephonyManager sInstance;
  
  private static final String sKernelCmdLine;
  
  private static final String sLteOnCdmaProductType;
  
  private static final Pattern sProductTypePattern;
  
  private static final DeathRecipient sServiceDeath;
  
  private static boolean sServiceHandleCacheEnabled = true;
  
  private final Context mContext;
  
  private final int mSubId;
  
  private SubscriptionManager mSubscriptionManager;
  
  private TelephonyScanManager mTelephonyScanManager;
  
  static {
    sServiceDeath = new DeathRecipient();
    sInstance = new TelephonyManager();
    EXTRA_STATE_IDLE = PhoneConstants.State.IDLE.toString();
    EXTRA_STATE_RINGING = PhoneConstants.State.RINGING.toString();
    EXTRA_STATE_OFFHOOK = PhoneConstants.State.OFFHOOK.toString();
    sKernelCmdLine = getProcCmdLine();
    sProductTypePattern = Pattern.compile("\\sproduct_type\\s*=\\s*(\\w+)");
    sLteOnCdmaProductType = TelephonyProperties.lte_on_cdma_product_type().orElse("");
    NETWORK_TYPES = new int[] { 
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 
        11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
    DEFAULT_PREFERRED_NETWORK_MODE = RILConstants.PREFERRED_NETWORK_MODE;
  }
  
  public enum MultiSimVariants {
    DSDA, DSDS, TSTS, UNKNOWN;
    
    private static final MultiSimVariants[] $VALUES;
    
    static {
      MultiSimVariants multiSimVariants = new MultiSimVariants("UNKNOWN", 3);
      $VALUES = new MultiSimVariants[] { DSDS, DSDA, TSTS, multiSimVariants };
    }
  }
  
  public TelephonyManager(Context paramContext) {
    this(paramContext, 2147483647);
  }
  
  public TelephonyManager(Context paramContext, int paramInt) {
    this.mSubId = paramInt;
    Context context = paramContext.getApplicationContext();
    if (context != null) {
      if (Objects.equals(paramContext.getAttributionTag(), context.getAttributionTag())) {
        this.mContext = context;
      } else {
        this.mContext = context.createAttributionContext(paramContext.getAttributionTag());
      } 
    } else {
      this.mContext = paramContext;
    } 
    this.mSubscriptionManager = SubscriptionManager.from(this.mContext);
  }
  
  private TelephonyManager() {
    this.mContext = null;
    this.mSubId = -1;
  }
  
  @Deprecated
  public static TelephonyManager getDefault() {
    return sInstance;
  }
  
  private String getOpPackageName() {
    Context context = this.mContext;
    if (context != null)
      return context.getOpPackageName(); 
    ITelephony iTelephony = getITelephony();
    if (iTelephony == null)
      return null; 
    try {
      return iTelephony.getCurrentPackageName();
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  private String getAttributionTag() {
    Context context = this.mContext;
    if (context != null)
      return context.getAttributionTag(); 
    return null;
  }
  
  private boolean isSystemProcess() {
    boolean bool;
    if (Process.myUid() == 1000) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public MultiSimVariants getMultiSimConfiguration() {
    String str = TelephonyProperties.multi_sim_config().orElse("");
    if (str.equals("dsds"))
      return MultiSimVariants.DSDS; 
    if (str.equals("dsda"))
      return MultiSimVariants.DSDA; 
    if (str.equals("tsts"))
      return MultiSimVariants.TSTS; 
    return MultiSimVariants.UNKNOWN;
  }
  
  @Deprecated
  public int getPhoneCount() {
    return getActiveModemCount();
  }
  
  public int getActiveModemCount() {
    int i = 1;
    int j = null.$SwitchMap$android$telephony$TelephonyManager$MultiSimVariants[getMultiSimConfiguration().ordinal()];
    if (j != 1) {
      if (j != 2 && j != 3) {
        if (j == 4)
          i = 3; 
      } else {
        i = 2;
      } 
    } else {
      j = 1;
      i = j;
      if (!isVoiceCapable()) {
        i = j;
        if (!isSmsCapable()) {
          i = j;
          if (!isDataCapable())
            i = 0; 
        } 
      } 
    } 
    return i;
  }
  
  public int getSupportedModemCount() {
    return ((Integer)TelephonyProperties.max_active_modems().orElse(Integer.valueOf(getActiveModemCount()))).intValue();
  }
  
  @SystemApi
  public int getMaxNumberOfSimultaneouslyActiveSims() {
    if (null.$SwitchMap$android$telephony$TelephonyManager$MultiSimVariants[getMultiSimConfiguration().ordinal()] != 3)
      return 1; 
    return 2;
  }
  
  public static TelephonyManager from(Context paramContext) {
    return (TelephonyManager)paramContext.getSystemService("phone");
  }
  
  public TelephonyManager createForSubscriptionId(int paramInt) {
    return new TelephonyManager(this.mContext, paramInt);
  }
  
  public TelephonyManager createForPhoneAccountHandle(PhoneAccountHandle paramPhoneAccountHandle) {
    int i = getSubscriptionId(paramPhoneAccountHandle);
    if (!SubscriptionManager.isValidSubscriptionId(i))
      return null; 
    return new TelephonyManager(this.mContext, i);
  }
  
  public boolean isMultiSimEnabled() {
    int i = getPhoneCount();
    boolean bool = true;
    if (i <= 1)
      bool = false; 
    return bool;
  }
  
  public String getDeviceSoftwareVersion() {
    return getDeviceSoftwareVersion(getSlotIndex());
  }
  
  @SystemApi
  public String getDeviceSoftwareVersion(int paramInt) {
    ITelephony iTelephony = getITelephony();
    if (iTelephony == null)
      return null; 
    try {
      String str = getOpPackageName();
      null = getAttributionTag();
      return iTelephony.getDeviceSoftwareVersionForSlot(paramInt, str, null);
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  @Deprecated
  public String getDeviceId() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return null; 
      String str1 = this.mContext.getOpPackageName();
      Context context = this.mContext;
      String str2 = context.getAttributionTag();
      return iTelephony.getDeviceIdWithFeature(str1, str2);
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  @Deprecated
  public String getDeviceId(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("");
    stringBuilder.append(paramInt);
    SeempLog.record_str(8, stringBuilder.toString());
    try {
      IPhoneSubInfo iPhoneSubInfo = getSubscriberInfoService();
      if (iPhoneSubInfo == null)
        return null; 
      String str1 = this.mContext.getOpPackageName();
      Context context = this.mContext;
      String str2 = context.getAttributionTag();
      return iPhoneSubInfo.getDeviceIdForPhone(paramInt, str1, str2);
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  public String getImei() {
    return getImei(getSlotIndex());
  }
  
  public String getImei(int paramInt) {
    ITelephony iTelephony = getITelephony();
    if (iTelephony == null)
      return null; 
    try {
      return iTelephony.getImeiForSlot(paramInt, getOpPackageName(), getAttributionTag());
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  public String getTypeAllocationCode() {
    return getTypeAllocationCode(getSlotIndex());
  }
  
  public String getTypeAllocationCode(int paramInt) {
    ITelephony iTelephony = getITelephony();
    if (iTelephony == null)
      return null; 
    try {
      return iTelephony.getTypeAllocationCodeForSlot(paramInt);
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  public String getMeid() {
    return getMeid(getSlotIndex());
  }
  
  public String getMeid(int paramInt) {
    ITelephony iTelephony = getITelephony();
    if (iTelephony == null)
      return null; 
    try {
      String str1 = getOpPackageName();
      String str2 = getAttributionTag();
      str1 = iTelephony.getMeidForSlot(paramInt, str1, str2);
      if (TextUtils.isEmpty(str1)) {
        Log.d("TelephonyManager", "getMeid: return null because MEID is not available");
        return null;
      } 
      return str1;
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  public String getManufacturerCode() {
    return getManufacturerCode(getSlotIndex());
  }
  
  public String getManufacturerCode(int paramInt) {
    ITelephony iTelephony = getITelephony();
    if (iTelephony == null)
      return null; 
    try {
      return iTelephony.getManufacturerCodeForSlot(paramInt);
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  public String getNai() {
    return getNaiBySubscriberId(getSubId());
  }
  
  public String getNai(int paramInt) {
    int[] arrayOfInt = SubscriptionManager.getSubId(paramInt);
    if (arrayOfInt == null)
      return null; 
    return getNaiBySubscriberId(arrayOfInt[0]);
  }
  
  private String getNaiBySubscriberId(int paramInt) {
    try {
      IPhoneSubInfo iPhoneSubInfo = getSubscriberInfoService();
      if (iPhoneSubInfo == null)
        return null; 
      String str2 = this.mContext.getOpPackageName();
      Context context = this.mContext;
      String str3 = context.getAttributionTag();
      String str1 = iPhoneSubInfo.getNaiForSubscriber(paramInt, str2, str3);
      if (Log.isLoggable("TelephonyManager", 2)) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Nai = ");
        stringBuilder.append(str1);
        Rlog.v("TelephonyManager", stringBuilder.toString());
      } 
      return str1;
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  @Deprecated
  public CellLocation getCellLocation() {
    SeempLog.record(49);
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null) {
        Rlog.d("TelephonyManager", "getCellLocation returning null because telephony is null");
        return null;
      } 
      String str1 = this.mContext.getOpPackageName();
      Context context = this.mContext;
      String str2 = context.getAttributionTag();
      CellIdentity cellIdentity = iTelephony.getCellLocation(str1, str2);
      CellLocation cellLocation = cellIdentity.asCellLocation();
      if (cellLocation == null || cellLocation.isEmpty()) {
        Rlog.d("TelephonyManager", "getCellLocation returning null because CellLocation is empty or phone type doesn't match CellLocation type");
        return null;
      } 
      return cellLocation;
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getCellLocation returning null due to RemoteException ");
      stringBuilder.append(remoteException);
      Rlog.d("TelephonyManager", stringBuilder.toString());
      return null;
    } 
  }
  
  public void enableLocationUpdates() {
    enableLocationUpdates(getSubId());
  }
  
  public void enableLocationUpdates(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.enableLocationUpdatesForSubscriber(paramInt); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
  }
  
  public void disableLocationUpdates() {
    disableLocationUpdates(getSubId());
  }
  
  public void disableLocationUpdates(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.disableLocationUpdatesForSubscriber(paramInt); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
  }
  
  @Deprecated
  public List<NeighboringCellInfo> getNeighboringCellInfo() {
    SeempLog.record(50);
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return null; 
      String str1 = this.mContext.getOpPackageName();
      Context context = this.mContext;
      String str2 = context.getAttributionTag();
      return iTelephony.getNeighboringCellInfo(str1, str2);
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  @SystemApi
  public int getCurrentPhoneType() {
    return getCurrentPhoneType(getSubId());
  }
  
  @SystemApi
  public int getCurrentPhoneType(int paramInt) {
    if (paramInt == -1) {
      paramInt = 0;
    } else {
      paramInt = SubscriptionManager.getPhoneId(paramInt);
    } 
    return getCurrentPhoneTypeForSlot(paramInt);
  }
  
  public int getCurrentPhoneTypeForSlot(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getActivePhoneTypeForSlot(paramInt); 
      return getPhoneTypeFromProperty(paramInt);
    } catch (RemoteException remoteException) {
      return getPhoneTypeFromProperty(paramInt);
    } catch (NullPointerException nullPointerException) {
      return getPhoneTypeFromProperty(paramInt);
    } 
  }
  
  public int getPhoneType() {
    if (!isVoiceCapable())
      return 0; 
    return getCurrentPhoneType();
  }
  
  private int getPhoneTypeFromProperty() {
    return getPhoneTypeFromProperty(getPhoneId());
  }
  
  private int getPhoneTypeFromProperty(int paramInt) {
    List<Integer> list = TelephonyProperties.current_active_phone();
    Integer integer = getTelephonyProperty(paramInt, list, (Integer)null);
    if (integer != null)
      return integer.intValue(); 
    return getPhoneTypeFromNetworkType(paramInt);
  }
  
  private int getPhoneTypeFromNetworkType() {
    return getPhoneTypeFromNetworkType(getPhoneId());
  }
  
  private int getPhoneTypeFromNetworkType(int paramInt) {
    Integer integer = getTelephonyProperty(paramInt, TelephonyProperties.default_network(), (Integer)null);
    if (integer != null)
      return getPhoneType(integer.intValue()); 
    return 0;
  }
  
  public static int getPhoneType(int paramInt) {
    if (paramInt != 11) {
      if (paramInt != 21)
        switch (paramInt) {
          default:
            return 1;
          case 4:
          case 5:
          case 6:
            return 2;
          case 7:
          case 8:
            break;
        }  
      return 2;
    } 
    if (getLteOnCdmaModeStatic() == 1)
      return 2; 
    return 1;
  }
  
  private static String getProcCmdLine() {
    StringBuilder stringBuilder2;
    String str2, str1 = "";
    FileInputStream fileInputStream1 = null, fileInputStream2 = null;
    FileInputStream fileInputStream3 = fileInputStream2, fileInputStream4 = fileInputStream1;
    try {
      FileInputStream fileInputStream = new FileInputStream();
      fileInputStream3 = fileInputStream2;
      fileInputStream4 = fileInputStream1;
      this("/proc/cmdline");
      fileInputStream2 = fileInputStream;
      fileInputStream3 = fileInputStream2;
      fileInputStream4 = fileInputStream2;
      byte[] arrayOfByte = new byte[2048];
      fileInputStream3 = fileInputStream2;
      fileInputStream4 = fileInputStream2;
      int i = fileInputStream2.read(arrayOfByte);
      str2 = str1;
      if (i > 0) {
        fileInputStream3 = fileInputStream2;
        FileInputStream fileInputStream5 = fileInputStream2;
        String str3 = new String();
        fileInputStream3 = fileInputStream2;
        fileInputStream5 = fileInputStream2;
        this(arrayOfByte, 0, i);
        str2 = str3;
      } 
      String str = str2;
      try {
        fileInputStream2.close();
        str1 = str2;
        str = str1;
      } catch (IOException iOException) {
        str1 = str;
        str = str1;
      } 
    } catch (IOException iOException) {
      StringBuilder stringBuilder3;
      String str = str2;
      StringBuilder stringBuilder4 = new StringBuilder();
      str = str2;
      this();
      str = str2;
      stringBuilder4.append("No /proc/cmdline exception=");
      str = str2;
      stringBuilder4.append(iOException);
      str = str2;
      Rlog.d("TelephonyManager", stringBuilder4.toString());
      str = str1;
      if (str2 != null) {
        str = str1;
        str2.close();
      } else {
        stringBuilder3 = new StringBuilder();
        stringBuilder3.append("/proc/cmdline=");
        stringBuilder3.append(str);
        Rlog.d("TelephonyManager", stringBuilder3.toString());
        return str;
      } 
      stringBuilder2 = stringBuilder3;
    } finally {}
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("/proc/cmdline=");
    stringBuilder1.append((String)stringBuilder2);
    Rlog.d("TelephonyManager", stringBuilder1.toString());
    return (String)stringBuilder2;
  }
  
  @SystemApi
  public static long getMaxNumberVerificationTimeoutMillis() {
    return 60000L;
  }
  
  public static int getLteOnCdmaModeStatic() {
    String str1 = "";
    Optional<Integer> optional = TelephonyProperties.lte_on_cdma_device();
    int i = ((Integer)optional.orElse(Integer.valueOf(-1))).intValue();
    int j = i;
    String str2 = str1;
    int k = j;
    if (j == -1) {
      Matcher matcher = sProductTypePattern.matcher(sKernelCmdLine);
      if (matcher.find()) {
        String str = matcher.group(1);
        if (sLteOnCdmaProductType.equals(str)) {
          k = 1;
        } else {
          k = 0;
        } 
      } else {
        k = 0;
        str2 = str1;
      } 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("getLteOnCdmaMode=");
    stringBuilder.append(k);
    stringBuilder.append(" curVal=");
    stringBuilder.append(i);
    stringBuilder.append(" product_type='");
    stringBuilder.append(str2);
    stringBuilder.append("' lteOnCdmaProductType='");
    stringBuilder.append(sLteOnCdmaProductType);
    stringBuilder.append("'");
    Rlog.d("TelephonyManager", stringBuilder.toString());
    return k;
  }
  
  public String getNetworkOperatorName() {
    return getNetworkOperatorName(getSubId());
  }
  
  public String getNetworkOperatorName(int paramInt) {
    paramInt = SubscriptionManager.getPhoneId(paramInt);
    return getTelephonyProperty(paramInt, TelephonyProperties.operator_alpha(), "");
  }
  
  public String getNetworkOperator() {
    return getNetworkOperatorForPhone(getPhoneId());
  }
  
  public String getNetworkOperator(int paramInt) {
    paramInt = SubscriptionManager.getPhoneId(paramInt);
    return getNetworkOperatorForPhone(paramInt);
  }
  
  public String getNetworkOperatorForPhone(int paramInt) {
    return getTelephonyProperty(paramInt, TelephonyProperties.operator_numeric(), "");
  }
  
  public String getNetworkSpecifier() {
    return String.valueOf(getSubId());
  }
  
  public PersistableBundle getCarrierConfig() {
    Context context = this.mContext;
    CarrierConfigManager carrierConfigManager = (CarrierConfigManager)context.getSystemService(CarrierConfigManager.class);
    return carrierConfigManager.getConfigForSubId(getSubId());
  }
  
  public boolean isNetworkRoaming() {
    return isNetworkRoaming(getSubId());
  }
  
  public boolean isNetworkRoaming(int paramInt) {
    paramInt = SubscriptionManager.getPhoneId(paramInt);
    return ((Boolean)getTelephonyProperty(paramInt, TelephonyProperties.operator_is_roaming(), Boolean.valueOf(false))).booleanValue();
  }
  
  public String getNetworkCountryIso() {
    return getNetworkCountryIso(getSlotIndex());
  }
  
  public String getNetworkCountryIso(int paramInt) {
    // Byte code:
    //   0: iload_1
    //   1: ldc 2147483647
    //   3: if_icmpeq -> 52
    //   6: iload_1
    //   7: invokestatic isValidSlotIndex : (I)Z
    //   10: ifeq -> 16
    //   13: goto -> 52
    //   16: new java/lang/IllegalArgumentException
    //   19: astore_2
    //   20: new java/lang/StringBuilder
    //   23: astore_3
    //   24: aload_3
    //   25: invokespecial <init> : ()V
    //   28: aload_3
    //   29: ldc_w 'invalid slot index '
    //   32: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   35: pop
    //   36: aload_3
    //   37: iload_1
    //   38: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   41: pop
    //   42: aload_2
    //   43: aload_3
    //   44: invokevirtual toString : ()Ljava/lang/String;
    //   47: invokespecial <init> : (Ljava/lang/String;)V
    //   50: aload_2
    //   51: athrow
    //   52: aload_0
    //   53: invokespecial getITelephony : ()Lcom/android/internal/telephony/ITelephony;
    //   56: astore_2
    //   57: aload_2
    //   58: ifnonnull -> 65
    //   61: ldc_w ''
    //   64: areturn
    //   65: aload_2
    //   66: iload_1
    //   67: invokeinterface getNetworkCountryIsoForPhone : (I)Ljava/lang/String;
    //   72: astore_2
    //   73: aload_2
    //   74: areturn
    //   75: astore_2
    //   76: ldc_w ''
    //   79: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2872	-> 0
    //   #2873	-> 6
    //   #2874	-> 16
    //   #2877	-> 52
    //   #2878	-> 57
    //   #2879	-> 65
    //   #2880	-> 75
    //   #2881	-> 76
    // Exception table:
    //   from	to	target	type
    //   6	13	75	android/os/RemoteException
    //   16	52	75	android/os/RemoteException
    //   52	57	75	android/os/RemoteException
    //   65	73	75	android/os/RemoteException
  }
  
  @Deprecated
  public String getNetworkCountryIsoForPhone(int paramInt) {
    return getNetworkCountryIso(paramInt);
  }
  
  public static int[] getAllNetworkTypes() {
    return NETWORK_TYPES;
  }
  
  @Deprecated
  public int getNetworkType() {
    return getNetworkType(getSubId(SubscriptionManager.getActiveDataSubscriptionId()));
  }
  
  public int getNetworkType(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        String str1 = getOpPackageName();
        String str2 = getAttributionTag();
        return iTelephony.getNetworkTypeForSubscriber(paramInt, str1, str2);
      } 
      return 0;
    } catch (RemoteException remoteException) {
      return 0;
    } catch (NullPointerException nullPointerException) {
      return 0;
    } 
  }
  
  public int getDataNetworkType() {
    return getDataNetworkType(getSubId(SubscriptionManager.getActiveDataSubscriptionId()));
  }
  
  public int getDataNetworkType(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        String str1 = getOpPackageName();
        String str2 = getAttributionTag();
        return iTelephony.getDataNetworkTypeForSubscriber(paramInt, str1, str2);
      } 
      return 0;
    } catch (RemoteException remoteException) {
      return 0;
    } catch (NullPointerException nullPointerException) {
      return 0;
    } 
  }
  
  public int getVoiceNetworkType() {
    return getVoiceNetworkType(getSubId());
  }
  
  public int getVoiceNetworkType(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        String str1 = getOpPackageName();
        String str2 = getAttributionTag();
        return iTelephony.getVoiceNetworkTypeForSubscriber(paramInt, str1, str2);
      } 
      return 0;
    } catch (RemoteException remoteException) {
      return 0;
    } catch (NullPointerException nullPointerException) {
      return 0;
    } 
  }
  
  public String getNetworkTypeName() {
    return getNetworkTypeName(getNetworkType());
  }
  
  public static String getNetworkTypeName(int paramInt) {
    switch (paramInt) {
      default:
        return "UNKNOWN";
      case 20:
        return "NR";
      case 19:
        return "LTE_CA";
      case 18:
        return "IWLAN";
      case 17:
        return "TD_SCDMA";
      case 16:
        return "GSM";
      case 15:
        return "HSPA+";
      case 14:
        return "CDMA - eHRPD";
      case 13:
        return "LTE";
      case 12:
        return "CDMA - EvDo rev. B";
      case 11:
        return "iDEN";
      case 10:
        return "HSPA";
      case 9:
        return "HSUPA";
      case 8:
        return "HSDPA";
      case 7:
        return "CDMA - 1xRTT";
      case 6:
        return "CDMA - EvDo rev. A";
      case 5:
        return "CDMA - EvDo rev. 0";
      case 4:
        return "CDMA";
      case 3:
        return "UMTS";
      case 2:
        return "EDGE";
      case 1:
        break;
    } 
    return "GPRS";
  }
  
  public static long getBitMaskForNetworkType(int paramInt) {
    switch (paramInt) {
      default:
        return 0L;
      case 20:
        return 524288L;
      case 19:
        return 262144L;
      case 17:
        return 65536L;
      case 16:
        return 32768L;
      case 15:
        return 16384L;
      case 14:
        return 8192L;
      case 13:
        return 4096L;
      case 12:
        return 2048L;
      case 10:
        return 512L;
      case 9:
        return 256L;
      case 8:
        return 128L;
      case 7:
        return 64L;
      case 6:
        return 32L;
      case 5:
        return 16L;
      case 4:
        return 8L;
      case 3:
        return 4L;
      case 2:
        return 2L;
      case 1:
        break;
    } 
    return 1L;
  }
  
  public boolean hasIccCard() {
    return hasIccCard(getSlotIndex());
  }
  
  public boolean hasIccCard(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return false; 
      return iTelephony.hasIccCardUsingSlotIndex(paramInt);
    } catch (RemoteException remoteException) {
      return false;
    } catch (NullPointerException nullPointerException) {
      return false;
    } 
  }
  
  public int getSimState() {
    int i = getSimStateIncludingLoaded();
    int j = i;
    if (i == 10)
      j = 5; 
    return j;
  }
  
  private int getSimStateIncludingLoaded() {
    int i = getSlotIndex();
    if (i < 0) {
      for (byte b = 0; b < getPhoneCount(); b++) {
        int j = getSimState(b);
        if (j != 1) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("getSimState: default sim:");
          stringBuilder1.append(i);
          stringBuilder1.append(", sim state for slotIndex=");
          stringBuilder1.append(b);
          stringBuilder1.append(" is ");
          stringBuilder1.append(j);
          stringBuilder1.append(", return state as unknown");
          Rlog.d("TelephonyManager", stringBuilder1.toString());
          return 0;
        } 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getSimState: default sim:");
      stringBuilder.append(i);
      stringBuilder.append(", all SIMs absent, return state as absent");
      Rlog.d("TelephonyManager", stringBuilder.toString());
      return 1;
    } 
    return SubscriptionManager.getSimStateForSlotIndex(i);
  }
  
  @SystemApi
  public int getSimCardState() {
    int i = getSimState();
    return getSimCardStateFromSimState(i);
  }
  
  @SystemApi
  public int getSimCardState(int paramInt) {
    paramInt = getSimState(getLogicalSlotIndex(paramInt));
    return getSimCardStateFromSimState(paramInt);
  }
  
  private int getSimCardStateFromSimState(int paramInt) {
    if (paramInt != 0 && paramInt != 1 && paramInt != 8 && paramInt != 9)
      return 11; 
    return paramInt;
  }
  
  private int getLogicalSlotIndex(int paramInt) {
    UiccSlotInfo[] arrayOfUiccSlotInfo = getUiccSlotsInfo();
    if (arrayOfUiccSlotInfo != null && paramInt >= 0 && paramInt < arrayOfUiccSlotInfo.length)
      return arrayOfUiccSlotInfo[paramInt].getLogicalSlotIdx(); 
    return -1;
  }
  
  @SystemApi
  public int getSimApplicationState() {
    int i = getSimStateIncludingLoaded();
    return getSimApplicationStateFromSimState(i);
  }
  
  @SystemApi
  public int getSimApplicationState(int paramInt) {
    paramInt = SubscriptionManager.getSimStateForSlotIndex(getLogicalSlotIndex(paramInt));
    return getSimApplicationStateFromSimState(paramInt);
  }
  
  private int getSimApplicationStateFromSimState(int paramInt) {
    if (paramInt != 0 && paramInt != 1)
      if (paramInt != 5) {
        if (paramInt != 8 && paramInt != 9)
          return paramInt; 
      } else {
        return 6;
      }  
    return 0;
  }
  
  @SystemApi
  public boolean isApplicationOnUicc(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.isApplicationOnUicc(getSubId(), paramInt); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#isApplicationOnUicc", (Throwable)remoteException);
    } 
    return false;
  }
  
  public int getSimState(int paramInt) {
    int i = SubscriptionManager.getSimStateForSlotIndex(paramInt);
    paramInt = i;
    if (i == 10)
      paramInt = 5; 
    return paramInt;
  }
  
  public String getSimOperator() {
    return getSimOperatorNumeric();
  }
  
  public String getSimOperator(int paramInt) {
    return getSimOperatorNumeric(paramInt);
  }
  
  public String getSimOperatorNumeric() {
    int i = this.mSubId;
    int j = i;
    if (!SubscriptionManager.isUsableSubIdValue(i)) {
      i = SubscriptionManager.getDefaultDataSubscriptionId();
      j = i;
      if (!SubscriptionManager.isUsableSubIdValue(i)) {
        i = SubscriptionManager.getDefaultSmsSubscriptionId();
        j = i;
        if (!SubscriptionManager.isUsableSubIdValue(i)) {
          i = SubscriptionManager.getDefaultVoiceSubscriptionId();
          j = i;
          if (!SubscriptionManager.isUsableSubIdValue(i))
            j = SubscriptionManager.getDefaultSubscriptionId(); 
        } 
      } 
    } 
    return getSimOperatorNumeric(j);
  }
  
  public String getSimOperatorNumeric(int paramInt) {
    paramInt = SubscriptionManager.getPhoneId(paramInt);
    return getSimOperatorNumericForPhone(paramInt);
  }
  
  public String getSimOperatorNumericForPhone(int paramInt) {
    return getTelephonyProperty(paramInt, TelephonyProperties.icc_operator_numeric(), "");
  }
  
  public String getSimOperatorName() {
    return getSimOperatorNameForPhone(getPhoneId());
  }
  
  public String getSimOperatorName(int paramInt) {
    paramInt = SubscriptionManager.getPhoneId(paramInt);
    return getSimOperatorNameForPhone(paramInt);
  }
  
  public String getSimOperatorNameForPhone(int paramInt) {
    return getTelephonyProperty(paramInt, TelephonyProperties.icc_operator_alpha(), "");
  }
  
  public String getSimCountryIso() {
    return getSimCountryIsoForPhone(getPhoneId());
  }
  
  public static String getSimCountryIso(int paramInt) {
    paramInt = SubscriptionManager.getPhoneId(paramInt);
    return getSimCountryIsoForPhone(paramInt);
  }
  
  public static String getSimCountryIsoForPhone(int paramInt) {
    return getTelephonyProperty(paramInt, TelephonyProperties.icc_operator_iso_country(), "");
  }
  
  public String getSimSerialNumber() {
    return getSimSerialNumber(getSubId());
  }
  
  public String getSimSerialNumber(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("");
    stringBuilder.append(paramInt);
    SeempLog.record_str(388, stringBuilder.toString());
    try {
      IPhoneSubInfo iPhoneSubInfo = getSubscriberInfoService();
      if (iPhoneSubInfo == null)
        return null; 
      String str1 = this.mContext.getOpPackageName();
      Context context = this.mContext;
      String str2 = context.getAttributionTag();
      return iPhoneSubInfo.getIccSerialNumberForSubscriber(paramInt, str1, str2);
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  @SystemApi
  public boolean isLteCdmaEvdoGsmWcdmaEnabled() {
    int i = getLteOnCdmaMode(getSubId());
    boolean bool = true;
    if (i != 1)
      bool = false; 
    return bool;
  }
  
  public int getLteOnCdmaMode(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return -1; 
      String str1 = getOpPackageName();
      String str2 = getAttributionTag();
      return iTelephony.getLteOnCdmaModeForSubscriber(paramInt, str1, str2);
    } catch (RemoteException remoteException) {
      return -1;
    } catch (NullPointerException nullPointerException) {
      return -1;
    } 
  }
  
  public int getCardIdForDefaultEuicc() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return -2; 
      return iTelephony.getCardIdForDefaultEuicc(this.mSubId, this.mContext.getOpPackageName());
    } catch (RemoteException remoteException) {
      return -2;
    } 
  }
  
  public List<UiccCardInfo> getUiccCardsInfo() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null) {
        Log.e("TelephonyManager", "Error in getUiccCardsInfo: unable to connect to Telephony service.");
        return new ArrayList<>();
      } 
      return iTelephony.getUiccCardsInfo(this.mContext.getOpPackageName());
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error in getUiccCardsInfo: ");
      stringBuilder.append(remoteException);
      Log.e("TelephonyManager", stringBuilder.toString());
      return new ArrayList<>();
    } 
  }
  
  @SystemApi
  public UiccSlotInfo[] getUiccSlotsInfo() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return null; 
      return iTelephony.getUiccSlotsInfo();
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public void refreshUiccProfile() {
    try {
      ITelephony iTelephony = getITelephony();
      iTelephony.refreshUiccProfile(this.mSubId);
    } catch (RemoteException remoteException) {
      Rlog.w("TelephonyManager", "RemoteException", (Throwable)remoteException);
    } 
  }
  
  @SystemApi
  public boolean switchSlots(int[] paramArrayOfint) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return false; 
      return iTelephony.switchSlots(paramArrayOfint);
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  @SystemApi
  public Map<Integer, Integer> getLogicalToPhysicalSlotMapping() {
    HashMap<Object, Object> hashMap = new HashMap<>();
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int[] arrayOfInt = iTelephony.getSlotsMapping();
        for (byte b = 0; b < arrayOfInt.length; b++)
          hashMap.put(Integer.valueOf(b), Integer.valueOf(arrayOfInt[b])); 
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "getSlotsMapping RemoteException", (Throwable)remoteException);
    } 
    return (Map)hashMap;
  }
  
  public String getSubscriberId() {
    return getSubscriberId(getSubId());
  }
  
  public String getSubscriberId(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("");
    stringBuilder.append(paramInt);
    SeempLog.record_str(389, stringBuilder.toString());
    try {
      IPhoneSubInfo iPhoneSubInfo = getSubscriberInfoService();
      if (iPhoneSubInfo == null)
        return null; 
      String str1 = this.mContext.getOpPackageName();
      Context context = this.mContext;
      String str2 = context.getAttributionTag();
      return iPhoneSubInfo.getSubscriberIdForSubscriber(paramInt, str1, str2);
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  @SystemApi
  public ImsiEncryptionInfo getCarrierInfoForImsiEncryption(int paramInt) {
    try {
      IPhoneSubInfo iPhoneSubInfo = getSubscriberInfoService();
      if (iPhoneSubInfo == null) {
        Rlog.e("TelephonyManager", "IMSI error: Subscriber Info is null");
        return null;
      } 
      int i = getSubId(SubscriptionManager.getDefaultDataSubscriptionId());
      if (paramInt == 1 || paramInt == 2) {
        Context context = this.mContext;
        String str = context.getOpPackageName();
        ImsiEncryptionInfo imsiEncryptionInfo = iPhoneSubInfo.getCarrierInfoForImsiEncryption(i, paramInt, str);
        if (imsiEncryptionInfo != null || !isImsiEncryptionRequired(i, paramInt))
          return imsiEncryptionInfo; 
        Rlog.e("TelephonyManager", "IMSI error: key is required but not found");
        IllegalArgumentException illegalArgumentException1 = new IllegalArgumentException();
        this("IMSI error: key is required but not found");
        throw illegalArgumentException1;
      } 
      IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
      this("IMSI error: Invalid key type");
      throw illegalArgumentException;
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getCarrierInfoForImsiEncryption RemoteException");
      stringBuilder.append(remoteException);
      Rlog.e("TelephonyManager", stringBuilder.toString());
    } catch (NullPointerException nullPointerException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getCarrierInfoForImsiEncryption NullPointerException");
      stringBuilder.append(nullPointerException);
      Rlog.e("TelephonyManager", stringBuilder.toString());
    } 
    return null;
  }
  
  @SystemApi
  public void resetCarrierKeysForImsiEncryption() {
    try {
      RuntimeException runtimeException;
      IPhoneSubInfo iPhoneSubInfo = getSubscriberInfoService();
      if (iPhoneSubInfo == null) {
        Rlog.e("TelephonyManager", "IMSI error: Subscriber Info is null");
        if (isSystemProcess())
          return; 
        runtimeException = new RuntimeException();
        this("IMSI error: Subscriber Info is null");
        throw runtimeException;
      } 
      int i = getSubId(SubscriptionManager.getDefaultDataSubscriptionId());
      runtimeException.resetCarrierKeysForImsiEncryption(i, this.mContext.getOpPackageName());
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getCarrierInfoForImsiEncryption RemoteException");
      stringBuilder.append(remoteException);
      Rlog.e("TelephonyManager", stringBuilder.toString());
      if (!isSystemProcess())
        remoteException.rethrowAsRuntimeException(); 
    } 
  }
  
  private static boolean isKeyEnabled(int paramInt1, int paramInt2) {
    boolean bool = true;
    if ((paramInt1 >> paramInt2 - 1 & 0x1) != 1)
      bool = false; 
    return bool;
  }
  
  private boolean isImsiEncryptionRequired(int paramInt1, int paramInt2) {
    Context context = this.mContext;
    CarrierConfigManager carrierConfigManager = (CarrierConfigManager)context.getSystemService("carrier_config");
    if (carrierConfigManager == null)
      return false; 
    PersistableBundle persistableBundle = carrierConfigManager.getConfigForSubId(paramInt1);
    if (persistableBundle == null)
      return false; 
    paramInt1 = persistableBundle.getInt("imsi_key_availability_int");
    return isKeyEnabled(paramInt1, paramInt2);
  }
  
  public void setCarrierInfoForImsiEncryption(ImsiEncryptionInfo paramImsiEncryptionInfo) {
    try {
      IPhoneSubInfo iPhoneSubInfo = getSubscriberInfoService();
      if (iPhoneSubInfo == null)
        return; 
      iPhoneSubInfo.setCarrierInfoForImsiEncryption(this.mSubId, this.mContext.getOpPackageName(), paramImsiEncryptionInfo);
      return;
    } catch (NullPointerException nullPointerException) {
      return;
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "setCarrierInfoForImsiEncryption RemoteException", (Throwable)remoteException);
      return;
    } 
  }
  
  public String getGroupIdLevel1() {
    try {
      IPhoneSubInfo iPhoneSubInfo = getSubscriberInfoService();
      if (iPhoneSubInfo == null)
        return null; 
      int i = getSubId();
      null = this.mContext.getOpPackageName();
      Context context = this.mContext;
      String str = context.getAttributionTag();
      return iPhoneSubInfo.getGroupIdLevel1ForSubscriber(i, null, str);
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  public String getGroupIdLevel1(int paramInt) {
    try {
      IPhoneSubInfo iPhoneSubInfo = getSubscriberInfoService();
      if (iPhoneSubInfo == null)
        return null; 
      String str1 = this.mContext.getOpPackageName();
      Context context = this.mContext;
      String str2 = context.getAttributionTag();
      return iPhoneSubInfo.getGroupIdLevel1ForSubscriber(paramInt, str1, str2);
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  public String getLine1Number() {
    return getLine1Number(getSubId());
  }
  
  public String getLine1Number(int paramInt) {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("");
    stringBuilder1.append(paramInt);
    SeempLog.record_str(9, stringBuilder1.toString());
    NullPointerException nullPointerException2 = null;
    StringBuilder stringBuilder2 = null;
    try {
      ITelephony iTelephony = getITelephony();
      stringBuilder1 = stringBuilder2;
      if (iTelephony != null) {
        String str1 = this.mContext.getOpPackageName();
        Context context = this.mContext;
        String str2 = context.getAttributionTag();
        str1 = iTelephony.getLine1NumberForDisplay(paramInt, str1, str2);
      } 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = stringBuilder2;
    } catch (NullPointerException nullPointerException1) {
      nullPointerException1 = nullPointerException2;
    } 
    if (nullPointerException1 != null)
      return (String)nullPointerException1; 
    try {
      IPhoneSubInfo iPhoneSubInfo = getSubscriberInfoService();
      if (iPhoneSubInfo == null)
        return null; 
      null = this.mContext.getOpPackageName();
      Context context = this.mContext;
      String str = context.getAttributionTag();
      return iPhoneSubInfo.getLine1NumberForSubscriber(paramInt, null, str);
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  public boolean setLine1NumberForDisplay(String paramString1, String paramString2) {
    return setLine1NumberForDisplay(getSubId(), paramString1, paramString2);
  }
  
  public boolean setLine1NumberForDisplay(int paramInt, String paramString1, String paramString2) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.setLine1NumberForDisplayForSubscriber(paramInt, paramString1, paramString2); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return false;
  }
  
  public String getLine1AlphaTag() {
    return getLine1AlphaTag(getSubId());
  }
  
  public String getLine1AlphaTag(int paramInt) {
    NullPointerException nullPointerException1 = null;
    String str = null;
    try {
      ITelephony iTelephony = getITelephony();
      String str1 = str;
      if (iTelephony != null) {
        str1 = getOpPackageName();
        String str2 = getAttributionTag();
        str1 = iTelephony.getLine1AlphaTagForDisplay(paramInt, str1, str2);
      } 
    } catch (RemoteException remoteException) {
      String str1 = str;
    } catch (NullPointerException nullPointerException2) {
      nullPointerException2 = nullPointerException1;
    } 
    if (nullPointerException2 != null)
      return (String)nullPointerException2; 
    try {
      IPhoneSubInfo iPhoneSubInfo = getSubscriberInfoService();
      if (iPhoneSubInfo == null)
        return null; 
      null = getOpPackageName();
      String str1 = getAttributionTag();
      return iPhoneSubInfo.getLine1AlphaTagForSubscriber(paramInt, null, str1);
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  @Deprecated
  public String[] getMergedSubscriberIds() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int i = getSubId();
        String str1 = getOpPackageName();
        String str2 = getAttributionTag();
        return iTelephony.getMergedSubscriberIds(i, str1, str2);
      } 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return null;
  }
  
  @SystemApi
  public String[] getMergedImsisFromGroup() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getMergedImsisFromGroup(getSubId(), getOpPackageName()); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return new String[0];
  }
  
  public String getMsisdn() {
    return getMsisdn(getSubId());
  }
  
  public String getMsisdn(int paramInt) {
    try {
      IPhoneSubInfo iPhoneSubInfo = getSubscriberInfoService();
      if (iPhoneSubInfo == null)
        return null; 
      return iPhoneSubInfo.getMsisdnForSubscriber(paramInt, getOpPackageName(), getAttributionTag());
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  public String getVoiceMailNumber() {
    return getVoiceMailNumber(getSubId());
  }
  
  public String getVoiceMailNumber(int paramInt) {
    try {
      IPhoneSubInfo iPhoneSubInfo = getSubscriberInfoService();
      if (iPhoneSubInfo == null)
        return null; 
      null = getOpPackageName();
      String str = getAttributionTag();
      return iPhoneSubInfo.getVoiceMailNumberForSubscriber(paramInt, null, str);
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  public boolean setVoiceMailNumber(String paramString1, String paramString2) {
    return setVoiceMailNumber(getSubId(), paramString1, paramString2);
  }
  
  public boolean setVoiceMailNumber(int paramInt, String paramString1, String paramString2) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.setVoiceMailNumber(paramInt, paramString1, paramString2); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return false;
  }
  
  @SystemApi
  public void setVisualVoicemailEnabled(PhoneAccountHandle paramPhoneAccountHandle, boolean paramBoolean) {}
  
  @SystemApi
  public boolean isVisualVoicemailEnabled(PhoneAccountHandle paramPhoneAccountHandle) {
    return false;
  }
  
  @SystemApi
  public Bundle getVisualVoicemailSettings() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        Context context = this.mContext;
        return iTelephony.getVisualVoicemailSettings(context.getOpPackageName(), this.mSubId);
      } 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return null;
  }
  
  public String getVisualVoicemailPackageName() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        String str1 = this.mContext.getOpPackageName();
        String str2 = getAttributionTag();
        int i = getSubId();
        return iTelephony.getVisualVoicemailPackageName(str1, str2, i);
      } 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return null;
  }
  
  public void setVisualVoicemailSmsFilterSettings(VisualVoicemailSmsFilterSettings paramVisualVoicemailSmsFilterSettings) {
    if (paramVisualVoicemailSmsFilterSettings == null) {
      disableVisualVoicemailSmsFilter(this.mSubId);
    } else {
      enableVisualVoicemailSmsFilter(this.mSubId, paramVisualVoicemailSmsFilterSettings);
    } 
  }
  
  public void sendVisualVoicemailSms(String paramString1, int paramInt, String paramString2, PendingIntent paramPendingIntent) {
    sendVisualVoicemailSmsForSubscriber(this.mSubId, paramString1, paramInt, paramString2, paramPendingIntent);
  }
  
  public void enableVisualVoicemailSmsFilter(int paramInt, VisualVoicemailSmsFilterSettings paramVisualVoicemailSmsFilterSettings) {
    if (paramVisualVoicemailSmsFilterSettings != null) {
      try {
        ITelephony iTelephony = getITelephony();
        if (iTelephony != null)
          iTelephony.enableVisualVoicemailSmsFilter(this.mContext.getOpPackageName(), paramInt, paramVisualVoicemailSmsFilterSettings); 
      } catch (RemoteException remoteException) {
      
      } catch (NullPointerException nullPointerException) {}
      return;
    } 
    throw new IllegalArgumentException("Settings cannot be null");
  }
  
  public void disableVisualVoicemailSmsFilter(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.disableVisualVoicemailSmsFilter(this.mContext.getOpPackageName(), paramInt); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
  }
  
  public VisualVoicemailSmsFilterSettings getVisualVoicemailSmsFilterSettings(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        Context context = this.mContext;
        return iTelephony.getVisualVoicemailSmsFilterSettings(context.getOpPackageName(), paramInt);
      } 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return null;
  }
  
  public VisualVoicemailSmsFilterSettings getActiveVisualVoicemailSmsFilterSettings(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getActiveVisualVoicemailSmsFilterSettings(paramInt); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return null;
  }
  
  public void sendVisualVoicemailSmsForSubscriber(int paramInt1, String paramString1, int paramInt2, String paramString2, PendingIntent paramPendingIntent) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        Context context = this.mContext;
        String str2 = context.getOpPackageName(), str1 = this.mContext.getAttributionTag();
        iTelephony.sendVisualVoicemailSmsForSubscriber(str2, str1, paramInt1, paramString1, paramInt2, paramString2, paramPendingIntent);
      } 
    } catch (RemoteException remoteException) {}
  }
  
  @SystemApi
  public void setVoiceActivationState(int paramInt) {
    setVoiceActivationState(getSubId(), paramInt);
  }
  
  public void setVoiceActivationState(int paramInt1, int paramInt2) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.setVoiceActivationState(paramInt1, paramInt2); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
  }
  
  @SystemApi
  public void setDataActivationState(int paramInt) {
    setDataActivationState(getSubId(), paramInt);
  }
  
  public void setDataActivationState(int paramInt1, int paramInt2) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.setDataActivationState(paramInt1, paramInt2); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
  }
  
  @SystemApi
  public int getVoiceActivationState() {
    return getVoiceActivationState(getSubId());
  }
  
  public int getVoiceActivationState(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getVoiceActivationState(paramInt, getOpPackageName()); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return 0;
  }
  
  @SystemApi
  public int getDataActivationState() {
    return getDataActivationState(getSubId());
  }
  
  public int getDataActivationState(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getDataActivationState(paramInt, getOpPackageName()); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return 0;
  }
  
  public int getVoiceMessageCount() {
    return getVoiceMessageCount(getSubId());
  }
  
  public int getVoiceMessageCount(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return 0; 
      String str1 = getOpPackageName();
      String str2 = getAttributionTag();
      return iTelephony.getVoiceMessageCountForSubscriber(paramInt, str1, str2);
    } catch (RemoteException remoteException) {
      return 0;
    } catch (NullPointerException nullPointerException) {
      return 0;
    } 
  }
  
  public String getVoiceMailAlphaTag() {
    return getVoiceMailAlphaTag(getSubId());
  }
  
  public String getVoiceMailAlphaTag(int paramInt) {
    try {
      IPhoneSubInfo iPhoneSubInfo = getSubscriberInfoService();
      if (iPhoneSubInfo == null)
        return null; 
      String str = getOpPackageName();
      null = getAttributionTag();
      return iPhoneSubInfo.getVoiceMailAlphaTagForSubscriber(paramInt, str, null);
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  public void sendDialerSpecialCode(String paramString) {
    try {
      RuntimeException runtimeException;
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null) {
        if (isSystemProcess())
          return; 
        runtimeException = new RuntimeException();
        this("Telephony service unavailable");
        throw runtimeException;
      } 
      iTelephony.sendDialerSpecialCode(this.mContext.getOpPackageName(), (String)runtimeException);
    } catch (RemoteException remoteException) {
      if (!isSystemProcess())
        remoteException.rethrowAsRuntimeException(); 
    } 
  }
  
  public String getIsimImpi() {
    try {
      IPhoneSubInfo iPhoneSubInfo = getSubscriberInfoService();
      if (iPhoneSubInfo == null)
        return null; 
      return iPhoneSubInfo.getIsimImpi(getSubId());
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  @SystemApi
  public String getIsimDomain() {
    try {
      IPhoneSubInfo iPhoneSubInfo = getSubscriberInfoService();
      if (iPhoneSubInfo == null)
        return null; 
      return iPhoneSubInfo.getIsimDomain(getSubId());
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  public String[] getIsimImpu() {
    try {
      IPhoneSubInfo iPhoneSubInfo = getSubscriberInfoService();
      if (iPhoneSubInfo == null)
        return null; 
      return iPhoneSubInfo.getIsimImpu(getSubId());
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  public int getCallState() {
    Context context = this.mContext;
    if (context != null) {
      TelecomManager telecomManager = (TelecomManager)context.getSystemService(TelecomManager.class);
      if (telecomManager != null)
        return telecomManager.getCallState(); 
    } 
    return 0;
  }
  
  public int getCallState(int paramInt) {
    paramInt = SubscriptionManager.getPhoneId(paramInt);
    return getCallStateForSlot(paramInt);
  }
  
  private IPhoneSubInfo getSubscriberInfo() {
    return getSubscriberInfoService();
  }
  
  public int getCallStateForSlot(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return 0; 
      return iTelephony.getCallStateForSlot(paramInt);
    } catch (RemoteException remoteException) {
      return 0;
    } catch (NullPointerException nullPointerException) {
      return 0;
    } 
  }
  
  public int getDataActivity() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return 0; 
      null = getSubId(SubscriptionManager.getActiveDataSubscriptionId());
      return iTelephony.getDataActivityForSubId(null);
    } catch (RemoteException remoteException) {
      return 0;
    } catch (NullPointerException nullPointerException) {
      return 0;
    } 
  }
  
  public int getDataState() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return 0; 
      int i = getSubId(SubscriptionManager.getActiveDataSubscriptionId());
      i = iTelephony.getDataStateForSubId(i);
      if (i == 4) {
        boolean bool = Compatibility.isChangeEnabled(148534348L);
        if (!bool)
          return 2; 
      } 
      return i;
    } catch (RemoteException remoteException) {
      return 0;
    } catch (NullPointerException nullPointerException) {
      return 0;
    } 
  }
  
  public static String dataStateToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt != 4) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("UNKNOWN(");
              stringBuilder.append(paramInt);
              stringBuilder.append(")");
              return stringBuilder.toString();
            } 
            return "DISCONNECTING";
          } 
          return "SUSPENDED";
        } 
        return "CONNECTED";
      } 
      return "CONNECTING";
    } 
    return "DISCONNECTED";
  }
  
  private ITelephony getITelephony() {
    IBinder iBinder = TelephonyFrameworkInitializer.getTelephonyServiceManager().getTelephonyServiceRegisterer().get();
    return ITelephony.Stub.asInterface(iBinder);
  }
  
  private IOns getIOns() {
    TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
    TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getOpportunisticNetworkServiceRegisterer();
    IBinder iBinder = serviceRegisterer.get();
    return IOns.Stub.asInterface(iBinder);
  }
  
  public void listen(PhoneStateListener paramPhoneStateListener, int paramInt) {
    boolean bool;
    if (this.mContext == null)
      return; 
    if (getITelephony() != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Context context = this.mContext;
    TelephonyRegistryManager telephonyRegistryManager = (TelephonyRegistryManager)context.getSystemService("telephony_registry");
    if (telephonyRegistryManager != null) {
      telephonyRegistryManager.listenForSubscriber(this.mSubId, getOpPackageName(), getAttributionTag(), paramPhoneStateListener, paramInt, bool);
    } else {
      Rlog.w("TelephonyManager", "telephony registry not ready.");
    } 
  }
  
  public CdmaEriInformation getCdmaEriInformation() {
    return new CdmaEriInformation(getCdmaEriIconIndex(getSubId()), getCdmaEriIconMode(getSubId()));
  }
  
  public int getCdmaEriIconIndex(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return -1; 
      String str1 = getOpPackageName();
      String str2 = getAttributionTag();
      return iTelephony.getCdmaEriIconIndexForSubscriber(paramInt, str1, str2);
    } catch (RemoteException remoteException) {
      return -1;
    } catch (NullPointerException nullPointerException) {
      return -1;
    } 
  }
  
  public int getCdmaEriIconMode(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return -1; 
      String str1 = getOpPackageName();
      String str2 = getAttributionTag();
      return iTelephony.getCdmaEriIconModeForSubscriber(paramInt, str1, str2);
    } catch (RemoteException remoteException) {
      return -1;
    } catch (NullPointerException nullPointerException) {
      return -1;
    } 
  }
  
  public String getCdmaEriText() {
    return getCdmaEriText(getSubId());
  }
  
  public String getCdmaEriText(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return null; 
      String str1 = getOpPackageName();
      String str2 = getAttributionTag();
      return iTelephony.getCdmaEriTextForSubscriber(paramInt, str1, str2);
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  public boolean isVoiceCapable() {
    Context context = this.mContext;
    if (context == null)
      return true; 
    return context.getResources().getBoolean(17891586);
  }
  
  public boolean isSmsCapable() {
    Context context = this.mContext;
    if (context == null)
      return true; 
    return context.getResources().getBoolean(17891535);
  }
  
  public List<CellInfo> getAllCellInfo() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return null; 
      return iTelephony.getAllCellInfo(getOpPackageName(), getAttributionTag());
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return null;
  }
  
  class CellInfoCallback {
    public static final int ERROR_MODEM_ERROR = 2;
    
    public static final int ERROR_TIMEOUT = 1;
    
    public abstract void onCellInfo(List<CellInfo> param1List);
    
    public void onError(int param1Int, Throwable param1Throwable) {
      onCellInfo(new ArrayList<>());
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface CellInfoCallbackError {}
  }
  
  public void requestCellInfoUpdate(Executor paramExecutor, CellInfoCallback paramCellInfoCallback) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return; 
      int i = getSubId();
      Object object = new Object();
      super(this, paramExecutor, paramCellInfoCallback);
      String str1 = getOpPackageName(), str2 = getAttributionTag();
      iTelephony.requestCellInfoUpdate(i, (ICellInfoCallback)object, str1, str2);
    } catch (RemoteException remoteException) {}
  }
  
  @SystemApi
  public void requestCellInfoUpdate(WorkSource paramWorkSource, Executor paramExecutor, CellInfoCallback paramCellInfoCallback) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return; 
      int i = getSubId();
      Object object = new Object();
      super(this, paramExecutor, paramCellInfoCallback);
      String str1 = getOpPackageName(), str2 = getAttributionTag();
      iTelephony.requestCellInfoUpdateWithWorkSource(i, (ICellInfoCallback)object, str1, str2, paramWorkSource);
    } catch (RemoteException remoteException) {
    
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Exception: ");
      stringBuilder.append(exception);
      Log.e("TelephonyManager", stringBuilder.toString());
    } 
  }
  
  private static Throwable createThrowableByClassName(String paramString1, String paramString2) {
    if (paramString1 == null)
      return null; 
    try {
      Class<?> clazz = Class.forName(paramString1);
      return clazz.getConstructor(new Class[] { String.class }).newInstance(new Object[] { paramString2 });
    } catch (ReflectiveOperationException|ClassCastException reflectiveOperationException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(": ");
      stringBuilder.append(paramString2);
      return new RuntimeException(stringBuilder.toString());
    } 
  }
  
  public void setCellInfoListRate(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.setCellInfoListRate(paramInt); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
  }
  
  public String getMmsUserAgent() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getMmsUserAgent(getSubId()); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return null;
  }
  
  public String getMmsUAProfUrl() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getMmsUAProfUrl(getSubId()); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return null;
  }
  
  @Deprecated
  public IccOpenLogicalChannelResponse iccOpenLogicalChannel(String paramString) {
    return iccOpenLogicalChannel(getSubId(), paramString, -1);
  }
  
  @SystemApi
  public IccOpenLogicalChannelResponse iccOpenLogicalChannelBySlot(int paramInt1, String paramString, int paramInt2) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.iccOpenLogicalChannelBySlot(paramInt1, getOpPackageName(), paramString, paramInt2); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return null;
  }
  
  @Deprecated
  public IccOpenLogicalChannelResponse iccOpenLogicalChannel(String paramString, int paramInt) {
    return iccOpenLogicalChannel(getSubId(), paramString, paramInt);
  }
  
  @Deprecated
  public IccOpenLogicalChannelResponse iccOpenLogicalChannel(int paramInt1, String paramString, int paramInt2) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.iccOpenLogicalChannel(paramInt1, getOpPackageName(), paramString, paramInt2); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return null;
  }
  
  @SystemApi
  @Deprecated
  public boolean iccCloseLogicalChannelBySlot(int paramInt1, int paramInt2) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.iccCloseLogicalChannelBySlot(paramInt1, paramInt2); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return false;
  }
  
  @Deprecated
  public boolean iccCloseLogicalChannel(int paramInt) {
    return iccCloseLogicalChannel(getSubId(), paramInt);
  }
  
  @Deprecated
  public boolean iccCloseLogicalChannel(int paramInt1, int paramInt2) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.iccCloseLogicalChannel(paramInt1, paramInt2); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return false;
  }
  
  @SystemApi
  @Deprecated
  public String iccTransmitApduLogicalChannelBySlot(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, String paramString) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.iccTransmitApduLogicalChannelBySlot(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramString); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return null;
  }
  
  @Deprecated
  public String iccTransmitApduLogicalChannel(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, String paramString) {
    return iccTransmitApduLogicalChannel(getSubId(), paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramString);
  }
  
  @Deprecated
  public String iccTransmitApduLogicalChannel(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, String paramString) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.iccTransmitApduLogicalChannel(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramString); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return "";
  }
  
  @SystemApi
  @Deprecated
  public String iccTransmitApduBasicChannelBySlot(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, String paramString) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.iccTransmitApduBasicChannelBySlot(paramInt1, getOpPackageName(), paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramString); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return null;
  }
  
  @Deprecated
  public String iccTransmitApduBasicChannel(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, String paramString) {
    return iccTransmitApduBasicChannel(getSubId(), paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramString);
  }
  
  @Deprecated
  public String iccTransmitApduBasicChannel(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, String paramString) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.iccTransmitApduBasicChannel(paramInt1, getOpPackageName(), paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramString); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return "";
  }
  
  @Deprecated
  public byte[] iccExchangeSimIO(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, String paramString) {
    return iccExchangeSimIO(getSubId(), paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramString);
  }
  
  @Deprecated
  public byte[] iccExchangeSimIO(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, String paramString) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.iccExchangeSimIO(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramString); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return null;
  }
  
  @Deprecated
  public String sendEnvelopeWithStatus(String paramString) {
    return sendEnvelopeWithStatus(getSubId(), paramString);
  }
  
  @Deprecated
  public String sendEnvelopeWithStatus(int paramInt, String paramString) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.sendEnvelopeWithStatus(paramInt, paramString); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return "";
  }
  
  public String nvReadItem(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.nvReadItem(paramInt); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "nvReadItem RemoteException", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Rlog.e("TelephonyManager", "nvReadItem NPE", nullPointerException);
    } 
    return "";
  }
  
  public boolean nvWriteItem(int paramInt, String paramString) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.nvWriteItem(paramInt, paramString); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "nvWriteItem RemoteException", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Rlog.e("TelephonyManager", "nvWriteItem NPE", nullPointerException);
    } 
    return false;
  }
  
  public boolean nvWriteCdmaPrl(byte[] paramArrayOfbyte) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.nvWriteCdmaPrl(paramArrayOfbyte); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "nvWriteCdmaPrl RemoteException", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Rlog.e("TelephonyManager", "nvWriteCdmaPrl NPE", nullPointerException);
    } 
    return false;
  }
  
  public boolean nvResetConfig(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        if (paramInt == 1)
          return iTelephony.rebootModem(getSlotIndex()); 
        if (paramInt == 3)
          return iTelephony.resetModemConfig(getSlotIndex()); 
        Rlog.e("TelephonyManager", "nvResetConfig unsupported reset type");
      } 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "nvResetConfig RemoteException", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Rlog.e("TelephonyManager", "nvResetConfig NPE", nullPointerException);
    } 
    return false;
  }
  
  @SystemApi
  public boolean resetRadioConfig() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.resetModemConfig(getSlotIndex()); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "resetRadioConfig RemoteException", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Rlog.e("TelephonyManager", "resetRadioConfig NPE", nullPointerException);
    } 
    return false;
  }
  
  @SystemApi
  public boolean rebootRadio() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.rebootModem(getSlotIndex()); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "rebootRadio RemoteException", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Rlog.e("TelephonyManager", "rebootRadio NPE", nullPointerException);
    } 
    return false;
  }
  
  public int getSubscriptionId() {
    return getSubId();
  }
  
  private int getSubId() {
    if (SubscriptionManager.isUsableSubIdValue(this.mSubId))
      return this.mSubId; 
    try {
      if (this.mContext != null) {
        int i = Settings.Global.getInt(this.mContext.getContentResolver(), "multi_sim_data_call", this.mSubId);
        boolean bool = SubscriptionManager.isUsableSubIdValue(i);
        if (bool)
          return i; 
      } 
    } catch (Exception exception) {}
    return SubscriptionManager.getDefaultSubscriptionId();
  }
  
  private int getSubId(int paramInt) {
    if (SubscriptionManager.isUsableSubIdValue(this.mSubId))
      return this.mSubId; 
    return paramInt;
  }
  
  private int getPhoneId() {
    return SubscriptionManager.getPhoneId(getSubId());
  }
  
  private int getPhoneId(int paramInt) {
    return SubscriptionManager.getPhoneId(getSubId(paramInt));
  }
  
  public int getSlotIndex() {
    int i = SubscriptionManager.getSlotIndex(getSubId());
    int j = i;
    if (i == -1)
      j = Integer.MAX_VALUE; 
    return j;
  }
  
  @SystemApi
  public void requestNumberVerification(PhoneNumberRange paramPhoneNumberRange, long paramLong, Executor paramExecutor, NumberVerificationCallback paramNumberVerificationCallback) {
    if (paramExecutor != null) {
      if (paramNumberVerificationCallback != null) {
        Object object = new Object(this, paramExecutor, paramNumberVerificationCallback);
        try {
          ITelephony iTelephony = getITelephony();
          if (iTelephony != null) {
            String str = getOpPackageName();
            iTelephony.requestNumberVerification(paramPhoneNumberRange, paramLong, (INumberVerificationCallback)object, str);
          } 
        } catch (RemoteException remoteException) {
          Rlog.e("TelephonyManager", "requestNumberVerification RemoteException", (Throwable)remoteException);
          paramExecutor.execute(new _$$Lambda$TelephonyManager$4i1RRVjnCzfQvX2hIGG9K8g4DaY(paramNumberVerificationCallback));
        } 
        return;
      } 
      throw new NullPointerException("Callback must be non-null");
    } 
    throw new NullPointerException("Executor must be non-null");
  }
  
  private static <T> List<T> updateTelephonyProperty(List<T> paramList, int paramInt, T paramT) {
    paramList = new ArrayList<>(paramList);
    for (; paramList.size() <= paramInt; paramList.add(null));
    paramList.set(paramInt, paramT);
    return paramList;
  }
  
  public static int getIntAtIndex(ContentResolver paramContentResolver, String paramString, int paramInt) throws Settings.SettingNotFoundException {
    String str = Settings.Global.getString(paramContentResolver, paramString);
    if (str != null) {
      String[] arrayOfString = str.split(",");
      if (paramInt >= 0 && paramInt < arrayOfString.length && arrayOfString[paramInt] != null)
        try {
          return Integer.parseInt(arrayOfString[paramInt]);
        } catch (NumberFormatException numberFormatException) {} 
    } 
    throw new Settings.SettingNotFoundException(paramString);
  }
  
  public static boolean putIntAtIndex(ContentResolver paramContentResolver, String paramString, int paramInt1, int paramInt2) {
    String str1 = "";
    String[] arrayOfString = null;
    String str2 = Settings.Global.getString(paramContentResolver, paramString);
    if (paramInt1 != Integer.MAX_VALUE) {
      if (paramInt1 >= 0) {
        StringBuilder stringBuilder2;
        if (str2 != null)
          arrayOfString = str2.split(","); 
        for (byte b = 0; b < paramInt1; b++) {
          String str3 = "";
          str2 = str3;
          if (arrayOfString != null) {
            str2 = str3;
            if (b < arrayOfString.length)
              str2 = arrayOfString[b]; 
          } 
          StringBuilder stringBuilder4 = new StringBuilder();
          stringBuilder4.append(str1);
          stringBuilder4.append(str2);
          stringBuilder4.append(",");
          str1 = stringBuilder4.toString();
        } 
        StringBuilder stringBuilder3 = new StringBuilder();
        stringBuilder3.append(str1);
        stringBuilder3.append(paramInt2);
        str1 = stringBuilder3.toString();
        String str = str1;
        if (arrayOfString != null) {
          paramInt1++;
          while (true) {
            str = str1;
            if (paramInt1 < arrayOfString.length) {
              stringBuilder2 = new StringBuilder();
              stringBuilder2.append(str1);
              stringBuilder2.append(",");
              stringBuilder2.append(arrayOfString[paramInt1]);
              str1 = stringBuilder2.toString();
              paramInt1++;
              continue;
            } 
            break;
          } 
        } 
        return Settings.Global.putString(paramContentResolver, paramString, (String)stringBuilder2);
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("putIntAtIndex index < 0 index=");
      stringBuilder1.append(paramInt1);
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("putIntAtIndex index == MAX_VALUE index=");
    stringBuilder.append(paramInt1);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static String getTelephonyProperty(int paramInt, String paramString1, String paramString2) {
    String str1 = null;
    String str2 = SystemProperties.get(paramString1);
    paramString1 = str1;
    if (str2 != null) {
      paramString1 = str1;
      if (str2.length() > 0) {
        String[] arrayOfString = str2.split(",");
        paramString1 = str1;
        if (paramInt >= 0) {
          paramString1 = str1;
          if (paramInt < arrayOfString.length) {
            paramString1 = str1;
            if (arrayOfString[paramInt] != null)
              paramString1 = arrayOfString[paramInt]; 
          } 
        } 
      } 
    } 
    if (paramString1 == null)
      paramString1 = paramString2; 
    return paramString1;
  }
  
  private static <T> T getTelephonyProperty(int paramInt, List<T> paramList, T paramT) {
    T t1 = null;
    T t2 = t1;
    if (paramInt >= 0) {
      t2 = t1;
      if (paramInt < paramList.size())
        t2 = paramList.get(paramInt); 
    } 
    if (t2 != null)
      paramT = t2; 
    return paramT;
  }
  
  public static String getTelephonyProperty(String paramString1, String paramString2) {
    paramString1 = SystemProperties.get(paramString1);
    if (TextUtils.isEmpty(paramString1))
      paramString1 = paramString2; 
    return paramString1;
  }
  
  public int getSimCount() {
    return getPhoneCount();
  }
  
  @SystemApi
  public String getIsimIst() {
    try {
      IPhoneSubInfo iPhoneSubInfo = getSubscriberInfoService();
      if (iPhoneSubInfo == null)
        return null; 
      return iPhoneSubInfo.getIsimIst(getSubId());
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  public String[] getIsimPcscf() {
    try {
      IPhoneSubInfo iPhoneSubInfo = getSubscriberInfoService();
      if (iPhoneSubInfo == null)
        return null; 
      return iPhoneSubInfo.getIsimPcscf(getSubId());
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  public String getIccAuthentication(int paramInt1, int paramInt2, String paramString) {
    return getIccAuthentication(getSubId(), paramInt1, paramInt2, paramString);
  }
  
  public String getIccAuthentication(int paramInt1, int paramInt2, int paramInt3, String paramString) {
    try {
      IPhoneSubInfo iPhoneSubInfo = getSubscriberInfoService();
      if (iPhoneSubInfo == null)
        return null; 
      return iPhoneSubInfo.getIccSimChallengeResponse(paramInt1, paramInt2, paramInt3, paramString);
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  public String[] getForbiddenPlmns() {
    return getForbiddenPlmns(getSubId(), 2);
  }
  
  public String[] getForbiddenPlmns(int paramInt1, int paramInt2) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return null; 
      String str1 = this.mContext.getOpPackageName();
      String str2 = getAttributionTag();
      return iTelephony.getForbiddenPlmns(paramInt1, paramInt2, str1, str2);
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  public int setForbiddenPlmns(List<String> paramList) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return -1; 
      null = getSubId();
      String str1 = getOpPackageName(), str2 = getAttributionTag();
      return iTelephony.setForbiddenPlmns(null, 2, paramList, str1, str2);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setForbiddenPlmns RemoteException: ");
      stringBuilder.append(remoteException.getMessage());
      Rlog.e("TelephonyManager", stringBuilder.toString());
    } catch (NullPointerException nullPointerException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setForbiddenPlmns NullPointerException: ");
      stringBuilder.append(nullPointerException.getMessage());
      Rlog.e("TelephonyManager", stringBuilder.toString());
    } 
    return -1;
  }
  
  public String[] getPcscfAddress(String paramString) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return new String[0]; 
      return iTelephony.getPcscfAddress(paramString, getOpPackageName(), getAttributionTag());
    } catch (RemoteException remoteException) {
      return new String[0];
    } 
  }
  
  @SystemApi
  public void resetIms(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.resetIms(paramInt); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("toggleImsOnOff, RemoteException: ");
      stringBuilder.append(remoteException.getMessage());
      String str = stringBuilder.toString();
      Rlog.e("TelephonyManager", str);
    } 
  }
  
  public void enableIms(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.enableIms(paramInt); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("enableIms, RemoteException: ");
      stringBuilder.append(remoteException.getMessage());
      String str = stringBuilder.toString();
      Rlog.e("TelephonyManager", str);
    } 
  }
  
  public void disableIms(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.disableIms(paramInt); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("disableIms, RemoteException: ");
      stringBuilder.append(remoteException.getMessage());
      String str = stringBuilder.toString();
      Rlog.e("TelephonyManager", str);
    } 
  }
  
  public IImsMmTelFeature getImsMmTelFeatureAndListen(int paramInt, IImsServiceFeatureCallback paramIImsServiceFeatureCallback) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getMmTelFeatureAndListen(paramInt, paramIImsServiceFeatureCallback); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getImsMmTelFeatureAndListen, RemoteException: ");
      stringBuilder.append(remoteException.getMessage());
      String str = stringBuilder.toString();
      Rlog.e("TelephonyManager", str);
    } 
    return null;
  }
  
  public IImsRcsFeature getImsRcsFeatureAndListen(int paramInt, IImsServiceFeatureCallback paramIImsServiceFeatureCallback) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getRcsFeatureAndListen(paramInt, paramIImsServiceFeatureCallback); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getImsRcsFeatureAndListen, RemoteException: ");
      stringBuilder.append(remoteException.getMessage());
      String str = stringBuilder.toString();
      Rlog.e("TelephonyManager", str);
    } 
    return null;
  }
  
  public void unregisterImsFeatureCallback(int paramInt1, int paramInt2, IImsServiceFeatureCallback paramIImsServiceFeatureCallback) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.unregisterImsFeatureCallback(paramInt1, paramInt2, paramIImsServiceFeatureCallback); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("unregisterImsFeatureCallback, RemoteException: ");
      stringBuilder.append(remoteException.getMessage());
      String str = stringBuilder.toString();
      Rlog.e("TelephonyManager", str);
    } 
  }
  
  public IImsRegistration getImsRegistration(int paramInt1, int paramInt2) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getImsRegistration(paramInt1, paramInt2); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getImsRegistration, RemoteException: ");
      stringBuilder.append(remoteException.getMessage());
      Rlog.e("TelephonyManager", stringBuilder.toString());
    } 
    return null;
  }
  
  public IImsConfig getImsConfig(int paramInt1, int paramInt2) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getImsConfig(paramInt1, paramInt2); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getImsRegistration, RemoteException: ");
      stringBuilder.append(remoteException.getMessage());
      Rlog.e("TelephonyManager", stringBuilder.toString());
    } 
    return null;
  }
  
  public void setImsRegistrationState(boolean paramBoolean) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.setImsRegistrationState(paramBoolean); 
    } catch (RemoteException remoteException) {}
  }
  
  @Deprecated
  public int getPreferredNetworkType(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getPreferredNetworkType(paramInt); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "getPreferredNetworkType RemoteException", (Throwable)remoteException);
    } 
    return -1;
  }
  
  @SystemApi
  public long getPreferredNetworkTypeBitmask() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int i = iTelephony.getPreferredNetworkType(getSubId());
        i = RadioAccessFamily.getRafFromNetworkType(i);
        return i;
      } 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "getPreferredNetworkTypeBitmask RemoteException", (Throwable)remoteException);
    } 
    return 0L;
  }
  
  @SystemApi
  public long getAllowedNetworkTypes() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getAllowedNetworkTypes(getSubId()); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "getAllowedNetworkTypes RemoteException", (Throwable)remoteException);
    } 
    return -1L;
  }
  
  public void setNetworkSelectionModeAutomatic() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.setNetworkSelectionModeAutomatic(getSubId()); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "setNetworkSelectionModeAutomatic RemoteException", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Rlog.e("TelephonyManager", "setNetworkSelectionModeAutomatic NPE", nullPointerException);
    } 
  }
  
  public CellNetworkScanResult getAvailableNetworks() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int i = getSubId();
        String str1 = getOpPackageName();
        String str2 = getAttributionTag();
        return iTelephony.getCellNetworkScanResults(i, str1, str2);
      } 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "getAvailableNetworks RemoteException", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Rlog.e("TelephonyManager", "getAvailableNetworks NPE", nullPointerException);
    } 
    return new CellNetworkScanResult(4, null);
  }
  
  public NetworkScan requestNetworkScan(NetworkScanRequest paramNetworkScanRequest, Executor paramExecutor, TelephonyScanManager.NetworkScanCallback paramNetworkScanCallback) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mTelephonyScanManager : Landroid/telephony/TelephonyScanManager;
    //   6: ifnonnull -> 25
    //   9: new android/telephony/TelephonyScanManager
    //   12: astore #4
    //   14: aload #4
    //   16: invokespecial <init> : ()V
    //   19: aload_0
    //   20: aload #4
    //   22: putfield mTelephonyScanManager : Landroid/telephony/TelephonyScanManager;
    //   25: aload_0
    //   26: monitorexit
    //   27: aload_0
    //   28: getfield mTelephonyScanManager : Landroid/telephony/TelephonyScanManager;
    //   31: astore #5
    //   33: aload_0
    //   34: invokespecial getSubId : ()I
    //   37: istore #6
    //   39: aload_0
    //   40: invokespecial getOpPackageName : ()Ljava/lang/String;
    //   43: astore #4
    //   45: aload_0
    //   46: invokespecial getAttributionTag : ()Ljava/lang/String;
    //   49: astore #7
    //   51: aload #5
    //   53: iload #6
    //   55: aload_1
    //   56: aload_2
    //   57: aload_3
    //   58: aload #4
    //   60: aload #7
    //   62: invokevirtual requestNetworkScan : (ILandroid/telephony/NetworkScanRequest;Ljava/util/concurrent/Executor;Landroid/telephony/TelephonyScanManager$NetworkScanCallback;Ljava/lang/String;Ljava/lang/String;)Landroid/telephony/NetworkScan;
    //   65: areturn
    //   66: astore_1
    //   67: aload_0
    //   68: monitorexit
    //   69: aload_1
    //   70: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #8074	-> 0
    //   #8075	-> 2
    //   #8076	-> 9
    //   #8078	-> 25
    //   #8079	-> 27
    //   #8080	-> 39
    //   #8079	-> 51
    //   #8078	-> 66
    // Exception table:
    //   from	to	target	type
    //   2	9	66	finally
    //   9	25	66	finally
    //   25	27	66	finally
    //   67	69	66	finally
  }
  
  @Deprecated
  public NetworkScan requestNetworkScan(NetworkScanRequest paramNetworkScanRequest, TelephonyScanManager.NetworkScanCallback paramNetworkScanCallback) {
    return requestNetworkScan(paramNetworkScanRequest, AsyncTask.SERIAL_EXECUTOR, paramNetworkScanCallback);
  }
  
  public boolean setNetworkSelectionModeManual(String paramString, boolean paramBoolean) {
    return setNetworkSelectionModeManual(new OperatorInfo("", "", paramString), paramBoolean);
  }
  
  public boolean setNetworkSelectionModeManual(String paramString, boolean paramBoolean, int paramInt) {
    return setNetworkSelectionModeManual(new OperatorInfo("", "", paramString, paramInt), paramBoolean);
  }
  
  public boolean setNetworkSelectionModeManual(OperatorInfo paramOperatorInfo, boolean paramBoolean) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int i = getSubId();
        return iTelephony.setNetworkSelectionModeManual(i, paramOperatorInfo, paramBoolean);
      } 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "setNetworkSelectionModeManual RemoteException", (Throwable)remoteException);
    } 
    return false;
  }
  
  public int getNetworkSelectionMode() {
    boolean bool = false;
    int i = 0;
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        i = iTelephony.getNetworkSelectionMode(getSubId()); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "getNetworkSelectionMode RemoteException", (Throwable)remoteException);
      i = bool;
    } 
    return i;
  }
  
  public String getManualNetworkSelectionPlmn() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null && isManualNetworkSelectionAllowed())
        return iTelephony.getManualNetworkSelectionPlmn(getSubId()); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "getManualNetworkSelectionPlmn RemoteException", (Throwable)remoteException);
    } 
    return "";
  }
  
  public boolean isInEmergencySmsMode() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.isInEmergencySmsMode(); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "isInEmergencySmsMode RemoteException", (Throwable)remoteException);
    } 
    return false;
  }
  
  @Deprecated
  public boolean setPreferredNetworkType(int paramInt1, int paramInt2) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.setPreferredNetworkType(paramInt1, paramInt2); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "setPreferredNetworkType RemoteException", (Throwable)remoteException);
    } 
    return false;
  }
  
  @SystemApi
  public boolean setPreferredNetworkTypeBitmask(long paramLong) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int i = getSubId(), j = RadioAccessFamily.getNetworkTypeFromRaf((int)paramLong);
        return iTelephony.setPreferredNetworkType(i, j);
      } 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "setPreferredNetworkTypeBitmask RemoteException", (Throwable)remoteException);
    } 
    return false;
  }
  
  @SystemApi
  public boolean setAllowedNetworkTypes(long paramLong) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.setAllowedNetworkTypes(getSubId(), paramLong); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "setAllowedNetworkTypes RemoteException", (Throwable)remoteException);
    } 
    return false;
  }
  
  public void setAllowedNetworkTypesForReason(int paramInt, long paramLong) {
    if (paramInt == 0) {
      try {
        ITelephony iTelephony = getITelephony();
        if (iTelephony != null) {
          iTelephony.setAllowedNetworkTypesForReason(getSubId(), paramInt, paramLong);
        } else {
          IllegalStateException illegalStateException = new IllegalStateException();
          this("telephony service is null.");
          throw illegalStateException;
        } 
      } catch (RemoteException remoteException) {
        Rlog.e("TelephonyManager", "setAllowedNetworkTypesForReason RemoteException", (Throwable)remoteException);
        remoteException.rethrowFromSystemServer();
      } 
      return;
    } 
    throw new IllegalArgumentException("invalid AllowedNetworkTypesReason.");
  }
  
  public long getAllowedNetworkTypesForReason(int paramInt) {
    if (paramInt == 0)
      try {
        ITelephony iTelephony = getITelephony();
        if (iTelephony != null)
          return iTelephony.getAllowedNetworkTypesForReason(getSubId(), paramInt); 
        IllegalStateException illegalStateException = new IllegalStateException();
        this("telephony service is null.");
        throw illegalStateException;
      } catch (RemoteException remoteException) {
        Rlog.e("TelephonyManager", "getAllowedNetworkTypesForReason RemoteException", (Throwable)remoteException);
        remoteException.rethrowFromSystemServer();
        return -1L;
      }  
    throw new IllegalArgumentException("invalid AllowedNetworkTypesReason.");
  }
  
  public static long getAllNetworkTypesBitmask() {
    return 916479L;
  }
  
  public long getEffectiveAllowedNetworkTypes() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getEffectiveAllowedNetworkTypes(getSubId()); 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("telephony service is null.");
      throw illegalStateException;
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "getEffectiveAllowedNetworkTypes RemoteException", (Throwable)remoteException);
      remoteException.rethrowFromSystemServer();
      return -1L;
    } 
  }
  
  public boolean setPreferredNetworkTypeToGlobal() {
    return setPreferredNetworkTypeToGlobal(getSubId());
  }
  
  public boolean setPreferredNetworkTypeToGlobal(int paramInt) {
    return setPreferredNetworkType(paramInt, 10);
  }
  
  @SystemApi
  public boolean isTetheringApnRequired() {
    return isTetheringApnRequired(getSubId(SubscriptionManager.getActiveDataSubscriptionId()));
  }
  
  public boolean isTetheringApnRequired(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.isTetheringApnRequiredForSubscriber(paramInt); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "hasMatchedTetherApnSetting RemoteException", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Rlog.e("TelephonyManager", "hasMatchedTetherApnSetting NPE", nullPointerException);
    } 
    return false;
  }
  
  public boolean hasCarrierPrivileges() {
    return hasCarrierPrivileges(getSubId());
  }
  
  public boolean hasCarrierPrivileges(int paramInt) {
    boolean bool = false;
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        paramInt = iTelephony.getCarrierPrivilegeStatus(paramInt);
        if (paramInt == 1)
          bool = true; 
        return bool;
      } 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "hasCarrierPrivileges RemoteException", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Rlog.e("TelephonyManager", "hasCarrierPrivileges NPE", nullPointerException);
    } 
    return false;
  }
  
  public boolean setOperatorBrandOverride(String paramString) {
    return setOperatorBrandOverride(getSubId(), paramString);
  }
  
  public boolean setOperatorBrandOverride(int paramInt, String paramString) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.setOperatorBrandOverride(paramInt, paramString); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "setOperatorBrandOverride RemoteException", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Rlog.e("TelephonyManager", "setOperatorBrandOverride NPE", nullPointerException);
    } 
    return false;
  }
  
  public boolean setRoamingOverride(List<String> paramList1, List<String> paramList2, List<String> paramList3, List<String> paramList4) {
    return setRoamingOverride(getSubId(), paramList1, paramList2, paramList3, paramList4);
  }
  
  public boolean setRoamingOverride(int paramInt, List<String> paramList1, List<String> paramList2, List<String> paramList3, List<String> paramList4) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.setRoamingOverride(paramInt, paramList1, paramList2, paramList3, paramList4); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "setRoamingOverride RemoteException", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Rlog.e("TelephonyManager", "setRoamingOverride NPE", nullPointerException);
    } 
    return false;
  }
  
  @SystemApi
  public String getCdmaMdn() {
    return getCdmaMdn(getSubId());
  }
  
  @SystemApi
  public String getCdmaMdn(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return null; 
      return iTelephony.getCdmaMdn(paramInt);
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  @SystemApi
  public String getCdmaMin() {
    return getCdmaMin(getSubId());
  }
  
  @SystemApi
  public String getCdmaMin(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return null; 
      return iTelephony.getCdmaMin(paramInt);
    } catch (RemoteException remoteException) {
      return null;
    } catch (NullPointerException nullPointerException) {
      return null;
    } 
  }
  
  @SystemApi
  public int checkCarrierPrivilegesForPackage(String paramString) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.checkCarrierPrivilegesForPackage(getSubId(), paramString); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "checkCarrierPrivilegesForPackage RemoteException", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Rlog.e("TelephonyManager", "checkCarrierPrivilegesForPackage NPE", nullPointerException);
    } 
    return 0;
  }
  
  @SystemApi
  public int checkCarrierPrivilegesForPackageAnyPhone(String paramString) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.checkCarrierPrivilegesForPackageAnyPhone(paramString); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "checkCarrierPrivilegesForPackageAnyPhone RemoteException", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Rlog.e("TelephonyManager", "checkCarrierPrivilegesForPackageAnyPhone NPE", nullPointerException);
    } 
    return 0;
  }
  
  @SystemApi
  public List<String> getCarrierPackageNamesForIntent(Intent paramIntent) {
    return getCarrierPackageNamesForIntentAndPhone(paramIntent, getPhoneId());
  }
  
  @SystemApi
  public List<String> getCarrierPackageNamesForIntentAndPhone(Intent paramIntent, int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getCarrierPackageNamesForIntentAndPhone(paramIntent, paramInt); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "getCarrierPackageNamesForIntentAndPhone RemoteException", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Rlog.e("TelephonyManager", "getCarrierPackageNamesForIntentAndPhone NPE", nullPointerException);
    } 
    return null;
  }
  
  public List<String> getPackagesWithCarrierPrivileges() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getPackagesWithCarrierPrivileges(getPhoneId()); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "getPackagesWithCarrierPrivileges RemoteException", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Rlog.e("TelephonyManager", "getPackagesWithCarrierPrivileges NPE", nullPointerException);
    } 
    return Collections.EMPTY_LIST;
  }
  
  @SystemApi
  public List<String> getCarrierPrivilegedPackagesForAllActiveSubscriptions() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getPackagesWithCarrierPrivilegesForAllPhones(); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "getCarrierPrivilegedPackagesForAllActiveSubscriptions RemoteException", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Rlog.e("TelephonyManager", "getCarrierPrivilegedPackagesForAllActiveSubscriptions NPE", nullPointerException);
    } 
    return Collections.EMPTY_LIST;
  }
  
  @SystemApi
  public void dial(String paramString) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.dial(paramString); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#dial", (Throwable)remoteException);
    } 
  }
  
  @SystemApi
  @Deprecated
  public void call(String paramString1, String paramString2) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.call(paramString1, paramString2); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#call", (Throwable)remoteException);
    } 
  }
  
  @SystemApi
  @Deprecated
  public boolean endCall() {
    return false;
  }
  
  @SystemApi
  @Deprecated
  public void answerRingingCall() {}
  
  @SystemApi
  @Deprecated
  public void silenceRinger() {}
  
  @SystemApi
  @Deprecated
  public boolean isOffhook() {
    TelecomManager telecomManager = (TelecomManager)this.mContext.getSystemService("telecom");
    return telecomManager.isInCall();
  }
  
  @SystemApi
  @Deprecated
  public boolean isRinging() {
    TelecomManager telecomManager = (TelecomManager)this.mContext.getSystemService("telecom");
    return telecomManager.isRinging();
  }
  
  @SystemApi
  @Deprecated
  public boolean isIdle() {
    TelecomManager telecomManager = (TelecomManager)this.mContext.getSystemService("telecom");
    return telecomManager.isInCall() ^ true;
  }
  
  @SystemApi
  @Deprecated
  public boolean isRadioOn() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.isRadioOnWithFeature(getOpPackageName(), getAttributionTag()); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#isRadioOn", (Throwable)remoteException);
    } 
    return false;
  }
  
  @SystemApi
  public boolean supplyPin(String paramString) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.supplyPinForSubscriber(getSubId(), paramString); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#supplyPinForSubscriber", (Throwable)remoteException);
    } 
    return false;
  }
  
  @SystemApi
  public boolean supplyPuk(String paramString1, String paramString2) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.supplyPukForSubscriber(getSubId(), paramString1, paramString2); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#supplyPukForSubscriber", (Throwable)remoteException);
    } 
    return false;
  }
  
  @SystemApi
  public int[] supplyPinReportResult(String paramString) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.supplyPinReportResultForSubscriber(getSubId(), paramString); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#supplyPinReportResultForSubscriber", (Throwable)remoteException);
    } 
    return new int[0];
  }
  
  @SystemApi
  public int[] supplyPukReportResult(String paramString1, String paramString2) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.supplyPukReportResultForSubscriber(getSubId(), paramString1, paramString2); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#]", (Throwable)remoteException);
    } 
    return new int[0];
  }
  
  public PinResult supplyPinReportPinResult(String paramString) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int[] arrayOfInt = iTelephony.supplyPinReportResultForSubscriber(getSubId(), paramString);
        return new PinResult(arrayOfInt[0], arrayOfInt[1]);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#supplyPinReportResultForSubscriber", (Throwable)remoteException);
    } 
    return null;
  }
  
  public PinResult supplyPukReportPinResult(String paramString1, String paramString2) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int[] arrayOfInt = iTelephony.supplyPukReportResultForSubscriber(getSubId(), paramString1, paramString2);
        return new PinResult(arrayOfInt[0], arrayOfInt[1]);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#]", (Throwable)remoteException);
    } 
    return null;
  }
  
  public static abstract class UssdResponseCallback {
    public void onReceiveUssdResponse(TelephonyManager param1TelephonyManager, String param1String, CharSequence param1CharSequence) {}
    
    public void onReceiveUssdResponseFailed(TelephonyManager param1TelephonyManager, String param1String, int param1Int) {}
  }
  
  public void sendUssdRequest(String paramString, UssdResponseCallback paramUssdResponseCallback, Handler paramHandler) {
    Preconditions.checkNotNull(paramUssdResponseCallback, "UssdResponseCallback cannot be null.");
    Object object = new Object(this, paramHandler, paramUssdResponseCallback, this);
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.handleUssdRequest(getSubId(), paramString, (ResultReceiver)object); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#sendUSSDCode", (Throwable)remoteException);
      UssdResponse ussdResponse = new UssdResponse(paramString, "");
      Bundle bundle = new Bundle();
      bundle.putParcelable("USSD_RESPONSE", ussdResponse);
      object.send(-2, bundle);
    } 
  }
  
  public boolean isConcurrentVoiceAndDataSupported() {
    boolean bool = false;
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int i = getSubId();
        bool = iTelephony.isConcurrentVoiceAndDataAllowed(i);
      } 
      return bool;
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#isConcurrentVoiceAndDataAllowed", (Throwable)remoteException);
      return false;
    } 
  }
  
  @SystemApi
  public boolean handlePinMmi(String paramString) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.handlePinMmi(paramString); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#handlePinMmi", (Throwable)remoteException);
    } 
    return false;
  }
  
  @SystemApi
  public boolean handlePinMmiForSubscriber(int paramInt, String paramString) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.handlePinMmiForSubscriber(paramInt, paramString); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#handlePinMmi", (Throwable)remoteException);
    } 
    return false;
  }
  
  @SystemApi
  public void toggleRadioOnOff() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.toggleRadioOnOff(); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#toggleRadioOnOff", (Throwable)remoteException);
    } 
  }
  
  @SystemApi
  public boolean setRadio(boolean paramBoolean) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.setRadio(paramBoolean); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#setRadio", (Throwable)remoteException);
    } 
    return false;
  }
  
  @SystemApi
  public boolean setRadioPower(boolean paramBoolean) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.setRadioPower(paramBoolean); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#setRadioPower", (Throwable)remoteException);
    } 
    return false;
  }
  
  @SystemApi
  public void shutdownAllRadios() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.shutdownMobileRadios(); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#shutdownAllRadios", (Throwable)remoteException);
      remoteException.rethrowAsRuntimeException();
    } 
  }
  
  @SystemApi
  public boolean isAnyRadioPoweredOn() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.needMobileRadioShutdown(); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#isAnyRadioPoweredOn", (Throwable)remoteException);
      remoteException.rethrowAsRuntimeException();
    } 
    return false;
  }
  
  @SystemApi
  public int getRadioPowerState() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        null = getSlotIndex();
        String str1 = this.mContext.getOpPackageName();
        Context context = this.mContext;
        String str2 = context.getAttributionTag();
        return iTelephony.getRadioPowerState(null, str1, str2);
      } 
    } catch (RemoteException remoteException) {}
    return 2;
  }
  
  @SystemApi
  public void updateServiceLocation() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.updateServiceLocation(); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#updateServiceLocation", (Throwable)remoteException);
    } 
  }
  
  @SystemApi
  public boolean enableDataConnectivity() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.enableDataConnectivity(); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#enableDataConnectivity", (Throwable)remoteException);
    } 
    return false;
  }
  
  @SystemApi
  public boolean disableDataConnectivity() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.disableDataConnectivity(); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#disableDataConnectivity", (Throwable)remoteException);
    } 
    return false;
  }
  
  @SystemApi
  public boolean isDataConnectivityPossible() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int i = SubscriptionManager.getActiveDataSubscriptionId();
        return iTelephony.isDataConnectivityPossible(getSubId(i));
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#isDataAllowed", (Throwable)remoteException);
    } 
    return false;
  }
  
  @SystemApi
  public boolean needsOtaServiceProvisioning() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.needsOtaServiceProvisioning(); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#needsOtaServiceProvisioning", (Throwable)remoteException);
    } 
    return false;
  }
  
  public void setDataEnabled(boolean paramBoolean) {
    setDataEnabled(getSubId(SubscriptionManager.getDefaultDataSubscriptionId()), paramBoolean);
  }
  
  @SystemApi
  @Deprecated
  public void setDataEnabled(int paramInt, boolean paramBoolean) {
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("setDataEnabled: enabled=");
      stringBuilder.append(paramBoolean);
      Log.d("TelephonyManager", stringBuilder.toString());
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.setUserDataEnabled(paramInt, paramBoolean); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#setUserDataEnabled", (Throwable)remoteException);
    } 
  }
  
  @SystemApi
  @Deprecated
  public boolean getDataEnabled() {
    return isDataEnabled();
  }
  
  public boolean isDataEnabled() {
    return getDataEnabled(getSubId(SubscriptionManager.getDefaultDataSubscriptionId()));
  }
  
  public boolean isDataRoamingEnabled() {
    boolean bool = false;
    boolean bool1 = false;
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int i = getSubId(SubscriptionManager.getDefaultDataSubscriptionId());
        bool1 = iTelephony.isDataRoamingEnabled(i);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#isDataRoamingEnabled", (Throwable)remoteException);
      bool1 = bool;
    } 
    return bool1;
  }
  
  public int getCdmaRoamingMode() {
    byte b2, b1 = -1;
    try {
      ITelephony iTelephony = getITelephony();
      b2 = b1;
      if (iTelephony != null)
        b2 = iTelephony.getCdmaRoamingMode(getSubId()); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#getCdmaRoamingMode", (Throwable)remoteException);
      b2 = b1;
    } 
    return b2;
  }
  
  public boolean setCdmaRoamingMode(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.setCdmaRoamingMode(getSubId(), paramInt); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#setCdmaRoamingMode", (Throwable)remoteException);
    } 
    return false;
  }
  
  public boolean setCdmaSubscriptionMode(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.setCdmaSubscriptionMode(getSubId(), paramInt); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#setCdmaSubscriptionMode", (Throwable)remoteException);
    } 
    return false;
  }
  
  @SystemApi
  public void setDataRoamingEnabled(boolean paramBoolean) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int i = getSubId(SubscriptionManager.getDefaultDataSubscriptionId());
        iTelephony.setDataRoamingEnabled(i, paramBoolean);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#setDataRoamingEnabled", (Throwable)remoteException);
    } 
  }
  
  @SystemApi
  @Deprecated
  public boolean getDataEnabled(int paramInt) {
    boolean bool = false;
    boolean bool1 = false;
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        bool1 = iTelephony.isUserDataEnabled(paramInt); 
    } catch (RemoteException|NullPointerException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#isUserDataEnabled", (Throwable)remoteException);
      bool1 = bool;
    } 
    return bool1;
  }
  
  @Deprecated
  public int invokeOemRilRequestRaw(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.invokeOemRilRequestRaw(paramArrayOfbyte1, paramArrayOfbyte2); 
    } catch (RemoteException remoteException) {
    
    } catch (NullPointerException nullPointerException) {}
    return -1;
  }
  
  @SystemApi
  @Deprecated
  public void enableVideoCalling(boolean paramBoolean) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.enableVideoCalling(paramBoolean); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#enableVideoCalling", (Throwable)remoteException);
    } 
  }
  
  @SystemApi
  @Deprecated
  public boolean isVideoCallingEnabled() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.isVideoCallingEnabled(getOpPackageName(), getAttributionTag()); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#isVideoCallingEnabled", (Throwable)remoteException);
    } 
    return false;
  }
  
  public boolean canChangeDtmfToneLength() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int i = this.mSubId;
        String str1 = getOpPackageName();
        String str2 = getAttributionTag();
        return iTelephony.canChangeDtmfToneLength(i, str1, str2);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#canChangeDtmfToneLength", (Throwable)remoteException);
    } catch (SecurityException securityException) {
      Log.e("TelephonyManager", "Permission error calling ITelephony#canChangeDtmfToneLength", securityException);
    } 
    return false;
  }
  
  public boolean isWorldPhone() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.isWorldPhone(this.mSubId, getOpPackageName(), getAttributionTag()); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#isWorldPhone", (Throwable)remoteException);
    } catch (SecurityException securityException) {
      Log.e("TelephonyManager", "Permission error calling ITelephony#isWorldPhone", securityException);
    } 
    return false;
  }
  
  @Deprecated
  public boolean isTtyModeSupported() {
    TelecomManager telecomManager = null;
    try {
      if (this.mContext != null)
        telecomManager = (TelecomManager)this.mContext.getSystemService(TelecomManager.class); 
      if (telecomManager != null)
        return telecomManager.isTtySupported(); 
    } catch (SecurityException securityException) {
      Log.e("TelephonyManager", "Permission error calling TelecomManager#isTtySupported", securityException);
    } 
    return false;
  }
  
  public boolean isRttSupported() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.isRttSupported(this.mSubId); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#isRttSupported", (Throwable)remoteException);
    } catch (SecurityException securityException) {
      Log.e("TelephonyManager", "Permission error calling ITelephony#isWorldPhone", securityException);
    } 
    return false;
  }
  
  public boolean isHearingAidCompatibilitySupported() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.isHearingAidCompatibilitySupported(); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#isHearingAidCompatibilitySupported", (Throwable)remoteException);
    } catch (SecurityException securityException) {
      Log.e("TelephonyManager", "Permission error calling ITelephony#isHearingAidCompatibilitySupported", securityException);
    } 
    return false;
  }
  
  public boolean isImsRegistered(int paramInt) {
    try {
      return getITelephony().isImsRegistered(paramInt);
    } catch (RemoteException|NullPointerException remoteException) {
      return false;
    } 
  }
  
  public boolean isImsRegistered() {
    try {
      return getITelephony().isImsRegistered(getSubId());
    } catch (RemoteException|NullPointerException remoteException) {
      return false;
    } 
  }
  
  public boolean isVolteAvailable() {
    try {
      return getITelephony().isAvailable(getSubId(), 1, 0);
    } catch (RemoteException|NullPointerException remoteException) {
      return false;
    } 
  }
  
  public boolean isVideoTelephonyAvailable() {
    try {
      return getITelephony().isVideoTelephonyAvailable(getSubId());
    } catch (RemoteException|NullPointerException remoteException) {
      return false;
    } 
  }
  
  public boolean isWifiCallingAvailable() {
    try {
      return getITelephony().isWifiCallingAvailable(getSubId());
    } catch (RemoteException|NullPointerException remoteException) {
      return false;
    } 
  }
  
  public int getImsRegTechnologyForMmTel() {
    try {
      return getITelephony().getImsRegTechnologyForMmTel(getSubId());
    } catch (RemoteException remoteException) {
      return -1;
    } 
  }
  
  public void setSimOperatorNumeric(String paramString) {
    int i = getPhoneId();
    setSimOperatorNumericForPhone(i, paramString);
  }
  
  public void setSimOperatorNumericForPhone(int paramInt, String paramString) {
    if (SubscriptionManager.isValidPhoneId(paramInt)) {
      List<String> list2 = TelephonyProperties.icc_operator_numeric();
      List<String> list1 = updateTelephonyProperty(list2, paramInt, paramString);
      TelephonyProperties.icc_operator_numeric(list1);
    } 
  }
  
  public void setSimOperatorName(String paramString) {
    int i = getPhoneId();
    setSimOperatorNameForPhone(i, paramString);
  }
  
  public void setSimOperatorNameForPhone(int paramInt, String paramString) {
    if (SubscriptionManager.isValidPhoneId(paramInt)) {
      List<String> list2 = TelephonyProperties.icc_operator_alpha();
      List<String> list1 = updateTelephonyProperty(list2, paramInt, paramString);
      TelephonyProperties.icc_operator_alpha(list1);
    } 
  }
  
  public void setSimCountryIso(String paramString) {
    int i = getPhoneId();
    setSimCountryIsoForPhone(i, paramString);
  }
  
  public void setSimCountryIsoForPhone(int paramInt, String paramString) {
    if (SubscriptionManager.isValidPhoneId(paramInt)) {
      List<String> list2 = TelephonyProperties.icc_operator_iso_country();
      List<String> list1 = updateTelephonyProperty(list2, paramInt, paramString);
      TelephonyProperties.icc_operator_iso_country(list1);
    } 
  }
  
  public void setSimState(String paramString) {
    int i = getPhoneId();
    setSimStateForPhone(i, paramString);
  }
  
  public void setSimStateForPhone(int paramInt, String paramString) {
    if (SubscriptionManager.isValidPhoneId(paramInt)) {
      List<String> list2 = TelephonyProperties.sim_state();
      List<String> list1 = updateTelephonyProperty(list2, paramInt, paramString);
      TelephonyProperties.sim_state(list1);
    } 
  }
  
  @SystemApi
  public void setSimPowerState(int paramInt) {
    setSimPowerStateForSlot(getSlotIndex(), paramInt);
  }
  
  @SystemApi
  public void setSimPowerStateForSlot(int paramInt1, int paramInt2) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.setSimPowerStateForSlot(paramInt1, paramInt2); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#setSimPowerStateForSlot", (Throwable)remoteException);
    } catch (SecurityException securityException) {
      Log.e("TelephonyManager", "Permission error calling ITelephony#setSimPowerStateForSlot", securityException);
    } 
  }
  
  public void setBasebandVersion(String paramString) {
    int i = getPhoneId();
    setBasebandVersionForPhone(i, paramString);
  }
  
  public void setBasebandVersionForPhone(int paramInt, String paramString) {
    if (SubscriptionManager.isValidPhoneId(paramInt)) {
      List<String> list2 = TelephonyProperties.baseband_version();
      List<String> list1 = updateTelephonyProperty(list2, paramInt, paramString);
      TelephonyProperties.baseband_version(list1);
    } 
  }
  
  public String getBasebandVersion() {
    int i = getPhoneId();
    return getBasebandVersionForPhone(i);
  }
  
  public String getBasebandVersionForPhone(int paramInt) {
    return getTelephonyProperty(paramInt, TelephonyProperties.baseband_version(), "");
  }
  
  public void setPhoneType(int paramInt) {
    int i = getPhoneId();
    setPhoneType(i, paramInt);
  }
  
  public void setPhoneType(int paramInt1, int paramInt2) {
    if (SubscriptionManager.isValidPhoneId(paramInt1)) {
      List<Integer> list = TelephonyProperties.current_active_phone();
      list = updateTelephonyProperty(list, paramInt1, Integer.valueOf(paramInt2));
      TelephonyProperties.current_active_phone(list);
    } 
  }
  
  public String getOtaSpNumberSchema(String paramString) {
    int i = getPhoneId();
    return getOtaSpNumberSchemaForPhone(i, paramString);
  }
  
  public String getOtaSpNumberSchemaForPhone(int paramInt, String paramString) {
    if (SubscriptionManager.isValidPhoneId(paramInt)) {
      List<String> list = TelephonyProperties.otasp_num_schema();
      return getTelephonyProperty(paramInt, list, paramString);
    } 
    return paramString;
  }
  
  public boolean getSmsReceiveCapable(boolean paramBoolean) {
    int i = getPhoneId();
    return getSmsReceiveCapableForPhone(i, paramBoolean);
  }
  
  public boolean getSmsReceiveCapableForPhone(int paramInt, boolean paramBoolean) {
    if (SubscriptionManager.isValidPhoneId(paramInt))
      return ((Boolean)getTelephonyProperty(paramInt, TelephonyProperties.sms_receive(), Boolean.valueOf(paramBoolean))).booleanValue(); 
    return paramBoolean;
  }
  
  public boolean getSmsSendCapable(boolean paramBoolean) {
    int i = getPhoneId();
    return getSmsSendCapableForPhone(i, paramBoolean);
  }
  
  public boolean getSmsSendCapableForPhone(int paramInt, boolean paramBoolean) {
    if (SubscriptionManager.isValidPhoneId(paramInt))
      return ((Boolean)getTelephonyProperty(paramInt, TelephonyProperties.sms_send(), Boolean.valueOf(paramBoolean))).booleanValue(); 
    return paramBoolean;
  }
  
  @SystemApi
  public ComponentName getAndUpdateDefaultRespondViaMessageApplication() {
    return SmsApplication.getDefaultRespondViaMessageApplication(this.mContext, true);
  }
  
  @SystemApi
  public ComponentName getDefaultRespondViaMessageApplication() {
    return SmsApplication.getDefaultRespondViaMessageApplication(this.mContext, false);
  }
  
  public void setNetworkOperatorName(String paramString) {
    int i = getPhoneId();
    setNetworkOperatorNameForPhone(i, paramString);
  }
  
  public void setNetworkOperatorNameForPhone(int paramInt, String paramString) {
    if (SubscriptionManager.isValidPhoneId(paramInt)) {
      List<String> list2 = TelephonyProperties.operator_alpha();
      List<String> list1 = updateTelephonyProperty(list2, paramInt, paramString);
      TelephonyProperties.operator_alpha(list1);
    } 
  }
  
  public void setNetworkOperatorNumeric(String paramString) {
    int i = getPhoneId();
    setNetworkOperatorNumericForPhone(i, paramString);
  }
  
  public void setNetworkOperatorNumericForPhone(int paramInt, String paramString) {
    if (SubscriptionManager.isValidPhoneId(paramInt)) {
      List<String> list2 = TelephonyProperties.operator_numeric();
      List<String> list1 = updateTelephonyProperty(list2, paramInt, paramString);
      TelephonyProperties.operator_numeric(list1);
    } 
  }
  
  public void setNetworkRoaming(boolean paramBoolean) {
    int i = getPhoneId();
    setNetworkRoamingForPhone(i, paramBoolean);
  }
  
  public void setNetworkRoamingForPhone(int paramInt, boolean paramBoolean) {
    if (SubscriptionManager.isValidPhoneId(paramInt)) {
      List<Boolean> list = TelephonyProperties.operator_is_roaming();
      list = updateTelephonyProperty(list, paramInt, Boolean.valueOf(paramBoolean));
      TelephonyProperties.operator_is_roaming(list);
    } 
  }
  
  public void setDataNetworkType(int paramInt) {
    int i = getPhoneId(SubscriptionManager.getDefaultDataSubscriptionId());
    setDataNetworkTypeForPhone(i, paramInt);
  }
  
  public void setDataNetworkTypeForPhone(int paramInt1, int paramInt2) {
    if (SubscriptionManager.isValidPhoneId(paramInt1)) {
      List<String> list1 = TelephonyProperties.data_network_type();
      String str = ServiceState.rilRadioTechnologyToString(paramInt2);
      List<String> list2 = updateTelephonyProperty(list1, paramInt1, str);
      TelephonyProperties.data_network_type(list2);
    } 
  }
  
  public int getSubIdForPhoneAccount(PhoneAccount paramPhoneAccount) {
    byte b2, b1 = -1;
    try {
      ITelephony iTelephony = getITelephony();
      b2 = b1;
      if (iTelephony != null)
        b2 = iTelephony.getSubIdForPhoneAccount(paramPhoneAccount); 
    } catch (RemoteException remoteException) {
      b2 = b1;
    } 
    return b2;
  }
  
  public PhoneAccountHandle getPhoneAccountHandleForSubscriptionId(int paramInt) {
    RemoteException remoteException1 = null;
    PhoneAccountHandle phoneAccountHandle = null;
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        phoneAccountHandle = iTelephony.getPhoneAccountHandleForSubscriptionId(paramInt); 
    } catch (RemoteException remoteException2) {
      remoteException2 = remoteException1;
    } 
    return (PhoneAccountHandle)remoteException2;
  }
  
  public int getSubscriptionId(PhoneAccountHandle paramPhoneAccountHandle) {
    byte b2, b1 = -1;
    try {
      ITelephony iTelephony = getITelephony();
      b2 = b1;
      if (iTelephony != null) {
        Context context1 = this.mContext;
        String str1 = context1.getOpPackageName();
        Context context2 = this.mContext;
        String str2 = context2.getAttributionTag();
        b2 = iTelephony.getSubIdForPhoneAccountHandle(paramPhoneAccountHandle, str1, str2);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "getSubscriptionId RemoteException", (Throwable)remoteException);
      remoteException.rethrowAsRuntimeException();
      b2 = b1;
    } 
    return b2;
  }
  
  public void factoryReset(int paramInt) {
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("factoryReset: subId=");
      stringBuilder.append(paramInt);
      Log.d("TelephonyManager", stringBuilder.toString());
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.factoryReset(paramInt); 
    } catch (RemoteException remoteException) {}
  }
  
  @SystemApi
  public void resetSettings() {
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("resetSettings: subId=");
      stringBuilder.append(getSubId());
      Log.d("TelephonyManager", stringBuilder.toString());
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.factoryReset(getSubId()); 
    } catch (RemoteException remoteException) {}
  }
  
  @SystemApi
  public Locale getSimLocale() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        String str = iTelephony.getSimLocaleForSubscriber(getSubId());
        if (!TextUtils.isEmpty(str))
          return Locale.forLanguageTag(str); 
      } 
    } catch (RemoteException remoteException) {}
    return null;
  }
  
  public String getLocaleFromDefaultSim() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getSimLocaleForSubscriber(getSubId()); 
    } catch (RemoteException remoteException) {}
    return null;
  }
  
  public void requestModemActivityInfo(ResultReceiver paramResultReceiver) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        iTelephony.requestModemActivityInfo(paramResultReceiver);
        return;
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#getModemActivityInfo", (Throwable)remoteException);
    } 
    paramResultReceiver.send(0, null);
  }
  
  public ServiceState getServiceState() {
    return getServiceStateForSubscriber(getSubId());
  }
  
  public ServiceState getServiceStateForSubscriber(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        String str1 = getOpPackageName();
        String str2 = getAttributionTag();
        return iTelephony.getServiceStateForSubscriber(paramInt, str1, str2);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#getServiceStateForSubscriber", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      UUID uUID = UUID.fromString("a3ab0b9d-f2aa-4baf-911d-7096c0d4645a");
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getServiceStateForSubscriber ");
      stringBuilder.append(paramInt);
      stringBuilder.append(" NPE");
      String str = stringBuilder.toString();
      AnomalyReporter.reportAnomaly(uUID, str);
    } 
    return null;
  }
  
  public Uri getVoicemailRingtoneUri(PhoneAccountHandle paramPhoneAccountHandle) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getVoicemailRingtoneUri(paramPhoneAccountHandle); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#getVoicemailRingtoneUri", (Throwable)remoteException);
    } 
    return null;
  }
  
  public void setVoicemailRingtoneUri(PhoneAccountHandle paramPhoneAccountHandle, Uri paramUri) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.setVoicemailRingtoneUri(getOpPackageName(), paramPhoneAccountHandle, paramUri); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#setVoicemailRingtoneUri", (Throwable)remoteException);
    } 
  }
  
  public boolean isVoicemailVibrationEnabled(PhoneAccountHandle paramPhoneAccountHandle) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.isVoicemailVibrationEnabled(paramPhoneAccountHandle); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#isVoicemailVibrationEnabled", (Throwable)remoteException);
    } 
    return false;
  }
  
  public void setVoicemailVibrationEnabled(PhoneAccountHandle paramPhoneAccountHandle, boolean paramBoolean) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.setVoicemailVibrationEnabled(getOpPackageName(), paramPhoneAccountHandle, paramBoolean); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#isVoicemailVibrationEnabled", (Throwable)remoteException);
    } 
  }
  
  public int getSimCarrierId() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getSubscriptionCarrierId(getSubId()); 
    } catch (RemoteException remoteException) {}
    return -1;
  }
  
  public CharSequence getSimCarrierIdName() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getSubscriptionCarrierName(getSubId()); 
    } catch (RemoteException remoteException) {}
    return null;
  }
  
  public int getSimSpecificCarrierId() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getSubscriptionSpecificCarrierId(getSubId()); 
    } catch (RemoteException remoteException) {}
    return -1;
  }
  
  public CharSequence getSimSpecificCarrierIdName() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getSubscriptionSpecificCarrierName(getSubId()); 
    } catch (RemoteException remoteException) {}
    return null;
  }
  
  public int getCarrierIdFromSimMccMnc() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getCarrierIdFromMccMnc(getSlotIndex(), getSimOperator(), true); 
    } catch (RemoteException remoteException) {}
    return -1;
  }
  
  public int getCarrierIdFromMccMnc(String paramString) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getCarrierIdFromMccMnc(getSlotIndex(), paramString, false); 
    } catch (RemoteException remoteException) {}
    return -1;
  }
  
  public List<String> getCertsFromCarrierPrivilegeAccessRules() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getCertsFromCarrierPrivilegeAccessRules(getSubId()); 
    } catch (RemoteException remoteException) {}
    return null;
  }
  
  @SystemApi
  public String getAidForAppType(int paramInt) {
    return getAidForAppType(getSubId(), paramInt);
  }
  
  public String getAidForAppType(int paramInt1, int paramInt2) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getAidForAppType(paramInt1, paramInt2); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#getAidForAppType", (Throwable)remoteException);
    } 
    return null;
  }
  
  public String getEsn() {
    return getEsn(getSubId());
  }
  
  public String getEsn(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getEsn(paramInt); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#getEsn", (Throwable)remoteException);
    } 
    return null;
  }
  
  @SystemApi
  public String getCdmaPrlVersion() {
    return getCdmaPrlVersion(getSubId());
  }
  
  public String getCdmaPrlVersion(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getCdmaPrlVersion(paramInt); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#getCdmaPrlVersion", (Throwable)remoteException);
    } 
    return null;
  }
  
  @SystemApi
  public List<TelephonyHistogram> getTelephonyHistograms() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getTelephonyHistograms(); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#getTelephonyHistograms", (Throwable)remoteException);
    } 
    return null;
  }
  
  @SystemApi
  public int setAllowedCarriers(int paramInt, List<CarrierIdentifier> paramList) {
    if (paramList == null || !SubscriptionManager.isValidPhoneId(paramInt))
      return -1; 
    CarrierRestrictionRules.Builder builder = CarrierRestrictionRules.newBuilder();
    builder = builder.setAllowedCarriers(paramList);
    builder = builder.setDefaultCarrierRestriction(0);
    CarrierRestrictionRules carrierRestrictionRules = builder.build();
    paramInt = setCarrierRestrictionRules(carrierRestrictionRules);
    if (paramInt == 0)
      return paramList.size(); 
    return -1;
  }
  
  @SystemApi
  public int setCarrierRestrictionRules(CarrierRestrictionRules paramCarrierRestrictionRules) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.setAllowedCarriers(paramCarrierRestrictionRules); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#setAllowedCarriers", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Log.e("TelephonyManager", "Error calling ITelephony#setAllowedCarriers", nullPointerException);
    } 
    return 2;
  }
  
  @SystemApi
  @Deprecated
  public List<CarrierIdentifier> getAllowedCarriers(int paramInt) {
    if (SubscriptionManager.isValidPhoneId(paramInt)) {
      CarrierRestrictionRules carrierRestrictionRules = getCarrierRestrictionRules();
      if (carrierRestrictionRules != null)
        return carrierRestrictionRules.getAllowedCarriers(); 
    } 
    return new ArrayList<>(0);
  }
  
  @SystemApi
  public CarrierRestrictionRules getCarrierRestrictionRules() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getAllowedCarriers(); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#getAllowedCarriers", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Log.e("TelephonyManager", "Error calling ITelephony#getAllowedCarriers", nullPointerException);
    } 
    return null;
  }
  
  @SystemApi
  public void setCarrierDataEnabled(boolean paramBoolean) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int i = getSubId(SubscriptionManager.getDefaultDataSubscriptionId());
        iTelephony.carrierActionSetMeteredApnsEnabled(i, paramBoolean);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#setCarrierDataEnabled", (Throwable)remoteException);
    } 
  }
  
  @SystemApi
  public void setRadioEnabled(boolean paramBoolean) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int i = getSubId(SubscriptionManager.getDefaultDataSubscriptionId());
        iTelephony.carrierActionSetRadioEnabled(i, paramBoolean);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#carrierActionSetRadioEnabled", (Throwable)remoteException);
    } 
  }
  
  @SystemApi
  public void reportDefaultNetworkStatus(boolean paramBoolean) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int i = getSubId(SubscriptionManager.getDefaultDataSubscriptionId());
        iTelephony.carrierActionReportDefaultNetworkStatus(i, paramBoolean);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#carrierActionReportDefaultNetworkStatus", (Throwable)remoteException);
    } 
  }
  
  @SystemApi
  public void resetAllCarrierActions() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int i = getSubId(SubscriptionManager.getDefaultDataSubscriptionId());
        iTelephony.carrierActionResetAll(i);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#carrierActionResetAll", (Throwable)remoteException);
    } 
  }
  
  public void setPolicyDataEnabled(boolean paramBoolean) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.setPolicyDataEnabled(paramBoolean, getSubId()); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#setPolicyDataEnabled", (Throwable)remoteException);
    } 
  }
  
  public List<ClientRequestStats> getClientRequestStats(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getClientRequestStats(getOpPackageName(), getAttributionTag(), paramInt); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#getClientRequestStats", (Throwable)remoteException);
    } 
    return null;
  }
  
  @SystemApi
  public boolean getEmergencyCallbackMode() {
    return getEmergencyCallbackMode(getSubId());
  }
  
  public boolean getEmergencyCallbackMode(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony == null)
        return false; 
      return iTelephony.getEmergencyCallbackMode(paramInt);
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#getEmergencyCallbackMode", (Throwable)remoteException);
      return false;
    } 
  }
  
  public boolean isManualNetworkSelectionAllowed() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.isManualNetworkSelectionAllowed(getSubId()); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#isManualNetworkSelectionAllowed", (Throwable)remoteException);
    } 
    return true;
  }
  
  public void setAlwaysReportSignalStrength(boolean paramBoolean) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.setAlwaysReportSignalStrength(getSubId(), paramBoolean); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "setAlwaysReportSignalStrength RemoteException", (Throwable)remoteException);
      remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public SignalStrength getSignalStrength() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getSignalStrength(getSubId()); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error calling ITelephony#getSignalStrength", (Throwable)remoteException);
    } 
    return null;
  }
  
  @SystemApi
  public boolean isDataConnectionAllowed() {
    boolean bool = false;
    boolean bool1 = false;
    try {
      int i = getSubId(SubscriptionManager.getDefaultDataSubscriptionId());
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        bool1 = iTelephony.isDataEnabled(i); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "Error isDataConnectionAllowed", (Throwable)remoteException);
      bool1 = bool;
    } 
    return bool1;
  }
  
  public boolean isDataCapable() {
    Context context = this.mContext;
    if (context == null)
      return true; 
    return context.getResources().getBoolean(17891487);
  }
  
  @Deprecated
  public void setCarrierTestOverride(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int i = getSubId();
        iTelephony.setCarrierTestOverride(i, paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramString7, null, null);
      } 
    } catch (RemoteException remoteException) {}
  }
  
  public void setCarrierTestOverride(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int i = getSubId();
        iTelephony.setCarrierTestOverride(i, paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramString7, paramString8, paramString9);
      } 
    } catch (RemoteException remoteException) {}
  }
  
  public int getCarrierIdListVersion() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getCarrierIdListVersion(getSubId()); 
    } catch (RemoteException remoteException) {}
    return -1;
  }
  
  public int getNumberOfModemsWithSimultaneousDataConnections() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        null = getSubId();
        String str1 = getOpPackageName(), str2 = getAttributionTag();
        return iTelephony.getNumberOfModemsWithSimultaneousDataConnections(null, str1, str2);
      } 
    } catch (RemoteException remoteException) {}
    return 0;
  }
  
  @SystemApi
  public boolean setOpportunisticNetworkState(boolean paramBoolean) {
    String str;
    Context context = this.mContext;
    if (context != null) {
      str = context.getOpPackageName();
    } else {
      str = "<unknown>";
    } 
    boolean bool = false;
    boolean bool1 = false;
    try {
      IOns iOns = getIOns();
      if (iOns != null)
        bool1 = iOns.setEnable(paramBoolean, str); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "enableOpportunisticNetwork RemoteException", (Throwable)remoteException);
      bool1 = bool;
    } 
    return bool1;
  }
  
  @SystemApi
  public boolean isOpportunisticNetworkEnabled() {
    String str;
    Context context = this.mContext;
    if (context != null) {
      str = context.getOpPackageName();
    } else {
      str = "<unknown>";
    } 
    boolean bool = false;
    boolean bool1 = false;
    try {
      IOns iOns = getIOns();
      if (iOns != null)
        bool1 = iOns.isEnabled(str); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "enableOpportunisticNetwork RemoteException", (Throwable)remoteException);
      bool1 = bool;
    } 
    return bool1;
  }
  
  @SystemApi
  public long getSupportedRadioAccessFamily() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int i = iTelephony.getRadioAccessFamily(getSlotIndex(), getOpPackageName());
        return i;
      } 
      return 0L;
    } catch (RemoteException remoteException) {
      return 0L;
    } catch (NullPointerException nullPointerException) {
      return 0L;
    } 
  }
  
  @SystemApi
  public void notifyOtaEmergencyNumberDbInstalled() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        iTelephony.notifyOtaEmergencyNumberDbInstalled();
      } else {
        IllegalStateException illegalStateException = new IllegalStateException();
        this("telephony service is null.");
        throw illegalStateException;
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "notifyOtaEmergencyNumberDatabaseInstalled RemoteException", (Throwable)remoteException);
      remoteException.rethrowAsRuntimeException();
    } 
  }
  
  @SystemApi
  public void updateOtaEmergencyNumberDbFilePath(ParcelFileDescriptor paramParcelFileDescriptor) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        iTelephony.updateOtaEmergencyNumberDbFilePath(paramParcelFileDescriptor);
      } else {
        IllegalStateException illegalStateException = new IllegalStateException();
        this("telephony service is null.");
        throw illegalStateException;
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "updateOtaEmergencyNumberDbFilePath RemoteException", (Throwable)remoteException);
      remoteException.rethrowAsRuntimeException();
    } 
  }
  
  @SystemApi
  public void resetOtaEmergencyNumberDbFilePath() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        iTelephony.resetOtaEmergencyNumberDbFilePath();
      } else {
        IllegalStateException illegalStateException = new IllegalStateException();
        this("telephony service is null.");
        throw illegalStateException;
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "resetOtaEmergencyNumberDbFilePath RemoteException", (Throwable)remoteException);
      remoteException.rethrowAsRuntimeException();
    } 
  }
  
  @SystemApi
  public boolean isEmergencyAssistanceEnabled() {
    this.mContext.enforceCallingOrSelfPermission("android.permission.READ_PRIVILEGED_PHONE_STATE", "isEmergencyAssistanceEnabled");
    return true;
  }
  
  public Map<Integer, List<EmergencyNumber>> getEmergencyNumberList() {
    HashMap<Object, Object> hashMap = new HashMap<>();
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        String str1 = this.mContext.getOpPackageName();
        Context context = this.mContext;
        String str2 = context.getAttributionTag();
        return iTelephony.getEmergencyNumberList(str1, str2);
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("telephony service is null.");
      throw illegalStateException;
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "getEmergencyNumberList RemoteException", (Throwable)remoteException);
      remoteException.rethrowAsRuntimeException();
      return (Map)hashMap;
    } 
  }
  
  public Map<Integer, List<EmergencyNumber>> getEmergencyNumberList(int paramInt) {
    Map<Integer, List<EmergencyNumber>> map;
    HashMap<Object, Object> hashMap1 = new HashMap<>();
    HashMap<Object, Object> hashMap2 = hashMap1;
    try {
      Map<Integer, List<EmergencyNumber>> map1;
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        hashMap2 = hashMap1;
        Context context = this.mContext;
        hashMap2 = hashMap1;
        String str2 = context.getOpPackageName();
        hashMap2 = hashMap1;
        String str1 = this.mContext.getAttributionTag();
        hashMap2 = hashMap1;
        map1 = iTelephony.getEmergencyNumberList(str2, str1);
        if (map1 != null) {
          Map<Integer, List<EmergencyNumber>> map2 = map1;
          Iterator<Integer> iterator = map1.keySet().iterator();
          label37: while (true) {
            map2 = map1;
            if (iterator.hasNext()) {
              map2 = map1;
              Integer integer = iterator.next();
              map2 = map1;
              List list = (List)map1.get(integer);
              map2 = map1;
              Iterator<EmergencyNumber> iterator1 = list.iterator();
              while (true) {
                map2 = map1;
                if (iterator1.hasNext()) {
                  map2 = map1;
                  EmergencyNumber emergencyNumber = iterator1.next();
                  map2 = map1;
                  if (!emergencyNumber.isInEmergencyServiceCategories(paramInt)) {
                    map2 = map1;
                    list.remove(emergencyNumber);
                  } 
                  continue;
                } 
                continue label37;
              } 
            } 
            break;
          } 
        } 
        return map1;
      } 
      map = map1;
      IllegalStateException illegalStateException = new IllegalStateException();
      map = map1;
      this("telephony service is null.");
      map = map1;
      throw illegalStateException;
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "getEmergencyNumberList with Categories RemoteException", (Throwable)remoteException);
      remoteException.rethrowAsRuntimeException();
      return map;
    } 
  }
  
  public boolean isEmergencyNumber(String paramString) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.isEmergencyNumber(paramString, true); 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("telephony service is null.");
      throw illegalStateException;
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "isEmergencyNumber RemoteException", (Throwable)remoteException);
      remoteException.rethrowAsRuntimeException();
      return false;
    } 
  }
  
  @SystemApi
  public boolean isPotentialEmergencyNumber(String paramString) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.isEmergencyNumber(paramString, false); 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("telephony service is null.");
      throw illegalStateException;
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "isEmergencyNumber RemoteException", (Throwable)remoteException);
      remoteException.rethrowAsRuntimeException();
      return false;
    } 
  }
  
  @SystemApi
  public int getEmergencyNumberDbVersion() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getEmergencyNumberDbVersion(getSubId()); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "getEmergencyNumberDbVersion RemoteException", (Throwable)remoteException);
      remoteException.rethrowAsRuntimeException();
    } 
    return -1;
  }
  
  public void setPreferredOpportunisticDataSubscription(int paramInt, boolean paramBoolean, Executor paramExecutor, Consumer<Integer> paramConsumer) {
    Context context = this.mContext;
    if (context != null) {
      String str = context.getOpPackageName();
    } else {
      String str = "<unknown>";
    } 
    try {
      _$$Lambda$TelephonyManager$bFqGX37e1Rs_GfEX9XeyjT1t0Ug _$$Lambda$TelephonyManager$bFqGX37e1Rs_GfEX9XeyjT1t0Ug;
      IOns iOns = getIOns();
      if (iOns == null) {
        if (paramExecutor == null || paramConsumer == null)
          return; 
        long l = Binder.clearCallingIdentity();
        try {
          _$$Lambda$TelephonyManager$bFqGX37e1Rs_GfEX9XeyjT1t0Ug = new _$$Lambda$TelephonyManager$bFqGX37e1Rs_GfEX9XeyjT1t0Ug();
          this(paramConsumer);
          paramExecutor.execute(_$$Lambda$TelephonyManager$bFqGX37e1Rs_GfEX9XeyjT1t0Ug);
          return;
        } finally {
          Binder.restoreCallingIdentity(l);
        } 
      } 
      Object object = new Object();
      super(this, paramExecutor, paramConsumer);
      iOns.setPreferredDataSubscriptionId(paramInt, paramBoolean, (ISetOpportunisticDataCallback)object, (String)_$$Lambda$TelephonyManager$bFqGX37e1Rs_GfEX9XeyjT1t0Ug);
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "setPreferredDataSubscriptionId RemoteException", (Throwable)remoteException);
    } 
  }
  
  public int getPreferredOpportunisticDataSubscription() {
    String str;
    byte b2;
    Context context1 = this.mContext;
    if (context1 != null) {
      str = context1.getOpPackageName();
    } else {
      str = "<unknown>";
    } 
    Context context2 = this.mContext;
    if (context2 != null) {
      String str1 = context2.getAttributionTag();
    } else {
      context2 = null;
    } 
    byte b1 = -1;
    try {
      IOns iOns = getIOns();
      b2 = b1;
      if (iOns != null)
        b2 = iOns.getPreferredDataSubscriptionId(str, (String)context2); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "getPreferredDataSubscriptionId RemoteException", (Throwable)remoteException);
      b2 = b1;
    } 
    return b2;
  }
  
  public void updateAvailableNetworks(List<AvailableNetworkInfo> paramList, Executor paramExecutor, Consumer<Integer> paramConsumer) {
    String str;
    Context context = this.mContext;
    if (context != null) {
      str = context.getOpPackageName();
    } else {
      str = "<unknown>";
    } 
    try {
      IOns iOns = getIOns();
      if (iOns == null || paramList == null) {
        if (paramExecutor == null || paramConsumer == null)
          return; 
        if (iOns == null) {
          long l = Binder.clearCallingIdentity();
          try {
            _$$Lambda$TelephonyManager$5Pi5a8OFp33Kx3BKVYB1lPE94F8 _$$Lambda$TelephonyManager$5Pi5a8OFp33Kx3BKVYB1lPE94F8 = new _$$Lambda$TelephonyManager$5Pi5a8OFp33Kx3BKVYB1lPE94F8();
            this(paramConsumer);
            paramExecutor.execute(_$$Lambda$TelephonyManager$5Pi5a8OFp33Kx3BKVYB1lPE94F8);
          } finally {
            Binder.restoreCallingIdentity(l);
          } 
        } else {
          long l = Binder.clearCallingIdentity();
          try {
            _$$Lambda$TelephonyManager$vzt8oYkDmrz31ou3_D2_gE5oG7s _$$Lambda$TelephonyManager$vzt8oYkDmrz31ou3_D2_gE5oG7s = new _$$Lambda$TelephonyManager$vzt8oYkDmrz31ou3_D2_gE5oG7s();
            this(paramConsumer);
            paramExecutor.execute(_$$Lambda$TelephonyManager$vzt8oYkDmrz31ou3_D2_gE5oG7s);
            return;
          } finally {
            Binder.restoreCallingIdentity(l);
          } 
        } 
        return;
      } 
      Object object = new Object();
      super(this, paramExecutor, paramConsumer);
      iOns.updateAvailableNetworks(paramList, (IUpdateAvailableNetworksCallback)object, str);
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "updateAvailableNetworks RemoteException", (Throwable)remoteException);
    } 
  }
  
  @SystemApi
  public boolean enableModemForSlot(int paramInt, boolean paramBoolean) {
    boolean bool = false;
    boolean bool1 = false;
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        bool1 = iTelephony.enableModemForSlot(paramInt, paramBoolean); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "enableModem RemoteException", (Throwable)remoteException);
      bool1 = bool;
    } 
    return bool1;
  }
  
  public boolean isModemEnabledForSlot(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        String str1 = this.mContext.getOpPackageName();
        Context context = this.mContext;
        String str2 = context.getAttributionTag();
        return iTelephony.isModemEnabledForSlot(paramInt, str1, str2);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "enableModem RemoteException", (Throwable)remoteException);
    } 
    return false;
  }
  
  @SystemApi
  public void setMultiSimCarrierRestriction(boolean paramBoolean) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.setMultiSimCarrierRestriction(paramBoolean); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "setMultiSimCarrierRestriction RemoteException", (Throwable)remoteException);
    } 
  }
  
  public int isMultiSimSupported() {
    if (getSupportedModemCount() < 2)
      return 1; 
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.isMultiSimSupported(getOpPackageName(), getAttributionTag()); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "isMultiSimSupported RemoteException", (Throwable)remoteException);
    } 
    return 1;
  }
  
  public void switchMultiSimConfig(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.switchMultiSimConfig(paramInt); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "switchMultiSimConfig RemoteException", (Throwable)remoteException);
    } 
  }
  
  public boolean doesSwitchMultiSimConfigTriggerReboot() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int i = getSubId();
        String str1 = getOpPackageName(), str2 = getAttributionTag();
        return iTelephony.doesSwitchMultiSimConfigTriggerReboot(i, str1, str2);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "doesSwitchMultiSimConfigTriggerReboot RemoteException", (Throwable)remoteException);
    } 
    return false;
  }
  
  public Pair<Integer, Integer> getRadioHalVersion() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null) {
        int i = iTelephony.getRadioHalVersion();
        if (i == -1)
          return new Pair<>(Integer.valueOf(-1), Integer.valueOf(-1)); 
        return new Pair<>(Integer.valueOf(i / 100), Integer.valueOf(i % 100));
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "getRadioHalVersion() RemoteException", (Throwable)remoteException);
    } 
    return new Pair<>(Integer.valueOf(-1), Integer.valueOf(-1));
  }
  
  @SystemApi
  public int getCarrierPrivilegeStatus(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getCarrierPrivilegeStatusForUid(getSubId(), paramInt); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "getCarrierPrivilegeStatus RemoteException", (Throwable)remoteException);
    } 
    return 0;
  }
  
  public List<ApnSetting> getDevicePolicyOverrideApns(Context paramContext) {
    Cursor cursor = paramContext.getContentResolver().query(Telephony.Carriers.DPC_URI, null, null, null, null);
    if (cursor == null)
      try {
        return (List)Collections.emptyList();
      } finally {
        if (cursor != null)
          try {
            cursor.close();
          } finally {
            cursor = null;
          }  
      }  
    ArrayList<ApnSetting> arrayList = new ArrayList();
    this();
    cursor.moveToPosition(-1);
    while (cursor.moveToNext()) {
      ApnSetting apnSetting = ApnSetting.makeApnSetting(cursor);
      arrayList.add(apnSetting);
    } 
    if (cursor != null)
      cursor.close(); 
    return arrayList;
  }
  
  public int addDevicePolicyOverrideApn(Context paramContext, ApnSetting paramApnSetting) {
    Uri uri = paramContext.getContentResolver().insert(Telephony.Carriers.DPC_URI, paramApnSetting.toContentValues());
    byte b = -1;
    int i = b;
    if (uri != null)
      try {
        i = Integer.parseInt(uri.getLastPathSegment());
      } catch (NumberFormatException numberFormatException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to parse inserted override APN id: ");
        stringBuilder.append(uri.getLastPathSegment());
        String str = stringBuilder.toString();
        Rlog.e("TelephonyManager", str);
        i = b;
      }  
    return i;
  }
  
  public boolean modifyDevicePolicyOverrideApn(Context paramContext, int paramInt, ApnSetting paramApnSetting) {
    boolean bool;
    ContentResolver contentResolver = paramContext.getContentResolver();
    Uri uri = Telephony.Carriers.DPC_URI;
    uri = Uri.withAppendedPath(uri, Integer.toString(paramInt));
    ContentValues contentValues = paramApnSetting.toContentValues();
    if (contentResolver.update(uri, contentValues, null, null) > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @SystemApi
  public boolean isDataEnabledForApn(int paramInt) {
    String str;
    Context context = this.mContext;
    if (context != null) {
      str = context.getOpPackageName();
    } else {
      str = "<unknown>";
    } 
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.isDataEnabledForApn(paramInt, getSubId(), str); 
    } catch (RemoteException remoteException) {
      if (!isSystemProcess())
        remoteException.rethrowAsRuntimeException(); 
    } 
    return false;
  }
  
  @SystemApi
  public boolean isApnMetered(int paramInt) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.isApnMetered(paramInt, getSubId()); 
    } catch (RemoteException remoteException) {
      if (!isSystemProcess())
        remoteException.rethrowAsRuntimeException(); 
    } 
    return true;
  }
  
  @SystemApi
  public void setSystemSelectionChannels(List<RadioAccessSpecifier> paramList, Executor paramExecutor, Consumer<Boolean> paramConsumer) {
    Objects.requireNonNull(paramList, "Specifiers must not be null.");
    Objects.requireNonNull(paramExecutor, "Executor must not be null.");
    Objects.requireNonNull(paramConsumer, "Callback must not be null.");
    setSystemSelectionChannelsInternal(paramList, paramExecutor, paramConsumer);
  }
  
  @SystemApi
  public void setSystemSelectionChannels(List<RadioAccessSpecifier> paramList) {
    Objects.requireNonNull(paramList, "Specifiers must not be null.");
    setSystemSelectionChannelsInternal(paramList, null, null);
  }
  
  private void setSystemSelectionChannelsInternal(List<RadioAccessSpecifier> paramList, Executor paramExecutor, Consumer<Boolean> paramConsumer) {
    Object object;
    if (paramConsumer == null) {
      paramExecutor = null;
    } else {
      object = new Object(this, paramExecutor, paramConsumer);
    } 
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.setSystemSelectionChannels(paramList, getSubId(), (IBooleanConsumer)object); 
    } catch (RemoteException remoteException) {
      if (!isSystemProcess())
        remoteException.rethrowAsRuntimeException(); 
    } 
  }
  
  @SystemApi
  public boolean matchesCurrentSimOperator(String paramString1, int paramInt, String paramString2) {
    try {
      if (!paramString1.equals(getSimOperator()))
        return false; 
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.isMvnoMatched(getSubId(), paramInt, paramString2); 
    } catch (RemoteException remoteException) {
      if (!isSystemProcess())
        remoteException.rethrowAsRuntimeException(); 
    } 
    return false;
  }
  
  public CallForwardingInfo getCallForwarding(int paramInt) {
    if (paramInt >= 0 && paramInt <= 5) {
      try {
        ITelephony iTelephony = getITelephony();
        if (iTelephony != null)
          return iTelephony.getCallForwarding(getSubId(), paramInt); 
      } catch (RemoteException remoteException) {
        Rlog.e("TelephonyManager", "getCallForwarding RemoteException", (Throwable)remoteException);
      } catch (NullPointerException nullPointerException) {
        Rlog.e("TelephonyManager", "getCallForwarding NPE", nullPointerException);
      } 
      return new CallForwardingInfo(3, 0, null, 0);
    } 
    throw new IllegalArgumentException("callForwardingReason is out of range");
  }
  
  public boolean setCallForwarding(CallForwardingInfo paramCallForwardingInfo) {
    if (paramCallForwardingInfo != null) {
      int i = paramCallForwardingInfo.getStatus();
      if (i == 1 || i == 0) {
        i = paramCallForwardingInfo.getReason();
        if (i >= 0 && i <= 5) {
          if (paramCallForwardingInfo.getNumber() != null) {
            if (paramCallForwardingInfo.getTimeoutSeconds() > 0) {
              try {
                ITelephony iTelephony = getITelephony();
                if (iTelephony != null)
                  return iTelephony.setCallForwarding(getSubId(), paramCallForwardingInfo); 
              } catch (RemoteException remoteException) {
                Rlog.e("TelephonyManager", "setCallForwarding RemoteException", (Throwable)remoteException);
              } catch (NullPointerException nullPointerException) {
                Rlog.e("TelephonyManager", "setCallForwarding NPE", nullPointerException);
              } 
              return false;
            } 
            throw new IllegalArgumentException("callForwarding timeout isn't positive");
          } 
          throw new IllegalArgumentException("callForwarding number is null");
        } 
        throw new IllegalArgumentException("callForwardingReason is out of range");
      } 
      throw new IllegalArgumentException("callForwardingStatus is neither active nor inactive");
    } 
    throw new IllegalArgumentException("callForwardingInfo is null");
  }
  
  public int getCallWaitingStatus() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.getCallWaitingStatus(getSubId()); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "getCallWaitingStatus RemoteException", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Rlog.e("TelephonyManager", "getCallWaitingStatus NPE", nullPointerException);
    } 
    return 3;
  }
  
  public boolean setCallWaitingStatus(boolean paramBoolean) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.setCallWaitingStatus(getSubId(), paramBoolean); 
    } catch (RemoteException remoteException) {
      Rlog.e("TelephonyManager", "setCallWaitingStatus RemoteException", (Throwable)remoteException);
    } catch (NullPointerException nullPointerException) {
      Rlog.e("TelephonyManager", "setCallWaitingStatus NPE", nullPointerException);
    } 
    return false;
  }
  
  public boolean setDataAllowedDuringVoiceCall(boolean paramBoolean) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.setDataAllowedDuringVoiceCall(getSubId(), paramBoolean); 
    } catch (RemoteException remoteException) {
      if (!isSystemProcess())
        remoteException.rethrowAsRuntimeException(); 
    } 
    return false;
  }
  
  public boolean isDataAllowedInVoiceCall() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.isDataAllowedInVoiceCall(getSubId()); 
    } catch (RemoteException remoteException) {
      if (!isSystemProcess())
        remoteException.rethrowAsRuntimeException(); 
    } 
    return false;
  }
  
  public boolean setAlwaysAllowMmsData(boolean paramBoolean) {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.setAlwaysAllowMmsData(getSubId(), paramBoolean); 
    } catch (RemoteException remoteException) {
      if (!isSystemProcess())
        remoteException.rethrowAsRuntimeException(); 
    } 
    return false;
  }
  
  @SystemApi
  public boolean isIccLockEnabled() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.isIccLockEnabled(getSubId()); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "isIccLockEnabled RemoteException", (Throwable)remoteException);
    } 
    return false;
  }
  
  public int setIccLockEnabled(boolean paramBoolean, String paramString) {
    Preconditions.checkNotNull(paramString, "setIccLockEnabled password can't be null.");
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.setIccLockEnabled(getSubId(), paramBoolean, paramString); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "setIccLockEnabled RemoteException", (Throwable)remoteException);
    } 
    return 0;
  }
  
  public int changeIccLockPassword(String paramString1, String paramString2) {
    Preconditions.checkNotNull(paramString1, "changeIccLockPassword oldPassword can't be null.");
    Preconditions.checkNotNull(paramString2, "changeIccLockPassword newPassword can't be null.");
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        return iTelephony.changeIccLockPassword(getSubId(), paramString1, paramString2); 
    } catch (RemoteException remoteException) {
      Log.e("TelephonyManager", "changeIccLockPassword RemoteException", (Throwable)remoteException);
    } 
    return 0;
  }
  
  public void notifyUserActivity() {
    try {
      ITelephony iTelephony = getITelephony();
      if (iTelephony != null)
        iTelephony.userActivity(); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("notifyUserActivity exception: ");
      stringBuilder.append(remoteException.getMessage());
      Log.w("TelephonyManager", stringBuilder.toString());
    } 
  }
  
  class DeathRecipient implements IBinder.DeathRecipient {
    private DeathRecipient() {}
    
    public void binderDied() {
      TelephonyManager.resetServiceCache();
    }
  }
  
  private static void resetServiceCache() {
    synchronized (sCacheLock) {
      if (sISub != null) {
        sISub.asBinder().unlinkToDeath(sServiceDeath, 0);
        sISub = null;
        SubscriptionManager.clearCaches();
      } 
      if (sISms != null) {
        sISms.asBinder().unlinkToDeath(sServiceDeath, 0);
        sISms = null;
      } 
      if (sIPhoneSubInfo != null) {
        sIPhoneSubInfo.asBinder().unlinkToDeath(sServiceDeath, 0);
        sIPhoneSubInfo = null;
      } 
      return;
    } 
  }
  
  static IPhoneSubInfo getSubscriberInfoService() {
    if (!sServiceHandleCacheEnabled) {
      TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
      TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getPhoneSubServiceRegisterer();
      IBinder iBinder = serviceRegisterer.get();
      return IPhoneSubInfo.Stub.asInterface(iBinder);
    } 
    if (sIPhoneSubInfo == null) {
      TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
      TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getPhoneSubServiceRegisterer();
      IBinder iBinder = serviceRegisterer.get();
      null = IPhoneSubInfo.Stub.asInterface(iBinder);
      synchronized (sCacheLock) {
        IPhoneSubInfo iPhoneSubInfo = sIPhoneSubInfo;
        if (iPhoneSubInfo == null && null != null)
          try {
            sIPhoneSubInfo = null;
            null.asBinder().linkToDeath(sServiceDeath, 0);
          } catch (Exception exception) {
            sIPhoneSubInfo = null;
          }  
      } 
    } 
    return sIPhoneSubInfo;
  }
  
  static ISub getSubscriptionService() {
    if (!sServiceHandleCacheEnabled) {
      TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
      TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getSubscriptionServiceRegisterer();
      IBinder iBinder = serviceRegisterer.get();
      return ISub.Stub.asInterface(iBinder);
    } 
    if (sISub == null) {
      TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
      TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getSubscriptionServiceRegisterer();
      IBinder iBinder = serviceRegisterer.get();
      null = ISub.Stub.asInterface(iBinder);
      synchronized (sCacheLock) {
        ISub iSub = sISub;
        if (iSub == null && null != null)
          try {
            sISub = null;
            null.asBinder().linkToDeath(sServiceDeath, 0);
          } catch (Exception exception) {
            sISub = null;
          }  
      } 
    } 
    return sISub;
  }
  
  static ISms getSmsService() {
    if (!sServiceHandleCacheEnabled) {
      TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
      TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getSmsServiceRegisterer();
      IBinder iBinder = serviceRegisterer.get();
      return ISms.Stub.asInterface(iBinder);
    } 
    if (sISms == null) {
      TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
      TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getSmsServiceRegisterer();
      IBinder iBinder = serviceRegisterer.get();
      null = ISms.Stub.asInterface(iBinder);
      synchronized (sCacheLock) {
        ISms iSms = sISms;
        if (iSms == null && null != null)
          try {
            sISms = null;
            null.asBinder().linkToDeath(sServiceDeath, 0);
          } catch (Exception exception) {
            sISms = null;
          }  
      } 
    } 
    return sISms;
  }
  
  public static void disableServiceHandleCaching() {
    sServiceHandleCacheEnabled = false;
  }
  
  public static void enableServiceHandleCaching() {
    sServiceHandleCacheEnabled = true;
  }
  
  public static int getNetworkClass(int paramInt) {
    switch (paramInt) {
      default:
        return 0;
      case 20:
        return 4;
      case 13:
      case 18:
      case 19:
        return 3;
      case 3:
      case 5:
      case 6:
      case 8:
      case 9:
      case 10:
      case 12:
      case 14:
      case 15:
      case 17:
        return 2;
      case 1:
      case 2:
      case 4:
      case 7:
      case 11:
      case 16:
        break;
    } 
    return 1;
  }
  
  public boolean canConnectTo5GInDsdsMode() {
    ITelephony iTelephony = getITelephony();
    if (iTelephony == null)
      return true; 
    try {
      return iTelephony.canConnectTo5GInDsdsMode();
    } catch (RemoteException remoteException) {
      return true;
    } catch (NullPointerException nullPointerException) {
      return true;
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class AllowedNetworkTypesReason implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class CallWaitingStatus implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class CdmaRoamingMode implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class CdmaSubscription implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class DataState implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class DefaultSubscriptionSelectType implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class IsMultiSimSupportedResult implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface KeyType {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface NetworkSelectionMode {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface NetworkTypeBitMask {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface PrefNetworkMode {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SetCarrierRestrictionResult {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SetOpportunisticSubscriptionResult {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SimCombinationWarningType {}
  
  public static @interface SimState {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface UpdateAvailableNetworksResult {}
  
  public static interface WifiCallingChoices {
    public static final int ALWAYS_USE = 0;
    
    public static final int ASK_EVERY_TIME = 1;
    
    public static final int NEVER_USE = 2;
  }
}
