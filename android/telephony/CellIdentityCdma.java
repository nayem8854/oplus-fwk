package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.cdma.CdmaCellLocation;
import java.util.Objects;

public final class CellIdentityCdma extends CellIdentity {
  private static final int BASESTATION_ID_MAX = 65535;
  
  public static final Parcelable.Creator<CellIdentityCdma> CREATOR;
  
  private static final boolean DBG = false;
  
  private static final int LATITUDE_MAX = 1296000;
  
  private static final int LATITUDE_MIN = -1296000;
  
  private static final int LONGITUDE_MAX = 2592000;
  
  private static final int LONGITUDE_MIN = -2592000;
  
  private static final int NETWORK_ID_MAX = 65535;
  
  private static final int SYSTEM_ID_MAX = 32767;
  
  private static final String TAG = CellIdentityCdma.class.getSimpleName();
  
  private final int mBasestationId;
  
  private final int mLatitude;
  
  private final int mLongitude;
  
  private final int mNetworkId;
  
  private final int mSystemId;
  
  public CellIdentityCdma() {
    super(TAG, 2, null, null, null, null);
    this.mNetworkId = Integer.MAX_VALUE;
    this.mSystemId = Integer.MAX_VALUE;
    this.mBasestationId = Integer.MAX_VALUE;
    this.mLongitude = Integer.MAX_VALUE;
    this.mLatitude = Integer.MAX_VALUE;
    this.mGlobalCellId = null;
  }
  
  public CellIdentityCdma(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, String paramString1, String paramString2) {
    super(TAG, 2, null, null, paramString1, paramString2);
    this.mNetworkId = inRangeOrUnavailable(paramInt1, 0, 65535);
    this.mSystemId = inRangeOrUnavailable(paramInt2, 0, 32767);
    this.mBasestationId = inRangeOrUnavailable(paramInt3, 0, 65535);
    paramInt1 = inRangeOrUnavailable(paramInt5, -1296000, 1296000);
    paramInt2 = inRangeOrUnavailable(paramInt4, -2592000, 2592000);
    if (!isNullIsland(paramInt1, paramInt2)) {
      this.mLongitude = paramInt2;
      this.mLatitude = paramInt1;
    } else {
      this.mLatitude = Integer.MAX_VALUE;
      this.mLongitude = Integer.MAX_VALUE;
    } 
    updateGlobalCellId();
  }
  
  public CellIdentityCdma(android.hardware.radio.V1_0.CellIdentityCdma paramCellIdentityCdma) {
    this(paramCellIdentityCdma.networkId, paramCellIdentityCdma.systemId, paramCellIdentityCdma.baseStationId, paramCellIdentityCdma.longitude, paramCellIdentityCdma.latitude, "", "");
  }
  
  public CellIdentityCdma(android.hardware.radio.V1_2.CellIdentityCdma paramCellIdentityCdma) {
    this(paramCellIdentityCdma.base.networkId, paramCellIdentityCdma.base.systemId, paramCellIdentityCdma.base.baseStationId, paramCellIdentityCdma.base.longitude, paramCellIdentityCdma.base.latitude, paramCellIdentityCdma.operatorNames.alphaLong, paramCellIdentityCdma.operatorNames.alphaShort);
  }
  
  private CellIdentityCdma(CellIdentityCdma paramCellIdentityCdma) {
    this(paramCellIdentityCdma.mNetworkId, paramCellIdentityCdma.mSystemId, paramCellIdentityCdma.mBasestationId, paramCellIdentityCdma.mLongitude, paramCellIdentityCdma.mLatitude, paramCellIdentityCdma.mAlphaLong, paramCellIdentityCdma.mAlphaShort);
  }
  
  CellIdentityCdma copy() {
    return new CellIdentityCdma(this);
  }
  
  public CellIdentityCdma sanitizeLocationInfo() {
    return new CellIdentityCdma(2147483647, 2147483647, 2147483647, 2147483647, 2147483647, this.mAlphaLong, this.mAlphaShort);
  }
  
  protected void updateGlobalCellId() {
    this.mGlobalCellId = null;
    if (this.mNetworkId != Integer.MAX_VALUE) {
      int i = this.mSystemId;
      if (i != Integer.MAX_VALUE && this.mBasestationId != Integer.MAX_VALUE) {
        this.mGlobalCellId = String.format("%04x%04x%04x", new Object[] { Integer.valueOf(i), Integer.valueOf(this.mNetworkId), Integer.valueOf(this.mBasestationId) });
        return;
      } 
    } 
  }
  
  private boolean isNullIsland(int paramInt1, int paramInt2) {
    paramInt1 = Math.abs(paramInt1);
    boolean bool = true;
    if (paramInt1 > 1 || Math.abs(paramInt2) > 1)
      bool = false; 
    return bool;
  }
  
  public int getNetworkId() {
    return this.mNetworkId;
  }
  
  public int getSystemId() {
    return this.mSystemId;
  }
  
  public int getBasestationId() {
    return this.mBasestationId;
  }
  
  public int getLongitude() {
    return this.mLongitude;
  }
  
  public int getLatitude() {
    return this.mLatitude;
  }
  
  public int hashCode() {
    int i = this.mNetworkId, j = this.mSystemId, k = this.mBasestationId, m = this.mLatitude, n = this.mLongitude;
    int i1 = super.hashCode();
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1) });
  }
  
  public CdmaCellLocation asCellLocation() {
    CdmaCellLocation cdmaCellLocation = new CdmaCellLocation();
    int i = this.mBasestationId;
    if (i == Integer.MAX_VALUE)
      i = -1; 
    int j = this.mSystemId;
    if (j == Integer.MAX_VALUE)
      j = -1; 
    int k = this.mNetworkId;
    if (k == Integer.MAX_VALUE)
      k = -1; 
    cdmaCellLocation.setCellLocationData(i, this.mLatitude, this.mLongitude, j, k);
    return cdmaCellLocation;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof CellIdentityCdma))
      return false; 
    CellIdentityCdma cellIdentityCdma = (CellIdentityCdma)paramObject;
    if (this.mNetworkId == cellIdentityCdma.mNetworkId && this.mSystemId == cellIdentityCdma.mSystemId && this.mBasestationId == cellIdentityCdma.mBasestationId && this.mLatitude == cellIdentityCdma.mLatitude && this.mLongitude == cellIdentityCdma.mLongitude)
      if (super.equals(paramObject))
        return null;  
    return false;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(TAG);
    stringBuilder.append(":{ mNetworkId=");
    stringBuilder.append(this.mNetworkId);
    stringBuilder.append(" mSystemId=");
    stringBuilder.append(this.mSystemId);
    stringBuilder.append(" mBasestationId=");
    stringBuilder.append(this.mBasestationId);
    stringBuilder.append(" mLongitude=");
    stringBuilder.append(this.mLongitude);
    stringBuilder.append(" mLatitude=");
    stringBuilder.append(this.mLatitude);
    stringBuilder.append(" mAlphaLong=");
    stringBuilder.append(this.mAlphaLong);
    stringBuilder.append(" mAlphaShort=");
    stringBuilder.append(this.mAlphaShort);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    super.writeToParcel(paramParcel, 2);
    paramParcel.writeInt(this.mNetworkId);
    paramParcel.writeInt(this.mSystemId);
    paramParcel.writeInt(this.mBasestationId);
    paramParcel.writeInt(this.mLongitude);
    paramParcel.writeInt(this.mLatitude);
  }
  
  private CellIdentityCdma(Parcel paramParcel) {
    super(TAG, 2, paramParcel);
    this.mNetworkId = paramParcel.readInt();
    this.mSystemId = paramParcel.readInt();
    this.mBasestationId = paramParcel.readInt();
    this.mLongitude = paramParcel.readInt();
    this.mLatitude = paramParcel.readInt();
    updateGlobalCellId();
  }
  
  static {
    CREATOR = (Parcelable.Creator<CellIdentityCdma>)new Object();
  }
  
  protected static CellIdentityCdma createFromParcelBody(Parcel paramParcel) {
    return new CellIdentityCdma(paramParcel);
  }
}
