package android.telephony;

import android.os.Looper;
import android.util.Log;

public class OplusPhoneStateListener extends PhoneStateListener {
  public static final int LISTEN_SRVCC_STATE_CHANGED = 16384;
  
  private static final String TAG = "ColorPhoneStateListener";
  
  public OplusPhoneStateListener(Looper paramLooper) {
    super(paramLooper);
  }
  
  public void onSrvccStateChanged(int paramInt) {
    super.onSrvccStateChanged(paramInt);
  }
  
  public void onCallStateChanged(int paramInt, String paramString) {
    super.onCallStateChanged(paramInt, paramString);
  }
  
  public void setSubId(int paramInt) {
    try {
      this.mSubId = Integer.valueOf(paramInt);
    } finally {
      Exception exception = null;
    } 
  }
  
  public int getSubId() {
    try {
      return this.mSubId.intValue();
    } finally {
      Exception exception = null;
      Log.e("ColorPhoneStateListener", exception.toString());
    } 
  }
}
