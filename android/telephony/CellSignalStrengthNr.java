package android.telephony;

import android.hardware.radio.V1_4.NrSignalStrength;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PersistableBundle;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;

public final class CellSignalStrengthNr extends CellSignalStrength implements Parcelable {
  private int[] mSsRsrpThresholds = new int[] { -110, -90, -80, -65 };
  
  private int[] mSsRsrqThresholds = new int[] { -16, -12, -9, -6 };
  
  private int[] mSsSinrThresholds = new int[] { -5, 5, 15, 30 };
  
  public CellSignalStrengthNr() {
    setDefaultValues();
  }
  
  public CellSignalStrengthNr(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    this.mCsiRsrp = inRangeOrUnavailable(paramInt1, -140, -44);
    this.mCsiRsrq = inRangeOrUnavailable(paramInt2, -20, -3);
    this.mCsiSinr = inRangeOrUnavailable(paramInt3, -23, 23);
    this.mSsRsrp = inRangeOrUnavailable(paramInt4, -140, -44);
    this.mSsRsrq = inRangeOrUnavailable(paramInt5, -20, -3);
    this.mSsSinr = inRangeOrUnavailable(paramInt6, -23, 40);
    updateLevel((PersistableBundle)null, (ServiceState)null);
  }
  
  public CellSignalStrengthNr(NrSignalStrength paramNrSignalStrength) {
    this(flip(paramNrSignalStrength.csiRsrp), flip(paramNrSignalStrength.csiRsrq), paramNrSignalStrength.csiSinr, flip(paramNrSignalStrength.ssRsrp), flip(paramNrSignalStrength.ssRsrq), paramNrSignalStrength.ssSinr);
  }
  
  private static int flip(int paramInt) {
    if (paramInt != Integer.MAX_VALUE)
      paramInt = -paramInt; 
    return paramInt;
  }
  
  public int getSsRsrp() {
    return this.mSsRsrp;
  }
  
  public int getSsRsrq() {
    return this.mSsRsrq;
  }
  
  public int getSsSinr() {
    return this.mSsSinr;
  }
  
  public int getCsiRsrp() {
    return this.mCsiRsrp;
  }
  
  public int getCsiRsrq() {
    return this.mCsiRsrq;
  }
  
  public int getCsiSinr() {
    return this.mCsiSinr;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mCsiRsrp);
    paramParcel.writeInt(this.mCsiRsrq);
    paramParcel.writeInt(this.mCsiSinr);
    paramParcel.writeInt(this.mSsRsrp);
    paramParcel.writeInt(this.mSsRsrq);
    paramParcel.writeInt(this.mSsSinr);
    paramParcel.writeInt(this.mLevel);
  }
  
  private CellSignalStrengthNr(Parcel paramParcel) {
    this.mCsiRsrp = paramParcel.readInt();
    this.mCsiRsrq = paramParcel.readInt();
    this.mCsiSinr = paramParcel.readInt();
    this.mSsRsrp = paramParcel.readInt();
    this.mSsRsrq = paramParcel.readInt();
    this.mSsSinr = paramParcel.readInt();
    this.mLevel = paramParcel.readInt();
  }
  
  public void setDefaultValues() {
    this.mCsiRsrp = Integer.MAX_VALUE;
    this.mCsiRsrq = Integer.MAX_VALUE;
    this.mCsiSinr = Integer.MAX_VALUE;
    this.mSsRsrp = Integer.MAX_VALUE;
    this.mSsRsrq = Integer.MAX_VALUE;
    this.mSsSinr = Integer.MAX_VALUE;
    this.mLevel = 0;
    this.mParametersUseForLevel = 1;
  }
  
  public int getLevel() {
    return this.mLevel;
  }
  
  private boolean isLevelForParameter(int paramInt) {
    boolean bool;
    if ((this.mParametersUseForLevel & paramInt) == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void updateLevel(PersistableBundle paramPersistableBundle, ServiceState paramServiceState) {
    if (paramPersistableBundle == null) {
      this.mParametersUseForLevel = 1;
    } else {
      this.mParametersUseForLevel = paramPersistableBundle.getInt("parameters_use_for_5g_nr_signal_bar_int", 1);
      this.mSsRsrpThresholds = paramPersistableBundle.getIntArray("5g_nr_ssrsrp_thresholds_int_array");
      this.mSsRsrqThresholds = paramPersistableBundle.getIntArray("5g_nr_ssrsrq_thresholds_int_array");
      this.mSsSinrThresholds = paramPersistableBundle.getIntArray("5g_nr_sssinr_thresholds_int_array");
    } 
    int i = Integer.MAX_VALUE;
    int j = Integer.MAX_VALUE;
    int k = Integer.MAX_VALUE;
    if (isLevelForParameter(1))
      i = updateLevelWithMeasure(this.mSsRsrp, this.mSsRsrpThresholds); 
    if (isLevelForParameter(2))
      j = updateLevelWithMeasure(this.mSsRsrq, this.mSsRsrqThresholds); 
    if (isLevelForParameter(4))
      k = updateLevelWithMeasure(this.mSsSinr, this.mSsSinrThresholds); 
    this.mLevel = Math.min(Math.min(i, j), k);
  }
  
  private int updateLevelWithMeasure(int paramInt, int[] paramArrayOfint) {
    if (paramInt == Integer.MAX_VALUE) {
      paramInt = 0;
    } else if (paramInt > paramArrayOfint[3]) {
      paramInt = 4;
    } else if (paramInt > paramArrayOfint[2]) {
      paramInt = 3;
    } else if (paramInt > paramArrayOfint[1]) {
      paramInt = 2;
    } else if (paramInt > paramArrayOfint[0]) {
      paramInt = 1;
    } else {
      paramInt = 0;
    } 
    return paramInt;
  }
  
  public int getAsuLevel() {
    int i = getDbm();
    if (i == Integer.MAX_VALUE) {
      i = 99;
    } else if (i <= -140) {
      i = 0;
    } else if (i >= -43) {
      i = 97;
    } else {
      i += 140;
    } 
    return i;
  }
  
  public int getDbm() {
    return this.mSsRsrp;
  }
  
  public CellSignalStrengthNr(CellSignalStrengthNr paramCellSignalStrengthNr) {
    this.mCsiRsrp = paramCellSignalStrengthNr.mCsiRsrp;
    this.mCsiRsrq = paramCellSignalStrengthNr.mCsiRsrq;
    this.mCsiSinr = paramCellSignalStrengthNr.mCsiSinr;
    this.mSsRsrp = paramCellSignalStrengthNr.mSsRsrp;
    this.mSsRsrq = paramCellSignalStrengthNr.mSsRsrq;
    this.mSsSinr = paramCellSignalStrengthNr.mSsSinr;
    this.mLevel = paramCellSignalStrengthNr.mLevel;
    this.mParametersUseForLevel = paramCellSignalStrengthNr.mParametersUseForLevel;
  }
  
  public CellSignalStrengthNr copy() {
    return new CellSignalStrengthNr(this);
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mCsiRsrp), Integer.valueOf(this.mCsiRsrq), Integer.valueOf(this.mCsiSinr), Integer.valueOf(this.mSsRsrp), Integer.valueOf(this.mSsRsrq), Integer.valueOf(this.mSsSinr), Integer.valueOf(this.mLevel) });
  }
  
  private static final CellSignalStrengthNr sInvalid = new CellSignalStrengthNr();
  
  public static final Parcelable.Creator<CellSignalStrengthNr> CREATOR;
  
  private static final String TAG = "CellSignalStrengthNr";
  
  public static final int UNKNOWN_ASU_LEVEL = 99;
  
  public static final int USE_SSRSRP = 1;
  
  public static final int USE_SSRSRQ = 2;
  
  public static final int USE_SSSINR = 4;
  
  private static final boolean VDBG = false;
  
  private int mCsiRsrp;
  
  private int mCsiRsrq;
  
  private int mCsiSinr;
  
  private int mLevel;
  
  private int mParametersUseForLevel;
  
  private int mSsRsrp;
  
  private int mSsRsrq;
  
  private int mSsSinr;
  
  public boolean isValid() {
    return equals(sInvalid) ^ true;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof CellSignalStrengthNr;
    boolean bool1 = false;
    if (bool) {
      paramObject = paramObject;
      bool = bool1;
      if (this.mCsiRsrp == ((CellSignalStrengthNr)paramObject).mCsiRsrp) {
        bool = bool1;
        if (this.mCsiRsrq == ((CellSignalStrengthNr)paramObject).mCsiRsrq) {
          bool = bool1;
          if (this.mCsiSinr == ((CellSignalStrengthNr)paramObject).mCsiSinr) {
            bool = bool1;
            if (this.mSsRsrp == ((CellSignalStrengthNr)paramObject).mSsRsrp) {
              bool = bool1;
              if (this.mSsRsrq == ((CellSignalStrengthNr)paramObject).mSsRsrq) {
                bool = bool1;
                if (this.mSsSinr == ((CellSignalStrengthNr)paramObject).mSsSinr) {
                  bool = bool1;
                  if (this.mLevel == ((CellSignalStrengthNr)paramObject).mLevel)
                    bool = true; 
                } 
              } 
            } 
          } 
        } 
      } 
      return bool;
    } 
    return false;
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("CellSignalStrengthNr:{");
    StringBuilder stringBuilder9 = new StringBuilder();
    stringBuilder9.append(" csiRsrp = ");
    stringBuilder9.append(this.mCsiRsrp);
    String str8 = stringBuilder9.toString();
    stringBuilder1.append(str8);
    StringBuilder stringBuilder8 = new StringBuilder();
    stringBuilder8.append(" csiRsrq = ");
    stringBuilder8.append(this.mCsiRsrq);
    String str7 = stringBuilder8.toString();
    stringBuilder1.append(str7);
    StringBuilder stringBuilder7 = new StringBuilder();
    stringBuilder7.append(" csiSinr = ");
    stringBuilder7.append(this.mCsiSinr);
    String str6 = stringBuilder7.toString();
    stringBuilder1.append(str6);
    StringBuilder stringBuilder6 = new StringBuilder();
    stringBuilder6.append(" ssRsrp = ");
    stringBuilder6.append(this.mSsRsrp);
    String str5 = stringBuilder6.toString();
    stringBuilder1.append(str5);
    StringBuilder stringBuilder5 = new StringBuilder();
    stringBuilder5.append(" ssRsrq = ");
    stringBuilder5.append(this.mSsRsrq);
    String str4 = stringBuilder5.toString();
    stringBuilder1.append(str4);
    StringBuilder stringBuilder4 = new StringBuilder();
    stringBuilder4.append(" ssSinr = ");
    stringBuilder4.append(this.mSsSinr);
    String str3 = stringBuilder4.toString();
    stringBuilder1.append(str3);
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append(" level = ");
    stringBuilder3.append(this.mLevel);
    String str2 = stringBuilder3.toString();
    stringBuilder1.append(str2);
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" parametersUseForLevel = ");
    stringBuilder2.append(this.mParametersUseForLevel);
    String str1 = stringBuilder2.toString();
    stringBuilder1.append(str1);
    stringBuilder1.append(" }");
    return stringBuilder1.toString();
  }
  
  static {
    CREATOR = new Parcelable.Creator<CellSignalStrengthNr>() {
        public CellSignalStrengthNr createFromParcel(Parcel param1Parcel) {
          return new CellSignalStrengthNr(param1Parcel);
        }
        
        public CellSignalStrengthNr[] newArray(int param1Int) {
          return new CellSignalStrengthNr[param1Int];
        }
      };
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class SignalLevelAndReportCriteriaSource implements Annotation {}
}
