package android.telephony;

import android.os.Build;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class Rlog {
  public static final String OPERATOR;
  
  public static final boolean SWITCH_LOG;
  
  private static final boolean USER_BUILD = Build.IS_USER;
  
  public static final boolean VDF_NON_DEBUG;
  
  public static final boolean VDF_OPERATOR;
  
  static {
    boolean bool1 = false;
    SWITCH_LOG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
    String str = SystemProperties.get("ro.vendor.oplus.operator", "OPPO");
    boolean bool = "VODAFONE_EEA".equals(str);
    boolean bool2 = bool1;
    if (!SWITCH_LOG) {
      bool2 = bool1;
      if (bool)
        bool2 = true; 
    } 
    VDF_NON_DEBUG = bool2;
  }
  
  public static int v(String paramString1, String paramString2) {
    if (VDF_NON_DEBUG)
      return 0; 
    return Log.println_native(1, 2, paramString1, paramString2);
  }
  
  public static int v(String paramString1, String paramString2, Throwable paramThrowable) {
    if (VDF_NON_DEBUG)
      return 0; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString2);
    stringBuilder.append('\n');
    stringBuilder.append(Log.getStackTraceString(paramThrowable));
    paramString2 = stringBuilder.toString();
    return Log.println_native(1, 2, paramString1, paramString2);
  }
  
  public static int d(String paramString1, String paramString2) {
    if (VDF_NON_DEBUG)
      return 0; 
    return Log.println_native(1, 3, paramString1, paramString2);
  }
  
  public static int d(String paramString1, String paramString2, Throwable paramThrowable) {
    if (VDF_NON_DEBUG)
      return 0; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString2);
    stringBuilder.append('\n');
    stringBuilder.append(Log.getStackTraceString(paramThrowable));
    paramString2 = stringBuilder.toString();
    return Log.println_native(1, 3, paramString1, paramString2);
  }
  
  public static int i(String paramString1, String paramString2) {
    if (VDF_NON_DEBUG)
      return 0; 
    return Log.println_native(1, 4, paramString1, paramString2);
  }
  
  public static int i(String paramString1, String paramString2, Throwable paramThrowable) {
    if (VDF_NON_DEBUG)
      return 0; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString2);
    stringBuilder.append('\n');
    stringBuilder.append(Log.getStackTraceString(paramThrowable));
    paramString2 = stringBuilder.toString();
    return Log.println_native(1, 4, paramString1, paramString2);
  }
  
  public static int w(String paramString1, String paramString2) {
    if (VDF_NON_DEBUG)
      return 0; 
    return Log.println_native(1, 5, paramString1, paramString2);
  }
  
  public static int w(String paramString1, String paramString2, Throwable paramThrowable) {
    if (VDF_NON_DEBUG)
      return 0; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString2);
    stringBuilder.append('\n');
    stringBuilder.append(Log.getStackTraceString(paramThrowable));
    paramString2 = stringBuilder.toString();
    return Log.println_native(1, 5, paramString1, paramString2);
  }
  
  public static int w(String paramString, Throwable paramThrowable) {
    if (VDF_NON_DEBUG)
      return 0; 
    return Log.println_native(1, 5, paramString, Log.getStackTraceString(paramThrowable));
  }
  
  public static int e(String paramString1, String paramString2) {
    if (VDF_NON_DEBUG)
      return 0; 
    return Log.println_native(1, 6, paramString1, paramString2);
  }
  
  public static int e(String paramString1, String paramString2, Throwable paramThrowable) {
    if (VDF_NON_DEBUG)
      return 0; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString2);
    stringBuilder.append('\n');
    stringBuilder.append(Log.getStackTraceString(paramThrowable));
    paramString2 = stringBuilder.toString();
    return Log.println_native(1, 6, paramString1, paramString2);
  }
  
  public static int println(int paramInt, String paramString1, String paramString2) {
    if (VDF_NON_DEBUG)
      return 0; 
    return Log.println_native(1, paramInt, paramString1, paramString2);
  }
  
  public static boolean isLoggable(String paramString, int paramInt) {
    if (VDF_NON_DEBUG)
      return false; 
    return Log.isLoggable(paramString, paramInt);
  }
  
  public static String pii(String paramString, Object paramObject) {
    String str = String.valueOf(paramObject);
    if (paramObject == null || TextUtils.isEmpty(str) || isLoggable(paramString, 2))
      return str; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[");
    stringBuilder.append(secureHash(str.getBytes()));
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public static String pii(boolean paramBoolean, Object paramObject) {
    String str = String.valueOf(paramObject);
    if (paramObject == null || TextUtils.isEmpty(str) || paramBoolean)
      return str; 
    paramObject = new StringBuilder();
    paramObject.append("[");
    paramObject.append(secureHash(str.getBytes()));
    paramObject.append("]");
    return paramObject.toString();
  }
  
  private static String secureHash(byte[] paramArrayOfbyte) {
    if (USER_BUILD)
      return "****"; 
    try {
      MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
      paramArrayOfbyte = messageDigest.digest(paramArrayOfbyte);
      return Base64.encodeToString(paramArrayOfbyte, 11);
    } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
      return "####";
    } 
  }
}
