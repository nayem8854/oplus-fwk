package android.telephony;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;

@SystemApi
public class UiccSlotInfo implements Parcelable {
  public static final int CARD_STATE_INFO_ABSENT = 1;
  
  public static final int CARD_STATE_INFO_ERROR = 3;
  
  public static final int CARD_STATE_INFO_PRESENT = 2;
  
  public static final int CARD_STATE_INFO_RESTRICTED = 4;
  
  public static final Parcelable.Creator<UiccSlotInfo> CREATOR = new Parcelable.Creator<UiccSlotInfo>() {
      public UiccSlotInfo createFromParcel(Parcel param1Parcel) {
        return new UiccSlotInfo(param1Parcel);
      }
      
      public UiccSlotInfo[] newArray(int param1Int) {
        return new UiccSlotInfo[param1Int];
      }
    };
  
  private final String mCardId;
  
  private final int mCardStateInfo;
  
  private final boolean mIsActive;
  
  private final boolean mIsEuicc;
  
  private final boolean mIsExtendedApduSupported;
  
  private final boolean mIsRemovable;
  
  private final int mLogicalSlotIdx;
  
  private UiccSlotInfo(Parcel paramParcel) {
    boolean bool2;
    byte b = paramParcel.readByte();
    boolean bool1 = true;
    if (b != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mIsActive = bool2;
    if (paramParcel.readByte() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mIsEuicc = bool2;
    this.mCardId = paramParcel.readString();
    this.mCardStateInfo = paramParcel.readInt();
    this.mLogicalSlotIdx = paramParcel.readInt();
    if (paramParcel.readByte() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mIsExtendedApduSupported = bool2;
    if (paramParcel.readByte() != 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.mIsRemovable = bool2;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByte((byte)this.mIsActive);
    paramParcel.writeByte((byte)this.mIsEuicc);
    paramParcel.writeString(this.mCardId);
    paramParcel.writeInt(this.mCardStateInfo);
    paramParcel.writeInt(this.mLogicalSlotIdx);
    paramParcel.writeByte((byte)this.mIsExtendedApduSupported);
    paramParcel.writeByte((byte)this.mIsRemovable);
  }
  
  public int describeContents() {
    return 0;
  }
  
  @Deprecated
  public UiccSlotInfo(boolean paramBoolean1, boolean paramBoolean2, String paramString, int paramInt1, int paramInt2, boolean paramBoolean3) {
    this.mIsActive = paramBoolean1;
    this.mIsEuicc = paramBoolean2;
    this.mCardId = paramString;
    this.mCardStateInfo = paramInt1;
    this.mLogicalSlotIdx = paramInt2;
    this.mIsExtendedApduSupported = paramBoolean3;
    this.mIsRemovable = false;
  }
  
  public UiccSlotInfo(boolean paramBoolean1, boolean paramBoolean2, String paramString, int paramInt1, int paramInt2, boolean paramBoolean3, boolean paramBoolean4) {
    this.mIsActive = paramBoolean1;
    this.mIsEuicc = paramBoolean2;
    this.mCardId = paramString;
    this.mCardStateInfo = paramInt1;
    this.mLogicalSlotIdx = paramInt2;
    this.mIsExtendedApduSupported = paramBoolean3;
    this.mIsRemovable = paramBoolean4;
  }
  
  public boolean getIsActive() {
    return this.mIsActive;
  }
  
  public boolean getIsEuicc() {
    return this.mIsEuicc;
  }
  
  public String getCardId() {
    return this.mCardId;
  }
  
  public int getCardStateInfo() {
    return this.mCardStateInfo;
  }
  
  public int getLogicalSlotIdx() {
    return this.mLogicalSlotIdx;
  }
  
  public boolean getIsExtendedApduSupported() {
    return this.mIsExtendedApduSupported;
  }
  
  public boolean isRemovable() {
    return this.mIsRemovable;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mIsActive == ((UiccSlotInfo)paramObject).mIsActive && this.mIsEuicc == ((UiccSlotInfo)paramObject).mIsEuicc) {
      String str1 = this.mCardId, str2 = ((UiccSlotInfo)paramObject).mCardId;
      if (Objects.equals(str1, str2) && this.mCardStateInfo == ((UiccSlotInfo)paramObject).mCardStateInfo && this.mLogicalSlotIdx == ((UiccSlotInfo)paramObject).mLogicalSlotIdx && this.mIsExtendedApduSupported == ((UiccSlotInfo)paramObject).mIsExtendedApduSupported && this.mIsRemovable == ((UiccSlotInfo)paramObject).mIsRemovable)
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    boolean bool1 = this.mIsActive;
    boolean bool2 = this.mIsEuicc;
    int i = Objects.hashCode(this.mCardId);
    int j = this.mCardStateInfo;
    int k = this.mLogicalSlotIdx;
    boolean bool3 = this.mIsExtendedApduSupported;
    boolean bool4 = this.mIsRemovable;
    return ((((((1 * 31 + bool1) * 31 + bool2) * 31 + i) * 31 + j) * 31 + k) * 31 + bool3) * 31 + bool4;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UiccSlotInfo (mIsActive=");
    stringBuilder.append(this.mIsActive);
    stringBuilder.append(", mIsEuicc=");
    stringBuilder.append(this.mIsEuicc);
    stringBuilder.append(", mCardId=");
    stringBuilder.append(this.mCardId);
    stringBuilder.append(", cardState=");
    stringBuilder.append(this.mCardStateInfo);
    stringBuilder.append(", phoneId=");
    stringBuilder.append(this.mLogicalSlotIdx);
    stringBuilder.append(", mIsExtendedApduSupported=");
    stringBuilder.append(this.mIsExtendedApduSupported);
    stringBuilder.append(", mIsRemovable=");
    stringBuilder.append(this.mIsRemovable);
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class CardStateInfo implements Annotation {}
}
