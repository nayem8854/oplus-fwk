package android.telephony;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.os.SystemClock;
import com.android.internal.telephony.OplusFeature;
import com.android.telephony.Rlog;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SignalStrength extends OplusSignalStrength implements Parcelable {
  CellSignalStrengthWcdma mWcdma;
  
  private long mTimestampMillis;
  
  CellSignalStrengthTdscdma mTdscdma;
  
  CellSignalStrengthNr mNr;
  
  private boolean mLteAsPrimaryInNrNsa = true;
  
  CellSignalStrengthLte mLte;
  
  CellSignalStrengthGsm mGsm;
  
  CellSignalStrengthCdma mCdma;
  
  private static final int WCDMA_RSCP_THRESHOLDS_NUM = 4;
  
  public static final int SIGNAL_STRENGTH_POOR = 1;
  
  public static final int SIGNAL_STRENGTH_NONE_OR_UNKNOWN = 0;
  
  public static final int SIGNAL_STRENGTH_MODERATE = 2;
  
  public static final int SIGNAL_STRENGTH_GREAT = 4;
  
  public static final int SIGNAL_STRENGTH_GOOD = 3;
  
  public static final int NUM_SIGNAL_STRENGTH_BINS = 5;
  
  private static final String MEASUREMENT_TYPE_RSCP = "rscp";
  
  private static final int LTE_RSRP_THRESHOLDS_NUM = 4;
  
  private static final String LOG_TAG = "SignalStrength";
  
  public static final int INVALID = 2147483647;
  
  private static final boolean DBG = false;
  
  public static SignalStrength newFromBundle(Bundle paramBundle) {
    SignalStrength signalStrength = new SignalStrength();
    signalStrength.setFromNotifierBundle(paramBundle);
    return signalStrength;
  }
  
  public SignalStrength() {
    this(new CellSignalStrengthCdma(), new CellSignalStrengthGsm(), new CellSignalStrengthWcdma(), new CellSignalStrengthTdscdma(), new CellSignalStrengthLte(), new CellSignalStrengthNr());
  }
  
  public SignalStrength(CellSignalStrengthCdma paramCellSignalStrengthCdma, CellSignalStrengthGsm paramCellSignalStrengthGsm, CellSignalStrengthWcdma paramCellSignalStrengthWcdma, CellSignalStrengthTdscdma paramCellSignalStrengthTdscdma, CellSignalStrengthLte paramCellSignalStrengthLte, CellSignalStrengthNr paramCellSignalStrengthNr) {
    this.mCdma = paramCellSignalStrengthCdma;
    this.mGsm = paramCellSignalStrengthGsm;
    this.mWcdma = paramCellSignalStrengthWcdma;
    this.mTdscdma = paramCellSignalStrengthTdscdma;
    this.mLte = paramCellSignalStrengthLte;
    this.mNr = paramCellSignalStrengthNr;
    this.mTimestampMillis = SystemClock.elapsedRealtime();
    this.mOEMLevel_0 = 0;
    this.mOEMLevel_1 = 0;
  }
  
  public SignalStrength(android.hardware.radio.V1_0.SignalStrength paramSignalStrength) {
    this(new CellSignalStrengthCdma(paramSignalStrength.cdma, paramSignalStrength.evdo), new CellSignalStrengthGsm(paramSignalStrength.gw), new CellSignalStrengthWcdma(), new CellSignalStrengthTdscdma(paramSignalStrength.tdScdma), new CellSignalStrengthLte(paramSignalStrength.lte), new CellSignalStrengthNr());
  }
  
  public SignalStrength(android.hardware.radio.V1_2.SignalStrength paramSignalStrength) {
    this(new CellSignalStrengthCdma(paramSignalStrength.cdma, paramSignalStrength.evdo), new CellSignalStrengthGsm(paramSignalStrength.gsm), new CellSignalStrengthWcdma(paramSignalStrength.wcdma), new CellSignalStrengthTdscdma(paramSignalStrength.tdScdma), new CellSignalStrengthLte(paramSignalStrength.lte), new CellSignalStrengthNr());
  }
  
  public SignalStrength(android.hardware.radio.V1_4.SignalStrength paramSignalStrength) {
    this(new CellSignalStrengthCdma(paramSignalStrength.cdma, paramSignalStrength.evdo), new CellSignalStrengthGsm(paramSignalStrength.gsm), new CellSignalStrengthWcdma(paramSignalStrength.wcdma), new CellSignalStrengthTdscdma(paramSignalStrength.tdscdma), new CellSignalStrengthLte(paramSignalStrength.lte), new CellSignalStrengthNr(paramSignalStrength.nr));
  }
  
  private CellSignalStrength getPrimary() {
    if (this.mLteAsPrimaryInNrNsa && 
      this.mLte.isValid())
      return this.mLte; 
    if (this.mNr.isValid())
      return this.mNr; 
    if (this.mLte.isValid())
      return this.mLte; 
    if (this.mCdma.isValid())
      return this.mCdma; 
    if (this.mTdscdma.isValid())
      return this.mTdscdma; 
    if (this.mWcdma.isValid())
      return this.mWcdma; 
    if (this.mGsm.isValid())
      return this.mGsm; 
    return this.mLte;
  }
  
  public List<CellSignalStrength> getCellSignalStrengths() {
    return getCellSignalStrengths(CellSignalStrength.class);
  }
  
  public <T extends CellSignalStrength> List<T> getCellSignalStrengths(Class<T> paramClass) {
    ArrayList<CellSignalStrengthLte> arrayList = new ArrayList(2);
    if (this.mLte.isValid() && paramClass.isAssignableFrom(CellSignalStrengthLte.class))
      arrayList.add(this.mLte); 
    if (this.mCdma.isValid() && paramClass.isAssignableFrom(CellSignalStrengthCdma.class))
      arrayList.add(this.mCdma); 
    if (this.mTdscdma.isValid() && paramClass.isAssignableFrom(CellSignalStrengthTdscdma.class))
      arrayList.add(this.mTdscdma); 
    if (this.mWcdma.isValid() && paramClass.isAssignableFrom(CellSignalStrengthWcdma.class))
      arrayList.add(this.mWcdma); 
    if (this.mGsm.isValid() && paramClass.isAssignableFrom(CellSignalStrengthGsm.class))
      arrayList.add(this.mGsm); 
    if (this.mNr.isValid() && paramClass.isAssignableFrom(CellSignalStrengthNr.class))
      arrayList.add(this.mNr); 
    return (List)arrayList;
  }
  
  public void updateLevel(PersistableBundle paramPersistableBundle, ServiceState paramServiceState) {
    if (paramPersistableBundle != null)
      this.mLteAsPrimaryInNrNsa = paramPersistableBundle.getBoolean("signal_strength_nr_nsa_use_lte_as_primary_bool", true); 
    this.mCdma.updateLevel(paramPersistableBundle, paramServiceState);
    this.mGsm.updateLevel(paramPersistableBundle, paramServiceState);
    this.mWcdma.updateLevel(paramPersistableBundle, paramServiceState);
    this.mTdscdma.updateLevel(paramPersistableBundle, paramServiceState);
    this.mLte.updateLevel(paramPersistableBundle, paramServiceState);
    this.mNr.updateLevel(paramPersistableBundle, paramServiceState);
  }
  
  public SignalStrength(SignalStrength paramSignalStrength) {
    copyFrom(paramSignalStrength);
  }
  
  public void copyFrom(SignalStrength paramSignalStrength) {
    this.mCdma = new CellSignalStrengthCdma(paramSignalStrength.mCdma);
    this.mGsm = new CellSignalStrengthGsm(paramSignalStrength.mGsm);
    this.mWcdma = new CellSignalStrengthWcdma(paramSignalStrength.mWcdma);
    this.mTdscdma = new CellSignalStrengthTdscdma(paramSignalStrength.mTdscdma);
    this.mLte = new CellSignalStrengthLte(paramSignalStrength.mLte);
    this.mNr = new CellSignalStrengthNr(paramSignalStrength.mNr);
    this.mTimestampMillis = paramSignalStrength.getTimestampMillis();
    this.mOEMLevel_0 = paramSignalStrength.mOEMLevel_0;
    this.mOEMLevel_1 = paramSignalStrength.mOEMLevel_1;
    this.mIsFake = paramSignalStrength.mIsFake;
  }
  
  public SignalStrength(Parcel paramParcel) {
    this.mCdma = (CellSignalStrengthCdma)paramParcel.readParcelable(CellSignalStrengthCdma.class.getClassLoader());
    this.mGsm = (CellSignalStrengthGsm)paramParcel.readParcelable(CellSignalStrengthGsm.class.getClassLoader());
    this.mWcdma = (CellSignalStrengthWcdma)paramParcel.readParcelable(CellSignalStrengthWcdma.class.getClassLoader());
    this.mTdscdma = (CellSignalStrengthTdscdma)paramParcel.readParcelable(CellSignalStrengthTdscdma.class.getClassLoader());
    this.mLte = (CellSignalStrengthLte)paramParcel.readParcelable(CellSignalStrengthLte.class.getClassLoader());
    this.mNr = (CellSignalStrengthNr)paramParcel.readParcelable(CellSignalStrengthLte.class.getClassLoader());
    this.mTimestampMillis = paramParcel.readLong();
    this.mOEMLevel_0 = paramParcel.readInt();
    this.mOEMLevel_1 = paramParcel.readInt();
    this.mIsFake = paramParcel.readBoolean();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.mCdma, paramInt);
    paramParcel.writeParcelable(this.mGsm, paramInt);
    paramParcel.writeParcelable(this.mWcdma, paramInt);
    paramParcel.writeParcelable(this.mTdscdma, paramInt);
    paramParcel.writeParcelable(this.mLte, paramInt);
    paramParcel.writeParcelable(this.mNr, paramInt);
    paramParcel.writeLong(this.mTimestampMillis);
    paramParcel.writeInt(this.mOEMLevel_0);
    paramParcel.writeInt(this.mOEMLevel_1);
    paramParcel.writeBoolean(this.mIsFake);
  }
  
  public long getTimestampMillis() {
    return this.mTimestampMillis;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<SignalStrength> CREATOR = new Parcelable.Creator<SignalStrength>() {
      public SignalStrength createFromParcel(Parcel param1Parcel) {
        return new SignalStrength(param1Parcel);
      }
      
      public SignalStrength[] newArray(int param1Int) {
        return new SignalStrength[param1Int];
      }
    };
  
  @Deprecated
  public int getGsmSignalStrength() {
    return this.mGsm.getAsuLevel();
  }
  
  @Deprecated
  public int getGsmBitErrorRate() {
    return this.mGsm.getBitErrorRate();
  }
  
  @Deprecated
  public int getCdmaDbm() {
    return this.mCdma.getCdmaDbm();
  }
  
  @Deprecated
  public int getCdmaEcio() {
    return this.mCdma.getCdmaEcio();
  }
  
  @Deprecated
  public int getEvdoDbm() {
    return this.mCdma.getEvdoDbm();
  }
  
  @Deprecated
  public int getEvdoEcio() {
    return this.mCdma.getEvdoEcio();
  }
  
  @Deprecated
  public int getEvdoSnr() {
    return this.mCdma.getEvdoSnr();
  }
  
  @Deprecated
  public int getLteSignalStrength() {
    return this.mLte.getRssi();
  }
  
  @Deprecated
  public int getLteRsrp() {
    return this.mLte.getRsrp();
  }
  
  @Deprecated
  public int getLteRsrq() {
    return this.mLte.getRsrq();
  }
  
  @Deprecated
  public int getLteRssnr() {
    return this.mLte.getRssnr();
  }
  
  @Deprecated
  public int getLteCqi() {
    return this.mLte.getCqi();
  }
  
  public int getLevel() {
    int i = getPrimary().getLevel();
    if (i < 0 || i > 4) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid Level ");
      stringBuilder.append(i);
      stringBuilder.append(", this=");
      stringBuilder.append(this);
      loge(stringBuilder.toString());
      return 0;
    } 
    return getPrimary().getLevel();
  }
  
  @Deprecated
  public int getAsuLevel() {
    return getPrimary().getAsuLevel();
  }
  
  @Deprecated
  public int getDbm() {
    return getPrimary().getDbm();
  }
  
  @Deprecated
  public int getGsmDbm() {
    return this.mGsm.getDbm();
  }
  
  @Deprecated
  public int getGsmLevel() {
    return this.mGsm.getLevel();
  }
  
  @Deprecated
  public int getGsmAsuLevel() {
    return this.mGsm.getAsuLevel();
  }
  
  @Deprecated
  public int getCdmaLevel() {
    return this.mCdma.getLevel();
  }
  
  @Deprecated
  public int getCdmaAsuLevel() {
    return this.mCdma.getAsuLevel();
  }
  
  @Deprecated
  public int getEvdoLevel() {
    return this.mCdma.getEvdoLevel();
  }
  
  @Deprecated
  public int getEvdoAsuLevel() {
    return this.mCdma.getEvdoAsuLevel();
  }
  
  @Deprecated
  public int getLteDbm() {
    return this.mLte.getRsrp();
  }
  
  @Deprecated
  public int getLteLevel() {
    return this.mLte.getLevel();
  }
  
  @Deprecated
  public int getLteAsuLevel() {
    return this.mLte.getAsuLevel();
  }
  
  @Deprecated
  public boolean isGsm() {
    return getPrimary() instanceof CellSignalStrengthCdma ^ true;
  }
  
  @Deprecated
  public int getTdScdmaDbm() {
    return this.mTdscdma.getRscp();
  }
  
  @Deprecated
  public int getTdScdmaLevel() {
    return this.mTdscdma.getLevel();
  }
  
  @Deprecated
  public int getTdScdmaAsuLevel() {
    return this.mTdscdma.getAsuLevel();
  }
  
  @Deprecated
  public int getWcdmaRscp() {
    return this.mWcdma.getRscp();
  }
  
  @Deprecated
  public int getWcdmaAsuLevel() {
    return this.mWcdma.getAsuLevel();
  }
  
  @Deprecated
  public int getWcdmaDbm() {
    return this.mWcdma.getDbm();
  }
  
  @Deprecated
  public int getWcdmaLevel() {
    return this.mWcdma.getLevel();
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mCdma, this.mGsm, this.mWcdma, this.mTdscdma, this.mLte, this.mNr });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof SignalStrength;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    if (this.mCdma.equals(((SignalStrength)paramObject).mCdma)) {
      CellSignalStrengthGsm cellSignalStrengthGsm1 = this.mGsm, cellSignalStrengthGsm2 = ((SignalStrength)paramObject).mGsm;
      if (cellSignalStrengthGsm1.equals(cellSignalStrengthGsm2)) {
        CellSignalStrengthWcdma cellSignalStrengthWcdma2 = this.mWcdma, cellSignalStrengthWcdma1 = ((SignalStrength)paramObject).mWcdma;
        if (cellSignalStrengthWcdma2.equals(cellSignalStrengthWcdma1)) {
          CellSignalStrengthTdscdma cellSignalStrengthTdscdma1 = this.mTdscdma, cellSignalStrengthTdscdma2 = ((SignalStrength)paramObject).mTdscdma;
          if (cellSignalStrengthTdscdma1.equals(cellSignalStrengthTdscdma2)) {
            CellSignalStrengthLte cellSignalStrengthLte2 = this.mLte, cellSignalStrengthLte1 = ((SignalStrength)paramObject).mLte;
            if (cellSignalStrengthLte2.equals(cellSignalStrengthLte1)) {
              CellSignalStrengthNr cellSignalStrengthNr = this.mNr;
              paramObject = ((SignalStrength)paramObject).mNr;
              if (cellSignalStrengthNr.equals(paramObject))
                bool1 = true; 
            } 
          } 
        } 
      } 
    } 
    return bool1;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SignalStrength:{");
    stringBuilder.append("mCdma=");
    stringBuilder.append(this.mCdma);
    stringBuilder.append(",mGsm=");
    stringBuilder.append(this.mGsm);
    stringBuilder.append(",mWcdma=");
    stringBuilder.append(this.mWcdma);
    stringBuilder.append(",mTdscdma=");
    stringBuilder.append(this.mTdscdma);
    stringBuilder.append(",mLte=");
    stringBuilder.append(this.mLte);
    stringBuilder.append(",mNr=");
    stringBuilder.append(this.mNr);
    stringBuilder.append(",primary=");
    stringBuilder.append(getPrimary().getClass().getSimpleName());
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  @Deprecated
  private void setFromNotifierBundle(Bundle paramBundle) {
    this.mCdma = (CellSignalStrengthCdma)paramBundle.getParcelable("Cdma");
    this.mGsm = (CellSignalStrengthGsm)paramBundle.getParcelable("Gsm");
    this.mWcdma = (CellSignalStrengthWcdma)paramBundle.getParcelable("Wcdma");
    this.mTdscdma = (CellSignalStrengthTdscdma)paramBundle.getParcelable("Tdscdma");
    this.mLte = (CellSignalStrengthLte)paramBundle.getParcelable("Lte");
    this.mNr = (CellSignalStrengthNr)paramBundle.getParcelable("Nr");
    this.mOEMLevel_0 = paramBundle.getInt("OEMLevel_0");
    this.mOEMLevel_1 = paramBundle.getInt("OEMLevel_1");
    this.mIsFake = paramBundle.getBoolean("IsFake");
  }
  
  @Deprecated
  public void fillInNotifierBundle(Bundle paramBundle) {
    paramBundle.putParcelable("Cdma", this.mCdma);
    paramBundle.putParcelable("Gsm", this.mGsm);
    paramBundle.putParcelable("Wcdma", this.mWcdma);
    paramBundle.putParcelable("Tdscdma", this.mTdscdma);
    paramBundle.putParcelable("Lte", this.mLte);
    paramBundle.putParcelable("Nr", this.mNr);
    paramBundle.putInt("OEMLevel_0", this.mOEMLevel_0);
    paramBundle.putInt("OEMLevel_1", this.mOEMLevel_1);
    paramBundle.putBoolean("IsFake", this.mIsFake);
  }
  
  private static void log(String paramString) {
    Rlog.w("SignalStrength", paramString);
  }
  
  private static void loge(String paramString) {
    Rlog.e("SignalStrength", paramString);
  }
  
  public int[] getColorOSLevel() {
    if (OplusFeature.OPLUS_FEATURE_JP_SIGNAL_STRENGTH) {
      int i = getLevel();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("OPLUS_FEATURE_JP_SIGNAL_STRENGTH is on, use original level = ");
      stringBuilder.append(i);
      Rlog.d("SignalStrength", stringBuilder.toString());
      return new int[] { i, i };
    } 
    return new int[] { this.mOEMLevel_0, this.mOEMLevel_1 };
  }
  
  public int getNrRsrp() {
    return this.mNr.getSsRsrp();
  }
}
