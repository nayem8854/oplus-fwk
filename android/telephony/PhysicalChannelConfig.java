package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;
import java.util.Objects;

public final class PhysicalChannelConfig implements Parcelable {
  public static final int CONNECTION_PRIMARY_SERVING = 1;
  
  public static final int CONNECTION_SECONDARY_SERVING = 2;
  
  public static final int CONNECTION_UNKNOWN = 2147483647;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mCellConnectionStatus);
    paramParcel.writeInt(this.mCellBandwidthDownlinkKhz);
    paramParcel.writeInt(this.mRat);
    paramParcel.writeInt(this.mChannelNumber);
    paramParcel.writeInt(this.mFrequencyRange);
    paramParcel.writeIntArray(this.mContextIds);
    paramParcel.writeInt(this.mPhysicalCellId);
  }
  
  public int getCellBandwidthDownlink() {
    return this.mCellBandwidthDownlinkKhz;
  }
  
  public int[] getContextIds() {
    return this.mContextIds;
  }
  
  public int getFrequencyRange() {
    return this.mFrequencyRange;
  }
  
  public int getChannelNumber() {
    return this.mChannelNumber;
  }
  
  public int getPhysicalCellId() {
    return this.mPhysicalCellId;
  }
  
  public int getRat() {
    return this.mRat;
  }
  
  public int getConnectionStatus() {
    return this.mCellConnectionStatus;
  }
  
  private String getConnectionStatusString() {
    int i = this.mCellConnectionStatus;
    if (i != 1) {
      if (i != 2) {
        if (i != Integer.MAX_VALUE) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Invalid(");
          stringBuilder.append(this.mCellConnectionStatus);
          stringBuilder.append(")");
          return stringBuilder.toString();
        } 
        return "Unknown";
      } 
      return "SecondaryServing";
    } 
    return "PrimaryServing";
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof PhysicalChannelConfig))
      return false; 
    PhysicalChannelConfig physicalChannelConfig = (PhysicalChannelConfig)paramObject;
    if (this.mCellConnectionStatus == physicalChannelConfig.mCellConnectionStatus && this.mCellBandwidthDownlinkKhz == physicalChannelConfig.mCellBandwidthDownlinkKhz && this.mRat == physicalChannelConfig.mRat && this.mFrequencyRange == physicalChannelConfig.mFrequencyRange && this.mChannelNumber == physicalChannelConfig.mChannelNumber && this.mPhysicalCellId == physicalChannelConfig.mPhysicalCellId) {
      paramObject = this.mContextIds;
      int[] arrayOfInt = physicalChannelConfig.mContextIds;
      if (Arrays.equals((int[])paramObject, arrayOfInt))
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = this.mCellConnectionStatus;
    int j = this.mCellBandwidthDownlinkKhz, k = this.mRat, m = this.mFrequencyRange, n = this.mChannelNumber;
    int i1 = this.mPhysicalCellId, arrayOfInt[] = this.mContextIds;
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), arrayOfInt });
  }
  
  public static final Parcelable.Creator<PhysicalChannelConfig> CREATOR = new Parcelable.Creator<PhysicalChannelConfig>() {
      public PhysicalChannelConfig createFromParcel(Parcel param1Parcel) {
        return new PhysicalChannelConfig(param1Parcel);
      }
      
      public PhysicalChannelConfig[] newArray(int param1Int) {
        return new PhysicalChannelConfig[param1Int];
      }
    };
  
  private int mCellBandwidthDownlinkKhz;
  
  private int mCellConnectionStatus;
  
  private int mChannelNumber;
  
  private int[] mContextIds;
  
  private int mFrequencyRange;
  
  private int mPhysicalCellId;
  
  private int mRat;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{mConnectionStatus=");
    stringBuilder.append(getConnectionStatusString());
    stringBuilder.append(",mCellBandwidthDownlinkKhz=");
    int i = this.mCellBandwidthDownlinkKhz;
    stringBuilder.append(i);
    stringBuilder.append(",mRat=");
    i = this.mRat;
    stringBuilder.append(TelephonyManager.getNetworkTypeName(i));
    stringBuilder.append(",mFrequencyRange=");
    i = this.mFrequencyRange;
    stringBuilder.append(ServiceState.frequencyRangeToString(i));
    stringBuilder.append(",mChannelNumber=");
    i = this.mChannelNumber;
    stringBuilder.append(i);
    stringBuilder.append(",mContextIds=");
    int[] arrayOfInt = this.mContextIds;
    stringBuilder.append(Arrays.toString(arrayOfInt));
    stringBuilder.append(",mPhysicalCellId=");
    i = this.mPhysicalCellId;
    stringBuilder.append(i);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  private PhysicalChannelConfig(Parcel paramParcel) {
    this.mCellConnectionStatus = paramParcel.readInt();
    this.mCellBandwidthDownlinkKhz = paramParcel.readInt();
    this.mRat = paramParcel.readInt();
    this.mChannelNumber = paramParcel.readInt();
    this.mFrequencyRange = paramParcel.readInt();
    this.mContextIds = paramParcel.createIntArray();
    this.mPhysicalCellId = paramParcel.readInt();
  }
  
  private PhysicalChannelConfig(Builder paramBuilder) {
    this.mCellConnectionStatus = paramBuilder.mCellConnectionStatus;
    this.mCellBandwidthDownlinkKhz = paramBuilder.mCellBandwidthDownlinkKhz;
    this.mRat = paramBuilder.mRat;
    this.mChannelNumber = paramBuilder.mChannelNumber;
    this.mFrequencyRange = paramBuilder.mFrequencyRange;
    this.mContextIds = paramBuilder.mContextIds;
    this.mPhysicalCellId = paramBuilder.mPhysicalCellId;
  }
  
  class Builder {
    private int mCellBandwidthDownlinkKhz;
    
    private int mCellConnectionStatus;
    
    private int mChannelNumber;
    
    private int[] mContextIds;
    
    private int mFrequencyRange;
    
    private int mPhysicalCellId;
    
    private int mRat;
    
    public Builder() {
      this.mRat = 0;
      this.mFrequencyRange = 0;
      this.mChannelNumber = Integer.MAX_VALUE;
      this.mCellBandwidthDownlinkKhz = 0;
      this.mCellConnectionStatus = Integer.MAX_VALUE;
      this.mContextIds = new int[0];
      this.mPhysicalCellId = Integer.MAX_VALUE;
    }
    
    public PhysicalChannelConfig build() {
      return new PhysicalChannelConfig(this);
    }
    
    public Builder setRat(int param1Int) {
      this.mRat = param1Int;
      return this;
    }
    
    public Builder setFrequencyRange(int param1Int) {
      this.mFrequencyRange = param1Int;
      return this;
    }
    
    public Builder setChannelNumber(int param1Int) {
      this.mChannelNumber = param1Int;
      return this;
    }
    
    public Builder setCellBandwidthDownlinkKhz(int param1Int) {
      this.mCellBandwidthDownlinkKhz = param1Int;
      return this;
    }
    
    public Builder setCellConnectionStatus(int param1Int) {
      this.mCellConnectionStatus = param1Int;
      return this;
    }
    
    public Builder setContextIds(int[] param1ArrayOfint) {
      if (param1ArrayOfint != null)
        Arrays.sort(param1ArrayOfint); 
      this.mContextIds = param1ArrayOfint;
      return this;
    }
    
    public Builder setPhysicalCellId(int param1Int) {
      this.mPhysicalCellId = param1Int;
      return this;
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ConnectionStatus implements Annotation {}
}
