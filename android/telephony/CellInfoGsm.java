package android.telephony;

import android.hardware.radio.V1_0.CellInfo;
import android.hardware.radio.V1_2.CellInfo;
import android.hardware.radio.V1_4.CellInfo;
import android.hardware.radio.V1_5.CellInfo;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.telephony.Rlog;

public final class CellInfoGsm extends CellInfo implements Parcelable {
  public CellInfoGsm() {
    this.mCellIdentityGsm = new CellIdentityGsm();
    this.mCellSignalStrengthGsm = new CellSignalStrengthGsm();
  }
  
  public CellInfoGsm(CellInfoGsm paramCellInfoGsm) {
    super(paramCellInfoGsm);
    this.mCellIdentityGsm = paramCellInfoGsm.mCellIdentityGsm.copy();
    this.mCellSignalStrengthGsm = paramCellInfoGsm.mCellSignalStrengthGsm.copy();
  }
  
  public CellInfoGsm(CellInfo paramCellInfo) {
    super(paramCellInfo);
    android.hardware.radio.V1_0.CellInfoGsm cellInfoGsm = paramCellInfo.gsm.get(0);
    this.mCellIdentityGsm = new CellIdentityGsm(cellInfoGsm.cellIdentityGsm);
    this.mCellSignalStrengthGsm = new CellSignalStrengthGsm(cellInfoGsm.signalStrengthGsm);
  }
  
  public CellInfoGsm(CellInfo paramCellInfo) {
    super(paramCellInfo);
    android.hardware.radio.V1_2.CellInfoGsm cellInfoGsm = paramCellInfo.gsm.get(0);
    this.mCellIdentityGsm = new CellIdentityGsm(cellInfoGsm.cellIdentityGsm);
    this.mCellSignalStrengthGsm = new CellSignalStrengthGsm(cellInfoGsm.signalStrengthGsm);
  }
  
  public CellInfoGsm(CellInfo paramCellInfo, long paramLong) {
    super(paramCellInfo, paramLong);
    android.hardware.radio.V1_2.CellInfoGsm cellInfoGsm = paramCellInfo.info.gsm();
    this.mCellIdentityGsm = new CellIdentityGsm(cellInfoGsm.cellIdentityGsm);
    this.mCellSignalStrengthGsm = new CellSignalStrengthGsm(cellInfoGsm.signalStrengthGsm);
  }
  
  public CellInfoGsm(CellInfo paramCellInfo, long paramLong) {
    super(paramCellInfo, paramLong);
    android.hardware.radio.V1_5.CellInfoGsm cellInfoGsm = paramCellInfo.ratSpecificInfo.gsm();
    this.mCellIdentityGsm = new CellIdentityGsm(cellInfoGsm.cellIdentityGsm);
    this.mCellSignalStrengthGsm = new CellSignalStrengthGsm(cellInfoGsm.signalStrengthGsm);
  }
  
  public CellIdentityGsm getCellIdentity() {
    return this.mCellIdentityGsm;
  }
  
  public void setCellIdentity(CellIdentityGsm paramCellIdentityGsm) {
    this.mCellIdentityGsm = paramCellIdentityGsm;
  }
  
  public CellSignalStrengthGsm getCellSignalStrength() {
    return this.mCellSignalStrengthGsm;
  }
  
  public CellInfo sanitizeLocationInfo() {
    CellInfoGsm cellInfoGsm = new CellInfoGsm(this);
    cellInfoGsm.mCellIdentityGsm = this.mCellIdentityGsm.sanitizeLocationInfo();
    return cellInfoGsm;
  }
  
  public void setCellSignalStrength(CellSignalStrengthGsm paramCellSignalStrengthGsm) {
    this.mCellSignalStrengthGsm = paramCellSignalStrengthGsm;
  }
  
  public int hashCode() {
    return super.hashCode() + this.mCellIdentityGsm.hashCode() + this.mCellSignalStrengthGsm.hashCode();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = super.equals(paramObject);
    boolean bool1 = false;
    if (!bool)
      return false; 
    try {
      CellInfoGsm cellInfoGsm = (CellInfoGsm)paramObject;
      if (this.mCellIdentityGsm.equals(cellInfoGsm.mCellIdentityGsm)) {
        paramObject = this.mCellSignalStrengthGsm;
        CellSignalStrengthGsm cellSignalStrengthGsm = cellInfoGsm.mCellSignalStrengthGsm;
        bool = paramObject.equals(cellSignalStrengthGsm);
        if (bool)
          bool1 = true; 
      } 
      return bool1;
    } catch (ClassCastException classCastException) {
      return false;
    } 
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append("CellInfoGsm:{");
    stringBuffer.append(super.toString());
    stringBuffer.append(" ");
    stringBuffer.append(this.mCellIdentityGsm);
    stringBuffer.append(" ");
    stringBuffer.append(this.mCellSignalStrengthGsm);
    stringBuffer.append("}");
    return stringBuffer.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcel(paramParcel, paramInt, 1);
    this.mCellIdentityGsm.writeToParcel(paramParcel, paramInt);
    this.mCellSignalStrengthGsm.writeToParcel(paramParcel, paramInt);
  }
  
  private CellInfoGsm(Parcel paramParcel) {
    super(paramParcel);
    this.mCellIdentityGsm = (CellIdentityGsm)CellIdentityGsm.CREATOR.createFromParcel(paramParcel);
    this.mCellSignalStrengthGsm = (CellSignalStrengthGsm)CellSignalStrengthGsm.CREATOR.createFromParcel(paramParcel);
  }
  
  public static final Parcelable.Creator<CellInfoGsm> CREATOR = (Parcelable.Creator<CellInfoGsm>)new Object();
  
  private static final boolean DBG = false;
  
  private static final String LOG_TAG = "CellInfoGsm";
  
  private CellIdentityGsm mCellIdentityGsm;
  
  private CellSignalStrengthGsm mCellSignalStrengthGsm;
  
  protected static CellInfoGsm createFromParcelBody(Parcel paramParcel) {
    return new CellInfoGsm(paramParcel);
  }
  
  private static void log(String paramString) {
    Rlog.w("CellInfoGsm", paramString);
  }
}
