package android.telephony;

import android.annotation.SystemApi;
import android.hardware.radio.V1_5.CellIdentity;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;
import java.util.Objects;

public final class BarringInfo implements Parcelable {
  private static final BarringServiceInfo BARRING_SERVICE_INFO_UNBARRED;
  
  public static final class BarringServiceInfo implements Parcelable {
    public static final int BARRING_TYPE_CONDITIONAL = 1;
    
    public static final int BARRING_TYPE_NONE = 0;
    
    public static final int BARRING_TYPE_UNCONDITIONAL = 2;
    
    public static final int BARRING_TYPE_UNKNOWN = -1;
    
    public BarringServiceInfo(int param1Int) {
      this(param1Int, false, 0, 0);
    }
    
    public BarringServiceInfo(int param1Int1, boolean param1Boolean, int param1Int2, int param1Int3) {
      this.mBarringType = param1Int1;
      this.mIsConditionallyBarred = param1Boolean;
      this.mConditionalBarringFactor = param1Int2;
      this.mConditionalBarringTimeSeconds = param1Int3;
    }
    
    public int getBarringType() {
      return this.mBarringType;
    }
    
    public boolean isConditionallyBarred() {
      return this.mIsConditionallyBarred;
    }
    
    public int getConditionalBarringFactor() {
      return this.mConditionalBarringFactor;
    }
    
    public int getConditionalBarringTimeSeconds() {
      return this.mConditionalBarringTimeSeconds;
    }
    
    public boolean isBarred() {
      int i = this.mBarringType;
      boolean bool1 = true, bool2 = bool1;
      if (i != 2)
        if (i == 1 && this.mIsConditionallyBarred) {
          bool2 = bool1;
        } else {
          bool2 = false;
        }  
      return bool2;
    }
    
    public int hashCode() {
      int i = this.mBarringType;
      boolean bool = this.mIsConditionallyBarred;
      int j = this.mConditionalBarringFactor;
      int k = this.mConditionalBarringTimeSeconds;
      return Objects.hash(new Object[] { Integer.valueOf(i), Boolean.valueOf(bool), Integer.valueOf(j), Integer.valueOf(k) });
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof BarringServiceInfo;
      boolean bool1 = false;
      if (!bool)
        return false; 
      param1Object = param1Object;
      bool = bool1;
      if (this.mBarringType == ((BarringServiceInfo)param1Object).mBarringType) {
        bool = bool1;
        if (this.mIsConditionallyBarred == ((BarringServiceInfo)param1Object).mIsConditionallyBarred) {
          bool = bool1;
          if (this.mConditionalBarringFactor == ((BarringServiceInfo)param1Object).mConditionalBarringFactor) {
            bool = bool1;
            if (this.mConditionalBarringTimeSeconds == ((BarringServiceInfo)param1Object).mConditionalBarringTimeSeconds)
              bool = true; 
          } 
        } 
      } 
      return bool;
    }
    
    public BarringServiceInfo(Parcel param1Parcel) {
      this.mBarringType = param1Parcel.readInt();
      this.mIsConditionallyBarred = param1Parcel.readBoolean();
      this.mConditionalBarringFactor = param1Parcel.readInt();
      this.mConditionalBarringTimeSeconds = param1Parcel.readInt();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mBarringType);
      param1Parcel.writeBoolean(this.mIsConditionallyBarred);
      param1Parcel.writeInt(this.mConditionalBarringFactor);
      param1Parcel.writeInt(this.mConditionalBarringTimeSeconds);
    }
    
    public static final Parcelable.Creator<BarringServiceInfo> CREATOR = new Parcelable.Creator<BarringServiceInfo>() {
        public BarringInfo.BarringServiceInfo createFromParcel(Parcel param2Parcel) {
          return new BarringInfo.BarringServiceInfo(param2Parcel);
        }
        
        public BarringInfo.BarringServiceInfo[] newArray(int param2Int) {
          return new BarringInfo.BarringServiceInfo[param2Int];
        }
      };
    
    private final int mBarringType;
    
    private final int mConditionalBarringFactor;
    
    private final int mConditionalBarringTimeSeconds;
    
    private final boolean mIsConditionallyBarred;
    
    public int describeContents() {
      return 0;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    class BarringType implements Annotation {}
  }
  
  class null implements Parcelable.Creator<BarringServiceInfo> {
    public BarringInfo.BarringServiceInfo createFromParcel(Parcel param1Parcel) {
      return new BarringInfo.BarringServiceInfo(param1Parcel);
    }
    
    public BarringInfo.BarringServiceInfo[] newArray(int param1Int) {
      return new BarringInfo.BarringServiceInfo[param1Int];
    }
  }
  
  private static final BarringServiceInfo BARRING_SERVICE_INFO_UNKNOWN = new BarringServiceInfo(-1);
  
  public static final int BARRING_SERVICE_TYPE_CS_FALLBACK = 5;
  
  public static final int BARRING_SERVICE_TYPE_CS_SERVICE = 0;
  
  public static final int BARRING_SERVICE_TYPE_CS_VOICE = 2;
  
  public static final int BARRING_SERVICE_TYPE_EMERGENCY = 8;
  
  public static final int BARRING_SERVICE_TYPE_MMTEL_VIDEO = 7;
  
  public static final int BARRING_SERVICE_TYPE_MMTEL_VOICE = 6;
  
  public static final int BARRING_SERVICE_TYPE_MO_DATA = 4;
  
  public static final int BARRING_SERVICE_TYPE_MO_SIGNALLING = 3;
  
  public static final int BARRING_SERVICE_TYPE_PS_SERVICE = 1;
  
  public static final int BARRING_SERVICE_TYPE_SMS = 9;
  
  public static final Parcelable.Creator<BarringInfo> CREATOR;
  
  private SparseArray<BarringServiceInfo> mBarringServiceInfos;
  
  private CellIdentity mCellIdentity;
  
  static {
    BARRING_SERVICE_INFO_UNBARRED = new BarringServiceInfo(0);
    CREATOR = new Parcelable.Creator<BarringInfo>() {
        public BarringInfo createFromParcel(Parcel param1Parcel) {
          return new BarringInfo(param1Parcel);
        }
        
        public BarringInfo[] newArray(int param1Int) {
          return new BarringInfo[param1Int];
        }
      };
  }
  
  @SystemApi
  public BarringInfo() {
    this.mBarringServiceInfos = new SparseArray<>();
  }
  
  public BarringInfo(CellIdentity paramCellIdentity, SparseArray<BarringServiceInfo> paramSparseArray) {
    this.mCellIdentity = paramCellIdentity;
    this.mBarringServiceInfos = paramSparseArray;
  }
  
  public static BarringInfo create(CellIdentity paramCellIdentity, List<android.hardware.radio.V1_5.BarringInfo> paramList) {
    CellIdentity cellIdentity = CellIdentity.create(paramCellIdentity);
    SparseArray<BarringServiceInfo> sparseArray = new SparseArray();
    for (android.hardware.radio.V1_5.BarringInfo barringInfo : paramList) {
      if (barringInfo.barringType == 1) {
        if (barringInfo.barringTypeSpecificInfo.getDiscriminator() != 1)
          continue; 
        android.hardware.radio.V1_5.BarringInfo.BarringTypeSpecificInfo barringTypeSpecificInfo = barringInfo.barringTypeSpecificInfo;
        android.hardware.radio.V1_5.BarringInfo.BarringTypeSpecificInfo.Conditional conditional = barringTypeSpecificInfo.conditional();
        sparseArray.put(barringInfo.serviceType, new BarringServiceInfo(barringInfo.barringType, conditional.isBarred, conditional.factor, conditional.timeSeconds));
        continue;
      } 
      sparseArray.put(barringInfo.serviceType, new BarringServiceInfo(barringInfo.barringType, false, 0, 0));
    } 
    return new BarringInfo(cellIdentity, sparseArray);
  }
  
  public BarringServiceInfo getBarringServiceInfo(int paramInt) {
    BarringServiceInfo barringServiceInfo = this.mBarringServiceInfos.get(paramInt);
    if (barringServiceInfo == null)
      if (this.mBarringServiceInfos.size() > 0) {
        barringServiceInfo = BARRING_SERVICE_INFO_UNBARRED;
      } else {
        barringServiceInfo = BARRING_SERVICE_INFO_UNKNOWN;
      }  
    return barringServiceInfo;
  }
  
  @SystemApi
  public BarringInfo createLocationInfoSanitizedCopy() {
    if (this.mCellIdentity == null)
      return this; 
    return new BarringInfo(this.mCellIdentity.sanitizeLocationInfo(), this.mBarringServiceInfos);
  }
  
  public BarringInfo(Parcel paramParcel) {
    this.mCellIdentity = (CellIdentity)paramParcel.readParcelable(CellIdentity.class.getClassLoader());
    this.mBarringServiceInfos = paramParcel.readSparseArray(BarringServiceInfo.class.getClassLoader());
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.mCellIdentity, paramInt);
    paramParcel.writeSparseArray(this.mBarringServiceInfos);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public int hashCode() {
    int i;
    CellIdentity cellIdentity = this.mCellIdentity;
    if (cellIdentity != null) {
      i = cellIdentity.hashCode();
    } else {
      i = 7;
    } 
    for (byte b = 0; b < this.mBarringServiceInfos.size(); b++) {
      int j = this.mBarringServiceInfos.keyAt(b);
      i = i + j * 15 + ((BarringServiceInfo)this.mBarringServiceInfos.valueAt(b)).hashCode() * 31;
    } 
    return i;
  }
  
  public boolean equals(Object paramObject) {
    if (!(paramObject instanceof BarringInfo))
      return false; 
    paramObject = paramObject;
    if (hashCode() != paramObject.hashCode())
      return false; 
    if (this.mBarringServiceInfos.size() != ((BarringInfo)paramObject).mBarringServiceInfos.size())
      return false; 
    for (byte b = 0; b < this.mBarringServiceInfos.size(); b++) {
      if (this.mBarringServiceInfos.keyAt(b) != ((BarringInfo)paramObject).mBarringServiceInfos.keyAt(b))
        return false; 
      Object object = this.mBarringServiceInfos.valueAt(b);
      SparseArray<BarringServiceInfo> sparseArray = ((BarringInfo)paramObject).mBarringServiceInfos;
      sparseArray = (SparseArray<BarringServiceInfo>)sparseArray.valueAt(b);
      if (!Objects.equals(object, sparseArray))
        return false; 
    } 
    return true;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("BarringInfo {mCellIdentity=");
    stringBuilder.append(this.mCellIdentity);
    stringBuilder.append(", mBarringServiceInfos=");
    stringBuilder.append(this.mBarringServiceInfos);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class BarringType implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class BarringServiceType implements Annotation {}
}
