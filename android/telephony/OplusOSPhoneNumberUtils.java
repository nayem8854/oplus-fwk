package android.telephony;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Settings;
import android.text.TextUtils;

public class OplusOSPhoneNumberUtils {
  private static final String IP_CALL = "ip_call";
  
  private static final String IP_CALL_PREFIX = "ip_call_prefix_sub";
  
  static final String LOG_TAG = "OplusOSPhoneNumberUtils";
  
  public static final char PAUSE = ',';
  
  public static final char WAIT = ';';
  
  public static final char WILD = 'N';
  
  public static String checkAndAppendPrefix(Intent paramIntent, int paramInt, String paramString, Context paramContext) {
    boolean bool = paramIntent.getBooleanExtra("ip_call", false);
    if (bool && paramString != null && 
      paramInt < TelephonyManager.getDefault().getPhoneCount()) {
      ContentResolver contentResolver = paramContext.getContentResolver();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ip_call_prefix_sub");
      stringBuilder.append(paramInt + 1);
      String str = Settings.System.getString(contentResolver, stringBuilder.toString());
      if (!TextUtils.isEmpty(str)) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(str);
        stringBuilder.append(paramString);
        return stringBuilder.toString();
      } 
    } 
    return paramString;
  }
  
  public static String getNumberFromIntent(Intent paramIntent, Context paramContext) {
    int[] arrayOfInt;
    boolean bool;
    String str4, str1 = null, str2 = null;
    Uri uri = paramIntent.getData();
    if (uri == null)
      return null; 
    String str3 = uri.getScheme();
    if (TelephonyManager.getDefault().isMultiSimEnabled()) {
      bool = SubscriptionManager.getDefaultVoicePhoneId();
      bool = paramIntent.getIntExtra("subscription", bool);
    } else {
      bool = false;
    } 
    if (str3.equals("tel") || str3.equals("sip"))
      return checkAndAppendPrefix(paramIntent, bool, uri.getSchemeSpecificPart(), paramContext); 
    if (str3.equals("voicemail")) {
      if (TelephonyManager.getDefault().isMultiSimEnabled()) {
        arrayOfInt = SubscriptionManager.getSubId(bool);
        if (arrayOfInt != null && arrayOfInt[0] > 0)
          return TelephonyManager.getDefault().getVoiceMailNumber(arrayOfInt[0]); 
      } 
      return TelephonyManager.getDefault().getVoiceMailNumber();
    } 
    if (paramContext == null)
      return null; 
    arrayOfInt.resolveType(paramContext);
    str3 = uri.getAuthority();
    if ("contacts".equals(str3)) {
      str4 = "number";
    } else if ("com.android.contacts".equals(str3)) {
      str4 = "data1";
    } else {
      str4 = null;
    } 
    String str5 = null;
    str3 = null;
    try {
      RuntimeException runtimeException1, runtimeException2;
      Cursor cursor = paramContext.getContentResolver().query(uri, new String[] { str4 }, null, null, null);
      str3 = str2;
      if (cursor != null)
        str3 = str2; 
      RuntimeException runtimeException3 = runtimeException2;
      if (runtimeException1 != null) {
        runtimeException1.close();
        runtimeException3 = runtimeException2;
      } 
    } catch (RuntimeException runtimeException) {
      str4 = str5;
      str3 = str4;
      Rlog.e("OplusOSPhoneNumberUtils", "Error getting phone number.", runtimeException);
      if (str4 != null)
        str4.close(); 
      str4 = str1;
    } finally {}
    return checkAndAppendPrefix((Intent)arrayOfInt, bool, str4, paramContext);
  }
}
