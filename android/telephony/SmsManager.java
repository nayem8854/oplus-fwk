package android.telephony;

import android.annotation.SystemApi;
import android.app.PendingIntent;
import android.compat.Compatibility;
import android.content.Context;
import android.database.CursorWindow;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.TelephonyServiceManager;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.util.SeempLog;
import com.android.internal.telephony.IIntegerConsumer;
import com.android.internal.telephony.ISms;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.SmsRawData;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

public final class SmsManager {
  public static final int CDMA_SMS_RECORD_LENGTH = 255;
  
  public static final String EXTRA_MMS_DATA = "android.telephony.extra.MMS_DATA";
  
  public static final String EXTRA_MMS_HTTP_STATUS = "android.telephony.extra.MMS_HTTP_STATUS";
  
  public static final String EXTRA_SIM_SUBSCRIPTION_ID = "android.telephony.extra.SIM_SUBSCRIPTION_ID";
  
  public static final String EXTRA_SMS_MESSAGE = "android.telephony.extra.SMS_MESSAGE";
  
  public static final String EXTRA_STATUS = "android.telephony.extra.STATUS";
  
  private static final long GET_TARGET_SDK_VERSION_CODE_CHANGE = 145147528L;
  
  public static final String MMS_CONFIG_ALIAS_ENABLED = "aliasEnabled";
  
  public static final String MMS_CONFIG_ALIAS_MAX_CHARS = "aliasMaxChars";
  
  public static final String MMS_CONFIG_ALIAS_MIN_CHARS = "aliasMinChars";
  
  public static final String MMS_CONFIG_ALLOW_ATTACH_AUDIO = "allowAttachAudio";
  
  public static final String MMS_CONFIG_APPEND_TRANSACTION_ID = "enabledTransID";
  
  public static final String MMS_CONFIG_CLOSE_CONNECTION = "mmsCloseConnection";
  
  public static final String MMS_CONFIG_EMAIL_GATEWAY_NUMBER = "emailGatewayNumber";
  
  public static final String MMS_CONFIG_GROUP_MMS_ENABLED = "enableGroupMms";
  
  public static final String MMS_CONFIG_HTTP_PARAMS = "httpParams";
  
  public static final String MMS_CONFIG_HTTP_SOCKET_TIMEOUT = "httpSocketTimeout";
  
  public static final String MMS_CONFIG_MAX_IMAGE_HEIGHT = "maxImageHeight";
  
  public static final String MMS_CONFIG_MAX_IMAGE_WIDTH = "maxImageWidth";
  
  public static final String MMS_CONFIG_MAX_MESSAGE_SIZE = "maxMessageSize";
  
  public static final String MMS_CONFIG_MESSAGE_TEXT_MAX_SIZE = "maxMessageTextSize";
  
  public static final String MMS_CONFIG_MMS_DELIVERY_REPORT_ENABLED = "enableMMSDeliveryReports";
  
  public static final String MMS_CONFIG_MMS_ENABLED = "enabledMMS";
  
  public static final String MMS_CONFIG_MMS_READ_REPORT_ENABLED = "enableMMSReadReports";
  
  public static final String MMS_CONFIG_MULTIPART_SMS_ENABLED = "enableMultipartSMS";
  
  public static final String MMS_CONFIG_NAI_SUFFIX = "naiSuffix";
  
  public static final String MMS_CONFIG_NOTIFY_WAP_MMSC_ENABLED = "enabledNotifyWapMMSC";
  
  public static final String MMS_CONFIG_RECIPIENT_LIMIT = "recipientLimit";
  
  public static final String MMS_CONFIG_SEND_MULTIPART_SMS_AS_SEPARATE_MESSAGES = "sendMultipartSmsAsSeparateMessages";
  
  public static final String MMS_CONFIG_SHOW_CELL_BROADCAST_APP_LINKS = "config_cellBroadcastAppLinks";
  
  public static final String MMS_CONFIG_SMS_DELIVERY_REPORT_ENABLED = "enableSMSDeliveryReports";
  
  public static final String MMS_CONFIG_SMS_TO_MMS_TEXT_LENGTH_THRESHOLD = "smsToMmsTextLengthThreshold";
  
  public static final String MMS_CONFIG_SMS_TO_MMS_TEXT_THRESHOLD = "smsToMmsTextThreshold";
  
  public static final String MMS_CONFIG_SUBJECT_MAX_LENGTH = "maxSubjectLength";
  
  public static final String MMS_CONFIG_SUPPORT_HTTP_CHARSET_HEADER = "supportHttpCharsetHeader";
  
  public static final String MMS_CONFIG_SUPPORT_MMS_CONTENT_DISPOSITION = "supportMmsContentDisposition";
  
  public static final String MMS_CONFIG_UA_PROF_TAG_NAME = "uaProfTagName";
  
  public static final String MMS_CONFIG_UA_PROF_URL = "uaProfUrl";
  
  public static final String MMS_CONFIG_USER_AGENT = "userAgent";
  
  public static final int MMS_ERROR_CONFIGURATION_ERROR = 7;
  
  public static final int MMS_ERROR_HTTP_FAILURE = 4;
  
  public static final int MMS_ERROR_INVALID_APN = 2;
  
  public static final int MMS_ERROR_IO_ERROR = 5;
  
  public static final int MMS_ERROR_NO_DATA_NETWORK = 8;
  
  public static final int MMS_ERROR_RETRY = 6;
  
  public static final int MMS_ERROR_UNABLE_CONNECT_MMS = 3;
  
  public static final int MMS_ERROR_UNSPECIFIED = 1;
  
  @SystemApi
  public static final int PREMIUM_SMS_CONSENT_ALWAYS_ALLOW = 3;
  
  @SystemApi
  public static final int PREMIUM_SMS_CONSENT_ASK_USER = 1;
  
  @SystemApi
  public static final int PREMIUM_SMS_CONSENT_NEVER_ALLOW = 2;
  
  @SystemApi
  public static final int PREMIUM_SMS_CONSENT_UNKNOWN = 0;
  
  public static final String REGEX_PREFIX_DELIMITER = ",";
  
  public static final int RESULT_BLUETOOTH_DISCONNECTED = 27;
  
  public static final int RESULT_CANCELLED = 23;
  
  public static final int RESULT_ENCODING_ERROR = 18;
  
  public static final int RESULT_ERROR_FDN_CHECK_FAILURE = 6;
  
  public static final int RESULT_ERROR_GENERIC_FAILURE = 1;
  
  public static final int RESULT_ERROR_LIMIT_EXCEEDED = 5;
  
  public static final int RESULT_ERROR_NONE = 0;
  
  public static final int RESULT_ERROR_NO_SERVICE = 4;
  
  public static final int RESULT_ERROR_NULL_PDU = 3;
  
  public static final int RESULT_ERROR_RADIO_OFF = 2;
  
  public static final int RESULT_ERROR_SHORT_CODE_NEVER_ALLOWED = 8;
  
  public static final int RESULT_ERROR_SHORT_CODE_NOT_ALLOWED = 7;
  
  public static final int RESULT_INTERNAL_ERROR = 21;
  
  public static final int RESULT_INVALID_ARGUMENTS = 11;
  
  public static final int RESULT_INVALID_BLUETOOTH_ADDRESS = 26;
  
  public static final int RESULT_INVALID_SMSC_ADDRESS = 19;
  
  public static final int RESULT_INVALID_SMS_FORMAT = 14;
  
  public static final int RESULT_INVALID_STATE = 12;
  
  public static final int RESULT_MODEM_ERROR = 16;
  
  public static final int RESULT_NETWORK_ERROR = 17;
  
  public static final int RESULT_NETWORK_REJECT = 10;
  
  public static final int RESULT_NO_BLUETOOTH_SERVICE = 25;
  
  public static final int RESULT_NO_DEFAULT_SMS_APP = 32;
  
  public static final int RESULT_NO_MEMORY = 13;
  
  public static final int RESULT_NO_RESOURCES = 22;
  
  public static final int RESULT_OPERATION_NOT_ALLOWED = 20;
  
  public static final int RESULT_RADIO_NOT_AVAILABLE = 9;
  
  public static final int RESULT_RECEIVE_DISPATCH_FAILURE = 500;
  
  public static final int RESULT_RECEIVE_INJECTED_NULL_PDU = 501;
  
  public static final int RESULT_RECEIVE_NULL_MESSAGE_FROM_RIL = 503;
  
  public static final int RESULT_RECEIVE_RUNTIME_EXCEPTION = 502;
  
  public static final int RESULT_RECEIVE_SQL_EXCEPTION = 505;
  
  public static final int RESULT_RECEIVE_URI_EXCEPTION = 506;
  
  public static final int RESULT_RECEIVE_WHILE_ENCRYPTED = 504;
  
  public static final int RESULT_REMOTE_EXCEPTION = 31;
  
  public static final int RESULT_REQUEST_NOT_SUPPORTED = 24;
  
  public static final int RESULT_RIL_CANCELLED = 119;
  
  public static final int RESULT_RIL_ENCODING_ERR = 109;
  
  public static final int RESULT_RIL_INTERNAL_ERR = 113;
  
  public static final int RESULT_RIL_INVALID_ARGUMENTS = 104;
  
  public static final int RESULT_RIL_INVALID_MODEM_STATE = 115;
  
  public static final int RESULT_RIL_INVALID_SMSC_ADDRESS = 110;
  
  public static final int RESULT_RIL_INVALID_SMS_FORMAT = 107;
  
  public static final int RESULT_RIL_INVALID_STATE = 103;
  
  public static final int RESULT_RIL_MODEM_ERR = 111;
  
  public static final int RESULT_RIL_NETWORK_ERR = 112;
  
  public static final int RESULT_RIL_NETWORK_NOT_READY = 116;
  
  public static final int RESULT_RIL_NETWORK_REJECT = 102;
  
  public static final int RESULT_RIL_NO_MEMORY = 105;
  
  public static final int RESULT_RIL_NO_RESOURCES = 118;
  
  public static final int RESULT_RIL_OPERATION_NOT_ALLOWED = 117;
  
  public static final int RESULT_RIL_RADIO_NOT_AVAILABLE = 100;
  
  public static final int RESULT_RIL_REQUEST_NOT_SUPPORTED = 114;
  
  public static final int RESULT_RIL_REQUEST_RATE_LIMITED = 106;
  
  public static final int RESULT_RIL_SIM_ABSENT = 120;
  
  public static final int RESULT_RIL_SMS_SEND_FAIL_RETRY = 101;
  
  public static final int RESULT_RIL_SYSTEM_ERR = 108;
  
  public static final int RESULT_SMS_BLOCKED_DURING_EMERGENCY = 29;
  
  public static final int RESULT_SMS_SEND_RETRY_FAILED = 30;
  
  public static final int RESULT_STATUS_SUCCESS = 0;
  
  public static final int RESULT_STATUS_TIMEOUT = 1;
  
  public static final int RESULT_SYSTEM_ERROR = 15;
  
  public static final int RESULT_UNEXPECTED_EVENT_STOP_SENDING = 28;
  
  public static final int SMS_CATEGORY_FREE_SHORT_CODE = 1;
  
  public static final int SMS_CATEGORY_NOT_SHORT_CODE = 0;
  
  public static final int SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE = 3;
  
  public static final int SMS_CATEGORY_PREMIUM_SHORT_CODE = 4;
  
  public static final int SMS_CATEGORY_STANDARD_SHORT_CODE = 2;
  
  public static final int SMS_MESSAGE_PERIOD_NOT_SPECIFIED = -1;
  
  public static final int SMS_MESSAGE_PRIORITY_NOT_SPECIFIED = -1;
  
  public static final int SMS_RECORD_LENGTH = 176;
  
  public static final int STATUS_ON_ICC_FREE = 0;
  
  public static final int STATUS_ON_ICC_READ = 1;
  
  public static final int STATUS_ON_ICC_SENT = 5;
  
  public static final int STATUS_ON_ICC_UNREAD = 3;
  
  public static final int STATUS_ON_ICC_UNSENT = 7;
  
  private static final String TAG = "SmsManager";
  
  private static final SmsManager sInstance = new SmsManager(2147483647);
  
  private static final Object sLockObject = new Object();
  
  private static final Map<Integer, SmsManager> sSubInstances = new ArrayMap<>();
  
  private int mSubId;
  
  public void sendTextMessage(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2) {
    SeempLog.record_str(75, paramString1);
    sendTextMessageInternal(paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2, true, (String)null, (String)null, 0L);
  }
  
  public void sendTextMessage(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, long paramLong) {
    sendTextMessageInternal(paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2, true, (String)null, (String)null, paramLong);
  }
  
  public void sendTextMessage(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, int paramInt1, boolean paramBoolean, int paramInt2) {
    sendTextMessageInternal(paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2, true, paramInt1, paramBoolean, paramInt2);
  }
  
  private void sendTextMessageInternal(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, boolean paramBoolean, String paramString4, String paramString5, long paramLong) {
    if (!TextUtils.isEmpty(paramString1)) {
      if (!TextUtils.isEmpty(paramString3)) {
        if (paramBoolean) {
          resolveSubscriptionForOperation((SubscriptionResolverResult)new Object(this, paramString4, paramString5, paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2, paramBoolean, paramLong));
        } else {
          ISms iSms = getISmsServiceOrThrow();
          try {
            iSms.sendTextForSubscriber(getSubscriptionId(), paramString4, paramString5, paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2, paramBoolean, paramLong);
          } catch (RemoteException remoteException) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("sendTextMessageInternal (no persist): Couldn't send SMS, exception - ");
            stringBuilder.append(remoteException.getMessage());
            stringBuilder.append(" id: ");
            stringBuilder.append(paramLong);
            String str = stringBuilder.toString();
            Log.e("SmsManager", str);
            notifySmsError(paramPendingIntent1, 31);
          } 
        } 
        return;
      } 
      throw new IllegalArgumentException("Invalid message body");
    } 
    throw new IllegalArgumentException("Invalid destinationAddress");
  }
  
  public void sendTextMessageWithoutPersisting(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2) {
    sendTextMessageInternal(paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2, false, (String)null, (String)null, 0L);
  }
  
  private void sendTextMessageInternal(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, boolean paramBoolean1, int paramInt1, boolean paramBoolean2, int paramInt2) {
    if (!TextUtils.isEmpty(paramString1)) {
      if (!TextUtils.isEmpty(paramString3)) {
        if (paramInt1 < 0 || paramInt1 > 3)
          paramInt1 = -1; 
        if (paramInt2 < 5 || paramInt2 > 635040)
          paramInt2 = -1; 
        if (paramBoolean1) {
          resolveSubscriptionForOperation((SubscriptionResolverResult)new Object(this, paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2, paramBoolean1, paramInt1, paramBoolean2, paramInt2));
        } else {
          try {
            ISms iSms = getISmsServiceOrThrow();
            if (iSms != null)
              iSms.sendTextForSubscriberWithOptions(getSubscriptionId(), null, null, paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2, paramBoolean1, paramInt1, paramBoolean2, paramInt2); 
          } catch (RemoteException remoteException) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("sendTextMessageInternal(no persist): Couldn't send SMS, exception - ");
            stringBuilder.append(remoteException.getMessage());
            String str = stringBuilder.toString();
            Log.e("SmsManager", str);
            notifySmsError(paramPendingIntent1, 31);
          } 
        } 
        return;
      } 
      throw new IllegalArgumentException("Invalid message body");
    } 
    throw new IllegalArgumentException("Invalid destinationAddress");
  }
  
  public void sendTextMessageWithoutPersisting(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, int paramInt1, boolean paramBoolean, int paramInt2) {
    sendTextMessageInternal(paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2, false, paramInt1, paramBoolean, paramInt2);
  }
  
  public void injectSmsPdu(byte[] paramArrayOfbyte, String paramString, PendingIntent paramPendingIntent) {
    if (paramString.equals("3gpp") || paramString.equals("3gpp2")) {
      try {
        ISms iSms = TelephonyManager.getSmsService();
        if (iSms != null) {
          int i = getSubscriptionId();
          iSms.injectSmsPduForSubscriber(i, paramArrayOfbyte, paramString, paramPendingIntent);
        } 
      } catch (RemoteException remoteException) {
        if (paramPendingIntent != null)
          try {
            paramPendingIntent.send(31);
          } catch (android.app.PendingIntent.CanceledException canceledException) {} 
      } 
      return;
    } 
    throw new IllegalArgumentException("Invalid pdu format. format must be either 3gpp or 3gpp2");
  }
  
  public ArrayList<String> divideMessage(String paramString) {
    if (paramString != null)
      return SmsMessage.fragmentText(paramString, getSubscriptionId()); 
    throw new IllegalArgumentException("text is null");
  }
  
  public void sendMultipartTextMessage(String paramString1, String paramString2, ArrayList<String> paramArrayList, ArrayList<PendingIntent> paramArrayList1, ArrayList<PendingIntent> paramArrayList2) {
    sendMultipartTextMessageInternal(paramString1, paramString2, paramArrayList, paramArrayList1, paramArrayList2, true, (String)null, (String)null, 0L);
  }
  
  public void sendMultipartTextMessage(String paramString1, String paramString2, List<String> paramList, List<PendingIntent> paramList1, List<PendingIntent> paramList2, long paramLong) {
    sendMultipartTextMessageInternal(paramString1, paramString2, paramList, paramList1, paramList2, true, (String)null, (String)null, paramLong);
  }
  
  public void sendMultipartTextMessage(String paramString1, String paramString2, List<String> paramList, List<PendingIntent> paramList1, List<PendingIntent> paramList2, String paramString3, String paramString4) {
    sendMultipartTextMessageInternal(paramString1, paramString2, paramList, paramList1, paramList2, true, paramString3, paramString4, 0L);
  }
  
  private void sendMultipartTextMessageInternal(String paramString1, String paramString2, List<String> paramList, List<PendingIntent> paramList1, List<PendingIntent> paramList2, boolean paramBoolean, String paramString3, String paramString4, long paramLong) {
    if (!TextUtils.isEmpty(paramString1)) {
      if (paramList != null && paramList.size() >= 1) {
        String str;
        StringBuilder stringBuilder;
        if (paramList.size() > 1) {
          if (paramBoolean) {
            resolveSubscriptionForOperation((SubscriptionResolverResult)new Object(this, paramString3, paramString4, paramString1, paramString2, paramList, paramList1, paramList2, paramBoolean, paramLong));
          } else {
            try {
              ISms iSms = getISmsServiceOrThrow();
              if (iSms != null)
                iSms.sendMultipartTextForSubscriber(getSubscriptionId(), paramString3, paramString4, paramString1, paramString2, paramList, paramList1, paramList2, paramBoolean, paramLong); 
            } catch (RemoteException remoteException) {
              stringBuilder = new StringBuilder();
              stringBuilder.append("sendMultipartTextMessageInternal: Couldn't send SMS - ");
              stringBuilder.append(remoteException.getMessage());
              stringBuilder.append(" id: ");
              stringBuilder.append(paramLong);
              str = stringBuilder.toString();
              Log.e("SmsManager", str);
              notifySmsError(paramList1, 31);
            } 
          } 
        } else {
          PendingIntent pendingIntent2 = null;
          PendingIntent pendingIntent1 = pendingIntent2;
          if (paramList1 != null) {
            pendingIntent1 = pendingIntent2;
            if (paramList1.size() > 0)
              pendingIntent1 = paramList1.get(0); 
          } 
          if (paramList2 != null && paramList2.size() > 0) {
            PendingIntent pendingIntent = paramList2.get(0);
          } else {
            paramList1 = null;
          } 
          sendTextMessageInternal(str, (String)stringBuilder, paramList.get(0), pendingIntent1, (PendingIntent)paramList1, true, paramString3, paramString4, paramLong);
        } 
        return;
      } 
      throw new IllegalArgumentException("Invalid message body");
    } 
    throw new IllegalArgumentException("Invalid destinationAddress");
  }
  
  @SystemApi
  public void sendMultipartTextMessageWithoutPersisting(String paramString1, String paramString2, List<String> paramList, List<PendingIntent> paramList1, List<PendingIntent> paramList2) {
    sendMultipartTextMessageInternal(paramString1, paramString2, paramList, paramList1, paramList2, false, (String)null, (String)null, 0L);
  }
  
  public void sendMultipartTextMessage(String paramString1, String paramString2, ArrayList<String> paramArrayList, ArrayList<PendingIntent> paramArrayList1, ArrayList<PendingIntent> paramArrayList2, int paramInt1, boolean paramBoolean, int paramInt2) {
    sendMultipartTextMessageInternal(paramString1, paramString2, paramArrayList, paramArrayList1, paramArrayList2, true, paramInt1, paramBoolean, paramInt2);
  }
  
  private void sendMultipartTextMessageInternal(String paramString1, String paramString2, List<String> paramList, List<PendingIntent> paramList1, List<PendingIntent> paramList2, boolean paramBoolean1, int paramInt1, boolean paramBoolean2, int paramInt2) {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   4: ifne -> 323
    //   7: aload_3
    //   8: ifnull -> 312
    //   11: aload_3
    //   12: invokeinterface size : ()I
    //   17: iconst_1
    //   18: if_icmplt -> 312
    //   21: iload #7
    //   23: iflt -> 38
    //   26: iload #7
    //   28: iconst_3
    //   29: if_icmple -> 35
    //   32: goto -> 38
    //   35: goto -> 41
    //   38: iconst_m1
    //   39: istore #7
    //   41: iload #9
    //   43: iconst_5
    //   44: if_icmplt -> 61
    //   47: iload #9
    //   49: ldc_w 635040
    //   52: if_icmple -> 58
    //   55: goto -> 61
    //   58: goto -> 64
    //   61: iconst_m1
    //   62: istore #9
    //   64: aload_3
    //   65: invokeinterface size : ()I
    //   70: iconst_1
    //   71: if_icmple -> 210
    //   74: iload #6
    //   76: ifeq -> 109
    //   79: aload_0
    //   80: new android/telephony/SmsManager$4
    //   83: dup
    //   84: aload_0
    //   85: aload_1
    //   86: aload_2
    //   87: aload_3
    //   88: aload #4
    //   90: aload #5
    //   92: iload #6
    //   94: iload #7
    //   96: iload #8
    //   98: iload #9
    //   100: invokespecial <init> : (Landroid/telephony/SmsManager;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZIZI)V
    //   103: invokespecial resolveSubscriptionForOperation : (Landroid/telephony/SmsManager$SubscriptionResolverResult;)V
    //   106: goto -> 207
    //   109: invokestatic getISmsServiceOrThrow : ()Lcom/android/internal/telephony/ISms;
    //   112: astore #10
    //   114: aload #10
    //   116: ifnull -> 158
    //   119: aload_0
    //   120: invokevirtual getSubscriptionId : ()I
    //   123: istore #11
    //   125: aload #10
    //   127: iload #11
    //   129: aconst_null
    //   130: aconst_null
    //   131: aload_1
    //   132: aload_2
    //   133: aload_3
    //   134: aload #4
    //   136: aload #5
    //   138: iload #6
    //   140: iload #7
    //   142: iload #8
    //   144: iload #9
    //   146: invokeinterface sendMultipartTextForSubscriberWithOptions : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZIZI)V
    //   151: goto -> 158
    //   154: astore_1
    //   155: goto -> 162
    //   158: goto -> 207
    //   161: astore_1
    //   162: new java/lang/StringBuilder
    //   165: dup
    //   166: invokespecial <init> : ()V
    //   169: astore_2
    //   170: aload_2
    //   171: ldc_w 'sendMultipartTextMessageInternal (no persist): Couldn't send SMS - '
    //   174: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   177: pop
    //   178: aload_2
    //   179: aload_1
    //   180: invokevirtual getMessage : ()Ljava/lang/String;
    //   183: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   186: pop
    //   187: aload_2
    //   188: invokevirtual toString : ()Ljava/lang/String;
    //   191: astore_1
    //   192: ldc_w 'SmsManager'
    //   195: aload_1
    //   196: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   199: pop
    //   200: aload #4
    //   202: bipush #31
    //   204: invokestatic notifySmsError : (Ljava/util/List;I)V
    //   207: goto -> 311
    //   210: aconst_null
    //   211: astore #12
    //   213: aload #12
    //   215: astore #10
    //   217: aload #4
    //   219: ifnull -> 249
    //   222: aload #12
    //   224: astore #10
    //   226: aload #4
    //   228: invokeinterface size : ()I
    //   233: ifle -> 249
    //   236: aload #4
    //   238: iconst_0
    //   239: invokeinterface get : (I)Ljava/lang/Object;
    //   244: checkcast android/app/PendingIntent
    //   247: astore #10
    //   249: aload #5
    //   251: ifnull -> 280
    //   254: aload #5
    //   256: invokeinterface size : ()I
    //   261: ifle -> 280
    //   264: aload #5
    //   266: iconst_0
    //   267: invokeinterface get : (I)Ljava/lang/Object;
    //   272: checkcast android/app/PendingIntent
    //   275: astore #4
    //   277: goto -> 283
    //   280: aconst_null
    //   281: astore #4
    //   283: aload_0
    //   284: aload_1
    //   285: aload_2
    //   286: aload_3
    //   287: iconst_0
    //   288: invokeinterface get : (I)Ljava/lang/Object;
    //   293: checkcast java/lang/String
    //   296: aload #10
    //   298: aload #4
    //   300: iload #6
    //   302: iload #7
    //   304: iload #8
    //   306: iload #9
    //   308: invokespecial sendTextMessageInternal : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;ZIZI)V
    //   311: return
    //   312: new java/lang/IllegalArgumentException
    //   315: dup
    //   316: ldc_w 'Invalid message body'
    //   319: invokespecial <init> : (Ljava/lang/String;)V
    //   322: athrow
    //   323: new java/lang/IllegalArgumentException
    //   326: dup
    //   327: ldc_w 'Invalid destinationAddress'
    //   330: invokespecial <init> : (Ljava/lang/String;)V
    //   333: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1228	-> 0
    //   #1231	-> 7
    //   #1235	-> 21
    //   #1236	-> 38
    //   #1239	-> 41
    //   #1240	-> 61
    //   #1243	-> 64
    //   #1244	-> 74
    //   #1245	-> 74
    //   #1246	-> 74
    //   #1247	-> 79
    //   #1273	-> 109
    //   #1274	-> 114
    //   #1275	-> 119
    //   #1280	-> 154
    //   #1274	-> 158
    //   #1284	-> 158
    //   #1280	-> 161
    //   #1281	-> 162
    //   #1282	-> 178
    //   #1281	-> 192
    //   #1283	-> 200
    //   #1286	-> 207
    //   #1287	-> 210
    //   #1288	-> 213
    //   #1289	-> 213
    //   #1290	-> 236
    //   #1292	-> 249
    //   #1293	-> 264
    //   #1295	-> 280
    //   #1299	-> 311
    //   #1231	-> 312
    //   #1232	-> 312
    //   #1229	-> 323
    // Exception table:
    //   from	to	target	type
    //   109	114	161	android/os/RemoteException
    //   119	125	161	android/os/RemoteException
    //   125	151	154	android/os/RemoteException
  }
  
  public void sendDataMessage(String paramString1, String paramString2, short paramShort, byte[] paramArrayOfbyte, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2) {
    SeempLog.record_str(73, paramString1);
    if (!TextUtils.isEmpty(paramString1)) {
      if (paramArrayOfbyte != null && paramArrayOfbyte.length != 0) {
        resolveSubscriptionForOperation((SubscriptionResolverResult)new Object(this, paramString1, paramString2, paramShort, paramArrayOfbyte, paramPendingIntent1, paramPendingIntent2));
        return;
      } 
      throw new IllegalArgumentException("Invalid message data");
    } 
    throw new IllegalArgumentException("Invalid destinationAddress");
  }
  
  public static SmsManager getDefault() {
    return sInstance;
  }
  
  public static SmsManager getSmsManagerForSubscriptionId(int paramInt) {
    synchronized (sLockObject) {
      SmsManager smsManager1 = sSubInstances.get(Integer.valueOf(paramInt));
      SmsManager smsManager2 = smsManager1;
      if (smsManager1 == null) {
        smsManager2 = new SmsManager();
        this(paramInt);
        sSubInstances.put(Integer.valueOf(paramInt), smsManager2);
      } 
      return smsManager2;
    } 
  }
  
  private SmsManager(int paramInt) {
    this.mSubId = paramInt;
  }
  
  public int getSubscriptionId() {
    try {
      int i;
      if (this.mSubId == Integer.MAX_VALUE) {
        i = getISmsServiceOrThrow().getPreferredSmsSubscription();
      } else {
        i = this.mSubId;
      } 
      return i;
    } catch (RemoteException remoteException) {
      return -1;
    } 
  }
  
  private void resolveSubscriptionForOperation(SubscriptionResolverResult paramSubscriptionResolverResult) {
    int i = getSubscriptionId();
    boolean bool = false;
    boolean bool1 = false;
    try {
      ISms iSms = getISmsService();
      if (iSms != null)
        bool1 = iSms.isSmsSimPickActivityNeeded(i); 
    } catch (RemoteException remoteException) {
      Log.e("SmsManager", "resolveSubscriptionForOperation", (Throwable)remoteException);
      bool1 = bool;
    } 
    if (!bool1) {
      sendResolverResult(paramSubscriptionResolverResult, i, false);
      return;
    } 
    Log.d("SmsManager", "resolveSubscriptionForOperation isSmsSimPickActivityNeeded is true for calling package. ");
    try {
      ITelephony iTelephony = getITelephony();
      Object object = new Object();
      super(this, paramSubscriptionResolverResult);
      iTelephony.enqueueSmsPickResult(null, null, (IIntegerConsumer)object);
    } catch (RemoteException remoteException) {
      Log.e("SmsManager", "Unable to launch activity", (Throwable)remoteException);
      sendResolverResult(paramSubscriptionResolverResult, i, true);
    } 
  }
  
  private void sendResolverResult(SubscriptionResolverResult paramSubscriptionResolverResult, int paramInt, boolean paramBoolean) {
    if (SubscriptionManager.isValidSubscriptionId(paramInt)) {
      paramSubscriptionResolverResult.onSuccess(paramInt);
      return;
    } 
    if (!Compatibility.isChangeEnabled(145147528L) && !paramBoolean) {
      paramSubscriptionResolverResult.onSuccess(paramInt);
    } else {
      paramSubscriptionResolverResult.onFailure();
    } 
  }
  
  private static ITelephony getITelephony() {
    TelephonyServiceManager telephonyServiceManager = TelephonyFrameworkInitializer.getTelephonyServiceManager();
    TelephonyServiceManager.ServiceRegisterer serviceRegisterer = telephonyServiceManager.getTelephonyServiceRegisterer();
    IBinder iBinder = serviceRegisterer.get();
    ITelephony iTelephony = ITelephony.Stub.asInterface(iBinder);
    if (iTelephony != null)
      return iTelephony; 
    throw new RuntimeException("Could not find Telephony Service.");
  }
  
  private static void notifySmsError(PendingIntent paramPendingIntent, int paramInt) {
    if (paramPendingIntent != null)
      try {
        paramPendingIntent.send(paramInt);
      } catch (android.app.PendingIntent.CanceledException canceledException) {} 
  }
  
  private static void notifySmsError(List<PendingIntent> paramList, int paramInt) {
    if (paramList != null)
      for (PendingIntent pendingIntent : paramList)
        notifySmsError(pendingIntent, paramInt);  
  }
  
  private static ISms getISmsServiceOrThrow() {
    ISms iSms = TelephonyManager.getSmsService();
    if (iSms != null)
      return iSms; 
    throw new UnsupportedOperationException("Sms is not supported");
  }
  
  private static ISms getISmsService() {
    return TelephonyManager.getSmsService();
  }
  
  public boolean copyMessageToIcc(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, int paramInt) {
    SeempLog.record(79);
    boolean bool = false;
    boolean bool1 = false;
    if (paramArrayOfbyte2 != null) {
      try {
        ISms iSms = getISmsService();
        if (iSms != null)
          bool1 = iSms.copyMessageToIccEfForSubscriber(getSubscriptionId(), null, paramInt, paramArrayOfbyte2, paramArrayOfbyte1); 
      } catch (RemoteException remoteException) {
        bool1 = bool;
      } 
      return bool1;
    } 
    throw new IllegalArgumentException("pdu is null");
  }
  
  public boolean deleteMessageFromIcc(int paramInt) {
    SeempLog.record(80);
    boolean bool = false;
    boolean bool1 = false;
    try {
      ISms iSms = getISmsService();
      if (iSms != null)
        bool1 = iSms.updateMessageOnIccEfForSubscriber(getSubscriptionId(), null, paramInt, 0, null); 
    } catch (RemoteException remoteException) {
      bool1 = bool;
    } 
    return bool1;
  }
  
  public boolean updateMessageOnIcc(int paramInt1, int paramInt2, byte[] paramArrayOfbyte) {
    SeempLog.record(81);
    boolean bool = false;
    boolean bool1 = false;
    try {
      ISms iSms = getISmsService();
      if (iSms != null)
        bool1 = iSms.updateMessageOnIccEfForSubscriber(getSubscriptionId(), null, paramInt1, paramInt2, paramArrayOfbyte); 
    } catch (RemoteException remoteException) {
      bool1 = bool;
    } 
    return bool1;
  }
  
  public List<SmsMessage> getMessagesFromIcc() {
    return getAllMessagesFromIcc();
  }
  
  public ArrayList<SmsMessage> getAllMessagesFromIcc() {
    RemoteException remoteException1 = null;
    List list = null;
    try {
      ISms iSms = getISmsService();
      if (iSms != null) {
        int i = getSubscriptionId();
        list = iSms.getAllMessagesFromIccEfForSubscriber(i, null);
      } 
    } catch (RemoteException remoteException2) {
      remoteException2 = remoteException1;
    } 
    return createMessageListFromRawRecords((List<SmsRawData>)remoteException2);
  }
  
  @SystemApi
  public boolean enableCellBroadcastRange(int paramInt1, int paramInt2, int paramInt3) {
    boolean bool = false;
    boolean bool1 = false;
    if (paramInt2 >= paramInt1) {
      try {
        ISms iSms = getISmsService();
        if (iSms != null)
          bool1 = iSms.enableCellBroadcastRangeForSubscriber(getSubscriptionId(), paramInt1, paramInt2, paramInt3); 
      } catch (RemoteException remoteException) {
        bool1 = bool;
      } 
      return bool1;
    } 
    throw new IllegalArgumentException("endMessageId < startMessageId");
  }
  
  @SystemApi
  public boolean disableCellBroadcastRange(int paramInt1, int paramInt2, int paramInt3) {
    boolean bool = false;
    boolean bool1 = false;
    if (paramInt2 >= paramInt1) {
      try {
        ISms iSms = getISmsService();
        if (iSms != null)
          bool1 = iSms.disableCellBroadcastRangeForSubscriber(getSubscriptionId(), paramInt1, paramInt2, paramInt3); 
      } catch (RemoteException remoteException) {
        bool1 = bool;
      } 
      return bool1;
    } 
    throw new IllegalArgumentException("endMessageId < startMessageId");
  }
  
  private ArrayList<SmsMessage> createMessageListFromRawRecords(List<SmsRawData> paramList) {
    ArrayList<SmsMessage> arrayList = new ArrayList();
    if (paramList != null) {
      int i = paramList.size();
      for (byte b = 0; b < i; b++) {
        SmsRawData smsRawData = paramList.get(b);
        if (smsRawData != null) {
          byte[] arrayOfByte = smsRawData.getBytes();
          int j = getSubscriptionId();
          SmsMessage smsMessage = SmsMessage.createFromEfRecord(b + 1, arrayOfByte, j);
          if (smsMessage != null)
            arrayList.add(smsMessage); 
        } 
      } 
    } 
    return arrayList;
  }
  
  public boolean isImsSmsSupported() {
    boolean bool = false;
    boolean bool1 = false;
    try {
      ISms iSms = getISmsService();
      if (iSms != null)
        bool1 = iSms.isImsSmsSupportedForSubscriber(getSubscriptionId()); 
    } catch (RemoteException remoteException) {
      bool1 = bool;
    } 
    return bool1;
  }
  
  public String getImsSmsFormat() {
    String str2, str1 = "unknown";
    try {
      ISms iSms = getISmsService();
      str2 = str1;
      if (iSms != null)
        str2 = iSms.getImsSmsFormatForSubscriber(getSubscriptionId()); 
    } catch (RemoteException remoteException) {
      str2 = str1;
    } 
    return str2;
  }
  
  public static int getDefaultSmsSubscriptionId() {
    try {
      return getISmsService().getPreferredSmsSubscription();
    } catch (RemoteException remoteException) {
      return -1;
    } catch (NullPointerException nullPointerException) {
      return -1;
    } 
  }
  
  public boolean isSMSPromptEnabled() {
    try {
      ISms iSms = TelephonyManager.getSmsService();
      return iSms.isSMSPromptEnabled();
    } catch (RemoteException remoteException) {
      return false;
    } catch (NullPointerException nullPointerException) {
      return false;
    } 
  }
  
  @SystemApi
  public int getSmsCapacityOnIcc() {
    boolean bool = false;
    int i = 0;
    try {
      ISms iSms = getISmsService();
      if (iSms != null)
        i = iSms.getSmsCapacityOnIccForSubscriber(getSubscriptionId()); 
    } catch (RemoteException remoteException) {
      i = bool;
    } 
    return i;
  }
  
  public void sendMultimediaMessage(Context paramContext, Uri paramUri, String paramString, Bundle paramBundle, PendingIntent paramPendingIntent) {
    if (paramUri != null) {
      MmsManager mmsManager = (MmsManager)paramContext.getSystemService("mms");
      if (mmsManager != null)
        mmsManager.sendMultimediaMessage(getSubscriptionId(), paramUri, paramString, paramBundle, paramPendingIntent, 0L); 
      return;
    } 
    throw new IllegalArgumentException("Uri contentUri null");
  }
  
  public void downloadMultimediaMessage(Context paramContext, String paramString, Uri paramUri, Bundle paramBundle, PendingIntent paramPendingIntent) {
    if (!TextUtils.isEmpty(paramString)) {
      if (paramUri != null) {
        MmsManager mmsManager = (MmsManager)paramContext.getSystemService("mms");
        if (mmsManager != null)
          mmsManager.downloadMultimediaMessage(getSubscriptionId(), paramString, paramUri, paramBundle, paramPendingIntent, 0L); 
        return;
      } 
      throw new IllegalArgumentException("Uri contentUri null");
    } 
    throw new IllegalArgumentException("Empty MMS location URL");
  }
  
  public Bundle getCarrierConfigValues() {
    try {
      ISms iSms = getISmsService();
      if (iSms != null)
        return iSms.getCarrierConfigValuesForSubscriber(getSubscriptionId()); 
    } catch (RemoteException remoteException) {}
    return new Bundle();
  }
  
  public String createAppSpecificSmsToken(PendingIntent paramPendingIntent) {
    try {
      ISms iSms = getISmsServiceOrThrow();
      return iSms.createAppSpecificSmsToken(getSubscriptionId(), null, paramPendingIntent);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
      return null;
    } 
  }
  
  public static abstract class FinancialSmsCallback {
    public abstract void onFinancialSmsMessages(CursorWindow param1CursorWindow);
  }
  
  public void getSmsMessagesForFinancialApp(Bundle paramBundle, Executor paramExecutor, FinancialSmsCallback paramFinancialSmsCallback) {}
  
  public String createAppSpecificSmsTokenWithPackageInfo(String paramString, PendingIntent paramPendingIntent) {
    try {
      ISms iSms = getISmsServiceOrThrow();
      return iSms.createAppSpecificSmsTokenWithPackageInfo(getSubscriptionId(), null, paramString, paramPendingIntent);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
      return null;
    } 
  }
  
  public int checkSmsShortCodeDestination(String paramString1, String paramString2) {
    try {
      ISms iSms = getISmsServiceOrThrow();
      if (iSms != null)
        return iSms.checkSmsShortCodeDestination(getSubscriptionId(), null, null, paramString1, paramString2); 
    } catch (RemoteException remoteException) {
      Log.e("SmsManager", "checkSmsShortCodeDestination() RemoteException", (Throwable)remoteException);
    } 
    return 0;
  }
  
  public String getSmscAddress() {
    String str = null;
    try {
      ISms iSms = getISmsService();
      if (iSms != null) {
        int i = getSubscriptionId();
        str = iSms.getSmscAddressFromIccEfForSubscriber(i, null);
      } 
      return str;
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException);
    } 
  }
  
  public boolean setSmscAddress(String paramString) {
    try {
      ISms iSms = getISmsService();
      if (iSms != null) {
        int i = getSubscriptionId();
        return iSms.setSmscAddressOnIccEfForSubscriber(paramString, i, null);
      } 
      return false;
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException);
    } 
  }
  
  @SystemApi
  public int getPremiumSmsConsent(String paramString) {
    boolean bool = false;
    int i = 0;
    try {
      ISms iSms = getISmsService();
      if (iSms != null)
        i = iSms.getPremiumSmsPermission(paramString); 
    } catch (RemoteException remoteException) {
      Log.e("SmsManager", "getPremiumSmsPermission() RemoteException", (Throwable)remoteException);
      i = bool;
    } 
    return i;
  }
  
  @SystemApi
  public void setPremiumSmsConsent(String paramString, int paramInt) {
    try {
      ISms iSms = getISmsService();
      if (iSms != null)
        iSms.setPremiumSmsPermission(paramString, paramInt); 
    } catch (RemoteException remoteException) {
      Log.e("SmsManager", "setPremiumSmsPermission() RemoteException", (Throwable)remoteException);
    } 
  }
  
  public boolean resetAllCellBroadcastRanges() {
    boolean bool = false;
    boolean bool1 = false;
    try {
      ISms iSms = getISmsService();
      if (iSms != null)
        bool1 = iSms.resetAllCellBroadcastRanges(getSubscriptionId()); 
    } catch (RemoteException remoteException) {
      bool1 = bool;
    } 
    return bool1;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface PremiumSmsConsent {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Result {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SmsShortCodeCategory {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface StatusOnIcc {}
  
  private static interface SubscriptionResolverResult {
    void onFailure();
    
    void onSuccess(int param1Int);
  }
}
