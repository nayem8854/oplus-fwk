package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public final class PhoneCapability implements Parcelable {
  public static final Parcelable.Creator<PhoneCapability> CREATOR;
  
  public static final PhoneCapability DEFAULT_DSDS_CAPABILITY;
  
  public static final PhoneCapability DEFAULT_SSSS_CAPABILITY;
  
  public final List<ModemInfo> logicalModemList;
  
  public final int max5G;
  
  public final int maxActiveData;
  
  public final int maxActiveVoiceCalls;
  
  public final boolean validationBeforeSwitchSupported;
  
  static {
    ModemInfo modemInfo1 = new ModemInfo(0, 0, true, true);
    ModemInfo modemInfo2 = new ModemInfo(1, 0, true, true);
    ArrayList<ModemInfo> arrayList2 = new ArrayList();
    arrayList2.add(modemInfo1);
    arrayList2.add(modemInfo2);
    DEFAULT_DSDS_CAPABILITY = new PhoneCapability(1, 1, 0, arrayList2, false);
    ArrayList<ModemInfo> arrayList1 = new ArrayList();
    arrayList1.add(modemInfo1);
    DEFAULT_SSSS_CAPABILITY = new PhoneCapability(1, 1, 0, arrayList1, false);
    CREATOR = new Parcelable.Creator() {
        public PhoneCapability createFromParcel(Parcel param1Parcel) {
          return new PhoneCapability(param1Parcel);
        }
        
        public PhoneCapability[] newArray(int param1Int) {
          return new PhoneCapability[param1Int];
        }
      };
  }
  
  public PhoneCapability(int paramInt1, int paramInt2, int paramInt3, List<ModemInfo> paramList, boolean paramBoolean) {
    this.maxActiveVoiceCalls = paramInt1;
    this.maxActiveData = paramInt2;
    this.max5G = paramInt3;
    if (paramList == null)
      paramList = new ArrayList<>(); 
    this.logicalModemList = paramList;
    this.validationBeforeSwitchSupported = paramBoolean;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("maxActiveVoiceCalls=");
    stringBuilder.append(this.maxActiveVoiceCalls);
    stringBuilder.append(" maxActiveData=");
    stringBuilder.append(this.maxActiveData);
    stringBuilder.append(" max5G=");
    stringBuilder.append(this.max5G);
    stringBuilder.append("logicalModemList:");
    List<ModemInfo> list = this.logicalModemList;
    stringBuilder.append(Arrays.toString(list.toArray()));
    return stringBuilder.toString();
  }
  
  private PhoneCapability(Parcel paramParcel) {
    this.maxActiveVoiceCalls = paramParcel.readInt();
    this.maxActiveData = paramParcel.readInt();
    this.max5G = paramParcel.readInt();
    this.validationBeforeSwitchSupported = paramParcel.readBoolean();
    ArrayList<ModemInfo> arrayList = new ArrayList();
    paramParcel.readList(arrayList, ModemInfo.class.getClassLoader());
  }
  
  public int hashCode() {
    int i = this.maxActiveVoiceCalls, j = this.maxActiveData, k = this.max5G;
    List<ModemInfo> list = this.logicalModemList;
    boolean bool = this.validationBeforeSwitchSupported;
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), list, Boolean.valueOf(bool) });
  }
  
  public boolean equals(Object<ModemInfo> paramObject) {
    boolean bool = false;
    if (paramObject == null || !(paramObject instanceof PhoneCapability) || hashCode() != paramObject.hashCode())
      return false; 
    if (this == paramObject)
      return true; 
    PhoneCapability phoneCapability = (PhoneCapability)paramObject;
    if (this.maxActiveVoiceCalls == phoneCapability.maxActiveVoiceCalls && this.maxActiveData == phoneCapability.maxActiveData && this.max5G == phoneCapability.max5G && this.validationBeforeSwitchSupported == phoneCapability.validationBeforeSwitchSupported) {
      paramObject = (Object<ModemInfo>)this.logicalModemList;
      List<ModemInfo> list = phoneCapability.logicalModemList;
      if (paramObject.equals(list))
        bool = true; 
    } 
    return bool;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.maxActiveVoiceCalls);
    paramParcel.writeInt(this.maxActiveData);
    paramParcel.writeInt(this.max5G);
    paramParcel.writeBoolean(this.validationBeforeSwitchSupported);
    paramParcel.writeList(this.logicalModemList);
  }
}
