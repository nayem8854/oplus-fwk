package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public final class TelephonyDisplayInfo implements Parcelable {
  public TelephonyDisplayInfo(int paramInt1, int paramInt2) {
    this.mNetworkType = paramInt1;
    this.mOverrideNetworkType = paramInt2;
  }
  
  public TelephonyDisplayInfo(Parcel paramParcel) {
    this.mNetworkType = paramParcel.readInt();
    this.mOverrideNetworkType = paramParcel.readInt();
  }
  
  public int getNetworkType() {
    return this.mNetworkType;
  }
  
  public int getOverrideNetworkType() {
    return this.mOverrideNetworkType;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mNetworkType);
    paramParcel.writeInt(this.mOverrideNetworkType);
  }
  
  public static final Parcelable.Creator<TelephonyDisplayInfo> CREATOR = new Parcelable.Creator<TelephonyDisplayInfo>() {
      public TelephonyDisplayInfo createFromParcel(Parcel param1Parcel) {
        return new TelephonyDisplayInfo(param1Parcel);
      }
      
      public TelephonyDisplayInfo[] newArray(int param1Int) {
        return new TelephonyDisplayInfo[param1Int];
      }
    };
  
  public static final int OVERRIDE_NETWORK_TYPE_LTE_ADVANCED_PRO = 2;
  
  public static final int OVERRIDE_NETWORK_TYPE_LTE_CA = 1;
  
  public static final int OVERRIDE_NETWORK_TYPE_NONE = 0;
  
  public static final int OVERRIDE_NETWORK_TYPE_NR_NSA = 3;
  
  public static final int OVERRIDE_NETWORK_TYPE_NR_NSA_MMWAVE = 4;
  
  private final int mNetworkType;
  
  private final int mOverrideNetworkType;
  
  public int describeContents() {
    return 0;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mNetworkType != ((TelephonyDisplayInfo)paramObject).mNetworkType || this.mOverrideNetworkType != ((TelephonyDisplayInfo)paramObject).mOverrideNetworkType)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mNetworkType), Integer.valueOf(this.mOverrideNetworkType) });
  }
  
  public static String overrideNetworkTypeToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt != 4)
              return "UNKNOWN"; 
            return "NR_NSA_MMWAVE";
          } 
          return "NR_NSA";
        } 
        return "LTE_ADV_PRO";
      } 
      return "LTE_CA";
    } 
    return "NONE";
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("TelephonyDisplayInfo {network=");
    stringBuilder.append(TelephonyManager.getNetworkTypeName(this.mNetworkType));
    stringBuilder.append(", override=");
    int i = this.mOverrideNetworkType;
    stringBuilder.append(overrideNetworkTypeToString(i));
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
}
