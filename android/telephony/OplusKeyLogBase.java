package android.telephony;

import java.util.HashMap;
import java.util.Map;

public class OplusKeyLogBase {
  public static final String CALL_EVENT_ID = "050101";
  
  public static final String DATA_EVENT_ID = "050401";
  
  public static final int LENGTH_1_BYTE = 1;
  
  public static final int LENGTH_2_BYTES = 2;
  
  public static final int LENGTH_4_BYTES = 4;
  
  public static final int SYS_MTK_DMF_CUSTOM_APP = 20000;
  
  public static final int SYS_MTK_NR_SA_RACH_FAIL_REASON_AND_COUNT = 1014;
  
  public static final int SYS_MTK_NR_SA_REGISTRATION_FAILURE = 1017;
  
  public static final int SYS_MTK_NR_SA_RLF_REASON_AND_COUNT = 1015;
  
  public static final int SYS_MTK_NR_SA_RRC_REESTABLEISHMENT_REASON_AND_COUNT = 1016;
  
  public static final int SYS_MTK_URC_3GPP_OOS_LOW_POWER_FOR_DMFAPP = 701;
  
  public static final int SYS_MTK_URC_AC_BAR_SSAC_BAR_FOR_DMFAPP = 1011;
  
  public static final int SYS_MTK_URC_ATTACH_REJ_FOR_DMFAPP = 304;
  
  public static final int SYS_MTK_URC_AUTHENTICATION_REJECT = 395;
  
  public static final int SYS_MTK_URC_AUTHEN_REJ_FOR_DMFAPP = 305;
  
  public static final int SYS_MTK_URC_CALL_C2K_CALL_DROP_FOR_DMFAPP = 508;
  
  public static final int SYS_MTK_URC_CALL_C2K_CALL_QUALITY_FOR_DMFAPP = 512;
  
  public static final int SYS_MTK_URC_CALL_CS_CALL_DROP_FOR_DMFAPP = 505;
  
  public static final int SYS_MTK_URC_CALL_GWL_CALL_QUALITY_FOR_DMFAPP = 511;
  
  public static final int SYS_MTK_URC_CALL_MO_C2K_CALL_DROP_FOR_DMFAPP = 507;
  
  public static final int SYS_MTK_URC_CALL_MO_CSFB_CALL_DROP_FOR_DMFAPP = 503;
  
  public static final int SYS_MTK_URC_CALL_MO_CS_CALL_DROP_FOR_DMFAPP = 504;
  
  public static final int SYS_MTK_URC_CALL_MT_C2K_CALL_DROP_FOR_DMFAPP = 506;
  
  public static final int SYS_MTK_URC_CALL_MT_CSFB_CALL_DROP_FOR_DMFAPP = 501;
  
  public static final int SYS_MTK_URC_CALL_MT_CS_CALL_DROP_FOR_DMFAPP = 502;
  
  public static final int SYS_MTK_URC_CARD_DROP = 89;
  
  public static final int SYS_MTK_URC_CARD_DROP_FOR_DMFAPP = 601;
  
  public static final int SYS_MTK_URC_DATA_DL_HIGH_BLER = 970;
  
  public static final int SYS_MTK_URC_DATA_DL_HIGH_BLER_FOR_DMFAPP = 404;
  
  public static final int SYS_MTK_URC_DATA_NODDS_FREQUENT_PAGING = 603;
  
  public static final int SYS_MTK_URC_DATA_PDCP_UL_ONE_WAY_PASS = 947;
  
  public static final int SYS_MTK_URC_DATA_PDCP_UL_ONE_WAY_PASS_FOR_DMFAPP = 403;
  
  public static final int SYS_MTK_URC_DATA_PDCP_UL_OUT_OF_BUFFER = 949;
  
  public static final int SYS_MTK_URC_DATA_PDCP_UL_OUT_OF_BUFFER_FOR_DMFAPP = 402;
  
  public static final int SYS_MTK_URC_DATA_PDCP_UL_TIMEOUT = 948;
  
  public static final int SYS_MTK_URC_DATA_PDCP_UL_TIMEOUT_FOR_DMFAPP = 401;
  
  public static final int SYS_MTK_URC_DATA_RLC_UL_RLF = 400;
  
  public static final int SYS_MTK_URC_FOR_DMFAPP = 2;
  
  public static final int SYS_MTK_URC_IA_APN_ERROR = 1019;
  
  public static final int SYS_MTK_URC_IA_APN_ERROR_FOR_DMFAPP = 201;
  
  public static final int SYS_MTK_URC_IMS_CALL_HO_FAIL_FOR_DMFAPP = 510;
  
  public static final int SYS_MTK_URC_IMS_REG_FAIL_FOR_DMFAPP = 801;
  
  public static final int SYS_MTK_URC_LTE_A2_RELEASE_FOR_DMFAPP = 109;
  
  public static final int SYS_MTK_URC_LTE_AUTHENTICATION_REJECT = 628;
  
  public static final int SYS_MTK_URC_LTE_CELL_BAR_FOR_DMFAPP = 102;
  
  public static final int SYS_MTK_URC_LTE_CELL_REESTABLISHMENT_FAIL_FOR_DMFAPP = 103;
  
  public static final int SYS_MTK_URC_LTE_FAKE_CELL_BAR_FOR_DMFAPP = 108;
  
  public static final int SYS_MTK_URC_LTE_HANDOVER_FAILURE = 357;
  
  public static final int SYS_MTK_URC_LTE_HANDOVER_FAILURE_FOR_DMFAPP = 101;
  
  public static final int SYS_MTK_URC_LTE_MODE3_INTERFERENCE_FOR_DMFAPP = 104;
  
  public static final int SYS_MTK_URC_LTE_NARROW_BANDWIDTH_SCELL = 334;
  
  public static final int SYS_MTK_URC_LTE_NARROW_BANDWIDTH_SCELL_FOR_DMFAPP = 106;
  
  public static final int SYS_MTK_URC_LTE_NARROW_BW_MONITORING = 402;
  
  public static final int SYS_MTK_URC_LTE_NARROW_BW_MONITORING_FOR_DMFAPP = 107;
  
  public static final int SYS_MTK_URC_LTE_REG_REJECT = 625;
  
  public static final int SYS_MTK_URC_LTE_RLC_UL_RLF_FOR_DMFAPP = 105;
  
  public static final int SYS_MTK_URC_LTE_RRC_ABNORMAL_BAR = 356;
  
  public static final int SYS_MTK_URC_LTE_RRC_ABNORMAL_TIMEOUT = 401;
  
  public static final int SYS_MTK_URC_LU_REJ_FOR_DMFAPP = 301;
  
  public static final int SYS_MTK_URC_MT_CSFB = 393;
  
  public static final int SYS_MTK_URC_MT_RACH = 25;
  
  public static final int SYS_MTK_URC_MT_REJECT = 256;
  
  public static final int SYS_MTK_URC_MT_RLF_GSM = 26;
  
  public static final int SYS_MTK_URC_MT_RRC = 133;
  
  public static final int SYS_MTK_URC_NETWORK_DETACH_FOR_DMFAPP = 307;
  
  public static final int SYS_MTK_URC_OUT_OF_CREDIT_REJECT = 234;
  
  public static final int SYS_MTK_URC_OUT_OF_CREDIT_REJECT_FOR_DMFAPP = 10000;
  
  public static final int SYS_MTK_URC_PCI_MODE3_INTERFERENCE = 355;
  
  public static final int SYS_MTK_URC_PDP_ACT_ERROR_FOR_DMFAPP = 203;
  
  public static final int SYS_MTK_URC_PDP_DEACT_ERROR = 1018;
  
  public static final int SYS_MTK_URC_PDP_DEACT_ERROR_FOR_DMFAPP = 202;
  
  public static final int SYS_MTK_URC_RAU_REJ_FOR_DMFAPP = 302;
  
  public static final int SYS_MTK_URC_REG_REJECT = 394;
  
  public static final int SYS_MTK_URC_RF_MIPI_HW_FAILED = 108;
  
  public static final int SYS_MTK_URC_RF_MIPI_HW_FAILED_FOR_DMFAPP = 602;
  
  public static final int SYS_MTK_URC_SCREEN_ON_TRIGGER_NW_SRCH = 659;
  
  public static final int SYS_MTK_URC_SCREEN_ON_TRIGGER_NW_SRCH_FOR_DMFAPP = 702;
  
  public static final int SYS_MTK_URC_SERVICE_REJECT_FOR_DMFAPP = 306;
  
  public static final int SYS_MTK_URC_SMART_IDLE_TIMEOUT_MONITOR_FOR_DMFAPP = 703;
  
  public static final int SYS_MTK_URC_TAU_DETACH_SERVICE_REJECT = 233;
  
  public static final int SYS_MTK_URC_TAU_REJ_FOR_DMFAPP = 303;
  
  public static final int SYS_MTK_URC_VOLTE_CALL_DROP_FOR_DMFAPP = 509;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_AC_BAR_SSAC_BAR = 180;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_APN_REASON_DATA_CALL_FAIL = 31;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_AS_FAILED = 65;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_AUTHENTICATION_REJECT = 64;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_C2K_CALL_QUALITY = 41;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_CALL_BASE = 10;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_CARD_DROP_RX_BREAK = 160;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_CARD_DROP_TIME_OUT = 161;
  
  private static final int SYS_OEM_NW_DIAG_CAUSE_CARD_SUPPORT_VOLTAGE_CLASS_BOTH = 165;
  
  private static final int SYS_OEM_NW_DIAG_CAUSE_CARD_SUPPORT_VOLTAGE_CLASS_B_ONLY = 163;
  
  private static final int SYS_OEM_NW_DIAG_CAUSE_CARD_SUPPORT_VOLTAGE_CLASS_C_ONLY = 164;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_CAUSE_ACQ_CNT = 93;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_CAUSE_ENDC_PWR_OPT = 103;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_CAUSE_INACTIVE_FULLBAND_CNT = 96;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_CAUSE_SCENES_INFO = 99;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_CAUSE_SCREEN_OFF_ACQ_CNT = 94;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_CAUSE_SCREEN_ON_NW_SRCH = 91;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_CAUSE_SCREEN_ON_TRIGGER_NW_SRCH = 92;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_CAUSE_SKIP_ACQ_CNT = 95;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_CM_SERV_REJ = 22;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_CONGEST_RATIO = 36;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_CS_INVAILD_CNT = 139;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_AIRPLANE_NUM = 26;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_BASE = 110;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_CALL_ERROR = 34;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_CELL_HO_COUNT = 126;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_DDS_SWITCH_ERR = 152;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_DDS_SWITCH_RES = 151;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_DDS_SWITCH_TRIGGER = 150;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_DISCONNECT_CALL_ERROR = 30;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_DL_HIGH_BLER = 123;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_DNS_FAIL = 115;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_DORECOVERY_KPI = 132;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_DORECOVERY_RESULT = 131;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_DS_CK_KPIS = 153;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_DUMP_DISABLE_5G = 154;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_GAME_KPIS = 137;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_GAME_LATENCY = 114;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_NET_SLOW_RESULT = 135;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_NODDS_FREQUENT_PAGING = 141;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_NOT_ALLOWED = 112;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_NO_AVAILABLE_APN = 111;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_NSA_LTE_ANCHOR = 128;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_PDCP_UL_ONE_WAY_PASS = 122;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_PDCP_UL_OUT_OF_BUFFER = 121;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_PDCP_UL_TIMEOUT = 120;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_PDN_ACTIVATION_DURATION = 29;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_PS_STATE = 117;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_RATE_LIMIT_ON_LTE = 116;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_RECOVERY_SUCC_NUM = 28;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_RLC_UL_RLF = 127;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_RLF = 45;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_SA_MODE_CHANGE = 138;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_SET_UP_DATA_ERROR = 110;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_STALL_ERROR = 113;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_DATA_USER_DATA_ENABLE_NUM = 27;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_FAKE_BS = 70;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_FAKE_BS_ONLY = 71;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_FORBIDDEN_TAI_OPT = 84;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_GAME_SPACE_MT_REJECT = 46;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_GSM_T3126_EXPIRED = 66;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_GWL_CALL_QUALITY = 39;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_IA_APN_ERROR = 133;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_IMS_BASE = 260;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_IMS_CALL_DISC_ABNORMAL = 264;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_IMS_CALL_DROP_Q850 = 266;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_IMS_CALL_HO_FAIL = 42;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_IMS_CALL_NORMAL = 265;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_IMS_MO_CALL_DROP = 230;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_IMS_MT_CALL_DROP = 231;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_IMS_MT_CALL_MISSED = 232;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_IMS_REG_FAIL = 43;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_IMS_SRVCC_CALL_DROP = 263;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_IMS_SRVCC_CANCEL = 262;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_IMS_SRVCC_COMPLETED = 260;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_IMS_SRVCC_FAILED = 261;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_LTE_A2_RELEASE_CELL = 108;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_LTE_ABNORMAL_DETACH = 104;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_LTE_AS_FAILED = 60;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_LTE_AUTHENTICATION_REJECT = 68;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_LTE_BACKOFF_PLMN = 86;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_LTE_BAR_BAD_FAKE_CELL = 85;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_LTE_HANDOVER_FAILURE = 105;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_LTE_NARROW_BANDWIDTH_SCELL = 97;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_LTE_NARROW_BW_MONITORING = 129;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_LTE_REG_FAIL_5TIMES = 87;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_LTE_REG_REJECT = 61;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_LTE_REG_SUCCESS_AFTER_REJECT = 88;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_LTE_REG_WITHOUT_LTE = 62;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_LTE_RRC_ABNORMAL_BAR = 106;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_LTE_RRC_ABNORMAL_TIMEOUT = 107;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_LTE_RRC_CONN_HOLD = 118;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_LTE_SERVICE_REJECT = 102;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_LTE_TAU_REJECT = 101;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_MCFG_CONFIG_CHANGE = 76;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_MCFG_ICCID_FAILED = 67;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_MCFG_LOAD_ISSUE = 98;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_MODEM_CRASH_EX = 217;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_MO_CALL_DROP = 23;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_MO_DROP = 10;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_MO_MT_DROP_RATE = 25;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_MT_CALL_DROP = 24;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_MT_CSFB = 14;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_MT_DISC = 20;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_MT_PAGE_FAIL = 19;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_MT_PCH = 13;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_MT_RACH = 11;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_MT_REJECT = 15;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_MT_RLF = 12;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_MT_RRC = 16;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_NOT_APN_REASON_DATA_CALL_FAIL = 32;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_NO_RECEIVE_DATA_ERROR = 37;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_NO_RESPONSE_FOR_DATA_CALL = 35;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_NW_COMPONENT_RADAR = 181;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_PCI_MODE3_INTERFERENCE = 109;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_PDP_ACT_ERROR = 124;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_PDP_DEACT_ERROR = 125;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_REG_1X_MAP = 90;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_REG_BASE = 60;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_REG_CT_CARD_NO_4G = 130;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_REG_ELEVATOR_MODE_1X = 149;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_REG_ELEVATOR_MODE_23G = 148;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_REG_ELEVATOR_MODE_OOS = 147;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_REG_LTE_INT_FAIL = 81;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_REG_LTE_REDIREC_UNEXPECT = 82;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_REG_LTE_TYPE_UNEXPECT = 83;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_REG_MCC_CHANGE = 69;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_REG_REJECT = 63;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_REG_REJECT_OUT_OF_CREDIT = 89;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_REG_SRV_ON = 75;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_REG_SRV_OOS = 80;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_REG_SRV_REQ_MCC = 73;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_REG_SRV_REQ_MNC = 74;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_REG_SRV_REQ_RAT = 72;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_RFFE_MISSING_NONFATAL = 211;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_RF_4G_PA_DAMAGE = 216;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_RF_BASE = 210;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_RF_CABLE_STATUS = 218;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_RF_ENTER_SAR_FORCE_DOWN_ANT = 215;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_RF_FORCE_REFRESH_FRAME = 220;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_RF_MIPI_HW_FAILED = 210;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_RF_MODEM_CRASH = 213;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_RF_XO_FREQ_OFFSET = 214;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_SA_RACH_FAIL = 142;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_SA_RAGISTRATION_FAIL = 145;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_SA_RLF = 143;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_SA_RRC_REESTABLISTMENT = 144;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_SCREEN_ON_DURATION = 33;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_SIGNAL_STATISTIC = 212;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_THERMAL_KPIS = 134;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_UIM_BASE = 160;
  
  public static final int SYS_OEM_NW_DIAG_CAUSE_VOLTE_RLF = 44;
  
  public static final int SYS_OEM_NW_DIAG_RAT_CDMA = 3;
  
  public static final int SYS_OEM_NW_DIAG_RAT_GSM = 0;
  
  public static final int SYS_OEM_NW_DIAG_RAT_HDR = 4;
  
  public static final int SYS_OEM_NW_DIAG_RAT_LTE = 5;
  
  public static final int SYS_OEM_NW_DIAG_RAT_NONE = -1;
  
  public static final int SYS_OEM_NW_DIAG_RAT_TDS = 1;
  
  public static final int SYS_OEM_NW_DIAG_RAT_WCDMA = 2;
  
  public static Map<Integer, Integer> sRatTableMap;
  
  public static Map<Integer, String> sTableMap = new HashMap<>();
  
  static {
    sRatTableMap = new HashMap<>();
    sTableMap.put(Integer.valueOf(10), "mo_drop");
    Map<Integer, String> map2 = sTableMap;
    Integer integer2 = Integer.valueOf(11);
    map2.put(integer2, "mt_rach");
    Map<Integer, String> map3 = sTableMap;
    Integer integer1 = Integer.valueOf(12);
    map3.put(integer1, "mt_rlf");
    map3 = sTableMap;
    Integer integer4 = Integer.valueOf(13);
    map3.put(integer4, "mt_pch");
    map3 = sTableMap;
    Integer integer5 = Integer.valueOf(14);
    map3.put(integer5, "mt_csfb");
    map3 = sTableMap;
    Integer integer6 = Integer.valueOf(15);
    map3.put(integer6, "mt_reject");
    map3 = sTableMap;
    Integer integer7 = Integer.valueOf(16);
    map3.put(integer7, "mt_rrc");
    sTableMap.put(Integer.valueOf(19), "mt_page_fail");
    sTableMap.put(Integer.valueOf(20), "mt_disc");
    sTableMap.put(Integer.valueOf(22), "cm_serv_rej");
    sTableMap.put(Integer.valueOf(23), "mo_call_drop");
    sTableMap.put(Integer.valueOf(24), "mt_call_drop");
    sTableMap.put(Integer.valueOf(25), "mo_mt_drop_rate");
    sTableMap.put(Integer.valueOf(26), "data_airplane_num");
    sTableMap.put(Integer.valueOf(27), "data_user_data_enable_num");
    sTableMap.put(Integer.valueOf(28), "data_recover_succ_num");
    sTableMap.put(Integer.valueOf(29), "data_pdn_activation_duration");
    sTableMap.put(Integer.valueOf(30), "data_disconnect_call_error");
    sTableMap.put(Integer.valueOf(31), "apn_reason_data_call_fail");
    sTableMap.put(Integer.valueOf(32), "not_apn_reason_data_call_fail");
    sTableMap.put(Integer.valueOf(33), "screen_on_duration");
    sTableMap.put(Integer.valueOf(34), "data_call_error");
    sTableMap.put(Integer.valueOf(35), "no_response_for_data_call");
    sTableMap.put(Integer.valueOf(36), "congest_ratio");
    sTableMap.put(Integer.valueOf(37), "congest_ratio");
    sTableMap.put(Integer.valueOf(39), "speech_issue_gwl");
    sTableMap.put(Integer.valueOf(41), "speech_issue_c2k");
    sTableMap.put(Integer.valueOf(42), "ims_call_handover_fail");
    sTableMap.put(Integer.valueOf(43), "ims_registration_fail");
    sTableMap.put(Integer.valueOf(44), "volte_rlf");
    sTableMap.put(Integer.valueOf(45), "data_rlf");
    sTableMap.put(Integer.valueOf(46), "game_space_auto_reject");
    sTableMap.put(Integer.valueOf(180), "nw_ac_bar_ssac_bar");
    sTableMap.put(Integer.valueOf(60), "lte_as_failed");
    Map<Integer, String> map4 = sTableMap;
    Integer integer3 = Integer.valueOf(61);
    map4.put(integer3, "lte_reg_reject");
    sTableMap.put(Integer.valueOf(62), "lte_reg_without_lte");
    Map<Integer, String> map5 = sTableMap;
    Integer integer8 = Integer.valueOf(63);
    map5.put(integer8, "reg_reject");
    Map<Integer, String> map6 = sTableMap;
    Integer integer9 = Integer.valueOf(64);
    map6.put(integer9, "authentication_reject");
    sTableMap.put(Integer.valueOf(65), "as_failed");
    sTableMap.put(Integer.valueOf(66), "gsm_t3126_expired");
    sTableMap.put(Integer.valueOf(67), "mcfg_iccid_failed");
    Map<Integer, String> map7 = sTableMap;
    Integer integer10 = Integer.valueOf(68);
    map7.put(integer10, "lte_authentication_reject");
    sTableMap.put(Integer.valueOf(69), "reg_mcc_change");
    sTableMap.put(Integer.valueOf(70), "fake_bs");
    sTableMap.put(Integer.valueOf(71), "fake_bs_only");
    sTableMap.put(Integer.valueOf(72), "srv_req_rat");
    sTableMap.put(Integer.valueOf(73), "srv_req_mcc");
    sTableMap.put(Integer.valueOf(74), "srv_req_mnc");
    sTableMap.put(Integer.valueOf(75), "srv_on");
    sTableMap.put(Integer.valueOf(76), "mcfg_config_change");
    sTableMap.put(Integer.valueOf(80), "srv_oos");
    sTableMap.put(Integer.valueOf(81), "lte_int_fail");
    sTableMap.put(Integer.valueOf(82), "lte_redirec_unexpect");
    sTableMap.put(Integer.valueOf(83), "lte_type_unexpect");
    sTableMap.put(Integer.valueOf(84), "forbidden_tai_opt");
    Map<Integer, String> map8 = sTableMap;
    Integer integer11 = Integer.valueOf(85);
    map8.put(integer11, "lte_bar_bad_fake_cell");
    sTableMap.put(Integer.valueOf(86), "lte_backoff_plmn");
    sTableMap.put(Integer.valueOf(87), "lte_reg_fail_5times");
    sTableMap.put(Integer.valueOf(88), "lte_reg_success_after_reject");
    sTableMap.put(Integer.valueOf(89), "reg_reject_out_of_credit");
    sTableMap.put(Integer.valueOf(90), "1x_reg_map_fail");
    sTableMap.put(Integer.valueOf(91), "screen_on_nw_srch");
    sTableMap.put(Integer.valueOf(92), "screen_on_trigger_nw_srch");
    sTableMap.put(Integer.valueOf(93), "acq_cnt");
    sTableMap.put(Integer.valueOf(94), "screen_off_acq_cnt");
    sTableMap.put(Integer.valueOf(95), "skip_acq_cnt");
    sTableMap.put(Integer.valueOf(96), "inactive_fullband_cnt");
    Map<Integer, String> map10 = sTableMap;
    Integer integer12 = Integer.valueOf(97);
    map10.put(integer12, "lte_narrow_bandwidth_scell");
    sTableMap.put(Integer.valueOf(98), "mcfg_load_issue");
    sTableMap.put(Integer.valueOf(99), "scenes_info");
    sTableMap.put(Integer.valueOf(101), "lte_tau_reject");
    sTableMap.put(Integer.valueOf(102), "lte_service_reject");
    sTableMap.put(Integer.valueOf(103), "endc_pwr_opt");
    sTableMap.put(Integer.valueOf(104), "lte_abnormal_detach");
    sTableMap.put(Integer.valueOf(105), "lte_handover_failed");
    sTableMap.put(Integer.valueOf(106), "lte_rrc_abnormal_bar");
    sTableMap.put(Integer.valueOf(107), "lte_rrc_abnormal_timeout");
    sTableMap.put(Integer.valueOf(108), "a2_release_cell");
    sTableMap.put(Integer.valueOf(109), "pci_mode3_interference");
    sTableMap.put(Integer.valueOf(181), "network_component_radar");
    sTableMap.put(Integer.valueOf(110), "data_setup_data_error");
    sTableMap.put(Integer.valueOf(111), "data_no_available_apn");
    sTableMap.put(Integer.valueOf(112), "data_not_allowed");
    sTableMap.put(Integer.valueOf(113), "data_stall_error");
    sTableMap.put(Integer.valueOf(114), "data_game_latency");
    sTableMap.put(Integer.valueOf(115), "dns_fail");
    sTableMap.put(Integer.valueOf(116), "data_limit_on_lte");
    sTableMap.put(Integer.valueOf(117), "ps_state");
    sTableMap.put(Integer.valueOf(118), "lte_rrc_conn_hold");
    sTableMap.put(Integer.valueOf(120), "data_pdcp_ul_timeout");
    sTableMap.put(Integer.valueOf(121), "data_pdcp_ul_out_of_buff+er");
    sTableMap.put(Integer.valueOf(122), "data_pdcp_ul_one_way_pass");
    sTableMap.put(Integer.valueOf(123), "data_dl_high_bler");
    sTableMap.put(Integer.valueOf(124), "data_pdp_active_error");
    sTableMap.put(Integer.valueOf(125), "data_pdp_deactive_error");
    sTableMap.put(Integer.valueOf(126), "cell_ho_count");
    sTableMap.put(Integer.valueOf(127), "data_rlc_ul_rlf");
    sTableMap.put(Integer.valueOf(128), "data_nsa_lte_anchor");
    sTableMap.put(Integer.valueOf(129), "lte_narrow_bw_monitoring");
    sTableMap.put(Integer.valueOf(130), "corrupted_ct_card_no_4g");
    sTableMap.put(Integer.valueOf(131), "data_dorecovery_result");
    sTableMap.put(Integer.valueOf(132), "data_dorecovery_kpi");
    sTableMap.put(Integer.valueOf(133), "data_ia_apn_error");
    sTableMap.put(Integer.valueOf(134), "thermal_kpis");
    sTableMap.put(Integer.valueOf(135), "data_net_slow_result");
    sTableMap.put(Integer.valueOf(137), "data_game_err");
    sTableMap.put(Integer.valueOf(138), "sa_mode_change");
    sTableMap.put(Integer.valueOf(141), "nodds_lte_frequent_paging");
    sTableMap.put(Integer.valueOf(150), "ddsswitch_trigger");
    sTableMap.put(Integer.valueOf(151), "ddsswitch_res");
    sTableMap.put(Integer.valueOf(152), "ddsswitch_err");
    sTableMap.put(Integer.valueOf(153), "ds_ck_kpi");
    sTableMap.put(Integer.valueOf(142), "sa_rach_fail");
    sTableMap.put(Integer.valueOf(143), "sa_rlf");
    sTableMap.put(Integer.valueOf(144), "sa_rrc_reestablishment");
    sTableMap.put(Integer.valueOf(145), "sa_registration_fail");
    sTableMap.put(Integer.valueOf(154), "dump_disable_5g");
    sTableMap.put(Integer.valueOf(139), "nondds_sub_cs_invalid_cnt");
    sTableMap.put(Integer.valueOf(147), "elevator_mode_oos");
    sTableMap.put(Integer.valueOf(148), "elevator_mode_23G");
    sTableMap.put(Integer.valueOf(149), "elevator_mode_1X");
    sTableMap.put(Integer.valueOf(160), "card_drop_rx_break");
    sTableMap.put(Integer.valueOf(161), "card_drop_time_out");
    sTableMap.put(Integer.valueOf(163), "only_support_voltage_class_b");
    sTableMap.put(Integer.valueOf(164), "only_support_voltage_class_c");
    sTableMap.put(Integer.valueOf(165), "support_voltage_class_b_and_c");
    sTableMap.put(Integer.valueOf(210), "rf_mipi_hw_failed");
    sTableMap.put(Integer.valueOf(211), "rffe_missing_nonfatal");
    sTableMap.put(Integer.valueOf(212), "rf_signal_statis");
    sTableMap.put(Integer.valueOf(213), "rf_modem_crash");
    sTableMap.put(Integer.valueOf(214), "rf_xo_freq_offset");
    sTableMap.put(Integer.valueOf(215), "enter_sar_force_down_ant");
    sTableMap.put(Integer.valueOf(216), "4R_PA_DAMAGE");
    sTableMap.put(Integer.valueOf(217), "modem_crash_ex");
    sTableMap.put(Integer.valueOf(218), "RF_CABLE_STATUS");
    sTableMap.put(Integer.valueOf(220), "osc_refresh_frame");
    sTableMap.put(Integer.valueOf(260), "srvcc_completed");
    sTableMap.put(Integer.valueOf(261), "srvcc_failed");
    sTableMap.put(Integer.valueOf(262), "srvcc_cancel");
    sTableMap.put(Integer.valueOf(263), "srvcc_call_drop");
    sTableMap.put(Integer.valueOf(264), "ims_call_disc_abnormal");
    sTableMap.put(Integer.valueOf(265), "ims_call_normal");
    sTableMap.put(Integer.valueOf(266), "ims_call_drop_q850");
    sTableMap.put(Integer.valueOf(230), "ims_mo_call_drop");
    sTableMap.put(Integer.valueOf(231), "ims_mt_call_drop");
    Map<Integer, Integer> map9 = sRatTableMap;
    Integer integer13 = Integer.valueOf(0);
    map9.put(integer2, integer13);
    sRatTableMap.put(integer4, integer13);
    sRatTableMap.put(integer1, integer13);
    sRatTableMap.put(integer5, integer13);
    Map<Integer, Integer> map1 = sRatTableMap;
    integer2 = Integer.valueOf(5);
    map1.put(integer6, integer2);
    sRatTableMap.put(integer7, Integer.valueOf(1));
    map1 = sRatTableMap;
    integer4 = Integer.valueOf(-1);
    map1.put(Integer.valueOf(160), integer4);
    sRatTableMap.put(integer3, integer2);
    sRatTableMap.put(integer10, integer2);
    sRatTableMap.put(integer8, integer4);
    sRatTableMap.put(integer9, integer4);
    sRatTableMap.put(Integer.valueOf(101), integer2);
    sRatTableMap.put(Integer.valueOf(104), integer2);
    sRatTableMap.put(Integer.valueOf(133), integer2);
    sRatTableMap.put(Integer.valueOf(125), integer2);
    sRatTableMap.put(Integer.valueOf(124), integer2);
    sRatTableMap.put(Integer.valueOf(127), integer2);
    sRatTableMap.put(Integer.valueOf(120), integer2);
    sRatTableMap.put(Integer.valueOf(121), integer2);
    sRatTableMap.put(Integer.valueOf(122), integer2);
    sRatTableMap.put(Integer.valueOf(123), integer2);
    sRatTableMap.put(Integer.valueOf(105), integer2);
    sRatTableMap.put(Integer.valueOf(106), integer2);
    sRatTableMap.put(Integer.valueOf(107), integer2);
    sRatTableMap.put(Integer.valueOf(109), integer2);
    sRatTableMap.put(integer12, integer2);
    sRatTableMap.put(Integer.valueOf(129), integer2);
    sRatTableMap.put(integer11, integer2);
    sRatTableMap.put(Integer.valueOf(108), integer2);
    sRatTableMap.put(Integer.valueOf(141), integer2);
    sRatTableMap.put(Integer.valueOf(147), integer4);
    sRatTableMap.put(Integer.valueOf(148), Integer.valueOf(2));
    sRatTableMap.put(Integer.valueOf(149), Integer.valueOf(3));
  }
  
  public static String getStringFromType(int paramInt) {
    if (sTableMap.containsKey(Integer.valueOf(paramInt)))
      return sTableMap.get(Integer.valueOf(paramInt)); 
    return "";
  }
  
  public static int getRatFromType(int paramInt) {
    if (sRatTableMap.containsKey(Integer.valueOf(paramInt)))
      return ((Integer)sRatTableMap.get(Integer.valueOf(paramInt))).intValue(); 
    return -1;
  }
  
  public class CriticalLogInfo {
    public long errcode;
    
    public String extra;
    
    public String issue;
    
    public long rat;
    
    final OplusKeyLogBase this$0;
    
    public long type;
    
    public CriticalLogInfo(int param1Int1, int param1Int2, int param1Int3, String param1String1, String param1String2) {
      this.type = param1Int1;
      this.errcode = param1Int2;
      this.rat = param1Int3;
      this.extra = param1String1;
      this.issue = param1String2;
    }
  }
}
