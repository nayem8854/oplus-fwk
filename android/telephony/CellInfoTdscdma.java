package android.telephony;

import android.hardware.radio.V1_0.CellInfo;
import android.hardware.radio.V1_2.CellInfo;
import android.hardware.radio.V1_4.CellInfo;
import android.hardware.radio.V1_5.CellInfo;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.telephony.Rlog;
import java.util.Objects;

public final class CellInfoTdscdma extends CellInfo implements Parcelable {
  public CellInfoTdscdma() {
    this.mCellIdentityTdscdma = new CellIdentityTdscdma();
    this.mCellSignalStrengthTdscdma = new CellSignalStrengthTdscdma();
  }
  
  public CellInfoTdscdma(CellInfoTdscdma paramCellInfoTdscdma) {
    super(paramCellInfoTdscdma);
    this.mCellIdentityTdscdma = paramCellInfoTdscdma.mCellIdentityTdscdma.copy();
    this.mCellSignalStrengthTdscdma = paramCellInfoTdscdma.mCellSignalStrengthTdscdma.copy();
  }
  
  public CellInfoTdscdma(CellInfo paramCellInfo) {
    super(paramCellInfo);
    android.hardware.radio.V1_0.CellInfoTdscdma cellInfoTdscdma = paramCellInfo.tdscdma.get(0);
    this.mCellIdentityTdscdma = new CellIdentityTdscdma(cellInfoTdscdma.cellIdentityTdscdma);
    this.mCellSignalStrengthTdscdma = new CellSignalStrengthTdscdma(cellInfoTdscdma.signalStrengthTdscdma);
  }
  
  public CellInfoTdscdma(CellInfo paramCellInfo) {
    super(paramCellInfo);
    android.hardware.radio.V1_2.CellInfoTdscdma cellInfoTdscdma = paramCellInfo.tdscdma.get(0);
    this.mCellIdentityTdscdma = new CellIdentityTdscdma(cellInfoTdscdma.cellIdentityTdscdma);
    this.mCellSignalStrengthTdscdma = new CellSignalStrengthTdscdma(cellInfoTdscdma.signalStrengthTdscdma);
  }
  
  public CellInfoTdscdma(CellInfo paramCellInfo, long paramLong) {
    super(paramCellInfo, paramLong);
    android.hardware.radio.V1_2.CellInfoTdscdma cellInfoTdscdma = paramCellInfo.info.tdscdma();
    this.mCellIdentityTdscdma = new CellIdentityTdscdma(cellInfoTdscdma.cellIdentityTdscdma);
    this.mCellSignalStrengthTdscdma = new CellSignalStrengthTdscdma(cellInfoTdscdma.signalStrengthTdscdma);
  }
  
  public CellInfoTdscdma(CellInfo paramCellInfo, long paramLong) {
    super(paramCellInfo, paramLong);
    android.hardware.radio.V1_5.CellInfoTdscdma cellInfoTdscdma = paramCellInfo.ratSpecificInfo.tdscdma();
    this.mCellIdentityTdscdma = new CellIdentityTdscdma(cellInfoTdscdma.cellIdentityTdscdma);
    this.mCellSignalStrengthTdscdma = new CellSignalStrengthTdscdma(cellInfoTdscdma.signalStrengthTdscdma);
  }
  
  public CellIdentityTdscdma getCellIdentity() {
    return this.mCellIdentityTdscdma;
  }
  
  public void setCellIdentity(CellIdentityTdscdma paramCellIdentityTdscdma) {
    this.mCellIdentityTdscdma = paramCellIdentityTdscdma;
  }
  
  public CellSignalStrengthTdscdma getCellSignalStrength() {
    return this.mCellSignalStrengthTdscdma;
  }
  
  public CellInfo sanitizeLocationInfo() {
    CellInfoTdscdma cellInfoTdscdma = new CellInfoTdscdma(this);
    cellInfoTdscdma.mCellIdentityTdscdma = this.mCellIdentityTdscdma.sanitizeLocationInfo();
    return cellInfoTdscdma;
  }
  
  public void setCellSignalStrength(CellSignalStrengthTdscdma paramCellSignalStrengthTdscdma) {
    this.mCellSignalStrengthTdscdma = paramCellSignalStrengthTdscdma;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(super.hashCode()), this.mCellIdentityTdscdma, this.mCellSignalStrengthTdscdma });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = super.equals(paramObject);
    boolean bool1 = false;
    if (!bool)
      return false; 
    try {
      CellInfoTdscdma cellInfoTdscdma = (CellInfoTdscdma)paramObject;
      if (this.mCellIdentityTdscdma.equals(cellInfoTdscdma.mCellIdentityTdscdma)) {
        paramObject = this.mCellSignalStrengthTdscdma;
        CellSignalStrengthTdscdma cellSignalStrengthTdscdma = cellInfoTdscdma.mCellSignalStrengthTdscdma;
        bool = paramObject.equals(cellSignalStrengthTdscdma);
        if (bool)
          bool1 = true; 
      } 
      return bool1;
    } catch (ClassCastException classCastException) {
      return false;
    } 
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append("CellInfoTdscdma:{");
    stringBuffer.append(super.toString());
    stringBuffer.append(" ");
    stringBuffer.append(this.mCellIdentityTdscdma);
    stringBuffer.append(" ");
    stringBuffer.append(this.mCellSignalStrengthTdscdma);
    stringBuffer.append("}");
    return stringBuffer.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcel(paramParcel, paramInt, 5);
    this.mCellIdentityTdscdma.writeToParcel(paramParcel, paramInt);
    this.mCellSignalStrengthTdscdma.writeToParcel(paramParcel, paramInt);
  }
  
  private CellInfoTdscdma(Parcel paramParcel) {
    super(paramParcel);
    this.mCellIdentityTdscdma = (CellIdentityTdscdma)CellIdentityTdscdma.CREATOR.createFromParcel(paramParcel);
    this.mCellSignalStrengthTdscdma = (CellSignalStrengthTdscdma)CellSignalStrengthTdscdma.CREATOR.createFromParcel(paramParcel);
  }
  
  public static final Parcelable.Creator<CellInfoTdscdma> CREATOR = (Parcelable.Creator<CellInfoTdscdma>)new Object();
  
  private static final boolean DBG = false;
  
  private static final String LOG_TAG = "CellInfoTdscdma";
  
  private CellIdentityTdscdma mCellIdentityTdscdma;
  
  private CellSignalStrengthTdscdma mCellSignalStrengthTdscdma;
  
  protected static CellInfoTdscdma createFromParcelBody(Parcel paramParcel) {
    return new CellInfoTdscdma(paramParcel);
  }
  
  private static void log(String paramString) {
    Rlog.w("CellInfoTdscdma", paramString);
  }
}
