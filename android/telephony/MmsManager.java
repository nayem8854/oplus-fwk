package android.telephony;

import android.app.ActivityThread;
import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.ServiceManager;
import com.android.internal.telephony.IMms;

public class MmsManager {
  private static final String TAG = "MmsManager";
  
  private final Context mContext;
  
  public MmsManager(Context paramContext) {
    this.mContext = paramContext;
  }
  
  public void sendMultimediaMessage(int paramInt, Uri paramUri, String paramString, Bundle paramBundle, PendingIntent paramPendingIntent, long paramLong) {
    try {
      IMms iMms = IMms.Stub.asInterface(ServiceManager.getService("imms"));
      if (iMms == null)
        return; 
      iMms.sendMessage(paramInt, ActivityThread.currentPackageName(), paramUri, paramString, paramBundle, paramPendingIntent, paramLong);
    } catch (RemoteException remoteException) {}
  }
  
  public void downloadMultimediaMessage(int paramInt, String paramString, Uri paramUri, Bundle paramBundle, PendingIntent paramPendingIntent, long paramLong) {
    try {
      IMms iMms = IMms.Stub.asInterface(ServiceManager.getService("imms"));
      if (iMms == null)
        return; 
      iMms.downloadMessage(paramInt, ActivityThread.currentPackageName(), paramString, paramUri, paramBundle, paramPendingIntent, paramLong);
    } catch (RemoteException remoteException) {}
  }
}
