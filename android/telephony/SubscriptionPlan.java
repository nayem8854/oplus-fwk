package android.telephony;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Range;
import android.util.RecurrenceRule;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.time.Period;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;

public final class SubscriptionPlan implements Parcelable {
  private long dataLimitBytes = -1L;
  
  private int dataLimitBehavior = -1;
  
  private long dataUsageBytes = -1L;
  
  private long dataUsageTime = -1L;
  
  private SubscriptionPlan(RecurrenceRule paramRecurrenceRule) {
    this.cycleRule = (RecurrenceRule)Preconditions.checkNotNull(paramRecurrenceRule);
    int[] arrayOfInt = TelephonyManager.getAllNetworkTypes();
    int i = (TelephonyManager.getAllNetworkTypes()).length;
    this.networkTypes = Arrays.copyOf(arrayOfInt, i);
  }
  
  private SubscriptionPlan(Parcel paramParcel) {
    this.cycleRule = (RecurrenceRule)paramParcel.readParcelable(null);
    this.title = paramParcel.readCharSequence();
    this.summary = paramParcel.readCharSequence();
    this.dataLimitBytes = paramParcel.readLong();
    this.dataLimitBehavior = paramParcel.readInt();
    this.dataUsageBytes = paramParcel.readLong();
    this.dataUsageTime = paramParcel.readLong();
    this.networkTypes = paramParcel.createIntArray();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.cycleRule, paramInt);
    paramParcel.writeCharSequence(this.title);
    paramParcel.writeCharSequence(this.summary);
    paramParcel.writeLong(this.dataLimitBytes);
    paramParcel.writeInt(this.dataLimitBehavior);
    paramParcel.writeLong(this.dataUsageBytes);
    paramParcel.writeLong(this.dataUsageTime);
    paramParcel.writeIntArray(this.networkTypes);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("SubscriptionPlan{");
    stringBuilder.append("cycleRule=");
    stringBuilder.append(this.cycleRule);
    stringBuilder.append(" title=");
    stringBuilder.append(this.title);
    stringBuilder.append(" summary=");
    stringBuilder.append(this.summary);
    stringBuilder.append(" dataLimitBytes=");
    stringBuilder.append(this.dataLimitBytes);
    stringBuilder.append(" dataLimitBehavior=");
    stringBuilder.append(this.dataLimitBehavior);
    stringBuilder.append(" dataUsageBytes=");
    stringBuilder.append(this.dataUsageBytes);
    stringBuilder.append(" dataUsageTime=");
    stringBuilder.append(this.dataUsageTime);
    stringBuilder.append(" networkTypes=");
    stringBuilder.append(Arrays.toString(this.networkTypes));
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int hashCode() {
    RecurrenceRule recurrenceRule = this.cycleRule;
    CharSequence charSequence1 = this.title, charSequence2 = this.summary;
    long l1 = this.dataLimitBytes;
    int i = this.dataLimitBehavior;
    long l2 = this.dataUsageBytes;
    long l3 = this.dataUsageTime;
    int j = Arrays.hashCode(this.networkTypes);
    return Objects.hash(new Object[] { recurrenceRule, charSequence1, charSequence2, Long.valueOf(l1), Integer.valueOf(i), Long.valueOf(l2), Long.valueOf(l3), Integer.valueOf(j) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof SubscriptionPlan;
    boolean bool1 = false;
    if (bool) {
      paramObject = paramObject;
      if (Objects.equals(this.cycleRule, ((SubscriptionPlan)paramObject).cycleRule)) {
        CharSequence charSequence1 = this.title, charSequence2 = ((SubscriptionPlan)paramObject).title;
        if (Objects.equals(charSequence1, charSequence2)) {
          charSequence2 = this.summary;
          charSequence1 = ((SubscriptionPlan)paramObject).summary;
          if (Objects.equals(charSequence2, charSequence1) && this.dataLimitBytes == ((SubscriptionPlan)paramObject).dataLimitBytes && this.dataLimitBehavior == ((SubscriptionPlan)paramObject).dataLimitBehavior && this.dataUsageBytes == ((SubscriptionPlan)paramObject).dataUsageBytes && this.dataUsageTime == ((SubscriptionPlan)paramObject).dataUsageTime) {
            int[] arrayOfInt = this.networkTypes;
            paramObject = ((SubscriptionPlan)paramObject).networkTypes;
            if (Arrays.equals(arrayOfInt, (int[])paramObject))
              bool1 = true; 
          } 
        } 
      } 
      return bool1;
    } 
    return false;
  }
  
  public static final Parcelable.Creator<SubscriptionPlan> CREATOR = new Parcelable.Creator<SubscriptionPlan>() {
      public SubscriptionPlan createFromParcel(Parcel param1Parcel) {
        return new SubscriptionPlan(param1Parcel);
      }
      
      public SubscriptionPlan[] newArray(int param1Int) {
        return new SubscriptionPlan[param1Int];
      }
    };
  
  public static final long BYTES_UNKNOWN = -1L;
  
  public static final long BYTES_UNLIMITED = 9223372036854775807L;
  
  public static final int LIMIT_BEHAVIOR_BILLED = 1;
  
  public static final int LIMIT_BEHAVIOR_DISABLED = 0;
  
  public static final int LIMIT_BEHAVIOR_THROTTLED = 2;
  
  public static final int LIMIT_BEHAVIOR_UNKNOWN = -1;
  
  public static final long TIME_UNKNOWN = -1L;
  
  private final RecurrenceRule cycleRule;
  
  private int[] networkTypes;
  
  private CharSequence summary;
  
  private CharSequence title;
  
  public RecurrenceRule getCycleRule() {
    return this.cycleRule;
  }
  
  public CharSequence getTitle() {
    return this.title;
  }
  
  public CharSequence getSummary() {
    return this.summary;
  }
  
  public long getDataLimitBytes() {
    return this.dataLimitBytes;
  }
  
  public int getDataLimitBehavior() {
    return this.dataLimitBehavior;
  }
  
  public long getDataUsageBytes() {
    return this.dataUsageBytes;
  }
  
  public long getDataUsageTime() {
    return this.dataUsageTime;
  }
  
  public int[] getNetworkTypes() {
    int[] arrayOfInt = this.networkTypes;
    return Arrays.copyOf(arrayOfInt, arrayOfInt.length);
  }
  
  public Iterator<Range<ZonedDateTime>> cycleIterator() {
    return this.cycleRule.cycleIterator();
  }
  
  class Builder {
    private final SubscriptionPlan plan;
    
    public Builder(SubscriptionPlan this$0, ZonedDateTime param1ZonedDateTime1, Period param1Period) {
      this.plan = new SubscriptionPlan(new RecurrenceRule((ZonedDateTime)this$0, param1ZonedDateTime1, param1Period));
    }
    
    public static Builder createNonrecurring(ZonedDateTime param1ZonedDateTime1, ZonedDateTime param1ZonedDateTime2) {
      if (param1ZonedDateTime2.isAfter(param1ZonedDateTime1))
        return new Builder(param1ZonedDateTime1, param1ZonedDateTime2, null); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("End ");
      stringBuilder.append(param1ZonedDateTime2);
      stringBuilder.append(" isn't after start ");
      stringBuilder.append(param1ZonedDateTime1);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public static Builder createRecurring(ZonedDateTime param1ZonedDateTime, Period param1Period) {
      if (!param1Period.isZero() && !param1Period.isNegative())
        return new Builder(param1ZonedDateTime, null, param1Period); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Period ");
      stringBuilder.append(param1Period);
      stringBuilder.append(" must be positive");
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    @SystemApi
    @Deprecated
    public static Builder createRecurringMonthly(ZonedDateTime param1ZonedDateTime) {
      return new Builder(param1ZonedDateTime, null, Period.ofMonths(1));
    }
    
    @SystemApi
    @Deprecated
    public static Builder createRecurringWeekly(ZonedDateTime param1ZonedDateTime) {
      return new Builder(param1ZonedDateTime, null, Period.ofDays(7));
    }
    
    @SystemApi
    @Deprecated
    public static Builder createRecurringDaily(ZonedDateTime param1ZonedDateTime) {
      return new Builder(param1ZonedDateTime, null, Period.ofDays(1));
    }
    
    public SubscriptionPlan build() {
      return this.plan;
    }
    
    public Builder setTitle(CharSequence param1CharSequence) {
      SubscriptionPlan.access$202(this.plan, param1CharSequence);
      return this;
    }
    
    public Builder setSummary(CharSequence param1CharSequence) {
      SubscriptionPlan.access$302(this.plan, param1CharSequence);
      return this;
    }
    
    public Builder setDataLimit(long param1Long, int param1Int) {
      if (param1Long >= 0L) {
        if (param1Int >= 0) {
          SubscriptionPlan.access$402(this.plan, param1Long);
          SubscriptionPlan.access$502(this.plan, param1Int);
          return this;
        } 
        throw new IllegalArgumentException("Limit behavior must be defined");
      } 
      throw new IllegalArgumentException("Limit bytes must be positive");
    }
    
    public Builder setDataUsage(long param1Long1, long param1Long2) {
      if (param1Long1 >= 0L) {
        if (param1Long2 >= 0L) {
          SubscriptionPlan.access$602(this.plan, param1Long1);
          SubscriptionPlan.access$702(this.plan, param1Long2);
          return this;
        } 
        throw new IllegalArgumentException("Usage time must be positive");
      } 
      throw new IllegalArgumentException("Usage bytes must be positive");
    }
    
    public Builder setNetworkTypes(int[] param1ArrayOfint) {
      SubscriptionPlan.access$802(this.plan, Arrays.copyOf(param1ArrayOfint, param1ArrayOfint.length));
      return this;
    }
    
    public Builder resetNetworkTypes() {
      SubscriptionPlan subscriptionPlan = this.plan;
      int[] arrayOfInt = TelephonyManager.getAllNetworkTypes();
      int i = (TelephonyManager.getAllNetworkTypes()).length;
      SubscriptionPlan.access$802(subscriptionPlan, Arrays.copyOf(arrayOfInt, i));
      return this;
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class LimitBehavior implements Annotation {}
}
