package android.telephony;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

@SystemApi
public final class DataSpecificRegistrationInfo implements Parcelable {
  DataSpecificRegistrationInfo(int paramInt, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, LteVopsSupportInfo paramLteVopsSupportInfo, boolean paramBoolean4) {
    this.maxDataCalls = paramInt;
    this.isDcNrRestricted = paramBoolean1;
    this.isNrAvailable = paramBoolean2;
    this.isEnDcAvailable = paramBoolean3;
    this.mLteVopsSupportInfo = paramLteVopsSupportInfo;
    this.mIsUsingCarrierAggregation = paramBoolean4;
  }
  
  DataSpecificRegistrationInfo(DataSpecificRegistrationInfo paramDataSpecificRegistrationInfo) {
    this.maxDataCalls = paramDataSpecificRegistrationInfo.maxDataCalls;
    this.isDcNrRestricted = paramDataSpecificRegistrationInfo.isDcNrRestricted;
    this.isNrAvailable = paramDataSpecificRegistrationInfo.isNrAvailable;
    this.isEnDcAvailable = paramDataSpecificRegistrationInfo.isEnDcAvailable;
    this.mLteVopsSupportInfo = paramDataSpecificRegistrationInfo.mLteVopsSupportInfo;
    this.mIsUsingCarrierAggregation = paramDataSpecificRegistrationInfo.mIsUsingCarrierAggregation;
  }
  
  private DataSpecificRegistrationInfo(Parcel paramParcel) {
    this.maxDataCalls = paramParcel.readInt();
    this.isDcNrRestricted = paramParcel.readBoolean();
    this.isNrAvailable = paramParcel.readBoolean();
    this.isEnDcAvailable = paramParcel.readBoolean();
    this.mLteVopsSupportInfo = (LteVopsSupportInfo)LteVopsSupportInfo.CREATOR.createFromParcel(paramParcel);
    this.mIsUsingCarrierAggregation = paramParcel.readBoolean();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.maxDataCalls);
    paramParcel.writeBoolean(this.isDcNrRestricted);
    paramParcel.writeBoolean(this.isNrAvailable);
    paramParcel.writeBoolean(this.isEnDcAvailable);
    this.mLteVopsSupportInfo.writeToParcel(paramParcel, paramInt);
    paramParcel.writeBoolean(this.mIsUsingCarrierAggregation);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(getClass().getName());
    stringBuilder1.append(" :{");
    StringBuilder stringBuilder6 = new StringBuilder();
    stringBuilder6.append(" maxDataCalls = ");
    stringBuilder6.append(this.maxDataCalls);
    String str5 = stringBuilder6.toString();
    stringBuilder1.append(str5);
    StringBuilder stringBuilder5 = new StringBuilder();
    stringBuilder5.append(" isDcNrRestricted = ");
    stringBuilder5.append(this.isDcNrRestricted);
    String str4 = stringBuilder5.toString();
    stringBuilder1.append(str4);
    StringBuilder stringBuilder4 = new StringBuilder();
    stringBuilder4.append(" isNrAvailable = ");
    stringBuilder4.append(this.isNrAvailable);
    String str3 = stringBuilder4.toString();
    stringBuilder1.append(str3);
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append(" isEnDcAvailable = ");
    stringBuilder3.append(this.isEnDcAvailable);
    String str2 = stringBuilder3.toString();
    stringBuilder1.append(str2);
    StringBuilder stringBuilder7 = new StringBuilder();
    stringBuilder7.append(" ");
    LteVopsSupportInfo lteVopsSupportInfo = this.mLteVopsSupportInfo;
    stringBuilder7.append(lteVopsSupportInfo.toString());
    stringBuilder1.append(stringBuilder7.toString());
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(" mIsUsingCarrierAggregation = ");
    stringBuilder2.append(this.mIsUsingCarrierAggregation);
    String str1 = stringBuilder2.toString();
    stringBuilder1.append(str1);
    stringBuilder1.append(" }");
    return stringBuilder1.toString();
  }
  
  public int hashCode() {
    int i = this.maxDataCalls;
    boolean bool1 = this.isDcNrRestricted, bool2 = this.isNrAvailable, bool3 = this.isEnDcAvailable;
    LteVopsSupportInfo lteVopsSupportInfo = this.mLteVopsSupportInfo;
    boolean bool4 = this.mIsUsingCarrierAggregation;
    return Objects.hash(new Object[] { Integer.valueOf(i), Boolean.valueOf(bool1), Boolean.valueOf(bool2), Boolean.valueOf(bool3), lteVopsSupportInfo, Boolean.valueOf(bool4) });
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof DataSpecificRegistrationInfo))
      return false; 
    paramObject = paramObject;
    if (this.maxDataCalls == ((DataSpecificRegistrationInfo)paramObject).maxDataCalls && this.isDcNrRestricted == ((DataSpecificRegistrationInfo)paramObject).isDcNrRestricted && this.isNrAvailable == ((DataSpecificRegistrationInfo)paramObject).isNrAvailable && this.isEnDcAvailable == ((DataSpecificRegistrationInfo)paramObject).isEnDcAvailable) {
      LteVopsSupportInfo lteVopsSupportInfo1 = this.mLteVopsSupportInfo, lteVopsSupportInfo2 = ((DataSpecificRegistrationInfo)paramObject).mLteVopsSupportInfo;
      if (lteVopsSupportInfo1.equals(lteVopsSupportInfo2) && this.mIsUsingCarrierAggregation == ((DataSpecificRegistrationInfo)paramObject).mIsUsingCarrierAggregation)
        return null; 
    } 
    return false;
  }
  
  public static final Parcelable.Creator<DataSpecificRegistrationInfo> CREATOR = new Parcelable.Creator<DataSpecificRegistrationInfo>() {
      public DataSpecificRegistrationInfo createFromParcel(Parcel param1Parcel) {
        return new DataSpecificRegistrationInfo(param1Parcel);
      }
      
      public DataSpecificRegistrationInfo[] newArray(int param1Int) {
        return new DataSpecificRegistrationInfo[param1Int];
      }
    };
  
  public final boolean isDcNrRestricted;
  
  public final boolean isEnDcAvailable;
  
  public final boolean isNrAvailable;
  
  public boolean mIsUsingCarrierAggregation;
  
  private final LteVopsSupportInfo mLteVopsSupportInfo;
  
  public final int maxDataCalls;
  
  public LteVopsSupportInfo getLteVopsSupportInfo() {
    return this.mLteVopsSupportInfo;
  }
  
  public void setIsUsingCarrierAggregation(boolean paramBoolean) {
    this.mIsUsingCarrierAggregation = paramBoolean;
  }
  
  public boolean isUsingCarrierAggregation() {
    return this.mIsUsingCarrierAggregation;
  }
}
