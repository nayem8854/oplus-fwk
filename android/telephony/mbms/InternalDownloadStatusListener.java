package android.telephony.mbms;

import android.os.Binder;
import android.os.RemoteException;
import java.util.concurrent.Executor;

public class InternalDownloadStatusListener extends IDownloadStatusListener.Stub {
  private final DownloadStatusListener mAppListener;
  
  private final Executor mExecutor;
  
  private volatile boolean mIsStopped = false;
  
  public InternalDownloadStatusListener(DownloadStatusListener paramDownloadStatusListener, Executor paramExecutor) {
    this.mAppListener = paramDownloadStatusListener;
    this.mExecutor = paramExecutor;
  }
  
  public void onStatusUpdated(DownloadRequest paramDownloadRequest, FileInfo paramFileInfo, int paramInt) throws RemoteException {
    if (this.mIsStopped)
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mExecutor;
      Object object = new Object();
      super(this, paramDownloadRequest, paramFileInfo, paramInt);
      executor.execute((Runnable)object);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void stop() {
    this.mIsStopped = true;
  }
}
