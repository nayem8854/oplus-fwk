package android.telephony.mbms;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IGroupCallCallback extends IInterface {
  void onBroadcastSignalStrengthUpdated(int paramInt) throws RemoteException;
  
  void onError(int paramInt, String paramString) throws RemoteException;
  
  void onGroupCallStateChanged(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements IGroupCallCallback {
    public void onError(int param1Int, String param1String) throws RemoteException {}
    
    public void onGroupCallStateChanged(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onBroadcastSignalStrengthUpdated(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IGroupCallCallback {
    private static final String DESCRIPTOR = "android.telephony.mbms.IGroupCallCallback";
    
    static final int TRANSACTION_onBroadcastSignalStrengthUpdated = 3;
    
    static final int TRANSACTION_onError = 1;
    
    static final int TRANSACTION_onGroupCallStateChanged = 2;
    
    public Stub() {
      attachInterface(this, "android.telephony.mbms.IGroupCallCallback");
    }
    
    public static IGroupCallCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.mbms.IGroupCallCallback");
      if (iInterface != null && iInterface instanceof IGroupCallCallback)
        return (IGroupCallCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onBroadcastSignalStrengthUpdated";
        } 
        return "onGroupCallStateChanged";
      } 
      return "onError";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.telephony.mbms.IGroupCallCallback");
            return true;
          } 
          param1Parcel1.enforceInterface("android.telephony.mbms.IGroupCallCallback");
          param1Int1 = param1Parcel1.readInt();
          onBroadcastSignalStrengthUpdated(param1Int1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.telephony.mbms.IGroupCallCallback");
        param1Int2 = param1Parcel1.readInt();
        param1Int1 = param1Parcel1.readInt();
        onGroupCallStateChanged(param1Int2, param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.telephony.mbms.IGroupCallCallback");
      param1Int1 = param1Parcel1.readInt();
      String str = param1Parcel1.readString();
      onError(param1Int1, str);
      return true;
    }
    
    private static class Proxy implements IGroupCallCallback {
      public static IGroupCallCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.mbms.IGroupCallCallback";
      }
      
      public void onError(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.mbms.IGroupCallCallback");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IGroupCallCallback.Stub.getDefaultImpl() != null) {
            IGroupCallCallback.Stub.getDefaultImpl().onError(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onGroupCallStateChanged(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.mbms.IGroupCallCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IGroupCallCallback.Stub.getDefaultImpl() != null) {
            IGroupCallCallback.Stub.getDefaultImpl().onGroupCallStateChanged(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onBroadcastSignalStrengthUpdated(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.mbms.IGroupCallCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IGroupCallCallback.Stub.getDefaultImpl() != null) {
            IGroupCallCallback.Stub.getDefaultImpl().onBroadcastSignalStrengthUpdated(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IGroupCallCallback param1IGroupCallCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IGroupCallCallback != null) {
          Proxy.sDefaultImpl = param1IGroupCallCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IGroupCallCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
