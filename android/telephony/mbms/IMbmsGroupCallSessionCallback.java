package android.telephony.mbms;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IMbmsGroupCallSessionCallback extends IInterface {
  void onAvailableSaisUpdated(List paramList1, List paramList2) throws RemoteException;
  
  void onError(int paramInt, String paramString) throws RemoteException;
  
  void onMiddlewareReady() throws RemoteException;
  
  void onServiceInterfaceAvailable(String paramString, int paramInt) throws RemoteException;
  
  class Default implements IMbmsGroupCallSessionCallback {
    public void onError(int param1Int, String param1String) throws RemoteException {}
    
    public void onAvailableSaisUpdated(List param1List1, List param1List2) throws RemoteException {}
    
    public void onServiceInterfaceAvailable(String param1String, int param1Int) throws RemoteException {}
    
    public void onMiddlewareReady() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMbmsGroupCallSessionCallback {
    private static final String DESCRIPTOR = "android.telephony.mbms.IMbmsGroupCallSessionCallback";
    
    static final int TRANSACTION_onAvailableSaisUpdated = 2;
    
    static final int TRANSACTION_onError = 1;
    
    static final int TRANSACTION_onMiddlewareReady = 4;
    
    static final int TRANSACTION_onServiceInterfaceAvailable = 3;
    
    public Stub() {
      attachInterface(this, "android.telephony.mbms.IMbmsGroupCallSessionCallback");
    }
    
    public static IMbmsGroupCallSessionCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.mbms.IMbmsGroupCallSessionCallback");
      if (iInterface != null && iInterface instanceof IMbmsGroupCallSessionCallback)
        return (IMbmsGroupCallSessionCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onMiddlewareReady";
          } 
          return "onServiceInterfaceAvailable";
        } 
        return "onAvailableSaisUpdated";
      } 
      return "onError";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ArrayList arrayList;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.telephony.mbms.IMbmsGroupCallSessionCallback");
              return true;
            } 
            param1Parcel1.enforceInterface("android.telephony.mbms.IMbmsGroupCallSessionCallback");
            onMiddlewareReady();
            return true;
          } 
          param1Parcel1.enforceInterface("android.telephony.mbms.IMbmsGroupCallSessionCallback");
          String str1 = param1Parcel1.readString();
          param1Int1 = param1Parcel1.readInt();
          onServiceInterfaceAvailable(str1, param1Int1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.telephony.mbms.IMbmsGroupCallSessionCallback");
        ClassLoader classLoader = getClass().getClassLoader();
        ArrayList arrayList1 = param1Parcel1.readArrayList(classLoader);
        arrayList = param1Parcel1.readArrayList(classLoader);
        onAvailableSaisUpdated(arrayList1, arrayList);
        return true;
      } 
      arrayList.enforceInterface("android.telephony.mbms.IMbmsGroupCallSessionCallback");
      param1Int1 = arrayList.readInt();
      String str = arrayList.readString();
      onError(param1Int1, str);
      return true;
    }
    
    private static class Proxy implements IMbmsGroupCallSessionCallback {
      public static IMbmsGroupCallSessionCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.mbms.IMbmsGroupCallSessionCallback";
      }
      
      public void onError(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.mbms.IMbmsGroupCallSessionCallback");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IMbmsGroupCallSessionCallback.Stub.getDefaultImpl() != null) {
            IMbmsGroupCallSessionCallback.Stub.getDefaultImpl().onError(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAvailableSaisUpdated(List param2List1, List param2List2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.mbms.IMbmsGroupCallSessionCallback");
          parcel.writeList(param2List1);
          parcel.writeList(param2List2);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IMbmsGroupCallSessionCallback.Stub.getDefaultImpl() != null) {
            IMbmsGroupCallSessionCallback.Stub.getDefaultImpl().onAvailableSaisUpdated(param2List1, param2List2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onServiceInterfaceAvailable(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.mbms.IMbmsGroupCallSessionCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IMbmsGroupCallSessionCallback.Stub.getDefaultImpl() != null) {
            IMbmsGroupCallSessionCallback.Stub.getDefaultImpl().onServiceInterfaceAvailable(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onMiddlewareReady() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.mbms.IMbmsGroupCallSessionCallback");
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IMbmsGroupCallSessionCallback.Stub.getDefaultImpl() != null) {
            IMbmsGroupCallSessionCallback.Stub.getDefaultImpl().onMiddlewareReady();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMbmsGroupCallSessionCallback param1IMbmsGroupCallSessionCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMbmsGroupCallSessionCallback != null) {
          Proxy.sDefaultImpl = param1IMbmsGroupCallSessionCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMbmsGroupCallSessionCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
