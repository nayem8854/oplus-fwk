package android.telephony.mbms;

import android.os.Binder;
import android.os.RemoteException;
import java.util.concurrent.Executor;

public class InternalDownloadProgressListener extends IDownloadProgressListener.Stub {
  private final DownloadProgressListener mAppListener;
  
  private final Executor mExecutor;
  
  private volatile boolean mIsStopped = false;
  
  public InternalDownloadProgressListener(DownloadProgressListener paramDownloadProgressListener, Executor paramExecutor) {
    this.mAppListener = paramDownloadProgressListener;
    this.mExecutor = paramExecutor;
  }
  
  public void onProgressUpdated(DownloadRequest paramDownloadRequest, FileInfo paramFileInfo, int paramInt1, int paramInt2, int paramInt3, int paramInt4) throws RemoteException {
    if (this.mIsStopped)
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mExecutor;
      Object object = new Object();
      super(this, paramDownloadRequest, paramFileInfo, paramInt1, paramInt2, paramInt3, paramInt4);
      executor.execute((Runnable)object);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void stop() {
    this.mIsStopped = true;
  }
}
