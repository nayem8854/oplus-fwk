package android.telephony.mbms;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class MbmsUtils {
  private static final String LOG_TAG = "MbmsUtils";
  
  public static boolean isContainedIn(File paramFile1, File paramFile2) {
    try {
      String str1 = paramFile1.getCanonicalPath();
      String str2 = paramFile2.getCanonicalPath();
      return str2.startsWith(str1);
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to resolve canonical paths: ");
      stringBuilder.append(iOException);
      throw new RuntimeException(stringBuilder.toString());
    } 
  }
  
  public static ComponentName toComponentName(ComponentInfo paramComponentInfo) {
    return new ComponentName(paramComponentInfo.packageName, paramComponentInfo.name);
  }
  
  public static ComponentName getOverrideServiceName(Context paramContext, String paramString) {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_1
    //   3: invokevirtual hashCode : ()I
    //   6: istore_3
    //   7: iload_3
    //   8: ldc -1374878107
    //   10: if_icmpeq -> 56
    //   13: iload_3
    //   14: ldc -407466459
    //   16: if_icmpeq -> 42
    //   19: iload_3
    //   20: ldc 1752202112
    //   22: if_icmpeq -> 28
    //   25: goto -> 70
    //   28: aload_1
    //   29: ldc 'android.telephony.action.EmbmsGroupCall'
    //   31: invokevirtual equals : (Ljava/lang/Object;)Z
    //   34: ifeq -> 25
    //   37: iconst_2
    //   38: istore_3
    //   39: goto -> 72
    //   42: aload_1
    //   43: ldc 'android.telephony.action.EmbmsDownload'
    //   45: invokevirtual equals : (Ljava/lang/Object;)Z
    //   48: ifeq -> 25
    //   51: iconst_0
    //   52: istore_3
    //   53: goto -> 72
    //   56: aload_1
    //   57: ldc 'android.telephony.action.EmbmsStreaming'
    //   59: invokevirtual equals : (Ljava/lang/Object;)Z
    //   62: ifeq -> 25
    //   65: iconst_1
    //   66: istore_3
    //   67: goto -> 72
    //   70: iconst_m1
    //   71: istore_3
    //   72: iload_3
    //   73: ifeq -> 103
    //   76: iload_3
    //   77: iconst_1
    //   78: if_icmpeq -> 97
    //   81: iload_3
    //   82: iconst_2
    //   83: if_icmpeq -> 91
    //   86: aload_2
    //   87: astore_1
    //   88: goto -> 106
    //   91: ldc 'mbms-group-call-service-override'
    //   93: astore_1
    //   94: goto -> 106
    //   97: ldc 'mbms-streaming-service-override'
    //   99: astore_1
    //   100: goto -> 106
    //   103: ldc 'mbms-download-service-override'
    //   105: astore_1
    //   106: aload_1
    //   107: ifnonnull -> 112
    //   110: aconst_null
    //   111: areturn
    //   112: aload_0
    //   113: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   116: astore_2
    //   117: aload_2
    //   118: aload_0
    //   119: invokevirtual getPackageName : ()Ljava/lang/String;
    //   122: sipush #128
    //   125: invokevirtual getApplicationInfo : (Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    //   128: astore_0
    //   129: aload_0
    //   130: getfield metaData : Landroid/os/Bundle;
    //   133: ifnonnull -> 138
    //   136: aconst_null
    //   137: areturn
    //   138: aload_0
    //   139: getfield metaData : Landroid/os/Bundle;
    //   142: aload_1
    //   143: invokevirtual getString : (Ljava/lang/String;)Ljava/lang/String;
    //   146: astore_0
    //   147: aload_0
    //   148: ifnonnull -> 153
    //   151: aconst_null
    //   152: areturn
    //   153: aload_0
    //   154: invokestatic unflattenFromString : (Ljava/lang/String;)Landroid/content/ComponentName;
    //   157: areturn
    //   158: astore_0
    //   159: aconst_null
    //   160: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #55	-> 0
    //   #56	-> 2
    //   #64	-> 91
    //   #61	-> 97
    //   #62	-> 100
    //   #58	-> 103
    //   #59	-> 106
    //   #67	-> 106
    //   #68	-> 110
    //   #73	-> 112
    //   #74	-> 117
    //   #77	-> 129
    //   #78	-> 129
    //   #79	-> 136
    //   #81	-> 138
    //   #82	-> 147
    //   #83	-> 151
    //   #85	-> 153
    //   #75	-> 158
    //   #76	-> 159
    // Exception table:
    //   from	to	target	type
    //   112	117	158	android/content/pm/PackageManager$NameNotFoundException
    //   117	129	158	android/content/pm/PackageManager$NameNotFoundException
  }
  
  public static ServiceInfo getMiddlewareServiceInfo(Context paramContext, String paramString) {
    List list;
    PackageManager packageManager = paramContext.getPackageManager();
    Intent intent = new Intent();
    intent.setAction(paramString);
    ComponentName componentName = getOverrideServiceName(paramContext, paramString);
    if (componentName == null) {
      list = packageManager.queryIntentServices(intent, 1048576);
    } else {
      intent.setComponent((ComponentName)list);
      list = packageManager.queryIntentServices(intent, 131072);
    } 
    if (list == null || list.size() == 0) {
      Log.w("MbmsUtils", "No MBMS services found, cannot get service info");
      return null;
    } 
    if (list.size() > 1) {
      Log.w("MbmsUtils", "More than one MBMS service found, cannot get unique service");
      return null;
    } 
    return ((ResolveInfo)list.get(0)).serviceInfo;
  }
  
  public static int startBinding(Context paramContext, String paramString, ServiceConnection paramServiceConnection) {
    Intent intent = new Intent();
    ServiceInfo serviceInfo = getMiddlewareServiceInfo(paramContext, paramString);
    if (serviceInfo == null)
      return 1; 
    intent.setComponent(toComponentName((ComponentInfo)serviceInfo));
    paramContext.bindService(intent, paramServiceConnection, 1);
    return 0;
  }
  
  public static File getEmbmsTempFileDirForService(Context paramContext, String paramString) {
    paramString = paramString.replaceAll("[^a-zA-Z0-9_]", "_");
    File file = MbmsTempFileProvider.getEmbmsTempFileDir(paramContext);
    return new File(file, paramString);
  }
}
