package android.telephony.mbms;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IMbmsStreamingSessionCallback extends IInterface {
  void onError(int paramInt, String paramString) throws RemoteException;
  
  void onMiddlewareReady() throws RemoteException;
  
  void onStreamingServicesUpdated(List<StreamingServiceInfo> paramList) throws RemoteException;
  
  class Default implements IMbmsStreamingSessionCallback {
    public void onError(int param1Int, String param1String) throws RemoteException {}
    
    public void onStreamingServicesUpdated(List<StreamingServiceInfo> param1List) throws RemoteException {}
    
    public void onMiddlewareReady() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMbmsStreamingSessionCallback {
    private static final String DESCRIPTOR = "android.telephony.mbms.IMbmsStreamingSessionCallback";
    
    static final int TRANSACTION_onError = 1;
    
    static final int TRANSACTION_onMiddlewareReady = 3;
    
    static final int TRANSACTION_onStreamingServicesUpdated = 2;
    
    public Stub() {
      attachInterface(this, "android.telephony.mbms.IMbmsStreamingSessionCallback");
    }
    
    public static IMbmsStreamingSessionCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.mbms.IMbmsStreamingSessionCallback");
      if (iInterface != null && iInterface instanceof IMbmsStreamingSessionCallback)
        return (IMbmsStreamingSessionCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onMiddlewareReady";
        } 
        return "onStreamingServicesUpdated";
      } 
      return "onError";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ArrayList<StreamingServiceInfo> arrayList;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.telephony.mbms.IMbmsStreamingSessionCallback");
            return true;
          } 
          param1Parcel1.enforceInterface("android.telephony.mbms.IMbmsStreamingSessionCallback");
          onMiddlewareReady();
          return true;
        } 
        param1Parcel1.enforceInterface("android.telephony.mbms.IMbmsStreamingSessionCallback");
        arrayList = param1Parcel1.createTypedArrayList(StreamingServiceInfo.CREATOR);
        onStreamingServicesUpdated(arrayList);
        return true;
      } 
      arrayList.enforceInterface("android.telephony.mbms.IMbmsStreamingSessionCallback");
      param1Int1 = arrayList.readInt();
      String str = arrayList.readString();
      onError(param1Int1, str);
      return true;
    }
    
    private static class Proxy implements IMbmsStreamingSessionCallback {
      public static IMbmsStreamingSessionCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.mbms.IMbmsStreamingSessionCallback";
      }
      
      public void onError(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.mbms.IMbmsStreamingSessionCallback");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IMbmsStreamingSessionCallback.Stub.getDefaultImpl() != null) {
            IMbmsStreamingSessionCallback.Stub.getDefaultImpl().onError(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onStreamingServicesUpdated(List<StreamingServiceInfo> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.mbms.IMbmsStreamingSessionCallback");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IMbmsStreamingSessionCallback.Stub.getDefaultImpl() != null) {
            IMbmsStreamingSessionCallback.Stub.getDefaultImpl().onStreamingServicesUpdated(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onMiddlewareReady() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.mbms.IMbmsStreamingSessionCallback");
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IMbmsStreamingSessionCallback.Stub.getDefaultImpl() != null) {
            IMbmsStreamingSessionCallback.Stub.getDefaultImpl().onMiddlewareReady();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMbmsStreamingSessionCallback param1IMbmsStreamingSessionCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMbmsStreamingSessionCallback != null) {
          Proxy.sDefaultImpl = param1IMbmsStreamingSessionCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMbmsStreamingSessionCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
