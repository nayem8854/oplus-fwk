package android.telephony.mbms;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Objects;

public class MbmsTempFileProvider extends ContentProvider {
  public static final String TEMP_FILE_ROOT_PREF_FILE_NAME = "MbmsTempFileRootPrefs";
  
  public static final String TEMP_FILE_ROOT_PREF_NAME = "mbms_temp_file_root";
  
  private String mAuthority;
  
  private Context mContext;
  
  public boolean onCreate() {
    return true;
  }
  
  public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2) {
    throw new UnsupportedOperationException("No querying supported");
  }
  
  public String getType(Uri paramUri) {
    return "application/octet-stream";
  }
  
  public Uri insert(Uri paramUri, ContentValues paramContentValues) {
    throw new UnsupportedOperationException("No inserting supported");
  }
  
  public int delete(Uri paramUri, String paramString, String[] paramArrayOfString) {
    throw new UnsupportedOperationException("No deleting supported");
  }
  
  public int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString) {
    throw new UnsupportedOperationException("No updating supported");
  }
  
  public ParcelFileDescriptor openFile(Uri paramUri, String paramString) throws FileNotFoundException {
    File file = getFileForUri(this.mContext, this.mAuthority, paramUri);
    int i = ParcelFileDescriptor.parseMode(paramString);
    return ParcelFileDescriptor.open(file, i);
  }
  
  public void attachInfo(Context paramContext, ProviderInfo paramProviderInfo) {
    super.attachInfo(paramContext, paramProviderInfo);
    if (!paramProviderInfo.exported) {
      if (paramProviderInfo.grantUriPermissions) {
        this.mAuthority = paramProviderInfo.authority;
        this.mContext = paramContext;
        return;
      } 
      throw new SecurityException("Provider must grant uri permissions");
    } 
    throw new SecurityException("Provider must not be exported");
  }
  
  public static Uri getUriForFile(Context paramContext, String paramString, File paramFile) {
    String str;
    try {
      String str1 = paramFile.getCanonicalPath();
      File file = getEmbmsTempFileDir(paramContext);
      if (MbmsUtils.isContainedIn(file, paramFile))
        try {
          String str2;
          str = file.getCanonicalPath();
          if (str.endsWith("/")) {
            str2 = str1.substring(str.length());
          } else {
            str2 = str1.substring(str.length() + 1);
          } 
          str = Uri.encode(str2);
          Uri.Builder builder = (new Uri.Builder()).scheme("content");
          return 
            builder.authority(paramString).encodedPath(str).build();
        } catch (IOException iOException) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Could not get canonical path for temp file root dir ");
          stringBuilder1.append(file);
          throw new RuntimeException(stringBuilder1.toString());
        }  
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("File ");
      stringBuilder.append(str);
      stringBuilder.append(" is not contained in the temp file directory, which is ");
      stringBuilder.append(file);
      throw new IllegalArgumentException(stringBuilder.toString());
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Could not get canonical path for file ");
      stringBuilder.append(str);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
  }
  
  public static File getFileForUri(Context paramContext, String paramString, Uri paramUri) throws FileNotFoundException {
    if ("content".equals(paramUri.getScheme())) {
      File file1, file2;
      if (Objects.equals(paramString, paramUri.getAuthority())) {
        paramString = Uri.decode(paramUri.getEncodedPath());
        try {
          File file = getEmbmsTempFileDir(paramContext).getCanonicalFile();
          file2 = new File();
          this(file, paramString);
          file1 = file2.getCanonicalFile();
          if (file1.getPath().startsWith(file.getPath()))
            return file1; 
          throw new SecurityException("Resolved path jumped beyond configured root");
        } catch (IOException iOException) {
          throw new FileNotFoundException("Could not resolve paths");
        } 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Uri does not have a matching authority: ");
      stringBuilder.append((String)file1);
      stringBuilder.append(", ");
      stringBuilder.append(file2.getAuthority());
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    throw new IllegalArgumentException("Uri must have scheme content");
  }
  
  public static File getEmbmsTempFileDir(Context paramContext) {
    SharedPreferences sharedPreferences = paramContext.getSharedPreferences("MbmsTempFileRootPrefs", 0);
    String str = sharedPreferences.getString("mbms_temp_file_root", null);
    if (str != null)
      try {
        File file1 = new File();
        this(str);
        return file1.getCanonicalFile();
      } catch (IOException iOException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unable to canonicalize temp file root path ");
        stringBuilder.append(iOException);
        throw new RuntimeException(stringBuilder.toString());
      }  
    File file = new File();
    this(iOException.getFilesDir(), "androidMbmsTempFileRoot");
    return file.getCanonicalFile();
  }
}
