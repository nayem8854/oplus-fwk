package android.telephony.mbms;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

public interface MbmsGroupCallSessionCallback {
  default void onError(int paramInt, String paramString) {}
  
  default void onAvailableSaisUpdated(List<Integer> paramList, List<List<Integer>> paramList1) {}
  
  default void onServiceInterfaceAvailable(String paramString, int paramInt) {}
  
  default void onMiddlewareReady() {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface GroupCallError {}
}
