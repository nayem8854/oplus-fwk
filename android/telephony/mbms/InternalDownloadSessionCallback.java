package android.telephony.mbms;

import android.os.Binder;
import java.util.List;
import java.util.concurrent.Executor;

public class InternalDownloadSessionCallback extends IMbmsDownloadSessionCallback.Stub {
  private final MbmsDownloadSessionCallback mAppCallback;
  
  private final Executor mExecutor;
  
  private volatile boolean mIsStopped = false;
  
  public InternalDownloadSessionCallback(MbmsDownloadSessionCallback paramMbmsDownloadSessionCallback, Executor paramExecutor) {
    this.mAppCallback = paramMbmsDownloadSessionCallback;
    this.mExecutor = paramExecutor;
  }
  
  public void onError(int paramInt, String paramString) {
    if (this.mIsStopped)
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mExecutor;
      Object object = new Object();
      super(this, paramInt, paramString);
      executor.execute((Runnable)object);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void onFileServicesUpdated(List<FileServiceInfo> paramList) {
    if (this.mIsStopped)
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mExecutor;
      Object object = new Object();
      super(this, paramList);
      executor.execute((Runnable)object);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void onMiddlewareReady() {
    if (this.mIsStopped)
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mExecutor;
      Object object = new Object();
      super(this);
      executor.execute((Runnable)object);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void stop() {
    this.mIsStopped = true;
  }
}
