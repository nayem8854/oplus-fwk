package android.telephony.mbms;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public final class StreamingServiceInfo extends ServiceInfo implements Parcelable {
  @SystemApi
  public StreamingServiceInfo(Map<Locale, String> paramMap, String paramString1, List<Locale> paramList, String paramString2, Date paramDate1, Date paramDate2) {
    super(paramMap, paramString1, paramList, paramString2, paramDate1, paramDate2);
  }
  
  public static final Parcelable.Creator<StreamingServiceInfo> CREATOR = new Parcelable.Creator<StreamingServiceInfo>() {
      public StreamingServiceInfo createFromParcel(Parcel param1Parcel) {
        return new StreamingServiceInfo(param1Parcel);
      }
      
      public StreamingServiceInfo[] newArray(int param1Int) {
        return new StreamingServiceInfo[param1Int];
      }
    };
  
  private StreamingServiceInfo(Parcel paramParcel) {
    super(paramParcel);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    super.writeToParcel(paramParcel, paramInt);
  }
  
  public int describeContents() {
    return 0;
  }
}
