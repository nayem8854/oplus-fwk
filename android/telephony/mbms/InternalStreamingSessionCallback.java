package android.telephony.mbms;

import android.os.Binder;
import android.os.RemoteException;
import java.util.List;
import java.util.concurrent.Executor;

public class InternalStreamingSessionCallback extends IMbmsStreamingSessionCallback.Stub {
  private final MbmsStreamingSessionCallback mAppCallback;
  
  private final Executor mExecutor;
  
  private volatile boolean mIsStopped = false;
  
  public InternalStreamingSessionCallback(MbmsStreamingSessionCallback paramMbmsStreamingSessionCallback, Executor paramExecutor) {
    this.mAppCallback = paramMbmsStreamingSessionCallback;
    this.mExecutor = paramExecutor;
  }
  
  public void onError(int paramInt, String paramString) throws RemoteException {
    if (this.mIsStopped)
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mExecutor;
      Object object = new Object();
      super(this, paramInt, paramString);
      executor.execute((Runnable)object);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void onStreamingServicesUpdated(List<StreamingServiceInfo> paramList) throws RemoteException {
    if (this.mIsStopped)
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mExecutor;
      Object object = new Object();
      super(this, paramList);
      executor.execute((Runnable)object);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void onMiddlewareReady() throws RemoteException {
    if (this.mIsStopped)
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mExecutor;
      Object object = new Object();
      super(this);
      executor.execute((Runnable)object);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void stop() {
    this.mIsStopped = true;
  }
}
