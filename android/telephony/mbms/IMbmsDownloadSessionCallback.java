package android.telephony.mbms;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IMbmsDownloadSessionCallback extends IInterface {
  void onError(int paramInt, String paramString) throws RemoteException;
  
  void onFileServicesUpdated(List<FileServiceInfo> paramList) throws RemoteException;
  
  void onMiddlewareReady() throws RemoteException;
  
  class Default implements IMbmsDownloadSessionCallback {
    public void onError(int param1Int, String param1String) throws RemoteException {}
    
    public void onFileServicesUpdated(List<FileServiceInfo> param1List) throws RemoteException {}
    
    public void onMiddlewareReady() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMbmsDownloadSessionCallback {
    private static final String DESCRIPTOR = "android.telephony.mbms.IMbmsDownloadSessionCallback";
    
    static final int TRANSACTION_onError = 1;
    
    static final int TRANSACTION_onFileServicesUpdated = 2;
    
    static final int TRANSACTION_onMiddlewareReady = 3;
    
    public Stub() {
      attachInterface(this, "android.telephony.mbms.IMbmsDownloadSessionCallback");
    }
    
    public static IMbmsDownloadSessionCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.mbms.IMbmsDownloadSessionCallback");
      if (iInterface != null && iInterface instanceof IMbmsDownloadSessionCallback)
        return (IMbmsDownloadSessionCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onMiddlewareReady";
        } 
        return "onFileServicesUpdated";
      } 
      return "onError";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ArrayList<FileServiceInfo> arrayList;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.telephony.mbms.IMbmsDownloadSessionCallback");
            return true;
          } 
          param1Parcel1.enforceInterface("android.telephony.mbms.IMbmsDownloadSessionCallback");
          onMiddlewareReady();
          return true;
        } 
        param1Parcel1.enforceInterface("android.telephony.mbms.IMbmsDownloadSessionCallback");
        arrayList = param1Parcel1.createTypedArrayList(FileServiceInfo.CREATOR);
        onFileServicesUpdated(arrayList);
        return true;
      } 
      arrayList.enforceInterface("android.telephony.mbms.IMbmsDownloadSessionCallback");
      param1Int1 = arrayList.readInt();
      String str = arrayList.readString();
      onError(param1Int1, str);
      return true;
    }
    
    private static class Proxy implements IMbmsDownloadSessionCallback {
      public static IMbmsDownloadSessionCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.mbms.IMbmsDownloadSessionCallback";
      }
      
      public void onError(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.mbms.IMbmsDownloadSessionCallback");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IMbmsDownloadSessionCallback.Stub.getDefaultImpl() != null) {
            IMbmsDownloadSessionCallback.Stub.getDefaultImpl().onError(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onFileServicesUpdated(List<FileServiceInfo> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.mbms.IMbmsDownloadSessionCallback");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IMbmsDownloadSessionCallback.Stub.getDefaultImpl() != null) {
            IMbmsDownloadSessionCallback.Stub.getDefaultImpl().onFileServicesUpdated(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onMiddlewareReady() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.mbms.IMbmsDownloadSessionCallback");
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IMbmsDownloadSessionCallback.Stub.getDefaultImpl() != null) {
            IMbmsDownloadSessionCallback.Stub.getDefaultImpl().onMiddlewareReady();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMbmsDownloadSessionCallback param1IMbmsDownloadSessionCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMbmsDownloadSessionCallback != null) {
          Proxy.sDefaultImpl = param1IMbmsDownloadSessionCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMbmsDownloadSessionCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
