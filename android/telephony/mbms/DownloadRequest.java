package android.telephony.mbms;

import android.annotation.SystemApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

public final class DownloadRequest implements Parcelable {
  class SerializationDataContainer implements Externalizable {
    private String appIntent;
    
    private Uri destination;
    
    private String fileServiceId;
    
    private Uri source;
    
    private int subscriptionId;
    
    private int version;
    
    public SerializationDataContainer() {}
    
    SerializationDataContainer(DownloadRequest this$0) {
      this.fileServiceId = this$0.fileServiceId;
      this.source = this$0.sourceUri;
      this.destination = this$0.destinationUri;
      this.subscriptionId = this$0.subscriptionId;
      this.appIntent = this$0.serializedResultIntentForApp;
      this.version = this$0.version;
    }
    
    public void writeExternal(ObjectOutput param1ObjectOutput) throws IOException {
      param1ObjectOutput.write(this.version);
      param1ObjectOutput.writeUTF(this.fileServiceId);
      param1ObjectOutput.writeUTF(this.source.toString());
      param1ObjectOutput.writeUTF(this.destination.toString());
      param1ObjectOutput.write(this.subscriptionId);
      param1ObjectOutput.writeUTF(this.appIntent);
    }
    
    public void readExternal(ObjectInput param1ObjectInput) throws IOException {
      this.version = param1ObjectInput.read();
      this.fileServiceId = param1ObjectInput.readUTF();
      this.source = Uri.parse(param1ObjectInput.readUTF());
      this.destination = Uri.parse(param1ObjectInput.readUTF());
      this.subscriptionId = param1ObjectInput.read();
      this.appIntent = param1ObjectInput.readUTF();
    }
  }
  
  class Builder {
    private String appIntent;
    
    private Uri destination;
    
    private String fileServiceId;
    
    private Uri source;
    
    private int subscriptionId;
    
    private int version = 1;
    
    public static Builder fromDownloadRequest(DownloadRequest param1DownloadRequest) {
      Builder builder = new Builder(param1DownloadRequest.sourceUri, param1DownloadRequest.destinationUri);
      builder = builder.setServiceId(param1DownloadRequest.fileServiceId);
      builder = builder.setSubscriptionId(param1DownloadRequest.subscriptionId);
      builder.appIntent = param1DownloadRequest.serializedResultIntentForApp;
      return builder;
    }
    
    public static Builder fromSerializedRequest(byte[] param1ArrayOfbyte) {
      try {
        ObjectInputStream objectInputStream = new ObjectInputStream();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream();
        this(param1ArrayOfbyte);
        this(byteArrayInputStream);
        DownloadRequest.SerializationDataContainer serializationDataContainer = (DownloadRequest.SerializationDataContainer)objectInputStream.readObject();
        Builder builder = new Builder();
        this(serializationDataContainer.source, serializationDataContainer.destination);
        builder.version = serializationDataContainer.version;
        builder.appIntent = serializationDataContainer.appIntent;
        builder.fileServiceId = serializationDataContainer.fileServiceId;
        builder.subscriptionId = serializationDataContainer.subscriptionId;
        return builder;
      } catch (IOException iOException) {
        Log.e("MbmsDownloadRequest", "Got IOException trying to parse opaque data");
        throw new IllegalArgumentException(iOException);
      } catch (ClassNotFoundException classNotFoundException) {
        Log.e("MbmsDownloadRequest", "Got ClassNotFoundException trying to parse opaque data");
        throw new IllegalArgumentException(classNotFoundException);
      } 
    }
    
    public Builder(DownloadRequest this$0, Uri param1Uri1) {
      if (this$0 != null && param1Uri1 != null) {
        this.source = (Uri)this$0;
        this.destination = param1Uri1;
        return;
      } 
      throw new IllegalArgumentException("Source and destination URIs must be non-null.");
    }
    
    public Builder setServiceInfo(FileServiceInfo param1FileServiceInfo) {
      this.fileServiceId = param1FileServiceInfo.getServiceId();
      return this;
    }
    
    @SystemApi
    public Builder setServiceId(String param1String) {
      this.fileServiceId = param1String;
      return this;
    }
    
    public Builder setSubscriptionId(int param1Int) {
      this.subscriptionId = param1Int;
      return this;
    }
    
    public Builder setAppIntent(Intent param1Intent) {
      String str = param1Intent.toUri(0);
      if (str.length() <= 50000)
        return this; 
      throw new IllegalArgumentException("App intent must not exceed length 50000");
    }
    
    public DownloadRequest build() {
      return new DownloadRequest(this.fileServiceId, this.source, this.destination, this.subscriptionId, this.appIntent, this.version);
    }
  }
  
  private DownloadRequest(String paramString1, Uri paramUri1, Uri paramUri2, int paramInt1, String paramString2, int paramInt2) {
    this.fileServiceId = paramString1;
    this.sourceUri = paramUri1;
    this.subscriptionId = paramInt1;
    this.destinationUri = paramUri2;
    this.serializedResultIntentForApp = paramString2;
    this.version = paramInt2;
  }
  
  private DownloadRequest(Parcel paramParcel) {
    this.fileServiceId = paramParcel.readString();
    this.sourceUri = (Uri)paramParcel.readParcelable(getClass().getClassLoader());
    this.destinationUri = (Uri)paramParcel.readParcelable(getClass().getClassLoader());
    this.subscriptionId = paramParcel.readInt();
    this.serializedResultIntentForApp = paramParcel.readString();
    this.version = paramParcel.readInt();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.fileServiceId);
    paramParcel.writeParcelable((Parcelable)this.sourceUri, paramInt);
    paramParcel.writeParcelable((Parcelable)this.destinationUri, paramInt);
    paramParcel.writeInt(this.subscriptionId);
    paramParcel.writeString(this.serializedResultIntentForApp);
    paramParcel.writeInt(this.version);
  }
  
  public String getFileServiceId() {
    return this.fileServiceId;
  }
  
  public Uri getSourceUri() {
    return this.sourceUri;
  }
  
  public Uri getDestinationUri() {
    return this.destinationUri;
  }
  
  public int getSubscriptionId() {
    return this.subscriptionId;
  }
  
  public Intent getIntentForApp() {
    try {
      return Intent.parseUri(this.serializedResultIntentForApp, 0);
    } catch (URISyntaxException uRISyntaxException) {
      return null;
    } 
  }
  
  public byte[] toByteArray() {
    try {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      this();
      ObjectOutputStream objectOutputStream = new ObjectOutputStream();
      this(byteArrayOutputStream);
      SerializationDataContainer serializationDataContainer = new SerializationDataContainer();
      this(this);
      objectOutputStream.writeObject(serializationDataContainer);
      objectOutputStream.flush();
      return byteArrayOutputStream.toByteArray();
    } catch (IOException iOException) {
      Log.e("MbmsDownloadRequest", "Got IOException trying to serialize opaque data");
      return null;
    } 
  }
  
  public int getVersion() {
    return this.version;
  }
  
  public static final Parcelable.Creator<DownloadRequest> CREATOR = new Parcelable.Creator<DownloadRequest>() {
      public DownloadRequest createFromParcel(Parcel param1Parcel) {
        return new DownloadRequest(param1Parcel);
      }
      
      public DownloadRequest[] newArray(int param1Int) {
        return new DownloadRequest[param1Int];
      }
    };
  
  private static final int CURRENT_VERSION = 1;
  
  private static final String LOG_TAG = "MbmsDownloadRequest";
  
  public static final int MAX_APP_INTENT_SIZE = 50000;
  
  public static final int MAX_DESTINATION_URI_SIZE = 50000;
  
  private final Uri destinationUri;
  
  private final String fileServiceId;
  
  private final String serializedResultIntentForApp;
  
  private final Uri sourceUri;
  
  private final int subscriptionId;
  
  private final int version;
  
  public static int getMaxAppIntentSize() {
    return 50000;
  }
  
  public static int getMaxDestinationUriSize() {
    return 50000;
  }
  
  public String getHash() {
    try {
      MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
      if (this.version >= 1) {
        messageDigest.update(this.sourceUri.toString().getBytes(StandardCharsets.UTF_8));
        messageDigest.update(this.destinationUri.toString().getBytes(StandardCharsets.UTF_8));
        String str = this.serializedResultIntentForApp;
        if (str != null)
          messageDigest.update(str.getBytes(StandardCharsets.UTF_8)); 
      } 
      return Base64.encodeToString(messageDigest.digest(), 10);
    } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
      throw new RuntimeException("Could not get sha256 hash object");
    } 
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (!(paramObject instanceof DownloadRequest))
      return false; 
    paramObject = paramObject;
    if (this.subscriptionId == ((DownloadRequest)paramObject).subscriptionId && this.version == ((DownloadRequest)paramObject).version) {
      String str1 = this.fileServiceId, str2 = ((DownloadRequest)paramObject).fileServiceId;
      if (Objects.equals(str1, str2)) {
        Uri uri1 = this.sourceUri, uri2 = ((DownloadRequest)paramObject).sourceUri;
        if (Objects.equals(uri1, uri2)) {
          uri1 = this.destinationUri;
          uri2 = ((DownloadRequest)paramObject).destinationUri;
          if (Objects.equals(uri1, uri2)) {
            String str = this.serializedResultIntentForApp;
            paramObject = ((DownloadRequest)paramObject).serializedResultIntentForApp;
            if (Objects.equals(str, paramObject))
              return null; 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    String str1 = this.fileServiceId;
    Uri uri1 = this.sourceUri, uri2 = this.destinationUri;
    int i = this.subscriptionId;
    String str2 = this.serializedResultIntentForApp;
    int j = this.version;
    return Objects.hash(new Object[] { str1, uri1, uri2, Integer.valueOf(i), str2, Integer.valueOf(j) });
  }
}
