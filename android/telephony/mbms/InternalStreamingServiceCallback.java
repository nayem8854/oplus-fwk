package android.telephony.mbms;

import android.os.Binder;
import android.os.RemoteException;
import java.util.concurrent.Executor;

public class InternalStreamingServiceCallback extends IStreamingServiceCallback.Stub {
  private final StreamingServiceCallback mAppCallback;
  
  private final Executor mExecutor;
  
  private volatile boolean mIsStopped = false;
  
  public InternalStreamingServiceCallback(StreamingServiceCallback paramStreamingServiceCallback, Executor paramExecutor) {
    this.mAppCallback = paramStreamingServiceCallback;
    this.mExecutor = paramExecutor;
  }
  
  public void onError(int paramInt, String paramString) throws RemoteException {
    if (this.mIsStopped)
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mExecutor;
      Object object = new Object();
      super(this, paramInt, paramString);
      executor.execute((Runnable)object);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void onStreamStateUpdated(int paramInt1, int paramInt2) throws RemoteException {
    if (this.mIsStopped)
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mExecutor;
      Object object = new Object();
      super(this, paramInt1, paramInt2);
      executor.execute((Runnable)object);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void onMediaDescriptionUpdated() throws RemoteException {
    if (this.mIsStopped)
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mExecutor;
      Object object = new Object();
      super(this);
      executor.execute((Runnable)object);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void onBroadcastSignalStrengthUpdated(int paramInt) throws RemoteException {
    if (this.mIsStopped)
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mExecutor;
      Object object = new Object();
      super(this, paramInt);
      executor.execute((Runnable)object);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void onStreamMethodUpdated(int paramInt) throws RemoteException {
    if (this.mIsStopped)
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mExecutor;
      Object object = new Object();
      super(this, paramInt);
      executor.execute((Runnable)object);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void stop() {
    this.mIsStopped = true;
  }
}
