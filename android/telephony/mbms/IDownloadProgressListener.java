package android.telephony.mbms;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IDownloadProgressListener extends IInterface {
  void onProgressUpdated(DownloadRequest paramDownloadRequest, FileInfo paramFileInfo, int paramInt1, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  class Default implements IDownloadProgressListener {
    public void onProgressUpdated(DownloadRequest param1DownloadRequest, FileInfo param1FileInfo, int param1Int1, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDownloadProgressListener {
    private static final String DESCRIPTOR = "android.telephony.mbms.IDownloadProgressListener";
    
    static final int TRANSACTION_onProgressUpdated = 1;
    
    public Stub() {
      attachInterface(this, "android.telephony.mbms.IDownloadProgressListener");
    }
    
    public static IDownloadProgressListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.mbms.IDownloadProgressListener");
      if (iInterface != null && iInterface instanceof IDownloadProgressListener)
        return (IDownloadProgressListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onProgressUpdated";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      DownloadRequest downloadRequest;
      FileInfo fileInfo;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.telephony.mbms.IDownloadProgressListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.telephony.mbms.IDownloadProgressListener");
      if (param1Parcel1.readInt() != 0) {
        downloadRequest = (DownloadRequest)DownloadRequest.CREATOR.createFromParcel(param1Parcel1);
      } else {
        downloadRequest = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        fileInfo = (FileInfo)FileInfo.CREATOR.createFromParcel(param1Parcel1);
      } else {
        fileInfo = null;
      } 
      param1Int2 = param1Parcel1.readInt();
      int i = param1Parcel1.readInt();
      param1Int1 = param1Parcel1.readInt();
      int j = param1Parcel1.readInt();
      onProgressUpdated(downloadRequest, fileInfo, param1Int2, i, param1Int1, j);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IDownloadProgressListener {
      public static IDownloadProgressListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.mbms.IDownloadProgressListener";
      }
      
      public void onProgressUpdated(DownloadRequest param2DownloadRequest, FileInfo param2FileInfo, int param2Int1, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.mbms.IDownloadProgressListener");
          if (param2DownloadRequest != null) {
            parcel1.writeInt(1);
            param2DownloadRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2FileInfo != null) {
            parcel1.writeInt(1);
            param2FileInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeInt(param2Int2);
              try {
                parcel1.writeInt(param2Int3);
                try {
                  parcel1.writeInt(param2Int4);
                  boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
                  if (!bool && IDownloadProgressListener.Stub.getDefaultImpl() != null) {
                    IDownloadProgressListener.Stub.getDefaultImpl().onProgressUpdated(param2DownloadRequest, param2FileInfo, param2Int1, param2Int2, param2Int3, param2Int4);
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } 
                  parcel2.readException();
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2DownloadRequest;
      }
    }
    
    public static boolean setDefaultImpl(IDownloadProgressListener param1IDownloadProgressListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDownloadProgressListener != null) {
          Proxy.sDefaultImpl = param1IDownloadProgressListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDownloadProgressListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
