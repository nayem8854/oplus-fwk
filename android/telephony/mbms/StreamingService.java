package android.telephony.mbms;

import android.net.Uri;
import android.os.RemoteException;
import android.telephony.MbmsStreamingSession;
import android.telephony.mbms.vendor.IMbmsStreamingService;
import android.util.Log;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class StreamingService implements AutoCloseable {
  public static final int BROADCAST_METHOD = 1;
  
  private static final String LOG_TAG = "MbmsStreamingService";
  
  public static final int REASON_BY_USER_REQUEST = 1;
  
  public static final int REASON_END_OF_SESSION = 2;
  
  public static final int REASON_FREQUENCY_CONFLICT = 3;
  
  public static final int REASON_LEFT_MBMS_BROADCAST_AREA = 6;
  
  public static final int REASON_NONE = 0;
  
  public static final int REASON_NOT_CONNECTED_TO_HOMECARRIER_LTE = 5;
  
  public static final int REASON_OUT_OF_MEMORY = 4;
  
  public static final int STATE_STALLED = 3;
  
  public static final int STATE_STARTED = 2;
  
  public static final int STATE_STOPPED = 1;
  
  public static final int UNICAST_METHOD = 2;
  
  private final InternalStreamingServiceCallback mCallback;
  
  private final MbmsStreamingSession mParentSession;
  
  private IMbmsStreamingService mService;
  
  private final StreamingServiceInfo mServiceInfo;
  
  private final int mSubscriptionId;
  
  public StreamingService(int paramInt, IMbmsStreamingService paramIMbmsStreamingService, MbmsStreamingSession paramMbmsStreamingSession, StreamingServiceInfo paramStreamingServiceInfo, InternalStreamingServiceCallback paramInternalStreamingServiceCallback) {
    this.mSubscriptionId = paramInt;
    this.mParentSession = paramMbmsStreamingSession;
    this.mService = paramIMbmsStreamingService;
    this.mServiceInfo = paramStreamingServiceInfo;
    this.mCallback = paramInternalStreamingServiceCallback;
  }
  
  public Uri getPlaybackUri() {
    IMbmsStreamingService iMbmsStreamingService = this.mService;
    if (iMbmsStreamingService != null)
      try {
        return iMbmsStreamingService.getPlaybackUri(this.mSubscriptionId, this.mServiceInfo.getServiceId());
      } catch (RemoteException remoteException) {
        Log.w("MbmsStreamingService", "Remote process died");
        this.mService = null;
        this.mParentSession.onStreamingServiceStopped(this);
        sendErrorToApp(3, null);
        return null;
      }  
    throw new IllegalStateException("No streaming service attached");
  }
  
  public StreamingServiceInfo getInfo() {
    return this.mServiceInfo;
  }
  
  public void close() {
    IMbmsStreamingService iMbmsStreamingService = this.mService;
    if (iMbmsStreamingService != null) {
      try {
        iMbmsStreamingService.stopStreaming(this.mSubscriptionId, this.mServiceInfo.getServiceId());
        this.mParentSession.onStreamingServiceStopped(this);
      } catch (RemoteException remoteException) {
        Log.w("MbmsStreamingService", "Remote process died");
        this.mService = null;
        sendErrorToApp(3, null);
        this.mParentSession.onStreamingServiceStopped(this);
      } finally {}
      return;
    } 
    throw new IllegalStateException("No streaming service attached");
  }
  
  public InternalStreamingServiceCallback getCallback() {
    return this.mCallback;
  }
  
  private void sendErrorToApp(int paramInt, String paramString) {
    try {
      this.mCallback.onError(paramInt, paramString);
    } catch (RemoteException remoteException) {}
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface StreamingState {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface StreamingStateChangeReason {}
}
