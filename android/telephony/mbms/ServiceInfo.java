package android.telephony.mbms;

import android.os.Parcel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;

public class ServiceInfo {
  static final int MAP_LIMIT = 1000;
  
  private final String className;
  
  private final List<Locale> locales;
  
  private final Map<Locale, String> names;
  
  private final String serviceId;
  
  private final Date sessionEndTime;
  
  private final Date sessionStartTime;
  
  public ServiceInfo(Map<Locale, String> paramMap, String paramString1, List<Locale> paramList, String paramString2, Date paramDate1, Date paramDate2) {
    if (paramMap != null && paramString1 != null && paramList != null && paramString2 != null && paramDate1 != null && paramDate2 != null) {
      StringBuilder stringBuilder1;
      if (paramMap.size() <= 1000) {
        if (paramList.size() <= 1000) {
          HashMap<Object, Object> hashMap = new HashMap<>(paramMap.size());
          hashMap.putAll(paramMap);
          this.className = paramString1;
          this.locales = new ArrayList<>(paramList);
          this.serviceId = paramString2;
          this.sessionStartTime = (Date)paramDate1.clone();
          this.sessionEndTime = (Date)paramDate2.clone();
          return;
        } 
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("bad locales length ");
        stringBuilder1.append(paramList.size());
        throw new RuntimeException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("bad map length ");
      stringBuilder2.append(stringBuilder1.size());
      throw new RuntimeException(stringBuilder2.toString());
    } 
    throw new IllegalArgumentException("Bad ServiceInfo construction");
  }
  
  protected ServiceInfo(Parcel paramParcel) {
    int i = paramParcel.readInt();
    if (i <= 1000 && i >= 0) {
      this.names = new HashMap<>(i);
      while (i > 0) {
        Locale locale = (Locale)paramParcel.readSerializable();
        String str = paramParcel.readString();
        this.names.put(locale, str);
        i--;
      } 
      this.className = paramParcel.readString();
      i = paramParcel.readInt();
      if (i <= 1000 && i >= 0) {
        this.locales = new ArrayList<>(i);
        while (i > 0) {
          Locale locale = (Locale)paramParcel.readSerializable();
          this.locales.add(locale);
          i--;
        } 
        this.serviceId = paramParcel.readString();
        this.sessionStartTime = (Date)paramParcel.readSerializable();
        this.sessionEndTime = (Date)paramParcel.readSerializable();
        return;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("bad locale length ");
      stringBuilder1.append(i);
      throw new RuntimeException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("bad map length");
    stringBuilder.append(i);
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    Set<Locale> set = this.names.keySet();
    paramParcel.writeInt(set.size());
    for (Locale locale : set) {
      paramParcel.writeSerializable(locale);
      paramParcel.writeString(this.names.get(locale));
    } 
    paramParcel.writeString(this.className);
    paramInt = this.locales.size();
    paramParcel.writeInt(paramInt);
    for (Locale locale : this.locales)
      paramParcel.writeSerializable(locale); 
    paramParcel.writeString(this.serviceId);
    paramParcel.writeSerializable(this.sessionStartTime);
    paramParcel.writeSerializable(this.sessionEndTime);
  }
  
  public CharSequence getNameForLocale(Locale paramLocale) {
    if (this.names.containsKey(paramLocale))
      return this.names.get(paramLocale); 
    throw new NoSuchElementException("Locale not supported");
  }
  
  public Set<Locale> getNamedContentLocales() {
    return Collections.unmodifiableSet(this.names.keySet());
  }
  
  public String getServiceClassName() {
    return this.className;
  }
  
  public List<Locale> getLocales() {
    return this.locales;
  }
  
  public String getServiceId() {
    return this.serviceId;
  }
  
  public Date getSessionStartTime() {
    return this.sessionStartTime;
  }
  
  public Date getSessionEndTime() {
    return this.sessionEndTime;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (!(paramObject instanceof ServiceInfo))
      return false; 
    paramObject = paramObject;
    if (Objects.equals(this.names, ((ServiceInfo)paramObject).names)) {
      String str1 = this.className, str2 = ((ServiceInfo)paramObject).className;
      if (Objects.equals(str1, str2)) {
        List<Locale> list2 = this.locales, list1 = ((ServiceInfo)paramObject).locales;
        if (Objects.equals(list2, list1)) {
          String str4 = this.serviceId, str3 = ((ServiceInfo)paramObject).serviceId;
          if (Objects.equals(str4, str3)) {
            Date date1 = this.sessionStartTime, date2 = ((ServiceInfo)paramObject).sessionStartTime;
            if (Objects.equals(date1, date2)) {
              date2 = this.sessionEndTime;
              paramObject = ((ServiceInfo)paramObject).sessionEndTime;
              if (Objects.equals(date2, paramObject))
                return null; 
            } 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.names, this.className, this.locales, this.serviceId, this.sessionStartTime, this.sessionEndTime });
  }
}
