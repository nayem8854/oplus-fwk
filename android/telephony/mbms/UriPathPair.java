package android.telephony.mbms;

import android.annotation.SystemApi;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class UriPathPair implements Parcelable {
  public UriPathPair(Uri paramUri1, Uri paramUri2) {
    if (paramUri1 != null && "file".equals(paramUri1.getScheme())) {
      if (paramUri2 != null && "content".equals(paramUri2.getScheme())) {
        this.mFilePathUri = paramUri1;
        this.mContentUri = paramUri2;
        return;
      } 
      throw new IllegalArgumentException("Content URI must have content scheme");
    } 
    throw new IllegalArgumentException("File URI must have file scheme");
  }
  
  private UriPathPair(Parcel paramParcel) {
    this.mFilePathUri = (Uri)paramParcel.readParcelable(Uri.class.getClassLoader());
    this.mContentUri = (Uri)paramParcel.readParcelable(Uri.class.getClassLoader());
  }
  
  public static final Parcelable.Creator<UriPathPair> CREATOR = new Parcelable.Creator<UriPathPair>() {
      public UriPathPair createFromParcel(Parcel param1Parcel) {
        return new UriPathPair(param1Parcel);
      }
      
      public UriPathPair[] newArray(int param1Int) {
        return new UriPathPair[param1Int];
      }
    };
  
  private final Uri mContentUri;
  
  private final Uri mFilePathUri;
  
  public Uri getFilePathUri() {
    return this.mFilePathUri;
  }
  
  public Uri getContentUri() {
    return this.mContentUri;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.mFilePathUri, paramInt);
    paramParcel.writeParcelable((Parcelable)this.mContentUri, paramInt);
  }
}
