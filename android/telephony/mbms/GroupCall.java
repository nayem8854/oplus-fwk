package android.telephony.mbms;

import android.os.RemoteException;
import android.telephony.MbmsGroupCallSession;
import android.telephony.mbms.vendor.IMbmsGroupCallService;
import android.util.Log;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

public class GroupCall implements AutoCloseable {
  private static final String LOG_TAG = "MbmsGroupCall";
  
  public static final int REASON_BY_USER_REQUEST = 1;
  
  public static final int REASON_FREQUENCY_CONFLICT = 3;
  
  public static final int REASON_LEFT_MBMS_BROADCAST_AREA = 6;
  
  public static final int REASON_NONE = 0;
  
  public static final int REASON_NOT_CONNECTED_TO_HOMECARRIER_LTE = 5;
  
  public static final int REASON_OUT_OF_MEMORY = 4;
  
  public static final int STATE_STALLED = 3;
  
  public static final int STATE_STARTED = 2;
  
  public static final int STATE_STOPPED = 1;
  
  private final InternalGroupCallCallback mCallback;
  
  private final MbmsGroupCallSession mParentSession;
  
  private IMbmsGroupCallService mService;
  
  private final int mSubscriptionId;
  
  private final long mTmgi;
  
  public GroupCall(int paramInt, IMbmsGroupCallService paramIMbmsGroupCallService, MbmsGroupCallSession paramMbmsGroupCallSession, long paramLong, InternalGroupCallCallback paramInternalGroupCallCallback) {
    this.mSubscriptionId = paramInt;
    this.mParentSession = paramMbmsGroupCallSession;
    this.mService = paramIMbmsGroupCallService;
    this.mTmgi = paramLong;
    this.mCallback = paramInternalGroupCallCallback;
  }
  
  public long getTmgi() {
    return this.mTmgi;
  }
  
  public void updateGroupCall(List<Integer> paramList1, List<Integer> paramList2) {
    IMbmsGroupCallService iMbmsGroupCallService = this.mService;
    if (iMbmsGroupCallService != null) {
      try {
        iMbmsGroupCallService.updateGroupCall(this.mSubscriptionId, this.mTmgi, paramList1, paramList2);
        this.mParentSession.onGroupCallStopped(this);
      } catch (RemoteException remoteException) {
        Log.w("MbmsGroupCall", "Remote process died");
        this.mService = null;
        sendErrorToApp(3, null);
        this.mParentSession.onGroupCallStopped(this);
      } finally {}
      return;
    } 
    throw new IllegalStateException("No group call service attached");
  }
  
  public void close() {
    IMbmsGroupCallService iMbmsGroupCallService = this.mService;
    if (iMbmsGroupCallService != null) {
      try {
        iMbmsGroupCallService.stopGroupCall(this.mSubscriptionId, this.mTmgi);
        this.mParentSession.onGroupCallStopped(this);
      } catch (RemoteException remoteException) {
        Log.w("MbmsGroupCall", "Remote process died");
        this.mService = null;
        sendErrorToApp(3, null);
        this.mParentSession.onGroupCallStopped(this);
      } finally {}
      return;
    } 
    throw new IllegalStateException("No group call service attached");
  }
  
  public InternalGroupCallCallback getCallback() {
    return this.mCallback;
  }
  
  private void sendErrorToApp(int paramInt, String paramString) {
    this.mCallback.onError(paramInt, paramString);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface GroupCallState {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface GroupCallStateChangeReason {}
}
