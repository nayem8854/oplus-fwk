package android.telephony.mbms;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IStreamingServiceCallback extends IInterface {
  void onBroadcastSignalStrengthUpdated(int paramInt) throws RemoteException;
  
  void onError(int paramInt, String paramString) throws RemoteException;
  
  void onMediaDescriptionUpdated() throws RemoteException;
  
  void onStreamMethodUpdated(int paramInt) throws RemoteException;
  
  void onStreamStateUpdated(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements IStreamingServiceCallback {
    public void onError(int param1Int, String param1String) throws RemoteException {}
    
    public void onStreamStateUpdated(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onMediaDescriptionUpdated() throws RemoteException {}
    
    public void onBroadcastSignalStrengthUpdated(int param1Int) throws RemoteException {}
    
    public void onStreamMethodUpdated(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IStreamingServiceCallback {
    private static final String DESCRIPTOR = "android.telephony.mbms.IStreamingServiceCallback";
    
    static final int TRANSACTION_onBroadcastSignalStrengthUpdated = 4;
    
    static final int TRANSACTION_onError = 1;
    
    static final int TRANSACTION_onMediaDescriptionUpdated = 3;
    
    static final int TRANSACTION_onStreamMethodUpdated = 5;
    
    static final int TRANSACTION_onStreamStateUpdated = 2;
    
    public Stub() {
      attachInterface(this, "android.telephony.mbms.IStreamingServiceCallback");
    }
    
    public static IStreamingServiceCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.mbms.IStreamingServiceCallback");
      if (iInterface != null && iInterface instanceof IStreamingServiceCallback)
        return (IStreamingServiceCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "onStreamMethodUpdated";
            } 
            return "onBroadcastSignalStrengthUpdated";
          } 
          return "onMediaDescriptionUpdated";
        } 
        return "onStreamStateUpdated";
      } 
      return "onError";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.telephony.mbms.IStreamingServiceCallback");
                return true;
              } 
              param1Parcel1.enforceInterface("android.telephony.mbms.IStreamingServiceCallback");
              param1Int1 = param1Parcel1.readInt();
              onStreamMethodUpdated(param1Int1);
              return true;
            } 
            param1Parcel1.enforceInterface("android.telephony.mbms.IStreamingServiceCallback");
            param1Int1 = param1Parcel1.readInt();
            onBroadcastSignalStrengthUpdated(param1Int1);
            return true;
          } 
          param1Parcel1.enforceInterface("android.telephony.mbms.IStreamingServiceCallback");
          onMediaDescriptionUpdated();
          return true;
        } 
        param1Parcel1.enforceInterface("android.telephony.mbms.IStreamingServiceCallback");
        param1Int1 = param1Parcel1.readInt();
        param1Int2 = param1Parcel1.readInt();
        onStreamStateUpdated(param1Int1, param1Int2);
        return true;
      } 
      param1Parcel1.enforceInterface("android.telephony.mbms.IStreamingServiceCallback");
      param1Int1 = param1Parcel1.readInt();
      String str = param1Parcel1.readString();
      onError(param1Int1, str);
      return true;
    }
    
    private static class Proxy implements IStreamingServiceCallback {
      public static IStreamingServiceCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.mbms.IStreamingServiceCallback";
      }
      
      public void onError(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.mbms.IStreamingServiceCallback");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IStreamingServiceCallback.Stub.getDefaultImpl() != null) {
            IStreamingServiceCallback.Stub.getDefaultImpl().onError(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onStreamStateUpdated(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.mbms.IStreamingServiceCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IStreamingServiceCallback.Stub.getDefaultImpl() != null) {
            IStreamingServiceCallback.Stub.getDefaultImpl().onStreamStateUpdated(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onMediaDescriptionUpdated() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.mbms.IStreamingServiceCallback");
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IStreamingServiceCallback.Stub.getDefaultImpl() != null) {
            IStreamingServiceCallback.Stub.getDefaultImpl().onMediaDescriptionUpdated();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onBroadcastSignalStrengthUpdated(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.mbms.IStreamingServiceCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IStreamingServiceCallback.Stub.getDefaultImpl() != null) {
            IStreamingServiceCallback.Stub.getDefaultImpl().onBroadcastSignalStrengthUpdated(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onStreamMethodUpdated(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.telephony.mbms.IStreamingServiceCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IStreamingServiceCallback.Stub.getDefaultImpl() != null) {
            IStreamingServiceCallback.Stub.getDefaultImpl().onStreamMethodUpdated(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IStreamingServiceCallback param1IStreamingServiceCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IStreamingServiceCallback != null) {
          Proxy.sDefaultImpl = param1IStreamingServiceCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IStreamingServiceCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
