package android.telephony.mbms;

import android.os.Binder;
import java.util.List;
import java.util.concurrent.Executor;

public class InternalGroupCallSessionCallback extends IMbmsGroupCallSessionCallback.Stub {
  private final MbmsGroupCallSessionCallback mAppCallback;
  
  private final Executor mExecutor;
  
  private volatile boolean mIsStopped = false;
  
  public InternalGroupCallSessionCallback(MbmsGroupCallSessionCallback paramMbmsGroupCallSessionCallback, Executor paramExecutor) {
    this.mAppCallback = paramMbmsGroupCallSessionCallback;
    this.mExecutor = paramExecutor;
  }
  
  public void onError(int paramInt, String paramString) {
    if (this.mIsStopped)
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mExecutor;
      Object object = new Object();
      super(this, paramInt, paramString);
      executor.execute((Runnable)object);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void onAvailableSaisUpdated(List paramList1, List paramList2) {
    if (this.mIsStopped)
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mExecutor;
      Object object = new Object();
      super(this, paramList1, paramList2);
      executor.execute((Runnable)object);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void onServiceInterfaceAvailable(String paramString, int paramInt) {
    if (this.mIsStopped)
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mExecutor;
      Object object = new Object();
      super(this, paramString, paramInt);
      executor.execute((Runnable)object);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void onMiddlewareReady() {
    if (this.mIsStopped)
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mExecutor;
      Object object = new Object();
      super(this);
      executor.execute((Runnable)object);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void stop() {
    this.mIsStopped = true;
  }
}
