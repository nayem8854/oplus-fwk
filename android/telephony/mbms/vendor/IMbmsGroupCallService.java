package android.telephony.mbms.vendor;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.mbms.IGroupCallCallback;
import android.telephony.mbms.IMbmsGroupCallSessionCallback;
import java.util.ArrayList;
import java.util.List;

public interface IMbmsGroupCallService extends IInterface {
  void dispose(int paramInt) throws RemoteException;
  
  int initialize(IMbmsGroupCallSessionCallback paramIMbmsGroupCallSessionCallback, int paramInt) throws RemoteException;
  
  int startGroupCall(int paramInt, long paramLong, List paramList1, List paramList2, IGroupCallCallback paramIGroupCallCallback) throws RemoteException;
  
  void stopGroupCall(int paramInt, long paramLong) throws RemoteException;
  
  void updateGroupCall(int paramInt, long paramLong, List paramList1, List paramList2) throws RemoteException;
  
  class Default implements IMbmsGroupCallService {
    public int initialize(IMbmsGroupCallSessionCallback param1IMbmsGroupCallSessionCallback, int param1Int) throws RemoteException {
      return 0;
    }
    
    public void stopGroupCall(int param1Int, long param1Long) throws RemoteException {}
    
    public void updateGroupCall(int param1Int, long param1Long, List param1List1, List param1List2) throws RemoteException {}
    
    public int startGroupCall(int param1Int, long param1Long, List param1List1, List param1List2, IGroupCallCallback param1IGroupCallCallback) throws RemoteException {
      return 0;
    }
    
    public void dispose(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMbmsGroupCallService {
    private static final String DESCRIPTOR = "android.telephony.mbms.vendor.IMbmsGroupCallService";
    
    static final int TRANSACTION_dispose = 5;
    
    static final int TRANSACTION_initialize = 1;
    
    static final int TRANSACTION_startGroupCall = 4;
    
    static final int TRANSACTION_stopGroupCall = 2;
    
    static final int TRANSACTION_updateGroupCall = 3;
    
    public Stub() {
      attachInterface(this, "android.telephony.mbms.vendor.IMbmsGroupCallService");
    }
    
    public static IMbmsGroupCallService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.mbms.vendor.IMbmsGroupCallService");
      if (iInterface != null && iInterface instanceof IMbmsGroupCallService)
        return (IMbmsGroupCallService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "dispose";
            } 
            return "startGroupCall";
          } 
          return "updateGroupCall";
        } 
        return "stopGroupCall";
      } 
      return "initialize";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ArrayList arrayList;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          IGroupCallCallback iGroupCallCallback;
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.telephony.mbms.vendor.IMbmsGroupCallService");
                return true;
              } 
              param1Parcel1.enforceInterface("android.telephony.mbms.vendor.IMbmsGroupCallService");
              param1Int1 = param1Parcel1.readInt();
              dispose(param1Int1);
              param1Parcel2.writeNoException();
              return true;
            } 
            param1Parcel1.enforceInterface("android.telephony.mbms.vendor.IMbmsGroupCallService");
            param1Int1 = param1Parcel1.readInt();
            long l2 = param1Parcel1.readLong();
            ClassLoader classLoader1 = getClass().getClassLoader();
            ArrayList arrayList3 = param1Parcel1.readArrayList(classLoader1);
            ArrayList arrayList2 = param1Parcel1.readArrayList(classLoader1);
            iGroupCallCallback = IGroupCallCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            param1Int1 = startGroupCall(param1Int1, l2, arrayList3, arrayList2, iGroupCallCallback);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          } 
          iGroupCallCallback.enforceInterface("android.telephony.mbms.vendor.IMbmsGroupCallService");
          param1Int1 = iGroupCallCallback.readInt();
          long l1 = iGroupCallCallback.readLong();
          ClassLoader classLoader = getClass().getClassLoader();
          ArrayList arrayList1 = iGroupCallCallback.readArrayList(classLoader);
          arrayList = iGroupCallCallback.readArrayList(classLoader);
          updateGroupCall(param1Int1, l1, arrayList1, arrayList);
          param1Parcel2.writeNoException();
          return true;
        } 
        arrayList.enforceInterface("android.telephony.mbms.vendor.IMbmsGroupCallService");
        param1Int1 = arrayList.readInt();
        long l = arrayList.readLong();
        stopGroupCall(param1Int1, l);
        param1Parcel2.writeNoException();
        return true;
      } 
      arrayList.enforceInterface("android.telephony.mbms.vendor.IMbmsGroupCallService");
      IMbmsGroupCallSessionCallback iMbmsGroupCallSessionCallback = IMbmsGroupCallSessionCallback.Stub.asInterface(arrayList.readStrongBinder());
      param1Int1 = arrayList.readInt();
      param1Int1 = initialize(iMbmsGroupCallSessionCallback, param1Int1);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(param1Int1);
      return true;
    }
    
    private static class Proxy implements IMbmsGroupCallService {
      public static IMbmsGroupCallService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.mbms.vendor.IMbmsGroupCallService";
      }
      
      public int initialize(IMbmsGroupCallSessionCallback param2IMbmsGroupCallSessionCallback, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsGroupCallService");
          if (param2IMbmsGroupCallSessionCallback != null) {
            iBinder = param2IMbmsGroupCallSessionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IMbmsGroupCallService.Stub.getDefaultImpl() != null) {
            param2Int = IMbmsGroupCallService.Stub.getDefaultImpl().initialize(param2IMbmsGroupCallSessionCallback, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopGroupCall(int param2Int, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsGroupCallService");
          parcel1.writeInt(param2Int);
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IMbmsGroupCallService.Stub.getDefaultImpl() != null) {
            IMbmsGroupCallService.Stub.getDefaultImpl().stopGroupCall(param2Int, param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateGroupCall(int param2Int, long param2Long, List param2List1, List param2List2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsGroupCallService");
          parcel1.writeInt(param2Int);
          parcel1.writeLong(param2Long);
          parcel1.writeList(param2List1);
          parcel1.writeList(param2List2);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IMbmsGroupCallService.Stub.getDefaultImpl() != null) {
            IMbmsGroupCallService.Stub.getDefaultImpl().updateGroupCall(param2Int, param2Long, param2List1, param2List2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int startGroupCall(int param2Int, long param2Long, List param2List1, List param2List2, IGroupCallCallback param2IGroupCallCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsGroupCallService");
          try {
            parcel1.writeInt(param2Int);
            try {
              parcel1.writeLong(param2Long);
              try {
                parcel1.writeList(param2List1);
                try {
                  IBinder iBinder;
                  parcel1.writeList(param2List2);
                  if (param2IGroupCallCallback != null) {
                    iBinder = param2IGroupCallCallback.asBinder();
                  } else {
                    iBinder = null;
                  } 
                  parcel1.writeStrongBinder(iBinder);
                  try {
                    boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
                    if (!bool && IMbmsGroupCallService.Stub.getDefaultImpl() != null) {
                      param2Int = IMbmsGroupCallService.Stub.getDefaultImpl().startGroupCall(param2Int, param2Long, param2List1, param2List2, param2IGroupCallCallback);
                      parcel2.recycle();
                      parcel1.recycle();
                      return param2Int;
                    } 
                    parcel2.readException();
                    param2Int = parcel2.readInt();
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2Int;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2List1;
      }
      
      public void dispose(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsGroupCallService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IMbmsGroupCallService.Stub.getDefaultImpl() != null) {
            IMbmsGroupCallService.Stub.getDefaultImpl().dispose(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMbmsGroupCallService param1IMbmsGroupCallService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMbmsGroupCallService != null) {
          Proxy.sDefaultImpl = param1IMbmsGroupCallService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMbmsGroupCallService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
