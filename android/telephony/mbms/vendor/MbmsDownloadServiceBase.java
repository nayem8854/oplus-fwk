package android.telephony.mbms.vendor;

import android.annotation.SystemApi;
import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.mbms.DownloadProgressListener;
import android.telephony.mbms.DownloadRequest;
import android.telephony.mbms.DownloadStatusListener;
import android.telephony.mbms.FileInfo;
import android.telephony.mbms.IDownloadProgressListener;
import android.telephony.mbms.IDownloadStatusListener;
import android.telephony.mbms.IMbmsDownloadSessionCallback;
import android.telephony.mbms.MbmsDownloadSessionCallback;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SystemApi
public class MbmsDownloadServiceBase extends IMbmsDownloadService.Stub {
  private final Map<IBinder, DownloadStatusListener> mDownloadStatusListenerBinderMap = new HashMap<>();
  
  private final Map<IBinder, DownloadProgressListener> mDownloadProgressListenerBinderMap = new HashMap<>();
  
  private final Map<IBinder, IBinder.DeathRecipient> mDownloadCallbackDeathRecipients = new HashMap<>();
  
  class VendorDownloadStatusListener extends DownloadStatusListener {
    private final IDownloadStatusListener mListener;
    
    public VendorDownloadStatusListener(MbmsDownloadServiceBase this$0) {
      this.mListener = (IDownloadStatusListener)this$0;
    }
    
    protected abstract void onRemoteException(RemoteException param1RemoteException);
    
    public void onStatusUpdated(DownloadRequest param1DownloadRequest, FileInfo param1FileInfo, int param1Int) {
      try {
        this.mListener.onStatusUpdated(param1DownloadRequest, param1FileInfo, param1Int);
      } catch (RemoteException remoteException) {
        onRemoteException(remoteException);
      } 
    }
  }
  
  class VendorDownloadProgressListener extends DownloadProgressListener {
    private final IDownloadProgressListener mListener;
    
    public VendorDownloadProgressListener(MbmsDownloadServiceBase this$0) {
      this.mListener = (IDownloadProgressListener)this$0;
    }
    
    public void onProgressUpdated(DownloadRequest param1DownloadRequest, FileInfo param1FileInfo, int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      try {
        this.mListener.onProgressUpdated(param1DownloadRequest, param1FileInfo, param1Int1, param1Int2, param1Int3, param1Int4);
      } catch (RemoteException remoteException) {
        onRemoteException(remoteException);
      } 
    }
    
    protected abstract void onRemoteException(RemoteException param1RemoteException);
  }
  
  public int initialize(int paramInt, MbmsDownloadSessionCallback paramMbmsDownloadSessionCallback) throws RemoteException {
    return 0;
  }
  
  public final int initialize(int paramInt, IMbmsDownloadSessionCallback paramIMbmsDownloadSessionCallback) throws RemoteException {
    if (paramIMbmsDownloadSessionCallback != null) {
      int i = Binder.getCallingUid();
      int j = initialize(paramInt, (MbmsDownloadSessionCallback)new Object(this, paramIMbmsDownloadSessionCallback, i, paramInt));
      if (j == 0)
        paramIMbmsDownloadSessionCallback.asBinder().linkToDeath((IBinder.DeathRecipient)new Object(this, i, paramInt), 0); 
      return j;
    } 
    throw new NullPointerException("Callback must not be null");
  }
  
  public int requestUpdateFileServices(int paramInt, List<String> paramList) throws RemoteException {
    return 0;
  }
  
  public int setTempFileRootDirectory(int paramInt, String paramString) throws RemoteException {
    return 0;
  }
  
  public int download(DownloadRequest paramDownloadRequest) throws RemoteException {
    return 0;
  }
  
  public int addStatusListener(DownloadRequest paramDownloadRequest, DownloadStatusListener paramDownloadStatusListener) throws RemoteException {
    return 0;
  }
  
  public final int addStatusListener(DownloadRequest paramDownloadRequest, IDownloadStatusListener paramIDownloadStatusListener) throws RemoteException {
    int i = Binder.getCallingUid();
    if (paramDownloadRequest != null) {
      if (paramIDownloadStatusListener != null) {
        Object object = new Object(this, paramIDownloadStatusListener, i, paramDownloadRequest);
        int j = addStatusListener(paramDownloadRequest, (DownloadStatusListener)object);
        if (j == 0) {
          Object object1 = new Object(this, i, paramDownloadRequest, paramIDownloadStatusListener);
          this.mDownloadCallbackDeathRecipients.put(paramIDownloadStatusListener.asBinder(), object1);
          paramIDownloadStatusListener.asBinder().linkToDeath((IBinder.DeathRecipient)object1, 0);
          this.mDownloadStatusListenerBinderMap.put(paramIDownloadStatusListener.asBinder(), object);
        } 
        return j;
      } 
      throw new NullPointerException("Callback must not be null");
    } 
    throw new NullPointerException("Download request must not be null");
  }
  
  public int removeStatusListener(DownloadRequest paramDownloadRequest, DownloadStatusListener paramDownloadStatusListener) throws RemoteException {
    return 0;
  }
  
  public final int removeStatusListener(DownloadRequest paramDownloadRequest, IDownloadStatusListener paramIDownloadStatusListener) throws RemoteException {
    if (paramDownloadRequest != null) {
      if (paramIDownloadStatusListener != null) {
        Map<IBinder, IBinder.DeathRecipient> map = this.mDownloadCallbackDeathRecipients;
        IBinder.DeathRecipient deathRecipient = map.remove(paramIDownloadStatusListener.asBinder());
        if (deathRecipient != null) {
          paramIDownloadStatusListener.asBinder().unlinkToDeath(deathRecipient, 0);
          Map<IBinder, DownloadStatusListener> map1 = this.mDownloadStatusListenerBinderMap;
          DownloadStatusListener downloadStatusListener = map1.remove(paramIDownloadStatusListener.asBinder());
          if (downloadStatusListener != null)
            return removeStatusListener(paramDownloadRequest, downloadStatusListener); 
          throw new IllegalArgumentException("Unknown listener");
        } 
        throw new IllegalArgumentException("Unknown listener");
      } 
      throw new NullPointerException("Callback must not be null");
    } 
    throw new NullPointerException("Download request must not be null");
  }
  
  public int addProgressListener(DownloadRequest paramDownloadRequest, DownloadProgressListener paramDownloadProgressListener) throws RemoteException {
    return 0;
  }
  
  public final int addProgressListener(DownloadRequest paramDownloadRequest, IDownloadProgressListener paramIDownloadProgressListener) throws RemoteException {
    int i = Binder.getCallingUid();
    if (paramDownloadRequest != null) {
      if (paramIDownloadProgressListener != null) {
        Object object = new Object(this, paramIDownloadProgressListener, i, paramDownloadRequest);
        int j = addProgressListener(paramDownloadRequest, (DownloadProgressListener)object);
        if (j == 0) {
          Object object1 = new Object(this, i, paramDownloadRequest, paramIDownloadProgressListener);
          this.mDownloadCallbackDeathRecipients.put(paramIDownloadProgressListener.asBinder(), object1);
          paramIDownloadProgressListener.asBinder().linkToDeath((IBinder.DeathRecipient)object1, 0);
          this.mDownloadProgressListenerBinderMap.put(paramIDownloadProgressListener.asBinder(), object);
        } 
        return j;
      } 
      throw new NullPointerException("Callback must not be null");
    } 
    throw new NullPointerException("Download request must not be null");
  }
  
  public int removeProgressListener(DownloadRequest paramDownloadRequest, DownloadProgressListener paramDownloadProgressListener) throws RemoteException {
    return 0;
  }
  
  public final int removeProgressListener(DownloadRequest paramDownloadRequest, IDownloadProgressListener paramIDownloadProgressListener) throws RemoteException {
    if (paramDownloadRequest != null) {
      if (paramIDownloadProgressListener != null) {
        Map<IBinder, IBinder.DeathRecipient> map = this.mDownloadCallbackDeathRecipients;
        IBinder.DeathRecipient deathRecipient = map.remove(paramIDownloadProgressListener.asBinder());
        if (deathRecipient != null) {
          paramIDownloadProgressListener.asBinder().unlinkToDeath(deathRecipient, 0);
          Map<IBinder, DownloadProgressListener> map1 = this.mDownloadProgressListenerBinderMap;
          DownloadProgressListener downloadProgressListener = map1.remove(paramIDownloadProgressListener.asBinder());
          if (downloadProgressListener != null)
            return removeProgressListener(paramDownloadRequest, downloadProgressListener); 
          throw new IllegalArgumentException("Unknown listener");
        } 
        throw new IllegalArgumentException("Unknown listener");
      } 
      throw new NullPointerException("Callback must not be null");
    } 
    throw new NullPointerException("Download request must not be null");
  }
  
  public List<DownloadRequest> listPendingDownloads(int paramInt) throws RemoteException {
    return null;
  }
  
  public int cancelDownload(DownloadRequest paramDownloadRequest) throws RemoteException {
    return 0;
  }
  
  public int requestDownloadState(DownloadRequest paramDownloadRequest, FileInfo paramFileInfo) throws RemoteException {
    return 0;
  }
  
  public int resetDownloadKnowledge(DownloadRequest paramDownloadRequest) throws RemoteException {
    return 0;
  }
  
  public void dispose(int paramInt) throws RemoteException {}
  
  public void onAppCallbackDied(int paramInt1, int paramInt2) {}
  
  @SystemApi
  public IBinder asBinder() {
    return super.asBinder();
  }
  
  @SystemApi
  public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2) throws RemoteException {
    return super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
  }
}
