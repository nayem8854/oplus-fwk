package android.telephony.mbms.vendor;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.mbms.DownloadRequest;
import android.telephony.mbms.FileInfo;
import android.telephony.mbms.IDownloadProgressListener;
import android.telephony.mbms.IDownloadStatusListener;
import android.telephony.mbms.IMbmsDownloadSessionCallback;
import java.util.ArrayList;
import java.util.List;

public interface IMbmsDownloadService extends IInterface {
  int addProgressListener(DownloadRequest paramDownloadRequest, IDownloadProgressListener paramIDownloadProgressListener) throws RemoteException;
  
  int addStatusListener(DownloadRequest paramDownloadRequest, IDownloadStatusListener paramIDownloadStatusListener) throws RemoteException;
  
  int cancelDownload(DownloadRequest paramDownloadRequest) throws RemoteException;
  
  void dispose(int paramInt) throws RemoteException;
  
  int download(DownloadRequest paramDownloadRequest) throws RemoteException;
  
  int initialize(int paramInt, IMbmsDownloadSessionCallback paramIMbmsDownloadSessionCallback) throws RemoteException;
  
  List<DownloadRequest> listPendingDownloads(int paramInt) throws RemoteException;
  
  int removeProgressListener(DownloadRequest paramDownloadRequest, IDownloadProgressListener paramIDownloadProgressListener) throws RemoteException;
  
  int removeStatusListener(DownloadRequest paramDownloadRequest, IDownloadStatusListener paramIDownloadStatusListener) throws RemoteException;
  
  int requestDownloadState(DownloadRequest paramDownloadRequest, FileInfo paramFileInfo) throws RemoteException;
  
  int requestUpdateFileServices(int paramInt, List<String> paramList) throws RemoteException;
  
  int resetDownloadKnowledge(DownloadRequest paramDownloadRequest) throws RemoteException;
  
  int setTempFileRootDirectory(int paramInt, String paramString) throws RemoteException;
  
  class Default implements IMbmsDownloadService {
    public int initialize(int param1Int, IMbmsDownloadSessionCallback param1IMbmsDownloadSessionCallback) throws RemoteException {
      return 0;
    }
    
    public int requestUpdateFileServices(int param1Int, List<String> param1List) throws RemoteException {
      return 0;
    }
    
    public int setTempFileRootDirectory(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public int download(DownloadRequest param1DownloadRequest) throws RemoteException {
      return 0;
    }
    
    public int addStatusListener(DownloadRequest param1DownloadRequest, IDownloadStatusListener param1IDownloadStatusListener) throws RemoteException {
      return 0;
    }
    
    public int removeStatusListener(DownloadRequest param1DownloadRequest, IDownloadStatusListener param1IDownloadStatusListener) throws RemoteException {
      return 0;
    }
    
    public int addProgressListener(DownloadRequest param1DownloadRequest, IDownloadProgressListener param1IDownloadProgressListener) throws RemoteException {
      return 0;
    }
    
    public int removeProgressListener(DownloadRequest param1DownloadRequest, IDownloadProgressListener param1IDownloadProgressListener) throws RemoteException {
      return 0;
    }
    
    public List<DownloadRequest> listPendingDownloads(int param1Int) throws RemoteException {
      return null;
    }
    
    public int cancelDownload(DownloadRequest param1DownloadRequest) throws RemoteException {
      return 0;
    }
    
    public int requestDownloadState(DownloadRequest param1DownloadRequest, FileInfo param1FileInfo) throws RemoteException {
      return 0;
    }
    
    public int resetDownloadKnowledge(DownloadRequest param1DownloadRequest) throws RemoteException {
      return 0;
    }
    
    public void dispose(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMbmsDownloadService {
    private static final String DESCRIPTOR = "android.telephony.mbms.vendor.IMbmsDownloadService";
    
    static final int TRANSACTION_addProgressListener = 7;
    
    static final int TRANSACTION_addStatusListener = 5;
    
    static final int TRANSACTION_cancelDownload = 10;
    
    static final int TRANSACTION_dispose = 13;
    
    static final int TRANSACTION_download = 4;
    
    static final int TRANSACTION_initialize = 1;
    
    static final int TRANSACTION_listPendingDownloads = 9;
    
    static final int TRANSACTION_removeProgressListener = 8;
    
    static final int TRANSACTION_removeStatusListener = 6;
    
    static final int TRANSACTION_requestDownloadState = 11;
    
    static final int TRANSACTION_requestUpdateFileServices = 2;
    
    static final int TRANSACTION_resetDownloadKnowledge = 12;
    
    static final int TRANSACTION_setTempFileRootDirectory = 3;
    
    public Stub() {
      attachInterface(this, "android.telephony.mbms.vendor.IMbmsDownloadService");
    }
    
    public static IMbmsDownloadService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.mbms.vendor.IMbmsDownloadService");
      if (iInterface != null && iInterface instanceof IMbmsDownloadService)
        return (IMbmsDownloadService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 13:
          return "dispose";
        case 12:
          return "resetDownloadKnowledge";
        case 11:
          return "requestDownloadState";
        case 10:
          return "cancelDownload";
        case 9:
          return "listPendingDownloads";
        case 8:
          return "removeProgressListener";
        case 7:
          return "addProgressListener";
        case 6:
          return "removeStatusListener";
        case 5:
          return "addStatusListener";
        case 4:
          return "download";
        case 3:
          return "setTempFileRootDirectory";
        case 2:
          return "requestUpdateFileServices";
        case 1:
          break;
      } 
      return "initialize";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        List<DownloadRequest> list;
        IDownloadProgressListener iDownloadProgressListener;
        IDownloadStatusListener iDownloadStatusListener;
        String str;
        ArrayList<String> arrayList;
        DownloadRequest downloadRequest;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 13:
            param1Parcel1.enforceInterface("android.telephony.mbms.vendor.IMbmsDownloadService");
            param1Int1 = param1Parcel1.readInt();
            dispose(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            param1Parcel1.enforceInterface("android.telephony.mbms.vendor.IMbmsDownloadService");
            if (param1Parcel1.readInt() != 0) {
              DownloadRequest downloadRequest1 = (DownloadRequest)DownloadRequest.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            param1Int1 = resetDownloadKnowledge((DownloadRequest)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 11:
            param1Parcel1.enforceInterface("android.telephony.mbms.vendor.IMbmsDownloadService");
            if (param1Parcel1.readInt() != 0) {
              downloadRequest = (DownloadRequest)DownloadRequest.CREATOR.createFromParcel(param1Parcel1);
            } else {
              downloadRequest = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              FileInfo fileInfo = (FileInfo)FileInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            param1Int1 = requestDownloadState(downloadRequest, (FileInfo)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 10:
            param1Parcel1.enforceInterface("android.telephony.mbms.vendor.IMbmsDownloadService");
            if (param1Parcel1.readInt() != 0) {
              DownloadRequest downloadRequest1 = (DownloadRequest)DownloadRequest.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            param1Int1 = cancelDownload((DownloadRequest)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.telephony.mbms.vendor.IMbmsDownloadService");
            param1Int1 = param1Parcel1.readInt();
            list = listPendingDownloads(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 8:
            list.enforceInterface("android.telephony.mbms.vendor.IMbmsDownloadService");
            if (list.readInt() != 0) {
              downloadRequest = (DownloadRequest)DownloadRequest.CREATOR.createFromParcel((Parcel)list);
            } else {
              downloadRequest = null;
            } 
            iDownloadProgressListener = IDownloadProgressListener.Stub.asInterface(list.readStrongBinder());
            param1Int1 = removeProgressListener(downloadRequest, iDownloadProgressListener);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 7:
            iDownloadProgressListener.enforceInterface("android.telephony.mbms.vendor.IMbmsDownloadService");
            if (iDownloadProgressListener.readInt() != 0) {
              downloadRequest = (DownloadRequest)DownloadRequest.CREATOR.createFromParcel((Parcel)iDownloadProgressListener);
            } else {
              downloadRequest = null;
            } 
            iDownloadProgressListener = IDownloadProgressListener.Stub.asInterface(iDownloadProgressListener.readStrongBinder());
            param1Int1 = addProgressListener(downloadRequest, iDownloadProgressListener);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 6:
            iDownloadProgressListener.enforceInterface("android.telephony.mbms.vendor.IMbmsDownloadService");
            if (iDownloadProgressListener.readInt() != 0) {
              downloadRequest = (DownloadRequest)DownloadRequest.CREATOR.createFromParcel((Parcel)iDownloadProgressListener);
            } else {
              downloadRequest = null;
            } 
            iDownloadStatusListener = IDownloadStatusListener.Stub.asInterface(iDownloadProgressListener.readStrongBinder());
            param1Int1 = removeStatusListener(downloadRequest, iDownloadStatusListener);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 5:
            iDownloadStatusListener.enforceInterface("android.telephony.mbms.vendor.IMbmsDownloadService");
            if (iDownloadStatusListener.readInt() != 0) {
              downloadRequest = (DownloadRequest)DownloadRequest.CREATOR.createFromParcel((Parcel)iDownloadStatusListener);
            } else {
              downloadRequest = null;
            } 
            iDownloadStatusListener = IDownloadStatusListener.Stub.asInterface(iDownloadStatusListener.readStrongBinder());
            param1Int1 = addStatusListener(downloadRequest, iDownloadStatusListener);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 4:
            iDownloadStatusListener.enforceInterface("android.telephony.mbms.vendor.IMbmsDownloadService");
            if (iDownloadStatusListener.readInt() != 0) {
              DownloadRequest downloadRequest1 = (DownloadRequest)DownloadRequest.CREATOR.createFromParcel((Parcel)iDownloadStatusListener);
            } else {
              iDownloadStatusListener = null;
            } 
            param1Int1 = download((DownloadRequest)iDownloadStatusListener);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 3:
            iDownloadStatusListener.enforceInterface("android.telephony.mbms.vendor.IMbmsDownloadService");
            param1Int1 = iDownloadStatusListener.readInt();
            str = iDownloadStatusListener.readString();
            param1Int1 = setTempFileRootDirectory(param1Int1, str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 2:
            str.enforceInterface("android.telephony.mbms.vendor.IMbmsDownloadService");
            param1Int1 = str.readInt();
            arrayList = str.createStringArrayList();
            param1Int1 = requestUpdateFileServices(param1Int1, arrayList);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 1:
            break;
        } 
        arrayList.enforceInterface("android.telephony.mbms.vendor.IMbmsDownloadService");
        param1Int1 = arrayList.readInt();
        IMbmsDownloadSessionCallback iMbmsDownloadSessionCallback = IMbmsDownloadSessionCallback.Stub.asInterface(arrayList.readStrongBinder());
        param1Int1 = initialize(param1Int1, iMbmsDownloadSessionCallback);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(param1Int1);
        return true;
      } 
      param1Parcel2.writeString("android.telephony.mbms.vendor.IMbmsDownloadService");
      return true;
    }
    
    private static class Proxy implements IMbmsDownloadService {
      public static IMbmsDownloadService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.mbms.vendor.IMbmsDownloadService";
      }
      
      public int initialize(int param2Int, IMbmsDownloadSessionCallback param2IMbmsDownloadSessionCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsDownloadService");
          parcel1.writeInt(param2Int);
          if (param2IMbmsDownloadSessionCallback != null) {
            iBinder = param2IMbmsDownloadSessionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IMbmsDownloadService.Stub.getDefaultImpl() != null) {
            param2Int = IMbmsDownloadService.Stub.getDefaultImpl().initialize(param2Int, param2IMbmsDownloadSessionCallback);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int requestUpdateFileServices(int param2Int, List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsDownloadService");
          parcel1.writeInt(param2Int);
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IMbmsDownloadService.Stub.getDefaultImpl() != null) {
            param2Int = IMbmsDownloadService.Stub.getDefaultImpl().requestUpdateFileServices(param2Int, param2List);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setTempFileRootDirectory(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsDownloadService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IMbmsDownloadService.Stub.getDefaultImpl() != null) {
            param2Int = IMbmsDownloadService.Stub.getDefaultImpl().setTempFileRootDirectory(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int download(DownloadRequest param2DownloadRequest) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsDownloadService");
          if (param2DownloadRequest != null) {
            parcel1.writeInt(1);
            param2DownloadRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IMbmsDownloadService.Stub.getDefaultImpl() != null)
            return IMbmsDownloadService.Stub.getDefaultImpl().download(param2DownloadRequest); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int addStatusListener(DownloadRequest param2DownloadRequest, IDownloadStatusListener param2IDownloadStatusListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsDownloadService");
          if (param2DownloadRequest != null) {
            parcel1.writeInt(1);
            param2DownloadRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IDownloadStatusListener != null) {
            iBinder = param2IDownloadStatusListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IMbmsDownloadService.Stub.getDefaultImpl() != null)
            return IMbmsDownloadService.Stub.getDefaultImpl().addStatusListener(param2DownloadRequest, param2IDownloadStatusListener); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int removeStatusListener(DownloadRequest param2DownloadRequest, IDownloadStatusListener param2IDownloadStatusListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsDownloadService");
          if (param2DownloadRequest != null) {
            parcel1.writeInt(1);
            param2DownloadRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IDownloadStatusListener != null) {
            iBinder = param2IDownloadStatusListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IMbmsDownloadService.Stub.getDefaultImpl() != null)
            return IMbmsDownloadService.Stub.getDefaultImpl().removeStatusListener(param2DownloadRequest, param2IDownloadStatusListener); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int addProgressListener(DownloadRequest param2DownloadRequest, IDownloadProgressListener param2IDownloadProgressListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsDownloadService");
          if (param2DownloadRequest != null) {
            parcel1.writeInt(1);
            param2DownloadRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IDownloadProgressListener != null) {
            iBinder = param2IDownloadProgressListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IMbmsDownloadService.Stub.getDefaultImpl() != null)
            return IMbmsDownloadService.Stub.getDefaultImpl().addProgressListener(param2DownloadRequest, param2IDownloadProgressListener); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int removeProgressListener(DownloadRequest param2DownloadRequest, IDownloadProgressListener param2IDownloadProgressListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsDownloadService");
          if (param2DownloadRequest != null) {
            parcel1.writeInt(1);
            param2DownloadRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IDownloadProgressListener != null) {
            iBinder = param2IDownloadProgressListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IMbmsDownloadService.Stub.getDefaultImpl() != null)
            return IMbmsDownloadService.Stub.getDefaultImpl().removeProgressListener(param2DownloadRequest, param2IDownloadProgressListener); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<DownloadRequest> listPendingDownloads(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsDownloadService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IMbmsDownloadService.Stub.getDefaultImpl() != null)
            return IMbmsDownloadService.Stub.getDefaultImpl().listPendingDownloads(param2Int); 
          parcel2.readException();
          return parcel2.createTypedArrayList(DownloadRequest.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int cancelDownload(DownloadRequest param2DownloadRequest) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsDownloadService");
          if (param2DownloadRequest != null) {
            parcel1.writeInt(1);
            param2DownloadRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IMbmsDownloadService.Stub.getDefaultImpl() != null)
            return IMbmsDownloadService.Stub.getDefaultImpl().cancelDownload(param2DownloadRequest); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int requestDownloadState(DownloadRequest param2DownloadRequest, FileInfo param2FileInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsDownloadService");
          if (param2DownloadRequest != null) {
            parcel1.writeInt(1);
            param2DownloadRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2FileInfo != null) {
            parcel1.writeInt(1);
            param2FileInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IMbmsDownloadService.Stub.getDefaultImpl() != null)
            return IMbmsDownloadService.Stub.getDefaultImpl().requestDownloadState(param2DownloadRequest, param2FileInfo); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int resetDownloadKnowledge(DownloadRequest param2DownloadRequest) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsDownloadService");
          if (param2DownloadRequest != null) {
            parcel1.writeInt(1);
            param2DownloadRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IMbmsDownloadService.Stub.getDefaultImpl() != null)
            return IMbmsDownloadService.Stub.getDefaultImpl().resetDownloadKnowledge(param2DownloadRequest); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void dispose(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsDownloadService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IMbmsDownloadService.Stub.getDefaultImpl() != null) {
            IMbmsDownloadService.Stub.getDefaultImpl().dispose(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMbmsDownloadService param1IMbmsDownloadService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMbmsDownloadService != null) {
          Proxy.sDefaultImpl = param1IMbmsDownloadService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMbmsDownloadService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
