package android.telephony.mbms.vendor;

import android.annotation.SystemApi;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.mbms.IMbmsStreamingSessionCallback;
import android.telephony.mbms.IStreamingServiceCallback;
import android.telephony.mbms.MbmsStreamingSessionCallback;
import android.telephony.mbms.StreamingServiceCallback;
import java.util.List;

@SystemApi
public class MbmsStreamingServiceBase extends IMbmsStreamingService.Stub {
  public int initialize(MbmsStreamingSessionCallback paramMbmsStreamingSessionCallback, int paramInt) throws RemoteException {
    return 0;
  }
  
  public final int initialize(IMbmsStreamingSessionCallback paramIMbmsStreamingSessionCallback, int paramInt) throws RemoteException {
    if (paramIMbmsStreamingSessionCallback != null) {
      int i = Binder.getCallingUid();
      int j = initialize((MbmsStreamingSessionCallback)new Object(this, paramIMbmsStreamingSessionCallback, i, paramInt), paramInt);
      if (j == 0)
        paramIMbmsStreamingSessionCallback.asBinder().linkToDeath((IBinder.DeathRecipient)new Object(this, i, paramInt), 0); 
      return j;
    } 
    throw new NullPointerException("Callback must not be null");
  }
  
  public int requestUpdateStreamingServices(int paramInt, List<String> paramList) throws RemoteException {
    return 0;
  }
  
  public int startStreaming(int paramInt, String paramString, StreamingServiceCallback paramStreamingServiceCallback) throws RemoteException {
    return 0;
  }
  
  public int startStreaming(int paramInt, String paramString, IStreamingServiceCallback paramIStreamingServiceCallback) throws RemoteException {
    if (paramIStreamingServiceCallback != null) {
      int i = Binder.getCallingUid();
      int j = startStreaming(paramInt, paramString, (StreamingServiceCallback)new Object(this, paramIStreamingServiceCallback, i, paramInt));
      if (j == 0)
        paramIStreamingServiceCallback.asBinder().linkToDeath((IBinder.DeathRecipient)new Object(this, i, paramInt), 0); 
      return j;
    } 
    throw new NullPointerException("Callback must not be null");
  }
  
  public Uri getPlaybackUri(int paramInt, String paramString) throws RemoteException {
    return null;
  }
  
  public void stopStreaming(int paramInt, String paramString) throws RemoteException {}
  
  public void dispose(int paramInt) throws RemoteException {}
  
  public void onAppCallbackDied(int paramInt1, int paramInt2) {}
  
  @SystemApi
  public IBinder asBinder() {
    return super.asBinder();
  }
  
  @SystemApi
  public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2) throws RemoteException {
    return super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
  }
}
