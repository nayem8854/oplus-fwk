package android.telephony.mbms.vendor;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.mbms.IMbmsStreamingSessionCallback;
import android.telephony.mbms.IStreamingServiceCallback;
import java.util.ArrayList;
import java.util.List;

public interface IMbmsStreamingService extends IInterface {
  void dispose(int paramInt) throws RemoteException;
  
  Uri getPlaybackUri(int paramInt, String paramString) throws RemoteException;
  
  int initialize(IMbmsStreamingSessionCallback paramIMbmsStreamingSessionCallback, int paramInt) throws RemoteException;
  
  int requestUpdateStreamingServices(int paramInt, List<String> paramList) throws RemoteException;
  
  int startStreaming(int paramInt, String paramString, IStreamingServiceCallback paramIStreamingServiceCallback) throws RemoteException;
  
  void stopStreaming(int paramInt, String paramString) throws RemoteException;
  
  class Default implements IMbmsStreamingService {
    public int initialize(IMbmsStreamingSessionCallback param1IMbmsStreamingSessionCallback, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int requestUpdateStreamingServices(int param1Int, List<String> param1List) throws RemoteException {
      return 0;
    }
    
    public int startStreaming(int param1Int, String param1String, IStreamingServiceCallback param1IStreamingServiceCallback) throws RemoteException {
      return 0;
    }
    
    public Uri getPlaybackUri(int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public void stopStreaming(int param1Int, String param1String) throws RemoteException {}
    
    public void dispose(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMbmsStreamingService {
    private static final String DESCRIPTOR = "android.telephony.mbms.vendor.IMbmsStreamingService";
    
    static final int TRANSACTION_dispose = 6;
    
    static final int TRANSACTION_getPlaybackUri = 4;
    
    static final int TRANSACTION_initialize = 1;
    
    static final int TRANSACTION_requestUpdateStreamingServices = 2;
    
    static final int TRANSACTION_startStreaming = 3;
    
    static final int TRANSACTION_stopStreaming = 5;
    
    public Stub() {
      attachInterface(this, "android.telephony.mbms.vendor.IMbmsStreamingService");
    }
    
    public static IMbmsStreamingService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.mbms.vendor.IMbmsStreamingService");
      if (iInterface != null && iInterface instanceof IMbmsStreamingService)
        return (IMbmsStreamingService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "dispose";
        case 5:
          return "stopStreaming";
        case 4:
          return "getPlaybackUri";
        case 3:
          return "startStreaming";
        case 2:
          return "requestUpdateStreamingServices";
        case 1:
          break;
      } 
      return "initialize";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String str1;
        Uri uri;
        IStreamingServiceCallback iStreamingServiceCallback;
        ArrayList<String> arrayList;
        String str2;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("android.telephony.mbms.vendor.IMbmsStreamingService");
            param1Int1 = param1Parcel1.readInt();
            dispose(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.telephony.mbms.vendor.IMbmsStreamingService");
            param1Int1 = param1Parcel1.readInt();
            str1 = param1Parcel1.readString();
            stopStreaming(param1Int1, str1);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str1.enforceInterface("android.telephony.mbms.vendor.IMbmsStreamingService");
            param1Int1 = str1.readInt();
            str1 = str1.readString();
            uri = getPlaybackUri(param1Int1, str1);
            param1Parcel2.writeNoException();
            if (uri != null) {
              param1Parcel2.writeInt(1);
              uri.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            uri.enforceInterface("android.telephony.mbms.vendor.IMbmsStreamingService");
            param1Int1 = uri.readInt();
            str2 = uri.readString();
            iStreamingServiceCallback = IStreamingServiceCallback.Stub.asInterface(uri.readStrongBinder());
            param1Int1 = startStreaming(param1Int1, str2, iStreamingServiceCallback);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 2:
            iStreamingServiceCallback.enforceInterface("android.telephony.mbms.vendor.IMbmsStreamingService");
            param1Int1 = iStreamingServiceCallback.readInt();
            arrayList = iStreamingServiceCallback.createStringArrayList();
            param1Int1 = requestUpdateStreamingServices(param1Int1, arrayList);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 1:
            break;
        } 
        arrayList.enforceInterface("android.telephony.mbms.vendor.IMbmsStreamingService");
        IMbmsStreamingSessionCallback iMbmsStreamingSessionCallback = IMbmsStreamingSessionCallback.Stub.asInterface(arrayList.readStrongBinder());
        param1Int1 = arrayList.readInt();
        param1Int1 = initialize(iMbmsStreamingSessionCallback, param1Int1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(param1Int1);
        return true;
      } 
      param1Parcel2.writeString("android.telephony.mbms.vendor.IMbmsStreamingService");
      return true;
    }
    
    private static class Proxy implements IMbmsStreamingService {
      public static IMbmsStreamingService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.mbms.vendor.IMbmsStreamingService";
      }
      
      public int initialize(IMbmsStreamingSessionCallback param2IMbmsStreamingSessionCallback, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsStreamingService");
          if (param2IMbmsStreamingSessionCallback != null) {
            iBinder = param2IMbmsStreamingSessionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IMbmsStreamingService.Stub.getDefaultImpl() != null) {
            param2Int = IMbmsStreamingService.Stub.getDefaultImpl().initialize(param2IMbmsStreamingSessionCallback, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int requestUpdateStreamingServices(int param2Int, List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsStreamingService");
          parcel1.writeInt(param2Int);
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IMbmsStreamingService.Stub.getDefaultImpl() != null) {
            param2Int = IMbmsStreamingService.Stub.getDefaultImpl().requestUpdateStreamingServices(param2Int, param2List);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int startStreaming(int param2Int, String param2String, IStreamingServiceCallback param2IStreamingServiceCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsStreamingService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          if (param2IStreamingServiceCallback != null) {
            iBinder = param2IStreamingServiceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IMbmsStreamingService.Stub.getDefaultImpl() != null) {
            param2Int = IMbmsStreamingService.Stub.getDefaultImpl().startStreaming(param2Int, param2String, param2IStreamingServiceCallback);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Uri getPlaybackUri(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsStreamingService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IMbmsStreamingService.Stub.getDefaultImpl() != null)
            return IMbmsStreamingService.Stub.getDefaultImpl().getPlaybackUri(param2Int, param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Uri uri = (Uri)Uri.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (Uri)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopStreaming(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsStreamingService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IMbmsStreamingService.Stub.getDefaultImpl() != null) {
            IMbmsStreamingService.Stub.getDefaultImpl().stopStreaming(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void dispose(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.mbms.vendor.IMbmsStreamingService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IMbmsStreamingService.Stub.getDefaultImpl() != null) {
            IMbmsStreamingService.Stub.getDefaultImpl().dispose(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMbmsStreamingService param1IMbmsStreamingService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMbmsStreamingService != null) {
          Proxy.sDefaultImpl = param1IMbmsStreamingService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMbmsStreamingService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
