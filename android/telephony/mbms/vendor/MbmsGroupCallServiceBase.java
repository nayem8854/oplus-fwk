package android.telephony.mbms.vendor;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.telephony.mbms.GroupCallCallback;
import android.telephony.mbms.IGroupCallCallback;
import android.telephony.mbms.IMbmsGroupCallSessionCallback;
import android.telephony.mbms.MbmsGroupCallSessionCallback;
import java.util.List;

@SystemApi
public class MbmsGroupCallServiceBase extends Service {
  private final IBinder mInterface = (IBinder)new IMbmsGroupCallService.Stub() {
      final MbmsGroupCallServiceBase this$0;
      
      public int initialize(IMbmsGroupCallSessionCallback param1IMbmsGroupCallSessionCallback, int param1Int) throws RemoteException {
        if (param1IMbmsGroupCallSessionCallback != null) {
          int i = Binder.getCallingUid();
          int j = MbmsGroupCallServiceBase.this.initialize((MbmsGroupCallSessionCallback)new Object(this, param1IMbmsGroupCallSessionCallback, i, param1Int), param1Int);
          if (j == 0)
            param1IMbmsGroupCallSessionCallback.asBinder().linkToDeath((IBinder.DeathRecipient)new Object(this, i, param1Int), 0); 
          return j;
        } 
        throw new NullPointerException("Callback must not be null");
      }
      
      public void stopGroupCall(int param1Int, long param1Long) {
        MbmsGroupCallServiceBase.this.stopGroupCall(param1Int, param1Long);
      }
      
      public void updateGroupCall(int param1Int, long param1Long, List<Integer> param1List1, List<Integer> param1List2) {
        MbmsGroupCallServiceBase.this.updateGroupCall(param1Int, param1Long, param1List1, param1List2);
      }
      
      public int startGroupCall(int param1Int, long param1Long, List<Integer> param1List1, List<Integer> param1List2, IGroupCallCallback param1IGroupCallCallback) throws RemoteException {
        if (param1IGroupCallCallback != null) {
          int i = Binder.getCallingUid();
          int j = MbmsGroupCallServiceBase.this.startGroupCall(param1Int, param1Long, param1List1, param1List2, (GroupCallCallback)new Object(this, param1IGroupCallCallback, i, param1Int));
          if (j == 0)
            param1IGroupCallCallback.asBinder().linkToDeath((IBinder.DeathRecipient)new Object(this, i, param1Int), 0); 
          return j;
        } 
        throw new NullPointerException("Callback must not be null");
      }
      
      public void dispose(int param1Int) throws RemoteException {
        MbmsGroupCallServiceBase.this.dispose(param1Int);
      }
    };
  
  public int initialize(MbmsGroupCallSessionCallback paramMbmsGroupCallSessionCallback, int paramInt) throws RemoteException {
    throw new UnsupportedOperationException("Not implemented");
  }
  
  public int startGroupCall(int paramInt, long paramLong, List<Integer> paramList1, List<Integer> paramList2, GroupCallCallback paramGroupCallCallback) {
    throw new UnsupportedOperationException("Not implemented");
  }
  
  public void stopGroupCall(int paramInt, long paramLong) {
    throw new UnsupportedOperationException("Not implemented");
  }
  
  public void updateGroupCall(int paramInt, long paramLong, List<Integer> paramList1, List<Integer> paramList2) {
    throw new UnsupportedOperationException("Not implemented");
  }
  
  public void dispose(int paramInt) throws RemoteException {
    throw new UnsupportedOperationException("Not implemented");
  }
  
  public void onAppCallbackDied(int paramInt1, int paramInt2) {}
  
  public IBinder onBind(Intent paramIntent) {
    return this.mInterface;
  }
}
