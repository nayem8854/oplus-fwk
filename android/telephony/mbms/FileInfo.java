package android.telephony.mbms;

import android.annotation.SystemApi;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public final class FileInfo implements Parcelable {
  public static final Parcelable.Creator<FileInfo> CREATOR = new Parcelable.Creator<FileInfo>() {
      public FileInfo createFromParcel(Parcel param1Parcel) {
        return new FileInfo(param1Parcel);
      }
      
      public FileInfo[] newArray(int param1Int) {
        return new FileInfo[param1Int];
      }
    };
  
  private final String mimeType;
  
  private final Uri uri;
  
  @SystemApi
  public FileInfo(Uri paramUri, String paramString) {
    this.uri = paramUri;
    this.mimeType = paramString;
  }
  
  private FileInfo(Parcel paramParcel) {
    this.uri = (Uri)paramParcel.readParcelable(null);
    this.mimeType = paramParcel.readString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.uri, paramInt);
    paramParcel.writeString(this.mimeType);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public Uri getUri() {
    return this.uri;
  }
  
  public String getMimeType() {
    return this.mimeType;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    FileInfo fileInfo = (FileInfo)paramObject;
    if (Objects.equals(this.uri, fileInfo.uri)) {
      paramObject = this.mimeType;
      String str = fileInfo.mimeType;
      if (Objects.equals(paramObject, str))
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.uri, this.mimeType });
  }
}
