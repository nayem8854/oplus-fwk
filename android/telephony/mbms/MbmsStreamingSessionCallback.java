package android.telephony.mbms;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

public class MbmsStreamingSessionCallback {
  public void onError(int paramInt, String paramString) {}
  
  public void onStreamingServicesUpdated(List<StreamingServiceInfo> paramList) {}
  
  public void onMiddlewareReady() {}
  
  @Retention(RetentionPolicy.SOURCE)
  private static @interface StreamingError {}
}
