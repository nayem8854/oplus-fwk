package android.telephony.mbms;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IDownloadStatusListener extends IInterface {
  void onStatusUpdated(DownloadRequest paramDownloadRequest, FileInfo paramFileInfo, int paramInt) throws RemoteException;
  
  class Default implements IDownloadStatusListener {
    public void onStatusUpdated(DownloadRequest param1DownloadRequest, FileInfo param1FileInfo, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDownloadStatusListener {
    private static final String DESCRIPTOR = "android.telephony.mbms.IDownloadStatusListener";
    
    static final int TRANSACTION_onStatusUpdated = 1;
    
    public Stub() {
      attachInterface(this, "android.telephony.mbms.IDownloadStatusListener");
    }
    
    public static IDownloadStatusListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.telephony.mbms.IDownloadStatusListener");
      if (iInterface != null && iInterface instanceof IDownloadStatusListener)
        return (IDownloadStatusListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onStatusUpdated";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      DownloadRequest downloadRequest;
      FileInfo fileInfo;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.telephony.mbms.IDownloadStatusListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.telephony.mbms.IDownloadStatusListener");
      if (param1Parcel1.readInt() != 0) {
        downloadRequest = (DownloadRequest)DownloadRequest.CREATOR.createFromParcel(param1Parcel1);
      } else {
        downloadRequest = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        fileInfo = (FileInfo)FileInfo.CREATOR.createFromParcel(param1Parcel1);
      } else {
        fileInfo = null;
      } 
      param1Int1 = param1Parcel1.readInt();
      onStatusUpdated(downloadRequest, fileInfo, param1Int1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    class Proxy implements IDownloadStatusListener {
      public static IDownloadStatusListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IDownloadStatusListener.Stub this$0) {
        this.mRemote = (IBinder)this$0;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.telephony.mbms.IDownloadStatusListener";
      }
      
      public void onStatusUpdated(DownloadRequest param2DownloadRequest, FileInfo param2FileInfo, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.telephony.mbms.IDownloadStatusListener");
          if (param2DownloadRequest != null) {
            parcel1.writeInt(1);
            param2DownloadRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2FileInfo != null) {
            parcel1.writeInt(1);
            param2FileInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IDownloadStatusListener.Stub.getDefaultImpl() != null) {
            IDownloadStatusListener.Stub.getDefaultImpl().onStatusUpdated(param2DownloadRequest, param2FileInfo, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDownloadStatusListener param1IDownloadStatusListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDownloadStatusListener != null) {
          Proxy.sDefaultImpl = param1IDownloadStatusListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDownloadStatusListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
