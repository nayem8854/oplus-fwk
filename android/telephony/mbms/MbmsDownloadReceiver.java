package android.telephony.mbms;

import android.annotation.SystemApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.FileAttribute;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class MbmsDownloadReceiver extends BroadcastReceiver {
  private static final boolean DEBUG = false;
  
  public static final String DOWNLOAD_TOKEN_SUFFIX = ".download_token";
  
  private static final String EMBMS_INTENT_PERMISSION = "android.permission.SEND_EMBMS_INTENTS";
  
  private static final String LOG_TAG = "MbmsDownloadReceiver";
  
  private static final int MAX_TEMP_FILE_RETRIES = 5;
  
  public static final String MBMS_FILE_PROVIDER_META_DATA_KEY = "mbms-file-provider-authority";
  
  @SystemApi
  public static final int RESULT_APP_NOTIFICATION_ERROR = 6;
  
  @SystemApi
  public static final int RESULT_BAD_TEMP_FILE_ROOT = 3;
  
  @SystemApi
  public static final int RESULT_DOWNLOAD_FINALIZATION_ERROR = 4;
  
  @SystemApi
  public static final int RESULT_INVALID_ACTION = 1;
  
  @SystemApi
  public static final int RESULT_MALFORMED_INTENT = 2;
  
  @SystemApi
  public static final int RESULT_OK = 0;
  
  @SystemApi
  public static final int RESULT_TEMP_FILE_GENERATION_ERROR = 5;
  
  private static final String TEMP_FILE_STAGING_LOCATION = "staged_completed_files";
  
  private static final String TEMP_FILE_SUFFIX = ".embms.temp";
  
  private String mFileProviderAuthorityCache = null;
  
  private String mMiddlewarePackageNameCache = null;
  
  public void onReceive(Context paramContext, Intent paramIntent) {
    verifyPermissionIntegrity(paramContext);
    if (!verifyIntentContents(paramContext, paramIntent)) {
      setResultCode(2);
      return;
    } 
    String str1 = paramIntent.getStringExtra("android.telephony.mbms.extra.TEMP_FILE_ROOT");
    String str2 = MbmsTempFileProvider.getEmbmsTempFileDir(paramContext).getPath();
    if (!Objects.equals(str1, str2)) {
      setResultCode(3);
      return;
    } 
    if ("android.telephony.mbms.action.DOWNLOAD_RESULT_INTERNAL".equals(paramIntent.getAction())) {
      moveDownloadedFile(paramContext, paramIntent);
      cleanupPostMove(paramContext, paramIntent);
    } else if ("android.telephony.mbms.action.FILE_DESCRIPTOR_REQUEST".equals(paramIntent.getAction())) {
      generateTempFiles(paramContext, paramIntent);
    } else if ("android.telephony.mbms.action.CLEANUP".equals(paramIntent.getAction())) {
      cleanupTempFiles(paramContext, paramIntent);
    } else {
      setResultCode(1);
    } 
  }
  
  private boolean verifyIntentContents(Context paramContext, Intent paramIntent) {
    File file;
    if ("android.telephony.mbms.action.DOWNLOAD_RESULT_INTERNAL".equals(paramIntent.getAction())) {
      if (!paramIntent.hasExtra("android.telephony.extra.MBMS_DOWNLOAD_RESULT")) {
        Log.w("MbmsDownloadReceiver", "Download result did not include a result code. Ignoring.");
        return false;
      } 
      if (!paramIntent.hasExtra("android.telephony.extra.MBMS_DOWNLOAD_REQUEST")) {
        Log.w("MbmsDownloadReceiver", "Download result did not include the associated request. Ignoring.");
        return false;
      } 
      if (1 != paramIntent.getIntExtra("android.telephony.extra.MBMS_DOWNLOAD_RESULT", 2))
        return true; 
      if (!paramIntent.hasExtra("android.telephony.mbms.extra.TEMP_FILE_ROOT")) {
        Log.w("MbmsDownloadReceiver", "Download result did not include the temp file root. Ignoring.");
        return false;
      } 
      if (!paramIntent.hasExtra("android.telephony.extra.MBMS_FILE_INFO")) {
        Log.w("MbmsDownloadReceiver", "Download result did not include the associated file info. Ignoring.");
        return false;
      } 
      if (!paramIntent.hasExtra("android.telephony.mbms.extra.FINAL_URI")) {
        Log.w("MbmsDownloadReceiver", "Download result did not include the path to the final temp file. Ignoring.");
        return false;
      } 
      DownloadRequest downloadRequest = (DownloadRequest)paramIntent.getParcelableExtra("android.telephony.extra.MBMS_DOWNLOAD_REQUEST");
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(downloadRequest.getHash());
      stringBuilder.append(".download_token");
      String str = stringBuilder.toString();
      file = new File(MbmsUtils.getEmbmsTempFileDirForService(paramContext, downloadRequest.getFileServiceId()), str);
      if (!file.exists()) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Supplied download request does not match a token that we have. Expected ");
        stringBuilder1.append(file);
        Log.w("MbmsDownloadReceiver", stringBuilder1.toString());
        return false;
      } 
    } else if ("android.telephony.mbms.action.FILE_DESCRIPTOR_REQUEST".equals(file.getAction())) {
      if (!file.hasExtra("android.telephony.mbms.extra.SERVICE_ID")) {
        Log.w("MbmsDownloadReceiver", "Temp file request did not include the associated service id. Ignoring.");
        return false;
      } 
      if (!file.hasExtra("android.telephony.mbms.extra.TEMP_FILE_ROOT")) {
        Log.w("MbmsDownloadReceiver", "Download result did not include the temp file root. Ignoring.");
        return false;
      } 
    } else if ("android.telephony.mbms.action.CLEANUP".equals(file.getAction())) {
      if (!file.hasExtra("android.telephony.mbms.extra.SERVICE_ID")) {
        Log.w("MbmsDownloadReceiver", "Cleanup request did not include the associated service id. Ignoring.");
        return false;
      } 
      if (!file.hasExtra("android.telephony.mbms.extra.TEMP_FILE_ROOT")) {
        Log.w("MbmsDownloadReceiver", "Cleanup request did not include the temp file root. Ignoring.");
        return false;
      } 
      if (!file.hasExtra("android.telephony.mbms.extra.TEMP_FILES_IN_USE")) {
        Log.w("MbmsDownloadReceiver", "Cleanup request did not include the list of temp files in use. Ignoring.");
        return false;
      } 
    } 
    return true;
  }
  
  private void moveDownloadedFile(Context paramContext, Intent paramIntent) {
    StringBuilder stringBuilder;
    DownloadRequest downloadRequest = (DownloadRequest)paramIntent.getParcelableExtra("android.telephony.extra.MBMS_DOWNLOAD_REQUEST");
    Intent intent = downloadRequest.getIntentForApp();
    if (intent == null) {
      Log.i("MbmsDownloadReceiver", "Malformed app notification intent");
      setResultCode(6);
      return;
    } 
    int i = paramIntent.getIntExtra("android.telephony.extra.MBMS_DOWNLOAD_RESULT", 2);
    intent.putExtra("android.telephony.extra.MBMS_DOWNLOAD_RESULT", i);
    intent.putExtra("android.telephony.extra.MBMS_DOWNLOAD_REQUEST", downloadRequest);
    if (i != 1) {
      Log.i("MbmsDownloadReceiver", "Download request indicated a failed download. Aborting.");
      paramContext.sendBroadcast(intent);
      setResultCode(0);
      return;
    } 
    Uri uri = (Uri)paramIntent.getParcelableExtra("android.telephony.mbms.extra.FINAL_URI");
    if (!verifyTempFilePath(paramContext, downloadRequest.getFileServiceId(), uri)) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Download result specified an invalid temp file ");
      stringBuilder.append(uri);
      Log.w("MbmsDownloadReceiver", stringBuilder.toString());
      setResultCode(4);
      return;
    } 
    FileInfo fileInfo = (FileInfo)paramIntent.getParcelableExtra("android.telephony.extra.MBMS_FILE_INFO");
    FileSystem fileSystem = FileSystems.getDefault();
    String str = downloadRequest.getDestinationUri().getPath();
    Path path = fileSystem.getPath(str, new String[0]);
    try {
      str = downloadRequest.getSourceUri().getPath();
      String str1 = fileInfo.getUri().getPath();
      str1 = getFileRelativePath(str, str1);
      uri = moveToFinalLocation(uri, path, str1);
      intent.putExtra("android.telephony.extra.MBMS_COMPLETED_FILE_URI", (Parcelable)uri);
      intent.putExtra("android.telephony.extra.MBMS_FILE_INFO", fileInfo);
      stringBuilder.sendBroadcast(intent);
      setResultCode(0);
      return;
    } catch (IOException iOException) {
      Log.w("MbmsDownloadReceiver", "Failed to move temp file to final destination");
      setResultCode(4);
      return;
    } 
  }
  
  private void cleanupPostMove(Context paramContext, Intent paramIntent) {
    DownloadRequest downloadRequest = (DownloadRequest)paramIntent.getParcelableExtra("android.telephony.extra.MBMS_DOWNLOAD_REQUEST");
    if (downloadRequest == null) {
      Log.w("MbmsDownloadReceiver", "Intent does not include a DownloadRequest. Ignoring.");
      return;
    } 
    ArrayList arrayList = paramIntent.getParcelableArrayListExtra("android.telephony.mbms.extra.TEMP_LIST");
    if (arrayList == null)
      return; 
    for (Uri uri : arrayList) {
      if (verifyTempFilePath(paramContext, downloadRequest.getFileServiceId(), uri)) {
        File file = new File(uri.getSchemeSpecificPart());
        if (!file.delete()) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Failed to delete temp file at ");
          stringBuilder.append(file.getPath());
          Log.w("MbmsDownloadReceiver", stringBuilder.toString());
        } 
      } 
    } 
  }
  
  private void generateTempFiles(Context paramContext, Intent paramIntent) {
    String str = paramIntent.getStringExtra("android.telephony.mbms.extra.SERVICE_ID");
    if (str == null) {
      Log.w("MbmsDownloadReceiver", "Temp file request did not include the associated service id. Ignoring.");
      setResultCode(2);
      return;
    } 
    int i = paramIntent.getIntExtra("android.telephony.mbms.extra.FD_COUNT", 0);
    ArrayList<Uri> arrayList = paramIntent.getParcelableArrayListExtra("android.telephony.mbms.extra.PAUSED_LIST");
    if (i == 0 && (arrayList == null || arrayList.size() == 0)) {
      Log.i("MbmsDownloadReceiver", "No temp files actually requested. Ending.");
      setResultCode(0);
      setResultExtras(Bundle.EMPTY);
      return;
    } 
    ArrayList<UriPathPair> arrayList1 = generateFreshTempFiles(paramContext, str, i);
    ArrayList<UriPathPair> arrayList2 = generateUrisForPausedFiles(paramContext, str, arrayList);
    Bundle bundle = new Bundle();
    bundle.putParcelableArrayList("android.telephony.mbms.extra.FREE_URI_LIST", arrayList1);
    bundle.putParcelableArrayList("android.telephony.mbms.extra.PAUSED_URI_LIST", arrayList2);
    setResultCode(0);
    setResultExtras(bundle);
  }
  
  private ArrayList<UriPathPair> generateFreshTempFiles(Context paramContext, String paramString, int paramInt) {
    File file = MbmsUtils.getEmbmsTempFileDirForService(paramContext, paramString);
    if (!file.exists())
      file.mkdirs(); 
    ArrayList<UriPathPair> arrayList = new ArrayList(paramInt);
    for (byte b = 0; b < paramInt; b++) {
      File file1 = generateSingleTempFile(file);
      if (file1 == null) {
        setResultCode(5);
        Log.w("MbmsDownloadReceiver", "Failed to generate a temp file. Moving on.");
      } else {
        Uri uri1 = Uri.fromFile(file1);
        String str = getFileProviderAuthorityCached(paramContext);
        Uri uri2 = MbmsTempFileProvider.getUriForFile(paramContext, str, file1);
        paramContext.grantUriPermission(getMiddlewarePackageCached(paramContext), uri2, 3);
        arrayList.add(new UriPathPair(uri1, uri2));
      } 
    } 
    return arrayList;
  }
  
  private static File generateSingleTempFile(File paramFile) {
    byte b = 0;
    while (b < 5) {
      b++;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(UUID.randomUUID());
      stringBuilder.append(".embms.temp");
      String str = stringBuilder.toString();
      File file = new File(paramFile, str);
      try {
        if (file.createNewFile())
          return file.getCanonicalFile(); 
      } catch (IOException iOException) {}
    } 
    return null;
  }
  
  private ArrayList<UriPathPair> generateUrisForPausedFiles(Context paramContext, String paramString, List<Uri> paramList) {
    if (paramList == null)
      return new ArrayList<>(0); 
    ArrayList<UriPathPair> arrayList = new ArrayList(paramList.size());
    for (Uri uri1 : paramList) {
      StringBuilder stringBuilder;
      if (!verifyTempFilePath(paramContext, paramString, uri1)) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Supplied file ");
        stringBuilder.append(uri1);
        stringBuilder.append(" is not a valid temp file to resume");
        Log.w("MbmsDownloadReceiver", stringBuilder.toString());
        setResultCode(5);
        continue;
      } 
      File file = new File(uri1.getSchemeSpecificPart());
      if (!file.exists()) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Supplied file ");
        stringBuilder.append(uri1);
        stringBuilder.append(" does not exist.");
        Log.w("MbmsDownloadReceiver", stringBuilder.toString());
        setResultCode(5);
        continue;
      } 
      String str = getFileProviderAuthorityCached(paramContext);
      Uri uri2 = MbmsTempFileProvider.getUriForFile(paramContext, str, (File)stringBuilder);
      paramContext.grantUriPermission(getMiddlewarePackageCached(paramContext), uri2, 3);
      arrayList.add(new UriPathPair(uri1, uri2));
    } 
    return arrayList;
  }
  
  private void cleanupTempFiles(Context paramContext, Intent paramIntent) {
    String str = paramIntent.getStringExtra("android.telephony.mbms.extra.SERVICE_ID");
    File file = MbmsUtils.getEmbmsTempFileDirForService(paramContext, str);
    ArrayList arrayList = paramIntent.getParcelableArrayListExtra("android.telephony.mbms.extra.TEMP_FILES_IN_USE");
    for (File file1 : file.listFiles((FileFilter)new Object(this, arrayList)))
      file1.delete(); 
  }
  
  private static Uri moveToFinalLocation(Uri paramUri, Path paramPath, String paramString) throws IOException {
    StringBuilder stringBuilder;
    if (!"file".equals(paramUri.getScheme())) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Downloaded file location uri ");
      stringBuilder.append(paramUri);
      stringBuilder.append(" does not have a file scheme");
      Log.w("MbmsDownloadReceiver", stringBuilder.toString());
      return null;
    } 
    Path path1 = FileSystems.getDefault().getPath(paramUri.getPath(), new String[0]);
    Path path2 = stringBuilder.resolve(paramString);
    if (!Files.isDirectory(path2.getParent(), new java.nio.file.LinkOption[0]))
      Files.createDirectories(path2.getParent(), (FileAttribute<?>[])new FileAttribute[0]); 
    path1 = Files.move(path1, path2, new CopyOption[] { StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.ATOMIC_MOVE });
    return Uri.fromFile(path1.toFile());
  }
  
  public static String getFileRelativePath(String paramString1, String paramString2) {
    String str = paramString1;
    if (paramString1.endsWith("*")) {
      int i = paramString1.lastIndexOf('/');
      str = paramString1.substring(0, i);
    } 
    if (!paramString2.startsWith(str))
      return null; 
    if (paramString2.length() == str.length())
      return str.substring(str.lastIndexOf('/') + 1); 
    paramString2 = paramString2.substring(str.length());
    paramString1 = paramString2;
    if (paramString2.startsWith("/"))
      paramString1 = paramString2.substring(1); 
    return paramString1;
  }
  
  private static boolean verifyTempFilePath(Context paramContext, String paramString, Uri paramUri) {
    StringBuilder stringBuilder;
    if (!"file".equals(paramUri.getScheme())) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Uri ");
      stringBuilder.append(paramUri);
      stringBuilder.append(" does not have a file scheme");
      Log.w("MbmsDownloadReceiver", stringBuilder.toString());
      return false;
    } 
    String str = paramUri.getSchemeSpecificPart();
    File file1 = new File(str);
    if (!file1.exists()) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("File at ");
      stringBuilder.append(str);
      stringBuilder.append(" does not exist.");
      Log.w("MbmsDownloadReceiver", stringBuilder.toString());
      return false;
    } 
    File file2 = MbmsUtils.getEmbmsTempFileDirForService((Context)stringBuilder, paramString);
    if (!MbmsUtils.isContainedIn(file2, file1)) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("File at ");
      stringBuilder1.append(str);
      stringBuilder1.append(" is not contained in the temp file root, which is ");
      stringBuilder1.append(MbmsUtils.getEmbmsTempFileDirForService((Context)stringBuilder, paramString));
      String str1 = stringBuilder1.toString();
      Log.w("MbmsDownloadReceiver", str1);
      return false;
    } 
    return true;
  }
  
  private String getFileProviderAuthorityCached(Context paramContext) {
    String str2 = this.mFileProviderAuthorityCache;
    if (str2 != null)
      return str2; 
    String str1 = getFileProviderAuthority(paramContext);
    return str1;
  }
  
  private static String getFileProviderAuthority(Context paramContext) {
    String str;
    try {
      PackageManager packageManager = paramContext.getPackageManager();
      ApplicationInfo applicationInfo = packageManager.getApplicationInfo(paramContext.getPackageName(), 128);
      if (applicationInfo.metaData != null) {
        str = applicationInfo.metaData.getString("mbms-file-provider-authority");
        if (str != null)
          return str; 
        throw new RuntimeException("App must declare the file provider authority as metadata in the manifest.");
      } 
      throw new RuntimeException("App must declare the file provider authority as metadata in the manifest.");
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Package manager couldn't find ");
      stringBuilder.append(str.getPackageName());
      throw new RuntimeException(stringBuilder.toString());
    } 
  }
  
  private String getMiddlewarePackageCached(Context paramContext) {
    if (this.mMiddlewarePackageNameCache == null)
      this.mMiddlewarePackageNameCache = (MbmsUtils.getMiddlewareServiceInfo(paramContext, "android.telephony.action.EmbmsDownload")).packageName; 
    return this.mMiddlewarePackageNameCache;
  }
  
  private void verifyPermissionIntegrity(Context paramContext) {
    PackageManager packageManager = paramContext.getPackageManager();
    Intent intent = new Intent(paramContext, MbmsDownloadReceiver.class);
    List list = packageManager.queryBroadcastReceivers(intent, 0);
    if (list.size() == 1) {
      ActivityInfo activityInfo = ((ResolveInfo)list.get(0)).activityInfo;
      if (activityInfo != null) {
        if (MbmsUtils.getOverrideServiceName(paramContext, "android.telephony.action.EmbmsDownload") != null) {
          if (activityInfo.permission != null)
            return; 
          throw new IllegalStateException("MbmsDownloadReceiver must require some permission");
        } 
        if (Objects.equals("android.permission.SEND_EMBMS_INTENTS", activityInfo.permission))
          return; 
        throw new IllegalStateException("MbmsDownloadReceiver must require the SEND_EMBMS_INTENTS permission.");
      } 
      throw new IllegalStateException("Queried ResolveInfo does not contain a receiver");
    } 
    throw new IllegalStateException("Non-unique download receiver in your app");
  }
}
