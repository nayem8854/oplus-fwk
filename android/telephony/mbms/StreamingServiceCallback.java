package android.telephony.mbms;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class StreamingServiceCallback {
  public static final int SIGNAL_STRENGTH_UNAVAILABLE = -1;
  
  public void onError(int paramInt, String paramString) {}
  
  public void onStreamStateUpdated(int paramInt1, int paramInt2) {}
  
  public void onMediaDescriptionUpdated() {}
  
  public void onBroadcastSignalStrengthUpdated(int paramInt) {}
  
  public void onStreamMethodUpdated(int paramInt) {}
  
  @Retention(RetentionPolicy.SOURCE)
  private static @interface StreamingServiceError {}
}
