package android.telephony.mbms;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public interface GroupCallCallback {
  public static final int SIGNAL_STRENGTH_UNAVAILABLE = -1;
  
  default void onError(int paramInt, String paramString) {}
  
  default void onGroupCallStateChanged(int paramInt1, int paramInt2) {}
  
  default void onBroadcastSignalStrengthUpdated(int paramInt) {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface GroupCallError {}
}
