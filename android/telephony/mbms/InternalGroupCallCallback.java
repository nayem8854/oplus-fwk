package android.telephony.mbms;

import android.os.Binder;
import java.util.concurrent.Executor;

public class InternalGroupCallCallback extends IGroupCallCallback.Stub {
  private final GroupCallCallback mAppCallback;
  
  private final Executor mExecutor;
  
  private volatile boolean mIsStopped = false;
  
  public InternalGroupCallCallback(GroupCallCallback paramGroupCallCallback, Executor paramExecutor) {
    this.mAppCallback = paramGroupCallCallback;
    this.mExecutor = paramExecutor;
  }
  
  public void onError(int paramInt, String paramString) {
    if (this.mIsStopped)
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mExecutor;
      Object object = new Object();
      super(this, paramInt, paramString);
      executor.execute((Runnable)object);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void onGroupCallStateChanged(int paramInt1, int paramInt2) {
    if (this.mIsStopped)
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mExecutor;
      Object object = new Object();
      super(this, paramInt1, paramInt2);
      executor.execute((Runnable)object);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void onBroadcastSignalStrengthUpdated(int paramInt) {
    if (this.mIsStopped)
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mExecutor;
      Object object = new Object();
      super(this, paramInt);
      executor.execute((Runnable)object);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void stop() {
    this.mIsStopped = true;
  }
}
