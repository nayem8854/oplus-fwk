package android.service.textclassifier;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Parcelable;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Slog;
import android.view.textclassifier.ConversationActions;
import android.view.textclassifier.SelectionEvent;
import android.view.textclassifier.TextClassification;
import android.view.textclassifier.TextClassificationContext;
import android.view.textclassifier.TextClassificationManager;
import android.view.textclassifier.TextClassificationSessionId;
import android.view.textclassifier.TextClassifier;
import android.view.textclassifier.TextClassifierEvent;
import android.view.textclassifier.TextLanguage;
import android.view.textclassifier.TextLinks;
import android.view.textclassifier.TextSelection;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SystemApi
public abstract class TextClassifierService extends Service {
  private final Handler mMainThreadHandler = new Handler(Looper.getMainLooper(), null, true);
  
  private final ExecutorService mSingleThreadExecutor = Executors.newSingleThreadExecutor();
  
  private final ITextClassifierService.Stub mBinder = new ITextClassifierService.Stub() {
      private final CancellationSignal mCancellationSignal = new CancellationSignal();
      
      final TextClassifierService this$0;
      
      public void onSuggestSelection(TextClassificationSessionId param1TextClassificationSessionId, TextSelection.Request param1Request, ITextClassifierCallback param1ITextClassifierCallback) {
        Preconditions.checkNotNull(param1Request);
        Preconditions.checkNotNull(param1ITextClassifierCallback);
        TextClassifierService.this.mMainThreadHandler.post(new _$$Lambda$TextClassifierService$1$mKOXH9oGuUFyRz_Oo15GnAPhABs(this, param1TextClassificationSessionId, param1Request, param1ITextClassifierCallback));
      }
      
      public void onClassifyText(TextClassificationSessionId param1TextClassificationSessionId, TextClassification.Request param1Request, ITextClassifierCallback param1ITextClassifierCallback) {
        Preconditions.checkNotNull(param1Request);
        Preconditions.checkNotNull(param1ITextClassifierCallback);
        TextClassifierService.this.mMainThreadHandler.post(new _$$Lambda$TextClassifierService$1$LziW7ahHkWlZlAFekrEQR96QofM(this, param1TextClassificationSessionId, param1Request, param1ITextClassifierCallback));
      }
      
      public void onGenerateLinks(TextClassificationSessionId param1TextClassificationSessionId, TextLinks.Request param1Request, ITextClassifierCallback param1ITextClassifierCallback) {
        Preconditions.checkNotNull(param1Request);
        Preconditions.checkNotNull(param1ITextClassifierCallback);
        TextClassifierService.this.mMainThreadHandler.post(new _$$Lambda$TextClassifierService$1$suS99xMAl9SLES4WhRmaub16wIc(this, param1TextClassificationSessionId, param1Request, param1ITextClassifierCallback));
      }
      
      public void onSelectionEvent(TextClassificationSessionId param1TextClassificationSessionId, SelectionEvent param1SelectionEvent) {
        Preconditions.checkNotNull(param1SelectionEvent);
        TextClassifierService.this.mMainThreadHandler.post(new _$$Lambda$TextClassifierService$1$_Nsl56ysLPoVPJ4Gu0VUwYCh4wE(this, param1TextClassificationSessionId, param1SelectionEvent));
      }
      
      public void onTextClassifierEvent(TextClassificationSessionId param1TextClassificationSessionId, TextClassifierEvent param1TextClassifierEvent) {
        Preconditions.checkNotNull(param1TextClassifierEvent);
        TextClassifierService.this.mMainThreadHandler.post(new _$$Lambda$TextClassifierService$1$bqy_LY0V0g3pGHWd_N7ARYwQWLY(this, param1TextClassificationSessionId, param1TextClassifierEvent));
      }
      
      public void onDetectLanguage(TextClassificationSessionId param1TextClassificationSessionId, TextLanguage.Request param1Request, ITextClassifierCallback param1ITextClassifierCallback) {
        Preconditions.checkNotNull(param1Request);
        Preconditions.checkNotNull(param1ITextClassifierCallback);
        TextClassifierService.this.mMainThreadHandler.post(new _$$Lambda$TextClassifierService$1$lcpBFMoy_hRkYQ42cWViBMbNnMk(this, param1TextClassificationSessionId, param1Request, param1ITextClassifierCallback));
      }
      
      public void onSuggestConversationActions(TextClassificationSessionId param1TextClassificationSessionId, ConversationActions.Request param1Request, ITextClassifierCallback param1ITextClassifierCallback) {
        Preconditions.checkNotNull(param1Request);
        Preconditions.checkNotNull(param1ITextClassifierCallback);
        TextClassifierService.this.mMainThreadHandler.post(new _$$Lambda$TextClassifierService$1$Xkudza2Bh6W4NodH1DO_FiRgfuM(this, param1TextClassificationSessionId, param1Request, param1ITextClassifierCallback));
      }
      
      public void onCreateTextClassificationSession(TextClassificationContext param1TextClassificationContext, TextClassificationSessionId param1TextClassificationSessionId) {
        Preconditions.checkNotNull(param1TextClassificationContext);
        Preconditions.checkNotNull(param1TextClassificationSessionId);
        TextClassifierService.this.mMainThreadHandler.post(new _$$Lambda$TextClassifierService$1$oecuM3n2XJWuEPg_O0hSZtoF0ls(this, param1TextClassificationContext, param1TextClassificationSessionId));
      }
      
      public void onDestroyTextClassificationSession(TextClassificationSessionId param1TextClassificationSessionId) {
        TextClassifierService.this.mMainThreadHandler.post(new _$$Lambda$TextClassifierService$1$fhIvecFpMXNthJWnvX_RvpNrPFA(this, param1TextClassificationSessionId));
      }
      
      public void onConnectedStateChanged(int param1Int) {
        _$$Lambda$7kiCIPNXdsOY6kUm9cdfxiUlQ3s _$$Lambda$7kiCIPNXdsOY6kUm9cdfxiUlQ3s;
        Handler handler = TextClassifierService.this.mMainThreadHandler;
        if (param1Int == 0) {
          _$$Lambda$uKcogIuTIdZyVutMMtkIe7_k8YE _$$Lambda$uKcogIuTIdZyVutMMtkIe7_k8YE = new _$$Lambda$uKcogIuTIdZyVutMMtkIe7_k8YE(TextClassifierService.this);
        } else {
          _$$Lambda$7kiCIPNXdsOY6kUm9cdfxiUlQ3s = new _$$Lambda$7kiCIPNXdsOY6kUm9cdfxiUlQ3s(TextClassifierService.this);
        } 
        handler.post(_$$Lambda$7kiCIPNXdsOY6kUm9cdfxiUlQ3s);
      }
    };
  
  public static final int CONNECTED = 0;
  
  public static final int DISCONNECTED = 1;
  
  private static final String KEY_RESULT = "key_result";
  
  private static final String LOG_TAG = "TextClassifierService";
  
  public static final String SERVICE_INTERFACE = "android.service.textclassifier.TextClassifierService";
  
  public final IBinder onBind(Intent paramIntent) {
    if ("android.service.textclassifier.TextClassifierService".equals(paramIntent.getAction()))
      return this.mBinder; 
    return null;
  }
  
  public boolean onUnbind(Intent paramIntent) {
    onDisconnected();
    return super.onUnbind(paramIntent);
  }
  
  public void onConnected() {}
  
  public void onDisconnected() {}
  
  public void onDetectLanguage(TextClassificationSessionId paramTextClassificationSessionId, TextLanguage.Request paramRequest, CancellationSignal paramCancellationSignal, Callback<TextLanguage> paramCallback) {
    this.mSingleThreadExecutor.submit(new _$$Lambda$TextClassifierService$9kfVuo6FJ1uQiU277_n9JgliEEc(this, paramCallback, paramRequest));
  }
  
  public void onSuggestConversationActions(TextClassificationSessionId paramTextClassificationSessionId, ConversationActions.Request paramRequest, CancellationSignal paramCancellationSignal, Callback<ConversationActions> paramCallback) {
    this.mSingleThreadExecutor.submit(new _$$Lambda$TextClassifierService$OMrgO9sL3mlBJfpfxbmg7ieGoWk(this, paramCallback, paramRequest));
  }
  
  @Deprecated
  public void onSelectionEvent(TextClassificationSessionId paramTextClassificationSessionId, SelectionEvent paramSelectionEvent) {}
  
  public void onTextClassifierEvent(TextClassificationSessionId paramTextClassificationSessionId, TextClassifierEvent paramTextClassifierEvent) {}
  
  public void onCreateTextClassificationSession(TextClassificationContext paramTextClassificationContext, TextClassificationSessionId paramTextClassificationSessionId) {}
  
  public void onDestroyTextClassificationSession(TextClassificationSessionId paramTextClassificationSessionId) {}
  
  @Deprecated
  public final TextClassifier getLocalTextClassifier() {
    return TextClassifier.NO_OP;
  }
  
  public static TextClassifier getDefaultTextClassifierImplementation(Context paramContext) {
    String str = paramContext.getPackageManager().getDefaultTextClassifierPackageName();
    if (TextUtils.isEmpty(str))
      return TextClassifier.NO_OP; 
    if (!str.equals(paramContext.getPackageName())) {
      TextClassificationManager textClassificationManager = (TextClassificationManager)paramContext.getSystemService(TextClassificationManager.class);
      return textClassificationManager.getTextClassifier(2);
    } 
    throw new RuntimeException("The default text classifier itself should not call thegetDefaultTextClassifierImplementation() method.");
  }
  
  public static <T extends Parcelable> T getResponse(Bundle paramBundle) {
    return paramBundle.getParcelable("key_result");
  }
  
  public static <T extends Parcelable> void putResponse(Bundle paramBundle, T paramT) {
    paramBundle.putParcelable("key_result", (Parcelable)paramT);
  }
  
  public static ComponentName getServiceComponentName(Context paramContext, String paramString, int paramInt) {
    Intent intent = (new Intent("android.service.textclassifier.TextClassifierService")).setPackage(paramString);
    ResolveInfo resolveInfo = paramContext.getPackageManager().resolveService(intent, paramInt);
    if (resolveInfo == null || resolveInfo.serviceInfo == null) {
      paramInt = paramContext.getUserId();
      Slog.w("TextClassifierService", String.format("Package or service not found in package %s for user %d", new Object[] { paramString, Integer.valueOf(paramInt) }));
      return null;
    } 
    ServiceInfo serviceInfo = resolveInfo.serviceInfo;
    paramString = serviceInfo.permission;
    if ("android.permission.BIND_TEXTCLASSIFIER_SERVICE".equals(paramString))
      return serviceInfo.getComponentName(); 
    ComponentName componentName = serviceInfo.getComponentName();
    String str = serviceInfo.permission;
    Slog.w("TextClassifierService", String.format("Service %s should require %s permission. Found %s permission", new Object[] { componentName, "android.permission.BIND_TEXTCLASSIFIER_SERVICE", str }));
    return null;
  }
  
  public abstract void onClassifyText(TextClassificationSessionId paramTextClassificationSessionId, TextClassification.Request paramRequest, CancellationSignal paramCancellationSignal, Callback<TextClassification> paramCallback);
  
  public abstract void onGenerateLinks(TextClassificationSessionId paramTextClassificationSessionId, TextLinks.Request paramRequest, CancellationSignal paramCancellationSignal, Callback<TextLinks> paramCallback);
  
  public abstract void onSuggestSelection(TextClassificationSessionId paramTextClassificationSessionId, TextSelection.Request paramRequest, CancellationSignal paramCancellationSignal, Callback<TextSelection> paramCallback);
  
  class Callback<T> {
    public abstract void onFailure(CharSequence param1CharSequence);
    
    public abstract void onSuccess(T param1T);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ConnectionState implements Annotation {}
  
  class ProxyCallback<T extends Parcelable> implements Callback<T> {
    private ITextClassifierCallback mTextClassifierCallback;
    
    private ProxyCallback(TextClassifierService this$0) {
      this.mTextClassifierCallback = (ITextClassifierCallback)Preconditions.checkNotNull(this$0);
    }
    
    public void onSuccess(T param1T) {
      try {
        Bundle bundle = new Bundle();
        this(1);
        bundle.putParcelable("key_result", (Parcelable)param1T);
        this.mTextClassifierCallback.onSuccess(bundle);
      } catch (RemoteException remoteException) {
        Slog.d("TextClassifierService", "Error calling callback");
      } 
    }
    
    public void onFailure(CharSequence param1CharSequence) {
      try {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Request fail: ");
        stringBuilder.append(param1CharSequence);
        Slog.w("TextClassifierService", stringBuilder.toString());
        this.mTextClassifierCallback.onFailure();
      } catch (RemoteException remoteException) {
        Slog.d("TextClassifierService", "Error calling callback");
      } 
    }
  }
}
