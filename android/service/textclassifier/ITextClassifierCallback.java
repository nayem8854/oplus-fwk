package android.service.textclassifier;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ITextClassifierCallback extends IInterface {
  void onFailure() throws RemoteException;
  
  void onSuccess(Bundle paramBundle) throws RemoteException;
  
  class Default implements ITextClassifierCallback {
    public void onSuccess(Bundle param1Bundle) throws RemoteException {}
    
    public void onFailure() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITextClassifierCallback {
    private static final String DESCRIPTOR = "android.service.textclassifier.ITextClassifierCallback";
    
    static final int TRANSACTION_onFailure = 2;
    
    static final int TRANSACTION_onSuccess = 1;
    
    public Stub() {
      attachInterface(this, "android.service.textclassifier.ITextClassifierCallback");
    }
    
    public static ITextClassifierCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.textclassifier.ITextClassifierCallback");
      if (iInterface != null && iInterface instanceof ITextClassifierCallback)
        return (ITextClassifierCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onFailure";
      } 
      return "onSuccess";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.service.textclassifier.ITextClassifierCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("android.service.textclassifier.ITextClassifierCallback");
        onFailure();
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.textclassifier.ITextClassifierCallback");
      if (param1Parcel1.readInt() != 0) {
        Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onSuccess((Bundle)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements ITextClassifierCallback {
      public static ITextClassifierCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.textclassifier.ITextClassifierCallback";
      }
      
      public void onSuccess(Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.textclassifier.ITextClassifierCallback");
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ITextClassifierCallback.Stub.getDefaultImpl() != null) {
            ITextClassifierCallback.Stub.getDefaultImpl().onSuccess(param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onFailure() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.textclassifier.ITextClassifierCallback");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ITextClassifierCallback.Stub.getDefaultImpl() != null) {
            ITextClassifierCallback.Stub.getDefaultImpl().onFailure();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITextClassifierCallback param1ITextClassifierCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITextClassifierCallback != null) {
          Proxy.sDefaultImpl = param1ITextClassifierCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITextClassifierCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
