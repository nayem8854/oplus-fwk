package android.service.textclassifier;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.textclassifier.ConversationActions;
import android.view.textclassifier.SelectionEvent;
import android.view.textclassifier.TextClassification;
import android.view.textclassifier.TextClassificationContext;
import android.view.textclassifier.TextClassificationSessionId;
import android.view.textclassifier.TextClassifierEvent;
import android.view.textclassifier.TextLanguage;
import android.view.textclassifier.TextLinks;
import android.view.textclassifier.TextSelection;

public interface ITextClassifierService extends IInterface {
  void onClassifyText(TextClassificationSessionId paramTextClassificationSessionId, TextClassification.Request paramRequest, ITextClassifierCallback paramITextClassifierCallback) throws RemoteException;
  
  void onConnectedStateChanged(int paramInt) throws RemoteException;
  
  void onCreateTextClassificationSession(TextClassificationContext paramTextClassificationContext, TextClassificationSessionId paramTextClassificationSessionId) throws RemoteException;
  
  void onDestroyTextClassificationSession(TextClassificationSessionId paramTextClassificationSessionId) throws RemoteException;
  
  void onDetectLanguage(TextClassificationSessionId paramTextClassificationSessionId, TextLanguage.Request paramRequest, ITextClassifierCallback paramITextClassifierCallback) throws RemoteException;
  
  void onGenerateLinks(TextClassificationSessionId paramTextClassificationSessionId, TextLinks.Request paramRequest, ITextClassifierCallback paramITextClassifierCallback) throws RemoteException;
  
  void onSelectionEvent(TextClassificationSessionId paramTextClassificationSessionId, SelectionEvent paramSelectionEvent) throws RemoteException;
  
  void onSuggestConversationActions(TextClassificationSessionId paramTextClassificationSessionId, ConversationActions.Request paramRequest, ITextClassifierCallback paramITextClassifierCallback) throws RemoteException;
  
  void onSuggestSelection(TextClassificationSessionId paramTextClassificationSessionId, TextSelection.Request paramRequest, ITextClassifierCallback paramITextClassifierCallback) throws RemoteException;
  
  void onTextClassifierEvent(TextClassificationSessionId paramTextClassificationSessionId, TextClassifierEvent paramTextClassifierEvent) throws RemoteException;
  
  class Default implements ITextClassifierService {
    public void onSuggestSelection(TextClassificationSessionId param1TextClassificationSessionId, TextSelection.Request param1Request, ITextClassifierCallback param1ITextClassifierCallback) throws RemoteException {}
    
    public void onClassifyText(TextClassificationSessionId param1TextClassificationSessionId, TextClassification.Request param1Request, ITextClassifierCallback param1ITextClassifierCallback) throws RemoteException {}
    
    public void onGenerateLinks(TextClassificationSessionId param1TextClassificationSessionId, TextLinks.Request param1Request, ITextClassifierCallback param1ITextClassifierCallback) throws RemoteException {}
    
    public void onSelectionEvent(TextClassificationSessionId param1TextClassificationSessionId, SelectionEvent param1SelectionEvent) throws RemoteException {}
    
    public void onTextClassifierEvent(TextClassificationSessionId param1TextClassificationSessionId, TextClassifierEvent param1TextClassifierEvent) throws RemoteException {}
    
    public void onCreateTextClassificationSession(TextClassificationContext param1TextClassificationContext, TextClassificationSessionId param1TextClassificationSessionId) throws RemoteException {}
    
    public void onDestroyTextClassificationSession(TextClassificationSessionId param1TextClassificationSessionId) throws RemoteException {}
    
    public void onDetectLanguage(TextClassificationSessionId param1TextClassificationSessionId, TextLanguage.Request param1Request, ITextClassifierCallback param1ITextClassifierCallback) throws RemoteException {}
    
    public void onSuggestConversationActions(TextClassificationSessionId param1TextClassificationSessionId, ConversationActions.Request param1Request, ITextClassifierCallback param1ITextClassifierCallback) throws RemoteException {}
    
    public void onConnectedStateChanged(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITextClassifierService {
    private static final String DESCRIPTOR = "android.service.textclassifier.ITextClassifierService";
    
    static final int TRANSACTION_onClassifyText = 2;
    
    static final int TRANSACTION_onConnectedStateChanged = 10;
    
    static final int TRANSACTION_onCreateTextClassificationSession = 6;
    
    static final int TRANSACTION_onDestroyTextClassificationSession = 7;
    
    static final int TRANSACTION_onDetectLanguage = 8;
    
    static final int TRANSACTION_onGenerateLinks = 3;
    
    static final int TRANSACTION_onSelectionEvent = 4;
    
    static final int TRANSACTION_onSuggestConversationActions = 9;
    
    static final int TRANSACTION_onSuggestSelection = 1;
    
    static final int TRANSACTION_onTextClassifierEvent = 5;
    
    public Stub() {
      attachInterface(this, "android.service.textclassifier.ITextClassifierService");
    }
    
    public static ITextClassifierService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.textclassifier.ITextClassifierService");
      if (iInterface != null && iInterface instanceof ITextClassifierService)
        return (ITextClassifierService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 10:
          return "onConnectedStateChanged";
        case 9:
          return "onSuggestConversationActions";
        case 8:
          return "onDetectLanguage";
        case 7:
          return "onDestroyTextClassificationSession";
        case 6:
          return "onCreateTextClassificationSession";
        case 5:
          return "onTextClassifierEvent";
        case 4:
          return "onSelectionEvent";
        case 3:
          return "onGenerateLinks";
        case 2:
          return "onClassifyText";
        case 1:
          break;
      } 
      return "onSuggestSelection";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        ConversationActions.Request request;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 10:
            param1Parcel1.enforceInterface("android.service.textclassifier.ITextClassifierService");
            param1Int1 = param1Parcel1.readInt();
            onConnectedStateChanged(param1Int1);
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.service.textclassifier.ITextClassifierService");
            if (param1Parcel1.readInt() != 0) {
              TextClassificationSessionId textClassificationSessionId = TextClassificationSessionId.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              request = ConversationActions.Request.CREATOR.createFromParcel(param1Parcel1);
            } else {
              request = null;
            } 
            iTextClassifierCallback = ITextClassifierCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            onSuggestConversationActions((TextClassificationSessionId)param1Parcel2, request, iTextClassifierCallback);
            return true;
          case 8:
            iTextClassifierCallback.enforceInterface("android.service.textclassifier.ITextClassifierService");
            if (iTextClassifierCallback.readInt() != 0) {
              TextClassificationSessionId textClassificationSessionId = TextClassificationSessionId.CREATOR.createFromParcel((Parcel)iTextClassifierCallback);
            } else {
              param1Parcel2 = null;
            } 
            if (iTextClassifierCallback.readInt() != 0) {
              TextLanguage.Request request1 = TextLanguage.Request.CREATOR.createFromParcel((Parcel)iTextClassifierCallback);
            } else {
              request = null;
            } 
            iTextClassifierCallback = ITextClassifierCallback.Stub.asInterface(iTextClassifierCallback.readStrongBinder());
            onDetectLanguage((TextClassificationSessionId)param1Parcel2, (TextLanguage.Request)request, iTextClassifierCallback);
            return true;
          case 7:
            iTextClassifierCallback.enforceInterface("android.service.textclassifier.ITextClassifierService");
            if (iTextClassifierCallback.readInt() != 0) {
              TextClassificationSessionId textClassificationSessionId = TextClassificationSessionId.CREATOR.createFromParcel((Parcel)iTextClassifierCallback);
            } else {
              iTextClassifierCallback = null;
            } 
            onDestroyTextClassificationSession((TextClassificationSessionId)iTextClassifierCallback);
            return true;
          case 6:
            iTextClassifierCallback.enforceInterface("android.service.textclassifier.ITextClassifierService");
            if (iTextClassifierCallback.readInt() != 0) {
              TextClassificationContext textClassificationContext = TextClassificationContext.CREATOR.createFromParcel((Parcel)iTextClassifierCallback);
            } else {
              param1Parcel2 = null;
            } 
            if (iTextClassifierCallback.readInt() != 0) {
              TextClassificationSessionId textClassificationSessionId = TextClassificationSessionId.CREATOR.createFromParcel((Parcel)iTextClassifierCallback);
            } else {
              iTextClassifierCallback = null;
            } 
            onCreateTextClassificationSession((TextClassificationContext)param1Parcel2, (TextClassificationSessionId)iTextClassifierCallback);
            return true;
          case 5:
            iTextClassifierCallback.enforceInterface("android.service.textclassifier.ITextClassifierService");
            if (iTextClassifierCallback.readInt() != 0) {
              TextClassificationSessionId textClassificationSessionId = TextClassificationSessionId.CREATOR.createFromParcel((Parcel)iTextClassifierCallback);
            } else {
              param1Parcel2 = null;
            } 
            if (iTextClassifierCallback.readInt() != 0) {
              TextClassifierEvent textClassifierEvent = TextClassifierEvent.CREATOR.createFromParcel((Parcel)iTextClassifierCallback);
            } else {
              iTextClassifierCallback = null;
            } 
            onTextClassifierEvent((TextClassificationSessionId)param1Parcel2, (TextClassifierEvent)iTextClassifierCallback);
            return true;
          case 4:
            iTextClassifierCallback.enforceInterface("android.service.textclassifier.ITextClassifierService");
            if (iTextClassifierCallback.readInt() != 0) {
              TextClassificationSessionId textClassificationSessionId = TextClassificationSessionId.CREATOR.createFromParcel((Parcel)iTextClassifierCallback);
            } else {
              param1Parcel2 = null;
            } 
            if (iTextClassifierCallback.readInt() != 0) {
              SelectionEvent selectionEvent = SelectionEvent.CREATOR.createFromParcel((Parcel)iTextClassifierCallback);
            } else {
              iTextClassifierCallback = null;
            } 
            onSelectionEvent((TextClassificationSessionId)param1Parcel2, (SelectionEvent)iTextClassifierCallback);
            return true;
          case 3:
            iTextClassifierCallback.enforceInterface("android.service.textclassifier.ITextClassifierService");
            if (iTextClassifierCallback.readInt() != 0) {
              TextClassificationSessionId textClassificationSessionId = TextClassificationSessionId.CREATOR.createFromParcel((Parcel)iTextClassifierCallback);
            } else {
              param1Parcel2 = null;
            } 
            if (iTextClassifierCallback.readInt() != 0) {
              TextLinks.Request request1 = TextLinks.Request.CREATOR.createFromParcel((Parcel)iTextClassifierCallback);
            } else {
              request = null;
            } 
            iTextClassifierCallback = ITextClassifierCallback.Stub.asInterface(iTextClassifierCallback.readStrongBinder());
            onGenerateLinks((TextClassificationSessionId)param1Parcel2, (TextLinks.Request)request, iTextClassifierCallback);
            return true;
          case 2:
            iTextClassifierCallback.enforceInterface("android.service.textclassifier.ITextClassifierService");
            if (iTextClassifierCallback.readInt() != 0) {
              TextClassificationSessionId textClassificationSessionId = TextClassificationSessionId.CREATOR.createFromParcel((Parcel)iTextClassifierCallback);
            } else {
              param1Parcel2 = null;
            } 
            if (iTextClassifierCallback.readInt() != 0) {
              TextClassification.Request request1 = TextClassification.Request.CREATOR.createFromParcel((Parcel)iTextClassifierCallback);
            } else {
              request = null;
            } 
            iTextClassifierCallback = ITextClassifierCallback.Stub.asInterface(iTextClassifierCallback.readStrongBinder());
            onClassifyText((TextClassificationSessionId)param1Parcel2, (TextClassification.Request)request, iTextClassifierCallback);
            return true;
          case 1:
            break;
        } 
        iTextClassifierCallback.enforceInterface("android.service.textclassifier.ITextClassifierService");
        if (iTextClassifierCallback.readInt() != 0) {
          TextClassificationSessionId textClassificationSessionId = TextClassificationSessionId.CREATOR.createFromParcel((Parcel)iTextClassifierCallback);
        } else {
          param1Parcel2 = null;
        } 
        if (iTextClassifierCallback.readInt() != 0) {
          TextSelection.Request request1 = TextSelection.Request.CREATOR.createFromParcel((Parcel)iTextClassifierCallback);
        } else {
          request = null;
        } 
        ITextClassifierCallback iTextClassifierCallback = ITextClassifierCallback.Stub.asInterface(iTextClassifierCallback.readStrongBinder());
        onSuggestSelection((TextClassificationSessionId)param1Parcel2, (TextSelection.Request)request, iTextClassifierCallback);
        return true;
      } 
      param1Parcel2.writeString("android.service.textclassifier.ITextClassifierService");
      return true;
    }
    
    private static class Proxy implements ITextClassifierService {
      public static ITextClassifierService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.textclassifier.ITextClassifierService";
      }
      
      public void onSuggestSelection(TextClassificationSessionId param2TextClassificationSessionId, TextSelection.Request param2Request, ITextClassifierCallback param2ITextClassifierCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.textclassifier.ITextClassifierService");
          if (param2TextClassificationSessionId != null) {
            parcel.writeInt(1);
            param2TextClassificationSessionId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Request != null) {
            parcel.writeInt(1);
            param2Request.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ITextClassifierCallback != null) {
            iBinder = param2ITextClassifierCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ITextClassifierService.Stub.getDefaultImpl() != null) {
            ITextClassifierService.Stub.getDefaultImpl().onSuggestSelection(param2TextClassificationSessionId, param2Request, param2ITextClassifierCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onClassifyText(TextClassificationSessionId param2TextClassificationSessionId, TextClassification.Request param2Request, ITextClassifierCallback param2ITextClassifierCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.textclassifier.ITextClassifierService");
          if (param2TextClassificationSessionId != null) {
            parcel.writeInt(1);
            param2TextClassificationSessionId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Request != null) {
            parcel.writeInt(1);
            param2Request.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ITextClassifierCallback != null) {
            iBinder = param2ITextClassifierCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ITextClassifierService.Stub.getDefaultImpl() != null) {
            ITextClassifierService.Stub.getDefaultImpl().onClassifyText(param2TextClassificationSessionId, param2Request, param2ITextClassifierCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onGenerateLinks(TextClassificationSessionId param2TextClassificationSessionId, TextLinks.Request param2Request, ITextClassifierCallback param2ITextClassifierCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.textclassifier.ITextClassifierService");
          if (param2TextClassificationSessionId != null) {
            parcel.writeInt(1);
            param2TextClassificationSessionId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Request != null) {
            parcel.writeInt(1);
            param2Request.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ITextClassifierCallback != null) {
            iBinder = param2ITextClassifierCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && ITextClassifierService.Stub.getDefaultImpl() != null) {
            ITextClassifierService.Stub.getDefaultImpl().onGenerateLinks(param2TextClassificationSessionId, param2Request, param2ITextClassifierCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSelectionEvent(TextClassificationSessionId param2TextClassificationSessionId, SelectionEvent param2SelectionEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.textclassifier.ITextClassifierService");
          if (param2TextClassificationSessionId != null) {
            parcel.writeInt(1);
            param2TextClassificationSessionId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2SelectionEvent != null) {
            parcel.writeInt(1);
            param2SelectionEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && ITextClassifierService.Stub.getDefaultImpl() != null) {
            ITextClassifierService.Stub.getDefaultImpl().onSelectionEvent(param2TextClassificationSessionId, param2SelectionEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTextClassifierEvent(TextClassificationSessionId param2TextClassificationSessionId, TextClassifierEvent param2TextClassifierEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.textclassifier.ITextClassifierService");
          if (param2TextClassificationSessionId != null) {
            parcel.writeInt(1);
            param2TextClassificationSessionId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2TextClassifierEvent != null) {
            parcel.writeInt(1);
            param2TextClassifierEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && ITextClassifierService.Stub.getDefaultImpl() != null) {
            ITextClassifierService.Stub.getDefaultImpl().onTextClassifierEvent(param2TextClassificationSessionId, param2TextClassifierEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCreateTextClassificationSession(TextClassificationContext param2TextClassificationContext, TextClassificationSessionId param2TextClassificationSessionId) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.textclassifier.ITextClassifierService");
          if (param2TextClassificationContext != null) {
            parcel.writeInt(1);
            param2TextClassificationContext.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2TextClassificationSessionId != null) {
            parcel.writeInt(1);
            param2TextClassificationSessionId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && ITextClassifierService.Stub.getDefaultImpl() != null) {
            ITextClassifierService.Stub.getDefaultImpl().onCreateTextClassificationSession(param2TextClassificationContext, param2TextClassificationSessionId);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDestroyTextClassificationSession(TextClassificationSessionId param2TextClassificationSessionId) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.textclassifier.ITextClassifierService");
          if (param2TextClassificationSessionId != null) {
            parcel.writeInt(1);
            param2TextClassificationSessionId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && ITextClassifierService.Stub.getDefaultImpl() != null) {
            ITextClassifierService.Stub.getDefaultImpl().onDestroyTextClassificationSession(param2TextClassificationSessionId);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDetectLanguage(TextClassificationSessionId param2TextClassificationSessionId, TextLanguage.Request param2Request, ITextClassifierCallback param2ITextClassifierCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.textclassifier.ITextClassifierService");
          if (param2TextClassificationSessionId != null) {
            parcel.writeInt(1);
            param2TextClassificationSessionId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Request != null) {
            parcel.writeInt(1);
            param2Request.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ITextClassifierCallback != null) {
            iBinder = param2ITextClassifierCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && ITextClassifierService.Stub.getDefaultImpl() != null) {
            ITextClassifierService.Stub.getDefaultImpl().onDetectLanguage(param2TextClassificationSessionId, param2Request, param2ITextClassifierCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSuggestConversationActions(TextClassificationSessionId param2TextClassificationSessionId, ConversationActions.Request param2Request, ITextClassifierCallback param2ITextClassifierCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.textclassifier.ITextClassifierService");
          if (param2TextClassificationSessionId != null) {
            parcel.writeInt(1);
            param2TextClassificationSessionId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Request != null) {
            parcel.writeInt(1);
            param2Request.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ITextClassifierCallback != null) {
            iBinder = param2ITextClassifierCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(9, parcel, (Parcel)null, 1);
          if (!bool && ITextClassifierService.Stub.getDefaultImpl() != null) {
            ITextClassifierService.Stub.getDefaultImpl().onSuggestConversationActions(param2TextClassificationSessionId, param2Request, param2ITextClassifierCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onConnectedStateChanged(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.textclassifier.ITextClassifierService");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel, (Parcel)null, 1);
          if (!bool && ITextClassifierService.Stub.getDefaultImpl() != null) {
            ITextClassifierService.Stub.getDefaultImpl().onConnectedStateChanged(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITextClassifierService param1ITextClassifierService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITextClassifierService != null) {
          Proxy.sDefaultImpl = param1ITextClassifierService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITextClassifierService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
