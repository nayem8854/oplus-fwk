package android.service.carrier;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.telephony.uicc.IccUtils;
import com.android.telephony.Rlog;
import java.util.Objects;

public class CarrierIdentifier implements Parcelable {
  public static final Parcelable.Creator<CarrierIdentifier> CREATOR = new Parcelable.Creator<CarrierIdentifier>() {
      public CarrierIdentifier createFromParcel(Parcel param1Parcel) {
        return new CarrierIdentifier(param1Parcel);
      }
      
      public CarrierIdentifier[] newArray(int param1Int) {
        return new CarrierIdentifier[param1Int];
      }
    };
  
  private int mCarrierId;
  
  private String mGid1;
  
  private String mGid2;
  
  private String mIccid;
  
  private String mImsi;
  
  private String mMcc;
  
  private String mMnc;
  
  private int mSpecificCarrierId;
  
  private String mSpn;
  
  public CarrierIdentifier(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6) {
    this(paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, null, -1, -1);
  }
  
  public CarrierIdentifier(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, int paramInt1, int paramInt2) {
    this(paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, null, paramInt1, paramInt2);
  }
  
  public CarrierIdentifier(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, int paramInt1, int paramInt2) {
    this.mCarrierId = -1;
    this.mSpecificCarrierId = -1;
    this.mMcc = paramString1;
    this.mMnc = paramString2;
    this.mSpn = paramString3;
    this.mImsi = paramString4;
    this.mGid1 = paramString5;
    this.mGid2 = paramString6;
    this.mIccid = paramString7;
    this.mCarrierId = paramInt1;
    this.mSpecificCarrierId = paramInt2;
  }
  
  public CarrierIdentifier(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7) {
    this(paramString1, paramString2, paramString3, paramString4, paramString5, paramString6);
    this.mIccid = paramString7;
  }
  
  public CarrierIdentifier(byte[] paramArrayOfbyte, String paramString1, String paramString2) {
    String str;
    this.mCarrierId = -1;
    this.mSpecificCarrierId = -1;
    if (paramArrayOfbyte.length == 3) {
      str = IccUtils.bytesToHexString(paramArrayOfbyte);
      this.mMcc = new String(new char[] { str.charAt(1), str.charAt(0), str.charAt(3) });
      if (str.charAt(2) == 'F') {
        this.mMnc = new String(new char[] { str.charAt(5), str.charAt(4) });
      } else {
        this.mMnc = new String(new char[] { str.charAt(5), str.charAt(4), str.charAt(2) });
      } 
      this.mGid1 = paramString1;
      this.mGid2 = paramString2;
      this.mSpn = null;
      this.mImsi = null;
      this.mIccid = null;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("MCC & MNC must be set by a 3-byte array: byte[");
    stringBuilder.append(str.length);
    stringBuilder.append("]");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public CarrierIdentifier(Parcel paramParcel) {
    this.mCarrierId = -1;
    this.mSpecificCarrierId = -1;
    readFromParcel(paramParcel);
  }
  
  public String getMcc() {
    return this.mMcc;
  }
  
  public String getMnc() {
    return this.mMnc;
  }
  
  public String getSpn() {
    return this.mSpn;
  }
  
  public String getImsi() {
    return this.mImsi;
  }
  
  public String getGid1() {
    return this.mGid1;
  }
  
  public String getGid2() {
    return this.mGid2;
  }
  
  public String getIccid() {
    return this.mIccid;
  }
  
  public int getCarrierId() {
    return this.mCarrierId;
  }
  
  public int getSpecificCarrierId() {
    return this.mSpecificCarrierId;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (Objects.equals(this.mMcc, ((CarrierIdentifier)paramObject).mMcc)) {
      String str1 = this.mMnc, str2 = ((CarrierIdentifier)paramObject).mMnc;
      if (Objects.equals(str1, str2)) {
        str1 = this.mSpn;
        str2 = ((CarrierIdentifier)paramObject).mSpn;
        if (Objects.equals(str1, str2)) {
          str1 = this.mImsi;
          str2 = ((CarrierIdentifier)paramObject).mImsi;
          if (Objects.equals(str1, str2)) {
            str1 = this.mGid1;
            str2 = ((CarrierIdentifier)paramObject).mGid1;
            if (Objects.equals(str1, str2)) {
              str1 = this.mGid2;
              str2 = ((CarrierIdentifier)paramObject).mGid2;
              if (Objects.equals(str1, str2)) {
                str2 = this.mIccid;
                str1 = ((CarrierIdentifier)paramObject).mIccid;
                if (Objects.equals(str2, str1)) {
                  int i = this.mCarrierId;
                  if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((CarrierIdentifier)paramObject).mCarrierId))) {
                    i = this.mSpecificCarrierId;
                    if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((CarrierIdentifier)paramObject).mSpecificCarrierId)))
                      return null; 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mMcc, this.mMnc, this.mSpn, this.mImsi, this.mGid1, this.mGid2, this.mIccid, Integer.valueOf(this.mCarrierId), Integer.valueOf(this.mSpecificCarrierId) });
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mMcc);
    paramParcel.writeString(this.mMnc);
    paramParcel.writeString(this.mSpn);
    paramParcel.writeString(this.mImsi);
    paramParcel.writeString(this.mGid1);
    paramParcel.writeString(this.mGid2);
    paramParcel.writeString(this.mIccid);
    paramParcel.writeInt(this.mCarrierId);
    paramParcel.writeInt(this.mSpecificCarrierId);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CarrierIdentifier{mcc=");
    stringBuilder.append(this.mMcc);
    stringBuilder.append(",mnc=");
    stringBuilder.append(this.mMnc);
    stringBuilder.append(",spn=");
    stringBuilder.append(this.mSpn);
    stringBuilder.append(",imsi=");
    String str = this.mImsi;
    stringBuilder.append(Rlog.pii(false, str));
    stringBuilder.append(",gid1=");
    stringBuilder.append(this.mGid1);
    stringBuilder.append(",gid2=");
    stringBuilder.append(this.mGid2);
    stringBuilder.append(",iccid=");
    stringBuilder.append(this.mIccid);
    stringBuilder.append(",carrierid=");
    stringBuilder.append(this.mCarrierId);
    stringBuilder.append(",specificCarrierId=");
    stringBuilder.append(this.mSpecificCarrierId);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mMcc = paramParcel.readString();
    this.mMnc = paramParcel.readString();
    this.mSpn = paramParcel.readString();
    this.mImsi = paramParcel.readString();
    this.mGid1 = paramParcel.readString();
    this.mGid2 = paramParcel.readString();
    this.mIccid = paramParcel.readString();
    this.mCarrierId = paramParcel.readInt();
    this.mSpecificCarrierId = paramParcel.readInt();
  }
  
  class MatchType {
    public static final int ALL = 0;
    
    public static final int GID1 = 3;
    
    public static final int GID2 = 4;
    
    public static final int ICCID = 5;
    
    public static final int IMSI_PREFIX = 2;
    
    public static final int SPN = 1;
  }
}
