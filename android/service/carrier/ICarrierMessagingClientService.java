package android.service.carrier;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ICarrierMessagingClientService extends IInterface {
  class Default implements ICarrierMessagingClientService {
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICarrierMessagingClientService {
    private static final String DESCRIPTOR = "android.service.carrier.ICarrierMessagingClientService";
    
    public Stub() {
      attachInterface(this, "android.service.carrier.ICarrierMessagingClientService");
    }
    
    public static ICarrierMessagingClientService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.carrier.ICarrierMessagingClientService");
      if (iInterface != null && iInterface instanceof ICarrierMessagingClientService)
        return (ICarrierMessagingClientService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      return null;
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902)
        return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
      param1Parcel2.writeString("android.service.carrier.ICarrierMessagingClientService");
      return true;
    }
    
    private static class Proxy implements ICarrierMessagingClientService {
      public static ICarrierMessagingClientService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.carrier.ICarrierMessagingClientService";
      }
    }
    
    public static boolean setDefaultImpl(ICarrierMessagingClientService param1ICarrierMessagingClientService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICarrierMessagingClientService != null) {
          Proxy.sDefaultImpl = param1ICarrierMessagingClientService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICarrierMessagingClientService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
