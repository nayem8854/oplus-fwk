package android.service.carrier;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface ICarrierMessagingService extends IInterface {
  void downloadMms(Uri paramUri1, int paramInt, Uri paramUri2, ICarrierMessagingCallback paramICarrierMessagingCallback) throws RemoteException;
  
  void filterSms(MessagePdu paramMessagePdu, String paramString, int paramInt1, int paramInt2, ICarrierMessagingCallback paramICarrierMessagingCallback) throws RemoteException;
  
  void sendDataSms(byte[] paramArrayOfbyte, int paramInt1, String paramString, int paramInt2, int paramInt3, ICarrierMessagingCallback paramICarrierMessagingCallback) throws RemoteException;
  
  void sendMms(Uri paramUri1, int paramInt, Uri paramUri2, ICarrierMessagingCallback paramICarrierMessagingCallback) throws RemoteException;
  
  void sendMultipartTextSms(List<String> paramList, int paramInt1, String paramString, int paramInt2, ICarrierMessagingCallback paramICarrierMessagingCallback) throws RemoteException;
  
  void sendTextSms(String paramString1, int paramInt1, String paramString2, int paramInt2, ICarrierMessagingCallback paramICarrierMessagingCallback) throws RemoteException;
  
  class Default implements ICarrierMessagingService {
    public void filterSms(MessagePdu param1MessagePdu, String param1String, int param1Int1, int param1Int2, ICarrierMessagingCallback param1ICarrierMessagingCallback) throws RemoteException {}
    
    public void sendTextSms(String param1String1, int param1Int1, String param1String2, int param1Int2, ICarrierMessagingCallback param1ICarrierMessagingCallback) throws RemoteException {}
    
    public void sendDataSms(byte[] param1ArrayOfbyte, int param1Int1, String param1String, int param1Int2, int param1Int3, ICarrierMessagingCallback param1ICarrierMessagingCallback) throws RemoteException {}
    
    public void sendMultipartTextSms(List<String> param1List, int param1Int1, String param1String, int param1Int2, ICarrierMessagingCallback param1ICarrierMessagingCallback) throws RemoteException {}
    
    public void sendMms(Uri param1Uri1, int param1Int, Uri param1Uri2, ICarrierMessagingCallback param1ICarrierMessagingCallback) throws RemoteException {}
    
    public void downloadMms(Uri param1Uri1, int param1Int, Uri param1Uri2, ICarrierMessagingCallback param1ICarrierMessagingCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICarrierMessagingService {
    private static final String DESCRIPTOR = "android.service.carrier.ICarrierMessagingService";
    
    static final int TRANSACTION_downloadMms = 6;
    
    static final int TRANSACTION_filterSms = 1;
    
    static final int TRANSACTION_sendDataSms = 3;
    
    static final int TRANSACTION_sendMms = 5;
    
    static final int TRANSACTION_sendMultipartTextSms = 4;
    
    static final int TRANSACTION_sendTextSms = 2;
    
    public Stub() {
      attachInterface(this, "android.service.carrier.ICarrierMessagingService");
    }
    
    public static ICarrierMessagingService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.carrier.ICarrierMessagingService");
      if (iInterface != null && iInterface instanceof ICarrierMessagingService)
        return (ICarrierMessagingService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "downloadMms";
        case 5:
          return "sendMms";
        case 4:
          return "sendMultipartTextSms";
        case 3:
          return "sendDataSms";
        case 2:
          return "sendTextSms";
        case 1:
          break;
      } 
      return "filterSms";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        String str1;
        byte[] arrayOfByte;
        Uri uri;
        ArrayList<String> arrayList;
        int i;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("android.service.carrier.ICarrierMessagingService");
            if (param1Parcel1.readInt() != 0) {
              Uri uri1 = Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              uri = Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              uri = null;
            } 
            iCarrierMessagingCallback = ICarrierMessagingCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            downloadMms((Uri)param1Parcel2, param1Int1, uri, iCarrierMessagingCallback);
            return true;
          case 5:
            iCarrierMessagingCallback.enforceInterface("android.service.carrier.ICarrierMessagingService");
            if (iCarrierMessagingCallback.readInt() != 0) {
              Uri uri1 = Uri.CREATOR.createFromParcel((Parcel)iCarrierMessagingCallback);
            } else {
              param1Parcel2 = null;
            } 
            param1Int1 = iCarrierMessagingCallback.readInt();
            if (iCarrierMessagingCallback.readInt() != 0) {
              uri = Uri.CREATOR.createFromParcel((Parcel)iCarrierMessagingCallback);
            } else {
              uri = null;
            } 
            iCarrierMessagingCallback = ICarrierMessagingCallback.Stub.asInterface(iCarrierMessagingCallback.readStrongBinder());
            sendMms((Uri)param1Parcel2, param1Int1, uri, iCarrierMessagingCallback);
            return true;
          case 4:
            iCarrierMessagingCallback.enforceInterface("android.service.carrier.ICarrierMessagingService");
            arrayList = iCarrierMessagingCallback.createStringArrayList();
            param1Int2 = iCarrierMessagingCallback.readInt();
            str1 = iCarrierMessagingCallback.readString();
            param1Int1 = iCarrierMessagingCallback.readInt();
            iCarrierMessagingCallback = ICarrierMessagingCallback.Stub.asInterface(iCarrierMessagingCallback.readStrongBinder());
            sendMultipartTextSms(arrayList, param1Int2, str1, param1Int1, iCarrierMessagingCallback);
            return true;
          case 3:
            iCarrierMessagingCallback.enforceInterface("android.service.carrier.ICarrierMessagingService");
            arrayOfByte = iCarrierMessagingCallback.createByteArray();
            param1Int2 = iCarrierMessagingCallback.readInt();
            str2 = iCarrierMessagingCallback.readString();
            i = iCarrierMessagingCallback.readInt();
            param1Int1 = iCarrierMessagingCallback.readInt();
            iCarrierMessagingCallback = ICarrierMessagingCallback.Stub.asInterface(iCarrierMessagingCallback.readStrongBinder());
            sendDataSms(arrayOfByte, param1Int2, str2, i, param1Int1, iCarrierMessagingCallback);
            return true;
          case 2:
            iCarrierMessagingCallback.enforceInterface("android.service.carrier.ICarrierMessagingService");
            str = iCarrierMessagingCallback.readString();
            param1Int2 = iCarrierMessagingCallback.readInt();
            str2 = iCarrierMessagingCallback.readString();
            param1Int1 = iCarrierMessagingCallback.readInt();
            iCarrierMessagingCallback = ICarrierMessagingCallback.Stub.asInterface(iCarrierMessagingCallback.readStrongBinder());
            sendTextSms(str, param1Int2, str2, param1Int1, iCarrierMessagingCallback);
            return true;
          case 1:
            break;
        } 
        iCarrierMessagingCallback.enforceInterface("android.service.carrier.ICarrierMessagingService");
        if (iCarrierMessagingCallback.readInt() != 0) {
          MessagePdu messagePdu = MessagePdu.CREATOR.createFromParcel((Parcel)iCarrierMessagingCallback);
        } else {
          str = null;
        } 
        String str2 = iCarrierMessagingCallback.readString();
        param1Int2 = iCarrierMessagingCallback.readInt();
        param1Int1 = iCarrierMessagingCallback.readInt();
        ICarrierMessagingCallback iCarrierMessagingCallback = ICarrierMessagingCallback.Stub.asInterface(iCarrierMessagingCallback.readStrongBinder());
        filterSms((MessagePdu)str, str2, param1Int2, param1Int1, iCarrierMessagingCallback);
        return true;
      } 
      str.writeString("android.service.carrier.ICarrierMessagingService");
      return true;
    }
    
    private static class Proxy implements ICarrierMessagingService {
      public static ICarrierMessagingService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.carrier.ICarrierMessagingService";
      }
      
      public void filterSms(MessagePdu param2MessagePdu, String param2String, int param2Int1, int param2Int2, ICarrierMessagingCallback param2ICarrierMessagingCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.carrier.ICarrierMessagingService");
          if (param2MessagePdu != null) {
            parcel.writeInt(1);
            param2MessagePdu.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2ICarrierMessagingCallback != null) {
            iBinder = param2ICarrierMessagingCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ICarrierMessagingService.Stub.getDefaultImpl() != null) {
            ICarrierMessagingService.Stub.getDefaultImpl().filterSms(param2MessagePdu, param2String, param2Int1, param2Int2, param2ICarrierMessagingCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendTextSms(String param2String1, int param2Int1, String param2String2, int param2Int2, ICarrierMessagingCallback param2ICarrierMessagingCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.carrier.ICarrierMessagingService");
          parcel.writeString(param2String1);
          parcel.writeInt(param2Int1);
          parcel.writeString(param2String2);
          parcel.writeInt(param2Int2);
          if (param2ICarrierMessagingCallback != null) {
            iBinder = param2ICarrierMessagingCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ICarrierMessagingService.Stub.getDefaultImpl() != null) {
            ICarrierMessagingService.Stub.getDefaultImpl().sendTextSms(param2String1, param2Int1, param2String2, param2Int2, param2ICarrierMessagingCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendDataSms(byte[] param2ArrayOfbyte, int param2Int1, String param2String, int param2Int2, int param2Int3, ICarrierMessagingCallback param2ICarrierMessagingCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.carrier.ICarrierMessagingService");
          try {
            parcel.writeByteArray(param2ArrayOfbyte);
            try {
              parcel.writeInt(param2Int1);
              try {
                parcel.writeString(param2String);
                try {
                  parcel.writeInt(param2Int2);
                  try {
                    IBinder iBinder;
                    parcel.writeInt(param2Int3);
                    if (param2ICarrierMessagingCallback != null) {
                      iBinder = param2ICarrierMessagingCallback.asBinder();
                    } else {
                      iBinder = null;
                    } 
                    parcel.writeStrongBinder(iBinder);
                    try {
                      boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
                      if (!bool && ICarrierMessagingService.Stub.getDefaultImpl() != null) {
                        ICarrierMessagingService.Stub.getDefaultImpl().sendDataSms(param2ArrayOfbyte, param2Int1, param2String, param2Int2, param2Int3, param2ICarrierMessagingCallback);
                        parcel.recycle();
                        return;
                      } 
                      parcel.recycle();
                      return;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2ArrayOfbyte;
      }
      
      public void sendMultipartTextSms(List<String> param2List, int param2Int1, String param2String, int param2Int2, ICarrierMessagingCallback param2ICarrierMessagingCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.carrier.ICarrierMessagingService");
          parcel.writeStringList(param2List);
          parcel.writeInt(param2Int1);
          parcel.writeString(param2String);
          parcel.writeInt(param2Int2);
          if (param2ICarrierMessagingCallback != null) {
            iBinder = param2ICarrierMessagingCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && ICarrierMessagingService.Stub.getDefaultImpl() != null) {
            ICarrierMessagingService.Stub.getDefaultImpl().sendMultipartTextSms(param2List, param2Int1, param2String, param2Int2, param2ICarrierMessagingCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendMms(Uri param2Uri1, int param2Int, Uri param2Uri2, ICarrierMessagingCallback param2ICarrierMessagingCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.carrier.ICarrierMessagingService");
          if (param2Uri1 != null) {
            parcel.writeInt(1);
            param2Uri1.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          if (param2Uri2 != null) {
            parcel.writeInt(1);
            param2Uri2.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ICarrierMessagingCallback != null) {
            iBinder = param2ICarrierMessagingCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && ICarrierMessagingService.Stub.getDefaultImpl() != null) {
            ICarrierMessagingService.Stub.getDefaultImpl().sendMms(param2Uri1, param2Int, param2Uri2, param2ICarrierMessagingCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void downloadMms(Uri param2Uri1, int param2Int, Uri param2Uri2, ICarrierMessagingCallback param2ICarrierMessagingCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.carrier.ICarrierMessagingService");
          if (param2Uri1 != null) {
            parcel.writeInt(1);
            param2Uri1.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          if (param2Uri2 != null) {
            parcel.writeInt(1);
            param2Uri2.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ICarrierMessagingCallback != null) {
            iBinder = param2ICarrierMessagingCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && ICarrierMessagingService.Stub.getDefaultImpl() != null) {
            ICarrierMessagingService.Stub.getDefaultImpl().downloadMms(param2Uri1, param2Int, param2Uri2, param2ICarrierMessagingCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICarrierMessagingService param1ICarrierMessagingService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICarrierMessagingService != null) {
          Proxy.sDefaultImpl = param1ICarrierMessagingService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICarrierMessagingService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
