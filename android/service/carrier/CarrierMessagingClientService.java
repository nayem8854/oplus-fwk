package android.service.carrier;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class CarrierMessagingClientService extends Service {
  private final ICarrierMessagingClientServiceImpl mImpl = new ICarrierMessagingClientServiceImpl();
  
  public final IBinder onBind(Intent paramIntent) {
    return this.mImpl.asBinder();
  }
  
  private class ICarrierMessagingClientServiceImpl extends ICarrierMessagingClientService.Stub {
    final CarrierMessagingClientService this$0;
    
    private ICarrierMessagingClientServiceImpl() {}
  }
}
