package android.service.carrier;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ResultReceiver;

public interface ICarrierService extends IInterface {
  void getCarrierConfig(CarrierIdentifier paramCarrierIdentifier, ResultReceiver paramResultReceiver) throws RemoteException;
  
  class Default implements ICarrierService {
    public void getCarrierConfig(CarrierIdentifier param1CarrierIdentifier, ResultReceiver param1ResultReceiver) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICarrierService {
    private static final String DESCRIPTOR = "android.service.carrier.ICarrierService";
    
    static final int TRANSACTION_getCarrierConfig = 1;
    
    public Stub() {
      attachInterface(this, "android.service.carrier.ICarrierService");
    }
    
    public static ICarrierService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.carrier.ICarrierService");
      if (iInterface != null && iInterface instanceof ICarrierService)
        return (ICarrierService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "getCarrierConfig";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.service.carrier.ICarrierService");
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.carrier.ICarrierService");
      if (param1Parcel1.readInt() != 0) {
        CarrierIdentifier carrierIdentifier = CarrierIdentifier.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        ResultReceiver resultReceiver = ResultReceiver.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      getCarrierConfig((CarrierIdentifier)param1Parcel2, (ResultReceiver)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements ICarrierService {
      public static ICarrierService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.carrier.ICarrierService";
      }
      
      public void getCarrierConfig(CarrierIdentifier param2CarrierIdentifier, ResultReceiver param2ResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.carrier.ICarrierService");
          if (param2CarrierIdentifier != null) {
            parcel.writeInt(1);
            param2CarrierIdentifier.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ResultReceiver != null) {
            parcel.writeInt(1);
            param2ResultReceiver.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ICarrierService.Stub.getDefaultImpl() != null) {
            ICarrierService.Stub.getDefaultImpl().getCarrierConfig(param2CarrierIdentifier, param2ResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICarrierService param1ICarrierService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICarrierService != null) {
          Proxy.sDefaultImpl = param1ICarrierService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICarrierService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
