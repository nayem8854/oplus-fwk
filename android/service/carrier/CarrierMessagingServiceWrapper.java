package android.service.carrier;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;
import com.android.internal.util.Preconditions;
import java.util.List;

public abstract class CarrierMessagingServiceWrapper {
  private volatile CarrierMessagingServiceConnection mCarrierMessagingServiceConnection;
  
  private volatile ICarrierMessagingService mICarrierMessagingService;
  
  public boolean bindToCarrierMessagingService(Context paramContext, String paramString) {
    boolean bool;
    if (this.mCarrierMessagingServiceConnection == null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkState(bool);
    Intent intent = new Intent("android.service.carrier.CarrierMessagingService");
    intent.setPackage(paramString);
    this.mCarrierMessagingServiceConnection = new CarrierMessagingServiceConnection();
    return paramContext.bindService(intent, this.mCarrierMessagingServiceConnection, 1);
  }
  
  public void disposeConnection(Context paramContext) {
    Preconditions.checkNotNull(this.mCarrierMessagingServiceConnection);
    paramContext.unbindService(this.mCarrierMessagingServiceConnection);
    this.mCarrierMessagingServiceConnection = null;
  }
  
  private void onServiceReady(ICarrierMessagingService paramICarrierMessagingService) {
    this.mICarrierMessagingService = paramICarrierMessagingService;
    onServiceReady();
  }
  
  public void filterSms(MessagePdu paramMessagePdu, String paramString, int paramInt1, int paramInt2, CarrierMessagingCallbackWrapper paramCarrierMessagingCallbackWrapper) {
    if (this.mICarrierMessagingService != null)
      try {
        ICarrierMessagingService iCarrierMessagingService = this.mICarrierMessagingService;
        CarrierMessagingCallbackWrapperInternal carrierMessagingCallbackWrapperInternal = new CarrierMessagingCallbackWrapperInternal();
        this(this, paramCarrierMessagingCallbackWrapper);
        iCarrierMessagingService.filterSms(paramMessagePdu, paramString, paramInt1, paramInt2, carrierMessagingCallbackWrapperInternal);
      } catch (RemoteException remoteException) {
        throw new RuntimeException(remoteException);
      }  
  }
  
  public void sendTextSms(String paramString1, int paramInt1, String paramString2, int paramInt2, CarrierMessagingCallbackWrapper paramCarrierMessagingCallbackWrapper) {
    if (this.mICarrierMessagingService != null)
      try {
        ICarrierMessagingService iCarrierMessagingService = this.mICarrierMessagingService;
        CarrierMessagingCallbackWrapperInternal carrierMessagingCallbackWrapperInternal = new CarrierMessagingCallbackWrapperInternal();
        this(this, paramCarrierMessagingCallbackWrapper);
        iCarrierMessagingService.sendTextSms(paramString1, paramInt1, paramString2, paramInt2, carrierMessagingCallbackWrapperInternal);
      } catch (RemoteException remoteException) {
        throw new RuntimeException(remoteException);
      }  
  }
  
  public void sendDataSms(byte[] paramArrayOfbyte, int paramInt1, String paramString, int paramInt2, int paramInt3, CarrierMessagingCallbackWrapper paramCarrierMessagingCallbackWrapper) {
    if (this.mICarrierMessagingService != null)
      try {
        ICarrierMessagingService iCarrierMessagingService = this.mICarrierMessagingService;
        CarrierMessagingCallbackWrapperInternal carrierMessagingCallbackWrapperInternal = new CarrierMessagingCallbackWrapperInternal();
        this(this, paramCarrierMessagingCallbackWrapper);
        iCarrierMessagingService.sendDataSms(paramArrayOfbyte, paramInt1, paramString, paramInt2, paramInt3, carrierMessagingCallbackWrapperInternal);
      } catch (RemoteException remoteException) {
        throw new RuntimeException(remoteException);
      }  
  }
  
  public void sendMultipartTextSms(List<String> paramList, int paramInt1, String paramString, int paramInt2, CarrierMessagingCallbackWrapper paramCarrierMessagingCallbackWrapper) {
    if (this.mICarrierMessagingService != null)
      try {
        ICarrierMessagingService iCarrierMessagingService = this.mICarrierMessagingService;
        CarrierMessagingCallbackWrapperInternal carrierMessagingCallbackWrapperInternal = new CarrierMessagingCallbackWrapperInternal();
        this(this, paramCarrierMessagingCallbackWrapper);
        iCarrierMessagingService.sendMultipartTextSms(paramList, paramInt1, paramString, paramInt2, carrierMessagingCallbackWrapperInternal);
      } catch (RemoteException remoteException) {
        throw new RuntimeException(remoteException);
      }  
  }
  
  public void sendMms(Uri paramUri1, int paramInt, Uri paramUri2, CarrierMessagingCallbackWrapper paramCarrierMessagingCallbackWrapper) {
    if (this.mICarrierMessagingService != null)
      try {
        ICarrierMessagingService iCarrierMessagingService = this.mICarrierMessagingService;
        CarrierMessagingCallbackWrapperInternal carrierMessagingCallbackWrapperInternal = new CarrierMessagingCallbackWrapperInternal();
        this(this, paramCarrierMessagingCallbackWrapper);
        iCarrierMessagingService.sendMms(paramUri1, paramInt, paramUri2, carrierMessagingCallbackWrapperInternal);
      } catch (RemoteException remoteException) {
        throw new RuntimeException(remoteException);
      }  
  }
  
  public void downloadMms(Uri paramUri1, int paramInt, Uri paramUri2, CarrierMessagingCallbackWrapper paramCarrierMessagingCallbackWrapper) {
    if (this.mICarrierMessagingService != null)
      try {
        ICarrierMessagingService iCarrierMessagingService = this.mICarrierMessagingService;
        CarrierMessagingCallbackWrapperInternal carrierMessagingCallbackWrapperInternal = new CarrierMessagingCallbackWrapperInternal();
        this(this, paramCarrierMessagingCallbackWrapper);
        iCarrierMessagingService.downloadMms(paramUri1, paramInt, paramUri2, carrierMessagingCallbackWrapperInternal);
      } catch (RemoteException remoteException) {
        throw new RuntimeException(remoteException);
      }  
  }
  
  public abstract void onServiceReady();
  
  class CarrierMessagingServiceConnection implements ServiceConnection {
    final CarrierMessagingServiceWrapper this$0;
    
    private CarrierMessagingServiceConnection() {}
    
    public void onServiceConnected(ComponentName param1ComponentName, IBinder param1IBinder) {
      CarrierMessagingServiceWrapper.this.onServiceReady(ICarrierMessagingService.Stub.asInterface(param1IBinder));
    }
    
    public void onServiceDisconnected(ComponentName param1ComponentName) {}
  }
  
  public static abstract class CarrierMessagingCallbackWrapper {
    public void onFilterComplete(int param1Int) {}
    
    public void onSendSmsComplete(int param1Int1, int param1Int2) {}
    
    public void onSendMultipartSmsComplete(int param1Int, int[] param1ArrayOfint) {}
    
    public void onSendMmsComplete(int param1Int, byte[] param1ArrayOfbyte) {}
    
    public void onDownloadMmsComplete(int param1Int) {}
  }
  
  class CarrierMessagingCallbackWrapperInternal extends ICarrierMessagingCallback.Stub {
    CarrierMessagingServiceWrapper.CarrierMessagingCallbackWrapper mCarrierMessagingCallbackWrapper;
    
    final CarrierMessagingServiceWrapper this$0;
    
    CarrierMessagingCallbackWrapperInternal(CarrierMessagingServiceWrapper.CarrierMessagingCallbackWrapper param1CarrierMessagingCallbackWrapper) {
      this.mCarrierMessagingCallbackWrapper = param1CarrierMessagingCallbackWrapper;
    }
    
    public void onFilterComplete(int param1Int) throws RemoteException {
      this.mCarrierMessagingCallbackWrapper.onFilterComplete(param1Int);
    }
    
    public void onSendSmsComplete(int param1Int1, int param1Int2) throws RemoteException {
      this.mCarrierMessagingCallbackWrapper.onSendSmsComplete(param1Int1, param1Int2);
    }
    
    public void onSendMultipartSmsComplete(int param1Int, int[] param1ArrayOfint) throws RemoteException {
      this.mCarrierMessagingCallbackWrapper.onSendMultipartSmsComplete(param1Int, param1ArrayOfint);
    }
    
    public void onSendMmsComplete(int param1Int, byte[] param1ArrayOfbyte) throws RemoteException {
      this.mCarrierMessagingCallbackWrapper.onSendMmsComplete(param1Int, param1ArrayOfbyte);
    }
    
    public void onDownloadMmsComplete(int param1Int) throws RemoteException {
      this.mCarrierMessagingCallbackWrapper.onDownloadMmsComplete(param1Int);
    }
  }
}
