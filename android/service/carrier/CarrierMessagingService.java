package android.service.carrier;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;
import java.util.List;

public abstract class CarrierMessagingService extends Service {
  public static final int DOWNLOAD_STATUS_ERROR = 2;
  
  public static final int DOWNLOAD_STATUS_OK = 0;
  
  public static final int DOWNLOAD_STATUS_RETRY_ON_CARRIER_NETWORK = 1;
  
  public static final int RECEIVE_OPTIONS_DEFAULT = 0;
  
  public static final int RECEIVE_OPTIONS_DROP = 1;
  
  public static final int RECEIVE_OPTIONS_SKIP_NOTIFY_WHEN_CREDENTIAL_PROTECTED_STORAGE_UNAVAILABLE = 2;
  
  public static final int SEND_FLAG_REQUEST_DELIVERY_STATUS = 1;
  
  public static final int SEND_STATUS_ERROR = 2;
  
  public static final int SEND_STATUS_OK = 0;
  
  public static final int SEND_STATUS_RETRY_ON_CARRIER_NETWORK = 1;
  
  public static final String SERVICE_INTERFACE = "android.service.carrier.CarrierMessagingService";
  
  private final ICarrierMessagingWrapper mWrapper = new ICarrierMessagingWrapper();
  
  @Deprecated
  public void onFilterSms(MessagePdu paramMessagePdu, String paramString, int paramInt1, int paramInt2, ResultCallback<Boolean> paramResultCallback) {
    try {
      paramResultCallback.onReceiveResult(Boolean.valueOf(true));
    } catch (RemoteException remoteException) {}
  }
  
  public void onReceiveTextSms(MessagePdu paramMessagePdu, String paramString, int paramInt1, int paramInt2, ResultCallback<Integer> paramResultCallback) {
    onFilterSms(paramMessagePdu, paramString, paramInt1, paramInt2, (ResultCallback<Boolean>)new Object(this, paramResultCallback));
  }
  
  @Deprecated
  public void onSendTextSms(String paramString1, int paramInt, String paramString2, ResultCallback<SendSmsResult> paramResultCallback) {
    try {
      SendSmsResult sendSmsResult = new SendSmsResult();
      this(1, 0);
      paramResultCallback.onReceiveResult(sendSmsResult);
    } catch (RemoteException remoteException) {}
  }
  
  public void onSendTextSms(String paramString1, int paramInt1, String paramString2, int paramInt2, ResultCallback<SendSmsResult> paramResultCallback) {
    onSendTextSms(paramString1, paramInt1, paramString2, paramResultCallback);
  }
  
  @Deprecated
  public void onSendDataSms(byte[] paramArrayOfbyte, int paramInt1, String paramString, int paramInt2, ResultCallback<SendSmsResult> paramResultCallback) {
    try {
      SendSmsResult sendSmsResult = new SendSmsResult();
      this(1, 0);
      paramResultCallback.onReceiveResult(sendSmsResult);
    } catch (RemoteException remoteException) {}
  }
  
  public void onSendDataSms(byte[] paramArrayOfbyte, int paramInt1, String paramString, int paramInt2, int paramInt3, ResultCallback<SendSmsResult> paramResultCallback) {
    onSendDataSms(paramArrayOfbyte, paramInt1, paramString, paramInt2, paramResultCallback);
  }
  
  @Deprecated
  public void onSendMultipartTextSms(List<String> paramList, int paramInt, String paramString, ResultCallback<SendMultipartSmsResult> paramResultCallback) {
    try {
      SendMultipartSmsResult sendMultipartSmsResult = new SendMultipartSmsResult();
      this(1, null);
      paramResultCallback.onReceiveResult(sendMultipartSmsResult);
    } catch (RemoteException remoteException) {}
  }
  
  public void onSendMultipartTextSms(List<String> paramList, int paramInt1, String paramString, int paramInt2, ResultCallback<SendMultipartSmsResult> paramResultCallback) {
    onSendMultipartTextSms(paramList, paramInt1, paramString, paramResultCallback);
  }
  
  public void onSendMms(Uri paramUri1, int paramInt, Uri paramUri2, ResultCallback<SendMmsResult> paramResultCallback) {
    try {
      SendMmsResult sendMmsResult = new SendMmsResult();
      this(1, null);
      paramResultCallback.onReceiveResult(sendMmsResult);
    } catch (RemoteException remoteException) {}
  }
  
  public void onDownloadMms(Uri paramUri1, int paramInt, Uri paramUri2, ResultCallback<Integer> paramResultCallback) {
    try {
      paramResultCallback.onReceiveResult(Integer.valueOf(1));
    } catch (RemoteException remoteException) {}
  }
  
  public IBinder onBind(Intent paramIntent) {
    if (!"android.service.carrier.CarrierMessagingService".equals(paramIntent.getAction()))
      return null; 
    return this.mWrapper;
  }
  
  class SendMmsResult {
    private byte[] mSendConfPdu;
    
    private int mSendStatus;
    
    public SendMmsResult(CarrierMessagingService this$0, byte[] param1ArrayOfbyte) {
      this.mSendStatus = this$0;
      this.mSendConfPdu = param1ArrayOfbyte;
    }
    
    public int getSendStatus() {
      return this.mSendStatus;
    }
    
    public byte[] getSendConfPdu() {
      return this.mSendConfPdu;
    }
  }
  
  class SendSmsResult {
    private final int mMessageRef;
    
    private final int mSendStatus;
    
    public SendSmsResult(CarrierMessagingService this$0, int param1Int1) {
      this.mSendStatus = this$0;
      this.mMessageRef = param1Int1;
    }
    
    public int getMessageRef() {
      return this.mMessageRef;
    }
    
    public int getSendStatus() {
      return this.mSendStatus;
    }
  }
  
  class SendMultipartSmsResult {
    private final int[] mMessageRefs;
    
    private final int mSendStatus;
    
    public SendMultipartSmsResult(CarrierMessagingService this$0, int[] param1ArrayOfint) {
      this.mSendStatus = this$0;
      this.mMessageRefs = param1ArrayOfint;
    }
    
    public int[] getMessageRefs() {
      return this.mMessageRefs;
    }
    
    public int getSendStatus() {
      return this.mSendStatus;
    }
  }
  
  private class ICarrierMessagingWrapper extends ICarrierMessagingService.Stub {
    final CarrierMessagingService this$0;
    
    private ICarrierMessagingWrapper() {}
    
    public void filterSms(MessagePdu param1MessagePdu, String param1String, int param1Int1, int param1Int2, ICarrierMessagingCallback param1ICarrierMessagingCallback) {
      CarrierMessagingService.this.onReceiveTextSms(param1MessagePdu, param1String, param1Int1, param1Int2, (CarrierMessagingService.ResultCallback<Integer>)new Object(this, param1ICarrierMessagingCallback));
    }
    
    public void sendTextSms(String param1String1, int param1Int1, String param1String2, int param1Int2, ICarrierMessagingCallback param1ICarrierMessagingCallback) {
      CarrierMessagingService.this.onSendTextSms(param1String1, param1Int1, param1String2, param1Int2, (CarrierMessagingService.ResultCallback<CarrierMessagingService.SendSmsResult>)new Object(this, param1ICarrierMessagingCallback));
    }
    
    public void sendDataSms(byte[] param1ArrayOfbyte, int param1Int1, String param1String, int param1Int2, int param1Int3, ICarrierMessagingCallback param1ICarrierMessagingCallback) {
      CarrierMessagingService.this.onSendDataSms(param1ArrayOfbyte, param1Int1, param1String, param1Int2, param1Int3, (CarrierMessagingService.ResultCallback<CarrierMessagingService.SendSmsResult>)new Object(this, param1ICarrierMessagingCallback));
    }
    
    public void sendMultipartTextSms(List<String> param1List, int param1Int1, String param1String, int param1Int2, ICarrierMessagingCallback param1ICarrierMessagingCallback) {
      CarrierMessagingService.this.onSendMultipartTextSms(param1List, param1Int1, param1String, param1Int2, (CarrierMessagingService.ResultCallback<CarrierMessagingService.SendMultipartSmsResult>)new Object(this, param1ICarrierMessagingCallback));
    }
    
    public void sendMms(Uri param1Uri1, int param1Int, Uri param1Uri2, ICarrierMessagingCallback param1ICarrierMessagingCallback) {
      CarrierMessagingService.this.onSendMms(param1Uri1, param1Int, param1Uri2, (CarrierMessagingService.ResultCallback<CarrierMessagingService.SendMmsResult>)new Object(this, param1ICarrierMessagingCallback));
    }
    
    public void downloadMms(Uri param1Uri1, int param1Int, Uri param1Uri2, ICarrierMessagingCallback param1ICarrierMessagingCallback) {
      CarrierMessagingService.this.onDownloadMms(param1Uri1, param1Int, param1Uri2, (CarrierMessagingService.ResultCallback<Integer>)new Object(this, param1ICarrierMessagingCallback));
    }
  }
  
  class ResultCallback<T> {
    public abstract void onReceiveResult(T param1T) throws RemoteException;
  }
}
