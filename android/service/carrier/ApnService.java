package android.service.carrier;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import java.util.List;

@SystemApi
public abstract class ApnService extends Service {
  private static final String LOG_TAG = "ApnService";
  
  private final IApnSourceService.Stub mBinder = new IApnSourceService.Stub() {
      final ApnService this$0;
      
      public ContentValues[] getApns(int param1Int) {
        try {
          List<ContentValues> list = ApnService.this.onRestoreApns(param1Int);
          return list.<ContentValues>toArray(new ContentValues[list.size()]);
        } catch (Exception exception) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Error in getApns for subId=");
          stringBuilder.append(param1Int);
          stringBuilder.append(": ");
          stringBuilder.append(exception.getMessage());
          Log.e("ApnService", stringBuilder.toString(), exception);
          return null;
        } 
      }
    };
  
  public IBinder onBind(Intent paramIntent) {
    return this.mBinder;
  }
  
  public abstract List<ContentValues> onRestoreApns(int paramInt);
}
