package android.service.carrier;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public final class MessagePdu implements Parcelable {
  public MessagePdu(List<byte[]> paramList) {
    if (paramList != null && !paramList.contains(null)) {
      this.mPduList = paramList;
      return;
    } 
    throw new IllegalArgumentException("pduList must not be null or contain nulls");
  }
  
  public List<byte[]> getPdus() {
    return this.mPduList;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    List<byte[]> list = this.mPduList;
    if (list == null) {
      paramParcel.writeInt(-1);
    } else {
      paramParcel.writeInt(list.size());
      for (byte[] arrayOfByte : this.mPduList)
        paramParcel.writeByteArray(arrayOfByte); 
    } 
  }
  
  public static final Parcelable.Creator<MessagePdu> CREATOR = new Parcelable.Creator<MessagePdu>() {
      public MessagePdu createFromParcel(Parcel param1Parcel) {
        ArrayList<byte[]> arrayList;
        int i = param1Parcel.readInt();
        if (i == -1) {
          arrayList = null;
        } else {
          ArrayList<byte[]> arrayList1 = new ArrayList(i);
          byte b = 0;
          while (true) {
            arrayList = arrayList1;
            if (b < i) {
              arrayList1.add(param1Parcel.createByteArray());
              b++;
              continue;
            } 
            break;
          } 
        } 
        return new MessagePdu((List<byte[]>)arrayList);
      }
      
      public MessagePdu[] newArray(int param1Int) {
        return new MessagePdu[param1Int];
      }
    };
  
  private static final int NULL_LENGTH = -1;
  
  private final List<byte[]> mPduList;
}
