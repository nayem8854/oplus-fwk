package android.service.carrier;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ICarrierMessagingCallback extends IInterface {
  void onDownloadMmsComplete(int paramInt) throws RemoteException;
  
  void onFilterComplete(int paramInt) throws RemoteException;
  
  void onSendMmsComplete(int paramInt, byte[] paramArrayOfbyte) throws RemoteException;
  
  void onSendMultipartSmsComplete(int paramInt, int[] paramArrayOfint) throws RemoteException;
  
  void onSendSmsComplete(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements ICarrierMessagingCallback {
    public void onFilterComplete(int param1Int) throws RemoteException {}
    
    public void onSendSmsComplete(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onSendMultipartSmsComplete(int param1Int, int[] param1ArrayOfint) throws RemoteException {}
    
    public void onSendMmsComplete(int param1Int, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void onDownloadMmsComplete(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICarrierMessagingCallback {
    private static final String DESCRIPTOR = "android.service.carrier.ICarrierMessagingCallback";
    
    static final int TRANSACTION_onDownloadMmsComplete = 5;
    
    static final int TRANSACTION_onFilterComplete = 1;
    
    static final int TRANSACTION_onSendMmsComplete = 4;
    
    static final int TRANSACTION_onSendMultipartSmsComplete = 3;
    
    static final int TRANSACTION_onSendSmsComplete = 2;
    
    public Stub() {
      attachInterface(this, "android.service.carrier.ICarrierMessagingCallback");
    }
    
    public static ICarrierMessagingCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.carrier.ICarrierMessagingCallback");
      if (iInterface != null && iInterface instanceof ICarrierMessagingCallback)
        return (ICarrierMessagingCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "onDownloadMmsComplete";
            } 
            return "onSendMmsComplete";
          } 
          return "onSendMultipartSmsComplete";
        } 
        return "onSendSmsComplete";
      } 
      return "onFilterComplete";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      int[] arrayOfInt;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          byte[] arrayOfByte;
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.service.carrier.ICarrierMessagingCallback");
                return true;
              } 
              param1Parcel1.enforceInterface("android.service.carrier.ICarrierMessagingCallback");
              param1Int1 = param1Parcel1.readInt();
              onDownloadMmsComplete(param1Int1);
              return true;
            } 
            param1Parcel1.enforceInterface("android.service.carrier.ICarrierMessagingCallback");
            param1Int1 = param1Parcel1.readInt();
            arrayOfByte = param1Parcel1.createByteArray();
            onSendMmsComplete(param1Int1, arrayOfByte);
            return true;
          } 
          arrayOfByte.enforceInterface("android.service.carrier.ICarrierMessagingCallback");
          param1Int1 = arrayOfByte.readInt();
          arrayOfInt = arrayOfByte.createIntArray();
          onSendMultipartSmsComplete(param1Int1, arrayOfInt);
          return true;
        } 
        arrayOfInt.enforceInterface("android.service.carrier.ICarrierMessagingCallback");
        param1Int2 = arrayOfInt.readInt();
        param1Int1 = arrayOfInt.readInt();
        onSendSmsComplete(param1Int2, param1Int1);
        return true;
      } 
      arrayOfInt.enforceInterface("android.service.carrier.ICarrierMessagingCallback");
      param1Int1 = arrayOfInt.readInt();
      onFilterComplete(param1Int1);
      return true;
    }
    
    private static class Proxy implements ICarrierMessagingCallback {
      public static ICarrierMessagingCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.carrier.ICarrierMessagingCallback";
      }
      
      public void onFilterComplete(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.carrier.ICarrierMessagingCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ICarrierMessagingCallback.Stub.getDefaultImpl() != null) {
            ICarrierMessagingCallback.Stub.getDefaultImpl().onFilterComplete(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSendSmsComplete(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.carrier.ICarrierMessagingCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ICarrierMessagingCallback.Stub.getDefaultImpl() != null) {
            ICarrierMessagingCallback.Stub.getDefaultImpl().onSendSmsComplete(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSendMultipartSmsComplete(int param2Int, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.carrier.ICarrierMessagingCallback");
          parcel.writeInt(param2Int);
          parcel.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && ICarrierMessagingCallback.Stub.getDefaultImpl() != null) {
            ICarrierMessagingCallback.Stub.getDefaultImpl().onSendMultipartSmsComplete(param2Int, param2ArrayOfint);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSendMmsComplete(int param2Int, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.carrier.ICarrierMessagingCallback");
          parcel.writeInt(param2Int);
          parcel.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && ICarrierMessagingCallback.Stub.getDefaultImpl() != null) {
            ICarrierMessagingCallback.Stub.getDefaultImpl().onSendMmsComplete(param2Int, param2ArrayOfbyte);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDownloadMmsComplete(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.carrier.ICarrierMessagingCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && ICarrierMessagingCallback.Stub.getDefaultImpl() != null) {
            ICarrierMessagingCallback.Stub.getDefaultImpl().onDownloadMmsComplete(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICarrierMessagingCallback param1ICarrierMessagingCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICarrierMessagingCallback != null) {
          Proxy.sDefaultImpl = param1ICarrierMessagingCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICarrierMessagingCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
