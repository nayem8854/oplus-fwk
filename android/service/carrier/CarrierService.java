package android.service.carrier;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PersistableBundle;
import android.os.ResultReceiver;
import android.telephony.TelephonyRegistryManager;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public abstract class CarrierService extends Service {
  public static final String CARRIER_SERVICE_INTERFACE = "android.service.carrier.CarrierService";
  
  private static final String LOG_TAG = "CarrierService";
  
  private final ICarrierService.Stub mStubWrapper = new ICarrierServiceWrapper();
  
  public final void notifyCarrierNetworkChange(boolean paramBoolean) {
    TelephonyRegistryManager telephonyRegistryManager = (TelephonyRegistryManager)getSystemService("telephony_registry");
    if (telephonyRegistryManager != null)
      telephonyRegistryManager.notifyCarrierNetworkChange(paramBoolean); 
  }
  
  public IBinder onBind(Intent paramIntent) {
    return this.mStubWrapper;
  }
  
  public abstract PersistableBundle onLoadConfig(CarrierIdentifier paramCarrierIdentifier);
  
  public class ICarrierServiceWrapper extends ICarrierService.Stub {
    public static final String KEY_CONFIG_BUNDLE = "config_bundle";
    
    public static final int RESULT_ERROR = 1;
    
    public static final int RESULT_OK = 0;
    
    final CarrierService this$0;
    
    public void getCarrierConfig(CarrierIdentifier param1CarrierIdentifier, ResultReceiver param1ResultReceiver) {
      try {
        Bundle bundle = new Bundle();
        this();
        bundle.putParcelable("config_bundle", CarrierService.this.onLoadConfig(param1CarrierIdentifier));
        param1ResultReceiver.send(0, bundle);
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Error in onLoadConfig: ");
        stringBuilder.append(exception.getMessage());
        Log.e("CarrierService", stringBuilder.toString(), exception);
        param1ResultReceiver.send(1, null);
      } 
    }
    
    protected void dump(FileDescriptor param1FileDescriptor, PrintWriter param1PrintWriter, String[] param1ArrayOfString) {
      CarrierService.this.dump(param1FileDescriptor, param1PrintWriter, param1ArrayOfString);
    }
  }
}
