package android.service.carrier;

import android.content.ContentValues;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IApnSourceService extends IInterface {
  ContentValues[] getApns(int paramInt) throws RemoteException;
  
  class Default implements IApnSourceService {
    public ContentValues[] getApns(int param1Int) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IApnSourceService {
    private static final String DESCRIPTOR = "android.service.carrier.IApnSourceService";
    
    static final int TRANSACTION_getApns = 1;
    
    public Stub() {
      attachInterface(this, "android.service.carrier.IApnSourceService");
    }
    
    public static IApnSourceService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.carrier.IApnSourceService");
      if (iInterface != null && iInterface instanceof IApnSourceService)
        return (IApnSourceService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "getApns";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.service.carrier.IApnSourceService");
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.carrier.IApnSourceService");
      param1Int1 = param1Parcel1.readInt();
      ContentValues[] arrayOfContentValues = getApns(param1Int1);
      param1Parcel2.writeNoException();
      param1Parcel2.writeTypedArray(arrayOfContentValues, 1);
      return true;
    }
    
    private static class Proxy implements IApnSourceService {
      public static IApnSourceService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.carrier.IApnSourceService";
      }
      
      public ContentValues[] getApns(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.carrier.IApnSourceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IApnSourceService.Stub.getDefaultImpl() != null)
            return IApnSourceService.Stub.getDefaultImpl().getApns(param2Int); 
          parcel2.readException();
          return parcel2.<ContentValues>createTypedArray(ContentValues.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IApnSourceService param1IApnSourceService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IApnSourceService != null) {
          Proxy.sDefaultImpl = param1IApnSourceService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IApnSourceService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
