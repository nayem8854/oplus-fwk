package android.service.attention;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAttentionService extends IInterface {
  void cancelAttentionCheck(IAttentionCallback paramIAttentionCallback) throws RemoteException;
  
  void checkAttention(IAttentionCallback paramIAttentionCallback) throws RemoteException;
  
  class Default implements IAttentionService {
    public void checkAttention(IAttentionCallback param1IAttentionCallback) throws RemoteException {}
    
    public void cancelAttentionCheck(IAttentionCallback param1IAttentionCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAttentionService {
    private static final String DESCRIPTOR = "android.service.attention.IAttentionService";
    
    static final int TRANSACTION_cancelAttentionCheck = 2;
    
    static final int TRANSACTION_checkAttention = 1;
    
    public Stub() {
      attachInterface(this, "android.service.attention.IAttentionService");
    }
    
    public static IAttentionService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.attention.IAttentionService");
      if (iInterface != null && iInterface instanceof IAttentionService)
        return (IAttentionService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "cancelAttentionCheck";
      } 
      return "checkAttention";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.service.attention.IAttentionService");
          return true;
        } 
        param1Parcel1.enforceInterface("android.service.attention.IAttentionService");
        iAttentionCallback = IAttentionCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
        cancelAttentionCheck(iAttentionCallback);
        return true;
      } 
      iAttentionCallback.enforceInterface("android.service.attention.IAttentionService");
      IAttentionCallback iAttentionCallback = IAttentionCallback.Stub.asInterface(iAttentionCallback.readStrongBinder());
      checkAttention(iAttentionCallback);
      return true;
    }
    
    private static class Proxy implements IAttentionService {
      public static IAttentionService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.attention.IAttentionService";
      }
      
      public void checkAttention(IAttentionCallback param2IAttentionCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.attention.IAttentionService");
          if (param2IAttentionCallback != null) {
            iBinder = param2IAttentionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IAttentionService.Stub.getDefaultImpl() != null) {
            IAttentionService.Stub.getDefaultImpl().checkAttention(param2IAttentionCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void cancelAttentionCheck(IAttentionCallback param2IAttentionCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.attention.IAttentionService");
          if (param2IAttentionCallback != null) {
            iBinder = param2IAttentionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IAttentionService.Stub.getDefaultImpl() != null) {
            IAttentionService.Stub.getDefaultImpl().cancelAttentionCheck(param2IAttentionCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAttentionService param1IAttentionService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAttentionService != null) {
          Proxy.sDefaultImpl = param1IAttentionService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAttentionService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
