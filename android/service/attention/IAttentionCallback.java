package android.service.attention;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAttentionCallback extends IInterface {
  void onFailure(int paramInt) throws RemoteException;
  
  void onSuccess(int paramInt, long paramLong) throws RemoteException;
  
  class Default implements IAttentionCallback {
    public void onSuccess(int param1Int, long param1Long) throws RemoteException {}
    
    public void onFailure(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAttentionCallback {
    private static final String DESCRIPTOR = "android.service.attention.IAttentionCallback";
    
    static final int TRANSACTION_onFailure = 2;
    
    static final int TRANSACTION_onSuccess = 1;
    
    public Stub() {
      attachInterface(this, "android.service.attention.IAttentionCallback");
    }
    
    public static IAttentionCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.attention.IAttentionCallback");
      if (iInterface != null && iInterface instanceof IAttentionCallback)
        return (IAttentionCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onFailure";
      } 
      return "onSuccess";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.service.attention.IAttentionCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("android.service.attention.IAttentionCallback");
        param1Int1 = param1Parcel1.readInt();
        onFailure(param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.attention.IAttentionCallback");
      param1Int1 = param1Parcel1.readInt();
      long l = param1Parcel1.readLong();
      onSuccess(param1Int1, l);
      return true;
    }
    
    private static class Proxy implements IAttentionCallback {
      public static IAttentionCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.attention.IAttentionCallback";
      }
      
      public void onSuccess(int param2Int, long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.attention.IAttentionCallback");
          parcel.writeInt(param2Int);
          parcel.writeLong(param2Long);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IAttentionCallback.Stub.getDefaultImpl() != null) {
            IAttentionCallback.Stub.getDefaultImpl().onSuccess(param2Int, param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onFailure(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.attention.IAttentionCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IAttentionCallback.Stub.getDefaultImpl() != null) {
            IAttentionCallback.Stub.getDefaultImpl().onFailure(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAttentionCallback param1IAttentionCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAttentionCallback != null) {
          Proxy.sDefaultImpl = param1IAttentionCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAttentionCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
