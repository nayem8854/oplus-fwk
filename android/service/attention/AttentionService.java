package android.service.attention;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public abstract class AttentionService extends Service {
  public static final int ATTENTION_FAILURE_CAMERA_PERMISSION_ABSENT = 6;
  
  public static final int ATTENTION_FAILURE_CANCELLED = 3;
  
  public static final int ATTENTION_FAILURE_PREEMPTED = 4;
  
  public static final int ATTENTION_FAILURE_TIMED_OUT = 5;
  
  public static final int ATTENTION_FAILURE_UNKNOWN = 2;
  
  public static final int ATTENTION_SUCCESS_ABSENT = 0;
  
  public static final int ATTENTION_SUCCESS_PRESENT = 1;
  
  public static final String SERVICE_INTERFACE = "android.service.attention.AttentionService";
  
  private final IAttentionService.Stub mBinder = new IAttentionService.Stub() {
      final AttentionService this$0;
      
      public void checkAttention(IAttentionCallback param1IAttentionCallback) {
        Preconditions.checkNotNull(param1IAttentionCallback);
        AttentionService.this.onCheckAttention(new AttentionService.AttentionCallback());
      }
      
      public void cancelAttentionCheck(IAttentionCallback param1IAttentionCallback) {
        Preconditions.checkNotNull(param1IAttentionCallback);
        AttentionService.this.onCancelAttentionCheck(new AttentionService.AttentionCallback());
      }
    };
  
  public final IBinder onBind(Intent paramIntent) {
    if ("android.service.attention.AttentionService".equals(paramIntent.getAction()))
      return this.mBinder; 
    return null;
  }
  
  public abstract void onCancelAttentionCheck(AttentionCallback paramAttentionCallback);
  
  public abstract void onCheckAttention(AttentionCallback paramAttentionCallback);
  
  class AttentionCallback {
    private final IAttentionCallback mCallback;
    
    private AttentionCallback(AttentionService this$0) {
      this.mCallback = (IAttentionCallback)this$0;
    }
    
    public void onSuccess(int param1Int, long param1Long) {
      try {
        this.mCallback.onSuccess(param1Int, param1Long);
      } catch (RemoteException remoteException) {
        remoteException.rethrowFromSystemServer();
      } 
    }
    
    public void onFailure(int param1Int) {
      try {
        this.mCallback.onFailure(param1Int);
      } catch (RemoteException remoteException) {
        remoteException.rethrowFromSystemServer();
      } 
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class AttentionFailureCodes implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class AttentionSuccessCodes implements Annotation {}
}
