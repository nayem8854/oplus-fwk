package android.service.wallpaper;

import android.graphics.Rect;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.MotionEvent;

public interface IWallpaperEngine extends IInterface {
  void destroy() throws RemoteException;
  
  void dispatchPointer(MotionEvent paramMotionEvent) throws RemoteException;
  
  void dispatchWallpaperCommand(String paramString, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle) throws RemoteException;
  
  void requestWallpaperColors() throws RemoteException;
  
  void setDesiredSize(int paramInt1, int paramInt2) throws RemoteException;
  
  void setDisplayPadding(Rect paramRect) throws RemoteException;
  
  void setInAmbientMode(boolean paramBoolean, long paramLong) throws RemoteException;
  
  void setVisibility(boolean paramBoolean) throws RemoteException;
  
  void setZoomOut(float paramFloat) throws RemoteException;
  
  class Default implements IWallpaperEngine {
    public void setDesiredSize(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void setDisplayPadding(Rect param1Rect) throws RemoteException {}
    
    public void setVisibility(boolean param1Boolean) throws RemoteException {}
    
    public void setInAmbientMode(boolean param1Boolean, long param1Long) throws RemoteException {}
    
    public void dispatchPointer(MotionEvent param1MotionEvent) throws RemoteException {}
    
    public void dispatchWallpaperCommand(String param1String, int param1Int1, int param1Int2, int param1Int3, Bundle param1Bundle) throws RemoteException {}
    
    public void requestWallpaperColors() throws RemoteException {}
    
    public void destroy() throws RemoteException {}
    
    public void setZoomOut(float param1Float) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWallpaperEngine {
    private static final String DESCRIPTOR = "android.service.wallpaper.IWallpaperEngine";
    
    static final int TRANSACTION_destroy = 8;
    
    static final int TRANSACTION_dispatchPointer = 5;
    
    static final int TRANSACTION_dispatchWallpaperCommand = 6;
    
    static final int TRANSACTION_requestWallpaperColors = 7;
    
    static final int TRANSACTION_setDesiredSize = 1;
    
    static final int TRANSACTION_setDisplayPadding = 2;
    
    static final int TRANSACTION_setInAmbientMode = 4;
    
    static final int TRANSACTION_setVisibility = 3;
    
    static final int TRANSACTION_setZoomOut = 9;
    
    public Stub() {
      attachInterface(this, "android.service.wallpaper.IWallpaperEngine");
    }
    
    public static IWallpaperEngine asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.wallpaper.IWallpaperEngine");
      if (iInterface != null && iInterface instanceof IWallpaperEngine)
        return (IWallpaperEngine)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 9:
          return "setZoomOut";
        case 8:
          return "destroy";
        case 7:
          return "requestWallpaperColors";
        case 6:
          return "dispatchWallpaperCommand";
        case 5:
          return "dispatchPointer";
        case 4:
          return "setInAmbientMode";
        case 3:
          return "setVisibility";
        case 2:
          return "setDisplayPadding";
        case 1:
          break;
      } 
      return "setDesiredSize";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        float f;
        int i;
        long l;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 9:
            param1Parcel1.enforceInterface("android.service.wallpaper.IWallpaperEngine");
            f = param1Parcel1.readFloat();
            setZoomOut(f);
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.service.wallpaper.IWallpaperEngine");
            destroy();
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.service.wallpaper.IWallpaperEngine");
            requestWallpaperColors();
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.service.wallpaper.IWallpaperEngine");
            str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            i = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            dispatchWallpaperCommand(str, param1Int1, i, param1Int2, (Bundle)param1Parcel1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.service.wallpaper.IWallpaperEngine");
            if (param1Parcel1.readInt() != 0) {
              MotionEvent motionEvent = (MotionEvent)MotionEvent.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            dispatchPointer((MotionEvent)param1Parcel1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.service.wallpaper.IWallpaperEngine");
            if (param1Parcel1.readInt() != 0)
              bool2 = true; 
            l = param1Parcel1.readLong();
            setInAmbientMode(bool2, l);
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.service.wallpaper.IWallpaperEngine");
            bool2 = bool1;
            if (param1Parcel1.readInt() != 0)
              bool2 = true; 
            setVisibility(bool2);
            return true;
          case 2:
            param1Parcel1.enforceInterface("android.service.wallpaper.IWallpaperEngine");
            if (param1Parcel1.readInt() != 0) {
              Rect rect = (Rect)Rect.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setDisplayPadding((Rect)param1Parcel1);
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("android.service.wallpaper.IWallpaperEngine");
        param1Int1 = param1Parcel1.readInt();
        param1Int2 = param1Parcel1.readInt();
        setDesiredSize(param1Int1, param1Int2);
        return true;
      } 
      str.writeString("android.service.wallpaper.IWallpaperEngine");
      return true;
    }
    
    private static class Proxy implements IWallpaperEngine {
      public static IWallpaperEngine sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.wallpaper.IWallpaperEngine";
      }
      
      public void setDesiredSize(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.wallpaper.IWallpaperEngine");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IWallpaperEngine.Stub.getDefaultImpl() != null) {
            IWallpaperEngine.Stub.getDefaultImpl().setDesiredSize(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setDisplayPadding(Rect param2Rect) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.wallpaper.IWallpaperEngine");
          if (param2Rect != null) {
            parcel.writeInt(1);
            param2Rect.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IWallpaperEngine.Stub.getDefaultImpl() != null) {
            IWallpaperEngine.Stub.getDefaultImpl().setDisplayPadding(param2Rect);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setVisibility(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.service.wallpaper.IWallpaperEngine");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel, null, 1);
          if (!bool1 && IWallpaperEngine.Stub.getDefaultImpl() != null) {
            IWallpaperEngine.Stub.getDefaultImpl().setVisibility(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setInAmbientMode(boolean param2Boolean, long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.service.wallpaper.IWallpaperEngine");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeLong(param2Long);
          boolean bool1 = this.mRemote.transact(4, parcel, null, 1);
          if (!bool1 && IWallpaperEngine.Stub.getDefaultImpl() != null) {
            IWallpaperEngine.Stub.getDefaultImpl().setInAmbientMode(param2Boolean, param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dispatchPointer(MotionEvent param2MotionEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.wallpaper.IWallpaperEngine");
          if (param2MotionEvent != null) {
            parcel.writeInt(1);
            param2MotionEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IWallpaperEngine.Stub.getDefaultImpl() != null) {
            IWallpaperEngine.Stub.getDefaultImpl().dispatchPointer(param2MotionEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dispatchWallpaperCommand(String param2String, int param2Int1, int param2Int2, int param2Int3, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.wallpaper.IWallpaperEngine");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IWallpaperEngine.Stub.getDefaultImpl() != null) {
            IWallpaperEngine.Stub.getDefaultImpl().dispatchWallpaperCommand(param2String, param2Int1, param2Int2, param2Int3, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestWallpaperColors() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.wallpaper.IWallpaperEngine");
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IWallpaperEngine.Stub.getDefaultImpl() != null) {
            IWallpaperEngine.Stub.getDefaultImpl().requestWallpaperColors();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void destroy() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.wallpaper.IWallpaperEngine");
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IWallpaperEngine.Stub.getDefaultImpl() != null) {
            IWallpaperEngine.Stub.getDefaultImpl().destroy();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setZoomOut(float param2Float) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.wallpaper.IWallpaperEngine");
          parcel.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IWallpaperEngine.Stub.getDefaultImpl() != null) {
            IWallpaperEngine.Stub.getDefaultImpl().setZoomOut(param2Float);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IWallpaperEngine param1IWallpaperEngine) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWallpaperEngine != null) {
          Proxy.sDefaultImpl = param1IWallpaperEngine;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWallpaperEngine getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
