package android.service.wallpaper;

import android.graphics.Rect;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IWallpaperService extends IInterface {
  void attach(IWallpaperConnection paramIWallpaperConnection, IBinder paramIBinder, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3, Rect paramRect, int paramInt4) throws RemoteException;
  
  void detach() throws RemoteException;
  
  class Default implements IWallpaperService {
    public void attach(IWallpaperConnection param1IWallpaperConnection, IBinder param1IBinder, int param1Int1, boolean param1Boolean, int param1Int2, int param1Int3, Rect param1Rect, int param1Int4) throws RemoteException {}
    
    public void detach() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWallpaperService {
    private static final String DESCRIPTOR = "android.service.wallpaper.IWallpaperService";
    
    static final int TRANSACTION_attach = 1;
    
    static final int TRANSACTION_detach = 2;
    
    public Stub() {
      attachInterface(this, "android.service.wallpaper.IWallpaperService");
    }
    
    public static IWallpaperService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.wallpaper.IWallpaperService");
      if (iInterface != null && iInterface instanceof IWallpaperService)
        return (IWallpaperService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "detach";
      } 
      return "attach";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.service.wallpaper.IWallpaperService");
          return true;
        } 
        param1Parcel1.enforceInterface("android.service.wallpaper.IWallpaperService");
        detach();
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.wallpaper.IWallpaperService");
      IWallpaperConnection iWallpaperConnection = IWallpaperConnection.Stub.asInterface(param1Parcel1.readStrongBinder());
      IBinder iBinder = param1Parcel1.readStrongBinder();
      int i = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      int j = param1Parcel1.readInt();
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        Rect rect = (Rect)Rect.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      param1Int2 = param1Parcel1.readInt();
      attach(iWallpaperConnection, iBinder, i, bool, j, param1Int1, (Rect)param1Parcel2, param1Int2);
      return true;
    }
    
    private static class Proxy implements IWallpaperService {
      public static IWallpaperService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.wallpaper.IWallpaperService";
      }
      
      public void attach(IWallpaperConnection param2IWallpaperConnection, IBinder param2IBinder, int param2Int1, boolean param2Boolean, int param2Int2, int param2Int3, Rect param2Rect, int param2Int4) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.wallpaper.IWallpaperService");
          if (param2IWallpaperConnection != null) {
            iBinder = param2IWallpaperConnection.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          try {
            parcel.writeStrongBinder(param2IBinder);
            try {
              boolean bool;
              parcel.writeInt(param2Int1);
              if (param2Boolean) {
                bool = true;
              } else {
                bool = false;
              } 
              parcel.writeInt(bool);
              try {
                parcel.writeInt(param2Int2);
                try {
                  parcel.writeInt(param2Int3);
                  if (param2Rect != null) {
                    parcel.writeInt(1);
                    param2Rect.writeToParcel(parcel, 0);
                  } else {
                    parcel.writeInt(0);
                  } 
                  parcel.writeInt(param2Int4);
                  boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
                  if (!bool1 && IWallpaperService.Stub.getDefaultImpl() != null) {
                    IWallpaperService.Stub.getDefaultImpl().attach(param2IWallpaperConnection, param2IBinder, param2Int1, param2Boolean, param2Int2, param2Int3, param2Rect, param2Int4);
                    parcel.recycle();
                    return;
                  } 
                  parcel.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2IWallpaperConnection;
      }
      
      public void detach() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.wallpaper.IWallpaperService");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IWallpaperService.Stub.getDefaultImpl() != null) {
            IWallpaperService.Stub.getDefaultImpl().detach();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IWallpaperService param1IWallpaperService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWallpaperService != null) {
          Proxy.sDefaultImpl = param1IWallpaperService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWallpaperService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
