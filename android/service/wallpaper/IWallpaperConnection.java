package android.service.wallpaper;

import android.app.WallpaperColors;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

public interface IWallpaperConnection extends IInterface {
  void attachEngine(IWallpaperEngine paramIWallpaperEngine, int paramInt) throws RemoteException;
  
  void engineShown(IWallpaperEngine paramIWallpaperEngine) throws RemoteException;
  
  void onWallpaperColorsChanged(WallpaperColors paramWallpaperColors, int paramInt) throws RemoteException;
  
  ParcelFileDescriptor setWallpaper(String paramString) throws RemoteException;
  
  class Default implements IWallpaperConnection {
    public void attachEngine(IWallpaperEngine param1IWallpaperEngine, int param1Int) throws RemoteException {}
    
    public void engineShown(IWallpaperEngine param1IWallpaperEngine) throws RemoteException {}
    
    public ParcelFileDescriptor setWallpaper(String param1String) throws RemoteException {
      return null;
    }
    
    public void onWallpaperColorsChanged(WallpaperColors param1WallpaperColors, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWallpaperConnection {
    private static final String DESCRIPTOR = "android.service.wallpaper.IWallpaperConnection";
    
    static final int TRANSACTION_attachEngine = 1;
    
    static final int TRANSACTION_engineShown = 2;
    
    static final int TRANSACTION_onWallpaperColorsChanged = 4;
    
    static final int TRANSACTION_setWallpaper = 3;
    
    public Stub() {
      attachInterface(this, "android.service.wallpaper.IWallpaperConnection");
    }
    
    public static IWallpaperConnection asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.wallpaper.IWallpaperConnection");
      if (iInterface != null && iInterface instanceof IWallpaperConnection)
        return (IWallpaperConnection)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onWallpaperColorsChanged";
          } 
          return "setWallpaper";
        } 
        return "engineShown";
      } 
      return "attachEngine";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IWallpaperEngine iWallpaperEngine1;
      if (param1Int1 != 1) {
        ParcelFileDescriptor parcelFileDescriptor;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            WallpaperColors wallpaperColors;
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.service.wallpaper.IWallpaperConnection");
              return true;
            } 
            param1Parcel1.enforceInterface("android.service.wallpaper.IWallpaperConnection");
            if (param1Parcel1.readInt() != 0) {
              wallpaperColors = (WallpaperColors)WallpaperColors.CREATOR.createFromParcel(param1Parcel1);
            } else {
              wallpaperColors = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            onWallpaperColorsChanged(wallpaperColors, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          } 
          param1Parcel1.enforceInterface("android.service.wallpaper.IWallpaperConnection");
          String str = param1Parcel1.readString();
          parcelFileDescriptor = setWallpaper(str);
          param1Parcel2.writeNoException();
          if (parcelFileDescriptor != null) {
            param1Parcel2.writeInt(1);
            parcelFileDescriptor.writeToParcel(param1Parcel2, 1);
          } else {
            param1Parcel2.writeInt(0);
          } 
          return true;
        } 
        parcelFileDescriptor.enforceInterface("android.service.wallpaper.IWallpaperConnection");
        iWallpaperEngine1 = IWallpaperEngine.Stub.asInterface(parcelFileDescriptor.readStrongBinder());
        engineShown(iWallpaperEngine1);
        param1Parcel2.writeNoException();
        return true;
      } 
      iWallpaperEngine1.enforceInterface("android.service.wallpaper.IWallpaperConnection");
      IWallpaperEngine iWallpaperEngine2 = IWallpaperEngine.Stub.asInterface(iWallpaperEngine1.readStrongBinder());
      param1Int1 = iWallpaperEngine1.readInt();
      attachEngine(iWallpaperEngine2, param1Int1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IWallpaperConnection {
      public static IWallpaperConnection sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.wallpaper.IWallpaperConnection";
      }
      
      public void attachEngine(IWallpaperEngine param2IWallpaperEngine, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.service.wallpaper.IWallpaperConnection");
          if (param2IWallpaperEngine != null) {
            iBinder = param2IWallpaperEngine.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IWallpaperConnection.Stub.getDefaultImpl() != null) {
            IWallpaperConnection.Stub.getDefaultImpl().attachEngine(param2IWallpaperEngine, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void engineShown(IWallpaperEngine param2IWallpaperEngine) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.service.wallpaper.IWallpaperConnection");
          if (param2IWallpaperEngine != null) {
            iBinder = param2IWallpaperEngine.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IWallpaperConnection.Stub.getDefaultImpl() != null) {
            IWallpaperConnection.Stub.getDefaultImpl().engineShown(param2IWallpaperEngine);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParcelFileDescriptor setWallpaper(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.wallpaper.IWallpaperConnection");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IWallpaperConnection.Stub.getDefaultImpl() != null)
            return IWallpaperConnection.Stub.getDefaultImpl().setWallpaper(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParcelFileDescriptor)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onWallpaperColorsChanged(WallpaperColors param2WallpaperColors, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.wallpaper.IWallpaperConnection");
          if (param2WallpaperColors != null) {
            parcel1.writeInt(1);
            param2WallpaperColors.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IWallpaperConnection.Stub.getDefaultImpl() != null) {
            IWallpaperConnection.Stub.getDefaultImpl().onWallpaperColorsChanged(param2WallpaperColors, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IWallpaperConnection param1IWallpaperConnection) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWallpaperConnection != null) {
          Proxy.sDefaultImpl = param1IWallpaperConnection;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWallpaperConnection getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
