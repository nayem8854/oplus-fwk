package android.service.wallpaper;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Trace;
import android.provider.Settings;
import android.util.Log;
import java.util.ArrayList;

public class OplusWallpaperServiceHelper {
  private static final int DARK_MODE_STATE_DEFAULT = 0;
  
  private static final int DARK_MODE_STATE_OPEN = 1;
  
  private static final float DARK_MODE_WINDOW_ALPHA = 0.76F;
  
  private static final String DARK_WALLPAPER_SETTINGS_STATE = "color_settings_dark_wallpaper";
  
  private static final int NIGHT_MODE = 32;
  
  private static final int NIGHT_MODE_FLAG = 48;
  
  private static final float NORMAL_MODE_WINDOW_ALPHA = 0.98F;
  
  private static final String TAG = "OplusWallpaperServiceHelper";
  
  private static boolean sNightMode;
  
  private final ArrayList<WallpaperService.Engine> mActiveEngines = new ArrayList<>();
  
  private DarkWallpaperSettingsChangeObserver mDarkWallpaperSettingsChangeObserver;
  
  private HandlerThread mHandlerThread;
  
  private boolean mIsRegisterDarkWallpaperContentObserver;
  
  public OplusWallpaperServiceHelper() {
    HandlerThread handlerThread = new HandlerThread("wallpaper-helper");
    handlerThread.start();
  }
  
  class DarkWallpaperSettingsChangeObserver extends ContentObserver {
    final OplusWallpaperServiceHelper this$0;
    
    public DarkWallpaperSettingsChangeObserver() {
      super(new Handler());
    }
    
    public void onChange(boolean param1Boolean) {
      super.onChange(param1Boolean);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("DarkWallpaperSettingsChangeObserver onChange selfChange = ");
      stringBuilder.append(param1Boolean);
      Log.d("OplusWallpaperServiceHelper", stringBuilder.toString());
      OplusWallpaperServiceHelper.this.updateEngineSurface();
    }
  }
  
  public void registerSetingsContentObserver(final Context appContext) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("registerSetingsContentObserver mIsRegisterDarkWallpaperContentObserver = ");
    stringBuilder.append(this.mIsRegisterDarkWallpaperContentObserver);
    Log.d("OplusWallpaperServiceHelper", stringBuilder.toString());
    if (appContext != null) {
      appContext = appContext.getApplicationContext();
      this.mHandlerThread.getThreadHandler().post(new Runnable() {
            final OplusWallpaperServiceHelper this$0;
            
            final Context val$appContext;
            
            public void run() {
              OplusWallpaperServiceHelper oplusWallpaperServiceHelper1 = OplusWallpaperServiceHelper.this;
              OplusWallpaperServiceHelper.access$102(oplusWallpaperServiceHelper1, new OplusWallpaperServiceHelper.DarkWallpaperSettingsChangeObserver());
              ContentResolver contentResolver = appContext.getContentResolver();
              Uri uri = Settings.Secure.getUriFor("color_settings_dark_wallpaper");
              OplusWallpaperServiceHelper oplusWallpaperServiceHelper2 = OplusWallpaperServiceHelper.this;
              OplusWallpaperServiceHelper.DarkWallpaperSettingsChangeObserver darkWallpaperSettingsChangeObserver = oplusWallpaperServiceHelper2.mDarkWallpaperSettingsChangeObserver;
              contentResolver.registerContentObserver(uri, true, darkWallpaperSettingsChangeObserver);
              OplusWallpaperServiceHelper.access$202(OplusWallpaperServiceHelper.this, true);
            }
          });
    } 
  }
  
  public void unregisterSettingsContentObserver(final Context appContext) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("unregisterSettingsContentObserver mIsRegisterDarkWallpaperContentObserver = ");
    stringBuilder.append(this.mIsRegisterDarkWallpaperContentObserver);
    Log.d("OplusWallpaperServiceHelper", stringBuilder.toString());
    if (appContext != null) {
      appContext = appContext.getApplicationContext();
      this.mHandlerThread.getThreadHandler().post(new Runnable() {
            final OplusWallpaperServiceHelper this$0;
            
            final Context val$appContext;
            
            public void run() {
              if (OplusWallpaperServiceHelper.this.mIsRegisterDarkWallpaperContentObserver) {
                appContext.getContentResolver().unregisterContentObserver(OplusWallpaperServiceHelper.this.mDarkWallpaperSettingsChangeObserver);
                OplusWallpaperServiceHelper.access$202(OplusWallpaperServiceHelper.this, false);
              } 
            }
          });
    } 
  }
  
  public void addEngine(WallpaperService.Engine paramEngine) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("addEngine engine= ");
    stringBuilder.append(paramEngine);
    Log.v("OplusWallpaperServiceHelper", stringBuilder.toString());
    this.mActiveEngines.add(paramEngine);
  }
  
  public void removeEngine(WallpaperService.Engine paramEngine) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("removeEngine engine= ");
    stringBuilder.append(paramEngine);
    Log.v("OplusWallpaperServiceHelper", stringBuilder.toString());
    this.mActiveEngines.remove(paramEngine);
  }
  
  private void updateEngineSurface() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("updateEngineSurface mActiveEngine size is: ");
    stringBuilder.append(this.mActiveEngines.size());
    Log.v("OplusWallpaperServiceHelper", stringBuilder.toString());
    for (byte b = 0; b < this.mActiveEngines.size(); b++) {
      WallpaperService.Engine engine = this.mActiveEngines.get(b);
      stringBuilder = new StringBuilder();
      stringBuilder.append("updateEngineSurface engine: ");
      stringBuilder.append(engine);
      Log.v("OplusWallpaperServiceHelper", stringBuilder.toString());
      engine.updateSurface(true, false, true);
    } 
  }
  
  public static float getDarkModeWallpaperWindowAlpha(Context paramContext) {
    boolean bool;
    Trace.traceBegin(8L, "OplusWallpaperServiceHelper.getDarkModeWallpaperWindowAlpha");
    if (isNightMode(paramContext) && isDarkModeWallpaperSwitchOpened(paramContext)) {
      bool = true;
    } else {
      bool = false;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("getDarkModeWallpaperWindowAlpha. nightMode = ");
    stringBuilder.append(bool);
    Log.i("OplusWallpaperServiceHelper", stringBuilder.toString());
    float f = 0.76F;
    if (!bool)
      f = 0.98F; 
    Trace.traceEnd(8L);
    return f;
  }
  
  private static boolean isNightMode(Context paramContext) {
    boolean bool = false;
    if (paramContext != null) {
      Resources resources = paramContext.getResources();
      if (resources != null) {
        Configuration configuration = paramContext.getResources().getConfiguration();
        if (configuration != null) {
          int i = configuration.uiMode & 0x30;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("isNightMode is ");
          if (32 == i) {
            bool1 = true;
          } else {
            bool1 = false;
          } 
          stringBuilder.append(bool1);
          Log.d("OplusWallpaperServiceHelper", stringBuilder.toString());
          boolean bool1 = bool;
          if (32 == i)
            bool1 = true; 
          return bool1;
        } 
      } 
    } 
    return false;
  }
  
  private static boolean isDarkModeWallpaperSwitchOpened(Context paramContext) {
    return getDarkWallpaperSwitchSettings(paramContext);
  }
  
  private static boolean getDarkWallpaperSwitchSettings(Context paramContext) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("getDarkWallpaperSwitchSettings context");
    stringBuilder.append(paramContext);
    Log.d("OplusWallpaperServiceHelper", stringBuilder.toString());
    boolean bool = false;
    if (paramContext != null) {
      if (Settings.Secure.getInt(paramContext.getContentResolver(), "color_settings_dark_wallpaper", 0) == 1)
        bool = true; 
      return bool;
    } 
    return false;
  }
}
