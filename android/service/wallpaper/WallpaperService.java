package android.service.wallpaper;

import android.annotation.SystemApi;
import android.app.Service;
import android.app.WallpaperColors;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.display.DisplayManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.os.Trace;
import android.util.Log;
import android.util.MergedConfiguration;
import android.view.Display;
import android.view.DisplayCutout;
import android.view.IWindow;
import android.view.IWindowSession;
import android.view.InputChannel;
import android.view.InputEvent;
import android.view.InputEventReceiver;
import android.view.InsetsSourceControl;
import android.view.InsetsState;
import android.view.MotionEvent;
import android.view.SurfaceControl;
import android.view.SurfaceHolder;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.view.WindowManagerGlobal;
import com.android.internal.os.HandlerCaller;
import com.android.internal.view.BaseIWindow;
import com.android.internal.view.BaseSurfaceHolder;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

public abstract class WallpaperService extends Service {
  static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static final int DO_ATTACH = 10;
  
  private static final int DO_DETACH = 20;
  
  private static final int DO_IN_AMBIENT_MODE = 50;
  
  private static final int DO_SET_DESIRED_SIZE = 30;
  
  private static final int DO_SET_DISPLAY_PADDING = 40;
  
  private static final int MSG_REQUEST_WALLPAPER_COLORS = 10050;
  
  private static final int MSG_SCALE = 10100;
  
  private static final int MSG_TOUCH_EVENT = 10040;
  
  private static final int MSG_UPDATE_SURFACE = 10000;
  
  private static final int MSG_VISIBILITY_CHANGED = 10010;
  
  private static final int MSG_WALLPAPER_COMMAND = 10025;
  
  private static final int MSG_WALLPAPER_OFFSETS = 10020;
  
  private static final int MSG_WINDOW_MOVED = 10035;
  
  private static final int MSG_WINDOW_RESIZED = 10030;
  
  private static final int NOTIFY_COLORS_RATE_LIMIT_MS = 1000;
  
  public static final String SERVICE_INTERFACE = "android.service.wallpaper.WallpaperService";
  
  public static final String SERVICE_META_DATA = "android.service.wallpaper";
  
  static final String TAG = "WallpaperService";
  
  private final ArrayList<Engine> mActiveEngines = new ArrayList<>();
  
  private boolean mIsFromSwitchingUser = false;
  
  class WallpaperCommand {
    String action;
    
    Bundle extras;
    
    boolean sync;
    
    int x;
    
    int y;
    
    int z;
  }
  
  class null extends BaseIWindow {
    final WallpaperService.Engine this$1;
    
    public void resized(Rect param1Rect1, Rect param1Rect2, Rect param1Rect3, Rect param1Rect4, boolean param1Boolean1, MergedConfiguration param1MergedConfiguration, Rect param1Rect5, boolean param1Boolean2, boolean param1Boolean3, int param1Int, DisplayCutout.ParcelableWrapper param1ParcelableWrapper) {
      HandlerCaller handlerCaller = this.this$1.mCaller;
      Message message = handlerCaller.obtainMessageI(10030, param1Boolean1);
      this.this$1.mCaller.sendMessage(message);
    }
    
    public void moved(int param1Int1, int param1Int2) {
      Message message = this.this$1.mCaller.obtainMessageII(10035, param1Int1, param1Int2);
      this.this$1.mCaller.sendMessage(message);
    }
    
    public void dispatchAppVisibility(boolean param1Boolean) {
      if (!this.this$1.mIWallpaperEngine.mIsPreview) {
        HandlerCaller handlerCaller = this.this$1.mCaller;
        Message message = handlerCaller.obtainMessageI(10010, param1Boolean);
        this.this$1.mCaller.sendMessage(message);
      } 
    }
    
    public void dispatchWallpaperOffsets(float param1Float1, float param1Float2, float param1Float3, float param1Float4, float param1Float5, boolean param1Boolean) {
      synchronized (this.this$1.mLock) {
        if (WallpaperService.DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Dispatch wallpaper offsets: ");
          stringBuilder.append(param1Float1);
          stringBuilder.append(", ");
          stringBuilder.append(param1Float2);
          Log.v("WallpaperService", stringBuilder.toString());
        } 
        this.this$1.mPendingXOffset = param1Float1;
        this.this$1.mPendingYOffset = param1Float2;
        this.this$1.mPendingXOffsetStep = param1Float3;
        this.this$1.mPendingYOffsetStep = param1Float4;
        if (param1Boolean)
          this.this$1.mPendingSync = true; 
        if (!this.this$1.mOffsetMessageEnqueued) {
          this.this$1.mOffsetMessageEnqueued = true;
          Message message1 = this.this$1.mCaller.obtainMessage(10020);
          this.this$1.mCaller.sendMessage(message1);
        } 
        Message message = this.this$1.mCaller.obtainMessageI(10100, Float.floatToIntBits(param1Float5));
        this.this$1.mCaller.sendMessage(message);
        return;
      } 
    }
    
    public void dispatchWallpaperCommand(String param1String, int param1Int1, int param1Int2, int param1Int3, Bundle param1Bundle, boolean param1Boolean) {
      synchronized (this.this$1.mLock) {
        if (WallpaperService.DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Dispatch wallpaper command: ");
          stringBuilder.append(param1Int1);
          stringBuilder.append(", ");
          stringBuilder.append(param1Int2);
          Log.v("WallpaperService", stringBuilder.toString());
        } 
        WallpaperService.WallpaperCommand wallpaperCommand = new WallpaperService.WallpaperCommand();
        this();
        wallpaperCommand.action = param1String;
        wallpaperCommand.x = param1Int1;
        wallpaperCommand.y = param1Int2;
        wallpaperCommand.z = param1Int3;
        wallpaperCommand.extras = param1Bundle;
        wallpaperCommand.sync = param1Boolean;
        Message message = this.this$1.mCaller.obtainMessage(10025);
        message.obj = wallpaperCommand;
        this.this$1.mCaller.sendMessage(message);
        return;
      } 
    }
  }
  
  class Engine {
    boolean mInitializing = true;
    
    float mZoom = 0.0F;
    
    int mWindowFlags = 16;
    
    int mWindowPrivateFlags = 4;
    
    int mCurWindowFlags = 16;
    
    int mCurWindowPrivateFlags = 4;
    
    final Rect mVisibleInsets = new Rect();
    
    final Rect mWinFrame = new Rect();
    
    final Rect mContentInsets = new Rect();
    
    final Rect mStableInsets = new Rect();
    
    final Rect mDispatchedContentInsets = new Rect();
    
    final Rect mDispatchedStableInsets = new Rect();
    
    final Rect mFinalSystemInsets = new Rect();
    
    final Rect mFinalStableInsets = new Rect();
    
    final Rect mBackdropFrame = new Rect();
    
    final DisplayCutout.ParcelableWrapper mDisplayCutout = new DisplayCutout.ParcelableWrapper();
    
    DisplayCutout mDispatchedDisplayCutout = DisplayCutout.NO_CUTOUT;
    
    final InsetsState mInsetsState = new InsetsState();
    
    final InsetsSourceControl[] mTempControls = new InsetsSourceControl[0];
    
    final MergedConfiguration mMergedConfiguration = new MergedConfiguration();
    
    private final Point mSurfaceSize = new Point();
    
    final WindowManager.LayoutParams mLayout = new WindowManager.LayoutParams();
    
    final Object mLock = new Object();
    
    private final Runnable mNotifyColorsChanged = new _$$Lambda$vsWBQpiXExY07tlrSzTqh4pNQAQ(this);
    
    SurfaceControl mSurfaceControl = new SurfaceControl();
    
    SurfaceControl mTmpSurfaceControl = new SurfaceControl();
    
    private boolean mIsSupportReportFinishDrawing = false;
    
    private boolean mReportedFinishDrawing = false;
    
    private boolean mIsFromSwitchingUser = false;
    
    final BaseSurfaceHolder mSurfaceHolder = (BaseSurfaceHolder)new Object(this);
    
    class WallpaperInputEventReceiver extends InputEventReceiver {
      final WallpaperService.Engine this$1;
      
      public WallpaperInputEventReceiver(InputChannel param2InputChannel, Looper param2Looper) {
        super(param2InputChannel, param2Looper);
      }
      
      public void onInputEvent(InputEvent param2InputEvent) {
        boolean bool1 = false;
        boolean bool2 = bool1;
        try {
          if (param2InputEvent instanceof MotionEvent) {
            bool2 = bool1;
            if ((param2InputEvent.getSource() & 0x2) != 0) {
              MotionEvent motionEvent = MotionEvent.obtainNoHistory((MotionEvent)param2InputEvent);
              WallpaperService.Engine.this.dispatchPointer(motionEvent);
              bool2 = true;
            } 
          } 
          return;
        } finally {
          finishInputEvent(param2InputEvent, false);
        } 
      }
    }
    
    final BaseIWindow mWindow = new BaseIWindow() {
        final WallpaperService.Engine this$1;
        
        public void resized(Rect param1Rect1, Rect param1Rect2, Rect param1Rect3, Rect param1Rect4, boolean param1Boolean1, MergedConfiguration param1MergedConfiguration, Rect param1Rect5, boolean param1Boolean2, boolean param1Boolean3, int param1Int, DisplayCutout.ParcelableWrapper param1ParcelableWrapper) {
          HandlerCaller handlerCaller = this.this$1.mCaller;
          Message message = handlerCaller.obtainMessageI(10030, param1Boolean1);
          this.this$1.mCaller.sendMessage(message);
        }
        
        public void moved(int param1Int1, int param1Int2) {
          Message message = this.this$1.mCaller.obtainMessageII(10035, param1Int1, param1Int2);
          this.this$1.mCaller.sendMessage(message);
        }
        
        public void dispatchAppVisibility(boolean param1Boolean) {
          if (!this.this$1.mIWallpaperEngine.mIsPreview) {
            HandlerCaller handlerCaller = this.this$1.mCaller;
            Message message = handlerCaller.obtainMessageI(10010, param1Boolean);
            this.this$1.mCaller.sendMessage(message);
          } 
        }
        
        public void dispatchWallpaperOffsets(float param1Float1, float param1Float2, float param1Float3, float param1Float4, float param1Float5, boolean param1Boolean) {
          synchronized (this.this$1.mLock) {
            if (WallpaperService.DEBUG) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Dispatch wallpaper offsets: ");
              stringBuilder.append(param1Float1);
              stringBuilder.append(", ");
              stringBuilder.append(param1Float2);
              Log.v("WallpaperService", stringBuilder.toString());
            } 
            this.this$1.mPendingXOffset = param1Float1;
            this.this$1.mPendingYOffset = param1Float2;
            this.this$1.mPendingXOffsetStep = param1Float3;
            this.this$1.mPendingYOffsetStep = param1Float4;
            if (param1Boolean)
              this.this$1.mPendingSync = true; 
            if (!this.this$1.mOffsetMessageEnqueued) {
              this.this$1.mOffsetMessageEnqueued = true;
              Message message1 = this.this$1.mCaller.obtainMessage(10020);
              this.this$1.mCaller.sendMessage(message1);
            } 
            Message message = this.this$1.mCaller.obtainMessageI(10100, Float.floatToIntBits(param1Float5));
            this.this$1.mCaller.sendMessage(message);
            return;
          } 
        }
        
        public void dispatchWallpaperCommand(String param1String, int param1Int1, int param1Int2, int param1Int3, Bundle param1Bundle, boolean param1Boolean) {
          synchronized (this.this$1.mLock) {
            if (WallpaperService.DEBUG) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Dispatch wallpaper command: ");
              stringBuilder.append(param1Int1);
              stringBuilder.append(", ");
              stringBuilder.append(param1Int2);
              Log.v("WallpaperService", stringBuilder.toString());
            } 
            WallpaperService.WallpaperCommand wallpaperCommand = new WallpaperService.WallpaperCommand();
            this();
            wallpaperCommand.action = param1String;
            wallpaperCommand.x = param1Int1;
            wallpaperCommand.y = param1Int2;
            wallpaperCommand.z = param1Int3;
            wallpaperCommand.extras = param1Bundle;
            wallpaperCommand.sync = param1Boolean;
            Message message = this.this$1.mCaller.obtainMessage(10025);
            message.obj = wallpaperCommand;
            this.this$1.mCaller.sendMessage(message);
            return;
          } 
        }
      };
    
    HandlerCaller mCaller;
    
    private final Supplier<Long> mClockFunction;
    
    IWallpaperConnection mConnection;
    
    boolean mCreated;
    
    int mCurHeight;
    
    int mCurWidth;
    
    boolean mDestroyed;
    
    private Display mDisplay;
    
    private Context mDisplayContext;
    
    private final DisplayManager.DisplayListener mDisplayListener;
    
    private int mDisplayState;
    
    boolean mDrawingAllowed;
    
    boolean mFixedSizeAllowed;
    
    int mFormat;
    
    private final Handler mHandler;
    
    int mHeight;
    
    WallpaperService.IWallpaperEngineWrapper mIWallpaperEngine;
    
    WallpaperInputEventReceiver mInputEventReceiver;
    
    boolean mIsCreating;
    
    boolean mIsInAmbientMode;
    
    private long mLastColorInvalidation;
    
    boolean mOffsetMessageEnqueued;
    
    boolean mOffsetsChanged;
    
    MotionEvent mPendingMove;
    
    boolean mPendingSync;
    
    float mPendingXOffset;
    
    float mPendingXOffsetStep;
    
    float mPendingYOffset;
    
    float mPendingYOffsetStep;
    
    boolean mReportedVisible;
    
    IWindowSession mSession;
    
    boolean mSurfaceCreated;
    
    int mType;
    
    boolean mVisible;
    
    int mWidth;
    
    IBinder mWindowToken;
    
    final WallpaperService this$0;
    
    public Engine() {
      this((Supplier<Long>)_$$Lambda$87Do_TfJA3qVM7QF6F_6BpQlQTA.INSTANCE, Handler.getMain());
    }
    
    public SurfaceHolder getSurfaceHolder() {
      return (SurfaceHolder)this.mSurfaceHolder;
    }
    
    public int getDesiredMinimumWidth() {
      return this.mIWallpaperEngine.mReqWidth;
    }
    
    public int getDesiredMinimumHeight() {
      return this.mIWallpaperEngine.mReqHeight;
    }
    
    public boolean isVisible() {
      return this.mReportedVisible;
    }
    
    public boolean isPreview() {
      return this.mIWallpaperEngine.mIsPreview;
    }
    
    @SystemApi
    public boolean isInAmbientMode() {
      return this.mIsInAmbientMode;
    }
    
    public boolean shouldZoomOutWallpaper() {
      return false;
    }
    
    public void setTouchEventsEnabled(boolean param1Boolean) {
      int i;
      if (param1Boolean) {
        i = this.mWindowFlags & 0xFFFFFFEF;
      } else {
        i = this.mWindowFlags | 0x10;
      } 
      this.mWindowFlags = i;
      if (this.mCreated)
        updateSurface(false, false, false); 
    }
    
    public void setOffsetNotificationsEnabled(boolean param1Boolean) {
      int i;
      if (param1Boolean) {
        i = this.mWindowPrivateFlags | 0x4;
      } else {
        i = this.mWindowPrivateFlags & 0xFFFFFFFB;
      } 
      this.mWindowPrivateFlags = i;
      if (this.mCreated)
        updateSurface(false, false, false); 
    }
    
    public void setFixedSizeAllowed(boolean param1Boolean) {
      this.mFixedSizeAllowed = param1Boolean;
    }
    
    public float getZoom() {
      return this.mZoom;
    }
    
    public void onCreate(SurfaceHolder param1SurfaceHolder) {}
    
    public void onDestroy() {}
    
    public void onVisibilityChanged(boolean param1Boolean) {}
    
    public void onApplyWindowInsets(WindowInsets param1WindowInsets) {}
    
    public void onTouchEvent(MotionEvent param1MotionEvent) {}
    
    public void onOffsetsChanged(float param1Float1, float param1Float2, float param1Float3, float param1Float4, int param1Int1, int param1Int2) {}
    
    public Bundle onCommand(String param1String, int param1Int1, int param1Int2, int param1Int3, Bundle param1Bundle, boolean param1Boolean) {
      if (WallpaperService.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onCommand: action = ");
        stringBuilder.append(param1String);
        Log.d("WallpaperService", stringBuilder.toString());
      } 
      if ("finishDrawing".equals(param1String)) {
        finishDrawing();
      } else if ("supportReportFinishDrawing".equals(param1String)) {
        this.mIsSupportReportFinishDrawing = true;
      } 
      return null;
    }
    
    @SystemApi
    public void onAmbientModeChanged(boolean param1Boolean, long param1Long) {}
    
    public void onDesiredSizeChanged(int param1Int1, int param1Int2) {}
    
    public void onSurfaceChanged(SurfaceHolder param1SurfaceHolder, int param1Int1, int param1Int2, int param1Int3) {}
    
    public void onSurfaceRedrawNeeded(SurfaceHolder param1SurfaceHolder) {}
    
    public void onSurfaceCreated(SurfaceHolder param1SurfaceHolder) {}
    
    public void onSurfaceDestroyed(SurfaceHolder param1SurfaceHolder) {}
    
    public void onZoomChanged(float param1Float) {}
    
    public void notifyColorsChanged() {
      long l = ((Long)this.mClockFunction.get()).longValue();
      if (l - this.mLastColorInvalidation < 1000L) {
        Log.w("WallpaperService", "This call has been deferred. You should only call notifyColorsChanged() once every 1.0 seconds.");
        if (!this.mHandler.hasCallbacks(this.mNotifyColorsChanged))
          this.mHandler.postDelayed(this.mNotifyColorsChanged, 1000L); 
        return;
      } 
      this.mLastColorInvalidation = l;
      this.mHandler.removeCallbacks(this.mNotifyColorsChanged);
      try {
        WallpaperColors wallpaperColors = onComputeColors();
        if (this.mConnection != null) {
          this.mConnection.onWallpaperColorsChanged(wallpaperColors, this.mDisplay.getDisplayId());
        } else {
          Log.w("WallpaperService", "Can't notify system because wallpaper connection was not established.");
        } 
      } catch (RemoteException remoteException) {
        Log.w("WallpaperService", "Can't notify system because wallpaper connection was lost.", (Throwable)remoteException);
      } 
    }
    
    public WallpaperColors onComputeColors() {
      return null;
    }
    
    public void setCreated(boolean param1Boolean) {
      this.mCreated = param1Boolean;
    }
    
    protected void dump(String param1String, FileDescriptor param1FileDescriptor, PrintWriter param1PrintWriter, String[] param1ArrayOfString) {
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mInitializing=");
      param1PrintWriter.print(this.mInitializing);
      param1PrintWriter.print(" mDestroyed=");
      param1PrintWriter.println(this.mDestroyed);
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mVisible=");
      param1PrintWriter.print(this.mVisible);
      param1PrintWriter.print(" mReportedVisible=");
      param1PrintWriter.println(this.mReportedVisible);
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mDisplay=");
      param1PrintWriter.println(this.mDisplay);
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mCreated=");
      param1PrintWriter.print(this.mCreated);
      param1PrintWriter.print(" mSurfaceCreated=");
      param1PrintWriter.print(this.mSurfaceCreated);
      param1PrintWriter.print(" mIsCreating=");
      param1PrintWriter.print(this.mIsCreating);
      param1PrintWriter.print(" mDrawingAllowed=");
      param1PrintWriter.println(this.mDrawingAllowed);
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mWidth=");
      param1PrintWriter.print(this.mWidth);
      param1PrintWriter.print(" mCurWidth=");
      param1PrintWriter.print(this.mCurWidth);
      param1PrintWriter.print(" mHeight=");
      param1PrintWriter.print(this.mHeight);
      param1PrintWriter.print(" mCurHeight=");
      param1PrintWriter.println(this.mCurHeight);
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mType=");
      param1PrintWriter.print(this.mType);
      param1PrintWriter.print(" mWindowFlags=");
      param1PrintWriter.print(this.mWindowFlags);
      param1PrintWriter.print(" mCurWindowFlags=");
      param1PrintWriter.println(this.mCurWindowFlags);
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mWindowPrivateFlags=");
      param1PrintWriter.print(this.mWindowPrivateFlags);
      param1PrintWriter.print(" mCurWindowPrivateFlags=");
      param1PrintWriter.println(this.mCurWindowPrivateFlags);
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mVisibleInsets=");
      param1PrintWriter.print(this.mVisibleInsets.toShortString());
      param1PrintWriter.print(" mWinFrame=");
      param1PrintWriter.print(this.mWinFrame.toShortString());
      param1PrintWriter.print(" mContentInsets=");
      param1PrintWriter.println(this.mContentInsets.toShortString());
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mConfiguration=");
      param1PrintWriter.println(this.mMergedConfiguration.getMergedConfiguration());
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mLayout=");
      param1PrintWriter.println(this.mLayout);
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mZoom=");
      param1PrintWriter.println(this.mZoom);
      synchronized (this.mLock) {
        param1PrintWriter.print(param1String);
        param1PrintWriter.print("mPendingXOffset=");
        param1PrintWriter.print(this.mPendingXOffset);
        param1PrintWriter.print(" mPendingXOffset=");
        param1PrintWriter.println(this.mPendingXOffset);
        param1PrintWriter.print(param1String);
        param1PrintWriter.print("mPendingXOffsetStep=");
        param1PrintWriter.print(this.mPendingXOffsetStep);
        param1PrintWriter.print(" mPendingXOffsetStep=");
        param1PrintWriter.println(this.mPendingXOffsetStep);
        param1PrintWriter.print(param1String);
        param1PrintWriter.print("mOffsetMessageEnqueued=");
        param1PrintWriter.print(this.mOffsetMessageEnqueued);
        param1PrintWriter.print(" mPendingSync=");
        param1PrintWriter.println(this.mPendingSync);
        if (this.mPendingMove != null) {
          param1PrintWriter.print(param1String);
          param1PrintWriter.print("mPendingMove=");
          param1PrintWriter.println(this.mPendingMove);
        } 
        return;
      } 
    }
    
    public void setZoom(float param1Float) {
      if (WallpaperService.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("set zoom received: ");
        stringBuilder.append(param1Float);
        Log.v("WallpaperService", stringBuilder.toString());
      } 
      boolean bool = false;
      synchronized (this.mLock) {
        if (WallpaperService.DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("mZoom: ");
          stringBuilder.append(this.mZoom);
          stringBuilder.append(" updated: ");
          stringBuilder.append(param1Float);
          Log.v("WallpaperService", stringBuilder.toString());
        } 
        if (this.mIsInAmbientMode)
          this.mZoom = 0.0F; 
        if (Float.compare(param1Float, this.mZoom) != 0) {
          this.mZoom = param1Float;
          bool = true;
        } 
        if (WallpaperService.DEBUG) {
          null = new StringBuilder();
          null.append("setZoom updated? ");
          null.append(bool);
          Log.v("WallpaperService", null.toString());
        } 
        if (bool && !this.mDestroyed)
          onZoomChanged(this.mZoom); 
        return;
      } 
    }
    
    private void dispatchPointer(MotionEvent param1MotionEvent) {
      if (param1MotionEvent.isTouchEvent()) {
        synchronized (this.mLock) {
          if (param1MotionEvent.getAction() == 2) {
            this.mPendingMove = param1MotionEvent;
          } else {
            this.mPendingMove = null;
          } 
          Message message = this.mCaller.obtainMessageO(10040, param1MotionEvent);
          this.mCaller.sendMessage(message);
        } 
      } else {
        param1MotionEvent.recycle();
      } 
    }
    
    void updateSurface(boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mDestroyed : Z
      //   4: ifeq -> 17
      //   7: ldc_w 'WallpaperService'
      //   10: ldc_w 'Ignoring updateSurface: destroyed'
      //   13: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   16: pop
      //   17: aload_0
      //   18: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   21: aload_0
      //   22: getfield mDisplayContext : Landroid/content/Context;
      //   25: invokestatic getDarkModeWallpaperWindowAlpha : (Landroid/content/Context;)F
      //   28: putfield alpha : F
      //   31: iconst_0
      //   32: istore #4
      //   34: aload_0
      //   35: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   38: invokevirtual getRequestedWidth : ()I
      //   41: istore #5
      //   43: iload #5
      //   45: ifgt -> 54
      //   48: iconst_m1
      //   49: istore #5
      //   51: goto -> 57
      //   54: iconst_1
      //   55: istore #4
      //   57: aload_0
      //   58: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   61: invokevirtual getRequestedHeight : ()I
      //   64: istore #6
      //   66: iload #6
      //   68: ifgt -> 85
      //   71: iconst_m1
      //   72: istore #7
      //   74: iload #4
      //   76: istore #6
      //   78: iload #7
      //   80: istore #4
      //   82: goto -> 92
      //   85: iload #6
      //   87: istore #4
      //   89: iconst_1
      //   90: istore #6
      //   92: aload_0
      //   93: getfield mCreated : Z
      //   96: iconst_1
      //   97: ixor
      //   98: istore #8
      //   100: aload_0
      //   101: getfield mSurfaceCreated : Z
      //   104: iconst_1
      //   105: ixor
      //   106: istore #9
      //   108: aload_0
      //   109: getfield mFormat : I
      //   112: aload_0
      //   113: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   116: invokevirtual getRequestedFormat : ()I
      //   119: if_icmpeq -> 128
      //   122: iconst_1
      //   123: istore #10
      //   125: goto -> 131
      //   128: iconst_0
      //   129: istore #10
      //   131: aload_0
      //   132: getfield mWidth : I
      //   135: iload #5
      //   137: if_icmpne -> 158
      //   140: aload_0
      //   141: getfield mHeight : I
      //   144: iload #4
      //   146: if_icmpeq -> 152
      //   149: goto -> 158
      //   152: iconst_0
      //   153: istore #11
      //   155: goto -> 161
      //   158: iconst_1
      //   159: istore #11
      //   161: aload_0
      //   162: getfield mCreated : Z
      //   165: istore #12
      //   167: aload_0
      //   168: getfield mType : I
      //   171: aload_0
      //   172: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   175: invokevirtual getRequestedType : ()I
      //   178: if_icmpeq -> 187
      //   181: iconst_1
      //   182: istore #7
      //   184: goto -> 190
      //   187: iconst_0
      //   188: istore #7
      //   190: aload_0
      //   191: getfield mCurWindowFlags : I
      //   194: aload_0
      //   195: getfield mWindowFlags : I
      //   198: if_icmpne -> 221
      //   201: aload_0
      //   202: getfield mCurWindowPrivateFlags : I
      //   205: aload_0
      //   206: getfield mWindowPrivateFlags : I
      //   209: if_icmpeq -> 215
      //   212: goto -> 221
      //   215: iconst_0
      //   216: istore #13
      //   218: goto -> 224
      //   221: iconst_1
      //   222: istore #13
      //   224: iload_1
      //   225: ifne -> 278
      //   228: iload #8
      //   230: ifne -> 278
      //   233: iload #9
      //   235: ifne -> 278
      //   238: iload #10
      //   240: ifne -> 278
      //   243: iload #11
      //   245: ifne -> 278
      //   248: iload #7
      //   250: ifne -> 278
      //   253: iload #13
      //   255: ifne -> 278
      //   258: iload_3
      //   259: ifne -> 278
      //   262: aload_0
      //   263: getfield mIWallpaperEngine : Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;
      //   266: getfield mShownReported : Z
      //   269: ifne -> 275
      //   272: goto -> 278
      //   275: goto -> 3278
      //   278: getstatic android/service/wallpaper/WallpaperService.DEBUG : Z
      //   281: ifeq -> 359
      //   284: new java/lang/StringBuilder
      //   287: dup
      //   288: invokespecial <init> : ()V
      //   291: astore #14
      //   293: aload #14
      //   295: ldc_w 'Changes: creating='
      //   298: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   301: pop
      //   302: aload #14
      //   304: iload #8
      //   306: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   309: pop
      //   310: aload #14
      //   312: ldc_w ' format='
      //   315: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   318: pop
      //   319: aload #14
      //   321: iload #10
      //   323: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   326: pop
      //   327: aload #14
      //   329: ldc_w ' size='
      //   332: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   335: pop
      //   336: aload #14
      //   338: iload #11
      //   340: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   343: pop
      //   344: ldc_w 'WallpaperService'
      //   347: aload #14
      //   349: invokevirtual toString : ()Ljava/lang/String;
      //   352: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
      //   355: pop
      //   356: goto -> 359
      //   359: aload_0
      //   360: iload #5
      //   362: putfield mWidth : I
      //   365: aload_0
      //   366: iload #4
      //   368: putfield mHeight : I
      //   371: aload_0
      //   372: aload_0
      //   373: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   376: invokevirtual getRequestedFormat : ()I
      //   379: putfield mFormat : I
      //   382: aload_0
      //   383: aload_0
      //   384: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   387: invokevirtual getRequestedType : ()I
      //   390: putfield mType : I
      //   393: aload_0
      //   394: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   397: iconst_0
      //   398: putfield x : I
      //   401: aload_0
      //   402: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   405: iconst_0
      //   406: putfield y : I
      //   409: aload_0
      //   410: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   413: iload #5
      //   415: putfield width : I
      //   418: aload_0
      //   419: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   422: iload #4
      //   424: putfield height : I
      //   427: aload_0
      //   428: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   431: aload_0
      //   432: getfield mFormat : I
      //   435: putfield format : I
      //   438: aload_0
      //   439: aload_0
      //   440: getfield mWindowFlags : I
      //   443: putfield mCurWindowFlags : I
      //   446: aload_0
      //   447: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   450: aload_0
      //   451: getfield mWindowFlags : I
      //   454: sipush #512
      //   457: ior
      //   458: ldc_w 65536
      //   461: ior
      //   462: sipush #256
      //   465: ior
      //   466: bipush #8
      //   468: ior
      //   469: putfield flags : I
      //   472: aload_0
      //   473: aload_0
      //   474: getfield mWindowPrivateFlags : I
      //   477: putfield mCurWindowPrivateFlags : I
      //   480: aload_0
      //   481: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   484: aload_0
      //   485: getfield mWindowPrivateFlags : I
      //   488: putfield privateFlags : I
      //   491: aload_0
      //   492: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   495: aload_0
      //   496: getfield mType : I
      //   499: putfield memoryType : I
      //   502: aload_0
      //   503: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   506: aload_0
      //   507: getfield mWindowToken : Landroid/os/IBinder;
      //   510: putfield token : Landroid/os/IBinder;
      //   513: aload_0
      //   514: getfield mCreated : Z
      //   517: istore_1
      //   518: iload_1
      //   519: ifne -> 800
      //   522: aload_0
      //   523: getfield this$0 : Landroid/service/wallpaper/WallpaperService;
      //   526: getstatic com/android/internal/R$styleable.Window : [I
      //   529: invokevirtual obtainStyledAttributes : ([I)Landroid/content/res/TypedArray;
      //   532: astore #14
      //   534: aload #14
      //   536: invokevirtual recycle : ()V
      //   539: aload_0
      //   540: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   543: aload_0
      //   544: getfield mIWallpaperEngine : Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;
      //   547: getfield mWindowType : I
      //   550: putfield type : I
      //   553: aload_0
      //   554: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   557: ldc_w 8388659
      //   560: putfield gravity : I
      //   563: aload_0
      //   564: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   567: iconst_0
      //   568: invokevirtual setFitInsetsTypes : (I)V
      //   571: aload_0
      //   572: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   575: aload_0
      //   576: getfield this$0 : Landroid/service/wallpaper/WallpaperService;
      //   579: invokevirtual getClass : ()Ljava/lang/Class;
      //   582: invokevirtual getName : ()Ljava/lang/String;
      //   585: invokevirtual setTitle : (Ljava/lang/CharSequence;)V
      //   588: aload_0
      //   589: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   592: ldc_w 16974606
      //   595: putfield windowAnimations : I
      //   598: new android/view/InputChannel
      //   601: astore #14
      //   603: aload #14
      //   605: invokespecial <init> : ()V
      //   608: aload_0
      //   609: getfield mSession : Landroid/view/IWindowSession;
      //   612: astore #15
      //   614: aload_0
      //   615: getfield mWindow : Lcom/android/internal/view/BaseIWindow;
      //   618: astore #16
      //   620: aload_0
      //   621: getfield mWindow : Lcom/android/internal/view/BaseIWindow;
      //   624: getfield mSeq : I
      //   627: istore #13
      //   629: aload_0
      //   630: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   633: astore #17
      //   635: aload_0
      //   636: getfield mDisplay : Landroid/view/Display;
      //   639: astore #18
      //   641: aload #18
      //   643: invokevirtual getDisplayId : ()I
      //   646: istore #7
      //   648: aload_0
      //   649: getfield mWinFrame : Landroid/graphics/Rect;
      //   652: astore #19
      //   654: aload_0
      //   655: getfield mContentInsets : Landroid/graphics/Rect;
      //   658: astore #20
      //   660: aload_0
      //   661: getfield mStableInsets : Landroid/graphics/Rect;
      //   664: astore #21
      //   666: aload_0
      //   667: getfield mDisplayCutout : Landroid/view/DisplayCutout$ParcelableWrapper;
      //   670: astore #18
      //   672: aload_0
      //   673: getfield mInsetsState : Landroid/view/InsetsState;
      //   676: astore #22
      //   678: aload_0
      //   679: getfield mTempControls : [Landroid/view/InsetsSourceControl;
      //   682: astore #23
      //   684: aload #15
      //   686: aload #16
      //   688: iload #13
      //   690: aload #17
      //   692: iconst_0
      //   693: iload #7
      //   695: aload #19
      //   697: aload #20
      //   699: aload #21
      //   701: aload #18
      //   703: aload #14
      //   705: aload #22
      //   707: aload #23
      //   709: invokeinterface addToDisplay : (Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/view/DisplayCutout$ParcelableWrapper;Landroid/view/InputChannel;Landroid/view/InsetsState;[Landroid/view/InsetsSourceControl;)I
      //   714: ifge -> 728
      //   717: ldc_w 'WallpaperService'
      //   720: ldc_w 'Failed to add window while updating wallpaper surface.'
      //   723: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   726: pop
      //   727: return
      //   728: aload_0
      //   729: getfield mSession : Landroid/view/IWindowSession;
      //   732: aload_0
      //   733: getfield mWindow : Lcom/android/internal/view/BaseIWindow;
      //   736: aload_0
      //   737: invokevirtual shouldZoomOutWallpaper : ()Z
      //   740: invokeinterface setShouldZoomOutWallpaper : (Landroid/os/IBinder;Z)V
      //   745: aload_0
      //   746: iconst_1
      //   747: putfield mCreated : Z
      //   750: new android/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver
      //   753: astore #15
      //   755: aload #15
      //   757: aload_0
      //   758: aload #14
      //   760: invokestatic myLooper : ()Landroid/os/Looper;
      //   763: invokespecial <init> : (Landroid/service/wallpaper/WallpaperService$Engine;Landroid/view/InputChannel;Landroid/os/Looper;)V
      //   766: aload_0
      //   767: aload #15
      //   769: putfield mInputEventReceiver : Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;
      //   772: goto -> 800
      //   775: astore #14
      //   777: goto -> 3163
      //   780: astore #14
      //   782: goto -> 3163
      //   785: astore #14
      //   787: goto -> 3163
      //   790: astore #14
      //   792: goto -> 3163
      //   795: astore #14
      //   797: goto -> 3163
      //   800: iload #11
      //   802: istore_1
      //   803: aload_0
      //   804: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   807: getfield mSurfaceLock : Ljava/util/concurrent/locks/ReentrantLock;
      //   810: invokevirtual lock : ()V
      //   813: aload_0
      //   814: iconst_1
      //   815: putfield mDrawingAllowed : Z
      //   818: iload #6
      //   820: ifne -> 848
      //   823: aload_0
      //   824: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   827: getfield surfaceInsets : Landroid/graphics/Rect;
      //   830: aload_0
      //   831: getfield mIWallpaperEngine : Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;
      //   834: getfield mDisplayPadding : Landroid/graphics/Rect;
      //   837: invokevirtual set : (Landroid/graphics/Rect;)V
      //   840: goto -> 862
      //   843: astore #14
      //   845: goto -> 3163
      //   848: aload_0
      //   849: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   852: getfield surfaceInsets : Landroid/graphics/Rect;
      //   855: iconst_0
      //   856: iconst_0
      //   857: iconst_0
      //   858: iconst_0
      //   859: invokevirtual set : (IIII)V
      //   862: aload_0
      //   863: getfield mSession : Landroid/view/IWindowSession;
      //   866: astore #15
      //   868: aload_0
      //   869: getfield mWindow : Lcom/android/internal/view/BaseIWindow;
      //   872: astore #22
      //   874: aload_0
      //   875: getfield mWindow : Lcom/android/internal/view/BaseIWindow;
      //   878: getfield mSeq : I
      //   881: istore #24
      //   883: aload_0
      //   884: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   887: astore #14
      //   889: aload_0
      //   890: getfield mWidth : I
      //   893: istore #13
      //   895: aload_0
      //   896: getfield mHeight : I
      //   899: istore #7
      //   901: aload_0
      //   902: getfield mWinFrame : Landroid/graphics/Rect;
      //   905: astore #18
      //   907: aload_0
      //   908: getfield mContentInsets : Landroid/graphics/Rect;
      //   911: astore #20
      //   913: aload_0
      //   914: getfield mVisibleInsets : Landroid/graphics/Rect;
      //   917: astore #21
      //   919: aload_0
      //   920: getfield mStableInsets : Landroid/graphics/Rect;
      //   923: astore #16
      //   925: aload_0
      //   926: getfield mBackdropFrame : Landroid/graphics/Rect;
      //   929: astore #19
      //   931: aload_0
      //   932: getfield mDisplayCutout : Landroid/view/DisplayCutout$ParcelableWrapper;
      //   935: astore #23
      //   937: aload_0
      //   938: getfield mMergedConfiguration : Landroid/util/MergedConfiguration;
      //   941: astore #17
      //   943: aload_0
      //   944: getfield mSurfaceControl : Landroid/view/SurfaceControl;
      //   947: astore #25
      //   949: aload #15
      //   951: aload #22
      //   953: iload #24
      //   955: aload #14
      //   957: iload #13
      //   959: iload #7
      //   961: iconst_0
      //   962: iconst_0
      //   963: ldc2_w -1
      //   966: aload #18
      //   968: aload #20
      //   970: aload #21
      //   972: aload #16
      //   974: aload #19
      //   976: aload #23
      //   978: aload #17
      //   980: aload #25
      //   982: aload_0
      //   983: getfield mInsetsState : Landroid/view/InsetsState;
      //   986: aload_0
      //   987: getfield mTempControls : [Landroid/view/InsetsSourceControl;
      //   990: aload_0
      //   991: getfield mSurfaceSize : Landroid/graphics/Point;
      //   994: aload_0
      //   995: getfield mTmpSurfaceControl : Landroid/view/SurfaceControl;
      //   998: invokeinterface relayout : (Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IIIIJLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/view/DisplayCutout$ParcelableWrapper;Landroid/util/MergedConfiguration;Landroid/view/SurfaceControl;Landroid/view/InsetsState;[Landroid/view/InsetsSourceControl;Landroid/graphics/Point;Landroid/view/SurfaceControl;)I
      //   1003: istore #26
      //   1005: aload_0
      //   1006: getfield mSurfaceControl : Landroid/view/SurfaceControl;
      //   1009: invokevirtual isValid : ()Z
      //   1012: istore #11
      //   1014: iload #11
      //   1016: ifeq -> 1048
      //   1019: aload_0
      //   1020: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   1023: getfield mSurface : Landroid/view/Surface;
      //   1026: aload_0
      //   1027: getfield mSurfaceControl : Landroid/view/SurfaceControl;
      //   1030: invokevirtual copyFrom : (Landroid/view/SurfaceControl;)V
      //   1033: aload_0
      //   1034: getfield mSurfaceControl : Landroid/view/SurfaceControl;
      //   1037: invokevirtual release : ()V
      //   1040: goto -> 1048
      //   1043: astore #14
      //   1045: goto -> 3163
      //   1048: getstatic android/service/wallpaper/WallpaperService.DEBUG : Z
      //   1051: istore #11
      //   1053: iload #11
      //   1055: ifeq -> 1133
      //   1058: new java/lang/StringBuilder
      //   1061: astore #14
      //   1063: aload #14
      //   1065: invokespecial <init> : ()V
      //   1068: aload #14
      //   1070: ldc_w 'New surface: '
      //   1073: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1076: pop
      //   1077: aload #14
      //   1079: aload_0
      //   1080: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   1083: getfield mSurface : Landroid/view/Surface;
      //   1086: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   1089: pop
      //   1090: aload #14
      //   1092: ldc_w ', frame='
      //   1095: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1098: pop
      //   1099: aload #14
      //   1101: aload_0
      //   1102: getfield mWinFrame : Landroid/graphics/Rect;
      //   1105: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   1108: pop
      //   1109: aload #14
      //   1111: invokevirtual toString : ()Ljava/lang/String;
      //   1114: astore #14
      //   1116: ldc_w 'WallpaperService'
      //   1119: aload #14
      //   1121: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
      //   1124: pop
      //   1125: goto -> 1133
      //   1128: astore #14
      //   1130: goto -> 3163
      //   1133: aload_0
      //   1134: getfield mWinFrame : Landroid/graphics/Rect;
      //   1137: invokevirtual width : ()I
      //   1140: istore #7
      //   1142: aload_0
      //   1143: getfield mWinFrame : Landroid/graphics/Rect;
      //   1146: invokevirtual height : ()I
      //   1149: istore #13
      //   1151: iload #6
      //   1153: ifne -> 1438
      //   1156: aload_0
      //   1157: getfield mIWallpaperEngine : Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;
      //   1160: getfield mDisplayPadding : Landroid/graphics/Rect;
      //   1163: astore #14
      //   1165: aload #14
      //   1167: getfield left : I
      //   1170: istore #5
      //   1172: aload #14
      //   1174: getfield right : I
      //   1177: istore #6
      //   1179: aload #14
      //   1181: getfield top : I
      //   1184: istore #24
      //   1186: aload #14
      //   1188: getfield bottom : I
      //   1191: istore #4
      //   1193: aload_0
      //   1194: getfield mContentInsets : Landroid/graphics/Rect;
      //   1197: astore #15
      //   1199: aload #15
      //   1201: aload #15
      //   1203: getfield left : I
      //   1206: aload #14
      //   1208: getfield left : I
      //   1211: iadd
      //   1212: putfield left : I
      //   1215: aload_0
      //   1216: getfield mContentInsets : Landroid/graphics/Rect;
      //   1219: astore #15
      //   1221: aload #15
      //   1223: aload #15
      //   1225: getfield top : I
      //   1228: aload #14
      //   1230: getfield top : I
      //   1233: iadd
      //   1234: putfield top : I
      //   1237: aload_0
      //   1238: getfield mContentInsets : Landroid/graphics/Rect;
      //   1241: astore #15
      //   1243: aload #15
      //   1245: aload #15
      //   1247: getfield right : I
      //   1250: aload #14
      //   1252: getfield right : I
      //   1255: iadd
      //   1256: putfield right : I
      //   1259: aload_0
      //   1260: getfield mContentInsets : Landroid/graphics/Rect;
      //   1263: astore #15
      //   1265: aload #15
      //   1267: aload #15
      //   1269: getfield bottom : I
      //   1272: aload #14
      //   1274: getfield bottom : I
      //   1277: iadd
      //   1278: putfield bottom : I
      //   1281: aload_0
      //   1282: getfield mStableInsets : Landroid/graphics/Rect;
      //   1285: astore #15
      //   1287: aload #15
      //   1289: aload #15
      //   1291: getfield left : I
      //   1294: aload #14
      //   1296: getfield left : I
      //   1299: iadd
      //   1300: putfield left : I
      //   1303: aload_0
      //   1304: getfield mStableInsets : Landroid/graphics/Rect;
      //   1307: astore #15
      //   1309: aload #15
      //   1311: aload #15
      //   1313: getfield top : I
      //   1316: aload #14
      //   1318: getfield top : I
      //   1321: iadd
      //   1322: putfield top : I
      //   1325: aload_0
      //   1326: getfield mStableInsets : Landroid/graphics/Rect;
      //   1329: astore #15
      //   1331: aload #15
      //   1333: aload #15
      //   1335: getfield right : I
      //   1338: aload #14
      //   1340: getfield right : I
      //   1343: iadd
      //   1344: putfield right : I
      //   1347: aload_0
      //   1348: getfield mStableInsets : Landroid/graphics/Rect;
      //   1351: astore #15
      //   1353: aload #15
      //   1355: aload #15
      //   1357: getfield bottom : I
      //   1360: aload #14
      //   1362: getfield bottom : I
      //   1365: iadd
      //   1366: putfield bottom : I
      //   1369: aload_0
      //   1370: getfield mDisplayCutout : Landroid/view/DisplayCutout$ParcelableWrapper;
      //   1373: aload_0
      //   1374: getfield mDisplayCutout : Landroid/view/DisplayCutout$ParcelableWrapper;
      //   1377: invokevirtual get : ()Landroid/view/DisplayCutout;
      //   1380: aload #14
      //   1382: getfield left : I
      //   1385: ineg
      //   1386: aload #14
      //   1388: getfield top : I
      //   1391: ineg
      //   1392: aload #14
      //   1394: getfield right : I
      //   1397: ineg
      //   1398: aload #14
      //   1400: getfield bottom : I
      //   1403: ineg
      //   1404: invokevirtual inset : (IIII)Landroid/view/DisplayCutout;
      //   1407: invokevirtual set : (Landroid/view/DisplayCutout;)V
      //   1410: iload #13
      //   1412: iload #24
      //   1414: iload #4
      //   1416: iadd
      //   1417: iadd
      //   1418: istore #4
      //   1420: iload #7
      //   1422: iload #5
      //   1424: iload #6
      //   1426: iadd
      //   1427: iadd
      //   1428: istore #5
      //   1430: goto -> 1438
      //   1433: astore #14
      //   1435: goto -> 3163
      //   1438: aload_0
      //   1439: getfield mCurWidth : I
      //   1442: istore #6
      //   1444: iload #6
      //   1446: iload #5
      //   1448: if_icmpeq -> 1470
      //   1451: iconst_1
      //   1452: istore #11
      //   1454: aload_0
      //   1455: iload #5
      //   1457: putfield mCurWidth : I
      //   1460: iconst_1
      //   1461: istore_1
      //   1462: goto -> 1470
      //   1465: astore #14
      //   1467: goto -> 3163
      //   1470: aload_0
      //   1471: getfield mCurHeight : I
      //   1474: istore #6
      //   1476: iload #6
      //   1478: iload #4
      //   1480: if_icmpeq -> 1497
      //   1483: iconst_1
      //   1484: istore_1
      //   1485: iconst_1
      //   1486: istore #11
      //   1488: aload_0
      //   1489: iload #4
      //   1491: putfield mCurHeight : I
      //   1494: goto -> 1497
      //   1497: getstatic android/service/wallpaper/WallpaperService.DEBUG : Z
      //   1500: istore #11
      //   1502: iload #11
      //   1504: ifeq -> 1588
      //   1507: iload_1
      //   1508: istore #11
      //   1510: new java/lang/StringBuilder
      //   1513: astore #14
      //   1515: iload_1
      //   1516: istore #11
      //   1518: aload #14
      //   1520: invokespecial <init> : ()V
      //   1523: iload_1
      //   1524: istore #11
      //   1526: aload #14
      //   1528: ldc_w 'Wallpaper size has changed: ('
      //   1531: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1534: pop
      //   1535: iload_1
      //   1536: istore #11
      //   1538: aload #14
      //   1540: aload_0
      //   1541: getfield mCurWidth : I
      //   1544: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   1547: pop
      //   1548: iload_1
      //   1549: istore #11
      //   1551: aload #14
      //   1553: ldc_w ', '
      //   1556: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1559: pop
      //   1560: iload_1
      //   1561: istore #11
      //   1563: aload #14
      //   1565: aload_0
      //   1566: getfield mCurHeight : I
      //   1569: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   1572: pop
      //   1573: iload_1
      //   1574: istore #11
      //   1576: ldc_w 'WallpaperService'
      //   1579: aload #14
      //   1581: invokevirtual toString : ()Ljava/lang/String;
      //   1584: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
      //   1587: pop
      //   1588: aload_0
      //   1589: getfield mDispatchedContentInsets : Landroid/graphics/Rect;
      //   1592: aload_0
      //   1593: getfield mContentInsets : Landroid/graphics/Rect;
      //   1596: invokevirtual equals : (Ljava/lang/Object;)Z
      //   1599: ifne -> 1608
      //   1602: iconst_1
      //   1603: istore #7
      //   1605: goto -> 1611
      //   1608: iconst_0
      //   1609: istore #7
      //   1611: aload_0
      //   1612: getfield mDispatchedStableInsets : Landroid/graphics/Rect;
      //   1615: aload_0
      //   1616: getfield mStableInsets : Landroid/graphics/Rect;
      //   1619: invokevirtual equals : (Ljava/lang/Object;)Z
      //   1622: ifne -> 1631
      //   1625: iconst_1
      //   1626: istore #13
      //   1628: goto -> 1634
      //   1631: iconst_0
      //   1632: istore #13
      //   1634: aload_0
      //   1635: getfield mDispatchedDisplayCutout : Landroid/view/DisplayCutout;
      //   1638: aload_0
      //   1639: getfield mDisplayCutout : Landroid/view/DisplayCutout$ParcelableWrapper;
      //   1642: invokevirtual get : ()Landroid/view/DisplayCutout;
      //   1645: invokevirtual equals : (Ljava/lang/Object;)Z
      //   1648: ifne -> 1657
      //   1651: iconst_1
      //   1652: istore #24
      //   1654: goto -> 1660
      //   1657: iconst_0
      //   1658: istore #24
      //   1660: aload_0
      //   1661: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   1664: iload #5
      //   1666: iload #4
      //   1668: invokevirtual setSurfaceFrameSize : (II)V
      //   1671: aload_0
      //   1672: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   1675: getfield mSurfaceLock : Ljava/util/concurrent/locks/ReentrantLock;
      //   1678: invokevirtual unlock : ()V
      //   1681: aload_0
      //   1682: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   1685: getfield mSurface : Landroid/view/Surface;
      //   1688: invokevirtual isValid : ()Z
      //   1691: istore #11
      //   1693: iload #11
      //   1695: ifne -> 1728
      //   1698: iload_1
      //   1699: istore #11
      //   1701: aload_0
      //   1702: invokevirtual reportSurfaceDestroyed : ()V
      //   1705: iload_1
      //   1706: istore #11
      //   1708: getstatic android/service/wallpaper/WallpaperService.DEBUG : Z
      //   1711: ifeq -> 1727
      //   1714: iload_1
      //   1715: istore #11
      //   1717: ldc_w 'WallpaperService'
      //   1720: ldc_w 'Layout: Surface destroyed'
      //   1723: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
      //   1726: pop
      //   1727: return
      //   1728: iconst_0
      //   1729: istore #6
      //   1731: aload_0
      //   1732: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   1735: invokevirtual ungetCallbacks : ()V
      //   1738: iload #9
      //   1740: ifeq -> 1890
      //   1743: aload_0
      //   1744: iconst_1
      //   1745: putfield mIsCreating : Z
      //   1748: iconst_1
      //   1749: istore #27
      //   1751: getstatic android/service/wallpaper/WallpaperService.DEBUG : Z
      //   1754: ifeq -> 1814
      //   1757: new java/lang/StringBuilder
      //   1760: astore #14
      //   1762: aload #14
      //   1764: invokespecial <init> : ()V
      //   1767: aload #14
      //   1769: ldc_w 'onSurfaceCreated('
      //   1772: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1775: pop
      //   1776: aload #14
      //   1778: aload_0
      //   1779: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   1782: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   1785: pop
      //   1786: aload #14
      //   1788: ldc_w '): '
      //   1791: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1794: pop
      //   1795: aload #14
      //   1797: aload_0
      //   1798: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   1801: pop
      //   1802: ldc_w 'WallpaperService'
      //   1805: aload #14
      //   1807: invokevirtual toString : ()Ljava/lang/String;
      //   1810: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
      //   1813: pop
      //   1814: aload_0
      //   1815: aload_0
      //   1816: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   1819: invokevirtual onSurfaceCreated : (Landroid/view/SurfaceHolder;)V
      //   1822: aload_0
      //   1823: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   1826: invokevirtual getCallbacks : ()[Landroid/view/SurfaceHolder$Callback;
      //   1829: astore #15
      //   1831: iload #27
      //   1833: istore #6
      //   1835: aload #15
      //   1837: ifnull -> 1890
      //   1840: aload #15
      //   1842: arraylength
      //   1843: istore #28
      //   1845: iconst_0
      //   1846: istore #29
      //   1848: iload #27
      //   1850: istore #6
      //   1852: iload #29
      //   1854: iload #28
      //   1856: if_icmpge -> 1890
      //   1859: aload #15
      //   1861: iload #29
      //   1863: aaload
      //   1864: astore #14
      //   1866: aload #14
      //   1868: aload_0
      //   1869: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   1872: invokeinterface surfaceCreated : (Landroid/view/SurfaceHolder;)V
      //   1877: iinc #29, 1
      //   1880: goto -> 1848
      //   1883: astore #14
      //   1885: iload_3
      //   1886: istore_1
      //   1887: goto -> 2940
      //   1890: iload #8
      //   1892: ifne -> 1911
      //   1895: iload #26
      //   1897: iconst_2
      //   1898: iand
      //   1899: ifeq -> 1905
      //   1902: goto -> 1911
      //   1905: iconst_0
      //   1906: istore #29
      //   1908: goto -> 1914
      //   1911: iconst_1
      //   1912: istore #29
      //   1914: iload_3
      //   1915: iload #29
      //   1917: ior
      //   1918: istore_3
      //   1919: iload_2
      //   1920: ifne -> 1948
      //   1923: iload #8
      //   1925: ifne -> 1948
      //   1928: iload #9
      //   1930: ifne -> 1948
      //   1933: iload #10
      //   1935: ifne -> 1948
      //   1938: iload_1
      //   1939: ifeq -> 1945
      //   1942: goto -> 1948
      //   1945: goto -> 2379
      //   1948: getstatic android/service/wallpaper/WallpaperService.DEBUG : Z
      //   1951: istore #11
      //   1953: iload #11
      //   1955: ifeq -> 2081
      //   1958: new java/lang/RuntimeException
      //   1961: astore #15
      //   1963: aload #15
      //   1965: invokespecial <init> : ()V
      //   1968: aload #15
      //   1970: invokevirtual fillInStackTrace : ()Ljava/lang/Throwable;
      //   1973: pop
      //   1974: new java/lang/StringBuilder
      //   1977: astore #14
      //   1979: aload #14
      //   1981: invokespecial <init> : ()V
      //   1984: aload #14
      //   1986: ldc_w 'forceReport='
      //   1989: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1992: pop
      //   1993: aload #14
      //   1995: iload_2
      //   1996: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   1999: pop
      //   2000: aload #14
      //   2002: ldc_w ' creating='
      //   2005: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2008: pop
      //   2009: aload #14
      //   2011: iload #8
      //   2013: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   2016: pop
      //   2017: aload #14
      //   2019: ldc_w ' formatChanged='
      //   2022: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2025: pop
      //   2026: aload #14
      //   2028: iload #10
      //   2030: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   2033: pop
      //   2034: aload #14
      //   2036: ldc_w ' sizeChanged='
      //   2039: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2042: pop
      //   2043: aload #14
      //   2045: iload_1
      //   2046: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   2049: pop
      //   2050: ldc_w 'WallpaperService'
      //   2053: aload #14
      //   2055: invokevirtual toString : ()Ljava/lang/String;
      //   2058: aload #15
      //   2060: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   2063: pop
      //   2064: goto -> 2081
      //   2067: astore #14
      //   2069: iload_3
      //   2070: istore_1
      //   2071: goto -> 2940
      //   2074: astore #14
      //   2076: iload_3
      //   2077: istore_1
      //   2078: goto -> 2940
      //   2081: iload #5
      //   2083: istore #29
      //   2085: iload #4
      //   2087: istore #29
      //   2089: getstatic android/service/wallpaper/WallpaperService.DEBUG : Z
      //   2092: istore_1
      //   2093: iload_1
      //   2094: ifeq -> 2221
      //   2097: new java/lang/StringBuilder
      //   2100: astore #14
      //   2102: aload #14
      //   2104: invokespecial <init> : ()V
      //   2107: aload #14
      //   2109: ldc_w 'onSurfaceChanged('
      //   2112: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2115: pop
      //   2116: aload #14
      //   2118: aload_0
      //   2119: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   2122: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   2125: pop
      //   2126: aload #14
      //   2128: ldc_w ', '
      //   2131: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2134: pop
      //   2135: aload #14
      //   2137: aload_0
      //   2138: getfield mFormat : I
      //   2141: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   2144: pop
      //   2145: aload #14
      //   2147: ldc_w ', '
      //   2150: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2153: pop
      //   2154: aload #14
      //   2156: aload_0
      //   2157: getfield mCurWidth : I
      //   2160: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   2163: pop
      //   2164: aload #14
      //   2166: ldc_w ', '
      //   2169: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2172: pop
      //   2173: aload #14
      //   2175: aload_0
      //   2176: getfield mCurHeight : I
      //   2179: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   2182: pop
      //   2183: aload #14
      //   2185: ldc_w '): '
      //   2188: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2191: pop
      //   2192: aload #14
      //   2194: aload_0
      //   2195: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   2198: pop
      //   2199: ldc_w 'WallpaperService'
      //   2202: aload #14
      //   2204: invokevirtual toString : ()Ljava/lang/String;
      //   2207: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
      //   2210: pop
      //   2211: goto -> 2221
      //   2214: astore #14
      //   2216: iload_3
      //   2217: istore_1
      //   2218: goto -> 2940
      //   2221: iconst_1
      //   2222: istore #27
      //   2224: iload #5
      //   2226: istore #29
      //   2228: iload #4
      //   2230: istore #29
      //   2232: aload_0
      //   2233: aload_0
      //   2234: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   2237: aload_0
      //   2238: getfield mFormat : I
      //   2241: aload_0
      //   2242: getfield mCurWidth : I
      //   2245: aload_0
      //   2246: getfield mCurHeight : I
      //   2249: invokevirtual onSurfaceChanged : (Landroid/view/SurfaceHolder;III)V
      //   2252: iload #5
      //   2254: istore #29
      //   2256: iload #4
      //   2258: istore #29
      //   2260: aload_0
      //   2261: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   2264: invokevirtual getCallbacks : ()[Landroid/view/SurfaceHolder$Callback;
      //   2267: astore #14
      //   2269: aload #14
      //   2271: ifnull -> 2375
      //   2274: iload #5
      //   2276: istore #29
      //   2278: iload #4
      //   2280: istore #29
      //   2282: aload #14
      //   2284: arraylength
      //   2285: istore #9
      //   2287: iconst_0
      //   2288: istore #6
      //   2290: iload #6
      //   2292: iload #9
      //   2294: if_icmpge -> 2368
      //   2297: aload #14
      //   2299: iload #6
      //   2301: aaload
      //   2302: astore #17
      //   2304: iload #5
      //   2306: istore #29
      //   2308: iload #4
      //   2310: istore #29
      //   2312: aload_0
      //   2313: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   2316: astore #15
      //   2318: iload #5
      //   2320: istore #29
      //   2322: iload #4
      //   2324: istore #29
      //   2326: aload_0
      //   2327: getfield mFormat : I
      //   2330: istore #28
      //   2332: aload_0
      //   2333: getfield mCurWidth : I
      //   2336: istore #29
      //   2338: aload #17
      //   2340: aload #15
      //   2342: iload #28
      //   2344: iload #29
      //   2346: aload_0
      //   2347: getfield mCurHeight : I
      //   2350: invokeinterface surfaceChanged : (Landroid/view/SurfaceHolder;III)V
      //   2355: iinc #6, 1
      //   2358: goto -> 2290
      //   2361: astore #14
      //   2363: iload_3
      //   2364: istore_1
      //   2365: goto -> 2940
      //   2368: iload #27
      //   2370: istore #6
      //   2372: goto -> 2379
      //   2375: iload #27
      //   2377: istore #6
      //   2379: iload #12
      //   2381: iconst_1
      //   2382: ixor
      //   2383: iload #7
      //   2385: ior
      //   2386: iload #13
      //   2388: ior
      //   2389: iload #24
      //   2391: ior
      //   2392: ifeq -> 2548
      //   2395: aload_0
      //   2396: getfield mDispatchedContentInsets : Landroid/graphics/Rect;
      //   2399: aload_0
      //   2400: getfield mContentInsets : Landroid/graphics/Rect;
      //   2403: invokevirtual set : (Landroid/graphics/Rect;)V
      //   2406: aload_0
      //   2407: getfield mDispatchedStableInsets : Landroid/graphics/Rect;
      //   2410: aload_0
      //   2411: getfield mStableInsets : Landroid/graphics/Rect;
      //   2414: invokevirtual set : (Landroid/graphics/Rect;)V
      //   2417: aload_0
      //   2418: aload_0
      //   2419: getfield mDisplayCutout : Landroid/view/DisplayCutout$ParcelableWrapper;
      //   2422: invokevirtual get : ()Landroid/view/DisplayCutout;
      //   2425: putfield mDispatchedDisplayCutout : Landroid/view/DisplayCutout;
      //   2428: aload_0
      //   2429: getfield mFinalStableInsets : Landroid/graphics/Rect;
      //   2432: aload_0
      //   2433: getfield mDispatchedStableInsets : Landroid/graphics/Rect;
      //   2436: invokevirtual set : (Landroid/graphics/Rect;)V
      //   2439: new android/view/WindowInsets
      //   2442: astore #14
      //   2444: aload_0
      //   2445: getfield mFinalSystemInsets : Landroid/graphics/Rect;
      //   2448: astore #15
      //   2450: aload_0
      //   2451: getfield mFinalStableInsets : Landroid/graphics/Rect;
      //   2454: astore #17
      //   2456: aload_0
      //   2457: getfield this$0 : Landroid/service/wallpaper/WallpaperService;
      //   2460: astore #16
      //   2462: aload #14
      //   2464: aload #15
      //   2466: aload #17
      //   2468: aload #16
      //   2470: invokevirtual getResources : ()Landroid/content/res/Resources;
      //   2473: invokevirtual getConfiguration : ()Landroid/content/res/Configuration;
      //   2476: invokevirtual isScreenRound : ()Z
      //   2479: iconst_0
      //   2480: aload_0
      //   2481: getfield mDispatchedDisplayCutout : Landroid/view/DisplayCutout;
      //   2484: invokespecial <init> : (Landroid/graphics/Rect;Landroid/graphics/Rect;ZZLandroid/view/DisplayCutout;)V
      //   2487: getstatic android/service/wallpaper/WallpaperService.DEBUG : Z
      //   2490: ifeq -> 2532
      //   2493: new java/lang/StringBuilder
      //   2496: astore #15
      //   2498: aload #15
      //   2500: invokespecial <init> : ()V
      //   2503: aload #15
      //   2505: ldc_w 'dispatching insets='
      //   2508: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2511: pop
      //   2512: aload #15
      //   2514: aload #14
      //   2516: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   2519: pop
      //   2520: ldc_w 'WallpaperService'
      //   2523: aload #15
      //   2525: invokevirtual toString : ()Ljava/lang/String;
      //   2528: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
      //   2531: pop
      //   2532: aload_0
      //   2533: aload #14
      //   2535: invokevirtual onApplyWindowInsets : (Landroid/view/WindowInsets;)V
      //   2538: goto -> 2548
      //   2541: astore #14
      //   2543: iload_3
      //   2544: istore_1
      //   2545: goto -> 2940
      //   2548: iload_3
      //   2549: ifeq -> 2624
      //   2552: aload_0
      //   2553: aload_0
      //   2554: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   2557: invokevirtual onSurfaceRedrawNeeded : (Landroid/view/SurfaceHolder;)V
      //   2560: aload_0
      //   2561: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   2564: invokevirtual getCallbacks : ()[Landroid/view/SurfaceHolder$Callback;
      //   2567: astore #14
      //   2569: aload #14
      //   2571: ifnull -> 2624
      //   2574: aload #14
      //   2576: arraylength
      //   2577: istore #4
      //   2579: iconst_0
      //   2580: istore #5
      //   2582: iload #5
      //   2584: iload #4
      //   2586: if_icmpge -> 2624
      //   2589: aload #14
      //   2591: iload #5
      //   2593: aaload
      //   2594: astore #15
      //   2596: aload #15
      //   2598: instanceof android/view/SurfaceHolder$Callback2
      //   2601: ifeq -> 2618
      //   2604: aload #15
      //   2606: checkcast android/view/SurfaceHolder$Callback2
      //   2609: aload_0
      //   2610: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
      //   2613: invokeinterface surfaceRedrawNeeded : (Landroid/view/SurfaceHolder;)V
      //   2618: iinc #5, 1
      //   2621: goto -> 2582
      //   2624: iload #6
      //   2626: ifeq -> 2741
      //   2629: aload_0
      //   2630: getfield mReportedVisible : Z
      //   2633: ifne -> 2741
      //   2636: aload_0
      //   2637: getfield mIsCreating : Z
      //   2640: ifeq -> 2692
      //   2643: getstatic android/service/wallpaper/WallpaperService.DEBUG : Z
      //   2646: ifeq -> 2687
      //   2649: new java/lang/StringBuilder
      //   2652: astore #14
      //   2654: aload #14
      //   2656: invokespecial <init> : ()V
      //   2659: aload #14
      //   2661: ldc_w 'onVisibilityChanged(true) at surface: '
      //   2664: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2667: pop
      //   2668: aload #14
      //   2670: aload_0
      //   2671: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   2674: pop
      //   2675: ldc_w 'WallpaperService'
      //   2678: aload #14
      //   2680: invokevirtual toString : ()Ljava/lang/String;
      //   2683: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
      //   2686: pop
      //   2687: aload_0
      //   2688: iconst_1
      //   2689: invokevirtual onVisibilityChanged : (Z)V
      //   2692: getstatic android/service/wallpaper/WallpaperService.DEBUG : Z
      //   2695: ifeq -> 2736
      //   2698: new java/lang/StringBuilder
      //   2701: astore #14
      //   2703: aload #14
      //   2705: invokespecial <init> : ()V
      //   2708: aload #14
      //   2710: ldc_w 'onVisibilityChanged(false) at surface: '
      //   2713: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2716: pop
      //   2717: aload #14
      //   2719: aload_0
      //   2720: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   2723: pop
      //   2724: ldc_w 'WallpaperService'
      //   2727: aload #14
      //   2729: invokevirtual toString : ()Ljava/lang/String;
      //   2732: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
      //   2735: pop
      //   2736: aload_0
      //   2737: iconst_0
      //   2738: invokevirtual onVisibilityChanged : (Z)V
      //   2741: aload_0
      //   2742: iconst_0
      //   2743: putfield mIsCreating : Z
      //   2746: aload_0
      //   2747: iconst_1
      //   2748: putfield mSurfaceCreated : Z
      //   2751: iload_3
      //   2752: ifeq -> 2912
      //   2755: getstatic android/service/wallpaper/WallpaperService.DEBUG : Z
      //   2758: ifeq -> 2855
      //   2761: new java/lang/StringBuilder
      //   2764: astore #14
      //   2766: aload #14
      //   2768: invokespecial <init> : ()V
      //   2771: aload #14
      //   2773: ldc_w 'updateSurface: creating = '
      //   2776: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2779: pop
      //   2780: aload #14
      //   2782: iload #8
      //   2784: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   2787: pop
      //   2788: aload #14
      //   2790: ldc_w ' , relayoutResult = '
      //   2793: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2796: pop
      //   2797: aload #14
      //   2799: iload #26
      //   2801: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   2804: pop
      //   2805: aload #14
      //   2807: ldc_w ' , mReportedFinishDrawing = '
      //   2810: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2813: pop
      //   2814: aload #14
      //   2816: aload_0
      //   2817: getfield mReportedFinishDrawing : Z
      //   2820: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   2823: pop
      //   2824: aload #14
      //   2826: ldc_w ' , mIsFromSwitchingUser = '
      //   2829: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2832: pop
      //   2833: aload #14
      //   2835: aload_0
      //   2836: getfield mIsFromSwitchingUser : Z
      //   2839: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   2842: pop
      //   2843: ldc_w 'WallpaperService'
      //   2846: aload #14
      //   2848: invokevirtual toString : ()Ljava/lang/String;
      //   2851: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   2854: pop
      //   2855: aload_0
      //   2856: invokespecial needDelayFinishDrawing : ()Z
      //   2859: ifeq -> 2893
      //   2862: aload_0
      //   2863: getfield mHandler : Landroid/os/Handler;
      //   2866: astore #14
      //   2868: new android/service/wallpaper/WallpaperService$Engine$3
      //   2871: astore #15
      //   2873: aload #15
      //   2875: aload_0
      //   2876: invokespecial <init> : (Landroid/service/wallpaper/WallpaperService$Engine;)V
      //   2879: aload #14
      //   2881: aload #15
      //   2883: ldc2_w 500
      //   2886: invokevirtual postDelayed : (Ljava/lang/Runnable;J)Z
      //   2889: pop
      //   2890: goto -> 2912
      //   2893: aload_0
      //   2894: getfield mSession : Landroid/view/IWindowSession;
      //   2897: aload_0
      //   2898: getfield mWindow : Lcom/android/internal/view/BaseIWindow;
      //   2901: aconst_null
      //   2902: invokeinterface finishDrawing : (Landroid/view/IWindow;Landroid/view/SurfaceControl$Transaction;)V
      //   2907: aload_0
      //   2908: iconst_1
      //   2909: putfield mReportedFinishDrawing : Z
      //   2912: aload_0
      //   2913: getfield mIWallpaperEngine : Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;
      //   2916: invokevirtual reportShown : ()V
      //   2919: goto -> 3163
      //   2922: astore #14
      //   2924: iload_3
      //   2925: istore_1
      //   2926: goto -> 2940
      //   2929: astore #14
      //   2931: iload_3
      //   2932: istore_1
      //   2933: goto -> 2940
      //   2936: astore #14
      //   2938: iload_3
      //   2939: istore_1
      //   2940: aload_0
      //   2941: iconst_0
      //   2942: putfield mIsCreating : Z
      //   2945: aload_0
      //   2946: iconst_1
      //   2947: putfield mSurfaceCreated : Z
      //   2950: iload_1
      //   2951: ifeq -> 3111
      //   2954: getstatic android/service/wallpaper/WallpaperService.DEBUG : Z
      //   2957: ifeq -> 3054
      //   2960: new java/lang/StringBuilder
      //   2963: astore #15
      //   2965: aload #15
      //   2967: invokespecial <init> : ()V
      //   2970: aload #15
      //   2972: ldc_w 'updateSurface: creating = '
      //   2975: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2978: pop
      //   2979: aload #15
      //   2981: iload #8
      //   2983: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   2986: pop
      //   2987: aload #15
      //   2989: ldc_w ' , relayoutResult = '
      //   2992: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2995: pop
      //   2996: aload #15
      //   2998: iload #26
      //   3000: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   3003: pop
      //   3004: aload #15
      //   3006: ldc_w ' , mReportedFinishDrawing = '
      //   3009: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   3012: pop
      //   3013: aload #15
      //   3015: aload_0
      //   3016: getfield mReportedFinishDrawing : Z
      //   3019: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   3022: pop
      //   3023: aload #15
      //   3025: ldc_w ' , mIsFromSwitchingUser = '
      //   3028: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   3031: pop
      //   3032: aload #15
      //   3034: aload_0
      //   3035: getfield mIsFromSwitchingUser : Z
      //   3038: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   3041: pop
      //   3042: ldc_w 'WallpaperService'
      //   3045: aload #15
      //   3047: invokevirtual toString : ()Ljava/lang/String;
      //   3050: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   3053: pop
      //   3054: aload_0
      //   3055: invokespecial needDelayFinishDrawing : ()Z
      //   3058: ifeq -> 3092
      //   3061: aload_0
      //   3062: getfield mHandler : Landroid/os/Handler;
      //   3065: astore #15
      //   3067: new android/service/wallpaper/WallpaperService$Engine$3
      //   3070: astore #17
      //   3072: aload #17
      //   3074: aload_0
      //   3075: invokespecial <init> : (Landroid/service/wallpaper/WallpaperService$Engine;)V
      //   3078: aload #15
      //   3080: aload #17
      //   3082: ldc2_w 500
      //   3085: invokevirtual postDelayed : (Ljava/lang/Runnable;J)Z
      //   3088: pop
      //   3089: goto -> 3111
      //   3092: aload_0
      //   3093: getfield mSession : Landroid/view/IWindowSession;
      //   3096: aload_0
      //   3097: getfield mWindow : Lcom/android/internal/view/BaseIWindow;
      //   3100: aconst_null
      //   3101: invokeinterface finishDrawing : (Landroid/view/IWindow;Landroid/view/SurfaceControl$Transaction;)V
      //   3106: aload_0
      //   3107: iconst_1
      //   3108: putfield mReportedFinishDrawing : Z
      //   3111: aload_0
      //   3112: getfield mIWallpaperEngine : Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;
      //   3115: invokevirtual reportShown : ()V
      //   3118: aload #14
      //   3120: athrow
      //   3121: astore #14
      //   3123: goto -> 3163
      //   3126: astore #14
      //   3128: goto -> 3163
      //   3131: astore #14
      //   3133: goto -> 3163
      //   3136: astore #14
      //   3138: goto -> 3163
      //   3141: astore #14
      //   3143: goto -> 3163
      //   3146: astore #14
      //   3148: goto -> 3163
      //   3151: astore #14
      //   3153: goto -> 3163
      //   3156: astore #14
      //   3158: goto -> 3163
      //   3161: astore #14
      //   3163: getstatic android/service/wallpaper/WallpaperService.DEBUG : Z
      //   3166: ifeq -> 3278
      //   3169: new java/lang/StringBuilder
      //   3172: dup
      //   3173: invokespecial <init> : ()V
      //   3176: astore #14
      //   3178: aload #14
      //   3180: ldc_w 'Layout: x='
      //   3183: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   3186: pop
      //   3187: aload #14
      //   3189: aload_0
      //   3190: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   3193: getfield x : I
      //   3196: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   3199: pop
      //   3200: aload #14
      //   3202: ldc_w ' y='
      //   3205: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   3208: pop
      //   3209: aload #14
      //   3211: aload_0
      //   3212: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   3215: getfield y : I
      //   3218: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   3221: pop
      //   3222: aload #14
      //   3224: ldc_w ' w='
      //   3227: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   3230: pop
      //   3231: aload #14
      //   3233: aload_0
      //   3234: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   3237: getfield width : I
      //   3240: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   3243: pop
      //   3244: aload #14
      //   3246: ldc_w ' h='
      //   3249: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   3252: pop
      //   3253: aload #14
      //   3255: aload_0
      //   3256: getfield mLayout : Landroid/view/WindowManager$LayoutParams;
      //   3259: getfield height : I
      //   3262: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   3265: pop
      //   3266: ldc_w 'WallpaperService'
      //   3269: aload #14
      //   3271: invokevirtual toString : ()Ljava/lang/String;
      //   3274: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
      //   3277: pop
      //   3278: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #853	-> 0
      //   #854	-> 7
      //   #858	-> 17
      //   #861	-> 31
      //   #862	-> 34
      //   #863	-> 43
      //   #864	-> 54
      //   #865	-> 57
      //   #866	-> 66
      //   #867	-> 85
      //   #869	-> 92
      //   #870	-> 100
      //   #871	-> 108
      //   #872	-> 131
      //   #873	-> 161
      //   #874	-> 167
      //   #875	-> 190
      //   #877	-> 224
      //   #881	-> 278
      //   #885	-> 359
      //   #886	-> 365
      //   #887	-> 371
      //   #888	-> 382
      //   #890	-> 393
      //   #891	-> 401
      //   #893	-> 409
      //   #894	-> 418
      //   #895	-> 427
      //   #897	-> 438
      //   #898	-> 446
      //   #903	-> 472
      //   #904	-> 480
      //   #906	-> 491
      //   #907	-> 502
      //   #909	-> 513
      //   #911	-> 522
      //   #913	-> 534
      //   #916	-> 539
      //   #917	-> 553
      //   #918	-> 563
      //   #919	-> 571
      //   #920	-> 588
      //   #922	-> 598
      //   #924	-> 608
      //   #925	-> 641
      //   #924	-> 684
      //   #928	-> 717
      //   #929	-> 727
      //   #931	-> 728
      //   #932	-> 745
      //   #934	-> 750
      //   #935	-> 755
      //   #1131	-> 775
      //   #909	-> 800
      //   #938	-> 800
      //   #939	-> 813
      //   #941	-> 818
      //   #942	-> 823
      //   #1131	-> 843
      //   #944	-> 848
      //   #947	-> 862
      //   #953	-> 1005
      //   #954	-> 1019
      //   #955	-> 1033
      //   #1131	-> 1043
      //   #958	-> 1048
      //   #1131	-> 1128
      //   #958	-> 1133
      //   #961	-> 1133
      //   #962	-> 1142
      //   #964	-> 1151
      //   #965	-> 1156
      //   #966	-> 1165
      //   #967	-> 1179
      //   #968	-> 1193
      //   #969	-> 1215
      //   #970	-> 1237
      //   #971	-> 1259
      //   #972	-> 1281
      //   #973	-> 1303
      //   #974	-> 1325
      //   #975	-> 1347
      //   #976	-> 1369
      //   #978	-> 1410
      //   #1131	-> 1433
      //   #979	-> 1438
      //   #980	-> 1438
      //   #983	-> 1438
      //   #984	-> 1451
      //   #985	-> 1454
      //   #1131	-> 1465
      //   #987	-> 1470
      //   #988	-> 1483
      //   #989	-> 1488
      //   #987	-> 1497
      //   #992	-> 1497
      //   #993	-> 1507
      //   #996	-> 1588
      //   #997	-> 1611
      //   #998	-> 1634
      //   #1000	-> 1660
      //   #1001	-> 1671
      //   #1003	-> 1681
      //   #1004	-> 1698
      //   #1005	-> 1705
      //   #1006	-> 1727
      //   #1009	-> 1728
      //   #1012	-> 1731
      //   #1014	-> 1738
      //   #1015	-> 1743
      //   #1016	-> 1748
      //   #1017	-> 1751
      //   #1019	-> 1814
      //   #1020	-> 1822
      //   #1021	-> 1831
      //   #1022	-> 1840
      //   #1023	-> 1866
      //   #1022	-> 1877
      //   #1105	-> 1883
      //   #1028	-> 1890
      //   #1031	-> 1919
      //   #1033	-> 1948
      //   #1034	-> 1958
      //   #1035	-> 1968
      //   #1036	-> 1974
      //   #1105	-> 2067
      //   #1033	-> 2081
      //   #1040	-> 2081
      //   #1105	-> 2214
      //   #1044	-> 2221
      //   #1045	-> 2224
      //   #1047	-> 2252
      //   #1048	-> 2269
      //   #1049	-> 2274
      //   #1050	-> 2304
      //   #1049	-> 2355
      //   #1105	-> 2361
      //   #1049	-> 2368
      //   #1048	-> 2375
      //   #1056	-> 2379
      //   #1057	-> 2395
      //   #1058	-> 2406
      //   #1059	-> 2417
      //   #1060	-> 2428
      //   #1061	-> 2439
      //   #1063	-> 2462
      //   #1065	-> 2487
      //   #1066	-> 2493
      //   #1068	-> 2532
      //   #1105	-> 2541
      //   #1071	-> 2548
      //   #1072	-> 2552
      //   #1073	-> 2560
      //   #1074	-> 2569
      //   #1075	-> 2574
      //   #1076	-> 2596
      //   #1077	-> 2604
      //   #1075	-> 2618
      //   #1084	-> 2624
      //   #1090	-> 2636
      //   #1095	-> 2643
      //   #1097	-> 2687
      //   #1099	-> 2692
      //   #1101	-> 2736
      //   #1105	-> 2741
      //   #1106	-> 2746
      //   #1107	-> 2751
      //   #1110	-> 2755
      //   #1111	-> 2761
      //   #1113	-> 2855
      //   #1114	-> 2862
      //   #1122	-> 2893
      //   #1125	-> 2907
      //   #1129	-> 2912
      //   #1130	-> 2919
      //   #1132	-> 2919
      //   #1105	-> 2922
      //   #1106	-> 2945
      //   #1107	-> 2950
      //   #1110	-> 2954
      //   #1111	-> 2960
      //   #1113	-> 3054
      //   #1114	-> 3061
      //   #1122	-> 3092
      //   #1125	-> 3106
      //   #1129	-> 3111
      //   #1130	-> 3118
      //   #1131	-> 3121
      //   #1133	-> 3163
      //   #1137	-> 3278
      // Exception table:
      //   from	to	target	type
      //   359	365	3161	android/os/RemoteException
      //   365	371	3161	android/os/RemoteException
      //   371	382	3161	android/os/RemoteException
      //   382	393	3161	android/os/RemoteException
      //   393	401	3161	android/os/RemoteException
      //   401	409	3161	android/os/RemoteException
      //   409	418	3161	android/os/RemoteException
      //   418	427	3161	android/os/RemoteException
      //   427	438	3161	android/os/RemoteException
      //   438	446	3161	android/os/RemoteException
      //   446	472	3161	android/os/RemoteException
      //   472	480	3161	android/os/RemoteException
      //   480	491	3161	android/os/RemoteException
      //   491	502	3161	android/os/RemoteException
      //   502	513	3161	android/os/RemoteException
      //   513	518	3161	android/os/RemoteException
      //   522	534	795	android/os/RemoteException
      //   534	539	795	android/os/RemoteException
      //   539	553	795	android/os/RemoteException
      //   553	563	795	android/os/RemoteException
      //   563	571	795	android/os/RemoteException
      //   571	588	795	android/os/RemoteException
      //   588	598	795	android/os/RemoteException
      //   598	608	795	android/os/RemoteException
      //   608	614	795	android/os/RemoteException
      //   614	620	790	android/os/RemoteException
      //   620	641	785	android/os/RemoteException
      //   641	660	785	android/os/RemoteException
      //   660	672	780	android/os/RemoteException
      //   672	678	775	android/os/RemoteException
      //   678	684	843	android/os/RemoteException
      //   684	717	843	android/os/RemoteException
      //   717	727	843	android/os/RemoteException
      //   728	745	843	android/os/RemoteException
      //   745	750	843	android/os/RemoteException
      //   750	755	843	android/os/RemoteException
      //   755	772	843	android/os/RemoteException
      //   803	813	3156	android/os/RemoteException
      //   813	818	3156	android/os/RemoteException
      //   823	840	843	android/os/RemoteException
      //   848	862	3156	android/os/RemoteException
      //   862	931	3156	android/os/RemoteException
      //   931	937	3151	android/os/RemoteException
      //   937	943	3146	android/os/RemoteException
      //   943	949	3141	android/os/RemoteException
      //   949	1005	3136	android/os/RemoteException
      //   1005	1014	3136	android/os/RemoteException
      //   1019	1033	1043	android/os/RemoteException
      //   1033	1040	1043	android/os/RemoteException
      //   1048	1053	3136	android/os/RemoteException
      //   1058	1116	1128	android/os/RemoteException
      //   1116	1125	1433	android/os/RemoteException
      //   1133	1142	3131	android/os/RemoteException
      //   1142	1151	3131	android/os/RemoteException
      //   1156	1165	1433	android/os/RemoteException
      //   1165	1179	1433	android/os/RemoteException
      //   1179	1193	1433	android/os/RemoteException
      //   1193	1215	1433	android/os/RemoteException
      //   1215	1237	1433	android/os/RemoteException
      //   1237	1259	1433	android/os/RemoteException
      //   1259	1281	1433	android/os/RemoteException
      //   1281	1303	1433	android/os/RemoteException
      //   1303	1325	1433	android/os/RemoteException
      //   1325	1347	1433	android/os/RemoteException
      //   1347	1369	1433	android/os/RemoteException
      //   1369	1410	1433	android/os/RemoteException
      //   1438	1444	3131	android/os/RemoteException
      //   1454	1460	1465	android/os/RemoteException
      //   1470	1476	3131	android/os/RemoteException
      //   1488	1494	1465	android/os/RemoteException
      //   1497	1502	3126	android/os/RemoteException
      //   1510	1515	1465	android/os/RemoteException
      //   1518	1523	1465	android/os/RemoteException
      //   1526	1535	1465	android/os/RemoteException
      //   1538	1548	1465	android/os/RemoteException
      //   1551	1560	1465	android/os/RemoteException
      //   1563	1573	1465	android/os/RemoteException
      //   1576	1588	1465	android/os/RemoteException
      //   1588	1602	3126	android/os/RemoteException
      //   1611	1625	3126	android/os/RemoteException
      //   1634	1651	3126	android/os/RemoteException
      //   1660	1671	3126	android/os/RemoteException
      //   1671	1681	3126	android/os/RemoteException
      //   1681	1693	3126	android/os/RemoteException
      //   1701	1705	1465	android/os/RemoteException
      //   1708	1714	1465	android/os/RemoteException
      //   1717	1727	1465	android/os/RemoteException
      //   1731	1738	2936	finally
      //   1743	1748	1883	finally
      //   1751	1814	1883	finally
      //   1814	1822	1883	finally
      //   1822	1831	1883	finally
      //   1840	1845	1883	finally
      //   1866	1877	1883	finally
      //   1948	1953	2929	finally
      //   1958	1968	2074	finally
      //   1968	1974	2074	finally
      //   1974	2009	2074	finally
      //   2009	2026	2067	finally
      //   2026	2064	2214	finally
      //   2089	2093	2922	finally
      //   2097	2211	2214	finally
      //   2232	2252	2922	finally
      //   2260	2269	2922	finally
      //   2282	2287	2922	finally
      //   2312	2318	2922	finally
      //   2326	2332	2922	finally
      //   2332	2338	2361	finally
      //   2338	2355	2541	finally
      //   2395	2406	2541	finally
      //   2406	2417	2541	finally
      //   2417	2428	2541	finally
      //   2428	2439	2541	finally
      //   2439	2462	2541	finally
      //   2462	2487	2541	finally
      //   2487	2493	2541	finally
      //   2493	2532	2541	finally
      //   2532	2538	2541	finally
      //   2552	2560	2541	finally
      //   2560	2569	2541	finally
      //   2574	2579	2541	finally
      //   2596	2604	2541	finally
      //   2604	2618	2541	finally
      //   2629	2636	2541	finally
      //   2636	2643	2541	finally
      //   2643	2687	2541	finally
      //   2687	2692	2541	finally
      //   2692	2736	2541	finally
      //   2736	2741	2541	finally
      //   2741	2746	3121	android/os/RemoteException
      //   2746	2751	3121	android/os/RemoteException
      //   2755	2761	3121	android/os/RemoteException
      //   2761	2855	3121	android/os/RemoteException
      //   2855	2862	3121	android/os/RemoteException
      //   2862	2890	3121	android/os/RemoteException
      //   2893	2907	3121	android/os/RemoteException
      //   2907	2912	3121	android/os/RemoteException
      //   2912	2919	3121	android/os/RemoteException
      //   2940	2945	3121	android/os/RemoteException
      //   2945	2950	3121	android/os/RemoteException
      //   2954	2960	3121	android/os/RemoteException
      //   2960	3054	3121	android/os/RemoteException
      //   3054	3061	3121	android/os/RemoteException
      //   3061	3089	3121	android/os/RemoteException
      //   3092	3106	3121	android/os/RemoteException
      //   3106	3111	3121	android/os/RemoteException
      //   3111	3118	3121	android/os/RemoteException
      //   3118	3121	3121	android/os/RemoteException
    }
    
    private boolean needDelayFinishDrawing() {
      // Byte code:
      //   0: aload_0
      //   1: invokevirtual isPreview : ()Z
      //   4: ifne -> 72
      //   7: aload_0
      //   8: getfield mIsFromSwitchingUser : Z
      //   11: ifeq -> 72
      //   14: aload_0
      //   15: getfield mIsSupportReportFinishDrawing : Z
      //   18: ifeq -> 72
      //   21: aload_0
      //   22: getfield mReportedFinishDrawing : Z
      //   25: ifne -> 72
      //   28: invokestatic getInstance : ()Lcom/oplus/multiuser/OplusMultiUserManager;
      //   31: invokevirtual hasMultiSystemUser : ()Z
      //   34: ifeq -> 72
      //   37: aload_0
      //   38: getfield this$0 : Landroid/service/wallpaper/WallpaperService;
      //   41: astore_1
      //   42: aload_1
      //   43: invokevirtual getUserId : ()I
      //   46: ifeq -> 67
      //   49: invokestatic getInstance : ()Lcom/oplus/multiuser/OplusMultiUserManager;
      //   52: aload_0
      //   53: getfield this$0 : Landroid/service/wallpaper/WallpaperService;
      //   56: invokevirtual getUserId : ()I
      //   59: invokevirtual isMultiSystemUserId : (I)Z
      //   62: istore_2
      //   63: iload_2
      //   64: ifeq -> 72
      //   67: iconst_1
      //   68: istore_2
      //   69: goto -> 74
      //   72: iconst_0
      //   73: istore_2
      //   74: goto -> 113
      //   77: astore_1
      //   78: new java/lang/StringBuilder
      //   81: dup
      //   82: invokespecial <init> : ()V
      //   85: astore_3
      //   86: aload_3
      //   87: ldc_w 'needDelayFinishDrawing: e = '
      //   90: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   93: pop
      //   94: aload_3
      //   95: aload_1
      //   96: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   99: pop
      //   100: ldc_w 'WallpaperService'
      //   103: aload_3
      //   104: invokevirtual toString : ()Ljava/lang/String;
      //   107: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   110: pop
      //   111: iconst_0
      //   112: istore_2
      //   113: iload_2
      //   114: ireturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1142	-> 0
      //   #1144	-> 0
      //   #1146	-> 28
      //   #1147	-> 42
      //   #1151	-> 74
      //   #1148	-> 77
      //   #1149	-> 78
      //   #1150	-> 111
      //   #1152	-> 113
      // Exception table:
      //   from	to	target	type
      //   0	28	77	java/lang/Exception
      //   28	42	77	java/lang/Exception
      //   42	63	77	java/lang/Exception
    }
    
    private void finishDrawing() {
      if (WallpaperService.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("finishDrawing: mSession = ");
        stringBuilder.append(this.mSession);
        stringBuilder.append(" , mReportedFinishDrawing = ");
        stringBuilder.append(this.mReportedFinishDrawing);
        Log.d("WallpaperService", stringBuilder.toString());
      } 
      IWindowSession iWindowSession = this.mSession;
      if (iWindowSession != null && 
        !this.mReportedFinishDrawing)
        try {
          iWindowSession.finishDrawing((IWindow)this.mWindow, null);
          this.mReportedFinishDrawing = true;
          if (WallpaperService.DEBUG)
            Log.d("WallpaperService", "finishDrawing: finishDrawing done."); 
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("finishDrawing: e = ");
          stringBuilder.append(remoteException);
          Log.e("WallpaperService", stringBuilder.toString());
        }  
    }
    
    void attach(WallpaperService.IWallpaperEngineWrapper param1IWallpaperEngineWrapper) {
      if (WallpaperService.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("attach: ");
        stringBuilder.append(this);
        stringBuilder.append(" wrapper=");
        stringBuilder.append(param1IWallpaperEngineWrapper);
        Log.v("WallpaperService", stringBuilder.toString());
      } 
      if (this.mDestroyed)
        return; 
      this.mIWallpaperEngine = param1IWallpaperEngineWrapper;
      this.mCaller = param1IWallpaperEngineWrapper.mCaller;
      this.mConnection = param1IWallpaperEngineWrapper.mConnection;
      this.mWindowToken = param1IWallpaperEngineWrapper.mWindowToken;
      this.mSurfaceHolder.setSizeFromLayout();
      this.mInitializing = true;
      IWindowSession iWindowSession = WindowManagerGlobal.getWindowSession();
      this.mWindow.setSession(iWindowSession);
      this.mLayout.packageName = WallpaperService.this.getPackageName();
      DisplayManager displayManager = this.mIWallpaperEngine.mDisplayManager;
      DisplayManager.DisplayListener displayListener = this.mDisplayListener;
      HandlerCaller handlerCaller = this.mCaller;
      Handler handler = handlerCaller.getHandler();
      displayManager.registerDisplayListener(displayListener, handler);
      Display display = this.mIWallpaperEngine.mDisplay;
      this.mDisplayContext = WallpaperService.this.createDisplayContext(display);
      this.mDisplayState = this.mDisplay.getState();
      if (WallpaperService.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onCreate(): ");
        stringBuilder.append(this);
        Log.v("WallpaperService", stringBuilder.toString());
      } 
      onCreate((SurfaceHolder)this.mSurfaceHolder);
      this.mIWallpaperEngine.oppoWallpaperServiceHelper.registerSetingsContentObserver(this.mDisplayContext);
      this.mInitializing = false;
      this.mReportedVisible = false;
      this.mIsFromSwitchingUser = WallpaperService.this.mIsFromSwitchingUser;
      Trace.traceBegin(8L, "WallpaperService.Engine.updateSurface");
      updateSurface(false, false, false);
      Trace.traceEnd(8L);
    }
    
    public Context getDisplayContext() {
      return this.mDisplayContext;
    }
    
    public void doAmbientModeChanged(boolean param1Boolean, long param1Long) {
      if (!this.mDestroyed) {
        if (WallpaperService.DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("onAmbientModeChanged(");
          stringBuilder.append(param1Boolean);
          stringBuilder.append(", ");
          stringBuilder.append(param1Long);
          stringBuilder.append("): ");
          stringBuilder.append(this);
          Log.v("WallpaperService", stringBuilder.toString());
        } 
        this.mIsInAmbientMode = param1Boolean;
        if (this.mCreated)
          onAmbientModeChanged(param1Boolean, param1Long); 
      } 
    }
    
    void doDesiredSizeChanged(int param1Int1, int param1Int2) {
      if (!this.mDestroyed) {
        if (WallpaperService.DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("onDesiredSizeChanged(");
          stringBuilder.append(param1Int1);
          stringBuilder.append(",");
          stringBuilder.append(param1Int2);
          stringBuilder.append("): ");
          stringBuilder.append(this);
          Log.v("WallpaperService", stringBuilder.toString());
        } 
        this.mIWallpaperEngine.mReqWidth = param1Int1;
        this.mIWallpaperEngine.mReqHeight = param1Int2;
        onDesiredSizeChanged(param1Int1, param1Int2);
        doOffsetsChanged(true);
      } 
    }
    
    void doDisplayPaddingChanged(Rect param1Rect) {
      if (!this.mDestroyed) {
        if (WallpaperService.DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("onDisplayPaddingChanged(");
          stringBuilder.append(param1Rect);
          stringBuilder.append("): ");
          stringBuilder.append(this);
          Log.v("WallpaperService", stringBuilder.toString());
        } 
        if (!this.mIWallpaperEngine.mDisplayPadding.equals(param1Rect)) {
          this.mIWallpaperEngine.mDisplayPadding.set(param1Rect);
          updateSurface(true, false, false);
        } 
      } 
    }
    
    void doVisibilityChanged(boolean param1Boolean) {
      if (!this.mDestroyed) {
        this.mVisible = param1Boolean;
        reportVisibility();
      } 
    }
    
    void reportVisibility() {
      if (!this.mDestroyed) {
        int i;
        boolean bool;
        Display display = this.mDisplay;
        if (display == null) {
          i = 0;
        } else {
          i = display.getState();
        } 
        this.mDisplayState = i;
        if (this.mVisible && i != 1 && i != 3 && i != 4) {
          bool = true;
        } else {
          bool = false;
        } 
        if (this.mReportedVisible != bool) {
          this.mReportedVisible = bool;
          if (WallpaperService.DEBUG) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("onVisibilityChanged(");
            stringBuilder.append(bool);
            stringBuilder.append("): ");
            stringBuilder.append(this);
            Log.v("WallpaperService", stringBuilder.toString());
          } 
          if (bool) {
            doOffsetsChanged(false);
            updateSurface(true, false, false);
          } 
          onVisibilityChanged(bool);
        } 
      } 
    }
    
    void doOffsetsChanged(boolean param1Boolean) {
      if (this.mDestroyed)
        return; 
      if (!param1Boolean && !this.mOffsetsChanged)
        return; 
      synchronized (this.mLock) {
        float f1 = this.mPendingXOffset;
        float f2 = this.mPendingYOffset;
        float f3 = this.mPendingXOffsetStep;
        float f4 = this.mPendingYOffsetStep;
        param1Boolean = this.mPendingSync;
        int i = 0;
        this.mPendingSync = false;
        this.mOffsetMessageEnqueued = false;
        if (this.mSurfaceCreated)
          if (this.mReportedVisible) {
            if (WallpaperService.DEBUG) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Offsets change in ");
              stringBuilder.append(this);
              stringBuilder.append(": ");
              stringBuilder.append(f1);
              stringBuilder.append(",");
              stringBuilder.append(f2);
              Log.v("WallpaperService", stringBuilder.toString());
            } 
            int j = this.mIWallpaperEngine.mReqWidth - this.mCurWidth;
            if (j > 0) {
              j = -((int)(j * f1 + 0.5F));
            } else {
              j = 0;
            } 
            int k = this.mIWallpaperEngine.mReqHeight - this.mCurHeight;
            if (k > 0)
              i = -((int)(k * f2 + 0.5F)); 
            onOffsetsChanged(f1, f2, f3, f4, j, i);
          } else {
            this.mOffsetsChanged = true;
          }  
        if (param1Boolean)
          try {
            if (WallpaperService.DEBUG)
              Log.v("WallpaperService", "Reporting offsets change complete"); 
            this.mSession.wallpaperOffsetsComplete(this.mWindow.asBinder());
          } catch (RemoteException remoteException) {} 
        return;
      } 
    }
    
    void doCommand(WallpaperService.WallpaperCommand param1WallpaperCommand) {
      Bundle bundle;
      if (!this.mDestroyed) {
        bundle = onCommand(param1WallpaperCommand.action, param1WallpaperCommand.x, param1WallpaperCommand.y, param1WallpaperCommand.z, param1WallpaperCommand.extras, param1WallpaperCommand.sync);
      } else {
        bundle = null;
      } 
      if (param1WallpaperCommand.sync)
        try {
          if (WallpaperService.DEBUG)
            Log.v("WallpaperService", "Reporting command complete"); 
          this.mSession.wallpaperCommandComplete(this.mWindow.asBinder(), bundle);
        } catch (RemoteException remoteException) {} 
    }
    
    void reportSurfaceDestroyed() {
      if (this.mSurfaceCreated) {
        byte b = 0;
        this.mSurfaceCreated = false;
        this.mSurfaceHolder.ungetCallbacks();
        SurfaceHolder.Callback[] arrayOfCallback = this.mSurfaceHolder.getCallbacks();
        if (arrayOfCallback != null)
          for (int i = arrayOfCallback.length; b < i; ) {
            SurfaceHolder.Callback callback = arrayOfCallback[b];
            callback.surfaceDestroyed((SurfaceHolder)this.mSurfaceHolder);
            b++;
          }  
        if (WallpaperService.DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("onSurfaceDestroyed(");
          stringBuilder.append(this.mSurfaceHolder);
          stringBuilder.append("): ");
          stringBuilder.append(this);
          Log.v("WallpaperService", stringBuilder.toString());
        } 
        onSurfaceDestroyed((SurfaceHolder)this.mSurfaceHolder);
      } 
    }
    
    void detach() {
      if (this.mDestroyed)
        return; 
      this.mDestroyed = true;
      if (this.mIWallpaperEngine.mDisplayManager != null)
        this.mIWallpaperEngine.mDisplayManager.unregisterDisplayListener(this.mDisplayListener); 
      if (this.mVisible) {
        this.mVisible = false;
        if (WallpaperService.DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("onVisibilityChanged(false): ");
          stringBuilder.append(this);
          Log.v("WallpaperService", stringBuilder.toString());
        } 
        onVisibilityChanged(false);
      } 
      reportSurfaceDestroyed();
      if (WallpaperService.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onDestroy(): ");
        stringBuilder.append(this);
        Log.v("WallpaperService", stringBuilder.toString());
      } 
      onDestroy();
      this.mIWallpaperEngine.oppoWallpaperServiceHelper.unregisterSettingsContentObserver(this.mDisplayContext);
      if (this.mCreated) {
        try {
          if (WallpaperService.DEBUG) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Removing window and destroying surface ");
            BaseSurfaceHolder baseSurfaceHolder = this.mSurfaceHolder;
            stringBuilder.append(baseSurfaceHolder.getSurface());
            stringBuilder.append(" of: ");
            stringBuilder.append(this);
            String str = stringBuilder.toString();
            Log.v("WallpaperService", str);
          } 
          if (this.mInputEventReceiver != null) {
            this.mInputEventReceiver.dispose();
            this.mInputEventReceiver = null;
          } 
          this.mSession.remove((IWindow)this.mWindow);
        } catch (RemoteException remoteException) {}
        this.mSurfaceHolder.mSurface.release();
        this.mCreated = false;
      } 
    }
    
    public Engine(Supplier<Long> param1Supplier, Handler param1Handler) {
      this.mDisplayListener = (DisplayManager.DisplayListener)new Object(this);
      this.mClockFunction = param1Supplier;
      this.mHandler = param1Handler;
    }
  }
  
  class IWallpaperEngineWrapper extends IWallpaperEngine.Stub implements HandlerCaller.Callback {
    final Rect mDisplayPadding = new Rect();
    
    private final AtomicBoolean mDetached = new AtomicBoolean();
    
    OplusWallpaperServiceHelper oppoWallpaperServiceHelper = new OplusWallpaperServiceHelper();
    
    private final HandlerCaller mCaller;
    
    final IWallpaperConnection mConnection;
    
    final Display mDisplay;
    
    final int mDisplayId;
    
    final DisplayManager mDisplayManager;
    
    WallpaperService.Engine mEngine;
    
    final boolean mIsPreview;
    
    int mReqHeight;
    
    int mReqWidth;
    
    boolean mShownReported;
    
    final IBinder mWindowToken;
    
    final int mWindowType;
    
    final WallpaperService this$0;
    
    IWallpaperEngineWrapper(WallpaperService param1WallpaperService1, IWallpaperConnection param1IWallpaperConnection, IBinder param1IBinder, int param1Int1, boolean param1Boolean, int param1Int2, int param1Int3, Rect param1Rect, int param1Int4) {
      this.mCaller = new HandlerCaller(param1WallpaperService1, param1WallpaperService1.getMainLooper(), this, true);
      this.mConnection = param1IWallpaperConnection;
      this.mWindowToken = param1IBinder;
      this.mWindowType = param1Int1;
      this.mIsPreview = param1Boolean;
      this.mReqWidth = param1Int2;
      this.mReqHeight = param1Int3;
      this.mDisplayPadding.set(param1Rect);
      this.mDisplayId = param1Int4;
      DisplayManager displayManager = WallpaperService.this.<DisplayManager>getSystemService(DisplayManager.class);
      Display display = displayManager.getDisplay(this.mDisplayId);
      if (display != null) {
        Message message = this.mCaller.obtainMessage(10);
        Handler handler = this.mCaller.getHandler();
        if (handler != null)
          handler.sendMessageAtFrontOfQueue(message); 
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Cannot find display with id");
      stringBuilder.append(this.mDisplayId);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public void setDesiredSize(int param1Int1, int param1Int2) {
      Message message = this.mCaller.obtainMessageII(30, param1Int1, param1Int2);
      this.mCaller.sendMessage(message);
    }
    
    public void setDisplayPadding(Rect param1Rect) {
      Message message = this.mCaller.obtainMessageO(40, param1Rect);
      this.mCaller.sendMessage(message);
    }
    
    public void setVisibility(boolean param1Boolean) {
      HandlerCaller handlerCaller = this.mCaller;
      Message message = handlerCaller.obtainMessageI(10010, param1Boolean);
      this.mCaller.sendMessage(message);
    }
    
    public void setInAmbientMode(boolean param1Boolean, long param1Long) throws RemoteException {
      HandlerCaller handlerCaller = this.mCaller;
      Message message = handlerCaller.obtainMessageIO(50, param1Boolean, Long.valueOf(param1Long));
      this.mCaller.sendMessage(message);
    }
    
    public void dispatchPointer(MotionEvent param1MotionEvent) {
      WallpaperService.Engine engine = this.mEngine;
      if (engine != null) {
        engine.dispatchPointer(param1MotionEvent);
      } else {
        param1MotionEvent.recycle();
      } 
    }
    
    public void dispatchWallpaperCommand(String param1String, int param1Int1, int param1Int2, int param1Int3, Bundle param1Bundle) {
      WallpaperService.Engine engine = this.mEngine;
      if (engine != null)
        engine.mWindow.dispatchWallpaperCommand(param1String, param1Int1, param1Int2, param1Int3, param1Bundle, false); 
    }
    
    public void setZoomOut(float param1Float) {
      Message message = this.mCaller.obtainMessageI(10100, Float.floatToIntBits(param1Float));
      this.mCaller.sendMessage(message);
    }
    
    public void reportShown() {
      if (!this.mShownReported) {
        this.mShownReported = true;
        try {
          this.mConnection.engineShown(this);
        } catch (RemoteException remoteException) {
          Log.w("WallpaperService", "Wallpaper host disappeared", (Throwable)remoteException);
          return;
        } 
      } 
    }
    
    public void requestWallpaperColors() {
      Message message = this.mCaller.obtainMessage(10050);
      this.mCaller.sendMessage(message);
    }
    
    public void destroy() {
      Message message = this.mCaller.obtainMessage(20);
      this.mCaller.sendMessage(message);
    }
    
    public void detach() {
      this.mDetached.set(true);
    }
    
    private void doDetachEngine() {
      WallpaperService.this.mActiveEngines.remove(this.mEngine);
      this.mEngine.detach();
      this.oppoWallpaperServiceHelper.removeEngine(this.mEngine);
    }
    
    public void executeMessage(Message param1Message) {
      IWallpaperConnection iWallpaperConnection;
      StringBuilder stringBuilder1;
      WallpaperService.WallpaperCommand wallpaperCommand;
      StringBuilder stringBuilder2;
      MotionEvent motionEvent;
      WallpaperService.Engine engine;
      boolean bool3;
      if (this.mDetached.get()) {
        if (WallpaperService.this.mActiveEngines.contains(this.mEngine))
          doDetachEngine(); 
        return;
      } 
      int i = param1Message.what;
      boolean bool1 = false, bool2 = false;
      switch (i) {
        default:
          stringBuilder2 = new StringBuilder();
          stringBuilder2.append("Unknown message type ");
          stringBuilder2.append(param1Message.what);
          Log.w("WallpaperService", stringBuilder2.toString());
        case 10100:
          this.mEngine.setZoom(Float.intBitsToFloat(param1Message.arg1));
        case 10050:
          iWallpaperConnection = this.mConnection;
          if (iWallpaperConnection != null)
            try {
              iWallpaperConnection.onWallpaperColorsChanged(this.mEngine.onComputeColors(), this.mDisplayId);
            } catch (RemoteException remoteException) {} 
        case 10040:
          i = 0;
          bool3 = false;
          motionEvent = (MotionEvent)((Message)remoteException).obj;
          if (motionEvent.getAction() == 2)
            synchronized (this.mEngine.mLock) {
              if (this.mEngine.mPendingMove == motionEvent) {
                this.mEngine.mPendingMove = null;
                i = bool3;
              } else {
                i = 1;
              } 
            }  
          if (i == 0) {
            if (WallpaperService.DEBUG) {
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append("Delivering touch event: ");
              stringBuilder1.append(motionEvent);
              Log.v("WallpaperService", stringBuilder1.toString());
            } 
            this.mEngine.onTouchEvent(motionEvent);
          } 
          motionEvent.recycle();
        case 10035:
          return;
        case 10030:
          if (((Message)stringBuilder1).arg1 != 0) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          this.mEngine.updateSurface(true, false, bool2);
          this.mEngine.doOffsetsChanged(true);
        case 10025:
          wallpaperCommand = (WallpaperService.WallpaperCommand)((Message)stringBuilder1).obj;
          this.mEngine.doCommand(wallpaperCommand);
        case 10020:
          this.mEngine.doOffsetsChanged(true);
        case 10010:
          if (WallpaperService.DEBUG) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Visibility change in ");
            stringBuilder.append(this.mEngine);
            stringBuilder.append(": ");
            stringBuilder.append(((Message)wallpaperCommand).arg1);
            Log.v("WallpaperService", stringBuilder.toString());
          } 
          engine = this.mEngine;
          if (((Message)wallpaperCommand).arg1 != 0)
            bool2 = true; 
          engine.doVisibilityChanged(bool2);
        case 10000:
          this.mEngine.updateSurface(true, false, true);
        case 50:
          engine = this.mEngine;
          bool2 = bool1;
          if (((Message)wallpaperCommand).arg1 != 0)
            bool2 = true; 
          engine.doAmbientModeChanged(bool2, ((Long)((Message)wallpaperCommand).obj).longValue());
          return;
        case 40:
          this.mEngine.doDisplayPaddingChanged((Rect)((Message)wallpaperCommand).obj);
          return;
        case 30:
          this.mEngine.doDesiredSizeChanged(((Message)wallpaperCommand).arg1, ((Message)wallpaperCommand).arg2);
          return;
        case 20:
          doDetachEngine();
          return;
        case 10:
          break;
      } 
      Trace.traceBegin(8L, "IWallpaperEngineWrapper.DO_ATTACH");
      try {
        this.mConnection.attachEngine(this, this.mDisplayId);
        WallpaperService.Engine engine1 = WallpaperService.this.onCreateEngine();
        this.mEngine = engine1;
        WallpaperService.this.mActiveEngines.add(engine1);
        this.oppoWallpaperServiceHelper.addEngine(engine1);
        Trace.traceBegin(8L, "WallpaperService.Engine.attach");
        engine1.attach(this);
        Trace.traceEnd(8L);
        Trace.traceEnd(8L);
        return;
      } catch (RemoteException remoteException1) {
        Log.w("WallpaperService", "Wallpaper host disappeared", (Throwable)remoteException1);
        return;
      } 
    }
  }
  
  class IWallpaperServiceWrapper extends IWallpaperService.Stub {
    private WallpaperService.IWallpaperEngineWrapper mEngineWrapper;
    
    private final WallpaperService mTarget;
    
    final WallpaperService this$0;
    
    public IWallpaperServiceWrapper(WallpaperService param1WallpaperService1) {
      this.mTarget = param1WallpaperService1;
    }
    
    public void attach(IWallpaperConnection param1IWallpaperConnection, IBinder param1IBinder, int param1Int1, boolean param1Boolean, int param1Int2, int param1Int3, Rect param1Rect, int param1Int4) {
      this.mEngineWrapper = new WallpaperService.IWallpaperEngineWrapper(this.mTarget, param1IWallpaperConnection, param1IBinder, param1Int1, param1Boolean, param1Int2, param1Int3, param1Rect, param1Int4);
    }
    
    public void detach() {
      this.mEngineWrapper.detach();
    }
  }
  
  public void onCreate() {
    super.onCreate();
  }
  
  public void onDestroy() {
    super.onDestroy();
    for (byte b = 0; b < this.mActiveEngines.size(); b++)
      ((Engine)this.mActiveEngines.get(b)).detach(); 
    this.mActiveEngines.clear();
  }
  
  public final IBinder onBind(Intent paramIntent) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onBind: intent = ");
      stringBuilder.append(paramIntent);
      Log.d("WallpaperService", stringBuilder.toString());
    } 
    this.mIsFromSwitchingUser = "fromSwitchingUser".equals(paramIntent.getIdentifier());
    return (IBinder)new IWallpaperServiceWrapper(this);
  }
  
  protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    paramPrintWriter.print("State of wallpaper ");
    paramPrintWriter.print(this);
    paramPrintWriter.println(":");
    for (byte b = 0; b < this.mActiveEngines.size(); b++) {
      Engine engine = this.mActiveEngines.get(b);
      paramPrintWriter.print("  Engine ");
      paramPrintWriter.print(engine);
      paramPrintWriter.println(":");
      engine.dump("    ", paramFileDescriptor, paramPrintWriter, paramArrayOfString);
    } 
  }
  
  public abstract Engine onCreateEngine();
}
