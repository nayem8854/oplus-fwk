package android.service.chooser;

import android.content.ComponentName;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IChooserTargetService extends IInterface {
  void getChooserTargets(ComponentName paramComponentName, IntentFilter paramIntentFilter, IChooserTargetResult paramIChooserTargetResult) throws RemoteException;
  
  class Default implements IChooserTargetService {
    public void getChooserTargets(ComponentName param1ComponentName, IntentFilter param1IntentFilter, IChooserTargetResult param1IChooserTargetResult) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IChooserTargetService {
    private static final String DESCRIPTOR = "android.service.chooser.IChooserTargetService";
    
    static final int TRANSACTION_getChooserTargets = 1;
    
    public Stub() {
      attachInterface(this, "android.service.chooser.IChooserTargetService");
    }
    
    public static IChooserTargetService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.chooser.IChooserTargetService");
      if (iInterface != null && iInterface instanceof IChooserTargetService)
        return (IChooserTargetService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "getChooserTargets";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IntentFilter intentFilter;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.service.chooser.IChooserTargetService");
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.chooser.IChooserTargetService");
      if (param1Parcel1.readInt() != 0) {
        ComponentName componentName = ComponentName.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        intentFilter = IntentFilter.CREATOR.createFromParcel(param1Parcel1);
      } else {
        intentFilter = null;
      } 
      IChooserTargetResult iChooserTargetResult = IChooserTargetResult.Stub.asInterface(param1Parcel1.readStrongBinder());
      getChooserTargets((ComponentName)param1Parcel2, intentFilter, iChooserTargetResult);
      return true;
    }
    
    private static class Proxy implements IChooserTargetService {
      public static IChooserTargetService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.chooser.IChooserTargetService";
      }
      
      public void getChooserTargets(ComponentName param2ComponentName, IntentFilter param2IntentFilter, IChooserTargetResult param2IChooserTargetResult) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.chooser.IChooserTargetService");
          if (param2ComponentName != null) {
            parcel.writeInt(1);
            param2ComponentName.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IntentFilter != null) {
            parcel.writeInt(1);
            param2IntentFilter.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IChooserTargetResult != null) {
            iBinder = param2IChooserTargetResult.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IChooserTargetService.Stub.getDefaultImpl() != null) {
            IChooserTargetService.Stub.getDefaultImpl().getChooserTargets(param2ComponentName, param2IntentFilter, param2IChooserTargetResult);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IChooserTargetService param1IChooserTargetService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IChooserTargetService != null) {
          Proxy.sDefaultImpl = param1IChooserTargetService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IChooserTargetService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
