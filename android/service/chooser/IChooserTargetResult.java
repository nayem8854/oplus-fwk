package android.service.chooser;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IChooserTargetResult extends IInterface {
  void sendResult(List<ChooserTarget> paramList) throws RemoteException;
  
  class Default implements IChooserTargetResult {
    public void sendResult(List<ChooserTarget> param1List) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IChooserTargetResult {
    private static final String DESCRIPTOR = "android.service.chooser.IChooserTargetResult";
    
    static final int TRANSACTION_sendResult = 1;
    
    public Stub() {
      attachInterface(this, "android.service.chooser.IChooserTargetResult");
    }
    
    public static IChooserTargetResult asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.chooser.IChooserTargetResult");
      if (iInterface != null && iInterface instanceof IChooserTargetResult)
        return (IChooserTargetResult)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "sendResult";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.service.chooser.IChooserTargetResult");
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.chooser.IChooserTargetResult");
      ArrayList<ChooserTarget> arrayList = param1Parcel1.createTypedArrayList(ChooserTarget.CREATOR);
      sendResult(arrayList);
      return true;
    }
    
    private static class Proxy implements IChooserTargetResult {
      public static IChooserTargetResult sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.chooser.IChooserTargetResult";
      }
      
      public void sendResult(List<ChooserTarget> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.chooser.IChooserTargetResult");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IChooserTargetResult.Stub.getDefaultImpl() != null) {
            IChooserTargetResult.Stub.getDefaultImpl().sendResult(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IChooserTargetResult param1IChooserTargetResult) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IChooserTargetResult != null) {
          Proxy.sDefaultImpl = param1IChooserTargetResult;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IChooserTargetResult getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
