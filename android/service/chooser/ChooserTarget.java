package android.service.chooser;

import android.content.ComponentName;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

@Deprecated
public final class ChooserTarget implements Parcelable {
  public ChooserTarget(CharSequence paramCharSequence, Icon paramIcon, float paramFloat, ComponentName paramComponentName, Bundle paramBundle) {
    this.mTitle = paramCharSequence;
    this.mIcon = paramIcon;
    if (paramFloat <= 1.0F && paramFloat >= 0.0F) {
      this.mScore = paramFloat;
      this.mComponentName = paramComponentName;
      this.mIntentExtras = paramBundle;
      return;
    } 
    paramCharSequence = new StringBuilder();
    paramCharSequence.append("Score ");
    paramCharSequence.append(paramFloat);
    paramCharSequence.append(" out of range; must be between 0.0f and 1.0f");
    throw new IllegalArgumentException(paramCharSequence.toString());
  }
  
  ChooserTarget(Parcel paramParcel) {
    this.mTitle = paramParcel.readCharSequence();
    if (paramParcel.readInt() != 0) {
      this.mIcon = Icon.CREATOR.createFromParcel(paramParcel);
    } else {
      this.mIcon = null;
    } 
    this.mScore = paramParcel.readFloat();
    this.mComponentName = ComponentName.readFromParcel(paramParcel);
    this.mIntentExtras = paramParcel.readBundle();
  }
  
  public CharSequence getTitle() {
    return this.mTitle;
  }
  
  public Icon getIcon() {
    return this.mIcon;
  }
  
  public float getScore() {
    return this.mScore;
  }
  
  public ComponentName getComponentName() {
    return this.mComponentName;
  }
  
  public Bundle getIntentExtras() {
    return this.mIntentExtras;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ChooserTarget{");
    stringBuilder.append(this.mComponentName);
    stringBuilder.append(", ");
    stringBuilder.append(this.mIntentExtras);
    stringBuilder.append(", '");
    stringBuilder.append(this.mTitle);
    stringBuilder.append("', ");
    stringBuilder.append(this.mScore);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeCharSequence(this.mTitle);
    if (this.mIcon != null) {
      paramParcel.writeInt(1);
      this.mIcon.writeToParcel(paramParcel, 0);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeFloat(this.mScore);
    ComponentName.writeToParcel(this.mComponentName, paramParcel);
    paramParcel.writeBundle(this.mIntentExtras);
  }
  
  public static final Parcelable.Creator<ChooserTarget> CREATOR = new Parcelable.Creator<ChooserTarget>() {
      public ChooserTarget createFromParcel(Parcel param1Parcel) {
        return new ChooserTarget(param1Parcel);
      }
      
      public ChooserTarget[] newArray(int param1Int) {
        return new ChooserTarget[param1Int];
      }
    };
  
  private static final String TAG = "ChooserTarget";
  
  private ComponentName mComponentName;
  
  private Icon mIcon;
  
  private Bundle mIntentExtras;
  
  private float mScore;
  
  private CharSequence mTitle;
}
