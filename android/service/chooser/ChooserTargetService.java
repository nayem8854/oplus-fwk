package android.service.chooser;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.RemoteException;
import java.util.List;

@Deprecated
public abstract class ChooserTargetService extends Service {
  public static final String BIND_PERMISSION = "android.permission.BIND_CHOOSER_TARGET_SERVICE";
  
  private static final boolean DEBUG = false;
  
  public static final String META_DATA_NAME = "android.service.chooser.chooser_target_service";
  
  public static final String SERVICE_INTERFACE = "android.service.chooser.ChooserTargetService";
  
  private final String TAG;
  
  private IChooserTargetServiceWrapper mWrapper;
  
  public ChooserTargetService() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(ChooserTargetService.class.getSimpleName());
    stringBuilder.append('[');
    stringBuilder.append(getClass().getSimpleName());
    stringBuilder.append(']');
    this.TAG = stringBuilder.toString();
    this.mWrapper = null;
  }
  
  public IBinder onBind(Intent paramIntent) {
    if (!"android.service.chooser.ChooserTargetService".equals(paramIntent.getAction()))
      return null; 
    if (this.mWrapper == null)
      this.mWrapper = new IChooserTargetServiceWrapper(); 
    return this.mWrapper;
  }
  
  public abstract List<ChooserTarget> onGetChooserTargets(ComponentName paramComponentName, IntentFilter paramIntentFilter);
  
  private class IChooserTargetServiceWrapper extends IChooserTargetService.Stub {
    final ChooserTargetService this$0;
    
    private IChooserTargetServiceWrapper() {}
    
    public void getChooserTargets(ComponentName param1ComponentName, IntentFilter param1IntentFilter, IChooserTargetResult param1IChooserTargetResult) throws RemoteException {
      long l = clearCallingIdentity();
      try {
        List<ChooserTarget> list = ChooserTargetService.this.onGetChooserTargets(param1ComponentName, param1IntentFilter);
        return;
      } finally {
        restoreCallingIdentity(l);
        param1IChooserTargetResult.sendResult(null);
      } 
    }
  }
}
