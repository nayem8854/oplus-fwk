package android.service.watchdog;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteCallback;
import android.os.RemoteException;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@SystemApi
public abstract class ExplicitHealthCheckService extends Service {
  private final ExplicitHealthCheckServiceWrapper mWrapper = new ExplicitHealthCheckServiceWrapper();
  
  private final Handler mHandler = new Handler(Looper.getMainLooper(), null, true);
  
  private RemoteCallback mCallback;
  
  private static final String TAG = "ExplicitHealthCheckService";
  
  public static final String SERVICE_INTERFACE = "android.service.watchdog.ExplicitHealthCheckService";
  
  public static final String EXTRA_SUPPORTED_PACKAGES = "android.service.watchdog.extra.supported_packages";
  
  public static final String EXTRA_REQUESTED_PACKAGES = "android.service.watchdog.extra.requested_packages";
  
  public static final String EXTRA_HEALTH_CHECK_PASSED_PACKAGE = "android.service.watchdog.extra.health_check_passed_package";
  
  public static final String BIND_PERMISSION = "android.permission.BIND_EXPLICIT_HEALTH_CHECK_SERVICE";
  
  public final IBinder onBind(Intent paramIntent) {
    return (IBinder)this.mWrapper;
  }
  
  public void setCallback(RemoteCallback paramRemoteCallback) {
    this.mCallback = paramRemoteCallback;
  }
  
  public final void notifyHealthCheckPassed(String paramString) {
    this.mHandler.post(new _$$Lambda$ExplicitHealthCheckService$ulagkAZ2bM7_LW9T7PSTxSLQfBE(this, paramString));
  }
  
  public abstract void onCancelHealthCheck(String paramString);
  
  public abstract List<String> onGetRequestedPackages();
  
  public abstract List<PackageConfig> onGetSupportedPackages();
  
  public abstract void onRequestHealthCheck(String paramString);
  
  @SystemApi
  class PackageConfig implements Parcelable {
    public static final Parcelable.Creator<PackageConfig> CREATOR = new Parcelable.Creator<PackageConfig>() {
        public ExplicitHealthCheckService.PackageConfig createFromParcel(Parcel param2Parcel) {
          return new ExplicitHealthCheckService.PackageConfig();
        }
        
        public ExplicitHealthCheckService.PackageConfig[] newArray(int param2Int) {
          return new ExplicitHealthCheckService.PackageConfig[param2Int];
        }
      };
    
    private static final long DEFAULT_HEALTH_CHECK_TIMEOUT_MILLIS = TimeUnit.HOURS.toMillis(1L);
    
    private final long mHealthCheckTimeoutMillis;
    
    private final String mPackageName;
    
    public PackageConfig(long param1Long) {
      this.mPackageName = (String)Preconditions.checkNotNull(this$0);
      if (param1Long == 0L) {
        this.mHealthCheckTimeoutMillis = DEFAULT_HEALTH_CHECK_TIMEOUT_MILLIS;
      } else {
        this.mHealthCheckTimeoutMillis = Preconditions.checkArgumentNonnegative(param1Long);
      } 
    }
    
    private PackageConfig(ExplicitHealthCheckService this$0) {
      this.mPackageName = this$0.readString();
      this.mHealthCheckTimeoutMillis = this$0.readLong();
    }
    
    public String getPackageName() {
      return this.mPackageName;
    }
    
    public long getHealthCheckTimeoutMillis() {
      return this.mHealthCheckTimeoutMillis;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("PackageConfig{");
      stringBuilder.append(this.mPackageName);
      stringBuilder.append(", ");
      stringBuilder.append(this.mHealthCheckTimeoutMillis);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public boolean equals(Object param1Object) {
      null = true;
      if (param1Object == this)
        return true; 
      if (!(param1Object instanceof PackageConfig))
        return false; 
      param1Object = param1Object;
      long l1 = param1Object.getHealthCheckTimeoutMillis(), l2 = this.mHealthCheckTimeoutMillis;
      if (Objects.equals(Long.valueOf(l1), Long.valueOf(l2)))
        if (Objects.equals(param1Object.getPackageName(), this.mPackageName))
          return null;  
      return false;
    }
    
    public int hashCode() {
      return Objects.hash(new Object[] { this.mPackageName, Long.valueOf(this.mHealthCheckTimeoutMillis) });
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeString(this.mPackageName);
      param1Parcel.writeLong(this.mHealthCheckTimeoutMillis);
    }
    
    static {
    
    }
  }
  
  private class ExplicitHealthCheckServiceWrapper extends IExplicitHealthCheckService.Stub {
    final ExplicitHealthCheckService this$0;
    
    private ExplicitHealthCheckServiceWrapper() {}
    
    public void setCallback(RemoteCallback param1RemoteCallback) throws RemoteException {
      ExplicitHealthCheckService.this.mHandler.post(new _$$Lambda$ExplicitHealthCheckService$ExplicitHealthCheckServiceWrapper$EnMyVE8D3ameNypg4gr2IMP7BCo(this, param1RemoteCallback));
    }
    
    public void request(String param1String) throws RemoteException {
      ExplicitHealthCheckService.this.mHandler.post(new _$$Lambda$ExplicitHealthCheckService$ExplicitHealthCheckServiceWrapper$Gn8La3kwBvbjLET_Nqtstvz2RZg(this, param1String));
    }
    
    public void cancel(String param1String) throws RemoteException {
      ExplicitHealthCheckService.this.mHandler.post(new _$$Lambda$ExplicitHealthCheckService$ExplicitHealthCheckServiceWrapper$pC_jIpmynGf4FhRLSRCGbJwUkGE(this, param1String));
    }
    
    public void getSupportedPackages(RemoteCallback param1RemoteCallback) throws RemoteException {
      ExplicitHealthCheckService.this.mHandler.post(new _$$Lambda$ExplicitHealthCheckService$ExplicitHealthCheckServiceWrapper$5Rv9E4_Jc0y0GMGqI_g_82qtYpg(this, param1RemoteCallback));
    }
    
    public void getRequestedPackages(RemoteCallback param1RemoteCallback) throws RemoteException {
      ExplicitHealthCheckService.this.mHandler.post(new _$$Lambda$ExplicitHealthCheckService$ExplicitHealthCheckServiceWrapper$yycuCTr7mDJWrqK_xpXb1sTmkyA(this, param1RemoteCallback));
    }
  }
}
