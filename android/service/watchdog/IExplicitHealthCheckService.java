package android.service.watchdog;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteCallback;
import android.os.RemoteException;

public interface IExplicitHealthCheckService extends IInterface {
  void cancel(String paramString) throws RemoteException;
  
  void getRequestedPackages(RemoteCallback paramRemoteCallback) throws RemoteException;
  
  void getSupportedPackages(RemoteCallback paramRemoteCallback) throws RemoteException;
  
  void request(String paramString) throws RemoteException;
  
  void setCallback(RemoteCallback paramRemoteCallback) throws RemoteException;
  
  class Default implements IExplicitHealthCheckService {
    public void setCallback(RemoteCallback param1RemoteCallback) throws RemoteException {}
    
    public void request(String param1String) throws RemoteException {}
    
    public void cancel(String param1String) throws RemoteException {}
    
    public void getSupportedPackages(RemoteCallback param1RemoteCallback) throws RemoteException {}
    
    public void getRequestedPackages(RemoteCallback param1RemoteCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IExplicitHealthCheckService {
    private static final String DESCRIPTOR = "android.service.watchdog.IExplicitHealthCheckService";
    
    static final int TRANSACTION_cancel = 3;
    
    static final int TRANSACTION_getRequestedPackages = 5;
    
    static final int TRANSACTION_getSupportedPackages = 4;
    
    static final int TRANSACTION_request = 2;
    
    static final int TRANSACTION_setCallback = 1;
    
    public Stub() {
      attachInterface(this, "android.service.watchdog.IExplicitHealthCheckService");
    }
    
    public static IExplicitHealthCheckService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.watchdog.IExplicitHealthCheckService");
      if (iInterface != null && iInterface instanceof IExplicitHealthCheckService)
        return (IExplicitHealthCheckService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "getRequestedPackages";
            } 
            return "getSupportedPackages";
          } 
          return "cancel";
        } 
        return "request";
      } 
      return "setCallback";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.service.watchdog.IExplicitHealthCheckService");
                return true;
              } 
              param1Parcel1.enforceInterface("android.service.watchdog.IExplicitHealthCheckService");
              if (param1Parcel1.readInt() != 0) {
                RemoteCallback remoteCallback = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel(param1Parcel1);
              } else {
                param1Parcel1 = null;
              } 
              getRequestedPackages((RemoteCallback)param1Parcel1);
              return true;
            } 
            param1Parcel1.enforceInterface("android.service.watchdog.IExplicitHealthCheckService");
            if (param1Parcel1.readInt() != 0) {
              RemoteCallback remoteCallback = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            getSupportedPackages((RemoteCallback)param1Parcel1);
            return true;
          } 
          param1Parcel1.enforceInterface("android.service.watchdog.IExplicitHealthCheckService");
          str = param1Parcel1.readString();
          cancel(str);
          return true;
        } 
        str.enforceInterface("android.service.watchdog.IExplicitHealthCheckService");
        str = str.readString();
        request(str);
        return true;
      } 
      str.enforceInterface("android.service.watchdog.IExplicitHealthCheckService");
      if (str.readInt() != 0) {
        RemoteCallback remoteCallback = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel((Parcel)str);
      } else {
        str = null;
      } 
      setCallback((RemoteCallback)str);
      return true;
    }
    
    private static class Proxy implements IExplicitHealthCheckService {
      public static IExplicitHealthCheckService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.watchdog.IExplicitHealthCheckService";
      }
      
      public void setCallback(RemoteCallback param2RemoteCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.watchdog.IExplicitHealthCheckService");
          if (param2RemoteCallback != null) {
            parcel.writeInt(1);
            param2RemoteCallback.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IExplicitHealthCheckService.Stub.getDefaultImpl() != null) {
            IExplicitHealthCheckService.Stub.getDefaultImpl().setCallback(param2RemoteCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void request(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.watchdog.IExplicitHealthCheckService");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IExplicitHealthCheckService.Stub.getDefaultImpl() != null) {
            IExplicitHealthCheckService.Stub.getDefaultImpl().request(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void cancel(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.watchdog.IExplicitHealthCheckService");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IExplicitHealthCheckService.Stub.getDefaultImpl() != null) {
            IExplicitHealthCheckService.Stub.getDefaultImpl().cancel(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getSupportedPackages(RemoteCallback param2RemoteCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.watchdog.IExplicitHealthCheckService");
          if (param2RemoteCallback != null) {
            parcel.writeInt(1);
            param2RemoteCallback.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IExplicitHealthCheckService.Stub.getDefaultImpl() != null) {
            IExplicitHealthCheckService.Stub.getDefaultImpl().getSupportedPackages(param2RemoteCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getRequestedPackages(RemoteCallback param2RemoteCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.watchdog.IExplicitHealthCheckService");
          if (param2RemoteCallback != null) {
            parcel.writeInt(1);
            param2RemoteCallback.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IExplicitHealthCheckService.Stub.getDefaultImpl() != null) {
            IExplicitHealthCheckService.Stub.getDefaultImpl().getRequestedPackages(param2RemoteCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IExplicitHealthCheckService param1IExplicitHealthCheckService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IExplicitHealthCheckService != null) {
          Proxy.sDefaultImpl = param1IExplicitHealthCheckService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IExplicitHealthCheckService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
