package android.service.euicc;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.euicc.DownloadableSubscription;

@SystemApi
public final class GetDownloadableSubscriptionMetadataResult implements Parcelable {
  public static final Parcelable.Creator<GetDownloadableSubscriptionMetadataResult> CREATOR = new Parcelable.Creator<GetDownloadableSubscriptionMetadataResult>() {
      public GetDownloadableSubscriptionMetadataResult createFromParcel(Parcel param1Parcel) {
        return new GetDownloadableSubscriptionMetadataResult(param1Parcel);
      }
      
      public GetDownloadableSubscriptionMetadataResult[] newArray(int param1Int) {
        return new GetDownloadableSubscriptionMetadataResult[param1Int];
      }
    };
  
  private final DownloadableSubscription mSubscription;
  
  @Deprecated
  public final int result;
  
  public int getResult() {
    return this.result;
  }
  
  public DownloadableSubscription getDownloadableSubscription() {
    return this.mSubscription;
  }
  
  public GetDownloadableSubscriptionMetadataResult(int paramInt, DownloadableSubscription paramDownloadableSubscription) {
    this.result = paramInt;
    if (paramInt == 0) {
      this.mSubscription = paramDownloadableSubscription;
    } else {
      if (paramDownloadableSubscription == null) {
        this.mSubscription = null;
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error result with non-null subscription: ");
      stringBuilder.append(paramInt);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
  }
  
  private GetDownloadableSubscriptionMetadataResult(Parcel paramParcel) {
    this.result = paramParcel.readInt();
    this.mSubscription = paramParcel.<DownloadableSubscription>readTypedObject(DownloadableSubscription.CREATOR);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.result);
    paramParcel.writeTypedObject(this.mSubscription, paramInt);
  }
  
  public int describeContents() {
    return 0;
  }
}
