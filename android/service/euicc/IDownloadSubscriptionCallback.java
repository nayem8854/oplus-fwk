package android.service.euicc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IDownloadSubscriptionCallback extends IInterface {
  void onComplete(DownloadSubscriptionResult paramDownloadSubscriptionResult) throws RemoteException;
  
  class Default implements IDownloadSubscriptionCallback {
    public void onComplete(DownloadSubscriptionResult param1DownloadSubscriptionResult) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDownloadSubscriptionCallback {
    private static final String DESCRIPTOR = "android.service.euicc.IDownloadSubscriptionCallback";
    
    static final int TRANSACTION_onComplete = 1;
    
    public Stub() {
      attachInterface(this, "android.service.euicc.IDownloadSubscriptionCallback");
    }
    
    public static IDownloadSubscriptionCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.euicc.IDownloadSubscriptionCallback");
      if (iInterface != null && iInterface instanceof IDownloadSubscriptionCallback)
        return (IDownloadSubscriptionCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onComplete";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.service.euicc.IDownloadSubscriptionCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.euicc.IDownloadSubscriptionCallback");
      if (param1Parcel1.readInt() != 0) {
        DownloadSubscriptionResult downloadSubscriptionResult = DownloadSubscriptionResult.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onComplete((DownloadSubscriptionResult)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IDownloadSubscriptionCallback {
      public static IDownloadSubscriptionCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.euicc.IDownloadSubscriptionCallback";
      }
      
      public void onComplete(DownloadSubscriptionResult param2DownloadSubscriptionResult) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.euicc.IDownloadSubscriptionCallback");
          if (param2DownloadSubscriptionResult != null) {
            parcel.writeInt(1);
            param2DownloadSubscriptionResult.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IDownloadSubscriptionCallback.Stub.getDefaultImpl() != null) {
            IDownloadSubscriptionCallback.Stub.getDefaultImpl().onComplete(param2DownloadSubscriptionResult);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDownloadSubscriptionCallback param1IDownloadSubscriptionCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDownloadSubscriptionCallback != null) {
          Proxy.sDefaultImpl = param1IDownloadSubscriptionCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDownloadSubscriptionCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
