package android.service.euicc;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.List;

@SystemApi
public final class GetEuiccProfileInfoListResult implements Parcelable {
  public static final Parcelable.Creator<GetEuiccProfileInfoListResult> CREATOR = new Parcelable.Creator<GetEuiccProfileInfoListResult>() {
      public GetEuiccProfileInfoListResult createFromParcel(Parcel param1Parcel) {
        return new GetEuiccProfileInfoListResult(param1Parcel);
      }
      
      public GetEuiccProfileInfoListResult[] newArray(int param1Int) {
        return new GetEuiccProfileInfoListResult[param1Int];
      }
    };
  
  private final boolean mIsRemovable;
  
  private final EuiccProfileInfo[] mProfiles;
  
  @Deprecated
  public final int result;
  
  public int getResult() {
    return this.result;
  }
  
  public List<EuiccProfileInfo> getProfiles() {
    EuiccProfileInfo[] arrayOfEuiccProfileInfo = this.mProfiles;
    if (arrayOfEuiccProfileInfo == null)
      return null; 
    return Arrays.asList(arrayOfEuiccProfileInfo);
  }
  
  public boolean getIsRemovable() {
    return this.mIsRemovable;
  }
  
  public GetEuiccProfileInfoListResult(int paramInt, EuiccProfileInfo[] paramArrayOfEuiccProfileInfo, boolean paramBoolean) {
    this.result = paramInt;
    this.mIsRemovable = paramBoolean;
    if (paramInt == 0) {
      this.mProfiles = paramArrayOfEuiccProfileInfo;
    } else {
      if (paramArrayOfEuiccProfileInfo == null || paramArrayOfEuiccProfileInfo.length <= 0) {
        this.mProfiles = null;
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error result with non-empty profiles: ");
      stringBuilder.append(paramInt);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
  }
  
  private GetEuiccProfileInfoListResult(Parcel paramParcel) {
    this.result = paramParcel.readInt();
    this.mProfiles = paramParcel.<EuiccProfileInfo>createTypedArray(EuiccProfileInfo.CREATOR);
    this.mIsRemovable = paramParcel.readBoolean();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.result);
    paramParcel.writeTypedArray(this.mProfiles, paramInt);
    paramParcel.writeBoolean(this.mIsRemovable);
  }
  
  public int describeContents() {
    return 0;
  }
}
