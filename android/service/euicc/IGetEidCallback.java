package android.service.euicc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IGetEidCallback extends IInterface {
  void onSuccess(String paramString) throws RemoteException;
  
  class Default implements IGetEidCallback {
    public void onSuccess(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IGetEidCallback {
    private static final String DESCRIPTOR = "android.service.euicc.IGetEidCallback";
    
    static final int TRANSACTION_onSuccess = 1;
    
    public Stub() {
      attachInterface(this, "android.service.euicc.IGetEidCallback");
    }
    
    public static IGetEidCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.euicc.IGetEidCallback");
      if (iInterface != null && iInterface instanceof IGetEidCallback)
        return (IGetEidCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onSuccess";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.service.euicc.IGetEidCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.euicc.IGetEidCallback");
      String str = param1Parcel1.readString();
      onSuccess(str);
      return true;
    }
    
    private static class Proxy implements IGetEidCallback {
      public static IGetEidCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.euicc.IGetEidCallback";
      }
      
      public void onSuccess(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.euicc.IGetEidCallback");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IGetEidCallback.Stub.getDefaultImpl() != null) {
            IGetEidCallback.Stub.getDefaultImpl().onSuccess(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IGetEidCallback param1IGetEidCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IGetEidCallback != null) {
          Proxy.sDefaultImpl = param1IGetEidCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IGetEidCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
