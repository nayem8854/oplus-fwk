package android.service.euicc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISwitchToSubscriptionCallback extends IInterface {
  void onComplete(int paramInt) throws RemoteException;
  
  class Default implements ISwitchToSubscriptionCallback {
    public void onComplete(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISwitchToSubscriptionCallback {
    private static final String DESCRIPTOR = "android.service.euicc.ISwitchToSubscriptionCallback";
    
    static final int TRANSACTION_onComplete = 1;
    
    public Stub() {
      attachInterface(this, "android.service.euicc.ISwitchToSubscriptionCallback");
    }
    
    public static ISwitchToSubscriptionCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.euicc.ISwitchToSubscriptionCallback");
      if (iInterface != null && iInterface instanceof ISwitchToSubscriptionCallback)
        return (ISwitchToSubscriptionCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onComplete";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.service.euicc.ISwitchToSubscriptionCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.euicc.ISwitchToSubscriptionCallback");
      param1Int1 = param1Parcel1.readInt();
      onComplete(param1Int1);
      return true;
    }
    
    private static class Proxy implements ISwitchToSubscriptionCallback {
      public static ISwitchToSubscriptionCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.euicc.ISwitchToSubscriptionCallback";
      }
      
      public void onComplete(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.euicc.ISwitchToSubscriptionCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ISwitchToSubscriptionCallback.Stub.getDefaultImpl() != null) {
            ISwitchToSubscriptionCallback.Stub.getDefaultImpl().onComplete(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISwitchToSubscriptionCallback param1ISwitchToSubscriptionCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISwitchToSubscriptionCallback != null) {
          Proxy.sDefaultImpl = param1ISwitchToSubscriptionCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISwitchToSubscriptionCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
