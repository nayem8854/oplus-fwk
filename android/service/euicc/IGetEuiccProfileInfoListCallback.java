package android.service.euicc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IGetEuiccProfileInfoListCallback extends IInterface {
  void onComplete(GetEuiccProfileInfoListResult paramGetEuiccProfileInfoListResult) throws RemoteException;
  
  class Default implements IGetEuiccProfileInfoListCallback {
    public void onComplete(GetEuiccProfileInfoListResult param1GetEuiccProfileInfoListResult) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IGetEuiccProfileInfoListCallback {
    private static final String DESCRIPTOR = "android.service.euicc.IGetEuiccProfileInfoListCallback";
    
    static final int TRANSACTION_onComplete = 1;
    
    public Stub() {
      attachInterface(this, "android.service.euicc.IGetEuiccProfileInfoListCallback");
    }
    
    public static IGetEuiccProfileInfoListCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.euicc.IGetEuiccProfileInfoListCallback");
      if (iInterface != null && iInterface instanceof IGetEuiccProfileInfoListCallback)
        return (IGetEuiccProfileInfoListCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onComplete";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.service.euicc.IGetEuiccProfileInfoListCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.euicc.IGetEuiccProfileInfoListCallback");
      if (param1Parcel1.readInt() != 0) {
        GetEuiccProfileInfoListResult getEuiccProfileInfoListResult = GetEuiccProfileInfoListResult.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onComplete((GetEuiccProfileInfoListResult)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IGetEuiccProfileInfoListCallback {
      public static IGetEuiccProfileInfoListCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.euicc.IGetEuiccProfileInfoListCallback";
      }
      
      public void onComplete(GetEuiccProfileInfoListResult param2GetEuiccProfileInfoListResult) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.euicc.IGetEuiccProfileInfoListCallback");
          if (param2GetEuiccProfileInfoListResult != null) {
            parcel.writeInt(1);
            param2GetEuiccProfileInfoListResult.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IGetEuiccProfileInfoListCallback.Stub.getDefaultImpl() != null) {
            IGetEuiccProfileInfoListCallback.Stub.getDefaultImpl().onComplete(param2GetEuiccProfileInfoListResult);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IGetEuiccProfileInfoListCallback param1IGetEuiccProfileInfoListCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IGetEuiccProfileInfoListCallback != null) {
          Proxy.sDefaultImpl = param1IGetEuiccProfileInfoListCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IGetEuiccProfileInfoListCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
