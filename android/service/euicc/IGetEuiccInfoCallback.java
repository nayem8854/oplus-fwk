package android.service.euicc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.euicc.EuiccInfo;

public interface IGetEuiccInfoCallback extends IInterface {
  void onSuccess(EuiccInfo paramEuiccInfo) throws RemoteException;
  
  class Default implements IGetEuiccInfoCallback {
    public void onSuccess(EuiccInfo param1EuiccInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IGetEuiccInfoCallback {
    private static final String DESCRIPTOR = "android.service.euicc.IGetEuiccInfoCallback";
    
    static final int TRANSACTION_onSuccess = 1;
    
    public Stub() {
      attachInterface(this, "android.service.euicc.IGetEuiccInfoCallback");
    }
    
    public static IGetEuiccInfoCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.euicc.IGetEuiccInfoCallback");
      if (iInterface != null && iInterface instanceof IGetEuiccInfoCallback)
        return (IGetEuiccInfoCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onSuccess";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.service.euicc.IGetEuiccInfoCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.euicc.IGetEuiccInfoCallback");
      if (param1Parcel1.readInt() != 0) {
        EuiccInfo euiccInfo = EuiccInfo.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onSuccess((EuiccInfo)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IGetEuiccInfoCallback {
      public static IGetEuiccInfoCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.euicc.IGetEuiccInfoCallback";
      }
      
      public void onSuccess(EuiccInfo param2EuiccInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.euicc.IGetEuiccInfoCallback");
          if (param2EuiccInfo != null) {
            parcel.writeInt(1);
            param2EuiccInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IGetEuiccInfoCallback.Stub.getDefaultImpl() != null) {
            IGetEuiccInfoCallback.Stub.getDefaultImpl().onSuccess(param2EuiccInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IGetEuiccInfoCallback param1IGetEuiccInfoCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IGetEuiccInfoCallback != null) {
          Proxy.sDefaultImpl = param1IGetEuiccInfoCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IGetEuiccInfoCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
