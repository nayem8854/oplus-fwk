package android.service.euicc;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.service.carrier.CarrierIdentifier;
import android.telephony.UiccAccessRule;
import android.text.TextUtils;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@SystemApi
public final class EuiccProfileInfo implements Parcelable {
  public static final Parcelable.Creator<EuiccProfileInfo> CREATOR = new Parcelable.Creator<EuiccProfileInfo>() {
      public EuiccProfileInfo createFromParcel(Parcel param1Parcel) {
        return new EuiccProfileInfo(param1Parcel);
      }
      
      public EuiccProfileInfo[] newArray(int param1Int) {
        return new EuiccProfileInfo[param1Int];
      }
    };
  
  public static final int POLICY_RULE_DELETE_AFTER_DISABLING = 4;
  
  public static final int POLICY_RULE_DO_NOT_DELETE = 2;
  
  public static final int POLICY_RULE_DO_NOT_DISABLE = 1;
  
  public static final int PROFILE_CLASS_OPERATIONAL = 2;
  
  public static final int PROFILE_CLASS_PROVISIONING = 1;
  
  public static final int PROFILE_CLASS_TESTING = 0;
  
  public static final int PROFILE_CLASS_UNSET = -1;
  
  public static final int PROFILE_STATE_DISABLED = 0;
  
  public static final int PROFILE_STATE_ENABLED = 1;
  
  public static final int PROFILE_STATE_UNSET = -1;
  
  private final UiccAccessRule[] mAccessRules;
  
  private final CarrierIdentifier mCarrierIdentifier;
  
  private final String mIccid;
  
  private final String mNickname;
  
  private final int mPolicyRules;
  
  private final int mProfileClass;
  
  private final String mProfileName;
  
  private final String mServiceProviderName;
  
  private final int mState;
  
  @Deprecated
  public EuiccProfileInfo(String paramString1, UiccAccessRule[] paramArrayOfUiccAccessRule, String paramString2) {
    if (TextUtils.isDigitsOnly(paramString1)) {
      this.mIccid = paramString1;
      this.mAccessRules = paramArrayOfUiccAccessRule;
      this.mNickname = paramString2;
      this.mServiceProviderName = null;
      this.mProfileName = null;
      this.mProfileClass = -1;
      this.mState = -1;
      this.mCarrierIdentifier = null;
      this.mPolicyRules = 0;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("iccid contains invalid characters: ");
    stringBuilder.append(paramString1);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private EuiccProfileInfo(Parcel paramParcel) {
    this.mIccid = paramParcel.readString();
    this.mNickname = paramParcel.readString();
    this.mServiceProviderName = paramParcel.readString();
    this.mProfileName = paramParcel.readString();
    this.mProfileClass = paramParcel.readInt();
    this.mState = paramParcel.readInt();
    byte b = paramParcel.readByte();
    if (b == 1) {
      this.mCarrierIdentifier = CarrierIdentifier.CREATOR.createFromParcel(paramParcel);
    } else {
      this.mCarrierIdentifier = null;
    } 
    this.mPolicyRules = paramParcel.readInt();
    this.mAccessRules = paramParcel.<UiccAccessRule>createTypedArray(UiccAccessRule.CREATOR);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mIccid);
    paramParcel.writeString(this.mNickname);
    paramParcel.writeString(this.mServiceProviderName);
    paramParcel.writeString(this.mProfileName);
    paramParcel.writeInt(this.mProfileClass);
    paramParcel.writeInt(this.mState);
    if (this.mCarrierIdentifier != null) {
      paramParcel.writeByte((byte)1);
      this.mCarrierIdentifier.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeByte((byte)0);
    } 
    paramParcel.writeInt(this.mPolicyRules);
    paramParcel.writeTypedArray(this.mAccessRules, paramInt);
  }
  
  public int describeContents() {
    return 0;
  }
  
  class Builder {
    private List<UiccAccessRule> mAccessRules;
    
    private CarrierIdentifier mCarrierIdentifier;
    
    private String mIccid;
    
    private String mNickname;
    
    private int mPolicyRules;
    
    private int mProfileClass;
    
    private String mProfileName;
    
    private String mServiceProviderName;
    
    private int mState;
    
    public Builder(EuiccProfileInfo this$0) {
      if (TextUtils.isDigitsOnly((CharSequence)this$0)) {
        this.mIccid = (String)this$0;
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("iccid contains invalid characters: ");
      stringBuilder.append((String)this$0);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public Builder(EuiccProfileInfo this$0) {
      this.mIccid = this$0.mIccid;
      this.mNickname = this$0.mNickname;
      this.mServiceProviderName = this$0.mServiceProviderName;
      this.mProfileName = this$0.mProfileName;
      this.mProfileClass = this$0.mProfileClass;
      this.mState = this$0.mState;
      this.mCarrierIdentifier = this$0.mCarrierIdentifier;
      this.mPolicyRules = this$0.mPolicyRules;
      this.mAccessRules = Arrays.asList(this$0.mAccessRules);
    }
    
    public EuiccProfileInfo build() {
      if (this.mIccid != null)
        return new EuiccProfileInfo(this.mIccid, this.mNickname, this.mServiceProviderName, this.mProfileName, this.mProfileClass, this.mState, this.mCarrierIdentifier, this.mPolicyRules, this.mAccessRules); 
      throw new IllegalStateException("ICCID must be set for a profile.");
    }
    
    public Builder setIccid(String param1String) {
      if (TextUtils.isDigitsOnly(param1String)) {
        this.mIccid = param1String;
        return this;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("iccid contains invalid characters: ");
      stringBuilder.append(param1String);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public Builder setNickname(String param1String) {
      this.mNickname = param1String;
      return this;
    }
    
    public Builder setServiceProviderName(String param1String) {
      this.mServiceProviderName = param1String;
      return this;
    }
    
    public Builder setProfileName(String param1String) {
      this.mProfileName = param1String;
      return this;
    }
    
    public Builder setProfileClass(int param1Int) {
      this.mProfileClass = param1Int;
      return this;
    }
    
    public Builder setState(int param1Int) {
      this.mState = param1Int;
      return this;
    }
    
    public Builder setCarrierIdentifier(CarrierIdentifier param1CarrierIdentifier) {
      this.mCarrierIdentifier = param1CarrierIdentifier;
      return this;
    }
    
    public Builder setPolicyRules(int param1Int) {
      this.mPolicyRules = param1Int;
      return this;
    }
    
    public Builder setUiccAccessRule(List<UiccAccessRule> param1List) {
      this.mAccessRules = param1List;
      return this;
    }
  }
  
  private EuiccProfileInfo(String paramString1, String paramString2, String paramString3, String paramString4, int paramInt1, int paramInt2, CarrierIdentifier paramCarrierIdentifier, int paramInt3, List<UiccAccessRule> paramList) {
    this.mIccid = paramString1;
    this.mNickname = paramString2;
    this.mServiceProviderName = paramString3;
    this.mProfileName = paramString4;
    this.mProfileClass = paramInt1;
    this.mState = paramInt2;
    this.mCarrierIdentifier = paramCarrierIdentifier;
    this.mPolicyRules = paramInt3;
    if (paramList != null && paramList.size() > 0) {
      this.mAccessRules = paramList.<UiccAccessRule>toArray(new UiccAccessRule[paramList.size()]);
    } else {
      this.mAccessRules = null;
    } 
  }
  
  public String getIccid() {
    return this.mIccid;
  }
  
  public List<UiccAccessRule> getUiccAccessRules() {
    UiccAccessRule[] arrayOfUiccAccessRule = this.mAccessRules;
    if (arrayOfUiccAccessRule == null)
      return null; 
    return Arrays.asList(arrayOfUiccAccessRule);
  }
  
  public String getNickname() {
    return this.mNickname;
  }
  
  public String getServiceProviderName() {
    return this.mServiceProviderName;
  }
  
  public String getProfileName() {
    return this.mProfileName;
  }
  
  public int getProfileClass() {
    return this.mProfileClass;
  }
  
  public int getState() {
    return this.mState;
  }
  
  public CarrierIdentifier getCarrierIdentifier() {
    return this.mCarrierIdentifier;
  }
  
  public int getPolicyRules() {
    return this.mPolicyRules;
  }
  
  public boolean hasPolicyRules() {
    boolean bool;
    if (this.mPolicyRules != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean hasPolicyRule(int paramInt) {
    boolean bool;
    if ((this.mPolicyRules & paramInt) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (Objects.equals(this.mIccid, ((EuiccProfileInfo)paramObject).mIccid)) {
      String str1 = this.mNickname, str2 = ((EuiccProfileInfo)paramObject).mNickname;
      if (Objects.equals(str1, str2)) {
        str1 = this.mServiceProviderName;
        str2 = ((EuiccProfileInfo)paramObject).mServiceProviderName;
        if (Objects.equals(str1, str2)) {
          str2 = this.mProfileName;
          str1 = ((EuiccProfileInfo)paramObject).mProfileName;
          if (Objects.equals(str2, str1) && this.mProfileClass == ((EuiccProfileInfo)paramObject).mProfileClass && this.mState == ((EuiccProfileInfo)paramObject).mState) {
            CarrierIdentifier carrierIdentifier2 = this.mCarrierIdentifier, carrierIdentifier1 = ((EuiccProfileInfo)paramObject).mCarrierIdentifier;
            if (Objects.equals(carrierIdentifier2, carrierIdentifier1) && this.mPolicyRules == ((EuiccProfileInfo)paramObject).mPolicyRules) {
              UiccAccessRule[] arrayOfUiccAccessRule = this.mAccessRules;
              paramObject = ((EuiccProfileInfo)paramObject).mAccessRules;
              if (Arrays.equals((Object[])arrayOfUiccAccessRule, (Object[])paramObject))
                return null; 
            } 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = Objects.hashCode(this.mIccid);
    int j = Objects.hashCode(this.mNickname);
    int k = Objects.hashCode(this.mServiceProviderName);
    int m = Objects.hashCode(this.mProfileName);
    int n = this.mProfileClass;
    int i1 = this.mState;
    int i2 = Objects.hashCode(this.mCarrierIdentifier);
    int i3 = this.mPolicyRules;
    int i4 = Arrays.hashCode((Object[])this.mAccessRules);
    return ((((((((1 * 31 + i) * 31 + j) * 31 + k) * 31 + m) * 31 + n) * 31 + i1) * 31 + i2) * 31 + i3) * 31 + i4;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("EuiccProfileInfo (nickname=");
    stringBuilder.append(this.mNickname);
    stringBuilder.append(", serviceProviderName=");
    stringBuilder.append(this.mServiceProviderName);
    stringBuilder.append(", profileName=");
    stringBuilder.append(this.mProfileName);
    stringBuilder.append(", profileClass=");
    stringBuilder.append(this.mProfileClass);
    stringBuilder.append(", state=");
    stringBuilder.append(this.mState);
    stringBuilder.append(", CarrierIdentifier=");
    stringBuilder.append(this.mCarrierIdentifier);
    stringBuilder.append(", policyRules=");
    stringBuilder.append(this.mPolicyRules);
    stringBuilder.append(", accessRules=");
    UiccAccessRule[] arrayOfUiccAccessRule = this.mAccessRules;
    stringBuilder.append(Arrays.toString((Object[])arrayOfUiccAccessRule));
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class PolicyRule implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ProfileClass implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ProfileState implements Annotation {}
}
