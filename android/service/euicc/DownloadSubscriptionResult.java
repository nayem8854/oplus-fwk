package android.service.euicc;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class DownloadSubscriptionResult implements Parcelable {
  public static final Parcelable.Creator<DownloadSubscriptionResult> CREATOR = (Parcelable.Creator<DownloadSubscriptionResult>)new Object();
  
  private final int mCardId;
  
  private final int mResolvableErrors;
  
  private final int mResult;
  
  public DownloadSubscriptionResult(int paramInt1, int paramInt2, int paramInt3) {
    this.mResult = paramInt1;
    this.mResolvableErrors = paramInt2;
    this.mCardId = paramInt3;
  }
  
  public int getResult() {
    return this.mResult;
  }
  
  public int getResolvableErrors() {
    return this.mResolvableErrors;
  }
  
  public int getCardId() {
    return this.mCardId;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mResult);
    paramParcel.writeInt(this.mResolvableErrors);
    paramParcel.writeInt(this.mCardId);
  }
  
  public int describeContents() {
    return 0;
  }
  
  private DownloadSubscriptionResult(Parcel paramParcel) {
    this.mResult = paramParcel.readInt();
    this.mResolvableErrors = paramParcel.readInt();
    this.mCardId = paramParcel.readInt();
  }
}
