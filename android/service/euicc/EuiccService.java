package android.service.euicc;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.telephony.euicc.DownloadableSubscription;
import android.telephony.euicc.EuiccInfo;
import android.text.TextUtils;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@SystemApi
public abstract class EuiccService extends Service {
  public static final String ACTION_BIND_CARRIER_PROVISIONING_SERVICE = "android.service.euicc.action.BIND_CARRIER_PROVISIONING_SERVICE";
  
  public static final String ACTION_DELETE_SUBSCRIPTION_PRIVILEGED = "android.service.euicc.action.DELETE_SUBSCRIPTION_PRIVILEGED";
  
  public static final String ACTION_MANAGE_EMBEDDED_SUBSCRIPTIONS = "android.service.euicc.action.MANAGE_EMBEDDED_SUBSCRIPTIONS";
  
  public static final String ACTION_PROVISION_EMBEDDED_SUBSCRIPTION = "android.service.euicc.action.PROVISION_EMBEDDED_SUBSCRIPTION";
  
  public static final String ACTION_RENAME_SUBSCRIPTION_PRIVILEGED = "android.service.euicc.action.RENAME_SUBSCRIPTION_PRIVILEGED";
  
  @Deprecated
  public static final String ACTION_RESOLVE_CONFIRMATION_CODE = "android.service.euicc.action.RESOLVE_CONFIRMATION_CODE";
  
  public static final String ACTION_RESOLVE_DEACTIVATE_SIM = "android.service.euicc.action.RESOLVE_DEACTIVATE_SIM";
  
  public static final String ACTION_RESOLVE_NO_PRIVILEGES = "android.service.euicc.action.RESOLVE_NO_PRIVILEGES";
  
  public static final String ACTION_RESOLVE_RESOLVABLE_ERRORS = "android.service.euicc.action.RESOLVE_RESOLVABLE_ERRORS";
  
  public static final String ACTION_START_CARRIER_ACTIVATION = "android.service.euicc.action.START_CARRIER_ACTIVATION";
  
  public static final String ACTION_START_EUICC_ACTIVATION = "android.service.euicc.action.START_EUICC_ACTIVATION";
  
  public static final String ACTION_TOGGLE_SUBSCRIPTION_PRIVILEGED = "android.service.euicc.action.TOGGLE_SUBSCRIPTION_PRIVILEGED";
  
  public static final String CATEGORY_EUICC_UI = "android.service.euicc.category.EUICC_UI";
  
  public static final String EUICC_SERVICE_INTERFACE = "android.service.euicc.EuiccService";
  
  public static final String EXTRA_RESOLUTION_ALLOW_POLICY_RULES = "android.service.euicc.extra.RESOLUTION_ALLOW_POLICY_RULES";
  
  public static final String EXTRA_RESOLUTION_CALLING_PACKAGE = "android.service.euicc.extra.RESOLUTION_CALLING_PACKAGE";
  
  public static final String EXTRA_RESOLUTION_CARD_ID = "android.service.euicc.extra.RESOLUTION_CARD_ID";
  
  public static final String EXTRA_RESOLUTION_CONFIRMATION_CODE = "android.service.euicc.extra.RESOLUTION_CONFIRMATION_CODE";
  
  public static final String EXTRA_RESOLUTION_CONFIRMATION_CODE_RETRIED = "android.service.euicc.extra.RESOLUTION_CONFIRMATION_CODE_RETRIED";
  
  public static final String EXTRA_RESOLUTION_CONSENT = "android.service.euicc.extra.RESOLUTION_CONSENT";
  
  public static final String EXTRA_RESOLVABLE_ERRORS = "android.service.euicc.extra.RESOLVABLE_ERRORS";
  
  public static final int RESOLVABLE_ERROR_CONFIRMATION_CODE = 1;
  
  public static final int RESOLVABLE_ERROR_POLICY_RULES = 2;
  
  public static final int RESULT_FIRST_USER = 1;
  
  public static final int RESULT_MUST_DEACTIVATE_SIM = -1;
  
  @Deprecated
  public static final int RESULT_NEED_CONFIRMATION_CODE = -2;
  
  public static final int RESULT_OK = 0;
  
  public static final int RESULT_RESOLVABLE_ERRORS = -2;
  
  private static final String TAG = "EuiccService";
  
  private ThreadPoolExecutor mExecutor;
  
  private final IEuiccService.Stub mStubWrapper = new IEuiccServiceWrapper();
  
  public int encodeSmdxSubjectAndReasonCode(String paramString1, String paramString2) throws NumberFormatException, IllegalArgumentException, UnsupportedOperationException {
    if (!TextUtils.isEmpty(paramString1) && !TextUtils.isEmpty(paramString2)) {
      String[] arrayOfString1 = paramString1.split("\\.");
      String[] arrayOfString2 = paramString2.split("\\.");
      if (arrayOfString1.length <= 3 && arrayOfString2.length <= 3) {
        int i = 10 << (3 - arrayOfString1.length) * 4;
        int j, k, m;
        for (j = arrayOfString1.length, k = 0, m = 0; m < j; ) {
          String str = arrayOfString1[m];
          int n = Integer.parseInt(str);
          if (n <= 15) {
            i = (i << 4) + n;
            m++;
          } 
          throw new UnsupportedOperationException("SubjectCode exceeds 15");
        } 
        i <<= (3 - arrayOfString2.length) * 4;
        for (j = arrayOfString2.length, m = k; m < j; ) {
          String str = arrayOfString2[m];
          k = Integer.parseInt(str);
          if (k <= 15) {
            i = (i << 4) + k;
            m++;
          } 
          throw new UnsupportedOperationException("ReasonCode exceeds 15");
        } 
        return i;
      } 
      throw new UnsupportedOperationException("Only three nested layer is supported.");
    } 
    throw new IllegalArgumentException("SubjectCode/ReasonCode is empty");
  }
  
  public void onCreate() {
    super.onCreate();
    ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(4, 4, 30L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(), (ThreadFactory)new Object(this));
    threadPoolExecutor.allowCoreThreadTimeOut(true);
  }
  
  public void onDestroy() {
    this.mExecutor.shutdownNow();
    super.onDestroy();
  }
  
  public IBinder onBind(Intent paramIntent) {
    return this.mStubWrapper;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Result implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ResolvableError implements Annotation {}
  
  class OtaStatusChangedCallback {
    public abstract void onOtaStatusChanged(int param1Int);
  }
  
  public DownloadSubscriptionResult onDownloadSubscription(int paramInt, DownloadableSubscription paramDownloadableSubscription, boolean paramBoolean1, boolean paramBoolean2, Bundle paramBundle) {
    return null;
  }
  
  @Deprecated
  public int onDownloadSubscription(int paramInt, DownloadableSubscription paramDownloadableSubscription, boolean paramBoolean1, boolean paramBoolean2) {
    return Integer.MIN_VALUE;
  }
  
  public int onEraseSubscriptions(int paramInt1, int paramInt2) {
    throw new UnsupportedOperationException("This method must be overridden to enable the ResetOption parameter");
  }
  
  public void dump(PrintWriter paramPrintWriter) {
    paramPrintWriter.println("The connected LPA does not implement EuiccService#dump()");
  }
  
  public abstract int onDeleteSubscription(int paramInt, String paramString);
  
  @Deprecated
  public abstract int onEraseSubscriptions(int paramInt);
  
  public abstract GetDefaultDownloadableSubscriptionListResult onGetDefaultDownloadableSubscriptionList(int paramInt, boolean paramBoolean);
  
  public abstract GetDownloadableSubscriptionMetadataResult onGetDownloadableSubscriptionMetadata(int paramInt, DownloadableSubscription paramDownloadableSubscription, boolean paramBoolean);
  
  public abstract String onGetEid(int paramInt);
  
  public abstract EuiccInfo onGetEuiccInfo(int paramInt);
  
  public abstract GetEuiccProfileInfoListResult onGetEuiccProfileInfoList(int paramInt);
  
  public abstract int onGetOtaStatus(int paramInt);
  
  public abstract int onRetainSubscriptionsForFactoryReset(int paramInt);
  
  public abstract void onStartOtaIfNecessary(int paramInt, OtaStatusChangedCallback paramOtaStatusChangedCallback);
  
  public abstract int onSwitchToSubscription(int paramInt, String paramString, boolean paramBoolean);
  
  public abstract int onUpdateSubscriptionNickname(int paramInt, String paramString1, String paramString2);
  
  private class IEuiccServiceWrapper extends IEuiccService.Stub {
    final EuiccService this$0;
    
    private IEuiccServiceWrapper() {}
    
    public void downloadSubscription(int param1Int, DownloadableSubscription param1DownloadableSubscription, boolean param1Boolean1, boolean param1Boolean2, Bundle param1Bundle, IDownloadSubscriptionCallback param1IDownloadSubscriptionCallback) {
      EuiccService.this.mExecutor.execute((Runnable)new Object(this, param1Int, param1DownloadableSubscription, param1Boolean1, param1Boolean2, param1Bundle, param1IDownloadSubscriptionCallback));
    }
    
    public void getEid(int param1Int, IGetEidCallback param1IGetEidCallback) {
      EuiccService.this.mExecutor.execute((Runnable)new Object(this, param1Int, param1IGetEidCallback));
    }
    
    public void startOtaIfNecessary(int param1Int, IOtaStatusChangedCallback param1IOtaStatusChangedCallback) {
      EuiccService.this.mExecutor.execute((Runnable)new Object(this, param1Int, param1IOtaStatusChangedCallback));
    }
    
    public void getOtaStatus(int param1Int, IGetOtaStatusCallback param1IGetOtaStatusCallback) {
      EuiccService.this.mExecutor.execute((Runnable)new Object(this, param1Int, param1IGetOtaStatusCallback));
    }
    
    public void getDownloadableSubscriptionMetadata(int param1Int, DownloadableSubscription param1DownloadableSubscription, boolean param1Boolean, IGetDownloadableSubscriptionMetadataCallback param1IGetDownloadableSubscriptionMetadataCallback) {
      EuiccService.this.mExecutor.execute((Runnable)new Object(this, param1Int, param1DownloadableSubscription, param1Boolean, param1IGetDownloadableSubscriptionMetadataCallback));
    }
    
    public void getDefaultDownloadableSubscriptionList(int param1Int, boolean param1Boolean, IGetDefaultDownloadableSubscriptionListCallback param1IGetDefaultDownloadableSubscriptionListCallback) {
      EuiccService.this.mExecutor.execute((Runnable)new Object(this, param1Int, param1Boolean, param1IGetDefaultDownloadableSubscriptionListCallback));
    }
    
    public void getEuiccProfileInfoList(int param1Int, IGetEuiccProfileInfoListCallback param1IGetEuiccProfileInfoListCallback) {
      EuiccService.this.mExecutor.execute((Runnable)new Object(this, param1Int, param1IGetEuiccProfileInfoListCallback));
    }
    
    public void getEuiccInfo(int param1Int, IGetEuiccInfoCallback param1IGetEuiccInfoCallback) {
      EuiccService.this.mExecutor.execute((Runnable)new Object(this, param1Int, param1IGetEuiccInfoCallback));
    }
    
    public void deleteSubscription(int param1Int, String param1String, IDeleteSubscriptionCallback param1IDeleteSubscriptionCallback) {
      EuiccService.this.mExecutor.execute((Runnable)new Object(this, param1Int, param1String, param1IDeleteSubscriptionCallback));
    }
    
    public void switchToSubscription(int param1Int, String param1String, boolean param1Boolean, ISwitchToSubscriptionCallback param1ISwitchToSubscriptionCallback) {
      EuiccService.this.mExecutor.execute((Runnable)new Object(this, param1Int, param1String, param1Boolean, param1ISwitchToSubscriptionCallback));
    }
    
    public void updateSubscriptionNickname(int param1Int, String param1String1, String param1String2, IUpdateSubscriptionNicknameCallback param1IUpdateSubscriptionNicknameCallback) {
      EuiccService.this.mExecutor.execute((Runnable)new Object(this, param1Int, param1String1, param1String2, param1IUpdateSubscriptionNicknameCallback));
    }
    
    public void eraseSubscriptions(int param1Int, IEraseSubscriptionsCallback param1IEraseSubscriptionsCallback) {
      EuiccService.this.mExecutor.execute((Runnable)new Object(this, param1Int, param1IEraseSubscriptionsCallback));
    }
    
    public void eraseSubscriptionsWithOptions(int param1Int1, int param1Int2, IEraseSubscriptionsCallback param1IEraseSubscriptionsCallback) {
      EuiccService.this.mExecutor.execute((Runnable)new Object(this, param1Int1, param1Int2, param1IEraseSubscriptionsCallback));
    }
    
    public void retainSubscriptionsForFactoryReset(int param1Int, IRetainSubscriptionsForFactoryResetCallback param1IRetainSubscriptionsForFactoryResetCallback) {
      EuiccService.this.mExecutor.execute((Runnable)new Object(this, param1Int, param1IRetainSubscriptionsForFactoryResetCallback));
    }
    
    public void dump(IEuiccServiceDumpResultCallback param1IEuiccServiceDumpResultCallback) throws RemoteException {
      EuiccService.this.mExecutor.execute((Runnable)new Object(this, param1IEuiccServiceDumpResultCallback));
    }
  }
}
