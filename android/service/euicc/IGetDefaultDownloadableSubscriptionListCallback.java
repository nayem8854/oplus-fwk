package android.service.euicc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IGetDefaultDownloadableSubscriptionListCallback extends IInterface {
  void onComplete(GetDefaultDownloadableSubscriptionListResult paramGetDefaultDownloadableSubscriptionListResult) throws RemoteException;
  
  class Default implements IGetDefaultDownloadableSubscriptionListCallback {
    public void onComplete(GetDefaultDownloadableSubscriptionListResult param1GetDefaultDownloadableSubscriptionListResult) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IGetDefaultDownloadableSubscriptionListCallback {
    private static final String DESCRIPTOR = "android.service.euicc.IGetDefaultDownloadableSubscriptionListCallback";
    
    static final int TRANSACTION_onComplete = 1;
    
    public Stub() {
      attachInterface(this, "android.service.euicc.IGetDefaultDownloadableSubscriptionListCallback");
    }
    
    public static IGetDefaultDownloadableSubscriptionListCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.euicc.IGetDefaultDownloadableSubscriptionListCallback");
      if (iInterface != null && iInterface instanceof IGetDefaultDownloadableSubscriptionListCallback)
        return (IGetDefaultDownloadableSubscriptionListCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onComplete";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.service.euicc.IGetDefaultDownloadableSubscriptionListCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.euicc.IGetDefaultDownloadableSubscriptionListCallback");
      if (param1Parcel1.readInt() != 0) {
        GetDefaultDownloadableSubscriptionListResult getDefaultDownloadableSubscriptionListResult = GetDefaultDownloadableSubscriptionListResult.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onComplete((GetDefaultDownloadableSubscriptionListResult)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IGetDefaultDownloadableSubscriptionListCallback {
      public static IGetDefaultDownloadableSubscriptionListCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.euicc.IGetDefaultDownloadableSubscriptionListCallback";
      }
      
      public void onComplete(GetDefaultDownloadableSubscriptionListResult param2GetDefaultDownloadableSubscriptionListResult) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.euicc.IGetDefaultDownloadableSubscriptionListCallback");
          if (param2GetDefaultDownloadableSubscriptionListResult != null) {
            parcel.writeInt(1);
            param2GetDefaultDownloadableSubscriptionListResult.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IGetDefaultDownloadableSubscriptionListCallback.Stub.getDefaultImpl() != null) {
            IGetDefaultDownloadableSubscriptionListCallback.Stub.getDefaultImpl().onComplete(param2GetDefaultDownloadableSubscriptionListResult);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IGetDefaultDownloadableSubscriptionListCallback param1IGetDefaultDownloadableSubscriptionListCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IGetDefaultDownloadableSubscriptionListCallback != null) {
          Proxy.sDefaultImpl = param1IGetDefaultDownloadableSubscriptionListCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IGetDefaultDownloadableSubscriptionListCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
