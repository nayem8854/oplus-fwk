package android.service.euicc;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.euicc.DownloadableSubscription;
import java.util.Arrays;
import java.util.List;

@SystemApi
public final class GetDefaultDownloadableSubscriptionListResult implements Parcelable {
  public static final Parcelable.Creator<GetDefaultDownloadableSubscriptionListResult> CREATOR = new Parcelable.Creator<GetDefaultDownloadableSubscriptionListResult>() {
      public GetDefaultDownloadableSubscriptionListResult createFromParcel(Parcel param1Parcel) {
        return new GetDefaultDownloadableSubscriptionListResult(param1Parcel);
      }
      
      public GetDefaultDownloadableSubscriptionListResult[] newArray(int param1Int) {
        return new GetDefaultDownloadableSubscriptionListResult[param1Int];
      }
    };
  
  private final DownloadableSubscription[] mSubscriptions;
  
  @Deprecated
  public final int result;
  
  public int getResult() {
    return this.result;
  }
  
  public List<DownloadableSubscription> getDownloadableSubscriptions() {
    DownloadableSubscription[] arrayOfDownloadableSubscription = this.mSubscriptions;
    if (arrayOfDownloadableSubscription == null)
      return null; 
    return Arrays.asList(arrayOfDownloadableSubscription);
  }
  
  public GetDefaultDownloadableSubscriptionListResult(int paramInt, DownloadableSubscription[] paramArrayOfDownloadableSubscription) {
    this.result = paramInt;
    if (paramInt == 0) {
      this.mSubscriptions = paramArrayOfDownloadableSubscription;
    } else {
      if (paramArrayOfDownloadableSubscription == null) {
        this.mSubscriptions = null;
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error result with non-null subscriptions: ");
      stringBuilder.append(paramInt);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
  }
  
  private GetDefaultDownloadableSubscriptionListResult(Parcel paramParcel) {
    this.result = paramParcel.readInt();
    this.mSubscriptions = paramParcel.<DownloadableSubscription>createTypedArray(DownloadableSubscription.CREATOR);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.result);
    paramParcel.writeTypedArray(this.mSubscriptions, paramInt);
  }
  
  public int describeContents() {
    return 0;
  }
}
