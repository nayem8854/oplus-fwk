package android.service.euicc;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.euicc.DownloadableSubscription;

public interface IEuiccService extends IInterface {
  void deleteSubscription(int paramInt, String paramString, IDeleteSubscriptionCallback paramIDeleteSubscriptionCallback) throws RemoteException;
  
  void downloadSubscription(int paramInt, DownloadableSubscription paramDownloadableSubscription, boolean paramBoolean1, boolean paramBoolean2, Bundle paramBundle, IDownloadSubscriptionCallback paramIDownloadSubscriptionCallback) throws RemoteException;
  
  void dump(IEuiccServiceDumpResultCallback paramIEuiccServiceDumpResultCallback) throws RemoteException;
  
  void eraseSubscriptions(int paramInt, IEraseSubscriptionsCallback paramIEraseSubscriptionsCallback) throws RemoteException;
  
  void eraseSubscriptionsWithOptions(int paramInt1, int paramInt2, IEraseSubscriptionsCallback paramIEraseSubscriptionsCallback) throws RemoteException;
  
  void getDefaultDownloadableSubscriptionList(int paramInt, boolean paramBoolean, IGetDefaultDownloadableSubscriptionListCallback paramIGetDefaultDownloadableSubscriptionListCallback) throws RemoteException;
  
  void getDownloadableSubscriptionMetadata(int paramInt, DownloadableSubscription paramDownloadableSubscription, boolean paramBoolean, IGetDownloadableSubscriptionMetadataCallback paramIGetDownloadableSubscriptionMetadataCallback) throws RemoteException;
  
  void getEid(int paramInt, IGetEidCallback paramIGetEidCallback) throws RemoteException;
  
  void getEuiccInfo(int paramInt, IGetEuiccInfoCallback paramIGetEuiccInfoCallback) throws RemoteException;
  
  void getEuiccProfileInfoList(int paramInt, IGetEuiccProfileInfoListCallback paramIGetEuiccProfileInfoListCallback) throws RemoteException;
  
  void getOtaStatus(int paramInt, IGetOtaStatusCallback paramIGetOtaStatusCallback) throws RemoteException;
  
  void retainSubscriptionsForFactoryReset(int paramInt, IRetainSubscriptionsForFactoryResetCallback paramIRetainSubscriptionsForFactoryResetCallback) throws RemoteException;
  
  void startOtaIfNecessary(int paramInt, IOtaStatusChangedCallback paramIOtaStatusChangedCallback) throws RemoteException;
  
  void switchToSubscription(int paramInt, String paramString, boolean paramBoolean, ISwitchToSubscriptionCallback paramISwitchToSubscriptionCallback) throws RemoteException;
  
  void updateSubscriptionNickname(int paramInt, String paramString1, String paramString2, IUpdateSubscriptionNicknameCallback paramIUpdateSubscriptionNicknameCallback) throws RemoteException;
  
  class Default implements IEuiccService {
    public void downloadSubscription(int param1Int, DownloadableSubscription param1DownloadableSubscription, boolean param1Boolean1, boolean param1Boolean2, Bundle param1Bundle, IDownloadSubscriptionCallback param1IDownloadSubscriptionCallback) throws RemoteException {}
    
    public void getDownloadableSubscriptionMetadata(int param1Int, DownloadableSubscription param1DownloadableSubscription, boolean param1Boolean, IGetDownloadableSubscriptionMetadataCallback param1IGetDownloadableSubscriptionMetadataCallback) throws RemoteException {}
    
    public void getEid(int param1Int, IGetEidCallback param1IGetEidCallback) throws RemoteException {}
    
    public void getOtaStatus(int param1Int, IGetOtaStatusCallback param1IGetOtaStatusCallback) throws RemoteException {}
    
    public void startOtaIfNecessary(int param1Int, IOtaStatusChangedCallback param1IOtaStatusChangedCallback) throws RemoteException {}
    
    public void getEuiccProfileInfoList(int param1Int, IGetEuiccProfileInfoListCallback param1IGetEuiccProfileInfoListCallback) throws RemoteException {}
    
    public void getDefaultDownloadableSubscriptionList(int param1Int, boolean param1Boolean, IGetDefaultDownloadableSubscriptionListCallback param1IGetDefaultDownloadableSubscriptionListCallback) throws RemoteException {}
    
    public void getEuiccInfo(int param1Int, IGetEuiccInfoCallback param1IGetEuiccInfoCallback) throws RemoteException {}
    
    public void deleteSubscription(int param1Int, String param1String, IDeleteSubscriptionCallback param1IDeleteSubscriptionCallback) throws RemoteException {}
    
    public void switchToSubscription(int param1Int, String param1String, boolean param1Boolean, ISwitchToSubscriptionCallback param1ISwitchToSubscriptionCallback) throws RemoteException {}
    
    public void updateSubscriptionNickname(int param1Int, String param1String1, String param1String2, IUpdateSubscriptionNicknameCallback param1IUpdateSubscriptionNicknameCallback) throws RemoteException {}
    
    public void eraseSubscriptions(int param1Int, IEraseSubscriptionsCallback param1IEraseSubscriptionsCallback) throws RemoteException {}
    
    public void eraseSubscriptionsWithOptions(int param1Int1, int param1Int2, IEraseSubscriptionsCallback param1IEraseSubscriptionsCallback) throws RemoteException {}
    
    public void retainSubscriptionsForFactoryReset(int param1Int, IRetainSubscriptionsForFactoryResetCallback param1IRetainSubscriptionsForFactoryResetCallback) throws RemoteException {}
    
    public void dump(IEuiccServiceDumpResultCallback param1IEuiccServiceDumpResultCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IEuiccService {
    private static final String DESCRIPTOR = "android.service.euicc.IEuiccService";
    
    static final int TRANSACTION_deleteSubscription = 9;
    
    static final int TRANSACTION_downloadSubscription = 1;
    
    static final int TRANSACTION_dump = 15;
    
    static final int TRANSACTION_eraseSubscriptions = 12;
    
    static final int TRANSACTION_eraseSubscriptionsWithOptions = 13;
    
    static final int TRANSACTION_getDefaultDownloadableSubscriptionList = 7;
    
    static final int TRANSACTION_getDownloadableSubscriptionMetadata = 2;
    
    static final int TRANSACTION_getEid = 3;
    
    static final int TRANSACTION_getEuiccInfo = 8;
    
    static final int TRANSACTION_getEuiccProfileInfoList = 6;
    
    static final int TRANSACTION_getOtaStatus = 4;
    
    static final int TRANSACTION_retainSubscriptionsForFactoryReset = 14;
    
    static final int TRANSACTION_startOtaIfNecessary = 5;
    
    static final int TRANSACTION_switchToSubscription = 10;
    
    static final int TRANSACTION_updateSubscriptionNickname = 11;
    
    public Stub() {
      attachInterface(this, "android.service.euicc.IEuiccService");
    }
    
    public static IEuiccService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.euicc.IEuiccService");
      if (iInterface != null && iInterface instanceof IEuiccService)
        return (IEuiccService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 15:
          return "dump";
        case 14:
          return "retainSubscriptionsForFactoryReset";
        case 13:
          return "eraseSubscriptionsWithOptions";
        case 12:
          return "eraseSubscriptions";
        case 11:
          return "updateSubscriptionNickname";
        case 10:
          return "switchToSubscription";
        case 9:
          return "deleteSubscription";
        case 8:
          return "getEuiccInfo";
        case 7:
          return "getDefaultDownloadableSubscriptionList";
        case 6:
          return "getEuiccProfileInfoList";
        case 5:
          return "startOtaIfNecessary";
        case 4:
          return "getOtaStatus";
        case 3:
          return "getEid";
        case 2:
          return "getDownloadableSubscriptionMetadata";
        case 1:
          break;
      } 
      return "downloadSubscription";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        IEuiccServiceDumpResultCallback iEuiccServiceDumpResultCallback;
        IRetainSubscriptionsForFactoryResetCallback iRetainSubscriptionsForFactoryResetCallback;
        IEraseSubscriptionsCallback iEraseSubscriptionsCallback;
        IUpdateSubscriptionNicknameCallback iUpdateSubscriptionNicknameCallback;
        ISwitchToSubscriptionCallback iSwitchToSubscriptionCallback;
        IDeleteSubscriptionCallback iDeleteSubscriptionCallback;
        IGetEuiccInfoCallback iGetEuiccInfoCallback;
        IGetDefaultDownloadableSubscriptionListCallback iGetDefaultDownloadableSubscriptionListCallback;
        IGetEuiccProfileInfoListCallback iGetEuiccProfileInfoListCallback;
        IOtaStatusChangedCallback iOtaStatusChangedCallback;
        IGetOtaStatusCallback iGetOtaStatusCallback;
        IGetEidCallback iGetEidCallback;
        IGetDownloadableSubscriptionMetadataCallback iGetDownloadableSubscriptionMetadataCallback;
        String str1;
        boolean bool1 = false, bool2 = false, bool3 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 15:
            param1Parcel1.enforceInterface("android.service.euicc.IEuiccService");
            iEuiccServiceDumpResultCallback = IEuiccServiceDumpResultCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            dump(iEuiccServiceDumpResultCallback);
            return true;
          case 14:
            iEuiccServiceDumpResultCallback.enforceInterface("android.service.euicc.IEuiccService");
            param1Int1 = iEuiccServiceDumpResultCallback.readInt();
            iRetainSubscriptionsForFactoryResetCallback = IRetainSubscriptionsForFactoryResetCallback.Stub.asInterface(iEuiccServiceDumpResultCallback.readStrongBinder());
            retainSubscriptionsForFactoryReset(param1Int1, iRetainSubscriptionsForFactoryResetCallback);
            return true;
          case 13:
            iRetainSubscriptionsForFactoryResetCallback.enforceInterface("android.service.euicc.IEuiccService");
            param1Int2 = iRetainSubscriptionsForFactoryResetCallback.readInt();
            param1Int1 = iRetainSubscriptionsForFactoryResetCallback.readInt();
            iEraseSubscriptionsCallback = IEraseSubscriptionsCallback.Stub.asInterface(iRetainSubscriptionsForFactoryResetCallback.readStrongBinder());
            eraseSubscriptionsWithOptions(param1Int2, param1Int1, iEraseSubscriptionsCallback);
            return true;
          case 12:
            iEraseSubscriptionsCallback.enforceInterface("android.service.euicc.IEuiccService");
            param1Int1 = iEraseSubscriptionsCallback.readInt();
            iEraseSubscriptionsCallback = IEraseSubscriptionsCallback.Stub.asInterface(iEraseSubscriptionsCallback.readStrongBinder());
            eraseSubscriptions(param1Int1, iEraseSubscriptionsCallback);
            return true;
          case 11:
            iEraseSubscriptionsCallback.enforceInterface("android.service.euicc.IEuiccService");
            param1Int1 = iEraseSubscriptionsCallback.readInt();
            str1 = iEraseSubscriptionsCallback.readString();
            str = iEraseSubscriptionsCallback.readString();
            iUpdateSubscriptionNicknameCallback = IUpdateSubscriptionNicknameCallback.Stub.asInterface(iEraseSubscriptionsCallback.readStrongBinder());
            updateSubscriptionNickname(param1Int1, str1, str, iUpdateSubscriptionNicknameCallback);
            return true;
          case 10:
            iUpdateSubscriptionNicknameCallback.enforceInterface("android.service.euicc.IEuiccService");
            param1Int1 = iUpdateSubscriptionNicknameCallback.readInt();
            str = iUpdateSubscriptionNicknameCallback.readString();
            bool1 = bool3;
            if (iUpdateSubscriptionNicknameCallback.readInt() != 0)
              bool1 = true; 
            iSwitchToSubscriptionCallback = ISwitchToSubscriptionCallback.Stub.asInterface(iUpdateSubscriptionNicknameCallback.readStrongBinder());
            switchToSubscription(param1Int1, str, bool1, iSwitchToSubscriptionCallback);
            return true;
          case 9:
            iSwitchToSubscriptionCallback.enforceInterface("android.service.euicc.IEuiccService");
            param1Int1 = iSwitchToSubscriptionCallback.readInt();
            str = iSwitchToSubscriptionCallback.readString();
            iDeleteSubscriptionCallback = IDeleteSubscriptionCallback.Stub.asInterface(iSwitchToSubscriptionCallback.readStrongBinder());
            deleteSubscription(param1Int1, str, iDeleteSubscriptionCallback);
            return true;
          case 8:
            iDeleteSubscriptionCallback.enforceInterface("android.service.euicc.IEuiccService");
            param1Int1 = iDeleteSubscriptionCallback.readInt();
            iGetEuiccInfoCallback = IGetEuiccInfoCallback.Stub.asInterface(iDeleteSubscriptionCallback.readStrongBinder());
            getEuiccInfo(param1Int1, iGetEuiccInfoCallback);
            return true;
          case 7:
            iGetEuiccInfoCallback.enforceInterface("android.service.euicc.IEuiccService");
            param1Int1 = iGetEuiccInfoCallback.readInt();
            if (iGetEuiccInfoCallback.readInt() != 0)
              bool1 = true; 
            iGetDefaultDownloadableSubscriptionListCallback = IGetDefaultDownloadableSubscriptionListCallback.Stub.asInterface(iGetEuiccInfoCallback.readStrongBinder());
            getDefaultDownloadableSubscriptionList(param1Int1, bool1, iGetDefaultDownloadableSubscriptionListCallback);
            return true;
          case 6:
            iGetDefaultDownloadableSubscriptionListCallback.enforceInterface("android.service.euicc.IEuiccService");
            param1Int1 = iGetDefaultDownloadableSubscriptionListCallback.readInt();
            iGetEuiccProfileInfoListCallback = IGetEuiccProfileInfoListCallback.Stub.asInterface(iGetDefaultDownloadableSubscriptionListCallback.readStrongBinder());
            getEuiccProfileInfoList(param1Int1, iGetEuiccProfileInfoListCallback);
            return true;
          case 5:
            iGetEuiccProfileInfoListCallback.enforceInterface("android.service.euicc.IEuiccService");
            param1Int1 = iGetEuiccProfileInfoListCallback.readInt();
            iOtaStatusChangedCallback = IOtaStatusChangedCallback.Stub.asInterface(iGetEuiccProfileInfoListCallback.readStrongBinder());
            startOtaIfNecessary(param1Int1, iOtaStatusChangedCallback);
            return true;
          case 4:
            iOtaStatusChangedCallback.enforceInterface("android.service.euicc.IEuiccService");
            param1Int1 = iOtaStatusChangedCallback.readInt();
            iGetOtaStatusCallback = IGetOtaStatusCallback.Stub.asInterface(iOtaStatusChangedCallback.readStrongBinder());
            getOtaStatus(param1Int1, iGetOtaStatusCallback);
            return true;
          case 3:
            iGetOtaStatusCallback.enforceInterface("android.service.euicc.IEuiccService");
            param1Int1 = iGetOtaStatusCallback.readInt();
            iGetEidCallback = IGetEidCallback.Stub.asInterface(iGetOtaStatusCallback.readStrongBinder());
            getEid(param1Int1, iGetEidCallback);
            return true;
          case 2:
            iGetEidCallback.enforceInterface("android.service.euicc.IEuiccService");
            param1Int1 = iGetEidCallback.readInt();
            if (iGetEidCallback.readInt() != 0) {
              DownloadableSubscription downloadableSubscription = DownloadableSubscription.CREATOR.createFromParcel((Parcel)iGetEidCallback);
            } else {
              str = null;
            } 
            bool1 = bool2;
            if (iGetEidCallback.readInt() != 0)
              bool1 = true; 
            iGetDownloadableSubscriptionMetadataCallback = IGetDownloadableSubscriptionMetadataCallback.Stub.asInterface(iGetEidCallback.readStrongBinder());
            getDownloadableSubscriptionMetadata(param1Int1, (DownloadableSubscription)str, bool1, iGetDownloadableSubscriptionMetadataCallback);
            return true;
          case 1:
            break;
        } 
        iGetDownloadableSubscriptionMetadataCallback.enforceInterface("android.service.euicc.IEuiccService");
        param1Int1 = iGetDownloadableSubscriptionMetadataCallback.readInt();
        if (iGetDownloadableSubscriptionMetadataCallback.readInt() != 0) {
          DownloadableSubscription downloadableSubscription = DownloadableSubscription.CREATOR.createFromParcel((Parcel)iGetDownloadableSubscriptionMetadataCallback);
        } else {
          str = null;
        } 
        if (iGetDownloadableSubscriptionMetadataCallback.readInt() != 0) {
          bool1 = true;
        } else {
          bool1 = false;
        } 
        if (iGetDownloadableSubscriptionMetadataCallback.readInt() != 0) {
          bool3 = true;
        } else {
          bool3 = false;
        } 
        if (iGetDownloadableSubscriptionMetadataCallback.readInt() != 0) {
          Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)iGetDownloadableSubscriptionMetadataCallback);
        } else {
          str1 = null;
        } 
        IDownloadSubscriptionCallback iDownloadSubscriptionCallback = IDownloadSubscriptionCallback.Stub.asInterface(iGetDownloadableSubscriptionMetadataCallback.readStrongBinder());
        downloadSubscription(param1Int1, (DownloadableSubscription)str, bool1, bool3, (Bundle)str1, iDownloadSubscriptionCallback);
        return true;
      } 
      str.writeString("android.service.euicc.IEuiccService");
      return true;
    }
    
    private static class Proxy implements IEuiccService {
      public static IEuiccService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.euicc.IEuiccService";
      }
      
      public void downloadSubscription(int param2Int, DownloadableSubscription param2DownloadableSubscription, boolean param2Boolean1, boolean param2Boolean2, Bundle param2Bundle, IDownloadSubscriptionCallback param2IDownloadSubscriptionCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.euicc.IEuiccService");
          try {
            boolean bool;
            IBinder iBinder;
            parcel.writeInt(param2Int);
            if (param2DownloadableSubscription != null) {
              parcel.writeInt(1);
              param2DownloadableSubscription.writeToParcel(parcel, 0);
            } else {
              parcel.writeInt(0);
            } 
            if (param2Boolean1) {
              bool = true;
            } else {
              bool = false;
            } 
            parcel.writeInt(bool);
            if (param2Boolean2) {
              bool = true;
            } else {
              bool = false;
            } 
            parcel.writeInt(bool);
            if (param2Bundle != null) {
              parcel.writeInt(1);
              param2Bundle.writeToParcel(parcel, 0);
            } else {
              parcel.writeInt(0);
            } 
            if (param2IDownloadSubscriptionCallback != null) {
              iBinder = param2IDownloadSubscriptionCallback.asBinder();
            } else {
              iBinder = null;
            } 
            parcel.writeStrongBinder(iBinder);
            try {
              boolean bool1 = this.mRemote.transact(1, parcel, (Parcel)null, 1);
              if (!bool1 && IEuiccService.Stub.getDefaultImpl() != null) {
                IEuiccService.Stub.getDefaultImpl().downloadSubscription(param2Int, param2DownloadableSubscription, param2Boolean1, param2Boolean2, param2Bundle, param2IDownloadSubscriptionCallback);
                parcel.recycle();
                return;
              } 
              parcel.recycle();
              return;
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2DownloadableSubscription;
      }
      
      public void getDownloadableSubscriptionMetadata(int param2Int, DownloadableSubscription param2DownloadableSubscription, boolean param2Boolean, IGetDownloadableSubscriptionMetadataCallback param2IGetDownloadableSubscriptionMetadataCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.euicc.IEuiccService");
          parcel.writeInt(param2Int);
          boolean bool = false;
          if (param2DownloadableSubscription != null) {
            parcel.writeInt(1);
            param2DownloadableSubscription.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          if (param2IGetDownloadableSubscriptionMetadataCallback != null) {
            iBinder = param2IGetDownloadableSubscriptionMetadataCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool1 = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool1 && IEuiccService.Stub.getDefaultImpl() != null) {
            IEuiccService.Stub.getDefaultImpl().getDownloadableSubscriptionMetadata(param2Int, param2DownloadableSubscription, param2Boolean, param2IGetDownloadableSubscriptionMetadataCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getEid(int param2Int, IGetEidCallback param2IGetEidCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.euicc.IEuiccService");
          parcel.writeInt(param2Int);
          if (param2IGetEidCallback != null) {
            iBinder = param2IGetEidCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IEuiccService.Stub.getDefaultImpl() != null) {
            IEuiccService.Stub.getDefaultImpl().getEid(param2Int, param2IGetEidCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getOtaStatus(int param2Int, IGetOtaStatusCallback param2IGetOtaStatusCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.euicc.IEuiccService");
          parcel.writeInt(param2Int);
          if (param2IGetOtaStatusCallback != null) {
            iBinder = param2IGetOtaStatusCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IEuiccService.Stub.getDefaultImpl() != null) {
            IEuiccService.Stub.getDefaultImpl().getOtaStatus(param2Int, param2IGetOtaStatusCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void startOtaIfNecessary(int param2Int, IOtaStatusChangedCallback param2IOtaStatusChangedCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.euicc.IEuiccService");
          parcel.writeInt(param2Int);
          if (param2IOtaStatusChangedCallback != null) {
            iBinder = param2IOtaStatusChangedCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IEuiccService.Stub.getDefaultImpl() != null) {
            IEuiccService.Stub.getDefaultImpl().startOtaIfNecessary(param2Int, param2IOtaStatusChangedCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getEuiccProfileInfoList(int param2Int, IGetEuiccProfileInfoListCallback param2IGetEuiccProfileInfoListCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.euicc.IEuiccService");
          parcel.writeInt(param2Int);
          if (param2IGetEuiccProfileInfoListCallback != null) {
            iBinder = param2IGetEuiccProfileInfoListCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IEuiccService.Stub.getDefaultImpl() != null) {
            IEuiccService.Stub.getDefaultImpl().getEuiccProfileInfoList(param2Int, param2IGetEuiccProfileInfoListCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getDefaultDownloadableSubscriptionList(int param2Int, boolean param2Boolean, IGetDefaultDownloadableSubscriptionListCallback param2IGetDefaultDownloadableSubscriptionListCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.euicc.IEuiccService");
          parcel.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2IGetDefaultDownloadableSubscriptionListCallback != null) {
            iBinder = param2IGetDefaultDownloadableSubscriptionListCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool1 = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool1 && IEuiccService.Stub.getDefaultImpl() != null) {
            IEuiccService.Stub.getDefaultImpl().getDefaultDownloadableSubscriptionList(param2Int, param2Boolean, param2IGetDefaultDownloadableSubscriptionListCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getEuiccInfo(int param2Int, IGetEuiccInfoCallback param2IGetEuiccInfoCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.euicc.IEuiccService");
          parcel.writeInt(param2Int);
          if (param2IGetEuiccInfoCallback != null) {
            iBinder = param2IGetEuiccInfoCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && IEuiccService.Stub.getDefaultImpl() != null) {
            IEuiccService.Stub.getDefaultImpl().getEuiccInfo(param2Int, param2IGetEuiccInfoCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void deleteSubscription(int param2Int, String param2String, IDeleteSubscriptionCallback param2IDeleteSubscriptionCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.euicc.IEuiccService");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          if (param2IDeleteSubscriptionCallback != null) {
            iBinder = param2IDeleteSubscriptionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(9, parcel, (Parcel)null, 1);
          if (!bool && IEuiccService.Stub.getDefaultImpl() != null) {
            IEuiccService.Stub.getDefaultImpl().deleteSubscription(param2Int, param2String, param2IDeleteSubscriptionCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void switchToSubscription(int param2Int, String param2String, boolean param2Boolean, ISwitchToSubscriptionCallback param2ISwitchToSubscriptionCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.euicc.IEuiccService");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2ISwitchToSubscriptionCallback != null) {
            iBinder = param2ISwitchToSubscriptionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool1 = this.mRemote.transact(10, parcel, (Parcel)null, 1);
          if (!bool1 && IEuiccService.Stub.getDefaultImpl() != null) {
            IEuiccService.Stub.getDefaultImpl().switchToSubscription(param2Int, param2String, param2Boolean, param2ISwitchToSubscriptionCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void updateSubscriptionNickname(int param2Int, String param2String1, String param2String2, IUpdateSubscriptionNicknameCallback param2IUpdateSubscriptionNicknameCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.euicc.IEuiccService");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2IUpdateSubscriptionNicknameCallback != null) {
            iBinder = param2IUpdateSubscriptionNicknameCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(11, parcel, (Parcel)null, 1);
          if (!bool && IEuiccService.Stub.getDefaultImpl() != null) {
            IEuiccService.Stub.getDefaultImpl().updateSubscriptionNickname(param2Int, param2String1, param2String2, param2IUpdateSubscriptionNicknameCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void eraseSubscriptions(int param2Int, IEraseSubscriptionsCallback param2IEraseSubscriptionsCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.euicc.IEuiccService");
          parcel.writeInt(param2Int);
          if (param2IEraseSubscriptionsCallback != null) {
            iBinder = param2IEraseSubscriptionsCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(12, parcel, (Parcel)null, 1);
          if (!bool && IEuiccService.Stub.getDefaultImpl() != null) {
            IEuiccService.Stub.getDefaultImpl().eraseSubscriptions(param2Int, param2IEraseSubscriptionsCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void eraseSubscriptionsWithOptions(int param2Int1, int param2Int2, IEraseSubscriptionsCallback param2IEraseSubscriptionsCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.euicc.IEuiccService");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2IEraseSubscriptionsCallback != null) {
            iBinder = param2IEraseSubscriptionsCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(13, parcel, (Parcel)null, 1);
          if (!bool && IEuiccService.Stub.getDefaultImpl() != null) {
            IEuiccService.Stub.getDefaultImpl().eraseSubscriptionsWithOptions(param2Int1, param2Int2, param2IEraseSubscriptionsCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void retainSubscriptionsForFactoryReset(int param2Int, IRetainSubscriptionsForFactoryResetCallback param2IRetainSubscriptionsForFactoryResetCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.euicc.IEuiccService");
          parcel.writeInt(param2Int);
          if (param2IRetainSubscriptionsForFactoryResetCallback != null) {
            iBinder = param2IRetainSubscriptionsForFactoryResetCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(14, parcel, (Parcel)null, 1);
          if (!bool && IEuiccService.Stub.getDefaultImpl() != null) {
            IEuiccService.Stub.getDefaultImpl().retainSubscriptionsForFactoryReset(param2Int, param2IRetainSubscriptionsForFactoryResetCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dump(IEuiccServiceDumpResultCallback param2IEuiccServiceDumpResultCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.euicc.IEuiccService");
          if (param2IEuiccServiceDumpResultCallback != null) {
            iBinder = param2IEuiccServiceDumpResultCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(15, parcel, (Parcel)null, 1);
          if (!bool && IEuiccService.Stub.getDefaultImpl() != null) {
            IEuiccService.Stub.getDefaultImpl().dump(param2IEuiccServiceDumpResultCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IEuiccService param1IEuiccService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IEuiccService != null) {
          Proxy.sDefaultImpl = param1IEuiccService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IEuiccService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
