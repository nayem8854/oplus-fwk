package android.service.controls.templates;

import android.os.Bundle;
import com.android.internal.util.Preconditions;

public final class ToggleTemplate extends ControlTemplate {
  private static final String KEY_BUTTON = "key_button";
  
  private static final int TYPE = 1;
  
  private final ControlButton mButton;
  
  public ToggleTemplate(String paramString, ControlButton paramControlButton) {
    super(paramString);
    Preconditions.checkNotNull(paramControlButton);
    this.mButton = paramControlButton;
  }
  
  ToggleTemplate(Bundle paramBundle) {
    super(paramBundle);
    this.mButton = paramBundle.<ControlButton>getParcelable("key_button");
  }
  
  public boolean isChecked() {
    return this.mButton.isChecked();
  }
  
  public CharSequence getContentDescription() {
    return this.mButton.getActionDescription();
  }
  
  public int getTemplateType() {
    return 1;
  }
  
  Bundle getDataBundle() {
    Bundle bundle = super.getDataBundle();
    bundle.putParcelable("key_button", this.mButton);
    return bundle;
  }
}
