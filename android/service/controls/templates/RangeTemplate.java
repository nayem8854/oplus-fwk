package android.service.controls.templates;

import android.os.Bundle;

public final class RangeTemplate extends ControlTemplate {
  private static final String KEY_CURRENT_VALUE = "key_current_value";
  
  private static final String KEY_FORMAT_STRING = "key_format_string";
  
  private static final String KEY_MAX_VALUE = "key_max_value";
  
  private static final String KEY_MIN_VALUE = "key_min_value";
  
  private static final String KEY_STEP_VALUE = "key_step_value";
  
  private static final int TYPE = 2;
  
  private final float mCurrentValue;
  
  private final CharSequence mFormatString;
  
  private final float mMaxValue;
  
  private final float mMinValue;
  
  private final float mStepValue;
  
  public RangeTemplate(String paramString, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, CharSequence paramCharSequence) {
    super(paramString);
    this.mMinValue = paramFloat1;
    this.mMaxValue = paramFloat2;
    this.mCurrentValue = paramFloat3;
    this.mStepValue = paramFloat4;
    if (paramCharSequence != null) {
      this.mFormatString = paramCharSequence;
    } else {
      this.mFormatString = "%.1f";
    } 
    validate();
  }
  
  RangeTemplate(Bundle paramBundle) {
    super(paramBundle);
    this.mMinValue = paramBundle.getFloat("key_min_value");
    this.mMaxValue = paramBundle.getFloat("key_max_value");
    this.mCurrentValue = paramBundle.getFloat("key_current_value");
    this.mStepValue = paramBundle.getFloat("key_step_value");
    this.mFormatString = paramBundle.getCharSequence("key_format_string", "%.1f");
    validate();
  }
  
  public float getMinValue() {
    return this.mMinValue;
  }
  
  public float getMaxValue() {
    return this.mMaxValue;
  }
  
  public float getCurrentValue() {
    return this.mCurrentValue;
  }
  
  public float getStepValue() {
    return this.mStepValue;
  }
  
  public CharSequence getFormatString() {
    return this.mFormatString;
  }
  
  public int getTemplateType() {
    return 2;
  }
  
  Bundle getDataBundle() {
    Bundle bundle = super.getDataBundle();
    bundle.putFloat("key_min_value", this.mMinValue);
    bundle.putFloat("key_max_value", this.mMaxValue);
    bundle.putFloat("key_current_value", this.mCurrentValue);
    bundle.putFloat("key_step_value", this.mStepValue);
    bundle.putCharSequence("key_format_string", this.mFormatString);
    return bundle;
  }
  
  private void validate() {
    if (Float.compare(this.mMinValue, this.mMaxValue) <= 0) {
      if (Float.compare(this.mMinValue, this.mCurrentValue) <= 0) {
        if (Float.compare(this.mCurrentValue, this.mMaxValue) <= 0) {
          if (this.mStepValue > 0.0F)
            return; 
          throw new IllegalArgumentException(String.format("stepValue=%f <= 0", new Object[] { Float.valueOf(this.mStepValue) }));
        } 
        float f2 = this.mCurrentValue;
        throw new IllegalArgumentException(String.format("currentValue=%f > maxValue=%f", new Object[] { Float.valueOf(f2), Float.valueOf(this.mMaxValue) }));
      } 
      float f1 = this.mMinValue;
      throw new IllegalArgumentException(String.format("minValue=%f > currentValue=%f", new Object[] { Float.valueOf(f1), Float.valueOf(this.mCurrentValue) }));
    } 
    float f = this.mMinValue;
    throw new IllegalArgumentException(String.format("minValue=%f > maxValue=%f", new Object[] { Float.valueOf(f), Float.valueOf(this.mMaxValue) }));
  }
}
