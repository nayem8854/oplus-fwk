package android.service.controls.templates;

import android.os.Bundle;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract class ControlTemplate {
  private static final ControlTemplate ERROR_TEMPLATE;
  
  private static final String KEY_TEMPLATE_ID = "key_template_id";
  
  private static final String KEY_TEMPLATE_TYPE = "key_template_type";
  
  public static final ControlTemplate NO_TEMPLATE = (ControlTemplate)new Object("");
  
  private static final String TAG = "ControlTemplate";
  
  public static final int TYPE_ERROR = -1;
  
  public static final int TYPE_NO_TEMPLATE = 0;
  
  public static final int TYPE_RANGE = 2;
  
  public static final int TYPE_STATELESS = 8;
  
  public static final int TYPE_TEMPERATURE = 7;
  
  public static final int TYPE_TOGGLE = 1;
  
  public static final int TYPE_TOGGLE_RANGE = 6;
  
  private final String mTemplateId;
  
  static {
    ERROR_TEMPLATE = (ControlTemplate)new Object("");
  }
  
  public String getTemplateId() {
    return this.mTemplateId;
  }
  
  Bundle getDataBundle() {
    Bundle bundle = new Bundle();
    bundle.putInt("key_template_type", getTemplateType());
    bundle.putString("key_template_id", this.mTemplateId);
    return bundle;
  }
  
  private ControlTemplate() {
    this.mTemplateId = "";
  }
  
  ControlTemplate(Bundle paramBundle) {
    this.mTemplateId = paramBundle.getString("key_template_id");
  }
  
  ControlTemplate(String paramString) {
    Preconditions.checkNotNull(paramString);
    this.mTemplateId = paramString;
  }
  
  static ControlTemplate createTemplateFromBundle(Bundle paramBundle) {
    if (paramBundle == null) {
      Log.e("ControlTemplate", "Null bundle");
      return ERROR_TEMPLATE;
    } 
    int i = paramBundle.getInt("key_template_type", -1);
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          if (i != 6) {
            if (i != 7) {
              if (i != 8)
                try {
                  return ERROR_TEMPLATE;
                } catch (Exception exception) {
                  Log.e("ControlTemplate", "Error creating template", exception);
                  return ERROR_TEMPLATE;
                }  
              return new StatelessTemplate((Bundle)exception);
            } 
            return new TemperatureControlTemplate((Bundle)exception);
          } 
          return new ToggleRangeTemplate((Bundle)exception);
        } 
        return new RangeTemplate((Bundle)exception);
      } 
      return new ToggleTemplate((Bundle)exception);
    } 
    return NO_TEMPLATE;
  }
  
  public static ControlTemplate getErrorTemplate() {
    return ERROR_TEMPLATE;
  }
  
  public static ControlTemplate getNoTemplateObject() {
    return NO_TEMPLATE;
  }
  
  public abstract int getTemplateType();
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface TemplateType {}
}
