package android.service.controls.templates;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;

public final class ControlTemplateWrapper implements Parcelable {
  public ControlTemplateWrapper(ControlTemplate paramControlTemplate) {
    Preconditions.checkNotNull(paramControlTemplate);
    this.mControlTemplate = paramControlTemplate;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public ControlTemplate getWrappedTemplate() {
    return this.mControlTemplate;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeBundle(this.mControlTemplate.getDataBundle());
  }
  
  public static final Parcelable.Creator<ControlTemplateWrapper> CREATOR = new Parcelable.Creator<ControlTemplateWrapper>() {
      public ControlTemplateWrapper createFromParcel(Parcel param1Parcel) {
        return 
          new ControlTemplateWrapper(ControlTemplate.createTemplateFromBundle(param1Parcel.readBundle()));
      }
      
      public ControlTemplateWrapper[] newArray(int param1Int) {
        return new ControlTemplateWrapper[param1Int];
      }
    };
  
  private final ControlTemplate mControlTemplate;
}
