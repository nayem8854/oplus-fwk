package android.service.controls.templates;

import android.os.Bundle;
import com.android.internal.util.Preconditions;

public final class ToggleRangeTemplate extends ControlTemplate {
  private static final String KEY_BUTTON = "key_button";
  
  private static final String KEY_RANGE = "key_range";
  
  private static final int TYPE = 6;
  
  private final ControlButton mControlButton;
  
  private final RangeTemplate mRangeTemplate;
  
  ToggleRangeTemplate(Bundle paramBundle) {
    super(paramBundle);
    this.mControlButton = paramBundle.<ControlButton>getParcelable("key_button");
    this.mRangeTemplate = new RangeTemplate(paramBundle.getBundle("key_range"));
  }
  
  public ToggleRangeTemplate(String paramString, ControlButton paramControlButton, RangeTemplate paramRangeTemplate) {
    super(paramString);
    Preconditions.checkNotNull(paramControlButton);
    Preconditions.checkNotNull(paramRangeTemplate);
    this.mControlButton = paramControlButton;
    this.mRangeTemplate = paramRangeTemplate;
  }
  
  public ToggleRangeTemplate(String paramString, boolean paramBoolean, CharSequence paramCharSequence, RangeTemplate paramRangeTemplate) {
    this(paramString, new ControlButton(paramBoolean, paramCharSequence), paramRangeTemplate);
  }
  
  Bundle getDataBundle() {
    Bundle bundle = super.getDataBundle();
    bundle.putParcelable("key_button", this.mControlButton);
    bundle.putBundle("key_range", this.mRangeTemplate.getDataBundle());
    return bundle;
  }
  
  public RangeTemplate getRange() {
    return this.mRangeTemplate;
  }
  
  public boolean isChecked() {
    return this.mControlButton.isChecked();
  }
  
  public CharSequence getActionDescription() {
    return this.mControlButton.getActionDescription();
  }
  
  public int getTemplateType() {
    return 6;
  }
}
