package android.service.controls.templates;

import android.os.Bundle;

public final class StatelessTemplate extends ControlTemplate {
  public int getTemplateType() {
    return 8;
  }
  
  StatelessTemplate(Bundle paramBundle) {
    super(paramBundle);
  }
  
  public StatelessTemplate(String paramString) {
    super(paramString);
  }
}
