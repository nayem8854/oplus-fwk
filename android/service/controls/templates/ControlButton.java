package android.service.controls.templates;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;

public final class ControlButton implements Parcelable {
  public ControlButton(boolean paramBoolean, CharSequence paramCharSequence) {
    Preconditions.checkNotNull(paramCharSequence);
    this.mChecked = paramBoolean;
    this.mActionDescription = paramCharSequence;
  }
  
  public boolean isChecked() {
    return this.mChecked;
  }
  
  public CharSequence getActionDescription() {
    return this.mActionDescription;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByte(this.mChecked);
    paramParcel.writeCharSequence(this.mActionDescription);
  }
  
  ControlButton(Parcel paramParcel) {
    boolean bool;
    if (paramParcel.readByte() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mChecked = bool;
    this.mActionDescription = paramParcel.readCharSequence();
  }
  
  public static final Parcelable.Creator<ControlButton> CREATOR = new Parcelable.Creator<ControlButton>() {
      public ControlButton createFromParcel(Parcel param1Parcel) {
        return new ControlButton(param1Parcel);
      }
      
      public ControlButton[] newArray(int param1Int) {
        return new ControlButton[param1Int];
      }
    };
  
  private final CharSequence mActionDescription;
  
  private final boolean mChecked;
}
