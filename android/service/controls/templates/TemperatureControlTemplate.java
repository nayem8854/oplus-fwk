package android.service.controls.templates;

import android.os.Bundle;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class TemperatureControlTemplate extends ControlTemplate {
  private static final int ALL_FLAGS = 62;
  
  public static final int FLAG_MODE_COOL = 8;
  
  public static final int FLAG_MODE_ECO = 32;
  
  public static final int FLAG_MODE_HEAT = 4;
  
  public static final int FLAG_MODE_HEAT_COOL = 16;
  
  public static final int FLAG_MODE_OFF = 2;
  
  private static final String KEY_CURRENT_ACTIVE_MODE = "key_current_active_mode";
  
  private static final String KEY_CURRENT_MODE = "key_current_mode";
  
  private static final String KEY_MODES = "key_modes";
  
  private static final String KEY_TEMPLATE = "key_template";
  
  public static final int MODE_COOL = 3;
  
  public static final int MODE_ECO = 5;
  
  public static final int MODE_HEAT = 2;
  
  public static final int MODE_HEAT_COOL = 4;
  
  public static final int MODE_OFF = 1;
  
  public static final int MODE_UNKNOWN = 0;
  
  private static final int NUM_MODES = 6;
  
  private static final String TAG = "ThermostatTemplate";
  
  private static final int TYPE = 7;
  
  private static final int[] modeToFlag = new int[] { 0, 2, 4, 8, 16, 32 };
  
  private final int mCurrentActiveMode;
  
  private final int mCurrentMode;
  
  private final int mModes;
  
  private final ControlTemplate mTemplate;
  
  public TemperatureControlTemplate(String paramString, ControlTemplate paramControlTemplate, int paramInt1, int paramInt2, int paramInt3) {
    super(paramString);
    Preconditions.checkNotNull(paramControlTemplate);
    this.mTemplate = paramControlTemplate;
    if (paramInt1 < 0 || paramInt1 >= 6) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Invalid current mode:");
      stringBuilder1.append(paramInt1);
      Log.e("ThermostatTemplate", stringBuilder1.toString());
      this.mCurrentMode = 0;
    } else {
      this.mCurrentMode = paramInt1;
    } 
    if (paramInt2 < 0 || paramInt2 >= 6) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Invalid current active mode:");
      stringBuilder1.append(paramInt2);
      Log.e("ThermostatTemplate", stringBuilder1.toString());
      this.mCurrentActiveMode = 0;
    } else {
      this.mCurrentActiveMode = paramInt2;
    } 
    this.mModes = paramInt1 = paramInt3 & 0x3E;
    paramInt3 = this.mCurrentMode;
    if (paramInt3 == 0 || (paramInt1 & modeToFlag[paramInt3]) != 0) {
      paramInt1 = this.mCurrentActiveMode;
      if (paramInt1 == 0 || (modeToFlag[paramInt1] & this.mModes) != 0)
        return; 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Mode ");
      stringBuilder1.append(paramInt2);
      stringBuilder1.append(" not supported in flag.");
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Mode ");
    stringBuilder.append(this.mCurrentMode);
    stringBuilder.append(" not supported in flag.");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  TemperatureControlTemplate(Bundle paramBundle) {
    super(paramBundle);
    this.mTemplate = ControlTemplate.createTemplateFromBundle(paramBundle.getBundle("key_template"));
    this.mCurrentMode = paramBundle.getInt("key_current_mode");
    this.mCurrentActiveMode = paramBundle.getInt("key_current_active_mode");
    this.mModes = paramBundle.getInt("key_modes");
  }
  
  Bundle getDataBundle() {
    Bundle bundle = super.getDataBundle();
    bundle.putBundle("key_template", this.mTemplate.getDataBundle());
    bundle.putInt("key_current_mode", this.mCurrentMode);
    bundle.putInt("key_current_active_mode", this.mCurrentActiveMode);
    bundle.putInt("key_modes", this.mModes);
    return bundle;
  }
  
  public ControlTemplate getTemplate() {
    return this.mTemplate;
  }
  
  public int getCurrentMode() {
    return this.mCurrentMode;
  }
  
  public int getCurrentActiveMode() {
    return this.mCurrentActiveMode;
  }
  
  public int getModes() {
    return this.mModes;
  }
  
  public int getTemplateType() {
    return 7;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Mode implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ModeFlag implements Annotation {}
}
