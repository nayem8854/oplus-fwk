package android.service.controls.actions;

import android.os.Bundle;

public final class CommandAction extends ControlAction {
  private static final int TYPE = 5;
  
  public CommandAction(String paramString1, String paramString2) {
    super(paramString1, paramString2);
  }
  
  public CommandAction(String paramString) {
    this(paramString, null);
  }
  
  CommandAction(Bundle paramBundle) {
    super(paramBundle);
  }
  
  public int getActionType() {
    return 5;
  }
}
