package android.service.controls.actions;

import android.os.Bundle;

public final class BooleanAction extends ControlAction {
  private static final String KEY_NEW_STATE = "key_new_state";
  
  private static final int TYPE = 1;
  
  private final boolean mNewState;
  
  public BooleanAction(String paramString, boolean paramBoolean) {
    this(paramString, paramBoolean, null);
  }
  
  public BooleanAction(String paramString1, boolean paramBoolean, String paramString2) {
    super(paramString1, paramString2);
    this.mNewState = paramBoolean;
  }
  
  BooleanAction(Bundle paramBundle) {
    super(paramBundle);
    this.mNewState = paramBundle.getBoolean("key_new_state");
  }
  
  public boolean getNewState() {
    return this.mNewState;
  }
  
  public int getActionType() {
    return 1;
  }
  
  Bundle getDataBundle() {
    Bundle bundle = super.getDataBundle();
    bundle.putBoolean("key_new_state", this.mNewState);
    return bundle;
  }
}
