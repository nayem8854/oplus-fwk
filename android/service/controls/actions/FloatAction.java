package android.service.controls.actions;

import android.os.Bundle;

public final class FloatAction extends ControlAction {
  private static final String KEY_NEW_VALUE = "key_new_value";
  
  private static final int TYPE = 2;
  
  private final float mNewValue;
  
  public FloatAction(String paramString, float paramFloat) {
    this(paramString, paramFloat, null);
  }
  
  public FloatAction(String paramString1, float paramFloat, String paramString2) {
    super(paramString1, paramString2);
    this.mNewValue = paramFloat;
  }
  
  FloatAction(Bundle paramBundle) {
    super(paramBundle);
    this.mNewValue = paramBundle.getFloat("key_new_value");
  }
  
  public float getNewValue() {
    return this.mNewValue;
  }
  
  public int getActionType() {
    return 2;
  }
  
  Bundle getDataBundle() {
    Bundle bundle = super.getDataBundle();
    bundle.putFloat("key_new_value", this.mNewValue);
    return bundle;
  }
}
