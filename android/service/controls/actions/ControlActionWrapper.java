package android.service.controls.actions;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;

public final class ControlActionWrapper implements Parcelable {
  public ControlActionWrapper(ControlAction paramControlAction) {
    Preconditions.checkNotNull(paramControlAction);
    this.mControlAction = paramControlAction;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeBundle(this.mControlAction.getDataBundle());
  }
  
  public ControlAction getWrappedAction() {
    return this.mControlAction;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<ControlActionWrapper> CREATOR = new Parcelable.Creator<ControlActionWrapper>() {
      public ControlActionWrapper createFromParcel(Parcel param1Parcel) {
        return 
          new ControlActionWrapper(ControlAction.createActionFromBundle(param1Parcel.readBundle()));
      }
      
      public ControlActionWrapper[] newArray(int param1Int) {
        return new ControlActionWrapper[param1Int];
      }
    };
  
  private final ControlAction mControlAction;
}
