package android.service.controls.actions;

import android.os.Bundle;

public final class ModeAction extends ControlAction {
  private static final String KEY_MODE = "key_mode";
  
  private static final int TYPE = 4;
  
  private final int mNewMode;
  
  public int getActionType() {
    return 4;
  }
  
  public ModeAction(String paramString1, int paramInt, String paramString2) {
    super(paramString1, paramString2);
    this.mNewMode = paramInt;
  }
  
  public ModeAction(String paramString, int paramInt) {
    this(paramString, paramInt, null);
  }
  
  ModeAction(Bundle paramBundle) {
    super(paramBundle);
    this.mNewMode = paramBundle.getInt("key_mode");
  }
  
  Bundle getDataBundle() {
    Bundle bundle = super.getDataBundle();
    bundle.putInt("key_mode", this.mNewMode);
    return bundle;
  }
  
  public int getNewMode() {
    return this.mNewMode;
  }
}
