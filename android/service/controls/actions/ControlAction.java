package android.service.controls.actions;

import android.os.Bundle;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract class ControlAction {
  public static final ControlAction ERROR_ACTION = (ControlAction)new Object();
  
  private static final String KEY_ACTION_TYPE = "key_action_type";
  
  private static final String KEY_CHALLENGE_VALUE = "key_challenge_value";
  
  private static final String KEY_TEMPLATE_ID = "key_template_id";
  
  private static final int NUM_RESPONSE_TYPES = 6;
  
  public static final int RESPONSE_CHALLENGE_ACK = 3;
  
  public static final int RESPONSE_CHALLENGE_PASSPHRASE = 5;
  
  public static final int RESPONSE_CHALLENGE_PIN = 4;
  
  public static final int RESPONSE_FAIL = 2;
  
  public static final int RESPONSE_OK = 1;
  
  public static final int RESPONSE_UNKNOWN = 0;
  
  private static final String TAG = "ControlAction";
  
  public static final int TYPE_BOOLEAN = 1;
  
  public static final int TYPE_COMMAND = 5;
  
  public static final int TYPE_ERROR = -1;
  
  public static final int TYPE_FLOAT = 2;
  
  public static final int TYPE_MODE = 4;
  
  private final String mChallengeValue;
  
  private final String mTemplateId;
  
  public static final boolean isValidResponse(int paramInt) {
    boolean bool;
    if (paramInt >= 0 && paramInt < 6) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private ControlAction() {
    this.mTemplateId = "";
    this.mChallengeValue = null;
  }
  
  ControlAction(String paramString1, String paramString2) {
    Preconditions.checkNotNull(paramString1);
    this.mTemplateId = paramString1;
    this.mChallengeValue = paramString2;
  }
  
  ControlAction(Bundle paramBundle) {
    this.mTemplateId = paramBundle.getString("key_template_id");
    this.mChallengeValue = paramBundle.getString("key_challenge_value");
  }
  
  public String getTemplateId() {
    return this.mTemplateId;
  }
  
  public String getChallengeValue() {
    return this.mChallengeValue;
  }
  
  Bundle getDataBundle() {
    Bundle bundle = new Bundle();
    bundle.putInt("key_action_type", getActionType());
    bundle.putString("key_template_id", this.mTemplateId);
    bundle.putString("key_challenge_value", this.mChallengeValue);
    return bundle;
  }
  
  static ControlAction createActionFromBundle(Bundle paramBundle) {
    if (paramBundle == null) {
      Log.e("ControlAction", "Null bundle");
      return ERROR_ACTION;
    } 
    int i = paramBundle.getInt("key_action_type", -1);
    if (i != 1) {
      if (i != 2) {
        if (i != 4) {
          if (i != 5)
            try {
              return ERROR_ACTION;
            } catch (Exception exception) {
              Log.e("ControlAction", "Error creating action", exception);
              return ERROR_ACTION;
            }  
          return new CommandAction((Bundle)exception);
        } 
        return new ModeAction((Bundle)exception);
      } 
      return new FloatAction((Bundle)exception);
    } 
    return new BooleanAction((Bundle)exception);
  }
  
  public static ControlAction getErrorAction() {
    return ERROR_ACTION;
  }
  
  public abstract int getActionType();
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ActionType {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ResponseResult {}
}
