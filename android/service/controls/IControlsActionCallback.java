package android.service.controls;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IControlsActionCallback extends IInterface {
  void accept(IBinder paramIBinder, String paramString, int paramInt) throws RemoteException;
  
  class Default implements IControlsActionCallback {
    public void accept(IBinder param1IBinder, String param1String, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IControlsActionCallback {
    private static final String DESCRIPTOR = "android.service.controls.IControlsActionCallback";
    
    static final int TRANSACTION_accept = 1;
    
    public Stub() {
      attachInterface(this, "android.service.controls.IControlsActionCallback");
    }
    
    public static IControlsActionCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.controls.IControlsActionCallback");
      if (iInterface != null && iInterface instanceof IControlsActionCallback)
        return (IControlsActionCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "accept";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.service.controls.IControlsActionCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.controls.IControlsActionCallback");
      IBinder iBinder = param1Parcel1.readStrongBinder();
      String str = param1Parcel1.readString();
      param1Int1 = param1Parcel1.readInt();
      accept(iBinder, str, param1Int1);
      return true;
    }
    
    private static class Proxy implements IControlsActionCallback {
      public static IControlsActionCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.controls.IControlsActionCallback";
      }
      
      public void accept(IBinder param2IBinder, String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.controls.IControlsActionCallback");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IControlsActionCallback.Stub.getDefaultImpl() != null) {
            IControlsActionCallback.Stub.getDefaultImpl().accept(param2IBinder, param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IControlsActionCallback param1IControlsActionCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IControlsActionCallback != null) {
          Proxy.sDefaultImpl = param1IControlsActionCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IControlsActionCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
