package android.service.controls;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.os.RemoteException;
import android.service.controls.actions.ControlAction;
import android.service.controls.actions.ControlActionWrapper;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.util.List;
import java.util.concurrent.Flow;
import java.util.function.Consumer;

public abstract class ControlsProviderService extends Service {
  public static final String ACTION_ADD_CONTROL = "android.service.controls.action.ADD_CONTROL";
  
  public static final String CALLBACK_BUNDLE = "CALLBACK_BUNDLE";
  
  public static final String CALLBACK_TOKEN = "CALLBACK_TOKEN";
  
  public static final String EXTRA_CONTROL = "android.service.controls.extra.CONTROL";
  
  public static final String SERVICE_CONTROLS = "android.service.controls.ControlsProviderService";
  
  public static final String TAG = "ControlsProviderService";
  
  private RequestHandler mHandler;
  
  private IBinder mToken;
  
  public Flow.Publisher<Control> createPublisherForSuggested() {
    return null;
  }
  
  public final IBinder onBind(Intent paramIntent) {
    this.mHandler = new RequestHandler(Looper.getMainLooper());
    Bundle bundle = paramIntent.getBundleExtra("CALLBACK_BUNDLE");
    this.mToken = bundle.getBinder("CALLBACK_TOKEN");
    return new IControlsProvider.Stub() {
        final ControlsProviderService this$0;
        
        public void load(IControlsSubscriber param1IControlsSubscriber) {
          ControlsProviderService.this.mHandler.obtainMessage(1, param1IControlsSubscriber).sendToTarget();
        }
        
        public void loadSuggested(IControlsSubscriber param1IControlsSubscriber) {
          Message message = ControlsProviderService.this.mHandler.obtainMessage(4, param1IControlsSubscriber);
          message.sendToTarget();
        }
        
        public void subscribe(List<String> param1List, IControlsSubscriber param1IControlsSubscriber) {
          ControlsProviderService.SubscribeMessage subscribeMessage = new ControlsProviderService.SubscribeMessage(param1List, param1IControlsSubscriber);
          ControlsProviderService.this.mHandler.obtainMessage(2, subscribeMessage).sendToTarget();
        }
        
        public void action(String param1String, ControlActionWrapper param1ControlActionWrapper, IControlsActionCallback param1IControlsActionCallback) {
          ControlsProviderService.ActionMessage actionMessage = new ControlsProviderService.ActionMessage(param1String, param1ControlActionWrapper.getWrappedAction(), param1IControlsActionCallback);
          ControlsProviderService.this.mHandler.obtainMessage(3, actionMessage).sendToTarget();
        }
      };
  }
  
  public final boolean onUnbind(Intent paramIntent) {
    this.mHandler = null;
    return true;
  }
  
  class RequestHandler extends Handler {
    private static final int MSG_ACTION = 3;
    
    private static final int MSG_LOAD = 1;
    
    private static final int MSG_LOAD_SUGGESTED = 4;
    
    private static final int MSG_SUBSCRIBE = 2;
    
    final ControlsProviderService this$0;
    
    RequestHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      ControlsProviderService.SubscriberProxy subscriberProxy;
      int i = param1Message.what;
      if (i != 1) {
        String str;
        if (i != 2) {
          if (i != 3) {
            if (i == 4) {
              IControlsSubscriber iControlsSubscriber = (IControlsSubscriber)param1Message.obj;
              subscriberProxy = new ControlsProviderService.SubscriberProxy(true, ControlsProviderService.this.mToken, iControlsSubscriber);
              ControlsProviderService controlsProviderService = ControlsProviderService.this;
              Flow.Publisher<Control> publisher = controlsProviderService.createPublisherForSuggested();
              if (publisher == null) {
                Log.i("ControlsProviderService", "No publisher provided for suggested controls");
                subscriberProxy.onComplete();
              } else {
                publisher.subscribe(subscriberProxy);
              } 
            } 
          } else {
            ControlsProviderService.ActionMessage actionMessage = (ControlsProviderService.ActionMessage)((Message)subscriberProxy).obj;
            ControlsProviderService controlsProviderService = ControlsProviderService.this;
            str = actionMessage.mControlId;
            ControlAction controlAction = actionMessage.mAction;
            String str1 = actionMessage.mControlId;
            IControlsActionCallback iControlsActionCallback = actionMessage.mCb;
            Consumer<Integer> consumer = consumerFor(str1, iControlsActionCallback);
            controlsProviderService.performControlAction(str, controlAction, consumer);
          } 
        } else {
          ControlsProviderService.SubscribeMessage subscribeMessage = (ControlsProviderService.SubscribeMessage)((Message)str).obj;
          subscriberProxy = new ControlsProviderService.SubscriberProxy(false, ControlsProviderService.this.mToken, subscribeMessage.mSubscriber);
          Flow.Publisher<Control> publisher = ControlsProviderService.this.createPublisherFor(subscribeMessage.mControlIds);
          publisher.subscribe(subscriberProxy);
        } 
      } else {
        IControlsSubscriber iControlsSubscriber = (IControlsSubscriber)((Message)subscriberProxy).obj;
        ControlsProviderService.SubscriberProxy subscriberProxy1 = new ControlsProviderService.SubscriberProxy(true, ControlsProviderService.this.mToken, iControlsSubscriber);
        ControlsProviderService.this.createPublisherForAllAvailable().subscribe(subscriberProxy1);
      } 
    }
    
    private Consumer<Integer> consumerFor(String param1String, IControlsActionCallback param1IControlsActionCallback) {
      return new _$$Lambda$ControlsProviderService$RequestHandler$mRK5NjhY1D2BY_5KfsEN3snmSCg(this, param1IControlsActionCallback, param1String);
    }
  }
  
  private static boolean isStatelessControl(Control paramControl) {
    if (paramControl.getStatus() == 0 && 
      paramControl.getControlTemplate().getTemplateType() == 0)
      if (TextUtils.isEmpty(paramControl.getStatusText()))
        return true;  
    return false;
  }
  
  class SubscriberProxy implements Flow.Subscriber<Control> {
    private IControlsSubscriber mCs;
    
    private boolean mEnforceStateless;
    
    private IBinder mToken;
    
    SubscriberProxy(ControlsProviderService this$0, IBinder param1IBinder, IControlsSubscriber param1IControlsSubscriber) {
      this.mEnforceStateless = this$0;
      this.mToken = param1IBinder;
      this.mCs = param1IControlsSubscriber;
    }
    
    public void onSubscribe(Flow.Subscription param1Subscription) {
      try {
        IControlsSubscriber iControlsSubscriber = this.mCs;
        IBinder iBinder = this.mToken;
        ControlsProviderService.SubscriptionAdapter subscriptionAdapter = new ControlsProviderService.SubscriptionAdapter();
        this(param1Subscription);
        iControlsSubscriber.onSubscribe(iBinder, subscriptionAdapter);
      } catch (RemoteException remoteException) {
        remoteException.rethrowAsRuntimeException();
      } 
    }
    
    public void onNext(Control param1Control) {
      Preconditions.checkNotNull(param1Control);
      Control control = param1Control;
      try {
        Control control1;
        if (this.mEnforceStateless) {
          control = param1Control;
          if (!ControlsProviderService.isStatelessControl(param1Control)) {
            Log.w("ControlsProviderService", "onNext(): control is not stateless. Use the Control.StatelessBuilder() to build the control.");
            Control.StatelessBuilder statelessBuilder = new Control.StatelessBuilder();
            this(param1Control);
            control1 = statelessBuilder.build();
          } 
        } 
        this.mCs.onNext(this.mToken, control1);
      } catch (RemoteException remoteException) {
        remoteException.rethrowAsRuntimeException();
      } 
    }
    
    public void onError(Throwable param1Throwable) {
      try {
        this.mCs.onError(this.mToken, param1Throwable.toString());
      } catch (RemoteException remoteException) {
        remoteException.rethrowAsRuntimeException();
      } 
    }
    
    public void onComplete() {
      try {
        this.mCs.onComplete(this.mToken);
      } catch (RemoteException remoteException) {
        remoteException.rethrowAsRuntimeException();
      } 
    }
  }
  
  public static void requestAddControl(Context paramContext, ComponentName paramComponentName, Control paramControl) {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramComponentName);
    Preconditions.checkNotNull(paramControl);
    String str = paramContext.getString(17039855);
    Intent intent = new Intent("android.service.controls.action.ADD_CONTROL");
    intent.putExtra("android.intent.extra.COMPONENT_NAME", (Parcelable)paramComponentName);
    intent.setPackage(str);
    if (isStatelessControl(paramControl)) {
      intent.putExtra("android.service.controls.extra.CONTROL", paramControl);
    } else {
      intent.putExtra("android.service.controls.extra.CONTROL", (new Control.StatelessBuilder(paramControl)).build());
    } 
    paramContext.sendBroadcast(intent, "android.permission.BIND_CONTROLS");
  }
  
  public abstract Flow.Publisher<Control> createPublisherFor(List<String> paramList);
  
  public abstract Flow.Publisher<Control> createPublisherForAllAvailable();
  
  public abstract void performControlAction(String paramString, ControlAction paramControlAction, Consumer<Integer> paramConsumer);
  
  private static class SubscriptionAdapter extends IControlsSubscription.Stub {
    final Flow.Subscription mSubscription;
    
    SubscriptionAdapter(Flow.Subscription param1Subscription) {
      this.mSubscription = param1Subscription;
    }
    
    public void request(long param1Long) {
      this.mSubscription.request(param1Long);
    }
    
    public void cancel() {
      this.mSubscription.cancel();
    }
  }
  
  class ActionMessage {
    final ControlAction mAction;
    
    final IControlsActionCallback mCb;
    
    final String mControlId;
    
    ActionMessage(ControlsProviderService this$0, ControlAction param1ControlAction, IControlsActionCallback param1IControlsActionCallback) {
      this.mControlId = (String)this$0;
      this.mAction = param1ControlAction;
      this.mCb = param1IControlsActionCallback;
    }
  }
  
  class SubscribeMessage {
    final List<String> mControlIds;
    
    final IControlsSubscriber mSubscriber;
    
    SubscribeMessage(ControlsProviderService this$0, IControlsSubscriber param1IControlsSubscriber) {
      this.mControlIds = (List<String>)this$0;
      this.mSubscriber = param1IControlsSubscriber;
    }
  }
}
