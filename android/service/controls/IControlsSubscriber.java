package android.service.controls;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IControlsSubscriber extends IInterface {
  void onComplete(IBinder paramIBinder) throws RemoteException;
  
  void onError(IBinder paramIBinder, String paramString) throws RemoteException;
  
  void onNext(IBinder paramIBinder, Control paramControl) throws RemoteException;
  
  void onSubscribe(IBinder paramIBinder, IControlsSubscription paramIControlsSubscription) throws RemoteException;
  
  class Default implements IControlsSubscriber {
    public void onSubscribe(IBinder param1IBinder, IControlsSubscription param1IControlsSubscription) throws RemoteException {}
    
    public void onNext(IBinder param1IBinder, Control param1Control) throws RemoteException {}
    
    public void onError(IBinder param1IBinder, String param1String) throws RemoteException {}
    
    public void onComplete(IBinder param1IBinder) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IControlsSubscriber {
    private static final String DESCRIPTOR = "android.service.controls.IControlsSubscriber";
    
    static final int TRANSACTION_onComplete = 4;
    
    static final int TRANSACTION_onError = 3;
    
    static final int TRANSACTION_onNext = 2;
    
    static final int TRANSACTION_onSubscribe = 1;
    
    public Stub() {
      attachInterface(this, "android.service.controls.IControlsSubscriber");
    }
    
    public static IControlsSubscriber asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.controls.IControlsSubscriber");
      if (iInterface != null && iInterface instanceof IControlsSubscriber)
        return (IControlsSubscriber)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onComplete";
          } 
          return "onError";
        } 
        return "onNext";
      } 
      return "onSubscribe";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          IBinder iBinder2;
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.service.controls.IControlsSubscriber");
              return true;
            } 
            param1Parcel1.enforceInterface("android.service.controls.IControlsSubscriber");
            iBinder2 = param1Parcel1.readStrongBinder();
            onComplete(iBinder2);
            return true;
          } 
          iBinder2.enforceInterface("android.service.controls.IControlsSubscriber");
          IBinder iBinder3 = iBinder2.readStrongBinder();
          str = iBinder2.readString();
          onError(iBinder3, str);
          return true;
        } 
        str.enforceInterface("android.service.controls.IControlsSubscriber");
        IBinder iBinder1 = str.readStrongBinder();
        if (str.readInt() != 0) {
          Control control = Control.CREATOR.createFromParcel((Parcel)str);
        } else {
          str = null;
        } 
        onNext(iBinder1, (Control)str);
        return true;
      } 
      str.enforceInterface("android.service.controls.IControlsSubscriber");
      IBinder iBinder = str.readStrongBinder();
      IControlsSubscription iControlsSubscription = IControlsSubscription.Stub.asInterface(str.readStrongBinder());
      onSubscribe(iBinder, iControlsSubscription);
      return true;
    }
    
    private static class Proxy implements IControlsSubscriber {
      public static IControlsSubscriber sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.controls.IControlsSubscriber";
      }
      
      public void onSubscribe(IBinder param2IBinder, IControlsSubscription param2IControlsSubscription) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.controls.IControlsSubscriber");
          parcel.writeStrongBinder(param2IBinder);
          if (param2IControlsSubscription != null) {
            iBinder = param2IControlsSubscription.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IControlsSubscriber.Stub.getDefaultImpl() != null) {
            IControlsSubscriber.Stub.getDefaultImpl().onSubscribe(param2IBinder, param2IControlsSubscription);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onNext(IBinder param2IBinder, Control param2Control) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.controls.IControlsSubscriber");
          parcel.writeStrongBinder(param2IBinder);
          if (param2Control != null) {
            parcel.writeInt(1);
            param2Control.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IControlsSubscriber.Stub.getDefaultImpl() != null) {
            IControlsSubscriber.Stub.getDefaultImpl().onNext(param2IBinder, param2Control);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onError(IBinder param2IBinder, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.controls.IControlsSubscriber");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IControlsSubscriber.Stub.getDefaultImpl() != null) {
            IControlsSubscriber.Stub.getDefaultImpl().onError(param2IBinder, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onComplete(IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.controls.IControlsSubscriber");
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IControlsSubscriber.Stub.getDefaultImpl() != null) {
            IControlsSubscriber.Stub.getDefaultImpl().onComplete(param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IControlsSubscriber param1IControlsSubscriber) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IControlsSubscriber != null) {
          Proxy.sDefaultImpl = param1IControlsSubscriber;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IControlsSubscriber getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
