package android.service.controls;

import android.app.PendingIntent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Icon;
import android.os.Parcel;
import android.os.Parcelable;
import android.service.controls.templates.ControlTemplate;
import android.service.controls.templates.ControlTemplateWrapper;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class Control implements Parcelable {
  Control(String paramString, int paramInt1, CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3, CharSequence paramCharSequence4, PendingIntent paramPendingIntent, Icon paramIcon, ColorStateList paramColorStateList, int paramInt2, ControlTemplate paramControlTemplate, CharSequence paramCharSequence5) {
    Preconditions.checkNotNull(paramString);
    Preconditions.checkNotNull(paramCharSequence1);
    Preconditions.checkNotNull(paramCharSequence2);
    Preconditions.checkNotNull(paramPendingIntent);
    Preconditions.checkNotNull(paramControlTemplate);
    Preconditions.checkNotNull(paramCharSequence5);
    this.mControlId = paramString;
    if (!DeviceTypes.validDeviceType(paramInt1)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid device type:");
      stringBuilder.append(paramInt1);
      Log.e("Control", stringBuilder.toString());
      this.mDeviceType = 0;
    } else {
      this.mDeviceType = paramInt1;
    } 
    this.mTitle = paramCharSequence1;
    this.mSubtitle = paramCharSequence2;
    this.mStructure = paramCharSequence3;
    this.mZone = paramCharSequence4;
    this.mAppIntent = paramPendingIntent;
    this.mCustomColor = paramColorStateList;
    this.mCustomIcon = paramIcon;
    if (paramInt2 < 0 || paramInt2 >= 5) {
      this.mStatus = 0;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Status unknown:");
      stringBuilder.append(paramInt2);
      Log.e("Control", stringBuilder.toString());
    } else {
      this.mStatus = paramInt2;
    } 
    this.mControlTemplate = paramControlTemplate;
    this.mStatusText = paramCharSequence5;
  }
  
  Control(Parcel paramParcel) {
    this.mControlId = paramParcel.readString();
    this.mDeviceType = paramParcel.readInt();
    this.mTitle = paramParcel.readCharSequence();
    this.mSubtitle = paramParcel.readCharSequence();
    if (paramParcel.readByte() == 1) {
      this.mStructure = paramParcel.readCharSequence();
    } else {
      this.mStructure = null;
    } 
    if (paramParcel.readByte() == 1) {
      this.mZone = paramParcel.readCharSequence();
    } else {
      this.mZone = null;
    } 
    this.mAppIntent = PendingIntent.CREATOR.createFromParcel(paramParcel);
    if (paramParcel.readByte() == 1) {
      this.mCustomIcon = Icon.CREATOR.createFromParcel(paramParcel);
    } else {
      this.mCustomIcon = null;
    } 
    if (paramParcel.readByte() == 1) {
      this.mCustomColor = ColorStateList.CREATOR.createFromParcel(paramParcel);
    } else {
      this.mCustomColor = null;
    } 
    this.mStatus = paramParcel.readInt();
    ControlTemplateWrapper controlTemplateWrapper = ControlTemplateWrapper.CREATOR.createFromParcel(paramParcel);
    this.mControlTemplate = controlTemplateWrapper.getWrappedTemplate();
    this.mStatusText = paramParcel.readCharSequence();
  }
  
  public String getControlId() {
    return this.mControlId;
  }
  
  public int getDeviceType() {
    return this.mDeviceType;
  }
  
  public CharSequence getTitle() {
    return this.mTitle;
  }
  
  public CharSequence getSubtitle() {
    return this.mSubtitle;
  }
  
  public CharSequence getStructure() {
    return this.mStructure;
  }
  
  public CharSequence getZone() {
    return this.mZone;
  }
  
  public PendingIntent getAppIntent() {
    return this.mAppIntent;
  }
  
  public Icon getCustomIcon() {
    return this.mCustomIcon;
  }
  
  public ColorStateList getCustomColor() {
    return this.mCustomColor;
  }
  
  public int getStatus() {
    return this.mStatus;
  }
  
  public ControlTemplate getControlTemplate() {
    return this.mControlTemplate;
  }
  
  public CharSequence getStatusText() {
    return this.mStatusText;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mControlId);
    paramParcel.writeInt(this.mDeviceType);
    paramParcel.writeCharSequence(this.mTitle);
    paramParcel.writeCharSequence(this.mSubtitle);
    if (this.mStructure != null) {
      paramParcel.writeByte((byte)1);
      paramParcel.writeCharSequence(this.mStructure);
    } else {
      paramParcel.writeByte((byte)0);
    } 
    if (this.mZone != null) {
      paramParcel.writeByte((byte)1);
      paramParcel.writeCharSequence(this.mZone);
    } else {
      paramParcel.writeByte((byte)0);
    } 
    this.mAppIntent.writeToParcel(paramParcel, paramInt);
    if (this.mCustomIcon != null) {
      paramParcel.writeByte((byte)1);
      this.mCustomIcon.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeByte((byte)0);
    } 
    if (this.mCustomColor != null) {
      paramParcel.writeByte((byte)1);
      this.mCustomColor.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeByte((byte)0);
    } 
    paramParcel.writeInt(this.mStatus);
    (new ControlTemplateWrapper(this.mControlTemplate)).writeToParcel(paramParcel, paramInt);
    paramParcel.writeCharSequence(this.mStatusText);
  }
  
  public static final Parcelable.Creator<Control> CREATOR = new Parcelable.Creator<Control>() {
      public Control createFromParcel(Parcel param1Parcel) {
        return new Control(param1Parcel);
      }
      
      public Control[] newArray(int param1Int) {
        return new Control[param1Int];
      }
    };
  
  private static final int NUM_STATUS = 5;
  
  public static final int STATUS_DISABLED = 4;
  
  public static final int STATUS_ERROR = 3;
  
  public static final int STATUS_NOT_FOUND = 2;
  
  public static final int STATUS_OK = 1;
  
  public static final int STATUS_UNKNOWN = 0;
  
  private static final String TAG = "Control";
  
  private final PendingIntent mAppIntent;
  
  private final String mControlId;
  
  private final ControlTemplate mControlTemplate;
  
  private final ColorStateList mCustomColor;
  
  private final Icon mCustomIcon;
  
  private final int mDeviceType;
  
  private final int mStatus;
  
  private final CharSequence mStatusText;
  
  private final CharSequence mStructure;
  
  private final CharSequence mSubtitle;
  
  private final CharSequence mTitle;
  
  private final CharSequence mZone;
  
  class StatelessBuilder {
    private int mDeviceType = 0;
    
    private CharSequence mTitle = "";
    
    private CharSequence mSubtitle = "";
    
    private static final String TAG = "StatelessBuilder";
    
    private PendingIntent mAppIntent;
    
    private String mControlId;
    
    private ColorStateList mCustomColor;
    
    private Icon mCustomIcon;
    
    private CharSequence mStructure;
    
    private CharSequence mZone;
    
    public StatelessBuilder(Control this$0, PendingIntent param1PendingIntent) {
      Preconditions.checkNotNull(this$0);
      Preconditions.checkNotNull(param1PendingIntent);
      this.mControlId = (String)this$0;
      this.mAppIntent = param1PendingIntent;
    }
    
    public StatelessBuilder(Control this$0) {
      Preconditions.checkNotNull(this$0);
      this.mControlId = this$0.mControlId;
      this.mDeviceType = this$0.mDeviceType;
      this.mTitle = this$0.mTitle;
      this.mSubtitle = this$0.mSubtitle;
      this.mStructure = this$0.mStructure;
      this.mZone = this$0.mZone;
      this.mAppIntent = this$0.mAppIntent;
      this.mCustomIcon = this$0.mCustomIcon;
      this.mCustomColor = this$0.mCustomColor;
    }
    
    public StatelessBuilder setControlId(String param1String) {
      Preconditions.checkNotNull(param1String);
      this.mControlId = param1String;
      return this;
    }
    
    public StatelessBuilder setDeviceType(int param1Int) {
      if (!DeviceTypes.validDeviceType(param1Int)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid device type:");
        stringBuilder.append(param1Int);
        Log.e("StatelessBuilder", stringBuilder.toString());
        this.mDeviceType = 0;
      } else {
        this.mDeviceType = param1Int;
      } 
      return this;
    }
    
    public StatelessBuilder setTitle(CharSequence param1CharSequence) {
      Preconditions.checkNotNull(param1CharSequence);
      this.mTitle = param1CharSequence;
      return this;
    }
    
    public StatelessBuilder setSubtitle(CharSequence param1CharSequence) {
      Preconditions.checkNotNull(param1CharSequence);
      this.mSubtitle = param1CharSequence;
      return this;
    }
    
    public StatelessBuilder setStructure(CharSequence param1CharSequence) {
      this.mStructure = param1CharSequence;
      return this;
    }
    
    public StatelessBuilder setZone(CharSequence param1CharSequence) {
      this.mZone = param1CharSequence;
      return this;
    }
    
    public StatelessBuilder setAppIntent(PendingIntent param1PendingIntent) {
      Preconditions.checkNotNull(param1PendingIntent);
      this.mAppIntent = param1PendingIntent;
      return this;
    }
    
    public StatelessBuilder setCustomIcon(Icon param1Icon) {
      this.mCustomIcon = param1Icon;
      return this;
    }
    
    public StatelessBuilder setCustomColor(ColorStateList param1ColorStateList) {
      this.mCustomColor = param1ColorStateList;
      return this;
    }
    
    public Control build() {
      return new Control(this.mControlId, this.mDeviceType, this.mTitle, this.mSubtitle, this.mStructure, this.mZone, this.mAppIntent, this.mCustomIcon, this.mCustomColor, 0, ControlTemplate.NO_TEMPLATE, "");
    }
  }
  
  class StatefulBuilder {
    private int mDeviceType = 0;
    
    private CharSequence mTitle = "";
    
    private CharSequence mSubtitle = "";
    
    private int mStatus = 0;
    
    private ControlTemplate mControlTemplate = ControlTemplate.NO_TEMPLATE;
    
    private CharSequence mStatusText = "";
    
    private static final String TAG = "StatefulBuilder";
    
    private PendingIntent mAppIntent;
    
    private String mControlId;
    
    private ColorStateList mCustomColor;
    
    private Icon mCustomIcon;
    
    private CharSequence mStructure;
    
    private CharSequence mZone;
    
    public StatefulBuilder(Control this$0, PendingIntent param1PendingIntent) {
      Preconditions.checkNotNull(this$0);
      Preconditions.checkNotNull(param1PendingIntent);
      this.mControlId = (String)this$0;
      this.mAppIntent = param1PendingIntent;
    }
    
    public StatefulBuilder(Control this$0) {
      Preconditions.checkNotNull(this$0);
      this.mControlId = this$0.mControlId;
      this.mDeviceType = this$0.mDeviceType;
      this.mTitle = this$0.mTitle;
      this.mSubtitle = this$0.mSubtitle;
      this.mStructure = this$0.mStructure;
      this.mZone = this$0.mZone;
      this.mAppIntent = this$0.mAppIntent;
      this.mCustomIcon = this$0.mCustomIcon;
      this.mCustomColor = this$0.mCustomColor;
      this.mStatus = this$0.mStatus;
      this.mControlTemplate = this$0.mControlTemplate;
      this.mStatusText = this$0.mStatusText;
    }
    
    public StatefulBuilder setControlId(String param1String) {
      Preconditions.checkNotNull(param1String);
      this.mControlId = param1String;
      return this;
    }
    
    public StatefulBuilder setDeviceType(int param1Int) {
      if (!DeviceTypes.validDeviceType(param1Int)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid device type:");
        stringBuilder.append(param1Int);
        Log.e("StatefulBuilder", stringBuilder.toString());
        this.mDeviceType = 0;
      } else {
        this.mDeviceType = param1Int;
      } 
      return this;
    }
    
    public StatefulBuilder setTitle(CharSequence param1CharSequence) {
      Preconditions.checkNotNull(param1CharSequence);
      this.mTitle = param1CharSequence;
      return this;
    }
    
    public StatefulBuilder setSubtitle(CharSequence param1CharSequence) {
      Preconditions.checkNotNull(param1CharSequence);
      this.mSubtitle = param1CharSequence;
      return this;
    }
    
    public StatefulBuilder setStructure(CharSequence param1CharSequence) {
      this.mStructure = param1CharSequence;
      return this;
    }
    
    public StatefulBuilder setZone(CharSequence param1CharSequence) {
      this.mZone = param1CharSequence;
      return this;
    }
    
    public StatefulBuilder setAppIntent(PendingIntent param1PendingIntent) {
      Preconditions.checkNotNull(param1PendingIntent);
      this.mAppIntent = param1PendingIntent;
      return this;
    }
    
    public StatefulBuilder setCustomIcon(Icon param1Icon) {
      this.mCustomIcon = param1Icon;
      return this;
    }
    
    public StatefulBuilder setCustomColor(ColorStateList param1ColorStateList) {
      this.mCustomColor = param1ColorStateList;
      return this;
    }
    
    public StatefulBuilder setStatus(int param1Int) {
      if (param1Int < 0 || param1Int >= 5) {
        this.mStatus = 0;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Status unknown:");
        stringBuilder.append(param1Int);
        Log.e("StatefulBuilder", stringBuilder.toString());
        return this;
      } 
      this.mStatus = param1Int;
      return this;
    }
    
    public StatefulBuilder setControlTemplate(ControlTemplate param1ControlTemplate) {
      Preconditions.checkNotNull(param1ControlTemplate);
      this.mControlTemplate = param1ControlTemplate;
      return this;
    }
    
    public StatefulBuilder setStatusText(CharSequence param1CharSequence) {
      Preconditions.checkNotNull(param1CharSequence);
      this.mStatusText = param1CharSequence;
      return this;
    }
    
    public Control build() {
      return new Control(this.mControlId, this.mDeviceType, this.mTitle, this.mSubtitle, this.mStructure, this.mZone, this.mAppIntent, this.mCustomIcon, this.mCustomColor, this.mStatus, this.mControlTemplate, this.mStatusText);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Status implements Annotation {}
}
