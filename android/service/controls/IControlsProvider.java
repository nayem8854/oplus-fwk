package android.service.controls;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.service.controls.actions.ControlActionWrapper;
import java.util.ArrayList;
import java.util.List;

public interface IControlsProvider extends IInterface {
  void action(String paramString, ControlActionWrapper paramControlActionWrapper, IControlsActionCallback paramIControlsActionCallback) throws RemoteException;
  
  void load(IControlsSubscriber paramIControlsSubscriber) throws RemoteException;
  
  void loadSuggested(IControlsSubscriber paramIControlsSubscriber) throws RemoteException;
  
  void subscribe(List<String> paramList, IControlsSubscriber paramIControlsSubscriber) throws RemoteException;
  
  class Default implements IControlsProvider {
    public void load(IControlsSubscriber param1IControlsSubscriber) throws RemoteException {}
    
    public void loadSuggested(IControlsSubscriber param1IControlsSubscriber) throws RemoteException {}
    
    public void subscribe(List<String> param1List, IControlsSubscriber param1IControlsSubscriber) throws RemoteException {}
    
    public void action(String param1String, ControlActionWrapper param1ControlActionWrapper, IControlsActionCallback param1IControlsActionCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IControlsProvider {
    private static final String DESCRIPTOR = "android.service.controls.IControlsProvider";
    
    static final int TRANSACTION_action = 4;
    
    static final int TRANSACTION_load = 1;
    
    static final int TRANSACTION_loadSuggested = 2;
    
    static final int TRANSACTION_subscribe = 3;
    
    public Stub() {
      attachInterface(this, "android.service.controls.IControlsProvider");
    }
    
    public static IControlsProvider asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.controls.IControlsProvider");
      if (iInterface != null && iInterface instanceof IControlsProvider)
        return (IControlsProvider)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "action";
          } 
          return "subscribe";
        } 
        return "loadSuggested";
      } 
      return "load";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          IControlsActionCallback iControlsActionCallback;
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.service.controls.IControlsProvider");
              return true;
            } 
            param1Parcel1.enforceInterface("android.service.controls.IControlsProvider");
            String str = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              ControlActionWrapper controlActionWrapper = ControlActionWrapper.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            iControlsActionCallback = IControlsActionCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            action(str, (ControlActionWrapper)param1Parcel2, iControlsActionCallback);
            return true;
          } 
          iControlsActionCallback.enforceInterface("android.service.controls.IControlsProvider");
          ArrayList<String> arrayList = iControlsActionCallback.createStringArrayList();
          iControlsSubscriber = IControlsSubscriber.Stub.asInterface(iControlsActionCallback.readStrongBinder());
          subscribe(arrayList, iControlsSubscriber);
          return true;
        } 
        iControlsSubscriber.enforceInterface("android.service.controls.IControlsProvider");
        iControlsSubscriber = IControlsSubscriber.Stub.asInterface(iControlsSubscriber.readStrongBinder());
        loadSuggested(iControlsSubscriber);
        return true;
      } 
      iControlsSubscriber.enforceInterface("android.service.controls.IControlsProvider");
      IControlsSubscriber iControlsSubscriber = IControlsSubscriber.Stub.asInterface(iControlsSubscriber.readStrongBinder());
      load(iControlsSubscriber);
      return true;
    }
    
    private static class Proxy implements IControlsProvider {
      public static IControlsProvider sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.controls.IControlsProvider";
      }
      
      public void load(IControlsSubscriber param2IControlsSubscriber) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.controls.IControlsProvider");
          if (param2IControlsSubscriber != null) {
            iBinder = param2IControlsSubscriber.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IControlsProvider.Stub.getDefaultImpl() != null) {
            IControlsProvider.Stub.getDefaultImpl().load(param2IControlsSubscriber);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void loadSuggested(IControlsSubscriber param2IControlsSubscriber) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.controls.IControlsProvider");
          if (param2IControlsSubscriber != null) {
            iBinder = param2IControlsSubscriber.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IControlsProvider.Stub.getDefaultImpl() != null) {
            IControlsProvider.Stub.getDefaultImpl().loadSuggested(param2IControlsSubscriber);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void subscribe(List<String> param2List, IControlsSubscriber param2IControlsSubscriber) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.controls.IControlsProvider");
          parcel.writeStringList(param2List);
          if (param2IControlsSubscriber != null) {
            iBinder = param2IControlsSubscriber.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IControlsProvider.Stub.getDefaultImpl() != null) {
            IControlsProvider.Stub.getDefaultImpl().subscribe(param2List, param2IControlsSubscriber);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void action(String param2String, ControlActionWrapper param2ControlActionWrapper, IControlsActionCallback param2IControlsActionCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.controls.IControlsProvider");
          parcel.writeString(param2String);
          if (param2ControlActionWrapper != null) {
            parcel.writeInt(1);
            param2ControlActionWrapper.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IControlsActionCallback != null) {
            iBinder = param2IControlsActionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IControlsProvider.Stub.getDefaultImpl() != null) {
            IControlsProvider.Stub.getDefaultImpl().action(param2String, param2ControlActionWrapper, param2IControlsActionCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IControlsProvider param1IControlsProvider) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IControlsProvider != null) {
          Proxy.sDefaultImpl = param1IControlsProvider;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IControlsProvider getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
