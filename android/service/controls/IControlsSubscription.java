package android.service.controls;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IControlsSubscription extends IInterface {
  void cancel() throws RemoteException;
  
  void request(long paramLong) throws RemoteException;
  
  class Default implements IControlsSubscription {
    public void request(long param1Long) throws RemoteException {}
    
    public void cancel() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IControlsSubscription {
    private static final String DESCRIPTOR = "android.service.controls.IControlsSubscription";
    
    static final int TRANSACTION_cancel = 2;
    
    static final int TRANSACTION_request = 1;
    
    public Stub() {
      attachInterface(this, "android.service.controls.IControlsSubscription");
    }
    
    public static IControlsSubscription asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.controls.IControlsSubscription");
      if (iInterface != null && iInterface instanceof IControlsSubscription)
        return (IControlsSubscription)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "cancel";
      } 
      return "request";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.service.controls.IControlsSubscription");
          return true;
        } 
        param1Parcel1.enforceInterface("android.service.controls.IControlsSubscription");
        cancel();
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.controls.IControlsSubscription");
      long l = param1Parcel1.readLong();
      request(l);
      return true;
    }
    
    private static class Proxy implements IControlsSubscription {
      public static IControlsSubscription sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.controls.IControlsSubscription";
      }
      
      public void request(long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.controls.IControlsSubscription");
          parcel.writeLong(param2Long);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IControlsSubscription.Stub.getDefaultImpl() != null) {
            IControlsSubscription.Stub.getDefaultImpl().request(param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void cancel() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.controls.IControlsSubscription");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IControlsSubscription.Stub.getDefaultImpl() != null) {
            IControlsSubscription.Stub.getDefaultImpl().cancel();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IControlsSubscription param1IControlsSubscription) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IControlsSubscription != null) {
          Proxy.sDefaultImpl = param1IControlsSubscription;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IControlsSubscription getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
