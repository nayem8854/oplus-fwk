package android.service.notification;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.Slog;
import android.util.proto.ProtoOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;
import java.util.UUID;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class ZenModeConfig implements Parcelable {
  private static final String ALLOW_ATT_ALARMS = "alarms";
  
  private static final String ALLOW_ATT_CALLS = "calls";
  
  private static final String ALLOW_ATT_CALLS_FROM = "callsFrom";
  
  private static final String ALLOW_ATT_CONV = "convos";
  
  private static final String ALLOW_ATT_CONV_FROM = "convosFrom";
  
  private static final String ALLOW_ATT_EVENTS = "events";
  
  private static final String ALLOW_ATT_FROM = "from";
  
  private static final String ALLOW_ATT_MEDIA = "media";
  
  private static final String ALLOW_ATT_MESSAGES = "messages";
  
  private static final String ALLOW_ATT_MESSAGES_FROM = "messagesFrom";
  
  private static final String ALLOW_ATT_REMINDERS = "reminders";
  
  private static final String ALLOW_ATT_REPEAT_CALLERS = "repeatCallers";
  
  private static final String ALLOW_ATT_SCREEN_OFF = "visualScreenOff";
  
  private static final String ALLOW_ATT_SCREEN_ON = "visualScreenOn";
  
  private static final String ALLOW_ATT_SYSTEM = "system";
  
  private static final String ALLOW_TAG = "allow";
  
  public static final int[] ALL_DAYS;
  
  private static final String AUTOMATIC_TAG = "automatic";
  
  private static final String CONDITION_ATT_FLAGS = "flags";
  
  private static final String CONDITION_ATT_ICON = "icon";
  
  private static final String CONDITION_ATT_ID = "id";
  
  private static final String CONDITION_ATT_LINE1 = "line1";
  
  private static final String CONDITION_ATT_LINE2 = "line2";
  
  private static final String CONDITION_ATT_STATE = "state";
  
  private static final String CONDITION_ATT_SUMMARY = "summary";
  
  public static final String COUNTDOWN_PATH = "countdown";
  
  public static final Parcelable.Creator<ZenModeConfig> CREATOR;
  
  private static final int DAY_MINUTES = 1440;
  
  private static final boolean DEFAULT_ALLOW_ALARMS = true;
  
  private static final boolean DEFAULT_ALLOW_CALLS = true;
  
  private static final boolean DEFAULT_ALLOW_CONV = false;
  
  private static final int DEFAULT_ALLOW_CONV_FROM = 3;
  
  private static final boolean DEFAULT_ALLOW_EVENTS = false;
  
  private static final boolean DEFAULT_ALLOW_MEDIA = true;
  
  private static final boolean DEFAULT_ALLOW_MESSAGES = false;
  
  private static final boolean DEFAULT_ALLOW_REMINDERS = false;
  
  private static final boolean DEFAULT_ALLOW_REPEAT_CALLERS = true;
  
  private static final boolean DEFAULT_ALLOW_SYSTEM = false;
  
  private static final int DEFAULT_CALLS_SOURCE = 2;
  
  private static final boolean DEFAULT_CHANNELS_BYPASSING_DND = false;
  
  public static final List<String> DEFAULT_RULE_IDS;
  
  private static final int DEFAULT_SOURCE = 1;
  
  private static final int DEFAULT_SUPPRESSED_VISUAL_EFFECTS = 0;
  
  private static final String DISALLOW_ATT_VISUAL_EFFECTS = "visualEffects";
  
  private static final String DISALLOW_TAG = "disallow";
  
  public static final String EVENTS_DEFAULT_RULE_ID = "EVENTS_DEFAULT_RULE";
  
  public static final String EVENT_PATH = "event";
  
  public static final String EVERY_NIGHT_DEFAULT_RULE_ID = "EVERY_NIGHT_DEFAULT_RULE";
  
  public static final String IS_ALARM_PATH = "alarm";
  
  public static final String MANUAL_RULE_ID = "MANUAL_RULE";
  
  private static final String MANUAL_TAG = "manual";
  
  public static final int MAX_SOURCE = 2;
  
  private static final int MINUTES_MS = 60000;
  
  public static final int[] MINUTE_BUCKETS;
  
  private static final String RULE_ATT_COMPONENT = "component";
  
  private static final String RULE_ATT_CONDITION_ID = "conditionId";
  
  private static final String RULE_ATT_CONFIG_ACTIVITY = "configActivity";
  
  private static final String RULE_ATT_CREATION_TIME = "creationTime";
  
  private static final String RULE_ATT_ENABLED = "enabled";
  
  private static final String RULE_ATT_ENABLER = "enabler";
  
  private static final String RULE_ATT_ID = "ruleId";
  
  private static final String RULE_ATT_MODIFIED = "modified";
  
  private static final String RULE_ATT_NAME = "name";
  
  private static final String RULE_ATT_SNOOZING = "snoozing";
  
  private static final String RULE_ATT_ZEN = "zen";
  
  public static final String SCHEDULE_PATH = "schedule";
  
  private static final int SECONDS_MS = 1000;
  
  private static final String SHOW_ATT_AMBIENT = "showAmbient";
  
  private static final String SHOW_ATT_BADGES = "showBadges";
  
  private static final String SHOW_ATT_FULL_SCREEN_INTENT = "showFullScreenIntent";
  
  private static final String SHOW_ATT_LIGHTS = "showLights";
  
  private static final String SHOW_ATT_NOTIFICATION_LIST = "showNotificationList";
  
  private static final String SHOW_ATT_PEEK = "shoePeek";
  
  private static final String SHOW_ATT_STATUS_BAR_ICONS = "showStatusBarIcons";
  
  public static final int SOURCE_ANYONE = 0;
  
  public static final int SOURCE_CONTACT = 1;
  
  public static final int SOURCE_STAR = 2;
  
  private static final String STATE_ATT_CHANNELS_BYPASSING_DND = "areChannelsBypassingDnd";
  
  private static final String STATE_TAG = "state";
  
  public static final String SYSTEM_AUTHORITY = "android";
  
  private static String TAG = "ZenModeConfig";
  
  public static final int XML_VERSION = 8;
  
  private static final String ZEN_ATT_USER = "user";
  
  private static final String ZEN_ATT_VERSION = "version";
  
  private static final String ZEN_POLICY_TAG = "zen_policy";
  
  public static final String ZEN_TAG = "zen";
  
  private static final int ZERO_VALUE_MS = 10000;
  
  public boolean allowAlarms;
  
  public boolean allowCalls;
  
  public int allowCallsFrom;
  
  public boolean allowConversations;
  
  public int allowConversationsFrom;
  
  public boolean allowEvents;
  
  public boolean allowMedia;
  
  public boolean allowMessages;
  
  public int allowMessagesFrom;
  
  public boolean allowReminders;
  
  public boolean allowRepeatCallers;
  
  public boolean allowSystem;
  
  public boolean areChannelsBypassingDnd;
  
  public ArrayMap<String, ZenRule> automaticRules;
  
  public ZenRule manualRule;
  
  public int suppressedVisualEffects;
  
  public int user;
  
  public int version;
  
  static {
    DEFAULT_RULE_IDS = Arrays.asList(new String[] { "EVERY_NIGHT_DEFAULT_RULE", "EVENTS_DEFAULT_RULE" });
    ALL_DAYS = new int[] { 1, 2, 3, 4, 5, 6, 7 };
    MINUTE_BUCKETS = generateMinuteBuckets();
    CREATOR = new Parcelable.Creator<ZenModeConfig>() {
        public ZenModeConfig createFromParcel(Parcel param1Parcel) {
          return new ZenModeConfig(param1Parcel);
        }
        
        public ZenModeConfig[] newArray(int param1Int) {
          return new ZenModeConfig[param1Int];
        }
      };
  }
  
  public ZenModeConfig() {
    this.allowAlarms = true;
    this.allowMedia = true;
    this.allowSystem = false;
    this.allowCalls = true;
    this.allowRepeatCallers = true;
    this.allowMessages = false;
    this.allowReminders = false;
    this.allowEvents = false;
    this.allowCallsFrom = 2;
    this.allowMessagesFrom = 1;
    this.allowConversations = false;
    this.allowConversationsFrom = 3;
    this.user = 0;
    this.suppressedVisualEffects = 0;
    this.areChannelsBypassingDnd = false;
    this.automaticRules = new ArrayMap();
  }
  
  public ZenModeConfig(Parcel paramParcel) {
    boolean bool2, bool1 = true;
    this.allowAlarms = true;
    this.allowMedia = true;
    this.allowSystem = false;
    this.allowCalls = true;
    this.allowRepeatCallers = true;
    this.allowMessages = false;
    this.allowReminders = false;
    this.allowEvents = false;
    this.allowCallsFrom = 2;
    this.allowMessagesFrom = 1;
    this.allowConversations = false;
    this.allowConversationsFrom = 3;
    this.user = 0;
    this.suppressedVisualEffects = 0;
    this.areChannelsBypassingDnd = false;
    this.automaticRules = new ArrayMap();
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.allowCalls = bool2;
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.allowRepeatCallers = bool2;
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.allowMessages = bool2;
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.allowReminders = bool2;
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.allowEvents = bool2;
    this.allowCallsFrom = paramParcel.readInt();
    this.allowMessagesFrom = paramParcel.readInt();
    this.user = paramParcel.readInt();
    this.manualRule = paramParcel.<ZenRule>readParcelable(null);
    int i = paramParcel.readInt();
    if (i > 0) {
      String[] arrayOfString = new String[i];
      ZenRule[] arrayOfZenRule = new ZenRule[i];
      paramParcel.readStringArray(arrayOfString);
      paramParcel.readTypedArray(arrayOfZenRule, ZenRule.CREATOR);
      for (byte b = 0; b < i; b++)
        this.automaticRules.put(arrayOfString[b], arrayOfZenRule[b]); 
    } 
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.allowAlarms = bool2;
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.allowMedia = bool2;
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.allowSystem = bool2;
    this.suppressedVisualEffects = paramParcel.readInt();
    if (paramParcel.readInt() == 1) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.areChannelsBypassingDnd = bool2;
    this.allowConversations = paramParcel.readBoolean();
    this.allowConversationsFrom = paramParcel.readInt();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.allowCalls);
    paramParcel.writeInt(this.allowRepeatCallers);
    paramParcel.writeInt(this.allowMessages);
    paramParcel.writeInt(this.allowReminders);
    paramParcel.writeInt(this.allowEvents);
    paramParcel.writeInt(this.allowCallsFrom);
    paramParcel.writeInt(this.allowMessagesFrom);
    paramParcel.writeInt(this.user);
    paramParcel.writeParcelable(this.manualRule, 0);
    if (!this.automaticRules.isEmpty()) {
      int i = this.automaticRules.size();
      String[] arrayOfString = new String[i];
      ZenRule[] arrayOfZenRule = new ZenRule[i];
      for (paramInt = 0; paramInt < i; paramInt++) {
        arrayOfString[paramInt] = (String)this.automaticRules.keyAt(paramInt);
        arrayOfZenRule[paramInt] = (ZenRule)this.automaticRules.valueAt(paramInt);
      } 
      paramParcel.writeInt(i);
      paramParcel.writeStringArray(arrayOfString);
      paramParcel.writeTypedArray(arrayOfZenRule, 0);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeInt(this.allowAlarms);
    paramParcel.writeInt(this.allowMedia);
    paramParcel.writeInt(this.allowSystem);
    paramParcel.writeInt(this.suppressedVisualEffects);
    paramParcel.writeInt(this.areChannelsBypassingDnd);
    paramParcel.writeBoolean(this.allowConversations);
    paramParcel.writeInt(this.allowConversationsFrom);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(ZenModeConfig.class.getSimpleName());
    stringBuilder.append('[');
    stringBuilder.append("user=");
    stringBuilder.append(this.user);
    stringBuilder.append(",allowAlarms=");
    stringBuilder.append(this.allowAlarms);
    stringBuilder.append(",allowMedia=");
    stringBuilder.append(this.allowMedia);
    stringBuilder.append(",allowSystem=");
    stringBuilder.append(this.allowSystem);
    stringBuilder.append(",allowReminders=");
    stringBuilder.append(this.allowReminders);
    stringBuilder.append(",allowEvents=");
    stringBuilder.append(this.allowEvents);
    stringBuilder.append(",allowCalls=");
    stringBuilder.append(this.allowCalls);
    stringBuilder.append(",allowRepeatCallers=");
    stringBuilder.append(this.allowRepeatCallers);
    stringBuilder.append(",allowMessages=");
    stringBuilder.append(this.allowMessages);
    stringBuilder.append(",allowConversations=");
    stringBuilder.append(this.allowConversations);
    stringBuilder.append(",allowCallsFrom=");
    stringBuilder.append(sourceToString(this.allowCallsFrom));
    stringBuilder.append(",allowMessagesFrom=");
    stringBuilder.append(sourceToString(this.allowMessagesFrom));
    stringBuilder.append(",allowConvFrom=");
    int i = this.allowConversationsFrom;
    String str = ZenPolicy.conversationTypeToString(i);
    stringBuilder.append(str);
    stringBuilder.append(",suppressedVisualEffects=");
    stringBuilder.append(this.suppressedVisualEffects);
    stringBuilder.append(",areChannelsBypassingDnd=");
    stringBuilder.append(this.areChannelsBypassingDnd);
    stringBuilder.append(",\nautomaticRules=");
    stringBuilder.append(rulesToString());
    stringBuilder.append(",\nmanualRule=");
    stringBuilder.append(this.manualRule);
    stringBuilder.append(']');
    return stringBuilder.toString();
  }
  
  private String rulesToString() {
    if (this.automaticRules.isEmpty())
      return "{}"; 
    StringBuilder stringBuilder = new StringBuilder(this.automaticRules.size() * 28);
    stringBuilder.append('{');
    for (byte b = 0; b < this.automaticRules.size(); b++) {
      if (b > 0)
        stringBuilder.append(",\n"); 
      Object object = this.automaticRules.valueAt(b);
      stringBuilder.append(object);
    } 
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public Diff diff(ZenModeConfig paramZenModeConfig) {
    Diff diff = new Diff();
    if (paramZenModeConfig == null)
      return diff.addLine("config", "delete"); 
    int i = this.user;
    if (i != paramZenModeConfig.user)
      diff.addLine("user", Integer.valueOf(i), Integer.valueOf(paramZenModeConfig.user)); 
    boolean bool = this.allowAlarms;
    if (bool != paramZenModeConfig.allowAlarms)
      diff.addLine("allowAlarms", Boolean.valueOf(bool), Boolean.valueOf(paramZenModeConfig.allowAlarms)); 
    bool = this.allowMedia;
    if (bool != paramZenModeConfig.allowMedia)
      diff.addLine("allowMedia", Boolean.valueOf(bool), Boolean.valueOf(paramZenModeConfig.allowMedia)); 
    bool = this.allowSystem;
    if (bool != paramZenModeConfig.allowSystem)
      diff.addLine("allowSystem", Boolean.valueOf(bool), Boolean.valueOf(paramZenModeConfig.allowSystem)); 
    bool = this.allowCalls;
    if (bool != paramZenModeConfig.allowCalls)
      diff.addLine("allowCalls", Boolean.valueOf(bool), Boolean.valueOf(paramZenModeConfig.allowCalls)); 
    bool = this.allowReminders;
    if (bool != paramZenModeConfig.allowReminders)
      diff.addLine("allowReminders", Boolean.valueOf(bool), Boolean.valueOf(paramZenModeConfig.allowReminders)); 
    bool = this.allowEvents;
    if (bool != paramZenModeConfig.allowEvents)
      diff.addLine("allowEvents", Boolean.valueOf(bool), Boolean.valueOf(paramZenModeConfig.allowEvents)); 
    bool = this.allowRepeatCallers;
    if (bool != paramZenModeConfig.allowRepeatCallers)
      diff.addLine("allowRepeatCallers", Boolean.valueOf(bool), Boolean.valueOf(paramZenModeConfig.allowRepeatCallers)); 
    bool = this.allowMessages;
    if (bool != paramZenModeConfig.allowMessages)
      diff.addLine("allowMessages", Boolean.valueOf(bool), Boolean.valueOf(paramZenModeConfig.allowMessages)); 
    i = this.allowCallsFrom;
    if (i != paramZenModeConfig.allowCallsFrom)
      diff.addLine("allowCallsFrom", Integer.valueOf(i), Integer.valueOf(paramZenModeConfig.allowCallsFrom)); 
    i = this.allowMessagesFrom;
    if (i != paramZenModeConfig.allowMessagesFrom)
      diff.addLine("allowMessagesFrom", Integer.valueOf(i), Integer.valueOf(paramZenModeConfig.allowMessagesFrom)); 
    i = this.suppressedVisualEffects;
    if (i != paramZenModeConfig.suppressedVisualEffects) {
      int k = paramZenModeConfig.suppressedVisualEffects;
      diff.addLine("suppressedVisualEffects", Integer.valueOf(i), Integer.valueOf(k));
    } 
    ArraySet<String> arraySet = new ArraySet();
    addKeys(arraySet, this.automaticRules);
    addKeys(arraySet, paramZenModeConfig.automaticRules);
    int j = arraySet.size();
    for (i = 0; i < j; i++) {
      String str = (String)arraySet.valueAt(i);
      ArrayMap<String, ZenRule> arrayMap1 = this.automaticRules;
      ZenRule zenRule = null;
      if (arrayMap1 != null) {
        ZenRule zenRule1 = (ZenRule)arrayMap1.get(str);
      } else {
        arrayMap1 = null;
      } 
      ArrayMap<String, ZenRule> arrayMap2 = paramZenModeConfig.automaticRules;
      if (arrayMap2 != null)
        zenRule = (ZenRule)arrayMap2.get(str); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("automaticRule[");
      stringBuilder.append(str);
      stringBuilder.append("]");
      ZenRule.appendDiff(diff, stringBuilder.toString(), (ZenRule)arrayMap1, zenRule);
    } 
    ZenRule.appendDiff(diff, "manualRule", this.manualRule, paramZenModeConfig.manualRule);
    bool = this.areChannelsBypassingDnd;
    if (bool != paramZenModeConfig.areChannelsBypassingDnd) {
      boolean bool1 = paramZenModeConfig.areChannelsBypassingDnd;
      diff.addLine("areChannelsBypassingDnd", Boolean.valueOf(bool), Boolean.valueOf(bool1));
    } 
    return diff;
  }
  
  public static Diff diff(ZenModeConfig paramZenModeConfig1, ZenModeConfig paramZenModeConfig2) {
    Diff diff;
    if (paramZenModeConfig1 == null) {
      diff = new Diff();
      if (paramZenModeConfig2 != null)
        diff.addLine("config", "insert"); 
      return diff;
    } 
    return diff.diff(paramZenModeConfig2);
  }
  
  private static <T> void addKeys(ArraySet<T> paramArraySet, ArrayMap<T, ?> paramArrayMap) {
    if (paramArrayMap != null)
      for (byte b = 0; b < paramArrayMap.size(); b++)
        paramArraySet.add(paramArrayMap.keyAt(b));  
  }
  
  public boolean isValid() {
    if (!isValidManualRule(this.manualRule))
      return false; 
    int i = this.automaticRules.size();
    for (byte b = 0; b < i; b++) {
      if (!isValidAutomaticRule((ZenRule)this.automaticRules.valueAt(b)))
        return false; 
    } 
    return true;
  }
  
  private static boolean isValidManualRule(ZenRule paramZenRule) {
    return (paramZenRule == null || (Settings.Global.isValidZenMode(paramZenRule.zenMode) && sameCondition(paramZenRule)));
  }
  
  private static boolean isValidAutomaticRule(ZenRule paramZenRule) {
    boolean bool;
    if (paramZenRule != null && !TextUtils.isEmpty(paramZenRule.name) && Settings.Global.isValidZenMode(paramZenRule.zenMode) && paramZenRule.conditionId != null && sameCondition(paramZenRule)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static boolean sameCondition(ZenRule paramZenRule) {
    boolean bool = false;
    null = false;
    if (paramZenRule == null)
      return false; 
    if (paramZenRule.conditionId == null) {
      if (paramZenRule.condition == null)
        null = true; 
      return null;
    } 
    if (paramZenRule.condition != null) {
      null = bool;
      return paramZenRule.conditionId.equals(paramZenRule.condition.id) ? true : null;
    } 
    return true;
  }
  
  private static int[] generateMinuteBuckets() {
    int[] arrayOfInt = new int[15];
    arrayOfInt[0] = 15;
    arrayOfInt[1] = 30;
    arrayOfInt[2] = 45;
    for (byte b = 1; b <= 12; b++)
      arrayOfInt[b + 2] = b * 60; 
    return arrayOfInt;
  }
  
  public static String sourceToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2)
          return "UNKNOWN"; 
        return "stars";
      } 
      return "contacts";
    } 
    return "anyone";
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof ZenModeConfig;
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (paramObject == this)
      return true; 
    paramObject = paramObject;
    if (((ZenModeConfig)paramObject).allowAlarms == this.allowAlarms && ((ZenModeConfig)paramObject).allowMedia == this.allowMedia && ((ZenModeConfig)paramObject).allowSystem == this.allowSystem && ((ZenModeConfig)paramObject).allowCalls == this.allowCalls && ((ZenModeConfig)paramObject).allowRepeatCallers == this.allowRepeatCallers && ((ZenModeConfig)paramObject).allowMessages == this.allowMessages && ((ZenModeConfig)paramObject).allowCallsFrom == this.allowCallsFrom && ((ZenModeConfig)paramObject).allowMessagesFrom == this.allowMessagesFrom && ((ZenModeConfig)paramObject).allowReminders == this.allowReminders && ((ZenModeConfig)paramObject).allowEvents == this.allowEvents && ((ZenModeConfig)paramObject).user == this.user) {
      ArrayMap<String, ZenRule> arrayMap1 = ((ZenModeConfig)paramObject).automaticRules, arrayMap2 = this.automaticRules;
      if (Objects.equals(arrayMap1, arrayMap2)) {
        ZenRule zenRule1 = ((ZenModeConfig)paramObject).manualRule, zenRule2 = this.manualRule;
        if (Objects.equals(zenRule1, zenRule2) && ((ZenModeConfig)paramObject).suppressedVisualEffects == this.suppressedVisualEffects && ((ZenModeConfig)paramObject).areChannelsBypassingDnd == this.areChannelsBypassingDnd && ((ZenModeConfig)paramObject).allowConversations == this.allowConversations && ((ZenModeConfig)paramObject).allowConversationsFrom == this.allowConversationsFrom)
          bool1 = true; 
      } 
    } 
    return bool1;
  }
  
  public int hashCode() {
    boolean bool1 = this.allowAlarms, bool2 = this.allowMedia, bool3 = this.allowSystem, bool4 = this.allowCalls, bool5 = this.allowRepeatCallers;
    boolean bool6 = this.allowMessages;
    int i = this.allowCallsFrom;
    int j = this.allowMessagesFrom;
    boolean bool7 = this.allowReminders, bool8 = this.allowEvents;
    int k = this.user;
    ArrayMap<String, ZenRule> arrayMap = this.automaticRules;
    ZenRule zenRule = this.manualRule;
    int m = this.suppressedVisualEffects;
    boolean bool9 = this.areChannelsBypassingDnd, bool10 = this.allowConversations;
    int n = this.allowConversationsFrom;
    return Objects.hash(new Object[] { 
          Boolean.valueOf(bool1), Boolean.valueOf(bool2), Boolean.valueOf(bool3), Boolean.valueOf(bool4), Boolean.valueOf(bool5), Boolean.valueOf(bool6), Integer.valueOf(i), Integer.valueOf(j), Boolean.valueOf(bool7), Boolean.valueOf(bool8), 
          Integer.valueOf(k), arrayMap, zenRule, Integer.valueOf(m), Boolean.valueOf(bool9), Boolean.valueOf(bool10), Integer.valueOf(n) });
  }
  
  private static String toDayList(int[] paramArrayOfint) {
    if (paramArrayOfint == null || paramArrayOfint.length == 0)
      return ""; 
    StringBuilder stringBuilder = new StringBuilder();
    for (byte b = 0; b < paramArrayOfint.length; b++) {
      if (b > 0)
        stringBuilder.append('.'); 
      stringBuilder.append(paramArrayOfint[b]);
    } 
    return stringBuilder.toString();
  }
  
  private static int[] tryParseDayList(String paramString1, String paramString2) {
    if (paramString1 == null)
      return null; 
    String[] arrayOfString = paramString1.split(paramString2);
    if (arrayOfString.length == 0)
      return null; 
    int[] arrayOfInt = new int[arrayOfString.length];
    for (byte b = 0; b < arrayOfString.length; b++) {
      int i = tryParseInt(arrayOfString[b], -1);
      if (i == -1)
        return null; 
      arrayOfInt[b] = i;
    } 
    return arrayOfInt;
  }
  
  private static int tryParseInt(String paramString, int paramInt) {
    if (TextUtils.isEmpty(paramString))
      return paramInt; 
    try {
      return Integer.parseInt(paramString);
    } catch (NumberFormatException numberFormatException) {
      return paramInt;
    } 
  }
  
  private static long tryParseLong(String paramString, long paramLong) {
    if (TextUtils.isEmpty(paramString))
      return paramLong; 
    try {
      return Long.parseLong(paramString);
    } catch (NumberFormatException numberFormatException) {
      return paramLong;
    } 
  }
  
  private static Long tryParseLong(String paramString, Long paramLong) {
    if (TextUtils.isEmpty(paramString))
      return paramLong; 
    try {
      long l = Long.parseLong(paramString);
      return Long.valueOf(l);
    } catch (NumberFormatException numberFormatException) {
      return paramLong;
    } 
  }
  
  public static ZenModeConfig readXml(XmlPullParser paramXmlPullParser) throws XmlPullParserException, IOException {
    int i = paramXmlPullParser.getEventType();
    if (i != 2)
      return null; 
    String str = paramXmlPullParser.getName();
    if (!"zen".equals(str))
      return null; 
    ZenModeConfig zenModeConfig = new ZenModeConfig();
    zenModeConfig.version = safeInt(paramXmlPullParser, "version", 8);
    zenModeConfig.user = safeInt(paramXmlPullParser, "user", zenModeConfig.user);
    i = 0;
    while (true) {
      int j = paramXmlPullParser.next();
      if (j != 1) {
        String str1 = paramXmlPullParser.getName();
        if (j == 3 && "zen".equals(str1))
          return zenModeConfig; 
        if (j == 2) {
          String str2;
          if ("allow".equals(str1)) {
            zenModeConfig.allowCalls = safeBoolean(paramXmlPullParser, "calls", true);
            zenModeConfig.allowRepeatCallers = safeBoolean(paramXmlPullParser, "repeatCallers", true);
            zenModeConfig.allowMessages = safeBoolean(paramXmlPullParser, "messages", false);
            zenModeConfig.allowReminders = safeBoolean(paramXmlPullParser, "reminders", false);
            zenModeConfig.allowConversations = safeBoolean(paramXmlPullParser, "convos", false);
            zenModeConfig.allowEvents = safeBoolean(paramXmlPullParser, "events", false);
            int k = safeInt(paramXmlPullParser, "from", -1);
            int m = safeInt(paramXmlPullParser, "callsFrom", -1);
            j = safeInt(paramXmlPullParser, "messagesFrom", -1);
            zenModeConfig.allowConversationsFrom = safeInt(paramXmlPullParser, "convosFrom", 3);
            if (isValidSource(m) && isValidSource(j)) {
              zenModeConfig.allowCallsFrom = m;
              zenModeConfig.allowMessagesFrom = j;
            } else if (isValidSource(k)) {
              str1 = TAG;
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Migrating existing shared 'from': ");
              stringBuilder.append(sourceToString(k));
              Slog.i(str1, stringBuilder.toString());
              zenModeConfig.allowCallsFrom = k;
              zenModeConfig.allowMessagesFrom = k;
            } else {
              zenModeConfig.allowCallsFrom = 2;
              zenModeConfig.allowMessagesFrom = 1;
            } 
            zenModeConfig.allowAlarms = safeBoolean(paramXmlPullParser, "alarms", true);
            zenModeConfig.allowMedia = safeBoolean(paramXmlPullParser, "media", true);
            zenModeConfig.allowSystem = safeBoolean(paramXmlPullParser, "system", false);
            Boolean bool = unsafeBoolean(paramXmlPullParser, "visualScreenOff");
            if (bool != null) {
              j = 1;
              i = j;
              if (!bool.booleanValue()) {
                zenModeConfig.suppressedVisualEffects |= 0xC;
                i = j;
              } 
            } 
            bool = unsafeBoolean(paramXmlPullParser, "visualScreenOn");
            if (bool != null) {
              j = 1;
              i = j;
              if (!bool.booleanValue()) {
                zenModeConfig.suppressedVisualEffects |= 0x10;
                i = j;
              } 
            } 
            if (i != 0) {
              str2 = TAG;
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Migrated visual effects to ");
              stringBuilder.append(zenModeConfig.suppressedVisualEffects);
              Slog.d(str2, stringBuilder.toString());
            } 
            continue;
          } 
          if ("disallow".equals(str2) && i == 0) {
            zenModeConfig.suppressedVisualEffects = safeInt(paramXmlPullParser, "visualEffects", 0);
            continue;
          } 
          if ("manual".equals(str2)) {
            zenModeConfig.manualRule = readRuleXml(paramXmlPullParser);
            continue;
          } 
          if ("automatic".equals(str2)) {
            str2 = paramXmlPullParser.getAttributeValue(null, "ruleId");
            ZenRule zenRule = readRuleXml(paramXmlPullParser);
            if (str2 != null && zenRule != null) {
              zenRule.id = str2;
              zenModeConfig.automaticRules.put(str2, zenRule);
            } 
            continue;
          } 
          if ("state".equals(str2))
            zenModeConfig.areChannelsBypassingDnd = safeBoolean(paramXmlPullParser, "areChannelsBypassingDnd", false); 
        } 
        continue;
      } 
      break;
    } 
    throw new IllegalStateException("Failed to reach END_DOCUMENT");
  }
  
  public void writeXml(XmlSerializer paramXmlSerializer, Integer paramInteger) throws IOException {
    paramXmlSerializer.startTag(null, "zen");
    if (paramInteger == null) {
      i = 8;
    } else {
      i = paramInteger.intValue();
    } 
    String str = Integer.toString(i);
    paramXmlSerializer.attribute(null, "version", str);
    paramXmlSerializer.attribute(null, "user", Integer.toString(this.user));
    paramXmlSerializer.startTag(null, "allow");
    paramXmlSerializer.attribute(null, "calls", Boolean.toString(this.allowCalls));
    paramXmlSerializer.attribute(null, "repeatCallers", Boolean.toString(this.allowRepeatCallers));
    paramXmlSerializer.attribute(null, "messages", Boolean.toString(this.allowMessages));
    paramXmlSerializer.attribute(null, "reminders", Boolean.toString(this.allowReminders));
    paramXmlSerializer.attribute(null, "events", Boolean.toString(this.allowEvents));
    paramXmlSerializer.attribute(null, "callsFrom", Integer.toString(this.allowCallsFrom));
    paramXmlSerializer.attribute(null, "messagesFrom", Integer.toString(this.allowMessagesFrom));
    paramXmlSerializer.attribute(null, "alarms", Boolean.toString(this.allowAlarms));
    paramXmlSerializer.attribute(null, "media", Boolean.toString(this.allowMedia));
    paramXmlSerializer.attribute(null, "system", Boolean.toString(this.allowSystem));
    paramXmlSerializer.attribute(null, "convos", Boolean.toString(this.allowConversations));
    paramXmlSerializer.attribute(null, "convosFrom", Integer.toString(this.allowConversationsFrom));
    paramXmlSerializer.endTag(null, "allow");
    paramXmlSerializer.startTag(null, "disallow");
    paramXmlSerializer.attribute(null, "visualEffects", Integer.toString(this.suppressedVisualEffects));
    paramXmlSerializer.endTag(null, "disallow");
    if (this.manualRule != null) {
      paramXmlSerializer.startTag(null, "manual");
      writeRuleXml(this.manualRule, paramXmlSerializer);
      paramXmlSerializer.endTag(null, "manual");
    } 
    int j = this.automaticRules.size();
    for (int i = 0; i < j; i++) {
      str = (String)this.automaticRules.keyAt(i);
      ZenRule zenRule = (ZenRule)this.automaticRules.valueAt(i);
      paramXmlSerializer.startTag(null, "automatic");
      paramXmlSerializer.attribute(null, "ruleId", str);
      writeRuleXml(zenRule, paramXmlSerializer);
      paramXmlSerializer.endTag(null, "automatic");
    } 
    paramXmlSerializer.startTag(null, "state");
    boolean bool = this.areChannelsBypassingDnd;
    str = Boolean.toString(bool);
    paramXmlSerializer.attribute(null, "areChannelsBypassingDnd", str);
    paramXmlSerializer.endTag(null, "state");
    paramXmlSerializer.endTag(null, "zen");
  }
  
  public static ZenRule readRuleXml(XmlPullParser paramXmlPullParser) {
    StringBuilder stringBuilder;
    String str1;
    ZenRule zenRule = new ZenRule();
    zenRule.enabled = safeBoolean(paramXmlPullParser, "enabled", true);
    zenRule.name = paramXmlPullParser.getAttributeValue(null, "name");
    String str2 = paramXmlPullParser.getAttributeValue(null, "zen");
    zenRule.zenMode = tryParseZenMode(str2, -1);
    if (zenRule.zenMode == -1) {
      str1 = TAG;
      stringBuilder = new StringBuilder();
      stringBuilder.append("Bad zen mode in rule xml:");
      stringBuilder.append(str2);
      Slog.w(str1, stringBuilder.toString());
      return null;
    } 
    ((ZenRule)str1).conditionId = safeUri((XmlPullParser)stringBuilder, "conditionId");
    ((ZenRule)str1).component = safeComponentName((XmlPullParser)stringBuilder, "component");
    ((ZenRule)str1).configurationActivity = safeComponentName((XmlPullParser)stringBuilder, "configActivity");
    if (((ZenRule)str1).component != null) {
      str2 = ((ZenRule)str1).component.getPackageName();
    } else if (((ZenRule)str1).configurationActivity != null) {
      str2 = ((ZenRule)str1).configurationActivity.getPackageName();
    } else {
      str2 = null;
    } 
    ((ZenRule)str1).pkg = str2;
    ((ZenRule)str1).creationTime = safeLong((XmlPullParser)stringBuilder, "creationTime", 0L);
    ((ZenRule)str1).enabler = stringBuilder.getAttributeValue(null, "enabler");
    ((ZenRule)str1).condition = readConditionXml((XmlPullParser)stringBuilder);
    if (((ZenRule)str1).zenMode != 1) {
      Uri uri = ((ZenRule)str1).conditionId;
      if (Condition.isValidId(uri, "android")) {
        String str = TAG;
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Updating zenMode of automatic rule ");
        stringBuilder1.append(((ZenRule)str1).name);
        Slog.i(str, stringBuilder1.toString());
        ((ZenRule)str1).zenMode = 1;
      } 
    } 
    ((ZenRule)str1).modified = safeBoolean((XmlPullParser)stringBuilder, "modified", false);
    ((ZenRule)str1).zenPolicy = readZenPolicyXml((XmlPullParser)stringBuilder);
    return (ZenRule)str1;
  }
  
  public static void writeRuleXml(ZenRule paramZenRule, XmlSerializer paramXmlSerializer) throws IOException {
    paramXmlSerializer.attribute(null, "enabled", Boolean.toString(paramZenRule.enabled));
    if (paramZenRule.name != null)
      paramXmlSerializer.attribute(null, "name", paramZenRule.name); 
    paramXmlSerializer.attribute(null, "zen", Integer.toString(paramZenRule.zenMode));
    if (paramZenRule.component != null)
      paramXmlSerializer.attribute(null, "component", paramZenRule.component.flattenToString()); 
    if (paramZenRule.configurationActivity != null) {
      ComponentName componentName = paramZenRule.configurationActivity;
      String str = componentName.flattenToString();
      paramXmlSerializer.attribute(null, "configActivity", str);
    } 
    if (paramZenRule.conditionId != null)
      paramXmlSerializer.attribute(null, "conditionId", paramZenRule.conditionId.toString()); 
    paramXmlSerializer.attribute(null, "creationTime", Long.toString(paramZenRule.creationTime));
    if (paramZenRule.enabler != null)
      paramXmlSerializer.attribute(null, "enabler", paramZenRule.enabler); 
    if (paramZenRule.condition != null)
      writeConditionXml(paramZenRule.condition, paramXmlSerializer); 
    if (paramZenRule.zenPolicy != null)
      writeZenPolicyXml(paramZenRule.zenPolicy, paramXmlSerializer); 
    paramXmlSerializer.attribute(null, "modified", Boolean.toString(paramZenRule.modified));
  }
  
  public static Condition readConditionXml(XmlPullParser paramXmlPullParser) {
    Uri uri = safeUri(paramXmlPullParser, "id");
    if (uri == null)
      return null; 
    String str1 = paramXmlPullParser.getAttributeValue(null, "summary");
    String str2 = paramXmlPullParser.getAttributeValue(null, "line1");
    String str3 = paramXmlPullParser.getAttributeValue(null, "line2");
    int i = safeInt(paramXmlPullParser, "icon", -1);
    int j = safeInt(paramXmlPullParser, "state", -1);
    int k = safeInt(paramXmlPullParser, "flags", -1);
    try {
      return new Condition(uri, str1, str2, str3, i, j, k);
    } catch (IllegalArgumentException illegalArgumentException) {
      Slog.w(TAG, "Unable to read condition xml", illegalArgumentException);
      return null;
    } 
  }
  
  public static void writeConditionXml(Condition paramCondition, XmlSerializer paramXmlSerializer) throws IOException {
    paramXmlSerializer.attribute(null, "id", paramCondition.id.toString());
    paramXmlSerializer.attribute(null, "summary", paramCondition.summary);
    paramXmlSerializer.attribute(null, "line1", paramCondition.line1);
    paramXmlSerializer.attribute(null, "line2", paramCondition.line2);
    paramXmlSerializer.attribute(null, "icon", Integer.toString(paramCondition.icon));
    paramXmlSerializer.attribute(null, "state", Integer.toString(paramCondition.state));
    paramXmlSerializer.attribute(null, "flags", Integer.toString(paramCondition.flags));
  }
  
  public static ZenPolicy readZenPolicyXml(XmlPullParser paramXmlPullParser) {
    boolean bool = false;
    ZenPolicy.Builder builder = new ZenPolicy.Builder();
    int i = safeInt(paramXmlPullParser, "callsFrom", 0);
    int j = safeInt(paramXmlPullParser, "messagesFrom", 0);
    int k = safeInt(paramXmlPullParser, "repeatCallers", 0);
    int m = safeInt(paramXmlPullParser, "alarms", 0);
    int n = safeInt(paramXmlPullParser, "media", 0);
    int i1 = safeInt(paramXmlPullParser, "system", 0);
    int i2 = safeInt(paramXmlPullParser, "events", 0);
    int i3 = safeInt(paramXmlPullParser, "reminders", 0);
    if (i != 0) {
      builder.allowCalls(i);
      bool = true;
    } 
    if (j != 0) {
      builder.allowMessages(j);
      bool = true;
    } 
    if (k != 0) {
      boolean bool1;
      if (k == 1) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      builder.allowRepeatCallers(bool1);
      bool = true;
    } 
    if (m != 0) {
      boolean bool1;
      if (m == 1) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      builder.allowAlarms(bool1);
      bool = true;
    } 
    if (n != 0) {
      boolean bool1;
      if (n == 1) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      builder.allowMedia(bool1);
      bool = true;
    } 
    if (i1 != 0) {
      boolean bool1;
      if (i1 == 1) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      builder.allowSystem(bool1);
      bool = true;
    } 
    if (i2 != 0) {
      boolean bool1;
      if (i2 == 1) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      builder.allowEvents(bool1);
      bool = true;
    } 
    if (i3 != 0) {
      boolean bool1;
      if (i3 == 1) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      builder.allowReminders(bool1);
      bool = true;
    } 
    j = safeInt(paramXmlPullParser, "showFullScreenIntent", 0);
    k = safeInt(paramXmlPullParser, "showLights", 0);
    m = safeInt(paramXmlPullParser, "shoePeek", 0);
    n = safeInt(paramXmlPullParser, "showStatusBarIcons", 0);
    i1 = safeInt(paramXmlPullParser, "showBadges", 0);
    i2 = safeInt(paramXmlPullParser, "showAmbient", 0);
    i3 = safeInt(paramXmlPullParser, "showNotificationList", 0);
    if (j != 0) {
      boolean bool1;
      if (j == 1) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      builder.showFullScreenIntent(bool1);
      bool = true;
    } 
    if (k != 0) {
      boolean bool1;
      if (k == 1) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      builder.showLights(bool1);
      bool = true;
    } 
    if (m != 0) {
      boolean bool1;
      if (m == 1) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      builder.showPeeking(bool1);
      bool = true;
    } 
    if (n != 0) {
      boolean bool1;
      if (n == 1) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      builder.showStatusBarIcons(bool1);
      bool = true;
    } 
    if (i1 != 0) {
      boolean bool1;
      if (i1 == 1) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      builder.showBadges(bool1);
      bool = true;
    } 
    if (i2 != 0) {
      boolean bool1;
      if (i2 == 1) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      builder.showInAmbientDisplay(bool1);
      bool = true;
    } 
    if (i3 != 0) {
      boolean bool1 = true;
      if (i3 != 1)
        bool1 = false; 
      builder.showInNotificationList(bool1);
      bool = true;
    } 
    if (bool)
      return builder.build(); 
    return null;
  }
  
  public static void writeZenPolicyXml(ZenPolicy paramZenPolicy, XmlSerializer paramXmlSerializer) throws IOException {
    writeZenPolicyState("callsFrom", paramZenPolicy.getPriorityCallSenders(), paramXmlSerializer);
    writeZenPolicyState("messagesFrom", paramZenPolicy.getPriorityMessageSenders(), paramXmlSerializer);
    writeZenPolicyState("repeatCallers", paramZenPolicy.getPriorityCategoryRepeatCallers(), paramXmlSerializer);
    writeZenPolicyState("alarms", paramZenPolicy.getPriorityCategoryAlarms(), paramXmlSerializer);
    writeZenPolicyState("media", paramZenPolicy.getPriorityCategoryMedia(), paramXmlSerializer);
    writeZenPolicyState("system", paramZenPolicy.getPriorityCategorySystem(), paramXmlSerializer);
    writeZenPolicyState("reminders", paramZenPolicy.getPriorityCategoryReminders(), paramXmlSerializer);
    writeZenPolicyState("events", paramZenPolicy.getPriorityCategoryEvents(), paramXmlSerializer);
    writeZenPolicyState("showFullScreenIntent", paramZenPolicy.getVisualEffectFullScreenIntent(), paramXmlSerializer);
    writeZenPolicyState("showLights", paramZenPolicy.getVisualEffectLights(), paramXmlSerializer);
    writeZenPolicyState("shoePeek", paramZenPolicy.getVisualEffectPeek(), paramXmlSerializer);
    writeZenPolicyState("showStatusBarIcons", paramZenPolicy.getVisualEffectStatusBar(), paramXmlSerializer);
    writeZenPolicyState("showBadges", paramZenPolicy.getVisualEffectBadge(), paramXmlSerializer);
    writeZenPolicyState("showAmbient", paramZenPolicy.getVisualEffectAmbient(), paramXmlSerializer);
    writeZenPolicyState("showNotificationList", paramZenPolicy.getVisualEffectNotificationList(), paramXmlSerializer);
  }
  
  private static void writeZenPolicyState(String paramString, int paramInt, XmlSerializer paramXmlSerializer) throws IOException {
    if (Objects.equals(paramString, "callsFrom") || Objects.equals(paramString, "messagesFrom")) {
      if (paramInt != 0)
        paramXmlSerializer.attribute(null, paramString, Integer.toString(paramInt)); 
      return;
    } 
    if (paramInt != 0)
      paramXmlSerializer.attribute(null, paramString, Integer.toString(paramInt)); 
  }
  
  public static boolean isValidHour(int paramInt) {
    boolean bool;
    if (paramInt >= 0 && paramInt < 24) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isValidMinute(int paramInt) {
    boolean bool;
    if (paramInt >= 0 && paramInt < 60) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static boolean isValidSource(int paramInt) {
    boolean bool;
    if (paramInt >= 0 && paramInt <= 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static Boolean unsafeBoolean(XmlPullParser paramXmlPullParser, String paramString) {
    String str = paramXmlPullParser.getAttributeValue(null, paramString);
    if (TextUtils.isEmpty(str))
      return null; 
    return Boolean.valueOf(Boolean.parseBoolean(str));
  }
  
  private static boolean safeBoolean(XmlPullParser paramXmlPullParser, String paramString, boolean paramBoolean) {
    String str = paramXmlPullParser.getAttributeValue(null, paramString);
    return safeBoolean(str, paramBoolean);
  }
  
  private static boolean safeBoolean(String paramString, boolean paramBoolean) {
    if (TextUtils.isEmpty(paramString))
      return paramBoolean; 
    return Boolean.parseBoolean(paramString);
  }
  
  private static int safeInt(XmlPullParser paramXmlPullParser, String paramString, int paramInt) {
    String str = paramXmlPullParser.getAttributeValue(null, paramString);
    return tryParseInt(str, paramInt);
  }
  
  private static ComponentName safeComponentName(XmlPullParser paramXmlPullParser, String paramString) {
    String str = paramXmlPullParser.getAttributeValue(null, paramString);
    if (TextUtils.isEmpty(str))
      return null; 
    return ComponentName.unflattenFromString(str);
  }
  
  private static Uri safeUri(XmlPullParser paramXmlPullParser, String paramString) {
    String str = paramXmlPullParser.getAttributeValue(null, paramString);
    if (TextUtils.isEmpty(str))
      return null; 
    return Uri.parse(str);
  }
  
  private static long safeLong(XmlPullParser paramXmlPullParser, String paramString, long paramLong) {
    String str = paramXmlPullParser.getAttributeValue(null, paramString);
    return tryParseLong(str, paramLong);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public ZenModeConfig copy() {
    Parcel parcel = Parcel.obtain();
    try {
      writeToParcel(parcel, 0);
      parcel.setDataPosition(0);
      return new ZenModeConfig(parcel);
    } finally {
      parcel.recycle();
    } 
  }
  
  public ZenPolicy toZenPolicy() {
    ZenPolicy.Builder builder = new ZenPolicy.Builder();
    if (this.allowCalls) {
      i = getZenPolicySenders(this.allowCallsFrom);
    } else {
      i = 4;
    } 
    builder = builder.allowCalls(i);
    boolean bool = this.allowRepeatCallers;
    builder = builder.allowRepeatCallers(bool);
    if (this.allowMessages) {
      i = getZenPolicySenders(this.allowMessagesFrom);
    } else {
      i = 4;
    } 
    builder = builder.allowMessages(i);
    bool = this.allowReminders;
    builder = builder.allowReminders(bool);
    bool = this.allowEvents;
    builder = builder.allowEvents(bool);
    bool = this.allowAlarms;
    builder = builder.allowAlarms(bool);
    bool = this.allowMedia;
    builder = builder.allowMedia(bool);
    bool = this.allowSystem;
    builder = builder.allowSystem(bool);
    if (this.allowConversations) {
      i = getZenPolicySenders(this.allowConversationsFrom);
    } else {
      i = 4;
    } 
    builder = builder.allowConversations(i);
    int i = this.suppressedVisualEffects;
    if (i == 0) {
      builder.showAllVisualEffects();
    } else {
      boolean bool1 = true;
      if ((i & 0x4) == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      builder.showFullScreenIntent(bool);
      if ((this.suppressedVisualEffects & 0x8) == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      builder.showLights(bool);
      if ((this.suppressedVisualEffects & 0x10) == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      builder.showPeeking(bool);
      if ((this.suppressedVisualEffects & 0x20) == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      builder.showStatusBarIcons(bool);
      if ((this.suppressedVisualEffects & 0x40) == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      builder.showBadges(bool);
      if ((this.suppressedVisualEffects & 0x80) == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      builder.showInAmbientDisplay(bool);
      if ((this.suppressedVisualEffects & 0x100) == 0) {
        bool = bool1;
      } else {
        bool = false;
      } 
      builder.showInNotificationList(bool);
    } 
    return builder.build();
  }
  
  public NotificationManager.Policy toNotificationPolicy(ZenPolicy paramZenPolicy) {
    NotificationManager.Policy policy = toNotificationPolicy();
    int i = 0;
    boolean bool = false;
    int j = policy.priorityCallSenders;
    int k = policy.priorityMessageSenders;
    int m = policy.priorityConversationSenders;
    boolean bool1 = isPriorityCategoryEnabled(1, policy);
    if (paramZenPolicy.isCategoryAllowed(0, bool1))
      i = false | true; 
    bool1 = isPriorityCategoryEnabled(2, policy);
    int n = i;
    if (paramZenPolicy.isCategoryAllowed(1, bool1))
      n = i | 0x2; 
    bool1 = isPriorityCategoryEnabled(4, policy);
    i = n;
    int i1 = k;
    if (paramZenPolicy.isCategoryAllowed(2, bool1)) {
      i = n | 0x4;
      i1 = getNotificationPolicySenders(paramZenPolicy.getPriorityMessageSenders(), k);
    } 
    bool1 = isPriorityCategoryEnabled(256, policy);
    n = i;
    k = m;
    if (paramZenPolicy.isCategoryAllowed(8, bool1)) {
      n = i | 0x100;
      i = paramZenPolicy.getPriorityConversationSenders();
      k = getNotificationPolicySenders(i, m);
    } 
    bool1 = isPriorityCategoryEnabled(8, policy);
    i = n;
    m = j;
    if (paramZenPolicy.isCategoryAllowed(3, bool1)) {
      i = n | 0x8;
      m = getNotificationPolicySenders(paramZenPolicy.getPriorityCallSenders(), j);
    } 
    bool1 = isPriorityCategoryEnabled(16, policy);
    j = i;
    if (paramZenPolicy.isCategoryAllowed(4, bool1))
      j = i | 0x10; 
    bool1 = isPriorityCategoryEnabled(32, policy);
    n = j;
    if (paramZenPolicy.isCategoryAllowed(5, bool1))
      n = j | 0x20; 
    bool1 = isPriorityCategoryEnabled(64, policy);
    i = n;
    if (paramZenPolicy.isCategoryAllowed(6, bool1))
      i = n | 0x40; 
    bool1 = isPriorityCategoryEnabled(128, policy);
    int i2 = i;
    if (paramZenPolicy.isCategoryAllowed(7, bool1))
      i2 = i | 0x80; 
    bool1 = isVisualEffectAllowed(4, policy);
    int i3 = paramZenPolicy.isVisualEffectAllowed(0, bool1) ^ true;
    bool1 = isVisualEffectAllowed(8, policy);
    j = paramZenPolicy.isVisualEffectAllowed(1, bool1) ^ true;
    bool1 = isVisualEffectAllowed(128, policy);
    int i4 = true ^ paramZenPolicy.isVisualEffectAllowed(5, bool1);
    n = bool;
    if (i3 != 0) {
      n = bool;
      if (j != 0) {
        n = bool;
        if (i4 != 0)
          n = false | true; 
      } 
    } 
    i = n;
    if (i3 != 0)
      i = n | 0x4; 
    n = i;
    if (j != 0)
      n = i | 0x8; 
    bool1 = isVisualEffectAllowed(16, policy);
    j = n;
    if (!paramZenPolicy.isVisualEffectAllowed(2, bool1))
      j = n | 0x10 | 0x2; 
    bool1 = isVisualEffectAllowed(32, policy);
    i = j;
    if (!paramZenPolicy.isVisualEffectAllowed(3, bool1))
      i = j | 0x20; 
    bool1 = isVisualEffectAllowed(64, policy);
    n = i;
    if (!paramZenPolicy.isVisualEffectAllowed(4, bool1))
      n = i | 0x40; 
    i = n;
    if (i4 != 0)
      i = n | 0x80; 
    bool1 = isVisualEffectAllowed(256, policy);
    n = i;
    if (!paramZenPolicy.isVisualEffectAllowed(6, bool1))
      n = i | 0x100; 
    return new NotificationManager.Policy(i2, m, i1, n, policy.state, k);
  }
  
  private boolean isPriorityCategoryEnabled(int paramInt, NotificationManager.Policy paramPolicy) {
    boolean bool;
    if ((paramPolicy.priorityCategories & paramInt) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isVisualEffectAllowed(int paramInt, NotificationManager.Policy paramPolicy) {
    boolean bool;
    if ((paramPolicy.suppressedVisualEffects & paramInt) == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private int getNotificationPolicySenders(int paramInt1, int paramInt2) {
    if (paramInt1 != 1) {
      if (paramInt1 != 2) {
        if (paramInt1 != 3)
          return paramInt2; 
        return 2;
      } 
      return 1;
    } 
    return 0;
  }
  
  public static int getZenPolicySenders(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1)
        return 3; 
      return 2;
    } 
    return 1;
  }
  
  public NotificationManager.Policy toNotificationPolicy() {
    int i = 0;
    if (this.allowConversations)
      i = Character.MIN_VALUE | 0x100; 
    int j = i;
    if (this.allowCalls)
      j = i | 0x8; 
    i = j;
    if (this.allowMessages)
      i = j | 0x4; 
    j = i;
    if (this.allowEvents)
      j = i | 0x2; 
    int k = j;
    if (this.allowReminders)
      k = j | 0x1; 
    i = k;
    if (this.allowRepeatCallers)
      i = k | 0x10; 
    j = i;
    if (this.allowAlarms)
      j = i | 0x20; 
    i = j;
    if (this.allowMedia)
      i = j | 0x40; 
    j = i;
    if (this.allowSystem)
      j = i | 0x80; 
    k = sourceToPrioritySenders(this.allowCallsFrom, 1);
    int m = sourceToPrioritySenders(this.allowMessagesFrom, 1);
    int n = this.allowConversationsFrom;
    int i1 = this.suppressedVisualEffects;
    if (this.areChannelsBypassingDnd) {
      i = 1;
    } else {
      i = 0;
    } 
    return new NotificationManager.Policy(j, k, m, i1, i, n);
  }
  
  public static ScheduleCalendar toScheduleCalendar(Uri paramUri) {
    ScheduleInfo scheduleInfo = tryParseScheduleConditionId(paramUri);
    if (scheduleInfo == null || scheduleInfo.days == null || scheduleInfo.days.length == 0)
      return null; 
    ScheduleCalendar scheduleCalendar = new ScheduleCalendar();
    scheduleCalendar.setSchedule(scheduleInfo);
    scheduleCalendar.setTimeZone(TimeZone.getDefault());
    return scheduleCalendar;
  }
  
  private static int sourceToPrioritySenders(int paramInt1, int paramInt2) {
    if (paramInt1 != 0) {
      if (paramInt1 != 1) {
        if (paramInt1 != 2)
          return paramInt2; 
        return 2;
      } 
      return 1;
    } 
    return 0;
  }
  
  private static int prioritySendersToSource(int paramInt1, int paramInt2) {
    if (paramInt1 != 0) {
      if (paramInt1 != 1) {
        if (paramInt1 != 2)
          return paramInt2; 
        return 2;
      } 
      return 1;
    } 
    return 0;
  }
  
  private static int normalizePrioritySenders(int paramInt1, int paramInt2) {
    if (paramInt1 != 1 && paramInt1 != 2 && paramInt1 != 0)
      return paramInt2; 
    return paramInt1;
  }
  
  private static int normalizeConversationSenders(boolean paramBoolean, int paramInt1, int paramInt2) {
    if (!paramBoolean)
      return 3; 
    if (paramInt1 != 1 && paramInt1 != 2 && paramInt1 != 3)
      return paramInt2; 
    return paramInt1;
  }
  
  public void applyNotificationPolicy(NotificationManager.Policy paramPolicy) {
    boolean bool2;
    if (paramPolicy == null)
      return; 
    int i = paramPolicy.priorityCategories;
    boolean bool1 = false;
    if ((i & 0x20) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.allowAlarms = bool2;
    if ((paramPolicy.priorityCategories & 0x40) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.allowMedia = bool2;
    if ((paramPolicy.priorityCategories & 0x80) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.allowSystem = bool2;
    if ((paramPolicy.priorityCategories & 0x2) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.allowEvents = bool2;
    if ((paramPolicy.priorityCategories & 0x1) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.allowReminders = bool2;
    if ((paramPolicy.priorityCategories & 0x8) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.allowCalls = bool2;
    if ((paramPolicy.priorityCategories & 0x4) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.allowMessages = bool2;
    if ((paramPolicy.priorityCategories & 0x10) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.allowRepeatCallers = bool2;
    this.allowCallsFrom = normalizePrioritySenders(paramPolicy.priorityCallSenders, this.allowCallsFrom);
    this.allowMessagesFrom = normalizePrioritySenders(paramPolicy.priorityMessageSenders, this.allowMessagesFrom);
    if (paramPolicy.suppressedVisualEffects != -1)
      this.suppressedVisualEffects = paramPolicy.suppressedVisualEffects; 
    if ((paramPolicy.priorityCategories & 0x100) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.allowConversations = bool2;
    this.allowConversationsFrom = normalizeConversationSenders(bool2, paramPolicy.priorityConversationSenders, this.allowConversationsFrom);
    if (paramPolicy.state != -1) {
      bool2 = bool1;
      if ((paramPolicy.state & 0x1) != 0)
        bool2 = true; 
      this.areChannelsBypassingDnd = bool2;
    } 
  }
  
  public static Condition toTimeCondition(Context paramContext, int paramInt1, int paramInt2) {
    return toTimeCondition(paramContext, paramInt1, paramInt2, false);
  }
  
  public static Condition toTimeCondition(Context paramContext, int paramInt1, int paramInt2, boolean paramBoolean) {
    long l2, l1 = System.currentTimeMillis();
    if (paramInt1 == 0) {
      l2 = 10000L;
    } else {
      l2 = (60000 * paramInt1);
    } 
    return toTimeCondition(paramContext, l1 + l2, paramInt1, paramInt2, paramBoolean);
  }
  
  public static Condition toTimeCondition(Context paramContext, long paramLong, int paramInt1, int paramInt2, boolean paramBoolean) {
    String str1, str2, str3;
    CharSequence charSequence = getFormattedTime(paramContext, paramLong, isToday(paramLong), paramInt2);
    Resources resources = paramContext.getResources();
    if (paramInt1 < 60) {
      if (paramBoolean) {
        paramInt2 = 18153507;
      } else {
        paramInt2 = 18153506;
      } 
      str2 = resources.getQuantityString(paramInt2, paramInt1, new Object[] { Integer.valueOf(paramInt1), charSequence });
      if (paramBoolean) {
        paramInt2 = 18153505;
      } else {
        paramInt2 = 18153504;
      } 
      str3 = resources.getQuantityString(paramInt2, paramInt1, new Object[] { Integer.valueOf(paramInt1), charSequence });
      str1 = resources.getString(17041607, new Object[] { charSequence });
    } else if (paramInt1 < 1440) {
      paramInt2 = Math.round(paramInt1 / 60.0F);
      if (paramBoolean) {
        paramInt1 = 18153503;
      } else {
        paramInt1 = 18153502;
      } 
      str2 = str1.getQuantityString(paramInt1, paramInt2, new Object[] { Integer.valueOf(paramInt2), charSequence });
      if (paramBoolean) {
        paramInt1 = 18153501;
      } else {
        paramInt1 = 18153500;
      } 
      str3 = str1.getQuantityString(paramInt1, paramInt2, new Object[] { Integer.valueOf(paramInt2), charSequence });
      str1 = str1.getString(17041607, new Object[] { charSequence });
    } else {
      str2 = str1 = str1.getString(17041607, new Object[] { charSequence });
      str3 = str1;
    } 
    Uri uri = toCountdownConditionId(paramLong, false);
    return new Condition(uri, str2, str3, str1, 0, 1, 1);
  }
  
  public static Condition toNextAlarmCondition(Context paramContext, long paramLong, int paramInt) {
    boolean bool = isToday(paramLong);
    CharSequence charSequence = getFormattedTime(paramContext, paramLong, bool, paramInt);
    Resources resources = paramContext.getResources();
    charSequence = resources.getString(17041607, new Object[] { charSequence });
    Uri uri = toCountdownConditionId(paramLong, true);
    return new Condition(uri, "", (String)charSequence, "", 0, 1, 1);
  }
  
  public static CharSequence getFormattedTime(Context paramContext, long paramLong, boolean paramBoolean, int paramInt) {
    String str2;
    StringBuilder stringBuilder = new StringBuilder();
    if (!paramBoolean) {
      str2 = "EEE ";
    } else {
      str2 = "";
    } 
    stringBuilder.append(str2);
    if (DateFormat.is24HourFormat(paramContext, paramInt)) {
      str1 = "Hm";
    } else {
      str1 = "hma";
    } 
    stringBuilder.append(str1);
    String str1 = stringBuilder.toString();
    str1 = DateFormat.getBestDateTimePattern(Locale.getDefault(), str1);
    return DateFormat.format(str1, paramLong);
  }
  
  public static boolean isToday(long paramLong) {
    GregorianCalendar gregorianCalendar1 = new GregorianCalendar();
    GregorianCalendar gregorianCalendar2 = new GregorianCalendar();
    gregorianCalendar2.setTimeInMillis(paramLong);
    if (gregorianCalendar1.get(1) == gregorianCalendar2.get(1) && 
      gregorianCalendar1.get(2) == gregorianCalendar2.get(2) && 
      gregorianCalendar1.get(5) == gregorianCalendar2.get(5))
      return true; 
    return false;
  }
  
  public static Uri toCountdownConditionId(long paramLong, boolean paramBoolean) {
    Uri.Builder builder = (new Uri.Builder()).scheme("condition");
    builder = builder.authority("android");
    builder = builder.appendPath("countdown");
    builder = builder.appendPath(Long.toString(paramLong));
    builder = builder.appendPath("alarm");
    builder = builder.appendPath(Boolean.toString(paramBoolean));
    return builder.build();
  }
  
  public static long tryParseCountdownConditionId(Uri paramUri) {
    if (!Condition.isValidId(paramUri, "android"))
      return 0L; 
    if (paramUri.getPathSegments().size() < 2 || 
      !"countdown".equals(paramUri.getPathSegments().get(0)))
      return 0L; 
    try {
      return Long.parseLong(paramUri.getPathSegments().get(1));
    } catch (RuntimeException runtimeException) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error parsing countdown condition: ");
      stringBuilder.append(paramUri);
      Slog.w(str, stringBuilder.toString(), runtimeException);
      return 0L;
    } 
  }
  
  public static boolean isValidCountdownConditionId(Uri paramUri) {
    boolean bool;
    if (tryParseCountdownConditionId(paramUri) != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isValidCountdownToAlarmConditionId(Uri paramUri) {
    if (tryParseCountdownConditionId(paramUri) != 0L) {
      if (paramUri.getPathSegments().size() < 4 || 
        !"alarm".equals(paramUri.getPathSegments().get(2)))
        return false; 
      try {
        return Boolean.parseBoolean(paramUri.getPathSegments().get(3));
      } catch (RuntimeException runtimeException) {
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Error parsing countdown alarm condition: ");
        stringBuilder.append(paramUri);
        Slog.w(str, stringBuilder.toString(), runtimeException);
        return false;
      } 
    } 
    return false;
  }
  
  public static Uri toScheduleConditionId(ScheduleInfo paramScheduleInfo) {
    Uri.Builder builder3 = (new Uri.Builder()).scheme("condition");
    builder3 = builder3.authority("android");
    Uri.Builder builder4 = builder3.appendPath("schedule");
    int[] arrayOfInt = paramScheduleInfo.days;
    Uri.Builder builder2 = builder4.appendQueryParameter("days", toDayList(arrayOfInt));
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(paramScheduleInfo.startHour);
    stringBuilder2.append(".");
    stringBuilder2.append(paramScheduleInfo.startMinute);
    String str2 = stringBuilder2.toString();
    builder2 = builder2.appendQueryParameter("start", str2);
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(paramScheduleInfo.endHour);
    stringBuilder1.append(".");
    stringBuilder1.append(paramScheduleInfo.endMinute);
    String str1 = stringBuilder1.toString();
    builder2 = builder2.appendQueryParameter("end", str1);
    boolean bool = paramScheduleInfo.exitAtAlarm;
    Uri.Builder builder1 = builder2.appendQueryParameter("exitAtAlarm", String.valueOf(bool));
    return builder1.build();
  }
  
  public static boolean isValidScheduleConditionId(Uri paramUri) {
    try {
      ScheduleInfo scheduleInfo = tryParseScheduleConditionId(paramUri);
      if (scheduleInfo == null || scheduleInfo.days == null || scheduleInfo.days.length == 0)
        return false; 
      return true;
    } catch (NullPointerException|ArrayIndexOutOfBoundsException nullPointerException) {
      return false;
    } 
  }
  
  public static boolean isValidScheduleConditionId(Uri paramUri, boolean paramBoolean) {
    try {
      ScheduleInfo scheduleInfo = tryParseScheduleConditionId(paramUri);
      if (scheduleInfo == null || (!paramBoolean && (scheduleInfo.days == null || scheduleInfo.days.length == 0)))
        return false; 
      return true;
    } catch (NullPointerException|ArrayIndexOutOfBoundsException nullPointerException) {
      return false;
    } 
  }
  
  public static ScheduleInfo tryParseScheduleConditionId(Uri paramUri) {
    boolean bool;
    if (paramUri != null && 
      "condition".equals(paramUri.getScheme()) && 
      "android".equals(paramUri.getAuthority()) && 
      paramUri.getPathSegments().size() == 1 && 
      "schedule".equals(paramUri.getPathSegments().get(0))) {
      bool = true;
    } else {
      bool = false;
    } 
    if (!bool)
      return null; 
    int[] arrayOfInt1 = tryParseHourAndMinute(paramUri.getQueryParameter("start"));
    int[] arrayOfInt2 = tryParseHourAndMinute(paramUri.getQueryParameter("end"));
    if (arrayOfInt1 == null || arrayOfInt2 == null)
      return null; 
    ScheduleInfo scheduleInfo = new ScheduleInfo();
    scheduleInfo.days = tryParseDayList(paramUri.getQueryParameter("days"), "\\.");
    scheduleInfo.startHour = arrayOfInt1[0];
    scheduleInfo.startMinute = arrayOfInt1[1];
    scheduleInfo.endHour = arrayOfInt2[0];
    scheduleInfo.endMinute = arrayOfInt2[1];
    scheduleInfo.exitAtAlarm = safeBoolean(paramUri.getQueryParameter("exitAtAlarm"), false);
    return scheduleInfo;
  }
  
  public static ComponentName getScheduleConditionProvider() {
    return new ComponentName("android", "ScheduleConditionProvider");
  }
  
  class ScheduleInfo {
    public int[] days;
    
    public int endHour;
    
    public int endMinute;
    
    public boolean exitAtAlarm;
    
    public long nextAlarm;
    
    public int startHour;
    
    public int startMinute;
    
    public int hashCode() {
      return 0;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof ScheduleInfo;
      boolean bool1 = false;
      if (!bool)
        return false; 
      param1Object = param1Object;
      bool = bool1;
      if (ZenModeConfig.toDayList(this.days).equals(ZenModeConfig.toDayList(((ScheduleInfo)param1Object).days))) {
        bool = bool1;
        if (this.startHour == ((ScheduleInfo)param1Object).startHour) {
          bool = bool1;
          if (this.startMinute == ((ScheduleInfo)param1Object).startMinute) {
            bool = bool1;
            if (this.endHour == ((ScheduleInfo)param1Object).endHour) {
              bool = bool1;
              if (this.endMinute == ((ScheduleInfo)param1Object).endMinute) {
                bool = bool1;
                if (this.exitAtAlarm == ((ScheduleInfo)param1Object).exitAtAlarm)
                  bool = true; 
              } 
            } 
          } 
        } 
      } 
      return bool;
    }
    
    public ScheduleInfo copy() {
      ScheduleInfo scheduleInfo = new ScheduleInfo();
      int[] arrayOfInt = this.days;
      if (arrayOfInt != null) {
        int[] arrayOfInt1 = new int[arrayOfInt.length];
        arrayOfInt = this.days;
        System.arraycopy(arrayOfInt, 0, arrayOfInt1, 0, arrayOfInt.length);
      } 
      scheduleInfo.startHour = this.startHour;
      scheduleInfo.startMinute = this.startMinute;
      scheduleInfo.endHour = this.endHour;
      scheduleInfo.endMinute = this.endMinute;
      scheduleInfo.exitAtAlarm = this.exitAtAlarm;
      scheduleInfo.nextAlarm = this.nextAlarm;
      return scheduleInfo;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ScheduleInfo{days=");
      int[] arrayOfInt = this.days;
      stringBuilder.append(Arrays.toString(arrayOfInt));
      stringBuilder.append(", startHour=");
      stringBuilder.append(this.startHour);
      stringBuilder.append(", startMinute=");
      stringBuilder.append(this.startMinute);
      stringBuilder.append(", endHour=");
      stringBuilder.append(this.endHour);
      stringBuilder.append(", endMinute=");
      stringBuilder.append(this.endMinute);
      stringBuilder.append(", exitAtAlarm=");
      stringBuilder.append(this.exitAtAlarm);
      stringBuilder.append(", nextAlarm=");
      long l = this.nextAlarm;
      stringBuilder.append(ts(l));
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
    
    protected static String ts(long param1Long) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(new Date(param1Long));
      stringBuilder.append(" (");
      stringBuilder.append(param1Long);
      stringBuilder.append(")");
      return stringBuilder.toString();
    }
  }
  
  public static Uri toEventConditionId(EventInfo paramEventInfo) {
    Uri.Builder builder3 = (new Uri.Builder()).scheme("condition");
    builder3 = builder3.authority("android");
    builder3 = builder3.appendPath("event");
    long l = paramEventInfo.userId;
    Uri.Builder builder4 = builder3.appendQueryParameter("userId", Long.toString(l));
    String str1 = paramEventInfo.calName, str2 = "";
    if (str1 != null) {
      str1 = paramEventInfo.calName;
    } else {
      str1 = "";
    } 
    builder4 = builder4.appendQueryParameter("calendar", str1);
    if (paramEventInfo.calendarId != null) {
      str1 = paramEventInfo.calendarId.toString();
    } else {
      str1 = str2;
    } 
    Uri.Builder builder2 = builder4.appendQueryParameter("calendarId", str1);
    int i = paramEventInfo.reply;
    Uri.Builder builder1 = builder2.appendQueryParameter("reply", Integer.toString(i));
    return builder1.build();
  }
  
  public static boolean isValidEventConditionId(Uri paramUri) {
    boolean bool;
    if (tryParseEventConditionId(paramUri) != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static EventInfo tryParseEventConditionId(Uri paramUri) {
    boolean bool = true;
    if (paramUri == null || 
      !"condition".equals(paramUri.getScheme()) || 
      !"android".equals(paramUri.getAuthority()) || 
      paramUri.getPathSegments().size() != 1 || 
      !"event".equals(paramUri.getPathSegments().get(0)))
      bool = false; 
    if (!bool)
      return null; 
    EventInfo eventInfo = new EventInfo();
    eventInfo.userId = tryParseInt(paramUri.getQueryParameter("userId"), -10000);
    eventInfo.calName = paramUri.getQueryParameter("calendar");
    if (TextUtils.isEmpty(eventInfo.calName))
      eventInfo.calName = null; 
    eventInfo.calendarId = tryParseLong(paramUri.getQueryParameter("calendarId"), (Long)null);
    eventInfo.reply = tryParseInt(paramUri.getQueryParameter("reply"), 0);
    return eventInfo;
  }
  
  public static ComponentName getEventConditionProvider() {
    return new ComponentName("android", "EventConditionProvider");
  }
  
  class EventInfo {
    public static final int REPLY_ANY_EXCEPT_NO = 0;
    
    public static final int REPLY_YES = 2;
    
    public static final int REPLY_YES_OR_MAYBE = 1;
    
    public String calName;
    
    public Long calendarId;
    
    public int reply;
    
    public int userId = -10000;
    
    public int hashCode() {
      return Objects.hash(new Object[] { Integer.valueOf(this.userId), this.calName, this.calendarId, Integer.valueOf(this.reply) });
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof EventInfo;
      boolean bool1 = false;
      if (!bool)
        return false; 
      param1Object = param1Object;
      if (this.userId == ((EventInfo)param1Object).userId) {
        String str1 = this.calName, str2 = ((EventInfo)param1Object).calName;
        if (Objects.equals(str1, str2) && this.reply == ((EventInfo)param1Object).reply) {
          Long long_ = this.calendarId;
          param1Object = ((EventInfo)param1Object).calendarId;
          if (Objects.equals(long_, param1Object))
            bool1 = true; 
        } 
      } 
      return bool1;
    }
    
    public EventInfo copy() {
      EventInfo eventInfo = new EventInfo();
      eventInfo.userId = this.userId;
      eventInfo.calName = this.calName;
      eventInfo.reply = this.reply;
      eventInfo.calendarId = this.calendarId;
      return eventInfo;
    }
    
    public static int resolveUserId(int param1Int) {
      if (param1Int == -10000)
        param1Int = ActivityManager.getCurrentUser(); 
      return param1Int;
    }
  }
  
  private static int[] tryParseHourAndMinute(String paramString) {
    int[] arrayOfInt;
    boolean bool = TextUtils.isEmpty(paramString);
    String str = null;
    if (bool)
      return null; 
    int i = paramString.indexOf('.');
    if (i < 1 || i >= paramString.length() - 1)
      return null; 
    int j = tryParseInt(paramString.substring(0, i), -1);
    i = tryParseInt(paramString.substring(i + 1), -1);
    paramString = str;
    if (isValidHour(j)) {
      paramString = str;
      if (isValidMinute(i)) {
        arrayOfInt = new int[2];
        arrayOfInt[0] = j;
        arrayOfInt[1] = i;
      } 
    } 
    return arrayOfInt;
  }
  
  private static int tryParseZenMode(String paramString, int paramInt) {
    int i = tryParseInt(paramString, paramInt);
    if (Settings.Global.isValidZenMode(i))
      paramInt = i; 
    return paramInt;
  }
  
  public static String newRuleId() {
    return UUID.randomUUID().toString().replace("-", "");
  }
  
  public static String getOwnerCaption(Context paramContext, String paramString) {
    PackageManager packageManager = paramContext.getPackageManager();
    try {
      ApplicationInfo applicationInfo = packageManager.getApplicationInfo(paramString, 0);
    } finally {
      packageManager = null;
    } 
    return "";
  }
  
  public static String getConditionSummary(Context paramContext, ZenModeConfig paramZenModeConfig, int paramInt, boolean paramBoolean) {
    return getConditionLine(paramContext, paramZenModeConfig, paramInt, false, paramBoolean);
  }
  
  private static String getConditionLine(Context paramContext, ZenModeConfig paramZenModeConfig, int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    String str1 = "";
    if (paramZenModeConfig == null)
      return ""; 
    String str2 = "";
    zenRule = paramZenModeConfig.manualRule;
    if (zenRule != null) {
      String str;
      Uri uri = zenRule.conditionId;
      if (paramZenModeConfig.manualRule.enabler != null) {
        str = getOwnerCaption(paramContext, paramZenModeConfig.manualRule.enabler);
      } else if (str == null) {
        str = paramContext.getString(17041604);
      } else {
        long l = tryParseCountdownConditionId((Uri)str);
        Condition condition = paramZenModeConfig.manualRule.condition;
        if (l > 0L) {
          long l1 = System.currentTimeMillis();
          condition = toTimeCondition(paramContext, l, Math.round((float)(l - l1) / 60000.0F), paramInt, paramBoolean2);
        } 
        if (condition == null) {
          str2 = "";
        } else if (paramBoolean1) {
          str2 = ((Condition)str2).line1;
        } else {
          str2 = ((Condition)str2).summary;
        } 
        if (TextUtils.isEmpty(str2))
          str2 = str1; 
      } 
    } 
    for (ZenRule zenRule : paramZenModeConfig.automaticRules.values()) {
      String str = str2;
      if (zenRule.isAutomaticActive())
        if (str2.isEmpty()) {
          str = zenRule.name;
        } else {
          Resources resources = paramContext.getResources();
          String str3 = zenRule.name;
          str = resources.getString(17041606, new Object[] { str2, str3 });
        }  
      str2 = str;
    } 
    return str2;
  }
  
  public static class ZenRule implements Parcelable {
    public ZenRule() {}
    
    public ZenRule(Parcel param1Parcel) {
      int i = param1Parcel.readInt();
      boolean bool1 = false;
      if (i == 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.enabled = bool2;
      if (param1Parcel.readInt() == 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.snoozing = bool2;
      if (param1Parcel.readInt() == 1)
        this.name = param1Parcel.readString(); 
      this.zenMode = param1Parcel.readInt();
      this.conditionId = param1Parcel.<Uri>readParcelable(null);
      this.condition = param1Parcel.<Condition>readParcelable(null);
      this.component = param1Parcel.<ComponentName>readParcelable(null);
      this.configurationActivity = param1Parcel.<ComponentName>readParcelable(null);
      if (param1Parcel.readInt() == 1)
        this.id = param1Parcel.readString(); 
      this.creationTime = param1Parcel.readLong();
      if (param1Parcel.readInt() == 1)
        this.enabler = param1Parcel.readString(); 
      this.zenPolicy = param1Parcel.<ZenPolicy>readParcelable(null);
      boolean bool2 = bool1;
      if (param1Parcel.readInt() == 1)
        bool2 = true; 
      this.modified = bool2;
      this.pkg = param1Parcel.readString();
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.enabled);
      param1Parcel.writeInt(this.snoozing);
      if (this.name != null) {
        param1Parcel.writeInt(1);
        param1Parcel.writeString(this.name);
      } else {
        param1Parcel.writeInt(0);
      } 
      param1Parcel.writeInt(this.zenMode);
      param1Parcel.writeParcelable(this.conditionId, 0);
      param1Parcel.writeParcelable(this.condition, 0);
      param1Parcel.writeParcelable((Parcelable)this.component, 0);
      param1Parcel.writeParcelable((Parcelable)this.configurationActivity, 0);
      if (this.id != null) {
        param1Parcel.writeInt(1);
        param1Parcel.writeString(this.id);
      } else {
        param1Parcel.writeInt(0);
      } 
      param1Parcel.writeLong(this.creationTime);
      if (this.enabler != null) {
        param1Parcel.writeInt(1);
        param1Parcel.writeString(this.enabler);
      } else {
        param1Parcel.writeInt(0);
      } 
      param1Parcel.writeParcelable(this.zenPolicy, 0);
      param1Parcel.writeInt(this.modified);
      param1Parcel.writeString(this.pkg);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder(ZenRule.class.getSimpleName());
      stringBuilder.append('[');
      stringBuilder.append("id=");
      stringBuilder.append(this.id);
      stringBuilder.append(",enabled=");
      stringBuilder.append(String.valueOf(this.enabled).toUpperCase());
      stringBuilder.append(",snoozing=");
      stringBuilder.append(this.snoozing);
      stringBuilder.append(",name=");
      stringBuilder.append(this.name);
      stringBuilder.append(",zenMode=");
      stringBuilder.append(Settings.Global.zenModeToString(this.zenMode));
      stringBuilder.append(",conditionId=");
      stringBuilder.append(this.conditionId);
      stringBuilder.append(",condition=");
      stringBuilder.append(this.condition);
      stringBuilder.append(",pkg=");
      stringBuilder.append(this.pkg);
      stringBuilder.append(",component=");
      stringBuilder.append(this.component);
      stringBuilder.append(",configActivity=");
      stringBuilder.append(this.configurationActivity);
      stringBuilder.append(",creationTime=");
      stringBuilder.append(this.creationTime);
      stringBuilder.append(",enabler=");
      stringBuilder.append(this.enabler);
      stringBuilder.append(",zenPolicy=");
      stringBuilder.append(this.zenPolicy);
      stringBuilder.append(",modified=");
      stringBuilder.append(this.modified);
      stringBuilder.append(']');
      return stringBuilder.toString();
    }
    
    public void dumpDebug(ProtoOutputStream param1ProtoOutputStream, long param1Long) {
      param1Long = param1ProtoOutputStream.start(param1Long);
      param1ProtoOutputStream.write(1138166333441L, this.id);
      param1ProtoOutputStream.write(1138166333442L, this.name);
      param1ProtoOutputStream.write(1112396529667L, this.creationTime);
      param1ProtoOutputStream.write(1133871366148L, this.enabled);
      param1ProtoOutputStream.write(1138166333445L, this.enabler);
      param1ProtoOutputStream.write(1133871366150L, this.snoozing);
      param1ProtoOutputStream.write(1159641169927L, this.zenMode);
      Uri uri = this.conditionId;
      if (uri != null)
        param1ProtoOutputStream.write(1138166333448L, uri.toString()); 
      Condition condition = this.condition;
      if (condition != null)
        condition.dumpDebug(param1ProtoOutputStream, 1146756268041L); 
      ComponentName componentName = this.component;
      if (componentName != null)
        componentName.dumpDebug(param1ProtoOutputStream, 1146756268042L); 
      ZenPolicy zenPolicy = this.zenPolicy;
      if (zenPolicy != null)
        zenPolicy.dumpDebug(param1ProtoOutputStream, 1146756268043L); 
      param1ProtoOutputStream.write(1133871366156L, this.modified);
      param1ProtoOutputStream.end(param1Long);
    }
    
    private static void appendDiff(ZenModeConfig.Diff param1Diff, String param1String, ZenRule param1ZenRule1, ZenRule param1ZenRule2) {
      if (param1Diff == null)
        return; 
      if (param1ZenRule1 == null) {
        if (param1ZenRule2 != null)
          param1Diff.addLine(param1String, "insert"); 
        return;
      } 
      param1ZenRule1.appendDiff(param1Diff, param1String, param1ZenRule2);
    }
    
    private void appendDiff(ZenModeConfig.Diff param1Diff, String param1String, ZenRule param1ZenRule) {
      if (param1ZenRule == null) {
        param1Diff.addLine(param1String, "delete");
        return;
      } 
      boolean bool = this.enabled;
      if (bool != param1ZenRule.enabled)
        param1Diff.addLine(param1String, "enabled", Boolean.valueOf(bool), Boolean.valueOf(param1ZenRule.enabled)); 
      bool = this.snoozing;
      if (bool != param1ZenRule.snoozing)
        param1Diff.addLine(param1String, "snoozing", Boolean.valueOf(bool), Boolean.valueOf(param1ZenRule.snoozing)); 
      if (!Objects.equals(this.name, param1ZenRule.name))
        param1Diff.addLine(param1String, "name", this.name, param1ZenRule.name); 
      int i = this.zenMode;
      if (i != param1ZenRule.zenMode)
        param1Diff.addLine(param1String, "zenMode", Integer.valueOf(i), Integer.valueOf(param1ZenRule.zenMode)); 
      if (!Objects.equals(this.conditionId, param1ZenRule.conditionId))
        param1Diff.addLine(param1String, "conditionId", this.conditionId, param1ZenRule.conditionId); 
      if (!Objects.equals(this.condition, param1ZenRule.condition))
        param1Diff.addLine(param1String, "condition", this.condition, param1ZenRule.condition); 
      if (!Objects.equals(this.component, param1ZenRule.component))
        param1Diff.addLine(param1String, "component", this.component, param1ZenRule.component); 
      if (!Objects.equals(this.configurationActivity, param1ZenRule.configurationActivity))
        param1Diff.addLine(param1String, "configActivity", this.configurationActivity, param1ZenRule.configurationActivity); 
      if (!Objects.equals(this.id, param1ZenRule.id))
        param1Diff.addLine(param1String, "id", this.id, param1ZenRule.id); 
      long l = this.creationTime;
      if (l != param1ZenRule.creationTime)
        param1Diff.addLine(param1String, "creationTime", Long.valueOf(l), Long.valueOf(param1ZenRule.creationTime)); 
      if (!Objects.equals(this.enabler, param1ZenRule.enabler))
        param1Diff.addLine(param1String, "enabler", this.enabler, param1ZenRule.enabler); 
      if (!Objects.equals(this.zenPolicy, param1ZenRule.zenPolicy))
        param1Diff.addLine(param1String, "zenPolicy", this.zenPolicy, param1ZenRule.zenPolicy); 
      bool = this.modified;
      if (bool != param1ZenRule.modified)
        param1Diff.addLine(param1String, "modified", Boolean.valueOf(bool), Boolean.valueOf(param1ZenRule.modified)); 
      if (!Objects.equals(this.pkg, param1ZenRule.pkg))
        param1Diff.addLine(param1String, "pkg", this.pkg, param1ZenRule.pkg); 
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof ZenRule;
      boolean bool1 = false;
      if (!bool)
        return false; 
      if (param1Object == this)
        return true; 
      param1Object = param1Object;
      if (((ZenRule)param1Object).enabled == this.enabled && ((ZenRule)param1Object).snoozing == this.snoozing) {
        String str1 = ((ZenRule)param1Object).name, str2 = this.name;
        if (Objects.equals(str1, str2) && ((ZenRule)param1Object).zenMode == this.zenMode) {
          Uri uri1 = ((ZenRule)param1Object).conditionId, uri2 = this.conditionId;
          if (Objects.equals(uri1, uri2)) {
            Condition condition2 = ((ZenRule)param1Object).condition, condition1 = this.condition;
            if (Objects.equals(condition2, condition1)) {
              ComponentName componentName2 = ((ZenRule)param1Object).component, componentName1 = this.component;
              if (Objects.equals(componentName2, componentName1)) {
                componentName2 = ((ZenRule)param1Object).configurationActivity;
                componentName1 = this.configurationActivity;
                if (Objects.equals(componentName2, componentName1)) {
                  String str3 = ((ZenRule)param1Object).id, str4 = this.id;
                  if (Objects.equals(str3, str4)) {
                    str3 = ((ZenRule)param1Object).enabler;
                    str4 = this.enabler;
                    if (Objects.equals(str3, str4)) {
                      ZenPolicy zenPolicy1 = ((ZenRule)param1Object).zenPolicy, zenPolicy2 = this.zenPolicy;
                      if (Objects.equals(zenPolicy1, zenPolicy2)) {
                        String str5 = ((ZenRule)param1Object).pkg, str6 = this.pkg;
                        if (Objects.equals(str5, str6) && ((ZenRule)param1Object).modified == this.modified)
                          bool1 = true; 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
      return bool1;
    }
    
    public int hashCode() {
      boolean bool1 = this.enabled, bool2 = this.snoozing;
      String str1 = this.name;
      int i = this.zenMode;
      Uri uri = this.conditionId;
      Condition condition = this.condition;
      ComponentName componentName1 = this.component, componentName2 = this.configurationActivity;
      String str2 = this.pkg, str3 = this.id, str4 = this.enabler;
      ZenPolicy zenPolicy = this.zenPolicy;
      boolean bool3 = this.modified;
      return Objects.hash(new Object[] { 
            Boolean.valueOf(bool1), Boolean.valueOf(bool2), str1, Integer.valueOf(i), uri, condition, componentName1, componentName2, str2, str3, 
            str4, zenPolicy, Boolean.valueOf(bool3) });
    }
    
    public boolean isAutomaticActive() {
      boolean bool;
      if (this.enabled && !this.snoozing && this.pkg != null && isTrueOrUnknown()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isTrueOrUnknown() {
      Condition condition = this.condition;
      boolean bool = true;
      if (condition == null || (condition.state != 1 && this.condition.state != 2))
        bool = false; 
      return bool;
    }
    
    public static final Parcelable.Creator<ZenRule> CREATOR = new Parcelable.Creator<ZenRule>() {
        public ZenModeConfig.ZenRule createFromParcel(Parcel param2Parcel) {
          return new ZenModeConfig.ZenRule(param2Parcel);
        }
        
        public ZenModeConfig.ZenRule[] newArray(int param2Int) {
          return new ZenModeConfig.ZenRule[param2Int];
        }
      };
    
    public ComponentName component;
    
    public Condition condition;
    
    public Uri conditionId;
    
    public ComponentName configurationActivity;
    
    public long creationTime;
    
    public boolean enabled;
    
    public String enabler;
    
    public String id;
    
    public boolean modified;
    
    public String name;
    
    public String pkg;
    
    public boolean snoozing;
    
    public int zenMode;
    
    public ZenPolicy zenPolicy;
  }
  
  class null implements Parcelable.Creator<ZenRule> {
    public ZenModeConfig.ZenRule createFromParcel(Parcel param1Parcel) {
      return new ZenModeConfig.ZenRule(param1Parcel);
    }
    
    public ZenModeConfig.ZenRule[] newArray(int param1Int) {
      return new ZenModeConfig.ZenRule[param1Int];
    }
  }
  
  class Diff {
    private final ArrayList<String> lines;
    
    public Diff() {
      this.lines = new ArrayList<>();
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder("Diff[");
      int i = this.lines.size();
      for (byte b = 0; b < i; b++) {
        if (b > 0)
          stringBuilder.append(",\n"); 
        stringBuilder.append(this.lines.get(b));
      } 
      stringBuilder.append(']');
      return stringBuilder.toString();
    }
    
    private Diff addLine(String param1String1, String param1String2) {
      ArrayList<String> arrayList = this.lines;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1String1);
      stringBuilder.append(":");
      stringBuilder.append(param1String2);
      arrayList.add(stringBuilder.toString());
      return this;
    }
    
    public Diff addLine(String param1String1, String param1String2, Object param1Object1, Object param1Object2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1String1);
      stringBuilder.append(".");
      stringBuilder.append(param1String2);
      return addLine(stringBuilder.toString(), param1Object1, param1Object2);
    }
    
    public Diff addLine(String param1String, Object param1Object1, Object param1Object2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1Object1);
      stringBuilder.append("->");
      stringBuilder.append(param1Object2);
      return addLine(param1String, stringBuilder.toString());
    }
  }
  
  public static boolean areAllPriorityOnlyRingerSoundsMuted(NotificationManager.Policy paramPolicy) {
    boolean bool2, bool3, bool4, bool5, bool6, bool7, bool8;
    int i = paramPolicy.priorityCategories;
    boolean bool1 = true;
    if ((i & 0x1) != 0) {
      i = 1;
    } else {
      i = 0;
    } 
    if ((paramPolicy.priorityCategories & 0x8) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if ((paramPolicy.priorityCategories & 0x4) != 0) {
      bool3 = true;
    } else {
      bool3 = false;
    } 
    if ((paramPolicy.priorityCategories & 0x2) != 0) {
      bool4 = true;
    } else {
      bool4 = false;
    } 
    if ((paramPolicy.priorityCategories & 0x10) != 0) {
      bool5 = true;
    } else {
      bool5 = false;
    } 
    if ((paramPolicy.priorityConversationSenders & 0x100) != 0) {
      bool6 = true;
    } else {
      bool6 = false;
    } 
    if ((paramPolicy.state & 0x1) != 0) {
      bool7 = true;
    } else {
      bool7 = false;
    } 
    if ((paramPolicy.priorityCategories & 0x80) != 0) {
      bool8 = true;
    } else {
      bool8 = false;
    } 
    if (i != 0 || bool2 || bool3 || bool4 || bool5 || bool7 || bool8 || bool6)
      bool1 = false; 
    return bool1;
  }
  
  public static boolean areAllZenBehaviorSoundsMuted(NotificationManager.Policy paramPolicy) {
    boolean bool2;
    int i = paramPolicy.priorityCategories;
    boolean bool1 = true;
    if ((i & 0x20) != 0) {
      i = 1;
    } else {
      i = 0;
    } 
    if ((paramPolicy.priorityCategories & 0x40) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (i != 0 || bool2 || !areAllPriorityOnlyRingerSoundsMuted(paramPolicy))
      bool1 = false; 
    return bool1;
  }
  
  public static boolean isZenOverridingRinger(int paramInt, NotificationManager.Policy paramPolicy) {
    boolean bool = true;
    if (paramInt != 2 && paramInt != 3) {
      if (paramInt == 1)
        if (areAllPriorityOnlyRingerSoundsMuted(paramPolicy))
          return bool;  
      bool = false;
    } 
    return bool;
  }
  
  public static boolean areAllPriorityOnlyRingerSoundsMuted(ZenModeConfig paramZenModeConfig) {
    boolean bool;
    if (!paramZenModeConfig.allowReminders && !paramZenModeConfig.allowCalls && !paramZenModeConfig.allowMessages && !paramZenModeConfig.allowEvents && !paramZenModeConfig.allowRepeatCallers && !paramZenModeConfig.areChannelsBypassingDnd && !paramZenModeConfig.allowSystem) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean areAllZenBehaviorSoundsMuted(ZenModeConfig paramZenModeConfig) {
    boolean bool;
    if (!paramZenModeConfig.allowAlarms && !paramZenModeConfig.allowMedia && 
      areAllPriorityOnlyRingerSoundsMuted(paramZenModeConfig)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static String getDescription(Context paramContext, boolean paramBoolean1, ZenModeConfig paramZenModeConfig, boolean paramBoolean2) {
    CharSequence charSequence1;
    Context context = null;
    if (!paramBoolean1 || paramZenModeConfig == null)
      return null; 
    String str = "";
    long l1 = -1L;
    ZenRule zenRule = paramZenModeConfig.manualRule;
    CharSequence charSequence2 = str;
    long l2 = l1;
    if (zenRule != null) {
      String str1;
      Uri uri = zenRule.conditionId;
      if (paramZenModeConfig.manualRule.enabler != null) {
        String str2 = getOwnerCaption(paramContext, paramZenModeConfig.manualRule.enabler);
        str1 = str;
        if (!str2.isEmpty())
          str1 = str2; 
        l2 = l1;
      } else {
        if (str1 == null) {
          if (paramBoolean2)
            return paramContext.getString(17041604); 
          return null;
        } 
        l1 = tryParseCountdownConditionId((Uri)str1);
        str1 = str;
        l2 = l1;
        if (l1 > 0L) {
          paramBoolean1 = isToday(l1);
          int i = paramContext.getUserId();
          charSequence2 = getFormattedTime(paramContext, l1, paramBoolean1, i);
          charSequence2 = paramContext.getString(17041607, new Object[] { charSequence2 });
          l2 = l1;
        } 
      } 
    } 
    for (Iterator<ZenRule> iterator = paramZenModeConfig.automaticRules.values().iterator(); iterator.hasNext(); ) {
      zenRule = iterator.next();
      CharSequence charSequence = charSequence2;
      l2 = l1;
      if (zenRule.isAutomaticActive()) {
        if (!isValidEventConditionId(zenRule.conditionId)) {
          Uri uri = zenRule.conditionId;
          if (!isValidScheduleConditionId(uri))
            return zenRule.name; 
        } 
        long l = parseAutomaticRuleEndTime(paramContext, zenRule.conditionId);
        charSequence = charSequence2;
        l2 = l1;
        if (l > l1) {
          l2 = l;
          charSequence = zenRule.name;
        } 
      } 
      charSequence2 = charSequence;
      l1 = l2;
    } 
    paramContext = context;
    if (!charSequence2.equals(""))
      charSequence1 = charSequence2; 
    return (String)charSequence1;
  }
  
  private static long parseAutomaticRuleEndTime(Context paramContext, Uri paramUri) {
    if (isValidEventConditionId(paramUri))
      return Long.MAX_VALUE; 
    if (isValidScheduleConditionId(paramUri)) {
      ScheduleCalendar scheduleCalendar = toScheduleCalendar(paramUri);
      long l = scheduleCalendar.getNextChangeTime(System.currentTimeMillis());
      if (scheduleCalendar.exitAtAlarm()) {
        long l1 = getNextAlarm(paramContext);
        scheduleCalendar.maybeSetNextAlarm(System.currentTimeMillis(), l1);
        if (scheduleCalendar.shouldExitForAlarm(l))
          return l1; 
      } 
      return l;
    } 
    return -1L;
  }
  
  private static long getNextAlarm(Context paramContext) {
    long l;
    AlarmManager alarmManager = (AlarmManager)paramContext.getSystemService("alarm");
    AlarmManager.AlarmClockInfo alarmClockInfo = alarmManager.getNextAlarmClock(paramContext.getUserId());
    if (alarmClockInfo != null) {
      l = alarmClockInfo.getTriggerTime();
    } else {
      l = 0L;
    } 
    return l;
  }
}
