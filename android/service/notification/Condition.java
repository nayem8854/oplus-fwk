package android.service.notification;

import android.content.Context;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.proto.ProtoOutputStream;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;

public final class Condition implements Parcelable {
  public Condition(Uri paramUri, String paramString, int paramInt) {
    this(paramUri, paramString, "", "", -1, paramInt, 2);
  }
  
  public Condition(Uri paramUri, String paramString1, String paramString2, String paramString3, int paramInt1, int paramInt2, int paramInt3) {
    if (paramUri != null) {
      if (paramString1 != null) {
        if (isValidState(paramInt2)) {
          this.id = paramUri;
          this.summary = paramString1;
          this.line1 = paramString2;
          this.line2 = paramString3;
          this.icon = paramInt1;
          this.state = paramInt2;
          this.flags = paramInt3;
          return;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("state is invalid: ");
        stringBuilder.append(paramInt2);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      throw new IllegalArgumentException("summary is required");
    } 
    throw new IllegalArgumentException("id is required");
  }
  
  public Condition(Parcel paramParcel) {
    this(uri, str1, str2, str3, i, j, k);
  }
  
  private static boolean isValidState(int paramInt) {
    boolean bool;
    if (paramInt >= 0 && paramInt <= 3) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.id, 0);
    paramParcel.writeString(this.summary);
    paramParcel.writeString(this.line1);
    paramParcel.writeString(this.line2);
    paramParcel.writeInt(this.icon);
    paramParcel.writeInt(this.state);
    paramParcel.writeInt(this.flags);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(Condition.class.getSimpleName());
    stringBuilder.append('[');
    stringBuilder.append("state=");
    stringBuilder.append(stateToString(this.state));
    stringBuilder.append(",id=");
    stringBuilder.append(this.id);
    stringBuilder.append(",summary=");
    stringBuilder.append(this.summary);
    stringBuilder.append(",line1=");
    stringBuilder.append(this.line1);
    stringBuilder.append(",line2=");
    stringBuilder.append(this.line2);
    stringBuilder.append(",icon=");
    stringBuilder.append(this.icon);
    stringBuilder.append(",flags=");
    stringBuilder.append(this.flags);
    stringBuilder.append(']');
    return stringBuilder.toString();
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    paramLong = paramProtoOutputStream.start(paramLong);
    paramProtoOutputStream.write(1138166333441L, this.id.toString());
    paramProtoOutputStream.write(1138166333442L, this.summary);
    paramProtoOutputStream.write(1138166333443L, this.line1);
    paramProtoOutputStream.write(1138166333444L, this.line2);
    paramProtoOutputStream.write(1120986464261L, this.icon);
    paramProtoOutputStream.write(1159641169926L, this.state);
    paramProtoOutputStream.write(1120986464263L, this.flags);
    paramProtoOutputStream.end(paramLong);
  }
  
  public static String stateToString(int paramInt) {
    if (paramInt == 0)
      return "STATE_FALSE"; 
    if (paramInt == 1)
      return "STATE_TRUE"; 
    if (paramInt == 2)
      return "STATE_UNKNOWN"; 
    if (paramInt == 3)
      return "STATE_ERROR"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("state is invalid: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static String relevanceToString(int paramInt) {
    boolean bool2;
    String str;
    boolean bool1 = false;
    if ((paramInt & 0x1) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if ((paramInt & 0x2) != 0)
      bool1 = true; 
    if (!bool2 && !bool1)
      return "NONE"; 
    if (bool2 && bool1)
      return "NOW, ALWAYS"; 
    if (bool2) {
      str = "NOW";
    } else {
      str = "ALWAYS";
    } 
    return str;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof Condition;
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (paramObject == this)
      return true; 
    paramObject = paramObject;
    if (Objects.equals(((Condition)paramObject).id, this.id)) {
      String str1 = ((Condition)paramObject).summary, str2 = this.summary;
      if (Objects.equals(str1, str2)) {
        str2 = ((Condition)paramObject).line1;
        str1 = this.line1;
        if (Objects.equals(str2, str1)) {
          str1 = ((Condition)paramObject).line2;
          str2 = this.line2;
          if (Objects.equals(str1, str2) && ((Condition)paramObject).icon == this.icon && ((Condition)paramObject).state == this.state && ((Condition)paramObject).flags == this.flags)
            bool1 = true; 
        } 
      } 
    } 
    return bool1;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.id, this.summary, this.line1, this.line2, Integer.valueOf(this.icon), Integer.valueOf(this.state), Integer.valueOf(this.flags) });
  }
  
  public int describeContents() {
    return 0;
  }
  
  public Condition copy() {
    Parcel parcel = Parcel.obtain();
    try {
      writeToParcel(parcel, 0);
      parcel.setDataPosition(0);
      return new Condition(parcel);
    } finally {
      parcel.recycle();
    } 
  }
  
  public static Uri.Builder newId(Context paramContext) {
    Uri.Builder builder = new Uri.Builder();
    builder = builder.scheme("condition");
    return builder.authority(paramContext.getPackageName());
  }
  
  public static boolean isValidId(Uri paramUri, String paramString) {
    boolean bool;
    if (paramUri != null && "condition".equals(paramUri.getScheme()) && paramString.equals(paramUri.getAuthority())) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static final Parcelable.Creator<Condition> CREATOR = new Parcelable.Creator<Condition>() {
      public Condition createFromParcel(Parcel param1Parcel) {
        return new Condition(param1Parcel);
      }
      
      public Condition[] newArray(int param1Int) {
        return new Condition[param1Int];
      }
    };
  
  public static final int FLAG_RELEVANT_ALWAYS = 2;
  
  public static final int FLAG_RELEVANT_NOW = 1;
  
  public static final String SCHEME = "condition";
  
  public static final int STATE_ERROR = 3;
  
  public static final int STATE_FALSE = 0;
  
  public static final int STATE_TRUE = 1;
  
  public static final int STATE_UNKNOWN = 2;
  
  public final int flags;
  
  public final int icon;
  
  public final Uri id;
  
  public final String line1;
  
  public final String line2;
  
  public final int state;
  
  public final String summary;
  
  @Retention(RetentionPolicy.SOURCE)
  class State implements Annotation {}
}
