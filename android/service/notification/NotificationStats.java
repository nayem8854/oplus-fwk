package android.service.notification;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public final class NotificationStats implements Parcelable {
  public NotificationStats() {
    this.mDismissalSurface = -1;
    this.mDismissalSentiment = -1000;
  }
  
  @SystemApi
  protected NotificationStats(Parcel paramParcel) {
    boolean bool2;
    this.mDismissalSurface = -1;
    this.mDismissalSentiment = -1000;
    byte b = paramParcel.readByte();
    boolean bool1 = true;
    if (b != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mSeen = bool2;
    if (paramParcel.readByte() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mExpanded = bool2;
    if (paramParcel.readByte() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mDirectReplied = bool2;
    if (paramParcel.readByte() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mSnoozed = bool2;
    if (paramParcel.readByte() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mViewedSettings = bool2;
    if (paramParcel.readByte() != 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.mInteracted = bool2;
    this.mDismissalSurface = paramParcel.readInt();
    this.mDismissalSentiment = paramParcel.readInt();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByte((byte)this.mSeen);
    paramParcel.writeByte((byte)this.mExpanded);
    paramParcel.writeByte((byte)this.mDirectReplied);
    paramParcel.writeByte((byte)this.mSnoozed);
    paramParcel.writeByte((byte)this.mViewedSettings);
    paramParcel.writeByte((byte)this.mInteracted);
    paramParcel.writeInt(this.mDismissalSurface);
    paramParcel.writeInt(this.mDismissalSentiment);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<NotificationStats> CREATOR = new Parcelable.Creator<NotificationStats>() {
      public NotificationStats createFromParcel(Parcel param1Parcel) {
        return new NotificationStats(param1Parcel);
      }
      
      public NotificationStats[] newArray(int param1Int) {
        return new NotificationStats[param1Int];
      }
    };
  
  public static final int DISMISSAL_AOD = 2;
  
  public static final int DISMISSAL_NOT_DISMISSED = -1;
  
  public static final int DISMISSAL_OTHER = 0;
  
  public static final int DISMISSAL_PEEK = 1;
  
  public static final int DISMISSAL_SHADE = 3;
  
  public static final int DISMISS_SENTIMENT_NEGATIVE = 0;
  
  public static final int DISMISS_SENTIMENT_NEUTRAL = 1;
  
  public static final int DISMISS_SENTIMENT_POSITIVE = 2;
  
  public static final int DISMISS_SENTIMENT_UNKNOWN = -1000;
  
  private boolean mDirectReplied;
  
  private int mDismissalSentiment;
  
  private int mDismissalSurface;
  
  private boolean mExpanded;
  
  private boolean mInteracted;
  
  private boolean mSeen;
  
  private boolean mSnoozed;
  
  private boolean mViewedSettings;
  
  public boolean hasSeen() {
    return this.mSeen;
  }
  
  public void setSeen() {
    this.mSeen = true;
  }
  
  public boolean hasExpanded() {
    return this.mExpanded;
  }
  
  public void setExpanded() {
    this.mExpanded = true;
    this.mInteracted = true;
  }
  
  public boolean hasDirectReplied() {
    return this.mDirectReplied;
  }
  
  public void setDirectReplied() {
    this.mDirectReplied = true;
    this.mInteracted = true;
  }
  
  public boolean hasSnoozed() {
    return this.mSnoozed;
  }
  
  public void setSnoozed() {
    this.mSnoozed = true;
    this.mInteracted = true;
  }
  
  public boolean hasViewedSettings() {
    return this.mViewedSettings;
  }
  
  public void setViewedSettings() {
    this.mViewedSettings = true;
    this.mInteracted = true;
  }
  
  public boolean hasInteracted() {
    return this.mInteracted;
  }
  
  public int getDismissalSurface() {
    return this.mDismissalSurface;
  }
  
  public void setDismissalSurface(int paramInt) {
    this.mDismissalSurface = paramInt;
  }
  
  public void setDismissalSentiment(int paramInt) {
    this.mDismissalSentiment = paramInt;
  }
  
  public int getDismissalSentiment() {
    return this.mDismissalSentiment;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mSeen != ((NotificationStats)paramObject).mSeen)
      return false; 
    if (this.mExpanded != ((NotificationStats)paramObject).mExpanded)
      return false; 
    if (this.mDirectReplied != ((NotificationStats)paramObject).mDirectReplied)
      return false; 
    if (this.mSnoozed != ((NotificationStats)paramObject).mSnoozed)
      return false; 
    if (this.mViewedSettings != ((NotificationStats)paramObject).mViewedSettings)
      return false; 
    if (this.mInteracted != ((NotificationStats)paramObject).mInteracted)
      return false; 
    if (this.mDismissalSurface != ((NotificationStats)paramObject).mDismissalSurface)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    boolean bool1 = this.mSeen;
    boolean bool2 = this.mExpanded;
    boolean bool3 = this.mDirectReplied;
    boolean bool4 = this.mSnoozed;
    boolean bool5 = this.mViewedSettings;
    boolean bool6 = this.mInteracted;
    int i = this.mDismissalSurface;
    return (((((bool1 * 31 + bool2) * 31 + bool3) * 31 + bool4) * 31 + bool5) * 31 + bool6) * 31 + i;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("NotificationStats{");
    stringBuilder.append("mSeen=");
    stringBuilder.append(this.mSeen);
    stringBuilder.append(", mExpanded=");
    stringBuilder.append(this.mExpanded);
    stringBuilder.append(", mDirectReplied=");
    stringBuilder.append(this.mDirectReplied);
    stringBuilder.append(", mSnoozed=");
    stringBuilder.append(this.mSnoozed);
    stringBuilder.append(", mViewedSettings=");
    stringBuilder.append(this.mViewedSettings);
    stringBuilder.append(", mInteracted=");
    stringBuilder.append(this.mInteracted);
    stringBuilder.append(", mDismissalSurface=");
    stringBuilder.append(this.mDismissalSurface);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class DismissalSentiment implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class DismissalSurface implements Annotation {}
}
