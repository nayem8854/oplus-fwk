package android.service.notification;

import android.app.INotificationManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;

@Deprecated
public abstract class ConditionProviderService extends Service {
  @Deprecated
  public static final String EXTRA_RULE_ID = "android.service.notification.extra.RULE_ID";
  
  @Deprecated
  public static final String META_DATA_CONFIGURATION_ACTIVITY = "android.service.zen.automatic.configurationActivity";
  
  @Deprecated
  public static final String META_DATA_RULE_INSTANCE_LIMIT = "android.service.zen.automatic.ruleInstanceLimit";
  
  @Deprecated
  public static final String META_DATA_RULE_TYPE = "android.service.zen.automatic.ruleType";
  
  public static final String SERVICE_INTERFACE = "android.service.notification.ConditionProviderService";
  
  private final String TAG;
  
  private final H mHandler;
  
  boolean mIsConnected;
  
  private INotificationManager mNoMan;
  
  private Provider mProvider;
  
  public ConditionProviderService() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(ConditionProviderService.class.getSimpleName());
    stringBuilder.append("[");
    stringBuilder.append(getClass().getSimpleName());
    stringBuilder.append("]");
    this.TAG = stringBuilder.toString();
    this.mHandler = new H();
  }
  
  public void onRequestConditions(int paramInt) {}
  
  private final INotificationManager getNotificationInterface() {
    if (this.mNoMan == null) {
      IBinder iBinder = ServiceManager.getService("notification");
      this.mNoMan = INotificationManager.Stub.asInterface(iBinder);
    } 
    return this.mNoMan;
  }
  
  public static final void requestRebind(ComponentName paramComponentName) {
    IBinder iBinder = ServiceManager.getService("notification");
    INotificationManager iNotificationManager = INotificationManager.Stub.asInterface(iBinder);
    try {
      iNotificationManager.requestBindProvider(paramComponentName);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public final void requestUnbind() {
    INotificationManager iNotificationManager = getNotificationInterface();
    try {
      iNotificationManager.requestUnbindProvider(this.mProvider);
      this.mIsConnected = false;
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public final void notifyCondition(Condition paramCondition) {
    if (paramCondition == null)
      return; 
    notifyConditions(new Condition[] { paramCondition });
  }
  
  @Deprecated
  public final void notifyConditions(Condition... paramVarArgs) {
    if (!isBound() || paramVarArgs == null)
      return; 
    try {
      getNotificationInterface().notifyConditions(getPackageName(), this.mProvider, paramVarArgs);
    } catch (RemoteException remoteException) {
      Log.v(this.TAG, "Unable to contact notification manager", (Throwable)remoteException);
    } 
  }
  
  public IBinder onBind(Intent paramIntent) {
    if (this.mProvider == null)
      this.mProvider = new Provider(); 
    return this.mProvider;
  }
  
  public boolean isBound() {
    if (!this.mIsConnected)
      Log.w(this.TAG, "Condition provider service not yet bound."); 
    return this.mIsConnected;
  }
  
  public abstract void onConnected();
  
  public abstract void onSubscribe(Uri paramUri);
  
  public abstract void onUnsubscribe(Uri paramUri);
  
  private final class Provider extends IConditionProvider.Stub {
    final ConditionProviderService this$0;
    
    private Provider() {}
    
    public void onConnected() {
      ConditionProviderService.this.mIsConnected = true;
      ConditionProviderService.this.mHandler.obtainMessage(1).sendToTarget();
    }
    
    public void onSubscribe(Uri param1Uri) {
      ConditionProviderService.this.mHandler.obtainMessage(3, param1Uri).sendToTarget();
    }
    
    public void onUnsubscribe(Uri param1Uri) {
      ConditionProviderService.this.mHandler.obtainMessage(4, param1Uri).sendToTarget();
    }
  }
  
  class H extends Handler {
    private static final int ON_CONNECTED = 1;
    
    private static final int ON_SUBSCRIBE = 3;
    
    private static final int ON_UNSUBSCRIBE = 4;
    
    final ConditionProviderService this$0;
    
    private H() {}
    
    public void handleMessage(Message param1Message) {
      String str = null;
      if (!ConditionProviderService.this.mIsConnected)
        return; 
      try {
      
      } finally {
        Exception exception = null;
        String str1 = ConditionProviderService.this.TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Error running ");
        stringBuilder.append(str);
      } 
    }
  }
}
