package android.service.notification;

import android.app.Notification;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.metrics.LogMaker;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.UserHandle;
import com.android.internal.logging.InstanceId;
import java.util.ArrayList;

public class StatusBarNotification implements Parcelable {
  public StatusBarNotification(String paramString1, String paramString2, int paramInt1, String paramString3, int paramInt2, int paramInt3, Notification paramNotification, UserHandle paramUserHandle, String paramString4, long paramLong) {
    if (paramString1 != null) {
      if (paramNotification != null) {
        this.pkg = paramString1;
        this.opPkg = paramString2;
        this.id = paramInt1;
        this.tag = paramString3;
        this.uid = paramInt2;
        this.initialPid = paramInt3;
        this.notification = paramNotification;
        this.user = paramUserHandle;
        this.postTime = paramLong;
        this.overrideGroupKey = paramString4;
        this.key = key();
        this.groupKey = groupKey();
        return;
      } 
      throw null;
    } 
    throw null;
  }
  
  @Deprecated
  public StatusBarNotification(String paramString1, String paramString2, int paramInt1, String paramString3, int paramInt2, int paramInt3, int paramInt4, Notification paramNotification, UserHandle paramUserHandle, long paramLong) {
    if (paramString1 != null) {
      if (paramNotification != null) {
        this.pkg = paramString1;
        this.opPkg = paramString2;
        this.id = paramInt1;
        this.tag = paramString3;
        this.uid = paramInt2;
        this.initialPid = paramInt3;
        this.notification = paramNotification;
        this.user = paramUserHandle;
        this.postTime = paramLong;
        this.key = key();
        this.groupKey = groupKey();
        return;
      } 
      throw null;
    } 
    throw null;
  }
  
  public StatusBarNotification(Parcel paramParcel) {
    this.pkg = paramParcel.readString();
    this.opPkg = paramParcel.readString();
    this.id = paramParcel.readInt();
    if (paramParcel.readInt() != 0) {
      this.tag = paramParcel.readString();
    } else {
      this.tag = null;
    } 
    this.uid = paramParcel.readInt();
    this.initialPid = paramParcel.readInt();
    this.notification = new Notification(paramParcel);
    this.user = UserHandle.readFromParcel(paramParcel);
    this.postTime = paramParcel.readLong();
    if (paramParcel.readInt() != 0)
      this.overrideGroupKey = paramParcel.readString(); 
    if (paramParcel.readInt() != 0)
      this.mInstanceId = InstanceId.CREATOR.createFromParcel(paramParcel); 
    this.key = key();
    this.groupKey = groupKey();
  }
  
  private String key() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.user.getIdentifier());
    stringBuilder.append("|");
    stringBuilder.append(this.pkg);
    stringBuilder.append("|");
    stringBuilder.append(this.id);
    stringBuilder.append("|");
    stringBuilder.append(this.tag);
    stringBuilder.append("|");
    stringBuilder.append(this.uid);
    String str2 = stringBuilder.toString();
    String str1 = str2;
    if (this.overrideGroupKey != null) {
      str1 = str2;
      if (getNotification().isGroupSummary()) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(str2);
        stringBuilder1.append("|");
        stringBuilder1.append(this.overrideGroupKey);
        str1 = stringBuilder1.toString();
      } 
    } 
    return str1;
  }
  
  private String groupKey() {
    if (this.overrideGroupKey != null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(this.user.getIdentifier());
      stringBuilder1.append("|");
      stringBuilder1.append(this.pkg);
      stringBuilder1.append("|g:");
      stringBuilder1.append(this.overrideGroupKey);
      return stringBuilder1.toString();
    } 
    null = getNotification().getGroup();
    String str = getNotification().getSortKey();
    if (null == null && str == null)
      return this.key; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.user.getIdentifier());
    stringBuilder.append("|");
    stringBuilder.append(this.pkg);
    stringBuilder.append("|");
    if (null == null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("c:");
      stringBuilder1.append(this.notification.getChannelId());
      null = stringBuilder1.toString();
    } else {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("g:");
      stringBuilder1.append(null);
      null = stringBuilder1.toString();
    } 
    stringBuilder.append(null);
    return stringBuilder.toString();
  }
  
  public boolean isGroup() {
    if (this.overrideGroupKey != null || isAppGroup())
      return true; 
    return false;
  }
  
  public boolean isAppGroup() {
    if (getNotification().getGroup() != null || getNotification().getSortKey() != null)
      return true; 
    return false;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.pkg);
    paramParcel.writeString(this.opPkg);
    paramParcel.writeInt(this.id);
    if (this.tag != null) {
      paramParcel.writeInt(1);
      paramParcel.writeString(this.tag);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeInt(this.uid);
    paramParcel.writeInt(this.initialPid);
    this.notification.writeToParcel(paramParcel, paramInt);
    this.user.writeToParcel(paramParcel, paramInt);
    paramParcel.writeLong(this.postTime);
    if (this.overrideGroupKey != null) {
      paramParcel.writeInt(1);
      paramParcel.writeString(this.overrideGroupKey);
    } else {
      paramParcel.writeInt(0);
    } 
    if (this.mInstanceId != null) {
      paramParcel.writeInt(1);
      this.mInstanceId.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeInt(0);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<StatusBarNotification> CREATOR = new Parcelable.Creator<StatusBarNotification>() {
      public StatusBarNotification createFromParcel(Parcel param1Parcel) {
        return new StatusBarNotification(param1Parcel);
      }
      
      public StatusBarNotification[] newArray(int param1Int) {
        return new StatusBarNotification[param1Int];
      }
    };
  
  static final int MAX_LOG_TAG_LENGTH = 36;
  
  private String groupKey;
  
  private final int id;
  
  private final int initialPid;
  
  private final String key;
  
  private Context mContext;
  
  private InstanceId mInstanceId;
  
  private final Notification notification;
  
  private final String opPkg;
  
  private String overrideGroupKey;
  
  private final String pkg;
  
  private final long postTime;
  
  private final String tag;
  
  private final int uid;
  
  private final UserHandle user;
  
  public StatusBarNotification cloneLight() {
    Notification notification = new Notification();
    this.notification.cloneInto(notification, false);
    return cloneShallow(notification);
  }
  
  public StatusBarNotification clone() {
    return cloneShallow(this.notification.clone());
  }
  
  StatusBarNotification cloneShallow(Notification paramNotification) {
    StatusBarNotification statusBarNotification = new StatusBarNotification(this.pkg, this.opPkg, this.id, this.tag, this.uid, this.initialPid, paramNotification, this.user, this.overrideGroupKey, this.postTime);
    statusBarNotification.setInstanceId(this.mInstanceId);
    return statusBarNotification;
  }
  
  public String toString() {
    String str1 = this.pkg;
    UserHandle userHandle = this.user;
    int i = this.id;
    String str2 = this.tag, str3 = this.key;
    Notification notification = this.notification;
    return String.format("StatusBarNotification(pkg=%s user=%s id=%d tag=%s key=%s: %s)", new Object[] { str1, userHandle, Integer.valueOf(i), str2, str3, notification });
  }
  
  public boolean isOngoing() {
    boolean bool;
    if ((this.notification.flags & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isClearable() {
    boolean bool;
    if ((this.notification.flags & 0x2) == 0 && (this.notification.flags & 0x20) == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @Deprecated
  public int getUserId() {
    return this.user.getIdentifier();
  }
  
  public int getNormalizedUserId() {
    int i = getUserId();
    int j = i;
    if (i == -1)
      j = 0; 
    return j;
  }
  
  public String getPackageName() {
    return this.pkg;
  }
  
  public int getId() {
    return this.id;
  }
  
  public String getTag() {
    return this.tag;
  }
  
  public int getUid() {
    return this.uid;
  }
  
  public String getOpPkg() {
    return this.opPkg;
  }
  
  public int getInitialPid() {
    return this.initialPid;
  }
  
  public Notification getNotification() {
    return this.notification;
  }
  
  public UserHandle getUser() {
    return this.user;
  }
  
  public long getPostTime() {
    return this.postTime;
  }
  
  public String getKey() {
    return this.key;
  }
  
  public String getGroupKey() {
    return this.groupKey;
  }
  
  public String getGroup() {
    String str = this.overrideGroupKey;
    if (str != null)
      return str; 
    return getNotification().getGroup();
  }
  
  public void setOverrideGroupKey(String paramString) {
    this.overrideGroupKey = paramString;
    this.groupKey = groupKey();
  }
  
  public String getOverrideGroupKey() {
    return this.overrideGroupKey;
  }
  
  public void clearPackageContext() {
    this.mContext = null;
  }
  
  public InstanceId getInstanceId() {
    return this.mInstanceId;
  }
  
  public void setInstanceId(InstanceId paramInstanceId) {
    this.mInstanceId = paramInstanceId;
  }
  
  public Context getPackageContext(Context paramContext) {
    if (this.mContext == null)
      try {
        PackageManager packageManager = paramContext.getPackageManager();
        String str = this.pkg;
        int i = getUserId();
        ApplicationInfo applicationInfo = packageManager.getApplicationInfoAsUser(str, 8192, i);
        this.mContext = paramContext.createApplicationContext(applicationInfo, 4);
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
        this.mContext = null;
      }  
    if (this.mContext == null)
      this.mContext = paramContext; 
    return this.mContext;
  }
  
  public LogMaker getLogMaker() {
    LogMaker logMaker2 = (new LogMaker(0)).setPackageName(getPackageName());
    logMaker2 = logMaker2.addTaggedData(796, Integer.valueOf(getId()));
    logMaker2 = logMaker2.addTaggedData(797, getTag());
    logMaker2 = logMaker2.addTaggedData(857, getChannelIdLogTag());
    logMaker2 = logMaker2.addTaggedData(946, getGroupLogTag());
    boolean bool = getNotification().isGroupSummary();
    LogMaker logMaker3 = logMaker2.addTaggedData(947, Integer.valueOf(bool));
    String str = (getNotification()).category;
    LogMaker logMaker1 = logMaker3.addTaggedData(1641, str);
    if ((getNotification()).extras != null) {
      String str1 = (getNotification()).extras.getString("android.template");
      if (str1 != null && !str1.isEmpty()) {
        int i = str1.hashCode();
        logMaker1.addTaggedData(1745, Integer.valueOf(i));
      } 
      ArrayList<Parcelable> arrayList = (getNotification()).extras.getParcelableArrayList("android.people.list");
      if (arrayList != null && !arrayList.isEmpty())
        logMaker1.addTaggedData(1744, Integer.valueOf(arrayList.size())); 
    } 
    return logMaker1;
  }
  
  public String getShortcutId() {
    return getNotification().getShortcutId();
  }
  
  public String getGroupLogTag() {
    return shortenTag(getGroup());
  }
  
  public String getChannelIdLogTag() {
    if (this.notification.getChannelId() == null)
      return null; 
    return shortenTag(this.notification.getChannelId());
  }
  
  private String shortenTag(String paramString) {
    if (paramString == null || paramString.length() <= 36)
      return paramString; 
    String str = Integer.toHexString(paramString.hashCode());
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString.substring(0, 36 - str.length() - 1));
    stringBuilder.append("-");
    stringBuilder.append(str);
    return stringBuilder.toString();
  }
}
