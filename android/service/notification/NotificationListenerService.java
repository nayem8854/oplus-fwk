package android.service.notification;

import android.annotation.SystemApi;
import android.app.INotificationManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.Person;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ParceledListSlice;
import android.content.pm.ShortcutInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.util.ArrayMap;
import android.util.Log;
import android.widget.RemoteViews;
import com.android.internal.os.SomeArgs;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class NotificationListenerService extends Service {
  private final String TAG = getClass().getSimpleName();
  
  private final Object mLock = new Object();
  
  protected NotificationListenerWrapper mWrapper = null;
  
  private boolean isConnected = false;
  
  public static final int HINT_HOST_DISABLE_CALL_EFFECTS = 4;
  
  public static final int HINT_HOST_DISABLE_EFFECTS = 1;
  
  public static final int HINT_HOST_DISABLE_NOTIFICATION_EFFECTS = 2;
  
  public static final int INTERRUPTION_FILTER_ALARMS = 4;
  
  public static final int INTERRUPTION_FILTER_ALL = 1;
  
  public static final int INTERRUPTION_FILTER_NONE = 3;
  
  public static final int INTERRUPTION_FILTER_PRIORITY = 2;
  
  public static final int INTERRUPTION_FILTER_UNKNOWN = 0;
  
  public static final int NOTIFICATION_CHANNEL_OR_GROUP_ADDED = 1;
  
  public static final int NOTIFICATION_CHANNEL_OR_GROUP_DELETED = 3;
  
  public static final int NOTIFICATION_CHANNEL_OR_GROUP_UPDATED = 2;
  
  public static final int REASON_APP_CANCEL = 8;
  
  public static final int REASON_APP_CANCEL_ALL = 9;
  
  public static final int REASON_CANCEL = 2;
  
  public static final int REASON_CANCEL_ALL = 3;
  
  public static final int REASON_CHANNEL_BANNED = 17;
  
  public static final int REASON_CLICK = 1;
  
  public static final int REASON_ERROR = 4;
  
  public static final int REASON_GROUP_OPTIMIZATION = 13;
  
  public static final int REASON_GROUP_SUMMARY_CANCELED = 12;
  
  public static final int REASON_LISTENER_CANCEL = 10;
  
  public static final int REASON_LISTENER_CANCEL_ALL = 11;
  
  public static final int REASON_PACKAGE_BANNED = 7;
  
  public static final int REASON_PACKAGE_CHANGED = 5;
  
  public static final int REASON_PACKAGE_SUSPENDED = 14;
  
  public static final int REASON_PROFILE_TURNED_OFF = 15;
  
  public static final int REASON_SNOOZED = 18;
  
  public static final int REASON_TIMEOUT = 19;
  
  public static final int REASON_UNAUTOBUNDLED = 16;
  
  public static final int REASON_USER_STOPPED = 6;
  
  public static final String SERVICE_INTERFACE = "android.service.notification.NotificationListenerService";
  
  @Deprecated
  public static final int SUPPRESSED_EFFECT_SCREEN_OFF = 1;
  
  @Deprecated
  public static final int SUPPRESSED_EFFECT_SCREEN_ON = 2;
  
  @SystemApi
  public static final int TRIM_FULL = 0;
  
  @SystemApi
  public static final int TRIM_LIGHT = 1;
  
  protected int mCurrentUser;
  
  private Handler mHandler;
  
  protected INotificationManager mNoMan;
  
  private RankingMap mRankingMap;
  
  protected Context mSystemContext;
  
  protected void attachBaseContext(Context paramContext) {
    super.attachBaseContext(paramContext);
    this.mHandler = new MyHandler(getMainLooper());
  }
  
  public void onNotificationPosted(StatusBarNotification paramStatusBarNotification) {}
  
  public void onNotificationPosted(StatusBarNotification paramStatusBarNotification, RankingMap paramRankingMap) {
    onNotificationPosted(paramStatusBarNotification);
  }
  
  public void onNotificationRemoved(StatusBarNotification paramStatusBarNotification) {}
  
  public void onNotificationRemoved(StatusBarNotification paramStatusBarNotification, RankingMap paramRankingMap) {
    onNotificationRemoved(paramStatusBarNotification);
  }
  
  public void onNotificationRemoved(StatusBarNotification paramStatusBarNotification, RankingMap paramRankingMap, int paramInt) {
    onNotificationRemoved(paramStatusBarNotification, paramRankingMap);
  }
  
  @SystemApi
  public void onNotificationRemoved(StatusBarNotification paramStatusBarNotification, RankingMap paramRankingMap, NotificationStats paramNotificationStats, int paramInt) {
    onNotificationRemoved(paramStatusBarNotification, paramRankingMap, paramInt);
  }
  
  public void onListenerConnected() {}
  
  public void onListenerDisconnected() {}
  
  public void onNotificationRankingUpdate(RankingMap paramRankingMap) {}
  
  public void onListenerHintsChanged(int paramInt) {}
  
  public void onSilentStatusBarIconsVisibilityChanged(boolean paramBoolean) {}
  
  public void onNotificationChannelModified(String paramString, UserHandle paramUserHandle, NotificationChannel paramNotificationChannel, int paramInt) {}
  
  public void onNotificationChannelGroupModified(String paramString, UserHandle paramUserHandle, NotificationChannelGroup paramNotificationChannelGroup, int paramInt) {}
  
  public void onInterruptionFilterChanged(int paramInt) {}
  
  protected final INotificationManager getNotificationInterface() {
    if (this.mNoMan == null) {
      IBinder iBinder = ServiceManager.getService("notification");
      this.mNoMan = INotificationManager.Stub.asInterface(iBinder);
    } 
    return this.mNoMan;
  }
  
  @Deprecated
  public final void cancelNotification(String paramString1, String paramString2, int paramInt) {
    if (!isBound())
      return; 
    try {
      getNotificationInterface().cancelNotificationFromListener(this.mWrapper, paramString1, paramString2, paramInt);
    } catch (RemoteException remoteException) {
      Log.v(this.TAG, "Unable to contact notification manager", (Throwable)remoteException);
    } 
  }
  
  public final void cancelNotification(String paramString) {
    if (!isBound())
      return; 
    try {
      getNotificationInterface().cancelNotificationsFromListener(this.mWrapper, new String[] { paramString });
    } catch (RemoteException remoteException) {
      Log.v(this.TAG, "Unable to contact notification manager", (Throwable)remoteException);
    } 
  }
  
  public final void cancelAllNotifications() {
    cancelNotifications(null);
  }
  
  public final void cancelNotifications(String[] paramArrayOfString) {
    if (!isBound())
      return; 
    try {
      getNotificationInterface().cancelNotificationsFromListener(this.mWrapper, paramArrayOfString);
    } catch (RemoteException remoteException) {
      Log.v(this.TAG, "Unable to contact notification manager", (Throwable)remoteException);
    } 
  }
  
  @SystemApi
  public final void snoozeNotification(String paramString1, String paramString2) {
    if (!isBound())
      return; 
    try {
      getNotificationInterface().snoozeNotificationUntilContextFromListener(this.mWrapper, paramString1, paramString2);
    } catch (RemoteException remoteException) {
      Log.v(this.TAG, "Unable to contact notification manager", (Throwable)remoteException);
    } 
  }
  
  public final void snoozeNotification(String paramString, long paramLong) {
    if (!isBound())
      return; 
    try {
      getNotificationInterface().snoozeNotificationUntilFromListener(this.mWrapper, paramString, paramLong);
    } catch (RemoteException remoteException) {
      Log.v(this.TAG, "Unable to contact notification manager", (Throwable)remoteException);
    } 
  }
  
  public final void setNotificationsShown(String[] paramArrayOfString) {
    if (!isBound())
      return; 
    try {
      getNotificationInterface().setNotificationsShownFromListener(this.mWrapper, paramArrayOfString);
    } catch (RemoteException remoteException) {
      Log.v(this.TAG, "Unable to contact notification manager", (Throwable)remoteException);
    } 
  }
  
  public final void updateNotificationChannel(String paramString, UserHandle paramUserHandle, NotificationChannel paramNotificationChannel) {
    if (!isBound())
      return; 
    try {
      getNotificationInterface().updateNotificationChannelFromPrivilegedListener(this.mWrapper, paramString, paramUserHandle, paramNotificationChannel);
      return;
    } catch (RemoteException remoteException) {
      Log.v(this.TAG, "Unable to contact notification manager", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public final List<NotificationChannel> getNotificationChannels(String paramString, UserHandle paramUserHandle) {
    if (!isBound())
      return null; 
    try {
      ParceledListSlice parceledListSlice = getNotificationInterface().getNotificationChannelsFromPrivilegedListener(this.mWrapper, paramString, paramUserHandle);
      return 
        parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      Log.v(this.TAG, "Unable to contact notification manager", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public final List<NotificationChannelGroup> getNotificationChannelGroups(String paramString, UserHandle paramUserHandle) {
    if (!isBound())
      return null; 
    try {
      ParceledListSlice parceledListSlice = getNotificationInterface().getNotificationChannelGroupsFromPrivilegedListener(this.mWrapper, paramString, paramUserHandle);
      return 
        parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      Log.v(this.TAG, "Unable to contact notification manager", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public final void setOnNotificationPostedTrim(int paramInt) {
    if (!isBound())
      return; 
    try {
      getNotificationInterface().setOnNotificationPostedTrimFromListener(this.mWrapper, paramInt);
    } catch (RemoteException remoteException) {
      Log.v(this.TAG, "Unable to contact notification manager", (Throwable)remoteException);
    } 
  }
  
  public StatusBarNotification[] getActiveNotifications() {
    StatusBarNotification[] arrayOfStatusBarNotification = getActiveNotifications(null, 0);
    if (arrayOfStatusBarNotification == null)
      arrayOfStatusBarNotification = new StatusBarNotification[0]; 
    return arrayOfStatusBarNotification;
  }
  
  public final StatusBarNotification[] getSnoozedNotifications() {
    try {
      INotificationManager iNotificationManager = getNotificationInterface();
      NotificationListenerWrapper notificationListenerWrapper = this.mWrapper;
      ParceledListSlice<StatusBarNotification> parceledListSlice = iNotificationManager.getSnoozedNotificationsFromListener(notificationListenerWrapper, 0);
      return cleanUpNotificationList(parceledListSlice);
    } catch (RemoteException remoteException) {
      Log.v(this.TAG, "Unable to contact notification manager", (Throwable)remoteException);
      return null;
    } 
  }
  
  @SystemApi
  public StatusBarNotification[] getActiveNotifications(int paramInt) {
    StatusBarNotification[] arrayOfStatusBarNotification = getActiveNotifications(null, paramInt);
    if (arrayOfStatusBarNotification == null)
      arrayOfStatusBarNotification = new StatusBarNotification[0]; 
    return arrayOfStatusBarNotification;
  }
  
  public StatusBarNotification[] getActiveNotifications(String[] paramArrayOfString) {
    StatusBarNotification[] arrayOfStatusBarNotification = getActiveNotifications(paramArrayOfString, 0);
    if (arrayOfStatusBarNotification == null)
      arrayOfStatusBarNotification = new StatusBarNotification[0]; 
    return arrayOfStatusBarNotification;
  }
  
  @SystemApi
  public StatusBarNotification[] getActiveNotifications(String[] paramArrayOfString, int paramInt) {
    if (!isBound())
      return null; 
    try {
      INotificationManager iNotificationManager = getNotificationInterface();
      NotificationListenerWrapper notificationListenerWrapper = this.mWrapper;
      ParceledListSlice<StatusBarNotification> parceledListSlice = iNotificationManager.getActiveNotificationsFromListener(notificationListenerWrapper, paramArrayOfString, paramInt);
      return cleanUpNotificationList(parceledListSlice);
    } catch (RemoteException remoteException) {
      Log.v(this.TAG, "Unable to contact notification manager", (Throwable)remoteException);
      return null;
    } 
  }
  
  private StatusBarNotification[] cleanUpNotificationList(ParceledListSlice<StatusBarNotification> paramParceledListSlice) {
    if (paramParceledListSlice == null || paramParceledListSlice.getList() == null)
      return new StatusBarNotification[0]; 
    List<StatusBarNotification> list = paramParceledListSlice.getList();
    Notification notification = null;
    int i = list.size();
    ArrayList<StatusBarNotification> arrayList;
    for (byte b = 0; b < i; b++, arrayList = arrayList1) {
      ArrayList<StatusBarNotification> arrayList1;
      StatusBarNotification statusBarNotification = list.get(b);
      Notification notification1 = statusBarNotification.getNotification();
      try {
        createLegacyIconExtras(notification1);
        maybePopulateRemoteViews(notification1);
        maybePopulatePeople(notification1);
        notification1 = notification;
      } catch (IllegalArgumentException illegalArgumentException) {
        Notification notification2 = notification;
        if (notification == null)
          arrayList1 = new ArrayList(i); 
        arrayList1.add(statusBarNotification);
        String str1 = this.TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("get(Active/Snoozed)Notifications: can't rebuild notification from ");
        stringBuilder.append(statusBarNotification.getPackageName());
        String str2 = stringBuilder.toString();
        Log.w(str1, str2);
      } 
    } 
    if (arrayList != null)
      list.removeAll(arrayList); 
    return list.<StatusBarNotification>toArray(new StatusBarNotification[list.size()]);
  }
  
  public final int getCurrentListenerHints() {
    if (!isBound())
      return 0; 
    try {
      return getNotificationInterface().getHintsFromListener(this.mWrapper);
    } catch (RemoteException remoteException) {
      Log.v(this.TAG, "Unable to contact notification manager", (Throwable)remoteException);
      return 0;
    } 
  }
  
  public final int getCurrentInterruptionFilter() {
    if (!isBound())
      return 0; 
    try {
      return getNotificationInterface().getInterruptionFilterFromListener(this.mWrapper);
    } catch (RemoteException remoteException) {
      Log.v(this.TAG, "Unable to contact notification manager", (Throwable)remoteException);
      return 0;
    } 
  }
  
  public final void clearRequestedListenerHints() {
    if (!isBound())
      return; 
    try {
      getNotificationInterface().clearRequestedListenerHints(this.mWrapper);
    } catch (RemoteException remoteException) {
      Log.v(this.TAG, "Unable to contact notification manager", (Throwable)remoteException);
    } 
  }
  
  public final void requestListenerHints(int paramInt) {
    if (!isBound())
      return; 
    try {
      getNotificationInterface().requestHintsFromListener(this.mWrapper, paramInt);
    } catch (RemoteException remoteException) {
      Log.v(this.TAG, "Unable to contact notification manager", (Throwable)remoteException);
    } 
  }
  
  public final void requestInterruptionFilter(int paramInt) {
    if (!isBound())
      return; 
    try {
      INotificationManager iNotificationManager = getNotificationInterface();
      NotificationListenerWrapper notificationListenerWrapper = this.mWrapper;
      iNotificationManager.requestInterruptionFilterFromListener(notificationListenerWrapper, paramInt);
    } catch (RemoteException remoteException) {
      Log.v(this.TAG, "Unable to contact notification manager", (Throwable)remoteException);
    } 
  }
  
  public RankingMap getCurrentRanking() {
    synchronized (this.mLock) {
      return this.mRankingMap;
    } 
  }
  
  public IBinder onBind(Intent paramIntent) {
    if (this.mWrapper == null)
      this.mWrapper = new NotificationListenerWrapper(); 
    return this.mWrapper;
  }
  
  protected boolean isBound() {
    if (this.mWrapper == null) {
      Log.w(this.TAG, "Notification listener service not yet bound.");
      return false;
    } 
    return true;
  }
  
  public void onDestroy() {
    onListenerDisconnected();
    super.onDestroy();
  }
  
  @SystemApi
  public void registerAsSystemService(Context paramContext, ComponentName paramComponentName, int paramInt) throws RemoteException {
    if (this.mWrapper == null)
      this.mWrapper = new NotificationListenerWrapper(); 
    this.mSystemContext = paramContext;
    INotificationManager iNotificationManager = getNotificationInterface();
    this.mHandler = new MyHandler(paramContext.getMainLooper());
    this.mCurrentUser = paramInt;
    iNotificationManager.registerListener(this.mWrapper, paramComponentName, paramInt);
  }
  
  @SystemApi
  public void unregisterAsSystemService() throws RemoteException {
    if (this.mWrapper != null) {
      INotificationManager iNotificationManager = getNotificationInterface();
      iNotificationManager.unregisterListener(this.mWrapper, this.mCurrentUser);
    } 
  }
  
  public static void requestRebind(ComponentName paramComponentName) {
    IBinder iBinder = ServiceManager.getService("notification");
    INotificationManager iNotificationManager = INotificationManager.Stub.asInterface(iBinder);
    try {
      iNotificationManager.requestBindListener(paramComponentName);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public final void requestUnbind() {
    if (this.mWrapper != null) {
      INotificationManager iNotificationManager = getNotificationInterface();
      try {
        iNotificationManager.requestUnbindListener(this.mWrapper);
        this.isConnected = false;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
  }
  
  public final void createLegacyIconExtras(Notification paramNotification) {
    if ((getContext().getApplicationInfo()).targetSdkVersion < 23) {
      Icon icon1 = paramNotification.getSmallIcon();
      Icon icon2 = paramNotification.getLargeIcon();
      if (icon1 != null && icon1.getType() == 2) {
        paramNotification.extras.putInt("android.icon", icon1.getResId());
        paramNotification.icon = icon1.getResId();
      } 
      if (icon2 != null) {
        Drawable drawable = icon2.loadDrawable(getContext());
        if (drawable != null && drawable instanceof BitmapDrawable) {
          Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();
          paramNotification.extras.putParcelable("android.largeIcon", (Parcelable)bitmap);
          paramNotification.largeIcon = bitmap;
        } 
      } 
    } 
  }
  
  private void maybePopulateRemoteViews(Notification paramNotification) {
    if ((getContext().getApplicationInfo()).targetSdkVersion < 24) {
      Notification.Builder builder = Notification.Builder.recoverBuilder(getContext(), paramNotification);
      RemoteViews remoteViews2 = builder.createContentView();
      RemoteViews remoteViews3 = builder.createBigContentView();
      RemoteViews remoteViews1 = builder.createHeadsUpContentView();
      paramNotification.contentView = remoteViews2;
      paramNotification.bigContentView = remoteViews3;
      paramNotification.headsUpContentView = remoteViews1;
    } 
  }
  
  private void maybePopulatePeople(Notification paramNotification) {
    if ((getContext().getApplicationInfo()).targetSdkVersion < 28) {
      ArrayList<Parcelable> arrayList = paramNotification.extras.getParcelableArrayList("android.people.list");
      if (arrayList != null && arrayList.isEmpty()) {
        int i = arrayList.size();
        String[] arrayOfString = new String[i];
        for (byte b = 0; b < i; b++) {
          Person person = (Person)arrayList.get(b);
          arrayOfString[b] = person.resolveToLegacyUri();
        } 
        paramNotification.extras.putStringArray("android.people", arrayOfString);
      } 
    } 
  }
  
  protected class NotificationListenerWrapper extends INotificationListener.Stub {
    final NotificationListenerService this$0;
    
    public void onNotificationPosted(IStatusBarNotificationHolder param1IStatusBarNotificationHolder, NotificationRankingUpdate param1NotificationRankingUpdate) {
      try {
        StatusBarNotification statusBarNotification = param1IStatusBarNotificationHolder.get();
        if (statusBarNotification == null) {
          Log.w(NotificationListenerService.this.TAG, "onNotificationPosted: Error receiving StatusBarNotification");
          return;
        } 
        try {
          NotificationListenerService.this.createLegacyIconExtras(statusBarNotification.getNotification());
          NotificationListenerService.this.maybePopulateRemoteViews(statusBarNotification.getNotification());
          NotificationListenerService.this.maybePopulatePeople(statusBarNotification.getNotification());
        } catch (IllegalArgumentException illegalArgumentException) {
          String str = NotificationListenerService.this.TAG;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("onNotificationPosted: can't rebuild notification from ");
          stringBuilder.append(statusBarNotification.getPackageName());
          null = stringBuilder.toString();
          Log.w(str, null);
          null = null;
        } 
        synchronized (NotificationListenerService.this.mLock) {
          NotificationListenerService.this.applyUpdateLocked(param1NotificationRankingUpdate);
          if (null != null) {
            SomeArgs someArgs = SomeArgs.obtain();
            someArgs.arg1 = null;
            someArgs.arg2 = NotificationListenerService.this.mRankingMap;
            Message message = NotificationListenerService.this.mHandler.obtainMessage(1, someArgs);
            message.sendToTarget();
          } else {
            Handler handler = NotificationListenerService.this.mHandler;
            NotificationListenerService notificationListenerService = NotificationListenerService.this;
            NotificationListenerService.RankingMap rankingMap = notificationListenerService.mRankingMap;
            Message message = handler.obtainMessage(4, rankingMap);
            message.sendToTarget();
          } 
          return;
        } 
      } catch (RemoteException remoteException) {
        Log.w(NotificationListenerService.this.TAG, "onNotificationPosted: Error receiving StatusBarNotification", (Throwable)remoteException);
        return;
      } 
    }
    
    public void onNotificationRemoved(IStatusBarNotificationHolder param1IStatusBarNotificationHolder, NotificationRankingUpdate param1NotificationRankingUpdate, NotificationStats param1NotificationStats, int param1Int) {
      try {
        StatusBarNotification statusBarNotification = param1IStatusBarNotificationHolder.get();
        if (statusBarNotification == null) {
          Log.w(NotificationListenerService.this.TAG, "onNotificationRemoved: Error receiving StatusBarNotification");
          return;
        } 
        synchronized (NotificationListenerService.this.mLock) {
          NotificationListenerService.this.applyUpdateLocked(param1NotificationRankingUpdate);
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = statusBarNotification;
          someArgs.arg2 = NotificationListenerService.this.mRankingMap;
          someArgs.arg3 = Integer.valueOf(param1Int);
          someArgs.arg4 = param1NotificationStats;
          Message message = NotificationListenerService.this.mHandler.obtainMessage(2, someArgs);
          message.sendToTarget();
          return;
        } 
      } catch (RemoteException remoteException) {
        Log.w(NotificationListenerService.this.TAG, "onNotificationRemoved: Error receiving StatusBarNotification", (Throwable)remoteException);
        return;
      } 
    }
    
    public void onListenerConnected(NotificationRankingUpdate param1NotificationRankingUpdate) {
      synchronized (NotificationListenerService.this.mLock) {
        NotificationListenerService.this.applyUpdateLocked(param1NotificationRankingUpdate);
        NotificationListenerService.access$602(NotificationListenerService.this, true);
        NotificationListenerService.this.mHandler.obtainMessage(3).sendToTarget();
        return;
      } 
    }
    
    public void onNotificationRankingUpdate(NotificationRankingUpdate param1NotificationRankingUpdate) throws RemoteException {
      synchronized (NotificationListenerService.this.mLock) {
        NotificationListenerService.this.applyUpdateLocked(param1NotificationRankingUpdate);
        Handler handler = NotificationListenerService.this.mHandler;
        NotificationListenerService notificationListenerService = NotificationListenerService.this;
        NotificationListenerService.RankingMap rankingMap = notificationListenerService.mRankingMap;
        Message message = handler.obtainMessage(4, rankingMap);
        message.sendToTarget();
        return;
      } 
    }
    
    public void onListenerHintsChanged(int param1Int) throws RemoteException {
      Message message = NotificationListenerService.this.mHandler.obtainMessage(5, param1Int, 0);
      message.sendToTarget();
    }
    
    public void onInterruptionFilterChanged(int param1Int) throws RemoteException {
      Message message = NotificationListenerService.this.mHandler.obtainMessage(6, param1Int, 0);
      message.sendToTarget();
    }
    
    public void onNotificationEnqueuedWithChannel(IStatusBarNotificationHolder param1IStatusBarNotificationHolder, NotificationChannel param1NotificationChannel) throws RemoteException {}
    
    public void onNotificationsSeen(List<String> param1List) throws RemoteException {}
    
    public void onPanelRevealed(int param1Int) throws RemoteException {}
    
    public void onPanelHidden() throws RemoteException {}
    
    public void onNotificationVisibilityChanged(String param1String, boolean param1Boolean) {}
    
    public void onNotificationSnoozedUntilContext(IStatusBarNotificationHolder param1IStatusBarNotificationHolder, String param1String) throws RemoteException {}
    
    public void onNotificationExpansionChanged(String param1String, boolean param1Boolean1, boolean param1Boolean2) {}
    
    public void onNotificationDirectReply(String param1String) {}
    
    public void onSuggestedReplySent(String param1String, CharSequence param1CharSequence, int param1Int) {}
    
    public void onActionClicked(String param1String, Notification.Action param1Action, int param1Int) {}
    
    public void onAllowedAdjustmentsChanged() {}
    
    public void onNotificationChannelModification(String param1String, UserHandle param1UserHandle, NotificationChannel param1NotificationChannel, int param1Int) {
      SomeArgs someArgs = SomeArgs.obtain();
      someArgs.arg1 = param1String;
      someArgs.arg2 = param1UserHandle;
      someArgs.arg3 = param1NotificationChannel;
      someArgs.arg4 = Integer.valueOf(param1Int);
      Message message = NotificationListenerService.this.mHandler.obtainMessage(7, someArgs);
      message.sendToTarget();
    }
    
    public void onNotificationChannelGroupModification(String param1String, UserHandle param1UserHandle, NotificationChannelGroup param1NotificationChannelGroup, int param1Int) {
      SomeArgs someArgs = SomeArgs.obtain();
      someArgs.arg1 = param1String;
      someArgs.arg2 = param1UserHandle;
      someArgs.arg3 = param1NotificationChannelGroup;
      someArgs.arg4 = Integer.valueOf(param1Int);
      Message message = NotificationListenerService.this.mHandler.obtainMessage(8, someArgs);
      message.sendToTarget();
    }
    
    public void onStatusBarIconsBehaviorChanged(boolean param1Boolean) {
      Handler handler = NotificationListenerService.this.mHandler;
      Message message = handler.obtainMessage(9, Boolean.valueOf(param1Boolean));
      message.sendToTarget();
    }
  }
  
  public final void applyUpdateLocked(NotificationRankingUpdate paramNotificationRankingUpdate) {
    this.mRankingMap = paramNotificationRankingUpdate.getRankingMap();
  }
  
  protected Context getContext() {
    Context context = this.mSystemContext;
    if (context != null)
      return context; 
    return (Context)this;
  }
  
  class Ranking {
    private static final int PARCEL_VERSION = 2;
    
    public static final int USER_SENTIMENT_NEGATIVE = -1;
    
    public static final int USER_SENTIMENT_NEUTRAL = 0;
    
    public static final int USER_SENTIMENT_POSITIVE = 1;
    
    public static final int VISIBILITY_NO_OVERRIDE = -1000;
    
    private boolean mCanBubble;
    
    private NotificationChannel mChannel;
    
    private boolean mHidden;
    
    private int mImportance;
    
    private CharSequence mImportanceExplanation;
    
    private boolean mIsAmbient;
    
    private boolean mIsBubble;
    
    private boolean mIsConversation;
    
    private String mKey;
    
    private long mLastAudiblyAlertedMs;
    
    private boolean mMatchesInterruptionFilter;
    
    private boolean mNoisy;
    
    private String mOverrideGroupKey;
    
    private ArrayList<String> mOverridePeople;
    
    private int mRank = -1;
    
    private ShortcutInfo mShortcutInfo;
    
    private boolean mShowBadge;
    
    private ArrayList<Notification.Action> mSmartActions;
    
    private ArrayList<CharSequence> mSmartReplies;
    
    private ArrayList<SnoozeCriterion> mSnoozeCriteria;
    
    private int mSuppressedVisualEffects;
    
    private int mUserSentiment = 0;
    
    private int mVisibilityOverride;
    
    private boolean mVisuallyInterruptive;
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      long l = param1Parcel.dataPosition();
      param1Parcel.writeInt(2);
      param1Parcel.writeString(this.mKey);
      param1Parcel.writeInt(this.mRank);
      param1Parcel.writeBoolean(this.mIsAmbient);
      param1Parcel.writeBoolean(this.mMatchesInterruptionFilter);
      param1Parcel.writeInt(this.mVisibilityOverride);
      param1Parcel.writeInt(this.mSuppressedVisualEffects);
      param1Parcel.writeInt(this.mImportance);
      param1Parcel.writeCharSequence(this.mImportanceExplanation);
      param1Parcel.writeString(this.mOverrideGroupKey);
      param1Parcel.writeParcelable((Parcelable)this.mChannel, param1Int);
      param1Parcel.writeStringList(this.mOverridePeople);
      param1Parcel.writeTypedList(this.mSnoozeCriteria, param1Int);
      param1Parcel.writeBoolean(this.mShowBadge);
      param1Parcel.writeInt(this.mUserSentiment);
      param1Parcel.writeBoolean(this.mHidden);
      param1Parcel.writeLong(this.mLastAudiblyAlertedMs);
      param1Parcel.writeBoolean(this.mNoisy);
      param1Parcel.writeTypedList(this.mSmartActions, param1Int);
      param1Parcel.writeCharSequenceList(this.mSmartReplies);
      param1Parcel.writeBoolean(this.mCanBubble);
      param1Parcel.writeBoolean(this.mVisuallyInterruptive);
      param1Parcel.writeBoolean(this.mIsConversation);
      param1Parcel.writeParcelable((Parcelable)this.mShortcutInfo, param1Int);
      param1Parcel.writeBoolean(this.mIsBubble);
    }
    
    public Ranking(NotificationListenerService this$0) {
      ClassLoader classLoader = getClass().getClassLoader();
      int i = this$0.readInt();
      if (i == 2) {
        this.mKey = this$0.readString();
        this.mRank = this$0.readInt();
        this.mIsAmbient = this$0.readBoolean();
        this.mMatchesInterruptionFilter = this$0.readBoolean();
        this.mVisibilityOverride = this$0.readInt();
        this.mSuppressedVisualEffects = this$0.readInt();
        this.mImportance = this$0.readInt();
        this.mImportanceExplanation = this$0.readCharSequence();
        this.mOverrideGroupKey = this$0.readString();
        this.mChannel = this$0.<NotificationChannel>readParcelable(classLoader);
        this.mOverridePeople = this$0.createStringArrayList();
        this.mSnoozeCriteria = this$0.createTypedArrayList(SnoozeCriterion.CREATOR);
        this.mShowBadge = this$0.readBoolean();
        this.mUserSentiment = this$0.readInt();
        this.mHidden = this$0.readBoolean();
        this.mLastAudiblyAlertedMs = this$0.readLong();
        this.mNoisy = this$0.readBoolean();
        this.mSmartActions = this$0.createTypedArrayList(Notification.Action.CREATOR);
        this.mSmartReplies = this$0.readCharSequenceList();
        this.mCanBubble = this$0.readBoolean();
        this.mVisuallyInterruptive = this$0.readBoolean();
        this.mIsConversation = this$0.readBoolean();
        this.mShortcutInfo = this$0.<ShortcutInfo>readParcelable(classLoader);
        this.mIsBubble = this$0.readBoolean();
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("malformed Ranking parcel: ");
      stringBuilder.append(this$0);
      stringBuilder.append(" version ");
      stringBuilder.append(i);
      stringBuilder.append(", expected ");
      stringBuilder.append(2);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public String getKey() {
      return this.mKey;
    }
    
    public int getRank() {
      return this.mRank;
    }
    
    public boolean isAmbient() {
      return this.mIsAmbient;
    }
    
    public int getVisibilityOverride() {
      return this.mVisibilityOverride;
    }
    
    public int getSuppressedVisualEffects() {
      return this.mSuppressedVisualEffects;
    }
    
    public boolean matchesInterruptionFilter() {
      return this.mMatchesInterruptionFilter;
    }
    
    public int getImportance() {
      return this.mImportance;
    }
    
    public CharSequence getImportanceExplanation() {
      return this.mImportanceExplanation;
    }
    
    public String getOverrideGroupKey() {
      return this.mOverrideGroupKey;
    }
    
    public NotificationChannel getChannel() {
      return this.mChannel;
    }
    
    public int getUserSentiment() {
      return this.mUserSentiment;
    }
    
    @SystemApi
    public List<String> getAdditionalPeople() {
      return this.mOverridePeople;
    }
    
    @SystemApi
    public List<SnoozeCriterion> getSnoozeCriteria() {
      return this.mSnoozeCriteria;
    }
    
    public List<Notification.Action> getSmartActions() {
      return this.mSmartActions;
    }
    
    public List<CharSequence> getSmartReplies() {
      return this.mSmartReplies;
    }
    
    public boolean canShowBadge() {
      return this.mShowBadge;
    }
    
    public boolean isSuspended() {
      return this.mHidden;
    }
    
    public long getLastAudiblyAlertedMillis() {
      return this.mLastAudiblyAlertedMs;
    }
    
    public boolean canBubble() {
      return this.mCanBubble;
    }
    
    public boolean visuallyInterruptive() {
      return this.mVisuallyInterruptive;
    }
    
    public boolean isNoisy() {
      return this.mNoisy;
    }
    
    public boolean isConversation() {
      return this.mIsConversation;
    }
    
    public boolean isBubble() {
      return this.mIsBubble;
    }
    
    public ShortcutInfo getShortcutInfo() {
      return this.mShortcutInfo;
    }
    
    public void populate(String param1String1, int param1Int1, boolean param1Boolean1, int param1Int2, int param1Int3, int param1Int4, CharSequence param1CharSequence, String param1String2, NotificationChannel param1NotificationChannel, ArrayList<String> param1ArrayList, ArrayList<SnoozeCriterion> param1ArrayList1, boolean param1Boolean2, int param1Int5, boolean param1Boolean3, long param1Long, boolean param1Boolean4, ArrayList<Notification.Action> param1ArrayList2, ArrayList<CharSequence> param1ArrayList3, boolean param1Boolean5, boolean param1Boolean6, boolean param1Boolean7, ShortcutInfo param1ShortcutInfo, boolean param1Boolean8) {
      boolean bool;
      this.mKey = param1String1;
      this.mRank = param1Int1;
      if (param1Int4 < 2) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mIsAmbient = bool;
      this.mMatchesInterruptionFilter = param1Boolean1;
      this.mVisibilityOverride = param1Int2;
      this.mSuppressedVisualEffects = param1Int3;
      this.mImportance = param1Int4;
      this.mImportanceExplanation = param1CharSequence;
      this.mOverrideGroupKey = param1String2;
      this.mChannel = param1NotificationChannel;
      this.mOverridePeople = param1ArrayList;
      this.mSnoozeCriteria = param1ArrayList1;
      this.mShowBadge = param1Boolean2;
      this.mUserSentiment = param1Int5;
      this.mHidden = param1Boolean3;
      this.mLastAudiblyAlertedMs = param1Long;
      this.mNoisy = param1Boolean4;
      this.mSmartActions = param1ArrayList2;
      this.mSmartReplies = param1ArrayList3;
      this.mCanBubble = param1Boolean5;
      this.mVisuallyInterruptive = param1Boolean6;
      this.mIsConversation = param1Boolean7;
      this.mShortcutInfo = param1ShortcutInfo;
      this.mIsBubble = param1Boolean8;
    }
    
    public void populate(Ranking param1Ranking) {
      populate(param1Ranking.mKey, param1Ranking.mRank, param1Ranking.mMatchesInterruptionFilter, param1Ranking.mVisibilityOverride, param1Ranking.mSuppressedVisualEffects, param1Ranking.mImportance, param1Ranking.mImportanceExplanation, param1Ranking.mOverrideGroupKey, param1Ranking.mChannel, param1Ranking.mOverridePeople, param1Ranking.mSnoozeCriteria, param1Ranking.mShowBadge, param1Ranking.mUserSentiment, param1Ranking.mHidden, param1Ranking.mLastAudiblyAlertedMs, param1Ranking.mNoisy, param1Ranking.mSmartActions, param1Ranking.mSmartReplies, param1Ranking.mCanBubble, param1Ranking.mVisuallyInterruptive, param1Ranking.mIsConversation, param1Ranking.mShortcutInfo, param1Ranking.mIsBubble);
    }
    
    public static String importanceToString(int param1Int) {
      if (param1Int != -1000) {
        if (param1Int != 0) {
          if (param1Int != 1) {
            if (param1Int != 2) {
              if (param1Int != 3) {
                if (param1Int != 4 && param1Int != 5) {
                  StringBuilder stringBuilder = new StringBuilder();
                  stringBuilder.append("UNKNOWN(");
                  stringBuilder.append(String.valueOf(param1Int));
                  stringBuilder.append(")");
                  return stringBuilder.toString();
                } 
                return "HIGH";
              } 
              return "DEFAULT";
            } 
            return "LOW";
          } 
          return "MIN";
        } 
        return "NONE";
      } 
      return "UNSPECIFIED";
    }
    
    public boolean equals(Object<String> param1Object) {
      null = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      Ranking ranking = (Ranking)param1Object;
      if (Objects.equals(this.mKey, ranking.mKey)) {
        int i = this.mRank;
        if (Objects.equals(Integer.valueOf(i), Integer.valueOf(ranking.mRank))) {
          boolean bool = this.mMatchesInterruptionFilter;
          if (Objects.equals(Boolean.valueOf(bool), Boolean.valueOf(ranking.mMatchesInterruptionFilter))) {
            i = this.mVisibilityOverride;
            if (Objects.equals(Integer.valueOf(i), Integer.valueOf(ranking.mVisibilityOverride))) {
              i = this.mSuppressedVisualEffects;
              if (Objects.equals(Integer.valueOf(i), Integer.valueOf(ranking.mSuppressedVisualEffects))) {
                i = this.mImportance;
                if (Objects.equals(Integer.valueOf(i), Integer.valueOf(ranking.mImportance))) {
                  CharSequence charSequence = this.mImportanceExplanation;
                  param1Object = (Object<String>)ranking.mImportanceExplanation;
                  if (Objects.equals(charSequence, param1Object)) {
                    param1Object = (Object<String>)this.mOverrideGroupKey;
                    charSequence = ranking.mOverrideGroupKey;
                    if (Objects.equals(param1Object, charSequence)) {
                      param1Object = (Object<String>)this.mChannel;
                      NotificationChannel notificationChannel = ranking.mChannel;
                      if (Objects.equals(param1Object, notificationChannel)) {
                        param1Object = (Object<String>)this.mOverridePeople;
                        ArrayList<String> arrayList = ranking.mOverridePeople;
                        if (Objects.equals(param1Object, arrayList)) {
                          ArrayList<SnoozeCriterion> arrayList2 = this.mSnoozeCriteria, arrayList1 = ranking.mSnoozeCriteria;
                          if (Objects.equals(arrayList2, arrayList1)) {
                            bool = this.mShowBadge;
                            if (Objects.equals(Boolean.valueOf(bool), Boolean.valueOf(ranking.mShowBadge))) {
                              i = this.mUserSentiment;
                              if (Objects.equals(Integer.valueOf(i), Integer.valueOf(ranking.mUserSentiment))) {
                                bool = this.mHidden;
                                if (Objects.equals(Boolean.valueOf(bool), Boolean.valueOf(ranking.mHidden))) {
                                  long l = this.mLastAudiblyAlertedMs;
                                  if (Objects.equals(Long.valueOf(l), Long.valueOf(ranking.mLastAudiblyAlertedMs))) {
                                    bool = this.mNoisy;
                                    if (Objects.equals(Boolean.valueOf(bool), Boolean.valueOf(ranking.mNoisy))) {
                                      int j;
                                      ArrayList<Notification.Action> arrayList3 = this.mSmartActions;
                                      if (arrayList3 == null) {
                                        i = 0;
                                      } else {
                                        i = arrayList3.size();
                                      } 
                                      arrayList3 = ranking.mSmartActions;
                                      if (arrayList3 == null) {
                                        j = 0;
                                      } else {
                                        j = arrayList3.size();
                                      } 
                                      if (i == j) {
                                        ArrayList<CharSequence> arrayList4 = this.mSmartReplies, arrayList5 = ranking.mSmartReplies;
                                        if (Objects.equals(arrayList4, arrayList5)) {
                                          bool = this.mCanBubble;
                                          if (Objects.equals(Boolean.valueOf(bool), Boolean.valueOf(ranking.mCanBubble))) {
                                            bool = this.mVisuallyInterruptive;
                                            if (Objects.equals(Boolean.valueOf(bool), Boolean.valueOf(ranking.mVisuallyInterruptive))) {
                                              bool = this.mIsConversation;
                                              if (Objects.equals(Boolean.valueOf(bool), Boolean.valueOf(ranking.mIsConversation))) {
                                                Integer integer1;
                                                String str1;
                                                Integer integer2;
                                                String str2;
                                                ShortcutInfo shortcutInfo1 = this.mShortcutInfo;
                                                if (shortcutInfo1 == null) {
                                                  integer1 = Integer.valueOf(0);
                                                } else {
                                                  str1 = integer1.getId();
                                                } 
                                                ShortcutInfo shortcutInfo2 = ranking.mShortcutInfo;
                                                if (shortcutInfo2 == null) {
                                                  integer2 = Integer.valueOf(0);
                                                } else {
                                                  str2 = integer2.getId();
                                                } 
                                                if (Objects.equals(str1, str2)) {
                                                  bool = this.mIsBubble;
                                                  if (Objects.equals(Boolean.valueOf(bool), Boolean.valueOf(ranking.mIsBubble)))
                                                    return null; 
                                                } 
                                              } 
                                            } 
                                          } 
                                        } 
                                      } 
                                    } 
                                  } 
                                } 
                              } 
                            } 
                          } 
                        } 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
      return false;
    }
    
    public Ranking() {}
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface UserSentiment {}
  }
  
  class RankingMap implements Parcelable {
    private ArrayList<String> mOrderedKeys = new ArrayList<>();
    
    private ArrayMap<String, NotificationListenerService.Ranking> mRankings = new ArrayMap();
    
    public RankingMap() {
      for (byte b = 0; b < this$0.length; b++) {
        String str = this$0[b].getKey();
        this.mOrderedKeys.add(str);
        this.mRankings.put(str, this$0[b]);
      } 
    }
    
    private RankingMap(NotificationListenerService this$0) {
      getClass().getClassLoader();
      int i = this$0.readInt();
      this.mOrderedKeys.ensureCapacity(i);
      this.mRankings.ensureCapacity(i);
      for (byte b = 0; b < i; b++) {
        NotificationListenerService.Ranking ranking = new NotificationListenerService.Ranking((Parcel)this$0);
        String str = ranking.getKey();
        this.mOrderedKeys.add(str);
        this.mRankings.put(str, ranking);
      } 
    }
    
    public boolean equals(Object<String, NotificationListenerService.Ranking> param1Object) {
      null = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      RankingMap rankingMap = (RankingMap)param1Object;
      if (this.mOrderedKeys.equals(rankingMap.mOrderedKeys)) {
        param1Object = (Object<String, NotificationListenerService.Ranking>)this.mRankings;
        ArrayMap<String, NotificationListenerService.Ranking> arrayMap = rankingMap.mRankings;
        if (param1Object.equals(arrayMap))
          return null; 
      } 
      return false;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      int i = this.mOrderedKeys.size();
      param1Parcel.writeInt(i);
      for (byte b = 0; b < i; b++)
        ((NotificationListenerService.Ranking)this.mRankings.get(this.mOrderedKeys.get(b))).writeToParcel(param1Parcel, param1Int); 
    }
    
    public static final Parcelable.Creator<RankingMap> CREATOR = new Parcelable.Creator<RankingMap>() {
        public NotificationListenerService.RankingMap createFromParcel(Parcel param2Parcel) {
          return new NotificationListenerService.RankingMap();
        }
        
        public NotificationListenerService.RankingMap[] newArray(int param2Int) {
          return new NotificationListenerService.RankingMap[param2Int];
        }
      };
    
    public String[] getOrderedKeys() {
      return this.mOrderedKeys.<String>toArray(new String[0]);
    }
    
    public boolean getRanking(String param1String, NotificationListenerService.Ranking param1Ranking) {
      if (this.mRankings.containsKey(param1String)) {
        param1Ranking.populate((NotificationListenerService.Ranking)this.mRankings.get(param1String));
        return true;
      } 
      return false;
    }
    
    public NotificationListenerService.Ranking getRawRankingObject(String param1String) {
      return (NotificationListenerService.Ranking)this.mRankings.get(param1String);
    }
  }
  
  class MyHandler extends Handler {
    public static final int MSG_ON_INTERRUPTION_FILTER_CHANGED = 6;
    
    public static final int MSG_ON_LISTENER_CONNECTED = 3;
    
    public static final int MSG_ON_LISTENER_HINTS_CHANGED = 5;
    
    public static final int MSG_ON_NOTIFICATION_CHANNEL_GROUP_MODIFIED = 8;
    
    public static final int MSG_ON_NOTIFICATION_CHANNEL_MODIFIED = 7;
    
    public static final int MSG_ON_NOTIFICATION_POSTED = 1;
    
    public static final int MSG_ON_NOTIFICATION_RANKING_UPDATE = 4;
    
    public static final int MSG_ON_NOTIFICATION_REMOVED = 2;
    
    public static final int MSG_ON_STATUS_BAR_ICON_BEHAVIOR_CHANGED = 9;
    
    final NotificationListenerService this$0;
    
    public MyHandler(Looper param1Looper) {
      super(param1Looper, null, false);
    }
    
    public void handleMessage(Message param1Message) {
      UserHandle userHandle1;
      String str1;
      NotificationListenerService.RankingMap rankingMap1;
      SomeArgs someArgs1, someArgs2;
      NotificationStats notificationStats;
      String str2;
      UserHandle userHandle2;
      NotificationListenerService.RankingMap rankingMap2;
      NotificationChannelGroup notificationChannelGroup;
      NotificationChannel notificationChannel;
      StatusBarNotification statusBarNotification2;
      int i;
      if (!NotificationListenerService.this.isConnected)
        return; 
      switch (param1Message.what) {
        default:
          return;
        case 9:
          NotificationListenerService.this.onSilentStatusBarIconsVisibilityChanged(((Boolean)param1Message.obj).booleanValue());
        case 8:
          someArgs2 = (SomeArgs)param1Message.obj;
          str2 = (String)someArgs2.arg1;
          userHandle1 = (UserHandle)someArgs2.arg2;
          notificationChannelGroup = (NotificationChannelGroup)someArgs2.arg3;
          i = ((Integer)someArgs2.arg4).intValue();
          NotificationListenerService.this.onNotificationChannelGroupModified(str2, userHandle1, notificationChannelGroup, i);
        case 7:
          someArgs2 = (SomeArgs)((Message)userHandle1).obj;
          str1 = (String)someArgs2.arg1;
          userHandle2 = (UserHandle)someArgs2.arg2;
          notificationChannel = (NotificationChannel)someArgs2.arg3;
          i = ((Integer)someArgs2.arg4).intValue();
          NotificationListenerService.this.onNotificationChannelModified(str1, userHandle2, notificationChannel, i);
        case 6:
          i = ((Message)str1).arg1;
          NotificationListenerService.this.onInterruptionFilterChanged(i);
        case 5:
          i = ((Message)str1).arg1;
          NotificationListenerService.this.onListenerHintsChanged(i);
        case 4:
          rankingMap1 = (NotificationListenerService.RankingMap)((Message)str1).obj;
          NotificationListenerService.this.onNotificationRankingUpdate(rankingMap1);
        case 3:
          NotificationListenerService.this.onListenerConnected();
        case 2:
          someArgs1 = (SomeArgs)((Message)rankingMap1).obj;
          statusBarNotification2 = (StatusBarNotification)someArgs1.arg1;
          rankingMap2 = (NotificationListenerService.RankingMap)someArgs1.arg2;
          i = ((Integer)someArgs1.arg3).intValue();
          notificationStats = (NotificationStats)someArgs1.arg4;
          someArgs1.recycle();
          NotificationListenerService.this.onNotificationRemoved(statusBarNotification2, rankingMap2, notificationStats, i);
        case 1:
          break;
      } 
      SomeArgs someArgs3 = (SomeArgs)((Message)someArgs1).obj;
      StatusBarNotification statusBarNotification1 = (StatusBarNotification)someArgs3.arg1;
      NotificationListenerService.RankingMap rankingMap3 = (NotificationListenerService.RankingMap)someArgs3.arg2;
      someArgs3.recycle();
      NotificationListenerService.this.onNotificationPosted(statusBarNotification1, rankingMap3);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ChannelOrGroupModificationTypes implements Annotation {}
  
  class NotificationCancelReason implements Annotation {}
}
