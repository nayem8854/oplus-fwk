package android.service.notification;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.proto.ProtoOutputStream;
import java.io.ByteArrayOutputStream;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

public final class ZenPolicy implements Parcelable {
  public static final int CONVERSATION_SENDERS_ANYONE = 1;
  
  public static final int CONVERSATION_SENDERS_IMPORTANT = 2;
  
  public static final int CONVERSATION_SENDERS_NONE = 3;
  
  public static final int CONVERSATION_SENDERS_UNSET = 0;
  
  public ZenPolicy() {
    Integer integer = Integer.valueOf(0);
    this.mPriorityMessages = 0;
    this.mPriorityCalls = 0;
    this.mConversationSenders = 0;
    this.mPriorityCategories = new ArrayList<>(Collections.nCopies(9, integer));
    this.mVisualEffects = new ArrayList<>(Collections.nCopies(7, integer));
  }
  
  public int getPriorityConversationSenders() {
    return this.mConversationSenders;
  }
  
  public int getPriorityMessageSenders() {
    return this.mPriorityMessages;
  }
  
  public int getPriorityCallSenders() {
    return this.mPriorityCalls;
  }
  
  public int getPriorityCategoryConversations() {
    return ((Integer)this.mPriorityCategories.get(8)).intValue();
  }
  
  public int getPriorityCategoryReminders() {
    return ((Integer)this.mPriorityCategories.get(0)).intValue();
  }
  
  public int getPriorityCategoryEvents() {
    return ((Integer)this.mPriorityCategories.get(1)).intValue();
  }
  
  public int getPriorityCategoryMessages() {
    return ((Integer)this.mPriorityCategories.get(2)).intValue();
  }
  
  public int getPriorityCategoryCalls() {
    return ((Integer)this.mPriorityCategories.get(3)).intValue();
  }
  
  public int getPriorityCategoryRepeatCallers() {
    return ((Integer)this.mPriorityCategories.get(4)).intValue();
  }
  
  public int getPriorityCategoryAlarms() {
    return ((Integer)this.mPriorityCategories.get(5)).intValue();
  }
  
  public int getPriorityCategoryMedia() {
    return ((Integer)this.mPriorityCategories.get(6)).intValue();
  }
  
  public int getPriorityCategorySystem() {
    return ((Integer)this.mPriorityCategories.get(7)).intValue();
  }
  
  public int getVisualEffectFullScreenIntent() {
    return ((Integer)this.mVisualEffects.get(0)).intValue();
  }
  
  public int getVisualEffectLights() {
    return ((Integer)this.mVisualEffects.get(1)).intValue();
  }
  
  public int getVisualEffectPeek() {
    return ((Integer)this.mVisualEffects.get(2)).intValue();
  }
  
  public int getVisualEffectStatusBar() {
    return ((Integer)this.mVisualEffects.get(3)).intValue();
  }
  
  public int getVisualEffectBadge() {
    return ((Integer)this.mVisualEffects.get(4)).intValue();
  }
  
  public int getVisualEffectAmbient() {
    return ((Integer)this.mVisualEffects.get(5)).intValue();
  }
  
  public int getVisualEffectNotificationList() {
    return ((Integer)this.mVisualEffects.get(6)).intValue();
  }
  
  public boolean shouldHideAllVisualEffects() {
    for (byte b = 0; b < this.mVisualEffects.size(); b++) {
      if (((Integer)this.mVisualEffects.get(b)).intValue() != 2)
        return false; 
    } 
    return true;
  }
  
  public boolean shouldShowAllVisualEffects() {
    for (byte b = 0; b < this.mVisualEffects.size(); b++) {
      if (((Integer)this.mVisualEffects.get(b)).intValue() != 1)
        return false; 
    } 
    return true;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class VisualEffect implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class State implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class PriorityCategory implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class PeopleType implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ConversationSenders implements Annotation {}
  
  class Builder {
    private ZenPolicy mZenPolicy;
    
    public Builder() {
      this.mZenPolicy = new ZenPolicy();
    }
    
    public Builder(ZenPolicy this$0) {
      if (this$0 != null) {
        this.mZenPolicy = this$0.copy();
      } else {
        this.mZenPolicy = new ZenPolicy();
      } 
    }
    
    public ZenPolicy build() {
      return this.mZenPolicy.copy();
    }
    
    public Builder allowAllSounds() {
      for (byte b = 0; b < this.mZenPolicy.mPriorityCategories.size(); b++)
        this.mZenPolicy.mPriorityCategories.set(b, Integer.valueOf(1)); 
      ZenPolicy.access$102(this.mZenPolicy, 1);
      ZenPolicy.access$202(this.mZenPolicy, 1);
      ZenPolicy.access$302(this.mZenPolicy, 1);
      return this;
    }
    
    public Builder disallowAllSounds() {
      for (byte b = 0; b < this.mZenPolicy.mPriorityCategories.size(); b++)
        this.mZenPolicy.mPriorityCategories.set(b, Integer.valueOf(2)); 
      ZenPolicy.access$102(this.mZenPolicy, 4);
      ZenPolicy.access$202(this.mZenPolicy, 4);
      ZenPolicy.access$302(this.mZenPolicy, 3);
      return this;
    }
    
    public Builder showAllVisualEffects() {
      for (byte b = 0; b < this.mZenPolicy.mVisualEffects.size(); b++)
        this.mZenPolicy.mVisualEffects.set(b, Integer.valueOf(1)); 
      return this;
    }
    
    public Builder hideAllVisualEffects() {
      for (byte b = 0; b < this.mZenPolicy.mVisualEffects.size(); b++)
        this.mZenPolicy.mVisualEffects.set(b, Integer.valueOf(2)); 
      return this;
    }
    
    public Builder unsetPriorityCategory(int param1Int) {
      this.mZenPolicy.mPriorityCategories.set(param1Int, Integer.valueOf(0));
      if (param1Int == 2) {
        ZenPolicy.access$102(this.mZenPolicy, 0);
      } else if (param1Int == 3) {
        ZenPolicy.access$202(this.mZenPolicy, 0);
      } else if (param1Int == 8) {
        ZenPolicy.access$302(this.mZenPolicy, 0);
      } 
      return this;
    }
    
    public Builder unsetVisualEffect(int param1Int) {
      this.mZenPolicy.mVisualEffects.set(param1Int, Integer.valueOf(0));
      return this;
    }
    
    public Builder allowReminders(boolean param1Boolean) {
      byte b;
      ArrayList<Integer> arrayList = this.mZenPolicy.mPriorityCategories;
      if (param1Boolean) {
        b = 1;
      } else {
        b = 2;
      } 
      arrayList.set(0, Integer.valueOf(b));
      return this;
    }
    
    public Builder allowEvents(boolean param1Boolean) {
      byte b;
      ArrayList<Integer> arrayList = this.mZenPolicy.mPriorityCategories;
      if (param1Boolean) {
        b = 1;
      } else {
        b = 2;
      } 
      arrayList.set(1, Integer.valueOf(b));
      return this;
    }
    
    public Builder allowConversations(int param1Int) {
      if (param1Int == 0)
        return unsetPriorityCategory(8); 
      if (param1Int == 3) {
        this.mZenPolicy.mPriorityCategories.set(8, Integer.valueOf(2));
      } else {
        if (param1Int == 1 || param1Int == 2) {
          this.mZenPolicy.mPriorityCategories.set(8, Integer.valueOf(1));
          ZenPolicy.access$302(this.mZenPolicy, param1Int);
          return this;
        } 
        return this;
      } 
      ZenPolicy.access$302(this.mZenPolicy, param1Int);
      return this;
    }
    
    public Builder allowMessages(int param1Int) {
      if (param1Int == 0)
        return unsetPriorityCategory(2); 
      if (param1Int == 4) {
        this.mZenPolicy.mPriorityCategories.set(2, Integer.valueOf(2));
      } else {
        if (param1Int == 1 || param1Int == 2 || param1Int == 3) {
          this.mZenPolicy.mPriorityCategories.set(2, Integer.valueOf(1));
          ZenPolicy.access$102(this.mZenPolicy, param1Int);
          return this;
        } 
        return this;
      } 
      ZenPolicy.access$102(this.mZenPolicy, param1Int);
      return this;
    }
    
    public Builder allowCalls(int param1Int) {
      if (param1Int == 0)
        return unsetPriorityCategory(3); 
      if (param1Int == 4) {
        this.mZenPolicy.mPriorityCategories.set(3, Integer.valueOf(2));
      } else {
        if (param1Int == 1 || param1Int == 2 || param1Int == 3) {
          this.mZenPolicy.mPriorityCategories.set(3, Integer.valueOf(1));
          ZenPolicy.access$202(this.mZenPolicy, param1Int);
          return this;
        } 
        return this;
      } 
      ZenPolicy.access$202(this.mZenPolicy, param1Int);
      return this;
    }
    
    public Builder allowRepeatCallers(boolean param1Boolean) {
      byte b;
      ArrayList<Integer> arrayList = this.mZenPolicy.mPriorityCategories;
      if (param1Boolean) {
        b = 1;
      } else {
        b = 2;
      } 
      arrayList.set(4, Integer.valueOf(b));
      return this;
    }
    
    public Builder allowAlarms(boolean param1Boolean) {
      byte b;
      ArrayList<Integer> arrayList = this.mZenPolicy.mPriorityCategories;
      if (param1Boolean) {
        b = 1;
      } else {
        b = 2;
      } 
      arrayList.set(5, Integer.valueOf(b));
      return this;
    }
    
    public Builder allowMedia(boolean param1Boolean) {
      byte b;
      ArrayList<Integer> arrayList = this.mZenPolicy.mPriorityCategories;
      if (param1Boolean) {
        b = 1;
      } else {
        b = 2;
      } 
      arrayList.set(6, Integer.valueOf(b));
      return this;
    }
    
    public Builder allowSystem(boolean param1Boolean) {
      byte b;
      ArrayList<Integer> arrayList = this.mZenPolicy.mPriorityCategories;
      if (param1Boolean) {
        b = 1;
      } else {
        b = 2;
      } 
      arrayList.set(7, Integer.valueOf(b));
      return this;
    }
    
    public Builder allowCategory(int param1Int, boolean param1Boolean) {
      if (param1Int != 0) {
        if (param1Int != 1) {
          if (param1Int != 4) {
            if (param1Int != 5) {
              if (param1Int != 6) {
                if (param1Int == 7)
                  allowSystem(param1Boolean); 
              } else {
                allowMedia(param1Boolean);
              } 
            } else {
              allowAlarms(param1Boolean);
            } 
          } else {
            allowRepeatCallers(param1Boolean);
          } 
        } else {
          allowEvents(param1Boolean);
        } 
      } else {
        allowReminders(param1Boolean);
      } 
      return this;
    }
    
    public Builder showFullScreenIntent(boolean param1Boolean) {
      byte b;
      ArrayList<Integer> arrayList = this.mZenPolicy.mVisualEffects;
      if (param1Boolean) {
        b = 1;
      } else {
        b = 2;
      } 
      arrayList.set(0, Integer.valueOf(b));
      return this;
    }
    
    public Builder showLights(boolean param1Boolean) {
      byte b;
      ArrayList<Integer> arrayList = this.mZenPolicy.mVisualEffects;
      if (param1Boolean) {
        b = 1;
      } else {
        b = 2;
      } 
      arrayList.set(1, Integer.valueOf(b));
      return this;
    }
    
    public Builder showPeeking(boolean param1Boolean) {
      byte b;
      ArrayList<Integer> arrayList = this.mZenPolicy.mVisualEffects;
      if (param1Boolean) {
        b = 1;
      } else {
        b = 2;
      } 
      arrayList.set(2, Integer.valueOf(b));
      return this;
    }
    
    public Builder showStatusBarIcons(boolean param1Boolean) {
      byte b;
      ArrayList<Integer> arrayList = this.mZenPolicy.mVisualEffects;
      if (param1Boolean) {
        b = 1;
      } else {
        b = 2;
      } 
      arrayList.set(3, Integer.valueOf(b));
      return this;
    }
    
    public Builder showBadges(boolean param1Boolean) {
      byte b;
      ArrayList<Integer> arrayList = this.mZenPolicy.mVisualEffects;
      if (param1Boolean) {
        b = 1;
      } else {
        b = 2;
      } 
      arrayList.set(4, Integer.valueOf(b));
      return this;
    }
    
    public Builder showInAmbientDisplay(boolean param1Boolean) {
      byte b;
      ArrayList<Integer> arrayList = this.mZenPolicy.mVisualEffects;
      if (param1Boolean) {
        b = 1;
      } else {
        b = 2;
      } 
      arrayList.set(5, Integer.valueOf(b));
      return this;
    }
    
    public Builder showInNotificationList(boolean param1Boolean) {
      byte b;
      ArrayList<Integer> arrayList = this.mZenPolicy.mVisualEffects;
      if (param1Boolean) {
        b = 1;
      } else {
        b = 2;
      } 
      arrayList.set(6, Integer.valueOf(b));
      return this;
    }
    
    public Builder showVisualEffect(int param1Int, boolean param1Boolean) {
      switch (param1Int) {
        default:
          return this;
        case 6:
          showInNotificationList(param1Boolean);
        case 5:
          showInAmbientDisplay(param1Boolean);
        case 4:
          showBadges(param1Boolean);
        case 3:
          showStatusBarIcons(param1Boolean);
        case 2:
          showPeeking(param1Boolean);
        case 1:
          showLights(param1Boolean);
        case 0:
          break;
      } 
      showFullScreenIntent(param1Boolean);
    }
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeList(this.mPriorityCategories);
    paramParcel.writeList(this.mVisualEffects);
    paramParcel.writeInt(this.mPriorityCalls);
    paramParcel.writeInt(this.mPriorityMessages);
    paramParcel.writeInt(this.mConversationSenders);
  }
  
  public static final Parcelable.Creator<ZenPolicy> CREATOR = new Parcelable.Creator<ZenPolicy>() {
      public ZenPolicy createFromParcel(Parcel param1Parcel) {
        ZenPolicy zenPolicy = new ZenPolicy();
        ZenPolicy.access$002(zenPolicy, param1Parcel.readArrayList(Integer.class.getClassLoader()));
        ZenPolicy.access$402(zenPolicy, param1Parcel.readArrayList(Integer.class.getClassLoader()));
        ZenPolicy.access$202(zenPolicy, param1Parcel.readInt());
        ZenPolicy.access$102(zenPolicy, param1Parcel.readInt());
        ZenPolicy.access$302(zenPolicy, param1Parcel.readInt());
        return zenPolicy;
      }
      
      public ZenPolicy[] newArray(int param1Int) {
        return new ZenPolicy[param1Int];
      }
    };
  
  public static final int PEOPLE_TYPE_ANYONE = 1;
  
  public static final int PEOPLE_TYPE_CONTACTS = 2;
  
  public static final int PEOPLE_TYPE_NONE = 4;
  
  public static final int PEOPLE_TYPE_STARRED = 3;
  
  public static final int PEOPLE_TYPE_UNSET = 0;
  
  public static final int PRIORITY_CATEGORY_ALARMS = 5;
  
  public static final int PRIORITY_CATEGORY_CALLS = 3;
  
  public static final int PRIORITY_CATEGORY_CONVERSATIONS = 8;
  
  public static final int PRIORITY_CATEGORY_EVENTS = 1;
  
  public static final int PRIORITY_CATEGORY_MEDIA = 6;
  
  public static final int PRIORITY_CATEGORY_MESSAGES = 2;
  
  public static final int PRIORITY_CATEGORY_REMINDERS = 0;
  
  public static final int PRIORITY_CATEGORY_REPEAT_CALLERS = 4;
  
  public static final int PRIORITY_CATEGORY_SYSTEM = 7;
  
  public static final int STATE_ALLOW = 1;
  
  public static final int STATE_DISALLOW = 2;
  
  public static final int STATE_UNSET = 0;
  
  public static final int VISUAL_EFFECT_AMBIENT = 5;
  
  public static final int VISUAL_EFFECT_BADGE = 4;
  
  public static final int VISUAL_EFFECT_FULL_SCREEN_INTENT = 0;
  
  public static final int VISUAL_EFFECT_LIGHTS = 1;
  
  public static final int VISUAL_EFFECT_NOTIFICATION_LIST = 6;
  
  public static final int VISUAL_EFFECT_PEEK = 2;
  
  public static final int VISUAL_EFFECT_STATUS_BAR = 3;
  
  private int mConversationSenders;
  
  private int mPriorityCalls;
  
  private ArrayList<Integer> mPriorityCategories;
  
  private int mPriorityMessages;
  
  private ArrayList<Integer> mVisualEffects;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(ZenPolicy.class.getSimpleName());
    stringBuilder.append('{');
    stringBuilder.append("priorityCategories=[");
    stringBuilder.append(priorityCategoriesToString());
    stringBuilder.append("], visualEffects=[");
    stringBuilder.append(visualEffectsToString());
    stringBuilder.append("], priorityCallsSenders=");
    stringBuilder.append(peopleTypeToString(this.mPriorityCalls));
    stringBuilder.append(", priorityMessagesSenders=");
    stringBuilder.append(peopleTypeToString(this.mPriorityMessages));
    stringBuilder.append(", priorityConversationSenders=");
    int i = this.mConversationSenders;
    null = conversationTypeToString(i);
    stringBuilder.append(null);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  private String priorityCategoriesToString() {
    StringBuilder stringBuilder = new StringBuilder();
    for (byte b = 0; b < this.mPriorityCategories.size(); b++) {
      if (((Integer)this.mPriorityCategories.get(b)).intValue() != 0) {
        stringBuilder.append(indexToCategory(b));
        stringBuilder.append("=");
        ArrayList<Integer> arrayList = this.mPriorityCategories;
        stringBuilder.append(stateToString(((Integer)arrayList.get(b)).intValue()));
        stringBuilder.append(" ");
      } 
    } 
    return stringBuilder.toString();
  }
  
  private String visualEffectsToString() {
    StringBuilder stringBuilder = new StringBuilder();
    for (byte b = 0; b < this.mVisualEffects.size(); b++) {
      if (((Integer)this.mVisualEffects.get(b)).intValue() != 0) {
        stringBuilder.append(indexToVisualEffect(b));
        stringBuilder.append("=");
        ArrayList<Integer> arrayList = this.mVisualEffects;
        stringBuilder.append(stateToString(((Integer)arrayList.get(b)).intValue()));
        stringBuilder.append(" ");
      } 
    } 
    return stringBuilder.toString();
  }
  
  private String indexToVisualEffect(int paramInt) {
    switch (paramInt) {
      default:
        return null;
      case 6:
        return "notificationList";
      case 5:
        return "ambient";
      case 4:
        return "badge";
      case 3:
        return "statusBar";
      case 2:
        return "peek";
      case 1:
        return "lights";
      case 0:
        break;
    } 
    return "fullScreenIntent";
  }
  
  private String indexToCategory(int paramInt) {
    switch (paramInt) {
      default:
        return null;
      case 8:
        return "convs";
      case 7:
        return "system";
      case 6:
        return "media";
      case 5:
        return "alarms";
      case 4:
        return "repeatCallers";
      case 3:
        return "calls";
      case 2:
        return "messages";
      case 1:
        return "events";
      case 0:
        break;
    } 
    return "reminders";
  }
  
  private String stateToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("invalidState{");
          stringBuilder.append(paramInt);
          stringBuilder.append("}");
          return stringBuilder.toString();
        } 
        return "disallow";
      } 
      return "allow";
    } 
    return "unset";
  }
  
  private String peopleTypeToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt != 4) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("invalidPeopleType{");
              stringBuilder.append(paramInt);
              stringBuilder.append("}");
              return stringBuilder.toString();
            } 
            return "none";
          } 
          return "starred_contacts";
        } 
        return "contacts";
      } 
      return "anyone";
    } 
    return "unset";
  }
  
  public static String conversationTypeToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("invalidConversationType{");
            stringBuilder.append(paramInt);
            stringBuilder.append("}");
            return stringBuilder.toString();
          } 
          return "none";
        } 
        return "important";
      } 
      return "anyone";
    } 
    return "unset";
  }
  
  public boolean equals(Object<Integer> paramObject) {
    boolean bool = paramObject instanceof ZenPolicy;
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (paramObject == this)
      return true; 
    ZenPolicy zenPolicy = (ZenPolicy)paramObject;
    if (Objects.equals(zenPolicy.mPriorityCategories, this.mPriorityCategories)) {
      paramObject = (Object<Integer>)zenPolicy.mVisualEffects;
      ArrayList<Integer> arrayList = this.mVisualEffects;
      if (Objects.equals(paramObject, arrayList) && zenPolicy.mPriorityCalls == this.mPriorityCalls && zenPolicy.mPriorityMessages == this.mPriorityMessages && zenPolicy.mConversationSenders == this.mConversationSenders)
        bool1 = true; 
    } 
    return bool1;
  }
  
  public int hashCode() {
    ArrayList<Integer> arrayList1 = this.mPriorityCategories, arrayList2 = this.mVisualEffects;
    int i = this.mPriorityCalls, j = this.mPriorityMessages, k = this.mConversationSenders;
    return Objects.hash(new Object[] { arrayList1, arrayList2, Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  private int getZenPolicyPriorityCategoryState(int paramInt) {
    switch (paramInt) {
      default:
        return -1;
      case 8:
        return getPriorityCategoryConversations();
      case 7:
        return getPriorityCategorySystem();
      case 6:
        return getPriorityCategoryMedia();
      case 5:
        return getPriorityCategoryAlarms();
      case 4:
        return getPriorityCategoryRepeatCallers();
      case 3:
        return getPriorityCategoryCalls();
      case 2:
        return getPriorityCategoryMessages();
      case 1:
        return getPriorityCategoryEvents();
      case 0:
        break;
    } 
    return getPriorityCategoryReminders();
  }
  
  private int getZenPolicyVisualEffectState(int paramInt) {
    switch (paramInt) {
      default:
        return -1;
      case 6:
        return getVisualEffectNotificationList();
      case 5:
        return getVisualEffectAmbient();
      case 4:
        return getVisualEffectBadge();
      case 3:
        return getVisualEffectStatusBar();
      case 2:
        return getVisualEffectPeek();
      case 1:
        return getVisualEffectLights();
      case 0:
        break;
    } 
    return getVisualEffectFullScreenIntent();
  }
  
  public boolean isCategoryAllowed(int paramInt, boolean paramBoolean) {
    paramInt = getZenPolicyPriorityCategoryState(paramInt);
    if (paramInt != 1) {
      if (paramInt != 2)
        return paramBoolean; 
      return false;
    } 
    return true;
  }
  
  public boolean isVisualEffectAllowed(int paramInt, boolean paramBoolean) {
    paramInt = getZenPolicyVisualEffectState(paramInt);
    if (paramInt != 1) {
      if (paramInt != 2)
        return paramBoolean; 
      return false;
    } 
    return true;
  }
  
  public void apply(ZenPolicy paramZenPolicy) {
    if (paramZenPolicy == null)
      return; 
    byte b;
    for (b = 0; b < this.mPriorityCategories.size(); b++) {
      if (((Integer)this.mPriorityCategories.get(b)).intValue() == 2)
        continue; 
      int i = ((Integer)paramZenPolicy.mPriorityCategories.get(b)).intValue();
      if (i != 0) {
        this.mPriorityCategories.set(b, Integer.valueOf(i));
        if (b == 2) {
          i = this.mPriorityMessages;
          int j = paramZenPolicy.mPriorityMessages;
          if (i < j) {
            this.mPriorityMessages = j;
            continue;
          } 
        } 
        if (b == 3) {
          i = this.mPriorityCalls;
          int j = paramZenPolicy.mPriorityCalls;
          if (i < j) {
            this.mPriorityCalls = j;
            continue;
          } 
        } 
        if (b == 8) {
          int j = this.mConversationSenders;
          i = paramZenPolicy.mConversationSenders;
          if (j < i)
            this.mConversationSenders = i; 
        } 
      } 
      continue;
    } 
    for (b = 0; b < this.mVisualEffects.size(); b++) {
      if (((Integer)this.mVisualEffects.get(b)).intValue() != 2)
        if (((Integer)paramZenPolicy.mVisualEffects.get(b)).intValue() != 0)
          this.mVisualEffects.set(b, paramZenPolicy.mVisualEffects.get(b));  
    } 
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    paramLong = paramProtoOutputStream.start(paramLong);
    paramProtoOutputStream.write(1159641169921L, getPriorityCategoryReminders());
    paramProtoOutputStream.write(1159641169922L, getPriorityCategoryEvents());
    paramProtoOutputStream.write(1159641169923L, getPriorityCategoryMessages());
    paramProtoOutputStream.write(1159641169924L, getPriorityCategoryCalls());
    paramProtoOutputStream.write(1159641169925L, getPriorityCategoryRepeatCallers());
    paramProtoOutputStream.write(1159641169926L, getPriorityCategoryAlarms());
    paramProtoOutputStream.write(1159641169927L, getPriorityCategoryMedia());
    paramProtoOutputStream.write(1159641169928L, getPriorityCategorySystem());
    paramProtoOutputStream.write(1159641169929L, getVisualEffectFullScreenIntent());
    paramProtoOutputStream.write(1159641169930L, getVisualEffectLights());
    paramProtoOutputStream.write(1159641169931L, getVisualEffectPeek());
    paramProtoOutputStream.write(1159641169932L, getVisualEffectStatusBar());
    paramProtoOutputStream.write(1159641169933L, getVisualEffectBadge());
    paramProtoOutputStream.write(1159641169934L, getVisualEffectAmbient());
    paramProtoOutputStream.write(1159641169935L, getVisualEffectNotificationList());
    paramProtoOutputStream.write(1159641169937L, getPriorityMessageSenders());
    paramProtoOutputStream.write(1159641169936L, getPriorityCallSenders());
    paramProtoOutputStream.end(paramLong);
  }
  
  public byte[] toProto() {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    ProtoOutputStream protoOutputStream = new ProtoOutputStream(byteArrayOutputStream);
    protoOutputStream.write(1159641169921L, getPriorityCategoryCalls());
    protoOutputStream.write(1159641169922L, getPriorityCategoryRepeatCallers());
    protoOutputStream.write(1159641169923L, getPriorityCategoryMessages());
    protoOutputStream.write(1159641169924L, getPriorityCategoryConversations());
    protoOutputStream.write(1159641169925L, getPriorityCategoryReminders());
    protoOutputStream.write(1159641169926L, getPriorityCategoryEvents());
    protoOutputStream.write(1159641169927L, getPriorityCategoryAlarms());
    protoOutputStream.write(1159641169928L, getPriorityCategoryMedia());
    protoOutputStream.write(1159641169929L, getPriorityCategorySystem());
    protoOutputStream.write(1159641169930L, getVisualEffectFullScreenIntent());
    protoOutputStream.write(1159641169931L, getVisualEffectLights());
    protoOutputStream.write(1159641169932L, getVisualEffectPeek());
    protoOutputStream.write(1159641169933L, getVisualEffectStatusBar());
    protoOutputStream.write(1159641169934L, getVisualEffectBadge());
    protoOutputStream.write(1159641169935L, getVisualEffectAmbient());
    protoOutputStream.write(1159641169936L, getVisualEffectNotificationList());
    protoOutputStream.write(1159641169937L, getPriorityCallSenders());
    protoOutputStream.write(1159641169938L, getPriorityMessageSenders());
    protoOutputStream.write(1159641169939L, getPriorityConversationSenders());
    protoOutputStream.flush();
    return byteArrayOutputStream.toByteArray();
  }
  
  public ZenPolicy copy() {
    Parcel parcel = Parcel.obtain();
    try {
      writeToParcel(parcel, 0);
      parcel.setDataPosition(0);
      return CREATOR.createFromParcel(parcel);
    } finally {
      parcel.recycle();
    } 
  }
}
