package android.service.notification;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public final class NotifyingApp implements Parcelable, Comparable<NotifyingApp> {
  public NotifyingApp() {}
  
  protected NotifyingApp(Parcel paramParcel) {
    this.mUserId = paramParcel.readInt();
    this.mPkg = paramParcel.readString();
    this.mLastNotified = paramParcel.readLong();
  }
  
  public int getUserId() {
    return this.mUserId;
  }
  
  public NotifyingApp setUserId(int paramInt) {
    this.mUserId = paramInt;
    return this;
  }
  
  public String getPackage() {
    return this.mPkg;
  }
  
  public NotifyingApp setPackage(String paramString) {
    this.mPkg = paramString;
    return this;
  }
  
  public long getLastNotified() {
    return this.mLastNotified;
  }
  
  public NotifyingApp setLastNotified(long paramLong) {
    this.mLastNotified = paramLong;
    return this;
  }
  
  public static final Parcelable.Creator<NotifyingApp> CREATOR = new Parcelable.Creator<NotifyingApp>() {
      public NotifyingApp createFromParcel(Parcel param1Parcel) {
        return new NotifyingApp(param1Parcel);
      }
      
      public NotifyingApp[] newArray(int param1Int) {
        return new NotifyingApp[param1Int];
      }
    };
  
  private long mLastNotified;
  
  private String mPkg;
  
  private int mUserId;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mUserId);
    paramParcel.writeString(this.mPkg);
    paramParcel.writeLong(this.mLastNotified);
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    NotifyingApp notifyingApp = (NotifyingApp)paramObject;
    if (getUserId() == notifyingApp.getUserId() && 
      getLastNotified() == notifyingApp.getLastNotified()) {
      paramObject = this.mPkg;
      String str = notifyingApp.mPkg;
      if (Objects.equals(paramObject, str))
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(getUserId()), this.mPkg, Long.valueOf(getLastNotified()) });
  }
  
  public int compareTo(NotifyingApp paramNotifyingApp) {
    if (getLastNotified() == paramNotifyingApp.getLastNotified()) {
      if (getUserId() == paramNotifyingApp.getUserId())
        return getPackage().compareTo(paramNotifyingApp.getPackage()); 
      return Integer.compare(getUserId(), paramNotifyingApp.getUserId());
    } 
    return -Long.compare(getLastNotified(), paramNotifyingApp.getLastNotified());
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("NotifyingApp{mUserId=");
    stringBuilder.append(this.mUserId);
    stringBuilder.append(", mPkg='");
    stringBuilder.append(this.mPkg);
    stringBuilder.append('\'');
    stringBuilder.append(", mLastNotified=");
    stringBuilder.append(this.mLastNotified);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
}
