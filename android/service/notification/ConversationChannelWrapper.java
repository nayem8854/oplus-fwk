package android.service.notification;

import android.app.NotificationChannel;
import android.content.pm.ShortcutInfo;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public final class ConversationChannelWrapper implements Parcelable {
  public ConversationChannelWrapper() {}
  
  protected ConversationChannelWrapper(Parcel paramParcel) {
    this.mNotificationChannel = paramParcel.<NotificationChannel>readParcelable(NotificationChannel.class.getClassLoader());
    this.mGroupLabel = paramParcel.readCharSequence();
    this.mParentChannelLabel = paramParcel.readCharSequence();
    this.mShortcutInfo = paramParcel.<ShortcutInfo>readParcelable(ShortcutInfo.class.getClassLoader());
    this.mPkg = paramParcel.readStringNoHelper();
    this.mUid = paramParcel.readInt();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.mNotificationChannel, paramInt);
    paramParcel.writeCharSequence(this.mGroupLabel);
    paramParcel.writeCharSequence(this.mParentChannelLabel);
    paramParcel.writeParcelable((Parcelable)this.mShortcutInfo, paramInt);
    paramParcel.writeStringNoHelper(this.mPkg);
    paramParcel.writeInt(this.mUid);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<ConversationChannelWrapper> CREATOR = new Parcelable.Creator<ConversationChannelWrapper>() {
      public ConversationChannelWrapper createFromParcel(Parcel param1Parcel) {
        return new ConversationChannelWrapper(param1Parcel);
      }
      
      public ConversationChannelWrapper[] newArray(int param1Int) {
        return new ConversationChannelWrapper[param1Int];
      }
    };
  
  private CharSequence mGroupLabel;
  
  private NotificationChannel mNotificationChannel;
  
  private CharSequence mParentChannelLabel;
  
  private String mPkg;
  
  private ShortcutInfo mShortcutInfo;
  
  private int mUid;
  
  public NotificationChannel getNotificationChannel() {
    return this.mNotificationChannel;
  }
  
  public void setNotificationChannel(NotificationChannel paramNotificationChannel) {
    this.mNotificationChannel = paramNotificationChannel;
  }
  
  public CharSequence getGroupLabel() {
    return this.mGroupLabel;
  }
  
  public void setGroupLabel(CharSequence paramCharSequence) {
    this.mGroupLabel = paramCharSequence;
  }
  
  public CharSequence getParentChannelLabel() {
    return this.mParentChannelLabel;
  }
  
  public void setParentChannelLabel(CharSequence paramCharSequence) {
    this.mParentChannelLabel = paramCharSequence;
  }
  
  public ShortcutInfo getShortcutInfo() {
    return this.mShortcutInfo;
  }
  
  public void setShortcutInfo(ShortcutInfo paramShortcutInfo) {
    this.mShortcutInfo = paramShortcutInfo;
  }
  
  public String getPkg() {
    return this.mPkg;
  }
  
  public void setPkg(String paramString) {
    this.mPkg = paramString;
  }
  
  public int getUid() {
    return this.mUid;
  }
  
  public void setUid(int paramInt) {
    this.mUid = paramInt;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (!Objects.equals(getNotificationChannel(), paramObject.getNotificationChannel()) || 
      !Objects.equals(getGroupLabel(), paramObject.getGroupLabel()) || 
      !Objects.equals(getParentChannelLabel(), paramObject.getParentChannelLabel()) || 
      !Objects.equals(getShortcutInfo(), paramObject.getShortcutInfo()) || 
      !Objects.equals(getPkg(), paramObject.getPkg()) || 
      getUid() != paramObject.getUid())
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    NotificationChannel notificationChannel = getNotificationChannel();
    CharSequence charSequence1 = getGroupLabel(), charSequence2 = getParentChannelLabel();
    ShortcutInfo shortcutInfo = getShortcutInfo();
    String str = getPkg();
    int i = getUid();
    return Objects.hash(new Object[] { notificationChannel, charSequence1, charSequence2, shortcutInfo, str, Integer.valueOf(i) });
  }
}
