package android.service.notification;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IStatusBarNotificationHolder extends IInterface {
  StatusBarNotification get() throws RemoteException;
  
  class Default implements IStatusBarNotificationHolder {
    public StatusBarNotification get() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IStatusBarNotificationHolder {
    private static final String DESCRIPTOR = "android.service.notification.IStatusBarNotificationHolder";
    
    static final int TRANSACTION_get = 1;
    
    public Stub() {
      attachInterface(this, "android.service.notification.IStatusBarNotificationHolder");
    }
    
    public static IStatusBarNotificationHolder asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.notification.IStatusBarNotificationHolder");
      if (iInterface != null && iInterface instanceof IStatusBarNotificationHolder)
        return (IStatusBarNotificationHolder)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "get";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.service.notification.IStatusBarNotificationHolder");
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.notification.IStatusBarNotificationHolder");
      StatusBarNotification statusBarNotification = get();
      param1Parcel2.writeNoException();
      if (statusBarNotification != null) {
        param1Parcel2.writeInt(1);
        statusBarNotification.writeToParcel(param1Parcel2, 1);
      } else {
        param1Parcel2.writeInt(0);
      } 
      return true;
    }
    
    private static class Proxy implements IStatusBarNotificationHolder {
      public static IStatusBarNotificationHolder sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.notification.IStatusBarNotificationHolder";
      }
      
      public StatusBarNotification get() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          StatusBarNotification statusBarNotification;
          parcel1.writeInterfaceToken("android.service.notification.IStatusBarNotificationHolder");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IStatusBarNotificationHolder.Stub.getDefaultImpl() != null) {
            statusBarNotification = IStatusBarNotificationHolder.Stub.getDefaultImpl().get();
            return statusBarNotification;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            statusBarNotification = StatusBarNotification.CREATOR.createFromParcel(parcel2);
          } else {
            statusBarNotification = null;
          } 
          return statusBarNotification;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IStatusBarNotificationHolder param1IStatusBarNotificationHolder) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IStatusBarNotificationHolder != null) {
          Proxy.sDefaultImpl = param1IStatusBarNotificationHolder;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IStatusBarNotificationHolder getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
