package android.service.notification;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IConditionListener extends IInterface {
  void onConditionsReceived(Condition[] paramArrayOfCondition) throws RemoteException;
  
  class Default implements IConditionListener {
    public void onConditionsReceived(Condition[] param1ArrayOfCondition) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IConditionListener {
    private static final String DESCRIPTOR = "android.service.notification.IConditionListener";
    
    static final int TRANSACTION_onConditionsReceived = 1;
    
    public Stub() {
      attachInterface(this, "android.service.notification.IConditionListener");
    }
    
    public static IConditionListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.notification.IConditionListener");
      if (iInterface != null && iInterface instanceof IConditionListener)
        return (IConditionListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onConditionsReceived";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.service.notification.IConditionListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.notification.IConditionListener");
      Condition[] arrayOfCondition = param1Parcel1.<Condition>createTypedArray(Condition.CREATOR);
      onConditionsReceived(arrayOfCondition);
      return true;
    }
    
    private static class Proxy implements IConditionListener {
      public static IConditionListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.notification.IConditionListener";
      }
      
      public void onConditionsReceived(Condition[] param2ArrayOfCondition) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.notification.IConditionListener");
          parcel.writeTypedArray(param2ArrayOfCondition, 0);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IConditionListener.Stub.getDefaultImpl() != null) {
            IConditionListener.Stub.getDefaultImpl().onConditionsReceived(param2ArrayOfCondition);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IConditionListener param1IConditionListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IConditionListener != null) {
          Proxy.sDefaultImpl = param1IConditionListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IConditionListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
