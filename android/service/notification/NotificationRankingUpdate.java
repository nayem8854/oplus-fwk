package android.service.notification;

import android.os.Parcel;
import android.os.Parcelable;

public class NotificationRankingUpdate implements Parcelable {
  public NotificationRankingUpdate(NotificationListenerService.Ranking[] paramArrayOfRanking) {
    this.mRankingMap = new NotificationListenerService.RankingMap(paramArrayOfRanking);
  }
  
  public NotificationRankingUpdate(Parcel paramParcel) {
    this.mRankingMap = paramParcel.<NotificationListenerService.RankingMap>readParcelable(getClass().getClassLoader());
  }
  
  public NotificationListenerService.RankingMap getRankingMap() {
    return this.mRankingMap;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    return this.mRankingMap.equals(((NotificationRankingUpdate)paramObject).mRankingMap);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.mRankingMap, paramInt);
  }
  
  public static final Parcelable.Creator<NotificationRankingUpdate> CREATOR = new Parcelable.Creator<NotificationRankingUpdate>() {
      public NotificationRankingUpdate createFromParcel(Parcel param1Parcel) {
        return new NotificationRankingUpdate(param1Parcel);
      }
      
      public NotificationRankingUpdate[] newArray(int param1Int) {
        return new NotificationRankingUpdate[param1Int];
      }
    };
  
  private final NotificationListenerService.RankingMap mRankingMap;
}
