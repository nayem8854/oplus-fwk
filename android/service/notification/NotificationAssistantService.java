package android.service.notification;

import android.annotation.SystemApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.android.internal.os.SomeArgs;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

@SystemApi
public abstract class NotificationAssistantService extends NotificationListenerService {
  public static final String SERVICE_INTERFACE = "android.service.notification.NotificationAssistantService";
  
  public static final int SOURCE_FROM_APP = 0;
  
  public static final int SOURCE_FROM_ASSISTANT = 1;
  
  private static final String TAG = "NotificationAssistants";
  
  protected Handler mHandler;
  
  protected void attachBaseContext(Context paramContext) {
    super.attachBaseContext(paramContext);
    this.mHandler = new MyHandler(getContext().getMainLooper());
  }
  
  public final IBinder onBind(Intent paramIntent) {
    if (this.mWrapper == null)
      this.mWrapper = new NotificationAssistantServiceWrapper(); 
    return this.mWrapper;
  }
  
  public Adjustment onNotificationEnqueued(StatusBarNotification paramStatusBarNotification, NotificationChannel paramNotificationChannel) {
    return onNotificationEnqueued(paramStatusBarNotification);
  }
  
  public void onNotificationRemoved(StatusBarNotification paramStatusBarNotification, NotificationListenerService.RankingMap paramRankingMap, NotificationStats paramNotificationStats, int paramInt) {
    onNotificationRemoved(paramStatusBarNotification, paramRankingMap, paramInt);
  }
  
  public void onNotificationsSeen(List<String> paramList) {}
  
  public void onPanelRevealed(int paramInt) {}
  
  public void onPanelHidden() {}
  
  public void onNotificationVisibilityChanged(String paramString, boolean paramBoolean) {}
  
  public void onNotificationExpansionChanged(String paramString, boolean paramBoolean1, boolean paramBoolean2) {}
  
  public void onNotificationDirectReplied(String paramString) {}
  
  public void onSuggestedReplySent(String paramString, CharSequence paramCharSequence, int paramInt) {}
  
  public void onActionInvoked(String paramString, Notification.Action paramAction, int paramInt) {}
  
  public void onAllowedAdjustmentsChanged() {}
  
  public final void adjustNotification(Adjustment paramAdjustment) {
    if (!isBound())
      return; 
    try {
      setAdjustmentIssuer(paramAdjustment);
      getNotificationInterface().applyEnqueuedAdjustmentFromAssistant(this.mWrapper, paramAdjustment);
      return;
    } catch (RemoteException remoteException) {
      Log.v("NotificationAssistants", "Unable to contact notification manager", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public final void adjustNotifications(List<Adjustment> paramList) {
    if (!isBound())
      return; 
    try {
      for (Adjustment adjustment : paramList)
        setAdjustmentIssuer(adjustment); 
      getNotificationInterface().applyAdjustmentsFromAssistant(this.mWrapper, paramList);
      return;
    } catch (RemoteException remoteException) {
      Log.v("NotificationAssistants", "Unable to contact notification manager", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public final void unsnoozeNotification(String paramString) {
    if (!isBound())
      return; 
    try {
      getNotificationInterface().unsnoozeNotificationFromAssistant(this.mWrapper, paramString);
    } catch (RemoteException remoteException) {
      Log.v("NotificationAssistants", "Unable to contact notification manager", (Throwable)remoteException);
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Source implements Annotation {}
  
  private class NotificationAssistantServiceWrapper extends NotificationListenerService.NotificationListenerWrapper {
    final NotificationAssistantService this$0;
    
    private NotificationAssistantServiceWrapper() {}
    
    public void onNotificationEnqueuedWithChannel(IStatusBarNotificationHolder param1IStatusBarNotificationHolder, NotificationChannel param1NotificationChannel) {
      try {
        StatusBarNotification statusBarNotification = param1IStatusBarNotificationHolder.get();
        if (statusBarNotification == null) {
          Log.w("NotificationAssistants", "onNotificationEnqueuedWithChannel: Error receiving StatusBarNotification");
          return;
        } 
        SomeArgs someArgs = SomeArgs.obtain();
        someArgs.arg1 = statusBarNotification;
        someArgs.arg2 = param1NotificationChannel;
        Message message = NotificationAssistantService.this.mHandler.obtainMessage(1, someArgs);
        message.sendToTarget();
        return;
      } catch (RemoteException remoteException) {
        Log.w("NotificationAssistants", "onNotificationEnqueued: Error receiving StatusBarNotification", (Throwable)remoteException);
        return;
      } 
    }
    
    public void onNotificationSnoozedUntilContext(IStatusBarNotificationHolder param1IStatusBarNotificationHolder, String param1String) {
      try {
        StatusBarNotification statusBarNotification = param1IStatusBarNotificationHolder.get();
        if (statusBarNotification == null) {
          Log.w("NotificationAssistants", "onNotificationSnoozed: Error receiving StatusBarNotification");
          return;
        } 
        SomeArgs someArgs = SomeArgs.obtain();
        someArgs.arg1 = statusBarNotification;
        someArgs.arg2 = param1String;
        Message message = NotificationAssistantService.this.mHandler.obtainMessage(2, someArgs);
        message.sendToTarget();
        return;
      } catch (RemoteException remoteException) {
        Log.w("NotificationAssistants", "onNotificationSnoozed: Error receiving StatusBarNotification", (Throwable)remoteException);
        return;
      } 
    }
    
    public void onNotificationsSeen(List<String> param1List) {
      SomeArgs someArgs = SomeArgs.obtain();
      someArgs.arg1 = param1List;
      Message message = NotificationAssistantService.this.mHandler.obtainMessage(3, someArgs);
      message.sendToTarget();
    }
    
    public void onPanelRevealed(int param1Int) {
      SomeArgs someArgs = SomeArgs.obtain();
      someArgs.argi1 = param1Int;
      Message message = NotificationAssistantService.this.mHandler.obtainMessage(9, someArgs);
      message.sendToTarget();
    }
    
    public void onPanelHidden() {
      SomeArgs someArgs = SomeArgs.obtain();
      Message message = NotificationAssistantService.this.mHandler.obtainMessage(10, someArgs);
      message.sendToTarget();
    }
    
    public void onNotificationVisibilityChanged(String param1String, boolean param1Boolean) {
      SomeArgs someArgs = SomeArgs.obtain();
      someArgs.arg1 = param1String;
      someArgs.argi1 = param1Boolean;
      Message message = NotificationAssistantService.this.mHandler.obtainMessage(11, someArgs);
      message.sendToTarget();
    }
    
    public void onNotificationExpansionChanged(String param1String, boolean param1Boolean1, boolean param1Boolean2) {
      SomeArgs someArgs = SomeArgs.obtain();
      someArgs.arg1 = param1String;
      someArgs.argi1 = param1Boolean1;
      someArgs.argi2 = param1Boolean2;
      Message message = NotificationAssistantService.this.mHandler.obtainMessage(4, someArgs);
      message.sendToTarget();
    }
    
    public void onNotificationDirectReply(String param1String) {
      SomeArgs someArgs = SomeArgs.obtain();
      someArgs.arg1 = param1String;
      Message message = NotificationAssistantService.this.mHandler.obtainMessage(5, someArgs);
      message.sendToTarget();
    }
    
    public void onSuggestedReplySent(String param1String, CharSequence param1CharSequence, int param1Int) {
      SomeArgs someArgs = SomeArgs.obtain();
      someArgs.arg1 = param1String;
      someArgs.arg2 = param1CharSequence;
      someArgs.argi2 = param1Int;
      NotificationAssistantService.this.mHandler.obtainMessage(6, someArgs).sendToTarget();
    }
    
    public void onActionClicked(String param1String, Notification.Action param1Action, int param1Int) {
      SomeArgs someArgs = SomeArgs.obtain();
      someArgs.arg1 = param1String;
      someArgs.arg2 = param1Action;
      someArgs.argi2 = param1Int;
      NotificationAssistantService.this.mHandler.obtainMessage(7, someArgs).sendToTarget();
    }
    
    public void onAllowedAdjustmentsChanged() {
      NotificationAssistantService.this.mHandler.obtainMessage(8).sendToTarget();
    }
  }
  
  private void setAdjustmentIssuer(Adjustment paramAdjustment) {
    if (paramAdjustment != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(getOpPackageName());
      stringBuilder.append("/");
      stringBuilder.append(getClass().getName());
      paramAdjustment.setIssuer(stringBuilder.toString());
    } 
  }
  
  public abstract Adjustment onNotificationEnqueued(StatusBarNotification paramStatusBarNotification);
  
  public abstract void onNotificationSnoozedUntilContext(StatusBarNotification paramStatusBarNotification, String paramString);
  
  class MyHandler extends Handler {
    public static final int MSG_ON_ACTION_INVOKED = 7;
    
    public static final int MSG_ON_ALLOWED_ADJUSTMENTS_CHANGED = 8;
    
    public static final int MSG_ON_NOTIFICATIONS_SEEN = 3;
    
    public static final int MSG_ON_NOTIFICATION_DIRECT_REPLY_SENT = 5;
    
    public static final int MSG_ON_NOTIFICATION_ENQUEUED = 1;
    
    public static final int MSG_ON_NOTIFICATION_EXPANSION_CHANGED = 4;
    
    public static final int MSG_ON_NOTIFICATION_SNOOZED = 2;
    
    public static final int MSG_ON_NOTIFICATION_VISIBILITY_CHANGED = 11;
    
    public static final int MSG_ON_PANEL_HIDDEN = 10;
    
    public static final int MSG_ON_PANEL_REVEALED = 9;
    
    public static final int MSG_ON_SUGGESTED_REPLY_SENT = 6;
    
    final NotificationAssistantService this$0;
    
    public MyHandler(Looper param1Looper) {
      super(param1Looper, null, false);
    }
    
    public void handleMessage(Message param1Message) {
      SomeArgs someArgs3;
      CharSequence charSequence;
      SomeArgs someArgs2;
      String str1;
      List<String> list;
      SomeArgs someArgs1;
      String str3;
      SomeArgs someArgs4;
      String str2;
      Notification.Action action;
      SomeArgs someArgs6;
      StatusBarNotification statusBarNotification2;
      int i = param1Message.what;
      boolean bool1 = false, bool2 = false;
      switch (i) {
        default:
          return;
        case 11:
          someArgs3 = (SomeArgs)param1Message.obj;
          str3 = (String)someArgs3.arg1;
          if (someArgs3.argi1 == 1)
            bool2 = true; 
          someArgs3.recycle();
          NotificationAssistantService.this.onNotificationVisibilityChanged(str3, bool2);
        case 10:
          NotificationAssistantService.this.onPanelHidden();
        case 9:
          someArgs3 = (SomeArgs)((Message)someArgs3).obj;
          i = someArgs3.argi1;
          someArgs3.recycle();
          NotificationAssistantService.this.onPanelRevealed(i);
        case 8:
          NotificationAssistantService.this.onAllowedAdjustmentsChanged();
        case 7:
          someArgs3 = (SomeArgs)((Message)someArgs3).obj;
          str3 = (String)someArgs3.arg1;
          action = (Notification.Action)someArgs3.arg2;
          i = someArgs3.argi2;
          someArgs3.recycle();
          NotificationAssistantService.this.onActionInvoked(str3, action, i);
        case 6:
          someArgs6 = (SomeArgs)((Message)someArgs3).obj;
          str3 = (String)someArgs6.arg1;
          charSequence = (CharSequence)someArgs6.arg2;
          i = someArgs6.argi2;
          someArgs6.recycle();
          NotificationAssistantService.this.onSuggestedReplySent(str3, charSequence, i);
        case 5:
          someArgs2 = (SomeArgs)((Message)charSequence).obj;
          str3 = (String)someArgs2.arg1;
          someArgs2.recycle();
          NotificationAssistantService.this.onNotificationDirectReplied(str3);
        case 4:
          someArgs4 = (SomeArgs)((Message)someArgs2).obj;
          str1 = (String)someArgs4.arg1;
          if (someArgs4.argi1 == 1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          if (someArgs4.argi2 == 1)
            bool1 = true; 
          someArgs4.recycle();
          NotificationAssistantService.this.onNotificationExpansionChanged(str1, bool2, bool1);
        case 3:
          someArgs4 = (SomeArgs)((Message)str1).obj;
          list = (List)someArgs4.arg1;
          someArgs4.recycle();
          NotificationAssistantService.this.onNotificationsSeen(list);
        case 2:
          someArgs1 = (SomeArgs)((Message)list).obj;
          statusBarNotification2 = (StatusBarNotification)someArgs1.arg1;
          str2 = (String)someArgs1.arg2;
          someArgs1.recycle();
          NotificationAssistantService.this.onNotificationSnoozedUntilContext(statusBarNotification2, str2);
        case 1:
          break;
      } 
      SomeArgs someArgs5 = (SomeArgs)((Message)someArgs1).obj;
      StatusBarNotification statusBarNotification1 = (StatusBarNotification)someArgs5.arg1;
      NotificationChannel notificationChannel = (NotificationChannel)someArgs5.arg2;
      someArgs5.recycle();
      Adjustment adjustment = NotificationAssistantService.this.onNotificationEnqueued(statusBarNotification1, notificationChannel);
      NotificationAssistantService.this.setAdjustmentIssuer(adjustment);
      if (adjustment != null) {
        if (!NotificationAssistantService.this.isBound()) {
          Log.w("NotificationAssistants", "MSG_ON_NOTIFICATION_ENQUEUED: service not bound, skip.");
          return;
        } 
        try {
          NotificationAssistantService.this.getNotificationInterface().applyEnqueuedAdjustmentFromAssistant(NotificationAssistantService.this.mWrapper, adjustment);
        } catch (RemoteException remoteException) {
          Log.v("NotificationAssistants", "Unable to contact notification manager", (Throwable)remoteException);
          throw remoteException.rethrowFromSystemServer();
        } catch (SecurityException securityException) {
          Log.w("NotificationAssistants", "Enqueue adjustment failed; no longer connected", securityException);
        } 
      } 
    }
  }
}
