package android.service.notification;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IConditionProvider extends IInterface {
  void onConnected() throws RemoteException;
  
  void onSubscribe(Uri paramUri) throws RemoteException;
  
  void onUnsubscribe(Uri paramUri) throws RemoteException;
  
  class Default implements IConditionProvider {
    public void onConnected() throws RemoteException {}
    
    public void onSubscribe(Uri param1Uri) throws RemoteException {}
    
    public void onUnsubscribe(Uri param1Uri) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IConditionProvider {
    private static final String DESCRIPTOR = "android.service.notification.IConditionProvider";
    
    static final int TRANSACTION_onConnected = 1;
    
    static final int TRANSACTION_onSubscribe = 2;
    
    static final int TRANSACTION_onUnsubscribe = 3;
    
    public Stub() {
      attachInterface(this, "android.service.notification.IConditionProvider");
    }
    
    public static IConditionProvider asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.notification.IConditionProvider");
      if (iInterface != null && iInterface instanceof IConditionProvider)
        return (IConditionProvider)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onUnsubscribe";
        } 
        return "onSubscribe";
      } 
      return "onConnected";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.service.notification.IConditionProvider");
            return true;
          } 
          param1Parcel1.enforceInterface("android.service.notification.IConditionProvider");
          if (param1Parcel1.readInt() != 0) {
            Uri uri = Uri.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          onUnsubscribe((Uri)param1Parcel1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.service.notification.IConditionProvider");
        if (param1Parcel1.readInt() != 0) {
          Uri uri = Uri.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onSubscribe((Uri)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.notification.IConditionProvider");
      onConnected();
      return true;
    }
    
    private static class Proxy implements IConditionProvider {
      public static IConditionProvider sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.notification.IConditionProvider";
      }
      
      public void onConnected() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.notification.IConditionProvider");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IConditionProvider.Stub.getDefaultImpl() != null) {
            IConditionProvider.Stub.getDefaultImpl().onConnected();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSubscribe(Uri param2Uri) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.notification.IConditionProvider");
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IConditionProvider.Stub.getDefaultImpl() != null) {
            IConditionProvider.Stub.getDefaultImpl().onSubscribe(param2Uri);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onUnsubscribe(Uri param2Uri) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.notification.IConditionProvider");
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IConditionProvider.Stub.getDefaultImpl() != null) {
            IConditionProvider.Stub.getDefaultImpl().onUnsubscribe(param2Uri);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IConditionProvider param1IConditionProvider) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IConditionProvider != null) {
          Proxy.sDefaultImpl = param1IConditionProvider;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IConditionProvider getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
