package android.service.notification;

import android.util.ArraySet;
import android.util.Log;
import java.util.Calendar;
import java.util.Objects;
import java.util.TimeZone;

public class ScheduleCalendar {
  public static final boolean DEBUG = Log.isLoggable("ConditionProviders", 3);
  
  private final ArraySet<Integer> mDays = new ArraySet();
  
  private final Calendar mCalendar = Calendar.getInstance();
  
  public static final String TAG = "ScheduleCalendar";
  
  private ZenModeConfig.ScheduleInfo mSchedule;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ScheduleCalendar[mDays=");
    stringBuilder.append(this.mDays);
    stringBuilder.append(", mSchedule=");
    stringBuilder.append(this.mSchedule);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public boolean exitAtAlarm() {
    return this.mSchedule.exitAtAlarm;
  }
  
  public void setSchedule(ZenModeConfig.ScheduleInfo paramScheduleInfo) {
    if (Objects.equals(this.mSchedule, paramScheduleInfo))
      return; 
    this.mSchedule = paramScheduleInfo;
    updateDays();
  }
  
  public void maybeSetNextAlarm(long paramLong1, long paramLong2) {
    ZenModeConfig.ScheduleInfo scheduleInfo = this.mSchedule;
    if (scheduleInfo != null && scheduleInfo.exitAtAlarm) {
      if (paramLong2 == 0L)
        this.mSchedule.nextAlarm = 0L; 
      if (paramLong2 > paramLong1) {
        if (this.mSchedule.nextAlarm == 0L || this.mSchedule.nextAlarm < paramLong1) {
          this.mSchedule.nextAlarm = paramLong2;
          return;
        } 
        scheduleInfo = this.mSchedule;
        scheduleInfo.nextAlarm = Math.min(scheduleInfo.nextAlarm, paramLong2);
      } else if (this.mSchedule.nextAlarm < paramLong1) {
        if (DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("All alarms are in the past ");
          stringBuilder.append(this.mSchedule.nextAlarm);
          Log.d("ScheduleCalendar", stringBuilder.toString());
        } 
        this.mSchedule.nextAlarm = 0L;
      } 
    } 
  }
  
  public void setTimeZone(TimeZone paramTimeZone) {
    this.mCalendar.setTimeZone(paramTimeZone);
  }
  
  public long getNextChangeTime(long paramLong) {
    ZenModeConfig.ScheduleInfo scheduleInfo = this.mSchedule;
    if (scheduleInfo == null)
      return 0L; 
    long l = getNextTime(paramLong, scheduleInfo.startHour, this.mSchedule.startMinute);
    paramLong = getNextTime(paramLong, this.mSchedule.endHour, this.mSchedule.endMinute);
    paramLong = Math.min(l, paramLong);
    return paramLong;
  }
  
  private long getNextTime(long paramLong, int paramInt1, int paramInt2) {
    long l = getTime(paramLong, paramInt1, paramInt2);
    if (l <= paramLong) {
      paramLong = addDays(l, 1);
    } else {
      paramLong = l;
    } 
    return paramLong;
  }
  
  private long getTime(long paramLong, int paramInt1, int paramInt2) {
    this.mCalendar.setTimeInMillis(paramLong);
    this.mCalendar.set(11, paramInt1);
    this.mCalendar.set(12, paramInt2);
    this.mCalendar.set(13, 0);
    this.mCalendar.set(14, 0);
    return this.mCalendar.getTimeInMillis();
  }
  
  public boolean isInSchedule(long paramLong) {
    ZenModeConfig.ScheduleInfo scheduleInfo = this.mSchedule;
    boolean bool = false;
    if (scheduleInfo == null || this.mDays.size() == 0)
      return false; 
    long l1 = getTime(paramLong, this.mSchedule.startHour, this.mSchedule.startMinute);
    long l2 = getTime(paramLong, this.mSchedule.endHour, this.mSchedule.endMinute);
    if (l2 <= l1)
      l2 = addDays(l2, 1); 
    if (isInSchedule(-1, paramLong, l1, l2) || isInSchedule(0, paramLong, l1, l2))
      bool = true; 
    return bool;
  }
  
  public boolean isAlarmInSchedule(long paramLong1, long paramLong2) {
    ZenModeConfig.ScheduleInfo scheduleInfo = this.mSchedule;
    boolean bool = false;
    if (scheduleInfo == null || this.mDays.size() == 0)
      return false; 
    long l1 = getTime(paramLong1, this.mSchedule.startHour, this.mSchedule.startMinute);
    long l2 = getTime(paramLong1, this.mSchedule.endHour, this.mSchedule.endMinute);
    if (l2 <= l1)
      l2 = addDays(l2, 1); 
    if ((isInSchedule(-1, paramLong1, l1, l2) && 
      isInSchedule(-1, paramLong2, l1, l2)) || (
      isInSchedule(0, paramLong1, l1, l2) && 
      isInSchedule(0, paramLong2, l1, l2)))
      bool = true; 
    return bool;
  }
  
  public boolean shouldExitForAlarm(long paramLong) {
    ZenModeConfig.ScheduleInfo scheduleInfo = this.mSchedule;
    boolean bool = false;
    if (scheduleInfo == null)
      return false; 
    if (scheduleInfo.exitAtAlarm && this.mSchedule.nextAlarm != 0L && paramLong >= this.mSchedule.nextAlarm) {
      long l = this.mSchedule.nextAlarm;
      if (isAlarmInSchedule(l, paramLong))
        bool = true; 
    } 
    return bool;
  }
  
  private boolean isInSchedule(int paramInt, long paramLong1, long paramLong2, long paramLong3) {
    int i = getDayOfWeek(paramLong1);
    boolean bool = true;
    paramLong2 = addDays(paramLong2, paramInt);
    paramLong3 = addDays(paramLong3, paramInt);
    if (!this.mDays.contains(Integer.valueOf((i - 1 + paramInt % 7 + 7) % 7 + 1)) || paramLong1 < paramLong2 || paramLong1 >= paramLong3)
      bool = false; 
    return bool;
  }
  
  private int getDayOfWeek(long paramLong) {
    this.mCalendar.setTimeInMillis(paramLong);
    return this.mCalendar.get(7);
  }
  
  private void updateDays() {
    this.mDays.clear();
    ZenModeConfig.ScheduleInfo scheduleInfo = this.mSchedule;
    if (scheduleInfo != null && scheduleInfo.days != null)
      for (byte b = 0; b < this.mSchedule.days.length; b++)
        this.mDays.add(Integer.valueOf(this.mSchedule.days[b]));  
  }
  
  private long addDays(long paramLong, int paramInt) {
    this.mCalendar.setTimeInMillis(paramLong);
    this.mCalendar.add(5, paramInt);
    return this.mCalendar.getTimeInMillis();
  }
}
