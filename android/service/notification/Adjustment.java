package android.service.notification;

import android.annotation.SystemApi;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.UserHandle;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public final class Adjustment extends OplusBaseAdjustment implements Parcelable {
  @SystemApi
  public Adjustment(String paramString1, String paramString2, Bundle paramBundle, CharSequence paramCharSequence, int paramInt) {
    this.mPackage = paramString1;
    this.mKey = paramString2;
    this.mSignals = paramBundle;
    this.mExplanation = paramCharSequence;
    this.mUser = paramInt;
  }
  
  public Adjustment(String paramString1, String paramString2, Bundle paramBundle, CharSequence paramCharSequence, UserHandle paramUserHandle) {
    this.mPackage = paramString1;
    this.mKey = paramString2;
    this.mSignals = paramBundle;
    this.mExplanation = paramCharSequence;
    this.mUser = paramUserHandle.getIdentifier();
  }
  
  @SystemApi
  protected Adjustment(Parcel paramParcel) {
    if (paramParcel.readInt() == 1) {
      this.mPackage = paramParcel.readString();
    } else {
      this.mPackage = null;
    } 
    if (paramParcel.readInt() == 1) {
      this.mKey = paramParcel.readString();
    } else {
      this.mKey = null;
    } 
    if (paramParcel.readInt() == 1) {
      this.mExplanation = paramParcel.readCharSequence();
    } else {
      this.mExplanation = null;
    } 
    this.mSignals = paramParcel.readBundle();
    this.mUser = paramParcel.readInt();
    this.mIssuer = paramParcel.readString();
  }
  
  public static final Parcelable.Creator<Adjustment> CREATOR = new Parcelable.Creator<Adjustment>() {
      public Adjustment createFromParcel(Parcel param1Parcel) {
        return new Adjustment(param1Parcel);
      }
      
      public Adjustment[] newArray(int param1Int) {
        return new Adjustment[param1Int];
      }
    };
  
  public static final String KEY_CONTEXTUAL_ACTIONS = "key_contextual_actions";
  
  public static final String KEY_GROUP_KEY = "key_group_key";
  
  public static final String KEY_IMPORTANCE = "key_importance";
  
  @SystemApi
  public static final String KEY_NOT_CONVERSATION = "key_not_conversation";
  
  @SystemApi
  public static final String KEY_PEOPLE = "key_people";
  
  public static final String KEY_RANKING_SCORE = "key_ranking_score";
  
  public static final String KEY_SNOOZE_CRITERIA = "key_snooze_criteria";
  
  public static final String KEY_TEXT_REPLIES = "key_text_replies";
  
  public static final String KEY_USER_SENTIMENT = "key_user_sentiment";
  
  private final CharSequence mExplanation;
  
  private String mIssuer;
  
  private final String mKey;
  
  private final String mPackage;
  
  private final Bundle mSignals;
  
  private final int mUser;
  
  public String getPackage() {
    return this.mPackage;
  }
  
  public String getKey() {
    return this.mKey;
  }
  
  public CharSequence getExplanation() {
    return this.mExplanation;
  }
  
  public Bundle getSignals() {
    return this.mSignals;
  }
  
  @SystemApi
  public int getUser() {
    return this.mUser;
  }
  
  public UserHandle getUserHandle() {
    return UserHandle.of(this.mUser);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (this.mPackage != null) {
      paramParcel.writeInt(1);
      paramParcel.writeString(this.mPackage);
    } else {
      paramParcel.writeInt(0);
    } 
    if (this.mKey != null) {
      paramParcel.writeInt(1);
      paramParcel.writeString(this.mKey);
    } else {
      paramParcel.writeInt(0);
    } 
    if (this.mExplanation != null) {
      paramParcel.writeInt(1);
      paramParcel.writeCharSequence(this.mExplanation);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeBundle(this.mSignals);
    paramParcel.writeInt(this.mUser);
    paramParcel.writeString(this.mIssuer);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Adjustment{mSignals=");
    stringBuilder.append(this.mSignals);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public void setIssuer(String paramString) {
    this.mIssuer = paramString;
  }
  
  public String getIssuer() {
    return this.mIssuer;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Keys implements Annotation {}
}
