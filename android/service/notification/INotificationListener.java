package android.service.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.UserHandle;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

public interface INotificationListener extends IInterface {
  void onActionClicked(String paramString, Notification.Action paramAction, int paramInt) throws RemoteException;
  
  void onAllowedAdjustmentsChanged() throws RemoteException;
  
  void onInterruptionFilterChanged(int paramInt) throws RemoteException;
  
  void onListenerConnected(NotificationRankingUpdate paramNotificationRankingUpdate) throws RemoteException;
  
  void onListenerHintsChanged(int paramInt) throws RemoteException;
  
  void onNotificationChannelGroupModification(String paramString, UserHandle paramUserHandle, NotificationChannelGroup paramNotificationChannelGroup, int paramInt) throws RemoteException;
  
  void onNotificationChannelModification(String paramString, UserHandle paramUserHandle, NotificationChannel paramNotificationChannel, int paramInt) throws RemoteException;
  
  void onNotificationDirectReply(String paramString) throws RemoteException;
  
  void onNotificationEnqueuedWithChannel(IStatusBarNotificationHolder paramIStatusBarNotificationHolder, NotificationChannel paramNotificationChannel) throws RemoteException;
  
  void onNotificationExpansionChanged(String paramString, boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void onNotificationPosted(IStatusBarNotificationHolder paramIStatusBarNotificationHolder, NotificationRankingUpdate paramNotificationRankingUpdate) throws RemoteException;
  
  void onNotificationRankingUpdate(NotificationRankingUpdate paramNotificationRankingUpdate) throws RemoteException;
  
  void onNotificationRemoved(IStatusBarNotificationHolder paramIStatusBarNotificationHolder, NotificationRankingUpdate paramNotificationRankingUpdate, NotificationStats paramNotificationStats, int paramInt) throws RemoteException;
  
  void onNotificationSnoozedUntilContext(IStatusBarNotificationHolder paramIStatusBarNotificationHolder, String paramString) throws RemoteException;
  
  void onNotificationVisibilityChanged(String paramString, boolean paramBoolean) throws RemoteException;
  
  void onNotificationsSeen(List<String> paramList) throws RemoteException;
  
  void onPanelHidden() throws RemoteException;
  
  void onPanelRevealed(int paramInt) throws RemoteException;
  
  void onStatusBarIconsBehaviorChanged(boolean paramBoolean) throws RemoteException;
  
  void onSuggestedReplySent(String paramString, CharSequence paramCharSequence, int paramInt) throws RemoteException;
  
  class Default implements INotificationListener {
    public void onListenerConnected(NotificationRankingUpdate param1NotificationRankingUpdate) throws RemoteException {}
    
    public void onNotificationPosted(IStatusBarNotificationHolder param1IStatusBarNotificationHolder, NotificationRankingUpdate param1NotificationRankingUpdate) throws RemoteException {}
    
    public void onStatusBarIconsBehaviorChanged(boolean param1Boolean) throws RemoteException {}
    
    public void onNotificationRemoved(IStatusBarNotificationHolder param1IStatusBarNotificationHolder, NotificationRankingUpdate param1NotificationRankingUpdate, NotificationStats param1NotificationStats, int param1Int) throws RemoteException {}
    
    public void onNotificationRankingUpdate(NotificationRankingUpdate param1NotificationRankingUpdate) throws RemoteException {}
    
    public void onListenerHintsChanged(int param1Int) throws RemoteException {}
    
    public void onInterruptionFilterChanged(int param1Int) throws RemoteException {}
    
    public void onNotificationChannelModification(String param1String, UserHandle param1UserHandle, NotificationChannel param1NotificationChannel, int param1Int) throws RemoteException {}
    
    public void onNotificationChannelGroupModification(String param1String, UserHandle param1UserHandle, NotificationChannelGroup param1NotificationChannelGroup, int param1Int) throws RemoteException {}
    
    public void onNotificationEnqueuedWithChannel(IStatusBarNotificationHolder param1IStatusBarNotificationHolder, NotificationChannel param1NotificationChannel) throws RemoteException {}
    
    public void onNotificationSnoozedUntilContext(IStatusBarNotificationHolder param1IStatusBarNotificationHolder, String param1String) throws RemoteException {}
    
    public void onNotificationsSeen(List<String> param1List) throws RemoteException {}
    
    public void onPanelRevealed(int param1Int) throws RemoteException {}
    
    public void onPanelHidden() throws RemoteException {}
    
    public void onNotificationVisibilityChanged(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void onNotificationExpansionChanged(String param1String, boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public void onNotificationDirectReply(String param1String) throws RemoteException {}
    
    public void onSuggestedReplySent(String param1String, CharSequence param1CharSequence, int param1Int) throws RemoteException {}
    
    public void onActionClicked(String param1String, Notification.Action param1Action, int param1Int) throws RemoteException {}
    
    public void onAllowedAdjustmentsChanged() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INotificationListener {
    private static final String DESCRIPTOR = "android.service.notification.INotificationListener";
    
    static final int TRANSACTION_onActionClicked = 19;
    
    static final int TRANSACTION_onAllowedAdjustmentsChanged = 20;
    
    static final int TRANSACTION_onInterruptionFilterChanged = 7;
    
    static final int TRANSACTION_onListenerConnected = 1;
    
    static final int TRANSACTION_onListenerHintsChanged = 6;
    
    static final int TRANSACTION_onNotificationChannelGroupModification = 9;
    
    static final int TRANSACTION_onNotificationChannelModification = 8;
    
    static final int TRANSACTION_onNotificationDirectReply = 17;
    
    static final int TRANSACTION_onNotificationEnqueuedWithChannel = 10;
    
    static final int TRANSACTION_onNotificationExpansionChanged = 16;
    
    static final int TRANSACTION_onNotificationPosted = 2;
    
    static final int TRANSACTION_onNotificationRankingUpdate = 5;
    
    static final int TRANSACTION_onNotificationRemoved = 4;
    
    static final int TRANSACTION_onNotificationSnoozedUntilContext = 11;
    
    static final int TRANSACTION_onNotificationVisibilityChanged = 15;
    
    static final int TRANSACTION_onNotificationsSeen = 12;
    
    static final int TRANSACTION_onPanelHidden = 14;
    
    static final int TRANSACTION_onPanelRevealed = 13;
    
    static final int TRANSACTION_onStatusBarIconsBehaviorChanged = 3;
    
    static final int TRANSACTION_onSuggestedReplySent = 18;
    
    public Stub() {
      attachInterface(this, "android.service.notification.INotificationListener");
    }
    
    public static INotificationListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.notification.INotificationListener");
      if (iInterface != null && iInterface instanceof INotificationListener)
        return (INotificationListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 20:
          return "onAllowedAdjustmentsChanged";
        case 19:
          return "onActionClicked";
        case 18:
          return "onSuggestedReplySent";
        case 17:
          return "onNotificationDirectReply";
        case 16:
          return "onNotificationExpansionChanged";
        case 15:
          return "onNotificationVisibilityChanged";
        case 14:
          return "onPanelHidden";
        case 13:
          return "onPanelRevealed";
        case 12:
          return "onNotificationsSeen";
        case 11:
          return "onNotificationSnoozedUntilContext";
        case 10:
          return "onNotificationEnqueuedWithChannel";
        case 9:
          return "onNotificationChannelGroupModification";
        case 8:
          return "onNotificationChannelModification";
        case 7:
          return "onInterruptionFilterChanged";
        case 6:
          return "onListenerHintsChanged";
        case 5:
          return "onNotificationRankingUpdate";
        case 4:
          return "onNotificationRemoved";
        case 3:
          return "onStatusBarIconsBehaviorChanged";
        case 2:
          return "onNotificationPosted";
        case 1:
          break;
      } 
      return "onListenerConnected";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IStatusBarNotificationHolder iStatusBarNotificationHolder;
      if (param1Int1 != 1598968902) {
        String str2;
        ArrayList<String> arrayList;
        String str1, str3, str4, str5;
        IStatusBarNotificationHolder iStatusBarNotificationHolder1;
        boolean bool1 = false, bool2 = false, bool3 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 20:
            param1Parcel1.enforceInterface("android.service.notification.INotificationListener");
            onAllowedAdjustmentsChanged();
            return true;
          case 19:
            param1Parcel1.enforceInterface("android.service.notification.INotificationListener");
            str4 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Notification.Action action = Notification.Action.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            onActionClicked(str4, (Notification.Action)param1Parcel2, param1Int1);
            return true;
          case 18:
            param1Parcel1.enforceInterface("android.service.notification.INotificationListener");
            str4 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              CharSequence charSequence = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            onSuggestedReplySent(str4, (CharSequence)param1Parcel2, param1Int1);
            return true;
          case 17:
            param1Parcel1.enforceInterface("android.service.notification.INotificationListener");
            str2 = param1Parcel1.readString();
            onNotificationDirectReply(str2);
            return true;
          case 16:
            str2.enforceInterface("android.service.notification.INotificationListener");
            str3 = str2.readString();
            if (str2.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            if (str2.readInt() != 0)
              bool3 = true; 
            onNotificationExpansionChanged(str3, bool1, bool3);
            return true;
          case 15:
            str2.enforceInterface("android.service.notification.INotificationListener");
            str3 = str2.readString();
            if (str2.readInt() != 0)
              bool1 = true; 
            onNotificationVisibilityChanged(str3, bool1);
            return true;
          case 14:
            str2.enforceInterface("android.service.notification.INotificationListener");
            onPanelHidden();
            return true;
          case 13:
            str2.enforceInterface("android.service.notification.INotificationListener");
            param1Int1 = str2.readInt();
            onPanelRevealed(param1Int1);
            return true;
          case 12:
            str2.enforceInterface("android.service.notification.INotificationListener");
            arrayList = str2.createStringArrayList();
            onNotificationsSeen(arrayList);
            return true;
          case 11:
            arrayList.enforceInterface("android.service.notification.INotificationListener");
            iStatusBarNotificationHolder = IStatusBarNotificationHolder.Stub.asInterface(arrayList.readStrongBinder());
            str1 = arrayList.readString();
            onNotificationSnoozedUntilContext(iStatusBarNotificationHolder, str1);
            return true;
          case 10:
            str1.enforceInterface("android.service.notification.INotificationListener");
            iStatusBarNotificationHolder = IStatusBarNotificationHolder.Stub.asInterface(str1.readStrongBinder());
            if (str1.readInt() != 0) {
              NotificationChannel notificationChannel = NotificationChannel.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            onNotificationEnqueuedWithChannel(iStatusBarNotificationHolder, (NotificationChannel)str1);
            return true;
          case 9:
            str1.enforceInterface("android.service.notification.INotificationListener");
            str5 = str1.readString();
            if (str1.readInt() != 0) {
              UserHandle userHandle = UserHandle.CREATOR.createFromParcel((Parcel)str1);
            } else {
              iStatusBarNotificationHolder = null;
            } 
            if (str1.readInt() != 0) {
              NotificationChannelGroup notificationChannelGroup = NotificationChannelGroup.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str4 = null;
            } 
            param1Int1 = str1.readInt();
            onNotificationChannelGroupModification(str5, (UserHandle)iStatusBarNotificationHolder, (NotificationChannelGroup)str4, param1Int1);
            return true;
          case 8:
            str1.enforceInterface("android.service.notification.INotificationListener");
            str5 = str1.readString();
            if (str1.readInt() != 0) {
              UserHandle userHandle = UserHandle.CREATOR.createFromParcel((Parcel)str1);
            } else {
              iStatusBarNotificationHolder = null;
            } 
            if (str1.readInt() != 0) {
              NotificationChannel notificationChannel = NotificationChannel.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str4 = null;
            } 
            param1Int1 = str1.readInt();
            onNotificationChannelModification(str5, (UserHandle)iStatusBarNotificationHolder, (NotificationChannel)str4, param1Int1);
            return true;
          case 7:
            str1.enforceInterface("android.service.notification.INotificationListener");
            param1Int1 = str1.readInt();
            onInterruptionFilterChanged(param1Int1);
            return true;
          case 6:
            str1.enforceInterface("android.service.notification.INotificationListener");
            param1Int1 = str1.readInt();
            onListenerHintsChanged(param1Int1);
            return true;
          case 5:
            str1.enforceInterface("android.service.notification.INotificationListener");
            if (str1.readInt() != 0) {
              NotificationRankingUpdate notificationRankingUpdate = NotificationRankingUpdate.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            onNotificationRankingUpdate((NotificationRankingUpdate)str1);
            return true;
          case 4:
            str1.enforceInterface("android.service.notification.INotificationListener");
            iStatusBarNotificationHolder1 = IStatusBarNotificationHolder.Stub.asInterface(str1.readStrongBinder());
            if (str1.readInt() != 0) {
              NotificationRankingUpdate notificationRankingUpdate = NotificationRankingUpdate.CREATOR.createFromParcel((Parcel)str1);
            } else {
              iStatusBarNotificationHolder = null;
            } 
            if (str1.readInt() != 0) {
              NotificationStats notificationStats = NotificationStats.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str4 = null;
            } 
            param1Int1 = str1.readInt();
            onNotificationRemoved(iStatusBarNotificationHolder1, (NotificationRankingUpdate)iStatusBarNotificationHolder, (NotificationStats)str4, param1Int1);
            return true;
          case 3:
            str1.enforceInterface("android.service.notification.INotificationListener");
            bool1 = bool2;
            if (str1.readInt() != 0)
              bool1 = true; 
            onStatusBarIconsBehaviorChanged(bool1);
            return true;
          case 2:
            str1.enforceInterface("android.service.notification.INotificationListener");
            iStatusBarNotificationHolder = IStatusBarNotificationHolder.Stub.asInterface(str1.readStrongBinder());
            if (str1.readInt() != 0) {
              NotificationRankingUpdate notificationRankingUpdate = NotificationRankingUpdate.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            onNotificationPosted(iStatusBarNotificationHolder, (NotificationRankingUpdate)str1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.service.notification.INotificationListener");
        if (str1.readInt() != 0) {
          NotificationRankingUpdate notificationRankingUpdate = NotificationRankingUpdate.CREATOR.createFromParcel((Parcel)str1);
        } else {
          str1 = null;
        } 
        onListenerConnected((NotificationRankingUpdate)str1);
        return true;
      } 
      iStatusBarNotificationHolder.writeString("android.service.notification.INotificationListener");
      return true;
    }
    
    private static class Proxy implements INotificationListener {
      public static INotificationListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.notification.INotificationListener";
      }
      
      public void onListenerConnected(NotificationRankingUpdate param2NotificationRankingUpdate) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.notification.INotificationListener");
          if (param2NotificationRankingUpdate != null) {
            parcel.writeInt(1);
            param2NotificationRankingUpdate.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && INotificationListener.Stub.getDefaultImpl() != null) {
            INotificationListener.Stub.getDefaultImpl().onListenerConnected(param2NotificationRankingUpdate);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onNotificationPosted(IStatusBarNotificationHolder param2IStatusBarNotificationHolder, NotificationRankingUpdate param2NotificationRankingUpdate) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.notification.INotificationListener");
          if (param2IStatusBarNotificationHolder != null) {
            iBinder = param2IStatusBarNotificationHolder.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2NotificationRankingUpdate != null) {
            parcel.writeInt(1);
            param2NotificationRankingUpdate.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && INotificationListener.Stub.getDefaultImpl() != null) {
            INotificationListener.Stub.getDefaultImpl().onNotificationPosted(param2IStatusBarNotificationHolder, param2NotificationRankingUpdate);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onStatusBarIconsBehaviorChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.service.notification.INotificationListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool1 && INotificationListener.Stub.getDefaultImpl() != null) {
            INotificationListener.Stub.getDefaultImpl().onStatusBarIconsBehaviorChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onNotificationRemoved(IStatusBarNotificationHolder param2IStatusBarNotificationHolder, NotificationRankingUpdate param2NotificationRankingUpdate, NotificationStats param2NotificationStats, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.notification.INotificationListener");
          if (param2IStatusBarNotificationHolder != null) {
            iBinder = param2IStatusBarNotificationHolder.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2NotificationRankingUpdate != null) {
            parcel.writeInt(1);
            param2NotificationRankingUpdate.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2NotificationStats != null) {
            parcel.writeInt(1);
            param2NotificationStats.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && INotificationListener.Stub.getDefaultImpl() != null) {
            INotificationListener.Stub.getDefaultImpl().onNotificationRemoved(param2IStatusBarNotificationHolder, param2NotificationRankingUpdate, param2NotificationStats, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onNotificationRankingUpdate(NotificationRankingUpdate param2NotificationRankingUpdate) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.notification.INotificationListener");
          if (param2NotificationRankingUpdate != null) {
            parcel.writeInt(1);
            param2NotificationRankingUpdate.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && INotificationListener.Stub.getDefaultImpl() != null) {
            INotificationListener.Stub.getDefaultImpl().onNotificationRankingUpdate(param2NotificationRankingUpdate);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onListenerHintsChanged(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.notification.INotificationListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && INotificationListener.Stub.getDefaultImpl() != null) {
            INotificationListener.Stub.getDefaultImpl().onListenerHintsChanged(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onInterruptionFilterChanged(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.notification.INotificationListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && INotificationListener.Stub.getDefaultImpl() != null) {
            INotificationListener.Stub.getDefaultImpl().onInterruptionFilterChanged(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onNotificationChannelModification(String param2String, UserHandle param2UserHandle, NotificationChannel param2NotificationChannel, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.notification.INotificationListener");
          parcel.writeString(param2String);
          if (param2UserHandle != null) {
            parcel.writeInt(1);
            param2UserHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2NotificationChannel != null) {
            parcel.writeInt(1);
            param2NotificationChannel.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && INotificationListener.Stub.getDefaultImpl() != null) {
            INotificationListener.Stub.getDefaultImpl().onNotificationChannelModification(param2String, param2UserHandle, param2NotificationChannel, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onNotificationChannelGroupModification(String param2String, UserHandle param2UserHandle, NotificationChannelGroup param2NotificationChannelGroup, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.notification.INotificationListener");
          parcel.writeString(param2String);
          if (param2UserHandle != null) {
            parcel.writeInt(1);
            param2UserHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2NotificationChannelGroup != null) {
            parcel.writeInt(1);
            param2NotificationChannelGroup.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel, (Parcel)null, 1);
          if (!bool && INotificationListener.Stub.getDefaultImpl() != null) {
            INotificationListener.Stub.getDefaultImpl().onNotificationChannelGroupModification(param2String, param2UserHandle, param2NotificationChannelGroup, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onNotificationEnqueuedWithChannel(IStatusBarNotificationHolder param2IStatusBarNotificationHolder, NotificationChannel param2NotificationChannel) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.notification.INotificationListener");
          if (param2IStatusBarNotificationHolder != null) {
            iBinder = param2IStatusBarNotificationHolder.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2NotificationChannel != null) {
            parcel.writeInt(1);
            param2NotificationChannel.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel, (Parcel)null, 1);
          if (!bool && INotificationListener.Stub.getDefaultImpl() != null) {
            INotificationListener.Stub.getDefaultImpl().onNotificationEnqueuedWithChannel(param2IStatusBarNotificationHolder, param2NotificationChannel);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onNotificationSnoozedUntilContext(IStatusBarNotificationHolder param2IStatusBarNotificationHolder, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.notification.INotificationListener");
          if (param2IStatusBarNotificationHolder != null) {
            iBinder = param2IStatusBarNotificationHolder.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(11, parcel, (Parcel)null, 1);
          if (!bool && INotificationListener.Stub.getDefaultImpl() != null) {
            INotificationListener.Stub.getDefaultImpl().onNotificationSnoozedUntilContext(param2IStatusBarNotificationHolder, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onNotificationsSeen(List<String> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.notification.INotificationListener");
          parcel.writeStringList(param2List);
          boolean bool = this.mRemote.transact(12, parcel, (Parcel)null, 1);
          if (!bool && INotificationListener.Stub.getDefaultImpl() != null) {
            INotificationListener.Stub.getDefaultImpl().onNotificationsSeen(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPanelRevealed(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.notification.INotificationListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel, (Parcel)null, 1);
          if (!bool && INotificationListener.Stub.getDefaultImpl() != null) {
            INotificationListener.Stub.getDefaultImpl().onPanelRevealed(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPanelHidden() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.notification.INotificationListener");
          boolean bool = this.mRemote.transact(14, parcel, (Parcel)null, 1);
          if (!bool && INotificationListener.Stub.getDefaultImpl() != null) {
            INotificationListener.Stub.getDefaultImpl().onPanelHidden();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onNotificationVisibilityChanged(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.service.notification.INotificationListener");
          parcel.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(15, parcel, (Parcel)null, 1);
          if (!bool1 && INotificationListener.Stub.getDefaultImpl() != null) {
            INotificationListener.Stub.getDefaultImpl().onNotificationVisibilityChanged(param2String, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onNotificationExpansionChanged(String param2String, boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.notification.INotificationListener");
          parcel.writeString(param2String);
          boolean bool1 = false;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel.writeInt(bool2);
          boolean bool2 = bool1;
          if (param2Boolean2)
            bool2 = true; 
          parcel.writeInt(bool2);
          boolean bool = this.mRemote.transact(16, parcel, (Parcel)null, 1);
          if (!bool && INotificationListener.Stub.getDefaultImpl() != null) {
            INotificationListener.Stub.getDefaultImpl().onNotificationExpansionChanged(param2String, param2Boolean1, param2Boolean2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onNotificationDirectReply(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.notification.INotificationListener");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(17, parcel, (Parcel)null, 1);
          if (!bool && INotificationListener.Stub.getDefaultImpl() != null) {
            INotificationListener.Stub.getDefaultImpl().onNotificationDirectReply(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSuggestedReplySent(String param2String, CharSequence param2CharSequence, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.notification.INotificationListener");
          parcel.writeString(param2String);
          if (param2CharSequence != null) {
            parcel.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(18, parcel, (Parcel)null, 1);
          if (!bool && INotificationListener.Stub.getDefaultImpl() != null) {
            INotificationListener.Stub.getDefaultImpl().onSuggestedReplySent(param2String, param2CharSequence, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onActionClicked(String param2String, Notification.Action param2Action, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.notification.INotificationListener");
          parcel.writeString(param2String);
          if (param2Action != null) {
            parcel.writeInt(1);
            param2Action.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(19, parcel, (Parcel)null, 1);
          if (!bool && INotificationListener.Stub.getDefaultImpl() != null) {
            INotificationListener.Stub.getDefaultImpl().onActionClicked(param2String, param2Action, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAllowedAdjustmentsChanged() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.notification.INotificationListener");
          boolean bool = this.mRemote.transact(20, parcel, (Parcel)null, 1);
          if (!bool && INotificationListener.Stub.getDefaultImpl() != null) {
            INotificationListener.Stub.getDefaultImpl().onAllowedAdjustmentsChanged();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INotificationListener param1INotificationListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INotificationListener != null) {
          Proxy.sDefaultImpl = param1INotificationListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INotificationListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
