package android.service.notification;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class SnoozeCriterion implements Parcelable {
  public SnoozeCriterion(String paramString, CharSequence paramCharSequence1, CharSequence paramCharSequence2) {
    this.mId = paramString;
    this.mExplanation = paramCharSequence1;
    this.mConfirmation = paramCharSequence2;
  }
  
  protected SnoozeCriterion(Parcel paramParcel) {
    if (paramParcel.readByte() != 0) {
      this.mId = paramParcel.readString();
    } else {
      this.mId = null;
    } 
    if (paramParcel.readByte() != 0) {
      this.mExplanation = paramParcel.readCharSequence();
    } else {
      this.mExplanation = null;
    } 
    if (paramParcel.readByte() != 0) {
      this.mConfirmation = paramParcel.readCharSequence();
    } else {
      this.mConfirmation = null;
    } 
  }
  
  public String getId() {
    return this.mId;
  }
  
  public CharSequence getExplanation() {
    return this.mExplanation;
  }
  
  public CharSequence getConfirmation() {
    return this.mConfirmation;
  }
  
  public static final Parcelable.Creator<SnoozeCriterion> CREATOR = new Parcelable.Creator<SnoozeCriterion>() {
      public SnoozeCriterion createFromParcel(Parcel param1Parcel) {
        return new SnoozeCriterion(param1Parcel);
      }
      
      public SnoozeCriterion[] newArray(int param1Int) {
        return new SnoozeCriterion[param1Int];
      }
    };
  
  private final CharSequence mConfirmation;
  
  private final CharSequence mExplanation;
  
  private final String mId;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (this.mId != null) {
      paramParcel.writeByte((byte)1);
      paramParcel.writeString(this.mId);
    } else {
      paramParcel.writeByte((byte)0);
    } 
    if (this.mExplanation != null) {
      paramParcel.writeByte((byte)1);
      paramParcel.writeCharSequence(this.mExplanation);
    } else {
      paramParcel.writeByte((byte)0);
    } 
    if (this.mConfirmation != null) {
      paramParcel.writeByte((byte)1);
      paramParcel.writeCharSequence(this.mConfirmation);
    } else {
      paramParcel.writeByte((byte)0);
    } 
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    String str = this.mId;
    if ((str != null) ? !str.equals(((SnoozeCriterion)paramObject).mId) : (((SnoozeCriterion)paramObject).mId != null))
      return false; 
    CharSequence charSequence = this.mExplanation;
    if ((charSequence != null) ? !charSequence.equals(((SnoozeCriterion)paramObject).mExplanation) : (((SnoozeCriterion)paramObject).mExplanation != null))
      return false; 
    charSequence = this.mConfirmation;
    if (charSequence != null) {
      bool = charSequence.equals(((SnoozeCriterion)paramObject).mConfirmation);
    } else if (((SnoozeCriterion)paramObject).mConfirmation != null) {
      bool = false;
    } 
    return bool;
  }
  
  public int hashCode() {
    byte b1, b2;
    String str = this.mId;
    int i = 0;
    if (str != null) {
      b1 = str.hashCode();
    } else {
      b1 = 0;
    } 
    CharSequence charSequence = this.mExplanation;
    if (charSequence != null) {
      b2 = charSequence.hashCode();
    } else {
      b2 = 0;
    } 
    charSequence = this.mConfirmation;
    if (charSequence != null)
      i = charSequence.hashCode(); 
    return (b1 * 31 + b2) * 31 + i;
  }
}
