package android.service.resolver;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class ResolverTarget implements Parcelable {
  public ResolverTarget() {}
  
  ResolverTarget(Parcel paramParcel) {
    this.mRecencyScore = paramParcel.readFloat();
    this.mTimeSpentScore = paramParcel.readFloat();
    this.mLaunchScore = paramParcel.readFloat();
    this.mChooserScore = paramParcel.readFloat();
    this.mSelectProbability = paramParcel.readFloat();
  }
  
  public float getRecencyScore() {
    return this.mRecencyScore;
  }
  
  public void setRecencyScore(float paramFloat) {
    this.mRecencyScore = paramFloat;
  }
  
  public float getTimeSpentScore() {
    return this.mTimeSpentScore;
  }
  
  public void setTimeSpentScore(float paramFloat) {
    this.mTimeSpentScore = paramFloat;
  }
  
  public float getLaunchScore() {
    return this.mLaunchScore;
  }
  
  public void setLaunchScore(float paramFloat) {
    this.mLaunchScore = paramFloat;
  }
  
  public float getChooserScore() {
    return this.mChooserScore;
  }
  
  public void setChooserScore(float paramFloat) {
    this.mChooserScore = paramFloat;
  }
  
  public float getSelectProbability() {
    return this.mSelectProbability;
  }
  
  public void setSelectProbability(float paramFloat) {
    this.mSelectProbability = paramFloat;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ResolverTarget{");
    stringBuilder.append(this.mRecencyScore);
    stringBuilder.append(", ");
    stringBuilder.append(this.mTimeSpentScore);
    stringBuilder.append(", ");
    stringBuilder.append(this.mLaunchScore);
    stringBuilder.append(", ");
    stringBuilder.append(this.mChooserScore);
    stringBuilder.append(", ");
    stringBuilder.append(this.mSelectProbability);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeFloat(this.mRecencyScore);
    paramParcel.writeFloat(this.mTimeSpentScore);
    paramParcel.writeFloat(this.mLaunchScore);
    paramParcel.writeFloat(this.mChooserScore);
    paramParcel.writeFloat(this.mSelectProbability);
  }
  
  public static final Parcelable.Creator<ResolverTarget> CREATOR = new Parcelable.Creator<ResolverTarget>() {
      public ResolverTarget createFromParcel(Parcel param1Parcel) {
        return new ResolverTarget(param1Parcel);
      }
      
      public ResolverTarget[] newArray(int param1Int) {
        return new ResolverTarget[param1Int];
      }
    };
  
  private static final String TAG = "ResolverTarget";
  
  private float mChooserScore;
  
  private float mLaunchScore;
  
  private float mRecencyScore;
  
  private float mSelectProbability;
  
  private float mTimeSpentScore;
}
