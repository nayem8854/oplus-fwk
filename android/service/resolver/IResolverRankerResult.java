package android.service.resolver;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IResolverRankerResult extends IInterface {
  void sendResult(List<ResolverTarget> paramList) throws RemoteException;
  
  class Default implements IResolverRankerResult {
    public void sendResult(List<ResolverTarget> param1List) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IResolverRankerResult {
    private static final String DESCRIPTOR = "android.service.resolver.IResolverRankerResult";
    
    static final int TRANSACTION_sendResult = 1;
    
    public Stub() {
      attachInterface(this, "android.service.resolver.IResolverRankerResult");
    }
    
    public static IResolverRankerResult asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.resolver.IResolverRankerResult");
      if (iInterface != null && iInterface instanceof IResolverRankerResult)
        return (IResolverRankerResult)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "sendResult";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.service.resolver.IResolverRankerResult");
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.resolver.IResolverRankerResult");
      ArrayList<ResolverTarget> arrayList = param1Parcel1.createTypedArrayList(ResolverTarget.CREATOR);
      sendResult(arrayList);
      return true;
    }
    
    private static class Proxy implements IResolverRankerResult {
      public static IResolverRankerResult sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.resolver.IResolverRankerResult";
      }
      
      public void sendResult(List<ResolverTarget> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.resolver.IResolverRankerResult");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IResolverRankerResult.Stub.getDefaultImpl() != null) {
            IResolverRankerResult.Stub.getDefaultImpl().sendResult(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IResolverRankerResult param1IResolverRankerResult) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IResolverRankerResult != null) {
          Proxy.sDefaultImpl = param1IResolverRankerResult;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IResolverRankerResult getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
