package android.service.resolver;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import java.util.List;

@SystemApi
public abstract class ResolverRankerService extends Service {
  public static final String BIND_PERMISSION = "android.permission.BIND_RESOLVER_RANKER_SERVICE";
  
  private static final boolean DEBUG = false;
  
  private static final String HANDLER_THREAD_NAME = "RESOLVER_RANKER_SERVICE";
  
  public static final String HOLD_PERMISSION = "android.permission.PROVIDE_RESOLVER_RANKER_SERVICE";
  
  public static final String SERVICE_INTERFACE = "android.service.resolver.ResolverRankerService";
  
  private static final String TAG = "ResolverRankerService";
  
  private volatile Handler mHandler;
  
  private HandlerThread mHandlerThread;
  
  private ResolverRankerServiceWrapper mWrapper = null;
  
  public void onPredictSharingProbabilities(List<ResolverTarget> paramList) {}
  
  public void onTrainRankingModel(List<ResolverTarget> paramList, int paramInt) {}
  
  public IBinder onBind(Intent paramIntent) {
    if (!"android.service.resolver.ResolverRankerService".equals(paramIntent.getAction()))
      return null; 
    if (this.mHandlerThread == null) {
      HandlerThread handlerThread = new HandlerThread("RESOLVER_RANKER_SERVICE");
      handlerThread.start();
      this.mHandler = new Handler(this.mHandlerThread.getLooper());
    } 
    if (this.mWrapper == null)
      this.mWrapper = new ResolverRankerServiceWrapper(); 
    return this.mWrapper;
  }
  
  public void onDestroy() {
    this.mHandler = null;
    HandlerThread handlerThread = this.mHandlerThread;
    if (handlerThread != null)
      handlerThread.quitSafely(); 
    super.onDestroy();
  }
  
  private static void sendResult(List<ResolverTarget> paramList, IResolverRankerResult paramIResolverRankerResult) {
    try {
      paramIResolverRankerResult.sendResult(paramList);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("failed to send results: ");
      stringBuilder.append(exception);
      Log.e("ResolverRankerService", stringBuilder.toString());
    } 
  }
  
  private class ResolverRankerServiceWrapper extends IResolverRankerService.Stub {
    final ResolverRankerService this$0;
    
    private ResolverRankerServiceWrapper() {}
    
    public void predict(List<ResolverTarget> param1List, IResolverRankerResult param1IResolverRankerResult) throws RemoteException {
      Object object = new Object(this, param1List, param1IResolverRankerResult);
      Handler handler = ResolverRankerService.this.mHandler;
      if (handler != null)
        handler.post((Runnable)object); 
    }
    
    public void train(List<ResolverTarget> param1List, int param1Int) throws RemoteException {
      Object object = new Object(this, param1List, param1Int);
      Handler handler = ResolverRankerService.this.mHandler;
      if (handler != null)
        handler.post((Runnable)object); 
    }
  }
}
