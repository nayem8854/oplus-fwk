package android.service.resolver;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IResolverRankerService extends IInterface {
  void predict(List<ResolverTarget> paramList, IResolverRankerResult paramIResolverRankerResult) throws RemoteException;
  
  void train(List<ResolverTarget> paramList, int paramInt) throws RemoteException;
  
  class Default implements IResolverRankerService {
    public void predict(List<ResolverTarget> param1List, IResolverRankerResult param1IResolverRankerResult) throws RemoteException {}
    
    public void train(List<ResolverTarget> param1List, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IResolverRankerService {
    private static final String DESCRIPTOR = "android.service.resolver.IResolverRankerService";
    
    static final int TRANSACTION_predict = 1;
    
    static final int TRANSACTION_train = 2;
    
    public Stub() {
      attachInterface(this, "android.service.resolver.IResolverRankerService");
    }
    
    public static IResolverRankerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.resolver.IResolverRankerService");
      if (iInterface != null && iInterface instanceof IResolverRankerService)
        return (IResolverRankerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "train";
      } 
      return "predict";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.service.resolver.IResolverRankerService");
          return true;
        } 
        param1Parcel1.enforceInterface("android.service.resolver.IResolverRankerService");
        ArrayList<ResolverTarget> arrayList1 = param1Parcel1.createTypedArrayList(ResolverTarget.CREATOR);
        param1Int1 = param1Parcel1.readInt();
        train(arrayList1, param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.resolver.IResolverRankerService");
      ArrayList<ResolverTarget> arrayList = param1Parcel1.createTypedArrayList(ResolverTarget.CREATOR);
      IResolverRankerResult iResolverRankerResult = IResolverRankerResult.Stub.asInterface(param1Parcel1.readStrongBinder());
      predict(arrayList, iResolverRankerResult);
      return true;
    }
    
    private static class Proxy implements IResolverRankerService {
      public static IResolverRankerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.resolver.IResolverRankerService";
      }
      
      public void predict(List<ResolverTarget> param2List, IResolverRankerResult param2IResolverRankerResult) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.resolver.IResolverRankerService");
          parcel.writeTypedList(param2List);
          if (param2IResolverRankerResult != null) {
            iBinder = param2IResolverRankerResult.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IResolverRankerService.Stub.getDefaultImpl() != null) {
            IResolverRankerService.Stub.getDefaultImpl().predict(param2List, param2IResolverRankerResult);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void train(List<ResolverTarget> param2List, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.resolver.IResolverRankerService");
          parcel.writeTypedList(param2List);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IResolverRankerService.Stub.getDefaultImpl() != null) {
            IResolverRankerService.Stub.getDefaultImpl().train(param2List, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IResolverRankerService param1IResolverRankerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IResolverRankerService != null) {
          Proxy.sDefaultImpl = param1IResolverRankerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IResolverRankerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
