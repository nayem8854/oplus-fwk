package android.service.contentcapture;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.contentcapture.ContentCaptureContext;
import android.view.contentcapture.DataRemovalRequest;
import android.view.contentcapture.DataShareRequest;
import com.android.internal.os.IResultReceiver;

public interface IContentCaptureService extends IInterface {
  void onActivityEvent(ActivityEvent paramActivityEvent) throws RemoteException;
  
  void onActivitySnapshot(int paramInt, SnapshotData paramSnapshotData) throws RemoteException;
  
  void onConnected(IBinder paramIBinder, boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void onDataRemovalRequest(DataRemovalRequest paramDataRemovalRequest) throws RemoteException;
  
  void onDataShared(DataShareRequest paramDataShareRequest, IDataShareCallback paramIDataShareCallback) throws RemoteException;
  
  void onDisconnected() throws RemoteException;
  
  void onSessionFinished(int paramInt) throws RemoteException;
  
  void onSessionStarted(ContentCaptureContext paramContentCaptureContext, int paramInt1, int paramInt2, IResultReceiver paramIResultReceiver, int paramInt3) throws RemoteException;
  
  class Default implements IContentCaptureService {
    public void onConnected(IBinder param1IBinder, boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public void onDisconnected() throws RemoteException {}
    
    public void onSessionStarted(ContentCaptureContext param1ContentCaptureContext, int param1Int1, int param1Int2, IResultReceiver param1IResultReceiver, int param1Int3) throws RemoteException {}
    
    public void onSessionFinished(int param1Int) throws RemoteException {}
    
    public void onActivitySnapshot(int param1Int, SnapshotData param1SnapshotData) throws RemoteException {}
    
    public void onDataRemovalRequest(DataRemovalRequest param1DataRemovalRequest) throws RemoteException {}
    
    public void onDataShared(DataShareRequest param1DataShareRequest, IDataShareCallback param1IDataShareCallback) throws RemoteException {}
    
    public void onActivityEvent(ActivityEvent param1ActivityEvent) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IContentCaptureService {
    private static final String DESCRIPTOR = "android.service.contentcapture.IContentCaptureService";
    
    static final int TRANSACTION_onActivityEvent = 8;
    
    static final int TRANSACTION_onActivitySnapshot = 5;
    
    static final int TRANSACTION_onConnected = 1;
    
    static final int TRANSACTION_onDataRemovalRequest = 6;
    
    static final int TRANSACTION_onDataShared = 7;
    
    static final int TRANSACTION_onDisconnected = 2;
    
    static final int TRANSACTION_onSessionFinished = 4;
    
    static final int TRANSACTION_onSessionStarted = 3;
    
    public Stub() {
      attachInterface(this, "android.service.contentcapture.IContentCaptureService");
    }
    
    public static IContentCaptureService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.contentcapture.IContentCaptureService");
      if (iInterface != null && iInterface instanceof IContentCaptureService)
        return (IContentCaptureService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "onActivityEvent";
        case 7:
          return "onDataShared";
        case 6:
          return "onDataRemovalRequest";
        case 5:
          return "onActivitySnapshot";
        case 4:
          return "onSessionFinished";
        case 3:
          return "onSessionStarted";
        case 2:
          return "onDisconnected";
        case 1:
          break;
      } 
      return "onConnected";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IBinder iBinder;
      if (param1Int1 != 1598968902) {
        IDataShareCallback iDataShareCallback;
        int i;
        IResultReceiver iResultReceiver;
        boolean bool2;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("android.service.contentcapture.IContentCaptureService");
            if (param1Parcel1.readInt() != 0) {
              ActivityEvent activityEvent = ActivityEvent.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onActivityEvent((ActivityEvent)param1Parcel1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.service.contentcapture.IContentCaptureService");
            if (param1Parcel1.readInt() != 0) {
              DataShareRequest dataShareRequest = DataShareRequest.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            iDataShareCallback = IDataShareCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            onDataShared((DataShareRequest)param1Parcel2, iDataShareCallback);
            return true;
          case 6:
            iDataShareCallback.enforceInterface("android.service.contentcapture.IContentCaptureService");
            if (iDataShareCallback.readInt() != 0) {
              DataRemovalRequest dataRemovalRequest = DataRemovalRequest.CREATOR.createFromParcel((Parcel)iDataShareCallback);
            } else {
              iDataShareCallback = null;
            } 
            onDataRemovalRequest((DataRemovalRequest)iDataShareCallback);
            return true;
          case 5:
            iDataShareCallback.enforceInterface("android.service.contentcapture.IContentCaptureService");
            param1Int1 = iDataShareCallback.readInt();
            if (iDataShareCallback.readInt() != 0) {
              SnapshotData snapshotData = SnapshotData.CREATOR.createFromParcel((Parcel)iDataShareCallback);
            } else {
              iDataShareCallback = null;
            } 
            onActivitySnapshot(param1Int1, (SnapshotData)iDataShareCallback);
            return true;
          case 4:
            iDataShareCallback.enforceInterface("android.service.contentcapture.IContentCaptureService");
            param1Int1 = iDataShareCallback.readInt();
            onSessionFinished(param1Int1);
            return true;
          case 3:
            iDataShareCallback.enforceInterface("android.service.contentcapture.IContentCaptureService");
            if (iDataShareCallback.readInt() != 0) {
              ContentCaptureContext contentCaptureContext = ContentCaptureContext.CREATOR.createFromParcel((Parcel)iDataShareCallback);
            } else {
              param1Parcel2 = null;
            } 
            param1Int2 = iDataShareCallback.readInt();
            i = iDataShareCallback.readInt();
            iResultReceiver = IResultReceiver.Stub.asInterface(iDataShareCallback.readStrongBinder());
            param1Int1 = iDataShareCallback.readInt();
            onSessionStarted((ContentCaptureContext)param1Parcel2, param1Int2, i, iResultReceiver, param1Int1);
            return true;
          case 2:
            iDataShareCallback.enforceInterface("android.service.contentcapture.IContentCaptureService");
            onDisconnected();
            return true;
          case 1:
            break;
        } 
        iDataShareCallback.enforceInterface("android.service.contentcapture.IContentCaptureService");
        iBinder = iDataShareCallback.readStrongBinder();
        param1Int1 = iDataShareCallback.readInt();
        boolean bool1 = false;
        if (param1Int1 != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        if (iDataShareCallback.readInt() != 0)
          bool1 = true; 
        onConnected(iBinder, bool2, bool1);
        return true;
      } 
      iBinder.writeString("android.service.contentcapture.IContentCaptureService");
      return true;
    }
    
    private static class Proxy implements IContentCaptureService {
      public static IContentCaptureService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.contentcapture.IContentCaptureService";
      }
      
      public void onConnected(IBinder param2IBinder, boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.contentcapture.IContentCaptureService");
          parcel.writeStrongBinder(param2IBinder);
          boolean bool1 = false;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel.writeInt(bool2);
          boolean bool2 = bool1;
          if (param2Boolean2)
            bool2 = true; 
          parcel.writeInt(bool2);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IContentCaptureService.Stub.getDefaultImpl() != null) {
            IContentCaptureService.Stub.getDefaultImpl().onConnected(param2IBinder, param2Boolean1, param2Boolean2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDisconnected() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.contentcapture.IContentCaptureService");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IContentCaptureService.Stub.getDefaultImpl() != null) {
            IContentCaptureService.Stub.getDefaultImpl().onDisconnected();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSessionStarted(ContentCaptureContext param2ContentCaptureContext, int param2Int1, int param2Int2, IResultReceiver param2IResultReceiver, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.contentcapture.IContentCaptureService");
          if (param2ContentCaptureContext != null) {
            parcel.writeInt(1);
            param2ContentCaptureContext.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IContentCaptureService.Stub.getDefaultImpl() != null) {
            IContentCaptureService.Stub.getDefaultImpl().onSessionStarted(param2ContentCaptureContext, param2Int1, param2Int2, param2IResultReceiver, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSessionFinished(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.contentcapture.IContentCaptureService");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IContentCaptureService.Stub.getDefaultImpl() != null) {
            IContentCaptureService.Stub.getDefaultImpl().onSessionFinished(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onActivitySnapshot(int param2Int, SnapshotData param2SnapshotData) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.contentcapture.IContentCaptureService");
          parcel.writeInt(param2Int);
          if (param2SnapshotData != null) {
            parcel.writeInt(1);
            param2SnapshotData.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IContentCaptureService.Stub.getDefaultImpl() != null) {
            IContentCaptureService.Stub.getDefaultImpl().onActivitySnapshot(param2Int, param2SnapshotData);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDataRemovalRequest(DataRemovalRequest param2DataRemovalRequest) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.contentcapture.IContentCaptureService");
          if (param2DataRemovalRequest != null) {
            parcel.writeInt(1);
            param2DataRemovalRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IContentCaptureService.Stub.getDefaultImpl() != null) {
            IContentCaptureService.Stub.getDefaultImpl().onDataRemovalRequest(param2DataRemovalRequest);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDataShared(DataShareRequest param2DataShareRequest, IDataShareCallback param2IDataShareCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.contentcapture.IContentCaptureService");
          if (param2DataShareRequest != null) {
            parcel.writeInt(1);
            param2DataShareRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IDataShareCallback != null) {
            iBinder = param2IDataShareCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && IContentCaptureService.Stub.getDefaultImpl() != null) {
            IContentCaptureService.Stub.getDefaultImpl().onDataShared(param2DataShareRequest, param2IDataShareCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onActivityEvent(ActivityEvent param2ActivityEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.contentcapture.IContentCaptureService");
          if (param2ActivityEvent != null) {
            parcel.writeInt(1);
            param2ActivityEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && IContentCaptureService.Stub.getDefaultImpl() != null) {
            IContentCaptureService.Stub.getDefaultImpl().onActivityEvent(param2ActivityEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IContentCaptureService param1IContentCaptureService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IContentCaptureService != null) {
          Proxy.sDefaultImpl = param1IContentCaptureService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IContentCaptureService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
