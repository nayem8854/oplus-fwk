package android.service.contentcapture;

import android.annotation.SystemApi;
import java.util.concurrent.Executor;

@SystemApi
public interface DataShareCallback {
  void onAccept(Executor paramExecutor, DataShareReadAdapter paramDataShareReadAdapter);
  
  void onReject();
}
