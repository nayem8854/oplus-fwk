package android.service.contentcapture;

import android.annotation.SystemApi;
import android.app.assist.AssistContent;
import android.app.assist.AssistStructure;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class SnapshotData implements Parcelable {
  public SnapshotData(Bundle paramBundle, AssistStructure paramAssistStructure, AssistContent paramAssistContent) {
    this.mAssistData = paramBundle;
    this.mAssistStructure = paramAssistStructure;
    this.mAssistContent = paramAssistContent;
  }
  
  SnapshotData(Parcel paramParcel) {
    this.mAssistData = paramParcel.readBundle();
    this.mAssistStructure = paramParcel.<AssistStructure>readParcelable(null);
    this.mAssistContent = paramParcel.<AssistContent>readParcelable(null);
  }
  
  public Bundle getAssistData() {
    return this.mAssistData;
  }
  
  public AssistStructure getAssistStructure() {
    return this.mAssistStructure;
  }
  
  public AssistContent getAssistContent() {
    return this.mAssistContent;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeBundle(this.mAssistData);
    paramParcel.writeParcelable((Parcelable)this.mAssistStructure, paramInt);
    paramParcel.writeParcelable((Parcelable)this.mAssistContent, paramInt);
  }
  
  public static final Parcelable.Creator<SnapshotData> CREATOR = new Parcelable.Creator<SnapshotData>() {
      public SnapshotData createFromParcel(Parcel param1Parcel) {
        return new SnapshotData(param1Parcel);
      }
      
      public SnapshotData[] newArray(int param1Int) {
        return new SnapshotData[param1Int];
      }
    };
  
  private final AssistContent mAssistContent;
  
  private final Bundle mAssistData;
  
  private final AssistStructure mAssistStructure;
}
