package android.service.contentcapture;

import android.content.ComponentName;
import android.content.ContentCaptureOptions;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.contentcapture.ContentCaptureCondition;
import java.util.ArrayList;
import java.util.List;

public interface IContentCaptureServiceCallback extends IInterface {
  void disableSelf() throws RemoteException;
  
  void setContentCaptureConditions(String paramString, List<ContentCaptureCondition> paramList) throws RemoteException;
  
  void setContentCaptureWhitelist(List<String> paramList, List<ComponentName> paramList1) throws RemoteException;
  
  void writeSessionFlush(int paramInt1, ComponentName paramComponentName, FlushMetrics paramFlushMetrics, ContentCaptureOptions paramContentCaptureOptions, int paramInt2) throws RemoteException;
  
  class Default implements IContentCaptureServiceCallback {
    public void setContentCaptureWhitelist(List<String> param1List, List<ComponentName> param1List1) throws RemoteException {}
    
    public void setContentCaptureConditions(String param1String, List<ContentCaptureCondition> param1List) throws RemoteException {}
    
    public void disableSelf() throws RemoteException {}
    
    public void writeSessionFlush(int param1Int1, ComponentName param1ComponentName, FlushMetrics param1FlushMetrics, ContentCaptureOptions param1ContentCaptureOptions, int param1Int2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IContentCaptureServiceCallback {
    private static final String DESCRIPTOR = "android.service.contentcapture.IContentCaptureServiceCallback";
    
    static final int TRANSACTION_disableSelf = 3;
    
    static final int TRANSACTION_setContentCaptureConditions = 2;
    
    static final int TRANSACTION_setContentCaptureWhitelist = 1;
    
    static final int TRANSACTION_writeSessionFlush = 4;
    
    public Stub() {
      attachInterface(this, "android.service.contentcapture.IContentCaptureServiceCallback");
    }
    
    public static IContentCaptureServiceCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.contentcapture.IContentCaptureServiceCallback");
      if (iInterface != null && iInterface instanceof IContentCaptureServiceCallback)
        return (IContentCaptureServiceCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "writeSessionFlush";
          } 
          return "disableSelf";
        } 
        return "setContentCaptureConditions";
      } 
      return "setContentCaptureWhitelist";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            FlushMetrics flushMetrics;
            ContentCaptureOptions contentCaptureOptions;
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.service.contentcapture.IContentCaptureServiceCallback");
              return true;
            } 
            param1Parcel1.enforceInterface("android.service.contentcapture.IContentCaptureServiceCallback");
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              flushMetrics = FlushMetrics.CREATOR.createFromParcel(param1Parcel1);
            } else {
              flushMetrics = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              contentCaptureOptions = ContentCaptureOptions.CREATOR.createFromParcel(param1Parcel1);
            } else {
              contentCaptureOptions = null;
            } 
            param1Int2 = param1Parcel1.readInt();
            writeSessionFlush(param1Int1, (ComponentName)param1Parcel2, flushMetrics, contentCaptureOptions, param1Int2);
            return true;
          } 
          param1Parcel1.enforceInterface("android.service.contentcapture.IContentCaptureServiceCallback");
          disableSelf();
          return true;
        } 
        param1Parcel1.enforceInterface("android.service.contentcapture.IContentCaptureServiceCallback");
        String str = param1Parcel1.readString();
        arrayList = param1Parcel1.createTypedArrayList(ContentCaptureCondition.CREATOR);
        setContentCaptureConditions(str, (List)arrayList);
        return true;
      } 
      arrayList.enforceInterface("android.service.contentcapture.IContentCaptureServiceCallback");
      ArrayList<String> arrayList1 = arrayList.createStringArrayList();
      ArrayList<?> arrayList = arrayList.createTypedArrayList(ComponentName.CREATOR);
      setContentCaptureWhitelist(arrayList1, (List)arrayList);
      return true;
    }
    
    private static class Proxy implements IContentCaptureServiceCallback {
      public static IContentCaptureServiceCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.contentcapture.IContentCaptureServiceCallback";
      }
      
      public void setContentCaptureWhitelist(List<String> param2List, List<ComponentName> param2List1) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.contentcapture.IContentCaptureServiceCallback");
          parcel.writeStringList(param2List);
          parcel.writeTypedList(param2List1);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IContentCaptureServiceCallback.Stub.getDefaultImpl() != null) {
            IContentCaptureServiceCallback.Stub.getDefaultImpl().setContentCaptureWhitelist(param2List, param2List1);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setContentCaptureConditions(String param2String, List<ContentCaptureCondition> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.contentcapture.IContentCaptureServiceCallback");
          parcel.writeString(param2String);
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IContentCaptureServiceCallback.Stub.getDefaultImpl() != null) {
            IContentCaptureServiceCallback.Stub.getDefaultImpl().setContentCaptureConditions(param2String, param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void disableSelf() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.contentcapture.IContentCaptureServiceCallback");
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IContentCaptureServiceCallback.Stub.getDefaultImpl() != null) {
            IContentCaptureServiceCallback.Stub.getDefaultImpl().disableSelf();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void writeSessionFlush(int param2Int1, ComponentName param2ComponentName, FlushMetrics param2FlushMetrics, ContentCaptureOptions param2ContentCaptureOptions, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.contentcapture.IContentCaptureServiceCallback");
          parcel.writeInt(param2Int1);
          if (param2ComponentName != null) {
            parcel.writeInt(1);
            param2ComponentName.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2FlushMetrics != null) {
            parcel.writeInt(1);
            param2FlushMetrics.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ContentCaptureOptions != null) {
            parcel.writeInt(1);
            param2ContentCaptureOptions.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IContentCaptureServiceCallback.Stub.getDefaultImpl() != null) {
            IContentCaptureServiceCallback.Stub.getDefaultImpl().writeSessionFlush(param2Int1, param2ComponentName, param2FlushMetrics, param2ContentCaptureOptions, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IContentCaptureServiceCallback param1IContentCaptureServiceCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IContentCaptureServiceCallback != null) {
          Proxy.sDefaultImpl = param1IContentCaptureServiceCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IContentCaptureServiceCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
