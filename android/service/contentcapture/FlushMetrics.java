package android.service.contentcapture;

import android.os.Parcel;
import android.os.Parcelable;

public final class FlushMetrics implements Parcelable {
  public void reset() {
    this.viewAppearedCount = 0;
    this.viewDisappearedCount = 0;
    this.viewTextChangedCount = 0;
    this.sessionStarted = 0;
    this.sessionFinished = 0;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.sessionStarted);
    paramParcel.writeInt(this.sessionFinished);
    paramParcel.writeInt(this.viewAppearedCount);
    paramParcel.writeInt(this.viewDisappearedCount);
    paramParcel.writeInt(this.viewTextChangedCount);
  }
  
  public static final Parcelable.Creator<FlushMetrics> CREATOR = new Parcelable.Creator<FlushMetrics>() {
      public FlushMetrics createFromParcel(Parcel param1Parcel) {
        FlushMetrics flushMetrics = new FlushMetrics();
        flushMetrics.sessionStarted = param1Parcel.readInt();
        flushMetrics.sessionFinished = param1Parcel.readInt();
        flushMetrics.viewAppearedCount = param1Parcel.readInt();
        flushMetrics.viewDisappearedCount = param1Parcel.readInt();
        flushMetrics.viewTextChangedCount = param1Parcel.readInt();
        return flushMetrics;
      }
      
      public FlushMetrics[] newArray(int param1Int) {
        return new FlushMetrics[param1Int];
      }
    };
  
  public int sessionFinished;
  
  public int sessionStarted;
  
  public int viewAppearedCount;
  
  public int viewDisappearedCount;
  
  public int viewTextChangedCount;
}
