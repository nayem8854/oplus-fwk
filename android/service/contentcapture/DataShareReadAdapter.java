package android.service.contentcapture;

import android.annotation.SystemApi;
import android.os.ParcelFileDescriptor;

@SystemApi
public interface DataShareReadAdapter {
  void onError(int paramInt);
  
  void onStart(ParcelFileDescriptor paramParcelFileDescriptor);
}
