package android.service.contentcapture;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IDataShareCallback extends IInterface {
  void accept(IDataShareReadAdapter paramIDataShareReadAdapter) throws RemoteException;
  
  void reject() throws RemoteException;
  
  class Default implements IDataShareCallback {
    public void accept(IDataShareReadAdapter param1IDataShareReadAdapter) throws RemoteException {}
    
    public void reject() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDataShareCallback {
    private static final String DESCRIPTOR = "android.service.contentcapture.IDataShareCallback";
    
    static final int TRANSACTION_accept = 1;
    
    static final int TRANSACTION_reject = 2;
    
    public Stub() {
      attachInterface(this, "android.service.contentcapture.IDataShareCallback");
    }
    
    public static IDataShareCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.contentcapture.IDataShareCallback");
      if (iInterface != null && iInterface instanceof IDataShareCallback)
        return (IDataShareCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "reject";
      } 
      return "accept";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.service.contentcapture.IDataShareCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("android.service.contentcapture.IDataShareCallback");
        reject();
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.contentcapture.IDataShareCallback");
      IDataShareReadAdapter iDataShareReadAdapter = IDataShareReadAdapter.Stub.asInterface(param1Parcel1.readStrongBinder());
      accept(iDataShareReadAdapter);
      return true;
    }
    
    private static class Proxy implements IDataShareCallback {
      public static IDataShareCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.contentcapture.IDataShareCallback";
      }
      
      public void accept(IDataShareReadAdapter param2IDataShareReadAdapter) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.contentcapture.IDataShareCallback");
          if (param2IDataShareReadAdapter != null) {
            iBinder = param2IDataShareReadAdapter.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IDataShareCallback.Stub.getDefaultImpl() != null) {
            IDataShareCallback.Stub.getDefaultImpl().accept(param2IDataShareReadAdapter);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void reject() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.contentcapture.IDataShareCallback");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IDataShareCallback.Stub.getDefaultImpl() != null) {
            IDataShareCallback.Stub.getDefaultImpl().reject();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDataShareCallback param1IDataShareCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDataShareCallback != null) {
          Proxy.sDefaultImpl = param1IDataShareCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDataShareCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
