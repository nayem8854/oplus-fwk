package android.service.contentcapture;

import android.annotation.SystemApi;
import android.content.ComponentName;
import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public final class ActivityEvent implements Parcelable {
  public ActivityEvent(ComponentName paramComponentName, int paramInt) {
    this.mComponentName = paramComponentName;
    this.mType = paramInt;
  }
  
  public ComponentName getComponentName() {
    return this.mComponentName;
  }
  
  public int getEventType() {
    return this.mType;
  }
  
  public static String getTypeAsString(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 23) {
          if (paramInt != 24) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("UKNOWN_TYPE: ");
            stringBuilder.append(paramInt);
            return stringBuilder.toString();
          } 
          return "ACTIVITY_DESTROYED";
        } 
        return "ACTIVITY_STOPPED";
      } 
      return "ACTIVITY_PAUSED";
    } 
    return "ACTIVITY_RESUMED";
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("ActivityEvent[");
    stringBuilder.append(this.mComponentName.toShortString());
    stringBuilder.append("]:");
    stringBuilder.append(getTypeAsString(this.mType));
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.mComponentName, paramInt);
    paramParcel.writeInt(this.mType);
  }
  
  public static final Parcelable.Creator<ActivityEvent> CREATOR = new Parcelable.Creator<ActivityEvent>() {
      public ActivityEvent createFromParcel(Parcel param1Parcel) {
        ComponentName componentName = param1Parcel.<ComponentName>readParcelable(null);
        int i = param1Parcel.readInt();
        return new ActivityEvent(componentName, i);
      }
      
      public ActivityEvent[] newArray(int param1Int) {
        return new ActivityEvent[param1Int];
      }
    };
  
  public static final int TYPE_ACTIVITY_DESTROYED = 24;
  
  public static final int TYPE_ACTIVITY_PAUSED = 2;
  
  public static final int TYPE_ACTIVITY_RESUMED = 1;
  
  public static final int TYPE_ACTIVITY_STOPPED = 23;
  
  private final ComponentName mComponentName;
  
  private final int mType;
  
  @Retention(RetentionPolicy.SOURCE)
  class ActivityEventType implements Annotation {}
}
