package android.service.contentcapture;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

public interface IDataShareReadAdapter extends IInterface {
  void error(int paramInt) throws RemoteException;
  
  void finish() throws RemoteException;
  
  void start(ParcelFileDescriptor paramParcelFileDescriptor) throws RemoteException;
  
  class Default implements IDataShareReadAdapter {
    public void start(ParcelFileDescriptor param1ParcelFileDescriptor) throws RemoteException {}
    
    public void error(int param1Int) throws RemoteException {}
    
    public void finish() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDataShareReadAdapter {
    private static final String DESCRIPTOR = "android.service.contentcapture.IDataShareReadAdapter";
    
    static final int TRANSACTION_error = 2;
    
    static final int TRANSACTION_finish = 3;
    
    static final int TRANSACTION_start = 1;
    
    public Stub() {
      attachInterface(this, "android.service.contentcapture.IDataShareReadAdapter");
    }
    
    public static IDataShareReadAdapter asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.contentcapture.IDataShareReadAdapter");
      if (iInterface != null && iInterface instanceof IDataShareReadAdapter)
        return (IDataShareReadAdapter)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "finish";
        } 
        return "error";
      } 
      return "start";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.service.contentcapture.IDataShareReadAdapter");
            return true;
          } 
          param1Parcel1.enforceInterface("android.service.contentcapture.IDataShareReadAdapter");
          finish();
          return true;
        } 
        param1Parcel1.enforceInterface("android.service.contentcapture.IDataShareReadAdapter");
        param1Int1 = param1Parcel1.readInt();
        error(param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.contentcapture.IDataShareReadAdapter");
      if (param1Parcel1.readInt() != 0) {
        ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      start((ParcelFileDescriptor)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IDataShareReadAdapter {
      public static IDataShareReadAdapter sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.contentcapture.IDataShareReadAdapter";
      }
      
      public void start(ParcelFileDescriptor param2ParcelFileDescriptor) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.contentcapture.IDataShareReadAdapter");
          if (param2ParcelFileDescriptor != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IDataShareReadAdapter.Stub.getDefaultImpl() != null) {
            IDataShareReadAdapter.Stub.getDefaultImpl().start(param2ParcelFileDescriptor);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void error(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.contentcapture.IDataShareReadAdapter");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IDataShareReadAdapter.Stub.getDefaultImpl() != null) {
            IDataShareReadAdapter.Stub.getDefaultImpl().error(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void finish() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.contentcapture.IDataShareReadAdapter");
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IDataShareReadAdapter.Stub.getDefaultImpl() != null) {
            IDataShareReadAdapter.Stub.getDefaultImpl().finish();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDataShareReadAdapter param1IDataShareReadAdapter) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDataShareReadAdapter != null) {
          Proxy.sDefaultImpl = param1IDataShareReadAdapter;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDataShareReadAdapter getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
