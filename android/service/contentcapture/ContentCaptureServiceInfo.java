package android.service.contentcapture;

import android.app.AppGlobals;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Slog;
import android.util.Xml;
import com.android.internal.R;
import java.io.PrintWriter;
import org.xmlpull.v1.XmlPullParser;

public final class ContentCaptureServiceInfo {
  private static final String TAG = ContentCaptureServiceInfo.class.getSimpleName();
  
  private static final String XML_TAG_SERVICE = "content-capture-service";
  
  private final ServiceInfo mServiceInfo;
  
  private final String mSettingsActivity;
  
  private static ServiceInfo getServiceInfoOrThrow(ComponentName paramComponentName, boolean paramBoolean, int paramInt) throws PackageManager.NameNotFoundException {
    String str;
    int i = 128;
    if (!paramBoolean)
      i = 0x80 | 0x100000; 
    ServiceInfo serviceInfo = null;
    try {
      ServiceInfo serviceInfo1 = AppGlobals.getPackageManager().getServiceInfo(paramComponentName, i, paramInt);
    } catch (RemoteException remoteException) {}
    if (serviceInfo == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Could not get serviceInfo for ");
      if (paramBoolean) {
        str = " (temp)";
      } else {
        str = "(default system)";
      } 
      stringBuilder.append(str);
      stringBuilder.append(" ");
      stringBuilder.append(paramComponentName.flattenToShortString());
      throw new PackageManager.NameNotFoundException(stringBuilder.toString());
    } 
    return (ServiceInfo)str;
  }
  
  public ContentCaptureServiceInfo(Context paramContext, ComponentName paramComponentName, boolean paramBoolean, int paramInt) throws PackageManager.NameNotFoundException {
    this(paramContext, getServiceInfoOrThrow(paramComponentName, paramBoolean, paramInt));
  }
  
  private ContentCaptureServiceInfo(Context paramContext, ServiceInfo paramServiceInfo) {
    if ("android.permission.BIND_CONTENT_CAPTURE_SERVICE".equals(paramServiceInfo.permission)) {
      String str1;
      this.mServiceInfo = paramServiceInfo;
      XmlResourceParser xmlResourceParser = paramServiceInfo.loadXmlMetaData(paramContext.getPackageManager(), "android.content_capture");
      if (xmlResourceParser == null) {
        this.mSettingsActivity = null;
        return;
      } 
      String str2 = null;
      AttributeSet attributeSet = null;
      String str3 = str2;
      try {
        Resources resources = paramContext.getPackageManager().getResourcesForApplication(paramServiceInfo.applicationInfo);
        int i = 0;
        while (i != 1 && i != 2) {
          str3 = str2;
          i = xmlResourceParser.next();
        } 
        str3 = str2;
        if ("content-capture-service".equals(xmlResourceParser.getName())) {
          str3 = str2;
          attributeSet = Xml.asAttributeSet((XmlPullParser)xmlResourceParser);
          paramContext = null;
          try {
            TypedArray typedArray2 = resources.obtainAttributes(attributeSet, R.styleable.ContentCaptureService);
            TypedArray typedArray1 = typedArray2;
          } finally {
            if (str1 != null) {
              str3 = str2;
              str1.recycle();
            } 
            str3 = str2;
          } 
        } else {
          str3 = str2;
          Log.e(TAG, "Meta-data does not start with content-capture-service tag");
          AttributeSet attributeSet1 = attributeSet;
        } 
      } catch (android.content.pm.PackageManager.NameNotFoundException|java.io.IOException|org.xmlpull.v1.XmlPullParserException nameNotFoundException) {
        Log.e(TAG, "Error parsing auto fill service meta-data", (Throwable)nameNotFoundException);
        str1 = str3;
      } 
      this.mSettingsActivity = str1;
      return;
    } 
    String str = TAG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ContentCaptureService from '");
    stringBuilder.append(paramServiceInfo.packageName);
    stringBuilder.append("' does not require permission ");
    stringBuilder.append("android.permission.BIND_CONTENT_CAPTURE_SERVICE");
    Slog.w(str, stringBuilder.toString());
    throw new SecurityException("Service does not require permission android.permission.BIND_CONTENT_CAPTURE_SERVICE");
  }
  
  public ServiceInfo getServiceInfo() {
    return this.mServiceInfo;
  }
  
  public String getSettingsActivity() {
    return this.mSettingsActivity;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(getClass().getSimpleName());
    stringBuilder.append("[");
    stringBuilder.append(this.mServiceInfo);
    stringBuilder.append(", settings:");
    stringBuilder.append(this.mSettingsActivity);
    return stringBuilder.toString();
  }
  
  public void dump(String paramString, PrintWriter paramPrintWriter) {
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("Component: ");
    paramPrintWriter.println(getServiceInfo().getComponentName());
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("Settings: ");
    paramPrintWriter.println(this.mSettingsActivity);
  }
}
