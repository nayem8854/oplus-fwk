package android.service.contentcapture;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.ComponentName;
import android.content.ContentCaptureOptions;
import android.content.Intent;
import android.content.pm.ParceledListSlice;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.util.Log;
import android.util.Slog;
import android.util.SparseIntArray;
import android.view.contentcapture.ContentCaptureCondition;
import android.view.contentcapture.ContentCaptureContext;
import android.view.contentcapture.ContentCaptureEvent;
import android.view.contentcapture.ContentCaptureHelper;
import android.view.contentcapture.ContentCaptureSessionId;
import android.view.contentcapture.DataRemovalRequest;
import android.view.contentcapture.DataShareRequest;
import android.view.contentcapture.IContentCaptureDirectManager;
import com.android.internal.os.IResultReceiver;
import com.android.internal.util.FrameworkStatsLog;
import com.android.internal.util.Preconditions;
import com.android.internal.util.function.HexConsumer;
import com.android.internal.util.function.QuintConsumer;
import com.android.internal.util.function.TriConsumer;
import com.android.internal.util.function.pooled.PooledLambda;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

@SystemApi
public abstract class ContentCaptureService extends Service {
  private static final String TAG = ContentCaptureService.class.getSimpleName();
  
  private final LocalDataShareAdapterResourceManager mDataShareAdapterResourceManager = new LocalDataShareAdapterResourceManager();
  
  private long mCallerMismatchTimeout = 1000L;
  
  private final IContentCaptureService mServerInterface = new IContentCaptureService.Stub() {
      final ContentCaptureService this$0;
      
      public void onConnected(IBinder param1IBinder, boolean param1Boolean1, boolean param1Boolean2) {
        ContentCaptureHelper.sVerbose = param1Boolean1;
        ContentCaptureHelper.sDebug = param1Boolean2;
        ContentCaptureService.this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$ContentCaptureService$1$iP7RXM_Va9lafd6bT9eXRx_D47Q.INSTANCE, ContentCaptureService.this, param1IBinder));
      }
      
      public void onDisconnected() {
        ContentCaptureService.this.mHandler.sendMessage(PooledLambda.obtainMessage((Consumer)_$$Lambda$ContentCaptureService$1$wPMOb7AM5r_kHmuyl3SBSylaH1A.INSTANCE, ContentCaptureService.this));
      }
      
      public void onSessionStarted(ContentCaptureContext param1ContentCaptureContext, int param1Int1, int param1Int2, IResultReceiver param1IResultReceiver, int param1Int3) {
        Handler handler = ContentCaptureService.this.mHandler;
        -$.Lambda.ContentCaptureService.null.PaMsQkJwdUJ1lCgOOaLG9Bm09t8 paMsQkJwdUJ1lCgOOaLG9Bm09t8 = _$$Lambda$ContentCaptureService$1$PaMsQkJwdUJ1lCgOOaLG9Bm09t8.INSTANCE;
        ContentCaptureService contentCaptureService = ContentCaptureService.this;
        handler.sendMessage(PooledLambda.obtainMessage((HexConsumer)paMsQkJwdUJ1lCgOOaLG9Bm09t8, contentCaptureService, param1ContentCaptureContext, Integer.valueOf(param1Int1), Integer.valueOf(param1Int2), param1IResultReceiver, Integer.valueOf(param1Int3)));
      }
      
      public void onActivitySnapshot(int param1Int, SnapshotData param1SnapshotData) {
        Handler handler = ContentCaptureService.this.mHandler;
        -$.Lambda.ContentCaptureService.null.NhSHlL57JqxWNJ8QcsuGxEhxv1Y nhSHlL57JqxWNJ8QcsuGxEhxv1Y = _$$Lambda$ContentCaptureService$1$NhSHlL57JqxWNJ8QcsuGxEhxv1Y.INSTANCE;
        ContentCaptureService contentCaptureService = ContentCaptureService.this;
        Message message = PooledLambda.obtainMessage((TriConsumer)nhSHlL57JqxWNJ8QcsuGxEhxv1Y, contentCaptureService, Integer.valueOf(param1Int), param1SnapshotData);
        handler.sendMessage(message);
      }
      
      public void onSessionFinished(int param1Int) {
        Handler handler = ContentCaptureService.this.mHandler;
        -$.Lambda.ContentCaptureService.null.jkZQ77YuBlPDClOdklQb8tj8Kpw jkZQ77YuBlPDClOdklQb8tj8Kpw = _$$Lambda$ContentCaptureService$1$jkZQ77YuBlPDClOdklQb8tj8Kpw.INSTANCE;
        ContentCaptureService contentCaptureService = ContentCaptureService.this;
        handler.sendMessage(PooledLambda.obtainMessage((BiConsumer)jkZQ77YuBlPDClOdklQb8tj8Kpw, contentCaptureService, Integer.valueOf(param1Int)));
      }
      
      public void onDataRemovalRequest(DataRemovalRequest param1DataRemovalRequest) {
        ContentCaptureService.this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$ContentCaptureService$1$sJuAS4AaQcXaSFkQpSEmVLBqyvw.INSTANCE, ContentCaptureService.this, param1DataRemovalRequest));
      }
      
      public void onDataShared(DataShareRequest param1DataShareRequest, IDataShareCallback param1IDataShareCallback) {
        ContentCaptureService.this.mHandler.sendMessage(PooledLambda.obtainMessage((TriConsumer)_$$Lambda$ContentCaptureService$1$zKz_M9eA5YGpi6vfMJkxAoy9wLQ.INSTANCE, ContentCaptureService.this, param1DataShareRequest, param1IDataShareCallback));
      }
      
      public void onActivityEvent(ActivityEvent param1ActivityEvent) {
        ContentCaptureService.this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$ContentCaptureService$1$w_tuWwiP1K_0_aWpap_nR9WJ3UQ.INSTANCE, ContentCaptureService.this, param1ActivityEvent));
      }
    };
  
  private final IContentCaptureDirectManager mClientInterface = (IContentCaptureDirectManager)new IContentCaptureDirectManager.Stub() {
      final ContentCaptureService this$0;
      
      public void sendEvents(ParceledListSlice param1ParceledListSlice, int param1Int, ContentCaptureOptions param1ContentCaptureOptions) {
        Handler handler = ContentCaptureService.this.mHandler;
        -$.Lambda.ContentCaptureService.null.nqaNcni5MOtmyGkMJfxu_qUHOk4 nqaNcni5MOtmyGkMJfxu_qUHOk4 = _$$Lambda$ContentCaptureService$2$nqaNcni5MOtmyGkMJfxu_qUHOk4.INSTANCE;
        ContentCaptureService contentCaptureService = ContentCaptureService.this;
        int i = Binder.getCallingUid();
        handler.sendMessage(PooledLambda.obtainMessage((QuintConsumer)nqaNcni5MOtmyGkMJfxu_qUHOk4, contentCaptureService, Integer.valueOf(i), param1ParceledListSlice, Integer.valueOf(param1Int), param1ContentCaptureOptions));
      }
    };
  
  private final SparseIntArray mSessionUids = new SparseIntArray();
  
  public static final String SERVICE_INTERFACE = "android.service.contentcapture.ContentCaptureService";
  
  public static final String SERVICE_META_DATA = "android.content_capture";
  
  private IContentCaptureServiceCallback mCallback;
  
  private Handler mHandler;
  
  private long mLastCallerMismatchLog;
  
  public void onCreate() {
    super.onCreate();
    this.mHandler = new Handler(Looper.getMainLooper(), null, true);
  }
  
  public final IBinder onBind(Intent paramIntent) {
    if ("android.service.contentcapture.ContentCaptureService".equals(paramIntent.getAction()))
      return this.mServerInterface.asBinder(); 
    String str = TAG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Tried to bind to wrong intent (should be android.service.contentcapture.ContentCaptureService: ");
    stringBuilder.append(paramIntent);
    Log.w(str, stringBuilder.toString());
    return null;
  }
  
  public final void setContentCaptureWhitelist(Set<String> paramSet, Set<ComponentName> paramSet1) {
    IContentCaptureServiceCallback iContentCaptureServiceCallback = this.mCallback;
    if (iContentCaptureServiceCallback == null) {
      Log.w(TAG, "setContentCaptureWhitelist(): no server callback");
      return;
    } 
    try {
      iContentCaptureServiceCallback.setContentCaptureWhitelist(ContentCaptureHelper.toList(paramSet), ContentCaptureHelper.toList(paramSet1));
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public final void setContentCaptureConditions(String paramString, Set<ContentCaptureCondition> paramSet) {
    IContentCaptureServiceCallback iContentCaptureServiceCallback = this.mCallback;
    if (iContentCaptureServiceCallback == null) {
      Log.w(TAG, "setContentCaptureConditions(): no server callback");
      return;
    } 
    try {
      iContentCaptureServiceCallback.setContentCaptureConditions(paramString, ContentCaptureHelper.toList(paramSet));
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void onConnected() {
    String str = TAG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("bound to ");
    stringBuilder.append(getClass().getName());
    Slog.i(str, stringBuilder.toString());
  }
  
  public void onCreateContentCaptureSession(ContentCaptureContext paramContentCaptureContext, ContentCaptureSessionId paramContentCaptureSessionId) {
    if (ContentCaptureHelper.sVerbose) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onCreateContentCaptureSession(id=");
      stringBuilder.append(paramContentCaptureSessionId);
      stringBuilder.append(", ctx=");
      stringBuilder.append(paramContentCaptureContext);
      stringBuilder.append(")");
      Log.v(str, stringBuilder.toString());
    } 
  }
  
  public void onContentCaptureEvent(ContentCaptureSessionId paramContentCaptureSessionId, ContentCaptureEvent paramContentCaptureEvent) {
    if (ContentCaptureHelper.sVerbose) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onContentCaptureEventsRequest(id=");
      stringBuilder.append(paramContentCaptureSessionId);
      stringBuilder.append(")");
      Log.v(str, stringBuilder.toString());
    } 
  }
  
  public void onDataRemovalRequest(DataRemovalRequest paramDataRemovalRequest) {
    if (ContentCaptureHelper.sVerbose)
      Log.v(TAG, "onDataRemovalRequest()"); 
  }
  
  @SystemApi
  public void onDataShareRequest(DataShareRequest paramDataShareRequest, DataShareCallback paramDataShareCallback) {
    if (ContentCaptureHelper.sVerbose)
      Log.v(TAG, "onDataShareRequest()"); 
  }
  
  public void onActivitySnapshot(ContentCaptureSessionId paramContentCaptureSessionId, SnapshotData paramSnapshotData) {
    if (ContentCaptureHelper.sVerbose) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onActivitySnapshot(id=");
      stringBuilder.append(paramContentCaptureSessionId);
      stringBuilder.append(")");
      Log.v(str, stringBuilder.toString());
    } 
  }
  
  public void onActivityEvent(ActivityEvent paramActivityEvent) {
    if (ContentCaptureHelper.sVerbose) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onActivityEvent(): ");
      stringBuilder.append(paramActivityEvent);
      Log.v(str, stringBuilder.toString());
    } 
  }
  
  public void onDestroyContentCaptureSession(ContentCaptureSessionId paramContentCaptureSessionId) {
    if (ContentCaptureHelper.sVerbose) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onDestroyContentCaptureSession(id=");
      stringBuilder.append(paramContentCaptureSessionId);
      stringBuilder.append(")");
      Log.v(str, stringBuilder.toString());
    } 
  }
  
  public final void disableSelf() {
    if (ContentCaptureHelper.sDebug)
      Log.d(TAG, "disableSelf()"); 
    IContentCaptureServiceCallback iContentCaptureServiceCallback = this.mCallback;
    if (iContentCaptureServiceCallback == null) {
      Log.w(TAG, "disableSelf(): no server callback");
      return;
    } 
    try {
      iContentCaptureServiceCallback.disableSelf();
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void onDisconnected() {
    String str = TAG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("unbinding from ");
    stringBuilder.append(getClass().getName());
    Slog.i(str, stringBuilder.toString());
  }
  
  protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    paramPrintWriter.print("Debug: ");
    paramPrintWriter.print(ContentCaptureHelper.sDebug);
    paramPrintWriter.print(" Verbose: ");
    paramPrintWriter.println(ContentCaptureHelper.sVerbose);
    int i = this.mSessionUids.size();
    paramPrintWriter.print("Number sessions: ");
    paramPrintWriter.println(i);
    if (i > 0)
      for (byte b = 0; b < i; b++) {
        paramPrintWriter.print("  ");
        paramPrintWriter.print(this.mSessionUids.keyAt(b));
        paramPrintWriter.print(": uid=");
        paramPrintWriter.println(this.mSessionUids.valueAt(b));
      }  
  }
  
  private void handleOnConnected(IBinder paramIBinder) {
    this.mCallback = IContentCaptureServiceCallback.Stub.asInterface(paramIBinder);
    onConnected();
  }
  
  private void handleOnDisconnected() {
    onDisconnected();
    this.mCallback = null;
  }
  
  private void handleOnCreateSession(ContentCaptureContext paramContentCaptureContext, int paramInt1, int paramInt2, IResultReceiver paramIResultReceiver, int paramInt3) {
    this.mSessionUids.put(paramInt1, paramInt2);
    onCreateContentCaptureSession(paramContentCaptureContext, new ContentCaptureSessionId(paramInt1));
    int i = paramContentCaptureContext.getFlags();
    paramInt1 = 0;
    if ((i & 0x2) != 0)
      paramInt1 = 0x0 | 0x20; 
    paramInt2 = paramInt1;
    if ((i & 0x1) != 0)
      paramInt2 = paramInt1 | 0x40; 
    if (paramInt2 != 0)
      paramInt3 = paramInt2 | 0x4; 
    setClientState(paramIResultReceiver, paramInt3, this.mClientInterface.asBinder());
  }
  
  private void handleSendEvents(int paramInt1, ParceledListSlice<ContentCaptureEvent> paramParceledListSlice, int paramInt2, ContentCaptureOptions paramContentCaptureOptions) {
    ComponentName componentName;
    List<ContentCaptureEvent> list = paramParceledListSlice.getList();
    if (list.isEmpty()) {
      Log.w(TAG, "handleSendEvents() received empty list of events");
      return;
    } 
    FlushMetrics flushMetrics = new FlushMetrics();
    int i;
    ContentCaptureSessionId contentCaptureSessionId;
    byte b;
    for (paramParceledListSlice = null, i = 0, contentCaptureSessionId = null, b = 0; b < list.size(); b++) {
      ContentCaptureEvent contentCaptureEvent = list.get(b);
      if (handleIsRightCallerFor(contentCaptureEvent, paramInt1)) {
        ComponentName componentName1;
        int j = contentCaptureEvent.getSessionId();
        int k = i;
        if (j != i) {
          contentCaptureSessionId = new ContentCaptureSessionId(j);
          if (b != 0) {
            writeFlushMetrics(j, (ComponentName)paramParceledListSlice, flushMetrics, paramContentCaptureOptions, paramInt2);
            flushMetrics.reset();
          } 
          k = j;
        } 
        ContentCaptureContext contentCaptureContext = contentCaptureEvent.getContentCaptureContext();
        ParceledListSlice<ContentCaptureEvent> parceledListSlice = paramParceledListSlice;
        if (paramParceledListSlice == null) {
          parceledListSlice = paramParceledListSlice;
          if (contentCaptureContext != null)
            componentName1 = contentCaptureContext.getActivityComponent(); 
        } 
        i = contentCaptureEvent.getType();
        if (i != -2) {
          if (i != -1) {
            if (i != 1) {
              if (i != 2) {
                if (i != 3) {
                  onContentCaptureEvent(contentCaptureSessionId, contentCaptureEvent);
                  componentName = componentName1;
                  i = k;
                } else {
                  onContentCaptureEvent(contentCaptureSessionId, contentCaptureEvent);
                  flushMetrics.viewTextChangedCount++;
                  componentName = componentName1;
                  i = k;
                } 
              } else {
                onContentCaptureEvent(contentCaptureSessionId, contentCaptureEvent);
                flushMetrics.viewDisappearedCount++;
                componentName = componentName1;
                i = k;
              } 
            } else {
              onContentCaptureEvent(contentCaptureSessionId, contentCaptureEvent);
              flushMetrics.viewAppearedCount++;
              componentName = componentName1;
              i = k;
            } 
          } else {
            contentCaptureContext.setParentSessionId(contentCaptureEvent.getParentSessionId());
            this.mSessionUids.put(j, paramInt1);
            onCreateContentCaptureSession(contentCaptureContext, contentCaptureSessionId);
            flushMetrics.sessionStarted++;
            componentName = componentName1;
            i = k;
          } 
        } else {
          this.mSessionUids.delete(j);
          onDestroyContentCaptureSession(contentCaptureSessionId);
          flushMetrics.sessionFinished++;
          i = k;
          componentName = componentName1;
        } 
      } 
    } 
    writeFlushMetrics(i, componentName, flushMetrics, paramContentCaptureOptions, paramInt2);
  }
  
  private void handleOnActivitySnapshot(int paramInt, SnapshotData paramSnapshotData) {
    onActivitySnapshot(new ContentCaptureSessionId(paramInt), paramSnapshotData);
  }
  
  private void handleFinishSession(int paramInt) {
    this.mSessionUids.delete(paramInt);
    onDestroyContentCaptureSession(new ContentCaptureSessionId(paramInt));
  }
  
  private void handleOnDataRemovalRequest(DataRemovalRequest paramDataRemovalRequest) {
    onDataRemovalRequest(paramDataRemovalRequest);
  }
  
  private void handleOnDataShared(DataShareRequest paramDataShareRequest, IDataShareCallback paramIDataShareCallback) {
    onDataShareRequest(paramDataShareRequest, (DataShareCallback)new Object(this, paramIDataShareCallback));
  }
  
  private void handleOnActivityEvent(ActivityEvent paramActivityEvent) {
    onActivityEvent(paramActivityEvent);
  }
  
  private boolean handleIsRightCallerFor(ContentCaptureEvent paramContentCaptureEvent, int paramInt) {
    int i = paramContentCaptureEvent.getType();
    if (i != -2 && i != -1) {
      i = paramContentCaptureEvent.getSessionId();
    } else {
      i = paramContentCaptureEvent.getParentSessionId();
    } 
    if (this.mSessionUids.indexOfKey(i) < 0) {
      if (ContentCaptureHelper.sVerbose) {
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("handleIsRightCallerFor(");
        stringBuilder.append(paramContentCaptureEvent);
        stringBuilder.append("): no session for ");
        stringBuilder.append(i);
        stringBuilder.append(": ");
        stringBuilder.append(this.mSessionUids);
        Log.v(str, stringBuilder.toString());
      } 
      return false;
    } 
    int j = this.mSessionUids.get(i);
    if (j != paramInt) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("invalid call from UID ");
      stringBuilder.append(paramInt);
      stringBuilder.append(": session ");
      stringBuilder.append(i);
      stringBuilder.append(" belongs to ");
      stringBuilder.append(j);
      Log.e(str, stringBuilder.toString());
      long l = System.currentTimeMillis();
      if (l - this.mLastCallerMismatchLog > this.mCallerMismatchTimeout) {
        str = getPackageManager().getNameForUid(j);
        String str1 = getPackageManager().getNameForUid(paramInt);
        FrameworkStatsLog.write(206, str, str1);
        this.mLastCallerMismatchLog = l;
      } 
      return false;
    } 
    return true;
  }
  
  public static void setClientState(IResultReceiver paramIResultReceiver, int paramInt, IBinder paramIBinder) {
    if (paramIBinder != null) {
      try {
        Bundle bundle2 = new Bundle();
        this();
        bundle2.putBinder("binder", paramIBinder);
        Bundle bundle1 = bundle2;
      } catch (RemoteException remoteException) {}
    } else {
      paramIBinder = null;
    } 
    paramIResultReceiver.send(paramInt, (Bundle)paramIBinder);
  }
  
  private void writeFlushMetrics(int paramInt1, ComponentName paramComponentName, FlushMetrics paramFlushMetrics, ContentCaptureOptions paramContentCaptureOptions, int paramInt2) {
    IContentCaptureServiceCallback iContentCaptureServiceCallback = this.mCallback;
    if (iContentCaptureServiceCallback == null) {
      Log.w(TAG, "writeSessionFlush(): no server callback");
      return;
    } 
    try {
      iContentCaptureServiceCallback.writeSessionFlush(paramInt1, paramComponentName, paramFlushMetrics, paramContentCaptureOptions, paramInt2);
    } catch (RemoteException remoteException) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("failed to write flush metrics: ");
      stringBuilder.append(remoteException);
      Log.e(str, stringBuilder.toString());
    } 
  }
  
  private static class DataShareReadAdapterDelegate extends IDataShareReadAdapter.Stub {
    private final Object mLock = new Object();
    
    private final WeakReference<ContentCaptureService.LocalDataShareAdapterResourceManager> mResourceManagerReference;
    
    DataShareReadAdapterDelegate(Executor param1Executor, DataShareReadAdapter param1DataShareReadAdapter, ContentCaptureService.LocalDataShareAdapterResourceManager param1LocalDataShareAdapterResourceManager) {
      Preconditions.checkNotNull(param1Executor);
      Preconditions.checkNotNull(param1DataShareReadAdapter);
      Preconditions.checkNotNull(param1LocalDataShareAdapterResourceManager);
      param1LocalDataShareAdapterResourceManager.initializeForDelegate(this, param1DataShareReadAdapter, param1Executor);
      this.mResourceManagerReference = new WeakReference<>(param1LocalDataShareAdapterResourceManager);
    }
    
    public void start(ParcelFileDescriptor param1ParcelFileDescriptor) throws RemoteException {
      synchronized (this.mLock) {
        _$$Lambda$ContentCaptureService$DataShareReadAdapterDelegate$uTXwFANtzR3r3Y1Oa0xuBOje1MM _$$Lambda$ContentCaptureService$DataShareReadAdapterDelegate$uTXwFANtzR3r3Y1Oa0xuBOje1MM = new _$$Lambda$ContentCaptureService$DataShareReadAdapterDelegate$uTXwFANtzR3r3Y1Oa0xuBOje1MM();
        this(param1ParcelFileDescriptor);
        executeAdapterMethodLocked(_$$Lambda$ContentCaptureService$DataShareReadAdapterDelegate$uTXwFANtzR3r3Y1Oa0xuBOje1MM, "onStart");
        return;
      } 
    }
    
    public void error(int param1Int) throws RemoteException {
      synchronized (this.mLock) {
        _$$Lambda$ContentCaptureService$DataShareReadAdapterDelegate$Wuxtt_LzmmvHteiH6rnqxFy6Gng _$$Lambda$ContentCaptureService$DataShareReadAdapterDelegate$Wuxtt_LzmmvHteiH6rnqxFy6Gng = new _$$Lambda$ContentCaptureService$DataShareReadAdapterDelegate$Wuxtt_LzmmvHteiH6rnqxFy6Gng();
        this(param1Int);
        executeAdapterMethodLocked(_$$Lambda$ContentCaptureService$DataShareReadAdapterDelegate$Wuxtt_LzmmvHteiH6rnqxFy6Gng, "onError");
        clearHardReferences();
        return;
      } 
    }
    
    public void finish() throws RemoteException {
      synchronized (this.mLock) {
        clearHardReferences();
        return;
      } 
    }
    
    private void executeAdapterMethodLocked(Consumer<DataShareReadAdapter> param1Consumer, String param1String) {
      StringBuilder stringBuilder;
      ContentCaptureService.LocalDataShareAdapterResourceManager localDataShareAdapterResourceManager = this.mResourceManagerReference.get();
      if (localDataShareAdapterResourceManager == null) {
        null = ContentCaptureService.TAG;
        stringBuilder = new StringBuilder();
        stringBuilder.append("Can't execute ");
        stringBuilder.append(param1String);
        stringBuilder.append("(), resource manager has been GC'ed");
        Slog.w(null, stringBuilder.toString());
        return;
      } 
      DataShareReadAdapter dataShareReadAdapter = localDataShareAdapterResourceManager.getAdapter(this);
      Executor executor = localDataShareAdapterResourceManager.getExecutor(this);
      if (dataShareReadAdapter == null || executor == null) {
        null = ContentCaptureService.TAG;
        stringBuilder = new StringBuilder();
        stringBuilder.append("Can't execute ");
        stringBuilder.append(param1String);
        stringBuilder.append("(), references are null");
        Slog.w(null, stringBuilder.toString());
        return;
      } 
      long l = Binder.clearCallingIdentity();
      try {
        _$$Lambda$ContentCaptureService$DataShareReadAdapterDelegate$RLJmZHFfMn__QO6_Wcm57K0fhj8 _$$Lambda$ContentCaptureService$DataShareReadAdapterDelegate$RLJmZHFfMn__QO6_Wcm57K0fhj8 = new _$$Lambda$ContentCaptureService$DataShareReadAdapterDelegate$RLJmZHFfMn__QO6_Wcm57K0fhj8();
        this((Consumer)null, (DataShareReadAdapter)stringBuilder);
        executor.execute(_$$Lambda$ContentCaptureService$DataShareReadAdapterDelegate$RLJmZHFfMn__QO6_Wcm57K0fhj8);
        return;
      } finally {
        Binder.restoreCallingIdentity(l);
      } 
    }
    
    private void clearHardReferences() {
      ContentCaptureService.LocalDataShareAdapterResourceManager localDataShareAdapterResourceManager = this.mResourceManagerReference.get();
      if (localDataShareAdapterResourceManager == null) {
        Slog.w(ContentCaptureService.TAG, "Can't clear references, resource manager has been GC'ed");
        return;
      } 
      localDataShareAdapterResourceManager.clearHardReferences(this);
    }
  }
  
  class LocalDataShareAdapterResourceManager {
    private Map<ContentCaptureService.DataShareReadAdapterDelegate, DataShareReadAdapter> mDataShareReadAdapterHardReferences;
    
    private Map<ContentCaptureService.DataShareReadAdapterDelegate, Executor> mExecutorHardReferences;
    
    private LocalDataShareAdapterResourceManager() {
      this.mDataShareReadAdapterHardReferences = new HashMap<>();
      this.mExecutorHardReferences = new HashMap<>();
    }
    
    void initializeForDelegate(ContentCaptureService.DataShareReadAdapterDelegate param1DataShareReadAdapterDelegate, DataShareReadAdapter param1DataShareReadAdapter, Executor param1Executor) {
      this.mDataShareReadAdapterHardReferences.put(param1DataShareReadAdapterDelegate, param1DataShareReadAdapter);
      this.mExecutorHardReferences.put(param1DataShareReadAdapterDelegate, param1Executor);
    }
    
    Executor getExecutor(ContentCaptureService.DataShareReadAdapterDelegate param1DataShareReadAdapterDelegate) {
      return this.mExecutorHardReferences.get(param1DataShareReadAdapterDelegate);
    }
    
    DataShareReadAdapter getAdapter(ContentCaptureService.DataShareReadAdapterDelegate param1DataShareReadAdapterDelegate) {
      return this.mDataShareReadAdapterHardReferences.get(param1DataShareReadAdapterDelegate);
    }
    
    void clearHardReferences(ContentCaptureService.DataShareReadAdapterDelegate param1DataShareReadAdapterDelegate) {
      this.mDataShareReadAdapterHardReferences.remove(param1DataShareReadAdapterDelegate);
      this.mExecutorHardReferences.remove(param1DataShareReadAdapterDelegate);
    }
  }
}
