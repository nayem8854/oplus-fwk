package android.service.gatekeeper;

import android.os.Parcel;
import android.os.Parcelable;

public final class GateKeeperResponse implements Parcelable {
  public static final Parcelable.Creator<GateKeeperResponse> CREATOR;
  
  public static final GateKeeperResponse ERROR = createGenericResponse(-1);
  
  public static final int RESPONSE_ERROR = -1;
  
  public static final int RESPONSE_OK = 0;
  
  public static final int RESPONSE_RETRY = 1;
  
  private byte[] mPayload;
  
  private final int mResponseCode;
  
  private boolean mShouldReEnroll;
  
  private int mTimeout;
  
  private GateKeeperResponse(int paramInt) {
    this.mResponseCode = paramInt;
  }
  
  public static GateKeeperResponse createGenericResponse(int paramInt) {
    return new GateKeeperResponse(paramInt);
  }
  
  private static GateKeeperResponse createRetryResponse(int paramInt) {
    GateKeeperResponse gateKeeperResponse = new GateKeeperResponse(1);
    gateKeeperResponse.mTimeout = paramInt;
    return gateKeeperResponse;
  }
  
  public static GateKeeperResponse createOkResponse(byte[] paramArrayOfbyte, boolean paramBoolean) {
    GateKeeperResponse gateKeeperResponse = new GateKeeperResponse(0);
    gateKeeperResponse.mPayload = paramArrayOfbyte;
    gateKeeperResponse.mShouldReEnroll = paramBoolean;
    return gateKeeperResponse;
  }
  
  public int describeContents() {
    return 0;
  }
  
  static {
    CREATOR = new Parcelable.Creator<GateKeeperResponse>() {
        public GateKeeperResponse createFromParcel(Parcel param1Parcel) {
          GateKeeperResponse gateKeeperResponse;
          int i = param1Parcel.readInt();
          boolean bool = true;
          if (i == 1) {
            gateKeeperResponse = GateKeeperResponse.createRetryResponse(param1Parcel.readInt());
          } else if (i == 0) {
            if (gateKeeperResponse.readInt() != 1)
              bool = false; 
            byte[] arrayOfByte = null;
            i = gateKeeperResponse.readInt();
            if (i > 0) {
              arrayOfByte = new byte[i];
              gateKeeperResponse.readByteArray(arrayOfByte);
            } 
            gateKeeperResponse = GateKeeperResponse.createOkResponse(arrayOfByte, bool);
          } else {
            gateKeeperResponse = GateKeeperResponse.createGenericResponse(i);
          } 
          return gateKeeperResponse;
        }
        
        public GateKeeperResponse[] newArray(int param1Int) {
          return new GateKeeperResponse[param1Int];
        }
      };
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mResponseCode);
    paramInt = this.mResponseCode;
    if (paramInt == 1) {
      paramParcel.writeInt(this.mTimeout);
    } else if (paramInt == 0) {
      paramParcel.writeInt(this.mShouldReEnroll);
      byte[] arrayOfByte = this.mPayload;
      if (arrayOfByte != null) {
        paramParcel.writeInt(arrayOfByte.length);
        paramParcel.writeByteArray(this.mPayload);
      } else {
        paramParcel.writeInt(0);
      } 
    } 
  }
  
  public byte[] getPayload() {
    return this.mPayload;
  }
  
  public int getTimeout() {
    return this.mTimeout;
  }
  
  public boolean getShouldReEnroll() {
    return this.mShouldReEnroll;
  }
  
  public int getResponseCode() {
    return this.mResponseCode;
  }
}
