package android.service.gatekeeper;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IGateKeeperService extends IInterface {
  void clearSecureUserId(int paramInt) throws RemoteException;
  
  GateKeeperResponse enroll(int paramInt, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3) throws RemoteException;
  
  long getSecureUserId(int paramInt) throws RemoteException;
  
  void reportDeviceSetupComplete() throws RemoteException;
  
  GateKeeperResponse verify(int paramInt, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws RemoteException;
  
  GateKeeperResponse verifyChallenge(int paramInt, long paramLong, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws RemoteException;
  
  class Default implements IGateKeeperService {
    public GateKeeperResponse enroll(int param1Int, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2, byte[] param1ArrayOfbyte3) throws RemoteException {
      return null;
    }
    
    public GateKeeperResponse verify(int param1Int, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) throws RemoteException {
      return null;
    }
    
    public GateKeeperResponse verifyChallenge(int param1Int, long param1Long, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) throws RemoteException {
      return null;
    }
    
    public long getSecureUserId(int param1Int) throws RemoteException {
      return 0L;
    }
    
    public void clearSecureUserId(int param1Int) throws RemoteException {}
    
    public void reportDeviceSetupComplete() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IGateKeeperService {
    private static final String DESCRIPTOR = "android.service.gatekeeper.IGateKeeperService";
    
    static final int TRANSACTION_clearSecureUserId = 5;
    
    static final int TRANSACTION_enroll = 1;
    
    static final int TRANSACTION_getSecureUserId = 4;
    
    static final int TRANSACTION_reportDeviceSetupComplete = 6;
    
    static final int TRANSACTION_verify = 2;
    
    static final int TRANSACTION_verifyChallenge = 3;
    
    public Stub() {
      attachInterface(this, "android.service.gatekeeper.IGateKeeperService");
    }
    
    public static IGateKeeperService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.gatekeeper.IGateKeeperService");
      if (iInterface != null && iInterface instanceof IGateKeeperService)
        return (IGateKeeperService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "reportDeviceSetupComplete";
        case 5:
          return "clearSecureUserId";
        case 4:
          return "getSecureUserId";
        case 3:
          return "verifyChallenge";
        case 2:
          return "verify";
        case 1:
          break;
      } 
      return "enroll";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        byte[] arrayOfByte3;
        GateKeeperResponse gateKeeperResponse3;
        byte[] arrayOfByte2;
        GateKeeperResponse gateKeeperResponse2;
        long l;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("android.service.gatekeeper.IGateKeeperService");
            reportDeviceSetupComplete();
            param1Parcel2.writeNoException();
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.service.gatekeeper.IGateKeeperService");
            param1Int1 = param1Parcel1.readInt();
            clearSecureUserId(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.service.gatekeeper.IGateKeeperService");
            param1Int1 = param1Parcel1.readInt();
            l = getSecureUserId(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.service.gatekeeper.IGateKeeperService");
            param1Int1 = param1Parcel1.readInt();
            l = param1Parcel1.readLong();
            arrayOfByte4 = param1Parcel1.createByteArray();
            arrayOfByte3 = param1Parcel1.createByteArray();
            gateKeeperResponse3 = verifyChallenge(param1Int1, l, arrayOfByte4, arrayOfByte3);
            param1Parcel2.writeNoException();
            if (gateKeeperResponse3 != null) {
              param1Parcel2.writeInt(1);
              gateKeeperResponse3.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 2:
            gateKeeperResponse3.enforceInterface("android.service.gatekeeper.IGateKeeperService");
            param1Int1 = gateKeeperResponse3.readInt();
            arrayOfByte4 = gateKeeperResponse3.createByteArray();
            arrayOfByte2 = gateKeeperResponse3.createByteArray();
            gateKeeperResponse2 = verify(param1Int1, arrayOfByte4, arrayOfByte2);
            param1Parcel2.writeNoException();
            if (gateKeeperResponse2 != null) {
              param1Parcel2.writeInt(1);
              gateKeeperResponse2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        gateKeeperResponse2.enforceInterface("android.service.gatekeeper.IGateKeeperService");
        param1Int1 = gateKeeperResponse2.readInt();
        byte[] arrayOfByte5 = gateKeeperResponse2.createByteArray();
        byte[] arrayOfByte4 = gateKeeperResponse2.createByteArray();
        byte[] arrayOfByte1 = gateKeeperResponse2.createByteArray();
        GateKeeperResponse gateKeeperResponse1 = enroll(param1Int1, arrayOfByte5, arrayOfByte4, arrayOfByte1);
        param1Parcel2.writeNoException();
        if (gateKeeperResponse1 != null) {
          param1Parcel2.writeInt(1);
          gateKeeperResponse1.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      param1Parcel2.writeString("android.service.gatekeeper.IGateKeeperService");
      return true;
    }
    
    private static class Proxy implements IGateKeeperService {
      public static IGateKeeperService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.gatekeeper.IGateKeeperService";
      }
      
      public GateKeeperResponse enroll(int param2Int, byte[] param2ArrayOfbyte1, byte[] param2ArrayOfbyte2, byte[] param2ArrayOfbyte3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.gatekeeper.IGateKeeperService");
          parcel1.writeInt(param2Int);
          parcel1.writeByteArray(param2ArrayOfbyte1);
          parcel1.writeByteArray(param2ArrayOfbyte2);
          parcel1.writeByteArray(param2ArrayOfbyte3);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IGateKeeperService.Stub.getDefaultImpl() != null)
            return IGateKeeperService.Stub.getDefaultImpl().enroll(param2Int, param2ArrayOfbyte1, param2ArrayOfbyte2, param2ArrayOfbyte3); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            GateKeeperResponse gateKeeperResponse = GateKeeperResponse.CREATOR.createFromParcel(parcel2);
          } else {
            param2ArrayOfbyte1 = null;
          } 
          return (GateKeeperResponse)param2ArrayOfbyte1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public GateKeeperResponse verify(int param2Int, byte[] param2ArrayOfbyte1, byte[] param2ArrayOfbyte2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.gatekeeper.IGateKeeperService");
          parcel1.writeInt(param2Int);
          parcel1.writeByteArray(param2ArrayOfbyte1);
          parcel1.writeByteArray(param2ArrayOfbyte2);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IGateKeeperService.Stub.getDefaultImpl() != null)
            return IGateKeeperService.Stub.getDefaultImpl().verify(param2Int, param2ArrayOfbyte1, param2ArrayOfbyte2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            GateKeeperResponse gateKeeperResponse = GateKeeperResponse.CREATOR.createFromParcel(parcel2);
          } else {
            param2ArrayOfbyte1 = null;
          } 
          return (GateKeeperResponse)param2ArrayOfbyte1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public GateKeeperResponse verifyChallenge(int param2Int, long param2Long, byte[] param2ArrayOfbyte1, byte[] param2ArrayOfbyte2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.gatekeeper.IGateKeeperService");
          parcel1.writeInt(param2Int);
          parcel1.writeLong(param2Long);
          parcel1.writeByteArray(param2ArrayOfbyte1);
          parcel1.writeByteArray(param2ArrayOfbyte2);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IGateKeeperService.Stub.getDefaultImpl() != null)
            return IGateKeeperService.Stub.getDefaultImpl().verifyChallenge(param2Int, param2Long, param2ArrayOfbyte1, param2ArrayOfbyte2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            GateKeeperResponse gateKeeperResponse = GateKeeperResponse.CREATOR.createFromParcel(parcel2);
          } else {
            param2ArrayOfbyte1 = null;
          } 
          return (GateKeeperResponse)param2ArrayOfbyte1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getSecureUserId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.gatekeeper.IGateKeeperService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IGateKeeperService.Stub.getDefaultImpl() != null)
            return IGateKeeperService.Stub.getDefaultImpl().getSecureUserId(param2Int); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearSecureUserId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.gatekeeper.IGateKeeperService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IGateKeeperService.Stub.getDefaultImpl() != null) {
            IGateKeeperService.Stub.getDefaultImpl().clearSecureUserId(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportDeviceSetupComplete() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.gatekeeper.IGateKeeperService");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IGateKeeperService.Stub.getDefaultImpl() != null) {
            IGateKeeperService.Stub.getDefaultImpl().reportDeviceSetupComplete();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IGateKeeperService param1IGateKeeperService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IGateKeeperService != null) {
          Proxy.sDefaultImpl = param1IGateKeeperService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IGateKeeperService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
