package android.service.contentsuggestions;

import android.annotation.SystemApi;
import android.app.Service;
import android.app.contentsuggestions.ClassificationsRequest;
import android.app.contentsuggestions.ContentSuggestionsManager;
import android.app.contentsuggestions.IClassificationsCallback;
import android.app.contentsuggestions.ISelectionsCallback;
import android.app.contentsuggestions.SelectionsRequest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.graphics.GraphicBuffer;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.util.Slog;
import com.android.internal.util.function.QuadConsumer;
import com.android.internal.util.function.TriConsumer;
import com.android.internal.util.function.pooled.PooledLambda;
import java.util.List;

@SystemApi
public abstract class ContentSuggestionsService extends Service {
  public static final String SERVICE_INTERFACE = "android.service.contentsuggestions.ContentSuggestionsService";
  
  private static final String TAG = ContentSuggestionsService.class.getSimpleName();
  
  private Handler mHandler;
  
  private final IContentSuggestionsService mInterface = new IContentSuggestionsService.Stub() {
      final ContentSuggestionsService this$0;
      
      public void provideContextImage(int param1Int1, GraphicBuffer param1GraphicBuffer, int param1Int2, Bundle param1Bundle) {
        if (!param1Bundle.containsKey("android.contentsuggestions.extra.BITMAP") || param1GraphicBuffer == null) {
          Bitmap bitmap = null;
          if (param1Bundle.containsKey("android.contentsuggestions.extra.BITMAP")) {
            bitmap = param1Bundle.<Bitmap>getParcelable("android.contentsuggestions.extra.BITMAP");
          } else if (param1GraphicBuffer != null) {
            ColorSpace colorSpace;
            Bitmap bitmap1 = null;
            bitmap = bitmap1;
            if (param1Int2 >= 0) {
              bitmap = bitmap1;
              if (param1Int2 < (ColorSpace.Named.values()).length)
                colorSpace = ColorSpace.get(ColorSpace.Named.values()[param1Int2]); 
            } 
            bitmap = Bitmap.wrapHardwareBuffer(param1GraphicBuffer, colorSpace);
          } 
          Handler handler = ContentSuggestionsService.this.mHandler;
          -$.Lambda.Mv-op4AGm9iWERwfXEFnqOVKWt0 mv-op4AGm9iWERwfXEFnqOVKWt0 = _$$Lambda$Mv_op4AGm9iWERwfXEFnqOVKWt0.INSTANCE;
          ContentSuggestionsService contentSuggestionsService = ContentSuggestionsService.this;
          Message message = PooledLambda.obtainMessage((QuadConsumer)mv-op4AGm9iWERwfXEFnqOVKWt0, contentSuggestionsService, Integer.valueOf(param1Int1), bitmap, param1Bundle);
          handler.sendMessage(message);
          return;
        } 
        throw new IllegalArgumentException("Two bitmaps provided; expected one.");
      }
      
      public void suggestContentSelections(SelectionsRequest param1SelectionsRequest, ISelectionsCallback param1ISelectionsCallback) {
        Handler handler = ContentSuggestionsService.this.mHandler;
        -$.Lambda.yZSFRdNS_6TrQJ8NQKXAv0kSKzk yZSFRdNS_6TrQJ8NQKXAv0kSKzk = _$$Lambda$yZSFRdNS_6TrQJ8NQKXAv0kSKzk.INSTANCE;
        ContentSuggestionsService contentSuggestionsService = ContentSuggestionsService.this;
        ContentSuggestionsManager.SelectionsCallback selectionsCallback = contentSuggestionsService.wrapSelectionsCallback(param1ISelectionsCallback);
        handler.sendMessage(PooledLambda.obtainMessage((TriConsumer)yZSFRdNS_6TrQJ8NQKXAv0kSKzk, contentSuggestionsService, param1SelectionsRequest, selectionsCallback));
      }
      
      public void classifyContentSelections(ClassificationsRequest param1ClassificationsRequest, IClassificationsCallback param1IClassificationsCallback) {
        Handler handler = ContentSuggestionsService.this.mHandler;
        -$.Lambda.oRtA6f92le979Nv8-bd2We4x10 oRtA6f92le979Nv8-bd2We4x10 = _$$Lambda$5oRtA6f92le979Nv8_bd2We4x10.INSTANCE;
        ContentSuggestionsService contentSuggestionsService = ContentSuggestionsService.this;
        ContentSuggestionsManager.ClassificationsCallback classificationsCallback = contentSuggestionsService.wrapClassificationCallback(param1IClassificationsCallback);
        handler.sendMessage(PooledLambda.obtainMessage((TriConsumer)oRtA6f92le979Nv8-bd2We4x10, contentSuggestionsService, param1ClassificationsRequest, classificationsCallback));
      }
      
      public void notifyInteraction(String param1String, Bundle param1Bundle) {
        Handler handler = ContentSuggestionsService.this.mHandler;
        -$.Lambda.XFxerYS8emT_xgiGwwUrQtqnPnc xFxerYS8emT_xgiGwwUrQtqnPnc = _$$Lambda$XFxerYS8emT_xgiGwwUrQtqnPnc.INSTANCE;
        ContentSuggestionsService contentSuggestionsService = ContentSuggestionsService.this;
        Message message = PooledLambda.obtainMessage((TriConsumer)xFxerYS8emT_xgiGwwUrQtqnPnc, contentSuggestionsService, param1String, param1Bundle);
        handler.sendMessage(message);
      }
    };
  
  public void onCreate() {
    super.onCreate();
    this.mHandler = new Handler(Looper.getMainLooper(), null, true);
  }
  
  public final IBinder onBind(Intent paramIntent) {
    if ("android.service.contentsuggestions.ContentSuggestionsService".equals(paramIntent.getAction()))
      return this.mInterface.asBinder(); 
    String str = TAG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Tried to bind to wrong intent (should be android.service.contentsuggestions.ContentSuggestionsService: ");
    stringBuilder.append(paramIntent);
    Log.w(str, stringBuilder.toString());
    return null;
  }
  
  private ContentSuggestionsManager.SelectionsCallback wrapSelectionsCallback(ISelectionsCallback paramISelectionsCallback) {
    return new _$$Lambda$ContentSuggestionsService$Cq6WuwbJQLqgS0UnqLBYUMft1GM(paramISelectionsCallback);
  }
  
  private ContentSuggestionsManager.ClassificationsCallback wrapClassificationCallback(IClassificationsCallback paramIClassificationsCallback) {
    return new _$$Lambda$ContentSuggestionsService$EMLezZyRGdfK3m_N1TAvrHKUEII(paramIClassificationsCallback);
  }
  
  public abstract void onClassifyContentSelections(ClassificationsRequest paramClassificationsRequest, ContentSuggestionsManager.ClassificationsCallback paramClassificationsCallback);
  
  public abstract void onNotifyInteraction(String paramString, Bundle paramBundle);
  
  public abstract void onProcessContextImage(int paramInt, Bitmap paramBitmap, Bundle paramBundle);
  
  public abstract void onSuggestContentSelections(SelectionsRequest paramSelectionsRequest, ContentSuggestionsManager.SelectionsCallback paramSelectionsCallback);
}
