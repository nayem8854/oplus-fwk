package android.service.contentsuggestions;

import android.app.contentsuggestions.ClassificationsRequest;
import android.app.contentsuggestions.IClassificationsCallback;
import android.app.contentsuggestions.ISelectionsCallback;
import android.app.contentsuggestions.SelectionsRequest;
import android.graphics.GraphicBuffer;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IContentSuggestionsService extends IInterface {
  void classifyContentSelections(ClassificationsRequest paramClassificationsRequest, IClassificationsCallback paramIClassificationsCallback) throws RemoteException;
  
  void notifyInteraction(String paramString, Bundle paramBundle) throws RemoteException;
  
  void provideContextImage(int paramInt1, GraphicBuffer paramGraphicBuffer, int paramInt2, Bundle paramBundle) throws RemoteException;
  
  void suggestContentSelections(SelectionsRequest paramSelectionsRequest, ISelectionsCallback paramISelectionsCallback) throws RemoteException;
  
  class Default implements IContentSuggestionsService {
    public void provideContextImage(int param1Int1, GraphicBuffer param1GraphicBuffer, int param1Int2, Bundle param1Bundle) throws RemoteException {}
    
    public void suggestContentSelections(SelectionsRequest param1SelectionsRequest, ISelectionsCallback param1ISelectionsCallback) throws RemoteException {}
    
    public void classifyContentSelections(ClassificationsRequest param1ClassificationsRequest, IClassificationsCallback param1IClassificationsCallback) throws RemoteException {}
    
    public void notifyInteraction(String param1String, Bundle param1Bundle) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IContentSuggestionsService {
    private static final String DESCRIPTOR = "android.service.contentsuggestions.IContentSuggestionsService";
    
    static final int TRANSACTION_classifyContentSelections = 3;
    
    static final int TRANSACTION_notifyInteraction = 4;
    
    static final int TRANSACTION_provideContextImage = 1;
    
    static final int TRANSACTION_suggestContentSelections = 2;
    
    public Stub() {
      attachInterface(this, "android.service.contentsuggestions.IContentSuggestionsService");
    }
    
    public static IContentSuggestionsService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.contentsuggestions.IContentSuggestionsService");
      if (iInterface != null && iInterface instanceof IContentSuggestionsService)
        return (IContentSuggestionsService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "notifyInteraction";
          } 
          return "classifyContentSelections";
        } 
        return "suggestContentSelections";
      } 
      return "provideContextImage";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ISelectionsCallback iSelectionsCallback;
      if (param1Int1 != 1) {
        IClassificationsCallback iClassificationsCallback;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.service.contentsuggestions.IContentSuggestionsService");
              return true;
            } 
            param1Parcel1.enforceInterface("android.service.contentsuggestions.IContentSuggestionsService");
            String str = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifyInteraction(str, (Bundle)param1Parcel1);
            return true;
          } 
          param1Parcel1.enforceInterface("android.service.contentsuggestions.IContentSuggestionsService");
          if (param1Parcel1.readInt() != 0) {
            ClassificationsRequest classificationsRequest = ClassificationsRequest.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel2 = null;
          } 
          iClassificationsCallback = IClassificationsCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
          classifyContentSelections((ClassificationsRequest)param1Parcel2, iClassificationsCallback);
          return true;
        } 
        iClassificationsCallback.enforceInterface("android.service.contentsuggestions.IContentSuggestionsService");
        if (iClassificationsCallback.readInt() != 0) {
          SelectionsRequest selectionsRequest = SelectionsRequest.CREATOR.createFromParcel((Parcel)iClassificationsCallback);
        } else {
          param1Parcel2 = null;
        } 
        iSelectionsCallback = ISelectionsCallback.Stub.asInterface(iClassificationsCallback.readStrongBinder());
        suggestContentSelections((SelectionsRequest)param1Parcel2, iSelectionsCallback);
        return true;
      } 
      iSelectionsCallback.enforceInterface("android.service.contentsuggestions.IContentSuggestionsService");
      param1Int2 = iSelectionsCallback.readInt();
      if (iSelectionsCallback.readInt() != 0) {
        GraphicBuffer graphicBuffer = GraphicBuffer.CREATOR.createFromParcel((Parcel)iSelectionsCallback);
      } else {
        param1Parcel2 = null;
      } 
      param1Int1 = iSelectionsCallback.readInt();
      if (iSelectionsCallback.readInt() != 0) {
        Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)iSelectionsCallback);
      } else {
        iSelectionsCallback = null;
      } 
      provideContextImage(param1Int2, (GraphicBuffer)param1Parcel2, param1Int1, (Bundle)iSelectionsCallback);
      return true;
    }
    
    private static class Proxy implements IContentSuggestionsService {
      public static IContentSuggestionsService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.contentsuggestions.IContentSuggestionsService";
      }
      
      public void provideContextImage(int param2Int1, GraphicBuffer param2GraphicBuffer, int param2Int2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.contentsuggestions.IContentSuggestionsService");
          parcel.writeInt(param2Int1);
          if (param2GraphicBuffer != null) {
            parcel.writeInt(1);
            param2GraphicBuffer.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int2);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IContentSuggestionsService.Stub.getDefaultImpl() != null) {
            IContentSuggestionsService.Stub.getDefaultImpl().provideContextImage(param2Int1, param2GraphicBuffer, param2Int2, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void suggestContentSelections(SelectionsRequest param2SelectionsRequest, ISelectionsCallback param2ISelectionsCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.contentsuggestions.IContentSuggestionsService");
          if (param2SelectionsRequest != null) {
            parcel.writeInt(1);
            param2SelectionsRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ISelectionsCallback != null) {
            iBinder = param2ISelectionsCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IContentSuggestionsService.Stub.getDefaultImpl() != null) {
            IContentSuggestionsService.Stub.getDefaultImpl().suggestContentSelections(param2SelectionsRequest, param2ISelectionsCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void classifyContentSelections(ClassificationsRequest param2ClassificationsRequest, IClassificationsCallback param2IClassificationsCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.contentsuggestions.IContentSuggestionsService");
          if (param2ClassificationsRequest != null) {
            parcel.writeInt(1);
            param2ClassificationsRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IClassificationsCallback != null) {
            iBinder = param2IClassificationsCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IContentSuggestionsService.Stub.getDefaultImpl() != null) {
            IContentSuggestionsService.Stub.getDefaultImpl().classifyContentSelections(param2ClassificationsRequest, param2IClassificationsCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyInteraction(String param2String, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.contentsuggestions.IContentSuggestionsService");
          parcel.writeString(param2String);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IContentSuggestionsService.Stub.getDefaultImpl() != null) {
            IContentSuggestionsService.Stub.getDefaultImpl().notifyInteraction(param2String, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IContentSuggestionsService param1IContentSuggestionsService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IContentSuggestionsService != null) {
          Proxy.sDefaultImpl = param1IContentSuggestionsService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IContentSuggestionsService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
