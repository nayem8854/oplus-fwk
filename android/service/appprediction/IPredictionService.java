package android.service.appprediction;

import android.app.prediction.AppPredictionContext;
import android.app.prediction.AppPredictionSessionId;
import android.app.prediction.AppTargetEvent;
import android.app.prediction.IPredictionCallback;
import android.content.pm.ParceledListSlice;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPredictionService extends IInterface {
  void notifyAppTargetEvent(AppPredictionSessionId paramAppPredictionSessionId, AppTargetEvent paramAppTargetEvent) throws RemoteException;
  
  void notifyLaunchLocationShown(AppPredictionSessionId paramAppPredictionSessionId, String paramString, ParceledListSlice paramParceledListSlice) throws RemoteException;
  
  void onCreatePredictionSession(AppPredictionContext paramAppPredictionContext, AppPredictionSessionId paramAppPredictionSessionId) throws RemoteException;
  
  void onDestroyPredictionSession(AppPredictionSessionId paramAppPredictionSessionId) throws RemoteException;
  
  void registerPredictionUpdates(AppPredictionSessionId paramAppPredictionSessionId, IPredictionCallback paramIPredictionCallback) throws RemoteException;
  
  void requestPredictionUpdate(AppPredictionSessionId paramAppPredictionSessionId) throws RemoteException;
  
  void sortAppTargets(AppPredictionSessionId paramAppPredictionSessionId, ParceledListSlice paramParceledListSlice, IPredictionCallback paramIPredictionCallback) throws RemoteException;
  
  void unregisterPredictionUpdates(AppPredictionSessionId paramAppPredictionSessionId, IPredictionCallback paramIPredictionCallback) throws RemoteException;
  
  class Default implements IPredictionService {
    public void onCreatePredictionSession(AppPredictionContext param1AppPredictionContext, AppPredictionSessionId param1AppPredictionSessionId) throws RemoteException {}
    
    public void notifyAppTargetEvent(AppPredictionSessionId param1AppPredictionSessionId, AppTargetEvent param1AppTargetEvent) throws RemoteException {}
    
    public void notifyLaunchLocationShown(AppPredictionSessionId param1AppPredictionSessionId, String param1String, ParceledListSlice param1ParceledListSlice) throws RemoteException {}
    
    public void sortAppTargets(AppPredictionSessionId param1AppPredictionSessionId, ParceledListSlice param1ParceledListSlice, IPredictionCallback param1IPredictionCallback) throws RemoteException {}
    
    public void registerPredictionUpdates(AppPredictionSessionId param1AppPredictionSessionId, IPredictionCallback param1IPredictionCallback) throws RemoteException {}
    
    public void unregisterPredictionUpdates(AppPredictionSessionId param1AppPredictionSessionId, IPredictionCallback param1IPredictionCallback) throws RemoteException {}
    
    public void requestPredictionUpdate(AppPredictionSessionId param1AppPredictionSessionId) throws RemoteException {}
    
    public void onDestroyPredictionSession(AppPredictionSessionId param1AppPredictionSessionId) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPredictionService {
    private static final String DESCRIPTOR = "android.service.appprediction.IPredictionService";
    
    static final int TRANSACTION_notifyAppTargetEvent = 2;
    
    static final int TRANSACTION_notifyLaunchLocationShown = 3;
    
    static final int TRANSACTION_onCreatePredictionSession = 1;
    
    static final int TRANSACTION_onDestroyPredictionSession = 8;
    
    static final int TRANSACTION_registerPredictionUpdates = 5;
    
    static final int TRANSACTION_requestPredictionUpdate = 7;
    
    static final int TRANSACTION_sortAppTargets = 4;
    
    static final int TRANSACTION_unregisterPredictionUpdates = 6;
    
    public Stub() {
      attachInterface(this, "android.service.appprediction.IPredictionService");
    }
    
    public static IPredictionService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.appprediction.IPredictionService");
      if (iInterface != null && iInterface instanceof IPredictionService)
        return (IPredictionService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "onDestroyPredictionSession";
        case 7:
          return "requestPredictionUpdate";
        case 6:
          return "unregisterPredictionUpdates";
        case 5:
          return "registerPredictionUpdates";
        case 4:
          return "sortAppTargets";
        case 3:
          return "notifyLaunchLocationShown";
        case 2:
          return "notifyAppTargetEvent";
        case 1:
          break;
      } 
      return "onCreatePredictionSession";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        IPredictionCallback iPredictionCallback;
        ParceledListSlice parceledListSlice;
        String str;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("android.service.appprediction.IPredictionService");
            if (param1Parcel1.readInt() != 0) {
              AppPredictionSessionId appPredictionSessionId = AppPredictionSessionId.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onDestroyPredictionSession((AppPredictionSessionId)param1Parcel1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.service.appprediction.IPredictionService");
            if (param1Parcel1.readInt() != 0) {
              AppPredictionSessionId appPredictionSessionId = AppPredictionSessionId.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            requestPredictionUpdate((AppPredictionSessionId)param1Parcel1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.service.appprediction.IPredictionService");
            if (param1Parcel1.readInt() != 0) {
              AppPredictionSessionId appPredictionSessionId = AppPredictionSessionId.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            iPredictionCallback = IPredictionCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            unregisterPredictionUpdates((AppPredictionSessionId)param1Parcel2, iPredictionCallback);
            return true;
          case 5:
            iPredictionCallback.enforceInterface("android.service.appprediction.IPredictionService");
            if (iPredictionCallback.readInt() != 0) {
              AppPredictionSessionId appPredictionSessionId = AppPredictionSessionId.CREATOR.createFromParcel((Parcel)iPredictionCallback);
            } else {
              param1Parcel2 = null;
            } 
            iPredictionCallback = IPredictionCallback.Stub.asInterface(iPredictionCallback.readStrongBinder());
            registerPredictionUpdates((AppPredictionSessionId)param1Parcel2, iPredictionCallback);
            return true;
          case 4:
            iPredictionCallback.enforceInterface("android.service.appprediction.IPredictionService");
            if (iPredictionCallback.readInt() != 0) {
              AppPredictionSessionId appPredictionSessionId = AppPredictionSessionId.CREATOR.createFromParcel((Parcel)iPredictionCallback);
            } else {
              param1Parcel2 = null;
            } 
            if (iPredictionCallback.readInt() != 0) {
              parceledListSlice = ParceledListSlice.CREATOR.createFromParcel((Parcel)iPredictionCallback);
            } else {
              parceledListSlice = null;
            } 
            iPredictionCallback = IPredictionCallback.Stub.asInterface(iPredictionCallback.readStrongBinder());
            sortAppTargets((AppPredictionSessionId)param1Parcel2, parceledListSlice, iPredictionCallback);
            return true;
          case 3:
            iPredictionCallback.enforceInterface("android.service.appprediction.IPredictionService");
            if (iPredictionCallback.readInt() != 0) {
              AppPredictionSessionId appPredictionSessionId = AppPredictionSessionId.CREATOR.createFromParcel((Parcel)iPredictionCallback);
            } else {
              param1Parcel2 = null;
            } 
            str = iPredictionCallback.readString();
            if (iPredictionCallback.readInt() != 0) {
              ParceledListSlice parceledListSlice1 = ParceledListSlice.CREATOR.createFromParcel((Parcel)iPredictionCallback);
            } else {
              iPredictionCallback = null;
            } 
            notifyLaunchLocationShown((AppPredictionSessionId)param1Parcel2, str, (ParceledListSlice)iPredictionCallback);
            return true;
          case 2:
            iPredictionCallback.enforceInterface("android.service.appprediction.IPredictionService");
            if (iPredictionCallback.readInt() != 0) {
              AppPredictionSessionId appPredictionSessionId = AppPredictionSessionId.CREATOR.createFromParcel((Parcel)iPredictionCallback);
            } else {
              param1Parcel2 = null;
            } 
            if (iPredictionCallback.readInt() != 0) {
              AppTargetEvent appTargetEvent = AppTargetEvent.CREATOR.createFromParcel((Parcel)iPredictionCallback);
            } else {
              iPredictionCallback = null;
            } 
            notifyAppTargetEvent((AppPredictionSessionId)param1Parcel2, (AppTargetEvent)iPredictionCallback);
            return true;
          case 1:
            break;
        } 
        iPredictionCallback.enforceInterface("android.service.appprediction.IPredictionService");
        if (iPredictionCallback.readInt() != 0) {
          AppPredictionContext appPredictionContext = AppPredictionContext.CREATOR.createFromParcel((Parcel)iPredictionCallback);
        } else {
          param1Parcel2 = null;
        } 
        if (iPredictionCallback.readInt() != 0) {
          AppPredictionSessionId appPredictionSessionId = AppPredictionSessionId.CREATOR.createFromParcel((Parcel)iPredictionCallback);
        } else {
          iPredictionCallback = null;
        } 
        onCreatePredictionSession((AppPredictionContext)param1Parcel2, (AppPredictionSessionId)iPredictionCallback);
        return true;
      } 
      param1Parcel2.writeString("android.service.appprediction.IPredictionService");
      return true;
    }
    
    private static class Proxy implements IPredictionService {
      public static IPredictionService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.appprediction.IPredictionService";
      }
      
      public void onCreatePredictionSession(AppPredictionContext param2AppPredictionContext, AppPredictionSessionId param2AppPredictionSessionId) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.appprediction.IPredictionService");
          if (param2AppPredictionContext != null) {
            parcel.writeInt(1);
            param2AppPredictionContext.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2AppPredictionSessionId != null) {
            parcel.writeInt(1);
            param2AppPredictionSessionId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IPredictionService.Stub.getDefaultImpl() != null) {
            IPredictionService.Stub.getDefaultImpl().onCreatePredictionSession(param2AppPredictionContext, param2AppPredictionSessionId);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyAppTargetEvent(AppPredictionSessionId param2AppPredictionSessionId, AppTargetEvent param2AppTargetEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.appprediction.IPredictionService");
          if (param2AppPredictionSessionId != null) {
            parcel.writeInt(1);
            param2AppPredictionSessionId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2AppTargetEvent != null) {
            parcel.writeInt(1);
            param2AppTargetEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IPredictionService.Stub.getDefaultImpl() != null) {
            IPredictionService.Stub.getDefaultImpl().notifyAppTargetEvent(param2AppPredictionSessionId, param2AppTargetEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyLaunchLocationShown(AppPredictionSessionId param2AppPredictionSessionId, String param2String, ParceledListSlice param2ParceledListSlice) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.appprediction.IPredictionService");
          if (param2AppPredictionSessionId != null) {
            parcel.writeInt(1);
            param2AppPredictionSessionId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeString(param2String);
          if (param2ParceledListSlice != null) {
            parcel.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IPredictionService.Stub.getDefaultImpl() != null) {
            IPredictionService.Stub.getDefaultImpl().notifyLaunchLocationShown(param2AppPredictionSessionId, param2String, param2ParceledListSlice);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sortAppTargets(AppPredictionSessionId param2AppPredictionSessionId, ParceledListSlice param2ParceledListSlice, IPredictionCallback param2IPredictionCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.appprediction.IPredictionService");
          if (param2AppPredictionSessionId != null) {
            parcel.writeInt(1);
            param2AppPredictionSessionId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ParceledListSlice != null) {
            parcel.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IPredictionCallback != null) {
            iBinder = param2IPredictionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IPredictionService.Stub.getDefaultImpl() != null) {
            IPredictionService.Stub.getDefaultImpl().sortAppTargets(param2AppPredictionSessionId, param2ParceledListSlice, param2IPredictionCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void registerPredictionUpdates(AppPredictionSessionId param2AppPredictionSessionId, IPredictionCallback param2IPredictionCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.appprediction.IPredictionService");
          if (param2AppPredictionSessionId != null) {
            parcel.writeInt(1);
            param2AppPredictionSessionId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IPredictionCallback != null) {
            iBinder = param2IPredictionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IPredictionService.Stub.getDefaultImpl() != null) {
            IPredictionService.Stub.getDefaultImpl().registerPredictionUpdates(param2AppPredictionSessionId, param2IPredictionCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void unregisterPredictionUpdates(AppPredictionSessionId param2AppPredictionSessionId, IPredictionCallback param2IPredictionCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.appprediction.IPredictionService");
          if (param2AppPredictionSessionId != null) {
            parcel.writeInt(1);
            param2AppPredictionSessionId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IPredictionCallback != null) {
            iBinder = param2IPredictionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IPredictionService.Stub.getDefaultImpl() != null) {
            IPredictionService.Stub.getDefaultImpl().unregisterPredictionUpdates(param2AppPredictionSessionId, param2IPredictionCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestPredictionUpdate(AppPredictionSessionId param2AppPredictionSessionId) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.appprediction.IPredictionService");
          if (param2AppPredictionSessionId != null) {
            parcel.writeInt(1);
            param2AppPredictionSessionId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && IPredictionService.Stub.getDefaultImpl() != null) {
            IPredictionService.Stub.getDefaultImpl().requestPredictionUpdate(param2AppPredictionSessionId);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDestroyPredictionSession(AppPredictionSessionId param2AppPredictionSessionId) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.appprediction.IPredictionService");
          if (param2AppPredictionSessionId != null) {
            parcel.writeInt(1);
            param2AppPredictionSessionId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && IPredictionService.Stub.getDefaultImpl() != null) {
            IPredictionService.Stub.getDefaultImpl().onDestroyPredictionSession(param2AppPredictionSessionId);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPredictionService param1IPredictionService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPredictionService != null) {
          Proxy.sDefaultImpl = param1IPredictionService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPredictionService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
