package android.service.appprediction;

import android.annotation.SystemApi;
import android.app.Service;
import android.app.prediction.AppPredictionContext;
import android.app.prediction.AppPredictionSessionId;
import android.app.prediction.AppTarget;
import android.app.prediction.AppTargetEvent;
import android.app.prediction.AppTargetId;
import android.app.prediction.IPredictionCallback;
import android.content.Intent;
import android.content.pm.ParceledListSlice;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.ArrayMap;
import android.util.Log;
import android.util.Slog;
import com.android.internal.util.function.QuadConsumer;
import com.android.internal.util.function.QuintConsumer;
import com.android.internal.util.function.TriConsumer;
import com.android.internal.util.function.pooled.PooledLambda;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

@SystemApi
public abstract class AppPredictionService extends Service {
  private final ArrayMap<AppPredictionSessionId, ArrayList<CallbackWrapper>> mSessionCallbacks = new ArrayMap();
  
  private final IPredictionService mInterface = new IPredictionService.Stub() {
      final AppPredictionService this$0;
      
      public void onCreatePredictionSession(AppPredictionContext param1AppPredictionContext, AppPredictionSessionId param1AppPredictionSessionId) {
        Handler handler = AppPredictionService.this.mHandler;
        -$.Lambda.AppPredictionService.null.dlPwi16n_6u5po2eN8wlW4I1bRw dlPwi16n_6u5po2eN8wlW4I1bRw = _$$Lambda$AppPredictionService$1$dlPwi16n_6u5po2eN8wlW4I1bRw.INSTANCE;
        AppPredictionService appPredictionService = AppPredictionService.this;
        Message message = PooledLambda.obtainMessage((TriConsumer)dlPwi16n_6u5po2eN8wlW4I1bRw, appPredictionService, param1AppPredictionContext, param1AppPredictionSessionId);
        handler.sendMessage(message);
      }
      
      public void notifyAppTargetEvent(AppPredictionSessionId param1AppPredictionSessionId, AppTargetEvent param1AppTargetEvent) {
        Handler handler = AppPredictionService.this.mHandler;
        -$.Lambda.L76XW8q2NG5cTm3_D3JVX8JtaW0 l76XW8q2NG5cTm3_D3JVX8JtaW0 = _$$Lambda$L76XW8q2NG5cTm3_D3JVX8JtaW0.INSTANCE;
        AppPredictionService appPredictionService = AppPredictionService.this;
        Message message = PooledLambda.obtainMessage((TriConsumer)l76XW8q2NG5cTm3_D3JVX8JtaW0, appPredictionService, param1AppPredictionSessionId, param1AppTargetEvent);
        handler.sendMessage(message);
      }
      
      public void notifyLaunchLocationShown(AppPredictionSessionId param1AppPredictionSessionId, String param1String, ParceledListSlice param1ParceledListSlice) {
        Handler handler = AppPredictionService.this.mHandler;
        -$.Lambda.GvHA1SFwOCThMjcs4Yg4JTLin4Y gvHA1SFwOCThMjcs4Yg4JTLin4Y = _$$Lambda$GvHA1SFwOCThMjcs4Yg4JTLin4Y.INSTANCE;
        AppPredictionService appPredictionService = AppPredictionService.this;
        List list = param1ParceledListSlice.getList();
        Message message = PooledLambda.obtainMessage((QuadConsumer)gvHA1SFwOCThMjcs4Yg4JTLin4Y, appPredictionService, param1AppPredictionSessionId, param1String, list);
        handler.sendMessage(message);
      }
      
      public void sortAppTargets(AppPredictionSessionId param1AppPredictionSessionId, ParceledListSlice param1ParceledListSlice, IPredictionCallback param1IPredictionCallback) {
        Handler handler = AppPredictionService.this.mHandler;
        -$.Lambda.hL9oFxwFQPM7PIyu9fQyFqB_mBk hL9oFxwFQPM7PIyu9fQyFqB_mBk = _$$Lambda$hL9oFxwFQPM7PIyu9fQyFqB_mBk.INSTANCE;
        AppPredictionService appPredictionService = AppPredictionService.this;
        List list = param1ParceledListSlice.getList();
        AppPredictionService.CallbackWrapper callbackWrapper = new AppPredictionService.CallbackWrapper(param1IPredictionCallback, null);
        Message message = PooledLambda.obtainMessage((QuintConsumer)hL9oFxwFQPM7PIyu9fQyFqB_mBk, appPredictionService, param1AppPredictionSessionId, list, null, callbackWrapper);
        handler.sendMessage(message);
      }
      
      public void registerPredictionUpdates(AppPredictionSessionId param1AppPredictionSessionId, IPredictionCallback param1IPredictionCallback) {
        Handler handler = AppPredictionService.this.mHandler;
        -$.Lambda.AppPredictionService.null.CDfn7BNaxDP2sak-07muIxqD0XM cDfn7BNaxDP2sak-07muIxqD0XM = _$$Lambda$AppPredictionService$1$CDfn7BNaxDP2sak_07muIxqD0XM.INSTANCE;
        AppPredictionService appPredictionService = AppPredictionService.this;
        Message message = PooledLambda.obtainMessage((TriConsumer)cDfn7BNaxDP2sak-07muIxqD0XM, appPredictionService, param1AppPredictionSessionId, param1IPredictionCallback);
        handler.sendMessage(message);
      }
      
      public void unregisterPredictionUpdates(AppPredictionSessionId param1AppPredictionSessionId, IPredictionCallback param1IPredictionCallback) {
        Handler handler = AppPredictionService.this.mHandler;
        -$.Lambda.AppPredictionService.null.o4A2wryMBwv4mIbcQKrEaoUyik o4A2wryMBwv4mIbcQKrEaoUyik = _$$Lambda$AppPredictionService$1$3o4A2wryMBwv4mIbcQKrEaoUyik.INSTANCE;
        AppPredictionService appPredictionService = AppPredictionService.this;
        Message message = PooledLambda.obtainMessage((TriConsumer)o4A2wryMBwv4mIbcQKrEaoUyik, appPredictionService, param1AppPredictionSessionId, param1IPredictionCallback);
        handler.sendMessage(message);
      }
      
      public void requestPredictionUpdate(AppPredictionSessionId param1AppPredictionSessionId) {
        Handler handler = AppPredictionService.this.mHandler;
        -$.Lambda.AppPredictionService.null.oaGU8LD9Stlihi_KoW_pb0jZjQk oaGU8LD9Stlihi_KoW_pb0jZjQk = _$$Lambda$AppPredictionService$1$oaGU8LD9Stlihi_KoW_pb0jZjQk.INSTANCE;
        AppPredictionService appPredictionService = AppPredictionService.this;
        Message message = PooledLambda.obtainMessage((BiConsumer)oaGU8LD9Stlihi_KoW_pb0jZjQk, appPredictionService, param1AppPredictionSessionId);
        handler.sendMessage(message);
      }
      
      public void onDestroyPredictionSession(AppPredictionSessionId param1AppPredictionSessionId) {
        Handler handler = AppPredictionService.this.mHandler;
        -$.Lambda.AppPredictionService.null.oZsrXgV2j_8Zo7GiDdpYvbTz4h8 oZsrXgV2j_8Zo7GiDdpYvbTz4h8 = _$$Lambda$AppPredictionService$1$oZsrXgV2j_8Zo7GiDdpYvbTz4h8.INSTANCE;
        AppPredictionService appPredictionService = AppPredictionService.this;
        Message message = PooledLambda.obtainMessage((BiConsumer)oZsrXgV2j_8Zo7GiDdpYvbTz4h8, appPredictionService, param1AppPredictionSessionId);
        handler.sendMessage(message);
      }
    };
  
  private Handler mHandler;
  
  private static final String TAG = "AppPredictionService";
  
  public static final String SERVICE_INTERFACE = "android.service.appprediction.AppPredictionService";
  
  public void onCreate() {
    super.onCreate();
    this.mHandler = new Handler(Looper.getMainLooper(), null, true);
  }
  
  public final IBinder onBind(Intent paramIntent) {
    if ("android.service.appprediction.AppPredictionService".equals(paramIntent.getAction()))
      return this.mInterface.asBinder(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Tried to bind to wrong intent (should be android.service.appprediction.AppPredictionService: ");
    stringBuilder.append(paramIntent);
    Log.w("AppPredictionService", stringBuilder.toString());
    return null;
  }
  
  private void doCreatePredictionSession(AppPredictionContext paramAppPredictionContext, AppPredictionSessionId paramAppPredictionSessionId) {
    this.mSessionCallbacks.put(paramAppPredictionSessionId, new ArrayList());
    onCreatePredictionSession(paramAppPredictionContext, paramAppPredictionSessionId);
  }
  
  public void onCreatePredictionSession(AppPredictionContext paramAppPredictionContext, AppPredictionSessionId paramAppPredictionSessionId) {}
  
  private void doRegisterPredictionUpdates(AppPredictionSessionId paramAppPredictionSessionId, IPredictionCallback paramIPredictionCallback) {
    StringBuilder stringBuilder;
    ArrayList<CallbackWrapper> arrayList = (ArrayList)this.mSessionCallbacks.get(paramAppPredictionSessionId);
    if (arrayList == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to register for updates for unknown session: ");
      stringBuilder.append(paramAppPredictionSessionId);
      Slog.e("AppPredictionService", stringBuilder.toString());
      return;
    } 
    CallbackWrapper callbackWrapper = findCallbackWrapper(arrayList, (IPredictionCallback)stringBuilder);
    if (callbackWrapper == null) {
      arrayList.add(new CallbackWrapper((IPredictionCallback)stringBuilder, new _$$Lambda$AppPredictionService$BU3RVDaz_RDf_0tC58L6QbapMAs(this, arrayList)));
      if (arrayList.size() == 1)
        onStartPredictionUpdates(); 
    } 
  }
  
  public void onStartPredictionUpdates() {}
  
  private void doUnregisterPredictionUpdates(AppPredictionSessionId paramAppPredictionSessionId, IPredictionCallback paramIPredictionCallback) {
    StringBuilder stringBuilder;
    ArrayList<CallbackWrapper> arrayList = (ArrayList)this.mSessionCallbacks.get(paramAppPredictionSessionId);
    if (arrayList == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to unregister for updates for unknown session: ");
      stringBuilder.append(paramAppPredictionSessionId);
      Slog.e("AppPredictionService", stringBuilder.toString());
      return;
    } 
    CallbackWrapper callbackWrapper = findCallbackWrapper(arrayList, (IPredictionCallback)stringBuilder);
    if (callbackWrapper != null)
      removeCallbackWrapper(arrayList, callbackWrapper); 
  }
  
  private void removeCallbackWrapper(ArrayList<CallbackWrapper> paramArrayList, CallbackWrapper paramCallbackWrapper) {
    if (paramArrayList == null)
      return; 
    paramArrayList.remove(paramCallbackWrapper);
    if (paramArrayList.isEmpty())
      onStopPredictionUpdates(); 
  }
  
  public void onStopPredictionUpdates() {}
  
  private void doRequestPredictionUpdate(AppPredictionSessionId paramAppPredictionSessionId) {
    ArrayList arrayList = (ArrayList)this.mSessionCallbacks.get(paramAppPredictionSessionId);
    if (arrayList != null && !arrayList.isEmpty())
      onRequestPredictionUpdate(paramAppPredictionSessionId); 
  }
  
  private void doDestroyPredictionSession(AppPredictionSessionId paramAppPredictionSessionId) {
    this.mSessionCallbacks.remove(paramAppPredictionSessionId);
    onDestroyPredictionSession(paramAppPredictionSessionId);
  }
  
  public void onDestroyPredictionSession(AppPredictionSessionId paramAppPredictionSessionId) {}
  
  public final void updatePredictions(AppPredictionSessionId paramAppPredictionSessionId, List<AppTarget> paramList) {
    List list = (List)this.mSessionCallbacks.get(paramAppPredictionSessionId);
    if (list != null)
      for (CallbackWrapper callbackWrapper : list)
        callbackWrapper.accept(paramList);  
  }
  
  private CallbackWrapper findCallbackWrapper(ArrayList<CallbackWrapper> paramArrayList, IPredictionCallback paramIPredictionCallback) {
    for (int i = paramArrayList.size() - 1; i >= 0; i--) {
      if (((CallbackWrapper)paramArrayList.get(i)).isCallback(paramIPredictionCallback))
        return paramArrayList.get(i); 
    } 
    return null;
  }
  
  public abstract void onAppTargetEvent(AppPredictionSessionId paramAppPredictionSessionId, AppTargetEvent paramAppTargetEvent);
  
  public abstract void onLaunchLocationShown(AppPredictionSessionId paramAppPredictionSessionId, String paramString, List<AppTargetId> paramList);
  
  public abstract void onRequestPredictionUpdate(AppPredictionSessionId paramAppPredictionSessionId);
  
  public abstract void onSortAppTargets(AppPredictionSessionId paramAppPredictionSessionId, List<AppTarget> paramList, CancellationSignal paramCancellationSignal, Consumer<List<AppTarget>> paramConsumer);
  
  class CallbackWrapper implements Consumer<List<AppTarget>>, IBinder.DeathRecipient {
    private IPredictionCallback mCallback;
    
    private final Consumer<CallbackWrapper> mOnBinderDied;
    
    CallbackWrapper(AppPredictionService this$0, Consumer<CallbackWrapper> param1Consumer) {
      this.mCallback = (IPredictionCallback)this$0;
      this.mOnBinderDied = param1Consumer;
      try {
        this$0.asBinder().linkToDeath(this, 0);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to link to death: ");
        stringBuilder.append(remoteException);
        Slog.e("AppPredictionService", stringBuilder.toString());
      } 
    }
    
    public boolean isCallback(IPredictionCallback param1IPredictionCallback) {
      IPredictionCallback iPredictionCallback = this.mCallback;
      if (iPredictionCallback == null) {
        Slog.e("AppPredictionService", "Callback is null, likely the binder has died.");
        return false;
      } 
      return iPredictionCallback.equals(param1IPredictionCallback);
    }
    
    public void accept(List<AppTarget> param1List) {
      try {
        if (this.mCallback != null) {
          IPredictionCallback iPredictionCallback = this.mCallback;
          ParceledListSlice parceledListSlice = new ParceledListSlice();
          this(param1List);
          iPredictionCallback.onResult(parceledListSlice);
        } 
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Error sending result:");
        stringBuilder.append(remoteException);
        Slog.e("AppPredictionService", stringBuilder.toString());
      } 
    }
    
    public void binderDied() {
      this.mCallback = null;
      Consumer<CallbackWrapper> consumer = this.mOnBinderDied;
      if (consumer != null)
        consumer.accept(this); 
    }
  }
}
