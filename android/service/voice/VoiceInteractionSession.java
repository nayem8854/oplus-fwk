package android.service.voice;

import android.R;
import android.app.Dialog;
import android.app.DirectAction;
import android.app.Instrumentation;
import android.app.VoiceInteractor;
import android.app.assist.AssistContent;
import android.app.assist.AssistStructure;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ParceledListSlice;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.Region;
import android.inputmethodservice.SoftInputWindow;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.IBinder;
import android.os.ICancellationSignal;
import android.os.Message;
import android.os.RemoteCallback;
import android.os.RemoteException;
import android.os.UserHandle;
import android.util.ArrayMap;
import android.util.DebugUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import com.android.internal.app.IVoiceInteractionManagerService;
import com.android.internal.app.IVoiceInteractionSessionShowCallback;
import com.android.internal.app.IVoiceInteractor;
import com.android.internal.app.IVoiceInteractorCallback;
import com.android.internal.app.IVoiceInteractorRequest;
import com.android.internal.os.HandlerCaller;
import com.android.internal.os.SomeArgs;
import com.android.internal.util.Preconditions;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.function.Consumer;

public class VoiceInteractionSession implements KeyEvent.Callback, ComponentCallbacks2 {
  final KeyEvent.DispatcherState mDispatcherState = new KeyEvent.DispatcherState();
  
  int mTheme = 0;
  
  boolean mUiEnabled = true;
  
  final ArrayMap<IBinder, Request> mActiveRequests = new ArrayMap();
  
  final Insets mTmpInsets = new Insets();
  
  final WeakReference<VoiceInteractionSession> mWeakRef = new WeakReference<>(this);
  
  final Map<SafeResultListener, Consumer<Bundle>> mRemoteCallbacks = (Map<SafeResultListener, Consumer<Bundle>>)new ArrayMap();
  
  final IVoiceInteractor mInteractor = (IVoiceInteractor)new Object(this);
  
  final IVoiceInteractionSession mSession = (IVoiceInteractionSession)new Object(this);
  
  class Request {
    final IVoiceInteractorCallback mCallback;
    
    final String mCallingPackage;
    
    final int mCallingUid;
    
    final Bundle mExtras;
    
    final IVoiceInteractorRequest mInterface = (IVoiceInteractorRequest)new Object(this);
    
    final WeakReference<VoiceInteractionSession> mSession;
    
    Request(VoiceInteractionSession this$0, int param1Int, IVoiceInteractorCallback param1IVoiceInteractorCallback, VoiceInteractionSession param1VoiceInteractionSession, Bundle param1Bundle) {
      this.mCallingPackage = (String)this$0;
      this.mCallingUid = param1Int;
      this.mCallback = param1IVoiceInteractorCallback;
      this.mSession = param1VoiceInteractionSession.mWeakRef;
      this.mExtras = param1Bundle;
    }
    
    public int getCallingUid() {
      return this.mCallingUid;
    }
    
    public String getCallingPackage() {
      return this.mCallingPackage;
    }
    
    public Bundle getExtras() {
      return this.mExtras;
    }
    
    public boolean isActive() {
      VoiceInteractionSession voiceInteractionSession = this.mSession.get();
      if (voiceInteractionSession == null)
        return false; 
      return voiceInteractionSession.isRequestActive(this.mInterface.asBinder());
    }
    
    void finishRequest() {
      VoiceInteractionSession voiceInteractionSession = this.mSession.get();
      if (voiceInteractionSession != null) {
        Request request = voiceInteractionSession.removeRequest(this.mInterface.asBinder());
        if (request != null) {
          if (request == this)
            return; 
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Current active request ");
          stringBuilder1.append(request);
          stringBuilder1.append(" not same as calling request ");
          stringBuilder1.append(this);
          throw new IllegalStateException(stringBuilder1.toString());
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Request not active: ");
        stringBuilder.append(this);
        throw new IllegalStateException(stringBuilder.toString());
      } 
      throw new IllegalStateException("VoiceInteractionSession has been destroyed");
    }
    
    public void cancel() {
      try {
        finishRequest();
        this.mCallback.deliverCancel(this.mInterface);
      } catch (RemoteException remoteException) {}
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder(128);
      DebugUtils.buildShortClassTag(this, stringBuilder);
      stringBuilder.append(" ");
      stringBuilder.append(this.mInterface.asBinder());
      stringBuilder.append(" pkg=");
      stringBuilder.append(this.mCallingPackage);
      stringBuilder.append(" uid=");
      UserHandle.formatUid(stringBuilder, this.mCallingUid);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
    
    void dump(String param1String, FileDescriptor param1FileDescriptor, PrintWriter param1PrintWriter, String[] param1ArrayOfString) {
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mInterface=");
      param1PrintWriter.println(this.mInterface.asBinder());
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mCallingPackage=");
      param1PrintWriter.print(this.mCallingPackage);
      param1PrintWriter.print(" mCallingUid=");
      UserHandle.formatUid(param1PrintWriter, this.mCallingUid);
      param1PrintWriter.println();
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mCallback=");
      param1PrintWriter.println(this.mCallback.asBinder());
      if (this.mExtras != null) {
        param1PrintWriter.print(param1String);
        param1PrintWriter.print("mExtras=");
        param1PrintWriter.println(this.mExtras);
      } 
    }
  }
  
  class ConfirmationRequest extends Request {
    final VoiceInteractor.Prompt mPrompt;
    
    ConfirmationRequest(VoiceInteractionSession this$0, int param1Int, IVoiceInteractorCallback param1IVoiceInteractorCallback, VoiceInteractionSession param1VoiceInteractionSession, VoiceInteractor.Prompt param1Prompt, Bundle param1Bundle) {
      super((String)this$0, param1Int, param1IVoiceInteractorCallback, param1VoiceInteractionSession, param1Bundle);
      this.mPrompt = param1Prompt;
    }
    
    public VoiceInteractor.Prompt getVoicePrompt() {
      return this.mPrompt;
    }
    
    @Deprecated
    public CharSequence getPrompt() {
      VoiceInteractor.Prompt prompt = this.mPrompt;
      if (prompt != null) {
        CharSequence charSequence = prompt.getVoicePromptAt(0);
      } else {
        prompt = null;
      } 
      return (CharSequence)prompt;
    }
    
    public void sendConfirmationResult(boolean param1Boolean, Bundle param1Bundle) {
      try {
        finishRequest();
        this.mCallback.deliverConfirmationResult(this.mInterface, param1Boolean, param1Bundle);
      } catch (RemoteException remoteException) {}
    }
    
    void dump(String param1String, FileDescriptor param1FileDescriptor, PrintWriter param1PrintWriter, String[] param1ArrayOfString) {
      super.dump(param1String, param1FileDescriptor, param1PrintWriter, param1ArrayOfString);
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mPrompt=");
      param1PrintWriter.println(this.mPrompt);
    }
  }
  
  class PickOptionRequest extends Request {
    final VoiceInteractor.PickOptionRequest.Option[] mOptions;
    
    final VoiceInteractor.Prompt mPrompt;
    
    PickOptionRequest(VoiceInteractionSession this$0, int param1Int, IVoiceInteractorCallback param1IVoiceInteractorCallback, VoiceInteractionSession param1VoiceInteractionSession, VoiceInteractor.Prompt param1Prompt, VoiceInteractor.PickOptionRequest.Option[] param1ArrayOfOption, Bundle param1Bundle) {
      super((String)this$0, param1Int, param1IVoiceInteractorCallback, param1VoiceInteractionSession, param1Bundle);
      this.mPrompt = param1Prompt;
      this.mOptions = param1ArrayOfOption;
    }
    
    public VoiceInteractor.Prompt getVoicePrompt() {
      return this.mPrompt;
    }
    
    @Deprecated
    public CharSequence getPrompt() {
      VoiceInteractor.Prompt prompt = this.mPrompt;
      if (prompt != null) {
        CharSequence charSequence = prompt.getVoicePromptAt(0);
      } else {
        prompt = null;
      } 
      return (CharSequence)prompt;
    }
    
    public VoiceInteractor.PickOptionRequest.Option[] getOptions() {
      return this.mOptions;
    }
    
    void sendPickOptionResult(boolean param1Boolean, VoiceInteractor.PickOptionRequest.Option[] param1ArrayOfOption, Bundle param1Bundle) {
      if (param1Boolean) {
        try {
          finishRequest();
          this.mCallback.deliverPickOptionResult(this.mInterface, param1Boolean, param1ArrayOfOption, param1Bundle);
        } catch (RemoteException remoteException) {}
        return;
      } 
      this.mCallback.deliverPickOptionResult(this.mInterface, param1Boolean, (VoiceInteractor.PickOptionRequest.Option[])remoteException, param1Bundle);
    }
    
    public void sendIntermediatePickOptionResult(VoiceInteractor.PickOptionRequest.Option[] param1ArrayOfOption, Bundle param1Bundle) {
      sendPickOptionResult(false, param1ArrayOfOption, param1Bundle);
    }
    
    public void sendPickOptionResult(VoiceInteractor.PickOptionRequest.Option[] param1ArrayOfOption, Bundle param1Bundle) {
      sendPickOptionResult(true, param1ArrayOfOption, param1Bundle);
    }
    
    void dump(String param1String, FileDescriptor param1FileDescriptor, PrintWriter param1PrintWriter, String[] param1ArrayOfString) {
      super.dump(param1String, param1FileDescriptor, param1PrintWriter, param1ArrayOfString);
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mPrompt=");
      param1PrintWriter.println(this.mPrompt);
      if (this.mOptions != null) {
        param1PrintWriter.print(param1String);
        param1PrintWriter.println("Options:");
        byte b = 0;
        while (true) {
          VoiceInteractor.PickOptionRequest.Option[] arrayOfOption = this.mOptions;
          if (b < arrayOfOption.length) {
            VoiceInteractor.PickOptionRequest.Option option = arrayOfOption[b];
            param1PrintWriter.print(param1String);
            param1PrintWriter.print("  #");
            param1PrintWriter.print(b);
            param1PrintWriter.println(":");
            param1PrintWriter.print(param1String);
            param1PrintWriter.print("    mLabel=");
            param1PrintWriter.println(option.getLabel());
            param1PrintWriter.print(param1String);
            param1PrintWriter.print("    mIndex=");
            param1PrintWriter.println(option.getIndex());
            if (option.countSynonyms() > 0) {
              param1PrintWriter.print(param1String);
              param1PrintWriter.println("    Synonyms:");
              for (byte b1 = 0; b1 < option.countSynonyms(); b1++) {
                param1PrintWriter.print(param1String);
                param1PrintWriter.print("      #");
                param1PrintWriter.print(b1);
                param1PrintWriter.print(": ");
                param1PrintWriter.println(option.getSynonymAt(b1));
              } 
            } 
            if (option.getExtras() != null) {
              param1PrintWriter.print(param1String);
              param1PrintWriter.print("    mExtras=");
              param1PrintWriter.println(option.getExtras());
            } 
            b++;
            continue;
          } 
          break;
        } 
      } 
    }
  }
  
  class CompleteVoiceRequest extends Request {
    final VoiceInteractor.Prompt mPrompt;
    
    CompleteVoiceRequest(VoiceInteractionSession this$0, int param1Int, IVoiceInteractorCallback param1IVoiceInteractorCallback, VoiceInteractionSession param1VoiceInteractionSession, VoiceInteractor.Prompt param1Prompt, Bundle param1Bundle) {
      super((String)this$0, param1Int, param1IVoiceInteractorCallback, param1VoiceInteractionSession, param1Bundle);
      this.mPrompt = param1Prompt;
    }
    
    public VoiceInteractor.Prompt getVoicePrompt() {
      return this.mPrompt;
    }
    
    @Deprecated
    public CharSequence getMessage() {
      VoiceInteractor.Prompt prompt = this.mPrompt;
      if (prompt != null) {
        CharSequence charSequence = prompt.getVoicePromptAt(0);
      } else {
        prompt = null;
      } 
      return (CharSequence)prompt;
    }
    
    public void sendCompleteResult(Bundle param1Bundle) {
      try {
        finishRequest();
        this.mCallback.deliverCompleteVoiceResult(this.mInterface, param1Bundle);
      } catch (RemoteException remoteException) {}
    }
    
    void dump(String param1String, FileDescriptor param1FileDescriptor, PrintWriter param1PrintWriter, String[] param1ArrayOfString) {
      super.dump(param1String, param1FileDescriptor, param1PrintWriter, param1ArrayOfString);
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mPrompt=");
      param1PrintWriter.println(this.mPrompt);
    }
  }
  
  class AbortVoiceRequest extends Request {
    final VoiceInteractor.Prompt mPrompt;
    
    AbortVoiceRequest(VoiceInteractionSession this$0, int param1Int, IVoiceInteractorCallback param1IVoiceInteractorCallback, VoiceInteractionSession param1VoiceInteractionSession, VoiceInteractor.Prompt param1Prompt, Bundle param1Bundle) {
      super((String)this$0, param1Int, param1IVoiceInteractorCallback, param1VoiceInteractionSession, param1Bundle);
      this.mPrompt = param1Prompt;
    }
    
    public VoiceInteractor.Prompt getVoicePrompt() {
      return this.mPrompt;
    }
    
    @Deprecated
    public CharSequence getMessage() {
      VoiceInteractor.Prompt prompt = this.mPrompt;
      if (prompt != null) {
        CharSequence charSequence = prompt.getVoicePromptAt(0);
      } else {
        prompt = null;
      } 
      return (CharSequence)prompt;
    }
    
    public void sendAbortResult(Bundle param1Bundle) {
      try {
        finishRequest();
        this.mCallback.deliverAbortVoiceResult(this.mInterface, param1Bundle);
      } catch (RemoteException remoteException) {}
    }
    
    void dump(String param1String, FileDescriptor param1FileDescriptor, PrintWriter param1PrintWriter, String[] param1ArrayOfString) {
      super.dump(param1String, param1FileDescriptor, param1PrintWriter, param1ArrayOfString);
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mPrompt=");
      param1PrintWriter.println(this.mPrompt);
    }
  }
  
  class CommandRequest extends Request {
    final String mCommand;
    
    CommandRequest(VoiceInteractionSession this$0, int param1Int, IVoiceInteractorCallback param1IVoiceInteractorCallback, VoiceInteractionSession param1VoiceInteractionSession, String param1String1, Bundle param1Bundle) {
      super((String)this$0, param1Int, param1IVoiceInteractorCallback, param1VoiceInteractionSession, param1Bundle);
      this.mCommand = param1String1;
    }
    
    public String getCommand() {
      return this.mCommand;
    }
    
    void sendCommandResult(boolean param1Boolean, Bundle param1Bundle) {
      if (param1Boolean) {
        try {
          finishRequest();
          this.mCallback.deliverCommandResult(this.mInterface, param1Boolean, param1Bundle);
        } catch (RemoteException remoteException) {}
        return;
      } 
      this.mCallback.deliverCommandResult(this.mInterface, param1Boolean, (Bundle)remoteException);
    }
    
    public void sendIntermediateResult(Bundle param1Bundle) {
      sendCommandResult(false, param1Bundle);
    }
    
    public void sendResult(Bundle param1Bundle) {
      sendCommandResult(true, param1Bundle);
    }
    
    void dump(String param1String, FileDescriptor param1FileDescriptor, PrintWriter param1PrintWriter, String[] param1ArrayOfString) {
      super.dump(param1String, param1FileDescriptor, param1PrintWriter, param1ArrayOfString);
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mCommand=");
      param1PrintWriter.println(this.mCommand);
    }
  }
  
  class MyCallbacks implements HandlerCaller.Callback, SoftInputWindow.Callback {
    final VoiceInteractionSession this$0;
    
    public void executeMessage(Message param1Message) {
      SomeArgs someArgs1, someArgs2;
      Message message = null;
      int i = param1Message.what;
      switch (i) {
        default:
          switch (i) {
            default:
              param1Message = message;
              break;
            case 108:
              VoiceInteractionSession.this.onLockscreenShown();
              param1Message = message;
              break;
            case 107:
              VoiceInteractionSession.this.doHide();
              param1Message = message;
              break;
            case 106:
              someArgs2 = (SomeArgs)param1Message.obj;
              VoiceInteractionSession.this.doShow((Bundle)someArgs2.arg1, param1Message.arg1, (IVoiceInteractionSessionShowCallback)someArgs2.arg2);
              someArgs1 = someArgs2;
              break;
            case 105:
              VoiceInteractionSession.this.onHandleScreenshot((Bitmap)((Message)someArgs1).obj);
              someArgs1 = someArgs2;
              break;
            case 104:
              someArgs1 = (SomeArgs)((Message)someArgs1).obj;
              VoiceInteractionSession.this.doOnHandleAssist(someArgs1.argi1, (IBinder)someArgs1.arg5, (Bundle)someArgs1.arg1, (AssistStructure)someArgs1.arg2, (Throwable)someArgs1.arg3, (AssistContent)someArgs1.arg4, someArgs1.argi5, someArgs1.argi6);
              break;
            case 103:
              VoiceInteractionSession.this.doDestroy();
              someArgs1 = someArgs2;
              break;
            case 102:
              VoiceInteractionSession.this.onCloseSystemDialogs();
              someArgs1 = someArgs2;
              break;
            case 101:
              VoiceInteractionSession.this.onTaskFinished((Intent)((Message)someArgs1).obj, ((Message)someArgs1).arg1);
              someArgs1 = someArgs2;
              break;
            case 100:
              break;
          } 
          VoiceInteractionSession.this.onTaskStarted((Intent)((Message)someArgs1).obj, ((Message)someArgs1).arg1);
          someArgs1 = someArgs2;
          break;
        case 7:
          VoiceInteractionSession.this.onCancelRequest((VoiceInteractionSession.Request)((Message)someArgs1).obj);
          someArgs1 = someArgs2;
          break;
        case 6:
          someArgs1 = (SomeArgs)((Message)someArgs1).obj;
          someArgs1.arg1 = VoiceInteractionSession.this.onGetSupportedCommands((String[])someArgs1.arg1);
          someArgs1.complete();
          someArgs1 = null;
          break;
        case 5:
          VoiceInteractionSession.this.onRequestCommand((VoiceInteractionSession.CommandRequest)((Message)someArgs1).obj);
          someArgs1 = someArgs2;
          break;
        case 4:
          VoiceInteractionSession.this.onRequestAbortVoice((VoiceInteractionSession.AbortVoiceRequest)((Message)someArgs1).obj);
          someArgs1 = someArgs2;
          break;
        case 3:
          VoiceInteractionSession.this.onRequestCompleteVoice((VoiceInteractionSession.CompleteVoiceRequest)((Message)someArgs1).obj);
          someArgs1 = someArgs2;
          break;
        case 2:
          VoiceInteractionSession.this.onRequestPickOption((VoiceInteractionSession.PickOptionRequest)((Message)someArgs1).obj);
          someArgs1 = someArgs2;
          break;
        case 1:
          VoiceInteractionSession.this.onRequestConfirmation((VoiceInteractionSession.ConfirmationRequest)((Message)someArgs1).obj);
          someArgs1 = someArgs2;
          break;
      } 
      if (someArgs1 != null)
        someArgs1.recycle(); 
    }
    
    public void onBackPressed() {
      VoiceInteractionSession.this.onBackPressed();
    }
  }
  
  final MyCallbacks mCallbacks = new MyCallbacks();
  
  class Insets {
    public static final int TOUCHABLE_INSETS_CONTENT = 1;
    
    public static final int TOUCHABLE_INSETS_FRAME = 0;
    
    public static final int TOUCHABLE_INSETS_REGION = 3;
    
    public final Rect contentInsets;
    
    public int touchableInsets;
    
    public final Region touchableRegion;
    
    public Insets() {
      this.contentInsets = new Rect();
      this.touchableRegion = new Region();
    }
  }
  
  final ViewTreeObserver.OnComputeInternalInsetsListener mInsetsComputer = (ViewTreeObserver.OnComputeInternalInsetsListener)new Object(this);
  
  static final boolean DEBUG = false;
  
  static final int MSG_CANCEL = 7;
  
  static final int MSG_CLOSE_SYSTEM_DIALOGS = 102;
  
  static final int MSG_DESTROY = 103;
  
  static final int MSG_HANDLE_ASSIST = 104;
  
  static final int MSG_HANDLE_SCREENSHOT = 105;
  
  static final int MSG_HIDE = 107;
  
  static final int MSG_ON_LOCKSCREEN_SHOWN = 108;
  
  static final int MSG_SHOW = 106;
  
  static final int MSG_START_ABORT_VOICE = 4;
  
  static final int MSG_START_COMMAND = 5;
  
  static final int MSG_START_COMPLETE_VOICE = 3;
  
  static final int MSG_START_CONFIRMATION = 1;
  
  static final int MSG_START_PICK_OPTION = 2;
  
  static final int MSG_SUPPORTS_COMMANDS = 6;
  
  static final int MSG_TASK_FINISHED = 101;
  
  static final int MSG_TASK_STARTED = 100;
  
  public static final int SHOW_SOURCE_ACTIVITY = 16;
  
  public static final int SHOW_SOURCE_APPLICATION = 8;
  
  public static final int SHOW_SOURCE_ASSIST_GESTURE = 4;
  
  public static final int SHOW_SOURCE_AUTOMOTIVE_SYSTEM_UI = 128;
  
  public static final int SHOW_SOURCE_NOTIFICATION = 64;
  
  public static final int SHOW_SOURCE_PUSH_TO_TALK = 32;
  
  public static final int SHOW_WITH_ASSIST = 1;
  
  public static final int SHOW_WITH_SCREENSHOT = 2;
  
  static final String TAG = "VoiceInteractionSession";
  
  FrameLayout mContentFrame;
  
  final Context mContext;
  
  final HandlerCaller mHandlerCaller;
  
  boolean mInShowWindow;
  
  LayoutInflater mInflater;
  
  boolean mInitialized;
  
  ICancellationSignal mKillCallback;
  
  View mRootView;
  
  IVoiceInteractionManagerService mSystemService;
  
  TypedArray mThemeAttrs;
  
  IBinder mToken;
  
  SoftInputWindow mWindow;
  
  boolean mWindowAdded;
  
  boolean mWindowVisible;
  
  boolean mWindowWasVisible;
  
  public VoiceInteractionSession(Context paramContext) {
    this(paramContext, new Handler());
  }
  
  public VoiceInteractionSession(Context paramContext, Handler paramHandler) {
    this.mContext = paramContext;
    this.mHandlerCaller = new HandlerCaller(paramContext, paramHandler.getLooper(), this.mCallbacks, true);
  }
  
  public Context getContext() {
    return this.mContext;
  }
  
  void addRequest(Request paramRequest) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mActiveRequests : Landroid/util/ArrayMap;
    //   6: aload_1
    //   7: getfield mInterface : Lcom/android/internal/app/IVoiceInteractorRequest;
    //   10: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   15: aload_1
    //   16: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   19: pop
    //   20: aload_0
    //   21: monitorexit
    //   22: return
    //   23: astore_1
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_1
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1021	-> 0
    //   #1022	-> 2
    //   #1023	-> 20
    //   #1024	-> 22
    //   #1023	-> 23
    // Exception table:
    //   from	to	target	type
    //   2	20	23	finally
    //   20	22	23	finally
    //   24	26	23	finally
  }
  
  boolean isRequestActive(IBinder paramIBinder) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mActiveRequests : Landroid/util/ArrayMap;
    //   6: aload_1
    //   7: invokevirtual containsKey : (Ljava/lang/Object;)Z
    //   10: istore_2
    //   11: aload_0
    //   12: monitorexit
    //   13: iload_2
    //   14: ireturn
    //   15: astore_1
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_1
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1027	-> 0
    //   #1028	-> 2
    //   #1029	-> 15
    // Exception table:
    //   from	to	target	type
    //   2	13	15	finally
    //   16	18	15	finally
  }
  
  Request removeRequest(IBinder paramIBinder) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mActiveRequests : Landroid/util/ArrayMap;
    //   6: aload_1
    //   7: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   10: checkcast android/service/voice/VoiceInteractionSession$Request
    //   13: astore_1
    //   14: aload_0
    //   15: monitorexit
    //   16: aload_1
    //   17: areturn
    //   18: astore_1
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_1
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1033	-> 0
    //   #1034	-> 2
    //   #1035	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	16	18	finally
    //   19	21	18	finally
  }
  
  void doCreate(IVoiceInteractionManagerService paramIVoiceInteractionManagerService, IBinder paramIBinder) {
    this.mSystemService = paramIVoiceInteractionManagerService;
    this.mToken = paramIBinder;
    onCreate();
  }
  
  void doShow(Bundle paramBundle, int paramInt, IVoiceInteractionSessionShowCallback paramIVoiceInteractionSessionShowCallback) {
    if (this.mInShowWindow) {
      Log.w("VoiceInteractionSession", "Re-entrance in to showWindow");
      return;
    } 
    try {
      this.mInShowWindow = true;
      onPrepareShow(paramBundle, paramInt);
      if (!this.mWindowVisible)
        ensureWindowAdded(); 
      onShow(paramBundle, paramInt);
      if (!this.mWindowVisible) {
        this.mWindowVisible = true;
        if (this.mUiEnabled)
          this.mWindow.show(); 
      } 
      if (paramIVoiceInteractionSessionShowCallback != null)
        if (this.mUiEnabled) {
          this.mRootView.invalidate();
          ViewTreeObserver viewTreeObserver = this.mRootView.getViewTreeObserver();
          Object object = new Object();
          super(this, paramIVoiceInteractionSessionShowCallback);
          viewTreeObserver.addOnPreDrawListener((ViewTreeObserver.OnPreDrawListener)object);
        } else {
          try {
            paramIVoiceInteractionSessionShowCallback.onShown();
          } catch (RemoteException remoteException) {
            Log.w("VoiceInteractionSession", "Error calling onShown", (Throwable)remoteException);
          } 
        }  
      return;
    } finally {
      this.mWindowWasVisible = true;
      this.mInShowWindow = false;
    } 
  }
  
  void doHide() {
    if (this.mWindowVisible) {
      ensureWindowHidden();
      this.mWindowVisible = false;
      onHide();
    } 
  }
  
  void doDestroy() {
    onDestroy();
    ICancellationSignal iCancellationSignal = this.mKillCallback;
    if (iCancellationSignal != null) {
      try {
        iCancellationSignal.cancel();
      } catch (RemoteException remoteException) {}
      this.mKillCallback = null;
    } 
    if (this.mInitialized) {
      this.mRootView.getViewTreeObserver().removeOnComputeInternalInsetsListener(this.mInsetsComputer);
      if (this.mWindowAdded) {
        this.mWindow.dismiss();
        this.mWindowAdded = false;
      } 
      this.mInitialized = false;
    } 
  }
  
  void ensureWindowCreated() {
    if (this.mInitialized)
      return; 
    if (this.mUiEnabled) {
      this.mInitialized = true;
      this.mInflater = (LayoutInflater)this.mContext.getSystemService("layout_inflater");
      SoftInputWindow softInputWindow = new SoftInputWindow(this.mContext, "VoiceInteractionSession", this.mTheme, this.mCallbacks, this, this.mDispatcherState, 2031, 80, true);
      softInputWindow.getWindow().getAttributes().setFitInsetsTypes(0);
      this.mWindow.getWindow().addFlags(16843008);
      this.mThemeAttrs = this.mContext.obtainStyledAttributes(R.styleable.VoiceInteractionSession);
      View view = this.mInflater.inflate(17367348, null);
      view.setSystemUiVisibility(1792);
      this.mWindow.setContentView(this.mRootView);
      this.mRootView.getViewTreeObserver().addOnComputeInternalInsetsListener(this.mInsetsComputer);
      this.mContentFrame = (FrameLayout)this.mRootView.findViewById(16908290);
      this.mWindow.getWindow().setLayout(-1, -1);
      this.mWindow.setToken(this.mToken);
      return;
    } 
    throw new IllegalStateException("setUiEnabled is false");
  }
  
  void ensureWindowAdded() {
    if (this.mUiEnabled && !this.mWindowAdded) {
      this.mWindowAdded = true;
      ensureWindowCreated();
      View view = onCreateContentView();
      if (view != null)
        setContentView(view); 
    } 
  }
  
  void ensureWindowHidden() {
    SoftInputWindow softInputWindow = this.mWindow;
    if (softInputWindow != null)
      softInputWindow.hide(); 
  }
  
  public void setDisabledShowContext(int paramInt) {
    try {
      this.mSystemService.setDisabledShowContext(paramInt);
    } catch (RemoteException remoteException) {}
  }
  
  public int getDisabledShowContext() {
    try {
      return this.mSystemService.getDisabledShowContext();
    } catch (RemoteException remoteException) {
      return 0;
    } 
  }
  
  public int getUserDisabledShowContext() {
    try {
      return this.mSystemService.getUserDisabledShowContext();
    } catch (RemoteException remoteException) {
      return 0;
    } 
  }
  
  public void show(Bundle paramBundle, int paramInt) {
    IBinder iBinder = this.mToken;
    if (iBinder != null) {
      try {
        this.mSystemService.showSessionFromSession(iBinder, paramBundle, paramInt);
      } catch (RemoteException remoteException) {}
      return;
    } 
    throw new IllegalStateException("Can't call before onCreate()");
  }
  
  public void hide() {
    IBinder iBinder = this.mToken;
    if (iBinder != null) {
      try {
        this.mSystemService.hideSessionFromSession(iBinder);
      } catch (RemoteException remoteException) {}
      return;
    } 
    throw new IllegalStateException("Can't call before onCreate()");
  }
  
  public void setUiEnabled(boolean paramBoolean) {
    if (this.mUiEnabled != paramBoolean) {
      this.mUiEnabled = paramBoolean;
      if (this.mWindowVisible)
        if (paramBoolean) {
          ensureWindowAdded();
          this.mWindow.show();
        } else {
          ensureWindowHidden();
        }  
    } 
  }
  
  public void setTheme(int paramInt) {
    if (this.mWindow == null) {
      this.mTheme = paramInt;
      return;
    } 
    throw new IllegalStateException("Must be called before onCreate()");
  }
  
  public void startVoiceActivity(Intent paramIntent) {
    if (this.mToken != null) {
      try {
        paramIntent.migrateExtraStreamToClipData(this.mContext);
        paramIntent.prepareToLeaveProcess(this.mContext);
        IVoiceInteractionManagerService iVoiceInteractionManagerService = this.mSystemService;
        IBinder iBinder = this.mToken;
        Context context1 = this.mContext;
        String str1 = paramIntent.resolveType(context1.getContentResolver());
        Context context2 = this.mContext;
        String str2 = context2.getAttributionTag();
        int i = iVoiceInteractionManagerService.startVoiceActivity(iBinder, paramIntent, str1, str2);
        Instrumentation.checkStartActivityResult(i, paramIntent);
      } catch (RemoteException remoteException) {}
      return;
    } 
    throw new IllegalStateException("Can't call before onCreate()");
  }
  
  public void startAssistantActivity(Intent paramIntent) {
    if (this.mToken != null) {
      try {
        paramIntent.migrateExtraStreamToClipData(this.mContext);
        paramIntent.prepareToLeaveProcess(this.mContext);
        IVoiceInteractionManagerService iVoiceInteractionManagerService = this.mSystemService;
        IBinder iBinder = this.mToken;
        Context context1 = this.mContext;
        String str1 = paramIntent.resolveType(context1.getContentResolver());
        Context context2 = this.mContext;
        String str2 = context2.getAttributionTag();
        int i = iVoiceInteractionManagerService.startAssistantActivity(iBinder, paramIntent, str1, str2);
        Instrumentation.checkStartActivityResult(i, paramIntent);
      } catch (RemoteException remoteException) {}
      return;
    } 
    throw new IllegalStateException("Can't call before onCreate()");
  }
  
  public final void requestDirectActions(ActivityId paramActivityId, CancellationSignal paramCancellationSignal, Executor paramExecutor, Consumer<List<DirectAction>> paramConsumer) {
    Preconditions.checkNotNull(paramActivityId);
    Preconditions.checkNotNull(paramExecutor);
    Preconditions.checkNotNull(paramConsumer);
    if (this.mToken != null) {
      if (paramCancellationSignal != null)
        paramCancellationSignal.throwIfCanceled(); 
      if (paramCancellationSignal != null) {
        RemoteCallback remoteCallback = new RemoteCallback(new _$$Lambda$VoiceInteractionSession$KRmvXWbKzOj6uOiuAkDjhkUvQiw(paramCancellationSignal));
      } else {
        paramCancellationSignal = null;
      } 
      try {
        IVoiceInteractionManagerService iVoiceInteractionManagerService = this.mSystemService;
        IBinder iBinder1 = this.mToken;
        int i = paramActivityId.getTaskId();
        IBinder iBinder2 = paramActivityId.getAssistToken();
        RemoteCallback remoteCallback = new RemoteCallback();
        _$$Lambda$VoiceInteractionSession$ONdRuCs_OqsJCBOvPdgOMEsz684 _$$Lambda$VoiceInteractionSession$ONdRuCs_OqsJCBOvPdgOMEsz684 = new _$$Lambda$VoiceInteractionSession$ONdRuCs_OqsJCBOvPdgOMEsz684();
        this(paramExecutor, paramConsumer);
        this(createSafeResultListener(_$$Lambda$VoiceInteractionSession$ONdRuCs_OqsJCBOvPdgOMEsz684));
        iVoiceInteractionManagerService.requestDirectActions(iBinder1, i, iBinder2, (RemoteCallback)paramCancellationSignal, remoteCallback);
      } catch (RemoteException remoteException) {
        remoteException.rethrowFromSystemServer();
      } 
      return;
    } 
    throw new IllegalStateException("Can't call before onCreate()");
  }
  
  public void onDirectActionsInvalidated(ActivityId paramActivityId) {}
  
  public final void performDirectAction(DirectAction paramDirectAction, Bundle paramBundle, CancellationSignal paramCancellationSignal, Executor paramExecutor, Consumer<Bundle> paramConsumer) {
    if (this.mToken != null) {
      Preconditions.checkNotNull(paramExecutor);
      Preconditions.checkNotNull(paramConsumer);
      if (paramCancellationSignal != null)
        paramCancellationSignal.throwIfCanceled(); 
      if (paramCancellationSignal != null) {
        RemoteCallback remoteCallback1 = new RemoteCallback(createSafeResultListener(new _$$Lambda$VoiceInteractionSession$2YI2merL0_kdgL83g93OW541J8w(paramCancellationSignal)));
      } else {
        paramCancellationSignal = null;
      } 
      RemoteCallback remoteCallback = new RemoteCallback(createSafeResultListener(new _$$Lambda$VoiceInteractionSession$9GV3ALC6LWOMyg5zazTo6TodsHU(paramExecutor, paramConsumer)));
      try {
        IVoiceInteractionManagerService iVoiceInteractionManagerService = this.mSystemService;
        IBinder iBinder2 = this.mToken;
        String str = paramDirectAction.getId();
        int i = paramDirectAction.getTaskId();
        IBinder iBinder1 = paramDirectAction.getActivityId();
        iVoiceInteractionManagerService.performDirectAction(iBinder2, str, paramBundle, i, iBinder1, (RemoteCallback)paramCancellationSignal, remoteCallback);
      } catch (RemoteException remoteException) {
        remoteException.rethrowFromSystemServer();
      } 
      return;
    } 
    throw new IllegalStateException("Can't call before onCreate()");
  }
  
  public void setKeepAwake(boolean paramBoolean) {
    IBinder iBinder = this.mToken;
    if (iBinder != null) {
      try {
        this.mSystemService.setKeepAwake(iBinder, paramBoolean);
      } catch (RemoteException remoteException) {}
      return;
    } 
    throw new IllegalStateException("Can't call before onCreate()");
  }
  
  public void closeSystemDialogs() {
    IBinder iBinder = this.mToken;
    if (iBinder != null) {
      try {
        this.mSystemService.closeSystemDialogs(iBinder);
      } catch (RemoteException remoteException) {}
      return;
    } 
    throw new IllegalStateException("Can't call before onCreate()");
  }
  
  public LayoutInflater getLayoutInflater() {
    ensureWindowCreated();
    return this.mInflater;
  }
  
  public Dialog getWindow() {
    ensureWindowCreated();
    return (Dialog)this.mWindow;
  }
  
  public void finish() {
    IBinder iBinder = this.mToken;
    if (iBinder != null) {
      try {
        this.mSystemService.finish(iBinder);
      } catch (RemoteException remoteException) {}
      return;
    } 
    throw new IllegalStateException("Can't call before onCreate()");
  }
  
  public void onCreate() {
    doOnCreate();
  }
  
  private void doOnCreate() {
    int i = this.mTheme;
    if (i == 0)
      i = 16974854; 
    this.mTheme = i;
  }
  
  public void onPrepareShow(Bundle paramBundle, int paramInt) {}
  
  public void onShow(Bundle paramBundle, int paramInt) {}
  
  public void onHide() {}
  
  public void onDestroy() {}
  
  public View onCreateContentView() {
    return null;
  }
  
  public void setContentView(View paramView) {
    ensureWindowCreated();
    this.mContentFrame.removeAllViews();
    this.mContentFrame.addView(paramView, (ViewGroup.LayoutParams)new FrameLayout.LayoutParams(-1, -1));
    this.mContentFrame.requestApplyInsets();
  }
  
  void doOnHandleAssist(int paramInt1, IBinder paramIBinder, Bundle paramBundle, AssistStructure paramAssistStructure, Throwable paramThrowable, AssistContent paramAssistContent, int paramInt2, int paramInt3) {
    if (paramThrowable != null)
      onAssistStructureFailure(paramThrowable); 
    AssistState assistState = new AssistState(new ActivityId(paramInt1, paramIBinder), paramBundle, paramAssistStructure, paramAssistContent, paramInt2, paramInt3);
    onHandleAssist(assistState);
  }
  
  public void onAssistStructureFailure(Throwable paramThrowable) {}
  
  @Deprecated
  public void onHandleAssist(Bundle paramBundle, AssistStructure paramAssistStructure, AssistContent paramAssistContent) {}
  
  public void onHandleAssist(AssistState paramAssistState) {
    AssistContent assistContent;
    if (paramAssistState.getIndex() == 0) {
      Bundle bundle = paramAssistState.getAssistData();
      AssistStructure assistStructure = paramAssistState.getAssistStructure();
      assistContent = paramAssistState.getAssistContent();
      onHandleAssist(bundle, assistStructure, assistContent);
    } else {
      Bundle bundle = assistContent.getAssistData();
      AssistStructure assistStructure = assistContent.getAssistStructure();
      AssistContent assistContent1 = assistContent.getAssistContent();
      int i = assistContent.getIndex(), j = assistContent.getCount();
      onHandleAssistSecondary(bundle, assistStructure, assistContent1, i, j);
    } 
  }
  
  @Deprecated
  public void onHandleAssistSecondary(Bundle paramBundle, AssistStructure paramAssistStructure, AssistContent paramAssistContent, int paramInt1, int paramInt2) {}
  
  public void onHandleScreenshot(Bitmap paramBitmap) {}
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    return false;
  }
  
  public boolean onKeyLongPress(int paramInt, KeyEvent paramKeyEvent) {
    return false;
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    return false;
  }
  
  public boolean onKeyMultiple(int paramInt1, int paramInt2, KeyEvent paramKeyEvent) {
    return false;
  }
  
  public void onBackPressed() {
    hide();
  }
  
  public void onCloseSystemDialogs() {
    hide();
  }
  
  public void onLockscreenShown() {
    hide();
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration) {}
  
  public void onLowMemory() {}
  
  public void onTrimMemory(int paramInt) {}
  
  public void onComputeInsets(Insets paramInsets) {
    paramInsets.contentInsets.left = 0;
    paramInsets.contentInsets.bottom = 0;
    paramInsets.contentInsets.right = 0;
    View view = getWindow().getWindow().getDecorView();
    paramInsets.contentInsets.top = view.getHeight();
    paramInsets.touchableInsets = 0;
    paramInsets.touchableRegion.setEmpty();
  }
  
  public void onTaskStarted(Intent paramIntent, int paramInt) {}
  
  public void onTaskFinished(Intent paramIntent, int paramInt) {
    hide();
  }
  
  public boolean[] onGetSupportedCommands(String[] paramArrayOfString) {
    return new boolean[paramArrayOfString.length];
  }
  
  public void onRequestConfirmation(ConfirmationRequest paramConfirmationRequest) {}
  
  public void onRequestPickOption(PickOptionRequest paramPickOptionRequest) {}
  
  public void onRequestCompleteVoice(CompleteVoiceRequest paramCompleteVoiceRequest) {}
  
  public void onRequestAbortVoice(AbortVoiceRequest paramAbortVoiceRequest) {}
  
  public void onRequestCommand(CommandRequest paramCommandRequest) {}
  
  public void onCancelRequest(Request paramRequest) {}
  
  public void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mToken=");
    paramPrintWriter.println(this.mToken);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mTheme=#");
    paramPrintWriter.println(Integer.toHexString(this.mTheme));
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mUiEnabled=");
    paramPrintWriter.println(this.mUiEnabled);
    paramPrintWriter.print(" mInitialized=");
    paramPrintWriter.println(this.mInitialized);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mWindowAdded=");
    paramPrintWriter.print(this.mWindowAdded);
    paramPrintWriter.print(" mWindowVisible=");
    paramPrintWriter.println(this.mWindowVisible);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mWindowWasVisible=");
    paramPrintWriter.print(this.mWindowWasVisible);
    paramPrintWriter.print(" mInShowWindow=");
    paramPrintWriter.println(this.mInShowWindow);
    if (this.mActiveRequests.size() > 0) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.println("Active requests:");
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append("    ");
      String str = stringBuilder.toString();
      for (byte b = 0; b < this.mActiveRequests.size(); b++) {
        Request request = (Request)this.mActiveRequests.valueAt(b);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("  #");
        paramPrintWriter.print(b);
        paramPrintWriter.print(": ");
        paramPrintWriter.println(request);
        request.dump(str, paramFileDescriptor, paramPrintWriter, paramArrayOfString);
      } 
    } 
  }
  
  private SafeResultListener createSafeResultListener(Consumer<Bundle> paramConsumer) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new android/service/voice/VoiceInteractionSession$SafeResultListener
    //   5: astore_2
    //   6: aload_2
    //   7: aload_1
    //   8: aload_0
    //   9: invokespecial <init> : (Ljava/util/function/Consumer;Landroid/service/voice/VoiceInteractionSession;)V
    //   12: aload_0
    //   13: getfield mRemoteCallbacks : Ljava/util/Map;
    //   16: aload_2
    //   17: aload_1
    //   18: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   23: pop
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_2
    //   27: areturn
    //   28: astore_1
    //   29: aload_0
    //   30: monitorexit
    //   31: aload_1
    //   32: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1955	-> 0
    //   #1956	-> 2
    //   #1957	-> 12
    //   #1958	-> 24
    //   #1959	-> 28
    // Exception table:
    //   from	to	target	type
    //   2	12	28	finally
    //   12	24	28	finally
    //   24	26	28	finally
    //   29	31	28	finally
  }
  
  private Consumer<Bundle> removeSafeResultListener(SafeResultListener paramSafeResultListener) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mRemoteCallbacks : Ljava/util/Map;
    //   6: aload_1
    //   7: invokeinterface remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   12: checkcast java/util/function/Consumer
    //   15: astore_1
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_1
    //   19: areturn
    //   20: astore_1
    //   21: aload_0
    //   22: monitorexit
    //   23: aload_1
    //   24: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1963	-> 0
    //   #1964	-> 2
    //   #1965	-> 20
    // Exception table:
    //   from	to	target	type
    //   2	18	20	finally
    //   21	23	20	finally
  }
  
  class AssistState {
    private final VoiceInteractionSession.ActivityId mActivityId;
    
    private final AssistContent mContent;
    
    private final int mCount;
    
    private final Bundle mData;
    
    private final int mIndex;
    
    private final AssistStructure mStructure;
    
    AssistState(VoiceInteractionSession this$0, Bundle param1Bundle, AssistStructure param1AssistStructure, AssistContent param1AssistContent, int param1Int1, int param1Int2) {
      this.mActivityId = (VoiceInteractionSession.ActivityId)this$0;
      this.mIndex = param1Int1;
      this.mCount = param1Int2;
      this.mData = param1Bundle;
      this.mStructure = param1AssistStructure;
      this.mContent = param1AssistContent;
    }
    
    public boolean isFocused() {
      boolean bool;
      if (this.mIndex == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int getIndex() {
      return this.mIndex;
    }
    
    public int getCount() {
      return this.mCount;
    }
    
    public VoiceInteractionSession.ActivityId getActivityId() {
      return this.mActivityId;
    }
    
    public Bundle getAssistData() {
      return this.mData;
    }
    
    public AssistStructure getAssistStructure() {
      return this.mStructure;
    }
    
    public AssistContent getAssistContent() {
      return this.mContent;
    }
  }
  
  class ActivityId {
    private final IBinder mAssistToken;
    
    private final int mTaskId;
    
    ActivityId(VoiceInteractionSession this$0, IBinder param1IBinder) {
      this.mTaskId = this$0;
      this.mAssistToken = param1IBinder;
    }
    
    int getTaskId() {
      return this.mTaskId;
    }
    
    IBinder getAssistToken() {
      return this.mAssistToken;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      if (this.mTaskId != ((ActivityId)param1Object).mTaskId)
        return false; 
      IBinder iBinder = this.mAssistToken;
      if (iBinder != null) {
        bool = iBinder.equals(((ActivityId)param1Object).mAssistToken);
      } else if (((ActivityId)param1Object).mAssistToken != null) {
        bool = false;
      } 
      return bool;
    }
    
    public int hashCode() {
      byte b;
      int i = this.mTaskId;
      IBinder iBinder = this.mAssistToken;
      if (iBinder != null) {
        b = iBinder.hashCode();
      } else {
        b = 0;
      } 
      return i * 31 + b;
    }
  }
  
  class SafeResultListener implements RemoteCallback.OnResultListener {
    private final WeakReference<VoiceInteractionSession> mWeakSession;
    
    SafeResultListener(VoiceInteractionSession this$0, VoiceInteractionSession param1VoiceInteractionSession) {
      this.mWeakSession = new WeakReference<>(param1VoiceInteractionSession);
    }
    
    public void onResult(Bundle param1Bundle) {
      VoiceInteractionSession voiceInteractionSession = this.mWeakSession.get();
      if (voiceInteractionSession != null) {
        Consumer<Bundle> consumer = voiceInteractionSession.removeSafeResultListener(this);
        if (consumer != null)
          consumer.accept(param1Bundle); 
      } 
    }
  }
}
