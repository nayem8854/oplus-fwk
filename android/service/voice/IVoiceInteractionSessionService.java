package android.service.voice;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IVoiceInteractionSessionService extends IInterface {
  void newSession(IBinder paramIBinder, Bundle paramBundle, int paramInt) throws RemoteException;
  
  class Default implements IVoiceInteractionSessionService {
    public void newSession(IBinder param1IBinder, Bundle param1Bundle, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVoiceInteractionSessionService {
    private static final String DESCRIPTOR = "android.service.voice.IVoiceInteractionSessionService";
    
    static final int TRANSACTION_newSession = 1;
    
    public Stub() {
      attachInterface(this, "android.service.voice.IVoiceInteractionSessionService");
    }
    
    public static IVoiceInteractionSessionService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.voice.IVoiceInteractionSessionService");
      if (iInterface != null && iInterface instanceof IVoiceInteractionSessionService)
        return (IVoiceInteractionSessionService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "newSession";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.service.voice.IVoiceInteractionSessionService");
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.voice.IVoiceInteractionSessionService");
      IBinder iBinder = param1Parcel1.readStrongBinder();
      if (param1Parcel1.readInt() != 0) {
        Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      param1Int1 = param1Parcel1.readInt();
      newSession(iBinder, (Bundle)param1Parcel2, param1Int1);
      return true;
    }
    
    private static class Proxy implements IVoiceInteractionSessionService {
      public static IVoiceInteractionSessionService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.voice.IVoiceInteractionSessionService";
      }
      
      public void newSession(IBinder param2IBinder, Bundle param2Bundle, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.voice.IVoiceInteractionSessionService");
          parcel.writeStrongBinder(param2IBinder);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IVoiceInteractionSessionService.Stub.getDefaultImpl() != null) {
            IVoiceInteractionSessionService.Stub.getDefaultImpl().newSession(param2IBinder, param2Bundle, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVoiceInteractionSessionService param1IVoiceInteractionSessionService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVoiceInteractionSessionService != null) {
          Proxy.sDefaultImpl = param1IVoiceInteractionSessionService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVoiceInteractionSessionService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
