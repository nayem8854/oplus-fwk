package android.service.voice;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.android.internal.app.IVoiceActionCheckCallback;
import java.util.ArrayList;
import java.util.List;

public interface IVoiceInteractionService extends IInterface {
  void getActiveServiceSupportedActions(List<String> paramList, IVoiceActionCheckCallback paramIVoiceActionCheckCallback) throws RemoteException;
  
  void launchVoiceAssistFromKeyguard() throws RemoteException;
  
  void ready() throws RemoteException;
  
  void shutdown() throws RemoteException;
  
  void soundModelsChanged() throws RemoteException;
  
  class Default implements IVoiceInteractionService {
    public void ready() throws RemoteException {}
    
    public void soundModelsChanged() throws RemoteException {}
    
    public void shutdown() throws RemoteException {}
    
    public void launchVoiceAssistFromKeyguard() throws RemoteException {}
    
    public void getActiveServiceSupportedActions(List<String> param1List, IVoiceActionCheckCallback param1IVoiceActionCheckCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVoiceInteractionService {
    private static final String DESCRIPTOR = "android.service.voice.IVoiceInteractionService";
    
    static final int TRANSACTION_getActiveServiceSupportedActions = 5;
    
    static final int TRANSACTION_launchVoiceAssistFromKeyguard = 4;
    
    static final int TRANSACTION_ready = 1;
    
    static final int TRANSACTION_shutdown = 3;
    
    static final int TRANSACTION_soundModelsChanged = 2;
    
    public Stub() {
      attachInterface(this, "android.service.voice.IVoiceInteractionService");
    }
    
    public static IVoiceInteractionService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.voice.IVoiceInteractionService");
      if (iInterface != null && iInterface instanceof IVoiceInteractionService)
        return (IVoiceInteractionService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "getActiveServiceSupportedActions";
            } 
            return "launchVoiceAssistFromKeyguard";
          } 
          return "shutdown";
        } 
        return "soundModelsChanged";
      } 
      return "ready";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IVoiceActionCheckCallback iVoiceActionCheckCallback;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.service.voice.IVoiceInteractionService");
                return true;
              } 
              param1Parcel1.enforceInterface("android.service.voice.IVoiceInteractionService");
              ArrayList<String> arrayList = param1Parcel1.createStringArrayList();
              iVoiceActionCheckCallback = IVoiceActionCheckCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
              getActiveServiceSupportedActions(arrayList, iVoiceActionCheckCallback);
              return true;
            } 
            iVoiceActionCheckCallback.enforceInterface("android.service.voice.IVoiceInteractionService");
            launchVoiceAssistFromKeyguard();
            return true;
          } 
          iVoiceActionCheckCallback.enforceInterface("android.service.voice.IVoiceInteractionService");
          shutdown();
          return true;
        } 
        iVoiceActionCheckCallback.enforceInterface("android.service.voice.IVoiceInteractionService");
        soundModelsChanged();
        return true;
      } 
      iVoiceActionCheckCallback.enforceInterface("android.service.voice.IVoiceInteractionService");
      ready();
      return true;
    }
    
    private static class Proxy implements IVoiceInteractionService {
      public static IVoiceInteractionService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.voice.IVoiceInteractionService";
      }
      
      public void ready() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.voice.IVoiceInteractionService");
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IVoiceInteractionService.Stub.getDefaultImpl() != null) {
            IVoiceInteractionService.Stub.getDefaultImpl().ready();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void soundModelsChanged() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.voice.IVoiceInteractionService");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IVoiceInteractionService.Stub.getDefaultImpl() != null) {
            IVoiceInteractionService.Stub.getDefaultImpl().soundModelsChanged();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void shutdown() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.voice.IVoiceInteractionService");
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IVoiceInteractionService.Stub.getDefaultImpl() != null) {
            IVoiceInteractionService.Stub.getDefaultImpl().shutdown();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void launchVoiceAssistFromKeyguard() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.voice.IVoiceInteractionService");
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IVoiceInteractionService.Stub.getDefaultImpl() != null) {
            IVoiceInteractionService.Stub.getDefaultImpl().launchVoiceAssistFromKeyguard();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getActiveServiceSupportedActions(List<String> param2List, IVoiceActionCheckCallback param2IVoiceActionCheckCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.voice.IVoiceInteractionService");
          parcel.writeStringList(param2List);
          if (param2IVoiceActionCheckCallback != null) {
            iBinder = param2IVoiceActionCheckCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IVoiceInteractionService.Stub.getDefaultImpl() != null) {
            IVoiceInteractionService.Stub.getDefaultImpl().getActiveServiceSupportedActions(param2List, param2IVoiceActionCheckCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVoiceInteractionService param1IVoiceInteractionService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVoiceInteractionService != null) {
          Proxy.sDefaultImpl = param1IVoiceInteractionService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVoiceInteractionService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
