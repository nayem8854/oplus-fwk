package android.service.voice;

import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.os.ServiceManager;
import com.android.internal.app.IVoiceInteractionManagerService;
import com.android.internal.os.HandlerCaller;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public abstract class VoiceInteractionSessionService extends Service {
  IVoiceInteractionManagerService mSystemService;
  
  VoiceInteractionSession mSession;
  
  IVoiceInteractionSessionService mInterface = new IVoiceInteractionSessionService.Stub() {
      final VoiceInteractionSessionService this$0;
      
      public void newSession(IBinder param1IBinder, Bundle param1Bundle, int param1Int) {
        VoiceInteractionSessionService.this.mHandlerCaller.sendMessage(VoiceInteractionSessionService.this.mHandlerCaller.obtainMessageIOO(1, param1Int, param1IBinder, param1Bundle));
      }
    };
  
  final HandlerCaller.Callback mHandlerCallerCallback = (HandlerCaller.Callback)new Object(this);
  
  HandlerCaller mHandlerCaller;
  
  static final int MSG_NEW_SESSION = 1;
  
  public void onCreate() {
    super.onCreate();
    IBinder iBinder = ServiceManager.getService("voiceinteraction");
    this.mSystemService = IVoiceInteractionManagerService.Stub.asInterface(iBinder);
    this.mHandlerCaller = new HandlerCaller(this, Looper.myLooper(), this.mHandlerCallerCallback, true);
  }
  
  public IBinder onBind(Intent paramIntent) {
    return this.mInterface.asBinder();
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration) {
    super.onConfigurationChanged(paramConfiguration);
    VoiceInteractionSession voiceInteractionSession = this.mSession;
    if (voiceInteractionSession != null)
      voiceInteractionSession.onConfigurationChanged(paramConfiguration); 
  }
  
  public void onLowMemory() {
    super.onLowMemory();
    VoiceInteractionSession voiceInteractionSession = this.mSession;
    if (voiceInteractionSession != null)
      voiceInteractionSession.onLowMemory(); 
  }
  
  public void onTrimMemory(int paramInt) {
    super.onTrimMemory(paramInt);
    VoiceInteractionSession voiceInteractionSession = this.mSession;
    if (voiceInteractionSession != null)
      voiceInteractionSession.onTrimMemory(paramInt); 
  }
  
  protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    if (this.mSession == null) {
      paramPrintWriter.println("(no active session)");
    } else {
      paramPrintWriter.println("VoiceInteractionSession:");
      this.mSession.dump("  ", paramFileDescriptor, paramPrintWriter, paramArrayOfString);
    } 
  }
  
  void doNewSession(IBinder paramIBinder, Bundle paramBundle, int paramInt) {
    VoiceInteractionSession voiceInteractionSession2 = this.mSession;
    if (voiceInteractionSession2 != null) {
      voiceInteractionSession2.doDestroy();
      this.mSession = null;
    } 
    VoiceInteractionSession voiceInteractionSession1 = onNewSession(paramBundle);
    try {
      this.mSystemService.deliverNewSession(paramIBinder, voiceInteractionSession1.mSession, this.mSession.mInteractor);
      this.mSession.doCreate(this.mSystemService, paramIBinder);
    } catch (RemoteException remoteException) {}
  }
  
  public abstract VoiceInteractionSession onNewSession(Bundle paramBundle);
}
