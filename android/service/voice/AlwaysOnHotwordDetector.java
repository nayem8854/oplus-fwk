package android.service.voice;

import android.content.Intent;
import android.hardware.soundtrigger.IRecognitionStatusCallback;
import android.hardware.soundtrigger.KeyphraseEnrollmentInfo;
import android.hardware.soundtrigger.KeyphraseMetadata;
import android.hardware.soundtrigger.SoundTrigger;
import android.media.AudioFormat;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.util.Slog;
import com.android.internal.app.IVoiceInteractionManagerService;
import java.io.PrintWriter;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Locale;

public class AlwaysOnHotwordDetector {
  private final String mText;
  
  private final IVoiceInteractionManagerService mModelManagementService;
  
  private final Object mLock = new Object();
  
  private final Locale mLocale;
  
  private KeyphraseMetadata mKeyphraseMetadata;
  
  private final KeyphraseEnrollmentInfo mKeyphraseEnrollmentInfo;
  
  private final SoundTriggerListener mInternalCallback;
  
  private final Handler mHandler;
  
  private final Callback mExternalCallback;
  
  private int mAvailability = 0;
  
  static final String TAG = "AlwaysOnHotwordDetector";
  
  private static final int STATUS_OK = 0;
  
  private static final int STATUS_ERROR = -2147483648;
  
  private static final int STATE_NOT_READY = 0;
  
  @Deprecated
  public static final int STATE_KEYPHRASE_UNSUPPORTED = -1;
  
  public static final int STATE_KEYPHRASE_UNENROLLED = 1;
  
  public static final int STATE_KEYPHRASE_ENROLLED = 2;
  
  private static final int STATE_INVALID = -3;
  
  public static final int STATE_HARDWARE_UNAVAILABLE = -2;
  
  public static final int RECOGNITION_MODE_VOICE_TRIGGER = 1;
  
  public static final int RECOGNITION_MODE_USER_IDENTIFICATION = 2;
  
  public static final int RECOGNITION_FLAG_NONE = 0;
  
  public static final int RECOGNITION_FLAG_ENABLE_AUDIO_NOISE_SUPPRESSION = 8;
  
  public static final int RECOGNITION_FLAG_ENABLE_AUDIO_ECHO_CANCELLATION = 4;
  
  public static final int RECOGNITION_FLAG_CAPTURE_TRIGGER_AUDIO = 1;
  
  public static final int RECOGNITION_FLAG_ALLOW_MULTIPLE_TRIGGERS = 2;
  
  private static final int MSG_HOTWORD_DETECTED = 2;
  
  private static final int MSG_DETECTION_RESUME = 5;
  
  private static final int MSG_DETECTION_PAUSE = 4;
  
  private static final int MSG_DETECTION_ERROR = 3;
  
  private static final int MSG_AVAILABILITY_CHANGED = 1;
  
  public static final int MODEL_PARAM_THRESHOLD_FACTOR = 0;
  
  static final boolean DBG = false;
  
  public static final int AUDIO_CAPABILITY_NOISE_SUPPRESSION = 2;
  
  public static final int AUDIO_CAPABILITY_ECHO_CANCELLATION = 1;
  
  public static final class ModelParamRange {
    private final SoundTrigger.ModelParamRange mModelParamRange;
    
    ModelParamRange(SoundTrigger.ModelParamRange param1ModelParamRange) {
      this.mModelParamRange = param1ModelParamRange;
    }
    
    public int getStart() {
      return this.mModelParamRange.getStart();
    }
    
    public int getEnd() {
      return this.mModelParamRange.getEnd();
    }
    
    public String toString() {
      return this.mModelParamRange.toString();
    }
    
    public boolean equals(Object param1Object) {
      return this.mModelParamRange.equals(param1Object);
    }
    
    public int hashCode() {
      return this.mModelParamRange.hashCode();
    }
  }
  
  public static class EventPayload {
    private final AudioFormat mAudioFormat;
    
    private final boolean mCaptureAvailable;
    
    private final int mCaptureSession;
    
    private final byte[] mData;
    
    private final boolean mTriggerAvailable;
    
    private EventPayload(boolean param1Boolean1, boolean param1Boolean2, AudioFormat param1AudioFormat, int param1Int, byte[] param1ArrayOfbyte) {
      this.mTriggerAvailable = param1Boolean1;
      this.mCaptureAvailable = param1Boolean2;
      this.mCaptureSession = param1Int;
      this.mAudioFormat = param1AudioFormat;
      this.mData = param1ArrayOfbyte;
    }
    
    public AudioFormat getCaptureAudioFormat() {
      return this.mAudioFormat;
    }
    
    public byte[] getTriggerAudio() {
      if (this.mTriggerAvailable)
        return this.mData; 
      return null;
    }
    
    public Integer getCaptureSession() {
      if (this.mCaptureAvailable)
        return Integer.valueOf(this.mCaptureSession); 
      return null;
    }
  }
  
  public static abstract class Callback {
    public abstract void onAvailabilityChanged(int param1Int);
    
    public abstract void onDetected(AlwaysOnHotwordDetector.EventPayload param1EventPayload);
    
    public abstract void onError();
    
    public abstract void onRecognitionPaused();
    
    public abstract void onRecognitionResumed();
  }
  
  public AlwaysOnHotwordDetector(String paramString, Locale paramLocale, Callback paramCallback, KeyphraseEnrollmentInfo paramKeyphraseEnrollmentInfo, IVoiceInteractionManagerService paramIVoiceInteractionManagerService) {
    this.mText = paramString;
    this.mLocale = paramLocale;
    this.mKeyphraseEnrollmentInfo = paramKeyphraseEnrollmentInfo;
    this.mExternalCallback = paramCallback;
    this.mHandler = new MyHandler();
    this.mInternalCallback = new SoundTriggerListener(this.mHandler);
    this.mModelManagementService = paramIVoiceInteractionManagerService;
    (new RefreshAvailabiltyTask()).execute((Object[])new Void[0]);
  }
  
  public int getSupportedRecognitionModes() {
    synchronized (this.mLock) {
      return getSupportedRecognitionModesLocked();
    } 
  }
  
  private int getSupportedRecognitionModesLocked() {
    int i = this.mAvailability;
    if (i != -3) {
      if (i == 2) {
        KeyphraseMetadata keyphraseMetadata = this.mKeyphraseMetadata;
        if (keyphraseMetadata != null)
          return keyphraseMetadata.getRecognitionModeFlags(); 
      } 
      throw new UnsupportedOperationException("Getting supported recognition modes for the keyphrase is not supported");
    } 
    throw new IllegalStateException("getSupportedRecognitionModes called on an invalid detector");
  }
  
  public int getSupportedAudioCapabilities() {
    synchronized (this.mLock) {
      return getSupportedAudioCapabilitiesLocked();
    } 
  }
  
  private int getSupportedAudioCapabilitiesLocked() {
    try {
      IVoiceInteractionManagerService iVoiceInteractionManagerService = this.mModelManagementService;
      SoundTrigger.ModuleProperties moduleProperties = iVoiceInteractionManagerService.getDspModuleProperties();
      if (moduleProperties != null)
        return moduleProperties.getAudioCapabilities(); 
      return 0;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean startRecognition(int paramInt) {
    synchronized (this.mLock) {
      if (this.mAvailability != -3) {
        if (this.mAvailability == 2) {
          boolean bool;
          if (startRecognitionLocked(paramInt) == 0) {
            bool = true;
          } else {
            bool = false;
          } 
          return bool;
        } 
        UnsupportedOperationException unsupportedOperationException = new UnsupportedOperationException();
        this("Recognition for the given keyphrase is not supported");
        throw unsupportedOperationException;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("startRecognition called on an invalid detector");
      throw illegalStateException;
    } 
  }
  
  public boolean stopRecognition() {
    synchronized (this.mLock) {
      if (this.mAvailability != -3) {
        if (this.mAvailability == 2) {
          boolean bool;
          if (stopRecognitionLocked() == 0) {
            bool = true;
          } else {
            bool = false;
          } 
          return bool;
        } 
        UnsupportedOperationException unsupportedOperationException = new UnsupportedOperationException();
        this("Recognition for the given keyphrase is not supported");
        throw unsupportedOperationException;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("stopRecognition called on an invalid detector");
      throw illegalStateException;
    } 
  }
  
  public int setParameter(int paramInt1, int paramInt2) {
    synchronized (this.mLock) {
      if (this.mAvailability != -3) {
        paramInt1 = setParameterLocked(paramInt1, paramInt2);
        return paramInt1;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("setParameter called on an invalid detector");
      throw illegalStateException;
    } 
  }
  
  public int getParameter(int paramInt) {
    synchronized (this.mLock) {
      if (this.mAvailability != -3) {
        paramInt = getParameterLocked(paramInt);
        return paramInt;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("getParameter called on an invalid detector");
      throw illegalStateException;
    } 
  }
  
  public ModelParamRange queryParameter(int paramInt) {
    synchronized (this.mLock) {
      if (this.mAvailability != -3)
        return queryParameterLocked(paramInt); 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("queryParameter called on an invalid detector");
      throw illegalStateException;
    } 
  }
  
  public Intent createEnrollIntent() {
    synchronized (this.mLock) {
      return getManageIntentLocked(0);
    } 
  }
  
  public Intent createUnEnrollIntent() {
    synchronized (this.mLock) {
      return getManageIntentLocked(2);
    } 
  }
  
  public Intent createReEnrollIntent() {
    synchronized (this.mLock) {
      return getManageIntentLocked(1);
    } 
  }
  
  private Intent getManageIntentLocked(int paramInt) {
    int i = this.mAvailability;
    if (i != -3) {
      if (i == 2 || i == 1)
        return this.mKeyphraseEnrollmentInfo.getManageKeyphraseIntent(paramInt, this.mText, this.mLocale); 
      throw new UnsupportedOperationException("Managing the given keyphrase is not supported");
    } 
    throw new IllegalStateException("getManageIntent called on an invalid detector");
  }
  
  void invalidate() {
    synchronized (this.mLock) {
      this.mAvailability = -3;
      notifyStateChangedLocked();
      return;
    } 
  }
  
  void onSoundModelsChanged() {
    synchronized (this.mLock) {
      if (this.mAvailability == -3 || this.mAvailability == -2) {
        Slog.w("AlwaysOnHotwordDetector", "Received onSoundModelsChanged for an unsupported keyphrase/config");
        return;
      } 
      if (this.mAvailability == 2)
        stopRecognitionLocked(); 
      RefreshAvailabiltyTask refreshAvailabiltyTask = new RefreshAvailabiltyTask();
      this(this);
      refreshAvailabiltyTask.execute((Object[])new Void[0]);
      return;
    } 
  }
  
  private int startRecognitionLocked(int paramInt) {
    boolean bool1, bool2;
    int i = this.mKeyphraseMetadata.getId();
    KeyphraseMetadata keyphraseMetadata = this.mKeyphraseMetadata;
    SoundTrigger.KeyphraseRecognitionExtra keyphraseRecognitionExtra = new SoundTrigger.KeyphraseRecognitionExtra(i, keyphraseMetadata.getRecognitionModeFlags(), 0, new SoundTrigger.ConfidenceLevel[0]);
    if ((paramInt & 0x1) != 0) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    if ((paramInt & 0x2) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    i = 0;
    if ((paramInt & 0x4) != 0)
      i = false | true; 
    int j = i;
    if ((paramInt & 0x8) != 0)
      j = i | 0x2; 
    try {
      IVoiceInteractionManagerService iVoiceInteractionManagerService = this.mModelManagementService;
      KeyphraseMetadata keyphraseMetadata1 = this.mKeyphraseMetadata;
      paramInt = keyphraseMetadata1.getId();
      String str = this.mLocale.toLanguageTag();
      SoundTriggerListener soundTriggerListener = this.mInternalCallback;
      SoundTrigger.RecognitionConfig recognitionConfig = new SoundTrigger.RecognitionConfig();
      this(bool1, bool2, new SoundTrigger.KeyphraseRecognitionExtra[] { keyphraseRecognitionExtra }, null, j);
      paramInt = iVoiceInteractionManagerService.startRecognition(paramInt, str, (IRecognitionStatusCallback)soundTriggerListener, recognitionConfig);
      if (paramInt != 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("startRecognition() failed with error code ");
        stringBuilder.append(paramInt);
        Slog.w("AlwaysOnHotwordDetector", stringBuilder.toString());
      } 
      return paramInt;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private int stopRecognitionLocked() {
    try {
      int i = this.mModelManagementService.stopRecognition(this.mKeyphraseMetadata.getId(), (IRecognitionStatusCallback)this.mInternalCallback);
      if (i != 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("stopRecognition() failed with error code ");
        stringBuilder.append(i);
        Slog.w("AlwaysOnHotwordDetector", stringBuilder.toString());
      } 
      return i;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private int setParameterLocked(int paramInt1, int paramInt2) {
    try {
      paramInt1 = this.mModelManagementService.setParameter(this.mKeyphraseMetadata.getId(), paramInt1, paramInt2);
      if (paramInt1 != 0) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("setParameter failed with error code ");
        stringBuilder.append(paramInt1);
        Slog.w("AlwaysOnHotwordDetector", stringBuilder.toString());
      } 
      return paramInt1;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private int getParameterLocked(int paramInt) {
    try {
      return this.mModelManagementService.getParameter(this.mKeyphraseMetadata.getId(), paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private ModelParamRange queryParameterLocked(int paramInt) {
    try {
      IVoiceInteractionManagerService iVoiceInteractionManagerService = this.mModelManagementService;
      KeyphraseMetadata keyphraseMetadata = this.mKeyphraseMetadata;
      SoundTrigger.ModelParamRange modelParamRange = iVoiceInteractionManagerService.queryParameter(keyphraseMetadata.getId(), paramInt);
      if (modelParamRange == null)
        return null; 
      return new ModelParamRange(modelParamRange);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private void notifyStateChangedLocked() {
    Message message = Message.obtain(this.mHandler, 1);
    message.arg1 = this.mAvailability;
    message.sendToTarget();
  }
  
  class SoundTriggerListener extends IRecognitionStatusCallback.Stub {
    private final Handler mHandler;
    
    public SoundTriggerListener(AlwaysOnHotwordDetector this$0) {
      this.mHandler = (Handler)this$0;
    }
    
    public void onKeyphraseDetected(SoundTrigger.KeyphraseRecognitionEvent param1KeyphraseRecognitionEvent) {
      Slog.i("AlwaysOnHotwordDetector", "onDetected");
      Message message = Message.obtain(this.mHandler, 2, new AlwaysOnHotwordDetector.EventPayload(param1KeyphraseRecognitionEvent.triggerInData, param1KeyphraseRecognitionEvent.captureAvailable, param1KeyphraseRecognitionEvent.captureFormat, param1KeyphraseRecognitionEvent.captureSession, param1KeyphraseRecognitionEvent.data));
      message.sendToTarget();
    }
    
    public void onGenericSoundTriggerDetected(SoundTrigger.GenericRecognitionEvent param1GenericRecognitionEvent) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Generic sound trigger event detected at AOHD: ");
      stringBuilder.append(param1GenericRecognitionEvent);
      Slog.w("AlwaysOnHotwordDetector", stringBuilder.toString());
    }
    
    public void onError(int param1Int) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onError: ");
      stringBuilder.append(param1Int);
      Slog.i("AlwaysOnHotwordDetector", stringBuilder.toString());
      this.mHandler.sendEmptyMessage(3);
    }
    
    public void onRecognitionPaused() {
      Slog.i("AlwaysOnHotwordDetector", "onRecognitionPaused");
      this.mHandler.sendEmptyMessage(4);
    }
    
    public void onRecognitionResumed() {
      Slog.i("AlwaysOnHotwordDetector", "onRecognitionResumed");
      this.mHandler.sendEmptyMessage(5);
    }
  }
  
  class MyHandler extends Handler {
    final AlwaysOnHotwordDetector this$0;
    
    public void handleMessage(Message param1Message) {
      synchronized (AlwaysOnHotwordDetector.this.mLock) {
        if (AlwaysOnHotwordDetector.this.mAvailability == -3) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Received message: ");
          stringBuilder.append(param1Message.what);
          stringBuilder.append(" for an invalid detector");
          Slog.w("AlwaysOnHotwordDetector", stringBuilder.toString());
          return;
        } 
        int i = param1Message.what;
        if (i != 1) {
          if (i != 2) {
            if (i != 3) {
              if (i != 4) {
                if (i != 5) {
                  super.handleMessage(param1Message);
                } else {
                  AlwaysOnHotwordDetector.this.mExternalCallback.onRecognitionResumed();
                } 
              } else {
                AlwaysOnHotwordDetector.this.mExternalCallback.onRecognitionPaused();
              } 
            } else {
              AlwaysOnHotwordDetector.this.mExternalCallback.onError();
            } 
          } else {
            AlwaysOnHotwordDetector.this.mExternalCallback.onDetected((AlwaysOnHotwordDetector.EventPayload)param1Message.obj);
          } 
        } else {
          AlwaysOnHotwordDetector.this.mExternalCallback.onAvailabilityChanged(param1Message.arg1);
        } 
        return;
      } 
    }
  }
  
  class RefreshAvailabiltyTask extends AsyncTask<Void, Void, Void> {
    final AlwaysOnHotwordDetector this$0;
    
    public Void doInBackground(Void... param1VarArgs) {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial internalGetInitialAvailability : ()I
      //   4: istore_2
      //   5: aload_0
      //   6: getfield this$0 : Landroid/service/voice/AlwaysOnHotwordDetector;
      //   9: invokestatic access$100 : (Landroid/service/voice/AlwaysOnHotwordDetector;)Ljava/lang/Object;
      //   12: astore_3
      //   13: aload_3
      //   14: monitorenter
      //   15: iload_2
      //   16: istore #4
      //   18: iload_2
      //   19: ifne -> 45
      //   22: aload_0
      //   23: invokespecial internalUpdateEnrolledKeyphraseMetadata : ()V
      //   26: aload_0
      //   27: getfield this$0 : Landroid/service/voice/AlwaysOnHotwordDetector;
      //   30: invokestatic access$400 : (Landroid/service/voice/AlwaysOnHotwordDetector;)Landroid/hardware/soundtrigger/KeyphraseMetadata;
      //   33: ifnull -> 42
      //   36: iconst_2
      //   37: istore #4
      //   39: goto -> 45
      //   42: iconst_1
      //   43: istore #4
      //   45: aload_0
      //   46: getfield this$0 : Landroid/service/voice/AlwaysOnHotwordDetector;
      //   49: iload #4
      //   51: invokestatic access$202 : (Landroid/service/voice/AlwaysOnHotwordDetector;I)I
      //   54: pop
      //   55: aload_0
      //   56: getfield this$0 : Landroid/service/voice/AlwaysOnHotwordDetector;
      //   59: invokestatic access$500 : (Landroid/service/voice/AlwaysOnHotwordDetector;)V
      //   62: aload_3
      //   63: monitorexit
      //   64: aconst_null
      //   65: areturn
      //   66: astore_1
      //   67: aload_3
      //   68: monitorexit
      //   69: aload_1
      //   70: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #939	-> 0
      //   #941	-> 5
      //   #942	-> 15
      //   #943	-> 22
      //   #944	-> 26
      //   #945	-> 36
      //   #947	-> 42
      //   #955	-> 45
      //   #956	-> 55
      //   #957	-> 62
      //   #958	-> 64
      //   #957	-> 66
      // Exception table:
      //   from	to	target	type
      //   22	26	66	finally
      //   26	36	66	finally
      //   45	55	66	finally
      //   55	62	66	finally
      //   62	64	66	finally
      //   67	69	66	finally
    }
    
    private int internalGetInitialAvailability() {
      synchronized (AlwaysOnHotwordDetector.this.mLock) {
        if (AlwaysOnHotwordDetector.this.mAvailability == -3)
          return -3; 
        try {
          null = AlwaysOnHotwordDetector.this;
          null = ((AlwaysOnHotwordDetector)null).mModelManagementService.getDspModuleProperties();
          if (null == null)
            return -2; 
          return 0;
        } catch (RemoteException null) {
          throw null.rethrowFromSystemServer();
        } 
      } 
    }
    
    private void internalUpdateEnrolledKeyphraseMetadata() {
      try {
        AlwaysOnHotwordDetector alwaysOnHotwordDetector1 = AlwaysOnHotwordDetector.this;
        IVoiceInteractionManagerService iVoiceInteractionManagerService = AlwaysOnHotwordDetector.this.mModelManagementService;
        AlwaysOnHotwordDetector alwaysOnHotwordDetector2 = AlwaysOnHotwordDetector.this;
        String str1 = alwaysOnHotwordDetector2.mText, str2 = AlwaysOnHotwordDetector.this.mLocale.toLanguageTag();
        AlwaysOnHotwordDetector.access$402(alwaysOnHotwordDetector1, iVoiceInteractionManagerService.getEnrolledKeyphraseMetadata(str1, str2));
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
  }
  
  public void dump(String paramString, PrintWriter paramPrintWriter) {
    synchronized (this.mLock) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("Text=");
      paramPrintWriter.println(this.mText);
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("Locale=");
      paramPrintWriter.println(this.mLocale);
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("Availability=");
      paramPrintWriter.println(this.mAvailability);
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("KeyphraseMetadata=");
      paramPrintWriter.println(this.mKeyphraseMetadata);
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("EnrollmentInfo=");
      paramPrintWriter.println(this.mKeyphraseEnrollmentInfo);
      return;
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface AudioCapabilities {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ModelParams {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface RecognitionFlags {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface RecognitionModes {}
}
