package android.service.voice;

import android.app.AppGlobals;
import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import com.android.internal.R;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

public class VoiceInteractionServiceInfo {
  static final String TAG = "VoiceInteractionServiceInfo";
  
  private String mParseError;
  
  private String mRecognitionService;
  
  private ServiceInfo mServiceInfo;
  
  private String mSessionService;
  
  private String mSettingsActivity;
  
  private boolean mSupportsAssist;
  
  private boolean mSupportsLaunchFromKeyguard;
  
  private boolean mSupportsLocalInteraction;
  
  public VoiceInteractionServiceInfo(PackageManager paramPackageManager, ComponentName paramComponentName) throws PackageManager.NameNotFoundException {
    this(paramPackageManager, paramPackageManager.getServiceInfo(paramComponentName, 128));
  }
  
  public VoiceInteractionServiceInfo(PackageManager paramPackageManager, ComponentName paramComponentName, int paramInt) throws PackageManager.NameNotFoundException {
    this(paramPackageManager, getServiceInfoOrThrow(paramComponentName, paramInt));
  }
  
  static ServiceInfo getServiceInfoOrThrow(ComponentName paramComponentName, int paramInt) throws PackageManager.NameNotFoundException {
    try {
      ServiceInfo serviceInfo = AppGlobals.getPackageManager().getServiceInfo(paramComponentName, 269222016, paramInt);
      if (serviceInfo != null)
        return serviceInfo; 
    } catch (RemoteException remoteException) {}
    throw new PackageManager.NameNotFoundException(paramComponentName.toString());
  }
  
  public VoiceInteractionServiceInfo(PackageManager paramPackageManager, ServiceInfo paramServiceInfo) {
    if (paramServiceInfo == null) {
      this.mParseError = "Service not available";
      return;
    } 
    if (!"android.permission.BIND_VOICE_INTERACTION".equals(paramServiceInfo.permission)) {
      this.mParseError = "Service does not require permission android.permission.BIND_VOICE_INTERACTION";
      return;
    } 
    XmlResourceParser xmlResourceParser1 = null, xmlResourceParser2 = null, xmlResourceParser3 = null, xmlResourceParser4 = null;
    try {
      StringBuilder stringBuilder;
      XmlResourceParser xmlResourceParser = paramServiceInfo.loadXmlMetaData(paramPackageManager, "android.voice_interaction");
      if (xmlResourceParser == null) {
        xmlResourceParser4 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser3 = xmlResourceParser;
        stringBuilder = new StringBuilder();
        xmlResourceParser4 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser3 = xmlResourceParser;
        this();
        xmlResourceParser4 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser3 = xmlResourceParser;
        stringBuilder.append("No android.voice_interaction meta-data for ");
        xmlResourceParser4 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser3 = xmlResourceParser;
        stringBuilder.append(paramServiceInfo.packageName);
        xmlResourceParser4 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser3 = xmlResourceParser;
        this.mParseError = stringBuilder.toString();
        if (xmlResourceParser != null)
          xmlResourceParser.close(); 
        return;
      } 
      xmlResourceParser4 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser3 = xmlResourceParser;
      Resources resources = stringBuilder.getResourcesForApplication(paramServiceInfo.applicationInfo);
      xmlResourceParser4 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser3 = xmlResourceParser;
      AttributeSet attributeSet = Xml.asAttributeSet(xmlResourceParser);
      while (true) {
        xmlResourceParser4 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser3 = xmlResourceParser;
        int i = xmlResourceParser.next();
        if (i != 1 && i != 2)
          continue; 
        break;
      } 
      xmlResourceParser4 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser3 = xmlResourceParser;
      String str = xmlResourceParser.getName();
      xmlResourceParser4 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser3 = xmlResourceParser;
      if (!"voice-interaction-service".equals(str)) {
        xmlResourceParser4 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser3 = xmlResourceParser;
        this.mParseError = "Meta-data does not start with voice-interaction-service tag";
        if (xmlResourceParser != null)
          xmlResourceParser.close(); 
        return;
      } 
      xmlResourceParser4 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser3 = xmlResourceParser;
      TypedArray typedArray = resources.obtainAttributes(attributeSet, R.styleable.VoiceInteractionService);
      xmlResourceParser4 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser3 = xmlResourceParser;
      this.mSessionService = typedArray.getString(1);
      xmlResourceParser4 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser3 = xmlResourceParser;
      this.mRecognitionService = typedArray.getString(2);
      xmlResourceParser4 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser3 = xmlResourceParser;
      this.mSettingsActivity = typedArray.getString(0);
      xmlResourceParser4 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser3 = xmlResourceParser;
      this.mSupportsAssist = typedArray.getBoolean(3, false);
      xmlResourceParser4 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser3 = xmlResourceParser;
      this.mSupportsLaunchFromKeyguard = typedArray.getBoolean(4, false);
      xmlResourceParser4 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser3 = xmlResourceParser;
      this.mSupportsLocalInteraction = typedArray.getBoolean(5, false);
      xmlResourceParser4 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser3 = xmlResourceParser;
      typedArray.recycle();
      xmlResourceParser4 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser3 = xmlResourceParser;
      if (this.mSessionService == null) {
        xmlResourceParser4 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser3 = xmlResourceParser;
        this.mParseError = "No sessionService specified";
        if (xmlResourceParser != null)
          xmlResourceParser.close(); 
        return;
      } 
      xmlResourceParser4 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser3 = xmlResourceParser;
      if (this.mRecognitionService == null) {
        xmlResourceParser4 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser3 = xmlResourceParser;
        this.mParseError = "No recognitionService specified";
        if (xmlResourceParser != null)
          xmlResourceParser.close(); 
        return;
      } 
      if (xmlResourceParser != null)
        xmlResourceParser.close(); 
      this.mServiceInfo = paramServiceInfo;
      return;
    } catch (XmlPullParserException xmlPullParserException) {
      xmlResourceParser4 = xmlResourceParser3;
      StringBuilder stringBuilder = new StringBuilder();
      xmlResourceParser4 = xmlResourceParser3;
      this();
      xmlResourceParser4 = xmlResourceParser3;
      stringBuilder.append("Error parsing voice interation service meta-data: ");
      xmlResourceParser4 = xmlResourceParser3;
      stringBuilder.append(xmlPullParserException);
      xmlResourceParser4 = xmlResourceParser3;
      this.mParseError = stringBuilder.toString();
      xmlResourceParser4 = xmlResourceParser3;
      Log.w("VoiceInteractionServiceInfo", "error parsing voice interaction service meta-data", (Throwable)xmlPullParserException);
      if (xmlResourceParser3 != null)
        xmlResourceParser3.close(); 
      return;
    } catch (IOException iOException) {
      xmlResourceParser4 = xmlResourceParser2;
      StringBuilder stringBuilder = new StringBuilder();
      xmlResourceParser4 = xmlResourceParser2;
      this();
      xmlResourceParser4 = xmlResourceParser2;
      stringBuilder.append("Error parsing voice interation service meta-data: ");
      xmlResourceParser4 = xmlResourceParser2;
      stringBuilder.append(iOException);
      xmlResourceParser4 = xmlResourceParser2;
      this.mParseError = stringBuilder.toString();
      xmlResourceParser4 = xmlResourceParser2;
      Log.w("VoiceInteractionServiceInfo", "error parsing voice interaction service meta-data", iOException);
      if (xmlResourceParser2 != null)
        xmlResourceParser2.close(); 
      return;
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      xmlResourceParser4 = xmlResourceParser1;
      StringBuilder stringBuilder = new StringBuilder();
      xmlResourceParser4 = xmlResourceParser1;
      this();
      xmlResourceParser4 = xmlResourceParser1;
      stringBuilder.append("Error parsing voice interation service meta-data: ");
      xmlResourceParser4 = xmlResourceParser1;
      stringBuilder.append(nameNotFoundException);
      xmlResourceParser4 = xmlResourceParser1;
      this.mParseError = stringBuilder.toString();
      xmlResourceParser4 = xmlResourceParser1;
      Log.w("VoiceInteractionServiceInfo", "error parsing voice interaction service meta-data", (Throwable)nameNotFoundException);
      if (xmlResourceParser1 != null)
        xmlResourceParser1.close(); 
      return;
    } finally {}
    if (xmlResourceParser4 != null)
      xmlResourceParser4.close(); 
    throw paramPackageManager;
  }
  
  public String getParseError() {
    return this.mParseError;
  }
  
  public ServiceInfo getServiceInfo() {
    return this.mServiceInfo;
  }
  
  public String getSessionService() {
    return this.mSessionService;
  }
  
  public String getRecognitionService() {
    return this.mRecognitionService;
  }
  
  public String getSettingsActivity() {
    return this.mSettingsActivity;
  }
  
  public boolean getSupportsAssist() {
    return this.mSupportsAssist;
  }
  
  public boolean getSupportsLaunchFromKeyguard() {
    return this.mSupportsLaunchFromKeyguard;
  }
  
  public boolean getSupportsLocalInteraction() {
    return this.mSupportsLocalInteraction;
  }
}
