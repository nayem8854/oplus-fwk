package android.service.voice;

import android.app.assist.AssistContent;
import android.app.assist.AssistStructure;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.android.internal.app.IVoiceInteractionSessionShowCallback;

public interface IVoiceInteractionSession extends IInterface {
  void closeSystemDialogs() throws RemoteException;
  
  void destroy() throws RemoteException;
  
  void handleAssist(int paramInt1, IBinder paramIBinder, Bundle paramBundle, AssistStructure paramAssistStructure, AssistContent paramAssistContent, int paramInt2, int paramInt3) throws RemoteException;
  
  void handleScreenshot(Bitmap paramBitmap) throws RemoteException;
  
  void hide() throws RemoteException;
  
  void onLockscreenShown() throws RemoteException;
  
  void show(Bundle paramBundle, int paramInt, IVoiceInteractionSessionShowCallback paramIVoiceInteractionSessionShowCallback) throws RemoteException;
  
  void taskFinished(Intent paramIntent, int paramInt) throws RemoteException;
  
  void taskStarted(Intent paramIntent, int paramInt) throws RemoteException;
  
  class Default implements IVoiceInteractionSession {
    public void show(Bundle param1Bundle, int param1Int, IVoiceInteractionSessionShowCallback param1IVoiceInteractionSessionShowCallback) throws RemoteException {}
    
    public void hide() throws RemoteException {}
    
    public void handleAssist(int param1Int1, IBinder param1IBinder, Bundle param1Bundle, AssistStructure param1AssistStructure, AssistContent param1AssistContent, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void handleScreenshot(Bitmap param1Bitmap) throws RemoteException {}
    
    public void taskStarted(Intent param1Intent, int param1Int) throws RemoteException {}
    
    public void taskFinished(Intent param1Intent, int param1Int) throws RemoteException {}
    
    public void closeSystemDialogs() throws RemoteException {}
    
    public void onLockscreenShown() throws RemoteException {}
    
    public void destroy() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVoiceInteractionSession {
    private static final String DESCRIPTOR = "android.service.voice.IVoiceInteractionSession";
    
    static final int TRANSACTION_closeSystemDialogs = 7;
    
    static final int TRANSACTION_destroy = 9;
    
    static final int TRANSACTION_handleAssist = 3;
    
    static final int TRANSACTION_handleScreenshot = 4;
    
    static final int TRANSACTION_hide = 2;
    
    static final int TRANSACTION_onLockscreenShown = 8;
    
    static final int TRANSACTION_show = 1;
    
    static final int TRANSACTION_taskFinished = 6;
    
    static final int TRANSACTION_taskStarted = 5;
    
    public Stub() {
      attachInterface(this, "android.service.voice.IVoiceInteractionSession");
    }
    
    public static IVoiceInteractionSession asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.voice.IVoiceInteractionSession");
      if (iInterface != null && iInterface instanceof IVoiceInteractionSession)
        return (IVoiceInteractionSession)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 9:
          return "destroy";
        case 8:
          return "onLockscreenShown";
        case 7:
          return "closeSystemDialogs";
        case 6:
          return "taskFinished";
        case 5:
          return "taskStarted";
        case 4:
          return "handleScreenshot";
        case 3:
          return "handleAssist";
        case 2:
          return "hide";
        case 1:
          break;
      } 
      return "show";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        IBinder iBinder;
        AssistStructure assistStructure;
        AssistContent assistContent;
        int i;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 9:
            param1Parcel1.enforceInterface("android.service.voice.IVoiceInteractionSession");
            destroy();
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.service.voice.IVoiceInteractionSession");
            onLockscreenShown();
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.service.voice.IVoiceInteractionSession");
            closeSystemDialogs();
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.service.voice.IVoiceInteractionSession");
            if (param1Parcel1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            taskFinished((Intent)param1Parcel2, param1Int1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.service.voice.IVoiceInteractionSession");
            if (param1Parcel1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            taskStarted((Intent)param1Parcel2, param1Int1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.service.voice.IVoiceInteractionSession");
            if (param1Parcel1.readInt() != 0) {
              Bitmap bitmap = (Bitmap)Bitmap.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            handleScreenshot((Bitmap)param1Parcel1);
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.service.voice.IVoiceInteractionSession");
            param1Int2 = param1Parcel1.readInt();
            iBinder = param1Parcel1.readStrongBinder();
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              assistStructure = (AssistStructure)AssistStructure.CREATOR.createFromParcel(param1Parcel1);
            } else {
              assistStructure = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              assistContent = (AssistContent)AssistContent.CREATOR.createFromParcel(param1Parcel1);
            } else {
              assistContent = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            i = param1Parcel1.readInt();
            handleAssist(param1Int2, iBinder, (Bundle)param1Parcel2, assistStructure, assistContent, param1Int1, i);
            return true;
          case 2:
            param1Parcel1.enforceInterface("android.service.voice.IVoiceInteractionSession");
            hide();
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("android.service.voice.IVoiceInteractionSession");
        if (param1Parcel1.readInt() != 0) {
          Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel2 = null;
        } 
        param1Int1 = param1Parcel1.readInt();
        IVoiceInteractionSessionShowCallback iVoiceInteractionSessionShowCallback = IVoiceInteractionSessionShowCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
        show((Bundle)param1Parcel2, param1Int1, iVoiceInteractionSessionShowCallback);
        return true;
      } 
      param1Parcel2.writeString("android.service.voice.IVoiceInteractionSession");
      return true;
    }
    
    private static class Proxy implements IVoiceInteractionSession {
      public static IVoiceInteractionSession sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.voice.IVoiceInteractionSession";
      }
      
      public void show(Bundle param2Bundle, int param2Int, IVoiceInteractionSessionShowCallback param2IVoiceInteractionSessionShowCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.voice.IVoiceInteractionSession");
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          if (param2IVoiceInteractionSessionShowCallback != null) {
            iBinder = param2IVoiceInteractionSessionShowCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IVoiceInteractionSession.Stub.getDefaultImpl() != null) {
            IVoiceInteractionSession.Stub.getDefaultImpl().show(param2Bundle, param2Int, param2IVoiceInteractionSessionShowCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void hide() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.voice.IVoiceInteractionSession");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IVoiceInteractionSession.Stub.getDefaultImpl() != null) {
            IVoiceInteractionSession.Stub.getDefaultImpl().hide();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void handleAssist(int param2Int1, IBinder param2IBinder, Bundle param2Bundle, AssistStructure param2AssistStructure, AssistContent param2AssistContent, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.voice.IVoiceInteractionSession");
          try {
            parcel.writeInt(param2Int1);
            try {
              parcel.writeStrongBinder(param2IBinder);
              if (param2Bundle != null) {
                parcel.writeInt(1);
                param2Bundle.writeToParcel(parcel, 0);
              } else {
                parcel.writeInt(0);
              } 
              if (param2AssistStructure != null) {
                parcel.writeInt(1);
                param2AssistStructure.writeToParcel(parcel, 0);
              } else {
                parcel.writeInt(0);
              } 
              if (param2AssistContent != null) {
                parcel.writeInt(1);
                param2AssistContent.writeToParcel(parcel, 0);
              } else {
                parcel.writeInt(0);
              } 
              try {
                parcel.writeInt(param2Int2);
                parcel.writeInt(param2Int3);
                boolean bool = this.mRemote.transact(3, parcel, null, 1);
                if (!bool && IVoiceInteractionSession.Stub.getDefaultImpl() != null) {
                  IVoiceInteractionSession.Stub.getDefaultImpl().handleAssist(param2Int1, param2IBinder, param2Bundle, param2AssistStructure, param2AssistContent, param2Int2, param2Int3);
                  parcel.recycle();
                  return;
                } 
                parcel.recycle();
                return;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2IBinder;
      }
      
      public void handleScreenshot(Bitmap param2Bitmap) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.voice.IVoiceInteractionSession");
          if (param2Bitmap != null) {
            parcel.writeInt(1);
            param2Bitmap.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IVoiceInteractionSession.Stub.getDefaultImpl() != null) {
            IVoiceInteractionSession.Stub.getDefaultImpl().handleScreenshot(param2Bitmap);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void taskStarted(Intent param2Intent, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.voice.IVoiceInteractionSession");
          if (param2Intent != null) {
            parcel.writeInt(1);
            param2Intent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IVoiceInteractionSession.Stub.getDefaultImpl() != null) {
            IVoiceInteractionSession.Stub.getDefaultImpl().taskStarted(param2Intent, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void taskFinished(Intent param2Intent, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.voice.IVoiceInteractionSession");
          if (param2Intent != null) {
            parcel.writeInt(1);
            param2Intent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IVoiceInteractionSession.Stub.getDefaultImpl() != null) {
            IVoiceInteractionSession.Stub.getDefaultImpl().taskFinished(param2Intent, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void closeSystemDialogs() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.voice.IVoiceInteractionSession");
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IVoiceInteractionSession.Stub.getDefaultImpl() != null) {
            IVoiceInteractionSession.Stub.getDefaultImpl().closeSystemDialogs();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onLockscreenShown() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.voice.IVoiceInteractionSession");
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IVoiceInteractionSession.Stub.getDefaultImpl() != null) {
            IVoiceInteractionSession.Stub.getDefaultImpl().onLockscreenShown();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void destroy() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.voice.IVoiceInteractionSession");
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IVoiceInteractionSession.Stub.getDefaultImpl() != null) {
            IVoiceInteractionSession.Stub.getDefaultImpl().destroy();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVoiceInteractionSession param1IVoiceInteractionSession) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVoiceInteractionSession != null) {
          Proxy.sDefaultImpl = param1IVoiceInteractionSession;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVoiceInteractionSession getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
