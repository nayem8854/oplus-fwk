package android.service.voice;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.hardware.soundtrigger.KeyphraseEnrollmentInfo;
import android.media.voice.KeyphraseModelManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.provider.Settings;
import android.util.ArraySet;
import android.util.Log;
import com.android.internal.app.IVoiceActionCheckCallback;
import com.android.internal.app.IVoiceInteractionManagerService;
import com.android.internal.util.function.pooled.PooledLambda;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

public class VoiceInteractionService extends Service {
  static final String TAG = VoiceInteractionService.class.getSimpleName();
  
  IVoiceInteractionService mInterface = (IVoiceInteractionService)new Object(this);
  
  private final Object mLock = new Object();
  
  public void onLaunchVoiceAssistFromKeyguard() {}
  
  public static boolean isActiveService(Context paramContext, ComponentName paramComponentName) {
    String str = Settings.Secure.getString(paramContext.getContentResolver(), "voice_interaction_service");
    if (str == null || str.isEmpty())
      return false; 
    ComponentName componentName = ComponentName.unflattenFromString(str);
    if (componentName == null)
      return false; 
    return componentName.equals(paramComponentName);
  }
  
  public void setDisabledShowContext(int paramInt) {
    try {
      this.mSystemService.setDisabledShowContext(paramInt);
    } catch (RemoteException remoteException) {}
  }
  
  public int getDisabledShowContext() {
    try {
      return this.mSystemService.getDisabledShowContext();
    } catch (RemoteException remoteException) {
      return 0;
    } 
  }
  
  public void showSession(Bundle paramBundle, int paramInt) {
    IVoiceInteractionManagerService iVoiceInteractionManagerService = this.mSystemService;
    if (iVoiceInteractionManagerService != null) {
      try {
        iVoiceInteractionManagerService.showSession(paramBundle, paramInt);
      } catch (RemoteException remoteException) {}
      return;
    } 
    throw new IllegalStateException("Not available until onReady() is called");
  }
  
  public Set<String> onGetSupportedVoiceActions(Set<String> paramSet) {
    return Collections.emptySet();
  }
  
  public IBinder onBind(Intent paramIntent) {
    if ("android.service.voice.VoiceInteractionService".equals(paramIntent.getAction()))
      return this.mInterface.asBinder(); 
    return null;
  }
  
  public void onReady() {
    IBinder iBinder = ServiceManager.getService("voiceinteraction");
    IVoiceInteractionManagerService iVoiceInteractionManagerService = IVoiceInteractionManagerService.Stub.asInterface(iBinder);
    Objects.requireNonNull(iVoiceInteractionManagerService);
    try {
      this.mSystemService.asBinder().linkToDeath(this.mDeathRecipient, 0);
    } catch (RemoteException remoteException) {
      Log.wtf(TAG, "unable to link to death with system service");
    } 
    this.mKeyphraseEnrollmentInfo = new KeyphraseEnrollmentInfo(getPackageManager());
  }
  
  private IBinder.DeathRecipient mDeathRecipient = new _$$Lambda$VoiceInteractionService$yug9azdKctkf1nIeEMtq_56D_GE(this);
  
  public static final String SERVICE_INTERFACE = "android.service.voice.VoiceInteractionService";
  
  public static final String SERVICE_META_DATA = "android.voice_interaction";
  
  private AlwaysOnHotwordDetector mHotwordDetector;
  
  private KeyphraseEnrollmentInfo mKeyphraseEnrollmentInfo;
  
  IVoiceInteractionManagerService mSystemService;
  
  private void onShutdownInternal() {
    onShutdown();
    safelyShutdownHotwordDetector();
  }
  
  public void onShutdown() {}
  
  private void onSoundModelsChangedInternal() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mHotwordDetector : Landroid/service/voice/AlwaysOnHotwordDetector;
    //   6: ifnull -> 16
    //   9: aload_0
    //   10: getfield mHotwordDetector : Landroid/service/voice/AlwaysOnHotwordDetector;
    //   13: invokevirtual onSoundModelsChanged : ()V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_1
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_1
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #282	-> 0
    //   #283	-> 2
    //   #285	-> 9
    //   #287	-> 16
    //   #288	-> 18
    //   #287	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	9	19	finally
    //   9	16	19	finally
    //   16	18	19	finally
    //   20	22	19	finally
  }
  
  private void onHandleVoiceActionCheck(List<String> paramList, IVoiceActionCheckCallback paramIVoiceActionCheckCallback) {
    if (paramIVoiceActionCheckCallback != null)
      try {
        ArraySet arraySet = new ArraySet();
        this(paramList);
        Set<String> set = onGetSupportedVoiceActions((Set<String>)arraySet);
        paramList = new ArrayList<>();
        super((Collection)set);
        paramIVoiceActionCheckCallback.onComplete(paramList);
      } catch (RemoteException remoteException) {} 
  }
  
  public final AlwaysOnHotwordDetector createAlwaysOnHotwordDetector(String paramString, Locale paramLocale, AlwaysOnHotwordDetector.Callback paramCallback) {
    if (this.mSystemService != null)
      synchronized (this.mLock) {
        safelyShutdownHotwordDetector();
        AlwaysOnHotwordDetector alwaysOnHotwordDetector = new AlwaysOnHotwordDetector();
        this(paramString, paramLocale, paramCallback, this.mKeyphraseEnrollmentInfo, this.mSystemService);
        this.mHotwordDetector = alwaysOnHotwordDetector;
        return alwaysOnHotwordDetector;
      }  
    throw new IllegalStateException("Not available until onReady() is called");
  }
  
  @SystemApi
  public final KeyphraseModelManager createKeyphraseModelManager() {
    if (this.mSystemService != null)
      synchronized (this.mLock) {
        KeyphraseModelManager keyphraseModelManager = new KeyphraseModelManager();
        this(this.mSystemService);
        return keyphraseModelManager;
      }  
    throw new IllegalStateException("Not available until onReady() is called");
  }
  
  protected final KeyphraseEnrollmentInfo getKeyphraseEnrollmentInfo() {
    return this.mKeyphraseEnrollmentInfo;
  }
  
  public final boolean isKeyphraseAndLocaleSupportedForHotword(String paramString, Locale paramLocale) {
    KeyphraseEnrollmentInfo keyphraseEnrollmentInfo = this.mKeyphraseEnrollmentInfo;
    boolean bool = false;
    if (keyphraseEnrollmentInfo == null)
      return false; 
    if (keyphraseEnrollmentInfo.getKeyphraseMetadata(paramString, paramLocale) != null)
      bool = true; 
    return bool;
  }
  
  private void safelyShutdownHotwordDetector() {
    synchronized (this.mLock) {
      if (this.mHotwordDetector == null)
        return; 
      try {
        this.mHotwordDetector.stopRecognition();
      } catch (Exception exception) {}
      try {
        this.mHotwordDetector.invalidate();
      } catch (Exception exception) {}
      this.mHotwordDetector = null;
      return;
    } 
  }
  
  public final void setUiHints(Bundle paramBundle) {
    if (paramBundle != null)
      try {
        this.mSystemService.setUiHints(paramBundle);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    throw new IllegalArgumentException("Hints must be non-null");
  }
  
  protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    paramPrintWriter.println("VOICE INTERACTION");
    synchronized (this.mLock) {
      paramPrintWriter.println("  AlwaysOnHotwordDetector");
      if (this.mHotwordDetector == null) {
        paramPrintWriter.println("    NULL");
      } else {
        this.mHotwordDetector.dump("    ", paramPrintWriter);
      } 
      return;
    } 
  }
}
