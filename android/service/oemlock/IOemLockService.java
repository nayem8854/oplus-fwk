package android.service.oemlock;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOemLockService extends IInterface {
  String getLockName() throws RemoteException;
  
  boolean isDeviceOemUnlocked() throws RemoteException;
  
  boolean isOemUnlockAllowed() throws RemoteException;
  
  boolean isOemUnlockAllowedByCarrier() throws RemoteException;
  
  boolean isOemUnlockAllowedByUser() throws RemoteException;
  
  void setOemUnlockAllowedByCarrier(boolean paramBoolean, byte[] paramArrayOfbyte) throws RemoteException;
  
  void setOemUnlockAllowedByUser(boolean paramBoolean) throws RemoteException;
  
  class Default implements IOemLockService {
    public String getLockName() throws RemoteException {
      return null;
    }
    
    public void setOemUnlockAllowedByCarrier(boolean param1Boolean, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public boolean isOemUnlockAllowedByCarrier() throws RemoteException {
      return false;
    }
    
    public void setOemUnlockAllowedByUser(boolean param1Boolean) throws RemoteException {}
    
    public boolean isOemUnlockAllowedByUser() throws RemoteException {
      return false;
    }
    
    public boolean isOemUnlockAllowed() throws RemoteException {
      return false;
    }
    
    public boolean isDeviceOemUnlocked() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOemLockService {
    private static final String DESCRIPTOR = "android.service.oemlock.IOemLockService";
    
    static final int TRANSACTION_getLockName = 1;
    
    static final int TRANSACTION_isDeviceOemUnlocked = 7;
    
    static final int TRANSACTION_isOemUnlockAllowed = 6;
    
    static final int TRANSACTION_isOemUnlockAllowedByCarrier = 3;
    
    static final int TRANSACTION_isOemUnlockAllowedByUser = 5;
    
    static final int TRANSACTION_setOemUnlockAllowedByCarrier = 2;
    
    static final int TRANSACTION_setOemUnlockAllowedByUser = 4;
    
    public Stub() {
      attachInterface(this, "android.service.oemlock.IOemLockService");
    }
    
    public static IOemLockService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.oemlock.IOemLockService");
      if (iInterface != null && iInterface instanceof IOemLockService)
        return (IOemLockService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "isDeviceOemUnlocked";
        case 6:
          return "isOemUnlockAllowed";
        case 5:
          return "isOemUnlockAllowedByUser";
        case 4:
          return "setOemUnlockAllowedByUser";
        case 3:
          return "isOemUnlockAllowedByCarrier";
        case 2:
          return "setOemUnlockAllowedByCarrier";
        case 1:
          break;
      } 
      return "getLockName";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        byte[] arrayOfByte;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("android.service.oemlock.IOemLockService");
            bool = isDeviceOemUnlocked();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.service.oemlock.IOemLockService");
            bool = isOemUnlockAllowed();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.service.oemlock.IOemLockService");
            bool = isOemUnlockAllowedByUser();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.service.oemlock.IOemLockService");
            if (param1Parcel1.readInt() != 0)
              bool2 = true; 
            setOemUnlockAllowedByUser(bool2);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.service.oemlock.IOemLockService");
            bool = isOemUnlockAllowedByCarrier();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 2:
            param1Parcel1.enforceInterface("android.service.oemlock.IOemLockService");
            bool2 = bool1;
            if (param1Parcel1.readInt() != 0)
              bool2 = true; 
            arrayOfByte = param1Parcel1.createByteArray();
            setOemUnlockAllowedByCarrier(bool2, arrayOfByte);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        arrayOfByte.enforceInterface("android.service.oemlock.IOemLockService");
        String str = getLockName();
        param1Parcel2.writeNoException();
        param1Parcel2.writeString(str);
        return true;
      } 
      param1Parcel2.writeString("android.service.oemlock.IOemLockService");
      return true;
    }
    
    private static class Proxy implements IOemLockService {
      public static IOemLockService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.oemlock.IOemLockService";
      }
      
      public String getLockName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.oemlock.IOemLockService");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOemLockService.Stub.getDefaultImpl() != null)
            return IOemLockService.Stub.getDefaultImpl().getLockName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setOemUnlockAllowedByCarrier(boolean param2Boolean, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.service.oemlock.IOemLockService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool1 && IOemLockService.Stub.getDefaultImpl() != null) {
            IOemLockService.Stub.getDefaultImpl().setOemUnlockAllowedByCarrier(param2Boolean, param2ArrayOfbyte);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isOemUnlockAllowedByCarrier() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.oemlock.IOemLockService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IOemLockService.Stub.getDefaultImpl() != null) {
            bool1 = IOemLockService.Stub.getDefaultImpl().isOemUnlockAllowedByCarrier();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setOemUnlockAllowedByUser(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.service.oemlock.IOemLockService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool1 && IOemLockService.Stub.getDefaultImpl() != null) {
            IOemLockService.Stub.getDefaultImpl().setOemUnlockAllowedByUser(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isOemUnlockAllowedByUser() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.oemlock.IOemLockService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IOemLockService.Stub.getDefaultImpl() != null) {
            bool1 = IOemLockService.Stub.getDefaultImpl().isOemUnlockAllowedByUser();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isOemUnlockAllowed() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.oemlock.IOemLockService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IOemLockService.Stub.getDefaultImpl() != null) {
            bool1 = IOemLockService.Stub.getDefaultImpl().isOemUnlockAllowed();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDeviceOemUnlocked() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.oemlock.IOemLockService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IOemLockService.Stub.getDefaultImpl() != null) {
            bool1 = IOemLockService.Stub.getDefaultImpl().isDeviceOemUnlocked();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOemLockService param1IOemLockService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOemLockService != null) {
          Proxy.sDefaultImpl = param1IOemLockService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOemLockService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
