package android.service.oemlock;

import android.annotation.SystemApi;
import android.os.RemoteException;

@SystemApi
public class OemLockManager {
  private IOemLockService mService;
  
  public OemLockManager(IOemLockService paramIOemLockService) {
    this.mService = paramIOemLockService;
  }
  
  public String getLockName() {
    try {
      return this.mService.getLockName();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setOemUnlockAllowedByCarrier(boolean paramBoolean, byte[] paramArrayOfbyte) {
    try {
      this.mService.setOemUnlockAllowedByCarrier(paramBoolean, paramArrayOfbyte);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isOemUnlockAllowedByCarrier() {
    try {
      return this.mService.isOemUnlockAllowedByCarrier();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setOemUnlockAllowedByUser(boolean paramBoolean) {
    try {
      this.mService.setOemUnlockAllowedByUser(paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isOemUnlockAllowedByUser() {
    try {
      return this.mService.isOemUnlockAllowedByUser();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isOemUnlockAllowed() {
    try {
      return this.mService.isOemUnlockAllowed();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isDeviceOemUnlocked() {
    try {
      return this.mService.isDeviceOemUnlocked();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
}
