package android.service.quicksettings;

import android.graphics.drawable.Icon;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IQSService extends IInterface {
  Tile getTile(IBinder paramIBinder) throws RemoteException;
  
  boolean isLocked() throws RemoteException;
  
  boolean isSecure() throws RemoteException;
  
  void onDialogHidden(IBinder paramIBinder) throws RemoteException;
  
  void onShowDialog(IBinder paramIBinder) throws RemoteException;
  
  void onStartActivity(IBinder paramIBinder) throws RemoteException;
  
  void onStartSuccessful(IBinder paramIBinder) throws RemoteException;
  
  void startUnlockAndRun(IBinder paramIBinder) throws RemoteException;
  
  void updateQsTile(Tile paramTile, IBinder paramIBinder) throws RemoteException;
  
  void updateStatusIcon(IBinder paramIBinder, Icon paramIcon, String paramString) throws RemoteException;
  
  class Default implements IQSService {
    public Tile getTile(IBinder param1IBinder) throws RemoteException {
      return null;
    }
    
    public void updateQsTile(Tile param1Tile, IBinder param1IBinder) throws RemoteException {}
    
    public void updateStatusIcon(IBinder param1IBinder, Icon param1Icon, String param1String) throws RemoteException {}
    
    public void onShowDialog(IBinder param1IBinder) throws RemoteException {}
    
    public void onStartActivity(IBinder param1IBinder) throws RemoteException {}
    
    public boolean isLocked() throws RemoteException {
      return false;
    }
    
    public boolean isSecure() throws RemoteException {
      return false;
    }
    
    public void startUnlockAndRun(IBinder param1IBinder) throws RemoteException {}
    
    public void onDialogHidden(IBinder param1IBinder) throws RemoteException {}
    
    public void onStartSuccessful(IBinder param1IBinder) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IQSService {
    private static final String DESCRIPTOR = "android.service.quicksettings.IQSService";
    
    static final int TRANSACTION_getTile = 1;
    
    static final int TRANSACTION_isLocked = 6;
    
    static final int TRANSACTION_isSecure = 7;
    
    static final int TRANSACTION_onDialogHidden = 9;
    
    static final int TRANSACTION_onShowDialog = 4;
    
    static final int TRANSACTION_onStartActivity = 5;
    
    static final int TRANSACTION_onStartSuccessful = 10;
    
    static final int TRANSACTION_startUnlockAndRun = 8;
    
    static final int TRANSACTION_updateQsTile = 2;
    
    static final int TRANSACTION_updateStatusIcon = 3;
    
    public Stub() {
      attachInterface(this, "android.service.quicksettings.IQSService");
    }
    
    public static IQSService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.quicksettings.IQSService");
      if (iInterface != null && iInterface instanceof IQSService)
        return (IQSService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 10:
          return "onStartSuccessful";
        case 9:
          return "onDialogHidden";
        case 8:
          return "startUnlockAndRun";
        case 7:
          return "isSecure";
        case 6:
          return "isLocked";
        case 5:
          return "onStartActivity";
        case 4:
          return "onShowDialog";
        case 3:
          return "updateStatusIcon";
        case 2:
          return "updateQsTile";
        case 1:
          break;
      } 
      return "getTile";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        IBinder iBinder2;
        String str;
        IBinder iBinder3;
        Icon icon;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 10:
            param1Parcel1.enforceInterface("android.service.quicksettings.IQSService");
            iBinder2 = param1Parcel1.readStrongBinder();
            onStartSuccessful(iBinder2);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            iBinder2.enforceInterface("android.service.quicksettings.IQSService");
            iBinder2 = iBinder2.readStrongBinder();
            onDialogHidden(iBinder2);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            iBinder2.enforceInterface("android.service.quicksettings.IQSService");
            iBinder2 = iBinder2.readStrongBinder();
            startUnlockAndRun(iBinder2);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            iBinder2.enforceInterface("android.service.quicksettings.IQSService");
            bool = isSecure();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 6:
            iBinder2.enforceInterface("android.service.quicksettings.IQSService");
            bool = isLocked();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 5:
            iBinder2.enforceInterface("android.service.quicksettings.IQSService");
            iBinder2 = iBinder2.readStrongBinder();
            onStartActivity(iBinder2);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            iBinder2.enforceInterface("android.service.quicksettings.IQSService");
            iBinder2 = iBinder2.readStrongBinder();
            onShowDialog(iBinder2);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            iBinder2.enforceInterface("android.service.quicksettings.IQSService");
            iBinder3 = iBinder2.readStrongBinder();
            if (iBinder2.readInt() != 0) {
              icon = Icon.CREATOR.createFromParcel((Parcel)iBinder2);
            } else {
              icon = null;
            } 
            str = iBinder2.readString();
            updateStatusIcon(iBinder3, icon, str);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str.enforceInterface("android.service.quicksettings.IQSService");
            if (str.readInt() != 0) {
              Tile tile1 = Tile.CREATOR.createFromParcel((Parcel)str);
            } else {
              icon = null;
            } 
            iBinder1 = str.readStrongBinder();
            updateQsTile((Tile)icon, iBinder1);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        iBinder1.enforceInterface("android.service.quicksettings.IQSService");
        IBinder iBinder1 = iBinder1.readStrongBinder();
        Tile tile = getTile(iBinder1);
        param1Parcel2.writeNoException();
        if (tile != null) {
          param1Parcel2.writeInt(1);
          tile.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      param1Parcel2.writeString("android.service.quicksettings.IQSService");
      return true;
    }
    
    private static class Proxy implements IQSService {
      public static IQSService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.quicksettings.IQSService";
      }
      
      public Tile getTile(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.quicksettings.IQSService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IQSService.Stub.getDefaultImpl() != null)
            return IQSService.Stub.getDefaultImpl().getTile(param2IBinder); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Tile tile = Tile.CREATOR.createFromParcel(parcel2);
          } else {
            param2IBinder = null;
          } 
          return (Tile)param2IBinder;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateQsTile(Tile param2Tile, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.quicksettings.IQSService");
          if (param2Tile != null) {
            parcel1.writeInt(1);
            param2Tile.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IQSService.Stub.getDefaultImpl() != null) {
            IQSService.Stub.getDefaultImpl().updateQsTile(param2Tile, param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateStatusIcon(IBinder param2IBinder, Icon param2Icon, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.quicksettings.IQSService");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Icon != null) {
            parcel1.writeInt(1);
            param2Icon.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IQSService.Stub.getDefaultImpl() != null) {
            IQSService.Stub.getDefaultImpl().updateStatusIcon(param2IBinder, param2Icon, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onShowDialog(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.quicksettings.IQSService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IQSService.Stub.getDefaultImpl() != null) {
            IQSService.Stub.getDefaultImpl().onShowDialog(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onStartActivity(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.quicksettings.IQSService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IQSService.Stub.getDefaultImpl() != null) {
            IQSService.Stub.getDefaultImpl().onStartActivity(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isLocked() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.quicksettings.IQSService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IQSService.Stub.getDefaultImpl() != null) {
            bool1 = IQSService.Stub.getDefaultImpl().isLocked();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSecure() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.quicksettings.IQSService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IQSService.Stub.getDefaultImpl() != null) {
            bool1 = IQSService.Stub.getDefaultImpl().isSecure();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startUnlockAndRun(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.quicksettings.IQSService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IQSService.Stub.getDefaultImpl() != null) {
            IQSService.Stub.getDefaultImpl().startUnlockAndRun(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onDialogHidden(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.quicksettings.IQSService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IQSService.Stub.getDefaultImpl() != null) {
            IQSService.Stub.getDefaultImpl().onDialogHidden(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onStartSuccessful(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.quicksettings.IQSService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IQSService.Stub.getDefaultImpl() != null) {
            IQSService.Stub.getDefaultImpl().onStartSuccessful(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IQSService param1IQSService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IQSService != null) {
          Proxy.sDefaultImpl = param1IQSService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IQSService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
