package android.service.quicksettings;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IQSTileService extends IInterface {
  void onClick(IBinder paramIBinder) throws RemoteException;
  
  void onStartListening() throws RemoteException;
  
  void onStopListening() throws RemoteException;
  
  void onTileAdded() throws RemoteException;
  
  void onTileRemoved() throws RemoteException;
  
  void onUnlockComplete() throws RemoteException;
  
  class Default implements IQSTileService {
    public void onTileAdded() throws RemoteException {}
    
    public void onTileRemoved() throws RemoteException {}
    
    public void onStartListening() throws RemoteException {}
    
    public void onStopListening() throws RemoteException {}
    
    public void onClick(IBinder param1IBinder) throws RemoteException {}
    
    public void onUnlockComplete() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IQSTileService {
    private static final String DESCRIPTOR = "android.service.quicksettings.IQSTileService";
    
    static final int TRANSACTION_onClick = 5;
    
    static final int TRANSACTION_onStartListening = 3;
    
    static final int TRANSACTION_onStopListening = 4;
    
    static final int TRANSACTION_onTileAdded = 1;
    
    static final int TRANSACTION_onTileRemoved = 2;
    
    static final int TRANSACTION_onUnlockComplete = 6;
    
    public Stub() {
      attachInterface(this, "android.service.quicksettings.IQSTileService");
    }
    
    public static IQSTileService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.quicksettings.IQSTileService");
      if (iInterface != null && iInterface instanceof IQSTileService)
        return (IQSTileService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "onUnlockComplete";
        case 5:
          return "onClick";
        case 4:
          return "onStopListening";
        case 3:
          return "onStartListening";
        case 2:
          return "onTileRemoved";
        case 1:
          break;
      } 
      return "onTileAdded";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        IBinder iBinder;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("android.service.quicksettings.IQSTileService");
            onUnlockComplete();
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.service.quicksettings.IQSTileService");
            iBinder = param1Parcel1.readStrongBinder();
            onClick(iBinder);
            return true;
          case 4:
            iBinder.enforceInterface("android.service.quicksettings.IQSTileService");
            onStopListening();
            return true;
          case 3:
            iBinder.enforceInterface("android.service.quicksettings.IQSTileService");
            onStartListening();
            return true;
          case 2:
            iBinder.enforceInterface("android.service.quicksettings.IQSTileService");
            onTileRemoved();
            return true;
          case 1:
            break;
        } 
        iBinder.enforceInterface("android.service.quicksettings.IQSTileService");
        onTileAdded();
        return true;
      } 
      param1Parcel2.writeString("android.service.quicksettings.IQSTileService");
      return true;
    }
    
    private static class Proxy implements IQSTileService {
      public static IQSTileService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.quicksettings.IQSTileService";
      }
      
      public void onTileAdded() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.quicksettings.IQSTileService");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IQSTileService.Stub.getDefaultImpl() != null) {
            IQSTileService.Stub.getDefaultImpl().onTileAdded();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTileRemoved() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.quicksettings.IQSTileService");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IQSTileService.Stub.getDefaultImpl() != null) {
            IQSTileService.Stub.getDefaultImpl().onTileRemoved();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onStartListening() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.quicksettings.IQSTileService");
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IQSTileService.Stub.getDefaultImpl() != null) {
            IQSTileService.Stub.getDefaultImpl().onStartListening();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onStopListening() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.quicksettings.IQSTileService");
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IQSTileService.Stub.getDefaultImpl() != null) {
            IQSTileService.Stub.getDefaultImpl().onStopListening();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onClick(IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.quicksettings.IQSTileService");
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IQSTileService.Stub.getDefaultImpl() != null) {
            IQSTileService.Stub.getDefaultImpl().onClick(param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onUnlockComplete() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.quicksettings.IQSTileService");
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IQSTileService.Stub.getDefaultImpl() != null) {
            IQSTileService.Stub.getDefaultImpl().onUnlockComplete();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IQSTileService param1IQSTileService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IQSTileService != null) {
          Proxy.sDefaultImpl = param1IQSTileService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IQSTileService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
