package android.service.quicksettings;

import android.graphics.drawable.Icon;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;

public final class Tile implements Parcelable {
  private IBinder mToken;
  
  private CharSequence mSubtitle;
  
  private CharSequence mStateDescription;
  
  private int mState = 1;
  
  private IQSService mService;
  
  private CharSequence mLabel;
  
  private Icon mIcon;
  
  private CharSequence mContentDescription;
  
  private static final String TAG = "Tile";
  
  public static final int STATE_UNAVAILABLE = 0;
  
  public static final int STATE_INACTIVE = 1;
  
  public static final int STATE_ACTIVE = 2;
  
  public Tile(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void setService(IQSService paramIQSService, IBinder paramIBinder) {
    this.mService = paramIQSService;
    this.mToken = paramIBinder;
  }
  
  public int getState() {
    return this.mState;
  }
  
  public void setState(int paramInt) {
    this.mState = paramInt;
  }
  
  public Icon getIcon() {
    return this.mIcon;
  }
  
  public void setIcon(Icon paramIcon) {
    this.mIcon = paramIcon;
  }
  
  public CharSequence getLabel() {
    return this.mLabel;
  }
  
  public void setLabel(CharSequence paramCharSequence) {
    this.mLabel = paramCharSequence;
  }
  
  public CharSequence getSubtitle() {
    return this.mSubtitle;
  }
  
  public void setSubtitle(CharSequence paramCharSequence) {
    this.mSubtitle = paramCharSequence;
  }
  
  public CharSequence getContentDescription() {
    return this.mContentDescription;
  }
  
  public CharSequence getStateDescription() {
    return this.mStateDescription;
  }
  
  public void setContentDescription(CharSequence paramCharSequence) {
    this.mContentDescription = paramCharSequence;
  }
  
  public void setStateDescription(CharSequence paramCharSequence) {
    this.mStateDescription = paramCharSequence;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void updateTile() {
    try {
      this.mService.updateQsTile(this, this.mToken);
    } catch (Exception exception) {
      Log.e("Tile", "Couldn't update tile");
    } 
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (this.mIcon != null) {
      paramParcel.writeByte((byte)1);
      this.mIcon.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeByte((byte)0);
    } 
    paramParcel.writeInt(this.mState);
    TextUtils.writeToParcel(this.mLabel, paramParcel, paramInt);
    TextUtils.writeToParcel(this.mSubtitle, paramParcel, paramInt);
    TextUtils.writeToParcel(this.mContentDescription, paramParcel, paramInt);
    TextUtils.writeToParcel(this.mStateDescription, paramParcel, paramInt);
  }
  
  private void readFromParcel(Parcel paramParcel) {
    if (paramParcel.readByte() != 0) {
      this.mIcon = Icon.CREATOR.createFromParcel(paramParcel);
    } else {
      this.mIcon = null;
    } 
    this.mState = paramParcel.readInt();
    this.mLabel = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    this.mSubtitle = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    this.mContentDescription = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    this.mStateDescription = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
  }
  
  public static final Parcelable.Creator<Tile> CREATOR = new Parcelable.Creator<Tile>() {
      public Tile createFromParcel(Parcel param1Parcel) {
        return new Tile(param1Parcel);
      }
      
      public Tile[] newArray(int param1Int) {
        return new Tile[param1Int];
      }
    };
  
  public Tile() {}
}
