package android.service.quicksettings;

import android.annotation.SystemApi;
import android.app.Dialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Icon;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;

public class TileService extends Service {
  public static final String ACTION_QS_TILE = "android.service.quicksettings.action.QS_TILE";
  
  public static final String ACTION_QS_TILE_PREFERENCES = "android.service.quicksettings.action.QS_TILE_PREFERENCES";
  
  public static final String ACTION_REQUEST_LISTENING = "android.service.quicksettings.action.REQUEST_LISTENING";
  
  private static final boolean DEBUG = false;
  
  public static final String EXTRA_SERVICE = "service";
  
  public static final String EXTRA_STATE = "state";
  
  public static final String EXTRA_TOKEN = "token";
  
  public static final String META_DATA_ACTIVE_TILE = "android.service.quicksettings.ACTIVE_TILE";
  
  public static final String META_DATA_TOGGLEABLE_TILE = "android.service.quicksettings.TOGGLEABLE_TILE";
  
  private static final String TAG = "TileService";
  
  private final H mHandler = new H(Looper.getMainLooper());
  
  private boolean mListening = false;
  
  private IQSService mService;
  
  private Tile mTile;
  
  private IBinder mTileToken;
  
  private IBinder mToken;
  
  private Runnable mUnlockRunnable;
  
  public void onDestroy() {
    if (this.mListening) {
      onStopListening();
      this.mListening = false;
    } 
    super.onDestroy();
  }
  
  public void onTileAdded() {}
  
  public void onTileRemoved() {}
  
  public void onStartListening() {}
  
  public void onStopListening() {}
  
  public void onClick() {}
  
  @SystemApi
  public final void setStatusIcon(Icon paramIcon, String paramString) {
    IQSService iQSService = this.mService;
    if (iQSService != null)
      try {
        iQSService.updateStatusIcon(this.mTileToken, paramIcon, paramString);
      } catch (RemoteException remoteException) {} 
  }
  
  public final void showDialog(Dialog paramDialog) {
    (paramDialog.getWindow().getAttributes()).token = this.mToken;
    paramDialog.getWindow().setType(2035);
    paramDialog.getWindow().getDecorView().addOnAttachStateChangeListener((View.OnAttachStateChangeListener)new Object(this));
    paramDialog.show();
    try {
      this.mService.onShowDialog(this.mTileToken);
    } catch (RemoteException remoteException) {}
  }
  
  public final void unlockAndRun(Runnable paramRunnable) {
    this.mUnlockRunnable = paramRunnable;
    try {
      this.mService.startUnlockAndRun(this.mTileToken);
    } catch (RemoteException remoteException) {}
  }
  
  public final boolean isSecure() {
    try {
      return this.mService.isSecure();
    } catch (RemoteException remoteException) {
      return true;
    } 
  }
  
  public final boolean isLocked() {
    try {
      return this.mService.isLocked();
    } catch (RemoteException remoteException) {
      return true;
    } 
  }
  
  public final void startActivityAndCollapse(Intent paramIntent) {
    startActivity(paramIntent);
    try {
      this.mService.onStartActivity(this.mTileToken);
    } catch (RemoteException remoteException) {}
  }
  
  public final Tile getQsTile() {
    return this.mTile;
  }
  
  public IBinder onBind(Intent paramIntent) {
    this.mService = IQSService.Stub.asInterface(paramIntent.getIBinderExtra("service"));
    IBinder iBinder = paramIntent.getIBinderExtra("token");
    try {
      Tile tile = this.mService.getTile(iBinder);
      if (tile != null) {
        tile.setService(this.mService, this.mTileToken);
        this.mHandler.sendEmptyMessage(7);
      } 
      return (IBinder)new Object(this);
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Unable to reach IQSService", remoteException);
    } 
  }
  
  class H extends Handler {
    private static final int MSG_START_LISTENING = 1;
    
    private static final int MSG_START_SUCCESS = 7;
    
    private static final int MSG_STOP_LISTENING = 2;
    
    private static final int MSG_TILE_ADDED = 3;
    
    private static final int MSG_TILE_CLICKED = 5;
    
    private static final int MSG_TILE_REMOVED = 4;
    
    private static final int MSG_UNLOCK_COMPLETE = 6;
    
    private final String mTileServiceName;
    
    final TileService this$0;
    
    public H(Looper param1Looper) {
      super(param1Looper);
      this.mTileServiceName = TileService.this.getClass().getSimpleName();
    }
    
    private void logMessage(String param1String) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this.mTileServiceName);
      stringBuilder.append(" Handler - ");
      stringBuilder.append(param1String);
      Log.d("TileService", stringBuilder.toString());
    }
    
    public void handleMessage(Message param1Message) {
      switch (param1Message.what) {
        default:
          return;
        case 7:
          try {
            TileService.this.mService.onStartSuccessful(TileService.this.mTileToken);
          } catch (RemoteException remoteException) {}
        case 6:
          if (TileService.this.mUnlockRunnable != null)
            TileService.this.mUnlockRunnable.run(); 
        case 5:
          TileService.access$402(TileService.this, (IBinder)((Message)remoteException).obj);
          TileService.this.onClick();
        case 4:
          if (TileService.this.mListening) {
            TileService.access$302(TileService.this, false);
            TileService.this.onStopListening();
          } 
          TileService.this.onTileRemoved();
        case 3:
          TileService.this.onTileAdded();
        case 2:
          if (TileService.this.mListening) {
            TileService.access$302(TileService.this, false);
            TileService.this.onStopListening();
          } 
        case 1:
          break;
      } 
      if (!TileService.this.mListening) {
        TileService.access$302(TileService.this, true);
        TileService.this.onStartListening();
      } 
    }
  }
  
  public static boolean isQuickSettingsSupported() {
    return Resources.getSystem().getBoolean(17891508);
  }
  
  public static final void requestListeningState(Context paramContext, ComponentName paramComponentName) {
    String str = paramContext.getResources().getString(17039967);
    ComponentName componentName = ComponentName.unflattenFromString(str);
    Intent intent = new Intent("android.service.quicksettings.action.REQUEST_LISTENING");
    intent.putExtra("android.intent.extra.COMPONENT_NAME", (Parcelable)paramComponentName);
    intent.setPackage(componentName.getPackageName());
    paramContext.sendBroadcast(intent, "android.permission.BIND_QUICK_SETTINGS_TILE");
  }
}
