package android.service.storage;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteCallback;
import android.os.RemoteException;
import android.os.storage.StorageVolume;

public interface IExternalStorageService extends IInterface {
  void endSession(String paramString, RemoteCallback paramRemoteCallback) throws RemoteException;
  
  void notifyVolumeStateChanged(String paramString, StorageVolume paramStorageVolume, RemoteCallback paramRemoteCallback) throws RemoteException;
  
  void startSession(String paramString1, int paramInt, ParcelFileDescriptor paramParcelFileDescriptor, String paramString2, String paramString3, RemoteCallback paramRemoteCallback) throws RemoteException;
  
  class Default implements IExternalStorageService {
    public void startSession(String param1String1, int param1Int, ParcelFileDescriptor param1ParcelFileDescriptor, String param1String2, String param1String3, RemoteCallback param1RemoteCallback) throws RemoteException {}
    
    public void endSession(String param1String, RemoteCallback param1RemoteCallback) throws RemoteException {}
    
    public void notifyVolumeStateChanged(String param1String, StorageVolume param1StorageVolume, RemoteCallback param1RemoteCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IExternalStorageService {
    private static final String DESCRIPTOR = "android.service.storage.IExternalStorageService";
    
    static final int TRANSACTION_endSession = 2;
    
    static final int TRANSACTION_notifyVolumeStateChanged = 3;
    
    static final int TRANSACTION_startSession = 1;
    
    public Stub() {
      attachInterface(this, "android.service.storage.IExternalStorageService");
    }
    
    public static IExternalStorageService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.storage.IExternalStorageService");
      if (iInterface != null && iInterface instanceof IExternalStorageService)
        return (IExternalStorageService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "notifyVolumeStateChanged";
        } 
        return "endSession";
      } 
      return "startSession";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.service.storage.IExternalStorageService");
            return true;
          } 
          param1Parcel1.enforceInterface("android.service.storage.IExternalStorageService");
          String str4 = param1Parcel1.readString();
          if (param1Parcel1.readInt() != 0) {
            StorageVolume storageVolume = StorageVolume.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel2 = null;
          } 
          if (param1Parcel1.readInt() != 0) {
            RemoteCallback remoteCallback = RemoteCallback.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          notifyVolumeStateChanged(str4, (StorageVolume)param1Parcel2, (RemoteCallback)param1Parcel1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.service.storage.IExternalStorageService");
        String str = param1Parcel1.readString();
        if (param1Parcel1.readInt() != 0) {
          RemoteCallback remoteCallback = RemoteCallback.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        endSession(str, (RemoteCallback)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.storage.IExternalStorageService");
      String str1 = param1Parcel1.readString();
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      String str2 = param1Parcel1.readString();
      String str3 = param1Parcel1.readString();
      if (param1Parcel1.readInt() != 0) {
        RemoteCallback remoteCallback = RemoteCallback.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      startSession(str1, param1Int1, (ParcelFileDescriptor)param1Parcel2, str2, str3, (RemoteCallback)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IExternalStorageService {
      public static IExternalStorageService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.storage.IExternalStorageService";
      }
      
      public void startSession(String param2String1, int param2Int, ParcelFileDescriptor param2ParcelFileDescriptor, String param2String2, String param2String3, RemoteCallback param2RemoteCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.storage.IExternalStorageService");
          try {
            parcel.writeString(param2String1);
            try {
              parcel.writeInt(param2Int);
              if (param2ParcelFileDescriptor != null) {
                parcel.writeInt(1);
                param2ParcelFileDescriptor.writeToParcel(parcel, 0);
              } else {
                parcel.writeInt(0);
              } 
              try {
                parcel.writeString(param2String2);
                try {
                  parcel.writeString(param2String3);
                  if (param2RemoteCallback != null) {
                    parcel.writeInt(1);
                    param2RemoteCallback.writeToParcel(parcel, 0);
                  } else {
                    parcel.writeInt(0);
                  } 
                  try {
                    boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
                    if (!bool && IExternalStorageService.Stub.getDefaultImpl() != null) {
                      IExternalStorageService.Stub.getDefaultImpl().startSession(param2String1, param2Int, param2ParcelFileDescriptor, param2String2, param2String3, param2RemoteCallback);
                      parcel.recycle();
                      return;
                    } 
                    parcel.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2String1;
      }
      
      public void endSession(String param2String, RemoteCallback param2RemoteCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.storage.IExternalStorageService");
          parcel.writeString(param2String);
          if (param2RemoteCallback != null) {
            parcel.writeInt(1);
            param2RemoteCallback.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IExternalStorageService.Stub.getDefaultImpl() != null) {
            IExternalStorageService.Stub.getDefaultImpl().endSession(param2String, param2RemoteCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyVolumeStateChanged(String param2String, StorageVolume param2StorageVolume, RemoteCallback param2RemoteCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.storage.IExternalStorageService");
          parcel.writeString(param2String);
          if (param2StorageVolume != null) {
            parcel.writeInt(1);
            param2StorageVolume.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2RemoteCallback != null) {
            parcel.writeInt(1);
            param2RemoteCallback.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IExternalStorageService.Stub.getDefaultImpl() != null) {
            IExternalStorageService.Stub.getDefaultImpl().notifyVolumeStateChanged(param2String, param2StorageVolume, param2RemoteCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IExternalStorageService param1IExternalStorageService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IExternalStorageService != null) {
          Proxy.sDefaultImpl = param1IExternalStorageService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IExternalStorageService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
