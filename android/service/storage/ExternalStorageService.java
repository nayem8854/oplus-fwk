package android.service.storage;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.os.ParcelableException;
import android.os.RemoteCallback;
import android.os.RemoteException;
import android.os.storage.StorageVolume;
import com.android.internal.os.BackgroundThread;
import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public abstract class ExternalStorageService extends Service {
  private final ExternalStorageServiceWrapper mWrapper = new ExternalStorageServiceWrapper();
  
  private final Handler mHandler = BackgroundThread.getHandler();
  
  public static final String SERVICE_INTERFACE = "android.service.storage.ExternalStorageService";
  
  public static final int FLAG_SESSION_TYPE_FUSE = 1;
  
  public static final int FLAG_SESSION_ATTRIBUTE_INDEXABLE = 2;
  
  public static final String EXTRA_SESSION_ID = "android.service.storage.extra.session_id";
  
  public static final String EXTRA_ERROR = "android.service.storage.extra.error";
  
  public final IBinder onBind(Intent paramIntent) {
    return this.mWrapper;
  }
  
  public abstract void onEndSession(String paramString) throws IOException;
  
  public abstract void onStartSession(String paramString, int paramInt, ParcelFileDescriptor paramParcelFileDescriptor, File paramFile1, File paramFile2) throws IOException;
  
  public abstract void onVolumeStateChanged(StorageVolume paramStorageVolume) throws IOException;
  
  private class ExternalStorageServiceWrapper extends IExternalStorageService.Stub {
    final ExternalStorageService this$0;
    
    private ExternalStorageServiceWrapper() {}
    
    public void startSession(String param1String1, int param1Int, ParcelFileDescriptor param1ParcelFileDescriptor, String param1String2, String param1String3, RemoteCallback param1RemoteCallback) throws RemoteException {
      ExternalStorageService.this.mHandler.post(new _$$Lambda$ExternalStorageService$ExternalStorageServiceWrapper$tdFhGuYAp6crc6_QZ_oDKVYQsTo(this, param1String1, param1Int, param1ParcelFileDescriptor, param1String2, param1String3, param1RemoteCallback));
    }
    
    public void notifyVolumeStateChanged(String param1String, StorageVolume param1StorageVolume, RemoteCallback param1RemoteCallback) {
      ExternalStorageService.this.mHandler.post(new _$$Lambda$ExternalStorageService$ExternalStorageServiceWrapper$QTZTHzgj4CuoCrqZdfOB1w2jnLc(this, param1StorageVolume, param1String, param1RemoteCallback));
    }
    
    public void endSession(String param1String, RemoteCallback param1RemoteCallback) throws RemoteException {
      ExternalStorageService.this.mHandler.post(new _$$Lambda$ExternalStorageService$ExternalStorageServiceWrapper$gN8yhoyB_zewcbm1c8jB01Hc5Lw(this, param1String, param1RemoteCallback));
    }
    
    private void sendResult(String param1String, Throwable param1Throwable, RemoteCallback param1RemoteCallback) {
      Bundle bundle = new Bundle();
      bundle.putString("android.service.storage.extra.session_id", param1String);
      if (param1Throwable != null)
        bundle.putParcelable("android.service.storage.extra.error", new ParcelableException(param1Throwable)); 
      param1RemoteCallback.sendResult(bundle);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class SessionFlag implements Annotation {}
}
