package android.service.settings.suggestions;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface ISuggestionService extends IInterface {
  void dismissSuggestion(Suggestion paramSuggestion) throws RemoteException;
  
  List<Suggestion> getSuggestions() throws RemoteException;
  
  void launchSuggestion(Suggestion paramSuggestion) throws RemoteException;
  
  class Default implements ISuggestionService {
    public List<Suggestion> getSuggestions() throws RemoteException {
      return null;
    }
    
    public void dismissSuggestion(Suggestion param1Suggestion) throws RemoteException {}
    
    public void launchSuggestion(Suggestion param1Suggestion) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISuggestionService {
    private static final String DESCRIPTOR = "android.service.settings.suggestions.ISuggestionService";
    
    static final int TRANSACTION_dismissSuggestion = 3;
    
    static final int TRANSACTION_getSuggestions = 2;
    
    static final int TRANSACTION_launchSuggestion = 4;
    
    public Stub() {
      attachInterface(this, "android.service.settings.suggestions.ISuggestionService");
    }
    
    public static ISuggestionService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.settings.suggestions.ISuggestionService");
      if (iInterface != null && iInterface instanceof ISuggestionService)
        return (ISuggestionService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 2) {
        if (param1Int != 3) {
          if (param1Int != 4)
            return null; 
          return "launchSuggestion";
        } 
        return "dismissSuggestion";
      } 
      return "getSuggestions";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 2) {
        if (param1Int1 != 3) {
          if (param1Int1 != 4) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.service.settings.suggestions.ISuggestionService");
            return true;
          } 
          param1Parcel1.enforceInterface("android.service.settings.suggestions.ISuggestionService");
          if (param1Parcel1.readInt() != 0) {
            Suggestion suggestion = Suggestion.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          launchSuggestion((Suggestion)param1Parcel1);
          param1Parcel2.writeNoException();
          return true;
        } 
        param1Parcel1.enforceInterface("android.service.settings.suggestions.ISuggestionService");
        if (param1Parcel1.readInt() != 0) {
          Suggestion suggestion = Suggestion.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        dismissSuggestion((Suggestion)param1Parcel1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.settings.suggestions.ISuggestionService");
      List<Suggestion> list = getSuggestions();
      param1Parcel2.writeNoException();
      param1Parcel2.writeTypedList(list);
      return true;
    }
    
    private static class Proxy implements ISuggestionService {
      public static ISuggestionService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.settings.suggestions.ISuggestionService";
      }
      
      public List<Suggestion> getSuggestions() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.settings.suggestions.ISuggestionService");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ISuggestionService.Stub.getDefaultImpl() != null)
            return ISuggestionService.Stub.getDefaultImpl().getSuggestions(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(Suggestion.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void dismissSuggestion(Suggestion param2Suggestion) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.settings.suggestions.ISuggestionService");
          if (param2Suggestion != null) {
            parcel1.writeInt(1);
            param2Suggestion.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ISuggestionService.Stub.getDefaultImpl() != null) {
            ISuggestionService.Stub.getDefaultImpl().dismissSuggestion(param2Suggestion);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void launchSuggestion(Suggestion param2Suggestion) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.settings.suggestions.ISuggestionService");
          if (param2Suggestion != null) {
            parcel1.writeInt(1);
            param2Suggestion.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ISuggestionService.Stub.getDefaultImpl() != null) {
            ISuggestionService.Stub.getDefaultImpl().launchSuggestion(param2Suggestion);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISuggestionService param1ISuggestionService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISuggestionService != null) {
          Proxy.sDefaultImpl = param1ISuggestionService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISuggestionService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
