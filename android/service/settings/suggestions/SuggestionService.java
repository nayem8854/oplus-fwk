package android.service.settings.suggestions;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import java.util.List;

@SystemApi
public abstract class SuggestionService extends Service {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "SuggestionService";
  
  public IBinder onBind(Intent paramIntent) {
    return new ISuggestionService.Stub() {
        final SuggestionService this$0;
        
        public List<Suggestion> getSuggestions() {
          return SuggestionService.this.onGetSuggestions();
        }
        
        public void dismissSuggestion(Suggestion param1Suggestion) {
          SuggestionService.this.onSuggestionDismissed(param1Suggestion);
        }
        
        public void launchSuggestion(Suggestion param1Suggestion) {
          SuggestionService.this.onSuggestionLaunched(param1Suggestion);
        }
      };
  }
  
  public abstract List<Suggestion> onGetSuggestions();
  
  public abstract void onSuggestionDismissed(Suggestion paramSuggestion);
  
  public abstract void onSuggestionLaunched(Suggestion paramSuggestion);
}
