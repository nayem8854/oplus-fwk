package android.service.settings.suggestions;

import android.annotation.SystemApi;
import android.app.PendingIntent;
import android.graphics.drawable.Icon;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public final class Suggestion implements Parcelable {
  public String getId() {
    return this.mId;
  }
  
  public CharSequence getTitle() {
    return this.mTitle;
  }
  
  public CharSequence getSummary() {
    return this.mSummary;
  }
  
  public Icon getIcon() {
    return this.mIcon;
  }
  
  public int getFlags() {
    return this.mFlags;
  }
  
  public PendingIntent getPendingIntent() {
    return this.mPendingIntent;
  }
  
  private Suggestion(Builder paramBuilder) {
    this.mId = paramBuilder.mId;
    this.mTitle = paramBuilder.mTitle;
    this.mSummary = paramBuilder.mSummary;
    this.mIcon = paramBuilder.mIcon;
    this.mFlags = paramBuilder.mFlags;
    this.mPendingIntent = paramBuilder.mPendingIntent;
  }
  
  private Suggestion(Parcel paramParcel) {
    this.mId = paramParcel.readString();
    this.mTitle = paramParcel.readCharSequence();
    this.mSummary = paramParcel.readCharSequence();
    this.mIcon = paramParcel.<Icon>readParcelable(Icon.class.getClassLoader());
    this.mFlags = paramParcel.readInt();
    this.mPendingIntent = paramParcel.<PendingIntent>readParcelable(PendingIntent.class.getClassLoader());
  }
  
  public static final Parcelable.Creator<Suggestion> CREATOR = new Parcelable.Creator<Suggestion>() {
      public Suggestion createFromParcel(Parcel param1Parcel) {
        return new Suggestion(param1Parcel);
      }
      
      public Suggestion[] newArray(int param1Int) {
        return new Suggestion[param1Int];
      }
    };
  
  public static final int FLAG_HAS_BUTTON = 1;
  
  public static final int FLAG_ICON_TINTABLE = 2;
  
  private final int mFlags;
  
  private final Icon mIcon;
  
  private final String mId;
  
  private final PendingIntent mPendingIntent;
  
  private final CharSequence mSummary;
  
  private final CharSequence mTitle;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mId);
    paramParcel.writeCharSequence(this.mTitle);
    paramParcel.writeCharSequence(this.mSummary);
    paramParcel.writeParcelable((Parcelable)this.mIcon, paramInt);
    paramParcel.writeInt(this.mFlags);
    paramParcel.writeParcelable((Parcelable)this.mPendingIntent, paramInt);
  }
  
  class Builder {
    private int mFlags;
    
    private Icon mIcon;
    
    private final String mId;
    
    private PendingIntent mPendingIntent;
    
    private CharSequence mSummary;
    
    private CharSequence mTitle;
    
    public Builder(Suggestion this$0) {
      if (!TextUtils.isEmpty((CharSequence)this$0)) {
        this.mId = (String)this$0;
        return;
      } 
      throw new IllegalArgumentException("Suggestion id cannot be empty");
    }
    
    public Builder setTitle(CharSequence param1CharSequence) {
      this.mTitle = param1CharSequence;
      return this;
    }
    
    public Builder setSummary(CharSequence param1CharSequence) {
      this.mSummary = param1CharSequence;
      return this;
    }
    
    public Builder setIcon(Icon param1Icon) {
      this.mIcon = param1Icon;
      return this;
    }
    
    public Builder setFlags(int param1Int) {
      this.mFlags = param1Int;
      return this;
    }
    
    public Builder setPendingIntent(PendingIntent param1PendingIntent) {
      this.mPendingIntent = param1PendingIntent;
      return this;
    }
    
    public Suggestion build() {
      return new Suggestion(this);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Flags implements Annotation {}
}
