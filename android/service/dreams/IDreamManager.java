package android.service.dreams;

import android.content.ComponentName;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IDreamManager extends IInterface {
  void awaken() throws RemoteException;
  
  void dream() throws RemoteException;
  
  void finishSelf(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  void forceAmbientDisplayEnabled(boolean paramBoolean) throws RemoteException;
  
  ComponentName getDefaultDreamComponentForUser(int paramInt) throws RemoteException;
  
  ComponentName[] getDreamComponents() throws RemoteException;
  
  ComponentName[] getDreamComponentsForUser(int paramInt) throws RemoteException;
  
  boolean isDreaming() throws RemoteException;
  
  void setDreamComponents(ComponentName[] paramArrayOfComponentName) throws RemoteException;
  
  void setDreamComponentsForUser(int paramInt, ComponentName[] paramArrayOfComponentName) throws RemoteException;
  
  void startDozing(IBinder paramIBinder, int paramInt1, int paramInt2) throws RemoteException;
  
  void stopDozing(IBinder paramIBinder) throws RemoteException;
  
  void testDream(int paramInt, ComponentName paramComponentName) throws RemoteException;
  
  class Default implements IDreamManager {
    public void dream() throws RemoteException {}
    
    public void awaken() throws RemoteException {}
    
    public void setDreamComponents(ComponentName[] param1ArrayOfComponentName) throws RemoteException {}
    
    public ComponentName[] getDreamComponents() throws RemoteException {
      return null;
    }
    
    public ComponentName getDefaultDreamComponentForUser(int param1Int) throws RemoteException {
      return null;
    }
    
    public void testDream(int param1Int, ComponentName param1ComponentName) throws RemoteException {}
    
    public boolean isDreaming() throws RemoteException {
      return false;
    }
    
    public void finishSelf(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {}
    
    public void startDozing(IBinder param1IBinder, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void stopDozing(IBinder param1IBinder) throws RemoteException {}
    
    public void forceAmbientDisplayEnabled(boolean param1Boolean) throws RemoteException {}
    
    public ComponentName[] getDreamComponentsForUser(int param1Int) throws RemoteException {
      return null;
    }
    
    public void setDreamComponentsForUser(int param1Int, ComponentName[] param1ArrayOfComponentName) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDreamManager {
    private static final String DESCRIPTOR = "android.service.dreams.IDreamManager";
    
    static final int TRANSACTION_awaken = 2;
    
    static final int TRANSACTION_dream = 1;
    
    static final int TRANSACTION_finishSelf = 8;
    
    static final int TRANSACTION_forceAmbientDisplayEnabled = 11;
    
    static final int TRANSACTION_getDefaultDreamComponentForUser = 5;
    
    static final int TRANSACTION_getDreamComponents = 4;
    
    static final int TRANSACTION_getDreamComponentsForUser = 12;
    
    static final int TRANSACTION_isDreaming = 7;
    
    static final int TRANSACTION_setDreamComponents = 3;
    
    static final int TRANSACTION_setDreamComponentsForUser = 13;
    
    static final int TRANSACTION_startDozing = 9;
    
    static final int TRANSACTION_stopDozing = 10;
    
    static final int TRANSACTION_testDream = 6;
    
    public Stub() {
      attachInterface(this, "android.service.dreams.IDreamManager");
    }
    
    public static IDreamManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.dreams.IDreamManager");
      if (iInterface != null && iInterface instanceof IDreamManager)
        return (IDreamManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 13:
          return "setDreamComponentsForUser";
        case 12:
          return "getDreamComponentsForUser";
        case 11:
          return "forceAmbientDisplayEnabled";
        case 10:
          return "stopDozing";
        case 9:
          return "startDozing";
        case 8:
          return "finishSelf";
        case 7:
          return "isDreaming";
        case 6:
          return "testDream";
        case 5:
          return "getDefaultDreamComponentForUser";
        case 4:
          return "getDreamComponents";
        case 3:
          return "setDreamComponents";
        case 2:
          return "awaken";
        case 1:
          break;
      } 
      return "dream";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        int i;
        ComponentName[] arrayOfComponentName2;
        IBinder iBinder1;
        ComponentName componentName, arrayOfComponentName1[];
        IBinder iBinder2;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 13:
            param1Parcel1.enforceInterface("android.service.dreams.IDreamManager");
            param1Int1 = param1Parcel1.readInt();
            arrayOfComponentName2 = param1Parcel1.<ComponentName>createTypedArray(ComponentName.CREATOR);
            setDreamComponentsForUser(param1Int1, arrayOfComponentName2);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            arrayOfComponentName2.enforceInterface("android.service.dreams.IDreamManager");
            param1Int1 = arrayOfComponentName2.readInt();
            arrayOfComponentName2 = getDreamComponentsForUser(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray(arrayOfComponentName2, 1);
            return true;
          case 11:
            arrayOfComponentName2.enforceInterface("android.service.dreams.IDreamManager");
            bool1 = bool2;
            if (arrayOfComponentName2.readInt() != 0)
              bool1 = true; 
            forceAmbientDisplayEnabled(bool1);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            arrayOfComponentName2.enforceInterface("android.service.dreams.IDreamManager");
            iBinder1 = arrayOfComponentName2.readStrongBinder();
            stopDozing(iBinder1);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            iBinder1.enforceInterface("android.service.dreams.IDreamManager");
            iBinder2 = iBinder1.readStrongBinder();
            param1Int2 = iBinder1.readInt();
            param1Int1 = iBinder1.readInt();
            startDozing(iBinder2, param1Int2, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            iBinder1.enforceInterface("android.service.dreams.IDreamManager");
            iBinder2 = iBinder1.readStrongBinder();
            if (iBinder1.readInt() != 0)
              bool1 = true; 
            finishSelf(iBinder2, bool1);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            iBinder1.enforceInterface("android.service.dreams.IDreamManager");
            bool = isDreaming();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 6:
            iBinder1.enforceInterface("android.service.dreams.IDreamManager");
            i = iBinder1.readInt();
            if (iBinder1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder1 = null;
            } 
            testDream(i, (ComponentName)iBinder1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            iBinder1.enforceInterface("android.service.dreams.IDreamManager");
            i = iBinder1.readInt();
            componentName = getDefaultDreamComponentForUser(i);
            param1Parcel2.writeNoException();
            if (componentName != null) {
              param1Parcel2.writeInt(1);
              componentName.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 4:
            componentName.enforceInterface("android.service.dreams.IDreamManager");
            arrayOfComponentName1 = getDreamComponents();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray(arrayOfComponentName1, 1);
            return true;
          case 3:
            arrayOfComponentName1.enforceInterface("android.service.dreams.IDreamManager");
            arrayOfComponentName1 = arrayOfComponentName1.<ComponentName>createTypedArray(ComponentName.CREATOR);
            setDreamComponents(arrayOfComponentName1);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            arrayOfComponentName1.enforceInterface("android.service.dreams.IDreamManager");
            awaken();
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        arrayOfComponentName1.enforceInterface("android.service.dreams.IDreamManager");
        dream();
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.service.dreams.IDreamManager");
      return true;
    }
    
    private static class Proxy implements IDreamManager {
      public static IDreamManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.dreams.IDreamManager";
      }
      
      public void dream() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.dreams.IDreamManager");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IDreamManager.Stub.getDefaultImpl() != null) {
            IDreamManager.Stub.getDefaultImpl().dream();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void awaken() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.dreams.IDreamManager");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IDreamManager.Stub.getDefaultImpl() != null) {
            IDreamManager.Stub.getDefaultImpl().awaken();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDreamComponents(ComponentName[] param2ArrayOfComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.dreams.IDreamManager");
          parcel1.writeTypedArray(param2ArrayOfComponentName, 0);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IDreamManager.Stub.getDefaultImpl() != null) {
            IDreamManager.Stub.getDefaultImpl().setDreamComponents(param2ArrayOfComponentName);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ComponentName[] getDreamComponents() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.dreams.IDreamManager");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IDreamManager.Stub.getDefaultImpl() != null)
            return IDreamManager.Stub.getDefaultImpl().getDreamComponents(); 
          parcel2.readException();
          return parcel2.<ComponentName>createTypedArray(ComponentName.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ComponentName getDefaultDreamComponentForUser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ComponentName componentName;
          parcel1.writeInterfaceToken("android.service.dreams.IDreamManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IDreamManager.Stub.getDefaultImpl() != null) {
            componentName = IDreamManager.Stub.getDefaultImpl().getDefaultDreamComponentForUser(param2Int);
            return componentName;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            componentName = ComponentName.CREATOR.createFromParcel(parcel2);
          } else {
            componentName = null;
          } 
          return componentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void testDream(int param2Int, ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.dreams.IDreamManager");
          parcel1.writeInt(param2Int);
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IDreamManager.Stub.getDefaultImpl() != null) {
            IDreamManager.Stub.getDefaultImpl().testDream(param2Int, param2ComponentName);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDreaming() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.dreams.IDreamManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IDreamManager.Stub.getDefaultImpl() != null) {
            bool1 = IDreamManager.Stub.getDefaultImpl().isDreaming();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void finishSelf(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.service.dreams.IDreamManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool1 && IDreamManager.Stub.getDefaultImpl() != null) {
            IDreamManager.Stub.getDefaultImpl().finishSelf(param2IBinder, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startDozing(IBinder param2IBinder, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.dreams.IDreamManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IDreamManager.Stub.getDefaultImpl() != null) {
            IDreamManager.Stub.getDefaultImpl().startDozing(param2IBinder, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopDozing(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.dreams.IDreamManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IDreamManager.Stub.getDefaultImpl() != null) {
            IDreamManager.Stub.getDefaultImpl().stopDozing(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void forceAmbientDisplayEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.service.dreams.IDreamManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool1 && IDreamManager.Stub.getDefaultImpl() != null) {
            IDreamManager.Stub.getDefaultImpl().forceAmbientDisplayEnabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ComponentName[] getDreamComponentsForUser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.dreams.IDreamManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IDreamManager.Stub.getDefaultImpl() != null)
            return IDreamManager.Stub.getDefaultImpl().getDreamComponentsForUser(param2Int); 
          parcel2.readException();
          return parcel2.<ComponentName>createTypedArray(ComponentName.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDreamComponentsForUser(int param2Int, ComponentName[] param2ArrayOfComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.dreams.IDreamManager");
          parcel1.writeInt(param2Int);
          parcel1.writeTypedArray(param2ArrayOfComponentName, 0);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IDreamManager.Stub.getDefaultImpl() != null) {
            IDreamManager.Stub.getDefaultImpl().setDreamComponentsForUser(param2Int, param2ArrayOfComponentName);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDreamManager param1IDreamManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDreamManager != null) {
          Proxy.sDefaultImpl = param1IDreamManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDreamManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
