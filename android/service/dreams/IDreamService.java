package android.service.dreams;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.IRemoteCallback;
import android.os.Parcel;
import android.os.RemoteException;

public interface IDreamService extends IInterface {
  void attach(IBinder paramIBinder, boolean paramBoolean, IRemoteCallback paramIRemoteCallback) throws RemoteException;
  
  void detach() throws RemoteException;
  
  void wakeUp() throws RemoteException;
  
  class Default implements IDreamService {
    public void attach(IBinder param1IBinder, boolean param1Boolean, IRemoteCallback param1IRemoteCallback) throws RemoteException {}
    
    public void detach() throws RemoteException {}
    
    public void wakeUp() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDreamService {
    private static final String DESCRIPTOR = "android.service.dreams.IDreamService";
    
    static final int TRANSACTION_attach = 1;
    
    static final int TRANSACTION_detach = 2;
    
    static final int TRANSACTION_wakeUp = 3;
    
    public Stub() {
      attachInterface(this, "android.service.dreams.IDreamService");
    }
    
    public static IDreamService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.dreams.IDreamService");
      if (iInterface != null && iInterface instanceof IDreamService)
        return (IDreamService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "wakeUp";
        } 
        return "detach";
      } 
      return "attach";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.service.dreams.IDreamService");
            return true;
          } 
          param1Parcel1.enforceInterface("android.service.dreams.IDreamService");
          wakeUp();
          return true;
        } 
        param1Parcel1.enforceInterface("android.service.dreams.IDreamService");
        detach();
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.dreams.IDreamService");
      IBinder iBinder = param1Parcel1.readStrongBinder();
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      IRemoteCallback iRemoteCallback = IRemoteCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
      attach(iBinder, bool, iRemoteCallback);
      return true;
    }
    
    private static class Proxy implements IDreamService {
      public static IDreamService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.dreams.IDreamService";
      }
      
      public void attach(IBinder param2IBinder, boolean param2Boolean, IRemoteCallback param2IRemoteCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.dreams.IDreamService");
          parcel.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2IRemoteCallback != null) {
            iBinder = param2IRemoteCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool1 = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool1 && IDreamService.Stub.getDefaultImpl() != null) {
            IDreamService.Stub.getDefaultImpl().attach(param2IBinder, param2Boolean, param2IRemoteCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void detach() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.dreams.IDreamService");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IDreamService.Stub.getDefaultImpl() != null) {
            IDreamService.Stub.getDefaultImpl().detach();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void wakeUp() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.dreams.IDreamService");
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IDreamService.Stub.getDefaultImpl() != null) {
            IDreamService.Stub.getDefaultImpl().wakeUp();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDreamService param1IDreamService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDreamService != null) {
          Proxy.sDefaultImpl = param1IDreamService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDreamService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
