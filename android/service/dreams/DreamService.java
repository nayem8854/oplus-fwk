package android.service.dreams;

import android.app.Activity;
import android.app.ActivityTaskManager;
import android.app.ActivityThread;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.IBinder;
import android.os.IRemoteCallback;
import android.os.Looper;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.Log;
import android.util.MathUtils;
import android.util.Slog;
import android.view.ActionMode;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SearchEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManagerGlobal;
import android.view.accessibility.AccessibilityEvent;
import com.android.internal.policy.PhoneWindow;
import com.android.internal.util.DumpUtils;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class DreamService extends Service implements Window.Callback {
  public static final String DREAM_META_DATA = "android.service.dream";
  
  public static final String DREAM_SERVICE = "dreams";
  
  public static final String SERVICE_INTERFACE = "android.service.dreams.DreamService";
  
  private final String TAG;
  
  private Activity mActivity;
  
  private boolean mCanDoze;
  
  private boolean mDebug;
  
  private Runnable mDispatchAfterOnAttachedToWindow;
  
  private int mDozeScreenBrightness;
  
  private int mDozeScreenState;
  
  private boolean mDozing;
  
  private final IDreamManager mDreamManager;
  
  private DreamServiceWrapper mDreamServiceWrapper;
  
  private IBinder mDreamToken;
  
  private boolean mFinished;
  
  private boolean mFullscreen;
  
  private final Handler mHandler;
  
  private boolean mInteractive;
  
  private boolean mScreenBright;
  
  private boolean mStarted;
  
  private boolean mWaking;
  
  private Window mWindow;
  
  private boolean mWindowless;
  
  public DreamService() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(DreamService.class.getSimpleName());
    stringBuilder.append("[");
    stringBuilder.append(getClass().getSimpleName());
    stringBuilder.append("]");
    this.TAG = stringBuilder.toString();
    this.mHandler = new Handler(Looper.getMainLooper());
    this.mScreenBright = true;
    this.mDozeScreenState = 0;
    this.mDozeScreenBrightness = -1;
    this.mDebug = SystemProperties.getBoolean("persist.sys.assert.panic", false);
    this.mDreamManager = IDreamManager.Stub.asInterface(ServiceManager.getService("dreams"));
  }
  
  public void setDebug(boolean paramBoolean) {
    this.mDebug = paramBoolean;
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent) {
    if (!this.mInteractive) {
      if (this.mDebug)
        Slog.v(this.TAG, "Waking up on keyEvent"); 
      wakeUp();
      return true;
    } 
    if (paramKeyEvent.getKeyCode() == 4) {
      if (this.mDebug)
        Slog.v(this.TAG, "Waking up on back key"); 
      wakeUp();
      return true;
    } 
    return this.mWindow.superDispatchKeyEvent(paramKeyEvent);
  }
  
  public boolean dispatchKeyShortcutEvent(KeyEvent paramKeyEvent) {
    if (!this.mInteractive) {
      if (this.mDebug)
        Slog.v(this.TAG, "Waking up on keyShortcutEvent"); 
      wakeUp();
      return true;
    } 
    return this.mWindow.superDispatchKeyShortcutEvent(paramKeyEvent);
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent) {
    if (!this.mInteractive) {
      if (this.mDebug)
        Slog.v(this.TAG, "Waking up on touchEvent"); 
      wakeUp();
      return true;
    } 
    return this.mWindow.superDispatchTouchEvent(paramMotionEvent);
  }
  
  public boolean dispatchTrackballEvent(MotionEvent paramMotionEvent) {
    if (!this.mInteractive) {
      if (this.mDebug)
        Slog.v(this.TAG, "Waking up on trackballEvent"); 
      wakeUp();
      return true;
    } 
    return this.mWindow.superDispatchTrackballEvent(paramMotionEvent);
  }
  
  public boolean dispatchGenericMotionEvent(MotionEvent paramMotionEvent) {
    if (!this.mInteractive) {
      if (this.mDebug)
        Slog.v(this.TAG, "Waking up on genericMotionEvent"); 
      wakeUp();
      return true;
    } 
    return this.mWindow.superDispatchGenericMotionEvent(paramMotionEvent);
  }
  
  public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    return false;
  }
  
  public View onCreatePanelView(int paramInt) {
    return null;
  }
  
  public boolean onCreatePanelMenu(int paramInt, Menu paramMenu) {
    return false;
  }
  
  public boolean onPreparePanel(int paramInt, View paramView, Menu paramMenu) {
    return false;
  }
  
  public boolean onMenuOpened(int paramInt, Menu paramMenu) {
    return false;
  }
  
  public boolean onMenuItemSelected(int paramInt, MenuItem paramMenuItem) {
    return false;
  }
  
  public void onWindowAttributesChanged(WindowManager.LayoutParams paramLayoutParams) {}
  
  public void onContentChanged() {}
  
  public void onWindowFocusChanged(boolean paramBoolean) {}
  
  public void onAttachedToWindow() {}
  
  public void onDetachedFromWindow() {}
  
  public void onPanelClosed(int paramInt, Menu paramMenu) {}
  
  public boolean onSearchRequested(SearchEvent paramSearchEvent) {
    return onSearchRequested();
  }
  
  public boolean onSearchRequested() {
    return false;
  }
  
  public ActionMode onWindowStartingActionMode(ActionMode.Callback paramCallback) {
    return null;
  }
  
  public ActionMode onWindowStartingActionMode(ActionMode.Callback paramCallback, int paramInt) {
    return null;
  }
  
  public void onActionModeStarted(ActionMode paramActionMode) {}
  
  public void onActionModeFinished(ActionMode paramActionMode) {}
  
  public WindowManager getWindowManager() {
    Window window = this.mWindow;
    if (window != null) {
      WindowManager windowManager = window.getWindowManager();
    } else {
      window = null;
    } 
    return (WindowManager)window;
  }
  
  public Window getWindow() {
    return this.mWindow;
  }
  
  public void setContentView(int paramInt) {
    getWindow().setContentView(paramInt);
  }
  
  public void setContentView(View paramView) {
    getWindow().setContentView(paramView);
  }
  
  public void setContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams) {
    getWindow().setContentView(paramView, paramLayoutParams);
  }
  
  public void addContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams) {
    getWindow().addContentView(paramView, paramLayoutParams);
  }
  
  public <T extends View> T findViewById(int paramInt) {
    return (T)getWindow().findViewById(paramInt);
  }
  
  public final <T extends View> T requireViewById(int paramInt) {
    T t = (T)findViewById(paramInt);
    if (t != null)
      return t; 
    throw new IllegalArgumentException("ID does not reference a View inside this DreamService");
  }
  
  public void setInteractive(boolean paramBoolean) {
    this.mInteractive = paramBoolean;
  }
  
  public boolean isInteractive() {
    return this.mInteractive;
  }
  
  public void setFullscreen(boolean paramBoolean) {
    if (this.mFullscreen != paramBoolean) {
      boolean bool;
      this.mFullscreen = paramBoolean;
      if (paramBoolean) {
        bool = true;
      } else {
        bool = false;
      } 
      applyWindowFlags(bool, 1024);
    } 
  }
  
  public boolean isFullscreen() {
    return this.mFullscreen;
  }
  
  public void setScreenBright(boolean paramBoolean) {
    if (this.mScreenBright != paramBoolean) {
      boolean bool;
      this.mScreenBright = paramBoolean;
      if (paramBoolean) {
        bool = true;
      } else {
        bool = false;
      } 
      applyWindowFlags(bool, 128);
    } 
  }
  
  public boolean isScreenBright() {
    return getWindowFlagValue(128, this.mScreenBright);
  }
  
  public void setWindowless(boolean paramBoolean) {
    this.mWindowless = paramBoolean;
  }
  
  public boolean isWindowless() {
    return this.mWindowless;
  }
  
  public boolean canDoze() {
    return this.mCanDoze;
  }
  
  public void startDozing() {
    if (this.mCanDoze && !this.mDozing) {
      this.mDozing = true;
      updateDoze();
    } 
  }
  
  private void updateDoze() {
    IBinder iBinder = this.mDreamToken;
    if (iBinder == null) {
      Slog.w(this.TAG, "Updating doze without a dream token.");
      return;
    } 
    if (this.mDozing)
      try {
        this.mDreamManager.startDozing(iBinder, this.mDozeScreenState, this.mDozeScreenBrightness);
      } catch (RemoteException remoteException) {} 
  }
  
  public void stopDozing() {
    if (this.mDozing) {
      this.mDozing = false;
      try {
        this.mDreamManager.stopDozing(this.mDreamToken);
      } catch (RemoteException remoteException) {}
    } 
  }
  
  public boolean isDozing() {
    return this.mDozing;
  }
  
  public int getDozeScreenState() {
    return this.mDozeScreenState;
  }
  
  public void setDozeScreenState(int paramInt) {
    if (this.mDozeScreenState != paramInt) {
      this.mDozeScreenState = paramInt;
      updateDoze();
    } 
  }
  
  public int getDozeScreenBrightness() {
    return this.mDozeScreenBrightness;
  }
  
  public void setDozeScreenBrightness(int paramInt) {
    int i = paramInt;
    if (paramInt != -1)
      i = clampAbsoluteBrightness(paramInt); 
    if (this.mDozeScreenBrightness != i) {
      this.mDozeScreenBrightness = i;
      updateDoze();
    } 
  }
  
  public void onCreate() {
    if (this.mDebug)
      Slog.v(this.TAG, "onCreate()"); 
    super.onCreate();
  }
  
  public void onDreamingStarted() {
    if (this.mDebug)
      Slog.v(this.TAG, "onDreamingStarted()"); 
  }
  
  public void onDreamingStopped() {
    if (this.mDebug)
      Slog.v(this.TAG, "onDreamingStopped()"); 
  }
  
  public void onWakeUp() {
    finish();
  }
  
  public final IBinder onBind(Intent paramIntent) {
    if (this.mDebug) {
      String str = this.TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onBind() intent = ");
      stringBuilder.append(paramIntent);
      Slog.v(str, stringBuilder.toString());
    } 
    DreamServiceWrapper dreamServiceWrapper = new DreamServiceWrapper();
    return dreamServiceWrapper;
  }
  
  public final void finish() {
    if (this.mDebug) {
      String str = this.TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("finish(): mFinished=");
      stringBuilder.append(this.mFinished);
      Slog.v(str, stringBuilder.toString());
    } 
    if (isCtsTest()) {
      Activity activity = this.mActivity;
      if (activity != null) {
        if (!activity.isFinishing())
          activity.finishAndRemoveTask(); 
        return;
      } 
    } 
    if (this.mFinished)
      return; 
    this.mFinished = true;
    IBinder iBinder = this.mDreamToken;
    if (iBinder == null) {
      Slog.w(this.TAG, "Finish was called before the dream was attached.");
      stopSelf();
      return;
    } 
    try {
      this.mDreamManager.finishSelf(iBinder, true);
    } catch (RemoteException remoteException) {}
    stopSelf();
  }
  
  public final void wakeUp() {
    wakeUp(false);
  }
  
  private void wakeUp(boolean paramBoolean) {
    if (this.mDebug) {
      String str = this.TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("wakeUp(): fromSystem=");
      stringBuilder.append(paramBoolean);
      stringBuilder.append(", mWaking=");
      stringBuilder.append(this.mWaking);
      stringBuilder.append(", mFinished=");
      stringBuilder.append(this.mFinished);
      Slog.v(str, stringBuilder.toString());
    } 
    if (!this.mWaking && !this.mFinished) {
      this.mWaking = true;
      onWakeUp();
      if (!paramBoolean && !this.mFinished)
        if (isCtsTest()) {
          if (this.mActivity == null) {
            Slog.w(this.TAG, "WakeUp was called before the dream was attached.");
          } else {
            try {
              this.mDreamManager.finishSelf(this.mDreamToken, false);
            } catch (RemoteException remoteException) {}
          } 
        } else {
          IBinder iBinder = this.mDreamToken;
          if (iBinder == null) {
            Slog.w(this.TAG, "WakeUp was called before the dream was attached.");
          } else {
            try {
              this.mDreamManager.finishSelf(iBinder, false);
            } catch (RemoteException remoteException) {}
          } 
        }  
    } 
  }
  
  public void onDestroy() {
    if (this.mDebug)
      Slog.v(this.TAG, "onDestroy()"); 
    detach();
    super.onDestroy();
  }
  
  private final void detach() {
    if (this.mStarted) {
      if (this.mDebug)
        Slog.v(this.TAG, "detach(): Calling onDreamingStopped()"); 
      this.mStarted = false;
      onDreamingStopped();
    } 
    if (isCtsTest()) {
      Activity activity = this.mActivity;
      if (activity != null && !activity.isFinishing()) {
        this.mActivity.finishAndRemoveTask();
      } else {
        finish();
      } 
      this.mDreamToken = null;
      this.mCanDoze = false;
    } else {
      if (this.mWindow != null) {
        if (this.mDebug)
          Slog.v(this.TAG, "detach(): Removing window from window manager"); 
        this.mWindow.getWindowManager().removeViewImmediate(this.mWindow.getDecorView());
        this.mWindow = null;
      } 
      if (this.mDreamToken != null) {
        WindowManagerGlobal windowManagerGlobal = WindowManagerGlobal.getInstance();
        IBinder iBinder = this.mDreamToken;
        String str = getClass().getName();
        windowManagerGlobal.closeAll(iBinder, str, "Dream");
        this.mDreamToken = null;
        this.mCanDoze = false;
      } 
    } 
  }
  
  private void attach(IBinder paramIBinder, boolean paramBoolean, IRemoteCallback paramIRemoteCallback) {
    String str;
    StringBuilder stringBuilder;
    if (this.mDreamToken != null) {
      str = this.TAG;
      stringBuilder = new StringBuilder();
      stringBuilder.append("attach() called when dream with token=");
      stringBuilder.append(this.mDreamToken);
      stringBuilder.append(" already attached");
      Slog.e(str, stringBuilder.toString());
      return;
    } 
    if (this.mFinished || this.mWaking) {
      Slog.w(this.TAG, "attach() called after dream already finished");
      try {
        this.mDreamManager.finishSelf((IBinder)str, true);
      } catch (RemoteException remoteException) {}
      return;
    } 
    this.mDreamToken = (IBinder)remoteException;
    this.mCanDoze = paramBoolean;
    if (!this.mWindowless || paramBoolean) {
      if (!isCtsTest() && 
        !this.mWindowless) {
        PhoneWindow phoneWindow = new PhoneWindow((Context)this);
        phoneWindow.setCallback(this);
        this.mWindow.requestFeature(1);
        this.mWindow.setBackgroundDrawable((Drawable)new ColorDrawable(-16777216));
        this.mWindow.setFormat(-2);
        paramBoolean = this.mDebug;
        char c = Character.MIN_VALUE;
        if (paramBoolean) {
          String str1 = this.TAG;
          Slog.v(str1, String.format("Attaching window token: %s to window of type %s", new Object[] { remoteException, Integer.valueOf(2015) }));
        } 
        WindowManager.LayoutParams layoutParams = this.mWindow.getAttributes();
        layoutParams.type = 2015;
        int i = layoutParams.flags;
        if (this.mFullscreen) {
          bool = true;
        } else {
          bool = false;
        } 
        if (this.mScreenBright)
          c = ''; 
        layoutParams.flags = i | c | bool | 0x100;
        layoutParams.privateFlags |= 0x10;
        this.mWindow.setAttributes(layoutParams);
        this.mWindow.clearFlags(-2147483648);
        this.mWindow.setWindowManager(null, (IBinder)remoteException, "dream", true);
        boolean bool = false;
        try {
          getWindowManager().addView(this.mWindow.getDecorView(), (ViewGroup.LayoutParams)this.mWindow.getAttributes());
        } catch (android.view.WindowManager.BadTokenException badTokenException) {
          Slog.i(this.TAG, "attach() called after window token already removed, dream will finish soon");
          this.mWindow = null;
          bool = true;
        } 
        if (bool) {
          try {
            this.mDreamManager.finishSelf(this.mDreamToken, true);
          } catch (RemoteException remoteException1) {}
          return;
        } 
      } 
      if (isCtsTest()) {
        this.mDispatchAfterOnAttachedToWindow = new _$$Lambda$DreamService$uqzqbrpptkCH_tc1jkx48W1A6vM(this, (IRemoteCallback)stringBuilder);
      } else {
        this.mHandler.post(new _$$Lambda$DreamService$sX_5kSZe0JgtearBJflirkhmy1g(this, (IRemoteCallback)stringBuilder));
      } 
      if (isCtsTest())
        if (!this.mWindowless) {
          Intent intent = new Intent((Context)this, DreamActivity.class);
          intent.setPackage(getApplicationContext().getPackageName());
          intent.setFlags(268435456);
          intent.putExtra("binder", this.mDreamServiceWrapper);
          try {
            if (!ActivityTaskManager.getService().startDreamActivity(intent)) {
              detach();
              return;
            } 
          } catch (RemoteException remoteException1) {
            Log.w(this.TAG, "Could not connect to activity task manager to start dream activity");
            remoteException1.rethrowFromSystemServer();
          } 
        } else {
          this.mDispatchAfterOnAttachedToWindow.run();
        }  
      return;
    } 
    throw new IllegalStateException("Only doze dreams can be windowless");
  }
  
  private void onWindowCreated(Window paramWindow) {
    boolean bool1;
    this.mWindow = paramWindow;
    paramWindow.setCallback(this);
    this.mWindow.requestFeature(1);
    WindowManager.LayoutParams layoutParams = this.mWindow.getAttributes();
    int i = layoutParams.flags;
    boolean bool = this.mFullscreen;
    char c = Character.MIN_VALUE;
    if (bool) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    if (this.mScreenBright)
      c = ''; 
    layoutParams.flags = i | bool1 | 0x1490101 | c;
    this.mWindow.setAttributes(layoutParams);
    this.mWindow.clearFlags(-2147483648);
    this.mWindow.getDecorView().addOnAttachStateChangeListener((View.OnAttachStateChangeListener)new Object(this));
  }
  
  private boolean getWindowFlagValue(int paramInt, boolean paramBoolean) {
    Window window = this.mWindow;
    if (window != null)
      if (((window.getAttributes()).flags & paramInt) != 0) {
        paramBoolean = true;
      } else {
        paramBoolean = false;
      }  
    return paramBoolean;
  }
  
  private void applyWindowFlags(int paramInt1, int paramInt2) {
    Window window = this.mWindow;
    if (window != null) {
      WindowManager.LayoutParams layoutParams = window.getAttributes();
      layoutParams.flags = applyFlags(layoutParams.flags, paramInt1, paramInt2);
      layoutParams.privateFlags |= 0x10;
      this.mWindow.setAttributes(layoutParams);
      this.mWindow.getWindowManager().updateViewLayout(this.mWindow.getDecorView(), (ViewGroup.LayoutParams)layoutParams);
    } 
  }
  
  private int applyFlags(int paramInt1, int paramInt2, int paramInt3) {
    return (paramInt3 ^ 0xFFFFFFFF) & paramInt1 | paramInt2 & paramInt3;
  }
  
  protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    DumpUtils.dumpAsync(this.mHandler, (DumpUtils.Dump)new Object(this, paramFileDescriptor, paramArrayOfString), paramPrintWriter, "", 1000L);
  }
  
  protected void dumpOnHandler(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.TAG);
    stringBuilder.append(": ");
    paramPrintWriter.print(stringBuilder.toString());
    if (this.mDreamToken == null) {
      paramPrintWriter.println("stopped");
    } else {
      stringBuilder = new StringBuilder();
      stringBuilder.append("running (dreamToken=");
      stringBuilder.append(this.mDreamToken);
      stringBuilder.append(")");
      paramPrintWriter.println(stringBuilder.toString());
    } 
    stringBuilder = new StringBuilder();
    stringBuilder.append("  window: ");
    stringBuilder.append(this.mWindow);
    paramPrintWriter.println(stringBuilder.toString());
    paramPrintWriter.print("  flags:");
    if (isInteractive())
      paramPrintWriter.print(" interactive"); 
    if (isFullscreen())
      paramPrintWriter.print(" fullscreen"); 
    if (isScreenBright())
      paramPrintWriter.print(" bright"); 
    if (isWindowless())
      paramPrintWriter.print(" windowless"); 
    if (isDozing()) {
      paramPrintWriter.print(" dozing");
    } else if (canDoze()) {
      paramPrintWriter.print(" candoze");
    } 
    paramPrintWriter.println();
    if (canDoze()) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("  doze screen state: ");
      stringBuilder.append(Display.stateToString(this.mDozeScreenState));
      paramPrintWriter.println(stringBuilder.toString());
      stringBuilder = new StringBuilder();
      stringBuilder.append("  doze screen brightness: ");
      stringBuilder.append(this.mDozeScreenBrightness);
      paramPrintWriter.println(stringBuilder.toString());
    } 
  }
  
  private static int clampAbsoluteBrightness(int paramInt) {
    return MathUtils.constrain(paramInt, 0, PowerManager.BRIGHTNESS_MULTIBITS_ON);
  }
  
  final class DreamServiceWrapper extends IDreamService.Stub {
    final DreamService this$0;
    
    public void attach(IBinder param1IBinder, boolean param1Boolean, IRemoteCallback param1IRemoteCallback) {
      DreamService.this.mHandler.post(new _$$Lambda$DreamService$DreamServiceWrapper$fKPQo1GZX03b5_nMg_IFcuuxhKs(this, param1IBinder, param1Boolean, param1IRemoteCallback));
    }
    
    public void detach() {
      if (DreamService.this.isCtsTest()) {
        DreamService.this.mHandler.post(new _$$Lambda$DreamService$DreamServiceWrapper$2R9T6zqLOUvx4z5eu1pSMWsejpU(DreamService.this));
      } else {
        DreamService.this.mHandler.post(new _$$Lambda$DreamService$DreamServiceWrapper$hJOtjpMLm6_DNo_Y__ha10dnTAY(this));
      } 
    }
    
    public void wakeUp() {
      DreamService.this.mHandler.post(new _$$Lambda$DreamService$DreamServiceWrapper$2Ev6a_R6X_WporX1DFe8eWAPk4E(this));
    }
    
    void onActivityCreated(DreamActivity param1DreamActivity) {
      DreamService.access$102(DreamService.this, param1DreamActivity);
      DreamService.this.onWindowCreated(param1DreamActivity.getWindow());
    }
  }
  
  private boolean isCtsTest() {
    ActivityThread activityThread = ActivityThread.currentActivityThread();
    if (activityThread == null)
      return false; 
    String str = activityThread.getApplication().getPackageName();
    if ("android.server.wm.app".equals(str))
      return true; 
    return false;
  }
}
