package android.service.dreams;

import android.app.Activity;
import android.os.Bundle;
import android.view.WindowInsets;

public class DreamActivity extends Activity {
  static final String EXTRA_CALLBACK = "binder";
  
  public void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    DreamService.DreamServiceWrapper dreamServiceWrapper = (DreamService.DreamServiceWrapper)getIntent().getIBinderExtra("binder");
    if (dreamServiceWrapper != null)
      dreamServiceWrapper.onActivityCreated(this); 
  }
  
  public void onResume() {
    super.onResume();
    getWindow().getInsetsController().hide(WindowInsets.Type.systemBars());
    overridePendingTransition(17432624, 17432625);
  }
  
  public void finishAndRemoveTask() {
    super.finishAndRemoveTask();
    overridePendingTransition(0, 17432623);
  }
}
