package android.service.dreams;

import android.content.ComponentName;

public abstract class DreamManagerInternal {
  public abstract ComponentName getActiveDreamComponent(boolean paramBoolean);
  
  public abstract boolean isDreaming();
  
  public abstract void startDream(boolean paramBoolean);
  
  public abstract void stopDream(boolean paramBoolean);
}
