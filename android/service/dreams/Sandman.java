package android.service.dreams;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Slog;

public final class Sandman {
  private static final String TAG = "Sandman";
  
  public static boolean shouldStartDockApp(Context paramContext, Intent paramIntent) {
    boolean bool;
    String str = paramContext.getResources().getString(17039966);
    ComponentName componentName2 = ComponentName.unflattenFromString(str);
    ComponentName componentName1 = paramIntent.resolveActivity(paramContext.getPackageManager());
    if (componentName1 != null && !componentName1.equals(componentName2)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static void startDreamByUserRequest(Context paramContext) {
    startDream(paramContext, false);
  }
  
  public static void startDreamWhenDockedIfAppropriate(Context paramContext) {
    if (!isScreenSaverEnabled(paramContext) || 
      !isScreenSaverActivatedOnDock(paramContext)) {
      Slog.i("Sandman", "Dreams currently disabled for docks.");
      return;
    } 
    startDream(paramContext, true);
  }
  
  private static void startDream(Context paramContext, boolean paramBoolean) {
    try {
      IBinder iBinder = ServiceManager.getService("dreams");
      IDreamManager iDreamManager = IDreamManager.Stub.asInterface(iBinder);
      if (iDreamManager != null && !iDreamManager.isDreaming()) {
        if (paramBoolean) {
          Slog.i("Sandman", "Activating dream while docked.");
          PowerManager powerManager = (PowerManager)paramContext.getSystemService(PowerManager.class);
          powerManager.wakeUp(SystemClock.uptimeMillis(), 3, "android.service.dreams:DREAM");
        } else {
          Slog.i("Sandman", "Activating dream by user request.");
        } 
        iDreamManager.dream();
      } 
    } catch (RemoteException remoteException) {
      Slog.e("Sandman", "Could not start dream when docked.", (Throwable)remoteException);
    } 
  }
  
  private static boolean isScreenSaverEnabled(Context paramContext) {
    boolean bool2;
    boolean bool = paramContext.getResources().getBoolean(17891429);
    boolean bool1 = true;
    if (bool) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (Settings.Secure.getIntForUser(paramContext.getContentResolver(), "screensaver_enabled", bool2, -2) == 0)
      bool1 = false; 
    return bool1;
  }
  
  private static boolean isScreenSaverActivatedOnDock(Context paramContext) {
    boolean bool2;
    boolean bool = paramContext.getResources().getBoolean(17891427);
    boolean bool1 = true;
    if (bool) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (Settings.Secure.getIntForUser(paramContext.getContentResolver(), "screensaver_activate_on_dock", bool2, -2) == 0)
      bool1 = false; 
    return bool1;
  }
}
