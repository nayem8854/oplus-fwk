package android.service.trust;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.os.RemoteException;
import android.os.UserHandle;
import android.os.UserManager;
import android.util.Log;
import android.util.Slog;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

@SystemApi
public class TrustAgentService extends Service {
  private static final boolean DEBUG = false;
  
  private static final String EXTRA_TOKEN = "token";
  
  private static final String EXTRA_TOKEN_HANDLE = "token_handle";
  
  private static final String EXTRA_TOKEN_REMOVED_RESULT = "token_removed_result";
  
  private static final String EXTRA_TOKEN_STATE = "token_state";
  
  private static final String EXTRA_USER_HANDLE = "user_handle";
  
  public static final int FLAG_GRANT_TRUST_DISMISS_KEYGUARD = 2;
  
  public static final int FLAG_GRANT_TRUST_INITIATED_BY_USER = 1;
  
  private static final int MSG_CONFIGURE = 2;
  
  private static final int MSG_DEVICE_LOCKED = 4;
  
  private static final int MSG_DEVICE_UNLOCKED = 5;
  
  private static final int MSG_ESCROW_TOKEN_ADDED = 7;
  
  private static final int MSG_ESCROW_TOKEN_REMOVED = 9;
  
  private static final int MSG_ESCROW_TOKEN_STATE_RECEIVED = 8;
  
  private static final int MSG_TRUST_TIMEOUT = 3;
  
  private static final int MSG_UNLOCK_ATTEMPT = 1;
  
  private static final int MSG_UNLOCK_LOCKOUT = 6;
  
  public static final String SERVICE_INTERFACE = "android.service.trust.TrustAgentService";
  
  public static final int TOKEN_STATE_ACTIVE = 1;
  
  public static final int TOKEN_STATE_INACTIVE = 0;
  
  public static final String TRUST_AGENT_META_DATA = "android.service.trust.trustagent";
  
  private final String TAG;
  
  private ITrustAgentServiceCallback mCallback;
  
  private Handler mHandler;
  
  private final Object mLock;
  
  private boolean mManagingTrust;
  
  private Runnable mPendingGrantTrustTask;
  
  public TrustAgentService() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(TrustAgentService.class.getSimpleName());
    stringBuilder.append("[");
    stringBuilder.append(getClass().getSimpleName());
    stringBuilder.append("]");
    this.TAG = stringBuilder.toString();
    this.mLock = new Object();
    this.mHandler = (Handler)new Object(this);
  }
  
  class ConfigurationData {
    final List<PersistableBundle> options;
    
    final IBinder token;
    
    ConfigurationData(TrustAgentService this$0, IBinder param1IBinder) {
      this.options = (List<PersistableBundle>)this$0;
      this.token = param1IBinder;
    }
  }
  
  public void onCreate() {
    super.onCreate();
    ComponentName componentName = new ComponentName(this, getClass());
    try {
      ServiceInfo serviceInfo = getPackageManager().getServiceInfo(componentName, 0);
      if (!"android.permission.BIND_TRUST_AGENT".equals(serviceInfo.permission)) {
        IllegalStateException illegalStateException = new IllegalStateException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(componentName.flattenToShortString());
        stringBuilder.append(" is not declared with the permission \"");
        stringBuilder.append("android.permission.BIND_TRUST_AGENT");
        stringBuilder.append("\"");
        this(stringBuilder.toString());
        throw illegalStateException;
      } 
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      String str = this.TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Can't get ServiceInfo for ");
      stringBuilder.append(componentName.toShortString());
      Log.e(str, stringBuilder.toString());
    } 
  }
  
  public void onUnlockAttempt(boolean paramBoolean) {}
  
  public void onTrustTimeout() {}
  
  public void onDeviceLocked() {}
  
  public void onDeviceUnlocked() {}
  
  public void onDeviceUnlockLockout(long paramLong) {}
  
  public void onEscrowTokenAdded(byte[] paramArrayOfbyte, long paramLong, UserHandle paramUserHandle) {}
  
  public void onEscrowTokenStateReceived(long paramLong, int paramInt) {}
  
  public void onEscrowTokenRemoved(long paramLong, boolean paramBoolean) {}
  
  private void onError(String paramString) {
    String str = this.TAG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Remote exception while ");
    stringBuilder.append(paramString);
    Slog.v(str, stringBuilder.toString());
  }
  
  public boolean onConfigure(List<PersistableBundle> paramList) {
    return false;
  }
  
  @Deprecated
  public final void grantTrust(CharSequence paramCharSequence, long paramLong, boolean paramBoolean) {
    grantTrust(paramCharSequence, paramLong, paramBoolean);
  }
  
  public final void grantTrust(CharSequence paramCharSequence, long paramLong, int paramInt) {
    synchronized (this.mLock) {
      if (this.mManagingTrust) {
        ITrustAgentServiceCallback iTrustAgentServiceCallback = this.mCallback;
        if (iTrustAgentServiceCallback != null) {
          try {
            this.mCallback.grantTrust(paramCharSequence.toString(), paramLong, paramInt);
          } catch (RemoteException remoteException) {
            onError("calling enableTrust()");
          } 
        } else {
          Object object = new Object();
          super(this, (CharSequence)remoteException, paramLong, paramInt);
          this.mPendingGrantTrustTask = (Runnable)object;
        } 
        return;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Cannot grant trust if agent is not managing trust. Call setManagingTrust(true) first.");
      throw illegalStateException;
    } 
  }
  
  public final void revokeTrust() {
    synchronized (this.mLock) {
      if (this.mPendingGrantTrustTask != null)
        this.mPendingGrantTrustTask = null; 
      ITrustAgentServiceCallback iTrustAgentServiceCallback = this.mCallback;
      if (iTrustAgentServiceCallback != null)
        try {
          this.mCallback.revokeTrust();
        } catch (RemoteException remoteException) {
          onError("calling revokeTrust()");
        }  
      return;
    } 
  }
  
  public final void setManagingTrust(boolean paramBoolean) {
    synchronized (this.mLock) {
      if (this.mManagingTrust != paramBoolean) {
        this.mManagingTrust = paramBoolean;
        ITrustAgentServiceCallback iTrustAgentServiceCallback = this.mCallback;
        if (iTrustAgentServiceCallback != null)
          try {
            this.mCallback.setManagingTrust(paramBoolean);
          } catch (RemoteException remoteException) {
            onError("calling setManagingTrust()");
          }  
      } 
      return;
    } 
  }
  
  public final void addEscrowToken(byte[] paramArrayOfbyte, UserHandle paramUserHandle) {
    synchronized (this.mLock) {
      ITrustAgentServiceCallback iTrustAgentServiceCallback = this.mCallback;
      if (iTrustAgentServiceCallback != null) {
        try {
          this.mCallback.addEscrowToken(paramArrayOfbyte, paramUserHandle.getIdentifier());
        } catch (RemoteException remoteException) {
          onError("calling addEscrowToken");
        } 
        return;
      } 
      Slog.w(this.TAG, "Cannot add escrow token if the agent is not connecting to framework");
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Trust agent is not connected");
      throw illegalStateException;
    } 
  }
  
  public final void isEscrowTokenActive(long paramLong, UserHandle paramUserHandle) {
    synchronized (this.mLock) {
      ITrustAgentServiceCallback iTrustAgentServiceCallback = this.mCallback;
      if (iTrustAgentServiceCallback != null) {
        try {
          this.mCallback.isEscrowTokenActive(paramLong, paramUserHandle.getIdentifier());
        } catch (RemoteException remoteException) {
          onError("calling isEscrowTokenActive");
        } 
        return;
      } 
      Slog.w(this.TAG, "Cannot add escrow token if the agent is not connecting to framework");
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Trust agent is not connected");
      throw illegalStateException;
    } 
  }
  
  public final void removeEscrowToken(long paramLong, UserHandle paramUserHandle) {
    synchronized (this.mLock) {
      ITrustAgentServiceCallback iTrustAgentServiceCallback = this.mCallback;
      if (iTrustAgentServiceCallback != null) {
        try {
          this.mCallback.removeEscrowToken(paramLong, paramUserHandle.getIdentifier());
        } catch (RemoteException remoteException) {
          onError("callling removeEscrowToken");
        } 
        return;
      } 
      Slog.w(this.TAG, "Cannot add escrow token if the agent is not connecting to framework");
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Trust agent is not connected");
      throw illegalStateException;
    } 
  }
  
  public final void unlockUserWithToken(long paramLong, byte[] paramArrayOfbyte, UserHandle paramUserHandle) {
    UserManager userManager = (UserManager)getSystemService("user");
    if (userManager.isUserUnlocked(paramUserHandle)) {
      Slog.i(this.TAG, "User already unlocked");
      return;
    } 
    synchronized (this.mLock) {
      ITrustAgentServiceCallback iTrustAgentServiceCallback = this.mCallback;
      if (iTrustAgentServiceCallback != null) {
        try {
          this.mCallback.unlockUserWithToken(paramLong, paramArrayOfbyte, paramUserHandle.getIdentifier());
        } catch (RemoteException remoteException) {
          onError("calling unlockUserWithToken");
        } 
        return;
      } 
      Slog.w(this.TAG, "Cannot add escrow token if the agent is not connecting to framework");
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Trust agent is not connected");
      throw illegalStateException;
    } 
  }
  
  public final void showKeyguardErrorMessage(CharSequence paramCharSequence) {
    if (paramCharSequence != null)
      synchronized (this.mLock) {
        ITrustAgentServiceCallback iTrustAgentServiceCallback = this.mCallback;
        if (iTrustAgentServiceCallback != null) {
          try {
            this.mCallback.showKeyguardErrorMessage(paramCharSequence);
          } catch (RemoteException remoteException) {
            onError("calling showKeyguardErrorMessage");
          } 
          return;
        } 
        Slog.w(this.TAG, "Cannot show message because service is not connected to framework.");
        IllegalStateException illegalStateException = new IllegalStateException();
        this("Trust agent is not connected");
        throw illegalStateException;
      }  
    throw new IllegalArgumentException("message cannot be null");
  }
  
  public final IBinder onBind(Intent paramIntent) {
    return (IBinder)new TrustAgentServiceWrapper();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class GrantTrustFlags implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class TokenState implements Annotation {}
  
  private final class TrustAgentServiceWrapper extends ITrustAgentService.Stub {
    final TrustAgentService this$0;
    
    private TrustAgentServiceWrapper() {}
    
    public void onUnlockAttempt(boolean param1Boolean) {
      TrustAgentService.this.mHandler.obtainMessage(1, param1Boolean, 0).sendToTarget();
    }
    
    public void onUnlockLockout(int param1Int) {
      TrustAgentService.this.mHandler.obtainMessage(6, param1Int, 0).sendToTarget();
    }
    
    public void onTrustTimeout() {
      TrustAgentService.this.mHandler.sendEmptyMessage(3);
    }
    
    public void onConfigure(List<PersistableBundle> param1List, IBinder param1IBinder) {
      Message message = TrustAgentService.this.mHandler.obtainMessage(2, new TrustAgentService.ConfigurationData(param1List, param1IBinder));
      message.sendToTarget();
    }
    
    public void onDeviceLocked() throws RemoteException {
      TrustAgentService.this.mHandler.obtainMessage(4).sendToTarget();
    }
    
    public void onDeviceUnlocked() throws RemoteException {
      TrustAgentService.this.mHandler.obtainMessage(5).sendToTarget();
    }
    
    public void setCallback(ITrustAgentServiceCallback param1ITrustAgentServiceCallback) {
      synchronized (TrustAgentService.this.mLock) {
        TrustAgentService.access$102(TrustAgentService.this, param1ITrustAgentServiceCallback);
        boolean bool = TrustAgentService.this.mManagingTrust;
        if (bool)
          try {
            TrustAgentService.this.mCallback.setManagingTrust(TrustAgentService.this.mManagingTrust);
          } catch (RemoteException remoteException) {
            TrustAgentService.this.onError("calling setManagingTrust()");
          }  
        if (TrustAgentService.this.mPendingGrantTrustTask != null) {
          TrustAgentService.this.mPendingGrantTrustTask.run();
          TrustAgentService.access$602(TrustAgentService.this, (Runnable)null);
        } 
        return;
      } 
    }
    
    public void onEscrowTokenAdded(byte[] param1ArrayOfbyte, long param1Long, UserHandle param1UserHandle) {
      Message message = TrustAgentService.this.mHandler.obtainMessage(7);
      message.getData().putByteArray("token", param1ArrayOfbyte);
      message.getData().putLong("token_handle", param1Long);
      message.getData().putParcelable("user_handle", (Parcelable)param1UserHandle);
      message.sendToTarget();
    }
    
    public void onTokenStateReceived(long param1Long, int param1Int) {
      Message message = TrustAgentService.this.mHandler.obtainMessage(8);
      message.getData().putLong("token_handle", param1Long);
      message.getData().putInt("token_state", param1Int);
      message.sendToTarget();
    }
    
    public void onEscrowTokenRemoved(long param1Long, boolean param1Boolean) {
      Message message = TrustAgentService.this.mHandler.obtainMessage(9);
      message.getData().putLong("token_handle", param1Long);
      message.getData().putBoolean("token_removed_result", param1Boolean);
      message.sendToTarget();
    }
  }
}
