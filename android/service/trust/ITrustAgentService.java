package android.service.trust;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.PersistableBundle;
import android.os.RemoteException;
import android.os.UserHandle;
import java.util.ArrayList;
import java.util.List;

public interface ITrustAgentService extends IInterface {
  void onConfigure(List<PersistableBundle> paramList, IBinder paramIBinder) throws RemoteException;
  
  void onDeviceLocked() throws RemoteException;
  
  void onDeviceUnlocked() throws RemoteException;
  
  void onEscrowTokenAdded(byte[] paramArrayOfbyte, long paramLong, UserHandle paramUserHandle) throws RemoteException;
  
  void onEscrowTokenRemoved(long paramLong, boolean paramBoolean) throws RemoteException;
  
  void onTokenStateReceived(long paramLong, int paramInt) throws RemoteException;
  
  void onTrustTimeout() throws RemoteException;
  
  void onUnlockAttempt(boolean paramBoolean) throws RemoteException;
  
  void onUnlockLockout(int paramInt) throws RemoteException;
  
  void setCallback(ITrustAgentServiceCallback paramITrustAgentServiceCallback) throws RemoteException;
  
  class Default implements ITrustAgentService {
    public void onUnlockAttempt(boolean param1Boolean) throws RemoteException {}
    
    public void onUnlockLockout(int param1Int) throws RemoteException {}
    
    public void onTrustTimeout() throws RemoteException {}
    
    public void onDeviceLocked() throws RemoteException {}
    
    public void onDeviceUnlocked() throws RemoteException {}
    
    public void onConfigure(List<PersistableBundle> param1List, IBinder param1IBinder) throws RemoteException {}
    
    public void setCallback(ITrustAgentServiceCallback param1ITrustAgentServiceCallback) throws RemoteException {}
    
    public void onEscrowTokenAdded(byte[] param1ArrayOfbyte, long param1Long, UserHandle param1UserHandle) throws RemoteException {}
    
    public void onTokenStateReceived(long param1Long, int param1Int) throws RemoteException {}
    
    public void onEscrowTokenRemoved(long param1Long, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITrustAgentService {
    private static final String DESCRIPTOR = "android.service.trust.ITrustAgentService";
    
    static final int TRANSACTION_onConfigure = 6;
    
    static final int TRANSACTION_onDeviceLocked = 4;
    
    static final int TRANSACTION_onDeviceUnlocked = 5;
    
    static final int TRANSACTION_onEscrowTokenAdded = 8;
    
    static final int TRANSACTION_onEscrowTokenRemoved = 10;
    
    static final int TRANSACTION_onTokenStateReceived = 9;
    
    static final int TRANSACTION_onTrustTimeout = 3;
    
    static final int TRANSACTION_onUnlockAttempt = 1;
    
    static final int TRANSACTION_onUnlockLockout = 2;
    
    static final int TRANSACTION_setCallback = 7;
    
    public Stub() {
      attachInterface(this, "android.service.trust.ITrustAgentService");
    }
    
    public static ITrustAgentService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.trust.ITrustAgentService");
      if (iInterface != null && iInterface instanceof ITrustAgentService)
        return (ITrustAgentService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 10:
          return "onEscrowTokenRemoved";
        case 9:
          return "onTokenStateReceived";
        case 8:
          return "onEscrowTokenAdded";
        case 7:
          return "setCallback";
        case 6:
          return "onConfigure";
        case 5:
          return "onDeviceUnlocked";
        case 4:
          return "onDeviceLocked";
        case 3:
          return "onTrustTimeout";
        case 2:
          return "onUnlockLockout";
        case 1:
          break;
      } 
      return "onUnlockAttempt";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ArrayList<PersistableBundle> arrayList;
      if (param1Int1 != 1598968902) {
        ITrustAgentServiceCallback iTrustAgentServiceCallback;
        IBinder iBinder;
        byte[] arrayOfByte;
        long l;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 10:
            param1Parcel1.enforceInterface("android.service.trust.ITrustAgentService");
            l = param1Parcel1.readLong();
            if (param1Parcel1.readInt() != 0)
              bool2 = true; 
            onEscrowTokenRemoved(l, bool2);
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.service.trust.ITrustAgentService");
            l = param1Parcel1.readLong();
            param1Int1 = param1Parcel1.readInt();
            onTokenStateReceived(l, param1Int1);
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.service.trust.ITrustAgentService");
            arrayOfByte = param1Parcel1.createByteArray();
            l = param1Parcel1.readLong();
            if (param1Parcel1.readInt() != 0) {
              UserHandle userHandle = (UserHandle)UserHandle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onEscrowTokenAdded(arrayOfByte, l, (UserHandle)param1Parcel1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.service.trust.ITrustAgentService");
            iTrustAgentServiceCallback = ITrustAgentServiceCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            setCallback(iTrustAgentServiceCallback);
            return true;
          case 6:
            iTrustAgentServiceCallback.enforceInterface("android.service.trust.ITrustAgentService");
            arrayList = iTrustAgentServiceCallback.createTypedArrayList(PersistableBundle.CREATOR);
            iBinder = iTrustAgentServiceCallback.readStrongBinder();
            onConfigure(arrayList, iBinder);
            return true;
          case 5:
            iBinder.enforceInterface("android.service.trust.ITrustAgentService");
            onDeviceUnlocked();
            return true;
          case 4:
            iBinder.enforceInterface("android.service.trust.ITrustAgentService");
            onDeviceLocked();
            return true;
          case 3:
            iBinder.enforceInterface("android.service.trust.ITrustAgentService");
            onTrustTimeout();
            return true;
          case 2:
            iBinder.enforceInterface("android.service.trust.ITrustAgentService");
            param1Int1 = iBinder.readInt();
            onUnlockLockout(param1Int1);
            return true;
          case 1:
            break;
        } 
        iBinder.enforceInterface("android.service.trust.ITrustAgentService");
        bool2 = bool1;
        if (iBinder.readInt() != 0)
          bool2 = true; 
        onUnlockAttempt(bool2);
        return true;
      } 
      arrayList.writeString("android.service.trust.ITrustAgentService");
      return true;
    }
    
    private static class Proxy implements ITrustAgentService {
      public static ITrustAgentService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.trust.ITrustAgentService";
      }
      
      public void onUnlockAttempt(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.service.trust.ITrustAgentService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && ITrustAgentService.Stub.getDefaultImpl() != null) {
            ITrustAgentService.Stub.getDefaultImpl().onUnlockAttempt(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onUnlockLockout(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.trust.ITrustAgentService");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && ITrustAgentService.Stub.getDefaultImpl() != null) {
            ITrustAgentService.Stub.getDefaultImpl().onUnlockLockout(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTrustTimeout() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.trust.ITrustAgentService");
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && ITrustAgentService.Stub.getDefaultImpl() != null) {
            ITrustAgentService.Stub.getDefaultImpl().onTrustTimeout();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDeviceLocked() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.trust.ITrustAgentService");
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && ITrustAgentService.Stub.getDefaultImpl() != null) {
            ITrustAgentService.Stub.getDefaultImpl().onDeviceLocked();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDeviceUnlocked() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.trust.ITrustAgentService");
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && ITrustAgentService.Stub.getDefaultImpl() != null) {
            ITrustAgentService.Stub.getDefaultImpl().onDeviceUnlocked();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onConfigure(List<PersistableBundle> param2List, IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.trust.ITrustAgentService");
          parcel.writeTypedList(param2List);
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && ITrustAgentService.Stub.getDefaultImpl() != null) {
            ITrustAgentService.Stub.getDefaultImpl().onConfigure(param2List, param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setCallback(ITrustAgentServiceCallback param2ITrustAgentServiceCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.trust.ITrustAgentService");
          if (param2ITrustAgentServiceCallback != null) {
            iBinder = param2ITrustAgentServiceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && ITrustAgentService.Stub.getDefaultImpl() != null) {
            ITrustAgentService.Stub.getDefaultImpl().setCallback(param2ITrustAgentServiceCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onEscrowTokenAdded(byte[] param2ArrayOfbyte, long param2Long, UserHandle param2UserHandle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.trust.ITrustAgentService");
          parcel.writeByteArray(param2ArrayOfbyte);
          parcel.writeLong(param2Long);
          if (param2UserHandle != null) {
            parcel.writeInt(1);
            param2UserHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && ITrustAgentService.Stub.getDefaultImpl() != null) {
            ITrustAgentService.Stub.getDefaultImpl().onEscrowTokenAdded(param2ArrayOfbyte, param2Long, param2UserHandle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTokenStateReceived(long param2Long, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.trust.ITrustAgentService");
          parcel.writeLong(param2Long);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && ITrustAgentService.Stub.getDefaultImpl() != null) {
            ITrustAgentService.Stub.getDefaultImpl().onTokenStateReceived(param2Long, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onEscrowTokenRemoved(long param2Long, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.service.trust.ITrustAgentService");
          parcel.writeLong(param2Long);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(10, parcel, null, 1);
          if (!bool1 && ITrustAgentService.Stub.getDefaultImpl() != null) {
            ITrustAgentService.Stub.getDefaultImpl().onEscrowTokenRemoved(param2Long, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITrustAgentService param1ITrustAgentService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITrustAgentService != null) {
          Proxy.sDefaultImpl = param1ITrustAgentService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITrustAgentService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
