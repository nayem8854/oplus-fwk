package android.service.trust;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.text.TextUtils;

public interface ITrustAgentServiceCallback extends IInterface {
  void addEscrowToken(byte[] paramArrayOfbyte, int paramInt) throws RemoteException;
  
  void grantTrust(CharSequence paramCharSequence, long paramLong, int paramInt) throws RemoteException;
  
  void isEscrowTokenActive(long paramLong, int paramInt) throws RemoteException;
  
  void onConfigureCompleted(boolean paramBoolean, IBinder paramIBinder) throws RemoteException;
  
  void removeEscrowToken(long paramLong, int paramInt) throws RemoteException;
  
  void revokeTrust() throws RemoteException;
  
  void setManagingTrust(boolean paramBoolean) throws RemoteException;
  
  void showKeyguardErrorMessage(CharSequence paramCharSequence) throws RemoteException;
  
  void unlockUserWithToken(long paramLong, byte[] paramArrayOfbyte, int paramInt) throws RemoteException;
  
  class Default implements ITrustAgentServiceCallback {
    public void grantTrust(CharSequence param1CharSequence, long param1Long, int param1Int) throws RemoteException {}
    
    public void revokeTrust() throws RemoteException {}
    
    public void setManagingTrust(boolean param1Boolean) throws RemoteException {}
    
    public void onConfigureCompleted(boolean param1Boolean, IBinder param1IBinder) throws RemoteException {}
    
    public void addEscrowToken(byte[] param1ArrayOfbyte, int param1Int) throws RemoteException {}
    
    public void isEscrowTokenActive(long param1Long, int param1Int) throws RemoteException {}
    
    public void removeEscrowToken(long param1Long, int param1Int) throws RemoteException {}
    
    public void unlockUserWithToken(long param1Long, byte[] param1ArrayOfbyte, int param1Int) throws RemoteException {}
    
    public void showKeyguardErrorMessage(CharSequence param1CharSequence) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITrustAgentServiceCallback {
    private static final String DESCRIPTOR = "android.service.trust.ITrustAgentServiceCallback";
    
    static final int TRANSACTION_addEscrowToken = 5;
    
    static final int TRANSACTION_grantTrust = 1;
    
    static final int TRANSACTION_isEscrowTokenActive = 6;
    
    static final int TRANSACTION_onConfigureCompleted = 4;
    
    static final int TRANSACTION_removeEscrowToken = 7;
    
    static final int TRANSACTION_revokeTrust = 2;
    
    static final int TRANSACTION_setManagingTrust = 3;
    
    static final int TRANSACTION_showKeyguardErrorMessage = 9;
    
    static final int TRANSACTION_unlockUserWithToken = 8;
    
    public Stub() {
      attachInterface(this, "android.service.trust.ITrustAgentServiceCallback");
    }
    
    public static ITrustAgentServiceCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.trust.ITrustAgentServiceCallback");
      if (iInterface != null && iInterface instanceof ITrustAgentServiceCallback)
        return (ITrustAgentServiceCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 9:
          return "showKeyguardErrorMessage";
        case 8:
          return "unlockUserWithToken";
        case 7:
          return "removeEscrowToken";
        case 6:
          return "isEscrowTokenActive";
        case 5:
          return "addEscrowToken";
        case 4:
          return "onConfigureCompleted";
        case 3:
          return "setManagingTrust";
        case 2:
          return "revokeTrust";
        case 1:
          break;
      } 
      return "grantTrust";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      byte[] arrayOfByte;
      if (param1Int1 != 1598968902) {
        IBinder iBinder;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 9:
            param1Parcel1.enforceInterface("android.service.trust.ITrustAgentServiceCallback");
            if (param1Parcel1.readInt() != 0) {
              CharSequence charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            showKeyguardErrorMessage((CharSequence)param1Parcel1);
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.service.trust.ITrustAgentServiceCallback");
            l = param1Parcel1.readLong();
            arrayOfByte = param1Parcel1.createByteArray();
            param1Int1 = param1Parcel1.readInt();
            unlockUserWithToken(l, arrayOfByte, param1Int1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.service.trust.ITrustAgentServiceCallback");
            l = param1Parcel1.readLong();
            param1Int1 = param1Parcel1.readInt();
            removeEscrowToken(l, param1Int1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.service.trust.ITrustAgentServiceCallback");
            l = param1Parcel1.readLong();
            param1Int1 = param1Parcel1.readInt();
            isEscrowTokenActive(l, param1Int1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.service.trust.ITrustAgentServiceCallback");
            arrayOfByte = param1Parcel1.createByteArray();
            param1Int1 = param1Parcel1.readInt();
            addEscrowToken(arrayOfByte, param1Int1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.service.trust.ITrustAgentServiceCallback");
            bool1 = bool2;
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            iBinder = param1Parcel1.readStrongBinder();
            onConfigureCompleted(bool1, iBinder);
            return true;
          case 3:
            iBinder.enforceInterface("android.service.trust.ITrustAgentServiceCallback");
            if (iBinder.readInt() != 0)
              bool1 = true; 
            setManagingTrust(bool1);
            return true;
          case 2:
            iBinder.enforceInterface("android.service.trust.ITrustAgentServiceCallback");
            revokeTrust();
            return true;
          case 1:
            break;
        } 
        iBinder.enforceInterface("android.service.trust.ITrustAgentServiceCallback");
        if (iBinder.readInt() != 0) {
          CharSequence charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)iBinder);
        } else {
          arrayOfByte = null;
        } 
        long l = iBinder.readLong();
        param1Int1 = iBinder.readInt();
        grantTrust((CharSequence)arrayOfByte, l, param1Int1);
        return true;
      } 
      arrayOfByte.writeString("android.service.trust.ITrustAgentServiceCallback");
      return true;
    }
    
    private static class Proxy implements ITrustAgentServiceCallback {
      public static ITrustAgentServiceCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.trust.ITrustAgentServiceCallback";
      }
      
      public void grantTrust(CharSequence param2CharSequence, long param2Long, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.trust.ITrustAgentServiceCallback");
          if (param2CharSequence != null) {
            parcel.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeLong(param2Long);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ITrustAgentServiceCallback.Stub.getDefaultImpl() != null) {
            ITrustAgentServiceCallback.Stub.getDefaultImpl().grantTrust(param2CharSequence, param2Long, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void revokeTrust() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.trust.ITrustAgentServiceCallback");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && ITrustAgentServiceCallback.Stub.getDefaultImpl() != null) {
            ITrustAgentServiceCallback.Stub.getDefaultImpl().revokeTrust();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setManagingTrust(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.service.trust.ITrustAgentServiceCallback");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel, null, 1);
          if (!bool1 && ITrustAgentServiceCallback.Stub.getDefaultImpl() != null) {
            ITrustAgentServiceCallback.Stub.getDefaultImpl().setManagingTrust(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onConfigureCompleted(boolean param2Boolean, IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.service.trust.ITrustAgentServiceCallback");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeStrongBinder(param2IBinder);
          boolean bool1 = this.mRemote.transact(4, parcel, null, 1);
          if (!bool1 && ITrustAgentServiceCallback.Stub.getDefaultImpl() != null) {
            ITrustAgentServiceCallback.Stub.getDefaultImpl().onConfigureCompleted(param2Boolean, param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void addEscrowToken(byte[] param2ArrayOfbyte, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.trust.ITrustAgentServiceCallback");
          parcel.writeByteArray(param2ArrayOfbyte);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && ITrustAgentServiceCallback.Stub.getDefaultImpl() != null) {
            ITrustAgentServiceCallback.Stub.getDefaultImpl().addEscrowToken(param2ArrayOfbyte, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void isEscrowTokenActive(long param2Long, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.trust.ITrustAgentServiceCallback");
          parcel.writeLong(param2Long);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && ITrustAgentServiceCallback.Stub.getDefaultImpl() != null) {
            ITrustAgentServiceCallback.Stub.getDefaultImpl().isEscrowTokenActive(param2Long, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeEscrowToken(long param2Long, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.trust.ITrustAgentServiceCallback");
          parcel.writeLong(param2Long);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && ITrustAgentServiceCallback.Stub.getDefaultImpl() != null) {
            ITrustAgentServiceCallback.Stub.getDefaultImpl().removeEscrowToken(param2Long, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void unlockUserWithToken(long param2Long, byte[] param2ArrayOfbyte, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.trust.ITrustAgentServiceCallback");
          parcel.writeLong(param2Long);
          parcel.writeByteArray(param2ArrayOfbyte);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && ITrustAgentServiceCallback.Stub.getDefaultImpl() != null) {
            ITrustAgentServiceCallback.Stub.getDefaultImpl().unlockUserWithToken(param2Long, param2ArrayOfbyte, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void showKeyguardErrorMessage(CharSequence param2CharSequence) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.trust.ITrustAgentServiceCallback");
          if (param2CharSequence != null) {
            parcel.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && ITrustAgentServiceCallback.Stub.getDefaultImpl() != null) {
            ITrustAgentServiceCallback.Stub.getDefaultImpl().showKeyguardErrorMessage(param2CharSequence);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITrustAgentServiceCallback param1ITrustAgentServiceCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITrustAgentServiceCallback != null) {
          Proxy.sDefaultImpl = param1ITrustAgentServiceCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITrustAgentServiceCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
