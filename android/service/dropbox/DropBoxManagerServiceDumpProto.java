package android.service.dropbox;

public final class DropBoxManagerServiceDumpProto {
  public static final long ENTRIES = 2246267895809L;
  
  class Entry {
    public static final long DATA = 1151051235330L;
    
    public static final long TIME_MS = 1112396529665L;
    
    final DropBoxManagerServiceDumpProto this$0;
  }
}
