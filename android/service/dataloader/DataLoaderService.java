package android.service.dataloader;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Intent;
import android.content.pm.DataLoaderParams;
import android.content.pm.DataLoaderParamsParcel;
import android.content.pm.FileSystemControlParcel;
import android.content.pm.IDataLoader;
import android.content.pm.IDataLoaderStatusListener;
import android.content.pm.InstallationFile;
import android.content.pm.InstallationFileParcel;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.util.ExceptionUtils;
import android.util.Slog;
import java.io.IOException;
import java.util.Collection;
import libcore.io.IoUtils;

@SystemApi
public abstract class DataLoaderService extends Service {
  private static final String TAG = "DataLoaderService";
  
  private final DataLoaderBinderService mBinder = new DataLoaderBinderService();
  
  @SystemApi
  public DataLoader onCreateDataLoader(DataLoaderParams paramDataLoaderParams) {
    return null;
  }
  
  public final IBinder onBind(Intent paramIntent) {
    return (IBinder)this.mBinder;
  }
  
  private native boolean nativeCreateDataLoader(int paramInt, FileSystemControlParcel paramFileSystemControlParcel, DataLoaderParamsParcel paramDataLoaderParamsParcel, IDataLoaderStatusListener paramIDataLoaderStatusListener);
  
  private native boolean nativeDestroyDataLoader(int paramInt);
  
  private native boolean nativePrepareImage(int paramInt, InstallationFileParcel[] paramArrayOfInstallationFileParcel, String[] paramArrayOfString);
  
  private native boolean nativeStartDataLoader(int paramInt);
  
  private native boolean nativeStopDataLoader(int paramInt);
  
  private static native void nativeWriteData(long paramLong1, String paramString, long paramLong2, long paramLong3, ParcelFileDescriptor paramParcelFileDescriptor);
  
  @SystemApi
  class DataLoader {
    public abstract boolean onCreate(DataLoaderParams param1DataLoaderParams, DataLoaderService.FileSystemConnector param1FileSystemConnector);
    
    public abstract boolean onPrepareImage(Collection<InstallationFile> param1Collection, Collection<String> param1Collection1);
  }
  
  private class DataLoaderBinderService extends IDataLoader.Stub {
    final DataLoaderService this$0;
    
    private DataLoaderBinderService() {}
    
    public void create(int param1Int, DataLoaderParamsParcel param1DataLoaderParamsParcel, FileSystemControlParcel param1FileSystemControlParcel, IDataLoaderStatusListener param1IDataLoaderStatusListener) throws RuntimeException {
      try {
        DataLoaderService.this.nativeCreateDataLoader(param1Int, param1FileSystemControlParcel, param1DataLoaderParamsParcel, param1IDataLoaderStatusListener);
        if (param1FileSystemControlParcel.incremental != null) {
          IoUtils.closeQuietly(param1FileSystemControlParcel.incremental.cmd);
          IoUtils.closeQuietly(param1FileSystemControlParcel.incremental.pendingReads);
          IoUtils.closeQuietly(param1FileSystemControlParcel.incremental.log);
        } 
        return;
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Failed to create native loader for ");
        stringBuilder.append(param1Int);
        Slog.e("DataLoaderService", stringBuilder.toString(), exception);
        destroy(param1Int);
        RuntimeException runtimeException = new RuntimeException();
        this(exception);
        throw runtimeException;
      } finally {}
      if (param1FileSystemControlParcel.incremental != null) {
        IoUtils.closeQuietly(param1FileSystemControlParcel.incremental.cmd);
        IoUtils.closeQuietly(param1FileSystemControlParcel.incremental.pendingReads);
        IoUtils.closeQuietly(param1FileSystemControlParcel.incremental.log);
      } 
      throw param1DataLoaderParamsParcel;
    }
    
    public void start(int param1Int) {
      if (!DataLoaderService.this.nativeStartDataLoader(param1Int)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to start loader: ");
        stringBuilder.append(param1Int);
        Slog.e("DataLoaderService", stringBuilder.toString());
      } 
    }
    
    public void stop(int param1Int) {
      if (!DataLoaderService.this.nativeStopDataLoader(param1Int)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to stop loader: ");
        stringBuilder.append(param1Int);
        Slog.w("DataLoaderService", stringBuilder.toString());
      } 
    }
    
    public void destroy(int param1Int) {
      if (!DataLoaderService.this.nativeDestroyDataLoader(param1Int)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to destroy loader: ");
        stringBuilder.append(param1Int);
        Slog.w("DataLoaderService", stringBuilder.toString());
      } 
    }
    
    public void prepareImage(int param1Int, InstallationFileParcel[] param1ArrayOfInstallationFileParcel, String[] param1ArrayOfString) {
      if (!DataLoaderService.this.nativePrepareImage(param1Int, param1ArrayOfInstallationFileParcel, param1ArrayOfString)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to prepare image for data loader: ");
        stringBuilder.append(param1Int);
        Slog.w("DataLoaderService", stringBuilder.toString());
      } 
    }
  }
  
  @SystemApi
  class FileSystemConnector {
    private final long mNativeInstance;
    
    FileSystemConnector(DataLoaderService this$0) {
      this.mNativeInstance = this$0;
    }
    
    public void writeData(String param1String, long param1Long1, long param1Long2, ParcelFileDescriptor param1ParcelFileDescriptor) throws IOException {
      try {
        DataLoaderService.nativeWriteData(this.mNativeInstance, param1String, param1Long1, param1Long2, param1ParcelFileDescriptor);
        return;
      } catch (RuntimeException runtimeException) {
        ExceptionUtils.maybeUnwrapIOException(runtimeException);
        throw runtimeException;
      } 
    }
  }
}
