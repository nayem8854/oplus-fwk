package android.service.vr;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IVrStateCallbacks extends IInterface {
  void onVrStateChanged(boolean paramBoolean) throws RemoteException;
  
  class Default implements IVrStateCallbacks {
    public void onVrStateChanged(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVrStateCallbacks {
    private static final String DESCRIPTOR = "android.service.vr.IVrStateCallbacks";
    
    static final int TRANSACTION_onVrStateChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.service.vr.IVrStateCallbacks");
    }
    
    public static IVrStateCallbacks asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.vr.IVrStateCallbacks");
      if (iInterface != null && iInterface instanceof IVrStateCallbacks)
        return (IVrStateCallbacks)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onVrStateChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.service.vr.IVrStateCallbacks");
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.vr.IVrStateCallbacks");
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      onVrStateChanged(bool);
      return true;
    }
    
    private static class Proxy implements IVrStateCallbacks {
      public static IVrStateCallbacks sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.vr.IVrStateCallbacks";
      }
      
      public void onVrStateChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.service.vr.IVrStateCallbacks");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && IVrStateCallbacks.Stub.getDefaultImpl() != null) {
            IVrStateCallbacks.Stub.getDefaultImpl().onVrStateChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVrStateCallbacks param1IVrStateCallbacks) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVrStateCallbacks != null) {
          Proxy.sDefaultImpl = param1IVrStateCallbacks;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVrStateCallbacks getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
