package android.service.vr;

import android.content.ComponentName;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IVrListener extends IInterface {
  void focusedActivityChanged(ComponentName paramComponentName, boolean paramBoolean, int paramInt) throws RemoteException;
  
  class Default implements IVrListener {
    public void focusedActivityChanged(ComponentName param1ComponentName, boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVrListener {
    private static final String DESCRIPTOR = "android.service.vr.IVrListener";
    
    static final int TRANSACTION_focusedActivityChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.service.vr.IVrListener");
    }
    
    public static IVrListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.vr.IVrListener");
      if (iInterface != null && iInterface instanceof IVrListener)
        return (IVrListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "focusedActivityChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.service.vr.IVrListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.vr.IVrListener");
      if (param1Parcel1.readInt() != 0) {
        ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      param1Int1 = param1Parcel1.readInt();
      focusedActivityChanged((ComponentName)param1Parcel2, bool, param1Int1);
      return true;
    }
    
    private static class Proxy implements IVrListener {
      public static IVrListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.vr.IVrListener";
      }
      
      public void focusedActivityChanged(ComponentName param2ComponentName, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.vr.IVrListener");
          boolean bool = false;
          if (param2ComponentName != null) {
            parcel.writeInt(1);
            param2ComponentName.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          parcel.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && IVrListener.Stub.getDefaultImpl() != null) {
            IVrListener.Stub.getDefaultImpl().focusedActivityChanged(param2ComponentName, param2Boolean, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVrListener param1IVrListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVrListener != null) {
          Proxy.sDefaultImpl = param1IVrListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVrListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
