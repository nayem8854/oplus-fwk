package android.service.vr;

import android.app.Vr2dDisplayProperties;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IVrManager extends IInterface {
  boolean getPersistentVrModeEnabled() throws RemoteException;
  
  int getVr2dDisplayId() throws RemoteException;
  
  boolean getVrModeState() throws RemoteException;
  
  void registerListener(IVrStateCallbacks paramIVrStateCallbacks) throws RemoteException;
  
  void registerPersistentVrStateListener(IPersistentVrStateCallbacks paramIPersistentVrStateCallbacks) throws RemoteException;
  
  void setAndBindCompositor(String paramString) throws RemoteException;
  
  void setPersistentVrModeEnabled(boolean paramBoolean) throws RemoteException;
  
  void setStandbyEnabled(boolean paramBoolean) throws RemoteException;
  
  void setVr2dDisplayProperties(Vr2dDisplayProperties paramVr2dDisplayProperties) throws RemoteException;
  
  void unregisterListener(IVrStateCallbacks paramIVrStateCallbacks) throws RemoteException;
  
  void unregisterPersistentVrStateListener(IPersistentVrStateCallbacks paramIPersistentVrStateCallbacks) throws RemoteException;
  
  class Default implements IVrManager {
    public void registerListener(IVrStateCallbacks param1IVrStateCallbacks) throws RemoteException {}
    
    public void unregisterListener(IVrStateCallbacks param1IVrStateCallbacks) throws RemoteException {}
    
    public void registerPersistentVrStateListener(IPersistentVrStateCallbacks param1IPersistentVrStateCallbacks) throws RemoteException {}
    
    public void unregisterPersistentVrStateListener(IPersistentVrStateCallbacks param1IPersistentVrStateCallbacks) throws RemoteException {}
    
    public boolean getVrModeState() throws RemoteException {
      return false;
    }
    
    public boolean getPersistentVrModeEnabled() throws RemoteException {
      return false;
    }
    
    public void setPersistentVrModeEnabled(boolean param1Boolean) throws RemoteException {}
    
    public void setVr2dDisplayProperties(Vr2dDisplayProperties param1Vr2dDisplayProperties) throws RemoteException {}
    
    public int getVr2dDisplayId() throws RemoteException {
      return 0;
    }
    
    public void setAndBindCompositor(String param1String) throws RemoteException {}
    
    public void setStandbyEnabled(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVrManager {
    private static final String DESCRIPTOR = "android.service.vr.IVrManager";
    
    static final int TRANSACTION_getPersistentVrModeEnabled = 6;
    
    static final int TRANSACTION_getVr2dDisplayId = 9;
    
    static final int TRANSACTION_getVrModeState = 5;
    
    static final int TRANSACTION_registerListener = 1;
    
    static final int TRANSACTION_registerPersistentVrStateListener = 3;
    
    static final int TRANSACTION_setAndBindCompositor = 10;
    
    static final int TRANSACTION_setPersistentVrModeEnabled = 7;
    
    static final int TRANSACTION_setStandbyEnabled = 11;
    
    static final int TRANSACTION_setVr2dDisplayProperties = 8;
    
    static final int TRANSACTION_unregisterListener = 2;
    
    static final int TRANSACTION_unregisterPersistentVrStateListener = 4;
    
    public Stub() {
      attachInterface(this, "android.service.vr.IVrManager");
    }
    
    public static IVrManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.vr.IVrManager");
      if (iInterface != null && iInterface instanceof IVrManager)
        return (IVrManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 11:
          return "setStandbyEnabled";
        case 10:
          return "setAndBindCompositor";
        case 9:
          return "getVr2dDisplayId";
        case 8:
          return "setVr2dDisplayProperties";
        case 7:
          return "setPersistentVrModeEnabled";
        case 6:
          return "getPersistentVrModeEnabled";
        case 5:
          return "getVrModeState";
        case 4:
          return "unregisterPersistentVrStateListener";
        case 3:
          return "registerPersistentVrStateListener";
        case 2:
          return "unregisterListener";
        case 1:
          break;
      } 
      return "registerListener";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        String str;
        IPersistentVrStateCallbacks iPersistentVrStateCallbacks;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 11:
            param1Parcel1.enforceInterface("android.service.vr.IVrManager");
            if (param1Parcel1.readInt() != 0)
              bool2 = true; 
            setStandbyEnabled(bool2);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            param1Parcel1.enforceInterface("android.service.vr.IVrManager");
            str = param1Parcel1.readString();
            setAndBindCompositor(str);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            str.enforceInterface("android.service.vr.IVrManager");
            param1Int1 = getVr2dDisplayId();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 8:
            str.enforceInterface("android.service.vr.IVrManager");
            if (str.readInt() != 0) {
              Vr2dDisplayProperties vr2dDisplayProperties = (Vr2dDisplayProperties)Vr2dDisplayProperties.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            setVr2dDisplayProperties((Vr2dDisplayProperties)str);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            str.enforceInterface("android.service.vr.IVrManager");
            bool2 = bool1;
            if (str.readInt() != 0)
              bool2 = true; 
            setPersistentVrModeEnabled(bool2);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            str.enforceInterface("android.service.vr.IVrManager");
            bool = getPersistentVrModeEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 5:
            str.enforceInterface("android.service.vr.IVrManager");
            bool = getVrModeState();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 4:
            str.enforceInterface("android.service.vr.IVrManager");
            iPersistentVrStateCallbacks = IPersistentVrStateCallbacks.Stub.asInterface(str.readStrongBinder());
            unregisterPersistentVrStateListener(iPersistentVrStateCallbacks);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            iPersistentVrStateCallbacks.enforceInterface("android.service.vr.IVrManager");
            iPersistentVrStateCallbacks = IPersistentVrStateCallbacks.Stub.asInterface(iPersistentVrStateCallbacks.readStrongBinder());
            registerPersistentVrStateListener(iPersistentVrStateCallbacks);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            iPersistentVrStateCallbacks.enforceInterface("android.service.vr.IVrManager");
            iVrStateCallbacks = IVrStateCallbacks.Stub.asInterface(iPersistentVrStateCallbacks.readStrongBinder());
            unregisterListener(iVrStateCallbacks);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        iVrStateCallbacks.enforceInterface("android.service.vr.IVrManager");
        IVrStateCallbacks iVrStateCallbacks = IVrStateCallbacks.Stub.asInterface(iVrStateCallbacks.readStrongBinder());
        registerListener(iVrStateCallbacks);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.service.vr.IVrManager");
      return true;
    }
    
    private static class Proxy implements IVrManager {
      public static IVrManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.vr.IVrManager";
      }
      
      public void registerListener(IVrStateCallbacks param2IVrStateCallbacks) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.service.vr.IVrManager");
          if (param2IVrStateCallbacks != null) {
            iBinder = param2IVrStateCallbacks.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IVrManager.Stub.getDefaultImpl() != null) {
            IVrManager.Stub.getDefaultImpl().registerListener(param2IVrStateCallbacks);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterListener(IVrStateCallbacks param2IVrStateCallbacks) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.service.vr.IVrManager");
          if (param2IVrStateCallbacks != null) {
            iBinder = param2IVrStateCallbacks.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IVrManager.Stub.getDefaultImpl() != null) {
            IVrManager.Stub.getDefaultImpl().unregisterListener(param2IVrStateCallbacks);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerPersistentVrStateListener(IPersistentVrStateCallbacks param2IPersistentVrStateCallbacks) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.service.vr.IVrManager");
          if (param2IPersistentVrStateCallbacks != null) {
            iBinder = param2IPersistentVrStateCallbacks.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IVrManager.Stub.getDefaultImpl() != null) {
            IVrManager.Stub.getDefaultImpl().registerPersistentVrStateListener(param2IPersistentVrStateCallbacks);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterPersistentVrStateListener(IPersistentVrStateCallbacks param2IPersistentVrStateCallbacks) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.service.vr.IVrManager");
          if (param2IPersistentVrStateCallbacks != null) {
            iBinder = param2IPersistentVrStateCallbacks.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IVrManager.Stub.getDefaultImpl() != null) {
            IVrManager.Stub.getDefaultImpl().unregisterPersistentVrStateListener(param2IPersistentVrStateCallbacks);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getVrModeState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.vr.IVrManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IVrManager.Stub.getDefaultImpl() != null) {
            bool1 = IVrManager.Stub.getDefaultImpl().getVrModeState();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getPersistentVrModeEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.vr.IVrManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IVrManager.Stub.getDefaultImpl() != null) {
            bool1 = IVrManager.Stub.getDefaultImpl().getPersistentVrModeEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPersistentVrModeEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.service.vr.IVrManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool1 && IVrManager.Stub.getDefaultImpl() != null) {
            IVrManager.Stub.getDefaultImpl().setPersistentVrModeEnabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVr2dDisplayProperties(Vr2dDisplayProperties param2Vr2dDisplayProperties) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.vr.IVrManager");
          if (param2Vr2dDisplayProperties != null) {
            parcel1.writeInt(1);
            param2Vr2dDisplayProperties.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IVrManager.Stub.getDefaultImpl() != null) {
            IVrManager.Stub.getDefaultImpl().setVr2dDisplayProperties(param2Vr2dDisplayProperties);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getVr2dDisplayId() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.vr.IVrManager");
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IVrManager.Stub.getDefaultImpl() != null)
            return IVrManager.Stub.getDefaultImpl().getVr2dDisplayId(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAndBindCompositor(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.vr.IVrManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IVrManager.Stub.getDefaultImpl() != null) {
            IVrManager.Stub.getDefaultImpl().setAndBindCompositor(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setStandbyEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.service.vr.IVrManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool1 && IVrManager.Stub.getDefaultImpl() != null) {
            IVrManager.Stub.getDefaultImpl().setStandbyEnabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVrManager param1IVrManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVrManager != null) {
          Proxy.sDefaultImpl = param1IVrManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVrManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
