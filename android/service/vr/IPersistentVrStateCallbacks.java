package android.service.vr;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPersistentVrStateCallbacks extends IInterface {
  void onPersistentVrStateChanged(boolean paramBoolean) throws RemoteException;
  
  class Default implements IPersistentVrStateCallbacks {
    public void onPersistentVrStateChanged(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPersistentVrStateCallbacks {
    private static final String DESCRIPTOR = "android.service.vr.IPersistentVrStateCallbacks";
    
    static final int TRANSACTION_onPersistentVrStateChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.service.vr.IPersistentVrStateCallbacks");
    }
    
    public static IPersistentVrStateCallbacks asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.vr.IPersistentVrStateCallbacks");
      if (iInterface != null && iInterface instanceof IPersistentVrStateCallbacks)
        return (IPersistentVrStateCallbacks)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onPersistentVrStateChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.service.vr.IPersistentVrStateCallbacks");
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.vr.IPersistentVrStateCallbacks");
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      onPersistentVrStateChanged(bool);
      return true;
    }
    
    private static class Proxy implements IPersistentVrStateCallbacks {
      public static IPersistentVrStateCallbacks sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.vr.IPersistentVrStateCallbacks";
      }
      
      public void onPersistentVrStateChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.service.vr.IPersistentVrStateCallbacks");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && IPersistentVrStateCallbacks.Stub.getDefaultImpl() != null) {
            IPersistentVrStateCallbacks.Stub.getDefaultImpl().onPersistentVrStateChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPersistentVrStateCallbacks param1IPersistentVrStateCallbacks) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPersistentVrStateCallbacks != null) {
          Proxy.sDefaultImpl = param1IPersistentVrStateCallbacks;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPersistentVrStateCallbacks getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
