package android.service.vr;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;

public abstract class VrListenerService extends Service {
  private static final int MSG_ON_CURRENT_VR_ACTIVITY_CHANGED = 1;
  
  public static final String SERVICE_INTERFACE = "android.service.vr.VrListenerService";
  
  private final IVrListener.Stub mBinder = new IVrListener.Stub() {
      final VrListenerService this$0;
      
      public void focusedActivityChanged(ComponentName param1ComponentName, boolean param1Boolean, int param1Int) {
        Message message = VrListenerService.this.mHandler.obtainMessage(1, param1Boolean, param1Int, param1ComponentName);
        message.sendToTarget();
      }
    };
  
  private final Handler mHandler;
  
  class VrListenerHandler extends Handler {
    final VrListenerService this$0;
    
    public VrListenerHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      boolean bool = true;
      if (i == 1) {
        VrListenerService vrListenerService = VrListenerService.this;
        ComponentName componentName = (ComponentName)param1Message.obj;
        if (param1Message.arg1 != 1)
          bool = false; 
        vrListenerService.onCurrentVrActivityChanged(componentName, bool, param1Message.arg2);
      } 
    }
  }
  
  public IBinder onBind(Intent paramIntent) {
    return (IBinder)this.mBinder;
  }
  
  public VrListenerService() {
    this.mHandler = new VrListenerHandler(Looper.getMainLooper());
  }
  
  public void onCurrentVrActivityChanged(ComponentName paramComponentName) {}
  
  public void onCurrentVrActivityChanged(ComponentName paramComponentName, boolean paramBoolean, int paramInt) {
    if (paramBoolean)
      paramComponentName = null; 
    onCurrentVrActivityChanged(paramComponentName);
  }
  
  public static final boolean isVrModePackageEnabled(Context paramContext, ComponentName paramComponentName) {
    ActivityManager activityManager = paramContext.<ActivityManager>getSystemService(ActivityManager.class);
    if (activityManager == null)
      return false; 
    return activityManager.isVrModePackageEnabled(paramComponentName);
  }
}
