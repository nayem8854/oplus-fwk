package android.service.textservice;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Process;
import android.os.RemoteException;
import android.text.method.WordIterator;
import android.view.textservice.SentenceSuggestionsInfo;
import android.view.textservice.SuggestionsInfo;
import android.view.textservice.TextInfo;
import com.android.internal.textservice.ISpellCheckerService;
import com.android.internal.textservice.ISpellCheckerServiceCallback;
import com.android.internal.textservice.ISpellCheckerSession;
import com.android.internal.textservice.ISpellCheckerSessionListener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Locale;

public abstract class SpellCheckerService extends Service {
  private static final boolean DBG = false;
  
  public static final String SERVICE_INTERFACE = "android.service.textservice.SpellCheckerService";
  
  private static final String TAG = SpellCheckerService.class.getSimpleName();
  
  private final SpellCheckerServiceBinder mBinder = new SpellCheckerServiceBinder(this);
  
  public abstract Session createSession();
  
  public final IBinder onBind(Intent paramIntent) {
    return (IBinder)this.mBinder;
  }
  
  class Session {
    private SpellCheckerService.InternalISpellCheckerSession mInternalSession;
    
    private volatile SpellCheckerService.SentenceLevelAdapter mSentenceLevelAdapter;
    
    public final void setInternalISpellCheckerSession(SpellCheckerService.InternalISpellCheckerSession param1InternalISpellCheckerSession) {
      this.mInternalSession = param1InternalISpellCheckerSession;
    }
    
    public SuggestionsInfo[] onGetSuggestionsMultiple(TextInfo[] param1ArrayOfTextInfo, int param1Int, boolean param1Boolean) {
      int i = param1ArrayOfTextInfo.length;
      SuggestionsInfo[] arrayOfSuggestionsInfo = new SuggestionsInfo[i];
      for (byte b = 0; b < i; b++) {
        arrayOfSuggestionsInfo[b] = onGetSuggestions(param1ArrayOfTextInfo[b], param1Int);
        SuggestionsInfo suggestionsInfo = arrayOfSuggestionsInfo[b];
        TextInfo textInfo = param1ArrayOfTextInfo[b];
        int j = textInfo.getCookie(), k = param1ArrayOfTextInfo[b].getSequence();
        suggestionsInfo.setCookieAndSequence(j, k);
      } 
      return arrayOfSuggestionsInfo;
    }
    
    public SentenceSuggestionsInfo[] onGetSentenceSuggestionsMultiple(TextInfo[] param1ArrayOfTextInfo, int param1Int) {
      // Byte code:
      //   0: aload_1
      //   1: ifnull -> 211
      //   4: aload_1
      //   5: arraylength
      //   6: ifne -> 12
      //   9: goto -> 211
      //   12: aload_0
      //   13: getfield mSentenceLevelAdapter : Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;
      //   16: ifnonnull -> 79
      //   19: aload_0
      //   20: monitorenter
      //   21: aload_0
      //   22: getfield mSentenceLevelAdapter : Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;
      //   25: ifnonnull -> 69
      //   28: aload_0
      //   29: invokevirtual getLocale : ()Ljava/lang/String;
      //   32: astore_3
      //   33: aload_3
      //   34: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
      //   37: ifne -> 69
      //   40: new android/service/textservice/SpellCheckerService$SentenceLevelAdapter
      //   43: astore #4
      //   45: new java/util/Locale
      //   48: astore #5
      //   50: aload #5
      //   52: aload_3
      //   53: invokespecial <init> : (Ljava/lang/String;)V
      //   56: aload #4
      //   58: aload #5
      //   60: invokespecial <init> : (Ljava/util/Locale;)V
      //   63: aload_0
      //   64: aload #4
      //   66: putfield mSentenceLevelAdapter : Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;
      //   69: aload_0
      //   70: monitorexit
      //   71: goto -> 79
      //   74: astore_1
      //   75: aload_0
      //   76: monitorexit
      //   77: aload_1
      //   78: athrow
      //   79: aload_0
      //   80: getfield mSentenceLevelAdapter : Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;
      //   83: ifnonnull -> 90
      //   86: getstatic android/service/textservice/SpellCheckerService$SentenceLevelAdapter.EMPTY_SENTENCE_SUGGESTIONS_INFOS : [Landroid/view/textservice/SentenceSuggestionsInfo;
      //   89: areturn
      //   90: aload_1
      //   91: arraylength
      //   92: istore #6
      //   94: iload #6
      //   96: anewarray android/view/textservice/SentenceSuggestionsInfo
      //   99: astore #4
      //   101: iconst_0
      //   102: istore #7
      //   104: iload #7
      //   106: iload #6
      //   108: if_icmpge -> 208
      //   111: aload_0
      //   112: getfield mSentenceLevelAdapter : Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;
      //   115: astore_3
      //   116: aload_1
      //   117: iload #7
      //   119: aaload
      //   120: astore #5
      //   122: aload_3
      //   123: aload #5
      //   125: invokestatic access$000 : (Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;Landroid/view/textservice/TextInfo;)Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceTextInfoParams;
      //   128: astore #5
      //   130: aload #5
      //   132: getfield mItems : Ljava/util/ArrayList;
      //   135: astore_3
      //   136: aload_3
      //   137: invokevirtual size : ()I
      //   140: istore #8
      //   142: iload #8
      //   144: anewarray android/view/textservice/TextInfo
      //   147: astore #9
      //   149: iconst_0
      //   150: istore #10
      //   152: iload #10
      //   154: iload #8
      //   156: if_icmpge -> 182
      //   159: aload #9
      //   161: iload #10
      //   163: aload_3
      //   164: iload #10
      //   166: invokevirtual get : (I)Ljava/lang/Object;
      //   169: checkcast android/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceWordItem
      //   172: getfield mTextInfo : Landroid/view/textservice/TextInfo;
      //   175: aastore
      //   176: iinc #10, 1
      //   179: goto -> 152
      //   182: aload_0
      //   183: aload #9
      //   185: iload_2
      //   186: iconst_1
      //   187: invokevirtual onGetSuggestionsMultiple : ([Landroid/view/textservice/TextInfo;IZ)[Landroid/view/textservice/SuggestionsInfo;
      //   190: astore_3
      //   191: aload #4
      //   193: iload #7
      //   195: aload #5
      //   197: aload_3
      //   198: invokestatic reconstructSuggestions : (Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceTextInfoParams;[Landroid/view/textservice/SuggestionsInfo;)Landroid/view/textservice/SentenceSuggestionsInfo;
      //   201: aastore
      //   202: iinc #7, 1
      //   205: goto -> 104
      //   208: aload #4
      //   210: areturn
      //   211: getstatic android/service/textservice/SpellCheckerService$SentenceLevelAdapter.EMPTY_SENTENCE_SUGGESTIONS_INFOS : [Landroid/view/textservice/SentenceSuggestionsInfo;
      //   214: areturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #166	-> 0
      //   #173	-> 12
      //   #174	-> 19
      //   #175	-> 21
      //   #176	-> 28
      //   #177	-> 33
      //   #178	-> 40
      //   #181	-> 69
      //   #183	-> 79
      //   #184	-> 86
      //   #186	-> 90
      //   #187	-> 94
      //   #188	-> 101
      //   #189	-> 111
      //   #190	-> 122
      //   #191	-> 130
      //   #193	-> 136
      //   #194	-> 142
      //   #195	-> 149
      //   #196	-> 159
      //   #195	-> 176
      //   #198	-> 182
      //   #199	-> 182
      //   #198	-> 191
      //   #188	-> 202
      //   #202	-> 208
      //   #167	-> 211
      // Exception table:
      //   from	to	target	type
      //   21	28	74	finally
      //   28	33	74	finally
      //   33	40	74	finally
      //   40	69	74	finally
      //   69	71	74	finally
      //   75	77	74	finally
    }
    
    public void onCancel() {}
    
    public void onClose() {}
    
    public String getLocale() {
      return this.mInternalSession.getLocale();
    }
    
    public Bundle getBundle() {
      return this.mInternalSession.getBundle();
    }
    
    public abstract void onCreate();
    
    public abstract SuggestionsInfo onGetSuggestions(TextInfo param1TextInfo, int param1Int);
  }
  
  private static class InternalISpellCheckerSession extends ISpellCheckerSession.Stub {
    private final Bundle mBundle;
    
    private ISpellCheckerSessionListener mListener;
    
    private final String mLocale;
    
    private final SpellCheckerService.Session mSession;
    
    public InternalISpellCheckerSession(String param1String, ISpellCheckerSessionListener param1ISpellCheckerSessionListener, Bundle param1Bundle, SpellCheckerService.Session param1Session) {
      this.mListener = param1ISpellCheckerSessionListener;
      this.mSession = param1Session;
      this.mLocale = param1String;
      this.mBundle = param1Bundle;
      param1Session.setInternalISpellCheckerSession(this);
    }
    
    public void onGetSuggestionsMultiple(TextInfo[] param1ArrayOfTextInfo, int param1Int, boolean param1Boolean) {
      int i = Process.getThreadPriority(Process.myTid());
      try {
        Process.setThreadPriority(10);
        ISpellCheckerSessionListener iSpellCheckerSessionListener = this.mListener;
        SpellCheckerService.Session session = this.mSession;
        SuggestionsInfo[] arrayOfSuggestionsInfo = session.onGetSuggestionsMultiple(param1ArrayOfTextInfo, param1Int, param1Boolean);
        iSpellCheckerSessionListener.onGetSuggestions(arrayOfSuggestionsInfo);
      } catch (RemoteException remoteException) {
      
      } finally {
        Process.setThreadPriority(i);
      } 
    }
    
    public void onGetSentenceSuggestionsMultiple(TextInfo[] param1ArrayOfTextInfo, int param1Int) {
      try {
        ISpellCheckerSessionListener iSpellCheckerSessionListener = this.mListener;
        SpellCheckerService.Session session = this.mSession;
        SentenceSuggestionsInfo[] arrayOfSentenceSuggestionsInfo = session.onGetSentenceSuggestionsMultiple(param1ArrayOfTextInfo, param1Int);
        iSpellCheckerSessionListener.onGetSentenceSuggestions(arrayOfSentenceSuggestionsInfo);
      } catch (RemoteException remoteException) {}
    }
    
    public void onCancel() {
      int i = Process.getThreadPriority(Process.myTid());
      try {
        Process.setThreadPriority(10);
        this.mSession.onCancel();
        return;
      } finally {
        Process.setThreadPriority(i);
      } 
    }
    
    public void onClose() {
      int i = Process.getThreadPriority(Process.myTid());
      try {
        Process.setThreadPriority(10);
        this.mSession.onClose();
        return;
      } finally {
        Process.setThreadPriority(i);
        this.mListener = null;
      } 
    }
    
    public String getLocale() {
      return this.mLocale;
    }
    
    public Bundle getBundle() {
      return this.mBundle;
    }
  }
  
  private static class SpellCheckerServiceBinder extends ISpellCheckerService.Stub {
    private final WeakReference<SpellCheckerService> mInternalServiceRef;
    
    public SpellCheckerServiceBinder(SpellCheckerService param1SpellCheckerService) {
      this.mInternalServiceRef = new WeakReference<>(param1SpellCheckerService);
    }
    
    public void getISpellCheckerSession(String param1String, ISpellCheckerSessionListener param1ISpellCheckerSessionListener, Bundle param1Bundle, ISpellCheckerServiceCallback param1ISpellCheckerServiceCallback) {
      SpellCheckerService.InternalISpellCheckerSession internalISpellCheckerSession;
      SpellCheckerService spellCheckerService = this.mInternalServiceRef.get();
      if (spellCheckerService == null) {
        param1String = null;
      } else {
        SpellCheckerService.Session session = spellCheckerService.createSession();
        internalISpellCheckerSession = new SpellCheckerService.InternalISpellCheckerSession(param1String, param1ISpellCheckerSessionListener, param1Bundle, session);
        session.onCreate();
      } 
      try {
        param1ISpellCheckerServiceCallback.onSessionCreated((ISpellCheckerSession)internalISpellCheckerSession);
      } catch (RemoteException remoteException) {}
    }
  }
  
  class SentenceLevelAdapter {
    public static final SentenceSuggestionsInfo[] EMPTY_SENTENCE_SUGGESTIONS_INFOS = new SentenceSuggestionsInfo[0];
    
    private static final SuggestionsInfo EMPTY_SUGGESTIONS_INFO = new SuggestionsInfo(0, null);
    
    private final WordIterator mWordIterator;
    
    public static class SentenceWordItem {
      public final int mLength;
      
      public final int mStart;
      
      public final TextInfo mTextInfo;
      
      public SentenceWordItem(TextInfo param2TextInfo, int param2Int1, int param2Int2) {
        this.mTextInfo = param2TextInfo;
        this.mStart = param2Int1;
        this.mLength = param2Int2 - param2Int1;
      }
    }
    
    public static class SentenceTextInfoParams {
      final ArrayList<SpellCheckerService.SentenceLevelAdapter.SentenceWordItem> mItems;
      
      final TextInfo mOriginalTextInfo;
      
      final int mSize;
      
      public SentenceTextInfoParams(TextInfo param2TextInfo, ArrayList<SpellCheckerService.SentenceLevelAdapter.SentenceWordItem> param2ArrayList) {
        this.mOriginalTextInfo = param2TextInfo;
        this.mItems = param2ArrayList;
        this.mSize = param2ArrayList.size();
      }
    }
    
    public SentenceLevelAdapter(SpellCheckerService this$0) {
      this.mWordIterator = new WordIterator((Locale)this$0);
    }
    
    private SentenceTextInfoParams getSplitWords(TextInfo param1TextInfo) {
      WordIterator wordIterator = this.mWordIterator;
      String str = param1TextInfo.getText();
      int i = param1TextInfo.getCookie();
      int j = str.length();
      ArrayList<SentenceWordItem> arrayList = new ArrayList();
      wordIterator.setCharSequence(str, 0, str.length());
      int k = wordIterator.following(0);
      int m = wordIterator.getBeginning(k);
      while (m <= j && k != -1 && m != -1) {
        if (k >= 0 && k > m) {
          CharSequence charSequence = str.subSequence(m, k);
          int n = charSequence.length();
          TextInfo textInfo = new TextInfo(charSequence, 0, n, i, charSequence.hashCode());
          arrayList.add(new SentenceWordItem(textInfo, m, k));
        } 
        k = wordIterator.following(k);
        if (k == -1)
          break; 
        m = wordIterator.getBeginning(k);
      } 
      return new SentenceTextInfoParams(param1TextInfo, arrayList);
    }
    
    public static SentenceSuggestionsInfo reconstructSuggestions(SentenceTextInfoParams param1SentenceTextInfoParams, SuggestionsInfo[] param1ArrayOfSuggestionsInfo) {
      if (param1ArrayOfSuggestionsInfo == null || param1ArrayOfSuggestionsInfo.length == 0)
        return null; 
      if (param1SentenceTextInfoParams == null)
        return null; 
      int i = param1SentenceTextInfoParams.mOriginalTextInfo.getCookie();
      TextInfo textInfo = param1SentenceTextInfoParams.mOriginalTextInfo;
      int j = textInfo.getSequence();
      int k = param1SentenceTextInfoParams.mSize;
      int[] arrayOfInt1 = new int[k];
      int[] arrayOfInt2 = new int[k];
      SuggestionsInfo[] arrayOfSuggestionsInfo = new SuggestionsInfo[k];
      for (byte b = 0; b < k; b++) {
        SuggestionsInfo suggestionsInfo;
        SentenceWordItem sentenceWordItem = param1SentenceTextInfoParams.mItems.get(b);
        TextInfo textInfo1 = null;
        byte b1 = 0;
        while (true) {
          textInfo = textInfo1;
          if (b1 < param1ArrayOfSuggestionsInfo.length) {
            suggestionsInfo = param1ArrayOfSuggestionsInfo[b1];
            if (suggestionsInfo != null && suggestionsInfo.getSequence() == sentenceWordItem.mTextInfo.getSequence()) {
              suggestionsInfo.setCookieAndSequence(i, j);
              break;
            } 
            b1++;
            continue;
          } 
          break;
        } 
        arrayOfInt1[b] = sentenceWordItem.mStart;
        arrayOfInt2[b] = sentenceWordItem.mLength;
        if (suggestionsInfo == null)
          suggestionsInfo = EMPTY_SUGGESTIONS_INFO; 
        arrayOfSuggestionsInfo[b] = suggestionsInfo;
      } 
      return new SentenceSuggestionsInfo(arrayOfSuggestionsInfo, arrayOfInt1, arrayOfInt2);
    }
  }
}
