package android.service.persistentdata;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPersistentDataBlockService extends IInterface {
  int getDataBlockSize() throws RemoteException;
  
  int getFlashLockState() throws RemoteException;
  
  long getMaximumDataBlockSize() throws RemoteException;
  
  boolean getOemUnlockEnabled() throws RemoteException;
  
  boolean hasFrpCredentialHandle() throws RemoteException;
  
  byte[] read() throws RemoteException;
  
  void setOemUnlockEnabled(boolean paramBoolean) throws RemoteException;
  
  void wipe() throws RemoteException;
  
  int write(byte[] paramArrayOfbyte) throws RemoteException;
  
  class Default implements IPersistentDataBlockService {
    public int write(byte[] param1ArrayOfbyte) throws RemoteException {
      return 0;
    }
    
    public byte[] read() throws RemoteException {
      return null;
    }
    
    public void wipe() throws RemoteException {}
    
    public int getDataBlockSize() throws RemoteException {
      return 0;
    }
    
    public long getMaximumDataBlockSize() throws RemoteException {
      return 0L;
    }
    
    public void setOemUnlockEnabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean getOemUnlockEnabled() throws RemoteException {
      return false;
    }
    
    public int getFlashLockState() throws RemoteException {
      return 0;
    }
    
    public boolean hasFrpCredentialHandle() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPersistentDataBlockService {
    private static final String DESCRIPTOR = "android.service.persistentdata.IPersistentDataBlockService";
    
    static final int TRANSACTION_getDataBlockSize = 4;
    
    static final int TRANSACTION_getFlashLockState = 8;
    
    static final int TRANSACTION_getMaximumDataBlockSize = 5;
    
    static final int TRANSACTION_getOemUnlockEnabled = 7;
    
    static final int TRANSACTION_hasFrpCredentialHandle = 9;
    
    static final int TRANSACTION_read = 2;
    
    static final int TRANSACTION_setOemUnlockEnabled = 6;
    
    static final int TRANSACTION_wipe = 3;
    
    static final int TRANSACTION_write = 1;
    
    public Stub() {
      attachInterface(this, "android.service.persistentdata.IPersistentDataBlockService");
    }
    
    public static IPersistentDataBlockService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.persistentdata.IPersistentDataBlockService");
      if (iInterface != null && iInterface instanceof IPersistentDataBlockService)
        return (IPersistentDataBlockService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 9:
          return "hasFrpCredentialHandle";
        case 8:
          return "getFlashLockState";
        case 7:
          return "getOemUnlockEnabled";
        case 6:
          return "setOemUnlockEnabled";
        case 5:
          return "getMaximumDataBlockSize";
        case 4:
          return "getDataBlockSize";
        case 3:
          return "wipe";
        case 2:
          return "read";
        case 1:
          break;
      } 
      return "write";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int j;
        boolean bool1;
        boolean bool;
        long l;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 9:
            param1Parcel1.enforceInterface("android.service.persistentdata.IPersistentDataBlockService");
            bool2 = hasFrpCredentialHandle();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.service.persistentdata.IPersistentDataBlockService");
            j = getFlashLockState();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.service.persistentdata.IPersistentDataBlockService");
            bool1 = getOemUnlockEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.service.persistentdata.IPersistentDataBlockService");
            if (param1Parcel1.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            setOemUnlockEnabled(bool);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.service.persistentdata.IPersistentDataBlockService");
            l = getMaximumDataBlockSize();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.service.persistentdata.IPersistentDataBlockService");
            i = getDataBlockSize();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.service.persistentdata.IPersistentDataBlockService");
            wipe();
            param1Parcel2.writeNoException();
            return true;
          case 2:
            param1Parcel1.enforceInterface("android.service.persistentdata.IPersistentDataBlockService");
            arrayOfByte = read();
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte);
            return true;
          case 1:
            break;
        } 
        arrayOfByte.enforceInterface("android.service.persistentdata.IPersistentDataBlockService");
        byte[] arrayOfByte = arrayOfByte.createByteArray();
        int i = write(arrayOfByte);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(i);
        return true;
      } 
      param1Parcel2.writeString("android.service.persistentdata.IPersistentDataBlockService");
      return true;
    }
    
    private static class Proxy implements IPersistentDataBlockService {
      public static IPersistentDataBlockService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.persistentdata.IPersistentDataBlockService";
      }
      
      public int write(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.persistentdata.IPersistentDataBlockService");
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IPersistentDataBlockService.Stub.getDefaultImpl() != null)
            return IPersistentDataBlockService.Stub.getDefaultImpl().write(param2ArrayOfbyte); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] read() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.persistentdata.IPersistentDataBlockService");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IPersistentDataBlockService.Stub.getDefaultImpl() != null)
            return IPersistentDataBlockService.Stub.getDefaultImpl().read(); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void wipe() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.persistentdata.IPersistentDataBlockService");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IPersistentDataBlockService.Stub.getDefaultImpl() != null) {
            IPersistentDataBlockService.Stub.getDefaultImpl().wipe();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDataBlockSize() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.persistentdata.IPersistentDataBlockService");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IPersistentDataBlockService.Stub.getDefaultImpl() != null)
            return IPersistentDataBlockService.Stub.getDefaultImpl().getDataBlockSize(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getMaximumDataBlockSize() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.persistentdata.IPersistentDataBlockService");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IPersistentDataBlockService.Stub.getDefaultImpl() != null)
            return IPersistentDataBlockService.Stub.getDefaultImpl().getMaximumDataBlockSize(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setOemUnlockEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.service.persistentdata.IPersistentDataBlockService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool1 && IPersistentDataBlockService.Stub.getDefaultImpl() != null) {
            IPersistentDataBlockService.Stub.getDefaultImpl().setOemUnlockEnabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getOemUnlockEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.persistentdata.IPersistentDataBlockService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IPersistentDataBlockService.Stub.getDefaultImpl() != null) {
            bool1 = IPersistentDataBlockService.Stub.getDefaultImpl().getOemUnlockEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getFlashLockState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.persistentdata.IPersistentDataBlockService");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IPersistentDataBlockService.Stub.getDefaultImpl() != null)
            return IPersistentDataBlockService.Stub.getDefaultImpl().getFlashLockState(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasFrpCredentialHandle() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.persistentdata.IPersistentDataBlockService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IPersistentDataBlockService.Stub.getDefaultImpl() != null) {
            bool1 = IPersistentDataBlockService.Stub.getDefaultImpl().hasFrpCredentialHandle();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPersistentDataBlockService param1IPersistentDataBlockService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPersistentDataBlockService != null) {
          Proxy.sDefaultImpl = param1IPersistentDataBlockService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPersistentDataBlockService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
