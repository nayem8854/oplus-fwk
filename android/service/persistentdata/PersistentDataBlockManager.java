package android.service.persistentdata;

import android.annotation.SystemApi;
import android.os.RemoteException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public class PersistentDataBlockManager {
  public static final int FLASH_LOCK_LOCKED = 1;
  
  public static final int FLASH_LOCK_UNKNOWN = -1;
  
  public static final int FLASH_LOCK_UNLOCKED = 0;
  
  private static final String TAG = PersistentDataBlockManager.class.getSimpleName();
  
  private IPersistentDataBlockService sService;
  
  public PersistentDataBlockManager(IPersistentDataBlockService paramIPersistentDataBlockService) {
    this.sService = paramIPersistentDataBlockService;
  }
  
  public int write(byte[] paramArrayOfbyte) {
    try {
      return this.sService.write(paramArrayOfbyte);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public byte[] read() {
    try {
      return this.sService.read();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getDataBlockSize() {
    try {
      return this.sService.getDataBlockSize();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public long getMaximumDataBlockSize() {
    try {
      return this.sService.getMaximumDataBlockSize();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void wipe() {
    try {
      this.sService.wipe();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setOemUnlockEnabled(boolean paramBoolean) {
    try {
      this.sService.setOemUnlockEnabled(paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean getOemUnlockEnabled() {
    try {
      return this.sService.getOemUnlockEnabled();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getFlashLockState() {
    try {
      return this.sService.getFlashLockState();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface FlashLockState {}
}
