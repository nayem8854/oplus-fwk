package android.service.quickaccesswallet;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IQuickAccessWalletServiceCallbacks extends IInterface {
  void onGetWalletCardsFailure(GetWalletCardsError paramGetWalletCardsError) throws RemoteException;
  
  void onGetWalletCardsSuccess(GetWalletCardsResponse paramGetWalletCardsResponse) throws RemoteException;
  
  void onWalletServiceEvent(WalletServiceEvent paramWalletServiceEvent) throws RemoteException;
  
  class Default implements IQuickAccessWalletServiceCallbacks {
    public void onGetWalletCardsSuccess(GetWalletCardsResponse param1GetWalletCardsResponse) throws RemoteException {}
    
    public void onGetWalletCardsFailure(GetWalletCardsError param1GetWalletCardsError) throws RemoteException {}
    
    public void onWalletServiceEvent(WalletServiceEvent param1WalletServiceEvent) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IQuickAccessWalletServiceCallbacks {
    private static final String DESCRIPTOR = "android.service.quickaccesswallet.IQuickAccessWalletServiceCallbacks";
    
    static final int TRANSACTION_onGetWalletCardsFailure = 2;
    
    static final int TRANSACTION_onGetWalletCardsSuccess = 1;
    
    static final int TRANSACTION_onWalletServiceEvent = 3;
    
    public Stub() {
      attachInterface(this, "android.service.quickaccesswallet.IQuickAccessWalletServiceCallbacks");
    }
    
    public static IQuickAccessWalletServiceCallbacks asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.quickaccesswallet.IQuickAccessWalletServiceCallbacks");
      if (iInterface != null && iInterface instanceof IQuickAccessWalletServiceCallbacks)
        return (IQuickAccessWalletServiceCallbacks)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onWalletServiceEvent";
        } 
        return "onGetWalletCardsFailure";
      } 
      return "onGetWalletCardsSuccess";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.service.quickaccesswallet.IQuickAccessWalletServiceCallbacks");
            return true;
          } 
          param1Parcel1.enforceInterface("android.service.quickaccesswallet.IQuickAccessWalletServiceCallbacks");
          if (param1Parcel1.readInt() != 0) {
            WalletServiceEvent walletServiceEvent = WalletServiceEvent.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          onWalletServiceEvent((WalletServiceEvent)param1Parcel1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.service.quickaccesswallet.IQuickAccessWalletServiceCallbacks");
        if (param1Parcel1.readInt() != 0) {
          GetWalletCardsError getWalletCardsError = GetWalletCardsError.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onGetWalletCardsFailure((GetWalletCardsError)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.quickaccesswallet.IQuickAccessWalletServiceCallbacks");
      if (param1Parcel1.readInt() != 0) {
        GetWalletCardsResponse getWalletCardsResponse = GetWalletCardsResponse.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onGetWalletCardsSuccess((GetWalletCardsResponse)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IQuickAccessWalletServiceCallbacks {
      public static IQuickAccessWalletServiceCallbacks sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.quickaccesswallet.IQuickAccessWalletServiceCallbacks";
      }
      
      public void onGetWalletCardsSuccess(GetWalletCardsResponse param2GetWalletCardsResponse) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.quickaccesswallet.IQuickAccessWalletServiceCallbacks");
          if (param2GetWalletCardsResponse != null) {
            parcel.writeInt(1);
            param2GetWalletCardsResponse.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IQuickAccessWalletServiceCallbacks.Stub.getDefaultImpl() != null) {
            IQuickAccessWalletServiceCallbacks.Stub.getDefaultImpl().onGetWalletCardsSuccess(param2GetWalletCardsResponse);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onGetWalletCardsFailure(GetWalletCardsError param2GetWalletCardsError) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.quickaccesswallet.IQuickAccessWalletServiceCallbacks");
          if (param2GetWalletCardsError != null) {
            parcel.writeInt(1);
            param2GetWalletCardsError.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IQuickAccessWalletServiceCallbacks.Stub.getDefaultImpl() != null) {
            IQuickAccessWalletServiceCallbacks.Stub.getDefaultImpl().onGetWalletCardsFailure(param2GetWalletCardsError);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onWalletServiceEvent(WalletServiceEvent param2WalletServiceEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.quickaccesswallet.IQuickAccessWalletServiceCallbacks");
          if (param2WalletServiceEvent != null) {
            parcel.writeInt(1);
            param2WalletServiceEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IQuickAccessWalletServiceCallbacks.Stub.getDefaultImpl() != null) {
            IQuickAccessWalletServiceCallbacks.Stub.getDefaultImpl().onWalletServiceEvent(param2WalletServiceEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IQuickAccessWalletServiceCallbacks param1IQuickAccessWalletServiceCallbacks) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IQuickAccessWalletServiceCallbacks != null) {
          Proxy.sDefaultImpl = param1IQuickAccessWalletServiceCallbacks;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IQuickAccessWalletServiceCallbacks getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
