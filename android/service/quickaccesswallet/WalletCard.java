package android.service.quickaccesswallet;

import android.app.PendingIntent;
import android.graphics.drawable.Icon;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public final class WalletCard implements Parcelable {
  private WalletCard(Builder paramBuilder) {
    this.mCardId = paramBuilder.mCardId;
    this.mCardImage = paramBuilder.mCardImage;
    this.mContentDescription = paramBuilder.mContentDescription;
    this.mPendingIntent = paramBuilder.mPendingIntent;
    this.mCardIcon = paramBuilder.mCardIcon;
    this.mCardLabel = paramBuilder.mCardLabel;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mCardId);
    this.mCardImage.writeToParcel(paramParcel, paramInt);
    TextUtils.writeToParcel(this.mContentDescription, paramParcel, paramInt);
    PendingIntent.writePendingIntentOrNullToParcel(this.mPendingIntent, paramParcel);
    if (this.mCardIcon == null) {
      paramParcel.writeByte((byte)0);
    } else {
      paramParcel.writeByte((byte)1);
      this.mCardIcon.writeToParcel(paramParcel, paramInt);
    } 
    TextUtils.writeToParcel(this.mCardLabel, paramParcel, paramInt);
  }
  
  private static WalletCard readFromParcel(Parcel paramParcel) {
    Icon icon2;
    String str = paramParcel.readString();
    Icon icon1 = Icon.CREATOR.createFromParcel(paramParcel);
    CharSequence charSequence2 = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    PendingIntent pendingIntent = PendingIntent.readPendingIntentOrNullFromParcel(paramParcel);
    if (paramParcel.readByte() == 0) {
      icon2 = null;
    } else {
      icon2 = Icon.CREATOR.createFromParcel(paramParcel);
    } 
    CharSequence charSequence1 = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    Builder builder2 = new Builder(str, icon1, charSequence2, pendingIntent);
    Builder builder3 = builder2.setCardIcon(icon2);
    Builder builder1 = builder3.setCardLabel(charSequence1);
    return builder1.build();
  }
  
  public static final Parcelable.Creator<WalletCard> CREATOR = new Parcelable.Creator<WalletCard>() {
      public WalletCard createFromParcel(Parcel param1Parcel) {
        return WalletCard.readFromParcel(param1Parcel);
      }
      
      public WalletCard[] newArray(int param1Int) {
        return new WalletCard[param1Int];
      }
    };
  
  private final Icon mCardIcon;
  
  private final String mCardId;
  
  private final Icon mCardImage;
  
  private final CharSequence mCardLabel;
  
  private final CharSequence mContentDescription;
  
  private final PendingIntent mPendingIntent;
  
  public String getCardId() {
    return this.mCardId;
  }
  
  public Icon getCardImage() {
    return this.mCardImage;
  }
  
  public CharSequence getContentDescription() {
    return this.mContentDescription;
  }
  
  public PendingIntent getPendingIntent() {
    return this.mPendingIntent;
  }
  
  public Icon getCardIcon() {
    return this.mCardIcon;
  }
  
  public CharSequence getCardLabel() {
    return this.mCardLabel;
  }
  
  class Builder {
    private Icon mCardIcon;
    
    private String mCardId;
    
    private Icon mCardImage;
    
    private CharSequence mCardLabel;
    
    private CharSequence mContentDescription;
    
    private PendingIntent mPendingIntent;
    
    public Builder(WalletCard this$0, Icon param1Icon, CharSequence param1CharSequence, PendingIntent param1PendingIntent) {
      this.mCardId = (String)this$0;
      this.mCardImage = param1Icon;
      this.mContentDescription = param1CharSequence;
      this.mPendingIntent = param1PendingIntent;
    }
    
    public Builder setCardIcon(Icon param1Icon) {
      this.mCardIcon = param1Icon;
      return this;
    }
    
    public Builder setCardLabel(CharSequence param1CharSequence) {
      this.mCardLabel = param1CharSequence;
      return this;
    }
    
    public WalletCard build() {
      return new WalletCard(this);
    }
  }
}
