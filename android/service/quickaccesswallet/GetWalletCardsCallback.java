package android.service.quickaccesswallet;

public interface GetWalletCardsCallback {
  void onFailure(GetWalletCardsError paramGetWalletCardsError);
  
  void onSuccess(GetWalletCardsResponse paramGetWalletCardsResponse);
}
