package android.service.quickaccesswallet;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IQuickAccessWalletService extends IInterface {
  void onWalletCardSelected(SelectWalletCardRequest paramSelectWalletCardRequest) throws RemoteException;
  
  void onWalletCardsRequested(GetWalletCardsRequest paramGetWalletCardsRequest, IQuickAccessWalletServiceCallbacks paramIQuickAccessWalletServiceCallbacks) throws RemoteException;
  
  void onWalletDismissed() throws RemoteException;
  
  void registerWalletServiceEventListener(WalletServiceEventListenerRequest paramWalletServiceEventListenerRequest, IQuickAccessWalletServiceCallbacks paramIQuickAccessWalletServiceCallbacks) throws RemoteException;
  
  void unregisterWalletServiceEventListener(WalletServiceEventListenerRequest paramWalletServiceEventListenerRequest) throws RemoteException;
  
  class Default implements IQuickAccessWalletService {
    public void onWalletCardsRequested(GetWalletCardsRequest param1GetWalletCardsRequest, IQuickAccessWalletServiceCallbacks param1IQuickAccessWalletServiceCallbacks) throws RemoteException {}
    
    public void onWalletCardSelected(SelectWalletCardRequest param1SelectWalletCardRequest) throws RemoteException {}
    
    public void onWalletDismissed() throws RemoteException {}
    
    public void registerWalletServiceEventListener(WalletServiceEventListenerRequest param1WalletServiceEventListenerRequest, IQuickAccessWalletServiceCallbacks param1IQuickAccessWalletServiceCallbacks) throws RemoteException {}
    
    public void unregisterWalletServiceEventListener(WalletServiceEventListenerRequest param1WalletServiceEventListenerRequest) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IQuickAccessWalletService {
    private static final String DESCRIPTOR = "android.service.quickaccesswallet.IQuickAccessWalletService";
    
    static final int TRANSACTION_onWalletCardSelected = 2;
    
    static final int TRANSACTION_onWalletCardsRequested = 1;
    
    static final int TRANSACTION_onWalletDismissed = 3;
    
    static final int TRANSACTION_registerWalletServiceEventListener = 4;
    
    static final int TRANSACTION_unregisterWalletServiceEventListener = 5;
    
    public Stub() {
      attachInterface(this, "android.service.quickaccesswallet.IQuickAccessWalletService");
    }
    
    public static IQuickAccessWalletService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.quickaccesswallet.IQuickAccessWalletService");
      if (iInterface != null && iInterface instanceof IQuickAccessWalletService)
        return (IQuickAccessWalletService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "unregisterWalletServiceEventListener";
            } 
            return "registerWalletServiceEventListener";
          } 
          return "onWalletDismissed";
        } 
        return "onWalletCardSelected";
      } 
      return "onWalletCardsRequested";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.service.quickaccesswallet.IQuickAccessWalletService");
                return true;
              } 
              param1Parcel1.enforceInterface("android.service.quickaccesswallet.IQuickAccessWalletService");
              if (param1Parcel1.readInt() != 0) {
                WalletServiceEventListenerRequest walletServiceEventListenerRequest = WalletServiceEventListenerRequest.CREATOR.createFromParcel(param1Parcel1);
              } else {
                param1Parcel1 = null;
              } 
              unregisterWalletServiceEventListener((WalletServiceEventListenerRequest)param1Parcel1);
              return true;
            } 
            param1Parcel1.enforceInterface("android.service.quickaccesswallet.IQuickAccessWalletService");
            if (param1Parcel1.readInt() != 0) {
              WalletServiceEventListenerRequest walletServiceEventListenerRequest = WalletServiceEventListenerRequest.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            iQuickAccessWalletServiceCallbacks = IQuickAccessWalletServiceCallbacks.Stub.asInterface(param1Parcel1.readStrongBinder());
            registerWalletServiceEventListener((WalletServiceEventListenerRequest)param1Parcel2, iQuickAccessWalletServiceCallbacks);
            return true;
          } 
          iQuickAccessWalletServiceCallbacks.enforceInterface("android.service.quickaccesswallet.IQuickAccessWalletService");
          onWalletDismissed();
          return true;
        } 
        iQuickAccessWalletServiceCallbacks.enforceInterface("android.service.quickaccesswallet.IQuickAccessWalletService");
        if (iQuickAccessWalletServiceCallbacks.readInt() != 0) {
          SelectWalletCardRequest selectWalletCardRequest = SelectWalletCardRequest.CREATOR.createFromParcel((Parcel)iQuickAccessWalletServiceCallbacks);
        } else {
          iQuickAccessWalletServiceCallbacks = null;
        } 
        onWalletCardSelected((SelectWalletCardRequest)iQuickAccessWalletServiceCallbacks);
        return true;
      } 
      iQuickAccessWalletServiceCallbacks.enforceInterface("android.service.quickaccesswallet.IQuickAccessWalletService");
      if (iQuickAccessWalletServiceCallbacks.readInt() != 0) {
        GetWalletCardsRequest getWalletCardsRequest = GetWalletCardsRequest.CREATOR.createFromParcel((Parcel)iQuickAccessWalletServiceCallbacks);
      } else {
        param1Parcel2 = null;
      } 
      IQuickAccessWalletServiceCallbacks iQuickAccessWalletServiceCallbacks = IQuickAccessWalletServiceCallbacks.Stub.asInterface(iQuickAccessWalletServiceCallbacks.readStrongBinder());
      onWalletCardsRequested((GetWalletCardsRequest)param1Parcel2, iQuickAccessWalletServiceCallbacks);
      return true;
    }
    
    private static class Proxy implements IQuickAccessWalletService {
      public static IQuickAccessWalletService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.quickaccesswallet.IQuickAccessWalletService";
      }
      
      public void onWalletCardsRequested(GetWalletCardsRequest param2GetWalletCardsRequest, IQuickAccessWalletServiceCallbacks param2IQuickAccessWalletServiceCallbacks) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.quickaccesswallet.IQuickAccessWalletService");
          if (param2GetWalletCardsRequest != null) {
            parcel.writeInt(1);
            param2GetWalletCardsRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IQuickAccessWalletServiceCallbacks != null) {
            iBinder = param2IQuickAccessWalletServiceCallbacks.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IQuickAccessWalletService.Stub.getDefaultImpl() != null) {
            IQuickAccessWalletService.Stub.getDefaultImpl().onWalletCardsRequested(param2GetWalletCardsRequest, param2IQuickAccessWalletServiceCallbacks);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onWalletCardSelected(SelectWalletCardRequest param2SelectWalletCardRequest) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.quickaccesswallet.IQuickAccessWalletService");
          if (param2SelectWalletCardRequest != null) {
            parcel.writeInt(1);
            param2SelectWalletCardRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IQuickAccessWalletService.Stub.getDefaultImpl() != null) {
            IQuickAccessWalletService.Stub.getDefaultImpl().onWalletCardSelected(param2SelectWalletCardRequest);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onWalletDismissed() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.quickaccesswallet.IQuickAccessWalletService");
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IQuickAccessWalletService.Stub.getDefaultImpl() != null) {
            IQuickAccessWalletService.Stub.getDefaultImpl().onWalletDismissed();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void registerWalletServiceEventListener(WalletServiceEventListenerRequest param2WalletServiceEventListenerRequest, IQuickAccessWalletServiceCallbacks param2IQuickAccessWalletServiceCallbacks) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.quickaccesswallet.IQuickAccessWalletService");
          if (param2WalletServiceEventListenerRequest != null) {
            parcel.writeInt(1);
            param2WalletServiceEventListenerRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IQuickAccessWalletServiceCallbacks != null) {
            iBinder = param2IQuickAccessWalletServiceCallbacks.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IQuickAccessWalletService.Stub.getDefaultImpl() != null) {
            IQuickAccessWalletService.Stub.getDefaultImpl().registerWalletServiceEventListener(param2WalletServiceEventListenerRequest, param2IQuickAccessWalletServiceCallbacks);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void unregisterWalletServiceEventListener(WalletServiceEventListenerRequest param2WalletServiceEventListenerRequest) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.quickaccesswallet.IQuickAccessWalletService");
          if (param2WalletServiceEventListenerRequest != null) {
            parcel.writeInt(1);
            param2WalletServiceEventListenerRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IQuickAccessWalletService.Stub.getDefaultImpl() != null) {
            IQuickAccessWalletService.Stub.getDefaultImpl().unregisterWalletServiceEventListener(param2WalletServiceEventListenerRequest);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IQuickAccessWalletService param1IQuickAccessWalletService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IQuickAccessWalletService != null) {
          Proxy.sDefaultImpl = param1IQuickAccessWalletService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IQuickAccessWalletService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
