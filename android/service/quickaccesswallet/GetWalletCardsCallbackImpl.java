package android.service.quickaccesswallet;

import android.graphics.Bitmap;
import android.graphics.drawable.Icon;
import android.os.Handler;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;

final class GetWalletCardsCallbackImpl implements GetWalletCardsCallback {
  private static final String TAG = "QAWalletCallback";
  
  private final IQuickAccessWalletServiceCallbacks mCallback;
  
  private boolean mCalled;
  
  private final Handler mHandler;
  
  private final GetWalletCardsRequest mRequest;
  
  GetWalletCardsCallbackImpl(GetWalletCardsRequest paramGetWalletCardsRequest, IQuickAccessWalletServiceCallbacks paramIQuickAccessWalletServiceCallbacks, Handler paramHandler) {
    this.mRequest = paramGetWalletCardsRequest;
    this.mCallback = paramIQuickAccessWalletServiceCallbacks;
    this.mHandler = paramHandler;
  }
  
  public void onSuccess(GetWalletCardsResponse paramGetWalletCardsResponse) {
    if (isValidResponse(paramGetWalletCardsResponse)) {
      this.mHandler.post(new _$$Lambda$GetWalletCardsCallbackImpl$QC3ZFKzSt3o3HZXPv79AXm_y5CU(this, paramGetWalletCardsResponse));
    } else {
      Log.w("QAWalletCallback", "Invalid GetWalletCards response");
      this.mHandler.post(new _$$Lambda$GetWalletCardsCallbackImpl$7xNzWesJhU8w8EsyP1THfKUBQJI(this));
    } 
  }
  
  public void onFailure(GetWalletCardsError paramGetWalletCardsError) {
    this.mHandler.post(new _$$Lambda$GetWalletCardsCallbackImpl$2AlMCVx8bzhL7Zj_xhmXCyi8dRM(this, paramGetWalletCardsError));
  }
  
  private void onSuccessInternal(GetWalletCardsResponse paramGetWalletCardsResponse) {
    if (this.mCalled) {
      Log.w("QAWalletCallback", "already called");
      return;
    } 
    this.mCalled = true;
    try {
      this.mCallback.onGetWalletCardsSuccess(paramGetWalletCardsResponse);
    } catch (RemoteException remoteException) {
      Log.w("QAWalletCallback", "Error returning wallet cards", (Throwable)remoteException);
    } 
  }
  
  private void onFailureInternal(GetWalletCardsError paramGetWalletCardsError) {
    if (this.mCalled) {
      Log.w("QAWalletCallback", "already called");
      return;
    } 
    this.mCalled = true;
    try {
      this.mCallback.onGetWalletCardsFailure(paramGetWalletCardsError);
    } catch (RemoteException remoteException) {
      Log.e("QAWalletCallback", "Error returning failure message", (Throwable)remoteException);
    } 
  }
  
  private boolean isValidResponse(GetWalletCardsResponse paramGetWalletCardsResponse) {
    if (paramGetWalletCardsResponse == null) {
      Log.w("QAWalletCallback", "Invalid response: response is null");
      return false;
    } 
    if (paramGetWalletCardsResponse.getWalletCards() == null) {
      Log.w("QAWalletCallback", "Invalid response: walletCards is null");
      return false;
    } 
    if (paramGetWalletCardsResponse.getSelectedIndex() < 0) {
      Log.w("QAWalletCallback", "Invalid response: selectedIndex is negative");
      return false;
    } 
    if (!paramGetWalletCardsResponse.getWalletCards().isEmpty() && 
      paramGetWalletCardsResponse.getSelectedIndex() >= paramGetWalletCardsResponse.getWalletCards().size()) {
      Log.w("QAWalletCallback", "Invalid response: selectedIndex out of bounds");
      return false;
    } 
    if (paramGetWalletCardsResponse.getWalletCards().size() > this.mRequest.getMaxCards()) {
      Log.w("QAWalletCallback", "Invalid response: too many cards");
      return false;
    } 
    for (WalletCard walletCard : paramGetWalletCardsResponse.getWalletCards()) {
      if (walletCard == null) {
        Log.w("QAWalletCallback", "Invalid response: card is null");
        return false;
      } 
      if (walletCard.getCardId() == null) {
        Log.w("QAWalletCallback", "Invalid response: cardId is null");
        return false;
      } 
      Icon icon = walletCard.getCardImage();
      if (icon == null) {
        Log.w("QAWalletCallback", "Invalid response: cardImage is null");
        return false;
      } 
      if (icon.getType() == 1 && 
        icon.getBitmap().getConfig() != Bitmap.Config.HARDWARE) {
        Log.w("QAWalletCallback", "Invalid response: cardImage bitmaps must be hardware bitmaps");
        return false;
      } 
      if (TextUtils.isEmpty(walletCard.getContentDescription())) {
        Log.w("QAWalletCallback", "Invalid response: contentDescription is null");
        return false;
      } 
      if (walletCard.getPendingIntent() == null) {
        Log.w("QAWalletCallback", "Invalid response: pendingIntent is null");
        return false;
      } 
    } 
    return true;
  }
}
