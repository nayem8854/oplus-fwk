package android.service.quickaccesswallet;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import java.io.Closeable;
import java.util.concurrent.Executor;

public interface QuickAccessWalletClient extends Closeable {
  static QuickAccessWalletClient create(Context paramContext) {
    return new QuickAccessWalletClientImpl(paramContext);
  }
  
  void addWalletServiceEventListener(WalletServiceEventListener paramWalletServiceEventListener);
  
  void addWalletServiceEventListener(Executor paramExecutor, WalletServiceEventListener paramWalletServiceEventListener);
  
  Intent createWalletIntent();
  
  Intent createWalletSettingsIntent();
  
  void disconnect();
  
  Drawable getLogo();
  
  CharSequence getServiceLabel();
  
  CharSequence getShortcutLongLabel();
  
  CharSequence getShortcutShortLabel();
  
  void getWalletCards(GetWalletCardsRequest paramGetWalletCardsRequest, OnWalletCardsRetrievedCallback paramOnWalletCardsRetrievedCallback);
  
  void getWalletCards(Executor paramExecutor, GetWalletCardsRequest paramGetWalletCardsRequest, OnWalletCardsRetrievedCallback paramOnWalletCardsRetrievedCallback);
  
  boolean isWalletFeatureAvailable();
  
  boolean isWalletFeatureAvailableWhenDeviceLocked();
  
  boolean isWalletServiceAvailable();
  
  void notifyWalletDismissed();
  
  void removeWalletServiceEventListener(WalletServiceEventListener paramWalletServiceEventListener);
  
  void selectWalletCard(SelectWalletCardRequest paramSelectWalletCardRequest);
  
  public static interface OnWalletCardsRetrievedCallback {
    void onWalletCardRetrievalError(GetWalletCardsError param1GetWalletCardsError);
    
    void onWalletCardsRetrieved(GetWalletCardsResponse param1GetWalletCardsResponse);
  }
  
  public static interface WalletServiceEventListener {
    void onWalletServiceEvent(WalletServiceEvent param1WalletServiceEvent);
  }
}
