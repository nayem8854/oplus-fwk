package android.service.quickaccesswallet;

import android.graphics.drawable.Icon;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public final class GetWalletCardsError implements Parcelable {
  public GetWalletCardsError(Icon paramIcon, CharSequence paramCharSequence) {
    this.mIcon = paramIcon;
    this.mMessage = paramCharSequence;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (this.mIcon == null) {
      paramParcel.writeByte((byte)0);
    } else {
      paramParcel.writeByte((byte)1);
      this.mIcon.writeToParcel(paramParcel, paramInt);
    } 
    TextUtils.writeToParcel(this.mMessage, paramParcel, paramInt);
  }
  
  private static GetWalletCardsError readFromParcel(Parcel paramParcel) {
    Icon icon;
    if (paramParcel.readByte() == 0) {
      icon = null;
    } else {
      icon = Icon.CREATOR.createFromParcel(paramParcel);
    } 
    CharSequence charSequence = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    return new GetWalletCardsError(icon, charSequence);
  }
  
  public static final Parcelable.Creator<GetWalletCardsError> CREATOR = new Parcelable.Creator<GetWalletCardsError>() {
      public GetWalletCardsError createFromParcel(Parcel param1Parcel) {
        return GetWalletCardsError.readFromParcel(param1Parcel);
      }
      
      public GetWalletCardsError[] newArray(int param1Int) {
        return new GetWalletCardsError[param1Int];
      }
    };
  
  private final Icon mIcon;
  
  private final CharSequence mMessage;
  
  public Icon getIcon() {
    return this.mIcon;
  }
  
  public CharSequence getMessage() {
    return this.mMessage;
  }
}
