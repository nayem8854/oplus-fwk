package android.service.quickaccesswallet;

import android.os.Parcel;
import android.os.Parcelable;

public final class SelectWalletCardRequest implements Parcelable {
  public SelectWalletCardRequest(String paramString) {
    this.mCardId = paramString;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mCardId);
  }
  
  public static final Parcelable.Creator<SelectWalletCardRequest> CREATOR = new Parcelable.Creator<SelectWalletCardRequest>() {
      public SelectWalletCardRequest createFromParcel(Parcel param1Parcel) {
        String str = param1Parcel.readString();
        return new SelectWalletCardRequest(str);
      }
      
      public SelectWalletCardRequest[] newArray(int param1Int) {
        return new SelectWalletCardRequest[param1Int];
      }
    };
  
  private final String mCardId;
  
  public String getCardId() {
    return this.mCardId;
  }
}
