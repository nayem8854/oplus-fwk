package android.service.quickaccesswallet;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class WalletServiceEvent implements Parcelable {
  public WalletServiceEvent(int paramInt) {
    this.mEventType = paramInt;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mEventType);
  }
  
  public static final Parcelable.Creator<WalletServiceEvent> CREATOR = new Parcelable.Creator<WalletServiceEvent>() {
      public WalletServiceEvent createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        return new WalletServiceEvent(i);
      }
      
      public WalletServiceEvent[] newArray(int param1Int) {
        return new WalletServiceEvent[param1Int];
      }
    };
  
  public static final int TYPE_NFC_PAYMENT_STARTED = 1;
  
  public static final int TYPE_WALLET_CARDS_UPDATED = 2;
  
  private final int mEventType;
  
  public int getEventType() {
    return this.mEventType;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class EventType implements Annotation {}
}
