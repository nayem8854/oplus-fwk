package android.service.quickaccesswallet;

import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;

public abstract class QuickAccessWalletService extends Service {
  public static final String ACTION_VIEW_WALLET = "android.service.quickaccesswallet.action.VIEW_WALLET";
  
  public static final String ACTION_VIEW_WALLET_SETTINGS = "android.service.quickaccesswallet.action.VIEW_WALLET_SETTINGS";
  
  public static final String SERVICE_INTERFACE = "android.service.quickaccesswallet.QuickAccessWalletService";
  
  public static final String SERVICE_META_DATA = "android.quickaccesswallet";
  
  private static final String TAG = "QAWalletService";
  
  private IQuickAccessWalletServiceCallbacks mEventListener;
  
  private String mEventListenerId;
  
  private final Handler mHandler = new Handler(Looper.getMainLooper());
  
  private final IQuickAccessWalletService mInterface = new IQuickAccessWalletService.Stub() {
      final QuickAccessWalletService this$0;
      
      public void onWalletCardsRequested(GetWalletCardsRequest param1GetWalletCardsRequest, IQuickAccessWalletServiceCallbacks param1IQuickAccessWalletServiceCallbacks) {
        QuickAccessWalletService.this.mHandler.post(new _$$Lambda$QuickAccessWalletService$1$pmX5cKFPyabzJ2dOCU_LsDg__vE(this, param1GetWalletCardsRequest, param1IQuickAccessWalletServiceCallbacks));
      }
      
      public void onWalletCardSelected(SelectWalletCardRequest param1SelectWalletCardRequest) {
        QuickAccessWalletService.this.mHandler.post(new _$$Lambda$QuickAccessWalletService$1$_Ou2Jo5kX_LbkKynHLFSKjfhSxo(this, param1SelectWalletCardRequest));
      }
      
      public void onWalletDismissed() {
        QuickAccessWalletService.this.mHandler.post(new _$$Lambda$eoUVVqlBTHEQjCFtLSXrcheCzY8(QuickAccessWalletService.this));
      }
      
      public void registerWalletServiceEventListener(WalletServiceEventListenerRequest param1WalletServiceEventListenerRequest, IQuickAccessWalletServiceCallbacks param1IQuickAccessWalletServiceCallbacks) {
        QuickAccessWalletService.this.mHandler.post(new _$$Lambda$QuickAccessWalletService$1$hHrL3hdXzFnVv3zziJxnOTslUkc(this, param1WalletServiceEventListenerRequest, param1IQuickAccessWalletServiceCallbacks));
      }
      
      public void unregisterWalletServiceEventListener(WalletServiceEventListenerRequest param1WalletServiceEventListenerRequest) {
        QuickAccessWalletService.this.mHandler.post(new _$$Lambda$QuickAccessWalletService$1$J4aE_vYSKeNpd1aOKr4Jss7Yyes(this, param1WalletServiceEventListenerRequest));
      }
    };
  
  private void onWalletCardsRequestedInternal(GetWalletCardsRequest paramGetWalletCardsRequest, IQuickAccessWalletServiceCallbacks paramIQuickAccessWalletServiceCallbacks) {
    onWalletCardsRequested(paramGetWalletCardsRequest, new GetWalletCardsCallbackImpl(paramGetWalletCardsRequest, paramIQuickAccessWalletServiceCallbacks, this.mHandler));
  }
  
  public IBinder onBind(Intent paramIntent) {
    if (Build.VERSION.SDK_INT < 30)
      Log.w("QAWalletService", "Warning: binding on pre-R device"); 
    if (!"android.service.quickaccesswallet.QuickAccessWalletService".equals(paramIntent.getAction())) {
      Log.w("QAWalletService", "Wrong action");
      return null;
    } 
    return this.mInterface.asBinder();
  }
  
  public final void sendWalletServiceEvent(WalletServiceEvent paramWalletServiceEvent) {
    this.mHandler.post(new _$$Lambda$QuickAccessWalletService$a0zGdWOVtWlyLPz_OW7fdSpVGxw(this, paramWalletServiceEvent));
  }
  
  private void sendWalletServiceEventInternal(WalletServiceEvent paramWalletServiceEvent) {
    IQuickAccessWalletServiceCallbacks iQuickAccessWalletServiceCallbacks = this.mEventListener;
    if (iQuickAccessWalletServiceCallbacks == null) {
      Log.i("QAWalletService", "No dismiss listener registered");
      return;
    } 
    try {
      iQuickAccessWalletServiceCallbacks.onWalletServiceEvent(paramWalletServiceEvent);
    } catch (RemoteException remoteException) {
      Log.w("QAWalletService", "onWalletServiceEvent error", (Throwable)remoteException);
      this.mEventListenerId = null;
      this.mEventListener = null;
    } 
  }
  
  private void registerDismissWalletListenerInternal(WalletServiceEventListenerRequest paramWalletServiceEventListenerRequest, IQuickAccessWalletServiceCallbacks paramIQuickAccessWalletServiceCallbacks) {
    this.mEventListenerId = paramWalletServiceEventListenerRequest.getListenerId();
    this.mEventListener = paramIQuickAccessWalletServiceCallbacks;
  }
  
  private void unregisterDismissWalletListenerInternal(WalletServiceEventListenerRequest paramWalletServiceEventListenerRequest) {
    String str = this.mEventListenerId;
    if (str != null && str.equals(paramWalletServiceEventListenerRequest.getListenerId())) {
      this.mEventListenerId = null;
      this.mEventListener = null;
    } else {
      Log.w("QAWalletService", "dismiss listener missing or replaced");
    } 
  }
  
  public abstract void onWalletCardSelected(SelectWalletCardRequest paramSelectWalletCardRequest);
  
  public abstract void onWalletCardsRequested(GetWalletCardsRequest paramGetWalletCardsRequest, GetWalletCardsCallback paramGetWalletCardsCallback);
  
  public abstract void onWalletDismissed();
}
