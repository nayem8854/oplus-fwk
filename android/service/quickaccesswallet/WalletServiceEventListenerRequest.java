package android.service.quickaccesswallet;

import android.os.Parcel;
import android.os.Parcelable;

public final class WalletServiceEventListenerRequest implements Parcelable {
  public WalletServiceEventListenerRequest(String paramString) {
    this.mListenerId = paramString;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mListenerId);
  }
  
  private static WalletServiceEventListenerRequest readFromParcel(Parcel paramParcel) {
    String str = paramParcel.readString();
    return new WalletServiceEventListenerRequest(str);
  }
  
  public static final Parcelable.Creator<WalletServiceEventListenerRequest> CREATOR = new Parcelable.Creator<WalletServiceEventListenerRequest>() {
      public WalletServiceEventListenerRequest createFromParcel(Parcel param1Parcel) {
        return WalletServiceEventListenerRequest.readFromParcel(param1Parcel);
      }
      
      public WalletServiceEventListenerRequest[] newArray(int param1Int) {
        return new WalletServiceEventListenerRequest[param1Int];
      }
    };
  
  private final String mListenerId;
  
  public String getListenerId() {
    return this.mListenerId;
  }
}
