package android.service.quickaccesswallet;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import com.android.internal.R;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;

class QuickAccessWalletServiceInfo {
  private static final String TAG = "QAWalletSInfo";
  
  private static final String TAG_WALLET_SERVICE = "quickaccesswallet-service";
  
  private final ServiceInfo mServiceInfo;
  
  private final ServiceMetadata mServiceMetadata;
  
  private QuickAccessWalletServiceInfo(ServiceInfo paramServiceInfo, ServiceMetadata paramServiceMetadata) {
    this.mServiceInfo = paramServiceInfo;
    this.mServiceMetadata = paramServiceMetadata;
  }
  
  static QuickAccessWalletServiceInfo tryCreate(Context paramContext) {
    ComponentName componentName = getDefaultPaymentApp(paramContext);
    if (componentName == null)
      return null; 
    ServiceInfo serviceInfo = getWalletServiceInfo(paramContext, componentName.getPackageName());
    if (serviceInfo == null)
      return null; 
    if (!"android.permission.BIND_QUICK_ACCESS_WALLET_SERVICE".equals(serviceInfo.permission)) {
      Log.w("QAWalletSInfo", String.format("%s.%s does not require permission %s", new Object[] { serviceInfo.packageName, serviceInfo.name, "android.permission.BIND_QUICK_ACCESS_WALLET_SERVICE" }));
      return null;
    } 
    ServiceMetadata serviceMetadata = parseServiceMetadata(paramContext, serviceInfo);
    return new QuickAccessWalletServiceInfo(serviceInfo, serviceMetadata);
  }
  
  private static ComponentName getDefaultPaymentApp(Context paramContext) {
    ComponentName componentName;
    ContentResolver contentResolver = paramContext.getContentResolver();
    String str = Settings.Secure.getString(contentResolver, "nfc_payment_default_component");
    if (str == null) {
      str = null;
    } else {
      componentName = ComponentName.unflattenFromString(str);
    } 
    return componentName;
  }
  
  private static ServiceInfo getWalletServiceInfo(Context paramContext, String paramString) {
    ServiceInfo serviceInfo;
    Intent intent = new Intent("android.service.quickaccesswallet.QuickAccessWalletService");
    intent.setPackage(paramString);
    List list = paramContext.getPackageManager().queryIntentServices(intent, 65536);
    if (list.isEmpty()) {
      list = null;
    } else {
      serviceInfo = ((ResolveInfo)list.get(0)).serviceInfo;
    } 
    return serviceInfo;
  }
  
  private static class ServiceMetadata {
    private final String mSettingsActivity;
    
    private final CharSequence mShortcutLongLabel;
    
    private final CharSequence mShortcutShortLabel;
    
    private final String mTargetActivity;
    
    private static ServiceMetadata empty() {
      return new ServiceMetadata(null, null, null, null);
    }
    
    private ServiceMetadata(String param1String1, String param1String2, CharSequence param1CharSequence1, CharSequence param1CharSequence2) {
      this.mTargetActivity = param1String1;
      this.mSettingsActivity = param1String2;
      this.mShortcutShortLabel = param1CharSequence1;
      this.mShortcutLongLabel = param1CharSequence2;
    }
  }
  
  private static ServiceMetadata parseServiceMetadata(Context paramContext, ServiceInfo paramServiceInfo) {
    PackageManager packageManager = paramContext.getPackageManager();
    XmlResourceParser xmlResourceParser = paramServiceInfo.loadXmlMetaData(packageManager, "android.quickaccesswallet");
    if (xmlResourceParser == null)
      return ServiceMetadata.empty(); 
    try {
      Resources resources = packageManager.getResourcesForApplication(paramServiceInfo.applicationInfo);
      int i = 0;
      while (i != 1 && i != 2)
        i = xmlResourceParser.next(); 
      if ("quickaccesswallet-service".equals(xmlResourceParser.getName())) {
        TypedArray typedArray;
        AttributeSet attributeSet = Xml.asAttributeSet((XmlPullParser)xmlResourceParser);
        xmlResourceParser = null;
        try {
          TypedArray typedArray1 = resources.obtainAttributes(attributeSet, R.styleable.QuickAccessWalletService);
          typedArray = typedArray1;
          String str1 = typedArray1.getString(0);
          typedArray = typedArray1;
          String str2 = typedArray1.getString(1);
          typedArray = typedArray1;
          CharSequence charSequence1 = typedArray1.getText(2);
          typedArray = typedArray1;
          CharSequence charSequence2 = typedArray1.getText(3);
          typedArray = typedArray1;
          ServiceMetadata serviceMetadata = new ServiceMetadata();
          typedArray = typedArray1;
          this(str1, str2, charSequence1, charSequence2);
          return serviceMetadata;
        } finally {
          if (typedArray != null)
            typedArray.recycle(); 
        } 
      } 
      Log.e("QAWalletSInfo", "Meta-data does not start with quickaccesswallet-service tag");
    } catch (android.content.pm.PackageManager.NameNotFoundException|java.io.IOException|org.xmlpull.v1.XmlPullParserException nameNotFoundException) {
      Log.e("QAWalletSInfo", "Error parsing quickaccesswallet service meta-data", (Throwable)nameNotFoundException);
    } 
    return ServiceMetadata.empty();
  }
  
  ComponentName getComponentName() {
    return this.mServiceInfo.getComponentName();
  }
  
  String getWalletActivity() {
    return this.mServiceMetadata.mTargetActivity;
  }
  
  String getSettingsActivity() {
    return this.mServiceMetadata.mSettingsActivity;
  }
  
  Drawable getWalletLogo(Context paramContext) {
    Drawable drawable = this.mServiceInfo.loadLogo(paramContext.getPackageManager());
    if (drawable != null)
      return drawable; 
    return this.mServiceInfo.loadIcon(paramContext.getPackageManager());
  }
  
  CharSequence getShortcutShortLabel(Context paramContext) {
    if (!TextUtils.isEmpty(this.mServiceMetadata.mShortcutShortLabel))
      return this.mServiceMetadata.mShortcutShortLabel; 
    return this.mServiceInfo.loadLabel(paramContext.getPackageManager());
  }
  
  CharSequence getShortcutLongLabel(Context paramContext) {
    if (!TextUtils.isEmpty(this.mServiceMetadata.mShortcutLongLabel))
      return this.mServiceMetadata.mShortcutLongLabel; 
    return this.mServiceInfo.loadLabel(paramContext.getPackageManager());
  }
  
  CharSequence getServiceLabel(Context paramContext) {
    return this.mServiceInfo.loadLabel(paramContext.getPackageManager());
  }
}
