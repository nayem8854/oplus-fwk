package android.service.quickaccesswallet;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.widget.LockPatternUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.Executor;

public class QuickAccessWalletClientImpl implements QuickAccessWalletClient, ServiceConnection {
  private static final int MSG_TIMEOUT_SERVICE = 5;
  
  private static final long SERVICE_CONNECTION_TIMEOUT_MS = 60000L;
  
  private static final String TAG = "QAWalletSClient";
  
  private final Context mContext;
  
  private final Map<QuickAccessWalletClient.WalletServiceEventListener, String> mEventListeners;
  
  private final Handler mHandler;
  
  private boolean mIsConnected;
  
  private final Queue<ApiCaller> mRequestQueue;
  
  private IQuickAccessWalletService mService;
  
  private final QuickAccessWalletServiceInfo mServiceInfo;
  
  QuickAccessWalletClientImpl(Context paramContext) {
    this.mContext = paramContext.getApplicationContext();
    this.mServiceInfo = QuickAccessWalletServiceInfo.tryCreate(paramContext);
    this.mHandler = new Handler(Looper.getMainLooper());
    this.mRequestQueue = new LinkedList<>();
    this.mEventListeners = new HashMap<>(1);
  }
  
  public boolean isWalletServiceAvailable() {
    boolean bool;
    if (this.mServiceInfo != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isWalletFeatureAvailable() {
    int i = ActivityManager.getCurrentUser();
    if (i == 0 && 
      checkUserSetupComplete() && 
      checkSecureSetting("global_actions_panel_enabled")) {
      LockPatternUtils lockPatternUtils = new LockPatternUtils(this.mContext);
      if (!lockPatternUtils.isUserInLockdown(i))
        return true; 
    } 
    return false;
  }
  
  public boolean isWalletFeatureAvailableWhenDeviceLocked() {
    return checkSecureSetting("power_menu_locked_show_content");
  }
  
  public void getWalletCards(GetWalletCardsRequest paramGetWalletCardsRequest, QuickAccessWalletClient.OnWalletCardsRetrievedCallback paramOnWalletCardsRetrievedCallback) {
    getWalletCards(this.mContext.getMainExecutor(), paramGetWalletCardsRequest, paramOnWalletCardsRetrievedCallback);
  }
  
  public void getWalletCards(Executor paramExecutor, final GetWalletCardsRequest request, QuickAccessWalletClient.OnWalletCardsRetrievedCallback paramOnWalletCardsRetrievedCallback) {
    if (!isWalletServiceAvailable()) {
      paramExecutor.execute(new _$$Lambda$QuickAccessWalletClientImpl$1rta7e7prvwImZMSYUVLF9RWQO0(paramOnWalletCardsRetrievedCallback));
      return;
    } 
    final Object serviceCallback = new Object(this, paramExecutor, paramOnWalletCardsRetrievedCallback);
    executeApiCall(new ApiCaller("onWalletCardsRequested") {
          final QuickAccessWalletClientImpl this$0;
          
          final GetWalletCardsRequest val$request;
          
          final QuickAccessWalletClientImpl.BaseCallbacks val$serviceCallback;
          
          public void performApiCall(IQuickAccessWalletService param1IQuickAccessWalletService) throws RemoteException {
            param1IQuickAccessWalletService.onWalletCardsRequested(request, serviceCallback);
          }
          
          public void onApiError() {
            serviceCallback.onGetWalletCardsFailure(new GetWalletCardsError(null, null));
          }
        });
  }
  
  public void selectWalletCard(final SelectWalletCardRequest request) {
    if (!isWalletServiceAvailable())
      return; 
    executeApiCall(new ApiCaller("onWalletCardSelected") {
          final QuickAccessWalletClientImpl this$0;
          
          final SelectWalletCardRequest val$request;
          
          public void performApiCall(IQuickAccessWalletService param1IQuickAccessWalletService) throws RemoteException {
            param1IQuickAccessWalletService.onWalletCardSelected(request);
          }
        });
  }
  
  public void notifyWalletDismissed() {
    if (!isWalletServiceAvailable())
      return; 
    executeApiCall(new ApiCaller("onWalletDismissed") {
          final QuickAccessWalletClientImpl this$0;
          
          public void performApiCall(IQuickAccessWalletService param1IQuickAccessWalletService) throws RemoteException {
            param1IQuickAccessWalletService.onWalletDismissed();
          }
        });
  }
  
  public void addWalletServiceEventListener(QuickAccessWalletClient.WalletServiceEventListener paramWalletServiceEventListener) {
    addWalletServiceEventListener(this.mContext.getMainExecutor(), paramWalletServiceEventListener);
  }
  
  public void addWalletServiceEventListener(Executor paramExecutor, final QuickAccessWalletClient.WalletServiceEventListener listener) {
    if (!isWalletServiceAvailable())
      return; 
    final Object callback = new Object(this, paramExecutor, listener);
    executeApiCall(new ApiCaller("registerListener") {
          final QuickAccessWalletClientImpl this$0;
          
          final QuickAccessWalletClientImpl.BaseCallbacks val$callback;
          
          final QuickAccessWalletClient.WalletServiceEventListener val$listener;
          
          public void performApiCall(IQuickAccessWalletService param1IQuickAccessWalletService) throws RemoteException {
            String str = UUID.randomUUID().toString();
            WalletServiceEventListenerRequest walletServiceEventListenerRequest = new WalletServiceEventListenerRequest(str);
            QuickAccessWalletClientImpl.this.mEventListeners.put(listener, str);
            param1IQuickAccessWalletService.registerWalletServiceEventListener(walletServiceEventListenerRequest, callback);
          }
        });
  }
  
  public void removeWalletServiceEventListener(final QuickAccessWalletClient.WalletServiceEventListener listener) {
    if (!isWalletServiceAvailable())
      return; 
    executeApiCall(new ApiCaller("unregisterListener") {
          final QuickAccessWalletClientImpl this$0;
          
          final QuickAccessWalletClient.WalletServiceEventListener val$listener;
          
          public void performApiCall(IQuickAccessWalletService param1IQuickAccessWalletService) throws RemoteException {
            String str = (String)QuickAccessWalletClientImpl.this.mEventListeners.remove(listener);
            if (str == null)
              return; 
            WalletServiceEventListenerRequest walletServiceEventListenerRequest = new WalletServiceEventListenerRequest(str);
            param1IQuickAccessWalletService.unregisterWalletServiceEventListener(walletServiceEventListenerRequest);
          }
        });
  }
  
  public void close() throws IOException {
    disconnect();
  }
  
  public void disconnect() {
    this.mHandler.post(new _$$Lambda$QuickAccessWalletClientImpl$vq8dtsjM9qYSfiFL0YG_3THsN0s(this));
  }
  
  public Intent createWalletIntent() {
    QuickAccessWalletServiceInfo quickAccessWalletServiceInfo = this.mServiceInfo;
    if (quickAccessWalletServiceInfo == null)
      return null; 
    String str2 = quickAccessWalletServiceInfo.getComponentName().getPackageName();
    String str1 = this.mServiceInfo.getWalletActivity();
    return createIntent(str1, str2, "android.service.quickaccesswallet.action.VIEW_WALLET");
  }
  
  public Intent createWalletSettingsIntent() {
    QuickAccessWalletServiceInfo quickAccessWalletServiceInfo = this.mServiceInfo;
    if (quickAccessWalletServiceInfo == null)
      return null; 
    String str1 = quickAccessWalletServiceInfo.getComponentName().getPackageName();
    String str2 = this.mServiceInfo.getSettingsActivity();
    return createIntent(str2, str1, "android.service.quickaccesswallet.action.VIEW_WALLET_SETTINGS");
  }
  
  private Intent createIntent(String paramString1, String paramString2, String paramString3) {
    PackageManager packageManager = this.mContext.getPackageManager();
    String str = paramString1;
    if (TextUtils.isEmpty(paramString1))
      str = queryActivityForAction(packageManager, paramString2, paramString3); 
    if (TextUtils.isEmpty(str))
      return null; 
    ComponentName componentName = new ComponentName(paramString2, str);
    if (!isActivityEnabled(packageManager, componentName))
      return null; 
    return (new Intent(paramString3)).setComponent(componentName);
  }
  
  private static String queryActivityForAction(PackageManager paramPackageManager, String paramString1, String paramString2) {
    Intent intent = (new Intent(paramString2)).setPackage(paramString1);
    ResolveInfo resolveInfo = paramPackageManager.resolveActivity(intent, 0);
    if (resolveInfo == null || resolveInfo.activityInfo == null || !resolveInfo.activityInfo.exported)
      return null; 
    return resolveInfo.activityInfo.name;
  }
  
  private static boolean isActivityEnabled(PackageManager paramPackageManager, ComponentName paramComponentName) {
    int i = paramPackageManager.getComponentEnabledSetting(paramComponentName);
    if (i == 1)
      return true; 
    if (i != 0)
      return false; 
    try {
      return paramPackageManager.getActivityInfo(paramComponentName, 0).isEnabled();
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      return false;
    } 
  }
  
  public Drawable getLogo() {
    Drawable drawable;
    QuickAccessWalletServiceInfo quickAccessWalletServiceInfo = this.mServiceInfo;
    if (quickAccessWalletServiceInfo == null) {
      quickAccessWalletServiceInfo = null;
    } else {
      drawable = quickAccessWalletServiceInfo.getWalletLogo(this.mContext);
    } 
    return drawable;
  }
  
  public CharSequence getServiceLabel() {
    CharSequence charSequence;
    QuickAccessWalletServiceInfo quickAccessWalletServiceInfo = this.mServiceInfo;
    if (quickAccessWalletServiceInfo == null) {
      quickAccessWalletServiceInfo = null;
    } else {
      charSequence = quickAccessWalletServiceInfo.getServiceLabel(this.mContext);
    } 
    return charSequence;
  }
  
  public CharSequence getShortcutShortLabel() {
    CharSequence charSequence;
    QuickAccessWalletServiceInfo quickAccessWalletServiceInfo = this.mServiceInfo;
    if (quickAccessWalletServiceInfo == null) {
      quickAccessWalletServiceInfo = null;
    } else {
      charSequence = quickAccessWalletServiceInfo.getShortcutShortLabel(this.mContext);
    } 
    return charSequence;
  }
  
  public CharSequence getShortcutLongLabel() {
    CharSequence charSequence;
    QuickAccessWalletServiceInfo quickAccessWalletServiceInfo = this.mServiceInfo;
    if (quickAccessWalletServiceInfo == null) {
      quickAccessWalletServiceInfo = null;
    } else {
      charSequence = quickAccessWalletServiceInfo.getShortcutLongLabel(this.mContext);
    } 
    return charSequence;
  }
  
  private void connect() {
    this.mHandler.post(new _$$Lambda$QuickAccessWalletClientImpl$zq3IA_w0x321BC_zCrZHGMkFsuo(this));
  }
  
  private void connectInternal() {
    if (this.mServiceInfo == null) {
      Log.w("QAWalletSClient", "Wallet service unavailable");
      return;
    } 
    if (this.mIsConnected)
      return; 
    this.mIsConnected = true;
    Intent intent = new Intent("android.service.quickaccesswallet.QuickAccessWalletService");
    intent.setComponent(this.mServiceInfo.getComponentName());
    this.mContext.bindService(intent, this, 33);
    resetServiceConnectionTimeout();
  }
  
  private void onConnectedInternal(IQuickAccessWalletService paramIQuickAccessWalletService) {
    if (!this.mIsConnected) {
      Log.w("QAWalletSClient", "onConnectInternal but connection closed");
      this.mService = null;
      return;
    } 
    this.mService = paramIQuickAccessWalletService;
    for (ApiCaller apiCaller : new ArrayList(this.mRequestQueue)) {
      performApiCallInternal(apiCaller, this.mService);
      this.mRequestQueue.remove(apiCaller);
    } 
  }
  
  private void resetServiceConnectionTimeout() {
    this.mHandler.removeMessages(5);
    this.mHandler.postDelayed(new _$$Lambda$QuickAccessWalletClientImpl$GBzEQZDL0gkMzecgeo8Z6BPZbAw(this), 5, 60000L);
  }
  
  private void disconnectInternal(boolean paramBoolean) {
    if (!this.mIsConnected) {
      Log.w("QAWalletSClient", "already disconnected");
      return;
    } 
    if (paramBoolean && !this.mEventListeners.isEmpty()) {
      for (QuickAccessWalletClient.WalletServiceEventListener walletServiceEventListener : this.mEventListeners.keySet())
        removeWalletServiceEventListener(walletServiceEventListener); 
      this.mHandler.post(new _$$Lambda$QuickAccessWalletClientImpl$7HA_OH57LeoccKifp5JF9tkdFeM(this));
      return;
    } 
    this.mIsConnected = false;
    this.mContext.unbindService(this);
    this.mService = null;
    this.mEventListeners.clear();
    this.mRequestQueue.clear();
  }
  
  private void executeApiCall(ApiCaller paramApiCaller) {
    this.mHandler.post(new _$$Lambda$QuickAccessWalletClientImpl$57_WwM8sEJa0kRLvImQRsKL0aJA(this, paramApiCaller));
  }
  
  private void executeInternal(ApiCaller paramApiCaller) {
    if (this.mIsConnected) {
      IQuickAccessWalletService iQuickAccessWalletService = this.mService;
      if (iQuickAccessWalletService != null) {
        performApiCallInternal(paramApiCaller, iQuickAccessWalletService);
        return;
      } 
    } 
    this.mRequestQueue.add(paramApiCaller);
    connect();
  }
  
  private void performApiCallInternal(ApiCaller paramApiCaller, IQuickAccessWalletService paramIQuickAccessWalletService) {
    if (paramIQuickAccessWalletService == null) {
      paramApiCaller.onApiError();
      return;
    } 
    try {
      paramApiCaller.performApiCall(paramIQuickAccessWalletService);
      resetServiceConnectionTimeout();
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("executeInternal error: ");
      stringBuilder.append(paramApiCaller.mDesc);
      Log.w("QAWalletSClient", stringBuilder.toString(), (Throwable)remoteException);
      paramApiCaller.onApiError();
      disconnect();
    } 
  }
  
  class ApiCaller {
    private final String mDesc;
    
    private ApiCaller(QuickAccessWalletClientImpl this$0) {
      this.mDesc = (String)this$0;
    }
    
    void onApiError() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("api error: ");
      stringBuilder.append(this.mDesc);
      Log.w("QAWalletSClient", stringBuilder.toString());
    }
    
    abstract void performApiCall(IQuickAccessWalletService param1IQuickAccessWalletService) throws RemoteException;
  }
  
  public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder) {
    IQuickAccessWalletService iQuickAccessWalletService = IQuickAccessWalletService.Stub.asInterface(paramIBinder);
    this.mHandler.post(new _$$Lambda$QuickAccessWalletClientImpl$diSuWqMhVKkvlN8SDX5FMwqS6XM(this, iQuickAccessWalletService));
  }
  
  public void onServiceDisconnected(ComponentName paramComponentName) {}
  
  public void onBindingDied(ComponentName paramComponentName) {
    disconnect();
  }
  
  public void onNullBinding(ComponentName paramComponentName) {
    disconnect();
  }
  
  private boolean checkSecureSetting(String paramString) {
    ContentResolver contentResolver = this.mContext.getContentResolver();
    boolean bool = false;
    if (Settings.Secure.getInt(contentResolver, paramString, 0) == 1)
      bool = true; 
    return bool;
  }
  
  private boolean checkUserSetupComplete() {
    Context context = this.mContext;
    ContentResolver contentResolver = context.getContentResolver();
    boolean bool = false;
    if (Settings.Secure.getIntForUser(contentResolver, "user_setup_complete", 0, -2) == 1)
      bool = true; 
    return bool;
  }
  
  class BaseCallbacks extends IQuickAccessWalletServiceCallbacks.Stub {
    private BaseCallbacks() {}
    
    public void onGetWalletCardsSuccess(GetWalletCardsResponse param1GetWalletCardsResponse) {
      throw new IllegalStateException();
    }
    
    public void onGetWalletCardsFailure(GetWalletCardsError param1GetWalletCardsError) {
      throw new IllegalStateException();
    }
    
    public void onWalletServiceEvent(WalletServiceEvent param1WalletServiceEvent) {
      throw new IllegalStateException();
    }
  }
}
