package android.service.quickaccesswallet;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public final class GetWalletCardsResponse implements Parcelable {
  public GetWalletCardsResponse(List<WalletCard> paramList, int paramInt) {
    this.mWalletCards = paramList;
    this.mSelectedIndex = paramInt;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mWalletCards.size());
    paramParcel.writeParcelableList(this.mWalletCards, paramInt);
    paramParcel.writeInt(this.mSelectedIndex);
  }
  
  private static GetWalletCardsResponse readFromParcel(Parcel paramParcel) {
    int i = paramParcel.readInt();
    ArrayList<Parcelable> arrayList = new ArrayList(i);
    List<Parcelable> list = paramParcel.readParcelableList(arrayList, WalletCard.class.getClassLoader());
    i = paramParcel.readInt();
    return new GetWalletCardsResponse((List)list, i);
  }
  
  public static final Parcelable.Creator<GetWalletCardsResponse> CREATOR = new Parcelable.Creator<GetWalletCardsResponse>() {
      public GetWalletCardsResponse createFromParcel(Parcel param1Parcel) {
        return GetWalletCardsResponse.readFromParcel(param1Parcel);
      }
      
      public GetWalletCardsResponse[] newArray(int param1Int) {
        return new GetWalletCardsResponse[param1Int];
      }
    };
  
  private final int mSelectedIndex;
  
  private final List<WalletCard> mWalletCards;
  
  public List<WalletCard> getWalletCards() {
    return this.mWalletCards;
  }
  
  public int getSelectedIndex() {
    return this.mSelectedIndex;
  }
}
