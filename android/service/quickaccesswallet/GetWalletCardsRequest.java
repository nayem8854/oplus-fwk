package android.service.quickaccesswallet;

import android.os.Parcel;
import android.os.Parcelable;

public final class GetWalletCardsRequest implements Parcelable {
  public GetWalletCardsRequest(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mCardWidthPx = paramInt1;
    this.mCardHeightPx = paramInt2;
    this.mIconSizePx = paramInt3;
    this.mMaxCards = paramInt4;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mCardWidthPx);
    paramParcel.writeInt(this.mCardHeightPx);
    paramParcel.writeInt(this.mIconSizePx);
    paramParcel.writeInt(this.mMaxCards);
  }
  
  public static final Parcelable.Creator<GetWalletCardsRequest> CREATOR = new Parcelable.Creator<GetWalletCardsRequest>() {
      public GetWalletCardsRequest createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        int k = param1Parcel.readInt();
        int m = param1Parcel.readInt();
        return new GetWalletCardsRequest(i, j, k, m);
      }
      
      public GetWalletCardsRequest[] newArray(int param1Int) {
        return new GetWalletCardsRequest[param1Int];
      }
    };
  
  private final int mCardHeightPx;
  
  private final int mCardWidthPx;
  
  private final int mIconSizePx;
  
  private final int mMaxCards;
  
  public int getCardWidthPx() {
    return this.mCardWidthPx;
  }
  
  public int getCardHeightPx() {
    return this.mCardHeightPx;
  }
  
  public int getIconSizePx() {
    return this.mIconSizePx;
  }
  
  public int getMaxCards() {
    return this.mMaxCards;
  }
}
