package android.service.media;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ResultReceiver;

public interface IMediaBrowserService extends IInterface {
  void addSubscription(String paramString, IBinder paramIBinder, Bundle paramBundle, IMediaBrowserServiceCallbacks paramIMediaBrowserServiceCallbacks) throws RemoteException;
  
  void addSubscriptionDeprecated(String paramString, IMediaBrowserServiceCallbacks paramIMediaBrowserServiceCallbacks) throws RemoteException;
  
  void connect(String paramString, Bundle paramBundle, IMediaBrowserServiceCallbacks paramIMediaBrowserServiceCallbacks) throws RemoteException;
  
  void disconnect(IMediaBrowserServiceCallbacks paramIMediaBrowserServiceCallbacks) throws RemoteException;
  
  void getMediaItem(String paramString, ResultReceiver paramResultReceiver, IMediaBrowserServiceCallbacks paramIMediaBrowserServiceCallbacks) throws RemoteException;
  
  void removeSubscription(String paramString, IBinder paramIBinder, IMediaBrowserServiceCallbacks paramIMediaBrowserServiceCallbacks) throws RemoteException;
  
  void removeSubscriptionDeprecated(String paramString, IMediaBrowserServiceCallbacks paramIMediaBrowserServiceCallbacks) throws RemoteException;
  
  class Default implements IMediaBrowserService {
    public void connect(String param1String, Bundle param1Bundle, IMediaBrowserServiceCallbacks param1IMediaBrowserServiceCallbacks) throws RemoteException {}
    
    public void disconnect(IMediaBrowserServiceCallbacks param1IMediaBrowserServiceCallbacks) throws RemoteException {}
    
    public void addSubscriptionDeprecated(String param1String, IMediaBrowserServiceCallbacks param1IMediaBrowserServiceCallbacks) throws RemoteException {}
    
    public void removeSubscriptionDeprecated(String param1String, IMediaBrowserServiceCallbacks param1IMediaBrowserServiceCallbacks) throws RemoteException {}
    
    public void getMediaItem(String param1String, ResultReceiver param1ResultReceiver, IMediaBrowserServiceCallbacks param1IMediaBrowserServiceCallbacks) throws RemoteException {}
    
    public void addSubscription(String param1String, IBinder param1IBinder, Bundle param1Bundle, IMediaBrowserServiceCallbacks param1IMediaBrowserServiceCallbacks) throws RemoteException {}
    
    public void removeSubscription(String param1String, IBinder param1IBinder, IMediaBrowserServiceCallbacks param1IMediaBrowserServiceCallbacks) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMediaBrowserService {
    private static final String DESCRIPTOR = "android.service.media.IMediaBrowserService";
    
    static final int TRANSACTION_addSubscription = 6;
    
    static final int TRANSACTION_addSubscriptionDeprecated = 3;
    
    static final int TRANSACTION_connect = 1;
    
    static final int TRANSACTION_disconnect = 2;
    
    static final int TRANSACTION_getMediaItem = 5;
    
    static final int TRANSACTION_removeSubscription = 7;
    
    static final int TRANSACTION_removeSubscriptionDeprecated = 4;
    
    public Stub() {
      attachInterface(this, "android.service.media.IMediaBrowserService");
    }
    
    public static IMediaBrowserService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.media.IMediaBrowserService");
      if (iInterface != null && iInterface instanceof IMediaBrowserService)
        return (IMediaBrowserService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "removeSubscription";
        case 6:
          return "addSubscription";
        case 5:
          return "getMediaItem";
        case 4:
          return "removeSubscriptionDeprecated";
        case 3:
          return "addSubscriptionDeprecated";
        case 2:
          return "disconnect";
        case 1:
          break;
      } 
      return "connect";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        IBinder iBinder1, iBinder2;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("android.service.media.IMediaBrowserService");
            str1 = param1Parcel1.readString();
            iBinder1 = param1Parcel1.readStrongBinder();
            iMediaBrowserServiceCallbacks = IMediaBrowserServiceCallbacks.Stub.asInterface(param1Parcel1.readStrongBinder());
            removeSubscription(str1, iBinder1, iMediaBrowserServiceCallbacks);
            return true;
          case 6:
            iMediaBrowserServiceCallbacks.enforceInterface("android.service.media.IMediaBrowserService");
            str1 = iMediaBrowserServiceCallbacks.readString();
            iBinder2 = iMediaBrowserServiceCallbacks.readStrongBinder();
            if (iMediaBrowserServiceCallbacks.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)iMediaBrowserServiceCallbacks);
            } else {
              iBinder1 = null;
            } 
            iMediaBrowserServiceCallbacks = IMediaBrowserServiceCallbacks.Stub.asInterface(iMediaBrowserServiceCallbacks.readStrongBinder());
            addSubscription(str1, iBinder2, (Bundle)iBinder1, iMediaBrowserServiceCallbacks);
            return true;
          case 5:
            iMediaBrowserServiceCallbacks.enforceInterface("android.service.media.IMediaBrowserService");
            str1 = iMediaBrowserServiceCallbacks.readString();
            if (iMediaBrowserServiceCallbacks.readInt() != 0) {
              ResultReceiver resultReceiver = ResultReceiver.CREATOR.createFromParcel((Parcel)iMediaBrowserServiceCallbacks);
            } else {
              iBinder1 = null;
            } 
            iMediaBrowserServiceCallbacks = IMediaBrowserServiceCallbacks.Stub.asInterface(iMediaBrowserServiceCallbacks.readStrongBinder());
            getMediaItem(str1, (ResultReceiver)iBinder1, iMediaBrowserServiceCallbacks);
            return true;
          case 4:
            iMediaBrowserServiceCallbacks.enforceInterface("android.service.media.IMediaBrowserService");
            str = iMediaBrowserServiceCallbacks.readString();
            iMediaBrowserServiceCallbacks = IMediaBrowserServiceCallbacks.Stub.asInterface(iMediaBrowserServiceCallbacks.readStrongBinder());
            removeSubscriptionDeprecated(str, iMediaBrowserServiceCallbacks);
            return true;
          case 3:
            iMediaBrowserServiceCallbacks.enforceInterface("android.service.media.IMediaBrowserService");
            str = iMediaBrowserServiceCallbacks.readString();
            iMediaBrowserServiceCallbacks = IMediaBrowserServiceCallbacks.Stub.asInterface(iMediaBrowserServiceCallbacks.readStrongBinder());
            addSubscriptionDeprecated(str, iMediaBrowserServiceCallbacks);
            return true;
          case 2:
            iMediaBrowserServiceCallbacks.enforceInterface("android.service.media.IMediaBrowserService");
            iMediaBrowserServiceCallbacks = IMediaBrowserServiceCallbacks.Stub.asInterface(iMediaBrowserServiceCallbacks.readStrongBinder());
            disconnect(iMediaBrowserServiceCallbacks);
            return true;
          case 1:
            break;
        } 
        iMediaBrowserServiceCallbacks.enforceInterface("android.service.media.IMediaBrowserService");
        String str1 = iMediaBrowserServiceCallbacks.readString();
        if (iMediaBrowserServiceCallbacks.readInt() != 0) {
          Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)iMediaBrowserServiceCallbacks);
        } else {
          str = null;
        } 
        IMediaBrowserServiceCallbacks iMediaBrowserServiceCallbacks = IMediaBrowserServiceCallbacks.Stub.asInterface(iMediaBrowserServiceCallbacks.readStrongBinder());
        connect(str1, (Bundle)str, iMediaBrowserServiceCallbacks);
        return true;
      } 
      str.writeString("android.service.media.IMediaBrowserService");
      return true;
    }
    
    private static class Proxy implements IMediaBrowserService {
      public static IMediaBrowserService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.media.IMediaBrowserService";
      }
      
      public void connect(String param2String, Bundle param2Bundle, IMediaBrowserServiceCallbacks param2IMediaBrowserServiceCallbacks) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.media.IMediaBrowserService");
          parcel.writeString(param2String);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IMediaBrowserServiceCallbacks != null) {
            iBinder = param2IMediaBrowserServiceCallbacks.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IMediaBrowserService.Stub.getDefaultImpl() != null) {
            IMediaBrowserService.Stub.getDefaultImpl().connect(param2String, param2Bundle, param2IMediaBrowserServiceCallbacks);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void disconnect(IMediaBrowserServiceCallbacks param2IMediaBrowserServiceCallbacks) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.media.IMediaBrowserService");
          if (param2IMediaBrowserServiceCallbacks != null) {
            iBinder = param2IMediaBrowserServiceCallbacks.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IMediaBrowserService.Stub.getDefaultImpl() != null) {
            IMediaBrowserService.Stub.getDefaultImpl().disconnect(param2IMediaBrowserServiceCallbacks);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void addSubscriptionDeprecated(String param2String, IMediaBrowserServiceCallbacks param2IMediaBrowserServiceCallbacks) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.media.IMediaBrowserService");
          parcel.writeString(param2String);
          if (param2IMediaBrowserServiceCallbacks != null) {
            iBinder = param2IMediaBrowserServiceCallbacks.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IMediaBrowserService.Stub.getDefaultImpl() != null) {
            IMediaBrowserService.Stub.getDefaultImpl().addSubscriptionDeprecated(param2String, param2IMediaBrowserServiceCallbacks);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeSubscriptionDeprecated(String param2String, IMediaBrowserServiceCallbacks param2IMediaBrowserServiceCallbacks) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.media.IMediaBrowserService");
          parcel.writeString(param2String);
          if (param2IMediaBrowserServiceCallbacks != null) {
            iBinder = param2IMediaBrowserServiceCallbacks.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IMediaBrowserService.Stub.getDefaultImpl() != null) {
            IMediaBrowserService.Stub.getDefaultImpl().removeSubscriptionDeprecated(param2String, param2IMediaBrowserServiceCallbacks);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getMediaItem(String param2String, ResultReceiver param2ResultReceiver, IMediaBrowserServiceCallbacks param2IMediaBrowserServiceCallbacks) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.media.IMediaBrowserService");
          parcel.writeString(param2String);
          if (param2ResultReceiver != null) {
            parcel.writeInt(1);
            param2ResultReceiver.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IMediaBrowserServiceCallbacks != null) {
            iBinder = param2IMediaBrowserServiceCallbacks.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IMediaBrowserService.Stub.getDefaultImpl() != null) {
            IMediaBrowserService.Stub.getDefaultImpl().getMediaItem(param2String, param2ResultReceiver, param2IMediaBrowserServiceCallbacks);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void addSubscription(String param2String, IBinder param2IBinder, Bundle param2Bundle, IMediaBrowserServiceCallbacks param2IMediaBrowserServiceCallbacks) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.media.IMediaBrowserService");
          parcel.writeString(param2String);
          parcel.writeStrongBinder(param2IBinder);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IMediaBrowserServiceCallbacks != null) {
            iBinder = param2IMediaBrowserServiceCallbacks.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IMediaBrowserService.Stub.getDefaultImpl() != null) {
            IMediaBrowserService.Stub.getDefaultImpl().addSubscription(param2String, param2IBinder, param2Bundle, param2IMediaBrowserServiceCallbacks);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeSubscription(String param2String, IBinder param2IBinder, IMediaBrowserServiceCallbacks param2IMediaBrowserServiceCallbacks) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.media.IMediaBrowserService");
          parcel.writeString(param2String);
          parcel.writeStrongBinder(param2IBinder);
          if (param2IMediaBrowserServiceCallbacks != null) {
            iBinder = param2IMediaBrowserServiceCallbacks.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && IMediaBrowserService.Stub.getDefaultImpl() != null) {
            IMediaBrowserService.Stub.getDefaultImpl().removeSubscription(param2String, param2IBinder, param2IMediaBrowserServiceCallbacks);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMediaBrowserService param1IMediaBrowserService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMediaBrowserService != null) {
          Proxy.sDefaultImpl = param1IMediaBrowserService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMediaBrowserService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
