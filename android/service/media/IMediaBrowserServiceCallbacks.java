package android.service.media;

import android.content.pm.ParceledListSlice;
import android.media.session.MediaSession;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IMediaBrowserServiceCallbacks extends IInterface {
  void onConnect(String paramString, MediaSession.Token paramToken, Bundle paramBundle) throws RemoteException;
  
  void onConnectFailed() throws RemoteException;
  
  void onLoadChildren(String paramString, ParceledListSlice paramParceledListSlice) throws RemoteException;
  
  void onLoadChildrenWithOptions(String paramString, ParceledListSlice paramParceledListSlice, Bundle paramBundle) throws RemoteException;
  
  class Default implements IMediaBrowserServiceCallbacks {
    public void onConnect(String param1String, MediaSession.Token param1Token, Bundle param1Bundle) throws RemoteException {}
    
    public void onConnectFailed() throws RemoteException {}
    
    public void onLoadChildren(String param1String, ParceledListSlice param1ParceledListSlice) throws RemoteException {}
    
    public void onLoadChildrenWithOptions(String param1String, ParceledListSlice param1ParceledListSlice, Bundle param1Bundle) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMediaBrowserServiceCallbacks {
    private static final String DESCRIPTOR = "android.service.media.IMediaBrowserServiceCallbacks";
    
    static final int TRANSACTION_onConnect = 1;
    
    static final int TRANSACTION_onConnectFailed = 2;
    
    static final int TRANSACTION_onLoadChildren = 3;
    
    static final int TRANSACTION_onLoadChildrenWithOptions = 4;
    
    public Stub() {
      attachInterface(this, "android.service.media.IMediaBrowserServiceCallbacks");
    }
    
    public static IMediaBrowserServiceCallbacks asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.media.IMediaBrowserServiceCallbacks");
      if (iInterface != null && iInterface instanceof IMediaBrowserServiceCallbacks)
        return (IMediaBrowserServiceCallbacks)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onLoadChildrenWithOptions";
          } 
          return "onLoadChildren";
        } 
        return "onConnectFailed";
      } 
      return "onConnect";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.service.media.IMediaBrowserServiceCallbacks");
              return true;
            } 
            param1Parcel1.enforceInterface("android.service.media.IMediaBrowserServiceCallbacks");
            String str2 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              ParceledListSlice parceledListSlice = ParceledListSlice.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onLoadChildrenWithOptions(str2, (ParceledListSlice)param1Parcel2, (Bundle)param1Parcel1);
            return true;
          } 
          param1Parcel1.enforceInterface("android.service.media.IMediaBrowserServiceCallbacks");
          String str1 = param1Parcel1.readString();
          if (param1Parcel1.readInt() != 0) {
            ParceledListSlice parceledListSlice = ParceledListSlice.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          onLoadChildren(str1, (ParceledListSlice)param1Parcel1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.service.media.IMediaBrowserServiceCallbacks");
        onConnectFailed();
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.media.IMediaBrowserServiceCallbacks");
      String str = param1Parcel1.readString();
      if (param1Parcel1.readInt() != 0) {
        MediaSession.Token token = MediaSession.Token.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onConnect(str, (MediaSession.Token)param1Parcel2, (Bundle)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IMediaBrowserServiceCallbacks {
      public static IMediaBrowserServiceCallbacks sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.media.IMediaBrowserServiceCallbacks";
      }
      
      public void onConnect(String param2String, MediaSession.Token param2Token, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.media.IMediaBrowserServiceCallbacks");
          parcel.writeString(param2String);
          if (param2Token != null) {
            parcel.writeInt(1);
            param2Token.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IMediaBrowserServiceCallbacks.Stub.getDefaultImpl() != null) {
            IMediaBrowserServiceCallbacks.Stub.getDefaultImpl().onConnect(param2String, param2Token, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onConnectFailed() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.media.IMediaBrowserServiceCallbacks");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IMediaBrowserServiceCallbacks.Stub.getDefaultImpl() != null) {
            IMediaBrowserServiceCallbacks.Stub.getDefaultImpl().onConnectFailed();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onLoadChildren(String param2String, ParceledListSlice param2ParceledListSlice) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.media.IMediaBrowserServiceCallbacks");
          parcel.writeString(param2String);
          if (param2ParceledListSlice != null) {
            parcel.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IMediaBrowserServiceCallbacks.Stub.getDefaultImpl() != null) {
            IMediaBrowserServiceCallbacks.Stub.getDefaultImpl().onLoadChildren(param2String, param2ParceledListSlice);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onLoadChildrenWithOptions(String param2String, ParceledListSlice param2ParceledListSlice, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.media.IMediaBrowserServiceCallbacks");
          parcel.writeString(param2String);
          if (param2ParceledListSlice != null) {
            parcel.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IMediaBrowserServiceCallbacks.Stub.getDefaultImpl() != null) {
            IMediaBrowserServiceCallbacks.Stub.getDefaultImpl().onLoadChildrenWithOptions(param2String, param2ParceledListSlice, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMediaBrowserServiceCallbacks param1IMediaBrowserServiceCallbacks) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMediaBrowserServiceCallbacks != null) {
          Proxy.sDefaultImpl = param1IMediaBrowserServiceCallbacks;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMediaBrowserServiceCallbacks getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
