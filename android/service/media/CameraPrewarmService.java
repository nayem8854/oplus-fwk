package android.service.media;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Messenger;

public abstract class CameraPrewarmService extends Service {
  public static final String ACTION_PREWARM = "android.service.media.CameraPrewarmService.ACTION_PREWARM";
  
  public static final int MSG_CAMERA_FIRED = 1;
  
  private boolean mCameraIntentFired;
  
  private final Handler mHandler = (Handler)new Object(this);
  
  public IBinder onBind(Intent paramIntent) {
    if ("android.service.media.CameraPrewarmService.ACTION_PREWARM".equals(paramIntent.getAction())) {
      onPrewarm();
      return (new Messenger(this.mHandler)).getBinder();
    } 
    return null;
  }
  
  public abstract void onCooldown(boolean paramBoolean);
  
  public abstract void onPrewarm();
  
  public boolean onUnbind(Intent paramIntent) {
    if ("android.service.media.CameraPrewarmService.ACTION_PREWARM".equals(paramIntent.getAction()))
      onCooldown(this.mCameraIntentFired); 
    return false;
  }
}
