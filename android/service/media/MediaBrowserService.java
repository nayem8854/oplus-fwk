package android.service.media;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.browse.MediaBrowser;
import android.media.browse.MediaBrowserUtils;
import android.media.session.MediaSession;
import android.media.session.MediaSessionManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.util.ArrayMap;
import android.util.Pair;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public abstract class MediaBrowserService extends Service {
  private static final boolean DBG = false;
  
  public static final String KEY_MEDIA_ITEM = "media_item";
  
  private static final int RESULT_ERROR = -1;
  
  private static final int RESULT_FLAG_ON_LOAD_ITEM_NOT_IMPLEMENTED = 2;
  
  private static final int RESULT_FLAG_OPTION_NOT_HANDLED = 1;
  
  private static final int RESULT_OK = 0;
  
  public static final String SERVICE_INTERFACE = "android.media.browse.MediaBrowserService";
  
  private static final String TAG = "MediaBrowserService";
  
  private ServiceBinder mBinder;
  
  private final ArrayMap<IBinder, ConnectionRecord> mConnections = new ArrayMap();
  
  private ConnectionRecord mCurConnection;
  
  private final Handler mHandler = new Handler();
  
  MediaSession.Token mSession;
  
  class ConnectionRecord implements IBinder.DeathRecipient {
    IMediaBrowserServiceCallbacks callbacks;
    
    int pid;
    
    String pkg;
    
    MediaBrowserService.BrowserRoot root;
    
    Bundle rootHints;
    
    private ConnectionRecord() {}
    
    HashMap<String, List<Pair<IBinder, Bundle>>> subscriptions = new HashMap<>();
    
    final MediaBrowserService this$0;
    
    int uid;
    
    public void binderDied() {
      MediaBrowserService.this.mHandler.post((Runnable)new Object(this));
    }
  }
  
  class Result<T> {
    private Object mDebug;
    
    private boolean mDetachCalled;
    
    private int mFlags;
    
    private boolean mSendResultCalled;
    
    final MediaBrowserService this$0;
    
    Result(Object param1Object) {
      this.mDebug = param1Object;
    }
    
    public void sendResult(T param1T) {
      if (!this.mSendResultCalled) {
        this.mSendResultCalled = true;
        onResultSent(param1T, this.mFlags);
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("sendResult() called twice for: ");
      stringBuilder.append(this.mDebug);
      throw new IllegalStateException(stringBuilder.toString());
    }
    
    public void detach() {
      if (!this.mDetachCalled) {
        if (!this.mSendResultCalled) {
          this.mDetachCalled = true;
          return;
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("detach() called when sendResult() had already been called for: ");
        stringBuilder1.append(this.mDebug);
        throw new IllegalStateException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("detach() called when detach() had already been called for: ");
      stringBuilder.append(this.mDebug);
      throw new IllegalStateException(stringBuilder.toString());
    }
    
    boolean isDone() {
      return (this.mDetachCalled || this.mSendResultCalled);
    }
    
    void setFlags(int param1Int) {
      this.mFlags = param1Int;
    }
    
    void onResultSent(T param1T, int param1Int) {}
  }
  
  private class ServiceBinder extends IMediaBrowserService.Stub {
    final MediaBrowserService this$0;
    
    private ServiceBinder() {}
    
    public void connect(String param1String, Bundle param1Bundle, IMediaBrowserServiceCallbacks param1IMediaBrowserServiceCallbacks) {
      int i = Binder.getCallingPid();
      int j = Binder.getCallingUid();
      if (MediaBrowserService.this.isValidPackage(param1String, j)) {
        MediaBrowserService.this.mHandler.post((Runnable)new Object(this, param1IMediaBrowserServiceCallbacks, param1String, i, j, param1Bundle));
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Package/uid mismatch: uid=");
      stringBuilder.append(j);
      stringBuilder.append(" package=");
      stringBuilder.append(param1String);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public void disconnect(IMediaBrowserServiceCallbacks param1IMediaBrowserServiceCallbacks) {
      MediaBrowserService.this.mHandler.post((Runnable)new Object(this, param1IMediaBrowserServiceCallbacks));
    }
    
    public void addSubscriptionDeprecated(String param1String, IMediaBrowserServiceCallbacks param1IMediaBrowserServiceCallbacks) {}
    
    public void addSubscription(String param1String, IBinder param1IBinder, Bundle param1Bundle, IMediaBrowserServiceCallbacks param1IMediaBrowserServiceCallbacks) {
      MediaBrowserService.this.mHandler.post((Runnable)new Object(this, param1IMediaBrowserServiceCallbacks, param1String, param1IBinder, param1Bundle));
    }
    
    public void removeSubscriptionDeprecated(String param1String, IMediaBrowserServiceCallbacks param1IMediaBrowserServiceCallbacks) {}
    
    public void removeSubscription(String param1String, IBinder param1IBinder, IMediaBrowserServiceCallbacks param1IMediaBrowserServiceCallbacks) {
      MediaBrowserService.this.mHandler.post((Runnable)new Object(this, param1IMediaBrowserServiceCallbacks, param1String, param1IBinder));
    }
    
    public void getMediaItem(String param1String, ResultReceiver param1ResultReceiver, IMediaBrowserServiceCallbacks param1IMediaBrowserServiceCallbacks) {
      MediaBrowserService.this.mHandler.post((Runnable)new Object(this, param1IMediaBrowserServiceCallbacks, param1String, param1ResultReceiver));
    }
  }
  
  public void onCreate() {
    super.onCreate();
    this.mBinder = new ServiceBinder();
  }
  
  public IBinder onBind(Intent paramIntent) {
    if ("android.media.browse.MediaBrowserService".equals(paramIntent.getAction()))
      return this.mBinder; 
    return null;
  }
  
  public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {}
  
  public void onLoadChildren(String paramString, Result<List<MediaBrowser.MediaItem>> paramResult, Bundle paramBundle) {
    paramResult.setFlags(1);
    onLoadChildren(paramString, paramResult);
  }
  
  public void onLoadItem(String paramString, Result<MediaBrowser.MediaItem> paramResult) {
    paramResult.setFlags(2);
    paramResult.sendResult(null);
  }
  
  public void setSessionToken(MediaSession.Token paramToken) {
    if (paramToken != null) {
      if (this.mSession == null) {
        this.mSession = paramToken;
        this.mHandler.post((Runnable)new Object(this, paramToken));
        return;
      } 
      throw new IllegalStateException("The session token has already been set.");
    } 
    throw new IllegalArgumentException("Session token may not be null.");
  }
  
  public MediaSession.Token getSessionToken() {
    return this.mSession;
  }
  
  public final Bundle getBrowserRootHints() {
    ConnectionRecord connectionRecord = this.mCurConnection;
    if (connectionRecord != null) {
      Bundle bundle;
      if (connectionRecord.rootHints == null) {
        connectionRecord = null;
      } else {
        bundle = new Bundle(this.mCurConnection.rootHints);
      } 
      return bundle;
    } 
    throw new IllegalStateException("This should be called inside of onGetRoot or onLoadChildren or onLoadItem methods");
  }
  
  public final MediaSessionManager.RemoteUserInfo getCurrentBrowserInfo() {
    ConnectionRecord connectionRecord = this.mCurConnection;
    if (connectionRecord != null)
      return new MediaSessionManager.RemoteUserInfo(connectionRecord.pkg, this.mCurConnection.pid, this.mCurConnection.uid); 
    throw new IllegalStateException("This should be called inside of onGetRoot or onLoadChildren or onLoadItem methods");
  }
  
  public void notifyChildrenChanged(String paramString) {
    notifyChildrenChangedInternal(paramString, null);
  }
  
  public void notifyChildrenChanged(String paramString, Bundle paramBundle) {
    if (paramBundle != null) {
      notifyChildrenChangedInternal(paramString, paramBundle);
      return;
    } 
    throw new IllegalArgumentException("options cannot be null in notifyChildrenChanged");
  }
  
  private void notifyChildrenChangedInternal(String paramString, Bundle paramBundle) {
    if (paramString != null) {
      this.mHandler.post((Runnable)new Object(this, paramString, paramBundle));
      return;
    } 
    throw new IllegalArgumentException("parentId cannot be null in notifyChildrenChanged");
  }
  
  private boolean isValidPackage(String paramString, int paramInt) {
    if (paramString == null)
      return false; 
    PackageManager packageManager = getPackageManager();
    String[] arrayOfString = packageManager.getPackagesForUid(paramInt);
    int i = arrayOfString.length;
    for (paramInt = 0; paramInt < i; paramInt++) {
      if (arrayOfString[paramInt].equals(paramString))
        return true; 
    } 
    return false;
  }
  
  private void addSubscription(String paramString, ConnectionRecord paramConnectionRecord, IBinder paramIBinder, Bundle paramBundle) {
    List<Pair> list1 = (List)paramConnectionRecord.subscriptions.get(paramString);
    List<Pair> list2 = list1;
    if (list1 == null)
      list2 = new ArrayList(); 
    for (Pair pair : list2) {
      if (paramIBinder == pair.first) {
        Bundle bundle = (Bundle)pair.second;
        if (MediaBrowserUtils.areSameOptions(paramBundle, bundle))
          return; 
      } 
    } 
    list2.add(new Pair(paramIBinder, paramBundle));
    paramConnectionRecord.subscriptions.put(paramString, list2);
    performLoadChildren(paramString, paramConnectionRecord, paramBundle);
  }
  
  private boolean removeSubscription(String paramString, ConnectionRecord paramConnectionRecord, IBinder paramIBinder) {
    if (paramIBinder == null) {
      boolean bool;
      if (paramConnectionRecord.subscriptions.remove(paramString) != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
    boolean bool2 = false, bool1 = false;
    List list = paramConnectionRecord.subscriptions.get(paramString);
    if (list != null) {
      Iterator iterator = list.iterator();
      while (iterator.hasNext()) {
        if (paramIBinder == ((Pair)iterator.next()).first) {
          bool1 = true;
          iterator.remove();
        } 
      } 
      bool2 = bool1;
      if (list.size() == 0) {
        paramConnectionRecord.subscriptions.remove(paramString);
        bool2 = bool1;
      } 
    } 
    return bool2;
  }
  
  private void performLoadChildren(String paramString, ConnectionRecord paramConnectionRecord, Bundle paramBundle) {
    Object object = new Object(this, paramString, paramConnectionRecord, paramString, paramBundle);
    this.mCurConnection = paramConnectionRecord;
    if (paramBundle == null) {
      onLoadChildren(paramString, (Result<List<MediaBrowser.MediaItem>>)object);
    } else {
      onLoadChildren(paramString, (Result<List<MediaBrowser.MediaItem>>)object, paramBundle);
    } 
    this.mCurConnection = null;
    if (object.isDone())
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("onLoadChildren must call detach() or sendResult() before returning for package=");
    stringBuilder.append(paramConnectionRecord.pkg);
    stringBuilder.append(" id=");
    stringBuilder.append(paramString);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  private List<MediaBrowser.MediaItem> applyOptions(List<MediaBrowser.MediaItem> paramList, Bundle paramBundle) {
    if (paramList == null)
      return null; 
    int i = paramBundle.getInt("android.media.browse.extra.PAGE", -1);
    int j = paramBundle.getInt("android.media.browse.extra.PAGE_SIZE", -1);
    if (i == -1 && j == -1)
      return paramList; 
    int k = j * i;
    int m = k + j;
    if (i < 0 || j < 1 || k >= paramList.size())
      return Collections.EMPTY_LIST; 
    j = m;
    if (m > paramList.size())
      j = paramList.size(); 
    return paramList.subList(k, j);
  }
  
  private void performLoadItem(String paramString, ConnectionRecord paramConnectionRecord, ResultReceiver paramResultReceiver) {
    Object object = new Object(this, paramString, paramConnectionRecord, paramString, paramResultReceiver);
    this.mCurConnection = paramConnectionRecord;
    onLoadItem(paramString, (Result<MediaBrowser.MediaItem>)object);
    this.mCurConnection = null;
    if (object.isDone())
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("onLoadItem must call detach() or sendResult() before returning for id=");
    stringBuilder.append(paramString);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public abstract BrowserRoot onGetRoot(String paramString, int paramInt, Bundle paramBundle);
  
  public abstract void onLoadChildren(String paramString, Result<List<MediaBrowser.MediaItem>> paramResult);
  
  class BrowserRoot {
    public static final String EXTRA_OFFLINE = "android.service.media.extra.OFFLINE";
    
    public static final String EXTRA_RECENT = "android.service.media.extra.RECENT";
    
    public static final String EXTRA_SUGGESTED = "android.service.media.extra.SUGGESTED";
    
    private final Bundle mExtras;
    
    private final String mRootId;
    
    public BrowserRoot(MediaBrowserService this$0, Bundle param1Bundle) {
      if (this$0 != null) {
        this.mRootId = (String)this$0;
        this.mExtras = param1Bundle;
        return;
      } 
      throw new IllegalArgumentException("The root id in BrowserRoot cannot be null. Use null for BrowserRoot instead.");
    }
    
    public String getRootId() {
      return this.mRootId;
    }
    
    public Bundle getExtras() {
      return this.mExtras;
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ResultFlags implements Annotation {}
}
