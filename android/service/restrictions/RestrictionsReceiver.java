package android.service.restrictions;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PersistableBundle;

public abstract class RestrictionsReceiver extends BroadcastReceiver {
  private static final String TAG = "RestrictionsReceiver";
  
  public void onReceive(Context paramContext, Intent paramIntent) {
    String str = paramIntent.getAction();
    if ("android.content.action.REQUEST_PERMISSION".equals(str)) {
      str = paramIntent.getStringExtra("android.content.extra.PACKAGE_NAME");
      String str1 = paramIntent.getStringExtra("android.content.extra.REQUEST_TYPE");
      String str2 = paramIntent.getStringExtra("android.content.extra.REQUEST_ID");
      PersistableBundle persistableBundle = (PersistableBundle)paramIntent.getParcelableExtra("android.content.extra.REQUEST_BUNDLE");
      onRequestPermission(paramContext, str, str1, str2, persistableBundle);
    } 
  }
  
  public abstract void onRequestPermission(Context paramContext, String paramString1, String paramString2, String paramString3, PersistableBundle paramPersistableBundle);
}
