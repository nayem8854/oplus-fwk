package android.service.autofill;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.util.Pair;
import android.view.autofill.AutofillId;
import android.view.autofill.Helper;
import android.widget.RemoteViews;
import com.android.internal.util.Preconditions;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class CharSequenceTransformation extends InternalTransformation implements Transformation, Parcelable {
  private CharSequenceTransformation(Builder paramBuilder) {
    this.mFields = paramBuilder.mFields;
  }
  
  public void apply(ValueFinder paramValueFinder, RemoteViews paramRemoteViews, int paramInt) throws Exception {
    String str;
    StringBuilder stringBuilder2 = new StringBuilder();
    int i = this.mFields.size();
    if (Helper.sDebug) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(i);
      stringBuilder.append(" fields on id ");
      stringBuilder.append(paramInt);
      Log.d("CharSequenceTransformation", stringBuilder.toString());
    } 
    for (Map.Entry<AutofillId, Pair<Pattern, String>> entry : this.mFields.entrySet()) {
      AutofillId autofillId = (AutofillId)entry.getKey();
      Pair pair = (Pair)entry.getValue();
      String str1 = paramValueFinder.findByAutofillId(autofillId);
      if (str1 == null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("No value for id ");
        stringBuilder.append(autofillId);
        Log.w("CharSequenceTransformation", stringBuilder.toString());
        return;
      } 
      try {
        Matcher matcher = ((Pattern)pair.first).matcher(str1);
        if (!matcher.find()) {
          if (Helper.sDebug) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Match for ");
            stringBuilder.append(pair.first);
            stringBuilder.append(" failed on id ");
            stringBuilder.append(autofillId);
            Log.d("CharSequenceTransformation", stringBuilder.toString());
          } 
          return;
        } 
        String str2 = matcher.replaceAll((String)pair.second);
        stringBuilder2.append(str2);
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Cannot apply ");
        stringBuilder.append(((Pattern)pair.first).pattern());
        stringBuilder.append("->");
        stringBuilder.append((String)pair.second);
        stringBuilder.append(" to field with autofill id");
        stringBuilder.append(autofillId);
        stringBuilder.append(": ");
        stringBuilder.append(exception.getClass());
        str = stringBuilder.toString();
        Log.w("CharSequenceTransformation", str);
        throw exception;
      } 
    } 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("Converting text on child ");
    stringBuilder1.append(paramInt);
    stringBuilder1.append(" to ");
    stringBuilder1.append(stringBuilder2.length());
    stringBuilder1.append("_chars");
    Log.d("CharSequenceTransformation", stringBuilder1.toString());
    str.setCharSequence(paramInt, "setText", stringBuilder2);
  }
  
  class Builder {
    private boolean mDestroyed;
    
    private final LinkedHashMap<AutofillId, Pair<Pattern, String>> mFields = new LinkedHashMap<>();
    
    public Builder(CharSequenceTransformation this$0, Pattern param1Pattern, String param1String) {
      addField((AutofillId)this$0, param1Pattern, param1String);
    }
    
    public Builder addField(AutofillId param1AutofillId, Pattern param1Pattern, String param1String) {
      throwIfDestroyed();
      Preconditions.checkNotNull(param1AutofillId);
      Preconditions.checkNotNull(param1Pattern);
      Preconditions.checkNotNull(param1String);
      this.mFields.put(param1AutofillId, new Pair(param1Pattern, param1String));
      return this;
    }
    
    public CharSequenceTransformation build() {
      throwIfDestroyed();
      this.mDestroyed = true;
      return new CharSequenceTransformation(this);
    }
    
    private void throwIfDestroyed() {
      Preconditions.checkState(this.mDestroyed ^ true, "Already called build()");
    }
  }
  
  public String toString() {
    if (!Helper.sDebug)
      return super.toString(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("MultipleViewsCharSequenceTransformation: [fields=");
    stringBuilder.append(this.mFields);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    int i = this.mFields.size();
    AutofillId[] arrayOfAutofillId = new AutofillId[i];
    Pattern[] arrayOfPattern = new Pattern[i];
    String[] arrayOfString = new String[i];
    i = 0;
    for (Map.Entry<AutofillId, Pair<Pattern, String>> entry : this.mFields.entrySet()) {
      arrayOfAutofillId[i] = (AutofillId)entry.getKey();
      Pair pair = (Pair)entry.getValue();
      arrayOfPattern[i] = (Pattern)pair.first;
      arrayOfString[i] = (String)pair.second;
      i++;
    } 
    paramParcel.writeParcelableArray(arrayOfAutofillId, paramInt);
    paramParcel.writeSerializable((Serializable)arrayOfPattern);
    paramParcel.writeStringArray(arrayOfString);
  }
  
  public static final Parcelable.Creator<CharSequenceTransformation> CREATOR = (Parcelable.Creator<CharSequenceTransformation>)new Object();
  
  private static final String TAG = "CharSequenceTransformation";
  
  private final LinkedHashMap<AutofillId, Pair<Pattern, String>> mFields;
}
