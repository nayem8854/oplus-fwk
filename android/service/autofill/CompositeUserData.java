package android.service.autofill;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.ArrayMap;
import android.view.autofill.Helper;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;
import java.util.Collections;

public final class CompositeUserData implements FieldClassificationUserData, Parcelable {
  public CompositeUserData(UserData paramUserData1, UserData paramUserData2) {
    this.mGenericUserData = paramUserData1;
    this.mPackageUserData = paramUserData2;
    String[] arrayOfString2 = paramUserData2.getCategoryIds();
    String[] arrayOfString4 = this.mPackageUserData.getValues();
    ArrayList<? super String> arrayList2 = new ArrayList(arrayOfString2.length);
    ArrayList<? super String> arrayList1 = new ArrayList(arrayOfString4.length);
    Collections.addAll(arrayList2, arrayOfString2);
    Collections.addAll(arrayList1, arrayOfString4);
    UserData userData = this.mGenericUserData;
    if (userData != null) {
      String[] arrayOfString = userData.getCategoryIds();
      arrayOfString2 = this.mGenericUserData.getValues();
      int i = (this.mGenericUserData.getCategoryIds()).length;
      for (byte b = 0; b < i; b++) {
        if (!arrayList2.contains(arrayOfString[b])) {
          arrayList2.add(arrayOfString[b]);
          arrayList1.add(arrayOfString2[b]);
        } 
      } 
    } 
    String[] arrayOfString3 = new String[arrayList2.size()];
    arrayList2.toArray(arrayOfString3);
    String[] arrayOfString1 = new String[arrayList1.size()];
    arrayList1.toArray(arrayOfString1);
  }
  
  public String getFieldClassificationAlgorithm() {
    String str1, str2 = this.mPackageUserData.getFieldClassificationAlgorithm();
    if (str2 != null)
      return str2; 
    UserData userData = this.mGenericUserData;
    if (userData == null) {
      userData = null;
    } else {
      str1 = userData.getFieldClassificationAlgorithm();
    } 
    return str1;
  }
  
  public Bundle getDefaultFieldClassificationArgs() {
    Bundle bundle1, bundle2 = this.mPackageUserData.getDefaultFieldClassificationArgs();
    if (bundle2 != null)
      return bundle2; 
    UserData userData = this.mGenericUserData;
    if (userData == null) {
      userData = null;
    } else {
      bundle1 = userData.getDefaultFieldClassificationArgs();
    } 
    return bundle1;
  }
  
  public String getFieldClassificationAlgorithmForCategory(String paramString) {
    Preconditions.checkNotNull(paramString);
    ArrayMap<String, String> arrayMap = getFieldClassificationAlgorithms();
    if (arrayMap == null || !arrayMap.containsKey(paramString))
      return null; 
    return (String)arrayMap.get(paramString);
  }
  
  public ArrayMap<String, String> getFieldClassificationAlgorithms() {
    ArrayMap<String, String> arrayMap1;
    UserData userData = this.mPackageUserData;
    ArrayMap<String, String> arrayMap2 = userData.getFieldClassificationAlgorithms();
    userData = this.mGenericUserData;
    if (userData == null) {
      userData = null;
    } else {
      arrayMap1 = userData.getFieldClassificationAlgorithms();
    } 
    ArrayMap<String, String> arrayMap3 = null;
    if (arrayMap2 != null || arrayMap1 != null) {
      ArrayMap<String, String> arrayMap = new ArrayMap();
      if (arrayMap1 != null)
        arrayMap.putAll(arrayMap1); 
      arrayMap3 = arrayMap;
      if (arrayMap2 != null) {
        arrayMap.putAll(arrayMap2);
        arrayMap3 = arrayMap;
      } 
    } 
    return arrayMap3;
  }
  
  public ArrayMap<String, Bundle> getFieldClassificationArgs() {
    ArrayMap<String, Bundle> arrayMap2, arrayMap1 = this.mPackageUserData.getFieldClassificationArgs();
    UserData userData = this.mGenericUserData;
    if (userData == null) {
      userData = null;
    } else {
      arrayMap2 = userData.getFieldClassificationArgs();
    } 
    ArrayMap<String, Bundle> arrayMap3 = null;
    if (arrayMap1 != null || arrayMap2 != null) {
      ArrayMap<String, Bundle> arrayMap = new ArrayMap();
      if (arrayMap2 != null)
        arrayMap.putAll(arrayMap2); 
      arrayMap3 = arrayMap;
      if (arrayMap1 != null) {
        arrayMap.putAll(arrayMap1);
        arrayMap3 = arrayMap;
      } 
    } 
    return arrayMap3;
  }
  
  public String[] getCategoryIds() {
    return this.mCategories;
  }
  
  public String[] getValues() {
    return this.mValues;
  }
  
  public String toString() {
    if (!Helper.sDebug)
      return super.toString(); 
    StringBuilder stringBuilder1 = new StringBuilder("genericUserData=");
    UserData userData = this.mGenericUserData;
    stringBuilder1.append(userData);
    stringBuilder1.append(", packageUserData=");
    StringBuilder stringBuilder2 = stringBuilder1.append(this.mPackageUserData);
    return stringBuilder2.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.mGenericUserData, 0);
    paramParcel.writeParcelable(this.mPackageUserData, 0);
  }
  
  public static final Parcelable.Creator<CompositeUserData> CREATOR = new Parcelable.Creator<CompositeUserData>() {
      public CompositeUserData createFromParcel(Parcel param1Parcel) {
        UserData userData2 = param1Parcel.<UserData>readParcelable(null);
        UserData userData1 = param1Parcel.<UserData>readParcelable(null);
        return new CompositeUserData(userData2, userData1);
      }
      
      public CompositeUserData[] newArray(int param1Int) {
        return new CompositeUserData[param1Int];
      }
    };
  
  private final String[] mCategories;
  
  private final UserData mGenericUserData;
  
  private final UserData mPackageUserData;
  
  private final String[] mValues;
}
