package android.service.autofill;

import android.content.IntentSender;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.text.TextUtils;

public interface ISaveCallback extends IInterface {
  void onFailure(CharSequence paramCharSequence) throws RemoteException;
  
  void onSuccess(IntentSender paramIntentSender) throws RemoteException;
  
  class Default implements ISaveCallback {
    public void onSuccess(IntentSender param1IntentSender) throws RemoteException {}
    
    public void onFailure(CharSequence param1CharSequence) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISaveCallback {
    private static final String DESCRIPTOR = "android.service.autofill.ISaveCallback";
    
    static final int TRANSACTION_onFailure = 2;
    
    static final int TRANSACTION_onSuccess = 1;
    
    public Stub() {
      attachInterface(this, "android.service.autofill.ISaveCallback");
    }
    
    public static ISaveCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.autofill.ISaveCallback");
      if (iInterface != null && iInterface instanceof ISaveCallback)
        return (ISaveCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onFailure";
      } 
      return "onSuccess";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.service.autofill.ISaveCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("android.service.autofill.ISaveCallback");
        if (param1Parcel1.readInt() != 0) {
          CharSequence charSequence = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onFailure((CharSequence)param1Parcel1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.autofill.ISaveCallback");
      if (param1Parcel1.readInt() != 0) {
        IntentSender intentSender = IntentSender.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onSuccess((IntentSender)param1Parcel1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements ISaveCallback {
      public static ISaveCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.autofill.ISaveCallback";
      }
      
      public void onSuccess(IntentSender param2IntentSender) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.autofill.ISaveCallback");
          if (param2IntentSender != null) {
            parcel1.writeInt(1);
            param2IntentSender.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ISaveCallback.Stub.getDefaultImpl() != null) {
            ISaveCallback.Stub.getDefaultImpl().onSuccess(param2IntentSender);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onFailure(CharSequence param2CharSequence) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.autofill.ISaveCallback");
          if (param2CharSequence != null) {
            parcel1.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ISaveCallback.Stub.getDefaultImpl() != null) {
            ISaveCallback.Stub.getDefaultImpl().onFailure(param2CharSequence);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISaveCallback param1ISaveCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISaveCallback != null) {
          Proxy.sDefaultImpl = param1ISaveCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISaveCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
