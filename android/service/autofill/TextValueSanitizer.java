package android.service.autofill;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Slog;
import android.view.autofill.AutofillValue;
import android.view.autofill.Helper;
import com.android.internal.util.Preconditions;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class TextValueSanitizer extends InternalSanitizer implements Sanitizer, Parcelable {
  public TextValueSanitizer(Pattern paramPattern, String paramString) {
    this.mRegex = (Pattern)Preconditions.checkNotNull(paramPattern);
    this.mSubst = (String)Preconditions.checkNotNull(paramString);
  }
  
  public AutofillValue sanitize(AutofillValue paramAutofillValue) {
    if (paramAutofillValue == null) {
      Slog.w("TextValueSanitizer", "sanitize() called with null value");
      return null;
    } 
    if (!paramAutofillValue.isText()) {
      if (Helper.sDebug) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("sanitize() called with non-text value: ");
        stringBuilder.append(paramAutofillValue);
        Slog.d("TextValueSanitizer", stringBuilder.toString());
      } 
      return null;
    } 
    CharSequence charSequence = paramAutofillValue.getTextValue();
    try {
      StringBuilder stringBuilder;
      Matcher matcher = this.mRegex.matcher(charSequence);
      if (!matcher.matches()) {
        if (Helper.sDebug) {
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("sanitize(): ");
          stringBuilder.append(this.mRegex);
          stringBuilder.append(" failed for ");
          stringBuilder.append(paramAutofillValue);
          Slog.d("TextValueSanitizer", stringBuilder.toString());
        } 
        return null;
      } 
      String str = stringBuilder.replaceAll(this.mSubst);
      return AutofillValue.forText(str);
    } catch (Exception exception) {
      charSequence = new StringBuilder();
      charSequence.append("Exception evaluating ");
      charSequence.append(this.mRegex);
      charSequence.append("/");
      charSequence.append(this.mSubst);
      charSequence.append(": ");
      charSequence.append(exception);
      Slog.w("TextValueSanitizer", charSequence.toString());
      return null;
    } 
  }
  
  public String toString() {
    if (!Helper.sDebug)
      return super.toString(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("TextValueSanitizer: [regex=");
    stringBuilder.append(this.mRegex);
    stringBuilder.append(", subst=");
    stringBuilder.append(this.mSubst);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeSerializable(this.mRegex);
    paramParcel.writeString(this.mSubst);
  }
  
  public static final Parcelable.Creator<TextValueSanitizer> CREATOR = (Parcelable.Creator<TextValueSanitizer>)new Object();
  
  private static final String TAG = "TextValueSanitizer";
  
  private final Pattern mRegex;
  
  private final String mSubst;
}
