package android.service.autofill;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.view.autofill.Helper;
import com.android.internal.util.Preconditions;

final class OptionalValidators extends InternalValidator {
  OptionalValidators(InternalValidator[] paramArrayOfInternalValidator) {
    this.mValidators = (InternalValidator[])Preconditions.checkArrayElementsNotNull((Object[])paramArrayOfInternalValidator, "validators");
  }
  
  public boolean isValid(ValueFinder paramValueFinder) {
    for (InternalValidator internalValidator : this.mValidators) {
      boolean bool = internalValidator.isValid(paramValueFinder);
      if (Helper.sDebug) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("isValid(");
        stringBuilder.append(internalValidator);
        stringBuilder.append("): ");
        stringBuilder.append(bool);
        Log.d("OptionalValidators", stringBuilder.toString());
      } 
      if (bool)
        return true; 
    } 
    return false;
  }
  
  public String toString() {
    if (!Helper.sDebug)
      return super.toString(); 
    StringBuilder stringBuilder = new StringBuilder("OptionalValidators: [validators=");
    stringBuilder.append(this.mValidators);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelableArray(this.mValidators, paramInt);
  }
  
  public static final Parcelable.Creator<OptionalValidators> CREATOR = (Parcelable.Creator<OptionalValidators>)new Object();
  
  private static final String TAG = "OptionalValidators";
  
  private final InternalValidator[] mValidators;
}
