package android.service.autofill;

import android.icu.text.DateFormat;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.view.autofill.AutofillValue;
import android.view.autofill.Helper;
import com.android.internal.util.Preconditions;
import java.io.Serializable;
import java.util.Date;

public final class DateValueSanitizer extends InternalSanitizer implements Sanitizer, Parcelable {
  public DateValueSanitizer(DateFormat paramDateFormat) {
    this.mDateFormat = (DateFormat)Preconditions.checkNotNull(paramDateFormat);
  }
  
  public AutofillValue sanitize(AutofillValue paramAutofillValue) {
    if (paramAutofillValue == null) {
      Log.w("DateValueSanitizer", "sanitize() called with null value");
      return null;
    } 
    if (!paramAutofillValue.isDate()) {
      if (Helper.sDebug) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramAutofillValue);
        stringBuilder.append(" is not a date");
        Log.d("DateValueSanitizer", stringBuilder.toString());
      } 
      return null;
    } 
    try {
      Date date2 = new Date();
      this(paramAutofillValue.getDateValue());
      String str = this.mDateFormat.format(date2);
      if (Helper.sDebug) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Transformed ");
        stringBuilder.append(date2);
        stringBuilder.append(" to ");
        stringBuilder.append(str);
        Log.d("DateValueSanitizer", stringBuilder.toString());
      } 
      Date date1 = this.mDateFormat.parse(str);
      if (Helper.sDebug) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Sanitized to ");
        stringBuilder.append(date1);
        Log.d("DateValueSanitizer", stringBuilder.toString());
      } 
      return AutofillValue.forDate(date1.getTime());
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Could not apply ");
      stringBuilder.append(this.mDateFormat);
      stringBuilder.append(" to ");
      stringBuilder.append(paramAutofillValue);
      stringBuilder.append(": ");
      stringBuilder.append(exception);
      Log.w("DateValueSanitizer", stringBuilder.toString());
      return null;
    } 
  }
  
  public String toString() {
    if (!Helper.sDebug)
      return super.toString(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("DateValueSanitizer: [dateFormat=");
    stringBuilder.append(this.mDateFormat);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeSerializable((Serializable)this.mDateFormat);
  }
  
  public static final Parcelable.Creator<DateValueSanitizer> CREATOR = (Parcelable.Creator<DateValueSanitizer>)new Object();
  
  private static final String TAG = "DateValueSanitizer";
  
  private final DateFormat mDateFormat;
}
