package android.service.autofill;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAutoFillService extends IInterface {
  void onConnectedStateChanged(boolean paramBoolean) throws RemoteException;
  
  void onFillRequest(FillRequest paramFillRequest, IFillCallback paramIFillCallback) throws RemoteException;
  
  void onSaveRequest(SaveRequest paramSaveRequest, ISaveCallback paramISaveCallback) throws RemoteException;
  
  class Default implements IAutoFillService {
    public void onConnectedStateChanged(boolean param1Boolean) throws RemoteException {}
    
    public void onFillRequest(FillRequest param1FillRequest, IFillCallback param1IFillCallback) throws RemoteException {}
    
    public void onSaveRequest(SaveRequest param1SaveRequest, ISaveCallback param1ISaveCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAutoFillService {
    private static final String DESCRIPTOR = "android.service.autofill.IAutoFillService";
    
    static final int TRANSACTION_onConnectedStateChanged = 1;
    
    static final int TRANSACTION_onFillRequest = 2;
    
    static final int TRANSACTION_onSaveRequest = 3;
    
    public Stub() {
      attachInterface(this, "android.service.autofill.IAutoFillService");
    }
    
    public static IAutoFillService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.autofill.IAutoFillService");
      if (iInterface != null && iInterface instanceof IAutoFillService)
        return (IAutoFillService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onSaveRequest";
        } 
        return "onFillRequest";
      } 
      return "onConnectedStateChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IFillCallback iFillCallback;
      boolean bool;
      if (param1Int1 != 1) {
        ISaveCallback iSaveCallback;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.service.autofill.IAutoFillService");
            return true;
          } 
          param1Parcel1.enforceInterface("android.service.autofill.IAutoFillService");
          if (param1Parcel1.readInt() != 0) {
            SaveRequest saveRequest = SaveRequest.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel2 = null;
          } 
          iSaveCallback = ISaveCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
          onSaveRequest((SaveRequest)param1Parcel2, iSaveCallback);
          return true;
        } 
        iSaveCallback.enforceInterface("android.service.autofill.IAutoFillService");
        if (iSaveCallback.readInt() != 0) {
          FillRequest fillRequest = FillRequest.CREATOR.createFromParcel((Parcel)iSaveCallback);
        } else {
          param1Parcel2 = null;
        } 
        iFillCallback = IFillCallback.Stub.asInterface(iSaveCallback.readStrongBinder());
        onFillRequest((FillRequest)param1Parcel2, iFillCallback);
        return true;
      } 
      iFillCallback.enforceInterface("android.service.autofill.IAutoFillService");
      if (iFillCallback.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      onConnectedStateChanged(bool);
      return true;
    }
    
    private static class Proxy implements IAutoFillService {
      public static IAutoFillService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.autofill.IAutoFillService";
      }
      
      public void onConnectedStateChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.service.autofill.IAutoFillService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool1 && IAutoFillService.Stub.getDefaultImpl() != null) {
            IAutoFillService.Stub.getDefaultImpl().onConnectedStateChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onFillRequest(FillRequest param2FillRequest, IFillCallback param2IFillCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.autofill.IAutoFillService");
          if (param2FillRequest != null) {
            parcel.writeInt(1);
            param2FillRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IFillCallback != null) {
            iBinder = param2IFillCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IAutoFillService.Stub.getDefaultImpl() != null) {
            IAutoFillService.Stub.getDefaultImpl().onFillRequest(param2FillRequest, param2IFillCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSaveRequest(SaveRequest param2SaveRequest, ISaveCallback param2ISaveCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.autofill.IAutoFillService");
          if (param2SaveRequest != null) {
            parcel.writeInt(1);
            param2SaveRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ISaveCallback != null) {
            iBinder = param2ISaveCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IAutoFillService.Stub.getDefaultImpl() != null) {
            IAutoFillService.Stub.getDefaultImpl().onSaveRequest(param2SaveRequest, param2ISaveCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAutoFillService param1IAutoFillService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAutoFillService != null) {
          Proxy.sDefaultImpl = param1IAutoFillService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAutoFillService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
