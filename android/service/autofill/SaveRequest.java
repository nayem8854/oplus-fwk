package android.service.autofill;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;
import java.util.List;

public final class SaveRequest implements Parcelable {
  public SaveRequest(ArrayList<FillContext> paramArrayList, Bundle paramBundle, ArrayList<String> paramArrayList1) {
    this.mFillContexts = (ArrayList<FillContext>)Preconditions.checkNotNull(paramArrayList, "fillContexts");
    this.mClientState = paramBundle;
    this.mDatasetIds = paramArrayList1;
  }
  
  private SaveRequest(Parcel paramParcel) {
    this(arrayList1, bundle, arrayList);
  }
  
  public List<FillContext> getFillContexts() {
    return this.mFillContexts;
  }
  
  public Bundle getClientState() {
    return this.mClientState;
  }
  
  public List<String> getDatasetIds() {
    return this.mDatasetIds;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeTypedList(this.mFillContexts, paramInt);
    paramParcel.writeBundle(this.mClientState);
    paramParcel.writeStringList(this.mDatasetIds);
  }
  
  public static final Parcelable.Creator<SaveRequest> CREATOR = new Parcelable.Creator<SaveRequest>() {
      public SaveRequest createFromParcel(Parcel param1Parcel) {
        return new SaveRequest(param1Parcel);
      }
      
      public SaveRequest[] newArray(int param1Int) {
        return new SaveRequest[param1Int];
      }
    };
  
  private final Bundle mClientState;
  
  private final ArrayList<String> mDatasetIds;
  
  private final ArrayList<FillContext> mFillContexts;
}
