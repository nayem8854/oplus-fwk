package android.service.autofill.augmented;

import android.annotation.SystemApi;
import android.os.RemoteException;
import android.util.Log;
import android.util.Pair;
import android.view.autofill.AutofillId;
import android.view.autofill.AutofillValue;
import com.android.internal.util.Preconditions;
import java.util.List;

@SystemApi
public final class FillController {
  private static final String TAG = FillController.class.getSimpleName();
  
  private final AugmentedAutofillService.AutofillProxy mProxy;
  
  FillController(AugmentedAutofillService.AutofillProxy paramAutofillProxy) {
    this.mProxy = paramAutofillProxy;
  }
  
  public void autofill(List<Pair<AutofillId, AutofillValue>> paramList) {
    Preconditions.checkNotNull(paramList);
    if (AugmentedAutofillService.sDebug) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("autofill() with ");
      stringBuilder.append(paramList.size());
      stringBuilder.append(" values");
      Log.d(str, stringBuilder.toString());
    } 
    try {
      this.mProxy.autofill(paramList);
    } catch (RemoteException remoteException) {
      remoteException.rethrowAsRuntimeException();
    } 
    FillWindow fillWindow = this.mProxy.getFillWindow();
    if (fillWindow != null)
      fillWindow.destroy(); 
  }
}
