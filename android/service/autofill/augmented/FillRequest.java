package android.service.autofill.augmented;

import android.annotation.NonNull;
import android.annotation.SystemApi;
import android.content.ComponentName;
import android.view.autofill.AutofillId;
import android.view.autofill.AutofillValue;
import android.view.inputmethod.InlineSuggestionsRequest;
import com.android.internal.util.AnnotationValidations;

@SystemApi
public final class FillRequest {
  private final InlineSuggestionsRequest mInlineSuggestionsRequest;
  
  private final AugmentedAutofillService.AutofillProxy mProxy;
  
  public int getTaskId() {
    return this.mProxy.mTaskId;
  }
  
  public ComponentName getActivityComponent() {
    return this.mProxy.mComponentName;
  }
  
  public AutofillId getFocusedId() {
    return this.mProxy.getFocusedId();
  }
  
  public AutofillValue getFocusedValue() {
    return this.mProxy.getFocusedValue();
  }
  
  public PresentationParams getPresentationParams() {
    return this.mProxy.getSmartSuggestionParams();
  }
  
  String proxyToString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("FillRequest[act=");
    stringBuilder.append(getActivityComponent().flattenToShortString());
    stringBuilder.append(", id=");
    AugmentedAutofillService.AutofillProxy autofillProxy = this.mProxy;
    stringBuilder.append(autofillProxy.getFocusedId());
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public FillRequest(AugmentedAutofillService.AutofillProxy paramAutofillProxy, InlineSuggestionsRequest paramInlineSuggestionsRequest) {
    this.mProxy = paramAutofillProxy;
    AnnotationValidations.validate(NonNull.class, null, paramAutofillProxy);
    this.mInlineSuggestionsRequest = paramInlineSuggestionsRequest;
  }
  
  public InlineSuggestionsRequest getInlineSuggestionsRequest() {
    return this.mInlineSuggestionsRequest;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("FillRequest { proxy = ");
    stringBuilder.append(proxyToString());
    stringBuilder.append(", inlineSuggestionsRequest = ");
    stringBuilder.append(this.mInlineSuggestionsRequest);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  @Deprecated
  private void __metadata() {}
}
