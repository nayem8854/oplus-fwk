package android.service.autofill.augmented;

import android.content.ComponentName;
import android.metrics.LogMaker;
import com.android.internal.logging.MetricsLogger;

public final class Helper {
  private static final MetricsLogger sMetricsLogger = new MetricsLogger();
  
  public static void logResponse(int paramInt1, String paramString, ComponentName paramComponentName, int paramInt2, long paramLong) {
    LogMaker logMaker3 = new LogMaker(1724);
    logMaker3 = logMaker3.setType(paramInt1);
    LogMaker logMaker2 = logMaker3.setComponentName(paramComponentName);
    logMaker2 = logMaker2.addTaggedData(1456, Integer.valueOf(paramInt2));
    LogMaker logMaker1 = logMaker2.addTaggedData(908, paramString);
    logMaker1 = logMaker1.addTaggedData(1145, Long.valueOf(paramLong));
    sMetricsLogger.write(logMaker1);
  }
  
  private Helper() {
    throw new UnsupportedOperationException("contains only static methods");
  }
}
