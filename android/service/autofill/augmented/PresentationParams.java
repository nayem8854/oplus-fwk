package android.service.autofill.augmented;

import android.annotation.SystemApi;
import android.graphics.Rect;
import java.io.PrintWriter;

@SystemApi
public abstract class PresentationParams {
  abstract void dump(String paramString, PrintWriter paramPrintWriter);
  
  public Area getSuggestionArea() {
    return null;
  }
  
  @SystemApi
  public static abstract class Area {
    private final Rect mBounds;
    
    public final AugmentedAutofillService.AutofillProxy proxy;
    
    private Area(AugmentedAutofillService.AutofillProxy param1AutofillProxy, Rect param1Rect) {
      this.proxy = param1AutofillProxy;
      this.mBounds = param1Rect;
    }
    
    public Rect getBounds() {
      return this.mBounds;
    }
    
    public String toString() {
      return this.mBounds.toString();
    }
  }
  
  class SystemPopupPresentationParams extends PresentationParams {
    private final PresentationParams.Area mSuggestionArea;
    
    public SystemPopupPresentationParams(PresentationParams this$0, Rect param1Rect) {
      this.mSuggestionArea = new PresentationParams.Area((AugmentedAutofillService.AutofillProxy)this$0, param1Rect) {
          final PresentationParams.SystemPopupPresentationParams this$0;
        };
    }
    
    public PresentationParams.Area getSuggestionArea() {
      return this.mSuggestionArea;
    }
    
    void dump(String param1String, PrintWriter param1PrintWriter) {
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("area: ");
      param1PrintWriter.println(this.mSuggestionArea);
    }
  }
}
