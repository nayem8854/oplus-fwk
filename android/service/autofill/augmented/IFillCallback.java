package android.service.autofill.augmented;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ICancellationSignal;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.service.autofill.Dataset;
import java.util.ArrayList;
import java.util.List;

public interface IFillCallback extends IInterface {
  void cancel() throws RemoteException;
  
  boolean isCompleted() throws RemoteException;
  
  void onCancellable(ICancellationSignal paramICancellationSignal) throws RemoteException;
  
  void onSuccess(List<Dataset> paramList, Bundle paramBundle, boolean paramBoolean) throws RemoteException;
  
  class Default implements IFillCallback {
    public void onCancellable(ICancellationSignal param1ICancellationSignal) throws RemoteException {}
    
    public void onSuccess(List<Dataset> param1List, Bundle param1Bundle, boolean param1Boolean) throws RemoteException {}
    
    public boolean isCompleted() throws RemoteException {
      return false;
    }
    
    public void cancel() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IFillCallback {
    private static final String DESCRIPTOR = "android.service.autofill.augmented.IFillCallback";
    
    static final int TRANSACTION_cancel = 4;
    
    static final int TRANSACTION_isCompleted = 3;
    
    static final int TRANSACTION_onCancellable = 1;
    
    static final int TRANSACTION_onSuccess = 2;
    
    public Stub() {
      attachInterface(this, "android.service.autofill.augmented.IFillCallback");
    }
    
    public static IFillCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.autofill.augmented.IFillCallback");
      if (iInterface != null && iInterface instanceof IFillCallback)
        return (IFillCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "cancel";
          } 
          return "isCompleted";
        } 
        return "onSuccess";
      } 
      return "onCancellable";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        Bundle bundle;
        boolean bool;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.service.autofill.augmented.IFillCallback");
              return true;
            } 
            param1Parcel1.enforceInterface("android.service.autofill.augmented.IFillCallback");
            cancel();
            param1Parcel2.writeNoException();
            return true;
          } 
          param1Parcel1.enforceInterface("android.service.autofill.augmented.IFillCallback");
          boolean bool1 = isCompleted();
          param1Parcel2.writeNoException();
          param1Parcel2.writeInt(bool1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.service.autofill.augmented.IFillCallback");
        ArrayList<Dataset> arrayList = param1Parcel1.createTypedArrayList(Dataset.CREATOR);
        if (param1Parcel1.readInt() != 0) {
          bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
        } else {
          bundle = null;
        } 
        if (param1Parcel1.readInt() != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        onSuccess(arrayList, bundle, bool);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.autofill.augmented.IFillCallback");
      ICancellationSignal iCancellationSignal = ICancellationSignal.Stub.asInterface(param1Parcel1.readStrongBinder());
      onCancellable(iCancellationSignal);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IFillCallback {
      public static IFillCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.autofill.augmented.IFillCallback";
      }
      
      public void onCancellable(ICancellationSignal param2ICancellationSignal) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.service.autofill.augmented.IFillCallback");
          if (param2ICancellationSignal != null) {
            iBinder = param2ICancellationSignal.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IFillCallback.Stub.getDefaultImpl() != null) {
            IFillCallback.Stub.getDefaultImpl().onCancellable(param2ICancellationSignal);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onSuccess(List<Dataset> param2List, Bundle param2Bundle, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.autofill.augmented.IFillCallback");
          parcel1.writeTypedList(param2List);
          boolean bool = true;
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool1 && IFillCallback.Stub.getDefaultImpl() != null) {
            IFillCallback.Stub.getDefaultImpl().onSuccess(param2List, param2Bundle, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isCompleted() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.autofill.augmented.IFillCallback");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IFillCallback.Stub.getDefaultImpl() != null) {
            bool1 = IFillCallback.Stub.getDefaultImpl().isCompleted();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancel() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.service.autofill.augmented.IFillCallback");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IFillCallback.Stub.getDefaultImpl() != null) {
            IFillCallback.Stub.getDefaultImpl().cancel();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IFillCallback param1IFillCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IFillCallback != null) {
          Proxy.sDefaultImpl = param1IFillCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IFillCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
