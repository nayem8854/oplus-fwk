package android.service.autofill.augmented;

import android.annotation.SystemApi;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.autofill.IAutofillWindowPresenter;
import com.android.internal.util.Preconditions;
import com.android.internal.util.function.pooled.PooledLambda;
import dalvik.system.CloseGuard;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

@SystemApi
public final class FillWindow implements AutoCloseable {
  private static final String TAG = FillWindow.class.getSimpleName();
  
  private final Object mLock = new Object();
  
  private final CloseGuard mCloseGuard = CloseGuard.get();
  
  private final Handler mUiThreadHandler = new Handler(Looper.getMainLooper());
  
  private Rect mBounds;
  
  private boolean mDestroyed;
  
  private View mFillView;
  
  private AugmentedAutofillService.AutofillProxy mProxy;
  
  private boolean mShowing;
  
  private boolean mUpdateCalled;
  
  private WindowManager mWm;
  
  public boolean update(PresentationParams.Area paramArea, View paramView, long paramLong) {
    if (AugmentedAutofillService.sDebug) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Updating ");
      stringBuilder.append(paramArea);
      stringBuilder.append(" + with ");
      stringBuilder.append(paramView);
      Log.d(str, stringBuilder.toString());
    } 
    Preconditions.checkNotNull(paramArea);
    Preconditions.checkNotNull(paramArea.proxy);
    Preconditions.checkNotNull(paramView);
    PresentationParams.SystemPopupPresentationParams systemPopupPresentationParams = paramArea.proxy.getSmartSuggestionParams();
    if (systemPopupPresentationParams == null) {
      Log.w(TAG, "No SmartSuggestionParams");
      return false;
    } 
    Rect rect = paramArea.getBounds();
    if (rect == null) {
      Log.wtf(TAG, "No Rect on SmartSuggestionParams");
      return false;
    } 
    synchronized (this.mLock) {
      checkNotDestroyedLocked();
      this.mProxy = paramArea.proxy;
      this.mWm = (WindowManager)paramView.getContext().getSystemService(WindowManager.class);
      this.mFillView = paramView;
      _$$Lambda$FillWindow$j5yljDJ4VfFL37B_iWRonGGOKN4 _$$Lambda$FillWindow$j5yljDJ4VfFL37B_iWRonGGOKN4 = new _$$Lambda$FillWindow$j5yljDJ4VfFL37B_iWRonGGOKN4();
      this(this);
      paramView.setOnTouchListener(_$$Lambda$FillWindow$j5yljDJ4VfFL37B_iWRonGGOKN4);
      this.mShowing = false;
      Rect rect1 = new Rect();
      this(paramArea.getBounds());
      this.mBounds = rect1;
      if (AugmentedAutofillService.sDebug) {
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Created FillWindow: params= ");
        stringBuilder.append(systemPopupPresentationParams);
        stringBuilder.append(" view=");
        stringBuilder.append(paramView);
        Log.d(str, stringBuilder.toString());
      } 
      this.mUpdateCalled = true;
      this.mDestroyed = false;
      this.mProxy.setFillWindow(this);
      return true;
    } 
  }
  
  void show() {
    if (AugmentedAutofillService.sDebug)
      Log.d(TAG, "show()"); 
    synchronized (this.mLock) {
      checkNotDestroyedLocked();
      if (this.mWm != null && this.mFillView != null) {
        AugmentedAutofillService.AutofillProxy autofillProxy = this.mProxy;
        if (autofillProxy != null) {
          try {
            autofillProxy = this.mProxy;
            int i = this.mBounds.right, j = this.mBounds.left, k = this.mBounds.bottom, m = this.mBounds.top;
            FillWindowPresenter fillWindowPresenter = new FillWindowPresenter();
            this(this);
            autofillProxy.requestShowFillUi(i - j, k - m, null, (IAutofillWindowPresenter)fillWindowPresenter);
          } catch (RemoteException remoteException) {
            Log.w(TAG, "Error requesting to show fill window", (Throwable)remoteException);
          } 
          this.mProxy.logEvent(2);
        } 
        return;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("update() not called yet, or already destroyed()");
      throw illegalStateException;
    } 
  }
  
  private void hide() {
    if (AugmentedAutofillService.sDebug)
      Log.d(TAG, "hide()"); 
    synchronized (this.mLock) {
      checkNotDestroyedLocked();
      if (this.mWm != null && this.mFillView != null) {
        if (this.mProxy != null) {
          boolean bool = this.mShowing;
          if (bool)
            try {
              this.mProxy.requestHideFillUi();
            } catch (RemoteException remoteException) {
              Log.w(TAG, "Error requesting to hide fill window", (Throwable)remoteException);
            }  
        } 
        return;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("update() not called yet, or already destroyed()");
      throw illegalStateException;
    } 
  }
  
  private void handleShow(WindowManager.LayoutParams paramLayoutParams) {
    if (AugmentedAutofillService.sDebug)
      Log.d(TAG, "handleShow()"); 
    synchronized (this.mLock) {
      if (this.mWm != null) {
        View view = this.mFillView;
        if (view != null)
          try {
            paramLayoutParams.flags |= 0x40000;
            if (!this.mShowing) {
              this.mWm.addView(this.mFillView, (ViewGroup.LayoutParams)paramLayoutParams);
              this.mShowing = true;
            } else {
              this.mWm.updateViewLayout(this.mFillView, (ViewGroup.LayoutParams)paramLayoutParams);
            } 
          } catch (android.view.WindowManager.BadTokenException badTokenException) {
            if (AugmentedAutofillService.sDebug) {
              String str = TAG;
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Filed with token ");
              stringBuilder.append(paramLayoutParams.token);
              stringBuilder.append(" gone.");
              Log.d(str, stringBuilder.toString());
            } 
          } catch (IllegalStateException illegalStateException) {
            if (AugmentedAutofillService.sDebug)
              Log.d(TAG, "Exception showing window."); 
          }  
      } 
      return;
    } 
  }
  
  private void handleHide() {
    if (AugmentedAutofillService.sDebug)
      Log.d(TAG, "handleHide()"); 
    synchronized (this.mLock) {
      if (this.mWm != null && this.mFillView != null) {
        boolean bool = this.mShowing;
        if (bool)
          try {
            this.mWm.removeView(this.mFillView);
            this.mShowing = false;
          } catch (IllegalStateException illegalStateException) {
            if (AugmentedAutofillService.sDebug)
              Log.d(TAG, "Exception hiding window."); 
          }  
      } 
      return;
    } 
  }
  
  public void destroy() {
    if (AugmentedAutofillService.sDebug) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("destroy(): mDestroyed=");
      stringBuilder.append(this.mDestroyed);
      stringBuilder.append(" mShowing=");
      stringBuilder.append(this.mShowing);
      stringBuilder.append(" mFillView=");
      stringBuilder.append(this.mFillView);
      Log.d(str, stringBuilder.toString());
    } 
    synchronized (this.mLock) {
      if (this.mDestroyed)
        return; 
      if (this.mUpdateCalled) {
        this.mFillView.setOnClickListener(null);
        hide();
        this.mProxy.logEvent(3);
      } 
      this.mDestroyed = true;
      this.mCloseGuard.close();
      return;
    } 
  }
  
  protected void finalize() throws Throwable {
    try {
      this.mCloseGuard.warnIfOpen();
      destroy();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  private void checkNotDestroyedLocked() {
    if (!this.mDestroyed)
      return; 
    throw new IllegalStateException("already destroyed()");
  }
  
  public void dump(String paramString, PrintWriter paramPrintWriter) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_2
    //   3: aload_1
    //   4: invokevirtual print : (Ljava/lang/String;)V
    //   7: aload_2
    //   8: ldc 'destroyed: '
    //   10: invokevirtual print : (Ljava/lang/String;)V
    //   13: aload_2
    //   14: aload_0
    //   15: getfield mDestroyed : Z
    //   18: invokevirtual println : (Z)V
    //   21: aload_2
    //   22: aload_1
    //   23: invokevirtual print : (Ljava/lang/String;)V
    //   26: aload_2
    //   27: ldc 'updateCalled: '
    //   29: invokevirtual print : (Ljava/lang/String;)V
    //   32: aload_2
    //   33: aload_0
    //   34: getfield mUpdateCalled : Z
    //   37: invokevirtual println : (Z)V
    //   40: aload_0
    //   41: getfield mFillView : Landroid/view/View;
    //   44: ifnull -> 136
    //   47: aload_2
    //   48: aload_1
    //   49: invokevirtual print : (Ljava/lang/String;)V
    //   52: aload_2
    //   53: ldc 'fill window: '
    //   55: invokevirtual print : (Ljava/lang/String;)V
    //   58: aload_0
    //   59: getfield mShowing : Z
    //   62: ifeq -> 71
    //   65: ldc 'shown'
    //   67: astore_3
    //   68: goto -> 74
    //   71: ldc 'hidden'
    //   73: astore_3
    //   74: aload_2
    //   75: aload_3
    //   76: invokevirtual println : (Ljava/lang/String;)V
    //   79: aload_2
    //   80: aload_1
    //   81: invokevirtual print : (Ljava/lang/String;)V
    //   84: aload_2
    //   85: ldc 'fill view: '
    //   87: invokevirtual print : (Ljava/lang/String;)V
    //   90: aload_2
    //   91: aload_0
    //   92: getfield mFillView : Landroid/view/View;
    //   95: invokevirtual println : (Ljava/lang/Object;)V
    //   98: aload_2
    //   99: aload_1
    //   100: invokevirtual print : (Ljava/lang/String;)V
    //   103: aload_2
    //   104: ldc 'mBounds: '
    //   106: invokevirtual print : (Ljava/lang/String;)V
    //   109: aload_2
    //   110: aload_0
    //   111: getfield mBounds : Landroid/graphics/Rect;
    //   114: invokevirtual println : (Ljava/lang/Object;)V
    //   117: aload_2
    //   118: aload_1
    //   119: invokevirtual print : (Ljava/lang/String;)V
    //   122: aload_2
    //   123: ldc 'mWm: '
    //   125: invokevirtual print : (Ljava/lang/String;)V
    //   128: aload_2
    //   129: aload_0
    //   130: getfield mWm : Landroid/view/WindowManager;
    //   133: invokevirtual println : (Ljava/lang/Object;)V
    //   136: aload_0
    //   137: monitorexit
    //   138: return
    //   139: astore_1
    //   140: aload_0
    //   141: monitorexit
    //   142: aload_1
    //   143: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #283	-> 0
    //   #284	-> 2
    //   #285	-> 21
    //   #286	-> 40
    //   #287	-> 47
    //   #288	-> 58
    //   #289	-> 79
    //   #290	-> 90
    //   #291	-> 98
    //   #292	-> 109
    //   #293	-> 117
    //   #294	-> 128
    //   #296	-> 136
    //   #297	-> 138
    //   #296	-> 139
    // Exception table:
    //   from	to	target	type
    //   2	21	139	finally
    //   21	40	139	finally
    //   40	47	139	finally
    //   47	58	139	finally
    //   58	65	139	finally
    //   74	79	139	finally
    //   79	90	139	finally
    //   90	98	139	finally
    //   98	109	139	finally
    //   109	117	139	finally
    //   117	128	139	finally
    //   128	136	139	finally
    //   136	138	139	finally
    //   140	142	139	finally
  }
  
  public void close() {
    destroy();
  }
  
  class FillWindowPresenter extends IAutofillWindowPresenter.Stub {
    private final WeakReference<FillWindow> mFillWindowReference;
    
    FillWindowPresenter(FillWindow this$0) {
      this.mFillWindowReference = new WeakReference<>(this$0);
    }
    
    public void show(WindowManager.LayoutParams param1LayoutParams, Rect param1Rect, boolean param1Boolean, int param1Int) {
      if (AugmentedAutofillService.sDebug)
        Log.d(FillWindow.TAG, "FillWindowPresenter.show()"); 
      FillWindow fillWindow = this.mFillWindowReference.get();
      if (fillWindow != null) {
        Handler handler = fillWindow.mUiThreadHandler;
        -$.Lambda.FillWindow.FillWindowPresenter.hdkNZGuYdsvcArvQ2SoMspO1EO8 hdkNZGuYdsvcArvQ2SoMspO1EO8 = _$$Lambda$FillWindow$FillWindowPresenter$hdkNZGuYdsvcArvQ2SoMspO1EO8.INSTANCE;
        Message message = PooledLambda.obtainMessage((BiConsumer)hdkNZGuYdsvcArvQ2SoMspO1EO8, fillWindow, param1LayoutParams);
        handler.sendMessage(message);
      } 
    }
    
    public void hide(Rect param1Rect) {
      if (AugmentedAutofillService.sDebug)
        Log.d(FillWindow.TAG, "FillWindowPresenter.hide()"); 
      FillWindow fillWindow = this.mFillWindowReference.get();
      if (fillWindow != null) {
        Handler handler = fillWindow.mUiThreadHandler;
        -$.Lambda.FillWindow.FillWindowPresenter.EnBAJTZRgK05SBPnOQ9Edaq3VXs enBAJTZRgK05SBPnOQ9Edaq3VXs = _$$Lambda$FillWindow$FillWindowPresenter$EnBAJTZRgK05SBPnOQ9Edaq3VXs.INSTANCE;
        Message message = PooledLambda.obtainMessage((Consumer)enBAJTZRgK05SBPnOQ9Edaq3VXs, fillWindow);
        handler.sendMessage(message);
      } 
    }
  }
}
