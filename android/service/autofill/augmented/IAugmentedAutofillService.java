package android.service.autofill.augmented;

import android.content.ComponentName;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.autofill.AutofillId;
import android.view.autofill.AutofillValue;
import android.view.inputmethod.InlineSuggestionsRequest;

public interface IAugmentedAutofillService extends IInterface {
  void onConnected(boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void onDestroyAllFillWindowsRequest() throws RemoteException;
  
  void onDisconnected() throws RemoteException;
  
  void onFillRequest(int paramInt1, IBinder paramIBinder, int paramInt2, ComponentName paramComponentName, AutofillId paramAutofillId, AutofillValue paramAutofillValue, long paramLong, InlineSuggestionsRequest paramInlineSuggestionsRequest, IFillCallback paramIFillCallback) throws RemoteException;
  
  class Default implements IAugmentedAutofillService {
    public void onConnected(boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public void onDisconnected() throws RemoteException {}
    
    public void onFillRequest(int param1Int1, IBinder param1IBinder, int param1Int2, ComponentName param1ComponentName, AutofillId param1AutofillId, AutofillValue param1AutofillValue, long param1Long, InlineSuggestionsRequest param1InlineSuggestionsRequest, IFillCallback param1IFillCallback) throws RemoteException {}
    
    public void onDestroyAllFillWindowsRequest() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAugmentedAutofillService {
    private static final String DESCRIPTOR = "android.service.autofill.augmented.IAugmentedAutofillService";
    
    static final int TRANSACTION_onConnected = 1;
    
    static final int TRANSACTION_onDestroyAllFillWindowsRequest = 4;
    
    static final int TRANSACTION_onDisconnected = 2;
    
    static final int TRANSACTION_onFillRequest = 3;
    
    public Stub() {
      attachInterface(this, "android.service.autofill.augmented.IAugmentedAutofillService");
    }
    
    public static IAugmentedAutofillService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.autofill.augmented.IAugmentedAutofillService");
      if (iInterface != null && iInterface instanceof IAugmentedAutofillService)
        return (IAugmentedAutofillService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onDestroyAllFillWindowsRequest";
          } 
          return "onFillRequest";
        } 
        return "onDisconnected";
      } 
      return "onConnected";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IFillCallback iFillCallback;
      boolean bool2;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          AutofillId autofillId;
          AutofillValue autofillValue;
          InlineSuggestionsRequest inlineSuggestionsRequest;
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.service.autofill.augmented.IAugmentedAutofillService");
              return true;
            } 
            param1Parcel1.enforceInterface("android.service.autofill.augmented.IAugmentedAutofillService");
            onDestroyAllFillWindowsRequest();
            return true;
          } 
          param1Parcel1.enforceInterface("android.service.autofill.augmented.IAugmentedAutofillService");
          param1Int2 = param1Parcel1.readInt();
          IBinder iBinder = param1Parcel1.readStrongBinder();
          param1Int1 = param1Parcel1.readInt();
          if (param1Parcel1.readInt() != 0) {
            ComponentName componentName = ComponentName.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel2 = null;
          } 
          if (param1Parcel1.readInt() != 0) {
            autofillId = AutofillId.CREATOR.createFromParcel(param1Parcel1);
          } else {
            autofillId = null;
          } 
          if (param1Parcel1.readInt() != 0) {
            autofillValue = AutofillValue.CREATOR.createFromParcel(param1Parcel1);
          } else {
            autofillValue = null;
          } 
          long l = param1Parcel1.readLong();
          if (param1Parcel1.readInt() != 0) {
            inlineSuggestionsRequest = InlineSuggestionsRequest.CREATOR.createFromParcel(param1Parcel1);
          } else {
            inlineSuggestionsRequest = null;
          } 
          iFillCallback = IFillCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
          onFillRequest(param1Int2, iBinder, param1Int1, (ComponentName)param1Parcel2, autofillId, autofillValue, l, inlineSuggestionsRequest, iFillCallback);
          return true;
        } 
        iFillCallback.enforceInterface("android.service.autofill.augmented.IAugmentedAutofillService");
        onDisconnected();
        return true;
      } 
      iFillCallback.enforceInterface("android.service.autofill.augmented.IAugmentedAutofillService");
      param1Int1 = iFillCallback.readInt();
      boolean bool1 = false;
      if (param1Int1 != 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (iFillCallback.readInt() != 0)
        bool1 = true; 
      onConnected(bool2, bool1);
      return true;
    }
    
    private static class Proxy implements IAugmentedAutofillService {
      public static IAugmentedAutofillService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.autofill.augmented.IAugmentedAutofillService";
      }
      
      public void onConnected(boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.autofill.augmented.IAugmentedAutofillService");
          boolean bool1 = false;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel.writeInt(bool2);
          boolean bool2 = bool1;
          if (param2Boolean2)
            bool2 = true; 
          parcel.writeInt(bool2);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IAugmentedAutofillService.Stub.getDefaultImpl() != null) {
            IAugmentedAutofillService.Stub.getDefaultImpl().onConnected(param2Boolean1, param2Boolean2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDisconnected() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.autofill.augmented.IAugmentedAutofillService");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IAugmentedAutofillService.Stub.getDefaultImpl() != null) {
            IAugmentedAutofillService.Stub.getDefaultImpl().onDisconnected();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onFillRequest(int param2Int1, IBinder param2IBinder, int param2Int2, ComponentName param2ComponentName, AutofillId param2AutofillId, AutofillValue param2AutofillValue, long param2Long, InlineSuggestionsRequest param2InlineSuggestionsRequest, IFillCallback param2IFillCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.autofill.augmented.IAugmentedAutofillService");
          parcel.writeInt(param2Int1);
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeInt(param2Int2);
          if (param2ComponentName != null) {
            try {
              parcel.writeInt(1);
              param2ComponentName.writeToParcel(parcel, 0);
            } finally {}
          } else {
            parcel.writeInt(0);
          } 
          if (param2AutofillId != null) {
            parcel.writeInt(1);
            param2AutofillId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2AutofillValue != null) {
            parcel.writeInt(1);
            param2AutofillValue.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeLong(param2Long);
          if (param2InlineSuggestionsRequest != null) {
            parcel.writeInt(1);
            param2InlineSuggestionsRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IFillCallback != null) {
            iBinder = param2IFillCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IAugmentedAutofillService.Stub.getDefaultImpl() != null) {
            IAugmentedAutofillService iAugmentedAutofillService = IAugmentedAutofillService.Stub.getDefaultImpl();
            try {
              iAugmentedAutofillService.onFillRequest(param2Int1, param2IBinder, param2Int2, param2ComponentName, param2AutofillId, param2AutofillValue, param2Long, param2InlineSuggestionsRequest, param2IFillCallback);
              parcel.recycle();
              return;
            } finally {}
          } else {
            parcel.recycle();
            return;
          } 
        } finally {}
        parcel.recycle();
        throw param2IBinder;
      }
      
      public void onDestroyAllFillWindowsRequest() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.autofill.augmented.IAugmentedAutofillService");
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IAugmentedAutofillService.Stub.getDefaultImpl() != null) {
            IAugmentedAutofillService.Stub.getDefaultImpl().onDestroyAllFillWindowsRequest();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAugmentedAutofillService param1IAugmentedAutofillService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAugmentedAutofillService != null) {
          Proxy.sDefaultImpl = param1IAugmentedAutofillService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAugmentedAutofillService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
