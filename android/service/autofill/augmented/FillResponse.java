package android.service.autofill.augmented;

import android.annotation.SystemApi;
import android.os.Bundle;
import android.service.autofill.Dataset;
import java.util.ArrayList;
import java.util.List;

@SystemApi
public final class FillResponse {
  private Bundle mClientState;
  
  private FillWindow mFillWindow;
  
  private List<Dataset> mInlineSuggestions;
  
  private static FillWindow defaultFillWindow() {
    return null;
  }
  
  private static List<Dataset> defaultInlineSuggestions() {
    return null;
  }
  
  private static Bundle defaultClientState() {
    return null;
  }
  
  static abstract class BaseBuilder {
    abstract FillResponse.Builder addInlineSuggestion(Dataset param1Dataset);
  }
  
  FillResponse(FillWindow paramFillWindow, List<Dataset> paramList, Bundle paramBundle) {
    this.mFillWindow = paramFillWindow;
    this.mInlineSuggestions = paramList;
    this.mClientState = paramBundle;
  }
  
  public FillWindow getFillWindow() {
    return this.mFillWindow;
  }
  
  public List<Dataset> getInlineSuggestions() {
    return this.mInlineSuggestions;
  }
  
  public Bundle getClientState() {
    return this.mClientState;
  }
  
  class Builder extends BaseBuilder {
    private long mBuilderFieldsSet = 0L;
    
    private Bundle mClientState;
    
    private FillWindow mFillWindow;
    
    private List<Dataset> mInlineSuggestions;
    
    public Builder setFillWindow(FillWindow param1FillWindow) {
      checkNotUsed();
      this.mBuilderFieldsSet |= 0x1L;
      this.mFillWindow = param1FillWindow;
      return this;
    }
    
    public Builder setInlineSuggestions(List<Dataset> param1List) {
      checkNotUsed();
      this.mBuilderFieldsSet |= 0x2L;
      this.mInlineSuggestions = param1List;
      return this;
    }
    
    Builder addInlineSuggestion(Dataset param1Dataset) {
      if (this.mInlineSuggestions == null)
        setInlineSuggestions(new ArrayList<>()); 
      this.mInlineSuggestions.add(param1Dataset);
      return this;
    }
    
    public Builder setClientState(Bundle param1Bundle) {
      checkNotUsed();
      this.mBuilderFieldsSet |= 0x4L;
      this.mClientState = param1Bundle;
      return this;
    }
    
    public FillResponse build() {
      checkNotUsed();
      long l = this.mBuilderFieldsSet | 0x8L;
      if ((l & 0x1L) == 0L)
        this.mFillWindow = FillResponse.defaultFillWindow(); 
      if ((this.mBuilderFieldsSet & 0x2L) == 0L)
        this.mInlineSuggestions = FillResponse.defaultInlineSuggestions(); 
      if ((this.mBuilderFieldsSet & 0x4L) == 0L)
        this.mClientState = FillResponse.defaultClientState(); 
      return new FillResponse(this.mFillWindow, this.mInlineSuggestions, this.mClientState);
    }
    
    private void checkNotUsed() {
      if ((this.mBuilderFieldsSet & 0x8L) == 0L)
        return; 
      throw new IllegalStateException("This Builder should not be reused. Use a new Builder instance instead");
    }
  }
  
  @Deprecated
  private void __metadata() {}
}
