package android.service.autofill.augmented;

import android.annotation.SystemApi;
import android.os.Bundle;
import android.service.autofill.Dataset;
import android.util.Log;
import java.util.List;

@SystemApi
public final class FillCallback {
  private static final String TAG = FillCallback.class.getSimpleName();
  
  private final AugmentedAutofillService.AutofillProxy mProxy;
  
  FillCallback(AugmentedAutofillService.AutofillProxy paramAutofillProxy) {
    this.mProxy = paramAutofillProxy;
  }
  
  public void onSuccess(FillResponse paramFillResponse) {
    if (AugmentedAutofillService.sDebug) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onSuccess(): ");
      stringBuilder.append(paramFillResponse);
      Log.d(str, stringBuilder.toString());
    } 
    if (paramFillResponse == null) {
      this.mProxy.logEvent(1);
      this.mProxy.reportResult(null, null, false);
      return;
    } 
    List<Dataset> list = paramFillResponse.getInlineSuggestions();
    Bundle bundle = paramFillResponse.getClientState();
    FillWindow fillWindow = paramFillResponse.getFillWindow();
    boolean bool = false;
    if (list != null && !list.isEmpty()) {
      this.mProxy.logEvent(4);
    } else if (fillWindow != null) {
      fillWindow.show();
      bool = true;
    } 
    this.mProxy.reportResult(list, bundle, bool);
  }
}
