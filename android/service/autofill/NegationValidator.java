package android.service.autofill;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.autofill.Helper;
import com.android.internal.util.Preconditions;

final class NegationValidator extends InternalValidator {
  NegationValidator(InternalValidator paramInternalValidator) {
    this.mValidator = (InternalValidator)Preconditions.checkNotNull(paramInternalValidator);
  }
  
  public boolean isValid(ValueFinder paramValueFinder) {
    return this.mValidator.isValid(paramValueFinder) ^ true;
  }
  
  public String toString() {
    if (!Helper.sDebug)
      return super.toString(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("NegationValidator: [validator=");
    stringBuilder.append(this.mValidator);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.mValidator, paramInt);
  }
  
  public static final Parcelable.Creator<NegationValidator> CREATOR = (Parcelable.Creator<NegationValidator>)new Object();
  
  private final InternalValidator mValidator;
}
