package android.service.autofill;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.Log;
import android.view.autofill.AutofillId;
import android.view.autofill.Helper;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class FillEventHistory implements Parcelable {
  public int getSessionId() {
    return this.mSessionId;
  }
  
  @Deprecated
  public Bundle getClientState() {
    return this.mClientState;
  }
  
  public List<Event> getEvents() {
    return this.mEvents;
  }
  
  public void addEvent(Event paramEvent) {
    if (this.mEvents == null)
      this.mEvents = new ArrayList<>(1); 
    this.mEvents.add(paramEvent);
  }
  
  public FillEventHistory(int paramInt, Bundle paramBundle) {
    this.mClientState = paramBundle;
    this.mSessionId = paramInt;
  }
  
  public String toString() {
    String str;
    List<Event> list = this.mEvents;
    if (list == null) {
      str = "no events";
    } else {
      str = str.toString();
    } 
    return str;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeBundle(this.mClientState);
    List<Event> list = this.mEvents;
    if (list == null) {
      paramParcel.writeInt(0);
    } else {
      paramParcel.writeInt(list.size());
      int i = this.mEvents.size();
      for (byte b = 0; b < i; b++) {
        Event event = this.mEvents.get(b);
        paramParcel.writeInt(event.mEventType);
        paramParcel.writeString(event.mDatasetId);
        paramParcel.writeBundle(event.mClientState);
        paramParcel.writeStringList(event.mSelectedDatasetIds);
        paramParcel.writeArraySet((ArraySet)event.mIgnoredDatasetIds);
        paramParcel.writeTypedList((List)event.mChangedFieldIds);
        paramParcel.writeStringList(event.mChangedDatasetIds);
        paramParcel.writeTypedList((List)event.mManuallyFilledFieldIds);
        if (event.mManuallyFilledFieldIds != null) {
          int j = event.mManuallyFilledFieldIds.size();
          for (byte b1 = 0; b1 < j; b1++)
            paramParcel.writeStringList(event.mManuallyFilledDatasetIds.get(b1)); 
        } 
        AutofillId[] arrayOfAutofillId = event.mDetectedFieldIds;
        paramParcel.writeParcelableArray(arrayOfAutofillId, paramInt);
        if (arrayOfAutofillId != null) {
          FieldClassification[] arrayOfFieldClassification = event.mDetectedFieldClassifications;
          FieldClassification.writeArrayToParcel(paramParcel, arrayOfFieldClassification);
        } 
      } 
    } 
  }
  
  class Event {
    public static final int TYPE_AUTHENTICATION_SELECTED = 2;
    
    public static final int TYPE_CONTEXT_COMMITTED = 4;
    
    public static final int TYPE_DATASETS_SHOWN = 5;
    
    public static final int TYPE_DATASET_AUTHENTICATION_SELECTED = 1;
    
    public static final int TYPE_DATASET_SELECTED = 0;
    
    public static final int TYPE_SAVE_SHOWN = 3;
    
    private final ArrayList<String> mChangedDatasetIds;
    
    private final ArrayList<AutofillId> mChangedFieldIds;
    
    private final Bundle mClientState;
    
    private final String mDatasetId;
    
    private final FieldClassification[] mDetectedFieldClassifications;
    
    private final AutofillId[] mDetectedFieldIds;
    
    private final int mEventType;
    
    private final ArraySet<String> mIgnoredDatasetIds;
    
    private final ArrayList<ArrayList<String>> mManuallyFilledDatasetIds;
    
    private final ArrayList<AutofillId> mManuallyFilledFieldIds;
    
    private final List<String> mSelectedDatasetIds;
    
    public int getType() {
      return this.mEventType;
    }
    
    public String getDatasetId() {
      return this.mDatasetId;
    }
    
    public Bundle getClientState() {
      return this.mClientState;
    }
    
    public Set<String> getSelectedDatasetIds() {
      ArraySet arraySet;
      if (this.mSelectedDatasetIds == null) {
        Set<?> set = Collections.emptySet();
      } else {
        arraySet = new ArraySet(this.mSelectedDatasetIds);
      } 
      return (Set<String>)arraySet;
    }
    
    public Set<String> getIgnoredDatasetIds() {
      Set<?> set;
      ArraySet<String> arraySet1 = this.mIgnoredDatasetIds, arraySet2 = arraySet1;
      if (arraySet1 == null)
        set = Collections.emptySet(); 
      return (Set)set;
    }
    
    public Map<AutofillId, String> getChangedFields() {
      ArrayList<AutofillId> arrayList = this.mChangedFieldIds;
      if (arrayList == null || this.mChangedDatasetIds == null)
        return Collections.emptyMap(); 
      int i = arrayList.size();
      ArrayMap arrayMap = new ArrayMap(i);
      for (byte b = 0; b < i; b++)
        arrayMap.put(this.mChangedFieldIds.get(b), this.mChangedDatasetIds.get(b)); 
      return (Map<AutofillId, String>)arrayMap;
    }
    
    public Map<AutofillId, FieldClassification> getFieldsClassification() {
      AutofillId[] arrayOfAutofillId = this.mDetectedFieldIds;
      if (arrayOfAutofillId == null)
        return Collections.emptyMap(); 
      int i = arrayOfAutofillId.length;
      ArrayMap arrayMap = new ArrayMap(i);
      for (byte b = 0; b < i; b++) {
        AutofillId autofillId = this.mDetectedFieldIds[b];
        FieldClassification fieldClassification = this.mDetectedFieldClassifications[b];
        if (Helper.sVerbose) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("getFieldsClassification[");
          stringBuilder.append(b);
          stringBuilder.append("]: id=");
          stringBuilder.append(autofillId);
          stringBuilder.append(", fc=");
          stringBuilder.append(fieldClassification);
          Log.v("FillEventHistory", stringBuilder.toString());
        } 
        arrayMap.put(autofillId, fieldClassification);
      } 
      return (Map<AutofillId, FieldClassification>)arrayMap;
    }
    
    public Map<AutofillId, Set<String>> getManuallyEnteredField() {
      ArrayList<AutofillId> arrayList = this.mManuallyFilledFieldIds;
      if (arrayList == null || this.mManuallyFilledDatasetIds == null)
        return Collections.emptyMap(); 
      int i = arrayList.size();
      ArrayMap<AutofillId, ArraySet> arrayMap = new ArrayMap(i);
      for (byte b = 0; b < i; b++) {
        AutofillId autofillId = this.mManuallyFilledFieldIds.get(b);
        arrayList = (ArrayList<AutofillId>)this.mManuallyFilledDatasetIds.get(b);
        arrayMap.put(autofillId, new ArraySet(arrayList));
      } 
      return (Map)arrayMap;
    }
    
    public Event(FillEventHistory this$0, String param1String, Bundle param1Bundle, List<String> param1List, ArraySet<String> param1ArraySet, ArrayList<AutofillId> param1ArrayList1, ArrayList<String> param1ArrayList, ArrayList<AutofillId> param1ArrayList2, ArrayList<ArrayList<String>> param1ArrayList3, AutofillId[] param1ArrayOfAutofillId, FieldClassification[] param1ArrayOfFieldClassification) {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial <init> : ()V
      //   4: iconst_0
      //   5: istore #12
      //   7: aload_0
      //   8: iload_1
      //   9: iconst_0
      //   10: iconst_5
      //   11: ldc 'eventType'
      //   13: invokestatic checkArgumentInRange : (IIILjava/lang/String;)I
      //   16: putfield mEventType : I
      //   19: aload_0
      //   20: aload_2
      //   21: putfield mDatasetId : Ljava/lang/String;
      //   24: aload_0
      //   25: aload_3
      //   26: putfield mClientState : Landroid/os/Bundle;
      //   29: aload_0
      //   30: aload #4
      //   32: putfield mSelectedDatasetIds : Ljava/util/List;
      //   35: aload_0
      //   36: aload #5
      //   38: putfield mIgnoredDatasetIds : Landroid/util/ArraySet;
      //   41: aload #6
      //   43: ifnull -> 88
      //   46: aload #6
      //   48: invokestatic isEmpty : (Ljava/util/Collection;)Z
      //   51: ifne -> 78
      //   54: aload #7
      //   56: ifnull -> 78
      //   59: aload #6
      //   61: invokevirtual size : ()I
      //   64: aload #7
      //   66: invokevirtual size : ()I
      //   69: if_icmpne -> 78
      //   72: iconst_1
      //   73: istore #13
      //   75: goto -> 81
      //   78: iconst_0
      //   79: istore #13
      //   81: iload #13
      //   83: ldc 'changed ids must have same length and not be empty'
      //   85: invokestatic checkArgument : (ZLjava/lang/Object;)V
      //   88: aload_0
      //   89: aload #6
      //   91: putfield mChangedFieldIds : Ljava/util/ArrayList;
      //   94: aload_0
      //   95: aload #7
      //   97: putfield mChangedDatasetIds : Ljava/util/ArrayList;
      //   100: aload #8
      //   102: ifnull -> 148
      //   105: aload #8
      //   107: invokestatic isEmpty : (Ljava/util/Collection;)Z
      //   110: ifne -> 137
      //   113: aload #9
      //   115: ifnull -> 137
      //   118: aload #8
      //   120: invokevirtual size : ()I
      //   123: aload #9
      //   125: invokevirtual size : ()I
      //   128: if_icmpne -> 137
      //   131: iconst_1
      //   132: istore #13
      //   134: goto -> 141
      //   137: iload #12
      //   139: istore #13
      //   141: iload #13
      //   143: ldc 'manually filled ids must have same length and not be empty'
      //   145: invokestatic checkArgument : (ZLjava/lang/Object;)V
      //   148: aload_0
      //   149: aload #8
      //   151: putfield mManuallyFilledFieldIds : Ljava/util/ArrayList;
      //   154: aload_0
      //   155: aload #9
      //   157: putfield mManuallyFilledDatasetIds : Ljava/util/ArrayList;
      //   160: aload_0
      //   161: aload #10
      //   163: putfield mDetectedFieldIds : [Landroid/view/autofill/AutofillId;
      //   166: aload_0
      //   167: aload #11
      //   169: putfield mDetectedFieldClassifications : [Landroid/service/autofill/FieldClassification;
      //   172: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #483	-> 0
      //   #484	-> 4
      //   #486	-> 19
      //   #487	-> 24
      //   #488	-> 29
      //   #489	-> 35
      //   #490	-> 41
      //   #491	-> 46
      //   #493	-> 59
      //   #491	-> 81
      //   #496	-> 88
      //   #497	-> 94
      //   #498	-> 100
      //   #499	-> 105
      //   #501	-> 118
      //   #499	-> 141
      //   #504	-> 148
      //   #505	-> 154
      //   #507	-> 160
      //   #508	-> 166
      //   #509	-> 172
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("FillEvent [datasetId=");
      stringBuilder.append(this.mDatasetId);
      stringBuilder.append(", type=");
      stringBuilder.append(this.mEventType);
      stringBuilder.append(", selectedDatasets=");
      stringBuilder.append(this.mSelectedDatasetIds);
      stringBuilder.append(", ignoredDatasetIds=");
      stringBuilder.append(this.mIgnoredDatasetIds);
      stringBuilder.append(", changedFieldIds=");
      stringBuilder.append(this.mChangedFieldIds);
      stringBuilder.append(", changedDatasetsIds=");
      stringBuilder.append(this.mChangedDatasetIds);
      stringBuilder.append(", manuallyFilledFieldIds=");
      stringBuilder.append(this.mManuallyFilledFieldIds);
      stringBuilder.append(", manuallyFilledDatasetIds=");
      stringBuilder.append(this.mManuallyFilledDatasetIds);
      stringBuilder.append(", detectedFieldIds=");
      AutofillId[] arrayOfAutofillId = this.mDetectedFieldIds;
      stringBuilder.append(Arrays.toString((Object[])arrayOfAutofillId));
      stringBuilder.append(", detectedFieldClassifications =");
      FieldClassification[] arrayOfFieldClassification = this.mDetectedFieldClassifications;
      stringBuilder.append(Arrays.toString((Object[])arrayOfFieldClassification));
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
    
    @Retention(RetentionPolicy.SOURCE)
    static @interface EventIds {}
  }
  
  public static final Parcelable.Creator<FillEventHistory> CREATOR = new Parcelable.Creator<FillEventHistory>() {
      public FillEventHistory createFromParcel(Parcel param1Parcel) {
        FillEventHistory fillEventHistory = new FillEventHistory(0, param1Parcel.readBundle());
        int i = param1Parcel.readInt();
        for (byte b = 0; b < i; b++) {
          FieldClassification[] arrayOfFieldClassification;
          int j = param1Parcel.readInt();
          String str = param1Parcel.readString();
          Bundle bundle = param1Parcel.readBundle();
          ArrayList<String> arrayList1 = param1Parcel.createStringArrayList();
          ArraySet<? extends Object> arraySet = param1Parcel.readArraySet(null);
          Parcelable.Creator<?> creator = AutofillId.CREATOR;
          ArrayList<?> arrayList2 = param1Parcel.createTypedArrayList(creator);
          ArrayList<String> arrayList3 = param1Parcel.createStringArrayList();
          creator = AutofillId.CREATOR;
          ArrayList<?> arrayList4 = param1Parcel.createTypedArrayList(creator);
          if (arrayList4 != null) {
            int k = arrayList4.size();
            ArrayList<ArrayList<String>> arrayList = new ArrayList(k);
            for (byte b1 = 0; b1 < k; b1++)
              arrayList.add(param1Parcel.createStringArrayList()); 
          } else {
            creator = null;
          } 
          AutofillId[] arrayOfAutofillId = param1Parcel.<AutofillId>readParcelableArray(null, AutofillId.class);
          if (arrayOfAutofillId != null) {
            arrayOfFieldClassification = FieldClassification.readArrayFromParcel(param1Parcel);
          } else {
            arrayOfFieldClassification = null;
          } 
          fillEventHistory.addEvent(new FillEventHistory.Event(j, str, bundle, arrayList1, (ArraySet)arraySet, (ArrayList)arrayList2, arrayList3, (ArrayList)arrayList4, (ArrayList)creator, arrayOfAutofillId, arrayOfFieldClassification));
        } 
        return fillEventHistory;
      }
      
      public FillEventHistory[] newArray(int param1Int) {
        return new FillEventHistory[param1Int];
      }
    };
  
  private static final String TAG = "FillEventHistory";
  
  private final Bundle mClientState;
  
  List<Event> mEvents;
  
  private final int mSessionId;
}
