package android.service.autofill;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.autofill.AutofillId;
import android.view.autofill.Helper;
import android.widget.RemoteViews;
import com.android.internal.util.Preconditions;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.regex.Pattern;

public final class ImageTransformation extends InternalTransformation implements Transformation, Parcelable {
  private ImageTransformation(Builder paramBuilder) {
    this.mId = paramBuilder.mId;
    this.mOptions = paramBuilder.mOptions;
  }
  
  public void apply(ValueFinder paramValueFinder, RemoteViews paramRemoteViews, int paramInt) throws Exception {
    StringBuilder stringBuilder;
    String str = paramValueFinder.findByAutofillId(this.mId);
    if (str == null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("No view for id ");
      stringBuilder1.append(this.mId);
      Log.w("ImageTransformation", stringBuilder1.toString());
      return;
    } 
    int i = this.mOptions.size();
    if (Helper.sDebug) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(i);
      stringBuilder1.append(" multiple options on id ");
      stringBuilder1.append(paramInt);
      stringBuilder1.append(" to compare against");
      Log.d("ImageTransformation", stringBuilder1.toString());
    } 
    for (byte b = 0; b < i; ) {
      Option option = this.mOptions.get(b);
      try {
        if (option.pattern.matcher(str).matches()) {
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Found match at ");
          stringBuilder.append(b);
          stringBuilder.append(": ");
          stringBuilder.append(option);
          Log.d("ImageTransformation", stringBuilder.toString());
          paramRemoteViews.setImageViewResource(paramInt, option.resId);
          if (option.contentDescription != null)
            paramRemoteViews.setContentDescription(paramInt, option.contentDescription); 
          return;
        } 
        b++;
      } catch (Exception exception) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Error matching regex #");
        stringBuilder.append(b);
        stringBuilder.append("(");
        stringBuilder.append(option.pattern);
        stringBuilder.append(") on id ");
        stringBuilder.append(option.resId);
        stringBuilder.append(": ");
        stringBuilder.append(exception.getClass());
        String str1 = stringBuilder.toString();
        Log.w("ImageTransformation", str1);
        throw exception;
      } 
    } 
    if (Helper.sDebug) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("No match for ");
      stringBuilder1.append((String)stringBuilder);
      Log.d("ImageTransformation", stringBuilder1.toString());
    } 
  }
  
  class Builder {
    private boolean mDestroyed;
    
    private final AutofillId mId;
    
    private final ArrayList<ImageTransformation.Option> mOptions = new ArrayList<>();
    
    @Deprecated
    public Builder(ImageTransformation this$0, Pattern param1Pattern, int param1Int) {
      this.mId = (AutofillId)Preconditions.checkNotNull(this$0);
      addOption(param1Pattern, param1Int);
    }
    
    public Builder(ImageTransformation this$0, Pattern param1Pattern, int param1Int, CharSequence param1CharSequence) {
      this.mId = (AutofillId)Preconditions.checkNotNull(this$0);
      addOption(param1Pattern, param1Int, param1CharSequence);
    }
    
    @Deprecated
    public Builder addOption(Pattern param1Pattern, int param1Int) {
      addOptionInternal(param1Pattern, param1Int, null);
      return this;
    }
    
    public Builder addOption(Pattern param1Pattern, int param1Int, CharSequence param1CharSequence) {
      addOptionInternal(param1Pattern, param1Int, (CharSequence)Preconditions.checkNotNull(param1CharSequence));
      return this;
    }
    
    private void addOptionInternal(Pattern param1Pattern, int param1Int, CharSequence param1CharSequence) {
      boolean bool;
      throwIfDestroyed();
      Preconditions.checkNotNull(param1Pattern);
      if (param1Int != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkArgument(bool);
      this.mOptions.add(new ImageTransformation.Option(param1Pattern, param1Int, param1CharSequence));
    }
    
    public ImageTransformation build() {
      throwIfDestroyed();
      this.mDestroyed = true;
      return new ImageTransformation(this);
    }
    
    private void throwIfDestroyed() {
      Preconditions.checkState(this.mDestroyed ^ true, "Already called build()");
    }
  }
  
  public String toString() {
    if (!Helper.sDebug)
      return super.toString(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ImageTransformation: [id=");
    stringBuilder.append(this.mId);
    stringBuilder.append(", options=");
    stringBuilder.append(this.mOptions);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.mId, paramInt);
    int i = this.mOptions.size();
    Pattern[] arrayOfPattern = new Pattern[i];
    int[] arrayOfInt = new int[i];
    String[] arrayOfString = new String[i];
    for (paramInt = 0; paramInt < i; paramInt++) {
      Option option = this.mOptions.get(paramInt);
      arrayOfPattern[paramInt] = option.pattern;
      arrayOfInt[paramInt] = option.resId;
      arrayOfString[paramInt] = (String)option.contentDescription;
    } 
    paramParcel.writeSerializable((Serializable)arrayOfPattern);
    paramParcel.writeIntArray(arrayOfInt);
    paramParcel.writeCharSequenceArray((CharSequence[])arrayOfString);
  }
  
  public static final Parcelable.Creator<ImageTransformation> CREATOR = (Parcelable.Creator<ImageTransformation>)new Object();
  
  private static final String TAG = "ImageTransformation";
  
  private final AutofillId mId;
  
  private final ArrayList<Option> mOptions;
  
  class Option {
    public final CharSequence contentDescription;
    
    public final Pattern pattern;
    
    public final int resId;
    
    Option(ImageTransformation this$0, int param1Int, CharSequence param1CharSequence) {
      this.pattern = (Pattern)this$0;
      this.resId = param1Int;
      this.contentDescription = TextUtils.trimNoCopySpans(param1CharSequence);
    }
  }
}
