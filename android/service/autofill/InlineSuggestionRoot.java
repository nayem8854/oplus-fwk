package android.service.autofill;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.util.MathUtils;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;

public class InlineSuggestionRoot extends FrameLayout {
  private static final String TAG = "InlineSuggestionRoot";
  
  private final IInlineSuggestionUiCallback mCallback;
  
  private float mDownX;
  
  private float mDownY;
  
  private final int mTouchSlop;
  
  public InlineSuggestionRoot(Context paramContext, IInlineSuggestionUiCallback paramIInlineSuggestionUiCallback) {
    super(paramContext);
    this.mCallback = paramIInlineSuggestionUiCallback;
    this.mTouchSlop = ViewConfiguration.get(paramContext).getScaledTouchSlop();
    setFocusable(false);
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getActionMasked();
    if (i != 0) {
      if (i != 2)
        return super.dispatchTouchEvent(paramMotionEvent); 
    } else {
      this.mDownX = paramMotionEvent.getX();
      this.mDownY = paramMotionEvent.getY();
    } 
    float f1 = this.mDownX, f2 = this.mDownY;
    float f3 = paramMotionEvent.getX(), f4 = paramMotionEvent.getY();
    f3 = MathUtils.dist(f1, f2, f3, f4);
    if ((0x2 & paramMotionEvent.getFlags()) == 0) {
      i = 1;
    } else {
      i = 0;
    } 
    if (i == 0 || f3 > this.mTouchSlop)
      try {
        IInlineSuggestionUiCallback iInlineSuggestionUiCallback = this.mCallback;
        IBinder iBinder = getViewRootImpl().getInputToken();
        i = getContext().getDisplayId();
        iInlineSuggestionUiCallback.onTransferTouchFocusToImeWindow(iBinder, i);
      } catch (RemoteException remoteException) {
        Log.w("InlineSuggestionRoot", "RemoteException transferring touch focus to IME");
      }  
    return super.dispatchTouchEvent(paramMotionEvent);
  }
}
