package android.service.autofill;

import android.os.Parcel;
import android.view.autofill.Helper;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class FieldClassification {
  private final ArrayList<Match> mMatches;
  
  public FieldClassification(ArrayList<Match> paramArrayList) {
    this.mMatches = paramArrayList = (ArrayList<Match>)Preconditions.checkNotNull(paramArrayList);
    Collections.sort(paramArrayList, new Comparator<Match>() {
          final FieldClassification this$0;
          
          public int compare(FieldClassification.Match param1Match1, FieldClassification.Match param1Match2) {
            if (param1Match1.mScore > param1Match2.mScore)
              return -1; 
            if (param1Match1.mScore < param1Match2.mScore)
              return 1; 
            return 0;
          }
        });
  }
  
  public List<Match> getMatches() {
    return this.mMatches;
  }
  
  public String toString() {
    if (!Helper.sDebug)
      return super.toString(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("FieldClassification: ");
    stringBuilder.append(this.mMatches);
    return stringBuilder.toString();
  }
  
  private void writeToParcel(Parcel paramParcel) {
    paramParcel.writeInt(this.mMatches.size());
    for (byte b = 0; b < this.mMatches.size(); b++)
      ((Match)this.mMatches.get(b)).writeToParcel(paramParcel); 
  }
  
  private static FieldClassification readFromParcel(Parcel paramParcel) {
    int i = paramParcel.readInt();
    ArrayList<Match> arrayList = new ArrayList();
    for (byte b = 0; b < i; b++)
      arrayList.add(b, Match.readFromParcel(paramParcel)); 
    return new FieldClassification(arrayList);
  }
  
  static FieldClassification[] readArrayFromParcel(Parcel paramParcel) {
    int i = paramParcel.readInt();
    FieldClassification[] arrayOfFieldClassification = new FieldClassification[i];
    for (byte b = 0; b < i; b++)
      arrayOfFieldClassification[b] = readFromParcel(paramParcel); 
    return arrayOfFieldClassification;
  }
  
  static void writeArrayToParcel(Parcel paramParcel, FieldClassification[] paramArrayOfFieldClassification) {
    paramParcel.writeInt(paramArrayOfFieldClassification.length);
    for (byte b = 0; b < paramArrayOfFieldClassification.length; b++)
      paramArrayOfFieldClassification[b].writeToParcel(paramParcel); 
  }
  
  public static final class Match {
    private final String mCategoryId;
    
    private final float mScore;
    
    public Match(String param1String, float param1Float) {
      this.mCategoryId = (String)Preconditions.checkNotNull(param1String);
      this.mScore = param1Float;
    }
    
    public String getCategoryId() {
      return this.mCategoryId;
    }
    
    public float getScore() {
      return this.mScore;
    }
    
    public String toString() {
      if (!Helper.sDebug)
        return super.toString(); 
      StringBuilder stringBuilder = new StringBuilder("Match: categoryId=");
      Helper.appendRedacted(stringBuilder, this.mCategoryId);
      stringBuilder.append(", score=");
      stringBuilder.append(this.mScore);
      return stringBuilder.toString();
    }
    
    private void writeToParcel(Parcel param1Parcel) {
      param1Parcel.writeString(this.mCategoryId);
      param1Parcel.writeFloat(this.mScore);
    }
    
    private static Match readFromParcel(Parcel param1Parcel) {
      return new Match(param1Parcel.readString(), param1Parcel.readFloat());
    }
  }
}
