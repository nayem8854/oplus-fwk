package android.service.autofill;

import android.content.IntentSender;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.SurfaceControlViewHost;

public interface IInlineSuggestionUiCallback extends IInterface {
  void onClick() throws RemoteException;
  
  void onContent(IInlineSuggestionUi paramIInlineSuggestionUi, SurfaceControlViewHost.SurfacePackage paramSurfacePackage, int paramInt1, int paramInt2) throws RemoteException;
  
  void onError() throws RemoteException;
  
  void onLongClick() throws RemoteException;
  
  void onStartIntentSender(IntentSender paramIntentSender) throws RemoteException;
  
  void onTransferTouchFocusToImeWindow(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  class Default implements IInlineSuggestionUiCallback {
    public void onClick() throws RemoteException {}
    
    public void onLongClick() throws RemoteException {}
    
    public void onContent(IInlineSuggestionUi param1IInlineSuggestionUi, SurfaceControlViewHost.SurfacePackage param1SurfacePackage, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onError() throws RemoteException {}
    
    public void onTransferTouchFocusToImeWindow(IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public void onStartIntentSender(IntentSender param1IntentSender) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInlineSuggestionUiCallback {
    private static final String DESCRIPTOR = "android.service.autofill.IInlineSuggestionUiCallback";
    
    static final int TRANSACTION_onClick = 1;
    
    static final int TRANSACTION_onContent = 3;
    
    static final int TRANSACTION_onError = 4;
    
    static final int TRANSACTION_onLongClick = 2;
    
    static final int TRANSACTION_onStartIntentSender = 6;
    
    static final int TRANSACTION_onTransferTouchFocusToImeWindow = 5;
    
    public Stub() {
      attachInterface(this, "android.service.autofill.IInlineSuggestionUiCallback");
    }
    
    public static IInlineSuggestionUiCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.autofill.IInlineSuggestionUiCallback");
      if (iInterface != null && iInterface instanceof IInlineSuggestionUiCallback)
        return (IInlineSuggestionUiCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "onStartIntentSender";
        case 5:
          return "onTransferTouchFocusToImeWindow";
        case 4:
          return "onError";
        case 3:
          return "onContent";
        case 2:
          return "onLongClick";
        case 1:
          break;
      } 
      return "onClick";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IBinder iBinder;
      if (param1Int1 != 1598968902) {
        IInlineSuggestionUi iInlineSuggestionUi;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("android.service.autofill.IInlineSuggestionUiCallback");
            if (param1Parcel1.readInt() != 0) {
              IntentSender intentSender = IntentSender.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onStartIntentSender((IntentSender)param1Parcel1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.service.autofill.IInlineSuggestionUiCallback");
            iBinder = param1Parcel1.readStrongBinder();
            param1Int1 = param1Parcel1.readInt();
            onTransferTouchFocusToImeWindow(iBinder, param1Int1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.service.autofill.IInlineSuggestionUiCallback");
            onError();
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.service.autofill.IInlineSuggestionUiCallback");
            iInlineSuggestionUi = IInlineSuggestionUi.Stub.asInterface(param1Parcel1.readStrongBinder());
            if (param1Parcel1.readInt() != 0) {
              SurfaceControlViewHost.SurfacePackage surfacePackage = SurfaceControlViewHost.SurfacePackage.CREATOR.createFromParcel(param1Parcel1);
            } else {
              iBinder = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            onContent(iInlineSuggestionUi, (SurfaceControlViewHost.SurfacePackage)iBinder, param1Int1, param1Int2);
            return true;
          case 2:
            param1Parcel1.enforceInterface("android.service.autofill.IInlineSuggestionUiCallback");
            onLongClick();
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("android.service.autofill.IInlineSuggestionUiCallback");
        onClick();
        return true;
      } 
      iBinder.writeString("android.service.autofill.IInlineSuggestionUiCallback");
      return true;
    }
    
    private static class Proxy implements IInlineSuggestionUiCallback {
      public static IInlineSuggestionUiCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.autofill.IInlineSuggestionUiCallback";
      }
      
      public void onClick() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.autofill.IInlineSuggestionUiCallback");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IInlineSuggestionUiCallback.Stub.getDefaultImpl() != null) {
            IInlineSuggestionUiCallback.Stub.getDefaultImpl().onClick();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onLongClick() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.autofill.IInlineSuggestionUiCallback");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IInlineSuggestionUiCallback.Stub.getDefaultImpl() != null) {
            IInlineSuggestionUiCallback.Stub.getDefaultImpl().onLongClick();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onContent(IInlineSuggestionUi param2IInlineSuggestionUi, SurfaceControlViewHost.SurfacePackage param2SurfacePackage, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.autofill.IInlineSuggestionUiCallback");
          if (param2IInlineSuggestionUi != null) {
            iBinder = param2IInlineSuggestionUi.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2SurfacePackage != null) {
            parcel.writeInt(1);
            param2SurfacePackage.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IInlineSuggestionUiCallback.Stub.getDefaultImpl() != null) {
            IInlineSuggestionUiCallback.Stub.getDefaultImpl().onContent(param2IInlineSuggestionUi, param2SurfacePackage, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onError() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.autofill.IInlineSuggestionUiCallback");
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IInlineSuggestionUiCallback.Stub.getDefaultImpl() != null) {
            IInlineSuggestionUiCallback.Stub.getDefaultImpl().onError();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTransferTouchFocusToImeWindow(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.autofill.IInlineSuggestionUiCallback");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IInlineSuggestionUiCallback.Stub.getDefaultImpl() != null) {
            IInlineSuggestionUiCallback.Stub.getDefaultImpl().onTransferTouchFocusToImeWindow(param2IBinder, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onStartIntentSender(IntentSender param2IntentSender) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.autofill.IInlineSuggestionUiCallback");
          if (param2IntentSender != null) {
            parcel.writeInt(1);
            param2IntentSender.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IInlineSuggestionUiCallback.Stub.getDefaultImpl() != null) {
            IInlineSuggestionUiCallback.Stub.getDefaultImpl().onStartIntentSender(param2IntentSender);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInlineSuggestionUiCallback param1IInlineSuggestionUiCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInlineSuggestionUiCallback != null) {
          Proxy.sDefaultImpl = param1IInlineSuggestionUiCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInlineSuggestionUiCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
