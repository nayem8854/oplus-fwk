package android.service.autofill;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteCallback;
import android.os.RemoteException;
import android.util.Log;
import android.view.autofill.AutofillValue;
import com.android.internal.util.function.NonaConsumer;
import com.android.internal.util.function.pooled.PooledLambda;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@SystemApi
public abstract class AutofillFieldClassificationService extends Service {
  public static final String EXTRA_SCORES = "scores";
  
  public static final String REQUIRED_ALGORITHM_CREDIT_CARD = "CREDIT_CARD";
  
  public static final String REQUIRED_ALGORITHM_EDIT_DISTANCE = "EDIT_DISTANCE";
  
  public static final String REQUIRED_ALGORITHM_EXACT_MATCH = "EXACT_MATCH";
  
  public static final String SERVICE_INTERFACE = "android.service.autofill.AutofillFieldClassificationService";
  
  public static final String SERVICE_META_DATA_KEY_AVAILABLE_ALGORITHMS = "android.autofill.field_classification.available_algorithms";
  
  public static final String SERVICE_META_DATA_KEY_DEFAULT_ALGORITHM = "android.autofill.field_classification.default_algorithm";
  
  private static final String TAG = "AutofillFieldClassificationService";
  
  private void calculateScores(RemoteCallback paramRemoteCallback, List<AutofillValue> paramList, String[] paramArrayOfString1, String[] paramArrayOfString2, String paramString, Bundle paramBundle, Map paramMap1, Map paramMap2) {
    Bundle bundle = new Bundle();
    List<String> list1 = Arrays.asList(paramArrayOfString1);
    List<String> list2 = Arrays.asList(paramArrayOfString2);
    float[][] arrayOfFloat = onCalculateScores(paramList, list1, list2, paramString, paramBundle, paramMap1, paramMap2);
    if (arrayOfFloat != null)
      bundle.putParcelable("scores", new Scores()); 
    paramRemoteCallback.sendResult(bundle);
  }
  
  private final Handler mHandler = new Handler(Looper.getMainLooper(), null, true);
  
  private AutofillFieldClassificationServiceWrapper mWrapper;
  
  public void onCreate() {
    super.onCreate();
    this.mWrapper = new AutofillFieldClassificationServiceWrapper();
  }
  
  public IBinder onBind(Intent paramIntent) {
    return this.mWrapper;
  }
  
  @SystemApi
  @Deprecated
  public float[][] onGetScores(String paramString, Bundle paramBundle, List<AutofillValue> paramList, List<String> paramList1) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("service implementation (");
    stringBuilder.append(getClass());
    stringBuilder.append(" does not implement onGetScores()");
    Log.e("AutofillFieldClassificationService", stringBuilder.toString());
    return null;
  }
  
  @SystemApi
  public float[][] onCalculateScores(List<AutofillValue> paramList, List<String> paramList1, List<String> paramList2, String paramString, Bundle paramBundle, Map paramMap1, Map paramMap2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("service implementation (");
    stringBuilder.append(getClass());
    stringBuilder.append(" does not implement onCalculateScore()");
    Log.e("AutofillFieldClassificationService", stringBuilder.toString());
    return null;
  }
  
  private final class AutofillFieldClassificationServiceWrapper extends IAutofillFieldClassificationService.Stub {
    final AutofillFieldClassificationService this$0;
    
    private AutofillFieldClassificationServiceWrapper() {}
    
    public void calculateScores(RemoteCallback param1RemoteCallback, List<AutofillValue> param1List, String[] param1ArrayOfString1, String[] param1ArrayOfString2, String param1String, Bundle param1Bundle, Map param1Map1, Map param1Map2) throws RemoteException {
      AutofillFieldClassificationService.this.mHandler.sendMessage(PooledLambda.obtainMessage((NonaConsumer)_$$Lambda$AutofillFieldClassificationService$AutofillFieldClassificationServiceWrapper$mUalgFt87R5lup2LhB9vW49Xixs.INSTANCE, AutofillFieldClassificationService.this, param1RemoteCallback, param1List, param1ArrayOfString1, param1ArrayOfString2, param1String, param1Bundle, param1Map1, param1Map2));
    }
  }
  
  class Scores implements Parcelable {
    private Scores(AutofillFieldClassificationService this$0) {
      int i = this$0.readInt();
      int j = this$0.readInt();
      this.scores = new float[i][j];
      for (byte b = 0; b < i; b++) {
        for (byte b1 = 0; b1 < j; b1++)
          this.scores[b][b1] = this$0.readFloat(); 
      } 
    }
    
    private Scores() {
      this.scores = (float[][])this$0;
    }
    
    public String toString() {
      float[][] arrayOfFloat = this.scores;
      int i = arrayOfFloat.length;
      int j = 0;
      if (i > 0)
        j = (arrayOfFloat[0]).length; 
      StringBuilder stringBuilder = new StringBuilder("Scores [");
      stringBuilder.append(i);
      stringBuilder.append("x");
      stringBuilder.append(j);
      stringBuilder = stringBuilder.append("] ");
      for (j = 0; j < i; j++) {
        stringBuilder.append(j);
        stringBuilder.append(": ");
        stringBuilder.append(Arrays.toString(this.scores[j]));
        stringBuilder.append(' ');
      } 
      return stringBuilder.toString();
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      float[][] arrayOfFloat = this.scores;
      int i = arrayOfFloat.length;
      int j = (arrayOfFloat[0]).length;
      param1Parcel.writeInt(i);
      param1Parcel.writeInt(j);
      for (param1Int = 0; param1Int < i; param1Int++) {
        for (byte b = 0; b < j; b++)
          param1Parcel.writeFloat(this.scores[param1Int][b]); 
      } 
    }
    
    public static final Parcelable.Creator<Scores> CREATOR = new Parcelable.Creator<Scores>() {
        public AutofillFieldClassificationService.Scores createFromParcel(Parcel param2Parcel) {
          return new AutofillFieldClassificationService.Scores();
        }
        
        public AutofillFieldClassificationService.Scores[] newArray(int param2Int) {
          return new AutofillFieldClassificationService.Scores[param2Int];
        }
      };
    
    public final float[][] scores;
  }
}
