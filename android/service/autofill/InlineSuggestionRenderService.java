package android.service.autofill;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.BaseBundle;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteCallback;
import android.os.RemoteException;
import android.util.Log;
import android.util.LruCache;
import android.util.Size;
import android.view.SurfaceControlViewHost;
import android.view.View;
import android.view.WindowManager;
import com.android.internal.util.function.NonaConsumer;
import com.android.internal.util.function.TriConsumer;
import com.android.internal.util.function.pooled.PooledLambda;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.function.BiConsumer;

@SystemApi
public abstract class InlineSuggestionRenderService extends Service {
  private final Handler mMainHandler = new Handler(Looper.getMainLooper(), null, true);
  
  private IInlineSuggestionUiCallback mCallback;
  
  private final LruCache<InlineSuggestionUiImpl, Boolean> mActiveInlineSuggestions = (LruCache<InlineSuggestionUiImpl, Boolean>)new Object(this, 30);
  
  private static final String TAG = "InlineSuggestionRenderService";
  
  public static final String SERVICE_INTERFACE = "android.service.autofill.InlineSuggestionRenderService";
  
  private Size measuredSize(View paramView, int paramInt1, int paramInt2, Size paramSize1, Size paramSize2) {
    if (paramInt1 != -2 && paramInt2 != -2)
      return new Size(paramInt1, paramInt2); 
    if (paramInt1 == -2) {
      paramInt1 = View.MeasureSpec.makeMeasureSpec(paramSize2.getWidth(), -2147483648);
    } else {
      paramInt1 = View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824);
    } 
    if (paramInt2 == -2) {
      paramInt2 = View.MeasureSpec.makeMeasureSpec(paramSize2.getHeight(), -2147483648);
    } else {
      paramInt2 = View.MeasureSpec.makeMeasureSpec(paramInt2, 1073741824);
    } 
    paramView.measure(paramInt1, paramInt2);
    paramInt1 = Math.max(paramView.getMeasuredWidth(), paramSize1.getWidth());
    return 
      new Size(paramInt1, Math.max(paramView.getMeasuredHeight(), paramSize1.getHeight()));
  }
  
  private void handleRenderSuggestion(IInlineSuggestionUiCallback paramIInlineSuggestionUiCallback, InlinePresentation paramInlinePresentation, int paramInt1, int paramInt2, IBinder paramIBinder, int paramInt3, int paramInt4, int paramInt5) {
    if (paramIBinder == null) {
      try {
        paramIInlineSuggestionUiCallback.onError();
      } catch (RemoteException null) {
        Log.w("InlineSuggestionRenderService", "RemoteException calling onError()");
      } 
      return;
    } 
    updateDisplay(paramInt3);
    try {
      View view = onRenderSuggestion(paramInlinePresentation, paramInt1, paramInt2);
      if (view == null) {
        Log.w("InlineSuggestionRenderService", "ExtServices failed to render the inline suggestion view.");
        try {
          null.onError();
        } catch (RemoteException remoteException) {
          Log.w("InlineSuggestionRenderService", "Null suggestion view returned by renderer");
        } 
        return;
      } 
      this.mCallback = (IInlineSuggestionUiCallback)remoteException;
      Size size2 = paramInlinePresentation.getInlinePresentationSpec().getMinSize();
      Size size1 = paramInlinePresentation.getInlinePresentationSpec().getMaxSize();
      size1 = measuredSize(view, paramInt1, paramInt2, size2, size1);
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("width=");
      stringBuilder.append(paramInt1);
      stringBuilder.append(", height=");
      stringBuilder.append(paramInt2);
      stringBuilder.append(", measuredSize=");
      stringBuilder.append(size1);
      Log.v("InlineSuggestionRenderService", stringBuilder.toString());
      InlineSuggestionRoot inlineSuggestionRoot = new InlineSuggestionRoot();
      this((Context)this, (IInlineSuggestionUiCallback)remoteException);
      inlineSuggestionRoot.addView(view);
      WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
      paramInt1 = size1.getWidth();
      this(paramInt1, size1.getHeight(), 2, 0, -2);
      SurfaceControlViewHost surfaceControlViewHost = new SurfaceControlViewHost();
      this((Context)this, getDisplay(), paramIBinder);
      surfaceControlViewHost.setView((View)inlineSuggestionRoot, layoutParams);
      view.setFocusable(false);
      _$$Lambda$InlineSuggestionRenderService$4WveOqCGJeRb08DN4p3fTmw8h_M _$$Lambda$InlineSuggestionRenderService$4WveOqCGJeRb08DN4p3fTmw8h_M = new _$$Lambda$InlineSuggestionRenderService$4WveOqCGJeRb08DN4p3fTmw8h_M();
      this((IInlineSuggestionUiCallback)remoteException);
      view.setOnClickListener(_$$Lambda$InlineSuggestionRenderService$4WveOqCGJeRb08DN4p3fTmw8h_M);
      View.OnLongClickListener onLongClickListener = view.getOnLongClickListener();
      _$$Lambda$InlineSuggestionRenderService$dfBSVSK_a4i7SZpYGrwwobqbHvM _$$Lambda$InlineSuggestionRenderService$dfBSVSK_a4i7SZpYGrwwobqbHvM = new _$$Lambda$InlineSuggestionRenderService$dfBSVSK_a4i7SZpYGrwwobqbHvM();
      this(onLongClickListener, (IInlineSuggestionUiCallback)remoteException);
      view.setOnLongClickListener(_$$Lambda$InlineSuggestionRenderService$dfBSVSK_a4i7SZpYGrwwobqbHvM);
      InlineSuggestionUiImpl inlineSuggestionUiImpl = new InlineSuggestionUiImpl();
      this(this, surfaceControlViewHost, this.mMainHandler, paramInt4, paramInt5);
      this.mActiveInlineSuggestions.put(inlineSuggestionUiImpl, Boolean.valueOf(true));
      Handler handler = this.mMainHandler;
      _$$Lambda$InlineSuggestionRenderService$YF_fG0Ukz_FD1JUnOOmqINNG7Xc _$$Lambda$InlineSuggestionRenderService$YF_fG0Ukz_FD1JUnOOmqINNG7Xc = new _$$Lambda$InlineSuggestionRenderService$YF_fG0Ukz_FD1JUnOOmqINNG7Xc();
      this((IInlineSuggestionUiCallback)remoteException, inlineSuggestionUiImpl, surfaceControlViewHost, size1);
      handler.post(_$$Lambda$InlineSuggestionRenderService$YF_fG0Ukz_FD1JUnOOmqINNG7Xc);
      return;
    } finally {
      updateDisplay(0);
    } 
  }
  
  private void handleGetInlineSuggestionsRendererInfo(RemoteCallback paramRemoteCallback) {
    Bundle bundle = onGetInlineSuggestionsRendererInfo();
    paramRemoteCallback.sendResult(bundle);
  }
  
  private void handleDestroySuggestionViews(int paramInt1, int paramInt2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("handleDestroySuggestionViews called for ");
    stringBuilder.append(paramInt1);
    stringBuilder.append(":");
    stringBuilder.append(paramInt2);
    Log.v("InlineSuggestionRenderService", stringBuilder.toString());
    for (InlineSuggestionUiImpl inlineSuggestionUiImpl : this.mActiveInlineSuggestions.snapshot().keySet()) {
      if (inlineSuggestionUiImpl.mUserId == paramInt1 && 
        inlineSuggestionUiImpl.mSessionId == paramInt2) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Destroy ");
        stringBuilder1.append(inlineSuggestionUiImpl);
        Log.v("InlineSuggestionRenderService", stringBuilder1.toString());
        inlineSuggestionUiImpl.releaseSurfaceControlViewHost();
      } 
    } 
  }
  
  private static final class InlineSuggestionUiWrapper extends IInlineSuggestionUi.Stub {
    private final WeakReference<InlineSuggestionRenderService.InlineSuggestionUiImpl> mUiImpl;
    
    InlineSuggestionUiWrapper(InlineSuggestionRenderService.InlineSuggestionUiImpl param1InlineSuggestionUiImpl) {
      this.mUiImpl = new WeakReference<>(param1InlineSuggestionUiImpl);
    }
    
    public void releaseSurfaceControlViewHost() {
      InlineSuggestionRenderService.InlineSuggestionUiImpl inlineSuggestionUiImpl = this.mUiImpl.get();
      if (inlineSuggestionUiImpl != null)
        inlineSuggestionUiImpl.releaseSurfaceControlViewHost(); 
    }
    
    public void getSurfacePackage(ISurfacePackageResultCallback param1ISurfacePackageResultCallback) {
      InlineSuggestionRenderService.InlineSuggestionUiImpl inlineSuggestionUiImpl = this.mUiImpl.get();
      if (inlineSuggestionUiImpl != null)
        inlineSuggestionUiImpl.getSurfacePackage(param1ISurfacePackageResultCallback); 
    }
  }
  
  class InlineSuggestionUiImpl {
    private final Handler mHandler;
    
    private final int mSessionId;
    
    private final int mUserId;
    
    private SurfaceControlViewHost mViewHost;
    
    final InlineSuggestionRenderService this$0;
    
    InlineSuggestionUiImpl(SurfaceControlViewHost param1SurfaceControlViewHost, Handler param1Handler, int param1Int1, int param1Int2) {
      this.mViewHost = param1SurfaceControlViewHost;
      this.mHandler = param1Handler;
      this.mUserId = param1Int1;
      this.mSessionId = param1Int2;
    }
    
    public void releaseSurfaceControlViewHost() {
      this.mHandler.post(new _$$Lambda$InlineSuggestionRenderService$InlineSuggestionUiImpl$tbRHULrrEhVwh_9OT70ovKqEssY(this));
    }
    
    public void getSurfacePackage(ISurfacePackageResultCallback param1ISurfacePackageResultCallback) {
      Log.d("InlineSuggestionRenderService", "getSurfacePackage");
      this.mHandler.post(new _$$Lambda$InlineSuggestionRenderService$InlineSuggestionUiImpl$vSBz9jp2iZorXLIdZ_fbHUHXfvg(this, param1ISurfacePackageResultCallback));
    }
  }
  
  protected final void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("mActiveInlineSuggestions: ");
    stringBuilder.append(this.mActiveInlineSuggestions.size());
    paramPrintWriter.println(stringBuilder.toString());
    for (InlineSuggestionUiImpl inlineSuggestionUiImpl : this.mActiveInlineSuggestions.snapshot().keySet()) {
      paramPrintWriter.printf("ui: [%s] - [%d]  [%d]\n", new Object[] { inlineSuggestionUiImpl, Integer.valueOf(InlineSuggestionUiImpl.access$000(inlineSuggestionUiImpl)), Integer.valueOf(InlineSuggestionUiImpl.access$100(inlineSuggestionUiImpl)) });
    } 
  }
  
  public final IBinder onBind(Intent paramIntent) {
    BaseBundle.setShouldDefuse(true);
    if ("android.service.autofill.InlineSuggestionRenderService".equals(paramIntent.getAction())) {
      IInlineSuggestionRenderService.Stub stub = new IInlineSuggestionRenderService.Stub() {
          final InlineSuggestionRenderService this$0;
          
          public void renderSuggestion(IInlineSuggestionUiCallback param1IInlineSuggestionUiCallback, InlinePresentation param1InlinePresentation, int param1Int1, int param1Int2, IBinder param1IBinder, int param1Int3, int param1Int4, int param1Int5) {
            Handler handler = InlineSuggestionRenderService.this.mMainHandler;
            -$.Lambda.InlineSuggestionRenderService.null.vd9fhWB_XiCZWrDarmJyqz4P8bk vd9fhWB_XiCZWrDarmJyqz4P8bk = _$$Lambda$InlineSuggestionRenderService$2$vd9fhWB_XiCZWrDarmJyqz4P8bk.INSTANCE;
            InlineSuggestionRenderService inlineSuggestionRenderService = InlineSuggestionRenderService.this;
            Message message = PooledLambda.obtainMessage((NonaConsumer)vd9fhWB_XiCZWrDarmJyqz4P8bk, inlineSuggestionRenderService, param1IInlineSuggestionUiCallback, param1InlinePresentation, Integer.valueOf(param1Int1), Integer.valueOf(param1Int2), param1IBinder, Integer.valueOf(param1Int3), Integer.valueOf(param1Int4), Integer.valueOf(param1Int5));
            handler.sendMessage(message);
          }
          
          public void getInlineSuggestionsRendererInfo(RemoteCallback param1RemoteCallback) {
            InlineSuggestionRenderService.this.mMainHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$InlineSuggestionRenderService$2$P8kuVpV8xrQ2XumDOSp6OzENy78.INSTANCE, InlineSuggestionRenderService.this, param1RemoteCallback));
          }
          
          public void destroySuggestionViews(int param1Int1, int param1Int2) {
            Handler handler = InlineSuggestionRenderService.this.mMainHandler;
            -$.Lambda.InlineSuggestionRenderService.null.HLyVtxm57XSuIYYPsfuwvD7CfU0 hLyVtxm57XSuIYYPsfuwvD7CfU0 = _$$Lambda$InlineSuggestionRenderService$2$HLyVtxm57XSuIYYPsfuwvD7CfU0.INSTANCE;
            InlineSuggestionRenderService inlineSuggestionRenderService = InlineSuggestionRenderService.this;
            handler.sendMessage(PooledLambda.obtainMessage((TriConsumer)hLyVtxm57XSuIYYPsfuwvD7CfU0, inlineSuggestionRenderService, Integer.valueOf(param1Int1), Integer.valueOf(param1Int2)));
          }
        };
      return stub.asBinder();
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Tried to bind to wrong intent (should be android.service.autofill.InlineSuggestionRenderService: ");
    stringBuilder.append(paramIntent);
    Log.w("InlineSuggestionRenderService", stringBuilder.toString());
    return null;
  }
  
  public final void startIntentSender(IntentSender paramIntentSender) {
    IInlineSuggestionUiCallback iInlineSuggestionUiCallback = this.mCallback;
    if (iInlineSuggestionUiCallback == null)
      return; 
    try {
      iInlineSuggestionUiCallback.onStartIntentSender(paramIntentSender);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Bundle onGetInlineSuggestionsRendererInfo() {
    return Bundle.EMPTY;
  }
  
  public View onRenderSuggestion(InlinePresentation paramInlinePresentation, int paramInt1, int paramInt2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("service implementation (");
    stringBuilder.append(getClass());
    stringBuilder.append(" does not implement onRenderSuggestion()");
    Log.e("InlineSuggestionRenderService", stringBuilder.toString());
    return null;
  }
}
