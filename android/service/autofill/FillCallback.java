package android.service.autofill;

import android.os.RemoteException;
import android.util.Log;

public final class FillCallback {
  private static final String TAG = "FillCallback";
  
  private final IFillCallback mCallback;
  
  private boolean mCalled;
  
  private final int mRequestId;
  
  public FillCallback(IFillCallback paramIFillCallback, int paramInt) {
    this.mCallback = paramIFillCallback;
    this.mRequestId = paramInt;
  }
  
  public void onSuccess(FillResponse paramFillResponse) {
    assertNotCalled();
    this.mCalled = true;
    if (paramFillResponse != null)
      paramFillResponse.setRequestId(this.mRequestId); 
    try {
      this.mCallback.onSuccess(paramFillResponse);
    } catch (RemoteException remoteException) {
      remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public void onFailure(CharSequence paramCharSequence) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("onFailure(): ");
    stringBuilder.append(paramCharSequence);
    Log.w("FillCallback", stringBuilder.toString());
    assertNotCalled();
    this.mCalled = true;
    try {
      this.mCallback.onFailure(this.mRequestId, paramCharSequence);
    } catch (RemoteException remoteException) {
      remoteException.rethrowAsRuntimeException();
    } 
  }
  
  private void assertNotCalled() {
    if (!this.mCalled)
      return; 
    throw new IllegalStateException("Already called");
  }
}
