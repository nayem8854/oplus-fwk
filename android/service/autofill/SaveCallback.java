package android.service.autofill;

import android.content.IntentSender;
import android.os.RemoteException;
import android.util.Log;
import com.android.internal.util.Preconditions;

public final class SaveCallback {
  private static final String TAG = "SaveCallback";
  
  private final ISaveCallback mCallback;
  
  private boolean mCalled;
  
  SaveCallback(ISaveCallback paramISaveCallback) {
    this.mCallback = paramISaveCallback;
  }
  
  public void onSuccess() {
    onSuccessInternal(null);
  }
  
  public void onSuccess(IntentSender paramIntentSender) {
    onSuccessInternal((IntentSender)Preconditions.checkNotNull(paramIntentSender));
  }
  
  private void onSuccessInternal(IntentSender paramIntentSender) {
    assertNotCalled();
    this.mCalled = true;
    try {
      this.mCallback.onSuccess(paramIntentSender);
    } catch (RemoteException remoteException) {
      remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public void onFailure(CharSequence paramCharSequence) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("onFailure(): ");
    stringBuilder.append(paramCharSequence);
    Log.w("SaveCallback", stringBuilder.toString());
    assertNotCalled();
    this.mCalled = true;
    try {
      this.mCallback.onFailure(paramCharSequence);
    } catch (RemoteException remoteException) {
      remoteException.rethrowAsRuntimeException();
    } 
  }
  
  private void assertNotCalled() {
    if (!this.mCalled)
      return; 
    throw new IllegalStateException("Already called");
  }
}
