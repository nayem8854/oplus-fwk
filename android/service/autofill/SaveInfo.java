package android.service.autofill;

import android.content.IntentSender;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.DebugUtils;
import android.view.autofill.AutofillId;
import android.view.autofill.Helper;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;

public final class SaveInfo implements Parcelable {
  private SaveInfo(Builder paramBuilder) {
    this.mType = paramBuilder.mType;
    this.mNegativeButtonStyle = paramBuilder.mNegativeButtonStyle;
    this.mNegativeActionListener = paramBuilder.mNegativeActionListener;
    this.mPositiveButtonStyle = paramBuilder.mPositiveButtonStyle;
    this.mRequiredIds = paramBuilder.mRequiredIds;
    this.mOptionalIds = paramBuilder.mOptionalIds;
    this.mDescription = paramBuilder.mDescription;
    this.mFlags = paramBuilder.mFlags;
    this.mCustomDescription = paramBuilder.mCustomDescription;
    this.mValidator = paramBuilder.mValidator;
    if (paramBuilder.mSanitizers == null) {
      this.mSanitizerKeys = null;
      this.mSanitizerValues = null;
    } else {
      int i = paramBuilder.mSanitizers.size();
      this.mSanitizerKeys = new InternalSanitizer[i];
      this.mSanitizerValues = new AutofillId[i][];
      for (byte b = 0; b < i; b++) {
        this.mSanitizerKeys[b] = (InternalSanitizer)paramBuilder.mSanitizers.keyAt(b);
        this.mSanitizerValues[b] = (AutofillId[])paramBuilder.mSanitizers.valueAt(b);
      } 
    } 
    this.mTriggerId = paramBuilder.mTriggerId;
  }
  
  public int getNegativeActionStyle() {
    return this.mNegativeButtonStyle;
  }
  
  public IntentSender getNegativeActionListener() {
    return this.mNegativeActionListener;
  }
  
  public int getPositiveActionStyle() {
    return this.mPositiveButtonStyle;
  }
  
  public AutofillId[] getRequiredIds() {
    return this.mRequiredIds;
  }
  
  public AutofillId[] getOptionalIds() {
    return this.mOptionalIds;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public int getFlags() {
    return this.mFlags;
  }
  
  public CharSequence getDescription() {
    return this.mDescription;
  }
  
  public CustomDescription getCustomDescription() {
    return this.mCustomDescription;
  }
  
  public InternalValidator getValidator() {
    return this.mValidator;
  }
  
  public InternalSanitizer[] getSanitizerKeys() {
    return this.mSanitizerKeys;
  }
  
  public AutofillId[][] getSanitizerValues() {
    return this.mSanitizerValues;
  }
  
  public AutofillId getTriggerId() {
    return this.mTriggerId;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class SaveInfoFlags implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class SaveDataType implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class PositiveButtonStyle implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class NegativeButtonStyle implements Annotation {}
  
  class Builder {
    private CustomDescription mCustomDescription;
    
    private CharSequence mDescription;
    
    private boolean mDestroyed;
    
    private int mFlags;
    
    private IntentSender mNegativeActionListener;
    
    private int mNegativeButtonStyle = 0;
    
    private AutofillId[] mOptionalIds;
    
    private int mPositiveButtonStyle = 0;
    
    private final AutofillId[] mRequiredIds;
    
    private ArraySet<AutofillId> mSanitizerIds;
    
    private ArrayMap<InternalSanitizer, AutofillId[]> mSanitizers;
    
    private AutofillId mTriggerId;
    
    private final int mType;
    
    private InternalValidator mValidator;
    
    public Builder(SaveInfo this$0, AutofillId[] param1ArrayOfAutofillId) {
      this.mType = this$0;
      this.mRequiredIds = AutofillServiceHelper.assertValid(param1ArrayOfAutofillId);
    }
    
    public Builder(SaveInfo this$0) {
      this.mType = this$0;
      this.mRequiredIds = null;
    }
    
    public Builder setFlags(int param1Int) {
      throwIfDestroyed();
      this.mFlags = Preconditions.checkFlagsArgument(param1Int, 7);
      return this;
    }
    
    public Builder setOptionalIds(AutofillId[] param1ArrayOfAutofillId) {
      throwIfDestroyed();
      this.mOptionalIds = AutofillServiceHelper.assertValid(param1ArrayOfAutofillId);
      return this;
    }
    
    public Builder setDescription(CharSequence param1CharSequence) {
      boolean bool;
      throwIfDestroyed();
      if (this.mCustomDescription == null) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkState(bool, "Can call setDescription() or setCustomDescription(), but not both");
      this.mDescription = param1CharSequence;
      return this;
    }
    
    public Builder setCustomDescription(CustomDescription param1CustomDescription) {
      boolean bool;
      throwIfDestroyed();
      if (this.mDescription == null) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkState(bool, "Can call setDescription() or setCustomDescription(), but not both");
      this.mCustomDescription = param1CustomDescription;
      return this;
    }
    
    public Builder setNegativeAction(int param1Int, IntentSender param1IntentSender) {
      throwIfDestroyed();
      Preconditions.checkArgumentInRange(param1Int, 0, 2, "style");
      this.mNegativeButtonStyle = param1Int;
      this.mNegativeActionListener = param1IntentSender;
      return this;
    }
    
    public Builder setPositiveAction(int param1Int) {
      throwIfDestroyed();
      Preconditions.checkArgumentInRange(param1Int, 0, 1, "style");
      this.mPositiveButtonStyle = param1Int;
      return this;
    }
    
    public Builder setValidator(Validator param1Validator) {
      throwIfDestroyed();
      boolean bool = param1Validator instanceof InternalValidator;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("not provided by Android System: ");
      stringBuilder.append(param1Validator);
      Preconditions.checkArgument(bool, stringBuilder.toString());
      this.mValidator = (InternalValidator)param1Validator;
      return this;
    }
    
    public Builder addSanitizer(Sanitizer param1Sanitizer, AutofillId... param1VarArgs) {
      throwIfDestroyed();
      Preconditions.checkArgument(ArrayUtils.isEmpty((Object[])param1VarArgs) ^ true, "ids cannot be empty or null");
      boolean bool = param1Sanitizer instanceof InternalSanitizer;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("not provided by Android System: ");
      stringBuilder.append(param1Sanitizer);
      Preconditions.checkArgument(bool, stringBuilder.toString());
      if (this.mSanitizers == null) {
        this.mSanitizers = new ArrayMap();
        this.mSanitizerIds = new ArraySet(param1VarArgs.length);
      } 
      int i;
      byte b;
      for (i = param1VarArgs.length, b = 0; b < i; ) {
        AutofillId autofillId = param1VarArgs[b];
        Preconditions.checkArgument(this.mSanitizerIds.contains(autofillId) ^ true, "already added %s", new Object[] { autofillId });
        this.mSanitizerIds.add(autofillId);
        b++;
      } 
      this.mSanitizers.put(param1Sanitizer, param1VarArgs);
      return this;
    }
    
    public Builder setTriggerId(AutofillId param1AutofillId) {
      throwIfDestroyed();
      this.mTriggerId = (AutofillId)Preconditions.checkNotNull(param1AutofillId);
      return this;
    }
    
    public SaveInfo build() {
      throwIfDestroyed();
      AutofillId[] arrayOfAutofillId = this.mRequiredIds;
      if (!ArrayUtils.isEmpty((Object[])arrayOfAutofillId) || !ArrayUtils.isEmpty((Object[])this.mOptionalIds) || (this.mFlags & 0x4) != 0) {
        boolean bool1 = true;
        Preconditions.checkState(bool1, "must have at least one required or optional id or FLAG_DELAYED_SAVE");
        this.mDestroyed = true;
        return new SaveInfo(this);
      } 
      boolean bool = false;
      Preconditions.checkState(bool, "must have at least one required or optional id or FLAG_DELAYED_SAVE");
      this.mDestroyed = true;
      return new SaveInfo(this);
    }
    
    private void throwIfDestroyed() {
      if (!this.mDestroyed)
        return; 
      throw new IllegalStateException("Already called #build()");
    }
  }
  
  public String toString() {
    if (!Helper.sDebug)
      return super.toString(); 
    StringBuilder stringBuilder = new StringBuilder("SaveInfo: [type=");
    int i = this.mType;
    stringBuilder.append(DebugUtils.flagsToString(SaveInfo.class, "SAVE_DATA_TYPE_", i));
    stringBuilder.append(", requiredIds=");
    stringBuilder.append(Arrays.toString((Object[])this.mRequiredIds));
    stringBuilder.append(", negative style=");
    stringBuilder.append(DebugUtils.flagsToString(SaveInfo.class, "NEGATIVE_BUTTON_STYLE_", this.mNegativeButtonStyle));
    stringBuilder.append(", positive style=");
    stringBuilder = stringBuilder.append(DebugUtils.flagsToString(SaveInfo.class, "POSITIVE_BUTTON_STYLE_", this.mPositiveButtonStyle));
    if (this.mOptionalIds != null) {
      stringBuilder.append(", optionalIds=");
      stringBuilder.append(Arrays.toString((Object[])this.mOptionalIds));
    } 
    if (this.mDescription != null) {
      stringBuilder.append(", description=");
      stringBuilder.append(this.mDescription);
    } 
    if (this.mFlags != 0) {
      stringBuilder.append(", flags=");
      stringBuilder.append(this.mFlags);
    } 
    if (this.mCustomDescription != null) {
      stringBuilder.append(", customDescription=");
      stringBuilder.append(this.mCustomDescription);
    } 
    if (this.mValidator != null) {
      stringBuilder.append(", validator=");
      stringBuilder.append(this.mValidator);
    } 
    if (this.mSanitizerKeys != null) {
      stringBuilder.append(", sanitizerKeys=");
      stringBuilder.append(this.mSanitizerKeys.length);
    } 
    if (this.mSanitizerValues != null) {
      stringBuilder.append(", sanitizerValues=");
      stringBuilder.append(this.mSanitizerValues.length);
    } 
    if (this.mTriggerId != null) {
      stringBuilder.append(", triggerId=");
      stringBuilder.append(this.mTriggerId);
    } 
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mType);
    paramParcel.writeParcelableArray(this.mRequiredIds, paramInt);
    paramParcel.writeParcelableArray(this.mOptionalIds, paramInt);
    paramParcel.writeInt(this.mNegativeButtonStyle);
    paramParcel.writeParcelable((Parcelable)this.mNegativeActionListener, paramInt);
    paramParcel.writeInt(this.mPositiveButtonStyle);
    paramParcel.writeCharSequence(this.mDescription);
    paramParcel.writeParcelable(this.mCustomDescription, paramInt);
    paramParcel.writeParcelable(this.mValidator, paramInt);
    paramParcel.writeParcelableArray(this.mSanitizerKeys, paramInt);
    if (this.mSanitizerKeys != null) {
      byte b = 0;
      while (true) {
        AutofillId[][] arrayOfAutofillId = this.mSanitizerValues;
        if (b < arrayOfAutofillId.length) {
          paramParcel.writeParcelableArray(arrayOfAutofillId[b], paramInt);
          b++;
          continue;
        } 
        break;
      } 
    } 
    paramParcel.writeParcelable((Parcelable)this.mTriggerId, paramInt);
    paramParcel.writeInt(this.mFlags);
  }
  
  public static final Parcelable.Creator<SaveInfo> CREATOR = new Parcelable.Creator<SaveInfo>() {
      public SaveInfo createFromParcel(Parcel param1Parcel) {
        SaveInfo.Builder builder;
        int i = param1Parcel.readInt();
        AutofillId[] arrayOfAutofillId1 = param1Parcel.<AutofillId>readParcelableArray(null, AutofillId.class);
        if (arrayOfAutofillId1 != null) {
          builder = new SaveInfo.Builder(i, arrayOfAutofillId1);
        } else {
          builder = new SaveInfo.Builder(i);
        } 
        AutofillId[] arrayOfAutofillId2 = param1Parcel.<AutofillId>readParcelableArray(null, AutofillId.class);
        if (arrayOfAutofillId2 != null)
          builder.setOptionalIds(arrayOfAutofillId2); 
        builder.setNegativeAction(param1Parcel.readInt(), param1Parcel.<IntentSender>readParcelable(null));
        builder.setPositiveAction(param1Parcel.readInt());
        builder.setDescription(param1Parcel.readCharSequence());
        CustomDescription customDescription = param1Parcel.<CustomDescription>readParcelable(null);
        if (customDescription != null)
          builder.setCustomDescription(customDescription); 
        InternalValidator internalValidator = param1Parcel.<InternalValidator>readParcelable(null);
        if (internalValidator != null)
          builder.setValidator(internalValidator); 
        InternalSanitizer[] arrayOfInternalSanitizer = param1Parcel.<InternalSanitizer>readParcelableArray(null, InternalSanitizer.class);
        if (arrayOfInternalSanitizer != null) {
          int j = arrayOfInternalSanitizer.length;
          for (i = 0; i < j; i++) {
            AutofillId[] arrayOfAutofillId = param1Parcel.<AutofillId>readParcelableArray(null, AutofillId.class);
            builder.addSanitizer(arrayOfInternalSanitizer[i], arrayOfAutofillId);
          } 
        } 
        AutofillId autofillId = param1Parcel.<AutofillId>readParcelable(null);
        if (autofillId != null)
          builder.setTriggerId(autofillId); 
        builder.setFlags(param1Parcel.readInt());
        return builder.build();
      }
      
      public SaveInfo[] newArray(int param1Int) {
        return new SaveInfo[param1Int];
      }
    };
  
  public static final int FLAG_DELAY_SAVE = 4;
  
  public static final int FLAG_DONT_SAVE_ON_FINISH = 2;
  
  public static final int FLAG_SAVE_ON_ALL_VIEWS_INVISIBLE = 1;
  
  public static final int NEGATIVE_BUTTON_STYLE_CANCEL = 0;
  
  public static final int NEGATIVE_BUTTON_STYLE_NEVER = 2;
  
  public static final int NEGATIVE_BUTTON_STYLE_REJECT = 1;
  
  public static final int POSITIVE_BUTTON_STYLE_CONTINUE = 1;
  
  public static final int POSITIVE_BUTTON_STYLE_SAVE = 0;
  
  public static final int SAVE_DATA_TYPE_ADDRESS = 2;
  
  public static final int SAVE_DATA_TYPE_CREDIT_CARD = 4;
  
  public static final int SAVE_DATA_TYPE_DEBIT_CARD = 32;
  
  public static final int SAVE_DATA_TYPE_EMAIL_ADDRESS = 16;
  
  public static final int SAVE_DATA_TYPE_GENERIC = 0;
  
  public static final int SAVE_DATA_TYPE_GENERIC_CARD = 128;
  
  public static final int SAVE_DATA_TYPE_PASSWORD = 1;
  
  public static final int SAVE_DATA_TYPE_PAYMENT_CARD = 64;
  
  public static final int SAVE_DATA_TYPE_USERNAME = 8;
  
  private final CustomDescription mCustomDescription;
  
  private final CharSequence mDescription;
  
  private final int mFlags;
  
  private final IntentSender mNegativeActionListener;
  
  private final int mNegativeButtonStyle;
  
  private final AutofillId[] mOptionalIds;
  
  private final int mPositiveButtonStyle;
  
  private final AutofillId[] mRequiredIds;
  
  private final InternalSanitizer[] mSanitizerKeys;
  
  private final AutofillId[][] mSanitizerValues;
  
  private final AutofillId mTriggerId;
  
  private final int mType;
  
  private final InternalValidator mValidator;
}
