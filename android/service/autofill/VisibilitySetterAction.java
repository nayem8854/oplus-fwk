package android.service.autofill;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Slog;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.autofill.Helper;
import com.android.internal.util.Preconditions;

public final class VisibilitySetterAction extends InternalOnClickAction implements OnClickAction, Parcelable {
  private VisibilitySetterAction(Builder paramBuilder) {
    this.mVisibilities = paramBuilder.mVisibilities;
  }
  
  public void onClick(ViewGroup paramViewGroup) {
    for (byte b = 0; b < this.mVisibilities.size(); b++) {
      StringBuilder stringBuilder;
      int i = this.mVisibilities.keyAt(b);
      View view = paramViewGroup.findViewById(i);
      if (view == null) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Skipping view id ");
        stringBuilder.append(i);
        stringBuilder.append(" because it's not found on ");
        stringBuilder.append(paramViewGroup);
        Slog.w("VisibilitySetterAction", stringBuilder.toString());
      } else {
        i = this.mVisibilities.valueAt(b);
        if (Helper.sVerbose) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Changing visibility of view ");
          stringBuilder1.append(stringBuilder);
          stringBuilder1.append(" from ");
          stringBuilder1.append(stringBuilder.getVisibility());
          stringBuilder1.append(" to  ");
          stringBuilder1.append(i);
          String str = stringBuilder1.toString();
          Slog.v("VisibilitySetterAction", str);
        } 
        stringBuilder.setVisibility(i);
      } 
    } 
  }
  
  class Builder {
    private boolean mDestroyed;
    
    private final SparseIntArray mVisibilities = new SparseIntArray();
    
    public Builder(VisibilitySetterAction this$0, int param1Int1) {
      setVisibility(this$0, param1Int1);
    }
    
    public Builder setVisibility(int param1Int1, int param1Int2) {
      throwIfDestroyed();
      if (param1Int2 == 0 || param1Int2 == 4 || param1Int2 == 8) {
        this.mVisibilities.put(param1Int1, param1Int2);
        return this;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid visibility: ");
      stringBuilder.append(param1Int2);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public VisibilitySetterAction build() {
      throwIfDestroyed();
      this.mDestroyed = true;
      return new VisibilitySetterAction(this);
    }
    
    private void throwIfDestroyed() {
      Preconditions.checkState(this.mDestroyed ^ true, "Already called build()");
    }
  }
  
  public String toString() {
    if (!Helper.sDebug)
      return super.toString(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("VisibilitySetterAction: [");
    stringBuilder.append(this.mVisibilities);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeSparseIntArray(this.mVisibilities);
  }
  
  public static final Parcelable.Creator<VisibilitySetterAction> CREATOR = (Parcelable.Creator<VisibilitySetterAction>)new Object();
  
  private static final String TAG = "VisibilitySetterAction";
  
  private final SparseIntArray mVisibilities;
}
