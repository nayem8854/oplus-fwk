package android.service.autofill;

import android.os.Binder;
import android.os.IBinder;
import android.os.ICancellationSignal;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.text.TextUtils;

public interface IFillCallback extends IInterface {
  void onCancellable(ICancellationSignal paramICancellationSignal) throws RemoteException;
  
  void onFailure(int paramInt, CharSequence paramCharSequence) throws RemoteException;
  
  void onSuccess(FillResponse paramFillResponse) throws RemoteException;
  
  class Default implements IFillCallback {
    public void onCancellable(ICancellationSignal param1ICancellationSignal) throws RemoteException {}
    
    public void onSuccess(FillResponse param1FillResponse) throws RemoteException {}
    
    public void onFailure(int param1Int, CharSequence param1CharSequence) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IFillCallback {
    private static final String DESCRIPTOR = "android.service.autofill.IFillCallback";
    
    static final int TRANSACTION_onCancellable = 1;
    
    static final int TRANSACTION_onFailure = 3;
    
    static final int TRANSACTION_onSuccess = 2;
    
    public Stub() {
      attachInterface(this, "android.service.autofill.IFillCallback");
    }
    
    public static IFillCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.autofill.IFillCallback");
      if (iInterface != null && iInterface instanceof IFillCallback)
        return (IFillCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onFailure";
        } 
        return "onSuccess";
      } 
      return "onCancellable";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.service.autofill.IFillCallback");
            return true;
          } 
          param1Parcel1.enforceInterface("android.service.autofill.IFillCallback");
          param1Int1 = param1Parcel1.readInt();
          if (param1Parcel1.readInt() != 0) {
            CharSequence charSequence = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          onFailure(param1Int1, (CharSequence)param1Parcel1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.service.autofill.IFillCallback");
        if (param1Parcel1.readInt() != 0) {
          FillResponse fillResponse = FillResponse.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onSuccess((FillResponse)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.autofill.IFillCallback");
      ICancellationSignal iCancellationSignal = ICancellationSignal.Stub.asInterface(param1Parcel1.readStrongBinder());
      onCancellable(iCancellationSignal);
      return true;
    }
    
    private static class Proxy implements IFillCallback {
      public static IFillCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.autofill.IFillCallback";
      }
      
      public void onCancellable(ICancellationSignal param2ICancellationSignal) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.autofill.IFillCallback");
          if (param2ICancellationSignal != null) {
            iBinder = param2ICancellationSignal.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IFillCallback.Stub.getDefaultImpl() != null) {
            IFillCallback.Stub.getDefaultImpl().onCancellable(param2ICancellationSignal);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSuccess(FillResponse param2FillResponse) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.autofill.IFillCallback");
          if (param2FillResponse != null) {
            parcel.writeInt(1);
            param2FillResponse.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IFillCallback.Stub.getDefaultImpl() != null) {
            IFillCallback.Stub.getDefaultImpl().onSuccess(param2FillResponse);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onFailure(int param2Int, CharSequence param2CharSequence) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.autofill.IFillCallback");
          parcel.writeInt(param2Int);
          if (param2CharSequence != null) {
            parcel.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IFillCallback.Stub.getDefaultImpl() != null) {
            IFillCallback.Stub.getDefaultImpl().onFailure(param2Int, param2CharSequence);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IFillCallback param1IFillCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IFillCallback != null) {
          Proxy.sDefaultImpl = param1IFillCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IFillCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
