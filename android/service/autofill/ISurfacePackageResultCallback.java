package android.service.autofill;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.SurfaceControlViewHost;

public interface ISurfacePackageResultCallback extends IInterface {
  void onResult(SurfaceControlViewHost.SurfacePackage paramSurfacePackage) throws RemoteException;
  
  class Default implements ISurfacePackageResultCallback {
    public void onResult(SurfaceControlViewHost.SurfacePackage param1SurfacePackage) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISurfacePackageResultCallback {
    private static final String DESCRIPTOR = "android.service.autofill.ISurfacePackageResultCallback";
    
    static final int TRANSACTION_onResult = 1;
    
    public Stub() {
      attachInterface(this, "android.service.autofill.ISurfacePackageResultCallback");
    }
    
    public static ISurfacePackageResultCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.autofill.ISurfacePackageResultCallback");
      if (iInterface != null && iInterface instanceof ISurfacePackageResultCallback)
        return (ISurfacePackageResultCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onResult";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.service.autofill.ISurfacePackageResultCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.autofill.ISurfacePackageResultCallback");
      if (param1Parcel1.readInt() != 0) {
        SurfaceControlViewHost.SurfacePackage surfacePackage = SurfaceControlViewHost.SurfacePackage.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onResult((SurfaceControlViewHost.SurfacePackage)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements ISurfacePackageResultCallback {
      public static ISurfacePackageResultCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.autofill.ISurfacePackageResultCallback";
      }
      
      public void onResult(SurfaceControlViewHost.SurfacePackage param2SurfacePackage) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.autofill.ISurfacePackageResultCallback");
          if (param2SurfacePackage != null) {
            parcel.writeInt(1);
            param2SurfacePackage.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ISurfacePackageResultCallback.Stub.getDefaultImpl() != null) {
            ISurfacePackageResultCallback.Stub.getDefaultImpl().onResult(param2SurfacePackage);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISurfacePackageResultCallback param1ISurfacePackageResultCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISurfacePackageResultCallback != null) {
          Proxy.sDefaultImpl = param1ISurfacePackageResultCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISurfacePackageResultCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
