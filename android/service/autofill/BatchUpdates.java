package android.service.autofill;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pair;
import android.view.autofill.Helper;
import android.widget.RemoteViews;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;

public final class BatchUpdates implements Parcelable {
  private BatchUpdates(Builder paramBuilder) {
    this.mTransformations = paramBuilder.mTransformations;
    this.mUpdates = paramBuilder.mUpdates;
  }
  
  public ArrayList<Pair<Integer, InternalTransformation>> getTransformations() {
    return this.mTransformations;
  }
  
  public RemoteViews getUpdates() {
    return this.mUpdates;
  }
  
  class Builder {
    private boolean mDestroyed;
    
    private ArrayList<Pair<Integer, InternalTransformation>> mTransformations;
    
    private RemoteViews mUpdates;
    
    public Builder updateTemplate(RemoteViews param1RemoteViews) {
      throwIfDestroyed();
      this.mUpdates = (RemoteViews)Preconditions.checkNotNull(param1RemoteViews);
      return this;
    }
    
    public Builder transformChild(int param1Int, Transformation param1Transformation) {
      throwIfDestroyed();
      boolean bool = param1Transformation instanceof InternalTransformation;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("not provided by Android System: ");
      stringBuilder.append(param1Transformation);
      Preconditions.checkArgument(bool, stringBuilder.toString());
      if (this.mTransformations == null)
        this.mTransformations = new ArrayList<>(); 
      this.mTransformations.add(new Pair(Integer.valueOf(param1Int), param1Transformation));
      return this;
    }
    
    public BatchUpdates build() {
      throwIfDestroyed();
      if (this.mUpdates != null || this.mTransformations != null) {
        boolean bool1 = true;
        Preconditions.checkState(bool1, "must call either updateTemplate() or transformChild() at least once");
        this.mDestroyed = true;
        return new BatchUpdates(this);
      } 
      boolean bool = false;
      Preconditions.checkState(bool, "must call either updateTemplate() or transformChild() at least once");
      this.mDestroyed = true;
      return new BatchUpdates(this);
    }
    
    private void throwIfDestroyed() {
      if (!this.mDestroyed)
        return; 
      throw new IllegalStateException("Already called #build()");
    }
  }
  
  public String toString() {
    String str;
    Integer integer;
    if (!Helper.sDebug)
      return super.toString(); 
    StringBuilder stringBuilder = new StringBuilder("BatchUpdates: [");
    stringBuilder.append(", transformations=");
    ArrayList<Pair<Integer, InternalTransformation>> arrayList = this.mTransformations;
    if (arrayList == null) {
      str = "N/A";
    } else {
      integer = Integer.valueOf(str.size());
    } 
    stringBuilder.append(integer);
    stringBuilder.append(", updates=");
    stringBuilder.append(this.mUpdates);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    ArrayList<Pair<Integer, InternalTransformation>> arrayList = this.mTransformations;
    if (arrayList == null) {
      paramParcel.writeIntArray(null);
    } else {
      int i = arrayList.size();
      int[] arrayOfInt = new int[i];
      InternalTransformation[] arrayOfInternalTransformation = new InternalTransformation[i];
      for (byte b = 0; b < i; b++) {
        Pair pair = this.mTransformations.get(b);
        arrayOfInt[b] = ((Integer)pair.first).intValue();
        arrayOfInternalTransformation[b] = (InternalTransformation)pair.second;
      } 
      paramParcel.writeIntArray(arrayOfInt);
      paramParcel.writeParcelableArray(arrayOfInternalTransformation, paramInt);
    } 
    paramParcel.writeParcelable((Parcelable)this.mUpdates, paramInt);
  }
  
  public static final Parcelable.Creator<BatchUpdates> CREATOR = new Parcelable.Creator<BatchUpdates>() {
      public BatchUpdates createFromParcel(Parcel param1Parcel) {
        BatchUpdates.Builder builder = new BatchUpdates.Builder();
        int[] arrayOfInt = param1Parcel.createIntArray();
        if (arrayOfInt != null) {
          InternalTransformation[] arrayOfInternalTransformation = param1Parcel.<InternalTransformation>readParcelableArray(null, InternalTransformation.class);
          int i = arrayOfInt.length;
          for (byte b = 0; b < i; b++)
            builder.transformChild(arrayOfInt[b], arrayOfInternalTransformation[b]); 
        } 
        RemoteViews remoteViews = param1Parcel.<RemoteViews>readParcelable(null);
        if (remoteViews != null)
          builder.updateTemplate(remoteViews); 
        return builder.build();
      }
      
      public BatchUpdates[] newArray(int param1Int) {
        return new BatchUpdates[param1Int];
      }
    };
  
  private final ArrayList<Pair<Integer, InternalTransformation>> mTransformations;
  
  private final RemoteViews mUpdates;
}
