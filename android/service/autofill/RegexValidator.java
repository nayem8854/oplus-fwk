package android.service.autofill;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.view.autofill.AutofillId;
import android.view.autofill.Helper;
import com.android.internal.util.Preconditions;
import java.util.regex.Pattern;

public final class RegexValidator extends InternalValidator implements Validator, Parcelable {
  public RegexValidator(AutofillId paramAutofillId, Pattern paramPattern) {
    this.mId = (AutofillId)Preconditions.checkNotNull(paramAutofillId);
    this.mRegex = (Pattern)Preconditions.checkNotNull(paramPattern);
  }
  
  public boolean isValid(ValueFinder paramValueFinder) {
    StringBuilder stringBuilder;
    String str = paramValueFinder.findByAutofillId(this.mId);
    if (str == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("No view for id ");
      stringBuilder.append(this.mId);
      Log.w("RegexValidator", stringBuilder.toString());
      return false;
    } 
    boolean bool = this.mRegex.matcher(stringBuilder).matches();
    if (Helper.sDebug) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("isValid(): ");
      stringBuilder.append(bool);
      Log.d("RegexValidator", stringBuilder.toString());
    } 
    return bool;
  }
  
  public String toString() {
    if (!Helper.sDebug)
      return super.toString(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("RegexValidator: [id=");
    stringBuilder.append(this.mId);
    stringBuilder.append(", regex=");
    stringBuilder.append(this.mRegex);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.mId, paramInt);
    paramParcel.writeSerializable(this.mRegex);
  }
  
  public static final Parcelable.Creator<RegexValidator> CREATOR = (Parcelable.Creator<RegexValidator>)new Object();
  
  private static final String TAG = "RegexValidator";
  
  private final AutofillId mId;
  
  private final Pattern mRegex;
}
