package android.service.autofill;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteCallback;
import android.os.RemoteException;
import android.view.autofill.AutofillValue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface IAutofillFieldClassificationService extends IInterface {
  void calculateScores(RemoteCallback paramRemoteCallback, List<AutofillValue> paramList, String[] paramArrayOfString1, String[] paramArrayOfString2, String paramString, Bundle paramBundle, Map paramMap1, Map paramMap2) throws RemoteException;
  
  class Default implements IAutofillFieldClassificationService {
    public void calculateScores(RemoteCallback param1RemoteCallback, List<AutofillValue> param1List, String[] param1ArrayOfString1, String[] param1ArrayOfString2, String param1String, Bundle param1Bundle, Map param1Map1, Map param1Map2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAutofillFieldClassificationService {
    private static final String DESCRIPTOR = "android.service.autofill.IAutofillFieldClassificationService";
    
    static final int TRANSACTION_calculateScores = 1;
    
    public Stub() {
      attachInterface(this, "android.service.autofill.IAutofillFieldClassificationService");
    }
    
    public static IAutofillFieldClassificationService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.autofill.IAutofillFieldClassificationService");
      if (iInterface != null && iInterface instanceof IAutofillFieldClassificationService)
        return (IAutofillFieldClassificationService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "calculateScores";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      Bundle bundle;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.service.autofill.IAutofillFieldClassificationService");
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.autofill.IAutofillFieldClassificationService");
      if (param1Parcel1.readInt() != 0) {
        RemoteCallback remoteCallback = RemoteCallback.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      ArrayList<?> arrayList = param1Parcel1.createTypedArrayList(AutofillValue.CREATOR);
      String[] arrayOfString1 = param1Parcel1.createStringArray();
      String[] arrayOfString2 = param1Parcel1.createStringArray();
      String str = param1Parcel1.readString();
      if (param1Parcel1.readInt() != 0) {
        bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
      } else {
        bundle = null;
      } 
      ClassLoader classLoader = getClass().getClassLoader();
      HashMap hashMap2 = param1Parcel1.readHashMap(classLoader);
      HashMap hashMap1 = param1Parcel1.readHashMap(classLoader);
      calculateScores((RemoteCallback)param1Parcel2, (List)arrayList, arrayOfString1, arrayOfString2, str, bundle, hashMap2, hashMap1);
      return true;
    }
    
    private static class Proxy implements IAutofillFieldClassificationService {
      public static IAutofillFieldClassificationService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.autofill.IAutofillFieldClassificationService";
      }
      
      public void calculateScores(RemoteCallback param2RemoteCallback, List<AutofillValue> param2List, String[] param2ArrayOfString1, String[] param2ArrayOfString2, String param2String, Bundle param2Bundle, Map param2Map1, Map param2Map2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.autofill.IAutofillFieldClassificationService");
          if (param2RemoteCallback != null) {
            parcel.writeInt(1);
            param2RemoteCallback.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          try {
            parcel.writeTypedList(param2List);
            try {
              parcel.writeStringArray(param2ArrayOfString1);
              try {
                parcel.writeStringArray(param2ArrayOfString2);
                parcel.writeString(param2String);
                if (param2Bundle != null) {
                  parcel.writeInt(1);
                  param2Bundle.writeToParcel(parcel, 0);
                } else {
                  parcel.writeInt(0);
                } 
                parcel.writeMap(param2Map1);
                parcel.writeMap(param2Map2);
                boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
                if (!bool && IAutofillFieldClassificationService.Stub.getDefaultImpl() != null) {
                  IAutofillFieldClassificationService.Stub.getDefaultImpl().calculateScores(param2RemoteCallback, param2List, param2ArrayOfString1, param2ArrayOfString2, param2String, param2Bundle, param2Map1, param2Map2);
                  parcel.recycle();
                  return;
                } 
                parcel.recycle();
                return;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2RemoteCallback;
      }
    }
    
    public static boolean setDefaultImpl(IAutofillFieldClassificationService param1IAutofillFieldClassificationService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAutofillFieldClassificationService != null) {
          Proxy.sDefaultImpl = param1IAutofillFieldClassificationService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAutofillFieldClassificationService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
