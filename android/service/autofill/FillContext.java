package android.service.autofill;

import android.annotation.NonNull;
import android.app.assist.AssistStructure;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.ArrayMap;
import android.util.SparseIntArray;
import android.view.autofill.AutofillId;
import android.view.autofill.Helper;
import com.android.internal.util.AnnotationValidations;
import java.util.LinkedList;

public final class FillContext implements Parcelable {
  public String toString() {
    if (!Helper.sDebug)
      return super.toString(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("FillContext [reqId=");
    stringBuilder.append(this.mRequestId);
    stringBuilder.append(", focusedId=");
    stringBuilder.append(this.mFocusedId);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public AssistStructure.ViewNode[] findViewNodesByAutofillIds(AutofillId[] paramArrayOfAutofillId) {
    LinkedList<AssistStructure.ViewNode> linkedList = new LinkedList();
    AssistStructure.ViewNode[] arrayOfViewNode = new AssistStructure.ViewNode[paramArrayOfAutofillId.length];
    SparseIntArray sparseIntArray = new SparseIntArray(paramArrayOfAutofillId.length);
    byte b;
    for (b = 0; b < paramArrayOfAutofillId.length; b++) {
      ArrayMap<AutofillId, AssistStructure.ViewNode> arrayMap = this.mViewNodeLookupTable;
      if (arrayMap != null) {
        int j = arrayMap.indexOfKey(paramArrayOfAutofillId[b]);
        if (j >= 0) {
          arrayOfViewNode[b] = (AssistStructure.ViewNode)this.mViewNodeLookupTable.valueAt(j);
        } else {
          sparseIntArray.put(b, 0);
        } 
      } else {
        sparseIntArray.put(b, 0);
      } 
    } 
    int i = this.mStructure.getWindowNodeCount();
    for (b = 0; b < i; b++)
      linkedList.add(this.mStructure.getWindowNodeAt(b).getRootViewNode()); 
    while (sparseIntArray.size() > 0 && !linkedList.isEmpty()) {
      AssistStructure.ViewNode viewNode = linkedList.removeFirst();
      for (b = 0; b < sparseIntArray.size(); b++) {
        i = sparseIntArray.keyAt(b);
        AutofillId autofillId = paramArrayOfAutofillId[i];
        if (autofillId.equals(viewNode.getAutofillId())) {
          arrayOfViewNode[i] = viewNode;
          if (this.mViewNodeLookupTable == null)
            this.mViewNodeLookupTable = new ArrayMap(paramArrayOfAutofillId.length); 
          this.mViewNodeLookupTable.put(autofillId, viewNode);
          sparseIntArray.removeAt(b);
          break;
        } 
      } 
      for (b = 0; b < viewNode.getChildCount(); b++)
        linkedList.addLast(viewNode.getChildAt(b)); 
    } 
    for (b = 0; b < sparseIntArray.size(); b++) {
      if (this.mViewNodeLookupTable == null)
        this.mViewNodeLookupTable = new ArrayMap(sparseIntArray.size()); 
      this.mViewNodeLookupTable.put(paramArrayOfAutofillId[sparseIntArray.keyAt(b)], null);
    } 
    return arrayOfViewNode;
  }
  
  public FillContext(int paramInt, AssistStructure paramAssistStructure, AutofillId paramAutofillId) {
    this.mRequestId = paramInt;
    this.mStructure = paramAssistStructure;
    AnnotationValidations.validate(NonNull.class, null, paramAssistStructure);
    this.mFocusedId = paramAutofillId;
    AnnotationValidations.validate(NonNull.class, null, paramAutofillId);
  }
  
  public int getRequestId() {
    return this.mRequestId;
  }
  
  public AssistStructure getStructure() {
    return this.mStructure;
  }
  
  public AutofillId getFocusedId() {
    return this.mFocusedId;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mRequestId);
    paramParcel.writeTypedObject(this.mStructure, paramInt);
    paramParcel.writeTypedObject(this.mFocusedId, paramInt);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<FillContext> CREATOR = new Parcelable.Creator<FillContext>() {
      public FillContext[] newArray(int param1Int) {
        return new FillContext[param1Int];
      }
      
      public FillContext createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        AssistStructure assistStructure = param1Parcel.<AssistStructure>readTypedObject(AssistStructure.CREATOR);
        AutofillId autofillId = param1Parcel.<AutofillId>readTypedObject(AutofillId.CREATOR);
        return new FillContext(i, assistStructure, autofillId);
      }
    };
  
  private final AutofillId mFocusedId;
  
  private final int mRequestId;
  
  private final AssistStructure mStructure;
  
  private transient ArrayMap<AutofillId, AssistStructure.ViewNode> mViewNodeLookupTable;
  
  @Deprecated
  private void __metadata() {}
}
