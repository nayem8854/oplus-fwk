package android.service.autofill;

import android.os.Parcelable;
import android.util.Log;
import android.util.Pair;
import android.view.autofill.Helper;
import android.widget.RemoteViews;
import java.util.ArrayList;

public abstract class InternalTransformation implements Transformation, Parcelable {
  private static final String TAG = "InternalTransformation";
  
  public static boolean batchApply(ValueFinder paramValueFinder, RemoteViews paramRemoteViews, ArrayList<Pair<Integer, InternalTransformation>> paramArrayList) {
    int i = paramArrayList.size();
    if (Helper.sDebug) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getPresentation(): applying ");
      stringBuilder.append(i);
      stringBuilder.append(" transformations");
      Log.d("InternalTransformation", stringBuilder.toString());
    } 
    for (byte b = 0; b < i; ) {
      Pair pair = paramArrayList.get(b);
      int j = ((Integer)pair.first).intValue();
      InternalTransformation internalTransformation = (InternalTransformation)pair.second;
      if (Helper.sDebug) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("#");
        stringBuilder.append(b);
        stringBuilder.append(": ");
        stringBuilder.append(internalTransformation);
        Log.d("InternalTransformation", stringBuilder.toString());
      } 
      try {
        internalTransformation.apply(paramValueFinder, paramRemoteViews, j);
        b++;
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Could not apply transformation ");
        stringBuilder.append(internalTransformation);
        stringBuilder.append(": ");
        stringBuilder.append(exception.getClass());
        String str = stringBuilder.toString();
        Log.e("InternalTransformation", str);
        return false;
      } 
    } 
    return true;
  }
  
  abstract void apply(ValueFinder paramValueFinder, RemoteViews paramRemoteViews, int paramInt) throws Exception;
}
