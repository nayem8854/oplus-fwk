package android.service.autofill;

import com.android.internal.util.Preconditions;

public final class Validators {
  private Validators() {
    throw new UnsupportedOperationException("contains static methods only");
  }
  
  public static Validator and(Validator... paramVarArgs) {
    return new RequiredValidators(getInternalValidators(paramVarArgs));
  }
  
  public static Validator or(Validator... paramVarArgs) {
    return new OptionalValidators(getInternalValidators(paramVarArgs));
  }
  
  public static Validator not(Validator paramValidator) {
    boolean bool = paramValidator instanceof InternalValidator;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("validator not provided by Android System: ");
    stringBuilder.append(paramValidator);
    Preconditions.checkArgument(bool, stringBuilder.toString());
    return new NegationValidator((InternalValidator)paramValidator);
  }
  
  private static InternalValidator[] getInternalValidators(Validator[] paramArrayOfValidator) {
    Preconditions.checkArrayElementsNotNull((Object[])paramArrayOfValidator, "validators");
    InternalValidator[] arrayOfInternalValidator = new InternalValidator[paramArrayOfValidator.length];
    for (byte b = 0; b < paramArrayOfValidator.length; b++) {
      boolean bool = paramArrayOfValidator[b] instanceof InternalValidator;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("element ");
      stringBuilder.append(b);
      stringBuilder.append(" not provided by Android System: ");
      stringBuilder.append(paramArrayOfValidator[b]);
      Preconditions.checkArgument(bool, stringBuilder.toString());
      arrayOfInternalValidator[b] = (InternalValidator)paramArrayOfValidator[b];
    } 
    return arrayOfInternalValidator;
  }
}
