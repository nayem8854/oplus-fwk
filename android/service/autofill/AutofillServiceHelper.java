package android.service.autofill;

import android.view.autofill.AutofillId;
import com.android.internal.util.Preconditions;

final class AutofillServiceHelper {
  static AutofillId[] assertValid(AutofillId[] paramArrayOfAutofillId) {
    StringBuilder stringBuilder;
    boolean bool;
    if (paramArrayOfAutofillId != null && paramArrayOfAutofillId.length > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "must have at least one id");
    for (byte b = 0; b < paramArrayOfAutofillId.length; ) {
      if (paramArrayOfAutofillId[b] != null) {
        b++;
        continue;
      } 
      stringBuilder = new StringBuilder();
      stringBuilder.append("ids[");
      stringBuilder.append(b);
      stringBuilder.append("] must not be null");
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    return (AutofillId[])stringBuilder;
  }
  
  private AutofillServiceHelper() {
    throw new UnsupportedOperationException("contains static members only");
  }
}
