package android.service.autofill;

import android.app.AppGlobals;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.metrics.LogMaker;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import com.android.internal.R;
import com.android.internal.logging.MetricsLogger;
import com.android.internal.util.XmlUtils;
import java.io.IOException;
import java.io.PrintWriter;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public final class AutofillServiceInfo {
  private static final String TAG = "AutofillServiceInfo";
  
  private static final String TAG_AUTOFILL_SERVICE = "autofill-service";
  
  private static final String TAG_COMPATIBILITY_PACKAGE = "compatibility-package";
  
  private final ArrayMap<String, Long> mCompatibilityPackages;
  
  private final boolean mInlineSuggestionsEnabled;
  
  private final ServiceInfo mServiceInfo;
  
  private final String mSettingsActivity;
  
  private static ServiceInfo getServiceInfoOrThrow(ComponentName paramComponentName, int paramInt) throws PackageManager.NameNotFoundException {
    try {
      ServiceInfo serviceInfo = AppGlobals.getPackageManager().getServiceInfo(paramComponentName, 128, paramInt);
      if (serviceInfo != null)
        return serviceInfo; 
    } catch (RemoteException remoteException) {}
    throw new PackageManager.NameNotFoundException(paramComponentName.toString());
  }
  
  public AutofillServiceInfo(Context paramContext, ComponentName paramComponentName, int paramInt) throws PackageManager.NameNotFoundException {
    this(paramContext, getServiceInfoOrThrow(paramComponentName, paramInt));
  }
  
  public AutofillServiceInfo(Context paramContext, ServiceInfo paramServiceInfo) {
    StringBuilder stringBuilder1;
    TypedArray typedArray1;
    if (!"android.permission.BIND_AUTOFILL_SERVICE".equals(paramServiceInfo.permission))
      if ("android.permission.BIND_AUTOFILL".equals(paramServiceInfo.permission)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("AutofillService from '");
        stringBuilder.append(paramServiceInfo.packageName);
        stringBuilder.append("' uses unsupported permission ");
        stringBuilder.append("android.permission.BIND_AUTOFILL");
        stringBuilder.append(". It works for now, but might not be supported on future releases");
        Log.w("AutofillServiceInfo", stringBuilder.toString());
        MetricsLogger metricsLogger = new MetricsLogger();
        LogMaker logMaker1 = new LogMaker(1289);
        String str = paramServiceInfo.packageName;
        LogMaker logMaker2 = logMaker1.setPackageName(str);
        metricsLogger.write(logMaker2);
      } else {
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("AutofillService from '");
        stringBuilder1.append(paramServiceInfo.packageName);
        stringBuilder1.append("' does not require permission ");
        stringBuilder1.append("android.permission.BIND_AUTOFILL_SERVICE");
        Log.w("AutofillServiceInfo", stringBuilder1.toString());
        throw new SecurityException("Service does not require permission android.permission.BIND_AUTOFILL_SERVICE");
      }  
    this.mServiceInfo = paramServiceInfo;
    XmlResourceParser xmlResourceParser = paramServiceInfo.loadXmlMetaData(stringBuilder1.getPackageManager(), "android.autofill");
    if (xmlResourceParser == null) {
      this.mSettingsActivity = null;
      this.mCompatibilityPackages = null;
      this.mInlineSuggestionsEnabled = false;
      return;
    } 
    TypedArray typedArray3 = null, typedArray4 = null;
    StringBuilder stringBuilder2 = null;
    PackageManager.NameNotFoundException nameNotFoundException2 = null;
    StringBuilder stringBuilder3 = null;
    boolean bool1 = false;
    boolean bool = false;
    TypedArray typedArray2 = typedArray4;
    boolean bool2 = bool1;
    try {
      Resources resources = stringBuilder1.getPackageManager().getResourcesForApplication(paramServiceInfo.applicationInfo);
      int i = 0;
      while (i != 1 && i != 2) {
        typedArray2 = typedArray4;
        bool2 = bool1;
        i = xmlResourceParser.next();
      } 
      typedArray2 = typedArray4;
      bool2 = bool1;
      if ("autofill-service".equals(xmlResourceParser.getName())) {
        ArrayMap<String, Long> arrayMap;
        typedArray2 = typedArray4;
        bool2 = bool1;
        null = Xml.asAttributeSet((XmlPullParser)xmlResourceParser);
        typedArray3 = null;
        stringBuilder1 = stringBuilder2;
        try {
          typedArray4 = resources.obtainAttributes(null, R.styleable.AutofillService);
          stringBuilder1 = stringBuilder2;
          typedArray3 = typedArray4;
          String str2 = typedArray4.getString(0);
          String str1 = str2;
          typedArray3 = typedArray4;
          bool1 = bool2 = typedArray4.getBoolean(1, false);
          if (typedArray4 != null) {
            String str = str2;
            bool2 = bool1;
            typedArray4.recycle();
          } 
        } finally {
          if (typedArray3 != null) {
            ArrayMap<String, Long> arrayMap2 = arrayMap;
            bool2 = bool1;
            typedArray3.recycle();
          } 
          ArrayMap<String, Long> arrayMap1 = arrayMap;
          bool2 = bool1;
        } 
      } else {
        typedArray2 = typedArray4;
        bool2 = bool1;
        Log.e("AutofillServiceInfo", "Meta-data does not start with autofill-service tag");
        bool1 = bool;
        stringBuilder1 = stringBuilder3;
        typedArray1 = typedArray3;
      } 
      bool2 = bool1;
    } catch (android.content.pm.PackageManager.NameNotFoundException|IOException|XmlPullParserException nameNotFoundException1) {
      Log.e("AutofillServiceInfo", "Error parsing auto fill service meta-data", (Throwable)nameNotFoundException1);
      nameNotFoundException1 = nameNotFoundException2;
      typedArray1 = typedArray2;
    } 
    this.mSettingsActivity = (String)typedArray1;
    this.mCompatibilityPackages = (ArrayMap<String, Long>)nameNotFoundException1;
    this.mInlineSuggestionsEnabled = bool2;
  }
  
  private ArrayMap<String, Long> parseCompatibilityPackages(XmlPullParser paramXmlPullParser, Resources paramResources) throws IOException, XmlPullParserException {
    ArrayMap<String, Long> arrayMap;
    int i = paramXmlPullParser.getDepth();
    AttributeSet attributeSet = null;
    while (true) {
      int j = paramXmlPullParser.next();
      if (j != 1 && (j != 3 || 
        paramXmlPullParser.getDepth() > i)) {
        if (j == 3 || j == 4)
          continue; 
        if ("compatibility-package".equals(paramXmlPullParser.getName())) {
          TypedArray typedArray1 = null, typedArray2 = null;
          try {
            AttributeSet attributeSet1 = Xml.asAttributeSet(paramXmlPullParser);
            int[] arrayOfInt = R.styleable.AutofillService_CompatibilityPackage;
            typedArray1 = typedArray2;
            try {
              Long long_;
              ArrayMap<String, Long> arrayMap1;
              TypedArray typedArray = paramResources.obtainAttributes(attributeSet1, arrayOfInt);
              typedArray1 = typedArray;
              String str2 = typedArray.getString(0);
              typedArray1 = typedArray;
              boolean bool = TextUtils.isEmpty(str2);
              if (bool) {
                typedArray1 = typedArray;
                StringBuilder stringBuilder = new StringBuilder();
                typedArray1 = typedArray;
                this();
                typedArray1 = typedArray;
                stringBuilder.append("Invalid compatibility package:");
                typedArray1 = typedArray;
                stringBuilder.append(str2);
                typedArray1 = typedArray;
                Log.e("AutofillServiceInfo", stringBuilder.toString());
                XmlUtils.skipCurrentTag(paramXmlPullParser);
                if (typedArray != null)
                  typedArray.recycle(); 
                break;
              } 
              typedArray1 = typedArray;
              String str1 = typedArray.getString(1);
              if (str1 != null) {
                typedArray1 = typedArray;
                try {
                  Long long_1 = Long.valueOf(Long.parseLong(str1));
                  long_ = long_1;
                  typedArray1 = typedArray;
                  if (long_1.longValue() < 0L) {
                    typedArray1 = typedArray;
                    StringBuilder stringBuilder = new StringBuilder();
                    typedArray1 = typedArray;
                    this();
                    typedArray1 = typedArray;
                    stringBuilder.append("Invalid compatibility max version code:");
                    typedArray1 = typedArray;
                    stringBuilder.append(long_1);
                    typedArray1 = typedArray;
                    Log.e("AutofillServiceInfo", stringBuilder.toString());
                    XmlUtils.skipCurrentTag(paramXmlPullParser);
                    if (typedArray != null)
                      typedArray.recycle(); 
                    break;
                  } 
                } catch (NumberFormatException numberFormatException) {
                  typedArray1 = typedArray;
                  StringBuilder stringBuilder = new StringBuilder();
                  typedArray1 = typedArray;
                  this();
                  typedArray1 = typedArray;
                  stringBuilder.append("Invalid compatibility max version code:");
                  typedArray1 = typedArray;
                  stringBuilder.append((String)long_);
                  typedArray1 = typedArray;
                  Log.e("AutofillServiceInfo", stringBuilder.toString());
                  XmlUtils.skipCurrentTag(paramXmlPullParser);
                  if (typedArray != null)
                    typedArray.recycle(); 
                  break;
                } 
              } else {
                typedArray1 = typedArray;
                long_ = Long.valueOf(Long.MAX_VALUE);
              } 
              attributeSet1 = attributeSet;
              if (attributeSet == null) {
                typedArray1 = typedArray;
                arrayMap1 = new ArrayMap();
                typedArray1 = typedArray;
                this();
              } 
              typedArray1 = typedArray;
              arrayMap1.put(str2, long_);
              XmlUtils.skipCurrentTag(paramXmlPullParser);
              if (typedArray != null)
                typedArray.recycle(); 
              arrayMap = arrayMap1;
              continue;
            } finally {}
          } finally {}
          XmlUtils.skipCurrentTag(paramXmlPullParser);
          if (typedArray1 != null)
            typedArray1.recycle(); 
          throw paramResources;
        } 
        continue;
      } 
      break;
    } 
    return arrayMap;
  }
  
  public ServiceInfo getServiceInfo() {
    return this.mServiceInfo;
  }
  
  public String getSettingsActivity() {
    return this.mSettingsActivity;
  }
  
  public ArrayMap<String, Long> getCompatibilityPackages() {
    return this.mCompatibilityPackages;
  }
  
  public boolean isInlineSuggestionsEnabled() {
    return this.mInlineSuggestionsEnabled;
  }
  
  public String toString() {
    boolean bool;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(getClass().getSimpleName());
    stringBuilder.append("[");
    stringBuilder.append(this.mServiceInfo);
    stringBuilder.append(", settings:");
    stringBuilder.append(this.mSettingsActivity);
    stringBuilder.append(", hasCompatPckgs:");
    ArrayMap<String, Long> arrayMap = this.mCompatibilityPackages;
    if (arrayMap != null && 
      !arrayMap.isEmpty()) {
      bool = true;
    } else {
      bool = false;
    } 
    stringBuilder.append(bool);
    stringBuilder.append("]");
    stringBuilder.append(", inline suggestions enabled:");
    stringBuilder.append(this.mInlineSuggestionsEnabled);
    return stringBuilder.toString();
  }
  
  public void dump(String paramString, PrintWriter paramPrintWriter) {
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("Component: ");
    paramPrintWriter.println(getServiceInfo().getComponentName());
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("Settings: ");
    paramPrintWriter.println(this.mSettingsActivity);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("Compat packages: ");
    paramPrintWriter.println(this.mCompatibilityPackages);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("Inline Suggestions Enabled: ");
    paramPrintWriter.println(this.mInlineSuggestionsEnabled);
  }
}
