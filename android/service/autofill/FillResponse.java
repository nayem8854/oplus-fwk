package android.service.autofill;

import android.content.IntentSender;
import android.content.pm.ParceledListSlice;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.autofill.AutofillId;
import android.view.autofill.Helper;
import android.widget.RemoteViews;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class FillResponse implements Parcelable {
  private FillResponse(Builder paramBuilder) {
    ParceledListSlice parceledListSlice;
    if (paramBuilder.mDatasets != null) {
      parceledListSlice = new ParceledListSlice(paramBuilder.mDatasets);
    } else {
      parceledListSlice = null;
    } 
    this.mDatasets = parceledListSlice;
    this.mSaveInfo = paramBuilder.mSaveInfo;
    this.mClientState = paramBuilder.mClientState;
    this.mPresentation = paramBuilder.mPresentation;
    this.mInlinePresentation = paramBuilder.mInlinePresentation;
    this.mHeader = paramBuilder.mHeader;
    this.mFooter = paramBuilder.mFooter;
    this.mAuthentication = paramBuilder.mAuthentication;
    this.mAuthenticationIds = paramBuilder.mAuthenticationIds;
    this.mIgnoredIds = paramBuilder.mIgnoredIds;
    this.mDisableDuration = paramBuilder.mDisableDuration;
    this.mFieldClassificationIds = paramBuilder.mFieldClassificationIds;
    this.mFlags = paramBuilder.mFlags;
    this.mRequestId = Integer.MIN_VALUE;
    this.mUserData = paramBuilder.mUserData;
    this.mCancelIds = paramBuilder.mCancelIds;
    this.mSupportsInlineSuggestions = paramBuilder.mSupportsInlineSuggestions;
  }
  
  public Bundle getClientState() {
    return this.mClientState;
  }
  
  public List<Dataset> getDatasets() {
    ParceledListSlice<Dataset> parceledListSlice = this.mDatasets;
    if (parceledListSlice != null) {
      List list = parceledListSlice.getList();
    } else {
      parceledListSlice = null;
    } 
    return (List<Dataset>)parceledListSlice;
  }
  
  public SaveInfo getSaveInfo() {
    return this.mSaveInfo;
  }
  
  public RemoteViews getPresentation() {
    return this.mPresentation;
  }
  
  public InlinePresentation getInlinePresentation() {
    return this.mInlinePresentation;
  }
  
  public RemoteViews getHeader() {
    return this.mHeader;
  }
  
  public RemoteViews getFooter() {
    return this.mFooter;
  }
  
  public IntentSender getAuthentication() {
    return this.mAuthentication;
  }
  
  public AutofillId[] getAuthenticationIds() {
    return this.mAuthenticationIds;
  }
  
  public AutofillId[] getIgnoredIds() {
    return this.mIgnoredIds;
  }
  
  public long getDisableDuration() {
    return this.mDisableDuration;
  }
  
  public AutofillId[] getFieldClassificationIds() {
    return this.mFieldClassificationIds;
  }
  
  public UserData getUserData() {
    return this.mUserData;
  }
  
  public int getFlags() {
    return this.mFlags;
  }
  
  public void setRequestId(int paramInt) {
    this.mRequestId = paramInt;
  }
  
  public int getRequestId() {
    return this.mRequestId;
  }
  
  public int[] getCancelIds() {
    return this.mCancelIds;
  }
  
  public boolean supportsInlineSuggestions() {
    return this.mSupportsInlineSuggestions;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class FillResponseFlags implements Annotation {}
  
  class Builder {
    private IntentSender mAuthentication;
    
    private AutofillId[] mAuthenticationIds;
    
    private int[] mCancelIds;
    
    private Bundle mClientState;
    
    private ArrayList<Dataset> mDatasets;
    
    private boolean mDestroyed;
    
    private long mDisableDuration;
    
    private AutofillId[] mFieldClassificationIds;
    
    private int mFlags;
    
    private RemoteViews mFooter;
    
    private RemoteViews mHeader;
    
    private AutofillId[] mIgnoredIds;
    
    private InlinePresentation mInlinePresentation;
    
    private RemoteViews mPresentation;
    
    private SaveInfo mSaveInfo;
    
    private boolean mSupportsInlineSuggestions;
    
    private UserData mUserData;
    
    public Builder setAuthentication(AutofillId[] param1ArrayOfAutofillId, IntentSender param1IntentSender, RemoteViews param1RemoteViews) {
      throwIfDestroyed();
      throwIfDisableAutofillCalled();
      if (this.mHeader == null && this.mFooter == null) {
        boolean bool2, bool1 = true;
        if (param1IntentSender == null) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        if (param1RemoteViews != null)
          bool1 = false; 
        if ((bool1 ^ bool2) == 0) {
          this.mAuthentication = param1IntentSender;
          this.mPresentation = param1RemoteViews;
          this.mAuthenticationIds = AutofillServiceHelper.assertValid(param1ArrayOfAutofillId);
          return this;
        } 
        throw new IllegalArgumentException("authentication and presentation must be both non-null or null");
      } 
      throw new IllegalStateException("Already called #setHeader() or #setFooter()");
    }
    
    public Builder setAuthentication(AutofillId[] param1ArrayOfAutofillId, IntentSender param1IntentSender, RemoteViews param1RemoteViews, InlinePresentation param1InlinePresentation) {
      throwIfDestroyed();
      throwIfDisableAutofillCalled();
      if (this.mHeader == null && this.mFooter == null) {
        boolean bool2, bool1 = true;
        if (param1IntentSender == null) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        if (param1RemoteViews != null || param1InlinePresentation != null)
          bool1 = false; 
        if ((bool1 ^ bool2) == 0) {
          this.mAuthentication = param1IntentSender;
          this.mPresentation = param1RemoteViews;
          this.mInlinePresentation = param1InlinePresentation;
          this.mAuthenticationIds = AutofillServiceHelper.assertValid(param1ArrayOfAutofillId);
          return this;
        } 
        throw new IllegalArgumentException("authentication and presentation (dropdown or inline), must be both non-null or null");
      } 
      throw new IllegalStateException("Already called #setHeader() or #setFooter()");
    }
    
    public Builder setIgnoredIds(AutofillId... param1VarArgs) {
      throwIfDestroyed();
      this.mIgnoredIds = param1VarArgs;
      return this;
    }
    
    public Builder addDataset(Dataset param1Dataset) {
      throwIfDestroyed();
      throwIfDisableAutofillCalled();
      if (param1Dataset == null)
        return this; 
      if (this.mDatasets == null)
        this.mDatasets = new ArrayList<>(); 
      if (!this.mDatasets.add(param1Dataset))
        return this; 
      return this;
    }
    
    public Builder setSaveInfo(SaveInfo param1SaveInfo) {
      throwIfDestroyed();
      throwIfDisableAutofillCalled();
      this.mSaveInfo = param1SaveInfo;
      return this;
    }
    
    public Builder setClientState(Bundle param1Bundle) {
      throwIfDestroyed();
      throwIfDisableAutofillCalled();
      this.mClientState = param1Bundle;
      return this;
    }
    
    public Builder setFieldClassificationIds(AutofillId... param1VarArgs) {
      throwIfDestroyed();
      throwIfDisableAutofillCalled();
      Preconditions.checkArrayElementsNotNull((Object[])param1VarArgs, "ids");
      int i = param1VarArgs.length;
      int j = UserData.getMaxFieldClassificationIdsSize();
      Preconditions.checkArgumentInRange(i, 1, j, "ids length");
      this.mFieldClassificationIds = param1VarArgs;
      this.mFlags |= 0x1;
      return this;
    }
    
    public Builder setFlags(int param1Int) {
      throwIfDestroyed();
      this.mFlags = Preconditions.checkFlagsArgument(param1Int, 3);
      return this;
    }
    
    public Builder disableAutofill(long param1Long) {
      throwIfDestroyed();
      if (param1Long > 0L) {
        if (this.mAuthentication == null && this.mDatasets == null && this.mSaveInfo == null && this.mFieldClassificationIds == null && this.mClientState == null) {
          this.mDisableDuration = param1Long;
          return this;
        } 
        throw new IllegalStateException("disableAutofill() must be the only method called");
      } 
      throw new IllegalArgumentException("duration must be greater than 0");
    }
    
    public Builder setHeader(RemoteViews param1RemoteViews) {
      throwIfDestroyed();
      throwIfAuthenticationCalled();
      this.mHeader = (RemoteViews)Preconditions.checkNotNull(param1RemoteViews);
      return this;
    }
    
    public Builder setFooter(RemoteViews param1RemoteViews) {
      throwIfDestroyed();
      throwIfAuthenticationCalled();
      this.mFooter = (RemoteViews)Preconditions.checkNotNull(param1RemoteViews);
      return this;
    }
    
    public Builder setUserData(UserData param1UserData) {
      throwIfDestroyed();
      throwIfAuthenticationCalled();
      this.mUserData = (UserData)Preconditions.checkNotNull(param1UserData);
      return this;
    }
    
    public Builder setPresentationCancelIds(int[] param1ArrayOfint) {
      throwIfDestroyed();
      this.mCancelIds = param1ArrayOfint;
      return this;
    }
    
    public FillResponse build() {
      throwIfDestroyed();
      if (this.mAuthentication != null || this.mDatasets != null || this.mSaveInfo != null || this.mDisableDuration != 0L || this.mFieldClassificationIds != null || this.mClientState != null) {
        if (this.mDatasets != null || (this.mHeader == null && this.mFooter == null)) {
          ArrayList<Dataset> arrayList = this.mDatasets;
          if (arrayList != null) {
            for (Dataset dataset : arrayList) {
              if (dataset.getFieldInlinePresentation(0) != null) {
                this.mSupportsInlineSuggestions = true;
                break;
              } 
            } 
          } else if (this.mInlinePresentation != null) {
            this.mSupportsInlineSuggestions = true;
          } 
          this.mDestroyed = true;
          return new FillResponse(this);
        } 
        throw new IllegalStateException("must add at least 1 dataset when using header or footer");
      } 
      throw new IllegalStateException("need to provide: at least one DataSet, or a SaveInfo, or an authentication with a presentation, or a FieldsDetection, or a client state, or disable autofill");
    }
    
    private void throwIfDestroyed() {
      if (!this.mDestroyed)
        return; 
      throw new IllegalStateException("Already called #build()");
    }
    
    private void throwIfDisableAutofillCalled() {
      if (this.mDisableDuration <= 0L)
        return; 
      throw new IllegalStateException("Already called #disableAutofill()");
    }
    
    private void throwIfAuthenticationCalled() {
      if (this.mAuthentication == null)
        return; 
      throw new IllegalStateException("Already called #setAuthentication()");
    }
  }
  
  public String toString() {
    if (!Helper.sDebug)
      return super.toString(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("FillResponse : [mRequestId=");
    stringBuilder.append(this.mRequestId);
    stringBuilder = new StringBuilder(stringBuilder.toString());
    if (this.mDatasets != null) {
      stringBuilder.append(", datasets=");
      stringBuilder.append(this.mDatasets.getList());
    } 
    if (this.mSaveInfo != null) {
      stringBuilder.append(", saveInfo=");
      stringBuilder.append(this.mSaveInfo);
    } 
    if (this.mClientState != null)
      stringBuilder.append(", hasClientState"); 
    if (this.mPresentation != null)
      stringBuilder.append(", hasPresentation"); 
    if (this.mInlinePresentation != null)
      stringBuilder.append(", hasInlinePresentation"); 
    if (this.mHeader != null)
      stringBuilder.append(", hasHeader"); 
    if (this.mFooter != null)
      stringBuilder.append(", hasFooter"); 
    if (this.mAuthentication != null)
      stringBuilder.append(", hasAuthentication"); 
    if (this.mAuthenticationIds != null) {
      stringBuilder.append(", authenticationIds=");
      stringBuilder.append(Arrays.toString((Object[])this.mAuthenticationIds));
    } 
    stringBuilder.append(", disableDuration=");
    stringBuilder.append(this.mDisableDuration);
    if (this.mFlags != 0) {
      stringBuilder.append(", flags=");
      stringBuilder.append(this.mFlags);
    } 
    AutofillId[] arrayOfAutofillId = this.mFieldClassificationIds;
    if (arrayOfAutofillId != null)
      stringBuilder.append(Arrays.toString((Object[])arrayOfAutofillId)); 
    if (this.mUserData != null) {
      stringBuilder.append(", userData=");
      stringBuilder.append(this.mUserData);
    } 
    if (this.mCancelIds != null) {
      stringBuilder.append(", mCancelIds=");
      stringBuilder.append(this.mCancelIds.length);
    } 
    stringBuilder.append(", mSupportInlinePresentations=");
    stringBuilder.append(this.mSupportsInlineSuggestions);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.mDatasets, paramInt);
    paramParcel.writeParcelable(this.mSaveInfo, paramInt);
    paramParcel.writeParcelable(this.mClientState, paramInt);
    paramParcel.writeParcelableArray(this.mAuthenticationIds, paramInt);
    paramParcel.writeParcelable((Parcelable)this.mAuthentication, paramInt);
    paramParcel.writeParcelable((Parcelable)this.mPresentation, paramInt);
    paramParcel.writeParcelable(this.mInlinePresentation, paramInt);
    paramParcel.writeParcelable((Parcelable)this.mHeader, paramInt);
    paramParcel.writeParcelable((Parcelable)this.mFooter, paramInt);
    paramParcel.writeParcelable(this.mUserData, paramInt);
    paramParcel.writeParcelableArray(this.mIgnoredIds, paramInt);
    paramParcel.writeLong(this.mDisableDuration);
    paramParcel.writeParcelableArray(this.mFieldClassificationIds, paramInt);
    paramParcel.writeInt(this.mFlags);
    paramParcel.writeIntArray(this.mCancelIds);
    paramParcel.writeInt(this.mRequestId);
  }
  
  public static final Parcelable.Creator<FillResponse> CREATOR = new Parcelable.Creator<FillResponse>() {
      public FillResponse createFromParcel(Parcel param1Parcel) {
        byte b1;
        FillResponse.Builder builder = new FillResponse.Builder();
        ParceledListSlice<Dataset> parceledListSlice = param1Parcel.<ParceledListSlice>readParcelable(null);
        if (parceledListSlice != null) {
          List list = parceledListSlice.getList();
        } else {
          parceledListSlice = null;
        } 
        if (parceledListSlice != null) {
          b1 = parceledListSlice.size();
        } else {
          b1 = 0;
        } 
        for (byte b2 = 0; b2 < b1; b2++)
          builder.addDataset(parceledListSlice.get(b2)); 
        builder.setSaveInfo(param1Parcel.<SaveInfo>readParcelable(null));
        builder.setClientState(param1Parcel.<Bundle>readParcelable(null));
        AutofillId[] arrayOfAutofillId2 = param1Parcel.<AutofillId>readParcelableArray(null, AutofillId.class);
        IntentSender intentSender = param1Parcel.<IntentSender>readParcelable(null);
        RemoteViews remoteViews2 = param1Parcel.<RemoteViews>readParcelable(null);
        InlinePresentation inlinePresentation = param1Parcel.<InlinePresentation>readParcelable(null);
        if (arrayOfAutofillId2 != null)
          builder.setAuthentication(arrayOfAutofillId2, intentSender, remoteViews2, inlinePresentation); 
        RemoteViews remoteViews1 = param1Parcel.<RemoteViews>readParcelable(null);
        if (remoteViews1 != null)
          builder.setHeader(remoteViews1); 
        remoteViews1 = param1Parcel.<RemoteViews>readParcelable(null);
        if (remoteViews1 != null)
          builder.setFooter(remoteViews1); 
        UserData userData = param1Parcel.<UserData>readParcelable(null);
        if (userData != null)
          builder.setUserData(userData); 
        builder.setIgnoredIds(param1Parcel.<AutofillId>readParcelableArray(null, AutofillId.class));
        long l = param1Parcel.readLong();
        if (l > 0L)
          builder.disableAutofill(l); 
        AutofillId[] arrayOfAutofillId1 = param1Parcel.<AutofillId>readParcelableArray(null, AutofillId.class);
        if (arrayOfAutofillId1 != null)
          builder.setFieldClassificationIds(arrayOfAutofillId1); 
        builder.setFlags(param1Parcel.readInt());
        int[] arrayOfInt = param1Parcel.createIntArray();
        builder.setPresentationCancelIds(arrayOfInt);
        FillResponse fillResponse = builder.build();
        fillResponse.setRequestId(param1Parcel.readInt());
        return fillResponse;
      }
      
      public FillResponse[] newArray(int param1Int) {
        return new FillResponse[param1Int];
      }
    };
  
  public static final int FLAG_DISABLE_ACTIVITY_ONLY = 2;
  
  public static final int FLAG_TRACK_CONTEXT_COMMITED = 1;
  
  private final IntentSender mAuthentication;
  
  private final AutofillId[] mAuthenticationIds;
  
  private final int[] mCancelIds;
  
  private final Bundle mClientState;
  
  private final ParceledListSlice<Dataset> mDatasets;
  
  private final long mDisableDuration;
  
  private final AutofillId[] mFieldClassificationIds;
  
  private final int mFlags;
  
  private final RemoteViews mFooter;
  
  private final RemoteViews mHeader;
  
  private final AutofillId[] mIgnoredIds;
  
  private final InlinePresentation mInlinePresentation;
  
  private final RemoteViews mPresentation;
  
  private int mRequestId;
  
  private final SaveInfo mSaveInfo;
  
  private final boolean mSupportsInlineSuggestions;
  
  private final UserData mUserData;
}
