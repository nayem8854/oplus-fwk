package android.service.autofill;

import android.app.ActivityThread;
import android.content.ContentResolver;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.Log;
import android.view.autofill.Helper;
import com.android.internal.util.Preconditions;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Map;

public final class UserData implements FieldClassificationUserData, Parcelable {
  private UserData(Builder paramBuilder) {
    this.mId = paramBuilder.mId;
    this.mCategoryIds = new String[paramBuilder.mCategoryIds.size()];
    paramBuilder.mCategoryIds.toArray((Object[])this.mCategoryIds);
    this.mValues = new String[paramBuilder.mValues.size()];
    paramBuilder.mValues.toArray((Object[])this.mValues);
    paramBuilder.mValues.toArray((Object[])this.mValues);
    this.mDefaultAlgorithm = paramBuilder.mDefaultAlgorithm;
    this.mDefaultArgs = paramBuilder.mDefaultArgs;
    this.mCategoryAlgorithms = paramBuilder.mCategoryAlgorithms;
    this.mCategoryArgs = paramBuilder.mCategoryArgs;
  }
  
  public String getFieldClassificationAlgorithm() {
    return this.mDefaultAlgorithm;
  }
  
  public Bundle getDefaultFieldClassificationArgs() {
    return this.mDefaultArgs;
  }
  
  public String getFieldClassificationAlgorithmForCategory(String paramString) {
    Preconditions.checkNotNull(paramString);
    ArrayMap<String, String> arrayMap = this.mCategoryAlgorithms;
    if (arrayMap == null || !arrayMap.containsKey(paramString))
      return null; 
    return (String)this.mCategoryAlgorithms.get(paramString);
  }
  
  public String getId() {
    return this.mId;
  }
  
  public String[] getCategoryIds() {
    return this.mCategoryIds;
  }
  
  public String[] getValues() {
    return this.mValues;
  }
  
  public ArrayMap<String, String> getFieldClassificationAlgorithms() {
    return this.mCategoryAlgorithms;
  }
  
  public ArrayMap<String, Bundle> getFieldClassificationArgs() {
    return this.mCategoryArgs;
  }
  
  public void dump(String paramString, PrintWriter paramPrintWriter) {
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("id: ");
    paramPrintWriter.print(this.mId);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("Default Algorithm: ");
    paramPrintWriter.print(this.mDefaultAlgorithm);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("Default Args");
    paramPrintWriter.print(this.mDefaultArgs);
    ArrayMap<String, String> arrayMap = this.mCategoryAlgorithms;
    if (arrayMap != null && arrayMap.size() > 0) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("Algorithms per category: ");
      for (byte b1 = 0; b1 < this.mCategoryAlgorithms.size(); b1++) {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print((String)this.mCategoryAlgorithms.keyAt(b1));
        paramPrintWriter.print(": ");
        paramPrintWriter.println(Helper.getRedacted((CharSequence)this.mCategoryAlgorithms.valueAt(b1)));
        paramPrintWriter.print("args=");
        paramPrintWriter.print(this.mCategoryArgs.get(this.mCategoryAlgorithms.keyAt(b1)));
      } 
    } 
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("Field ids size: ");
    paramPrintWriter.println(this.mCategoryIds.length);
    byte b;
    for (b = 0; b < this.mCategoryIds.length; b++) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print(paramString);
      paramPrintWriter.print(b);
      paramPrintWriter.print(": ");
      paramPrintWriter.println(Helper.getRedacted(this.mCategoryIds[b]));
    } 
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("Values size: ");
    paramPrintWriter.println(this.mValues.length);
    for (b = 0; b < this.mValues.length; b++) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print(paramString);
      paramPrintWriter.print(b);
      paramPrintWriter.print(": ");
      paramPrintWriter.println(Helper.getRedacted(this.mValues[b]));
    } 
  }
  
  public static void dumpConstraints(String paramString, PrintWriter paramPrintWriter) {
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("maxUserDataSize: ");
    paramPrintWriter.println(getMaxUserDataSize());
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("maxFieldClassificationIdsSize: ");
    paramPrintWriter.println(getMaxFieldClassificationIdsSize());
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("maxCategoryCount: ");
    paramPrintWriter.println(getMaxCategoryCount());
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("minValueLength: ");
    paramPrintWriter.println(getMinValueLength());
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("maxValueLength: ");
    paramPrintWriter.println(getMaxValueLength());
  }
  
  class Builder {
    private ArrayMap<String, String> mCategoryAlgorithms;
    
    private ArrayMap<String, Bundle> mCategoryArgs;
    
    private final ArrayList<String> mCategoryIds;
    
    private String mDefaultAlgorithm;
    
    private Bundle mDefaultArgs;
    
    private boolean mDestroyed;
    
    private final String mId;
    
    private final ArraySet<String> mUniqueCategoryIds;
    
    private final ArraySet<String> mUniqueValueCategoryPairs;
    
    private final ArrayList<String> mValues;
    
    public Builder(UserData this$0, String param1String1, String param1String2) {
      this.mId = checkNotEmpty("id", (String)this$0);
      checkNotEmpty("categoryId", param1String2);
      checkValidValue(param1String1);
      int i = UserData.getMaxUserDataSize();
      this.mCategoryIds = new ArrayList<>(i);
      this.mValues = new ArrayList<>(i);
      this.mUniqueValueCategoryPairs = new ArraySet(i);
      this.mUniqueCategoryIds = new ArraySet(UserData.getMaxCategoryCount());
      addMapping(param1String1, param1String2);
    }
    
    public Builder setFieldClassificationAlgorithm(String param1String, Bundle param1Bundle) {
      throwIfDestroyed();
      this.mDefaultAlgorithm = param1String;
      this.mDefaultArgs = param1Bundle;
      return this;
    }
    
    public Builder setFieldClassificationAlgorithmForCategory(String param1String1, String param1String2, Bundle param1Bundle) {
      throwIfDestroyed();
      Preconditions.checkNotNull(param1String1);
      if (this.mCategoryAlgorithms == null)
        this.mCategoryAlgorithms = new ArrayMap(UserData.getMaxCategoryCount()); 
      if (this.mCategoryArgs == null)
        this.mCategoryArgs = new ArrayMap(UserData.getMaxCategoryCount()); 
      this.mCategoryAlgorithms.put(param1String1, param1String2);
      this.mCategoryArgs.put(param1String1, param1Bundle);
      return this;
    }
    
    public Builder add(String param1String1, String param1String2) {
      throwIfDestroyed();
      checkNotEmpty("categoryId", param1String2);
      checkValidValue(param1String1);
      boolean bool = this.mUniqueCategoryIds.contains(param1String2);
      boolean bool1 = true;
      if (!bool) {
        if (this.mUniqueCategoryIds.size() < UserData.getMaxCategoryCount()) {
          bool = true;
        } else {
          bool = false;
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("already added ");
        ArraySet<String> arraySet = this.mUniqueCategoryIds;
        stringBuilder1.append(arraySet.size());
        stringBuilder1.append(" unique category ids");
        String str1 = stringBuilder1.toString();
        Preconditions.checkState(bool, str1);
      } 
      if (this.mValues.size() < UserData.getMaxUserDataSize()) {
        bool = bool1;
      } else {
        bool = false;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("already added ");
      ArrayList<String> arrayList = this.mValues;
      stringBuilder.append(arrayList.size());
      stringBuilder.append(" elements");
      String str = stringBuilder.toString();
      Preconditions.checkState(bool, str);
      addMapping(param1String1, param1String2);
      return this;
    }
    
    private void addMapping(String param1String1, String param1String2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1String1);
      stringBuilder.append(":");
      stringBuilder.append(param1String2);
      String str = stringBuilder.toString();
      if (this.mUniqueValueCategoryPairs.contains(str)) {
        Log.w("UserData", "Ignoring entry with same value / category");
        return;
      } 
      this.mCategoryIds.add(param1String2);
      this.mValues.add(param1String1);
      this.mUniqueCategoryIds.add(param1String2);
      this.mUniqueValueCategoryPairs.add(str);
    }
    
    private String checkNotEmpty(String param1String1, String param1String2) {
      Preconditions.checkNotNull(param1String2);
      Preconditions.checkArgument(TextUtils.isEmpty(param1String2) ^ true, "%s cannot be empty", new Object[] { param1String1 });
      return param1String2;
    }
    
    private void checkValidValue(String param1String) {
      Preconditions.checkNotNull(param1String);
      int i = param1String.length();
      int j = UserData.getMinValueLength();
      int k = UserData.getMaxValueLength();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("value length (");
      stringBuilder.append(i);
      stringBuilder.append(")");
      String str = stringBuilder.toString();
      Preconditions.checkArgumentInRange(i, j, k, str);
    }
    
    public UserData build() {
      throwIfDestroyed();
      this.mDestroyed = true;
      return new UserData(this);
    }
    
    private void throwIfDestroyed() {
      if (!this.mDestroyed)
        return; 
      throw new IllegalStateException("Already called #build()");
    }
  }
  
  public String toString() {
    if (!Helper.sDebug)
      return super.toString(); 
    StringBuilder stringBuilder = (new StringBuilder("UserData: [id=")).append(this.mId);
    stringBuilder.append(", categoryIds=");
    Helper.appendRedacted(stringBuilder, this.mCategoryIds);
    stringBuilder.append(", values=");
    Helper.appendRedacted(stringBuilder, this.mValues);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mId);
    paramParcel.writeStringArray(this.mCategoryIds);
    paramParcel.writeStringArray(this.mValues);
    paramParcel.writeString(this.mDefaultAlgorithm);
    paramParcel.writeBundle(this.mDefaultArgs);
    paramParcel.writeMap((Map)this.mCategoryAlgorithms);
    paramParcel.writeMap((Map)this.mCategoryArgs);
  }
  
  public static final Parcelable.Creator<UserData> CREATOR = new Parcelable.Creator<UserData>() {
      public UserData createFromParcel(Parcel param1Parcel) {
        String str1 = param1Parcel.readString();
        String[] arrayOfString1 = param1Parcel.readStringArray();
        String[] arrayOfString2 = param1Parcel.readStringArray();
        String str2 = param1Parcel.readString();
        Bundle bundle = param1Parcel.readBundle();
        ArrayMap arrayMap1 = new ArrayMap();
        param1Parcel.readMap((Map)arrayMap1, String.class.getClassLoader());
        ArrayMap arrayMap2 = new ArrayMap();
        param1Parcel.readMap((Map)arrayMap2, Bundle.class.getClassLoader());
        UserData.Builder builder = new UserData.Builder(str1, arrayOfString2[0], arrayOfString1[0]);
        builder = builder.setFieldClassificationAlgorithm(str2, bundle);
        byte b;
        for (b = 1; b < arrayOfString1.length; b++) {
          String str = arrayOfString1[b];
          builder.add(arrayOfString2[b], str);
        } 
        int i = arrayMap1.size();
        if (i > 0)
          for (b = 0; b < i; b++) {
            String str3 = (String)arrayMap1.keyAt(b);
            String str4 = (String)arrayMap1.valueAt(b);
            Bundle bundle1 = (Bundle)arrayMap2.get(str3);
            builder.setFieldClassificationAlgorithmForCategory(str3, str4, bundle1);
          }  
        return builder.build();
      }
      
      public UserData[] newArray(int param1Int) {
        return new UserData[param1Int];
      }
    };
  
  private static final int DEFAULT_MAX_CATEGORY_COUNT = 10;
  
  private static final int DEFAULT_MAX_FIELD_CLASSIFICATION_IDS_SIZE = 10;
  
  private static final int DEFAULT_MAX_USER_DATA_SIZE = 50;
  
  private static final int DEFAULT_MAX_VALUE_LENGTH = 100;
  
  private static final int DEFAULT_MIN_VALUE_LENGTH = 3;
  
  private static final String TAG = "UserData";
  
  private final ArrayMap<String, String> mCategoryAlgorithms;
  
  private final ArrayMap<String, Bundle> mCategoryArgs;
  
  private final String[] mCategoryIds;
  
  private final String mDefaultAlgorithm;
  
  private final Bundle mDefaultArgs;
  
  private final String mId;
  
  private final String[] mValues;
  
  public static int getMaxUserDataSize() {
    return getInt("autofill_user_data_max_user_data_size", 50);
  }
  
  public static int getMaxFieldClassificationIdsSize() {
    return getInt("autofill_user_data_max_field_classification_size", 10);
  }
  
  public static int getMaxCategoryCount() {
    return getInt("autofill_user_data_max_category_count", 10);
  }
  
  public static int getMinValueLength() {
    return getInt("autofill_user_data_min_value_length", 3);
  }
  
  public static int getMaxValueLength() {
    return getInt("autofill_user_data_max_value_length", 100);
  }
  
  private static int getInt(String paramString, int paramInt) {
    StringBuilder stringBuilder;
    ContentResolver contentResolver = null;
    ActivityThread activityThread = ActivityThread.currentActivityThread();
    if (activityThread != null)
      contentResolver = activityThread.getApplication().getContentResolver(); 
    if (contentResolver == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Could not read from ");
      stringBuilder.append(paramString);
      stringBuilder.append("; hardcoding ");
      stringBuilder.append(paramInt);
      Log.w("UserData", stringBuilder.toString());
      return paramInt;
    } 
    return Settings.Secure.getInt((ContentResolver)stringBuilder, paramString, paramInt);
  }
}
