package android.service.autofill;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IInlineSuggestionUi extends IInterface {
  void getSurfacePackage(ISurfacePackageResultCallback paramISurfacePackageResultCallback) throws RemoteException;
  
  void releaseSurfaceControlViewHost() throws RemoteException;
  
  class Default implements IInlineSuggestionUi {
    public void getSurfacePackage(ISurfacePackageResultCallback param1ISurfacePackageResultCallback) throws RemoteException {}
    
    public void releaseSurfaceControlViewHost() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInlineSuggestionUi {
    private static final String DESCRIPTOR = "android.service.autofill.IInlineSuggestionUi";
    
    static final int TRANSACTION_getSurfacePackage = 1;
    
    static final int TRANSACTION_releaseSurfaceControlViewHost = 2;
    
    public Stub() {
      attachInterface(this, "android.service.autofill.IInlineSuggestionUi");
    }
    
    public static IInlineSuggestionUi asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.autofill.IInlineSuggestionUi");
      if (iInterface != null && iInterface instanceof IInlineSuggestionUi)
        return (IInlineSuggestionUi)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "releaseSurfaceControlViewHost";
      } 
      return "getSurfacePackage";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.service.autofill.IInlineSuggestionUi");
          return true;
        } 
        param1Parcel1.enforceInterface("android.service.autofill.IInlineSuggestionUi");
        releaseSurfaceControlViewHost();
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.autofill.IInlineSuggestionUi");
      ISurfacePackageResultCallback iSurfacePackageResultCallback = ISurfacePackageResultCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
      getSurfacePackage(iSurfacePackageResultCallback);
      return true;
    }
    
    private static class Proxy implements IInlineSuggestionUi {
      public static IInlineSuggestionUi sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.autofill.IInlineSuggestionUi";
      }
      
      public void getSurfacePackage(ISurfacePackageResultCallback param2ISurfacePackageResultCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.autofill.IInlineSuggestionUi");
          if (param2ISurfacePackageResultCallback != null) {
            iBinder = param2ISurfacePackageResultCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IInlineSuggestionUi.Stub.getDefaultImpl() != null) {
            IInlineSuggestionUi.Stub.getDefaultImpl().getSurfacePackage(param2ISurfacePackageResultCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void releaseSurfaceControlViewHost() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.autofill.IInlineSuggestionUi");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IInlineSuggestionUi.Stub.getDefaultImpl() != null) {
            IInlineSuggestionUi.Stub.getDefaultImpl().releaseSurfaceControlViewHost();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInlineSuggestionUi param1IInlineSuggestionUi) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInlineSuggestionUi != null) {
          Proxy.sDefaultImpl = param1IInlineSuggestionUi;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInlineSuggestionUi getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
