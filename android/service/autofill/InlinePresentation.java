package android.service.autofill;

import android.annotation.NonNull;
import android.app.slice.Slice;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.inline.InlinePresentationSpec;
import com.android.internal.util.AnnotationValidations;
import java.util.List;
import java.util.Objects;

public final class InlinePresentation implements Parcelable {
  public String[] getAutofillHints() {
    List list = this.mSlice.getHints();
    return (String[])list.toArray((Object[])new String[list.size()]);
  }
  
  public InlinePresentation(Slice paramSlice, InlinePresentationSpec paramInlinePresentationSpec, boolean paramBoolean) {
    this.mSlice = paramSlice;
    AnnotationValidations.validate(NonNull.class, null, paramSlice);
    this.mInlinePresentationSpec = paramInlinePresentationSpec;
    AnnotationValidations.validate(NonNull.class, null, paramInlinePresentationSpec);
    this.mPinned = paramBoolean;
  }
  
  public Slice getSlice() {
    return this.mSlice;
  }
  
  public InlinePresentationSpec getInlinePresentationSpec() {
    return this.mInlinePresentationSpec;
  }
  
  public boolean isPinned() {
    return this.mPinned;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("InlinePresentation { slice = ");
    stringBuilder.append(this.mSlice);
    stringBuilder.append(", inlinePresentationSpec = ");
    stringBuilder.append(this.mInlinePresentationSpec);
    stringBuilder.append(", pinned = ");
    stringBuilder.append(this.mPinned);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    Slice slice1 = this.mSlice, slice2 = ((InlinePresentation)paramObject).mSlice;
    if (Objects.equals(slice1, slice2)) {
      InlinePresentationSpec inlinePresentationSpec1 = this.mInlinePresentationSpec, inlinePresentationSpec2 = ((InlinePresentation)paramObject).mInlinePresentationSpec;
      if (Objects.equals(inlinePresentationSpec1, inlinePresentationSpec2) && this.mPinned == ((InlinePresentation)paramObject).mPinned)
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = Objects.hashCode(this.mSlice);
    int j = Objects.hashCode(this.mInlinePresentationSpec);
    int k = Boolean.hashCode(this.mPinned);
    return ((1 * 31 + i) * 31 + j) * 31 + k;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    byte b = 0;
    if (this.mPinned)
      b = (byte)(0x0 | 0x4); 
    paramParcel.writeByte(b);
    paramParcel.writeTypedObject(this.mSlice, paramInt);
    paramParcel.writeTypedObject(this.mInlinePresentationSpec, paramInt);
  }
  
  public int describeContents() {
    return 0;
  }
  
  InlinePresentation(Parcel paramParcel) {
    boolean bool;
    byte b = paramParcel.readByte();
    if ((b & 0x4) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Slice slice = paramParcel.<Slice>readTypedObject(Slice.CREATOR);
    InlinePresentationSpec inlinePresentationSpec = paramParcel.<InlinePresentationSpec>readTypedObject(InlinePresentationSpec.CREATOR);
    this.mSlice = slice;
    AnnotationValidations.validate(NonNull.class, null, slice);
    this.mInlinePresentationSpec = inlinePresentationSpec;
    AnnotationValidations.validate(NonNull.class, null, inlinePresentationSpec);
    this.mPinned = bool;
  }
  
  public static final Parcelable.Creator<InlinePresentation> CREATOR = (Parcelable.Creator<InlinePresentation>)new Object();
  
  private final InlinePresentationSpec mInlinePresentationSpec;
  
  private final boolean mPinned;
  
  private final Slice mSlice;
  
  @Deprecated
  private void __metadata() {}
}
