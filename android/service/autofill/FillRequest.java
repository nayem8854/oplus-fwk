package android.service.autofill;

import android.annotation.NonNull;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.inputmethod.InlineSuggestionsRequest;
import com.android.internal.util.AnnotationValidations;
import com.android.internal.util.BitUtils;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;
import java.util.function.IntFunction;

public final class FillRequest implements Parcelable {
  private void onConstructed() {
    Preconditions.checkCollectionElementsNotNull(this.mFillContexts, "contexts");
  }
  
  public static String requestFlagsToString(int paramInt) {
    return BitUtils.flagsToString(paramInt, (IntFunction)_$$Lambda$1t9tMtzuwhLlqE8N1Jp6U_FLHd4.INSTANCE);
  }
  
  static String singleRequestFlagsToString(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 4) {
          if (paramInt != 16)
            return Integer.toHexString(paramInt); 
          return "FLAG_VIEW_NOT_FOCUSED";
        } 
        return "FLAG_PASSWORD_INPUT_TYPE";
      } 
      return "FLAG_COMPATIBILITY_MODE_REQUEST";
    } 
    return "FLAG_MANUAL_REQUEST";
  }
  
  public FillRequest(int paramInt1, List<FillContext> paramList, Bundle paramBundle, int paramInt2, InlineSuggestionsRequest paramInlineSuggestionsRequest) {
    this.mId = paramInt1;
    this.mFillContexts = paramList;
    AnnotationValidations.validate(NonNull.class, null, paramList);
    this.mClientState = paramBundle;
    this.mFlags = paramInt2;
    Preconditions.checkFlagsArgument(paramInt2, 23);
    this.mInlineSuggestionsRequest = paramInlineSuggestionsRequest;
    onConstructed();
  }
  
  public int getId() {
    return this.mId;
  }
  
  public List<FillContext> getFillContexts() {
    return this.mFillContexts;
  }
  
  public Bundle getClientState() {
    return this.mClientState;
  }
  
  public int getFlags() {
    return this.mFlags;
  }
  
  public InlineSuggestionsRequest getInlineSuggestionsRequest() {
    return this.mInlineSuggestionsRequest;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("FillRequest { id = ");
    stringBuilder.append(this.mId);
    stringBuilder.append(", fillContexts = ");
    stringBuilder.append(this.mFillContexts);
    stringBuilder.append(", clientState = ");
    stringBuilder.append(this.mClientState);
    stringBuilder.append(", flags = ");
    int i = this.mFlags;
    stringBuilder.append(requestFlagsToString(i));
    stringBuilder.append(", inlineSuggestionsRequest = ");
    stringBuilder.append(this.mInlineSuggestionsRequest);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    byte b1 = 0;
    if (this.mClientState != null)
      b1 = (byte)(0x0 | 0x4); 
    byte b2 = b1;
    if (this.mInlineSuggestionsRequest != null)
      b2 = (byte)(b1 | 0x10); 
    paramParcel.writeByte(b2);
    paramParcel.writeInt(this.mId);
    paramParcel.writeParcelableList(this.mFillContexts, paramInt);
    Bundle bundle = this.mClientState;
    if (bundle != null)
      paramParcel.writeBundle(bundle); 
    paramParcel.writeInt(this.mFlags);
    InlineSuggestionsRequest inlineSuggestionsRequest = this.mInlineSuggestionsRequest;
    if (inlineSuggestionsRequest != null)
      paramParcel.writeTypedObject(inlineSuggestionsRequest, paramInt); 
  }
  
  public int describeContents() {
    return 0;
  }
  
  FillRequest(Parcel paramParcel) {
    InlineSuggestionsRequest inlineSuggestionsRequest;
    Bundle bundle;
    byte b = paramParcel.readByte();
    int i = paramParcel.readInt();
    ArrayList<Parcelable> arrayList = new ArrayList();
    paramParcel.readParcelableList(arrayList, FillContext.class.getClassLoader());
    if ((b & 0x4) == 0) {
      bundle = null;
    } else {
      bundle = paramParcel.readBundle();
    } 
    int j = paramParcel.readInt();
    if ((b & 0x10) == 0) {
      paramParcel = null;
    } else {
      inlineSuggestionsRequest = paramParcel.<InlineSuggestionsRequest>readTypedObject(InlineSuggestionsRequest.CREATOR);
    } 
    this.mId = i;
    this.mFillContexts = (List)arrayList;
    AnnotationValidations.validate(NonNull.class, null, arrayList);
    this.mClientState = bundle;
    this.mFlags = j;
    Preconditions.checkFlagsArgument(j, 23);
    this.mInlineSuggestionsRequest = inlineSuggestionsRequest;
    onConstructed();
  }
  
  public static final Parcelable.Creator<FillRequest> CREATOR = new Parcelable.Creator<FillRequest>() {
      public FillRequest[] newArray(int param1Int) {
        return new FillRequest[param1Int];
      }
      
      public FillRequest createFromParcel(Parcel param1Parcel) {
        return new FillRequest(param1Parcel);
      }
    };
  
  public static final int FLAG_COMPATIBILITY_MODE_REQUEST = 2;
  
  public static final int FLAG_MANUAL_REQUEST = 1;
  
  public static final int FLAG_PASSWORD_INPUT_TYPE = 4;
  
  public static final int FLAG_VIEW_NOT_FOCUSED = 16;
  
  public static final int INVALID_REQUEST_ID = -2147483648;
  
  private final Bundle mClientState;
  
  private final List<FillContext> mFillContexts;
  
  private final int mFlags;
  
  private final int mId;
  
  private final InlineSuggestionsRequest mInlineSuggestionsRequest;
  
  @Deprecated
  private void __metadata() {}
  
  @Retention(RetentionPolicy.SOURCE)
  class RequestFlags implements Annotation {}
}
