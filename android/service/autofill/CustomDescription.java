package android.service.autofill;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pair;
import android.util.SparseArray;
import android.view.autofill.Helper;
import android.widget.RemoteViews;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;

public final class CustomDescription implements Parcelable {
  private CustomDescription(Builder paramBuilder) {
    this.mPresentation = paramBuilder.mPresentation;
    this.mTransformations = paramBuilder.mTransformations;
    this.mUpdates = paramBuilder.mUpdates;
    this.mActions = paramBuilder.mActions;
  }
  
  public RemoteViews getPresentation() {
    return this.mPresentation;
  }
  
  public ArrayList<Pair<Integer, InternalTransformation>> getTransformations() {
    return this.mTransformations;
  }
  
  public ArrayList<Pair<InternalValidator, BatchUpdates>> getUpdates() {
    return this.mUpdates;
  }
  
  public SparseArray<InternalOnClickAction> getActions() {
    return this.mActions;
  }
  
  class Builder {
    private SparseArray<InternalOnClickAction> mActions;
    
    private boolean mDestroyed;
    
    private final RemoteViews mPresentation;
    
    private ArrayList<Pair<Integer, InternalTransformation>> mTransformations;
    
    private ArrayList<Pair<InternalValidator, BatchUpdates>> mUpdates;
    
    public Builder(CustomDescription this$0) {
      this.mPresentation = (RemoteViews)Preconditions.checkNotNull(this$0);
    }
    
    public Builder addChild(int param1Int, Transformation param1Transformation) {
      throwIfDestroyed();
      boolean bool = param1Transformation instanceof InternalTransformation;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("not provided by Android System: ");
      stringBuilder.append(param1Transformation);
      Preconditions.checkArgument(bool, stringBuilder.toString());
      if (this.mTransformations == null)
        this.mTransformations = new ArrayList<>(); 
      this.mTransformations.add(new Pair(Integer.valueOf(param1Int), param1Transformation));
      return this;
    }
    
    public Builder batchUpdate(Validator param1Validator, BatchUpdates param1BatchUpdates) {
      throwIfDestroyed();
      boolean bool = param1Validator instanceof InternalValidator;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("not provided by Android System: ");
      stringBuilder.append(param1Validator);
      Preconditions.checkArgument(bool, stringBuilder.toString());
      Preconditions.checkNotNull(param1BatchUpdates);
      if (this.mUpdates == null)
        this.mUpdates = new ArrayList<>(); 
      this.mUpdates.add(new Pair(param1Validator, param1BatchUpdates));
      return this;
    }
    
    public Builder addOnClickAction(int param1Int, OnClickAction param1OnClickAction) {
      throwIfDestroyed();
      boolean bool = param1OnClickAction instanceof InternalOnClickAction;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("not provided by Android System: ");
      stringBuilder.append(param1OnClickAction);
      Preconditions.checkArgument(bool, stringBuilder.toString());
      if (this.mActions == null)
        this.mActions = new SparseArray(); 
      this.mActions.put(param1Int, param1OnClickAction);
      return this;
    }
    
    public CustomDescription build() {
      throwIfDestroyed();
      this.mDestroyed = true;
      return new CustomDescription(this);
    }
    
    private void throwIfDestroyed() {
      if (!this.mDestroyed)
        return; 
      throw new IllegalStateException("Already called #build()");
    }
  }
  
  public String toString() {
    String str3;
    Integer integer3;
    String str2;
    Integer integer2;
    String str1;
    Integer integer1;
    if (!Helper.sDebug)
      return super.toString(); 
    StringBuilder stringBuilder = new StringBuilder("CustomDescription: [presentation=");
    RemoteViews remoteViews = this.mPresentation;
    stringBuilder.append(remoteViews);
    stringBuilder.append(", transformations=");
    ArrayList<Pair<Integer, InternalTransformation>> arrayList1 = this.mTransformations;
    String str4 = "N/A";
    if (arrayList1 == null) {
      str3 = "N/A";
    } else {
      integer3 = Integer.valueOf(str3.size());
    } 
    stringBuilder.append(integer3);
    stringBuilder.append(", updates=");
    ArrayList<Pair<InternalValidator, BatchUpdates>> arrayList = this.mUpdates;
    if (arrayList == null) {
      str2 = "N/A";
    } else {
      integer2 = Integer.valueOf(str2.size());
    } 
    stringBuilder.append(integer2);
    stringBuilder.append(", actions=");
    SparseArray<InternalOnClickAction> sparseArray = this.mActions;
    if (sparseArray == null) {
      str1 = str4;
    } else {
      integer1 = Integer.valueOf(str1.size());
    } 
    stringBuilder.append(integer1);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.mPresentation, paramInt);
    if (this.mPresentation == null)
      return; 
    ArrayList<Pair<Integer, InternalTransformation>> arrayList1 = this.mTransformations;
    if (arrayList1 == null) {
      paramParcel.writeIntArray(null);
    } else {
      int i = arrayList1.size();
      int[] arrayOfInt = new int[i];
      InternalTransformation[] arrayOfInternalTransformation = new InternalTransformation[i];
      for (byte b = 0; b < i; b++) {
        Pair pair = this.mTransformations.get(b);
        arrayOfInt[b] = ((Integer)pair.first).intValue();
        arrayOfInternalTransformation[b] = (InternalTransformation)pair.second;
      } 
      paramParcel.writeIntArray(arrayOfInt);
      paramParcel.writeParcelableArray(arrayOfInternalTransformation, paramInt);
    } 
    ArrayList<Pair<InternalValidator, BatchUpdates>> arrayList = this.mUpdates;
    if (arrayList == null) {
      paramParcel.writeParcelableArray(null, paramInt);
    } else {
      int i = arrayList.size();
      InternalValidator[] arrayOfInternalValidator = new InternalValidator[i];
      BatchUpdates[] arrayOfBatchUpdates = new BatchUpdates[i];
      for (byte b = 0; b < i; b++) {
        Pair pair = this.mUpdates.get(b);
        arrayOfInternalValidator[b] = (InternalValidator)pair.first;
        arrayOfBatchUpdates[b] = (BatchUpdates)pair.second;
      } 
      paramParcel.writeParcelableArray(arrayOfInternalValidator, paramInt);
      paramParcel.writeParcelableArray(arrayOfBatchUpdates, paramInt);
    } 
    SparseArray<InternalOnClickAction> sparseArray = this.mActions;
    if (sparseArray == null) {
      paramParcel.writeIntArray(null);
    } else {
      int i = sparseArray.size();
      int[] arrayOfInt = new int[i];
      InternalOnClickAction[] arrayOfInternalOnClickAction = new InternalOnClickAction[i];
      for (byte b = 0; b < i; b++) {
        arrayOfInt[b] = this.mActions.keyAt(b);
        arrayOfInternalOnClickAction[b] = (InternalOnClickAction)this.mActions.valueAt(b);
      } 
      paramParcel.writeIntArray(arrayOfInt);
      paramParcel.writeParcelableArray(arrayOfInternalOnClickAction, paramInt);
    } 
  }
  
  public static final Parcelable.Creator<CustomDescription> CREATOR = new Parcelable.Creator<CustomDescription>() {
      public CustomDescription createFromParcel(Parcel param1Parcel) {
        RemoteViews remoteViews = param1Parcel.<RemoteViews>readParcelable(null);
        if (remoteViews == null)
          return null; 
        CustomDescription.Builder builder = new CustomDescription.Builder(remoteViews);
        int[] arrayOfInt1 = param1Parcel.createIntArray();
        if (arrayOfInt1 != null) {
          InternalTransformation[] arrayOfInternalTransformation = param1Parcel.<InternalTransformation>readParcelableArray(null, InternalTransformation.class);
          int i = arrayOfInt1.length;
          for (byte b = 0; b < i; b++)
            builder.addChild(arrayOfInt1[b], arrayOfInternalTransformation[b]); 
        } 
        InternalValidator[] arrayOfInternalValidator = param1Parcel.<InternalValidator>readParcelableArray(null, InternalValidator.class);
        if (arrayOfInternalValidator != null) {
          BatchUpdates[] arrayOfBatchUpdates = param1Parcel.<BatchUpdates>readParcelableArray(null, BatchUpdates.class);
          int i = arrayOfInternalValidator.length;
          for (byte b = 0; b < i; b++)
            builder.batchUpdate(arrayOfInternalValidator[b], arrayOfBatchUpdates[b]); 
        } 
        int[] arrayOfInt2 = param1Parcel.createIntArray();
        if (arrayOfInt2 != null) {
          InternalOnClickAction[] arrayOfInternalOnClickAction = param1Parcel.<InternalOnClickAction>readParcelableArray(null, InternalOnClickAction.class);
          int i = arrayOfInt2.length;
          for (byte b = 0; b < i; b++)
            builder.addOnClickAction(arrayOfInt2[b], arrayOfInternalOnClickAction[b]); 
        } 
        return builder.build();
      }
      
      public CustomDescription[] newArray(int param1Int) {
        return new CustomDescription[param1Int];
      }
    };
  
  private final SparseArray<InternalOnClickAction> mActions;
  
  private final RemoteViews mPresentation;
  
  private final ArrayList<Pair<Integer, InternalTransformation>> mTransformations;
  
  private final ArrayList<Pair<InternalValidator, BatchUpdates>> mUpdates;
}
