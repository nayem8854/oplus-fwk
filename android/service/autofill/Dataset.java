package android.service.autofill;

import android.annotation.SystemApi;
import android.content.IntentSender;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.autofill.AutofillId;
import android.view.autofill.AutofillValue;
import android.view.autofill.Helper;
import android.widget.RemoteViews;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;
import java.util.regex.Pattern;

public final class Dataset implements Parcelable {
  private Dataset(Builder paramBuilder) {
    this.mFieldIds = paramBuilder.mFieldIds;
    this.mFieldValues = paramBuilder.mFieldValues;
    this.mFieldPresentations = paramBuilder.mFieldPresentations;
    this.mFieldInlinePresentations = paramBuilder.mFieldInlinePresentations;
    this.mFieldFilters = paramBuilder.mFieldFilters;
    this.mPresentation = paramBuilder.mPresentation;
    this.mInlinePresentation = paramBuilder.mInlinePresentation;
    this.mAuthentication = paramBuilder.mAuthentication;
    this.mId = paramBuilder.mId;
  }
  
  public ArrayList<AutofillId> getFieldIds() {
    return this.mFieldIds;
  }
  
  public ArrayList<AutofillValue> getFieldValues() {
    return this.mFieldValues;
  }
  
  public RemoteViews getFieldPresentation(int paramInt) {
    RemoteViews remoteViews = this.mFieldPresentations.get(paramInt);
    if (remoteViews == null)
      remoteViews = this.mPresentation; 
    return remoteViews;
  }
  
  public InlinePresentation getFieldInlinePresentation(int paramInt) {
    InlinePresentation inlinePresentation = this.mFieldInlinePresentations.get(paramInt);
    if (inlinePresentation == null)
      inlinePresentation = this.mInlinePresentation; 
    return inlinePresentation;
  }
  
  public DatasetFieldFilter getFilter(int paramInt) {
    return this.mFieldFilters.get(paramInt);
  }
  
  public IntentSender getAuthentication() {
    return this.mAuthentication;
  }
  
  public boolean isEmpty() {
    ArrayList<AutofillId> arrayList = this.mFieldIds;
    return (arrayList == null || arrayList.isEmpty());
  }
  
  public String toString() {
    if (!Helper.sDebug)
      return super.toString(); 
    StringBuilder stringBuilder = new StringBuilder("Dataset[");
    if (this.mId == null) {
      stringBuilder.append("noId");
    } else {
      stringBuilder.append("id=");
      stringBuilder.append(this.mId.length());
      stringBuilder.append("_chars");
    } 
    if (this.mFieldIds != null) {
      stringBuilder.append(", fieldIds=");
      stringBuilder.append(this.mFieldIds);
    } 
    if (this.mFieldValues != null) {
      stringBuilder.append(", fieldValues=");
      stringBuilder.append(this.mFieldValues);
    } 
    if (this.mFieldPresentations != null) {
      stringBuilder.append(", fieldPresentations=");
      stringBuilder.append(this.mFieldPresentations.size());
    } 
    if (this.mFieldInlinePresentations != null) {
      stringBuilder.append(", fieldInlinePresentations=");
      stringBuilder.append(this.mFieldInlinePresentations.size());
    } 
    if (this.mFieldFilters != null) {
      stringBuilder.append(", fieldFilters=");
      stringBuilder.append(this.mFieldFilters.size());
    } 
    if (this.mPresentation != null)
      stringBuilder.append(", hasPresentation"); 
    if (this.mInlinePresentation != null)
      stringBuilder.append(", hasInlinePresentation"); 
    if (this.mAuthentication != null)
      stringBuilder.append(", hasAuthentication"); 
    stringBuilder.append(']');
    return stringBuilder.toString();
  }
  
  public String getId() {
    return this.mId;
  }
  
  class Builder {
    private IntentSender mAuthentication;
    
    private boolean mDestroyed;
    
    private ArrayList<Dataset.DatasetFieldFilter> mFieldFilters;
    
    private ArrayList<AutofillId> mFieldIds;
    
    private ArrayList<InlinePresentation> mFieldInlinePresentations;
    
    private ArrayList<RemoteViews> mFieldPresentations;
    
    private ArrayList<AutofillValue> mFieldValues;
    
    private String mId;
    
    private InlinePresentation mInlinePresentation;
    
    private RemoteViews mPresentation;
    
    public Builder(Dataset this$0) {
      Preconditions.checkNotNull(this$0, "presentation must be non-null");
      this.mPresentation = (RemoteViews)this$0;
    }
    
    @SystemApi
    public Builder(Dataset this$0) {
      Preconditions.checkNotNull(this$0, "inlinePresentation must be non-null");
      this.mInlinePresentation = (InlinePresentation)this$0;
    }
    
    public Builder() {}
    
    public Builder setInlinePresentation(InlinePresentation param1InlinePresentation) {
      throwIfDestroyed();
      Preconditions.checkNotNull(param1InlinePresentation, "inlinePresentation must be non-null");
      this.mInlinePresentation = param1InlinePresentation;
      return this;
    }
    
    public Builder setAuthentication(IntentSender param1IntentSender) {
      throwIfDestroyed();
      this.mAuthentication = param1IntentSender;
      return this;
    }
    
    public Builder setId(String param1String) {
      throwIfDestroyed();
      this.mId = param1String;
      return this;
    }
    
    public Builder setValue(AutofillId param1AutofillId, AutofillValue param1AutofillValue) {
      throwIfDestroyed();
      setLifeTheUniverseAndEverything(param1AutofillId, param1AutofillValue, null, null, null);
      return this;
    }
    
    public Builder setValue(AutofillId param1AutofillId, AutofillValue param1AutofillValue, RemoteViews param1RemoteViews) {
      throwIfDestroyed();
      Preconditions.checkNotNull(param1RemoteViews, "presentation cannot be null");
      setLifeTheUniverseAndEverything(param1AutofillId, param1AutofillValue, param1RemoteViews, null, null);
      return this;
    }
    
    public Builder setValue(AutofillId param1AutofillId, AutofillValue param1AutofillValue, Pattern param1Pattern) {
      boolean bool;
      throwIfDestroyed();
      if (this.mPresentation != null) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkState(bool, "Dataset presentation not set on constructor");
      setLifeTheUniverseAndEverything(param1AutofillId, param1AutofillValue, null, null, new Dataset.DatasetFieldFilter(param1Pattern));
      return this;
    }
    
    public Builder setValue(AutofillId param1AutofillId, AutofillValue param1AutofillValue, Pattern param1Pattern, RemoteViews param1RemoteViews) {
      throwIfDestroyed();
      Preconditions.checkNotNull(param1RemoteViews, "presentation cannot be null");
      setLifeTheUniverseAndEverything(param1AutofillId, param1AutofillValue, param1RemoteViews, null, new Dataset.DatasetFieldFilter(param1Pattern));
      return this;
    }
    
    public Builder setValue(AutofillId param1AutofillId, AutofillValue param1AutofillValue, RemoteViews param1RemoteViews, InlinePresentation param1InlinePresentation) {
      throwIfDestroyed();
      Preconditions.checkNotNull(param1RemoteViews, "presentation cannot be null");
      Preconditions.checkNotNull(param1InlinePresentation, "inlinePresentation cannot be null");
      setLifeTheUniverseAndEverything(param1AutofillId, param1AutofillValue, param1RemoteViews, param1InlinePresentation, null);
      return this;
    }
    
    public Builder setValue(AutofillId param1AutofillId, AutofillValue param1AutofillValue, Pattern param1Pattern, RemoteViews param1RemoteViews, InlinePresentation param1InlinePresentation) {
      throwIfDestroyed();
      Preconditions.checkNotNull(param1RemoteViews, "presentation cannot be null");
      Preconditions.checkNotNull(param1InlinePresentation, "inlinePresentation cannot be null");
      setLifeTheUniverseAndEverything(param1AutofillId, param1AutofillValue, param1RemoteViews, param1InlinePresentation, new Dataset.DatasetFieldFilter(param1Pattern));
      return this;
    }
    
    @SystemApi
    public Builder setFieldInlinePresentation(AutofillId param1AutofillId, AutofillValue param1AutofillValue, Pattern param1Pattern, InlinePresentation param1InlinePresentation) {
      throwIfDestroyed();
      Preconditions.checkNotNull(param1InlinePresentation, "inlinePresentation cannot be null");
      setLifeTheUniverseAndEverything(param1AutofillId, param1AutofillValue, null, param1InlinePresentation, new Dataset.DatasetFieldFilter(param1Pattern));
      return this;
    }
    
    private void setLifeTheUniverseAndEverything(AutofillId param1AutofillId, AutofillValue param1AutofillValue, RemoteViews param1RemoteViews, InlinePresentation param1InlinePresentation, Dataset.DatasetFieldFilter param1DatasetFieldFilter) {
      Preconditions.checkNotNull(param1AutofillId, "id cannot be null");
      ArrayList<AutofillId> arrayList = this.mFieldIds;
      if (arrayList != null) {
        int i = arrayList.indexOf(param1AutofillId);
        if (i >= 0) {
          this.mFieldValues.set(i, param1AutofillValue);
          this.mFieldPresentations.set(i, param1RemoteViews);
          this.mFieldInlinePresentations.set(i, param1InlinePresentation);
          this.mFieldFilters.set(i, param1DatasetFieldFilter);
          return;
        } 
      } else {
        this.mFieldIds = new ArrayList<>();
        this.mFieldValues = new ArrayList<>();
        this.mFieldPresentations = new ArrayList<>();
        this.mFieldInlinePresentations = new ArrayList<>();
        this.mFieldFilters = new ArrayList<>();
      } 
      this.mFieldIds.add(param1AutofillId);
      this.mFieldValues.add(param1AutofillValue);
      this.mFieldPresentations.add(param1RemoteViews);
      this.mFieldInlinePresentations.add(param1InlinePresentation);
      this.mFieldFilters.add(param1DatasetFieldFilter);
    }
    
    public Dataset build() {
      throwIfDestroyed();
      this.mDestroyed = true;
      if (this.mFieldIds != null)
        return new Dataset(this); 
      throw new IllegalStateException("at least one value must be set");
    }
    
    private void throwIfDestroyed() {
      if (!this.mDestroyed)
        return; 
      throw new IllegalStateException("Already called #build()");
    }
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.mPresentation, paramInt);
    paramParcel.writeParcelable(this.mInlinePresentation, paramInt);
    paramParcel.writeTypedList(this.mFieldIds, paramInt);
    paramParcel.writeTypedList(this.mFieldValues, paramInt);
    paramParcel.writeTypedList(this.mFieldPresentations, paramInt);
    paramParcel.writeTypedList(this.mFieldInlinePresentations, paramInt);
    paramParcel.writeTypedList(this.mFieldFilters, paramInt);
    paramParcel.writeParcelable((Parcelable)this.mAuthentication, paramInt);
    paramParcel.writeString(this.mId);
  }
  
  public static final Parcelable.Creator<Dataset> CREATOR = new Parcelable.Creator<Dataset>() {
      public Dataset createFromParcel(Parcel param1Parcel) {
        Dataset.Builder builder;
        RemoteViews remoteViews = param1Parcel.<RemoteViews>readParcelable(null);
        InlinePresentation inlinePresentation = param1Parcel.<InlinePresentation>readParcelable(null);
        if (remoteViews != null) {
          if (inlinePresentation == null) {
            builder = new Dataset.Builder(remoteViews);
          } else {
            builder = (new Dataset.Builder((RemoteViews)builder)).setInlinePresentation(inlinePresentation);
          } 
        } else if (inlinePresentation == null) {
          builder = new Dataset.Builder();
        } else {
          builder = new Dataset.Builder(inlinePresentation);
        } 
        Parcelable.Creator<?> creator = AutofillId.CREATOR;
        ArrayList<?> arrayList1 = param1Parcel.createTypedArrayList(creator);
        creator = AutofillValue.CREATOR;
        ArrayList<?> arrayList2 = param1Parcel.createTypedArrayList(creator);
        creator = RemoteViews.CREATOR;
        ArrayList<?> arrayList3 = param1Parcel.createTypedArrayList(creator);
        creator = InlinePresentation.CREATOR;
        ArrayList<?> arrayList4 = param1Parcel.createTypedArrayList(creator);
        creator = Dataset.DatasetFieldFilter.CREATOR;
        ArrayList<?> arrayList5 = param1Parcel.createTypedArrayList(creator);
        int i = arrayList4.size();
        for (byte b = 0; b < arrayList1.size(); b++) {
          AutofillId autofillId = (AutofillId)arrayList1.get(b);
          AutofillValue autofillValue = (AutofillValue)arrayList2.get(b);
          RemoteViews remoteViews1 = (RemoteViews)arrayList3.get(b);
          if (b < i) {
            InlinePresentation inlinePresentation1 = (InlinePresentation)arrayList4.get(b);
          } else {
            creator = null;
          } 
          Dataset.DatasetFieldFilter datasetFieldFilter = (Dataset.DatasetFieldFilter)arrayList5.get(b);
          builder.setLifeTheUniverseAndEverything(autofillId, autofillValue, remoteViews1, (InlinePresentation)creator, datasetFieldFilter);
        } 
        builder.setAuthentication(param1Parcel.<IntentSender>readParcelable(null));
        builder.setId(param1Parcel.readString());
        return builder.build();
      }
      
      public Dataset[] newArray(int param1Int) {
        return new Dataset[param1Int];
      }
    };
  
  private final IntentSender mAuthentication;
  
  private final ArrayList<DatasetFieldFilter> mFieldFilters;
  
  private final ArrayList<AutofillId> mFieldIds;
  
  private final ArrayList<InlinePresentation> mFieldInlinePresentations;
  
  private final ArrayList<RemoteViews> mFieldPresentations;
  
  private final ArrayList<AutofillValue> mFieldValues;
  
  String mId;
  
  private final InlinePresentation mInlinePresentation;
  
  private final RemoteViews mPresentation;
  
  public static final class DatasetFieldFilter implements Parcelable {
    private DatasetFieldFilter(Pattern param1Pattern) {
      this.pattern = param1Pattern;
    }
    
    public String toString() {
      String str;
      if (!Helper.sDebug)
        return super.toString(); 
      if (this.pattern == null) {
        str = "null";
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.pattern.pattern().length());
        stringBuilder.append("_chars");
        str = stringBuilder.toString();
      } 
      return str;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeSerializable(this.pattern);
    }
    
    public static final Parcelable.Creator<DatasetFieldFilter> CREATOR = new Parcelable.Creator<DatasetFieldFilter>() {
        public Dataset.DatasetFieldFilter createFromParcel(Parcel param2Parcel) {
          return new Dataset.DatasetFieldFilter((Pattern)param2Parcel.readSerializable());
        }
        
        public Dataset.DatasetFieldFilter[] newArray(int param2Int) {
          return new Dataset.DatasetFieldFilter[param2Int];
        }
      };
    
    public final Pattern pattern;
  }
  
  class null implements Parcelable.Creator<DatasetFieldFilter> {
    public Dataset.DatasetFieldFilter createFromParcel(Parcel param1Parcel) {
      return new Dataset.DatasetFieldFilter((Pattern)param1Parcel.readSerializable());
    }
    
    public Dataset.DatasetFieldFilter[] newArray(int param1Int) {
      return new Dataset.DatasetFieldFilter[param1Int];
    }
  }
}
