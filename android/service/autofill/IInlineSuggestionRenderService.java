package android.service.autofill;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteCallback;
import android.os.RemoteException;

public interface IInlineSuggestionRenderService extends IInterface {
  void destroySuggestionViews(int paramInt1, int paramInt2) throws RemoteException;
  
  void getInlineSuggestionsRendererInfo(RemoteCallback paramRemoteCallback) throws RemoteException;
  
  void renderSuggestion(IInlineSuggestionUiCallback paramIInlineSuggestionUiCallback, InlinePresentation paramInlinePresentation, int paramInt1, int paramInt2, IBinder paramIBinder, int paramInt3, int paramInt4, int paramInt5) throws RemoteException;
  
  class Default implements IInlineSuggestionRenderService {
    public void renderSuggestion(IInlineSuggestionUiCallback param1IInlineSuggestionUiCallback, InlinePresentation param1InlinePresentation, int param1Int1, int param1Int2, IBinder param1IBinder, int param1Int3, int param1Int4, int param1Int5) throws RemoteException {}
    
    public void getInlineSuggestionsRendererInfo(RemoteCallback param1RemoteCallback) throws RemoteException {}
    
    public void destroySuggestionViews(int param1Int1, int param1Int2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInlineSuggestionRenderService {
    private static final String DESCRIPTOR = "android.service.autofill.IInlineSuggestionRenderService";
    
    static final int TRANSACTION_destroySuggestionViews = 3;
    
    static final int TRANSACTION_getInlineSuggestionsRendererInfo = 2;
    
    static final int TRANSACTION_renderSuggestion = 1;
    
    public Stub() {
      attachInterface(this, "android.service.autofill.IInlineSuggestionRenderService");
    }
    
    public static IInlineSuggestionRenderService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.service.autofill.IInlineSuggestionRenderService");
      if (iInterface != null && iInterface instanceof IInlineSuggestionRenderService)
        return (IInlineSuggestionRenderService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "destroySuggestionViews";
        } 
        return "getInlineSuggestionsRendererInfo";
      } 
      return "renderSuggestion";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.service.autofill.IInlineSuggestionRenderService");
            return true;
          } 
          param1Parcel1.enforceInterface("android.service.autofill.IInlineSuggestionRenderService");
          param1Int1 = param1Parcel1.readInt();
          param1Int2 = param1Parcel1.readInt();
          destroySuggestionViews(param1Int1, param1Int2);
          return true;
        } 
        param1Parcel1.enforceInterface("android.service.autofill.IInlineSuggestionRenderService");
        if (param1Parcel1.readInt() != 0) {
          RemoteCallback remoteCallback = RemoteCallback.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        getInlineSuggestionsRendererInfo((RemoteCallback)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.service.autofill.IInlineSuggestionRenderService");
      IInlineSuggestionUiCallback iInlineSuggestionUiCallback = IInlineSuggestionUiCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
      if (param1Parcel1.readInt() != 0) {
        InlinePresentation inlinePresentation = InlinePresentation.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      param1Int1 = param1Parcel1.readInt();
      int i = param1Parcel1.readInt();
      IBinder iBinder = param1Parcel1.readStrongBinder();
      param1Int2 = param1Parcel1.readInt();
      int j = param1Parcel1.readInt();
      int k = param1Parcel1.readInt();
      renderSuggestion(iInlineSuggestionUiCallback, (InlinePresentation)param1Parcel2, param1Int1, i, iBinder, param1Int2, j, k);
      return true;
    }
    
    private static class Proxy implements IInlineSuggestionRenderService {
      public static IInlineSuggestionRenderService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.service.autofill.IInlineSuggestionRenderService";
      }
      
      public void renderSuggestion(IInlineSuggestionUiCallback param2IInlineSuggestionUiCallback, InlinePresentation param2InlinePresentation, int param2Int1, int param2Int2, IBinder param2IBinder, int param2Int3, int param2Int4, int param2Int5) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.service.autofill.IInlineSuggestionRenderService");
          if (param2IInlineSuggestionUiCallback != null) {
            iBinder = param2IInlineSuggestionUiCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2InlinePresentation != null) {
            parcel.writeInt(1);
            param2InlinePresentation.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          try {
            parcel.writeInt(param2Int1);
            try {
              parcel.writeInt(param2Int2);
              try {
                parcel.writeStrongBinder(param2IBinder);
                try {
                  parcel.writeInt(param2Int3);
                  parcel.writeInt(param2Int4);
                  parcel.writeInt(param2Int5);
                  boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
                  if (!bool && IInlineSuggestionRenderService.Stub.getDefaultImpl() != null) {
                    IInlineSuggestionRenderService.Stub.getDefaultImpl().renderSuggestion(param2IInlineSuggestionUiCallback, param2InlinePresentation, param2Int1, param2Int2, param2IBinder, param2Int3, param2Int4, param2Int5);
                    parcel.recycle();
                    return;
                  } 
                  parcel.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2IInlineSuggestionUiCallback;
      }
      
      public void getInlineSuggestionsRendererInfo(RemoteCallback param2RemoteCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.autofill.IInlineSuggestionRenderService");
          if (param2RemoteCallback != null) {
            parcel.writeInt(1);
            param2RemoteCallback.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IInlineSuggestionRenderService.Stub.getDefaultImpl() != null) {
            IInlineSuggestionRenderService.Stub.getDefaultImpl().getInlineSuggestionsRendererInfo(param2RemoteCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void destroySuggestionViews(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.service.autofill.IInlineSuggestionRenderService");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IInlineSuggestionRenderService.Stub.getDefaultImpl() != null) {
            IInlineSuggestionRenderService.Stub.getDefaultImpl().destroySuggestionViews(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInlineSuggestionRenderService param1IInlineSuggestionRenderService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInlineSuggestionRenderService != null) {
          Proxy.sDefaultImpl = param1IInlineSuggestionRenderService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInlineSuggestionRenderService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
