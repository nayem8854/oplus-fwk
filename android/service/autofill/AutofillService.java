package android.service.autofill;

import android.app.Service;
import android.content.Intent;
import android.os.BaseBundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.IBinder;
import android.os.ICancellationSignal;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import android.view.autofill.AutofillManager;
import com.android.internal.util.function.QuadConsumer;
import com.android.internal.util.function.TriConsumer;
import com.android.internal.util.function.pooled.PooledLambda;
import java.util.function.Consumer;

public abstract class AutofillService extends Service {
  public static final String SERVICE_INTERFACE = "android.service.autofill.AutofillService";
  
  public static final String SERVICE_META_DATA = "android.autofill";
  
  private static final String TAG = "AutofillService";
  
  private Handler mHandler;
  
  private final IAutoFillService mInterface = new IAutoFillService.Stub() {
      final AutofillService this$0;
      
      public void onConnectedStateChanged(boolean param1Boolean) {
        -$.Lambda.eWz26esczusoIA84WEwFlxQuDGQ eWz26esczusoIA84WEwFlxQuDGQ;
        Handler handler = AutofillService.this.mHandler;
        if (param1Boolean) {
          -$.Lambda.amIBeR2CTPTUHkT8htLcarZmUYc amIBeR2CTPTUHkT8htLcarZmUYc = _$$Lambda$amIBeR2CTPTUHkT8htLcarZmUYc.INSTANCE;
        } else {
          eWz26esczusoIA84WEwFlxQuDGQ = _$$Lambda$eWz26esczusoIA84WEwFlxQuDGQ.INSTANCE;
        } 
        AutofillService autofillService = AutofillService.this;
        handler.sendMessage(PooledLambda.obtainMessage((Consumer)eWz26esczusoIA84WEwFlxQuDGQ, autofillService));
      }
      
      public void onFillRequest(FillRequest param1FillRequest, IFillCallback param1IFillCallback) {
        ICancellationSignal iCancellationSignal = CancellationSignal.createTransport();
        try {
          param1IFillCallback.onCancellable(iCancellationSignal);
        } catch (RemoteException remoteException) {
          remoteException.rethrowFromSystemServer();
        } 
        Handler handler = AutofillService.this.mHandler;
        -$.Lambda.I0gCKFrBTO70VZfSZTq2fj-wyG8 i0gCKFrBTO70VZfSZTq2fj-wyG8 = _$$Lambda$I0gCKFrBTO70VZfSZTq2fj_wyG8.INSTANCE;
        AutofillService autofillService = AutofillService.this;
        CancellationSignal cancellationSignal = CancellationSignal.fromTransport(iCancellationSignal);
        FillCallback fillCallback = new FillCallback(param1IFillCallback, param1FillRequest.getId());
        handler.sendMessage(PooledLambda.obtainMessage((QuadConsumer)i0gCKFrBTO70VZfSZTq2fj-wyG8, autofillService, param1FillRequest, cancellationSignal, fillCallback));
      }
      
      public void onSaveRequest(SaveRequest param1SaveRequest, ISaveCallback param1ISaveCallback) {
        AutofillService.this.mHandler.sendMessage(PooledLambda.obtainMessage((TriConsumer)_$$Lambda$KrOZIsyY_3lh3prHWFldsWopHBw.INSTANCE, AutofillService.this, param1SaveRequest, new SaveCallback(param1ISaveCallback)));
      }
    };
  
  public void onCreate() {
    super.onCreate();
    this.mHandler = new Handler(Looper.getMainLooper(), null, true);
    BaseBundle.setShouldDefuse(true);
  }
  
  public final IBinder onBind(Intent paramIntent) {
    if ("android.service.autofill.AutofillService".equals(paramIntent.getAction()))
      return this.mInterface.asBinder(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Tried to bind to wrong intent (should be android.service.autofill.AutofillService: ");
    stringBuilder.append(paramIntent);
    Log.w("AutofillService", stringBuilder.toString());
    return null;
  }
  
  public void onConnected() {}
  
  public void onDisconnected() {}
  
  public final FillEventHistory getFillEventHistory() {
    AutofillManager autofillManager = (AutofillManager)getSystemService(AutofillManager.class);
    if (autofillManager == null)
      return null; 
    return autofillManager.getFillEventHistory();
  }
  
  public abstract void onFillRequest(FillRequest paramFillRequest, CancellationSignal paramCancellationSignal, FillCallback paramFillCallback);
  
  public abstract void onSaveRequest(SaveRequest paramSaveRequest, SaveCallback paramSaveCallback);
}
