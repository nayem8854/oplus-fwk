package android.service.autofill;

import android.view.autofill.AutofillId;
import android.view.autofill.AutofillValue;

public interface ValueFinder {
  default String findByAutofillId(AutofillId paramAutofillId) {
    AutofillValue autofillValue = findRawValueByAutofillId(paramAutofillId);
    return (autofillValue == null || !autofillValue.isText()) ? null : autofillValue.getTextValue().toString();
  }
  
  AutofillValue findRawValueByAutofillId(AutofillId paramAutofillId);
}
