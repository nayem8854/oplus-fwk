package android.service.autofill;

import android.icu.text.DateFormat;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.view.autofill.AutofillId;
import android.view.autofill.AutofillValue;
import android.view.autofill.Helper;
import android.widget.RemoteViews;
import com.android.internal.util.Preconditions;
import java.io.Serializable;
import java.util.Date;

public final class DateTransformation extends InternalTransformation implements Transformation, Parcelable {
  public DateTransformation(AutofillId paramAutofillId, DateFormat paramDateFormat) {
    this.mFieldId = (AutofillId)Preconditions.checkNotNull(paramAutofillId);
    this.mDateFormat = (DateFormat)Preconditions.checkNotNull(paramDateFormat);
  }
  
  public void apply(ValueFinder paramValueFinder, RemoteViews paramRemoteViews, int paramInt) throws Exception {
    StringBuilder stringBuilder1, stringBuilder2;
    AutofillValue autofillValue = paramValueFinder.findRawValueByAutofillId(this.mFieldId);
    if (autofillValue == null) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("No value for id ");
      stringBuilder1.append(this.mFieldId);
      Log.w("DateTransformation", stringBuilder1.toString());
      return;
    } 
    if (!stringBuilder1.isDate()) {
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append("Value for ");
      stringBuilder2.append(this.mFieldId);
      stringBuilder2.append(" is not date: ");
      stringBuilder2.append(stringBuilder1);
      Log.w("DateTransformation", stringBuilder2.toString());
      return;
    } 
    try {
      Date date = new Date();
      this(stringBuilder1.getDateValue());
      String str = this.mDateFormat.format(date);
      if (Helper.sDebug) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Transformed ");
        stringBuilder.append(date);
        stringBuilder.append(" to ");
        stringBuilder.append(str);
        Log.d("DateTransformation", stringBuilder.toString());
      } 
      stringBuilder2.setCharSequence(paramInt, "setText", str);
    } catch (Exception exception) {
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append("Could not apply ");
      stringBuilder2.append(this.mDateFormat);
      stringBuilder2.append(" to ");
      stringBuilder2.append(stringBuilder1);
      stringBuilder2.append(": ");
      stringBuilder2.append(exception);
      Log.w("DateTransformation", stringBuilder2.toString());
    } 
  }
  
  public String toString() {
    if (!Helper.sDebug)
      return super.toString(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("DateTransformation: [id=");
    stringBuilder.append(this.mFieldId);
    stringBuilder.append(", format=");
    stringBuilder.append(this.mDateFormat);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.mFieldId, paramInt);
    paramParcel.writeSerializable((Serializable)this.mDateFormat);
  }
  
  public static final Parcelable.Creator<DateTransformation> CREATOR = (Parcelable.Creator<DateTransformation>)new Object();
  
  private static final String TAG = "DateTransformation";
  
  private final DateFormat mDateFormat;
  
  private final AutofillId mFieldId;
}
