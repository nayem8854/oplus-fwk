package android.service.autofill;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.view.autofill.AutofillId;
import android.view.autofill.Helper;
import com.android.internal.util.Preconditions;
import java.util.Arrays;

public final class LuhnChecksumValidator extends InternalValidator implements Validator, Parcelable {
  public LuhnChecksumValidator(AutofillId... paramVarArgs) {
    this.mIds = (AutofillId[])Preconditions.checkArrayElementsNotNull((Object[])paramVarArgs, "ids");
  }
  
  private static boolean isLuhnChecksumValid(String paramString) {
    int i = 0;
    int j = 0;
    int k = paramString.length();
    boolean bool = true;
    int m = k - 1;
    while (true) {
      boolean bool1 = false;
      if (m >= 0) {
        int n = paramString.charAt(m) - 48;
        int i1 = i;
        k = j;
        if (n >= 0)
          if (n > 9) {
            i1 = i;
            k = j;
          } else {
            if (j) {
              i1 = n * 2;
              k = i1;
              if (i1 > 9)
                k = i1 - 9; 
            } else {
              k = n;
            } 
            i1 = i + k;
            k = bool1;
            if (!j)
              k = 1; 
          }  
        m--;
        i = i1;
        j = k;
        continue;
      } 
      break;
    } 
    if (i % 10 != 0)
      bool = false; 
    return bool;
  }
  
  public boolean isValid(ValueFinder paramValueFinder) {
    AutofillId[] arrayOfAutofillId = this.mIds;
    if (arrayOfAutofillId == null || arrayOfAutofillId.length == 0)
      return false; 
    StringBuilder stringBuilder = new StringBuilder();
    for (AutofillId autofillId : this.mIds) {
      String str1 = paramValueFinder.findByAutofillId(autofillId);
      if (str1 == null) {
        if (Helper.sDebug) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("No partial number for id ");
          stringBuilder1.append(autofillId);
          Log.d("LuhnChecksumValidator", stringBuilder1.toString());
        } 
        return false;
      } 
      stringBuilder.append(str1);
    } 
    String str = stringBuilder.toString();
    boolean bool = isLuhnChecksumValid(str);
    if (Helper.sDebug) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("isValid(");
      stringBuilder1.append(str.length());
      stringBuilder1.append(" chars): ");
      stringBuilder1.append(bool);
      Log.d("LuhnChecksumValidator", stringBuilder1.toString());
    } 
    return bool;
  }
  
  public String toString() {
    if (!Helper.sDebug)
      return super.toString(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("LuhnChecksumValidator: [ids=");
    stringBuilder.append(Arrays.toString((Object[])this.mIds));
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelableArray(this.mIds, paramInt);
  }
  
  public static final Parcelable.Creator<LuhnChecksumValidator> CREATOR = (Parcelable.Creator<LuhnChecksumValidator>)new Object();
  
  private static final String TAG = "LuhnChecksumValidator";
  
  private final AutofillId[] mIds;
}
