package android.inputmethodservice;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.view.InputChannel;
import android.view.KeyEvent;
import com.android.internal.inputmethod.IMultiClientInputMethod;
import com.android.internal.inputmethod.IMultiClientInputMethodPrivilegedOperations;
import com.android.internal.inputmethod.IMultiClientInputMethodSession;
import com.android.internal.inputmethod.MultiClientInputMethodPrivilegedOperations;
import com.android.internal.view.IInputMethodSession;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;

final class MultiClientInputMethodServiceDelegateImpl {
  private static final String TAG = "MultiClientInputMethodServiceDelegateImpl";
  
  private final Context mContext;
  
  private int mInitializationPhase;
  
  private final Object mLock = new Object();
  
  private final MultiClientInputMethodPrivilegedOperations mPrivOps = new MultiClientInputMethodPrivilegedOperations();
  
  private final MultiClientInputMethodServiceDelegate.ServiceCallback mServiceCallback;
  
  MultiClientInputMethodServiceDelegateImpl(Context paramContext, MultiClientInputMethodServiceDelegate.ServiceCallback paramServiceCallback) {
    this.mInitializationPhase = 1;
    this.mContext = paramContext;
    this.mServiceCallback = paramServiceCallback;
  }
  
  void onDestroy() {
    synchronized (this.mLock) {
      int i = this.mInitializationPhase;
      if (i != 1 && i != 4) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("unexpected state=");
        stringBuilder.append(this.mInitializationPhase);
        Log.e("MultiClientInputMethodServiceDelegateImpl", stringBuilder.toString());
      } else {
        this.mInitializationPhase = 5;
      } 
      return;
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  private static @interface InitializationPhase {
    public static final int INITIALIZE_CALLED = 3;
    
    public static final int INSTANTIATED = 1;
    
    public static final int ON_BIND_CALLED = 2;
    
    public static final int ON_DESTROY_CALLED = 5;
    
    public static final int ON_UNBIND_CALLED = 4;
  }
  
  class ServiceImpl extends IMultiClientInputMethod.Stub {
    private final WeakReference<MultiClientInputMethodServiceDelegateImpl> mImpl;
    
    ServiceImpl(MultiClientInputMethodServiceDelegateImpl this$0) {
      this.mImpl = new WeakReference<>(this$0);
    }
    
    public void initialize(IMultiClientInputMethodPrivilegedOperations param1IMultiClientInputMethodPrivilegedOperations) {
      MultiClientInputMethodServiceDelegateImpl multiClientInputMethodServiceDelegateImpl = this.mImpl.get();
      if (multiClientInputMethodServiceDelegateImpl == null)
        return; 
      synchronized (multiClientInputMethodServiceDelegateImpl.mLock) {
        StringBuilder stringBuilder;
        if (multiClientInputMethodServiceDelegateImpl.mInitializationPhase != 2) {
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("unexpected state=");
          stringBuilder.append(multiClientInputMethodServiceDelegateImpl.mInitializationPhase);
          Log.e("MultiClientInputMethodServiceDelegateImpl", stringBuilder.toString());
        } else {
          multiClientInputMethodServiceDelegateImpl.mPrivOps.set((IMultiClientInputMethodPrivilegedOperations)stringBuilder);
          MultiClientInputMethodServiceDelegateImpl.access$102(multiClientInputMethodServiceDelegateImpl, 3);
          multiClientInputMethodServiceDelegateImpl.mServiceCallback.initialized();
        } 
        return;
      } 
    }
    
    public void addClient(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      MultiClientInputMethodServiceDelegateImpl multiClientInputMethodServiceDelegateImpl = this.mImpl.get();
      if (multiClientInputMethodServiceDelegateImpl == null)
        return; 
      multiClientInputMethodServiceDelegateImpl.mServiceCallback.addClient(param1Int1, param1Int2, param1Int3, param1Int4);
    }
    
    public void removeClient(int param1Int) {
      MultiClientInputMethodServiceDelegateImpl multiClientInputMethodServiceDelegateImpl = this.mImpl.get();
      if (multiClientInputMethodServiceDelegateImpl == null)
        return; 
      multiClientInputMethodServiceDelegateImpl.mServiceCallback.removeClient(param1Int);
    }
  }
  
  IBinder onBind(Intent paramIntent) {
    synchronized (this.mLock) {
      if (this.mInitializationPhase != 1) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("unexpected state=");
        stringBuilder.append(this.mInitializationPhase);
        Log.e("MultiClientInputMethodServiceDelegateImpl", stringBuilder.toString());
        return null;
      } 
      this.mInitializationPhase = 2;
      ServiceImpl serviceImpl = new ServiceImpl();
      this(this);
      return (IBinder)serviceImpl;
    } 
  }
  
  boolean onUnbind(Intent paramIntent) {
    synchronized (this.mLock) {
      int i = this.mInitializationPhase;
      if (i != 2 && i != 3) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("unexpected state=");
        stringBuilder.append(this.mInitializationPhase);
        Log.e("MultiClientInputMethodServiceDelegateImpl", stringBuilder.toString());
      } else {
        this.mInitializationPhase = 4;
        this.mPrivOps.dispose();
      } 
      return false;
    } 
  }
  
  IBinder createInputMethodWindowToken(int paramInt) {
    return this.mPrivOps.createInputMethodWindowToken(paramInt);
  }
  
  void acceptClient(int paramInt, MultiClientInputMethodServiceDelegate.ClientCallback paramClientCallback, KeyEvent.DispatcherState paramDispatcherState, Looper paramLooper) {
    InputChannel[] arrayOfInputChannel = InputChannel.openInputChannelPair("MSIMS-session");
    InputChannel inputChannel1 = arrayOfInputChannel[0];
    InputChannel inputChannel2 = arrayOfInputChannel[1];
    try {
      MultiClientInputMethodClientCallbackAdaptor multiClientInputMethodClientCallbackAdaptor = new MultiClientInputMethodClientCallbackAdaptor();
      this(paramClientCallback, paramLooper, paramDispatcherState, inputChannel2);
      MultiClientInputMethodPrivilegedOperations multiClientInputMethodPrivilegedOperations = this.mPrivOps;
      IInputMethodSession.Stub stub = multiClientInputMethodClientCallbackAdaptor.createIInputMethodSession();
      IMultiClientInputMethodSession.Stub stub1 = multiClientInputMethodClientCallbackAdaptor.createIMultiClientInputMethodSession();
      multiClientInputMethodPrivilegedOperations.acceptClient(paramInt, (IInputMethodSession)stub, (IMultiClientInputMethodSession)stub1, inputChannel1);
      return;
    } finally {
      inputChannel1.dispose();
    } 
  }
  
  void reportImeWindowTarget(int paramInt1, int paramInt2, IBinder paramIBinder) {
    this.mPrivOps.reportImeWindowTarget(paramInt1, paramInt2, paramIBinder);
  }
  
  boolean isUidAllowedOnDisplay(int paramInt1, int paramInt2) {
    return this.mPrivOps.isUidAllowedOnDisplay(paramInt1, paramInt2);
  }
  
  void setActive(int paramInt, boolean paramBoolean) {
    this.mPrivOps.setActive(paramInt, paramBoolean);
  }
}
