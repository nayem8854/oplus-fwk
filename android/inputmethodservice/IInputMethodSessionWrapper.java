package android.inputmethodservice;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.SparseArray;
import android.view.InputChannel;
import android.view.InputEvent;
import android.view.InputEventReceiver;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CursorAnchorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.InputMethodSession;
import com.android.internal.os.HandlerCaller;
import com.android.internal.os.SomeArgs;
import com.android.internal.view.IInputMethodSession;

class IInputMethodSessionWrapper extends IInputMethodSession.Stub implements HandlerCaller.Callback {
  private static final int DO_APP_PRIVATE_COMMAND = 100;
  
  private static final int DO_DISPLAY_COMPLETIONS = 65;
  
  private static final int DO_FINISH_SESSION = 110;
  
  private static final int DO_NOTIFY_IME_HIDDEN = 120;
  
  private static final int DO_REMOVE_IME_SURFACE = 130;
  
  private static final int DO_TOGGLE_SOFT_INPUT = 105;
  
  private static final int DO_UPDATE_CURSOR = 95;
  
  private static final int DO_UPDATE_CURSOR_ANCHOR_INFO = 99;
  
  private static final int DO_UPDATE_EXTRACTED_TEXT = 67;
  
  private static final int DO_UPDATE_SELECTION = 90;
  
  private static final int DO_VIEW_CLICKED = 115;
  
  private static final String TAG = "InputMethodWrapper";
  
  HandlerCaller mCaller;
  
  InputChannel mChannel;
  
  InputMethodSession mInputMethodSession;
  
  ImeInputEventReceiver mReceiver;
  
  public IInputMethodSessionWrapper(Context paramContext, InputMethodSession paramInputMethodSession, InputChannel paramInputChannel) {
    this.mCaller = new HandlerCaller(paramContext, null, this, true);
    this.mInputMethodSession = paramInputMethodSession;
    this.mChannel = paramInputChannel;
    if (paramInputChannel != null)
      this.mReceiver = new ImeInputEventReceiver(paramInputChannel, paramContext.getMainLooper()); 
  }
  
  public InputMethodSession getInternalInputMethodSession() {
    return this.mInputMethodSession;
  }
  
  public void executeMessage(Message paramMessage) {
    SomeArgs someArgs;
    if (this.mInputMethodSession == null) {
      int j = paramMessage.what;
      if (j == 90 || j == 100) {
        someArgs = (SomeArgs)paramMessage.obj;
        someArgs.recycle();
      } 
      return;
    } 
    int i = ((Message)someArgs).what;
    if (i != 65) {
      if (i != 67) {
        if (i != 90) {
          if (i != 95) {
            if (i != 105) {
              if (i != 110) {
                if (i != 115) {
                  if (i != 120) {
                    if (i != 130) {
                      if (i != 99) {
                        if (i != 100) {
                          StringBuilder stringBuilder = new StringBuilder();
                          stringBuilder.append("Unhandled message code: ");
                          stringBuilder.append(((Message)someArgs).what);
                          Log.w("InputMethodWrapper", stringBuilder.toString());
                          return;
                        } 
                        someArgs = (SomeArgs)((Message)someArgs).obj;
                        this.mInputMethodSession.appPrivateCommand((String)someArgs.arg1, (Bundle)someArgs.arg2);
                        someArgs.recycle();
                        return;
                      } 
                      this.mInputMethodSession.updateCursorAnchorInfo((CursorAnchorInfo)((Message)someArgs).obj);
                      return;
                    } 
                    this.mInputMethodSession.removeImeSurface();
                    return;
                  } 
                  this.mInputMethodSession.notifyImeHidden();
                  return;
                } 
                InputMethodSession inputMethodSession = this.mInputMethodSession;
                i = ((Message)someArgs).arg1;
                boolean bool = true;
                if (i != 1)
                  bool = false; 
                inputMethodSession.viewClicked(bool);
                return;
              } 
              doFinishSession();
              return;
            } 
            this.mInputMethodSession.toggleSoftInput(((Message)someArgs).arg1, ((Message)someArgs).arg2);
            return;
          } 
          this.mInputMethodSession.updateCursor((Rect)((Message)someArgs).obj);
          return;
        } 
        someArgs = (SomeArgs)((Message)someArgs).obj;
        this.mInputMethodSession.updateSelection(someArgs.argi1, someArgs.argi2, someArgs.argi3, someArgs.argi4, someArgs.argi5, someArgs.argi6);
        someArgs.recycle();
        return;
      } 
      this.mInputMethodSession.updateExtractedText(((Message)someArgs).arg1, (ExtractedText)((Message)someArgs).obj);
      return;
    } 
    this.mInputMethodSession.displayCompletions((CompletionInfo[])((Message)someArgs).obj);
  }
  
  private void doFinishSession() {
    this.mInputMethodSession = null;
    ImeInputEventReceiver imeInputEventReceiver = this.mReceiver;
    if (imeInputEventReceiver != null) {
      imeInputEventReceiver.dispose();
      this.mReceiver = null;
    } 
    InputChannel inputChannel = this.mChannel;
    if (inputChannel != null) {
      inputChannel.dispose();
      this.mChannel = null;
    } 
  }
  
  public void displayCompletions(CompletionInfo[] paramArrayOfCompletionInfo) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageO(65, paramArrayOfCompletionInfo));
  }
  
  public void updateExtractedText(int paramInt, ExtractedText paramExtractedText) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageIO(67, paramInt, paramExtractedText));
  }
  
  public void updateSelection(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageIIIIII(90, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6));
  }
  
  public void viewClicked(boolean paramBoolean) {
    HandlerCaller handlerCaller = this.mCaller;
    Message message = handlerCaller.obtainMessageI(115, paramBoolean);
    handlerCaller.executeOrSendMessage(message);
  }
  
  public void notifyImeHidden() {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessage(120));
  }
  
  public void removeImeSurface() {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessage(130));
  }
  
  public void updateCursor(Rect paramRect) {
    HandlerCaller handlerCaller = this.mCaller;
    Message message = handlerCaller.obtainMessageO(95, paramRect);
    handlerCaller.executeOrSendMessage(message);
  }
  
  public void updateCursorAnchorInfo(CursorAnchorInfo paramCursorAnchorInfo) {
    HandlerCaller handlerCaller = this.mCaller;
    Message message = handlerCaller.obtainMessageO(99, paramCursorAnchorInfo);
    handlerCaller.executeOrSendMessage(message);
  }
  
  public void appPrivateCommand(String paramString, Bundle paramBundle) {
    HandlerCaller handlerCaller = this.mCaller;
    Message message = handlerCaller.obtainMessageOO(100, paramString, paramBundle);
    handlerCaller.executeOrSendMessage(message);
  }
  
  public void toggleSoftInput(int paramInt1, int paramInt2) {
    HandlerCaller handlerCaller = this.mCaller;
    Message message = handlerCaller.obtainMessageII(105, paramInt1, paramInt2);
    handlerCaller.executeOrSendMessage(message);
  }
  
  public void finishSession() {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessage(110));
  }
  
  class ImeInputEventReceiver extends InputEventReceiver implements InputMethodSession.EventCallback {
    private final SparseArray<InputEvent> mPendingEvents = new SparseArray();
    
    final IInputMethodSessionWrapper this$0;
    
    public ImeInputEventReceiver(InputChannel param1InputChannel, Looper param1Looper) {
      super(param1InputChannel, param1Looper);
    }
    
    public void onInputEvent(InputEvent param1InputEvent) {
      KeyEvent keyEvent;
      if (IInputMethodSessionWrapper.this.mInputMethodSession == null) {
        finishInputEvent(param1InputEvent, false);
        return;
      } 
      int i = param1InputEvent.getSequenceNumber();
      this.mPendingEvents.put(i, param1InputEvent);
      if (param1InputEvent instanceof KeyEvent) {
        keyEvent = (KeyEvent)param1InputEvent;
        IInputMethodSessionWrapper.this.mInputMethodSession.dispatchKeyEvent(i, keyEvent, this);
      } else {
        MotionEvent motionEvent = (MotionEvent)keyEvent;
        if (motionEvent.isFromSource(4)) {
          IInputMethodSessionWrapper.this.mInputMethodSession.dispatchTrackballEvent(i, motionEvent, this);
        } else {
          IInputMethodSessionWrapper.this.mInputMethodSession.dispatchGenericMotionEvent(i, motionEvent, this);
        } 
      } 
    }
    
    public void finishedEvent(int param1Int, boolean param1Boolean) {
      param1Int = this.mPendingEvents.indexOfKey(param1Int);
      if (param1Int >= 0) {
        InputEvent inputEvent = (InputEvent)this.mPendingEvents.valueAt(param1Int);
        this.mPendingEvents.removeAt(param1Int);
        finishInputEvent(inputEvent, param1Boolean);
      } 
    }
  }
}
