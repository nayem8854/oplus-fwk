package android.inputmethodservice;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class CompactExtractEditLayout extends LinearLayout {
  private View mInputExtractAccessories;
  
  private View mInputExtractAction;
  
  private View mInputExtractEditText;
  
  private boolean mPerformLayoutChanges;
  
  public CompactExtractEditLayout(Context paramContext) {
    super(paramContext);
  }
  
  public CompactExtractEditLayout(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public CompactExtractEditLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  protected void onFinishInflate() {
    super.onFinishInflate();
    this.mInputExtractEditText = findViewById(16908325);
    this.mInputExtractAccessories = findViewById(16909078);
    View view = findViewById(16909079);
    if (this.mInputExtractEditText != null && this.mInputExtractAccessories != null && view != null)
      this.mPerformLayoutChanges = true; 
  }
  
  private int applyFractionInt(int paramInt1, int paramInt2) {
    return Math.round(getResources().getFraction(paramInt1, paramInt2, paramInt2));
  }
  
  private static void setLayoutHeight(View paramView, int paramInt) {
    ViewGroup.LayoutParams layoutParams = paramView.getLayoutParams();
    layoutParams.height = paramInt;
    paramView.setLayoutParams(layoutParams);
  }
  
  private static void setLayoutMarginBottom(View paramView, int paramInt) {
    ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)paramView.getLayoutParams();
    marginLayoutParams.bottomMargin = paramInt;
    paramView.setLayoutParams((ViewGroup.LayoutParams)marginLayoutParams);
  }
  
  private void applyProportionalLayout(int paramInt1, int paramInt2) {
    if (getResources().getConfiguration().isScreenRound())
      setGravity(80); 
    setLayoutHeight((View)this, applyFractionInt(18022409, paramInt2));
    int i = applyFractionInt(18022410, paramInt1);
    paramInt1 = applyFractionInt(18022412, paramInt1);
    setPadding(i, 0, paramInt1, 0);
    View view = this.mInputExtractEditText;
    paramInt1 = applyFractionInt(18022413, paramInt2);
    setLayoutMarginBottom(view, paramInt1);
    view = this.mInputExtractAccessories;
    paramInt1 = applyFractionInt(18022408, paramInt2);
    setLayoutMarginBottom(view, paramInt1);
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    if (this.mPerformLayoutChanges) {
      Resources resources = getResources();
      Configuration configuration = resources.getConfiguration();
      DisplayMetrics displayMetrics = resources.getDisplayMetrics();
      int i = displayMetrics.widthPixels;
      int j = displayMetrics.heightPixels;
      int k = j;
      if (configuration.isScreenRound()) {
        k = j;
        if (j < i)
          k = i; 
      } 
      applyProportionalLayout(i, k);
    } 
  }
}
