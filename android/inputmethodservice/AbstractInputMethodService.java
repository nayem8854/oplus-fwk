package android.inputmethodservice;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputContentInfo;
import android.view.inputmethod.InputMethod;
import android.view.inputmethod.InputMethodSession;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public abstract class AbstractInputMethodService extends Service implements KeyEvent.Callback {
  final KeyEvent.DispatcherState mDispatcherState = new KeyEvent.DispatcherState();
  
  private InputMethod mInputMethod;
  
  class AbstractInputMethodImpl implements InputMethod {
    final AbstractInputMethodService this$0;
    
    public void createSession(InputMethod.SessionCallback param1SessionCallback) {
      param1SessionCallback.sessionCreated(AbstractInputMethodService.this.onCreateInputMethodSessionInterface());
    }
    
    public void setSessionEnabled(InputMethodSession param1InputMethodSession, boolean param1Boolean) {
      ((AbstractInputMethodService.AbstractInputMethodSessionImpl)param1InputMethodSession).setEnabled(param1Boolean);
    }
    
    public void revokeSession(InputMethodSession param1InputMethodSession) {
      ((AbstractInputMethodService.AbstractInputMethodSessionImpl)param1InputMethodSession).revokeSelf();
    }
  }
  
  class AbstractInputMethodSessionImpl implements InputMethodSession {
    boolean mEnabled;
    
    boolean mRevoked;
    
    final AbstractInputMethodService this$0;
    
    public AbstractInputMethodSessionImpl() {
      this.mEnabled = true;
    }
    
    public boolean isEnabled() {
      return this.mEnabled;
    }
    
    public boolean isRevoked() {
      return this.mRevoked;
    }
    
    public void setEnabled(boolean param1Boolean) {
      if (!this.mRevoked)
        this.mEnabled = param1Boolean; 
    }
    
    public void revokeSelf() {
      this.mRevoked = true;
      this.mEnabled = false;
    }
    
    public void dispatchKeyEvent(int param1Int, KeyEvent param1KeyEvent, InputMethodSession.EventCallback param1EventCallback) {
      AbstractInputMethodService abstractInputMethodService = AbstractInputMethodService.this;
      boolean bool = param1KeyEvent.dispatch(abstractInputMethodService, abstractInputMethodService.mDispatcherState, this);
      if (param1EventCallback != null)
        param1EventCallback.finishedEvent(param1Int, bool); 
    }
    
    public void dispatchTrackballEvent(int param1Int, MotionEvent param1MotionEvent, InputMethodSession.EventCallback param1EventCallback) {
      boolean bool = AbstractInputMethodService.this.onTrackballEvent(param1MotionEvent);
      if (param1EventCallback != null)
        param1EventCallback.finishedEvent(param1Int, bool); 
    }
    
    public void dispatchGenericMotionEvent(int param1Int, MotionEvent param1MotionEvent, InputMethodSession.EventCallback param1EventCallback) {
      boolean bool = AbstractInputMethodService.this.onGenericMotionEvent(param1MotionEvent);
      if (param1EventCallback != null)
        param1EventCallback.finishedEvent(param1Int, bool); 
    }
  }
  
  public KeyEvent.DispatcherState getKeyDispatcherState() {
    return this.mDispatcherState;
  }
  
  protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {}
  
  public final IBinder onBind(Intent paramIntent) {
    if (this.mInputMethod == null)
      this.mInputMethod = onCreateInputMethodInterface(); 
    return (IBinder)new IInputMethodWrapper(this, this.mInputMethod);
  }
  
  public boolean onTrackballEvent(MotionEvent paramMotionEvent) {
    return false;
  }
  
  public boolean onGenericMotionEvent(MotionEvent paramMotionEvent) {
    return false;
  }
  
  public void exposeContent(InputContentInfo paramInputContentInfo, InputConnection paramInputConnection) {}
  
  public void notifyUserActionIfNecessary() {}
  
  public final boolean isUiContext() {
    return true;
  }
  
  public abstract AbstractInputMethodImpl onCreateInputMethodInterface();
  
  public abstract AbstractInputMethodSessionImpl onCreateInputMethodSessionInterface();
}
