package android.inputmethodservice;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.ResultReceiver;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CursorAnchorInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

public final class MultiClientInputMethodServiceDelegate {
  public static final int INVALID_CLIENT_ID = -1;
  
  public static final int INVALID_WINDOW_HANDLE = -1;
  
  public static final String SERVICE_INTERFACE = "android.inputmethodservice.MultiClientInputMethodService";
  
  private final MultiClientInputMethodServiceDelegateImpl mImpl;
  
  private MultiClientInputMethodServiceDelegate(Context paramContext, ServiceCallback paramServiceCallback) {
    this.mImpl = new MultiClientInputMethodServiceDelegateImpl(paramContext, paramServiceCallback);
  }
  
  public static MultiClientInputMethodServiceDelegate create(Context paramContext, ServiceCallback paramServiceCallback) {
    return new MultiClientInputMethodServiceDelegate(paramContext, paramServiceCallback);
  }
  
  public void onDestroy() {
    this.mImpl.onDestroy();
  }
  
  public IBinder onBind(Intent paramIntent) {
    return this.mImpl.onBind(paramIntent);
  }
  
  public boolean onUnbind(Intent paramIntent) {
    return this.mImpl.onUnbind(paramIntent);
  }
  
  public IBinder createInputMethodWindowToken(int paramInt) {
    return this.mImpl.createInputMethodWindowToken(paramInt);
  }
  
  public void acceptClient(int paramInt, ClientCallback paramClientCallback, KeyEvent.DispatcherState paramDispatcherState, Looper paramLooper) {
    this.mImpl.acceptClient(paramInt, paramClientCallback, paramDispatcherState, paramLooper);
  }
  
  public void reportImeWindowTarget(int paramInt1, int paramInt2, IBinder paramIBinder) {
    this.mImpl.reportImeWindowTarget(paramInt1, paramInt2, paramIBinder);
  }
  
  public boolean isUidAllowedOnDisplay(int paramInt1, int paramInt2) {
    return this.mImpl.isUidAllowedOnDisplay(paramInt1, paramInt2);
  }
  
  public void setActive(int paramInt, boolean paramBoolean) {
    this.mImpl.setActive(paramInt, paramBoolean);
  }
  
  public static interface ClientCallback {
    void onAppPrivateCommand(String param1String, Bundle param1Bundle);
    
    void onDisplayCompletions(CompletionInfo[] param1ArrayOfCompletionInfo);
    
    void onFinishSession();
    
    boolean onGenericMotionEvent(MotionEvent param1MotionEvent);
    
    void onHideSoftInput(int param1Int, ResultReceiver param1ResultReceiver);
    
    boolean onKeyDown(int param1Int, KeyEvent param1KeyEvent);
    
    boolean onKeyLongPress(int param1Int, KeyEvent param1KeyEvent);
    
    boolean onKeyMultiple(int param1Int, KeyEvent param1KeyEvent);
    
    boolean onKeyUp(int param1Int, KeyEvent param1KeyEvent);
    
    void onShowSoftInput(int param1Int, ResultReceiver param1ResultReceiver);
    
    void onStartInputOrWindowGainedFocus(InputConnection param1InputConnection, EditorInfo param1EditorInfo, int param1Int1, int param1Int2, int param1Int3);
    
    void onToggleSoftInput(int param1Int1, int param1Int2);
    
    boolean onTrackballEvent(MotionEvent param1MotionEvent);
    
    void onUpdateCursorAnchorInfo(CursorAnchorInfo param1CursorAnchorInfo);
    
    void onUpdateSelection(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6);
  }
  
  public static interface ServiceCallback {
    void addClient(int param1Int1, int param1Int2, int param1Int3, int param1Int4);
    
    void initialized();
    
    void removeClient(int param1Int);
  }
}
