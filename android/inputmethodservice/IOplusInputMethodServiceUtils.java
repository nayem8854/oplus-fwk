package android.inputmethodservice;

import android.app.Dialog;
import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.view.WindowInsets;

public interface IOplusInputMethodServiceUtils extends IOplusCommonFeature {
  public static final IOplusInputMethodServiceUtils DEFAULT = (IOplusInputMethodServiceUtils)new Object();
  
  default IOplusInputMethodServiceUtils getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusInputMethodServiceUtils;
  }
  
  default void init(Context paramContext) {}
  
  default boolean getDockSide() {
    return false;
  }
  
  default void onChange(Uri paramUri) {}
  
  default void updateNavigationGuardColor(Dialog paramDialog) {}
  
  default void updateNavigationGuardColorDelay(Dialog paramDialog) {}
  
  default void onComputeRaise(InputMethodService.Insets paramInsets, Dialog paramDialog) {}
  
  default void uploadData(long paramLong) {}
  
  default void onCreateAndRegister(ContentObserver paramContentObserver) {}
  
  default boolean showRaiseKeyboard(WindowInsets paramWindowInsets) {
    return false;
  }
  
  default void onDestroy() {}
  
  default boolean hideImmediately(int paramInt, Dialog paramDialog) {
    return false;
  }
  
  default int changeFlag(int paramInt) {
    return paramInt;
  }
  
  default boolean skipInsetChange(int paramInt) {
    return true;
  }
  
  default boolean shouldPreventTouch() {
    return false;
  }
}
