package android.inputmethodservice;

import android.content.Context;
import android.util.AttributeSet;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class ExtractEditText extends EditText {
  private InputMethodService mIME;
  
  private int mSettingExtractedText;
  
  public ExtractEditText(Context paramContext) {
    super(paramContext, null);
  }
  
  public ExtractEditText(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet, 16842862);
  }
  
  public ExtractEditText(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public ExtractEditText(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  void setIME(InputMethodService paramInputMethodService) {
    this.mIME = paramInputMethodService;
  }
  
  public void startInternalChanges() {
    this.mSettingExtractedText++;
  }
  
  public void finishInternalChanges() {
    this.mSettingExtractedText--;
  }
  
  public void setExtractedText(ExtractedText paramExtractedText) {
    try {
      this.mSettingExtractedText++;
      super.setExtractedText(paramExtractedText);
      return;
    } finally {
      this.mSettingExtractedText--;
    } 
  }
  
  protected void onSelectionChanged(int paramInt1, int paramInt2) {
    if (this.mSettingExtractedText == 0) {
      InputMethodService inputMethodService = this.mIME;
      if (inputMethodService != null && paramInt1 >= 0 && paramInt2 >= 0)
        inputMethodService.onExtractedSelectionChanged(paramInt1, paramInt2); 
    } 
  }
  
  public boolean performClick() {
    if (!super.performClick()) {
      InputMethodService inputMethodService = this.mIME;
      if (inputMethodService != null) {
        inputMethodService.onExtractedTextClicked();
        return true;
      } 
    } 
    return false;
  }
  
  public boolean onTextContextMenuItem(int paramInt) {
    if (paramInt == 16908319 || paramInt == 16908340)
      return super.onTextContextMenuItem(paramInt); 
    InputMethodService inputMethodService = this.mIME;
    if (inputMethodService != null && inputMethodService.onExtractTextContextMenuItem(paramInt)) {
      if (paramInt == 16908321 || paramInt == 16908322 || paramInt == 16908341)
        stopTextActionMode(); 
      return true;
    } 
    return super.onTextContextMenuItem(paramInt);
  }
  
  public boolean isInputMethodTarget() {
    return true;
  }
  
  public boolean hasVerticalScrollBar() {
    boolean bool;
    if (computeVerticalScrollRange() > computeVerticalScrollExtent()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean hasWindowFocus() {
    return isEnabled();
  }
  
  public boolean isFocused() {
    return isEnabled();
  }
  
  public boolean hasFocus() {
    return isEnabled();
  }
  
  protected void viewClicked(InputMethodManager paramInputMethodManager) {
    InputMethodService inputMethodService = this.mIME;
    if (inputMethodService != null)
      inputMethodService.onViewClicked(false); 
  }
  
  public boolean isInExtractedMode() {
    return true;
  }
  
  protected void deleteText_internal(int paramInt1, int paramInt2) {
    this.mIME.onExtractedDeleteText(paramInt1, paramInt2);
  }
  
  protected void replaceText_internal(int paramInt1, int paramInt2, CharSequence paramCharSequence) {
    this.mIME.onExtractedReplaceText(paramInt1, paramInt2, paramCharSequence);
  }
  
  protected void setSpan_internal(Object paramObject, int paramInt1, int paramInt2, int paramInt3) {
    this.mIME.onExtractedSetSpan(paramObject, paramInt1, paramInt2, paramInt3);
  }
  
  protected void setCursorPosition_internal(int paramInt1, int paramInt2) {
    this.mIME.onExtractedSelectionChanged(paramInt1, paramInt2);
  }
}
