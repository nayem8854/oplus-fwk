package android.inputmethodservice;

import android.content.Context;
import android.os.Binder;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.InputChannel;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputBinding;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethod;
import android.view.inputmethod.InputMethodSession;
import android.view.inputmethod.InputMethodSubtype;
import com.android.internal.inputmethod.CancellationGroup;
import com.android.internal.inputmethod.IInputMethodPrivilegedOperations;
import com.android.internal.os.HandlerCaller;
import com.android.internal.os.SomeArgs;
import com.android.internal.view.IInlineSuggestionsRequestCallback;
import com.android.internal.view.IInputContext;
import com.android.internal.view.IInputMethod;
import com.android.internal.view.IInputMethodSession;
import com.android.internal.view.IInputSessionCallback;
import com.android.internal.view.InlineSuggestionsRequestInfo;
import com.android.internal.view.InputConnectionWrapper;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

class IInputMethodWrapper extends IInputMethod.Stub implements HandlerCaller.Callback {
  private static final int DO_CHANGE_INPUTMETHOD_SUBTYPE = 80;
  
  private static final int DO_CREATE_INLINE_SUGGESTIONS_REQUEST = 90;
  
  private static final int DO_CREATE_SESSION = 40;
  
  private static final int DO_DUMP = 1;
  
  private static final int DO_HIDE_SOFT_INPUT = 70;
  
  private static final int DO_INITIALIZE_INTERNAL = 10;
  
  private static final int DO_REVOKE_SESSION = 50;
  
  private static final int DO_SET_INPUT_CONTEXT = 20;
  
  private static final int DO_SET_SESSION_ENABLED = 45;
  
  private static final int DO_SHOW_SOFT_INPUT = 60;
  
  private static final int DO_START_INPUT = 32;
  
  private static final int DO_UNSET_INPUT_CONTEXT = 30;
  
  private static final String TAG = "InputMethodWrapper";
  
  final HandlerCaller mCaller;
  
  CancellationGroup mCancellationGroup = null;
  
  final Context mContext;
  
  final WeakReference<InputMethod> mInputMethod;
  
  final WeakReference<AbstractInputMethodService> mTarget;
  
  final int mTargetSdkVersion;
  
  class InputMethodSessionCallbackWrapper implements InputMethod.SessionCallback {
    final IInputSessionCallback mCb;
    
    final InputChannel mChannel;
    
    final Context mContext;
    
    InputMethodSessionCallbackWrapper(IInputMethodWrapper this$0, InputChannel param1InputChannel, IInputSessionCallback param1IInputSessionCallback) {
      this.mContext = (Context)this$0;
      this.mChannel = param1InputChannel;
      this.mCb = param1IInputSessionCallback;
    }
    
    public void sessionCreated(InputMethodSession param1InputMethodSession) {
      if (param1InputMethodSession != null) {
        try {
          IInputMethodSessionWrapper iInputMethodSessionWrapper = new IInputMethodSessionWrapper();
          this(this.mContext, param1InputMethodSession, this.mChannel);
          this.mCb.sessionCreated((IInputMethodSession)iInputMethodSessionWrapper);
        } catch (RemoteException remoteException) {}
      } else {
        if (this.mChannel != null)
          this.mChannel.dispose(); 
        this.mCb.sessionCreated(null);
      } 
    }
  }
  
  public IInputMethodWrapper(AbstractInputMethodService paramAbstractInputMethodService, InputMethod paramInputMethod) {
    this.mTarget = new WeakReference<>(paramAbstractInputMethodService);
    Context context = paramAbstractInputMethodService.getApplicationContext();
    this.mCaller = new HandlerCaller(context, null, this, true);
    this.mInputMethod = new WeakReference<>(paramInputMethod);
    this.mTargetSdkVersion = (paramAbstractInputMethodService.getApplicationInfo()).targetSdkVersion;
  }
  
  public void executeMessage(Message paramMessage) {
    StringBuilder stringBuilder;
    SomeArgs someArgs2;
    InputMethodSession inputMethodSession;
    SomeArgs someArgs1, someArgs3;
    IBinder iBinder;
    IInputContext iInputContext;
    EditorInfo editorInfo;
    boolean bool2;
    InputMethod inputMethod = this.mInputMethod.get();
    boolean bool1 = true;
    if (inputMethod == null && paramMessage.what != 1) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Input method reference was null, ignoring message: ");
      stringBuilder1.append(paramMessage.what);
      Log.w("InputMethodWrapper", stringBuilder1.toString());
      return;
    } 
    switch (paramMessage.what) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unhandled message code: ");
        stringBuilder.append(paramMessage.what);
        Log.w("InputMethodWrapper", stringBuilder.toString());
        return;
      case 90:
        null = (SomeArgs)paramMessage.obj;
        stringBuilder.onCreateInlineSuggestionsRequest((InlineSuggestionsRequestInfo)null.arg1, (IInlineSuggestionsRequestCallback)null.arg2);
        null.recycle();
        return;
      case 80:
        stringBuilder.changeInputMethodSubtype((InputMethodSubtype)((Message)null).obj);
        return;
      case 70:
        someArgs2 = (SomeArgs)((Message)null).obj;
        stringBuilder.hideSoftInputWithToken(((Message)null).arg1, (ResultReceiver)someArgs2.arg2, (IBinder)someArgs2.arg1);
        someArgs2.recycle();
        return;
      case 60:
        someArgs2 = (SomeArgs)((Message)null).obj;
        stringBuilder.showSoftInputWithToken(((Message)null).arg1, (ResultReceiver)someArgs2.arg2, (IBinder)someArgs2.arg1);
        someArgs2.recycle();
        return;
      case 50:
        stringBuilder.revokeSession((InputMethodSession)((Message)null).obj);
        return;
      case 45:
        inputMethodSession = (InputMethodSession)((Message)null).obj;
        if (((Message)null).arg1 == 0)
          bool1 = false; 
        stringBuilder.setSessionEnabled(inputMethodSession, bool1);
        return;
      case 40:
        null = (SomeArgs)((Message)null).obj;
        stringBuilder.createSession(new InputMethodSessionCallbackWrapper(this.mContext, (InputChannel)null.arg1, (IInputSessionCallback)null.arg2));
        null.recycle();
        return;
      case 32:
        someArgs3 = (SomeArgs)((Message)null).obj;
        iBinder = (IBinder)someArgs3.arg1;
        iInputContext = (IInputContext)someArgs3.arg2;
        editorInfo = (EditorInfo)someArgs3.arg3;
        null = (CancellationGroup)someArgs3.arg4;
        someArgs1 = (SomeArgs)someArgs3.arg5;
        if (iInputContext != null) {
          InputConnectionWrapper inputConnectionWrapper = new InputConnectionWrapper(this.mTarget, iInputContext, someArgs1.argi3, null);
        } else {
          null = null;
        } 
        editorInfo.makeCompatible(this.mTargetSdkVersion);
        if (someArgs1.argi1 == 1) {
          bool1 = true;
        } else {
          bool1 = false;
        } 
        if (someArgs1.argi2 == 1) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        stringBuilder.dispatchStartInputWithToken((InputConnection)null, editorInfo, bool1, iBinder, bool2);
        someArgs3.recycle();
        someArgs1.recycle();
        return;
      case 30:
        stringBuilder.unbindInput();
        return;
      case 20:
        stringBuilder.bindInput((InputBinding)((Message)null).obj);
        return;
      case 10:
        someArgs1 = (SomeArgs)((Message)null).obj;
        try {
          stringBuilder.initializeInternal((IBinder)someArgs1.arg1, ((Message)null).arg1, (IInputMethodPrivilegedOperations)someArgs1.arg2);
          return;
        } finally {
          someArgs1.recycle();
        } 
      case 1:
        break;
    } 
    AbstractInputMethodService abstractInputMethodService = this.mTarget.get();
    if (abstractInputMethodService == null)
      return; 
    null = (SomeArgs)((Message)null).obj;
    try {
      abstractInputMethodService.dump((FileDescriptor)null.arg1, (PrintWriter)null.arg2, (String[])null.arg3);
    } catch (RuntimeException runtimeException) {
      PrintWriter printWriter = (PrintWriter)null.arg2;
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Exception: ");
      stringBuilder1.append(runtimeException);
      printWriter.println(stringBuilder1.toString());
    } 
    synchronized (null.arg4) {
      ((CountDownLatch)null.arg4).countDown();
      null.recycle();
      return;
    } 
  }
  
  protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    String str;
    AbstractInputMethodService abstractInputMethodService = this.mTarget.get();
    if (abstractInputMethodService == null)
      return; 
    if (abstractInputMethodService.checkCallingOrSelfPermission("android.permission.DUMP") != 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Permission Denial: can't dump InputMethodManager from from pid=");
      stringBuilder.append(Binder.getCallingPid());
      stringBuilder.append(", uid=");
      stringBuilder.append(Binder.getCallingUid());
      str = stringBuilder.toString();
      paramPrintWriter.println(str);
      return;
    } 
    CountDownLatch countDownLatch = new CountDownLatch(1);
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageOOOO(1, str, paramPrintWriter, paramArrayOfString, countDownLatch));
    try {
      if (!countDownLatch.await(5L, TimeUnit.SECONDS))
        paramPrintWriter.println("Timeout waiting for dump"); 
    } catch (InterruptedException interruptedException) {
      paramPrintWriter.println("Interrupted waiting for dump");
    } 
  }
  
  public void initializeInternal(IBinder paramIBinder, int paramInt, IInputMethodPrivilegedOperations paramIInputMethodPrivilegedOperations) {
    HandlerCaller handlerCaller = this.mCaller;
    Message message = handlerCaller.obtainMessageIOO(10, paramInt, paramIBinder, paramIInputMethodPrivilegedOperations);
    handlerCaller.executeOrSendMessage(message);
  }
  
  public void onCreateInlineSuggestionsRequest(InlineSuggestionsRequestInfo paramInlineSuggestionsRequestInfo, IInlineSuggestionsRequestCallback paramIInlineSuggestionsRequestCallback) {
    HandlerCaller handlerCaller = this.mCaller;
    Message message = handlerCaller.obtainMessageOO(90, paramInlineSuggestionsRequestInfo, paramIInlineSuggestionsRequestCallback);
    handlerCaller.executeOrSendMessage(message);
  }
  
  public void bindInput(InputBinding paramInputBinding) {
    if (this.mCancellationGroup != null)
      Log.e("InputMethodWrapper", "bindInput must be paired with unbindInput."); 
    this.mCancellationGroup = new CancellationGroup();
    WeakReference<AbstractInputMethodService> weakReference = this.mTarget;
    InputConnectionWrapper inputConnectionWrapper = new InputConnectionWrapper(weakReference, IInputContext.Stub.asInterface(paramInputBinding.getConnectionToken()), 0, this.mCancellationGroup);
    InputBinding inputBinding = new InputBinding((InputConnection)inputConnectionWrapper, paramInputBinding);
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageO(20, inputBinding));
  }
  
  public void unbindInput() {
    CancellationGroup cancellationGroup = this.mCancellationGroup;
    if (cancellationGroup != null) {
      cancellationGroup.cancelAll();
      this.mCancellationGroup = null;
    } else {
      Log.e("InputMethodWrapper", "unbindInput must be paired with bindInput.");
    } 
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessage(30));
  }
  
  public void startInput(IBinder paramIBinder, IInputContext paramIInputContext, int paramInt, EditorInfo paramEditorInfo, boolean paramBoolean1, boolean paramBoolean2) {
    if (this.mCancellationGroup == null) {
      Log.e("InputMethodWrapper", "startInput must be called after bindInput.");
      this.mCancellationGroup = new CancellationGroup();
    } 
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.argi1 = paramBoolean1;
    someArgs.argi2 = paramBoolean2;
    someArgs.argi3 = paramInt;
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageOOOOO(32, paramIBinder, paramIInputContext, paramEditorInfo, this.mCancellationGroup, someArgs));
  }
  
  public void createSession(InputChannel paramInputChannel, IInputSessionCallback paramIInputSessionCallback) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageOO(40, paramInputChannel, paramIInputSessionCallback));
  }
  
  public void setSessionEnabled(IInputMethodSession paramIInputMethodSession, boolean paramBoolean) {
    try {
      boolean bool;
      IInputMethodSessionWrapper iInputMethodSessionWrapper = (IInputMethodSessionWrapper)paramIInputMethodSession;
      InputMethodSession inputMethodSession = iInputMethodSessionWrapper.getInternalInputMethodSession();
      if (inputMethodSession == null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Session is already finished: ");
        stringBuilder.append(paramIInputMethodSession);
        Log.w("InputMethodWrapper", stringBuilder.toString());
        return;
      } 
      HandlerCaller handlerCaller1 = this.mCaller, handlerCaller2 = this.mCaller;
      if (paramBoolean) {
        bool = true;
      } else {
        bool = false;
      } 
      handlerCaller1.executeOrSendMessage(handlerCaller2.obtainMessageIO(45, bool, inputMethodSession));
    } catch (ClassCastException classCastException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Incoming session not of correct type: ");
      stringBuilder.append(paramIInputMethodSession);
      Log.w("InputMethodWrapper", stringBuilder.toString(), classCastException);
    } 
  }
  
  public void revokeSession(IInputMethodSession paramIInputMethodSession) {
    try {
      StringBuilder stringBuilder;
      IInputMethodSessionWrapper iInputMethodSessionWrapper = (IInputMethodSessionWrapper)paramIInputMethodSession;
      InputMethodSession inputMethodSession = iInputMethodSessionWrapper.getInternalInputMethodSession();
      if (inputMethodSession == null) {
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Session is already finished: ");
        stringBuilder.append(paramIInputMethodSession);
        Log.w("InputMethodWrapper", stringBuilder.toString());
        return;
      } 
      this.mCaller.executeOrSendMessage(this.mCaller.obtainMessageO(50, stringBuilder));
    } catch (ClassCastException classCastException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Incoming session not of correct type: ");
      stringBuilder.append(paramIInputMethodSession);
      Log.w("InputMethodWrapper", stringBuilder.toString(), classCastException);
    } 
  }
  
  public void showSoftInput(IBinder paramIBinder, int paramInt, ResultReceiver paramResultReceiver) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageIOO(60, paramInt, paramIBinder, paramResultReceiver));
  }
  
  public void hideSoftInput(IBinder paramIBinder, int paramInt, ResultReceiver paramResultReceiver) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageIOO(70, paramInt, paramIBinder, paramResultReceiver));
  }
  
  public void changeInputMethodSubtype(InputMethodSubtype paramInputMethodSubtype) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageO(80, paramInputMethodSubtype));
  }
}
