package android.inputmethodservice;

import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import android.view.autofill.AutofillId;
import android.view.inputmethod.InlineSuggestionsRequest;
import android.view.inputmethod.InlineSuggestionsResponse;
import com.android.internal.view.IInlineSuggestionsRequestCallback;
import com.android.internal.view.InlineSuggestionsRequestInfo;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

class InlineSuggestionSessionController {
  private static final String TAG = "InlineSuggestionSessionController";
  
  private final Supplier<IBinder> mHostInputTokenSupplier;
  
  private AutofillId mImeClientFieldId;
  
  private String mImeClientPackageName;
  
  private boolean mImeInputStarted;
  
  private boolean mImeInputViewStarted;
  
  private final Handler mMainThreadHandler = new Handler(Looper.getMainLooper(), null, true);
  
  private final Function<Bundle, InlineSuggestionsRequest> mRequestSupplier;
  
  private final Consumer<InlineSuggestionsResponse> mResponseConsumer;
  
  private InlineSuggestionSession mSession;
  
  InlineSuggestionSessionController(Function<Bundle, InlineSuggestionsRequest> paramFunction, Supplier<IBinder> paramSupplier, Consumer<InlineSuggestionsResponse> paramConsumer) {
    this.mRequestSupplier = paramFunction;
    this.mHostInputTokenSupplier = paramSupplier;
    this.mResponseConsumer = paramConsumer;
  }
  
  void onMakeInlineSuggestionsRequest(InlineSuggestionsRequestInfo paramInlineSuggestionsRequestInfo, IInlineSuggestionsRequestCallback paramIInlineSuggestionsRequestCallback) {
    if (InputMethodService.DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onMakeInlineSuggestionsRequest: ");
      stringBuilder.append(paramInlineSuggestionsRequestInfo);
      Log.d("InlineSuggestionSessionController", stringBuilder.toString());
    } 
    InlineSuggestionSession inlineSuggestionSession2 = this.mSession;
    if (inlineSuggestionSession2 != null)
      inlineSuggestionSession2.invalidate(); 
    InlineSuggestionSession inlineSuggestionSession1 = new InlineSuggestionSession(paramInlineSuggestionsRequestInfo, paramIInlineSuggestionsRequestCallback, this.mRequestSupplier, this.mHostInputTokenSupplier, this.mResponseConsumer, this, this.mMainThreadHandler);
    if (this.mImeInputStarted && match(inlineSuggestionSession1.getRequestInfo())) {
      this.mSession.makeInlineSuggestionRequestUncheck();
      if (this.mImeInputViewStarted)
        try {
          this.mSession.getRequestCallback().onInputMethodStartInputView();
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("onInputMethodStartInputView() remote exception:");
          stringBuilder.append(remoteException);
          Log.w("InlineSuggestionSessionController", stringBuilder.toString());
        }  
    } 
  }
  
  void notifyOnStartInput(String paramString, AutofillId paramAutofillId) {
    if (InputMethodService.DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("notifyOnStartInput: ");
      stringBuilder.append(paramString);
      stringBuilder.append(", ");
      stringBuilder.append(paramAutofillId);
      Log.d("InlineSuggestionSessionController", stringBuilder.toString());
    } 
    if (paramString == null || paramAutofillId == null)
      return; 
    this.mImeInputStarted = true;
    this.mImeClientPackageName = paramString;
    this.mImeClientFieldId = paramAutofillId;
    InlineSuggestionSession inlineSuggestionSession = this.mSession;
    if (inlineSuggestionSession != null) {
      inlineSuggestionSession.consumeInlineSuggestionsResponse(InlineSuggestionSession.EMPTY_RESPONSE);
      if (!this.mSession.isCallbackInvoked() && match(this.mSession.getRequestInfo())) {
        this.mSession.makeInlineSuggestionRequestUncheck();
      } else if (this.mSession.shouldSendImeStatus()) {
        try {
          this.mSession.getRequestCallback().onInputMethodStartInput(this.mImeClientFieldId);
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("onInputMethodStartInput() remote exception:");
          stringBuilder.append(remoteException);
          Log.w("InlineSuggestionSessionController", stringBuilder.toString());
        } 
      } 
    } 
  }
  
  void notifyOnShowInputRequested(boolean paramBoolean) {
    if (InputMethodService.DEBUG)
      Log.d("InlineSuggestionSessionController", "notifyShowInputRequested"); 
    InlineSuggestionSession inlineSuggestionSession = this.mSession;
    if (inlineSuggestionSession != null && inlineSuggestionSession.shouldSendImeStatus())
      try {
        this.mSession.getRequestCallback().onInputMethodShowInputRequested(paramBoolean);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onInputMethodShowInputRequested() remote exception:");
        stringBuilder.append(remoteException);
        Log.w("InlineSuggestionSessionController", stringBuilder.toString());
      }  
  }
  
  void notifyOnStartInputView() {
    if (InputMethodService.DEBUG)
      Log.d("InlineSuggestionSessionController", "notifyOnStartInputView"); 
    this.mImeInputViewStarted = true;
    InlineSuggestionSession inlineSuggestionSession = this.mSession;
    if (inlineSuggestionSession != null && inlineSuggestionSession.shouldSendImeStatus())
      try {
        this.mSession.getRequestCallback().onInputMethodStartInputView();
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onInputMethodStartInputView() remote exception:");
        stringBuilder.append(remoteException);
        Log.w("InlineSuggestionSessionController", stringBuilder.toString());
      }  
  }
  
  void notifyOnFinishInputView() {
    if (InputMethodService.DEBUG)
      Log.d("InlineSuggestionSessionController", "notifyOnFinishInputView"); 
    this.mImeInputViewStarted = false;
    InlineSuggestionSession inlineSuggestionSession = this.mSession;
    if (inlineSuggestionSession != null && inlineSuggestionSession.shouldSendImeStatus())
      try {
        this.mSession.getRequestCallback().onInputMethodFinishInputView();
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onInputMethodFinishInputView() remote exception:");
        stringBuilder.append(remoteException);
        Log.w("InlineSuggestionSessionController", stringBuilder.toString());
      }  
  }
  
  void notifyOnFinishInput() {
    if (InputMethodService.DEBUG)
      Log.d("InlineSuggestionSessionController", "notifyOnFinishInput"); 
    this.mImeClientPackageName = null;
    this.mImeClientFieldId = null;
    this.mImeInputViewStarted = false;
    this.mImeInputStarted = false;
    InlineSuggestionSession inlineSuggestionSession = this.mSession;
    if (inlineSuggestionSession != null && inlineSuggestionSession.shouldSendImeStatus())
      try {
        this.mSession.getRequestCallback().onInputMethodFinishInput();
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onInputMethodFinishInput() remote exception:");
        stringBuilder.append(remoteException);
        Log.w("InlineSuggestionSessionController", stringBuilder.toString());
      }  
  }
  
  boolean match(InlineSuggestionsRequestInfo paramInlineSuggestionsRequestInfo) {
    return match(paramInlineSuggestionsRequestInfo, this.mImeClientPackageName, this.mImeClientFieldId);
  }
  
  boolean match(AutofillId paramAutofillId) {
    return match(paramAutofillId, this.mImeClientFieldId);
  }
  
  private static boolean match(InlineSuggestionsRequestInfo paramInlineSuggestionsRequestInfo, String paramString, AutofillId paramAutofillId) {
    boolean bool = false;
    if (paramInlineSuggestionsRequestInfo == null || paramString == null || paramAutofillId == null)
      return false; 
    if (paramInlineSuggestionsRequestInfo.getComponentName().getPackageName().equals(paramString) && 
      match(paramInlineSuggestionsRequestInfo.getAutofillId(), paramAutofillId))
      bool = true; 
    return bool;
  }
  
  private static boolean match(AutofillId paramAutofillId1, AutofillId paramAutofillId2) {
    boolean bool;
    if (paramAutofillId1 != null && paramAutofillId2 != null && 
      paramAutofillId1.getViewId() == paramAutofillId2.getViewId()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
