package android.inputmethodservice;

import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ResultReceiver;
import android.view.InputChannel;
import android.view.InputEvent;
import android.view.InputEventReceiver;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CursorAnchorInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.InputConnection;
import com.android.internal.inputmethod.CancellationGroup;
import com.android.internal.inputmethod.IMultiClientInputMethodSession;
import com.android.internal.os.SomeArgs;
import com.android.internal.util.function.TriConsumer;
import com.android.internal.util.function.pooled.PooledLambda;
import com.android.internal.view.IInputContext;
import com.android.internal.view.IInputMethodSession;
import com.android.internal.view.InputConnectionWrapper;
import java.lang.ref.WeakReference;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

final class MultiClientInputMethodClientCallbackAdaptor {
  static final String TAG = MultiClientInputMethodClientCallbackAdaptor.class.getSimpleName();
  
  private final Object mSessionLock = new Object();
  
  private final CancellationGroup mCancellationGroup = new CancellationGroup();
  
  static final boolean DEBUG = false;
  
  CallbackImpl mCallbackImpl;
  
  KeyEvent.DispatcherState mDispatcherState;
  
  Handler mHandler;
  
  InputEventReceiver mInputEventReceiver;
  
  InputChannel mReadChannel;
  
  IInputMethodSession.Stub createIInputMethodSession() {
    synchronized (this.mSessionLock) {
      InputMethodSessionImpl inputMethodSessionImpl = new InputMethodSessionImpl();
      this(this.mSessionLock, this.mCallbackImpl, this.mHandler, this.mCancellationGroup);
      return inputMethodSessionImpl;
    } 
  }
  
  IMultiClientInputMethodSession.Stub createIMultiClientInputMethodSession() {
    synchronized (this.mSessionLock) {
      MultiClientInputMethodSessionImpl multiClientInputMethodSessionImpl = new MultiClientInputMethodSessionImpl();
      this(this.mSessionLock, this.mCallbackImpl, this.mHandler, this.mCancellationGroup);
      return multiClientInputMethodSessionImpl;
    } 
  }
  
  MultiClientInputMethodClientCallbackAdaptor(MultiClientInputMethodServiceDelegate.ClientCallback paramClientCallback, Looper paramLooper, KeyEvent.DispatcherState paramDispatcherState, InputChannel paramInputChannel) {
    synchronized (this.mSessionLock) {
      CallbackImpl callbackImpl2 = new CallbackImpl();
      this(this, paramClientCallback);
      this.mCallbackImpl = callbackImpl2;
      this.mDispatcherState = paramDispatcherState;
      Handler handler = new Handler();
      this(paramLooper, null, true);
      this.mHandler = handler;
      this.mReadChannel = paramInputChannel;
      ImeInputEventReceiver imeInputEventReceiver = new ImeInputEventReceiver();
      paramLooper = handler.getLooper();
      CancellationGroup cancellationGroup = this.mCancellationGroup;
      KeyEvent.DispatcherState dispatcherState = this.mDispatcherState;
      CallbackImpl callbackImpl1 = this.mCallbackImpl;
      this(paramInputChannel, paramLooper, cancellationGroup, dispatcherState, callbackImpl1.mOriginalCallback);
      this.mInputEventReceiver = imeInputEventReceiver;
      return;
    } 
  }
  
  class KeyEventCallbackAdaptor implements KeyEvent.Callback {
    private final MultiClientInputMethodServiceDelegate.ClientCallback mLocalCallback;
    
    KeyEventCallbackAdaptor(MultiClientInputMethodClientCallbackAdaptor this$0) {
      this.mLocalCallback = (MultiClientInputMethodServiceDelegate.ClientCallback)this$0;
    }
    
    public boolean onKeyDown(int param1Int, KeyEvent param1KeyEvent) {
      return this.mLocalCallback.onKeyDown(param1Int, param1KeyEvent);
    }
    
    public boolean onKeyLongPress(int param1Int, KeyEvent param1KeyEvent) {
      return this.mLocalCallback.onKeyLongPress(param1Int, param1KeyEvent);
    }
    
    public boolean onKeyUp(int param1Int, KeyEvent param1KeyEvent) {
      return this.mLocalCallback.onKeyUp(param1Int, param1KeyEvent);
    }
    
    public boolean onKeyMultiple(int param1Int1, int param1Int2, KeyEvent param1KeyEvent) {
      return this.mLocalCallback.onKeyMultiple(param1Int1, param1KeyEvent);
    }
  }
  
  class ImeInputEventReceiver extends InputEventReceiver {
    private final CancellationGroup mCancellationGroupOnFinishSession;
    
    private final MultiClientInputMethodServiceDelegate.ClientCallback mClientCallback;
    
    private final KeyEvent.DispatcherState mDispatcherState;
    
    private final MultiClientInputMethodClientCallbackAdaptor.KeyEventCallbackAdaptor mKeyEventCallbackAdaptor;
    
    ImeInputEventReceiver(MultiClientInputMethodClientCallbackAdaptor this$0, Looper param1Looper, CancellationGroup param1CancellationGroup, KeyEvent.DispatcherState param1DispatcherState, MultiClientInputMethodServiceDelegate.ClientCallback param1ClientCallback) {
      super((InputChannel)this$0, param1Looper);
      this.mCancellationGroupOnFinishSession = param1CancellationGroup;
      this.mDispatcherState = param1DispatcherState;
      this.mClientCallback = param1ClientCallback;
      this.mKeyEventCallbackAdaptor = new MultiClientInputMethodClientCallbackAdaptor.KeyEventCallbackAdaptor(param1ClientCallback);
    }
    
    public void onInputEvent(InputEvent param1InputEvent) {
      if (this.mCancellationGroupOnFinishSession.isCanceled()) {
        finishInputEvent(param1InputEvent, false);
        return;
      } 
      try {
        boolean bool;
        if (param1InputEvent instanceof KeyEvent) {
          KeyEvent keyEvent = (KeyEvent)param1InputEvent;
          bool = keyEvent.dispatch(this.mKeyEventCallbackAdaptor, this.mDispatcherState, this.mKeyEventCallbackAdaptor);
        } else {
          MotionEvent motionEvent = (MotionEvent)param1InputEvent;
          if (motionEvent.isFromSource(4)) {
            bool = this.mClientCallback.onTrackballEvent(motionEvent);
          } else {
            bool = this.mClientCallback.onGenericMotionEvent(motionEvent);
          } 
        } 
        return;
      } finally {
        finishInputEvent(param1InputEvent, false);
      } 
    }
  }
  
  class InputMethodSessionImpl extends IInputMethodSession.Stub {
    private MultiClientInputMethodClientCallbackAdaptor.CallbackImpl mCallbackImpl;
    
    private final CancellationGroup mCancellationGroupOnFinishSession;
    
    private Handler mHandler;
    
    private final Object mSessionLock;
    
    InputMethodSessionImpl(MultiClientInputMethodClientCallbackAdaptor this$0, MultiClientInputMethodClientCallbackAdaptor.CallbackImpl param1CallbackImpl, Handler param1Handler, CancellationGroup param1CancellationGroup) {
      this.mSessionLock = this$0;
      this.mCallbackImpl = param1CallbackImpl;
      this.mHandler = param1Handler;
      this.mCancellationGroupOnFinishSession = param1CancellationGroup;
    }
    
    public void updateExtractedText(int param1Int, ExtractedText param1ExtractedText) {
      MultiClientInputMethodClientCallbackAdaptor.reportNotSupported();
    }
    
    public void updateSelection(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6) {
      synchronized (this.mSessionLock) {
        if (this.mCallbackImpl == null || this.mHandler == null)
          return; 
        SomeArgs someArgs = SomeArgs.obtain();
        someArgs.argi1 = param1Int1;
        someArgs.argi2 = param1Int2;
        someArgs.argi3 = param1Int3;
        someArgs.argi4 = param1Int4;
        someArgs.argi5 = param1Int5;
        someArgs.argi6 = param1Int6;
        this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$zVy_pAXuQfncxA_AL_8DWyVpYXc.INSTANCE, this.mCallbackImpl, someArgs));
        return;
      } 
    }
    
    public void viewClicked(boolean param1Boolean) {
      MultiClientInputMethodClientCallbackAdaptor.reportNotSupported();
    }
    
    public void updateCursor(Rect param1Rect) {
      MultiClientInputMethodClientCallbackAdaptor.reportNotSupported();
    }
    
    public void displayCompletions(CompletionInfo[] param1ArrayOfCompletionInfo) {
      synchronized (this.mSessionLock) {
        if (this.mCallbackImpl == null || this.mHandler == null)
          return; 
        this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$RawqPImrGiEy8dXqjapbiFcFS9w.INSTANCE, this.mCallbackImpl, param1ArrayOfCompletionInfo));
        return;
      } 
    }
    
    public void appPrivateCommand(String param1String, Bundle param1Bundle) {
      synchronized (this.mSessionLock) {
        if (this.mCallbackImpl == null || this.mHandler == null)
          return; 
        this.mHandler.sendMessage(PooledLambda.obtainMessage((TriConsumer)_$$Lambda$nzQNVb4Z0e33hB95nNP1BM_A3r4.INSTANCE, this.mCallbackImpl, param1String, param1Bundle));
        return;
      } 
    }
    
    public void toggleSoftInput(int param1Int1, int param1Int2) {
      synchronized (this.mSessionLock) {
        if (this.mCallbackImpl == null || this.mHandler == null)
          return; 
        Handler handler = this.mHandler;
        -$.Lambda.GapYa6Lyify6RwP-rgkklzmDV8I gapYa6Lyify6RwP-rgkklzmDV8I = _$$Lambda$GapYa6Lyify6RwP_rgkklzmDV8I.INSTANCE;
        MultiClientInputMethodClientCallbackAdaptor.CallbackImpl callbackImpl = this.mCallbackImpl;
        handler.sendMessage(PooledLambda.obtainMessage((TriConsumer)gapYa6Lyify6RwP-rgkklzmDV8I, callbackImpl, Integer.valueOf(param1Int1), Integer.valueOf(param1Int2)));
        return;
      } 
    }
    
    public void finishSession() {
      synchronized (this.mSessionLock) {
        if (this.mCallbackImpl == null || this.mHandler == null)
          return; 
        this.mCancellationGroupOnFinishSession.cancelAll();
        this.mHandler.sendMessage(PooledLambda.obtainMessage((Consumer)_$$Lambda$50K3nJOOPDYkhKRI6jLQ5NjnbLU.INSTANCE, this.mCallbackImpl));
        this.mCallbackImpl = null;
        this.mHandler = null;
        return;
      } 
    }
    
    public void updateCursorAnchorInfo(CursorAnchorInfo param1CursorAnchorInfo) {
      synchronized (this.mSessionLock) {
        if (this.mCallbackImpl == null || this.mHandler == null)
          return; 
        this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$BAvs3tw1MzE4gOJqYOA5MCJasPE.INSTANCE, this.mCallbackImpl, param1CursorAnchorInfo));
        return;
      } 
    }
    
    public final void notifyImeHidden() {
      MultiClientInputMethodClientCallbackAdaptor.reportNotSupported();
    }
    
    public void removeImeSurface() {
      MultiClientInputMethodClientCallbackAdaptor.reportNotSupported();
    }
  }
  
  class MultiClientInputMethodSessionImpl extends IMultiClientInputMethodSession.Stub {
    private MultiClientInputMethodClientCallbackAdaptor.CallbackImpl mCallbackImpl;
    
    private final CancellationGroup mCancellationGroupOnFinishSession;
    
    private Handler mHandler;
    
    private final Object mSessionLock;
    
    MultiClientInputMethodSessionImpl(MultiClientInputMethodClientCallbackAdaptor this$0, MultiClientInputMethodClientCallbackAdaptor.CallbackImpl param1CallbackImpl, Handler param1Handler, CancellationGroup param1CancellationGroup) {
      this.mSessionLock = this$0;
      this.mCallbackImpl = param1CallbackImpl;
      this.mHandler = param1Handler;
      this.mCancellationGroupOnFinishSession = param1CancellationGroup;
    }
    
    public void startInputOrWindowGainedFocus(IInputContext param1IInputContext, int param1Int1, EditorInfo param1EditorInfo, int param1Int2, int param1Int3, int param1Int4) {
      synchronized (this.mSessionLock) {
        InputConnectionWrapper inputConnectionWrapper;
        if (this.mCallbackImpl == null || this.mHandler == null)
          return; 
        SomeArgs someArgs = SomeArgs.obtain();
        WeakReference weakReference = new WeakReference();
        IInputContext iInputContext = null;
        this(null);
        if (param1IInputContext == null) {
          param1IInputContext = iInputContext;
        } else {
          inputConnectionWrapper = new InputConnectionWrapper(weakReference, param1IInputContext, param1Int1, this.mCancellationGroupOnFinishSession);
        } 
        someArgs.arg1 = inputConnectionWrapper;
        someArgs.arg2 = param1EditorInfo;
        someArgs.argi1 = param1Int2;
        someArgs.argi2 = param1Int3;
        someArgs.argi3 = param1Int4;
        this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$Xt9K6cDxkSefTfR7zi9ni_dRFZ8.INSTANCE, this.mCallbackImpl, someArgs));
        return;
      } 
    }
    
    public void showSoftInput(int param1Int, ResultReceiver param1ResultReceiver) {
      synchronized (this.mSessionLock) {
        if (this.mCallbackImpl == null || this.mHandler == null)
          return; 
        Handler handler = this.mHandler;
        -$.Lambda.m1uOlwS-mRsg9KSUY6vV9l9ksWc m1uOlwS-mRsg9KSUY6vV9l9ksWc = _$$Lambda$m1uOlwS_mRsg9KSUY6vV9l9ksWc.INSTANCE;
        MultiClientInputMethodClientCallbackAdaptor.CallbackImpl callbackImpl = this.mCallbackImpl;
        handler.sendMessage(PooledLambda.obtainMessage((TriConsumer)m1uOlwS-mRsg9KSUY6vV9l9ksWc, callbackImpl, Integer.valueOf(param1Int), param1ResultReceiver));
        return;
      } 
    }
    
    public void hideSoftInput(int param1Int, ResultReceiver param1ResultReceiver) {
      synchronized (this.mSessionLock) {
        if (this.mCallbackImpl == null || this.mHandler == null)
          return; 
        Handler handler = this.mHandler;
        -$.Lambda.tnQSRQlZ73hLobz1ZfjUIoiCl0 tnQSRQlZ73hLobz1ZfjUIoiCl0 = _$$Lambda$0tnQSRQlZ73hLobz1ZfjUIoiCl0.INSTANCE;
        MultiClientInputMethodClientCallbackAdaptor.CallbackImpl callbackImpl = this.mCallbackImpl;
        handler.sendMessage(PooledLambda.obtainMessage((TriConsumer)tnQSRQlZ73hLobz1ZfjUIoiCl0, callbackImpl, Integer.valueOf(param1Int), param1ResultReceiver));
        return;
      } 
    }
  }
  
  private static final class CallbackImpl {
    private final MultiClientInputMethodClientCallbackAdaptor mCallbackAdaptor;
    
    private boolean mFinished = false;
    
    private final MultiClientInputMethodServiceDelegate.ClientCallback mOriginalCallback;
    
    CallbackImpl(MultiClientInputMethodClientCallbackAdaptor param1MultiClientInputMethodClientCallbackAdaptor, MultiClientInputMethodServiceDelegate.ClientCallback param1ClientCallback) {
      this.mCallbackAdaptor = param1MultiClientInputMethodClientCallbackAdaptor;
      this.mOriginalCallback = param1ClientCallback;
    }
    
    void updateSelection(SomeArgs param1SomeArgs) {
      try {
        boolean bool = this.mFinished;
        if (bool)
          return; 
        this.mOriginalCallback.onUpdateSelection(param1SomeArgs.argi1, param1SomeArgs.argi2, param1SomeArgs.argi3, param1SomeArgs.argi4, param1SomeArgs.argi5, param1SomeArgs.argi6);
        return;
      } finally {
        param1SomeArgs.recycle();
      } 
    }
    
    void displayCompletions(CompletionInfo[] param1ArrayOfCompletionInfo) {
      if (this.mFinished)
        return; 
      this.mOriginalCallback.onDisplayCompletions(param1ArrayOfCompletionInfo);
    }
    
    void appPrivateCommand(String param1String, Bundle param1Bundle) {
      if (this.mFinished)
        return; 
      this.mOriginalCallback.onAppPrivateCommand(param1String, param1Bundle);
    }
    
    void toggleSoftInput(int param1Int1, int param1Int2) {
      if (this.mFinished)
        return; 
      this.mOriginalCallback.onToggleSoftInput(param1Int1, param1Int2);
    }
    
    void finishSession() {
      if (this.mFinished)
        return; 
      this.mFinished = true;
      this.mOriginalCallback.onFinishSession();
      synchronized (this.mCallbackAdaptor.mSessionLock) {
        this.mCallbackAdaptor.mDispatcherState = null;
        if (this.mCallbackAdaptor.mReadChannel != null) {
          this.mCallbackAdaptor.mReadChannel.dispose();
          this.mCallbackAdaptor.mReadChannel = null;
        } 
        this.mCallbackAdaptor.mInputEventReceiver = null;
        return;
      } 
    }
    
    void updateCursorAnchorInfo(CursorAnchorInfo param1CursorAnchorInfo) {
      if (this.mFinished)
        return; 
      this.mOriginalCallback.onUpdateCursorAnchorInfo(param1CursorAnchorInfo);
    }
    
    void startInputOrWindowGainedFocus(SomeArgs param1SomeArgs) {
      try {
        boolean bool = this.mFinished;
        if (bool)
          return; 
        InputConnectionWrapper inputConnectionWrapper = (InputConnectionWrapper)param1SomeArgs.arg1;
        EditorInfo editorInfo = (EditorInfo)param1SomeArgs.arg2;
        int i = param1SomeArgs.argi1;
        int j = param1SomeArgs.argi2;
        int k = param1SomeArgs.argi3;
        this.mOriginalCallback.onStartInputOrWindowGainedFocus((InputConnection)inputConnectionWrapper, editorInfo, i, j, k);
        return;
      } finally {
        param1SomeArgs.recycle();
      } 
    }
    
    void showSoftInput(int param1Int, ResultReceiver param1ResultReceiver) {
      if (this.mFinished)
        return; 
      this.mOriginalCallback.onShowSoftInput(param1Int, param1ResultReceiver);
    }
    
    void hideSoftInput(int param1Int, ResultReceiver param1ResultReceiver) {
      if (this.mFinished)
        return; 
      this.mOriginalCallback.onHideSoftInput(param1Int, param1ResultReceiver);
    }
  }
  
  private static void reportNotSupported() {}
}
